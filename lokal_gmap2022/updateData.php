<?php
//require_once("_Reuseable/budget_2014_dbCalling.php");
require_once("_Reuseable/ebudget_dbCalling.php");
require_once("_Reuseable/postgres_databaseCalling.php");

//untuk google map
$doc = new DOMDocument();
$doc->version = '1.0';
$doc->encoding = 'UTF-8';
$tag_gml = $doc->createElement('gml');
$doc->appendChild($tag_gml);
$res = pg_query($con, 'select * from ' . $skema_gis . '.geodata_road');

$maxLat = -7.189505;
$minLat = -7.393752;
$maxLong = 112.897099;
$minLong = 112.622609;
//800x600
// (-7.393752,112.622609)
// (-7.189505,112.897099)
//1000x1000
// (-7.376760,112.673905)
// (-7.206558,112.845811)
$map_width = 25600;
$map_height = 19200;
if (pg_num_rows($res) > 1) {
    while ($baris = pg_fetch_array($res)) {
        //jalan
        if ($baris['tipe'] == 1) {
            $latlong = $baris['latlong'];
            //$linestring;
            $temp = explode(" ", $latlong);
            //echo count($temp);

            $linestring = "";

            for ($i = 0; $i < count($temp); $i++) {
                $temp2 = explode(",", $temp[$i]);
                $yCoordinate = $map_height - ($temp2[0] - $minLat) * $map_height / ($maxLat - $minLat);
                $xCoordinate = ($temp2[1] - $minLong) * $map_width / ($maxLong - $minLong);
                $linestring = $linestring . " " . $xCoordinate . "," . $yCoordinate;
            }

            $temp2 = explode(",", $temp[0]);
            $yCoordinate = $map_height - ($temp2[0] - $minLat) * $map_height / ($maxLat - $minLat) + 5;
            $xCoordinate = ($temp2[1] - $minLong) * $map_width / ($maxLong - $minLong) + 5;

            $temp2 = explode(",", $temp[0]);
            $temp3 = explode(",", $temp[1]);
            if ($temp3[0] > $temp2[0]) {
                $temp4 = $temp3[0];
                $temp3[0] = $temp2[0];
                $temp2[0] = $temp4;
            }
            if ($temp3[1] > $temp2[1]) {
                $temp4 = $temp3[1];
                $temp3[1] = $temp2[1];
                $temp2[1] = $temp4;
            }
            $angle = round(atan(($temp3[0] - $temp2[0]) / ($temp3[1] - $temp2[1])) * 180 / 3.14, 0);

            $tag_place = $doc->createElement('road');
            $tag_id = $doc->createElement('id', $baris['id']);
            $tag_desc = $doc->createElement('desc', $baris['nama']);
            $tag_x = $doc->createElement('xCoordinate', $xCoordinate);
            $tag_y = $doc->createElement('yCoordinate', $yCoordinate);
            $tag_angle = $doc->createElement('angle', $angle);
            $tag_coordinates = $doc->createElement('lineString', $linestring);

            $tag_gml->appendChild($tag_place);
            $tag_place->appendChild($tag_id);
            $tag_place->appendchild($tag_coordinates);
            $tag_place->appendchild($tag_x);
            $tag_place->appendchild($tag_y);
            $tag_place->appendchild($tag_angle);
            $tag_place->appendchild($tag_desc);
        }
        //jembatan
        elseif ($baris['tipe'] == 2) {
            $temp = explode(",", $baris['latlong']);
            $yCoordinate = $map_height - ($temp[0] - $minLat) * $map_height / ($maxLat - $minLat) - 16;
            $xCoordinate = ($temp[1] - $minLong) * $map_width / ($maxLong - $minLong) - 16;
            $linestring = $xCoordinate . "," . $yCoordinate;

            $tag_place = $doc->createElement('bridge');
            $tag_id = $doc->createElement('id', $baris['id']);
            $tag_desc = $doc->createElement('desc', $baris['nama']);
            $tag_x = $doc->createElement('xCoordinate', $xCoordinate);
            $tag_y = $doc->createElement('yCoordinate', $yCoordinate);

            $tag_gml->appendChild($tag_place);
            $tag_place->appendchild($tag_id);
            $tag_place->appendchild($tag_desc);
            $tag_place->appendchild($tag_x);
            $tag_place->appendchild($tag_y);
        }
    }


    $doc->formatOutput = true;

//    $xslt = new xsltProcessor;
//    $xsl = new DOMDocument();
//    $xsl->load('LayerMap.xsl');
//    $xslt->importStyleSheet($xsl);
//    print $xslt->transformToXML($doc);
    //$doc->save('cek.gml');
} else {
    $tag_place = $doc->createElement('road');

    $tag_gml->appendChild($tag_place);

    $doc->formatOutput = true;

    //$xslt = new xsltProcessor;
//    $xsl = new DOMDocument();
//    $xsl->load('LayerMap.xsl');
    //$xslt->importStyleSheet($xsl);
    //print $xslt->transformToXML($doc);
    //$doc->save('cek.gml');
}

// GET SEMUA PARAMETER YANG DIDAPAT
$unit_id = $_GET["unit_id"];
$kegiatan_code = $_GET["kode_kegiatan"];
$detail_no = $_GET["detail_no"];
$satuan = $_GET["satuan"];
$volume = $_GET["volume"];
$nilai_anggaran = $_GET["nilai_anggaran"];
$tahun = $_GET["tahun"];
$mlokasi = $_GET["mlokasi"];
$id_kelompok = $_GET["id_kelompok"];
$th_load = $_GET["th_load"];
$level = $_GET["level"];
$nmuser = $_GET["nm_user"];

$total_lokasi = $_GET["total_lokasi"];
$lokasi_ke = $_GET["lokasi_ke"];

$command_unit = pg_query($conb, 'SELECT unit_name FROM "public"."unit_kerja" WHERE "unit_id"=\'' . $unit_id . '\' ');
$row_unit = pg_fetch_assoc($command_unit);
$nama_skpd = $row_unit['unit_name'];

$command = pg_query($conb, 'SELECT * FROM public.master_kelompok_gmap WHERE "id_kelompok"=\'' . $id_kelompok . '\' LIMIT 1');
$row = pg_fetch_assoc($command);
$tipe;
$tipe_objek = $row['tipe_objek'];

if ($tipe_objek == 1) {
    $tipe = 'Point';
} elseif ($tipe_objek == 2) {
    $tipe = 'LineString';
} elseif ($tipe_objek == 3) {
    $tipe = 'Polygon';
}

$warna = $row['warna'];
$nama_objek = $row['nama_objek'];

$command2 = pg_query($conb, 'SELECT * FROM ' . $skema_budget . '."rincian_detail" '
        . 'WHERE detail_no=\'' . $detail_no . '\' and kegiatan_code=\'' . $kegiatan_code . '\' and unit_id=\'' . $unit_id . '\' LIMIT 1');
$row2 = pg_fetch_assoc($command2);
$komponen_name = $row2['komponen_name'] . ' ' . $row2['detail_name'];
$volume_rd = $row2['volume'];
$satuan_rd = $row2['satuan'];

$res1 = pg_query($con, 'SELECT * FROM ' . $skema_gis . '.geojsonlokasi_rev1 '
        . 'WHERE "kegiatan_code"=\'' . $kegiatan_code . '\' AND "detail_no"=\'' . $detail_no . '\' AND "unit_id"=\'' . $unit_id . '\' AND "lokasi_ke"=\'' . $lokasi_ke . '\' LIMIT 1');
$baris1 = pg_fetch_assoc($res1);
$arr_data = $baris1['geojson'];
$keterangan = $baris1['keterangan'];
$jalanFix = $baris1['mlokasi'];
$keteranganAlamat = $baris1['keterangan_alamat'];

// Ambil semua data yang memiliki alamat yang sama, kalau tahunnya sama di letakkan di peta dengan alpha yang dikurangi, sedangkan untuk tahun sebelumnya hingga 5 tahun kebelakang di masukkan saja di semacam list box
$arr_data_lain = array();
$command3 = pg_query($con, 'SELECT * from ' . $skema_gis . '.geojsonlokasi_rev1 
    WHERE id_kelompok=\'' . $id_kelompok . '\' and status_hapus = false and idgeolokasi NOT IN(SELECT idgeolokasi 
        FROM ' . $skema_gis . '.geojsonlokasi_rev1 WHERE detail_no=\'' . $detail_no . '\' and kegiatan_code=\'' . $kegiatan_code . '\' and unit_id=\'' . $unit_id . '\' and lokasi_ke=\'' . $lokasi_ke . '\') 
            ORDER BY idgeolokasi DESC');
while ($row3 = pg_fetch_assoc($command3)) {
    $hasil = $row3;

    $command5 = pg_query($conb, 'SELECT * FROM ' . $skema_budget . '."rincian_detail" '
            . 'WHERE detail_no=\'' . $row3['detail_no'] . '\' and kegiatan_code=\'' . $row3['kegiatan_code'] . '\' and unit_id=\'' . $row3['unit_id'] . '\' LIMIT 1');
    $row5 = pg_fetch_assoc($command5);

    $command6 = pg_query($conb, 'SELECT * FROM public.master_kelompok_gmap WHERE "id_kelompok"=\'' . $row3['id_kelompok'] . '\' LIMIT 1');
    $row6 = pg_fetch_assoc($command6);
    $hasil['warna'] = $row6['warna'];
    $hasil['nama_objek'] = $row6['nama_objek'];
    $hasil['komponen_name'] = $row5['komponen_name'] . ' ' . $row5['detail_name'];
    $hasil['nilai_anggaran'] = number_format($row5['nilai_anggaran'], 0, ',', '.');

    $arr_data_lain[] = $hasil;
}

// cari max tahun yang di inputkan oleh user
$command4 = pg_query($con, 'SELECT max(tahun) as max1 FROM ' . $skema_gis . '.geojsonlokasi_rev1');
$row4 = pg_fetch_assoc($command4);
$maxtahun = $row4['max1'];

$command_lokasi = pg_query($conb, 'SELECT * FROM "public"."master_lokasi_gmap" order by nama_lokasi');
?>

<html>
    <head>
        <meta charset="UTF-8">
        <title>e-Budgeting - Pemkot Surabaya</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <link rel="icon" href="img/Ebudgeting.png" type="image/x-icon" />
        <!-- Bootstrap 3.3.2 -->
        <link href="AdminLTE/bootstrap/css/bootstrap.min.css" type="text/css" rel="stylesheet"/>
        <!-- Font Awesome Icons -->
        <link href="font-awesome/css/font-awesome.min.css" type="text/css" rel="stylesheet"/>
        <!-- Ionicons -->
        <link href="AdminLTE/ionicons/ionicons.min.css" type="text/css" rel="stylesheet"/>
        <!-- Theme style -->
        <link href="AdminLTE/dist/css/AdminLTE.min.css" type="text/css" rel="stylesheet"/>
        <!-- AdminLTE Skins. Choose a skin from the css/skins 
             folder instead of downloading all of them to reduce the load. -->
        <link href="AdminLTE/dist/css/skins/skin-blue.min.css" type="text/css" rel="stylesheet"/>

        <!-- jQuery 2.1.3 -->
        <script src="AdminLTE/plugins/jQuery/jQuery-2.1.3.min.js"></script>
        <!-- Bootstrap 3.3.2 JS -->
        <script src="AdminLTE/bootstrap/js/bootstrap.min.js"></script>
        <!-- FastClick -->
        <script src="AdminLTE/plugins/fastclick/fastclick.min.js"></script>
        <!-- AdminLTE App -->
        <script src="AdminLTE/dist/js/app.min.js"></script>

        <script type="text/javascript" src="_Reuseable/md5.js"></script>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <!-- <script src="https://maps.googleapis.com/maps/api/js?v=3.6&key=AIzaSyA-318W-HfokybWpB4CyBh7XoODjG89X9w&sensor=false&libraries=geometry,places"></script> -->
        <script src="https://maps.googleapis.com/maps/api/js?v=3.30&key=AIzaSyCP2Ldnd_jUTViNUVV4I_l_iRlZrbaqLeg&sensor=false&libraries=geometry,places"></script>

        <script>
            
            var hitungLuasan = 0;
            var countMark = 1;
            var map;
            var markersArray = [];
            var tipe;
            var unit_id;
            var kegiatan_code;
            var detail_no;
            var satuan;
            var volume;
            var nilai_anggaran;
            var tahun;
            var mlokasi;
            var id_kelompok;
            var nmuser;
            var level;
            var th_load;
            var keterangan;
            var keterangan_alamat;
            var warna = '<?php echo $warna; ?>';
            var maxtahun = '<?php echo $maxtahun ?>';
            // ISI DATA YANG LAIN
            var arr_data = new Array();
            var arr_json = new Array();
            var other_mark = new Array();
            var koordinat;

            var total_lokasi;
            var lokasi_ke;
<?php
foreach ($arr_data_lain as $data) {
    ?>
                var data1 = new Array();
                data1['idgeolokasi'] = '<?php echo $data['idgeolokasi'] ?>';
                data1['unit_id'] = '<?php echo $data['unit_id'] ?>';
                data1['kegiatan_code'] = '<?php echo $data['kegiatan_code'] ?>';
                data1['detail_no'] = '<?php echo $data['detail_no'] ?>';
                data1['satuan'] = '<?php echo $data['satuan'] ?>';
                data1['volume'] = '<?php echo $data['volume'] ?>';
                data1['nilai_anggaran'] = '<?php echo $data['nilai_anggaran'] ?>';
                data1['tahun'] = '<?php echo $data['tahun'] ?>';
                data1['mlokasi'] = '<?php echo $data['mlokasi'] ?>';
                data1['id_kelompok'] = '<?php echo $data['id_kelompok'] ?>';
                data1['keterangan'] = '<?php echo $data['keterangan'] ?>';
                data1['keterangan_alamat'] = '<?php echo $data['keterangan_alamat'] ?>';
                data1['nmuser'] = '<?php echo $data['nmuser'] ?>';
                data1['unit_name'] = '<?php echo $data['unit_name'] ?>';
                data1['komponen_name'] = '<?php echo $data['komponen_name'] ?>';
                data1['nama_objek'] = '<?php echo $data['nama_objek'] ?>';
                data1['warna'] = '<?php echo $data['warna'] ?>';
                data1['level'] = '<?php echo $data['level'] ?>';
                data1['keterangan_alamat'] = '<?php echo $data['keterangan_alamat'] ?>';
                arr_data.push(data1);
                arr_json.push($.parseJSON('<?php echo $data['geojson'] ?>'));
    <?php
}
?>

            $(document).ready(function() {
                // $('#inputTipeBaru').change(function() {
                // tipe=$('#inputTipeBaru').val();
                // //overlay.setMap(null);
                // //document.getElementById('hiddenLayer').appendChild(document.getElementById('layer1'));
                // initialize();
                // });
            });
            var bacaJson = $.parseJSON('<?php echo $arr_data; ?>');
            var geocoder;
            function codeAddress() {
                var sAddress = $("#searchTextField").val();
                sAddress = sAddress.replace('Jl. ', '') + ', Surabaya, East Java, Indonesia';
                geocoder.geocode({'address': sAddress}, function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        map.setCenter(results[0].geometry.location);
                        if (tipe == 'Point') {
                            markersArray[0].setPosition(results[0].geometry.location);
                        }
                        //alert('done');
                    }
                    else {
                        //jika tidak bisa lakukan sesuatu
                        //alert('err');
                        $("#searchTextField").val('Jimerto, Surabaya, East Java, Indonesia');
                        codeAddress();
                    }
                });
            }

            function setAllMap(map) {
                for (var i = 0; i < other_mark.length; i++) {
                    other_mark[i]['obj'].setMap(map);
                }
            }

            function initialize() {
                unit_id = '<?php echo $unit_id ?>';
                kegiatan_code = '<?php echo $kegiatan_code ?>';
                detail_no = '<?php echo $detail_no ?>';
                satuan = '<?php echo $satuan ?>';
                volume = '<?php echo $volume ?>';
                nilai_anggaran = '<?php echo $nilai_anggaran ?>';
                tahun = '<?php echo $tahun ?>';
                mlokasi = '<?php echo $mlokasi ?>';
                id_kelompok = '<?php echo $id_kelompok ?>';
                tipe = '<?php echo $tipe; ?>';
                nmuser = '<?php echo $nmuser ?>';
                level = '<?php echo $level ?>';
                th_load = '<?php echo $th_load ?>';
                keterangan = '<?php echo $keterangan ?>';
                keterangan_alamat = '<?php echo $keterangan_alamat ?>';
                koordinat;
                geocoder = new google.maps.Geocoder();

                total_lokasi = '<?php echo $total_lokasi ?>';
                lokasi_ke = '<?php echo $lokasi_ke ?>';

                hitungLuasan = 0;
                countMark = 1;
                markersArray = [];

                var areaOptions = {
                    strokeColor: '#FF0000',
                    strokeOpacity: 0.8,
                    strokeWeight: 3,
                    fillColor: '#FF0000',
                    fillOpacity: 0.35,
                    draggable: true
                }
                area = new google.maps.Polygon(areaOptions);

                var polyOptions = {
                    strokeColor: '#FF0000',
                    strokeOpacity: 1.0,
                    strokeWeight: 3
                }
                poly = new google.maps.Polyline(polyOptions);
                var newcenterX = -7.257822;
                var newcenterY = 112.746998;
                var maxX = -180;
                var minX = 180;
                var maxY = -180;
                var minY = 180;

                if (tipe != "Point") {
                    if (tipe == bacaJson.features[0].geometry.type) {
                        if (tipe == 'LineString') {
                            for (var i = 0; i < bacaJson.features[0].geometry.coordinates.length; i++) {
                                if (bacaJson.features[0].geometry.coordinates[i][0] > maxX) {
                                    maxX = bacaJson.features[0].geometry.coordinates[i][0];
                                }
                                if (bacaJson.features[0].geometry.coordinates[i][0] < minX) {
                                    minX = bacaJson.features[0].geometry.coordinates[i][0];
                                }
                                if (bacaJson.features[0].geometry.coordinates[i][1] > maxY) {
                                    maxY = bacaJson.features[0].geometry.coordinates[i][1];
                                }
                                if (bacaJson.features[0].geometry.coordinates[i][1] < minY) {
                                    minY = bacaJson.features[0].geometry.coordinates[i][1];
                                }
                            }
                        } else {
                            for (var i = 0; i < bacaJson.features[0].geometry.coordinates[0].length; i++) {
                                if (bacaJson.features[0].geometry.coordinates[0][i][0] > maxX) {
                                    maxX = bacaJson.features[0].geometry.coordinates[0][i][0];
                                }
                                if (bacaJson.features[0].geometry.coordinates[0][i][0] < minX) {
                                    minX = bacaJson.features[0].geometry.coordinates[0][i][0];
                                }
                                if (bacaJson.features[0].geometry.coordinates[0][i][1] > maxY) {
                                    maxY = bacaJson.features[0].geometry.coordinates[0][i][1];
                                }
                                if (bacaJson.features[0].geometry.coordinates[0][i][1] < minY) {
                                    minY = bacaJson.features[0].geometry.coordinates[0][i][1];
                                }
                            }
                        }
                        newcenterX = (minX + maxX) / 2;
                        newcenterY = (minY + maxY) / 2;
                    } else {
                        newcenterX = -7.257822;
                        newcenterY = 112.746998;
                    }
                } else {
                    if (tipe == bacaJson.features[0].geometry.type) {
                        newcenterX = bacaJson.features[0].geometry.coordinates[0];
                        newcenterY = bacaJson.features[0].geometry.coordinates[1];
                    } else {
                        newcenterX = -7.257822;
                        newcenterY = 112.746998;
                    }
                }

                var mapOptions = {
                    zoom: 17,
                    center: new google.maps.LatLng(newcenterX, newcenterY),
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };
                map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

                //searchplace robert
                var defaultbounds = new google.maps.LatLngBounds(
                        new google.maps.LatLng(-7.192188, 112.593956),
                        new google.maps.LatLng(-7.439411, 112.810936));
                var options = {
                    bounds: defaultbounds,
                    componentRestrictions: {country: 'id'}
                };

                //autocomplete
                var input = /** @type {HTMLInputElement} */(document.getElementById('searchTextField'));
                var autocomplete = new google.maps.places.Autocomplete(input, options);

                autocomplete.bindTo('bounds', map);

                var infowindow = new google.maps.InfoWindow();
                var marker = new google.maps.Marker({
                    map: map
                });

                google.maps.event.addListener(autocomplete, 'place_changed', function() {
                    infowindow.close();
                    marker.setVisible(false);
                    input.className = '';
                    var place = autocomplete.getPlace();
                    if (!place.geometry) {
                        // Inform the user that the place was not found and return.
                        input.className = 'notfound';
                        return;
                    }

                    // If the place has a geometry, then present it on a map.
                    if (place.geometry.viewport) {
                        map.fitBounds(place.geometry.viewport);
                    } else {
                        map.setCenter(place.geometry.location);
                        map.setZoom(17);  // Why 17? Because it looks good.
                    }
                    // marker.setIcon(/** @type {google.maps.Icon} */({
                    // url: place.icon,
                    // size: new google.maps.Size(71, 71),
                    // origin: new google.maps.Point(0, 0),
                    // anchor: new google.maps.Point(17, 34),
                    // scaledSize: new google.maps.Size(35, 35)
                    // }));
                    marker.setPosition(place.geometry.location);
                    marker.setVisible(true);

                    // var address = '';
                    // if (place.address_components) {
                    // address = [
                    // (place.address_components[0] && place.address_components[0].short_name || ''),
                    // (place.address_components[1] && place.address_components[1].short_name || ''),
                    // (place.address_components[2] && place.address_components[2].short_name || '')
                    // ].join(' ');
                    // }

                    // infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
                    // infowindow.open(map, marker);
                });
                //end searchplace robert
                //set bounding overlay --Marcell
                var swBound = new google.maps.LatLng(-7.393752, 112.622609);
                var neBound = new google.maps.LatLng(-7.189505, 112.897099);

                var bounds = new google.maps.LatLngBounds(swBound, neBound);
                //overlay = new USGSOverlay(bounds, map);

                if (tipe != "Point") {
                    if (tipe == bacaJson.features[0].geometry.type) {
                        if (tipe == 'LineString') {
                            for (var i = 0; i < bacaJson.features[0].geometry.coordinates.length; i++) {
                                var new_posXY = new google.maps.LatLng(bacaJson.features[0].geometry.coordinates[i][0], bacaJson.features[0].geometry.coordinates[i][1]);
                                addMarker(new_posXY);
                            }
                        } else {
                            for (var i = 0; i < bacaJson.features[0].geometry.coordinates[0].length; i++) {
                                var new_posXY = new google.maps.LatLng(bacaJson.features[0].geometry.coordinates[0][i][0], bacaJson.features[0].geometry.coordinates[0][i][1]);
                                addMarker(new_posXY);
                            }
                        }
                    }
                    google.maps.event.addListener(map, 'click', function(event) {
                        addMarker(event.latLng);
                    });
                } else {
                    if (tipe == bacaJson.features[0].geometry.type) {
                        marker = new google.maps.Marker({
                            position: new google.maps.LatLng(bacaJson.features[0].geometry.coordinates[0], bacaJson.features[0].geometry.coordinates[1]),
                            map: map,
                            draggable: true,
                            animation: google.maps.Animation.DROP,
                            title: countMark + ""
                        });
                    } else {
                        marker = new google.maps.Marker({
                            position: new google.maps.LatLng(-7.257822, 112.746998),
                            map: map,
                            draggable: true,
                            animation: google.maps.Animation.DROP,
                            title: countMark + ""
                        });
                    }
                    countMark++;
                    markersArray.push(marker);
                }

                if (tipe == 'Polygon') {
                    area.setMap(map);
                }

                if (tipe == 'LineString') {
                    poly.setMap(map);
                }

                google.maps.event.addListener(area, 'drag', dragArea);
                if (tipe == 'Point') {
                    $("#jarak_marker").css('display', 'block');
                    $("#jarak_marker font").html('1 Titik');
                } else if (tipe == 'LineString') {
                    $("#jarak_marker").css('display', 'block');
                    $("#jarak_marker font").html('0 M1');
                } else if (tipe == 'Polygon') {
                    $("#jarak_marker").css('display', 'block');
                    $("#jarak_marker font").html('0 M2');
                }
                hitungGeometry();
                isiDataKeMap();

                // $("#searchTextField").val(mlokasi);
                // codeAddress();
            }

            function dragArea() {
                var pathArea = area.getPath();
                //var pathPoly = poly.getPath();
                var besarArrayPathArea = pathArea.getArray().length;
                for (var i = 0; i < besarArrayPathArea; i++) {
                    var posXY = new google.maps.LatLng(pathArea.getArray()[i].lat(), pathArea.getArray()[i].lng());
                    markersArray[i].setPosition(posXY);
                    //pathPoly.setAt(i,posXY);
                }
                hitungGeometry();
            }

            function isiDataKeMap() {
                setAllMap(null);
                var infowindows = new Array();
                for (var i = 0; i < arr_data.length; i++) {
                    //isikan data ke map
                    var tipe1 = (arr_json[i].features[0].geometry.type);
                    if (tipe1 == 'Point') {
                        other_mark[i] = [];
                        other_mark[i]['obj'] = new google.maps.Marker({
                            position: new google.maps.LatLng(arr_json[i].features[0].geometry.coordinates[0], arr_json[i].features[0].geometry.coordinates[1]),
                            map: map,
                            draggable: false,
                            animation: google.maps.Animation.DROP,
                            icon: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=X|' + arr_data[i]['warna'].substr(1)
                        });
                        other_mark[i]['tipe'] = tipe1;
                    } else if (tipe1 == 'LineString') {
                        if (arr_data[i]['nilai_anggaran'] == 0 || arr_data[i]['volume'] == 0) {
                            var polyOptions = {
                                strokeColor: arr_data[i]['warna'],
                                strokeOpacity: 0,
                                strokeWeight: 3,
                                icons: [{
                                        icon: {
                                            path: 'M 0,-1 0,1',
                                            strokeOpacity: 0.7,
                                            scale: 4
                                        },
                                        offset: '0',
                                        repeat: '20px'
                                    }]
                            }
                        } else {
                            var polyOptions = {
                                strokeColor: arr_data[i]['warna'],
                                strokeWeight: 3
                            }
                        }

                        other_mark[i] = [];
                        other_mark[i]['obj'] = new google.maps.Polyline(polyOptions);
                        for (var j = 0; j < arr_json[i].features[0].geometry.coordinates.length; j++) {
                            var new_posXY = new google.maps.LatLng(arr_json[i].features[0].geometry.coordinates[j][0], arr_json[i].features[0].geometry.coordinates[j][1]);
                            var pathPolyOther = other_mark[i]['obj'].getPath();
                            pathPolyOther.push(new_posXY);
                        }
                        other_mark[i]['obj'].setMap(map);
                        other_mark[i]['tipe'] = tipe1;
                    } else if (tipe1 == 'Polygon') {
                        if (arr_data[i]['nilai_anggaran'] == 0 || arr_data[i]['volume'] == 0) {
                            var areaOptions = {
                                strokeColor: arr_data[i]['warna'],
                                strokeOpacity: 0.7,
                                strokeWeight: 3,
                                fillColor: arr_data[i]['warna'],
                                fillOpacity: 0
                            }
                        } else {
                            var areaOptions = {
                                strokeColor: arr_data[i]['warna'],
                                strokeWeight: 3,
                                fillColor: arr_data[i]['warna']
                            }
                        }

                        other_mark[i] = [];
                        other_mark[i]['obj'] = new google.maps.Polygon(areaOptions);
                        for (var j = 0; j < arr_json[i].features[0].geometry.coordinates[0].length; j++) {
                            var new_posXY = new google.maps.LatLng(arr_json[i].features[0].geometry.coordinates[0][j][0], arr_json[i].features[0].geometry.coordinates[0][j][1]);
                            var pathAreaOther = other_mark[i]['obj'].getPath();
                            pathAreaOther.push(new_posXY);
                        }
                        other_mark[i]['obj'].setMap(map);
                        other_mark[i]['tipe'] = tipe1;


                    }
                    var isi = '';
                    isi = '<table class="infowindow_table">';
                    //koderka
                    isi += '<tr>';
                    isi += '<td>';
                    isi += 'Kode';
                    isi += '</td>';
                    isi += '<td>';
                    isi += ' : ';
                    isi += '</td>';
                    isi += '<td>';
                    isi += '[' + arr_data[i]['unit_id'] + '.' + arr_data[i]['kegiatan_code'] + '.' + arr_data[i]['detail_no'] + ']';
                    isi += '</td>';
                    isi += '</tr>';
                    //komponen name
                    isi += '<tr>';
                    isi += '<td>';
                    isi += 'Komponen';
                    isi += '</td>';
                    isi += '<td>';
                    isi += ' : ';
                    isi += '</td>';
                    isi += '<td>';
                    if (!arr_data[i]['komponen_name']) {
                        isi += '';
                    } else {
                        isi += arr_data[i]['komponen_name'];
                    }
                    isi += '</td>';
                    isi += '</tr>';
                    //kegiatan
                    isi += '<tr>';
                    isi += '<td>';
                    isi += 'Kegiatan';
                    isi += '</td>';
                    isi += '<td>';
                    isi += ' : ';
                    isi += '</td>';
                    isi += '<td>';
                    if (!arr_data[i]['kegiatan_code']) {
                        isi += '';
                    } else {
                        isi += arr_data[i]['kegiatan_code'];
                    }
                    isi += '</td>';
                    isi += '</tr>';
                    //SKPD
                    isi += '<tr>';
                    isi += '<td>';
                    isi += 'SKPD';
                    isi += '</td>';
                    isi += '<td>';
                    isi += ' : ';
                    isi += '</td>';
                    isi += '<td>';
                    if (!arr_data[i]['unit_name']) {
                        isi += '';
                    } else {
                        isi += arr_data[i]['unit_name'];
                    }
                    isi += '</td>';
                    isi += '</tr>';
                    //Keterangan Alamat
                    isi += '<tr>';
                    isi += '<td>';
                    isi += 'Anggaran';
                    isi += '</td>';
                    isi += '<td>';
                    isi += ' : ';
                    isi += '</td>';
                    isi += '<td>';
                    if (!arr_data[i]['nilai_anggaran']) {
                        isi += '';
                    } else {
                        isi += arr_data[i]['nilai_anggaran'];
                    }
                    isi += '</td>';
                    isi += '</tr>';
                    //Keterangan
                    isi += '<tr>';
                    isi += '<td>';
                    isi += 'Keterangan';
                    isi += '</td>';
                    isi += '<td>';
                    isi += ' : ';
                    isi += '</td>';
                    isi += '<td>';
                    if (!arr_data[i]['keterangan']) {
                        isi += '';
                    } else {
                        isi += arr_data[i]['keterangan'];
                    }
                    isi += '</td>';
                    isi += '</tr>';
                    isi += '</table>';

                    infowindows[i] = new google.maps.InfoWindow({
                        content: isi
                    });
                    google.maps.event.addListener(other_mark[i]['obj'], 'click', function(i) {
                        return function(event) {
                            infowindows[i].setPosition(event.latLng);
                            infowindows[i].open(map);
                        }
                    }(i));

                }
                setAllMap(map);
            }

            function dragMark() {
                var pathArea = area.getPath();
                var pathPoly = poly.getPath();
                var indexMark = parseInt(this.getTitle()) - 1;
                var posXY = new google.maps.LatLng(this.getPosition().lat(), this.getPosition().lng())
                pathArea.setAt(indexMark, posXY);
                pathPoly.setAt(indexMark, posXY);
                hitungGeometry();
            }

            function addMarker(location) {
                var x = location.lat();
                var y = location.lng();
                marker = new google.maps.Marker({
                    position: location,
                    map: map,
                    draggable: true,
                    animation: google.maps.Animation.DROP,
                    title: countMark + ""
                });
                countMark++;
                google.maps.event.addListener(marker, 'dblclick', removeMark);
                google.maps.event.addListener(marker, 'drag', dragMark);
                markersArray.push(marker);
                var pathArea = area.getPath();
                pathArea.push(location);
                var pathPoly = poly.getPath();
                pathPoly.push(location);
                hitungGeometry();
            }

            function removeMark() {
                //alert(this.getPosition());
                var hapusMark;
                var countUlang = 1;
                var pathArea = area.getPath();
                var pathPoly = poly.getPath();
                for (i in markersArray) {
                    if (markersArray[i].getPosition() == this.getPosition()) {
                        hapusMark = i;
                    } else {
                        markersArray[i].setTitle(countUlang + "");
                        countUlang++;
                    }
                }
                pathArea.removeAt(hapusMark);
                pathPoly.removeAt(hapusMark);
                markersArray[hapusMark].setMap(null);
                markersArray.splice(hapusMark, 1);
                countMark--;
                hitungGeometry();
            }
            //Function overlay master dari db --marcell
            function USGSOverlay(bounds, map) {

                this.bounds_ = bounds;
                this.map_ = map;
                this.div_ = null;
                this.setMap(map);
            }

            //USGSOverlay.prototype = new google.maps.OverlayView();

            USGSOverlay.prototype.onAdd = function() {
                var div = document.getElementById('layer1');
                div.style.border = 'none';
                div.style.borderWidth = '0px';
                div.style.position = 'absolute';

                this.div_ = div;

                var panes = this.getPanes();
                panes.overlayLayer.appendChild(div);
            }
            USGSOverlay.prototype.draw = function() {
                var overlayProjection = this.getProjection();

                var sw = overlayProjection.fromLatLngToDivPixel(this.bounds_.getSouthWest());
                var ne = overlayProjection.fromLatLngToDivPixel(this.bounds_.getNorthEast());

                // Resize the image's DIV to fit the indicated dimensions.
                var div = this.div_;
                div.style.left = sw.x + 'px';
                div.style.top = ne.y + 'px';
                div.style.width = (ne.x - sw.x) + 'px';
                div.style.height = (sw.y - ne.y) + 'px';
            }
            USGSOverlay.prototype.onRemove = function() {
                //this.div_.parentNode.removeChild(this.div_);
                this.div_ = null;
            }
            //end of overlay function	
            google.maps.event.addDomListener(window, 'load', initialize);

            function submitMap() {
                if (confirm("Apakah data ingin disimpan ?")) {
                    //var iduser = '<?php // echo $user_id;                               ?>';
                    //lokasi=$("#searchTextField").val();
                    //nama=$("#lblNama").html();

                    keterangantambahan = $("#tambahanField").val();
                    jalanfix = $("#jalanFIX").val();
                    jalanfix2 = $("#jalanFIX2").val();
                    alamattambahan = $("#tambahanAlamat").val();

                    jalanasal = $("#jalanAsal").val();

                    radio1 = $("#pilihjalanradio1").val();
                    radio2 = $("#pilihjalanradio2").val();

                    var myJSON = {};
                    myJSON.type = "FeatureCollection"
                    myJSON.features = [];
                    myJSON.keterangan = keterangantambahan;
                    myJSON.keteranganalamat = alamattambahan;
                    myJSON.features[0] = {};
                    myJSON.features[0].type = "Feature";
                    myJSON.features[0].geometry = {};
                    myJSON.features[0].geometry.type = tipe;
                    myJSON.features[0].geometry.coordinates = [];
                    if (tipe == "Point") {
                        myJSON.features[0].geometry.coordinates = [markersArray[0].getPosition().lat(), markersArray[0].getPosition().lng()];
                        koordinat = [markersArray[0].getPosition().lat(), markersArray[0].getPosition().lng()];
                    } else if (tipe == "LineString") {
                        var pathPoly = poly.getPath();
                        var besarArrayPathPoly = pathPoly.getArray().length;
                        for (var i = 0; i < besarArrayPathPoly; i++) {
                            myJSON.features[0].geometry.coordinates[i] = [pathPoly.getArray()[i].lat(), pathPoly.getArray()[i].lng()];
                        }
                        myJSON.features[0].properties = {};
                        myJSON.features[0].properties.jarak = hitungLuasan;

                        for (var i = 0; i < besarArrayPathPoly; i++) {
                            if (i == 0) {
                                koordinat = "" + [pathPoly.getArray()[i].lat(), pathPoly.getArray()[i].lng()];
                            } else {
                                koordinat = koordinat + "|" + [pathPoly.getArray()[i].lat(), pathPoly.getArray()[i].lng()];
                            }
                        }
                    } else if (tipe == "Polygon") {
                        var pathArea = area.getPath();
                        var besarArrayPathArea = pathArea.getArray().length;
                        myJSON.features[0].geometry.coordinates[0] = [];
                        for (var i = 0; i < besarArrayPathArea; i++) {
                            myJSON.features[0].geometry.coordinates[0][i] = [pathArea.getArray()[i].lat(), pathArea.getArray()[i].lng()];
                        }
                        myJSON.features[0].properties = {};
                        myJSON.features[0].properties.luas = hitungLuasan;

                        for (var i = 0; i < besarArrayPathArea; i++) {
                            if (i == 0) {
                                koordinat = "" + [pathArea.getArray()[i].lat(), pathArea.getArray()[i].lng()];
                            } else {
                                koordinat = koordinat + "|" + [pathArea.getArray()[i].lat(), pathArea.getArray()[i].lng()];
                            }
                        }
                    }

                    var myJSONText = JSON.stringify(myJSON);
                    $.post("proses_UpdateProyek.php", {
                        unit_id: unit_id,
                        kegiatan_code: kegiatan_code,
                        detail_no: detail_no,
                        satuan: satuan,
                        volume: volume,
                        nilai_anggaran: nilai_anggaran,
                        tahun: tahun,
                        mlokasi: mlokasi,
                        id_kelompok: id_kelompok,
                        geojson: myJSONText,
                        keterangan: keterangantambahan,
                        nmuser: nmuser,
                        level: level,
                        jalan: jalanfix,
                        jalan2: jalanfix2,
                        radio1: radio1,
                        radio2: radio2,
                        keteranganalamat: alamattambahan,
                        jalanasal: jalanasal,
                        koordinat: koordinat,
                        lokasi_ke: lokasi_ke
                    },
                    function(data) {
                        if (data.result1 == 0) {
                            alert('Data geolokasi gagal diupdate !');
                        } else {
                            if (data.result2 == 0) {
                                alert('Data log geolokasi gagal dimasukkan !');
                            } else {
                                if (data.resultcekkoordinat == 0) {
<?php if ($posisi_aplikasi == 'server') { ?>
                                        window.location.href = 'https://ebudgeting.surabaya.go.id/gmap2022/updateData.php?unit_id=' + unit_id + '&kode_kegiatan=' + kegiatan_code + '&detail_no=' + detail_no + '&satuan=' + satuan + '&volume=' + volume + '&nilai_anggaran=' + nilai_anggaran + '&tahun=' + tahun + '&mlokasi=' + mlokasi + '&id_kelompok=' + id_kelompok + '&th_load=0&level=' + level + '&nm_user=' + nmuser + '&total_lokasi=' + total_lokasi + '&lokasi_ke=' + lokasi_ke;
<?php } else if ($posisi_aplikasi == 'testbed') { ?>
                                        window.location.href = 'https://ebudgeting.surabaya.go.id/gmap2022/updateData.php?unit_id=' + unit_id + '&kode_kegiatan=' + kegiatan_code + '&detail_no=' + detail_no + '&satuan=' + satuan + '&volume=' + volume + '&nilai_anggaran=' + nilai_anggaran + '&tahun=' + tahun + '&mlokasi=' + mlokasi + '&id_kelompok=' + id_kelompok + '&th_load=0&level=' + level + '&nm_user=' + nmuser + '&total_lokasi=' + total_lokasi + '&lokasi_ke=' + lokasi_ke;
<?php } else if ($posisi_aplikasi == 'lokal') { ?>
                                        window.location.href = 'http://budget.localhost/gmap2022/updateData.php?unit_id=' + unit_id + '&kode_kegiatan=' + kegiatan_code + '&detail_no=' + detail_no + '&satuan=' + satuan + '&volume=' + volume + '&nilai_anggaran=' + nilai_anggaran + '&tahun=' + tahun + '&mlokasi=' + mlokasi + '&id_kelompok=' + id_kelompok + '&th_load=0&level=' + level + '&nm_user=' + nmuser + '&total_lokasi=' + total_lokasi + '&lokasi_ke=' + lokasi_ke;
<?php } ?>
                                } else {
<?php if ($posisi_aplikasi == 'server') { ?>
                                        window.location.href = 'https://ebudgeting.surabaya.go.id/gmap2022/updateData.php?unit_id=' + unit_id + '&kode_kegiatan=' + kegiatan_code + '&detail_no=' + detail_no + '&satuan=' + satuan + '&volume=' + volume + '&nilai_anggaran=' + nilai_anggaran + '&tahun=' + tahun + '&mlokasi=' + mlokasi + '&id_kelompok=' + id_kelompok + '&th_load=0&level=' + level + '&nm_user=' + nmuser + '&total_lokasi=' + total_lokasi + '&lokasi_ke=' + lokasi_ke;
<?php } else if ($posisi_aplikasi == 'testbed') { ?>
                                        window.location.href = 'https://ebudgeting.surabaya.go.id/gmap2022/updateData.php?unit_id=' + unit_id + '&kode_kegiatan=' + kegiatan_code + '&detail_no=' + detail_no + '&satuan=' + satuan + '&volume=' + volume + '&nilai_anggaran=' + nilai_anggaran + '&tahun=' + tahun + '&mlokasi=' + mlokasi + '&id_kelompok=' + id_kelompok + '&th_load=0&level=' + level + '&nm_user=' + nmuser + '&total_lokasi=' + total_lokasi + '&lokasi_ke=' + lokasi_ke;
<?php } else if ($posisi_aplikasi == 'lokal') { ?>
                                        window.location.href = 'http://budget.localhost/gmap2022/updateData.php?unit_id=' + unit_id + '&kode_kegiatan=' + kegiatan_code + '&detail_no=' + detail_no + '&satuan=' + satuan + '&volume=' + volume + '&nilai_anggaran=' + nilai_anggaran + '&tahun=' + tahun + '&mlokasi=' + mlokasi + '&id_kelompok=' + id_kelompok + '&th_load=0&level=' + level + '&nm_user=' + nmuser + '&total_lokasi=' + total_lokasi + '&lokasi_ke=' + lokasi_ke;
<?php } ?>
    
<?php // if ($posisi_aplikasi == 'server') { ?>
//                                        window.location.href = 'https://ebudgeting.surabaya.go.id/2022/index.php/gmap/gmap.html?user_id=' + nmuser + '&unit=' + unit_id + '&kode_kegiatan=' + kegiatan_code + '&detail_no=' + detail_no + '&ket=' + keterangantambahan + '&kode=' + md5('update');
<?php // } else if ($posisi_aplikasi == 'testbed') { ?>
//                                        window.location.href = 'https://ebudgeting.surabaya.go.id/testbed2021sf/index.php/gmap/gmap.html?user_id=' + nmuser + '&unit=' + unit_id + '&kode_kegiatan=' + kegiatan_code + '&detail_no=' + detail_no + '&ket=' + keterangantambahan + '&kode=' + md5('update');
<?php // } else if ($posisi_aplikasi == 'lokal') { ?>
//                                        window.location.href = 'http://budget.localhost/2022/index.php/gmap/gmap.html?user_id=' + nmuser + '&unit=' + unit_id + '&kode_kegiatan=' + kegiatan_code + '&detail_no=' + detail_no + '&ket=' + keterangantambahan + '&kode=' + md5('update');
<?php // } ?>
                                }
                            }
                        }
                    });
                }
                ;
            }

            function tambahMap() {
                if (confirm("Apakah data ingin disimpan & menambah lokasi baru?")) {
                    //var iduser = '<?php // echo $user_id;                               ?>';
                    //lokasi=$("#searchTextField").val();
                    //nama=$("#lblNama").html();

                    keterangantambahan = $("#tambahanField").val();
                    jalanfix = $("#jalanFIX").val();
                    jalanfix2 = $("#jalanFIX2").val();
                    alamattambahan = $("#tambahanAlamat").val();

                    jalanasal = $("#jalanAsal").val();

                    radio1 = $("#pilihjalanradio1").val();
                    radio2 = $("#pilihjalanradio2").val();

                    var myJSON = {};
                    myJSON.type = "FeatureCollection"
                    myJSON.features = [];
                    myJSON.keterangan = keterangantambahan;
                    myJSON.keteranganalamat = alamattambahan;
                    myJSON.features[0] = {};
                    myJSON.features[0].type = "Feature";
                    myJSON.features[0].geometry = {};
                    myJSON.features[0].geometry.type = tipe;
                    myJSON.features[0].geometry.coordinates = [];
                    if (tipe == "Point") {
                        myJSON.features[0].geometry.coordinates = [markersArray[0].getPosition().lat(), markersArray[0].getPosition().lng()];
                        koordinat = [markersArray[0].getPosition().lat(), markersArray[0].getPosition().lng()];
                    } else if (tipe == "LineString") {
                        var pathPoly = poly.getPath();
                        var besarArrayPathPoly = pathPoly.getArray().length;
                        for (var i = 0; i < besarArrayPathPoly; i++) {
                            myJSON.features[0].geometry.coordinates[i] = [pathPoly.getArray()[i].lat(), pathPoly.getArray()[i].lng()];
                        }
                        myJSON.features[0].properties = {};
                        myJSON.features[0].properties.jarak = hitungLuasan;

                        for (var i = 0; i < besarArrayPathPoly; i++) {
                            if (i == 0) {
                                koordinat = "" + [pathPoly.getArray()[i].lat(), pathPoly.getArray()[i].lng()];
                            } else {
                                koordinat = koordinat + "|" + [pathPoly.getArray()[i].lat(), pathPoly.getArray()[i].lng()];
                            }
                        }
                    } else if (tipe == "Polygon") {
                        var pathArea = area.getPath();
                        var besarArrayPathArea = pathArea.getArray().length;
                        myJSON.features[0].geometry.coordinates[0] = [];
                        for (var i = 0; i < besarArrayPathArea; i++) {
                            myJSON.features[0].geometry.coordinates[0][i] = [pathArea.getArray()[i].lat(), pathArea.getArray()[i].lng()];
                        }
                        myJSON.features[0].properties = {};
                        myJSON.features[0].properties.luas = hitungLuasan;

                        for (var i = 0; i < besarArrayPathArea; i++) {
                            if (i == 0) {
                                koordinat = "" + [pathArea.getArray()[i].lat(), pathArea.getArray()[i].lng()];
                            } else {
                                koordinat = koordinat + "|" + [pathArea.getArray()[i].lat(), pathArea.getArray()[i].lng()];
                            }
                        }
                    }

                    var myJSONText = JSON.stringify(myJSON);
                    $.post("proses_UpdateProyek.php", {
                        unit_id: unit_id,
                        kegiatan_code: kegiatan_code,
                        detail_no: detail_no,
                        satuan: satuan,
                        volume: volume,
                        nilai_anggaran: nilai_anggaran,
                        tahun: tahun,
                        mlokasi: mlokasi,
                        id_kelompok: id_kelompok,
                        geojson: myJSONText,
                        keterangan: keterangantambahan,
                        nmuser: nmuser,
                        level: level,
                        jalan: jalanfix,
                        jalan2: jalanfix2,
                        radio1: radio1,
                        radio2: radio2,
                        keteranganalamat: alamattambahan,
                        jalanasal: jalanasal,
                        koordinat: koordinat,
                        lokasi_ke: lokasi_ke
                    },
                    function(data) {
                        if (data.result1 == 0) {
                            alert('Data geolokasi gagal diupdate !');
                        } else {
                            if (data.result2 == 0) {
                                alert('Data log geolokasi gagal dimasukkan !');
                            } else {
                                if (data.resultcekkoordinat == 0) {
<?php if ($posisi_aplikasi == 'server') { ?>
                                        window.location.href = 'https://ebudgeting.surabaya.go.id/gmap2022/updateData.php?unit_id=' + unit_id + '&kode_kegiatan=' + kegiatan_code + '&detail_no=' + detail_no + '&satuan=' + satuan + '&volume=' + volume + '&nilai_anggaran=' + nilai_anggaran + '&tahun=' + tahun + '&mlokasi=' + mlokasi + '&id_kelompok=' + id_kelompok + '&th_load=0&level=' + level + '&nm_user=' + nmuser + '&total_lokasi=' + total_lokasi + '&lokasi_ke=' + lokasi_ke;
<?php } else if ($posisi_aplikasi == 'testbed') { ?>
                                        window.location.href = 'https://ebudgeting.surabaya.go.id/gmap2022/updateData.php?unit_id=' + unit_id + '&kode_kegiatan=' + kegiatan_code + '&detail_no=' + detail_no + '&satuan=' + satuan + '&volume=' + volume + '&nilai_anggaran=' + nilai_anggaran + '&tahun=' + tahun + '&mlokasi=' + mlokasi + '&id_kelompok=' + id_kelompok + '&th_load=0&level=' + level + '&nm_user=' + nmuser + '&total_lokasi=' + total_lokasi + '&lokasi_ke=' + lokasi_ke;
<?php } else if ($posisi_aplikasi == 'lokal') { ?>
                                        window.location.href = 'http://budget.localhost/gmap2022/updateData.php?unit_id=' + unit_id + '&kode_kegiatan=' + kegiatan_code + '&detail_no=' + detail_no + '&satuan=' + satuan + '&volume=' + volume + '&nilai_anggaran=' + nilai_anggaran + '&tahun=' + tahun + '&mlokasi=' + mlokasi + '&id_kelompok=' + id_kelompok + '&th_load=0&level=' + level + '&nm_user=' + nmuser + '&total_lokasi=' + total_lokasi + '&lokasi_ke=' + lokasi_ke;
<?php } ?>
                                } else {
                                    lokasi_ke++;
<?php if ($posisi_aplikasi == 'server') { ?>
                                        window.location.href = 'https://ebudgeting.surabaya.go.id/gmap2022/insertBaru.php?unit_id=' + unit_id + '&kode_kegiatan=' + kegiatan_code + '&detail_no=' + detail_no + '&satuan=' + satuan + '&volume=' + volume + '&nilai_anggaran=' + nilai_anggaran + '&tahun=' + tahun + '&mlokasi=&id_kelompok=' + id_kelompok + '&th_load=0&level=' + level + '&nm_user=' + nmuser + '&lokasi_ke=' + lokasi_ke;
<?php } else if ($posisi_aplikasi == 'testbed') { ?>
                                        window.location.href = 'https://ebudgeting.surabaya.go.id/gmap2022/insertBaru.php?unit_id=' + unit_id + '&kode_kegiatan=' + kegiatan_code + '&detail_no=' + detail_no + '&satuan=' + satuan + '&volume=' + volume + '&nilai_anggaran=' + nilai_anggaran + '&tahun=' + tahun + '&mlokasi=&id_kelompok=' + id_kelompok + '&th_load=0&level=' + level + '&nm_user=' + nmuser + '&lokasi_ke=' + lokasi_ke;
<?php } else if ($posisi_aplikasi == 'lokal') { ?>
                                        window.location.href = 'http://budget.localhost/gmap2022/insertBaru.php?unit_id=' + unit_id + '&kode_kegiatan=' + kegiatan_code + '&detail_no=' + detail_no + '&satuan=' + satuan + '&volume=' + volume + '&nilai_anggaran=' + nilai_anggaran + '&tahun' + tahun + '&mlokasi=&id_kelompok=' + id_kelompok + '&th_load=0&level=' + level + '&nm_user=' + nmuser + '&lokasi_ke=' + lokasi_ke;
<?php } ?>
                                }
                            }
                        }
                    });
                }
                ;
            }

            function hitungGeometry() {
                if (tipe == "LineString") {
                    hitungLuasan = google.maps.geometry.spherical.computeLength(poly.getPath());
                    $("#jarak_marker font").html((Math.floor(hitungLuasan * 1000) / 1000) + ' M1');
                } else if (tipe == "Polygon") {
                    hitungLuasan = google.maps.geometry.spherical.computeArea(area.getPath());
                    $("#jarak_marker font").html((Math.floor(hitungLuasan * 1000) / 1000) + ' M2');
                } else if (tipe == "Point") {
                    $("#jarak_marker font").html('1 Titik');
                }
            }

            function backBudget() {
                var kode_kembali = md5('kembali');
<?php if ($posisi_aplikasi == 'server') { ?>
                    window.location.href = 'https://ebudgeting.surabaya.go.id/2022/index.php/gmap/gmap.html?user_id=' + nmuser + '&unit=' + unit_id + '&kode_kegiatan=' + kegiatan_code + '&kode=' + kode_kembali + '&detail_no=' + detail_no;
<?php } else if ($posisi_aplikasi == 'testbed') { ?>
                    window.location.href = 'https://ebudgeting.surabaya.go.id/testbed2021sf/index.php/gmap/gmap.html?user_id=' + nmuser + '&unit=' + unit_id + '&kode_kegiatan=' + kegiatan_code + '&kode=' + kode_kembali + '&detail_no=' + detail_no;
<?php } else if ($posisi_aplikasi == 'lokal') { ?>
                    window.location.href = 'http://budget.localhost/2022/index.php/gmap/gmap.html?user_id=' + nmuser + '&unit=' + unit_id + '&kode_kegiatan=' + kegiatan_code + '&kode=' + kode_kembali + '&detail_no=' + detail_no;
<?php } ?>
            }

            function nextBudget() {
                lokasi_ke++;
<?php if ($posisi_aplikasi == 'server') { ?>
                    window.location.href = 'https://ebudgeting.surabaya.go.id/gmap2022/updateData.php?unit_id=' + unit_id + '&kode_kegiatan=' + kegiatan_code + '&detail_no=' + detail_no + '&satuan=' + satuan + '&volume=' + volume + '&nilai_anggaran=' + nilai_anggaran + '&tahun=' + tahun + '&mlokasi=&id_kelompok=' + id_kelompok + '&th_load=0&level=' + level + '&nm_user=' + nmuser + '&total_lokasi=' + total_lokasi + '&lokasi_ke=' + lokasi_ke;
<?php } else if ($posisi_aplikasi == 'testbed') { ?>
                    window.location.href = 'https://ebudgeting.surabaya.go.id/gmap2022/updateData.php?unit_id=' + unit_id + '&kode_kegiatan=' + kegiatan_code + '&detail_no=' + detail_no + '&satuan=' + satuan + '&volume=' + volume + '&nilai_anggaran=' + nilai_anggaran + '&tahun=' + tahun + '&mlokasi=&id_kelompok=' + id_kelompok + '&th_load=0&level=' + level + '&nm_user=' + nmuser + '&total_lokasi=' + total_lokasi + '&lokasi_ke=' + lokasi_ke;
<?php } else if ($posisi_aplikasi == 'lokal') { ?>
                    window.location.href = 'http://budget.localhost/gmap2022/updateData.php?unit_id=' + unit_id + '&kode_kegiatan=' + kegiatan_code + '&detail_no=' + detail_no + '&satuan=' + satuan + '&volume=' + volume + '&nilai_anggaran=' + nilai_anggaran + '&tahun=' + tahun + '&mlokasi=&id_kelompok=' + id_kelompok + '&th_load=0&level=' + level + '&nm_user=' + nmuser + '&total_lokasi=' + total_lokasi + '&lokasi_ke=' + lokasi_ke;
<?php } ?>
            }

            function prevBudget() {
                lokasi_ke--;
<?php if ($posisi_aplikasi == 'server') { ?>
                    window.location.href = 'https://ebudgeting.surabaya.go.id/gmap2022/updateData.php?unit_id=' + unit_id + '&kode_kegiatan=' + kegiatan_code + '&detail_no=' + detail_no + '&satuan=' + satuan + '&volume=' + volume + '&nilai_anggaran=' + nilai_anggaran + '&tahun=' + tahun + '&mlokasi=&id_kelompok=' + id_kelompok + '&th_load=0&level=' + level + '&nm_user=' + nmuser + '&total_lokasi=' + total_lokasi + '&lokasi_ke=' + lokasi_ke;
<?php } else if ($posisi_aplikasi == 'testbed') { ?>
                    window.location.href = 'https://ebudgeting.surabaya.go.id/gmap2022/updateData.php?unit_id=' + unit_id + '&kode_kegiatan=' + kegiatan_code + '&detail_no=' + detail_no + '&satuan=' + satuan + '&volume=' + volume + '&nilai_anggaran=' + nilai_anggaran + '&tahun=' + tahun + '&mlokasi=&id_kelompok=' + id_kelompok + '&th_load=0&level=' + level + '&nm_user=' + nmuser + '&total_lokasi=' + total_lokasi + '&lokasi_ke=' + lokasi_ke;
<?php } else if ($posisi_aplikasi == 'lokal') { ?>
                    window.location.href = 'http://budget.localhost/gmap2022/updateData.php?unit_id=' + unit_id + '&kode_kegiatan=' + kegiatan_code + '&detail_no=' + detail_no + '&satuan=' + satuan + '&volume=' + volume + '&nilai_anggaran=' + nilai_anggaran + '&tahun=' + tahun + '&mlokasi=&id_kelompok=' + id_kelompok + '&th_load=0&level=' + level + '&nm_user=' + nmuser + '&total_lokasi=' + total_lokasi + '&lokasi_ke=' + lokasi_ke;
<?php } ?>
            }

            function pilihjalan() {
                if (document.getElementById('pilihjalanradio1').checked == true) {
                    document.getElementById('jalanFIX2').disabled = true;
                    document.getElementById('jalanFIX').disabled = false;
                    document.getElementById('jalanFIX').required = true;
                    document.getElementById('pilihjalanradio1').value = 1;
                    document.getElementById('pilihjalanradio2').value = 0;
                } else {
                    document.getElementById('jalanFIX').disabled = true;
                    document.getElementById('jalanFIX2').disabled = false;
                    document.getElementById('jalanFIX2').required = true;
                    document.getElementById('pilihjalanradio1').value = 0;
                    document.getElementById('pilihjalanradio2').value = 2;
                }
            }

            /*help function*/
            $(document).ready(function() {
                $('#btnClose').click(function(event) {
                    $('#manualScreen').hide(300);
                });
                $('#btnHelp').click(function(event) {
                    $('#manualScreen').show(300);
                });
            });

            function autocomplet() {
                var min_length = 0; // min caracters to display the autocomplete
                var keyword = $('#jalanFIX').val();
                if (keyword.length >= min_length) {
                    $.ajax({
                        url: 'ajax_refresh.php',
                        type: 'POST',
                        data: {keyword: keyword},
                        success: function(data) {
                            $('#jalan_list_id').show();
                            $('#jalan_list_id').html(data);
                        }
                    });
                } else {
                    $('#jalan_list_id').hide();
                }
            }

// set_item : this function will be executed when we select an item
            function set_item(item) {
                // change input value
                $('#jalanFIX').val(item);
                // hide proposition list
                $('#jalan_list_id').hide();
            }

// Deletes all markers in the array by removing references to them.
            function deleteMarkers() {
                var hapusMark;
                var pathArea = area.getPath();
                var pathPoly = poly.getPath();

                for (l = 0; l < markersArray.length; l++) {
                    hapusMark = markersArray[l];
                    pathArea.removeAt(hapusMark);
                    pathPoly.removeAt(hapusMark);
                    markersArray[l].setMap(null);
                }
                hitungGeometry();
            }
        </script>

    </head>
    <div id="hiddenLayer"></div>
    <!-- ADD THE CLASS sidedar-collapse TO HIDE THE SIDEBAR PRIOR TO LOADING THE SITE -->
    <body class="skin-blue sidebar-collapse fixed">
        <!-- Site wrapper -->
        <div class="wrapper">
            <header class="main-header">
                <a href="#" class="logo">
                    <b>
                        <?php
                        if ($posisi_aplikasi == 'server') {
                            echo 'e-Budgeting ' . $tahun;
                        } else if ($posisi_aplikasi == 'testbed') {
                            echo 'TESTBED ' . $tahun;
                        } elseif ($posisi_aplikasi == 'lokal') {
                            echo 'LOKAL ' . $tahun;
                        }
                        ?>                        
                    </b>
                </a>
                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top" role="navigation">
                    &nbsp;
                </nav>
            </header>
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>Ubah Lokasi GMAP untuk komponen</h1>                        
                </section>
                <!-- Main content -->
                <section class="content">
                    <!-- Default box -->
                    <div style="width: 100%">
                        <div class="col-xs-5 left">
                            <div class="box box-solid box-primary">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Bantuan</h3>
                                    <div class="box-tools pull-right text-bold">
                                        <button type="button" class="btn btn-info btn-sm btn-flat" data-toggle="modal" data-target="#myModal">Petunjuk Penggunaan</button>
                                    </div>
                                </div>
                                <div class="box-body">
                                    <button type="button" class="btn btn-warning btn-sm btn-flat" onclick="deleteMarkers();">Hapus Marker</button>
                                </div>
                            </div>
                            <div class="box box-solid box-primary">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Isi Geolokasi <?php echo '(' . $lokasi_ke . ' dari ' . $total_lokasi . ' lokasi)'; ?></h3>
                                    <div class="box-tools pull-right text-bold"><?php echo $tahun ?></div>
                                </div>
                                <div class="box-body">
                                    <div id="jarak_marker" class="text-bold" style="margin-top: 10px; margin-bottom: 10px;">                                        
                                        <div class="col-xs-6">
                                            <div style="text-align: left">Volume RKA   : <?php echo $volume_rd . ' ' . $satuan_rd ?></div>
                                        </div>
                                        <div class="col-xs-6 text-right">
                                            <div style="text-align: right">Jarak : <font></font></div><br/>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div> 
                                    <div class="row" style="margin-bottom: 10px;">
                                        <div class="col-xs-3 text-bold text-right">
                                            Tipe Objek
                                        </div>
                                        <div class="col-xs-9">
                                            <label id='txt_tipeMarkah' name='txt_tipeMarkah'>
                                                <?php
                                                if ($tipe == 'Point') {
                                                    echo 'Titik';
                                                } else if ($tipe == 'LineString') {
                                                    echo 'Garis';
                                                } else if ($tipe == 'Polygon') {
                                                    echo 'Area';
                                                }
                                                ?>
                                            </label>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="row" style="margin-bottom: 10px;">
                                        <div class="col-xs-3 text-bold text-right">
                                            SKPD
                                        </div>
                                        <div class="col-xs-9">
                                            <label id='txt_unitId' name='txt_unitId'><?php echo $nama_skpd ?></label>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="row" style="margin-bottom: 10px;">
                                        <div class="col-xs-3 text-bold text-right">
                                            Nama Komponen
                                        </div>
                                        <div class="col-xs-9">
                                            <label id='txt_komponenNama' name='txt_komponenNama'><?php echo $komponen_name . ' ' . $detail_name ?></label>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="row" style="margin-bottom: 10px;">
                                        <div class="col-xs-3 text-bold text-right">
                                            Cari Jalan
                                        </div>
                                        <div class="col-xs-9">
                                            <input type="text" style="width: 100%"  name="searchTextField" id="searchTextField" class="m form-control" onclick="this.select();" value=""/>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <!--                                    <div class="row" style="margin-bottom: 10px;">
                                                                            <div class="col-xs-3 text-bold text-right">
                                                                                Nama Jalan
                                                                            </div>
                                                                            <div class="col-xs-9">&nbsp;</div>
                                                                            <div class="clearfix"></div>
                                                                        </div>
                                                                        <div class="row" style="margin-bottom: 10px;">
                                                                            <div class="col-xs-1 text-bold text-right">
                                                                                <input type="radio" name="pilihjalanradio" id="pilihjalanradio1" value="1" onclick="pilihjalan()" checked="true"/>
                                                                            </div>
                                                                            <div class="col-xs-11">Memilih Data Jalan Yang Ada <font style="color: red">(Pilih dengan Auto Complete)</font></div>
                                                                            <div class="clearfix"></div>
                                                                        </div>
                                                                        <div class="row" style="margin-bottom: 10px;">
                                                                            <div class="col-xs-12">
                                                                                <input type="text" class="form-control" id="jalanFIX" name="jalanFIX" onkeyup="autocomplet()" value="<?php echo $jalanFix ?>">
                                                                                <ul id="jalan_list_id"></ul>
                                                                            </div>
                                                                            <div class="clearfix"></div>
                                                                        </div>
                                                                        <div class="row" style="margin-bottom: 10px;">
                                                                            <div class="col-xs-1 text-bold text-right">
                                                                                <input type="radio" name="pilihjalanradio" id="pilihjalanradio2" value="0" onclick="pilihjalan()" />
                                                                            </div>
                                                                            <div class="col-xs-11">Menambah Jalan Baru <font style="color: red">(Pilih bila tidak ditemukan pada list diatas)</font></div>
                                                                            <div class="clearfix"></div>
                                                                        </div>
                                                                        <div class="row" style="margin-bottom: 10px;">
                                                                            <div class="col-xs-12">
                                                                                <textarea id="jalanFIX2" name="jalanFIX2" class="form-control" onclick="this.select();" placeholder="dengan FORMAT 'Jl. NAMA JALAN'" disabled="true"><?php echo $jalanFix ?></textarea>
                                                                                <span style="color: red">* Hanya berisi nama jalan (kata Jalan singkatan dengan 'Jl. ') tanpa ada Gang, RT, RW, NO</span>
                                                                            </div>
                                                                            <div class="clearfix"></div>
                                                                        </div>-->
<!--                                    <div class="row" style="margin-bottom: 10px;">
                                        <div class="col-xs-4 text-bold text-right">
                                            Alamat Lengkap
                                        </div>
                                        <div class="col-xs-8">&nbsp;</div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="row" style="margin-bottom: 10px;">
                                        <div class="col-xs-12">
                                            <input type="text" style="width: 100%"  name="tambahanAlamat" id="tambahanAlamat" class="form-control" value="<?php echo trim($keteranganAlamat); ?>" placeholder="untuk tambahan alamat seperti RT, RW, NO, dll"/>
                                            <textarea id="tambahanAlamat" name="tambahanAlamat" class="form-control" onclick="this.select();" placeholder="untuk tambahan alamat seperti RT, RW, NO, dll" required="true"><?php echo trim($keteranganAlamat); ?></textarea>
                                            <span style="color: red">* Isikan dengan alamat yang lengkap dengan nama jalan, gang, RT, RW, NO</span>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>-->
                                    <div class="row" style="margin-bottom: 10px;">
                                        <div class="col-xs-4 text-bold text-right">
                                            Alamat Lengkap
                                        </div>
                                        <div class="col-xs-8">&nbsp;</div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="row" style="margin-bottom: 10px;">
                                        <div class="col-xs-12">
                                            <input type="text" style="width: 100%"  name="tambahanField" id="tambahanField" class="form-control" value="<?php echo $keterangan; ?>"/>
                                            <!--<textarea id="tambahanField" name="tambahanField" class="form-control" onclick="this.select();" ><?php echo $keterangan; ?></textarea>-->
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div><!-- /.box-body -->
                                <div class="box-footer text-right">
                                    <input type="button" class="btn btn-default btn-flat" value="Kembali" name="btnKembali" id="btnKembali" onclick="backBudget()"/>
                                    <?php if ($lokasi_ke > 1) { ?>
                                        <input type="button" class="btn btn-default btn-flat" value="Lokasi Sebelumnya" name="btnPrev" id="btnPrev" onclick="prevBudget()"/>                                            
                                    <?php } ?>
                                    <?php if ($lokasi_ke < $total_lokasi) { ?>
                                        <input type="button" class="btn btn-default btn-flat" value="Lokasi Selanjutnya" name="btnNext" id="btnNext" onclick="nextBudget()"/>
                                    <?php } ?>                                    
                                    <?php if ($lokasi_ke == $total_lokasi) { ?>
                                        <input type="button" class="btn btn-warning btn-flat" value="Simpan data & Tambah Baru" name="btnTambah" id="btnTambah" onclick="tambahMap()"/>        
                                    <?php } ?>                                    
                                    <input type="button" class="btn btn-primary btn-flat" value="Update Data" name="btnSubmit" id="btnSubmit" onclick="submitMap()"/>
                                </div><!-- /.box-footer-->
                            </div><!-- /.box -->
                        </div>
                        <div class="col-xs-7 right" id="map-canvas" style="border: 1px solid blue; height: 80%"></div>
                        <div class="clearfix"></div>
                    </div>

                </section><!-- /.content -->



                <!-- Modal -->
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog" style="width: 70%">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Petunjuk Penggunaan GMAP untuk objek <?php echo $tipe ?></h4>
                            </div>
                            <div class="modal-body">
                                <?php if ($tipe == 'Point') { ?>
                                    <ul>
                                        <li>
                                            <p>Ketik nama lokasi yang ingin dibuat pada tempat yang telah disediakan, kemudian pilih lokasi yang dimaksud.</p>
                                            <p class="text-center"><img src="img/carijalan.jpg" class="img-rounded" /></p>
                                        </li>
                                        <li>
                                            <p>Geser titik ke tempat yang di inginkan. Titik akan terangkat pada saat tombol kiri mouse ditekan dan ditahan, titik akan jatuh ke tempat bertanda silang (X) pada saat tombol kiri mouse dilepas.</p>
                                            <p class="text-center"><img src="img/titik.gif" class="img-rounded" /></p>
                                        </li>
                                        <li>
                                            <p>Isikan keterangan tambahan jika diperlukan.</p>
                                            <p class="text-center"><img src="img/Keterangan.jpg" class="img-rounded" /></p>
                                        </li>
                                        <li>
                                            <p>Tekan tombol simpan data untuk menyimpan lokasi yang telah ditandai.</p>
                                            <p class="text-center"><img src="img/ButtonSubmit.jpg" class="img-rounded" /></p>
                                        </li>
                                    </ul>                                    
                                <?php } else if ($tipe == 'LineString') {
                                    ?>
                                    <ol>
                                        <li>
                                            <p>Ketik nama lokasi yang ingin dibuat pada tempat yang telah disediakan, kemudian pilih lokasi yang dimaksud.</p>
                                            <p class="text-center"><img src="img/carijalan.jpg" class="img-rounded" /></p>
                                        </li>
                                        <li>
                                            <p>Klik dua titik atau lebih pada peta.</p>
                                            <p class="text-center"><img src="img/garis.gif" class="img-rounded" /></p>
                                        </li>
                                        <li>
                                            <p>Geser tiap titik sehingga garis yang ditandai pada peta sesuai dengan keinginan. Titik akan terangkat pada saat tombol kiri mouse ditekan dan ditahan, titik akan jatuh ke tempat bertanda silang (X) pada saat tombol kiri mouse dilepas.</p>
                                            <p class="text-center"><img src="img/garisUpdate.gif" class="img-rounded" /></p>
                                        </li>
                                        <li>
                                            <p>Jika titik yang dibuat berlebih, maka lakukan double click pada titik yang tidak diinginkan untuk menghapus titik tersebut. Garis yang terbentuk akan menyesuaikan perubahan secara otomatis.</p>
                                            <p class="text-center"><img src="img/garisDelete.gif" class="img-rounded" /></p>
                                        </li>
                                        <li>
                                            <p>Isikan keterangan tambahan jika diperlukan.</p>
                                            <p class="text-center"><img src="img/Keterangan.jpg" class="img-rounded" /></p>
                                        </li>
                                        <li>
                                            <p>Tekan tombol simpan data untuk menyimpan lokasi yang telah ditandai.</p>
                                            <p class="text-center"><img src="img/ButtonSubmit.jpg" class="img-rounded" /></p>
                                        </li>
                                    </ol>
                                <?php } else if ($tipe == 'Polygon') { ?>
                                    <ol>
                                        <li>
                                            <p>Ketik nama lokasi yang ingin dibuat pada tempat yang telah disediakan, kemudian pilih lokasi yang dimaksud.</p>
                                            <p class="text-center"><img src="img/carijalan.jpg" class="img-rounded" /></p>
                                        </li>
                                        <li>
                                            <p>Klik tiga titik atau lebih pada peta.</p>
                                            <p class="text-center"><img src="img/area.gif" class="img-rounded" /></p>
                                        </li>
                                        <li>
                                            <p>Geser tiap titik sehingga area yang ditandai pada peta sesuai dengan keinginan. Titik akan terangkat pada saat tombol kiri mouse ditekan dan ditahan, titik akan jatuh ke tempat bertanda silang (X) pada saat tombol kiri mouse dilepas.</p>
                                            <p class="text-center"><img src="img/areaUpdate.gif" class="img-rounded" /></p>
                                        </li>
                                        <li>
                                            <p>Jika titik yang dibuat berlebih, maka lakukan double click pada titik yang tidak diinginkan untuk menghapus titik tersebut. Garis yang terbentuk akan menyesuaikan perubahan secara otomatis.</p>
                                            <p class="text-center"><img src="img/areaDelete.gif" class="img-rounded" /></p>
                                        </li>
                                        <li>
                                            <p>Isikan keterangan tambahan jika diperlukan.</p>
                                            <p class="text-center"><img src="img/Keterangan.jpg" class="img-rounded" /></p>
                                        </li>
                                        <li>
                                            <p>Tekan tombol simpan data untuk menyimpan lokasi yang telah ditandai.</p>
                                            <p class="text-center"><img src="img/ButtonSubmit.jpg" class="img-rounded" /></p>
                                        </li>
                                    </ol>
                                    <?php
                                }
                                ?>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- /.content-wrapper -->

            <footer class="main-footer">
                <div class="pull-right hidden-xs">
                    <b>Version</b> 1.3
                </div>
                <strong>Copyright &copy; <?php echo $tahun ?>  <a href="https://bp.surabaya.go.id" target="_blank">Bagian Administrasi Pembangunan</a> - <a href="https://www.surabaya.go.id" target="_blank">Pemerintah Kota Surabaya</a>.</strong> All rights reserved.
            </footer>
        </div><!-- ./wrapper -->
    </body>
</html>