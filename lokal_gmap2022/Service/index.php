<?php

function getStatusCodeMessage($status) {
    // these could be stored in a .ini file and loaded
    // via parse_ini_file()... however, this will suffice
    // for an example
    $codes = Array(
        100 => 'Continue',
        101 => 'Switching Protocols',
        200 => 'OK',
        201 => 'Created',
        202 => 'Accepted',
        203 => 'Non-Authoritative Information',
        204 => 'No Content',
        205 => 'Reset Content',
        206 => 'Partial Content',
        300 => 'Multiple Choices',
        301 => 'Moved Permanently',
        302 => 'Found',
        303 => 'See Other',
        304 => 'Not Modified',
        305 => 'Use Proxy',
        306 => '(Unused)',
        307 => 'Temporary Redirect',
        400 => 'Bad Request',
        401 => 'Unauthorized',
        402 => 'Payment Required',
        403 => 'Forbidden',
        404 => 'Not Found',
        405 => 'Method Not Allowed',
        406 => 'Not Acceptable',
        407 => 'Proxy Authentication Required',
        408 => 'Request Timeout',
        409 => 'Conflict',
        410 => 'Gone',
        411 => 'Length Required',
        412 => 'Precondition Failed',
        413 => 'Request Entity Too Large',
        414 => 'Request-URI Too Long',
        415 => 'Unsupported Media Type',
        416 => 'Requested Range Not Satisfiable',
        417 => 'Expectation Failed',
        500 => 'Internal Server Error',
        501 => 'Not Implemented',
        502 => 'Bad Gateway',
        503 => 'Service Unavailable',
        504 => 'Gateway Timeout',
        505 => 'HTTP Version Not Supported'
    );

    return (isset($codes[$status])) ? $codes[$status] : '';
}

// Helper method to send a HTTP response code/message
function sendResponse($status = 200, $body = '', $content_type = 'text/html') {
    $status_header = 'HTTP/1.1 ' . $status . ' ' . getStatusCodeMessage($status);
    header($status_header);
    header('Content-type: ' . $content_type);
    echo $body;
}

class Budget {

    //testbed
//	static $host = "localhost";
//	static $user = "budget";
//	static $pass = "budget2014";
//	static $db = "gis_2014";
//	
//	static $hostBudget = "localhost";
//	static $userBudget = "budget";
//	static $passBudget = "budget2014";
//	static $dbBudget = "budget_2014";
    //local
    static $host = "localhost";
    static $user = "budget";
    static $pass = "rahasia";
    static $db = "gis_2015";
    static $hostBudget = "localhost";
    static $userBudget = "budget";
    static $passBudget = "rahasia";
    static $dbBudget = "budget_2015";

    function __construct() {
        $con = pg_connect("host=" . self::$host . " dbname=" . self::$db . " user=" . self::$user . " password=" . self::$pass) or die("Could not connect to server\n");
        $query = "SET TIME ZONE 'ASIA/JAKARTA'";
        $result = pg_query($query) or die("Cannot execute query: $query\n");

        //echo "construct";
        pg_close();
    }

    function __destruct() {
        //echo "destruct";
    }

    function sinkronise() {

        /* $ArrIDCollection = array();
          $ctr = 0;

          $con = pg_connect("host=". self::$host ." dbname=". self::$db ." user=". self::$user ." password=". self::$pass) or die ("Could not connect to server\n"); //make connection

          $sqlstmt = "SELECT * FROM geojsonlokasi_rev1 where idgeolokasi in (SELECT distinct(idgeolokasi) FROM public.loggmap_rev1 where tanggal >= to_date('$date','YYYY-MM-DD') order by idgeolokasi)";
          $select = pg_query($sqlstmt) or die("Cannot Execute Query : $query\n");
          if(pg_num_rows($select) > 0){
          $ctr = 0;
          while($row = pg_fetch_array($select)){
          $ArrIDCollection[$ctr][0] = $row[1];
          $ArrIDCollection[$ctr][1] = $row[2];
          $ArrIDCollection[$ctr][2] = $row[3];
          $ctr++;
          }
          }

          pg_close();

          $con = pg_connect("host=". self::$hostBudget ." dbname=". self::$dbBudget ." user=". self::$userBudget ." password=". self::$passBudget) or die ("Could not connect to server\n"); //make connection

          $sqlstmt = "SELECT * FROM geojsonlokasi_rev1 where idgeolokasi in (SELECT distinct(idgeolokasi) FROM public.loggmap_rev1 where tanggal >= to_date('$date','YYYY-MM-DD') order by idgeolokasi)";
          $select = pg_query($sqlstmt) or die("Cannot Execute Query : $query\n");
          if(pg_num_rows($select) > 0){
          $ctr = 0;
          while($row = pg_fetch_array($select)){
          $ArrIDCollection[$ctr][0] = $row[1];
          $ArrIDCollection[$ctr][1] = $row[2];
          $ArrIDCollection[$ctr][2] = $row[3];
          $ctr++;
          }
          }

          pg_close(); */

        $con = pg_connect("host=" . self::$host . " dbname=" . self::$db . " user=" . self::$user . " password=" . self::$pass) or die("Could not connect to server\n"); //make connection

        $arrResp = array(); //contain response data
        $curYear = date('Y');
        $date = $_REQUEST["date"];
        $sqlstmt = "SELECT * FROM geojsonlokasi_rev1 where idgeolokasi in (SELECT distinct(idgeolokasi) FROM public.loggmap_rev1 where tanggal >= to_date('$date','YYYY-MM-DD') order by idgeolokasi)";
        $select = pg_query($sqlstmt) or die("Cannot Execute Query : $query\n");
        $array = array();
        $arrayBack = array();
        if (pg_num_rows($select) > 0) {
            while ($row = pg_fetch_array($select)) {
                $array[0] = $row[0];
                $array[1] = $row[1];
                $array[2] = $row[2];
                $array[3] = $row[3];
                $array[4] = $row[4];
                $array[5] = $row[5];
                $array[6] = $row[6];
                $array[7] = $row[7];
                $array[8] = $row[8];
                $array[9] = $row[9];
                $array[10] = $row[11];
                $array[11] = $row[12];
                $array[12] = $row[13];
                $array[13] = $row[14];
                $arrayBack[$ctr] = $array;
                $ctr++;
            }
        }

        pg_close();
        $arrResp["DATA"] = $arrayBack;
        $arrResp["STATUSCODE"] = 1;
        sendResponse(200, json_encode($arrResp));
    }

}

date_default_timezone_set('Asia/Jakarta');

$Budget = new Budget;
$mode = "";

date_default_timezone_set('Asia/Jakarta');

if (isset($_REQUEST["mode"])) {
    $mode = $_REQUEST["mode"];
}
if ($mode == "sinkronise") {
    //STATUSCODE = 1
    $Budget->sinkronise();
} else {
    echo "noOperation";
}
?>