<?php

//require_once("_Reuseable/budget_2014_dbCalling.php");
require_once("_Reuseable/ebudget_dbCalling.php");
require_once("_Reuseable/postgres_databaseCalling.php");
require_once("_Reuseable/function.php");

if ($_POST["radio1"] == 1) {
    $jalanFix = trim($_POST["jalan"]);
} else if ($_POST["radio2"] == 2) {
    $jalanFix = trim($_POST["jalan2"]);
}

//get data post
$unit_id = $_POST["unit_id"];
$kegiatan_code = $_POST["kegiatan_code"];
$id_waiting = $_POST["id_waiting"];
$satuan = $_POST["satuan"];
$volume = $_POST["volume"];
$nilai_anggaran = $_POST["nilai_anggaran"];
$tahun = $_POST["tahun"];
$id_kelompok = $_POST["id_kelompok"];
$geojson = $_POST["geojson"];
$keterangan = $_POST["keterangan"];
$nmuser = $_POST["nmuser"];
$level = $_POST["level"];
$arr_data = array();
$datalama = '';
$keteranganalamat = $_POST["keteranganalamat"];
$koordinat = $_POST["koordinat"];
$lokasi_ke = $_POST["lokasi_ke"];

$unit_id = str_replace('%20', ' ', $unit_id);
$kegiatan_code = str_replace('%20', ' ', $kegiatan_code);
$id_waiting = str_replace('%20', ' ', $id_waiting);
$satuan = str_replace('%20', ' ', $satuan);
$volume = str_replace('%20', ' ', $volume);
$nilai_anggaran = str_replace('%20', ' ', $nilai_anggaran);
$tahun = str_replace('%20', ' ', $tahun);
$id_kelompok = str_replace('%20', ' ', $id_kelompok);
$geojson = str_replace('%20', ' ', $geojson);
$keterangan = str_replace('%20', ' ', $keterangan);
$nmuser = str_replace('%20', ' ', $nmuser);
$level = str_replace('%20', ' ', $level);
$keteranganalamat = str_replace('%20', ' ', $keteranganalamat);
$jalan = str_replace('%20', ' ', $jalanFix);
$koordinat = str_replace('%20', ' ', $koordinat);
$lokasi_ke = str_replace('%20', '', $lokasi_ke);

if ($koordinat == 'NULL' || $koordinat == '') {
    $o['kode'] = md5('kembali');
    $o['resultcekkoordinat'] = '0';
    echo json_encode($o);
} else {
    $res1 = pg_query($con, 'SELECT * FROM ' . $skema_gis . '.geojsonlokasi_waitinglist 
    WHERE id_waiting=\'' . $id_waiting . '\' and kegiatan_code=\'' . $kegiatan_code . '\' and unit_id=\'' . $unit_id . '\' '
            . 'and lokasi_ke=\'' . $lokasi_ke . '\'');
    while ($baris1 = pg_fetch_array($res1)) {
        $arr_data[] = $baris1;
    }
    $id_geolokasi = $arr_data[0]["idgeolokasi"];
    $datalama = $arr_data[0]["geojson"];
    $mlokasi = $arr_data[0]["mlokasi"];

    $tanggal = date('Y-m-d H:i:s');
    $status = 1; // 1 untuk update
    $ipaddress = explode("|", _ip());

    if ($_POST["radio2"] == 2) {
//irul 30juni cek-tambah master_lokasi_gmap
        $cek_jalan = pg_query($conb, "SELECT COUNT(*) as tot FROM public.master_lokasi_gmap "
                . "WHERE trim(upper(replace(nama_lokasi,' ',''))) = trim(upper(replace('$jalan',' ',''))) LIMIT 1");
        if ($cek_jalan) {
            $row2 = pg_fetch_assoc($cek_jalan);
            $tot = $row2['tot'];

            if ($tot > 0) {
                $ambil_jalan = pg_query($conb, "SELECT * FROM public.master_lokasi_gmap "
                        . "WHERE trim(upper(replace(nama_lokasi,' ',''))) = trim(upper(replace('$jalan',' ',''))) LIMIT 1");
                if ($ambil_jalan) {
                    $row = pg_fetch_assoc($ambil_jalan);
                    $jalan = $row['nama_lokasi'];
                }
            } else {
                $max_jalan = pg_query($conb, "SELECT max(id) as tot FROM public.master_lokasi_gmap");
                $row = pg_fetch_assoc($max_jalan);
                $baru = $row['tot'];
                $baru = $baru + 1;

                $command_bikin_jalan = pg_query($conb, 'INSERT INTO 
                            "public"."master_lokasi_gmap" VALUES(
                              ' . $baru . ',
                              \'' . ucwords(trim($jalan)) . '\'
                            )') or die('1' . pg_last_error());
            }
        } else {
            $keteranganalamat = 'EROR CEK';
        }
//irul 30juni cek-tambah master_lokasi_gmap 
    }

    if ($jalan == $mlokasi) {
        $command = pg_query($con, 'UPDATE ' . $skema_gis . '.geojsonlokasi_waitinglist 
        set "koordinat"=\'' . $koordinat . '\', "geojson"=\'' . $geojson . '\', "keterangan_alamat"=\'' . trim($keteranganalamat) . '\', "keterangan"=\'' . trim($keterangan) . '\', "last_edit_time"=\'' . $tanggal . '\' 
            WHERE id_waiting=\'' . $id_waiting . '\' and kegiatan_code=\'' . $kegiatan_code . '\' and unit_id=\'' . $unit_id . '\' '
                . 'and lokasi_ke=\'' . $lokasi_ke . '\'');
    } else {
        $command = pg_query($con, 'UPDATE ' . $skema_gis . '.geojsonlokasi_waitinglist 
        set "koordinat"=\'' . $koordinat . '\', "mlokasi"=\'' . $jalan . '\', "geojson"=\'' . $geojson . '\', "keterangan_alamat"=\'' . trim($keteranganalamat) . '\', "keterangan"=\'' . trim($keterangan) . '\' , "last_edit_time"=\'' . $tanggal . '\' 
            WHERE id_waiting=\'' . $id_waiting . '\' and kegiatan_code=\'' . $kegiatan_code . '\' and unit_id=\'' . $unit_id . '\' '
                . 'and lokasi_ke=\'' . $lokasi_ke . '\'');
    }


    if (!$command) {
        echo("Mengubah data geolokasi gagal");
        $o['result1'] = '0';
    } else {
        $o['result1'] = '1';
        echo("Mengubah data geolokasi berhasil");

        /////////////////////////////////////////
//    $command2 = pg_query($con, 'INSERT INTO 
//        "loggmap_rev1" VALUES(
//											nextval(\'loggmap_rev1_idlog_seq\'::regclass),
//											\'' . $lokasi . '\',
//											\'' . $iduser . '\',
//											\'' . $datalama . '\',
//											\'' . $geo_json . '\',
//											\'' . $tanggal . '\',
//											' . $status . ',
//											\'' . $ipaddress[0] . '\',
//											\'' . $ipaddress[1] . '\'
//											)');
        /////////////////////////////////////////    

        $command2 = pg_query($con, 'INSERT INTO 
        ' . $skema_gis . '.loggmap_waitinglist VALUES(
        nextval(\'' . $skema_gis . '.loggmap__waitinglist_seq\'::regclass),
        \'' . $id_geolokasi . '\','
                . '\'' . $nmuser . '\',
                \'' . $datalama . '\',
                    \'' . $geojson . '\',
                        \'' . $tanggal . '\',
                            ' . $status . ',
                                \'' . $ipaddress[0] . '\',
                                    \'' . $ipaddress[1] . '\',
                                        \'UPDATE\'    
                                        )');
        if (!$command2) {
            echo ("\nPencatatan log gagal!");
            $o['result2'] = '0';
        } else {
            $o['result2'] = '1';
        }
    }
    $o['kode'] = md5('update');
    echo json_encode($o);
}
?>