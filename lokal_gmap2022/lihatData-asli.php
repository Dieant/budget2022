<!DOCTYPE html>
<?php
	require_once("_Reuseable/postgres_databaseCalling.php");
	$kode_lokasi = $_GET["kode"];
	$user_id = $_GET["iduser"];
	//untuk google map
	$doc = new DOMDocument();
	$doc->version = '1.0';
	$doc->encoding = 'UTF-8';
	$tag_gml = $doc->createElement('gml');
	$doc->appendChild($tag_gml);
	$res = pg_query($con,'select * from "geodata_road"');
	
	$maxLat=-7.189505;
	$minLat=-7.393752;
	$maxLong=112.897099;
	$minLong=112.622609;
	//800x600
	// (-7.393752,112.622609)
	// (-7.189505,112.897099)
	//1000x1000
	// (-7.376760,112.673905)
	// (-7.206558,112.845811)
	$map_width=25600;
	$map_height=19200;
	if(pg_num_rows($res)>1){
		while($baris = pg_fetch_array($res))
		{
			//jalan
			if($baris['tipe']==1){
				$latlong=$baris['latlong'];
				//$linestring;
				$temp=explode(" ",$latlong);
				//echo count($temp);

				$linestring="";

				for($i=0;$i<count($temp);$i++){
					$temp2=explode(",",$temp[$i]);
					$yCoordinate=$map_height-($temp2[0]-$minLat)*$map_height/($maxLat-$minLat);
					$xCoordinate=($temp2[1]-$minLong)*$map_width/($maxLong-$minLong);
					$linestring=$linestring." ".$xCoordinate.",".$yCoordinate;
				}

				$temp2=explode(",",$temp[0]);
				$yCoordinate=$map_height-($temp2[0]-$minLat)*$map_height/($maxLat-$minLat)+5;
				$xCoordinate=($temp2[1]-$minLong)*$map_width/($maxLong-$minLong)+5;
				
				$temp2=explode(",",$temp[0]);
				$temp3=explode(",",$temp[1]);
				if ($temp3[0]>$temp2[0]){
					$temp4=$temp3[0];$temp3[0]=$temp2[0];$temp2[0]=$temp4;
				}
				if ($temp3[1]>$temp2[1]){
					$temp4=$temp3[1];$temp3[1]=$temp2[1];$temp2[1]=$temp4;
				}
				$angle=round(atan(($temp3[0]-$temp2[0])/($temp3[1]-$temp2[1]))*180/3.14,0);
				
				$tag_place = $doc->createElement('road');
				$tag_id = $doc->createElement('id',$baris['id']);
				$tag_desc = $doc->createElement('desc',$baris['nama']);
				$tag_x = $doc->createElement('xCoordinate',$xCoordinate);
				$tag_y = $doc->createElement('yCoordinate',$yCoordinate);
				$tag_angle = $doc->createElement('angle',$angle);
				$tag_coordinates   = $doc->createElement('lineString',$linestring);
				
				$tag_gml->appendChild($tag_place);
				$tag_place->appendChild($tag_id);
				$tag_place->appendchild($tag_coordinates);
				$tag_place->appendchild($tag_x);
				$tag_place->appendchild($tag_y);
				$tag_place->appendchild($tag_angle);
				$tag_place->appendchild($tag_desc);
			}
			//jembatan
			elseif ($baris['tipe']==2){
				$temp=explode(",",$baris['latlong']);
				$yCoordinate=$map_height-($temp[0]-$minLat)*$map_height/($maxLat-$minLat)-16;
				$xCoordinate=($temp[1]-$minLong)*$map_width/($maxLong-$minLong)-16;
				$linestring=$xCoordinate.",".$yCoordinate;

				$tag_place = $doc->createElement('bridge');
				$tag_id = $doc->createElement('id',$baris['id']);
				$tag_desc = $doc->createElement('desc',$baris['nama']);
				$tag_x = $doc->createElement('xCoordinate',$xCoordinate);
				$tag_y = $doc->createElement('yCoordinate',$yCoordinate);
				
				$tag_gml->appendChild($tag_place);
				$tag_place->appendchild($tag_id);
				$tag_place->appendchild($tag_desc);
				$tag_place->appendchild($tag_x);
				$tag_place->appendchild($tag_y);
			}
		}
		
		
		$doc->formatOutput = true;
		 
		$xslt = new xsltProcessor;
		$xsl = new DOMDocument(); 
		$xsl->load('LayerMap.xsl'); 
		$xslt->importStyleSheet($xsl);
		print $xslt->transformToXML($doc);
		//$doc->save('cek.gml');
	}else{
		$tag_place = $doc->createElement('road');
		
		$tag_gml->appendChild($tag_place);

		$doc->formatOutput = true;
		 
		//$xslt = new xsltProcessor;
		$xsl = new DOMDocument(); 
		$xsl->load('LayerMap.xsl'); 
		//$xslt->importStyleSheet($xsl);
		//print $xslt->transformToXML($doc);
		//$doc->save('cek.gml');
	}
	$arr_data = array();
	$res1 = pg_query($con,'SELECT * FROM "geojsonlokasi" WHERE "kode_lokasi"=\''.$kode_lokasi.'\'');
	while($baris1 = pg_fetch_array($res1)){
		$arr_data[] = $baris1;
	}
?>
<html>
<title>Geo+ eBudgeting - Ubah data geolokasi baru</title>
<link href="css/ebudgeting.css" rel="stylesheet" type="text/css"/>
	<link href="css/util.css" rel="stylesheet" type="text/css"/>
	<meta name="viewport" content="initial-scale=1.0, user-scalable=no">
	<meta charset="utf-8">
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<!--<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyA-318W-HfokybWpB4CyBh7XoODjG89X9w&sensor=false&libraries=places"></script>-->
	<!-- <script src="http://maps.googleapis.com/maps/api/js?v=3.6&key=AIzaSyA-318W-HfokybWpB4CyBh7XoODjG89X9w&sensor=false&libraries=geometry,places"></script> -->
	<script src="https://maps.googleapis.com/maps/api/js?v=3.30&key=AIzaSyCP2Ldnd_jUTViNUVV4I_l_iRlZrbaqLeg&sensor=false&libraries=geometry,places"></script>
	
	<script>
		var kode_lokasi = "<?php echo $kode_lokasi; ?>";
		var hitungLuasan=0;
		var countMark=1;
		var map;
		var markersArray = [];
		var bacaJson = $.parseJSON('<?php echo $arr_data[0]["geojson"]; ?>');
		var tipe = bacaJson.features[0].geometry.type;
		$(document).ready(function(){
			$('#inputTipeBaru').change(function() {
				tipe=$('#inputTipeBaru').val();
				//overlay.setMap(null);
				//document.getElementById('hiddenLayer').appendChild(document.getElementById('layer1'));
				initialize();
			});
		});
		
		var geocoder;
		function codeAddress(){
			var sAddress = $("#searchTextField").val();
			geocoder.geocode( { 'address': sAddress}, function(results, status) { 
			if (status == google.maps.GeocoderStatus.OK) {
				map.setCenter(results[0].geometry.location);
				if(tipe=='Point'){
					markersArray[0].setPosition(results[0].geometry.location);
				}
				//alert('done');
			}
			else{
				//jika tidak bisa lakukan sesuatu
				//alert('err');
			}
			});
		}
		
		function initialize() {
			geocoder = new google.maps.Geocoder();
			
			$("#inputTipeBaru").val(tipe);
			$("#tambahanField").val(bacaJson.keterangan);
			
			hitungLuasan=0;
			countMark=1;
			markersArray = [];
			
			var areaOptions = { 
				strokeColor: '#FF0000',
				strokeOpacity: 0.8,
				strokeWeight: 3,
				fillColor: '#FF0000',
				fillOpacity: 0.35,
				draggable:false
			}
			area = new google.maps.Polygon(areaOptions);
			
			var polyOptions = {
				strokeColor: '#FF0000',
				strokeOpacity: 1.0,
				strokeWeight: 3
			}
			poly = new google.maps.Polyline(polyOptions);
			var newcenterX=-7.257822;
			var newcenterY=112.746998;
			var maxX=-180;
			var minX=180;
			var maxY=-180;
			var minY=180;
			
			if(tipe!="Point"){
				if(tipe==bacaJson.features[0].geometry.type){
					if(tipe=='LineString'){
						for (var i = 0; i < bacaJson.features[0].geometry.coordinates.length; i++) {
							if(bacaJson.features[0].geometry.coordinates[i][0] > maxX){
								maxX = bacaJson.features[0].geometry.coordinates[i][0];
							}
							if(bacaJson.features[0].geometry.coordinates[i][0] < minX){
								minX = bacaJson.features[0].geometry.coordinates[i][0];
							}
							if(bacaJson.features[0].geometry.coordinates[i][1] > maxY){
								maxY = bacaJson.features[0].geometry.coordinates[i][1];
							}
							if(bacaJson.features[0].geometry.coordinates[i][1] < minY){
								minY = bacaJson.features[0].geometry.coordinates[i][1];
							}
						}
					}else{
						for (var i = 0; i < bacaJson.features[0].geometry.coordinates[0].length; i++) {
							if(bacaJson.features[0].geometry.coordinates[0][i][0] > maxX){
								maxX = bacaJson.features[0].geometry.coordinates[0][i][0];
							}
							if(bacaJson.features[0].geometry.coordinates[0][i][0] < minX){
								minX = bacaJson.features[0].geometry.coordinates[0][i][0];
							}
							if(bacaJson.features[0].geometry.coordinates[0][i][1] > maxY){
								maxY = bacaJson.features[0].geometry.coordinates[0][i][1];
							}
							if(bacaJson.features[0].geometry.coordinates[0][i][1] < minY){
								minY = bacaJson.features[0].geometry.coordinates[0][i][1];
							}
						}	
					}
					newcenterX = (minX+maxX)/2;
					newcenterY = (minY+maxY)/2;
				}else{
					newcenterX=-7.257822;
					newcenterY=112.746998;
				}
			}else{
				if(tipe==bacaJson.features[0].geometry.type){
					newcenterX = bacaJson.features[0].geometry.coordinates[0];
					newcenterY = bacaJson.features[0].geometry.coordinates[1];
				}else{
					newcenterX=-7.257822;
					newcenterY=112.746998;
				}
			}
			
			var mapOptions = {
				zoom: 17,
				center: new google.maps.LatLng(newcenterX, newcenterY),
				mapTypeId: google.maps.MapTypeId.ROADMAP
			};
			map = new google.maps.Map(document.getElementById('map-canvas'),mapOptions);
			
			//searchplace robert
			var defaultbounds=new google.maps.LatLngBounds(
				new google.maps.LatLng(-7.192188,112.593956),
				new google.maps.LatLng(-7.439411,112.810936));
			var options={
			bounds:defaultbounds,
			componentRestrictions:{country: 'id'}
			};
			
			//autocomplete
		  var input = /** @type {HTMLInputElement} */(document.getElementById('searchTextField'));
		  var autocomplete = new google.maps.places.Autocomplete(input,options);

		  autocomplete.bindTo('bounds', map);

		  var infowindow = new google.maps.InfoWindow();
		  var marker = new google.maps.Marker({
			map: map
		  });

		  google.maps.event.addListener(autocomplete, 'place_changed', function() {
			infowindow.close();
			marker.setVisible(false);
			input.className = '';
			var place = autocomplete.getPlace();
			if (!place.geometry) {
			  // Inform the user that the place was not found and return.
			  input.className = 'notfound';
			  return;
			}

			// If the place has a geometry, then present it on a map.
			if (place.geometry.viewport) {
			  map.fitBounds(place.geometry.viewport);
			} else {
			  map.setCenter(place.geometry.location);
			  map.setZoom(17);  // Why 17? Because it looks good.
			}
			marker.setIcon(/** @type {google.maps.Icon} */({
			  url: place.icon,
			  size: new google.maps.Size(71, 71),
			  origin: new google.maps.Point(0, 0),
			  anchor: new google.maps.Point(17, 34),
			  scaledSize: new google.maps.Size(35, 35)
			}));
			marker.setPosition(place.geometry.location);
			marker.setVisible(true);

			var address = '';
			if (place.address_components) {
			  address = [
				(place.address_components[0] && place.address_components[0].short_name || ''),
				(place.address_components[1] && place.address_components[1].short_name || ''),
				(place.address_components[2] && place.address_components[2].short_name || '')
			  ].join(' ');
			}

			infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
			infowindow.open(map, marker);
		  });
			//end searchplace robert
			//set bounding overlay --Marcell
			var swBound = new google.maps.LatLng(-7.393752,112.622609);
			var neBound = new google.maps.LatLng(-7.189505,112.897099);

			var bounds = new google.maps.LatLngBounds(swBound, neBound);
			//overlay = new USGSOverlay(bounds, map);

			if(tipe!="Point"){
				if(tipe==bacaJson.features[0].geometry.type){
					if(tipe=='LineString'){
						for (var i = 0; i < bacaJson.features[0].geometry.coordinates.length; i++) {
							var new_posXY = new google.maps.LatLng(bacaJson.features[0].geometry.coordinates[i][0],bacaJson.features[0].geometry.coordinates[i][1]);
							addMarker(new_posXY);
						}
					}else{
						for (var i = 0; i < bacaJson.features[0].geometry.coordinates[0].length; i++) {
							var new_posXY = new google.maps.LatLng(bacaJson.features[0].geometry.coordinates[0][i][0],bacaJson.features[0].geometry.coordinates[0][i][1]);
							addMarker(new_posXY);
						}				
					}
				}
				//google.maps.event.addListener(map, 'click', function(event) {addMarker(event.latLng);});
			}else{
				if(tipe==bacaJson.features[0].geometry.type){
					marker = new google.maps.Marker({
						position: new google.maps.LatLng(bacaJson.features[0].geometry.coordinates[0], bacaJson.features[0].geometry.coordinates[1]),
						map: map,
						draggable:false,
						animation: google.maps.Animation.DROP,
						title:countMark+""
					});
				}else{
					marker = new google.maps.Marker({
						position: new google.maps.LatLng(-7.257822,112.746998),
						map: map,
						draggable:false,
						animation: google.maps.Animation.DROP,
						title:countMark+""
					});
				}
				countMark++;
				markersArray.push(marker);
			}
			
			if(tipe=='Polygon'){area.setMap(map);}
		
			if(tipe=='LineString'){poly.setMap(map);}
			
			//google.maps.event.addListener(area, 'drag', dragArea);
			if(tipe=='Point'){
				$("#keteranganPopUp").css('display','none');
				$("#keteranganPopUp p").html('1 Titik');
			}else if(tipe=='LineString'){
				$("#keteranganPopUp").css('display','block');
				$("#keteranganPopUp div").html('Jarak :');
				$("#keteranganPopUp p").html('0 M1');
			}else if(tipe=='Polygon'){
				$("#keteranganPopUp").css('display','block');
				$("#keteranganPopUp div").html('Luas :');
				$("#keteranganPopUp p").html('0 M2');
			}
			hitungGeometry();
		}
		
		function dragArea(){
			var pathArea = area.getPath();
			//var pathPoly = poly.getPath();
			var besarArrayPathArea = pathArea.getArray().length;
			for(var i=0;i<besarArrayPathArea;i++){
				var posXY = new google.maps.LatLng(pathArea.getArray()[i].lat(),pathArea.getArray()[i].lng());
				markersArray[i].setPosition(posXY);
				//pathPoly.setAt(i,posXY);
			}
			hitungGeometry();
		}
		
		function dragMark(){
			var pathArea = area.getPath();
			var pathPoly = poly.getPath();
			var indexMark = parseInt(this.getTitle())-1;
			var posXY=new google.maps.LatLng(this.getPosition().lat(),this.getPosition().lng())
			pathArea.setAt(indexMark,posXY);
			pathPoly.setAt(indexMark,posXY);
			hitungGeometry();
		}
		
		function addMarker(location) {
			var x=location.lat();
			var y=location.lng();
			marker = new google.maps.Marker({
				position: location,
				map: map,
				draggable:false,
				animation: google.maps.Animation.DROP,
				title:countMark+""
			});
			countMark++;
			//google.maps.event.addListener(marker, 'dblclick', removeMark);
			//google.maps.event.addListener(marker, 'drag', dragMark);
			markersArray.push(marker);
			var pathArea = area.getPath();
			pathArea.push(location);
			var pathPoly = poly.getPath();
			pathPoly.push(location);
			hitungGeometry();
		}
		
		function removeMark() {
			//alert(this.getPosition());
			var hapusMark;
			var countUlang=1;
			var pathArea = area.getPath();
			var pathPoly = poly.getPath();
			for(i in markersArray){
				if(markersArray[i].getPosition() == this.getPosition()){
					hapusMark=i;
				}else{
					markersArray[i].setTitle(countUlang+"");
					countUlang++;
				}
			}
			pathArea.removeAt(hapusMark);
			pathPoly.removeAt(hapusMark);
			markersArray[hapusMark].setMap(null);
			markersArray.splice(hapusMark,1);
			countMark--;
			hitungGeometry();
		}
		//Function overlay master dari db --marcell
		function USGSOverlay(bounds, map) {

			this.bounds_ = bounds;
			this.map_ = map;
			this.div_ = null;
			this.setMap(map);
		}

		//USGSOverlay.prototype = new google.maps.OverlayView();

		USGSOverlay.prototype.onAdd = function() {
			var div = document.getElementById('layer1');
			div.style.border = 'none';
			div.style.borderWidth = '0px';
			div.style.position = 'absolute';
			
			this.div_ = div;

			var panes = this.getPanes();
			panes.overlayLayer.appendChild(div);
		}
		USGSOverlay.prototype.draw = function() {
			var overlayProjection = this.getProjection();

			var sw = overlayProjection.fromLatLngToDivPixel(this.bounds_.getSouthWest());
			var ne = overlayProjection.fromLatLngToDivPixel(this.bounds_.getNorthEast());

			// Resize the image's DIV to fit the indicated dimensions.
			var div = this.div_;
			div.style.left = sw.x + 'px';
			div.style.top = ne.y + 'px';
			div.style.width = (ne.x - sw.x) + 'px';
			div.style.height = (sw.y - ne.y) + 'px';
		}
		USGSOverlay.prototype.onRemove = function() {
			//this.div_.parentNode.removeChild(this.div_);
			this.div_ = null;
		}
		//end of overlay function	
		google.maps.event.addDomListener(window, 'load', initialize);
		
		function submitMap(){
			var iduser = '<?php echo $user_id; ?>';
			keterangantambahan = $("#tambahanField").val();
			tipe=$("#inputTipeBaru").val();
			var myJSON = {};
			myJSON.type="FeatureCollection"
			myJSON.features=[];
			myJSON.keterangan=keterangantambahan;
			myJSON.features[0]={};
			myJSON.features[0].type="Feature";
			myJSON.features[0].geometry={};
			myJSON.features[0].geometry.type=tipe;
			myJSON.features[0].geometry.coordinates=[];
			if(tipe=="Point"){
				myJSON.features[0].geometry.coordinates=[markersArray[0].getPosition().lat(),markersArray[0].getPosition().lng()];
			}else if(tipe=="LineString"){
				var pathPoly = poly.getPath();
				var besarArrayPathPoly= pathPoly.getArray().length;
				for(var i=0;i<besarArrayPathPoly;i++){
					myJSON.features[0].geometry.coordinates[i]=[pathPoly.getArray()[i].lat(),pathPoly.getArray()[i].lng()];
				}
				myJSON.features[0].properties={};
				myJSON.features[0].properties.jarak=hitungLuasan;
			}else if(tipe=="Polygon"){
				var pathArea = area.getPath();
				var besarArrayPathArea = pathArea.getArray().length;
				myJSON.features[0].geometry.coordinates[0]=[];
				for(var i=0;i<besarArrayPathArea;i++){
					myJSON.features[0].geometry.coordinates[0][i]=[pathArea.getArray()[i].lat(),pathArea.getArray()[i].lng()];
				}
				myJSON.features[0].properties={};
				myJSON.features[0].properties.luas=hitungLuasan;
			}
			
			var myJSONText = JSON.stringify(myJSON);
			$.post("proses_UpdateProyek.php",{
			geo_json:myJSONText,lokasi:kode_lokasi,iduser:iduser},
				function(data){
					alert(data);
					window.opener.document.location.href = "http://gmap.surabaya-eproc.or.id/budget2014sf-gmap/index.php/lokasi/list.html";
                                        //location.reload(true);
					window.location.reload();
				}
			);
		}
		function hitungGeometry(){
			if(tipe=="LineString"){
				hitungLuasan = google.maps.geometry.spherical.computeLength(poly.getPath());
				$("#keteranganPopUp p").html((Math.floor(hitungLuasan * 1000) / 1000)+' M1');
			}else if(tipe=="Polygon"){
				hitungLuasan = google.maps.geometry.spherical.computeArea(area.getPath());
				$("#keteranganPopUp p").html((Math.floor(hitungLuasan * 1000) / 1000)+' M2');
			}else if(tipe=="Point"){
				$("#keteranganPopUp p").html('1 Titik');
			}
		}
		
	</script>
</head>
<div id="hiddenLayer"></div>
<body>
	
	<div style="position:relative;width:1000px">

	
	<div id="keteranganPopUp"><div>JARAK:</div><p></p></div>
		<div class="fr callout-new">
				<table style="width:92%;" class="tablecalloutnew">
					<tr><td>Jenis markah:</td></tr>
					<tr><td>
						<select id="inputTipeBaru" name="inputTipeBaru" disabled>
							<option id="Point" value="Point">Titik</option>
							<option id="LineString" value="LineString">Garis</option>
							<option id="Polygon" value="Polygon">Area</option>
						</select>
					</td></tr>
					<tr><td></td></tr>
					<tr><td><input type="text" name="searchTextField" id="searchTextField" class="m" onclick="this.select();" value="" style='display:none'/></td></tr>
					<tr><td>Keterangan Tambahan:</td></tr>
					<tr><td><input type="text" name="tambahanField" id="tambahanField" class="m" onclick="this.select();" value="" disabled /></td></tr>
					
					<tr><td><input type="button" value="Ubah Data" name="btnSubmit" id="btnSubmit" onclick="submitMap()"style='display:none'/></td></tr>
				</table>
		</div>
	<div class="cb"></div>
	<div id="map-canvas"></div>
	</div>
</body>
</html>