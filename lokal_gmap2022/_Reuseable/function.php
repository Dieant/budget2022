<?php

function _ip() {
    $ip = $_SERVER['REMOTE_ADDR'];
    //Log $ip here 

    if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ip .= "|" . $_SERVER['HTTP_X_FORWARDED_FOR'];
        //Log $forwardedIP here 
    }
    return $ip;
}

?>