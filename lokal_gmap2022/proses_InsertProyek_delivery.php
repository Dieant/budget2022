<?php

//require_once("_Reuseable/budget_2014_dbCalling.php");
require_once("_Reuseable/ebudget_dbCalling.php");
require_once("_Reuseable/postgres_databaseCalling.php");
require_once("_Reuseable/function.php");

if ($_POST["radio1"] == 1) {
    $jalanFix = trim($_POST["jalan"]);
} else if ($_POST["radio2"] == 2) {
    $jalanFix = trim($_POST["jalan2"]);
}

$unit_id = $_POST["unit_id"];
$kegiatan_code = $_POST["kegiatan_code"];
$detail_no = $_POST["detail_no"];
$satuan = $_POST["satuan"];
$volume = $_POST["volume"];
$nilai_anggaran = $_POST["nilai_anggaran"];
$tahun = $_POST["tahun"];
//$mlokasi = $_POST["mlokasi"];
$id_kelompok = $_POST["id_kelompok"];
$geojson = $_POST["geojson"];
$keterangan = $_POST["keterangan"];
$nmuser = $_POST["nmuser"];
$level = $_POST["level"];
$keteranganalamat = $_POST["keteranganalamat"];
$koordinat = $_POST["koordinat"];
$lokasi_ke = $_POST["lokasi_ke"];

$unit_id = str_replace('%20', ' ', $unit_id);
$kegiatan_code = str_replace('%20', ' ', $kegiatan_code);
$detail_no = str_replace('%20', ' ', $detail_no);
$satuan = str_replace('%20', ' ', $satuan);
$volume = str_replace('%20', ' ', $volume);
$nilai_anggaran = str_replace('%20', ' ', $nilai_anggaran);
$tahun = str_replace('%20', ' ', $tahun);
//$mlokasi = str_replace('%20', ' ', $mlokasi);
$id_kelompok = str_replace('%20', ' ', $id_kelompok);
$geojson = str_replace('%20', ' ', $geojson);
$keterangan = str_replace('%20', ' ', $keterangan);
$nmuser = str_replace('%20', ' ', $nmuser);
$level = str_replace('%20', ' ', $level);
$jalan = str_replace('%20', ' ', $jalanFix);
$keteranganalamat = str_replace('%20', ' ', $keteranganalamat);
$koordinat = str_replace('%20', ' ', $koordinat);
$lokasi_ke = str_replace('%20', '', $lokasi_ke);

if ($koordinat == 'NULL' || $koordinat == '') {
    $o['kode'] = md5('kembali');
    $o['resultcekkoordinat'] = '0';
    echo json_encode($o);
} else {
    //lengkapi dengan data lain
    $datalama = '';
    $tanggal = date('Y-m-d H:i:s');
    $status = 0; // 0 untuk insert
    $ipaddress = explode("|", _ip());
    $o;
    $o['unit_id'] = $unit_id;
    $o['kegiatan_code'] = $kegiatan_code;
    $o['detail_no'] = $detail_no;

    $ambil_data = pg_query($conb, 'SELECT * FROM "' . $skema_budget . '"."dinas_rincian_detail" '
            . 'WHERE detail_no=\'' . $detail_no . '\' and kegiatan_code=\'' . $kegiatan_code . '\' and unit_id=\'' . $unit_id . '\' LIMIT 1');
    if ($ambil_data) {
        $row = pg_fetch_assoc($ambil_data);
        $komponen_name = $row['komponen_name'] . ' ' . $row['detail_name'];
    }

    $ambil_data_gis = pg_query($conb, 'SELECT count(*) as total FROM "' . $skema_gis . '.geojsonlokasi_rev1" '
            . 'WHERE detail_no=\'' . $detail_no . '\' and kegiatan_code=\'' . $kegiatan_code . '\' and unit_id=\'' . $unit_id . '\' and lokasi = \'' . $lokasi_ke . '\' and status_hapus = false ');
    if ($ambil_data_gis) {
        $row_data_gis = pg_fetch_assoc($ambil_data_gis);
        $total_ada_gis = $row_data_gis['total'];
    }

    $ambil_nama_skpd = pg_query($conb, "SELECT * FROM public.unit_kerja WHERE unit_id='$unit_id' LIMIT 1");
    if ($ambil_nama_skpd) {
        $row = pg_fetch_assoc($ambil_nama_skpd);
        $unit_name = trim($row['unit_name']);
    }

    if ($_POST["radio2"] == 2) {
//irul 30juni cek-tambah master_lokasi_gmap
        $cek_jalan = pg_query($conb, "SELECT COUNT(*) as tot FROM public.master_lokasi_gmap "
                . "WHERE trim(upper(replace(nama_lokasi,' ',''))) = trim(upper(replace('$jalan',' ',''))) LIMIT 1");
        if ($cek_jalan) {
            $row2 = pg_fetch_assoc($cek_jalan);
            $tot = $row2['tot'];
            if ($tot > 0) {
                $ambil_jalan = pg_query($conb, "SELECT * FROM public.master_lokasi_gmap "
                        . "WHERE trim(upper(replace(nama_lokasi,' ',''))) = trim(upper(replace('$jalan',' ',''))) LIMIT 1");
                if ($ambil_jalan) {
                    $row = pg_fetch_assoc($ambil_jalan);
                    $jalan = $row['nama_lokasi'];
                }
            } else {
                $max_jalan = pg_query($conb, "SELECT max(id) as tot FROM public.master_lokasi_gmap");
                $row = pg_fetch_assoc($max_jalan);
                $baru = $row['tot'];
                $baru = $baru + 1;

                $command_bikin_jalan = pg_query($conb, 'INSERT INTO 
                            "public"."master_lokasi_gmap" VALUES(
                              ' . $baru . ',
                              \'' . ucwords(trim($jalan)) . '\'
                            )') or die('1' . pg_last_error());
            }
        } else {
            $keteranganalamat = 'EROR CEK';
        }
//irul 30juni cek-tambah master_lokasi_gmap
    }

    if ($total_ada_gis == 0) {
        $command = pg_query($con, 'INSERT INTO 
                            ' . $skema_gis . '.geojsonlokasi_rev1 VALUES(
                              nextval(\'' . $skema_gis . '.geojsonlokasi_seq\'::regclass),
                              \'' . $unit_id . '\',
                              \'' . $kegiatan_code . '\',
                              ' . $detail_no . ',
                              \'' . trim($satuan) . '\',
                              \'' . $volume . '\',
                              ' . $nilai_anggaran . ',
                              \'' . $tahun . '\',
                              \'' . trim($jalan) . '\',
                              ' . $id_kelompok . ',
                              \'' . $geojson . '\',
                              \'' . trim($keterangan) . '\',
                              \'' . $nmuser . '\',
                              ' . $level . ',
                              \'' . trim($komponen_name) . '\',    
                              FALSE,
                              \'' . $keteranganalamat . '\',
                              \'' . trim($unit_name) . '\' ,
                              \'' . $tanggal . '\',
                              \'' . $tanggal . '\',
                              \'' . $koordinat . '\',
                              \'' . $lokasi_ke . '\',
                              \'' . $unit_id . '.' . $kegiatan_code . '.' . $detail_no . '\'
                            )') or die('1' . pg_last_error());
        if (!$command) {
            $o['result1'] = '0';
        } else {
            $o['result1'] = '1';
            $command3 = pg_query($con, 'SELECT * FROM ' . $skema_gis . '.geojsonlokasi_rev1 
        WHERE detail_no=\'' . $detail_no . '\' and kegiatan_code=\'' . $kegiatan_code . '\' and unit_id=\'' . $unit_id . '\' 
            and lokasi_ke=\'' . $lokasi_ke . '\' 
            ORDER BY idgeolokasi DESC LIMIT 1');
            if (!$command3) {
                $o['result2'] = '0';
            } else {
                $row = pg_fetch_assoc($command3);
                $command2 = pg_query($con, 'INSERT INTO 
                                ' . $skema_gis . '.loggmap_rev1 VALUES(
                                nextval(\'' . $skema_gis . '.loggmap_seq\'::regclass),
                                \'' . $row['idgeolokasi'] . '\',
                                \'' . $nmuser . '\',
                                \'' . $datalama . '\',
                                \'' . $geojson . '\',
                                \'' . $tanggal . '\',
                                ' . $status . ',
                                \'' . $ipaddress[0] . '\',
                                \'' . $ipaddress[1] . '\',
                                \'INSERT\'    
                                )') or die(pg_last_error());
                $o['idgeolokasi'] = $row['idgeolokasi'];
                if (!$command2) {
                    $o['result2'] = '0';
                } else {
                    $o['result2'] = '1';
//            $command_statushapus = pg_query($conb2014, 'UPDATE "budget2014"."dinas_rincian_detail" set "status_hapus"=false WHERE detail_no=\'' . $detail_no . '\' and kegiatan_code=\'' . $kegiatan_code . '\' and unit_id=\'' . $unit_id . '\'');
                    $command_statushapus = pg_query($conb, 'UPDATE "' . $skema_budget . '"."dinas_rincian_detail" '
                            . 'set "status_hapus"=false WHERE detail_no=\'' . $detail_no . '\' and kegiatan_code=\'' . $kegiatan_code . '\' and unit_id=\'' . $unit_id . '\'');
                }
            }
        }
    } else {
        $o['result1'] = '1';
        $o['result2'] = '1';
    }
    $o['kode'] = md5('insert');
    echo json_encode($o);
}
?>