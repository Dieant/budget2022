//http://www.onlineleaf.com/
var idleTime = 0;
var standby = false;
function CheckInactivity() { idleTime += 1; if(idleTime > 50) { InitializeStandBy(); } }
function InitializeStandBy() { if(!standby) { var h = 0; h = jQuery(window).height(); jQuery("#energysaving").show().css({ height: "0", width: "0", left: "50%", top: "50%" }).animate({ width: "100%", height: h, left: "0", top: "0", opacity: "1" }, 1000); standby = true; } }
function StopStandBy() { if(standby) { jQuery("#energysaving").animate({ width: "0", height: "0", opacity: "0", left: "50%", top: "50%" }, 500); setTimeout('jQuery("#energysaving").hide();', 800); standby = false; } }
function InitJQuery() { if(typeof(jQuery) == "undefined") { setTimeout("InitJQuery();", 50); } else { jQuery(function() { jQuery(document).ready(function() { setInterval("CheckInactivity();", 10000); jQuery(this).mousemove(function(e) { idleTime = 0; StopStandBy(); }); jQuery(document).keypress(function(e) { idleTime = 0; StopStandBy(); });
                jQuery("body").append('<div id="energysaving"><p>Mode Penyimpanan Energy<br /><span>Gerakan Mouse anda untuk kembali ke halaman awal.</span></p><div id="copyrightOnlineLeaf">Copyright 2018 <font style="color: #666666;">ebudgeting.surabaya.go.id</font> - All rights reserved</div></div>'); jQuery("#energysaving").hide(); }); }); } } InitJQuery();