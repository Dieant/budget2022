<?php
$allowedIP = array('10.200.3.205','10.200.3.201','::1','10.200.3.74','10.200.3.75');
if(!in_array($_SERVER['REMOTE_ADDR'], $allowedIP)){
//   echo @$_SERVER['REMOTE_ADDR'];
//    echo gethostbyaddr($_SERVER['REMOTE_ADDR']);
    die('You are not allowed to access this address.');
}

define('SF_ROOT_DIR',    realpath(dirname(__FILE__).'/..'));
define('SF_APP',         'budgeting');
define('SF_ENVIRONMENT', 'dev');
define('SF_DEBUG',       true);

require_once(SF_ROOT_DIR.DIRECTORY_SEPARATOR.'apps'.DIRECTORY_SEPARATOR.SF_APP.DIRECTORY_SEPARATOR.'config'.DIRECTORY_SEPARATOR.'config.php');
sfContext::getInstance()->getController()->dispatch();
