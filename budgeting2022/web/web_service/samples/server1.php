<?php
//call library
require_once('../lib/nusoap.php');
$URL       = "http://budget.localhost/budget2016sf/web_service/samples/server1.php";
$namespace = $URL . '?wsdl';
//using soap_server to create server object
$server    = new soap_server;
$server->configureWSDL('hellotesting', $namespace);

//register a function that works on server
$server->register('hello',
    array("name" => "xsd:string", "hai" => "xsd:number"),
    array("return" => "xsd:string"),
    $namespace);

// create the function
function hello($name, $hai)
{
    if (!$name) {
        return new soap_fault('Client', '', 'Put your name!');
    }
    $result = "Hello, " . $name . " - " . $hai;
    return $result;
}
// create HTTP listener
$server->service($HTTP_RAW_POST_DATA);
exit();
?>