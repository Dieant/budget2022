<?php


abstract class BaseBaruHistoryPekerjaanPeer {

	
	const DATABASE_NAME = 'gis';

	
	const TABLE_NAME = 'baru_history_pekerjaan';

	
	const CLASS_DEFAULT = 'lib.model.gis.BaruHistoryPekerjaan';

	
	const NUM_COLUMNS = 13;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const ID_HISTORY = 'baru_history_pekerjaan.ID_HISTORY';

	
	const TAHUN = 'baru_history_pekerjaan.TAHUN';

	
	const KODE_BUDGETING = 'baru_history_pekerjaan.KODE_BUDGETING';

	
	const KOMPONEN_NAME = 'baru_history_pekerjaan.KOMPONEN_NAME';

	
	const DETAIL_NAME = 'baru_history_pekerjaan.DETAIL_NAME';

	
	const SATUAN = 'baru_history_pekerjaan.SATUAN';

	
	const VOLUME = 'baru_history_pekerjaan.VOLUME';

	
	const NILAI_ANGGARAN = 'baru_history_pekerjaan.NILAI_ANGGARAN';

	
	const UNIT_ID = 'baru_history_pekerjaan.UNIT_ID';

	
	const KEGIATAN_CODE = 'baru_history_pekerjaan.KEGIATAN_CODE';

	
	const DETAIL_NO = 'baru_history_pekerjaan.DETAIL_NO';

	
	const SCHEMA_NAME = 'baru_history_pekerjaan.SCHEMA_NAME';

	
	const DATABASE_NAME = 'baru_history_pekerjaan.DATABASE_NAME';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('IdHistory', 'Tahun', 'KodeBudgeting', 'KomponenName', 'DetailName', 'Satuan', 'Volume', 'NilaiAnggaran', 'UnitId', 'KegiatanCode', 'DetailNo', 'SchemaName', 'DatabaseName', ),
		BasePeer::TYPE_COLNAME => array (BaruHistoryPekerjaanPeer::ID_HISTORY, BaruHistoryPekerjaanPeer::TAHUN, BaruHistoryPekerjaanPeer::KODE_BUDGETING, BaruHistoryPekerjaanPeer::KOMPONEN_NAME, BaruHistoryPekerjaanPeer::DETAIL_NAME, BaruHistoryPekerjaanPeer::SATUAN, BaruHistoryPekerjaanPeer::VOLUME, BaruHistoryPekerjaanPeer::NILAI_ANGGARAN, BaruHistoryPekerjaanPeer::UNIT_ID, BaruHistoryPekerjaanPeer::KEGIATAN_CODE, BaruHistoryPekerjaanPeer::DETAIL_NO, BaruHistoryPekerjaanPeer::SCHEMA_NAME, BaruHistoryPekerjaanPeer::DATABASE_NAME, ),
		BasePeer::TYPE_FIELDNAME => array ('id_history', 'tahun', 'kode_budgeting', 'komponen_name', 'detail_name', 'satuan', 'volume', 'nilai_anggaran', 'unit_id', 'kegiatan_code', 'detail_no', 'schema_name', 'database_name', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('IdHistory' => 0, 'Tahun' => 1, 'KodeBudgeting' => 2, 'KomponenName' => 3, 'DetailName' => 4, 'Satuan' => 5, 'Volume' => 6, 'NilaiAnggaran' => 7, 'UnitId' => 8, 'KegiatanCode' => 9, 'DetailNo' => 10, 'SchemaName' => 11, 'DatabaseName' => 12, ),
		BasePeer::TYPE_COLNAME => array (BaruHistoryPekerjaanPeer::ID_HISTORY => 0, BaruHistoryPekerjaanPeer::TAHUN => 1, BaruHistoryPekerjaanPeer::KODE_BUDGETING => 2, BaruHistoryPekerjaanPeer::KOMPONEN_NAME => 3, BaruHistoryPekerjaanPeer::DETAIL_NAME => 4, BaruHistoryPekerjaanPeer::SATUAN => 5, BaruHistoryPekerjaanPeer::VOLUME => 6, BaruHistoryPekerjaanPeer::NILAI_ANGGARAN => 7, BaruHistoryPekerjaanPeer::UNIT_ID => 8, BaruHistoryPekerjaanPeer::KEGIATAN_CODE => 9, BaruHistoryPekerjaanPeer::DETAIL_NO => 10, BaruHistoryPekerjaanPeer::SCHEMA_NAME => 11, BaruHistoryPekerjaanPeer::DATABASE_NAME => 12, ),
		BasePeer::TYPE_FIELDNAME => array ('id_history' => 0, 'tahun' => 1, 'kode_budgeting' => 2, 'komponen_name' => 3, 'detail_name' => 4, 'satuan' => 5, 'volume' => 6, 'nilai_anggaran' => 7, 'unit_id' => 8, 'kegiatan_code' => 9, 'detail_no' => 10, 'schema_name' => 11, 'database_name' => 12, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/gis/map/BaruHistoryPekerjaanMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.gis.map.BaruHistoryPekerjaanMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = BaruHistoryPekerjaanPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(BaruHistoryPekerjaanPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(BaruHistoryPekerjaanPeer::ID_HISTORY);

		$criteria->addSelectColumn(BaruHistoryPekerjaanPeer::TAHUN);

		$criteria->addSelectColumn(BaruHistoryPekerjaanPeer::KODE_BUDGETING);

		$criteria->addSelectColumn(BaruHistoryPekerjaanPeer::KOMPONEN_NAME);

		$criteria->addSelectColumn(BaruHistoryPekerjaanPeer::DETAIL_NAME);

		$criteria->addSelectColumn(BaruHistoryPekerjaanPeer::SATUAN);

		$criteria->addSelectColumn(BaruHistoryPekerjaanPeer::VOLUME);

		$criteria->addSelectColumn(BaruHistoryPekerjaanPeer::NILAI_ANGGARAN);

		$criteria->addSelectColumn(BaruHistoryPekerjaanPeer::UNIT_ID);

		$criteria->addSelectColumn(BaruHistoryPekerjaanPeer::KEGIATAN_CODE);

		$criteria->addSelectColumn(BaruHistoryPekerjaanPeer::DETAIL_NO);

		$criteria->addSelectColumn(BaruHistoryPekerjaanPeer::SCHEMA_NAME);

		$criteria->addSelectColumn(BaruHistoryPekerjaanPeer::DATABASE_NAME);

	}

	const COUNT = 'COUNT(*)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT *)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(BaruHistoryPekerjaanPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(BaruHistoryPekerjaanPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = BaruHistoryPekerjaanPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = BaruHistoryPekerjaanPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return BaruHistoryPekerjaanPeer::populateObjects(BaruHistoryPekerjaanPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			BaruHistoryPekerjaanPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = BaruHistoryPekerjaanPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return BaruHistoryPekerjaanPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(BaruHistoryPekerjaanPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(BaruHistoryPekerjaanPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof BaruHistoryPekerjaan) {

			$criteria = $values->buildCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
												if(count($values) == count($values, COUNT_RECURSIVE))
			{
								$values = array($values);
			}
			$vals = array();
			foreach($values as $value)
			{

			}

		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(BaruHistoryPekerjaan $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(BaruHistoryPekerjaanPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(BaruHistoryPekerjaanPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(BaruHistoryPekerjaanPeer::DATABASE_NAME, BaruHistoryPekerjaanPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = BaruHistoryPekerjaanPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

} 
if (Propel::isInit()) {
			try {
		BaseBaruHistoryPekerjaanPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/gis/map/BaruHistoryPekerjaanMapBuilder.php';
	Propel::registerMapBuilder('lib.model.gis.map.BaruHistoryPekerjaanMapBuilder');
}
