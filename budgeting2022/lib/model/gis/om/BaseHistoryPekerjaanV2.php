<?php


abstract class BaseHistoryPekerjaanV2 extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id_history;


	
	protected $tahun;


	
	protected $kode_rka;


	
	protected $status_hapus;


	
	protected $jalan;


	
	protected $gang;


	
	protected $nomor;


	
	protected $rw;


	
	protected $rt;


	
	protected $keterangan;


	
	protected $tempat;


	
	protected $komponen;


	
	protected $kecamatan;


	
	protected $kelurahan;


	
	protected $lokasi;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getIdHistory()
	{

		return $this->id_history;
	}

	
	public function getTahun()
	{

		return $this->tahun;
	}

	
	public function getKodeRka()
	{

		return $this->kode_rka;
	}

	
	public function getStatusHapus()
	{

		return $this->status_hapus;
	}

	
	public function getJalan()
	{

		return $this->jalan;
	}

	
	public function getGang()
	{

		return $this->gang;
	}

	
	public function getNomor()
	{

		return $this->nomor;
	}

	
	public function getRw()
	{

		return $this->rw;
	}

	
	public function getRt()
	{

		return $this->rt;
	}

	
	public function getKeterangan()
	{

		return $this->keterangan;
	}

	
	public function getTempat()
	{

		return $this->tempat;
	}

	
	public function getKomponen()
	{

		return $this->komponen;
	}

	
	public function getKecamatan()
	{

		return $this->kecamatan;
	}

	
	public function getKelurahan()
	{

		return $this->kelurahan;
	}

	
	public function getLokasi()
	{

		return $this->lokasi;
	}

	
	public function setIdHistory($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id_history !== $v) {
			$this->id_history = $v;
			$this->modifiedColumns[] = HistoryPekerjaanV2Peer::ID_HISTORY;
		}

	} 
	
	public function setTahun($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->tahun !== $v) {
			$this->tahun = $v;
			$this->modifiedColumns[] = HistoryPekerjaanV2Peer::TAHUN;
		}

	} 
	
	public function setKodeRka($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_rka !== $v) {
			$this->kode_rka = $v;
			$this->modifiedColumns[] = HistoryPekerjaanV2Peer::KODE_RKA;
		}

	} 
	
	public function setStatusHapus($v)
	{

		if ($this->status_hapus !== $v) {
			$this->status_hapus = $v;
			$this->modifiedColumns[] = HistoryPekerjaanV2Peer::STATUS_HAPUS;
		}

	} 
	
	public function setJalan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->jalan !== $v) {
			$this->jalan = $v;
			$this->modifiedColumns[] = HistoryPekerjaanV2Peer::JALAN;
		}

	} 
	
	public function setGang($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->gang !== $v) {
			$this->gang = $v;
			$this->modifiedColumns[] = HistoryPekerjaanV2Peer::GANG;
		}

	} 
	
	public function setNomor($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nomor !== $v) {
			$this->nomor = $v;
			$this->modifiedColumns[] = HistoryPekerjaanV2Peer::NOMOR;
		}

	} 
	
	public function setRw($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->rw !== $v) {
			$this->rw = $v;
			$this->modifiedColumns[] = HistoryPekerjaanV2Peer::RW;
		}

	} 
	
	public function setRt($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->rt !== $v) {
			$this->rt = $v;
			$this->modifiedColumns[] = HistoryPekerjaanV2Peer::RT;
		}

	} 
	
	public function setKeterangan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->keterangan !== $v) {
			$this->keterangan = $v;
			$this->modifiedColumns[] = HistoryPekerjaanV2Peer::KETERANGAN;
		}

	} 
	
	public function setTempat($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->tempat !== $v) {
			$this->tempat = $v;
			$this->modifiedColumns[] = HistoryPekerjaanV2Peer::TEMPAT;
		}

	} 
	
	public function setKomponen($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->komponen !== $v) {
			$this->komponen = $v;
			$this->modifiedColumns[] = HistoryPekerjaanV2Peer::KOMPONEN;
		}

	} 
	
	public function setKecamatan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kecamatan !== $v) {
			$this->kecamatan = $v;
			$this->modifiedColumns[] = HistoryPekerjaanV2Peer::KECAMATAN;
		}

	} 
	
	public function setKelurahan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kelurahan !== $v) {
			$this->kelurahan = $v;
			$this->modifiedColumns[] = HistoryPekerjaanV2Peer::KELURAHAN;
		}

	} 
	
	public function setLokasi($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->lokasi !== $v) {
			$this->lokasi = $v;
			$this->modifiedColumns[] = HistoryPekerjaanV2Peer::LOKASI;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id_history = $rs->getInt($startcol + 0);

			$this->tahun = $rs->getString($startcol + 1);

			$this->kode_rka = $rs->getString($startcol + 2);

			$this->status_hapus = $rs->getBoolean($startcol + 3);

			$this->jalan = $rs->getString($startcol + 4);

			$this->gang = $rs->getString($startcol + 5);

			$this->nomor = $rs->getString($startcol + 6);

			$this->rw = $rs->getString($startcol + 7);

			$this->rt = $rs->getString($startcol + 8);

			$this->keterangan = $rs->getString($startcol + 9);

			$this->tempat = $rs->getString($startcol + 10);

			$this->komponen = $rs->getString($startcol + 11);

			$this->kecamatan = $rs->getString($startcol + 12);

			$this->kelurahan = $rs->getString($startcol + 13);

			$this->lokasi = $rs->getString($startcol + 14);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 15; 
		} catch (Exception $e) {
			throw new PropelException("Error populating HistoryPekerjaanV2 object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(HistoryPekerjaanV2Peer::DATABASE_NAME);
		}

		try {
			$con->begin();
			HistoryPekerjaanV2Peer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(HistoryPekerjaanV2Peer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = HistoryPekerjaanV2Peer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += HistoryPekerjaanV2Peer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = HistoryPekerjaanV2Peer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = HistoryPekerjaanV2Peer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getIdHistory();
				break;
			case 1:
				return $this->getTahun();
				break;
			case 2:
				return $this->getKodeRka();
				break;
			case 3:
				return $this->getStatusHapus();
				break;
			case 4:
				return $this->getJalan();
				break;
			case 5:
				return $this->getGang();
				break;
			case 6:
				return $this->getNomor();
				break;
			case 7:
				return $this->getRw();
				break;
			case 8:
				return $this->getRt();
				break;
			case 9:
				return $this->getKeterangan();
				break;
			case 10:
				return $this->getTempat();
				break;
			case 11:
				return $this->getKomponen();
				break;
			case 12:
				return $this->getKecamatan();
				break;
			case 13:
				return $this->getKelurahan();
				break;
			case 14:
				return $this->getLokasi();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = HistoryPekerjaanV2Peer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getIdHistory(),
			$keys[1] => $this->getTahun(),
			$keys[2] => $this->getKodeRka(),
			$keys[3] => $this->getStatusHapus(),
			$keys[4] => $this->getJalan(),
			$keys[5] => $this->getGang(),
			$keys[6] => $this->getNomor(),
			$keys[7] => $this->getRw(),
			$keys[8] => $this->getRt(),
			$keys[9] => $this->getKeterangan(),
			$keys[10] => $this->getTempat(),
			$keys[11] => $this->getKomponen(),
			$keys[12] => $this->getKecamatan(),
			$keys[13] => $this->getKelurahan(),
			$keys[14] => $this->getLokasi(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = HistoryPekerjaanV2Peer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setIdHistory($value);
				break;
			case 1:
				$this->setTahun($value);
				break;
			case 2:
				$this->setKodeRka($value);
				break;
			case 3:
				$this->setStatusHapus($value);
				break;
			case 4:
				$this->setJalan($value);
				break;
			case 5:
				$this->setGang($value);
				break;
			case 6:
				$this->setNomor($value);
				break;
			case 7:
				$this->setRw($value);
				break;
			case 8:
				$this->setRt($value);
				break;
			case 9:
				$this->setKeterangan($value);
				break;
			case 10:
				$this->setTempat($value);
				break;
			case 11:
				$this->setKomponen($value);
				break;
			case 12:
				$this->setKecamatan($value);
				break;
			case 13:
				$this->setKelurahan($value);
				break;
			case 14:
				$this->setLokasi($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = HistoryPekerjaanV2Peer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setIdHistory($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setTahun($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setKodeRka($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setStatusHapus($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setJalan($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setGang($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setNomor($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setRw($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setRt($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setKeterangan($arr[$keys[9]]);
		if (array_key_exists($keys[10], $arr)) $this->setTempat($arr[$keys[10]]);
		if (array_key_exists($keys[11], $arr)) $this->setKomponen($arr[$keys[11]]);
		if (array_key_exists($keys[12], $arr)) $this->setKecamatan($arr[$keys[12]]);
		if (array_key_exists($keys[13], $arr)) $this->setKelurahan($arr[$keys[13]]);
		if (array_key_exists($keys[14], $arr)) $this->setLokasi($arr[$keys[14]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(HistoryPekerjaanV2Peer::DATABASE_NAME);

		if ($this->isColumnModified(HistoryPekerjaanV2Peer::ID_HISTORY)) $criteria->add(HistoryPekerjaanV2Peer::ID_HISTORY, $this->id_history);
		if ($this->isColumnModified(HistoryPekerjaanV2Peer::TAHUN)) $criteria->add(HistoryPekerjaanV2Peer::TAHUN, $this->tahun);
		if ($this->isColumnModified(HistoryPekerjaanV2Peer::KODE_RKA)) $criteria->add(HistoryPekerjaanV2Peer::KODE_RKA, $this->kode_rka);
		if ($this->isColumnModified(HistoryPekerjaanV2Peer::STATUS_HAPUS)) $criteria->add(HistoryPekerjaanV2Peer::STATUS_HAPUS, $this->status_hapus);
		if ($this->isColumnModified(HistoryPekerjaanV2Peer::JALAN)) $criteria->add(HistoryPekerjaanV2Peer::JALAN, $this->jalan);
		if ($this->isColumnModified(HistoryPekerjaanV2Peer::GANG)) $criteria->add(HistoryPekerjaanV2Peer::GANG, $this->gang);
		if ($this->isColumnModified(HistoryPekerjaanV2Peer::NOMOR)) $criteria->add(HistoryPekerjaanV2Peer::NOMOR, $this->nomor);
		if ($this->isColumnModified(HistoryPekerjaanV2Peer::RW)) $criteria->add(HistoryPekerjaanV2Peer::RW, $this->rw);
		if ($this->isColumnModified(HistoryPekerjaanV2Peer::RT)) $criteria->add(HistoryPekerjaanV2Peer::RT, $this->rt);
		if ($this->isColumnModified(HistoryPekerjaanV2Peer::KETERANGAN)) $criteria->add(HistoryPekerjaanV2Peer::KETERANGAN, $this->keterangan);
		if ($this->isColumnModified(HistoryPekerjaanV2Peer::TEMPAT)) $criteria->add(HistoryPekerjaanV2Peer::TEMPAT, $this->tempat);
		if ($this->isColumnModified(HistoryPekerjaanV2Peer::KOMPONEN)) $criteria->add(HistoryPekerjaanV2Peer::KOMPONEN, $this->komponen);
		if ($this->isColumnModified(HistoryPekerjaanV2Peer::KECAMATAN)) $criteria->add(HistoryPekerjaanV2Peer::KECAMATAN, $this->kecamatan);
		if ($this->isColumnModified(HistoryPekerjaanV2Peer::KELURAHAN)) $criteria->add(HistoryPekerjaanV2Peer::KELURAHAN, $this->kelurahan);
		if ($this->isColumnModified(HistoryPekerjaanV2Peer::LOKASI)) $criteria->add(HistoryPekerjaanV2Peer::LOKASI, $this->lokasi);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(HistoryPekerjaanV2Peer::DATABASE_NAME);

		$criteria->add(HistoryPekerjaanV2Peer::ID_HISTORY, $this->id_history);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getIdHistory();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setIdHistory($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setTahun($this->tahun);

		$copyObj->setKodeRka($this->kode_rka);

		$copyObj->setStatusHapus($this->status_hapus);

		$copyObj->setJalan($this->jalan);

		$copyObj->setGang($this->gang);

		$copyObj->setNomor($this->nomor);

		$copyObj->setRw($this->rw);

		$copyObj->setRt($this->rt);

		$copyObj->setKeterangan($this->keterangan);

		$copyObj->setTempat($this->tempat);

		$copyObj->setKomponen($this->komponen);

		$copyObj->setKecamatan($this->kecamatan);

		$copyObj->setKelurahan($this->kelurahan);

		$copyObj->setLokasi($this->lokasi);


		$copyObj->setNew(true);

		$copyObj->setIdHistory(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new HistoryPekerjaanV2Peer();
		}
		return self::$peer;
	}

} 