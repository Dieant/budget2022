<?php


abstract class BaseBaruHistoryPekerjaan extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id_history;


	
	protected $tahun;


	
	protected $kode_budgeting;


	
	protected $komponen_name;


	
	protected $detail_name;


	
	protected $satuan;


	
	protected $volume;


	
	protected $nilai_anggaran;


	
	protected $unit_id;


	
	protected $kegiatan_code;


	
	protected $detail_no;


	
	protected $schema_name;


	
	protected $database_name;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getIdHistory()
	{

		return $this->id_history;
	}

	
	public function getTahun()
	{

		return $this->tahun;
	}

	
	public function getKodeBudgeting()
	{

		return $this->kode_budgeting;
	}

	
	public function getKomponenName()
	{

		return $this->komponen_name;
	}

	
	public function getDetailName()
	{

		return $this->detail_name;
	}

	
	public function getSatuan()
	{

		return $this->satuan;
	}

	
	public function getVolume()
	{

		return $this->volume;
	}

	
	public function getNilaiAnggaran()
	{

		return $this->nilai_anggaran;
	}

	
	public function getUnitId()
	{

		return $this->unit_id;
	}

	
	public function getKegiatanCode()
	{

		return $this->kegiatan_code;
	}

	
	public function getDetailNo()
	{

		return $this->detail_no;
	}

	
	public function getSchemaName()
	{

		return $this->schema_name;
	}

	
	public function getDatabaseName()
	{

		return $this->database_name;
	}

	
	public function setIdHistory($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id_history !== $v) {
			$this->id_history = $v;
			$this->modifiedColumns[] = BaruHistoryPekerjaanPeer::ID_HISTORY;
		}

	} 
	
	public function setTahun($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->tahun !== $v) {
			$this->tahun = $v;
			$this->modifiedColumns[] = BaruHistoryPekerjaanPeer::TAHUN;
		}

	} 
	
	public function setKodeBudgeting($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_budgeting !== $v) {
			$this->kode_budgeting = $v;
			$this->modifiedColumns[] = BaruHistoryPekerjaanPeer::KODE_BUDGETING;
		}

	} 
	
	public function setKomponenName($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->komponen_name !== $v) {
			$this->komponen_name = $v;
			$this->modifiedColumns[] = BaruHistoryPekerjaanPeer::KOMPONEN_NAME;
		}

	} 
	
	public function setDetailName($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->detail_name !== $v) {
			$this->detail_name = $v;
			$this->modifiedColumns[] = BaruHistoryPekerjaanPeer::DETAIL_NAME;
		}

	} 
	
	public function setSatuan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->satuan !== $v) {
			$this->satuan = $v;
			$this->modifiedColumns[] = BaruHistoryPekerjaanPeer::SATUAN;
		}

	} 
	
	public function setVolume($v)
	{

		if ($this->volume !== $v) {
			$this->volume = $v;
			$this->modifiedColumns[] = BaruHistoryPekerjaanPeer::VOLUME;
		}

	} 
	
	public function setNilaiAnggaran($v)
	{

		if ($this->nilai_anggaran !== $v) {
			$this->nilai_anggaran = $v;
			$this->modifiedColumns[] = BaruHistoryPekerjaanPeer::NILAI_ANGGARAN;
		}

	} 
	
	public function setUnitId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->unit_id !== $v) {
			$this->unit_id = $v;
			$this->modifiedColumns[] = BaruHistoryPekerjaanPeer::UNIT_ID;
		}

	} 
	
	public function setKegiatanCode($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kegiatan_code !== $v) {
			$this->kegiatan_code = $v;
			$this->modifiedColumns[] = BaruHistoryPekerjaanPeer::KEGIATAN_CODE;
		}

	} 
	
	public function setDetailNo($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->detail_no !== $v) {
			$this->detail_no = $v;
			$this->modifiedColumns[] = BaruHistoryPekerjaanPeer::DETAIL_NO;
		}

	} 
	
	public function setSchemaName($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->schema_name !== $v) {
			$this->schema_name = $v;
			$this->modifiedColumns[] = BaruHistoryPekerjaanPeer::SCHEMA_NAME;
		}

	} 
	
	public function setDatabaseName($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->database_name !== $v) {
			$this->database_name = $v;
			$this->modifiedColumns[] = BaruHistoryPekerjaanPeer::DATABASE_NAME;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id_history = $rs->getInt($startcol + 0);

			$this->tahun = $rs->getString($startcol + 1);

			$this->kode_budgeting = $rs->getString($startcol + 2);

			$this->komponen_name = $rs->getString($startcol + 3);

			$this->detail_name = $rs->getString($startcol + 4);

			$this->satuan = $rs->getString($startcol + 5);

			$this->volume = $rs->getFloat($startcol + 6);

			$this->nilai_anggaran = $rs->getFloat($startcol + 7);

			$this->unit_id = $rs->getString($startcol + 8);

			$this->kegiatan_code = $rs->getString($startcol + 9);

			$this->detail_no = $rs->getString($startcol + 10);

			$this->schema_name = $rs->getString($startcol + 11);

			$this->database_name = $rs->getString($startcol + 12);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 13; 
		} catch (Exception $e) {
			throw new PropelException("Error populating BaruHistoryPekerjaan object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(BaruHistoryPekerjaanPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			BaruHistoryPekerjaanPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(BaruHistoryPekerjaanPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = BaruHistoryPekerjaanPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += BaruHistoryPekerjaanPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = BaruHistoryPekerjaanPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = BaruHistoryPekerjaanPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getIdHistory();
				break;
			case 1:
				return $this->getTahun();
				break;
			case 2:
				return $this->getKodeBudgeting();
				break;
			case 3:
				return $this->getKomponenName();
				break;
			case 4:
				return $this->getDetailName();
				break;
			case 5:
				return $this->getSatuan();
				break;
			case 6:
				return $this->getVolume();
				break;
			case 7:
				return $this->getNilaiAnggaran();
				break;
			case 8:
				return $this->getUnitId();
				break;
			case 9:
				return $this->getKegiatanCode();
				break;
			case 10:
				return $this->getDetailNo();
				break;
			case 11:
				return $this->getSchemaName();
				break;
			case 12:
				return $this->getDatabaseName();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = BaruHistoryPekerjaanPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getIdHistory(),
			$keys[1] => $this->getTahun(),
			$keys[2] => $this->getKodeBudgeting(),
			$keys[3] => $this->getKomponenName(),
			$keys[4] => $this->getDetailName(),
			$keys[5] => $this->getSatuan(),
			$keys[6] => $this->getVolume(),
			$keys[7] => $this->getNilaiAnggaran(),
			$keys[8] => $this->getUnitId(),
			$keys[9] => $this->getKegiatanCode(),
			$keys[10] => $this->getDetailNo(),
			$keys[11] => $this->getSchemaName(),
			$keys[12] => $this->getDatabaseName(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = BaruHistoryPekerjaanPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setIdHistory($value);
				break;
			case 1:
				$this->setTahun($value);
				break;
			case 2:
				$this->setKodeBudgeting($value);
				break;
			case 3:
				$this->setKomponenName($value);
				break;
			case 4:
				$this->setDetailName($value);
				break;
			case 5:
				$this->setSatuan($value);
				break;
			case 6:
				$this->setVolume($value);
				break;
			case 7:
				$this->setNilaiAnggaran($value);
				break;
			case 8:
				$this->setUnitId($value);
				break;
			case 9:
				$this->setKegiatanCode($value);
				break;
			case 10:
				$this->setDetailNo($value);
				break;
			case 11:
				$this->setSchemaName($value);
				break;
			case 12:
				$this->setDatabaseName($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = BaruHistoryPekerjaanPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setIdHistory($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setTahun($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setKodeBudgeting($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setKomponenName($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setDetailName($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setSatuan($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setVolume($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setNilaiAnggaran($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setUnitId($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setKegiatanCode($arr[$keys[9]]);
		if (array_key_exists($keys[10], $arr)) $this->setDetailNo($arr[$keys[10]]);
		if (array_key_exists($keys[11], $arr)) $this->setSchemaName($arr[$keys[11]]);
		if (array_key_exists($keys[12], $arr)) $this->setDatabaseName($arr[$keys[12]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(BaruHistoryPekerjaanPeer::DATABASE_NAME);

		if ($this->isColumnModified(BaruHistoryPekerjaanPeer::ID_HISTORY)) $criteria->add(BaruHistoryPekerjaanPeer::ID_HISTORY, $this->id_history);
		if ($this->isColumnModified(BaruHistoryPekerjaanPeer::TAHUN)) $criteria->add(BaruHistoryPekerjaanPeer::TAHUN, $this->tahun);
		if ($this->isColumnModified(BaruHistoryPekerjaanPeer::KODE_BUDGETING)) $criteria->add(BaruHistoryPekerjaanPeer::KODE_BUDGETING, $this->kode_budgeting);
		if ($this->isColumnModified(BaruHistoryPekerjaanPeer::KOMPONEN_NAME)) $criteria->add(BaruHistoryPekerjaanPeer::KOMPONEN_NAME, $this->komponen_name);
		if ($this->isColumnModified(BaruHistoryPekerjaanPeer::DETAIL_NAME)) $criteria->add(BaruHistoryPekerjaanPeer::DETAIL_NAME, $this->detail_name);
		if ($this->isColumnModified(BaruHistoryPekerjaanPeer::SATUAN)) $criteria->add(BaruHistoryPekerjaanPeer::SATUAN, $this->satuan);
		if ($this->isColumnModified(BaruHistoryPekerjaanPeer::VOLUME)) $criteria->add(BaruHistoryPekerjaanPeer::VOLUME, $this->volume);
		if ($this->isColumnModified(BaruHistoryPekerjaanPeer::NILAI_ANGGARAN)) $criteria->add(BaruHistoryPekerjaanPeer::NILAI_ANGGARAN, $this->nilai_anggaran);
		if ($this->isColumnModified(BaruHistoryPekerjaanPeer::UNIT_ID)) $criteria->add(BaruHistoryPekerjaanPeer::UNIT_ID, $this->unit_id);
		if ($this->isColumnModified(BaruHistoryPekerjaanPeer::KEGIATAN_CODE)) $criteria->add(BaruHistoryPekerjaanPeer::KEGIATAN_CODE, $this->kegiatan_code);
		if ($this->isColumnModified(BaruHistoryPekerjaanPeer::DETAIL_NO)) $criteria->add(BaruHistoryPekerjaanPeer::DETAIL_NO, $this->detail_no);
		if ($this->isColumnModified(BaruHistoryPekerjaanPeer::SCHEMA_NAME)) $criteria->add(BaruHistoryPekerjaanPeer::SCHEMA_NAME, $this->schema_name);
		if ($this->isColumnModified(BaruHistoryPekerjaanPeer::DATABASE_NAME)) $criteria->add(BaruHistoryPekerjaanPeer::DATABASE_NAME, $this->database_name);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(BaruHistoryPekerjaanPeer::DATABASE_NAME);


		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return null;
	}

	
	 public function setPrimaryKey($pk)
	 {
		 	 }

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setIdHistory($this->id_history);

		$copyObj->setTahun($this->tahun);

		$copyObj->setKodeBudgeting($this->kode_budgeting);

		$copyObj->setKomponenName($this->komponen_name);

		$copyObj->setDetailName($this->detail_name);

		$copyObj->setSatuan($this->satuan);

		$copyObj->setVolume($this->volume);

		$copyObj->setNilaiAnggaran($this->nilai_anggaran);

		$copyObj->setUnitId($this->unit_id);

		$copyObj->setKegiatanCode($this->kegiatan_code);

		$copyObj->setDetailNo($this->detail_no);

		$copyObj->setSchemaName($this->schema_name);

		$copyObj->setDatabaseName($this->database_name);


		$copyObj->setNew(true);

	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new BaruHistoryPekerjaanPeer();
		}
		return self::$peer;
	}

} 