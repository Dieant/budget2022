<?php


abstract class BaseHistoryPekerjaan3Peer {

	
	const DATABASE_NAME = 'gis';

	
	const TABLE_NAME = 'history_pekerjaan3';

	
	const CLASS_DEFAULT = 'lib.model.gis.HistoryPekerjaan3';

	
	const NUM_COLUMNS = 8;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const UNIT_ID = 'history_pekerjaan3.UNIT_ID';

	
	const KOMPONEN_NAME = 'history_pekerjaan3.KOMPONEN_NAME';

	
	const VOLUME = 'history_pekerjaan3.VOLUME';

	
	const SATUAN = 'history_pekerjaan3.SATUAN';

	
	const REALISASI = 'history_pekerjaan3.REALISASI';

	
	const KODE_DETAIL_KEGIATAN = 'history_pekerjaan3.KODE_DETAIL_KEGIATAN';

	
	const DETAIL_NAME = 'history_pekerjaan3.DETAIL_NAME';

	
	const TAHUN = 'history_pekerjaan3.TAHUN';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('UnitId', 'KomponenName', 'Volume', 'Satuan', 'Realisasi', 'KodeDetailKegiatan', 'DetailName', 'Tahun', ),
		BasePeer::TYPE_COLNAME => array (HistoryPekerjaan3Peer::UNIT_ID, HistoryPekerjaan3Peer::KOMPONEN_NAME, HistoryPekerjaan3Peer::VOLUME, HistoryPekerjaan3Peer::SATUAN, HistoryPekerjaan3Peer::REALISASI, HistoryPekerjaan3Peer::KODE_DETAIL_KEGIATAN, HistoryPekerjaan3Peer::DETAIL_NAME, HistoryPekerjaan3Peer::TAHUN, ),
		BasePeer::TYPE_FIELDNAME => array ('unit_id', 'komponen_name', 'volume', 'satuan', 'realisasi', 'kode_detail_kegiatan', 'detail_name', 'tahun', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('UnitId' => 0, 'KomponenName' => 1, 'Volume' => 2, 'Satuan' => 3, 'Realisasi' => 4, 'KodeDetailKegiatan' => 5, 'DetailName' => 6, 'Tahun' => 7, ),
		BasePeer::TYPE_COLNAME => array (HistoryPekerjaan3Peer::UNIT_ID => 0, HistoryPekerjaan3Peer::KOMPONEN_NAME => 1, HistoryPekerjaan3Peer::VOLUME => 2, HistoryPekerjaan3Peer::SATUAN => 3, HistoryPekerjaan3Peer::REALISASI => 4, HistoryPekerjaan3Peer::KODE_DETAIL_KEGIATAN => 5, HistoryPekerjaan3Peer::DETAIL_NAME => 6, HistoryPekerjaan3Peer::TAHUN => 7, ),
		BasePeer::TYPE_FIELDNAME => array ('unit_id' => 0, 'komponen_name' => 1, 'volume' => 2, 'satuan' => 3, 'realisasi' => 4, 'kode_detail_kegiatan' => 5, 'detail_name' => 6, 'tahun' => 7, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/gis/map/HistoryPekerjaan3MapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.gis.map.HistoryPekerjaan3MapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = HistoryPekerjaan3Peer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(HistoryPekerjaan3Peer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(HistoryPekerjaan3Peer::UNIT_ID);

		$criteria->addSelectColumn(HistoryPekerjaan3Peer::KOMPONEN_NAME);

		$criteria->addSelectColumn(HistoryPekerjaan3Peer::VOLUME);

		$criteria->addSelectColumn(HistoryPekerjaan3Peer::SATUAN);

		$criteria->addSelectColumn(HistoryPekerjaan3Peer::REALISASI);

		$criteria->addSelectColumn(HistoryPekerjaan3Peer::KODE_DETAIL_KEGIATAN);

		$criteria->addSelectColumn(HistoryPekerjaan3Peer::DETAIL_NAME);

		$criteria->addSelectColumn(HistoryPekerjaan3Peer::TAHUN);

	}

	const COUNT = 'COUNT(*)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT *)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(HistoryPekerjaan3Peer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(HistoryPekerjaan3Peer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = HistoryPekerjaan3Peer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = HistoryPekerjaan3Peer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return HistoryPekerjaan3Peer::populateObjects(HistoryPekerjaan3Peer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			HistoryPekerjaan3Peer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = HistoryPekerjaan3Peer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return HistoryPekerjaan3Peer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(HistoryPekerjaan3Peer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(HistoryPekerjaan3Peer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof HistoryPekerjaan3) {

			$criteria = $values->buildCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
												if(count($values) == count($values, COUNT_RECURSIVE))
			{
								$values = array($values);
			}
			$vals = array();
			foreach($values as $value)
			{

			}

		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(HistoryPekerjaan3 $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(HistoryPekerjaan3Peer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(HistoryPekerjaan3Peer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(HistoryPekerjaan3Peer::DATABASE_NAME, HistoryPekerjaan3Peer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = HistoryPekerjaan3Peer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

} 
if (Propel::isInit()) {
			try {
		BaseHistoryPekerjaan3Peer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/gis/map/HistoryPekerjaan3MapBuilder.php';
	Propel::registerMapBuilder('lib.model.gis.map.HistoryPekerjaan3MapBuilder');
}
