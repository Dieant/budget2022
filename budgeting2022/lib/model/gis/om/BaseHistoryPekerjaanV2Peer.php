<?php


abstract class BaseHistoryPekerjaanV2Peer {

	
	const DATABASE_NAME = 'gis';

	
	const TABLE_NAME = 'history_pekerjaan_v2';

	
	const CLASS_DEFAULT = 'lib.model.gis.HistoryPekerjaanV2';

	
	const NUM_COLUMNS = 15;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const ID_HISTORY = 'history_pekerjaan_v2.ID_HISTORY';

	
	const TAHUN = 'history_pekerjaan_v2.TAHUN';

	
	const KODE_RKA = 'history_pekerjaan_v2.KODE_RKA';

	
	const STATUS_HAPUS = 'history_pekerjaan_v2.STATUS_HAPUS';

	
	const JALAN = 'history_pekerjaan_v2.JALAN';

	
	const GANG = 'history_pekerjaan_v2.GANG';

	
	const NOMOR = 'history_pekerjaan_v2.NOMOR';

	
	const RW = 'history_pekerjaan_v2.RW';

	
	const RT = 'history_pekerjaan_v2.RT';

	
	const KETERANGAN = 'history_pekerjaan_v2.KETERANGAN';

	
	const TEMPAT = 'history_pekerjaan_v2.TEMPAT';

	
	const KOMPONEN = 'history_pekerjaan_v2.KOMPONEN';

	
	const KECAMATAN = 'history_pekerjaan_v2.KECAMATAN';

	
	const KELURAHAN = 'history_pekerjaan_v2.KELURAHAN';

	
	const LOKASI = 'history_pekerjaan_v2.LOKASI';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('IdHistory', 'Tahun', 'KodeRka', 'StatusHapus', 'Jalan', 'Gang', 'Nomor', 'Rw', 'Rt', 'Keterangan', 'Tempat', 'Komponen', 'Kecamatan', 'Kelurahan', 'Lokasi', ),
		BasePeer::TYPE_COLNAME => array (HistoryPekerjaanV2Peer::ID_HISTORY, HistoryPekerjaanV2Peer::TAHUN, HistoryPekerjaanV2Peer::KODE_RKA, HistoryPekerjaanV2Peer::STATUS_HAPUS, HistoryPekerjaanV2Peer::JALAN, HistoryPekerjaanV2Peer::GANG, HistoryPekerjaanV2Peer::NOMOR, HistoryPekerjaanV2Peer::RW, HistoryPekerjaanV2Peer::RT, HistoryPekerjaanV2Peer::KETERANGAN, HistoryPekerjaanV2Peer::TEMPAT, HistoryPekerjaanV2Peer::KOMPONEN, HistoryPekerjaanV2Peer::KECAMATAN, HistoryPekerjaanV2Peer::KELURAHAN, HistoryPekerjaanV2Peer::LOKASI, ),
		BasePeer::TYPE_FIELDNAME => array ('id_history', 'tahun', 'kode_rka', 'status_hapus', 'jalan', 'gang', 'nomor', 'rw', 'rt', 'keterangan', 'tempat', 'komponen', 'kecamatan', 'kelurahan', 'lokasi', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('IdHistory' => 0, 'Tahun' => 1, 'KodeRka' => 2, 'StatusHapus' => 3, 'Jalan' => 4, 'Gang' => 5, 'Nomor' => 6, 'Rw' => 7, 'Rt' => 8, 'Keterangan' => 9, 'Tempat' => 10, 'Komponen' => 11, 'Kecamatan' => 12, 'Kelurahan' => 13, 'Lokasi' => 14, ),
		BasePeer::TYPE_COLNAME => array (HistoryPekerjaanV2Peer::ID_HISTORY => 0, HistoryPekerjaanV2Peer::TAHUN => 1, HistoryPekerjaanV2Peer::KODE_RKA => 2, HistoryPekerjaanV2Peer::STATUS_HAPUS => 3, HistoryPekerjaanV2Peer::JALAN => 4, HistoryPekerjaanV2Peer::GANG => 5, HistoryPekerjaanV2Peer::NOMOR => 6, HistoryPekerjaanV2Peer::RW => 7, HistoryPekerjaanV2Peer::RT => 8, HistoryPekerjaanV2Peer::KETERANGAN => 9, HistoryPekerjaanV2Peer::TEMPAT => 10, HistoryPekerjaanV2Peer::KOMPONEN => 11, HistoryPekerjaanV2Peer::KECAMATAN => 12, HistoryPekerjaanV2Peer::KELURAHAN => 13, HistoryPekerjaanV2Peer::LOKASI => 14, ),
		BasePeer::TYPE_FIELDNAME => array ('id_history' => 0, 'tahun' => 1, 'kode_rka' => 2, 'status_hapus' => 3, 'jalan' => 4, 'gang' => 5, 'nomor' => 6, 'rw' => 7, 'rt' => 8, 'keterangan' => 9, 'tempat' => 10, 'komponen' => 11, 'kecamatan' => 12, 'kelurahan' => 13, 'lokasi' => 14, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/gis/map/HistoryPekerjaanV2MapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.gis.map.HistoryPekerjaanV2MapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = HistoryPekerjaanV2Peer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(HistoryPekerjaanV2Peer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(HistoryPekerjaanV2Peer::ID_HISTORY);

		$criteria->addSelectColumn(HistoryPekerjaanV2Peer::TAHUN);

		$criteria->addSelectColumn(HistoryPekerjaanV2Peer::KODE_RKA);

		$criteria->addSelectColumn(HistoryPekerjaanV2Peer::STATUS_HAPUS);

		$criteria->addSelectColumn(HistoryPekerjaanV2Peer::JALAN);

		$criteria->addSelectColumn(HistoryPekerjaanV2Peer::GANG);

		$criteria->addSelectColumn(HistoryPekerjaanV2Peer::NOMOR);

		$criteria->addSelectColumn(HistoryPekerjaanV2Peer::RW);

		$criteria->addSelectColumn(HistoryPekerjaanV2Peer::RT);

		$criteria->addSelectColumn(HistoryPekerjaanV2Peer::KETERANGAN);

		$criteria->addSelectColumn(HistoryPekerjaanV2Peer::TEMPAT);

		$criteria->addSelectColumn(HistoryPekerjaanV2Peer::KOMPONEN);

		$criteria->addSelectColumn(HistoryPekerjaanV2Peer::KECAMATAN);

		$criteria->addSelectColumn(HistoryPekerjaanV2Peer::KELURAHAN);

		$criteria->addSelectColumn(HistoryPekerjaanV2Peer::LOKASI);

	}

	const COUNT = 'COUNT(history_pekerjaan_v2.ID_HISTORY)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT history_pekerjaan_v2.ID_HISTORY)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(HistoryPekerjaanV2Peer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(HistoryPekerjaanV2Peer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = HistoryPekerjaanV2Peer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = HistoryPekerjaanV2Peer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return HistoryPekerjaanV2Peer::populateObjects(HistoryPekerjaanV2Peer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			HistoryPekerjaanV2Peer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = HistoryPekerjaanV2Peer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return HistoryPekerjaanV2Peer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(HistoryPekerjaanV2Peer::ID_HISTORY);
			$selectCriteria->add(HistoryPekerjaanV2Peer::ID_HISTORY, $criteria->remove(HistoryPekerjaanV2Peer::ID_HISTORY), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(HistoryPekerjaanV2Peer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(HistoryPekerjaanV2Peer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof HistoryPekerjaanV2) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(HistoryPekerjaanV2Peer::ID_HISTORY, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(HistoryPekerjaanV2 $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(HistoryPekerjaanV2Peer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(HistoryPekerjaanV2Peer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(HistoryPekerjaanV2Peer::DATABASE_NAME, HistoryPekerjaanV2Peer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = HistoryPekerjaanV2Peer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(HistoryPekerjaanV2Peer::DATABASE_NAME);

		$criteria->add(HistoryPekerjaanV2Peer::ID_HISTORY, $pk);


		$v = HistoryPekerjaanV2Peer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(HistoryPekerjaanV2Peer::ID_HISTORY, $pks, Criteria::IN);
			$objs = HistoryPekerjaanV2Peer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BaseHistoryPekerjaanV2Peer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/gis/map/HistoryPekerjaanV2MapBuilder.php';
	Propel::registerMapBuilder('lib.model.gis.map.HistoryPekerjaanV2MapBuilder');
}
