<?php


abstract class BasemasterLokasiSimbada extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $kode_lokasi_simbada;


	
	protected $nama_lokasi_simbada;


	
	protected $kecamatan;


	
	protected $kelurahan;


	
	protected $catatan;


	
	protected $kode_jasmas;


	
	protected $alamat;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getKodeLokasiSimbada()
	{

		return $this->kode_lokasi_simbada;
	}

	
	public function getNamaLokasiSimbada()
	{

		return $this->nama_lokasi_simbada;
	}

	
	public function getKecamatan()
	{

		return $this->kecamatan;
	}

	
	public function getKelurahan()
	{

		return $this->kelurahan;
	}

	
	public function getCatatan()
	{

		return $this->catatan;
	}

	
	public function getKodeJasmas()
	{

		return $this->kode_jasmas;
	}

	
	public function getAlamat()
	{

		return $this->alamat;
	}

	
	public function setKodeLokasiSimbada($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_lokasi_simbada !== $v) {
			$this->kode_lokasi_simbada = $v;
			$this->modifiedColumns[] = masterLokasiSimbadaPeer::KODE_LOKASI_SIMBADA;
		}

	} 
	
	public function setNamaLokasiSimbada($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nama_lokasi_simbada !== $v) {
			$this->nama_lokasi_simbada = $v;
			$this->modifiedColumns[] = masterLokasiSimbadaPeer::NAMA_LOKASI_SIMBADA;
		}

	} 
	
	public function setKecamatan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kecamatan !== $v) {
			$this->kecamatan = $v;
			$this->modifiedColumns[] = masterLokasiSimbadaPeer::KECAMATAN;
		}

	} 
	
	public function setKelurahan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kelurahan !== $v) {
			$this->kelurahan = $v;
			$this->modifiedColumns[] = masterLokasiSimbadaPeer::KELURAHAN;
		}

	} 
	
	public function setCatatan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->catatan !== $v) {
			$this->catatan = $v;
			$this->modifiedColumns[] = masterLokasiSimbadaPeer::CATATAN;
		}

	} 
	
	public function setKodeJasmas($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_jasmas !== $v) {
			$this->kode_jasmas = $v;
			$this->modifiedColumns[] = masterLokasiSimbadaPeer::KODE_JASMAS;
		}

	} 
	
	public function setAlamat($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->alamat !== $v) {
			$this->alamat = $v;
			$this->modifiedColumns[] = masterLokasiSimbadaPeer::ALAMAT;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->kode_lokasi_simbada = $rs->getString($startcol + 0);

			$this->nama_lokasi_simbada = $rs->getString($startcol + 1);

			$this->kecamatan = $rs->getString($startcol + 2);

			$this->kelurahan = $rs->getString($startcol + 3);

			$this->catatan = $rs->getString($startcol + 4);

			$this->kode_jasmas = $rs->getString($startcol + 5);

			$this->alamat = $rs->getString($startcol + 6);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 7; 
		} catch (Exception $e) {
			throw new PropelException("Error populating masterLokasiSimbada object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(masterLokasiSimbadaPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			masterLokasiSimbadaPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(masterLokasiSimbadaPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = masterLokasiSimbadaPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += masterLokasiSimbadaPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = masterLokasiSimbadaPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = masterLokasiSimbadaPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getKodeLokasiSimbada();
				break;
			case 1:
				return $this->getNamaLokasiSimbada();
				break;
			case 2:
				return $this->getKecamatan();
				break;
			case 3:
				return $this->getKelurahan();
				break;
			case 4:
				return $this->getCatatan();
				break;
			case 5:
				return $this->getKodeJasmas();
				break;
			case 6:
				return $this->getAlamat();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = masterLokasiSimbadaPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getKodeLokasiSimbada(),
			$keys[1] => $this->getNamaLokasiSimbada(),
			$keys[2] => $this->getKecamatan(),
			$keys[3] => $this->getKelurahan(),
			$keys[4] => $this->getCatatan(),
			$keys[5] => $this->getKodeJasmas(),
			$keys[6] => $this->getAlamat(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = masterLokasiSimbadaPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setKodeLokasiSimbada($value);
				break;
			case 1:
				$this->setNamaLokasiSimbada($value);
				break;
			case 2:
				$this->setKecamatan($value);
				break;
			case 3:
				$this->setKelurahan($value);
				break;
			case 4:
				$this->setCatatan($value);
				break;
			case 5:
				$this->setKodeJasmas($value);
				break;
			case 6:
				$this->setAlamat($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = masterLokasiSimbadaPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setKodeLokasiSimbada($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setNamaLokasiSimbada($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setKecamatan($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setKelurahan($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setCatatan($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setKodeJasmas($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setAlamat($arr[$keys[6]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(masterLokasiSimbadaPeer::DATABASE_NAME);

		if ($this->isColumnModified(masterLokasiSimbadaPeer::KODE_LOKASI_SIMBADA)) $criteria->add(masterLokasiSimbadaPeer::KODE_LOKASI_SIMBADA, $this->kode_lokasi_simbada);
		if ($this->isColumnModified(masterLokasiSimbadaPeer::NAMA_LOKASI_SIMBADA)) $criteria->add(masterLokasiSimbadaPeer::NAMA_LOKASI_SIMBADA, $this->nama_lokasi_simbada);
		if ($this->isColumnModified(masterLokasiSimbadaPeer::KECAMATAN)) $criteria->add(masterLokasiSimbadaPeer::KECAMATAN, $this->kecamatan);
		if ($this->isColumnModified(masterLokasiSimbadaPeer::KELURAHAN)) $criteria->add(masterLokasiSimbadaPeer::KELURAHAN, $this->kelurahan);
		if ($this->isColumnModified(masterLokasiSimbadaPeer::CATATAN)) $criteria->add(masterLokasiSimbadaPeer::CATATAN, $this->catatan);
		if ($this->isColumnModified(masterLokasiSimbadaPeer::KODE_JASMAS)) $criteria->add(masterLokasiSimbadaPeer::KODE_JASMAS, $this->kode_jasmas);
		if ($this->isColumnModified(masterLokasiSimbadaPeer::ALAMAT)) $criteria->add(masterLokasiSimbadaPeer::ALAMAT, $this->alamat);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(masterLokasiSimbadaPeer::DATABASE_NAME);

		$criteria->add(masterLokasiSimbadaPeer::KODE_LOKASI_SIMBADA, $this->kode_lokasi_simbada);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getKodeLokasiSimbada();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setKodeLokasiSimbada($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setNamaLokasiSimbada($this->nama_lokasi_simbada);

		$copyObj->setKecamatan($this->kecamatan);

		$copyObj->setKelurahan($this->kelurahan);

		$copyObj->setCatatan($this->catatan);

		$copyObj->setKodeJasmas($this->kode_jasmas);

		$copyObj->setAlamat($this->alamat);


		$copyObj->setNew(true);

		$copyObj->setKodeLokasiSimbada(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new masterLokasiSimbadaPeer();
		}
		return self::$peer;
	}

} 