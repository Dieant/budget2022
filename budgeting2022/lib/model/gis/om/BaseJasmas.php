<?php


abstract class BaseJasmas extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $kode_jasmas;


	
	protected $nama;


	
	protected $alamat;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getKodeJasmas()
	{

		return $this->kode_jasmas;
	}

	
	public function getNama()
	{

		return $this->nama;
	}

	
	public function getAlamat()
	{

		return $this->alamat;
	}

	
	public function setKodeJasmas($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_jasmas !== $v) {
			$this->kode_jasmas = $v;
			$this->modifiedColumns[] = JasmasPeer::KODE_JASMAS;
		}

	} 
	
	public function setNama($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nama !== $v) {
			$this->nama = $v;
			$this->modifiedColumns[] = JasmasPeer::NAMA;
		}

	} 
	
	public function setAlamat($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->alamat !== $v) {
			$this->alamat = $v;
			$this->modifiedColumns[] = JasmasPeer::ALAMAT;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->kode_jasmas = $rs->getString($startcol + 0);

			$this->nama = $rs->getString($startcol + 1);

			$this->alamat = $rs->getString($startcol + 2);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 3; 
		} catch (Exception $e) {
			throw new PropelException("Error populating Jasmas object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(JasmasPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			JasmasPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(JasmasPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = JasmasPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += JasmasPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = JasmasPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = JasmasPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getKodeJasmas();
				break;
			case 1:
				return $this->getNama();
				break;
			case 2:
				return $this->getAlamat();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = JasmasPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getKodeJasmas(),
			$keys[1] => $this->getNama(),
			$keys[2] => $this->getAlamat(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = JasmasPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setKodeJasmas($value);
				break;
			case 1:
				$this->setNama($value);
				break;
			case 2:
				$this->setAlamat($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = JasmasPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setKodeJasmas($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setNama($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setAlamat($arr[$keys[2]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(JasmasPeer::DATABASE_NAME);

		if ($this->isColumnModified(JasmasPeer::KODE_JASMAS)) $criteria->add(JasmasPeer::KODE_JASMAS, $this->kode_jasmas);
		if ($this->isColumnModified(JasmasPeer::NAMA)) $criteria->add(JasmasPeer::NAMA, $this->nama);
		if ($this->isColumnModified(JasmasPeer::ALAMAT)) $criteria->add(JasmasPeer::ALAMAT, $this->alamat);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(JasmasPeer::DATABASE_NAME);

		$criteria->add(JasmasPeer::KODE_JASMAS, $this->kode_jasmas);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getKodeJasmas();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setKodeJasmas($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setNama($this->nama);

		$copyObj->setAlamat($this->alamat);


		$copyObj->setNew(true);

		$copyObj->setKodeJasmas(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new JasmasPeer();
		}
		return self::$peer;
	}

} 