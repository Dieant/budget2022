<?php


abstract class BaseHistoryPekerjaan3 extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $unit_id;


	
	protected $komponen_name;


	
	protected $volume;


	
	protected $satuan;


	
	protected $realisasi;


	
	protected $kode_detail_kegiatan;


	
	protected $detail_name;


	
	protected $tahun;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getUnitId()
	{

		return $this->unit_id;
	}

	
	public function getKomponenName()
	{

		return $this->komponen_name;
	}

	
	public function getVolume()
	{

		return $this->volume;
	}

	
	public function getSatuan()
	{

		return $this->satuan;
	}

	
	public function getRealisasi()
	{

		return $this->realisasi;
	}

	
	public function getKodeDetailKegiatan()
	{

		return $this->kode_detail_kegiatan;
	}

	
	public function getDetailName()
	{

		return $this->detail_name;
	}

	
	public function getTahun()
	{

		return $this->tahun;
	}

	
	public function setUnitId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->unit_id !== $v) {
			$this->unit_id = $v;
			$this->modifiedColumns[] = HistoryPekerjaan3Peer::UNIT_ID;
		}

	} 
	
	public function setKomponenName($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->komponen_name !== $v) {
			$this->komponen_name = $v;
			$this->modifiedColumns[] = HistoryPekerjaan3Peer::KOMPONEN_NAME;
		}

	} 
	
	public function setVolume($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->volume !== $v) {
			$this->volume = $v;
			$this->modifiedColumns[] = HistoryPekerjaan3Peer::VOLUME;
		}

	} 
	
	public function setSatuan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->satuan !== $v) {
			$this->satuan = $v;
			$this->modifiedColumns[] = HistoryPekerjaan3Peer::SATUAN;
		}

	} 
	
	public function setRealisasi($v)
	{

		if ($this->realisasi !== $v) {
			$this->realisasi = $v;
			$this->modifiedColumns[] = HistoryPekerjaan3Peer::REALISASI;
		}

	} 
	
	public function setKodeDetailKegiatan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_detail_kegiatan !== $v) {
			$this->kode_detail_kegiatan = $v;
			$this->modifiedColumns[] = HistoryPekerjaan3Peer::KODE_DETAIL_KEGIATAN;
		}

	} 
	
	public function setDetailName($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->detail_name !== $v) {
			$this->detail_name = $v;
			$this->modifiedColumns[] = HistoryPekerjaan3Peer::DETAIL_NAME;
		}

	} 
	
	public function setTahun($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->tahun !== $v) {
			$this->tahun = $v;
			$this->modifiedColumns[] = HistoryPekerjaan3Peer::TAHUN;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->unit_id = $rs->getString($startcol + 0);

			$this->komponen_name = $rs->getString($startcol + 1);

			$this->volume = $rs->getString($startcol + 2);

			$this->satuan = $rs->getString($startcol + 3);

			$this->realisasi = $rs->getFloat($startcol + 4);

			$this->kode_detail_kegiatan = $rs->getString($startcol + 5);

			$this->detail_name = $rs->getString($startcol + 6);

			$this->tahun = $rs->getString($startcol + 7);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 8; 
		} catch (Exception $e) {
			throw new PropelException("Error populating HistoryPekerjaan3 object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(HistoryPekerjaan3Peer::DATABASE_NAME);
		}

		try {
			$con->begin();
			HistoryPekerjaan3Peer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(HistoryPekerjaan3Peer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = HistoryPekerjaan3Peer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += HistoryPekerjaan3Peer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = HistoryPekerjaan3Peer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = HistoryPekerjaan3Peer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getUnitId();
				break;
			case 1:
				return $this->getKomponenName();
				break;
			case 2:
				return $this->getVolume();
				break;
			case 3:
				return $this->getSatuan();
				break;
			case 4:
				return $this->getRealisasi();
				break;
			case 5:
				return $this->getKodeDetailKegiatan();
				break;
			case 6:
				return $this->getDetailName();
				break;
			case 7:
				return $this->getTahun();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = HistoryPekerjaan3Peer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getUnitId(),
			$keys[1] => $this->getKomponenName(),
			$keys[2] => $this->getVolume(),
			$keys[3] => $this->getSatuan(),
			$keys[4] => $this->getRealisasi(),
			$keys[5] => $this->getKodeDetailKegiatan(),
			$keys[6] => $this->getDetailName(),
			$keys[7] => $this->getTahun(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = HistoryPekerjaan3Peer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setUnitId($value);
				break;
			case 1:
				$this->setKomponenName($value);
				break;
			case 2:
				$this->setVolume($value);
				break;
			case 3:
				$this->setSatuan($value);
				break;
			case 4:
				$this->setRealisasi($value);
				break;
			case 5:
				$this->setKodeDetailKegiatan($value);
				break;
			case 6:
				$this->setDetailName($value);
				break;
			case 7:
				$this->setTahun($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = HistoryPekerjaan3Peer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setUnitId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setKomponenName($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setVolume($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setSatuan($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setRealisasi($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setKodeDetailKegiatan($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setDetailName($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setTahun($arr[$keys[7]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(HistoryPekerjaan3Peer::DATABASE_NAME);

		if ($this->isColumnModified(HistoryPekerjaan3Peer::UNIT_ID)) $criteria->add(HistoryPekerjaan3Peer::UNIT_ID, $this->unit_id);
		if ($this->isColumnModified(HistoryPekerjaan3Peer::KOMPONEN_NAME)) $criteria->add(HistoryPekerjaan3Peer::KOMPONEN_NAME, $this->komponen_name);
		if ($this->isColumnModified(HistoryPekerjaan3Peer::VOLUME)) $criteria->add(HistoryPekerjaan3Peer::VOLUME, $this->volume);
		if ($this->isColumnModified(HistoryPekerjaan3Peer::SATUAN)) $criteria->add(HistoryPekerjaan3Peer::SATUAN, $this->satuan);
		if ($this->isColumnModified(HistoryPekerjaan3Peer::REALISASI)) $criteria->add(HistoryPekerjaan3Peer::REALISASI, $this->realisasi);
		if ($this->isColumnModified(HistoryPekerjaan3Peer::KODE_DETAIL_KEGIATAN)) $criteria->add(HistoryPekerjaan3Peer::KODE_DETAIL_KEGIATAN, $this->kode_detail_kegiatan);
		if ($this->isColumnModified(HistoryPekerjaan3Peer::DETAIL_NAME)) $criteria->add(HistoryPekerjaan3Peer::DETAIL_NAME, $this->detail_name);
		if ($this->isColumnModified(HistoryPekerjaan3Peer::TAHUN)) $criteria->add(HistoryPekerjaan3Peer::TAHUN, $this->tahun);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(HistoryPekerjaan3Peer::DATABASE_NAME);


		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return null;
	}

	
	 public function setPrimaryKey($pk)
	 {
		 	 }

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setUnitId($this->unit_id);

		$copyObj->setKomponenName($this->komponen_name);

		$copyObj->setVolume($this->volume);

		$copyObj->setSatuan($this->satuan);

		$copyObj->setRealisasi($this->realisasi);

		$copyObj->setKodeDetailKegiatan($this->kode_detail_kegiatan);

		$copyObj->setDetailName($this->detail_name);

		$copyObj->setTahun($this->tahun);


		$copyObj->setNew(true);

	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new HistoryPekerjaan3Peer();
		}
		return self::$peer;
	}

} 