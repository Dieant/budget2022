<?php



class VLokasiMapBuilder {

	
	const CLASS_NAME = 'lib.model.gis.map.VLokasiMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('gis');

		$tMap = $this->dbMap->addTable('v_lokasi');
		$tMap->setPhpName('VLokasi');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('KODE', 'Kode', 'string', CreoleTypes::VARCHAR, true, 16);

		$tMap->addColumn('NAMA', 'Nama', 'string', CreoleTypes::VARCHAR, false, 300);

	} 
} 