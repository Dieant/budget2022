<?php



class JasmasMapBuilder {

	
	const CLASS_NAME = 'lib.model.gis.map.JasmasMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('gis');

		$tMap = $this->dbMap->addTable('jasmas');
		$tMap->setPhpName('Jasmas');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('KODE_JASMAS', 'KodeJasmas', 'string', CreoleTypes::VARCHAR, true, 10);

		$tMap->addColumn('NAMA', 'Nama', 'string', CreoleTypes::VARCHAR, false, 200);

		$tMap->addColumn('ALAMAT', 'Alamat', 'string', CreoleTypes::VARCHAR, false, 500);

	} 
} 