<?php



class HistoryPekerjaan3MapBuilder {

	
	const CLASS_NAME = 'lib.model.gis.map.HistoryPekerjaan3MapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('gis');

		$tMap = $this->dbMap->addTable('history_pekerjaan3');
		$tMap->setPhpName('HistoryPekerjaan3');

		$tMap->setUseIdGenerator(false);

		$tMap->addColumn('UNIT_ID', 'UnitId', 'string', CreoleTypes::VARCHAR, false, 20);

		$tMap->addColumn('KOMPONEN_NAME', 'KomponenName', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('VOLUME', 'Volume', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('SATUAN', 'Satuan', 'string', CreoleTypes::VARCHAR, false, 50);

		$tMap->addColumn('REALISASI', 'Realisasi', 'double', CreoleTypes::DOUBLE, false, null);

		$tMap->addColumn('KODE_DETAIL_KEGIATAN', 'KodeDetailKegiatan', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('DETAIL_NAME', 'DetailName', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('TAHUN', 'Tahun', 'string', CreoleTypes::VARCHAR, false, 10);

	} 
} 