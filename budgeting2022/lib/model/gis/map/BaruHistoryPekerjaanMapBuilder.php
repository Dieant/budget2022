<?php



class BaruHistoryPekerjaanMapBuilder {

	
	const CLASS_NAME = 'lib.model.gis.map.BaruHistoryPekerjaanMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('gis');

		$tMap = $this->dbMap->addTable('baru_history_pekerjaan');
		$tMap->setPhpName('BaruHistoryPekerjaan');

		$tMap->setUseIdGenerator(false);

		$tMap->addColumn('ID_HISTORY', 'IdHistory', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('TAHUN', 'Tahun', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('KODE_BUDGETING', 'KodeBudgeting', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('KOMPONEN_NAME', 'KomponenName', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('DETAIL_NAME', 'DetailName', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('SATUAN', 'Satuan', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('VOLUME', 'Volume', 'double', CreoleTypes::DOUBLE, false, null);

		$tMap->addColumn('NILAI_ANGGARAN', 'NilaiAnggaran', 'double', CreoleTypes::DOUBLE, false, null);

		$tMap->addColumn('UNIT_ID', 'UnitId', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('KEGIATAN_CODE', 'KegiatanCode', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('DETAIL_NO', 'DetailNo', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('SCHEMA_NAME', 'SchemaName', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('DATABASE_NAME', 'DatabaseName', 'string', CreoleTypes::LONGVARCHAR, false, null);

	} 
} 