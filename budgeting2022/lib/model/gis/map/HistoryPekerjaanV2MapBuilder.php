<?php



class HistoryPekerjaanV2MapBuilder {

	
	const CLASS_NAME = 'lib.model.gis.map.HistoryPekerjaanV2MapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('gis');

		$tMap = $this->dbMap->addTable('history_pekerjaan_v2');
		$tMap->setPhpName('HistoryPekerjaanV2');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('ID_HISTORY', 'IdHistory', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('TAHUN', 'Tahun', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('KODE_RKA', 'KodeRka', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('STATUS_HAPUS', 'StatusHapus', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('JALAN', 'Jalan', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('GANG', 'Gang', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('NOMOR', 'Nomor', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('RW', 'Rw', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('RT', 'Rt', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('KETERANGAN', 'Keterangan', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('TEMPAT', 'Tempat', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('KOMPONEN', 'Komponen', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('KECAMATAN', 'Kecamatan', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('KELURAHAN', 'Kelurahan', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('LOKASI', 'Lokasi', 'string', CreoleTypes::LONGVARCHAR, false, null);

	} 
} 