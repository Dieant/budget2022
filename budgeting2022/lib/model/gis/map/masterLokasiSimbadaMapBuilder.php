<?php



class masterLokasiSimbadaMapBuilder {

	
	const CLASS_NAME = 'lib.model.gis.map.masterLokasiSimbadaMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('gis');

		$tMap = $this->dbMap->addTable('master_lokasi_simbada');
		$tMap->setPhpName('masterLokasiSimbada');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('KODE_LOKASI_SIMBADA', 'KodeLokasiSimbada', 'string', CreoleTypes::VARCHAR, true, 16);

		$tMap->addColumn('NAMA_LOKASI_SIMBADA', 'NamaLokasiSimbada', 'string', CreoleTypes::VARCHAR, false, 200);

		$tMap->addColumn('KECAMATAN', 'Kecamatan', 'string', CreoleTypes::VARCHAR, false, 50);

		$tMap->addColumn('KELURAHAN', 'Kelurahan', 'string', CreoleTypes::VARCHAR, false, 50);

		$tMap->addColumn('CATATAN', 'Catatan', 'string', CreoleTypes::VARCHAR, false, 500);

		$tMap->addColumn('KODE_JASMAS', 'KodeJasmas', 'string', CreoleTypes::VARCHAR, false, 10);

		$tMap->addColumn('ALAMAT', 'Alamat', 'string', CreoleTypes::VARCHAR, false, 500);

	} 
} 