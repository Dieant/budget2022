<?php



class PekerjaanMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.PekerjaanMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('pekerjaan');
		$tMap->setPhpName('Pekerjaan');

		$tMap->setUseIdGenerator(false);

		$tMap->addColumn('UNIT_ID', 'UnitId', 'string', CreoleTypes::VARCHAR, false, 16);

		$tMap->addColumn('KEGIATAN_CODE', 'KegiatanCode', 'string', CreoleTypes::VARCHAR, false, 16);

		$tMap->addColumn('KOMPONEN_ID', 'KomponenId', 'string', CreoleTypes::VARCHAR, false, 30);

		$tMap->addColumn('UNIT_NAME', 'UnitName', 'string', CreoleTypes::VARCHAR, false, 200);

		$tMap->addColumn('KEGIATAN_NAME', 'KegiatanName', 'string', CreoleTypes::VARCHAR, false, 300);

		$tMap->addColumn('SUBTITLE', 'Subtitle', 'string', CreoleTypes::VARCHAR, false, 300);

		$tMap->addColumn('KOMPONEN_NAME', 'KomponenName', 'string', CreoleTypes::VARCHAR, false, 400);

		$tMap->addColumn('DETAIL_NAME', 'DetailName', 'string', CreoleTypes::VARCHAR, false, 400);

		$tMap->addColumn('KETERANGAN_KOEFISIEN', 'KeteranganKoefisien', 'string', CreoleTypes::VARCHAR, false, 200);

		$tMap->addColumn('KECAMATAN', 'Kecamatan', 'string', CreoleTypes::VARCHAR, false, 500);

		$tMap->addColumn('KELURAHAN', 'Kelurahan', 'string', CreoleTypes::VARCHAR, false, 800);

	} 
} 