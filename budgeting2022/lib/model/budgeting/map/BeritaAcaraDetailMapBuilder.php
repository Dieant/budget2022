<?php



class BeritaAcaraDetailMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.BeritaAcaraDetailMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.berita_acara_detail');
		$tMap->setPhpName('BeritaAcaraDetail');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('ID_BERITA_ACARA', 'IdBeritaAcara', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addPrimaryKey('DETAIL_NO', 'DetailNo', 'int', CreoleTypes::SMALLINT, true, null);

		$tMap->addColumn('BELANJA', 'Belanja', 'string', CreoleTypes::VARCHAR, false, 50);

		$tMap->addColumn('REKENING_CODE', 'RekeningCode', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('REKENING_CODE_SEMULA', 'RekeningCodeSemula', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('SUBTITLE', 'Subtitle', 'string', CreoleTypes::VARCHAR, false, 500);

		$tMap->addColumn('SEMULA_KOMPONEN', 'SemulaKomponen', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('MENJADI_KOMPONEN', 'MenjadiKomponen', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('SEMULA_SATUAN', 'SemulaSatuan', 'string', CreoleTypes::VARCHAR, false, 50);

		$tMap->addColumn('MENJADI_SATUAN', 'MenjadiSatuan', 'string', CreoleTypes::VARCHAR, false, 50);

		$tMap->addColumn('SEMULA_KOEFISIEN', 'SemulaKoefisien', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('MENJADI_KOEFISIEN', 'MenjadiKoefisien', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('SEMULA_HARGA', 'SemulaHarga', 'double', CreoleTypes::DOUBLE, false, null);

		$tMap->addColumn('MENJADI_HARGA', 'MenjadiHarga', 'double', CreoleTypes::DOUBLE, false, null);

		$tMap->addColumn('SEMULA_HASIL', 'SemulaHasil', 'double', CreoleTypes::NUMERIC, false, 20);

		$tMap->addColumn('MENJADI_HASIL', 'MenjadiHasil', 'double', CreoleTypes::NUMERIC, false, 20);

		$tMap->addColumn('SEMULA_PAJAK', 'SemulaPajak', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('MENJADI_PAJAK', 'MenjadiPajak', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('SEMULA_TOTAL', 'SemulaTotal', 'double', CreoleTypes::NUMERIC, false, 20);

		$tMap->addColumn('MENJADI_TOTAL', 'MenjadiTotal', 'double', CreoleTypes::NUMERIC, false, 20);

		$tMap->addColumn('CATATAN', 'Catatan', 'string', CreoleTypes::VARCHAR, false, 300);

	} 
} 