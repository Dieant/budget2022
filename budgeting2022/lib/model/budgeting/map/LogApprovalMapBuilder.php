<?php



class LogApprovalMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.LogApprovalMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.log_approval');
		$tMap->setPhpName('LogApproval');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('ID', 'Id', 'string', CreoleTypes::BIGINT, true, null);

		$tMap->addColumn('UNIT_ID', 'UnitId', 'string', CreoleTypes::VARCHAR, false, 8);

		$tMap->addColumn('KEGIATAN_CODE', 'KegiatanCode', 'string', CreoleTypes::VARCHAR, false, 20);

		$tMap->addColumn('USER_ID', 'UserId', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('WAKTU', 'Waktu', 'int', CreoleTypes::TIMESTAMP, false, null);

		$tMap->addColumn('KUMPULAN_DETAIL_NO', 'KumpulanDetailNo', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('SEBAGAI', 'Sebagai', 'string', CreoleTypes::VARCHAR, false, 50);

		$tMap->addColumn('TAHAP', 'Tahap', 'string', CreoleTypes::VARCHAR, false, 20);

		$tMap->addColumn('CATATAN', 'Catatan', 'string', CreoleTypes::VARCHAR, false, null);

	} 
} 