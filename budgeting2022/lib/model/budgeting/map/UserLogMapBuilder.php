<?php



class UserLogMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.UserLogMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.user_log');
		$tMap->setPhpName('UserLog');

		$tMap->setUseIdGenerator(true);

		$tMap->setPrimaryKeyMethodInfo('ebudget.user_log_SEQ');

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('WAKTU', 'Waktu', 'int', CreoleTypes::TIMESTAMP, false, null);

		$tMap->addColumn('NAMA_USER', 'NamaUser', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('DESKRIPSI', 'Deskripsi', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('IP_ADDRESS', 'IpAddress', 'string', CreoleTypes::VARCHAR, false, 50);

		$tMap->addColumn('NAMA_LENGKAP', 'NamaLengkap', 'string', CreoleTypes::VARCHAR, false, 100);

	} 
} 