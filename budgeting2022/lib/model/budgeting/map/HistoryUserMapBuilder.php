<?php



class HistoryUserMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.HistoryUserMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.history_user');
		$tMap->setPhpName('HistoryUser');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('USERNAME', 'Username', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('IP', 'Ip', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('TIME_ACT', 'TimeAct', 'int', CreoleTypes::TIMESTAMP, false, null);

		$tMap->addColumn('UNIT_ID', 'UnitId', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('KEGIATAN_CODE', 'KegiatanCode', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('DETAIL_NO', 'DetailNo', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('DESCRIPTION', 'Description', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('AKSI', 'Aksi', 'string', CreoleTypes::LONGVARCHAR, false, null);

	} 
} 