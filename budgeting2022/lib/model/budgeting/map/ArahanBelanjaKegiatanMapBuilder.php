<?php



class ArahanBelanjaKegiatanMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.ArahanBelanjaKegiatanMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.arahan_belanja_kegiatan');
		$tMap->setPhpName('ArahanBelanjaKegiatan');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addPrimaryKey('UNIT_ID', 'UnitId', 'string', CreoleTypes::VARCHAR, true, null);

		$tMap->addPrimaryKey('KEGIATAN_CODE', 'KegiatanCode', 'string', CreoleTypes::VARCHAR, true, 32);

		$tMap->addPrimaryKey('KODE_BELANJA', 'KodeBelanja', 'string', CreoleTypes::VARCHAR, true, 32);

		$tMap->addColumn('ANGGARAN', 'Anggaran', 'double', CreoleTypes::DOUBLE, false, null);

		$tMap->addColumn('STATUS', 'Status', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('KODE_DETAIL_KEGIATAN', 'KodeDetailKegiatan', 'string', CreoleTypes::VARCHAR, false, 32);

		$tMap->addColumn('CREATED_AT', 'CreatedAt', 'int', CreoleTypes::TIMESTAMP, false, null);

		$tMap->addColumn('UPDATED_AT', 'UpdatedAt', 'int', CreoleTypes::TIMESTAMP, false, null);

	} 
} 