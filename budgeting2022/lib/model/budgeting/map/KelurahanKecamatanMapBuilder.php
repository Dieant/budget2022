<?php



class KelurahanKecamatanMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.KelurahanKecamatanMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('kelurahan_kecamatan');
		$tMap->setPhpName('KelurahanKecamatan');

		$tMap->setUseIdGenerator(false);

		$tMap->addColumn('OID', 'Oid', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('NAMA_KECAMATAN', 'NamaKecamatan', 'string', CreoleTypes::VARCHAR, false, 50);

		$tMap->addColumn('NAMA_KELURAHAN', 'NamaKelurahan', 'string', CreoleTypes::VARCHAR, false, 50);

		$tMap->addColumn('ID_KECAMATAN', 'IdKecamatan', 'int', CreoleTypes::INTEGER, false, null);

	} 
} 