<?php



class BappekoShsdMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.BappekoShsdMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.bappeko_shsd');
		$tMap->setPhpName('BappekoShsd');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('SHSD_ID', 'ShsdId', 'string', CreoleTypes::VARCHAR, true, 50);

		$tMap->addColumn('SATUAN', 'Satuan', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('SHSD_NAME', 'ShsdName', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('SHSD_HARGA', 'ShsdHarga', 'double', CreoleTypes::DOUBLE, false, null);

		$tMap->addColumn('SHSD_HARGA_DASAR', 'ShsdHargaDasar', 'double', CreoleTypes::DOUBLE, false, null);

		$tMap->addColumn('SHSD_SHOW', 'ShsdShow', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('SHSD_FAKTOR_INFLASI', 'ShsdFaktorInflasi', 'double', CreoleTypes::NUMERIC, false, 6);

		$tMap->addColumn('SHSD_LOCKED', 'ShsdLocked', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('REKENING_CODE', 'RekeningCode', 'string', CreoleTypes::VARCHAR, false, 300);

		$tMap->addColumn('SHSD_MERK', 'ShsdMerk', 'string', CreoleTypes::VARCHAR, false, 250);

		$tMap->addColumn('NON_PAJAK', 'NonPajak', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('SHSD_ID_OLD', 'ShsdIdOld', 'string', CreoleTypes::VARCHAR, false, 30);

		$tMap->addColumn('NAMA_DASAR', 'NamaDasar', 'string', CreoleTypes::VARCHAR, false, 300);

		$tMap->addColumn('SPEC', 'Spec', 'string', CreoleTypes::VARCHAR, false, 500);

		$tMap->addColumn('HIDDEN_SPEC', 'HiddenSpec', 'string', CreoleTypes::VARCHAR, false, 500);

	} 
} 