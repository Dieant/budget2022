<?php



class PegawaiMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.PegawaiMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('pegawai');
		$tMap->setPhpName('Pegawai');

		$tMap->setUseIdGenerator(false);

		$tMap->addColumn('NIP', 'Nip', 'string', CreoleTypes::VARCHAR, false, 25);

		$tMap->addColumn('NIP_LAMA', 'NipLama', 'string', CreoleTypes::VARCHAR, false, 25);

		$tMap->addColumn('NAMA', 'Nama', 'string', CreoleTypes::VARCHAR, false, 70);

		$tMap->addColumn('TEMPAT_LAHIR', 'TempatLahir', 'string', CreoleTypes::VARCHAR, false, 30);

		$tMap->addColumn('TANGGAL_LAHIR', 'TanggalLahir', 'string', CreoleTypes::VARCHAR, false, 15);

		$tMap->addColumn('NAMA_PANGKAT', 'NamaPangkat', 'string', CreoleTypes::VARCHAR, false, 25);

		$tMap->addColumn('NAMA_GOLONGAN', 'NamaGolongan', 'string', CreoleTypes::VARCHAR, false, 6);

		$tMap->addColumn('TMT_PANGKAT', 'TmtPangkat', 'string', CreoleTypes::VARCHAR, false, 15);

		$tMap->addColumn('TMT_CPNS', 'TmtCpns', 'string', CreoleTypes::VARCHAR, false, 15);

		$tMap->addColumn('JENIS_KELAMIN', 'JenisKelamin', 'string', CreoleTypes::VARCHAR, false, 10);

		$tMap->addColumn('NAMA_AGAMA', 'NamaAgama', 'string', CreoleTypes::VARCHAR, false, 12);

		$tMap->addColumn('NAMA_JABATAN', 'NamaJabatan', 'string', CreoleTypes::VARCHAR, false, 150);

		$tMap->addColumn('ESELON', 'Eselon', 'string', CreoleTypes::VARCHAR, false, 8);

		$tMap->addColumn('TMT_JABATAN', 'TmtJabatan', 'string', CreoleTypes::VARCHAR, false, 15);

		$tMap->addColumn('NAMA_JURUSAN_SK', 'NamaJurusanSk', 'string', CreoleTypes::VARCHAR, false, 50);

		$tMap->addColumn('LULUS', 'Lulus', 'string', CreoleTypes::VARCHAR, false, 5);

		$tMap->addColumn('JENIS_PEGAWAI', 'JenisPegawai', 'string', CreoleTypes::VARCHAR, false, 14);

		$tMap->addColumn('NAMA_INSTANSI', 'NamaInstansi', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('NAMA_UNIT_KERJA', 'NamaUnitKerja', 'string', CreoleTypes::VARCHAR, false, 150);

		$tMap->addColumn('NAMA_UNIT_KERJA2', 'NamaUnitKerja2', 'string', CreoleTypes::VARCHAR, false, 150);

		$tMap->addColumn('UNIT_ID', 'UnitId', 'string', CreoleTypes::VARCHAR, false, 10);

	} 
} 