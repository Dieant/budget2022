<?php



class KonektorMusrenbangMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.KonektorMusrenbangMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.konektor_musrenbang');
		$tMap->setPhpName('KonektorMusrenbang');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('KODE_DEVPLAN', 'KodeDevplan', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('NAMA_DEVPLAN', 'NamaDevplan', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('SATUAN_DEVPLAN', 'SatuanDevplan', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('KOMPONEN_ID', 'KomponenId', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('KOMPONEN_REKENING', 'KomponenRekening', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('HARGA_DEVPLAN', 'HargaDevplan', 'double', CreoleTypes::DOUBLE, false, null);

	} 
} 