<?php



class DeskripsiResumeRapatKegiatanQRMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.DeskripsiResumeRapatKegiatanQRMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.deskripsi_resume_rapat_qr');
		$tMap->setPhpName('DeskripsiResumeRapatKegiatanQR');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('UNIT_ID', 'UnitId', 'string', CreoleTypes::VARCHAR, true, 10);

		$tMap->addColumn('TAHAP', 'Tahap', 'int', CreoleTypes::SMALLINT, true, null);

		$tMap->addColumn('TOKEN', 'Token', 'string', CreoleTypes::VARCHAR, false, 500);

	} 
} 