<?php



class KategoriHspkMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.KategoriHspkMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.kategori_hspk');
		$tMap->setPhpName('KategoriHspk');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('KATEGORI_HSPK_ID', 'KategoriHspkId', 'string', CreoleTypes::VARCHAR, true, 50);

		$tMap->addColumn('KATEGORI_HSPK_NAME', 'KategoriHspkName', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('REKENING_CODE', 'RekeningCode', 'string', CreoleTypes::VARCHAR, false, 50);

	} 
} 