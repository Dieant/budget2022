<?php



class SubKegiatanMemberSalahMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.SubKegiatanMemberSalahMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.sub_kegiatan_member_salah');
		$tMap->setPhpName('SubKegiatanMemberSalah');

		$tMap->setUseIdGenerator(false);

		$tMap->addColumn('SUB_KEGIATAN_ID', 'SubKegiatanId', 'string', CreoleTypes::VARCHAR, true, 50);

		$tMap->addColumn('DETAIL_NO', 'DetailNo', 'int', CreoleTypes::SMALLINT, true, null);

		$tMap->addColumn('KOMPONEN_ID', 'KomponenId', 'string', CreoleTypes::VARCHAR, true, 50);

		$tMap->addColumn('REKENING_CODE', 'RekeningCode', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('DETAIL_NAME', 'DetailName', 'string', CreoleTypes::VARCHAR, false, 150);

		$tMap->addColumn('VOLUME', 'Volume', 'double', CreoleTypes::DOUBLE, false, null);

		$tMap->addColumn('KETERANGAN_KOEFISIEN', 'KeteranganKoefisien', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('KOEFISIEN', 'Koefisien', 'double', CreoleTypes::DOUBLE, false, null);

		$tMap->addColumn('SUBTITLE', 'Subtitle', 'string', CreoleTypes::VARCHAR, false, 300);

		$tMap->addColumn('KOMPONEN_HARGA', 'KomponenHarga', 'double', CreoleTypes::DOUBLE, false, null);

		$tMap->addColumn('KOMPONEN_HARGA_AWAL', 'KomponenHargaAwal', 'double', CreoleTypes::DOUBLE, false, null);

		$tMap->addColumn('KOMPONEN_NAME', 'KomponenName', 'string', CreoleTypes::VARCHAR, false, 500);

		$tMap->addColumn('SATUAN', 'Satuan', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('PAJAK', 'Pajak', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('PARAM', 'Param', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('FROM_KEGIATAN_MEMBER', 'FromKegiatanMember', 'string', CreoleTypes::VARCHAR, false, 30);

		$tMap->addColumn('TEMP_KOMPONEN_ID', 'TempKomponenId', 'string', CreoleTypes::VARCHAR, false, 50);

	} 
} 