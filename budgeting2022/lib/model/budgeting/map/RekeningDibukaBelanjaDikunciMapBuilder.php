<?php



class RekeningDibukaBelanjaDikunciMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.RekeningDibukaBelanjaDikunciMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.rekening_dibuka_belanja_dikunci');
		$tMap->setPhpName('RekeningDibukaBelanjaDikunci');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('BELANJA_DIKUNCI_ID', 'BelanjaDikunciId', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('UNIT_ID', 'UnitId', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('KEGIATAN_CODE', 'KegiatanCode', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('REKENING_CODE', 'RekeningCode', 'string', CreoleTypes::LONGVARCHAR, false, null);

	} 
} 