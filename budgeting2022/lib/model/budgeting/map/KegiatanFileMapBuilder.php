<?php



class KegiatanFileMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.KegiatanFileMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.kegiatan_file');
		$tMap->setPhpName('KegiatanFile');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('UNIT_ID', 'UnitId', 'string', CreoleTypes::VARCHAR, true, 10);

		$tMap->addPrimaryKey('KEGIATAN_CODE', 'KegiatanCode', 'string', CreoleTypes::VARCHAR, true, 20);

		$tMap->addPrimaryKey('URUT', 'Urut', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('NAMA_FILE', 'NamaFile', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('TIPE_FILE', 'TipeFile', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('ISI_FILE', 'IsiFile', 'string', CreoleTypes::BINARY, false, null);

	} 
} 