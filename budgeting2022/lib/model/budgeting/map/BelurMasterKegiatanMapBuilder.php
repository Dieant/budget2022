<?php



class BelurMasterKegiatanMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.BelurMasterKegiatanMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.belur_master_kegiatan');
		$tMap->setPhpName('BelurMasterKegiatan');

		$tMap->setUseIdGenerator(true);

		$tMap->setPrimaryKeyMethodInfo('ebudget.belur_master_kegi_SEQ_13');

		$tMap->addPrimaryKey('UNIT_ID', 'UnitId', 'string', CreoleTypes::VARCHAR, true, 10);

		$tMap->addPrimaryKey('KODE_KEGIATAN', 'KodeKegiatan', 'string', CreoleTypes::VARCHAR, true, 20);

		$tMap->addColumn('KODE_BIDANG', 'KodeBidang', 'string', CreoleTypes::VARCHAR, false, 2);

		$tMap->addColumn('KODE_URUSAN_WAJIB', 'KodeUrusanWajib', 'string', CreoleTypes::VARCHAR, false, 2);

		$tMap->addColumn('KODE_PROGRAM', 'KodeProgram', 'string', CreoleTypes::VARCHAR, false, 2);

		$tMap->addColumn('KODE_SASARAN', 'KodeSasaran', 'string', CreoleTypes::VARCHAR, false, 5);

		$tMap->addColumn('KODE_INDIKATOR', 'KodeIndikator', 'string', CreoleTypes::VARCHAR, false, 2);

		$tMap->addColumn('ALOKASI_DANA', 'AlokasiDana', 'double', CreoleTypes::DOUBLE, false, null);

		$tMap->addColumn('NAMA_KEGIATAN', 'NamaKegiatan', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('MASUKAN', 'Masukan', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('OUTPUT', 'Output', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('OUTCOME', 'Outcome', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('BENEFIT', 'Benefit', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('IMPACT', 'Impact', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('TIPE', 'Tipe', 'string', CreoleTypes::VARCHAR, false, 10);

		$tMap->addColumn('KEGIATAN_ACTIVE', 'KegiatanActive', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('TO_KEGIATAN_CODE', 'ToKegiatanCode', 'string', CreoleTypes::VARCHAR, false, 12);

		$tMap->addColumn('CATATAN', 'Catatan', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('TARGET_OUTCOME', 'TargetOutcome', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('LOKASI', 'Lokasi', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('JUMLAH_PREV', 'JumlahPrev', 'double', CreoleTypes::DOUBLE, false, null);

		$tMap->addColumn('JUMLAH_NOW', 'JumlahNow', 'double', CreoleTypes::DOUBLE, false, null);

		$tMap->addColumn('JUMLAH_NEXT', 'JumlahNext', 'double', CreoleTypes::DOUBLE, false, null);

		$tMap->addColumn('KODE_PROGRAM2', 'KodeProgram2', 'string', CreoleTypes::VARCHAR, false, 30);

		$tMap->addColumn('KODE_URUSAN', 'KodeUrusan', 'string', CreoleTypes::VARCHAR, false, 10);

		$tMap->addColumn('LAST_UPDATE_USER', 'LastUpdateUser', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('LAST_UPDATE_TIME', 'LastUpdateTime', 'int', CreoleTypes::TIMESTAMP, false, null);

		$tMap->addColumn('LAST_UPDATE_IP', 'LastUpdateIp', 'string', CreoleTypes::VARCHAR, false, 20);

		$tMap->addColumn('TAHAP', 'Tahap', 'string', CreoleTypes::VARCHAR, false, 20);

		$tMap->addColumn('KODE_MISI', 'KodeMisi', 'string', CreoleTypes::VARCHAR, false, 2);

		$tMap->addColumn('KODE_TUJUAN', 'KodeTujuan', 'string', CreoleTypes::VARCHAR, false, 2);

		$tMap->addColumn('RANKING', 'Ranking', 'int', CreoleTypes::SMALLINT, false, null);

		$tMap->addColumn('NOMOR13', 'Nomor13', 'string', CreoleTypes::VARCHAR, false, 4);

		$tMap->addColumn('PPA_NAMA', 'PpaNama', 'string', CreoleTypes::VARCHAR, false, 200);

		$tMap->addColumn('PPA_PANGKAT', 'PpaPangkat', 'string', CreoleTypes::VARCHAR, false, 200);

		$tMap->addColumn('PPA_NIP', 'PpaNip', 'string', CreoleTypes::VARCHAR, false, 30);

		$tMap->addColumn('LANJUTAN', 'Lanjutan', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('USER_ID', 'UserId', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('TAHUN', 'Tahun', 'string', CreoleTypes::VARCHAR, false, 5);

		$tMap->addColumn('TAMBAHAN_PAGU', 'TambahanPagu', 'double', CreoleTypes::DOUBLE, false, null);

		$tMap->addColumn('GENDER', 'Gender', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('KODE_KEG_KEUANGAN', 'KodeKegKeuangan', 'string', CreoleTypes::VARCHAR, false, 15);

	} 
} 