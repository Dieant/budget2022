<?php



class SasaranPdMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.SasaranPdMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.sasaran_pd');
		$tMap->setPhpName('SasaranPd');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('ID_SASARAN', 'IdSasaran', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('SASARAN_PD', 'SasaranPd', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('UNIT_ID', 'UnitId', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('KODE_KEGIATAN', 'KodeKegiatan', 'string', CreoleTypes::VARCHAR, false, null);

	} 
} 