<?php



class KomentarMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.KomentarMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('komentar');
		$tMap->setPhpName('Komentar');

		$tMap->setUseIdGenerator(true);

		$tMap->setPrimaryKeyMethodInfo('komentar_SEQ');

		$tMap->addPrimaryKey('NO', 'No', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('JUDUL', 'Judul', 'string', CreoleTypes::VARCHAR, false, 255);

		$tMap->addColumn('ISI', 'Isi', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addForeignKey('USER_ID', 'UserId', 'string', CreoleTypes::VARCHAR, 'master_user', 'USER_ID', false, 20);

		$tMap->addColumn('CREATED_AT', 'CreatedAt', 'int', CreoleTypes::TIMESTAMP, false, null);

		$tMap->addColumn('UPDATED_AT', 'UpdatedAt', 'int', CreoleTypes::TIMESTAMP, false, null);

		$tMap->addColumn('LOCKED', 'Locked', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('TIPE', 'Tipe', 'int', CreoleTypes::SMALLINT, false, null);

	} 
} 