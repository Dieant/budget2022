<?php



class TempLokasiMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.TempLokasiMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('temp_lokasi');
		$tMap->setPhpName('TempLokasi');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('KODE', 'Kode', 'string', CreoleTypes::VARCHAR, true, 10);

		$tMap->addColumn('NAMA', 'Nama', 'string', CreoleTypes::VARCHAR, false, 500);

	} 
} 