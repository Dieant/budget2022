<?php



class UnitKerjaMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.UnitKerjaMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('unit_kerja');
		$tMap->setPhpName('UnitKerja');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('UNIT_ID', 'UnitId', 'string', CreoleTypes::VARCHAR, true, 10);

		$tMap->addForeignKey('KELOMPOK_ID', 'KelompokId', 'double', CreoleTypes::NUMERIC, 'kelompok_dinas', 'KELOMPOK_ID', false, 2);

		$tMap->addColumn('UNIT_NAME', 'UnitName', 'string', CreoleTypes::VARCHAR, false, 500);

		$tMap->addColumn('UNIT_ADDRESS', 'UnitAddress', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('KEPALA_NAMA', 'KepalaNama', 'string', CreoleTypes::VARCHAR, false, 50);

		$tMap->addColumn('KEPALA_PANGKAT', 'KepalaPangkat', 'string', CreoleTypes::VARCHAR, false, 50);

		$tMap->addColumn('KEPALA_NIP', 'KepalaNip', 'string', CreoleTypes::VARCHAR, false, 50);

		$tMap->addColumn('KODE_PERMEN', 'KodePermen', 'string', CreoleTypes::VARCHAR, false, 10);

		$tMap->addColumn('PAGU', 'Pagu', 'double', CreoleTypes::DOUBLE, false, null);

		$tMap->addColumn('JUMLAH_PNS', 'JumlahPns', 'double', CreoleTypes::DOUBLE, false, null);

		$tMap->addColumn('JUMLAH_HONDA', 'JumlahHonda', 'double', CreoleTypes::DOUBLE, false, null);

		$tMap->addColumn('JUMLAH_NONPNS', 'JumlahNonpns', 'double', CreoleTypes::DOUBLE, false, null);

	} 
} 