<?php



class RekeningMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.RekeningMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.rekening');
		$tMap->setPhpName('Rekening');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('REKENING_CODE', 'RekeningCode', 'string', CreoleTypes::VARCHAR, true, 100);

		$tMap->addColumn('USER_ID', 'UserId', 'string', CreoleTypes::VARCHAR, false, 20);

		$tMap->addColumn('BELANJA_ID', 'BelanjaId', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('REKENING_NAME', 'RekeningName', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('REKENING_PPN', 'RekeningPpn', 'int', CreoleTypes::SMALLINT, false, null);

		$tMap->addColumn('REKENING_PPH', 'RekeningPph', 'int', CreoleTypes::SMALLINT, false, null);

		$tMap->addColumn('IP_ADDRESS', 'IpAddress', 'string', CreoleTypes::VARCHAR, false, 20);

		$tMap->addColumn('WAKTU_ACCESS', 'WaktuAccess', 'int', CreoleTypes::TIMESTAMP, false, null);

		$tMap->addColumn('AKRUAL_CODE', 'AkrualCode', 'string', CreoleTypes::VARCHAR, false, 50);

		$tMap->addColumn('AKRUAL_KONSTRUKSI', 'AkrualKonstruksi', 'string', CreoleTypes::VARCHAR, false, 20);

	} 
} 