<?php



class MasterSchemaMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.MasterSchemaMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('master_schema');
		$tMap->setPhpName('MasterSchema');

		$tMap->setUseIdGenerator(true);

		$tMap->setPrimaryKeyMethodInfo('master_schema_SEQ');

		$tMap->addPrimaryKey('SCHEMA_ID', 'SchemaId', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('SCHEMA_NAME', 'SchemaName', 'string', CreoleTypes::VARCHAR, false, 30);

	} 
} 