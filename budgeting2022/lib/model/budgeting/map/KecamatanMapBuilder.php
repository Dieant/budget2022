<?php



class KecamatanMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.KecamatanMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('kecamatan');
		$tMap->setPhpName('Kecamatan');

		$tMap->setUseIdGenerator(false);

		$tMap->addColumn('NAMA', 'Nama', 'string', CreoleTypes::VARCHAR, false, 150);

		$tMap->addColumn('ID', 'Id', 'int', CreoleTypes::INTEGER, false, null);

	} 
} 