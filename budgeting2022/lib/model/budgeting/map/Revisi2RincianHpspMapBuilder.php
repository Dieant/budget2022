<?php



class Revisi2RincianHpspMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.Revisi2RincianHpspMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.revisi2_rincian_hpsp');
		$tMap->setPhpName('Revisi2RincianHpsp');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('UNIT_ID', 'UnitId', 'string', CreoleTypes::VARCHAR, true, 8);

		$tMap->addPrimaryKey('KEGIATAN_CODE', 'KegiatanCode', 'string', CreoleTypes::VARCHAR, true, 20);

		$tMap->addColumn('KOMPONEN_ID', 'KomponenId', 'string', CreoleTypes::VARCHAR, false, 50);

		$tMap->addPrimaryKey('DETAIL_NO', 'DetailNo', 'int', CreoleTypes::SMALLINT, true, null);

		$tMap->addColumn('ID_KOMPONEN_LELANG', 'IdKomponenLelang', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('SISA_ANGGARAN', 'SisaAnggaran', 'double', CreoleTypes::DOUBLE, false, null);

		$tMap->addColumn('CATATAN', 'Catatan', 'string', CreoleTypes::VARCHAR, false, 300);

		$tMap->addColumn('TAHAP', 'Tahap', 'string', CreoleTypes::VARCHAR, false, null);

	} 
} 