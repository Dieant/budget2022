<?php



class SubKegiatanMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.SubKegiatanMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.sub_kegiatan');
		$tMap->setPhpName('SubKegiatan');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('SUB_KEGIATAN_ID', 'SubKegiatanId', 'string', CreoleTypes::VARCHAR, true, 50);

		$tMap->addColumn('SUB_KEGIATAN_NAME', 'SubKegiatanName', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('PARAM', 'Param', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('SATUAN', 'Satuan', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('STATUS', 'Status', 'string', CreoleTypes::VARCHAR, false, 15);

		$tMap->addColumn('PEMBAGI', 'Pembagi', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('KETERANGAN', 'Keterangan', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('UNIT_ID', 'UnitId', 'string', CreoleTypes::VARCHAR, false, 20);

		$tMap->addColumn('PENELITIAN_SKALA', 'PenelitianSkala', 'string', CreoleTypes::VARCHAR, false, 10);

		$tMap->addColumn('PENELITIAN_WAKTU', 'PenelitianWaktu', 'int', CreoleTypes::SMALLINT, false, null);

		$tMap->addColumn('BATAS_NILAI', 'BatasNilai', 'string', CreoleTypes::BIGINT, false, null);

		$tMap->addColumn('TENAGA_AHLI', 'TenagaAhli', 'int', CreoleTypes::SMALLINT, false, null);

	} 
} 