<?php



class MurniRkaMemberMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.MurniRkaMemberMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.murni_rka_member');
		$tMap->setPhpName('MurniRkaMember');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('KODE_SUB', 'KodeSub', 'string', CreoleTypes::VARCHAR, true, 30);

		$tMap->addColumn('UNIT_ID', 'UnitId', 'string', CreoleTypes::VARCHAR, true, 10);

		$tMap->addColumn('KEGIATAN_CODE', 'KegiatanCode', 'string', CreoleTypes::VARCHAR, true, 12);

		$tMap->addColumn('KOMPONEN_ID', 'KomponenId', 'string', CreoleTypes::VARCHAR, false, 50);

		$tMap->addColumn('KOMPONEN_NAME', 'KomponenName', 'string', CreoleTypes::VARCHAR, false, 300);

		$tMap->addColumn('DETAIL_NAME', 'DetailName', 'string', CreoleTypes::VARCHAR, false, 200);

		$tMap->addColumn('REKENING_ASLI', 'RekeningAsli', 'string', CreoleTypes::VARCHAR, false, 30);

		$tMap->addColumn('TAHUN', 'Tahun', 'string', CreoleTypes::VARCHAR, false, 5);

		$tMap->addColumn('DETAIL_NO', 'DetailNo', 'int', CreoleTypes::SMALLINT, true, null);

	} 
} 