<?php



class SurveyorMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.SurveyorMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.surveyor');
		$tMap->setPhpName('Surveyor');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('NAMA', 'Nama', 'string', CreoleTypes::LONGVARCHAR, true, null);

	} 
} 