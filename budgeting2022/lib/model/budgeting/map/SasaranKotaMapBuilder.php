<?php



class SasaranKotaMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.SasaranKotaMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.sasaran_kota');
		$tMap->setPhpName('SasaranKota');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('KODE_SASARAN_KOTA', 'KodeSasaranKota', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('SASARAN_KOTA', 'SasaranKota', 'string', CreoleTypes::VARCHAR, false, null);

	} 
} 