<?php



class LogRequestDinasMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.LogRequestDinasMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.log_request_dinas');
		$tMap->setPhpName('LogRequestDinas');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('UNIT_ID', 'UnitId', 'string', CreoleTypes::VARCHAR, false, 10);

		$tMap->addColumn('KEGIATAN_CODE', 'KegiatanCode', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('TIPE', 'Tipe', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('PATH', 'Path', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('CREATED_AT', 'CreatedAt', 'int', CreoleTypes::TIMESTAMP, false, null);

		$tMap->addColumn('UPDATED_AT', 'UpdatedAt', 'int', CreoleTypes::TIMESTAMP, false, null);

		$tMap->addColumn('CATATAN', 'Catatan', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('IS_KHUSUS', 'IsKhusus', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('STATUS', 'Status', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('CATATAN_TOLAK', 'CatatanTolak', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('USER_ID', 'UserId', 'string', CreoleTypes::VARCHAR, false, 50);

	} 
} 