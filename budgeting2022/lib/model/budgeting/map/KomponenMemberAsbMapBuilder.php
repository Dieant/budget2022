<?php



class KomponenMemberAsbMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.KomponenMemberAsbMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.komponen_member_asb');
		$tMap->setPhpName('KomponenMemberAsb');

		$tMap->setUseIdGenerator(false);

		$tMap->addColumn('KOMPONEN_ID', 'KomponenId', 'string', CreoleTypes::VARCHAR, true, 50);

		$tMap->addColumn('MEMBER_ID', 'MemberId', 'string', CreoleTypes::VARCHAR, true, 50);

		$tMap->addColumn('SATUAN', 'Satuan', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('MEMBER_NAME', 'MemberName', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('MEMBER_HARGA', 'MemberHarga', 'double', CreoleTypes::DOUBLE, false, null);

		$tMap->addColumn('KOEFISIEN', 'Koefisien', 'double', CreoleTypes::DOUBLE, false, null);

		$tMap->addColumn('MEMBER_TOTAL', 'MemberTotal', 'double', CreoleTypes::DOUBLE, false, null);

		$tMap->addColumn('IP_ADDRESS', 'IpAddress', 'string', CreoleTypes::VARCHAR, false, 20);

		$tMap->addColumn('WAKTU_ACCESS', 'WaktuAccess', 'int', CreoleTypes::TIMESTAMP, false, null);

		$tMap->addColumn('MEMBER_TIPE', 'MemberTipe', 'string', CreoleTypes::VARCHAR, false, 10);

		$tMap->addColumn('SUBTITLE', 'Subtitle', 'string', CreoleTypes::VARCHAR, false, 250);

		$tMap->addColumn('MEMBER_NO', 'MemberNo', 'string', CreoleTypes::BIGINT, false, null);

		$tMap->addColumn('TIPE', 'Tipe', 'string', CreoleTypes::VARCHAR, false, 40);

		$tMap->addColumn('KOMPONEN_TIPE', 'KomponenTipe', 'string', CreoleTypes::VARCHAR, false, 10);

		$tMap->addColumn('FROM_ID', 'FromId', 'string', CreoleTypes::VARCHAR, false, 30);

		$tMap->addColumn('FROM_KOEF', 'FromKoef', 'double', CreoleTypes::DOUBLE, false, null);

		$tMap->addColumn('HSPK_NAME', 'HspkName', 'string', CreoleTypes::VARCHAR, false, 300);

	} 
} 