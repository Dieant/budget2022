<?php



class KategoriShsdTempMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.KategoriShsdTempMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.kategori_shsd_temp');
		$tMap->setPhpName('KategoriShsdTemp');

		$tMap->setUseIdGenerator(false);

		$tMap->addColumn('KATEGORI_SHSD_ID', 'KategoriShsdId', 'string', CreoleTypes::VARCHAR, true, 50);

		$tMap->addColumn('KATEGORI_SHSD_NAME', 'KategoriShsdName', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('REKENING_CODE', 'RekeningCode', 'string', CreoleTypes::VARCHAR, false, 50);

		$tMap->addColumn('REKENING', 'Rekening', 'string', CreoleTypes::VARCHAR, false, null);

	} 
} 