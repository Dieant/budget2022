<?php



class MasterSubMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.MasterSubMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.master_sub');
		$tMap->setPhpName('MasterSub');

		$tMap->setUseIdGenerator(true);

		$tMap->setPrimaryKeyMethodInfo('ebudget.master_sub_SEQ');

		$tMap->addColumn('KODE_SUB', 'KodeSub', 'string', CreoleTypes::VARCHAR, false, 40);

		$tMap->addColumn('SUB', 'Sub', 'string', CreoleTypes::VARCHAR, false, 400);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

	} 
} 