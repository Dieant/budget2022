<?php



class BappekoKomponenRekeningMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.BappekoKomponenRekeningMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.bappeko_komponen_rekening');
		$tMap->setPhpName('BappekoKomponenRekening');

		$tMap->setUseIdGenerator(false);

		$tMap->addColumn('REKENING_CODE', 'RekeningCode', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('KOMPONEN_ID', 'KomponenId', 'string', CreoleTypes::VARCHAR, false, 50);

		$tMap->addColumn('REKENING_CODE29', 'RekeningCode29', 'string', CreoleTypes::VARCHAR, false, 15);

	} 
} 