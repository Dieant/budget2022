<?php



class BeritaAcaraRevisiMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.BeritaAcaraRevisiMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.berita_acara_revisi');
		$tMap->setPhpName('BeritaAcaraRevisi');

		$tMap->setUseIdGenerator(false);

		$tMap->addColumn('UNIT_ID', 'UnitId', 'string', CreoleTypes::VARCHAR, false, 10);

		$tMap->addColumn('KEGIATAN_CODE', 'KegiatanCode', 'string', CreoleTypes::VARCHAR, false, 12);

		$tMap->addColumn('NOMOR', 'Nomor', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('KESIMPULAN', 'Kesimpulan', 'string', CreoleTypes::VARCHAR, false, 800);

		$tMap->addColumn('DASAR1', 'Dasar1', 'string', CreoleTypes::VARCHAR, false, 500);

		$tMap->addColumn('DASAR2', 'Dasar2', 'string', CreoleTypes::VARCHAR, false, 500);

		$tMap->addColumn('DASAR3', 'Dasar3', 'string', CreoleTypes::VARCHAR, false, 500);

		$tMap->addColumn('DASAR4', 'Dasar4', 'string', CreoleTypes::VARCHAR, false, 500);

		$tMap->addColumn('DASAR5', 'Dasar5', 'string', CreoleTypes::VARCHAR, false, 500);

		$tMap->addColumn('DASAR6', 'Dasar6', 'string', CreoleTypes::VARCHAR, false, 500);

		$tMap->addColumn('DASAR7', 'Dasar7', 'string', CreoleTypes::VARCHAR, false, 500);

		$tMap->addColumn('DASAR8', 'Dasar8', 'string', CreoleTypes::VARCHAR, false, 500);

		$tMap->addColumn('DASAR9', 'Dasar9', 'string', CreoleTypes::VARCHAR, false, 500);

		$tMap->addColumn('DASAR10', 'Dasar10', 'string', CreoleTypes::VARCHAR, false, 500);

		$tMap->addColumn('PERTIMBANGAN1', 'Pertimbangan1', 'string', CreoleTypes::VARCHAR, false, 500);

		$tMap->addColumn('PERTIMBANGAN2', 'Pertimbangan2', 'string', CreoleTypes::VARCHAR, false, 500);

		$tMap->addColumn('PERTIMBANGAN3', 'Pertimbangan3', 'string', CreoleTypes::VARCHAR, false, 500);

		$tMap->addColumn('PERTIMBANGAN4', 'Pertimbangan4', 'string', CreoleTypes::VARCHAR, false, 500);

		$tMap->addColumn('PERTIMBANGAN5', 'Pertimbangan5', 'string', CreoleTypes::VARCHAR, false, 500);

		$tMap->addColumn('PERTIMBANGAN6', 'Pertimbangan6', 'string', CreoleTypes::VARCHAR, false, 500);

		$tMap->addColumn('PERTIMBANGAN7', 'Pertimbangan7', 'string', CreoleTypes::VARCHAR, false, 500);

		$tMap->addColumn('PERTIMBANGAN8', 'Pertimbangan8', 'string', CreoleTypes::VARCHAR, false, 500);

		$tMap->addColumn('PERTIMBANGAN9', 'Pertimbangan9', 'string', CreoleTypes::VARCHAR, false, 500);

		$tMap->addColumn('PERTIMBANGAN10', 'Pertimbangan10', 'string', CreoleTypes::VARCHAR, false, 500);

	} 
} 