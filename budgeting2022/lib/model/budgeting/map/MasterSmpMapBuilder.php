<?php



class MasterSmpMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.MasterSmpMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('master_smp');
		$tMap->setPhpName('MasterSmp');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('KODE', 'Kode', 'string', CreoleTypes::VARCHAR, true, 16);

		$tMap->addColumn('NAMA_SMP', 'NamaSmp', 'string', CreoleTypes::VARCHAR, false, 200);

		$tMap->addColumn('ALAMAT', 'Alamat', 'string', CreoleTypes::VARCHAR, false, 200);

		$tMap->addColumn('STATUS', 'Status', 'string', CreoleTypes::VARCHAR, false, 50);

		$tMap->addColumn('KECAMATAN', 'Kecamatan', 'string', CreoleTypes::VARCHAR, false, 50);

		$tMap->addColumn('KELURAHAN', 'Kelurahan', 'string', CreoleTypes::VARCHAR, false, 50);

		$tMap->addColumn('KODE_JALAN', 'KodeJalan', 'string', CreoleTypes::VARCHAR, false, 16);

	} 
} 