<?php



class UserHandleV2MapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.UserHandleV2MapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('user_handle_v2');
		$tMap->setPhpName('UserHandleV2');

		$tMap->setUseIdGenerator(false);

		$tMap->addColumn('USER_ID', 'UserId', 'string', CreoleTypes::VARCHAR, true, 20);

		$tMap->addColumn('SCHEMA_ID', 'SchemaId', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('UNIT_ID', 'UnitId', 'string', CreoleTypes::VARCHAR, true, 10);

	} 
} 