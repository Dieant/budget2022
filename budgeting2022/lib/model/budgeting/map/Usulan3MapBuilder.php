<?php



class Usulan3MapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.Usulan3MapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('usulan3');
		$tMap->setPhpName('Usulan3');

		$tMap->setUseIdGenerator(false);

		$tMap->addColumn('DARI', 'Dari', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('PEKERJAAN', 'Pekerjaan', 'string', CreoleTypes::VARCHAR, false, 800);

		$tMap->addColumn('KECAMATAN', 'Kecamatan', 'string', CreoleTypes::VARCHAR, false, 500);

		$tMap->addColumn('KELURAHAN', 'Kelurahan', 'string', CreoleTypes::VARCHAR, false, 800);

		$tMap->addColumn('NAMA', 'Nama', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('TIPE', 'Tipe', 'string', CreoleTypes::VARCHAR, false, 30);

		$tMap->addColumn('LOKASI', 'Lokasi', 'string', CreoleTypes::VARCHAR, false, 300);

		$tMap->addColumn('VOLUME', 'Volume', 'string', CreoleTypes::VARCHAR, false, 300);

		$tMap->addColumn('KETERANGAN', 'Keterangan', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('DANA', 'Dana', 'double', CreoleTypes::DOUBLE, false, null);

		$tMap->addColumn('SKPD', 'Skpd', 'string', CreoleTypes::VARCHAR, false, 300);

		$tMap->addColumn('TAHAP', 'Tahap', 'int', CreoleTypes::SMALLINT, false, null);

	} 
} 