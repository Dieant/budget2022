<?php



class MasterSasaranIndikatorMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.MasterSasaranIndikatorMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.master_sasaran_indikator');
		$tMap->setPhpName('MasterSasaranIndikator');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('KODE_SASARAN2', 'KodeSasaran2', 'string', CreoleTypes::VARCHAR, true, 10);

		$tMap->addPrimaryKey('KODE_SASARAN_INDIKATOR', 'KodeSasaranIndikator', 'string', CreoleTypes::VARCHAR, true, 10);

		$tMap->addColumn('INDIKATOR', 'Indikator', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('TARGET', 'Target', 'string', CreoleTypes::VARCHAR, false, null);

	} 
} 