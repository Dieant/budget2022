<?php



class MasterIndikatorMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.MasterIndikatorMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.master_indikator');
		$tMap->setPhpName('MasterIndikator');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('KODE_PROGRAM', 'KodeProgram', 'string', CreoleTypes::CHAR, true, 2);

		$tMap->addPrimaryKey('KODE_INDIKATOR', 'KodeIndikator', 'string', CreoleTypes::CHAR, true, 2);

		$tMap->addColumn('NAMA_INDIKATOR', 'NamaIndikator', 'string', CreoleTypes::VARCHAR, false, 200);

		$tMap->addColumn('KODE_INDIKATOR2', 'KodeIndikator2', 'string', CreoleTypes::VARCHAR, false, 10);

	} 
} 