<?php



class KelompokRekeningMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.KelompokRekeningMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.kelompok_rekening');
		$tMap->setPhpName('KelompokRekening');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('REKENING_CODE', 'RekeningCode', 'string', CreoleTypes::VARCHAR, true, 20);

		$tMap->addColumn('REKENING_NAME', 'RekeningName', 'string', CreoleTypes::VARCHAR, false, 300);

	} 
} 