<?php



class MasterSaluranMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.MasterSaluranMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('master_saluran');
		$tMap->setPhpName('MasterSaluran');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('KODE', 'Kode', 'string', CreoleTypes::VARCHAR, true, 16);

		$tMap->addColumn('NAMA_SALURAN', 'NamaSaluran', 'string', CreoleTypes::VARCHAR, false, 200);

		$tMap->addColumn('BATASAN', 'Batasan', 'string', CreoleTypes::VARCHAR, false, 500);

		$tMap->addColumn('KECAMATAN', 'Kecamatan', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('PANJANG', 'Panjang', 'string', CreoleTypes::VARCHAR, false, 50);

		$tMap->addColumn('LEBAR', 'Lebar', 'string', CreoleTypes::VARCHAR, false, 50);

		$tMap->addColumn('KETERANGAN', 'Keterangan', 'string', CreoleTypes::VARCHAR, false, 500);

		$tMap->addColumn('KELURAHAN', 'Kelurahan', 'string', CreoleTypes::VARCHAR, false, 50);

	} 
} 