<?php



class MasterMisiMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.MasterMisiMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.master_misi');
		$tMap->setPhpName('MasterMisi');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('KODE_MISI', 'KodeMisi', 'string', CreoleTypes::VARCHAR, true, 2);

		$tMap->addColumn('NAMA_MISI', 'NamaMisi', 'string', CreoleTypes::VARCHAR, false, 400);

	} 
} 