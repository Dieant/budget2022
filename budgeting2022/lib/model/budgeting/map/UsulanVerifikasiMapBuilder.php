<?php



class UsulanVerifikasiMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.UsulanVerifikasiMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.usulan_verifikasi');
		$tMap->setPhpName('UsulanVerifikasi');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('ID_VERIFIKASI', 'IdVerifikasi', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('SHSD_ID', 'ShsdId', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('SHSD_NAME', 'ShsdName', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('HARGA', 'Harga', 'double', CreoleTypes::DOUBLE, false, null);

		$tMap->addColumn('NAMA', 'Nama', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('SPEC', 'Spec', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('HIDDEN_SPEC', 'HiddenSpec', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('MEREK', 'Merek', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('SATUAN', 'Satuan', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('REKENING', 'Rekening', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('PAJAK', 'Pajak', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('KETERANGAN', 'Keterangan', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('ID_SPJM', 'IdSpjm', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('ID_USULAN', 'IdUsulan', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('CREATED_AT', 'CreatedAt', 'int', CreoleTypes::TIMESTAMP, false, null);

		$tMap->addColumn('UPDATED_AT', 'UpdatedAt', 'int', CreoleTypes::TIMESTAMP, false, null);

		$tMap->addColumn('UNIT_ID', 'UnitId', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('TIPE', 'Tipe', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('TAHAP', 'Tahap', 'string', CreoleTypes::LONGVARCHAR, false, null);

	} 
} 