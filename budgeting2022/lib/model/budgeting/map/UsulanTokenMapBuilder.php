<?php



class UsulanTokenMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.UsulanTokenMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.usulan_token');
		$tMap->setPhpName('UsulanToken');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('ID_TOKEN', 'IdToken', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('REQUIRED_AT', 'RequiredAt', 'int', CreoleTypes::TIMESTAMP, false, null);

		$tMap->addColumn('EXPIRED_AT', 'ExpiredAt', 'int', CreoleTypes::TIMESTAMP, false, null);

		$tMap->addColumn('USED_AT', 'UsedAt', 'int', CreoleTypes::TIMESTAMP, false, null);

		$tMap->addColumn('PENYELIA', 'Penyelia', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('UNIT_ID', 'UnitId', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('CREATED_BY', 'CreatedBy', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('JUMLAH_SSH', 'JumlahSsh', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('TOKEN', 'Token', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('STATUS_USED', 'StatusUsed', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('NOMOR_SURAT', 'NomorSurat', 'string', CreoleTypes::LONGVARCHAR, false, null);

	} 
} 