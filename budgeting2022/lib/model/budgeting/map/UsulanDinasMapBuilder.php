<?php



class UsulanDinasMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.UsulanDinasMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.usulan_dinas');
		$tMap->setPhpName('UsulanDinas');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('ID_USULAN', 'IdUsulan', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('TGL_USULAN', 'TglUsulan', 'int', CreoleTypes::TIMESTAMP, false, null);

		$tMap->addColumn('CREATED_AT', 'CreatedAt', 'int', CreoleTypes::TIMESTAMP, false, null);

		$tMap->addColumn('UPDATED_AT', 'UpdatedAt', 'int', CreoleTypes::TIMESTAMP, false, null);

		$tMap->addColumn('PENYELIA', 'Penyelia', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('SKPD', 'Skpd', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('JENIS_USULAN', 'JenisUsulan', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('JUMLAH_DUKUNGAN', 'JumlahDukungan', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('JUMLAH_USULAN', 'JumlahUsulan', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('STATUS_VERIFIKASI', 'StatusVerifikasi', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('STATUS_HAPUS', 'StatusHapus', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('FILEPATH', 'Filepath', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('FILEPATH_RAR', 'FilepathRar', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('KOMENTAR_VERIFIKATOR', 'KomentarVerifikator', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('NOMOR_SURAT', 'NomorSurat', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('STATUS_TOLAK', 'StatusTolak', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('KETERANGAN_DINAS', 'KeteranganDinas', 'string', CreoleTypes::LONGVARCHAR, false, null);

	} 
} 