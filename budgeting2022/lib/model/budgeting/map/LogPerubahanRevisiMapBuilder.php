<?php



class LogPerubahanRevisiMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.LogPerubahanRevisiMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.log_perubahan_revisi');
		$tMap->setPhpName('LogPerubahanRevisi');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('KEGIATAN_CODE', 'KegiatanCode', 'string', CreoleTypes::VARCHAR, true, 20);

		$tMap->addPrimaryKey('DETAIL_NO', 'DetailNo', 'int', CreoleTypes::SMALLINT, true, null);

		$tMap->addPrimaryKey('UNIT_ID', 'UnitId', 'string', CreoleTypes::VARCHAR, true, 8);

		$tMap->addPrimaryKey('TAHAP', 'Tahap', 'int', CreoleTypes::SMALLINT, true, null);

		$tMap->addColumn('STATUS', 'Status', 'int', CreoleTypes::SMALLINT, true, null);

		$tMap->addColumn('NILAI_ANGGARAN_SEMULA', 'NilaiAnggaranSemula', 'double', CreoleTypes::NUMERIC, true, 20);

		$tMap->addColumn('NILAI_ANGGARAN_MENJADI', 'NilaiAnggaranMenjadi', 'double', CreoleTypes::NUMERIC, true, 20);

		$tMap->addColumn('VOLUME_SEMULA', 'VolumeSemula', 'double', CreoleTypes::DOUBLE, false, null);

		$tMap->addColumn('VOLUME_MENJADI', 'VolumeMenjadi', 'double', CreoleTypes::DOUBLE, false, null);

		$tMap->addColumn('SATUAN_SEMULA', 'SatuanSemula', 'string', CreoleTypes::VARCHAR, false, 50);

		$tMap->addColumn('SATUAN_MENJADI', 'SatuanMenjadi', 'string', CreoleTypes::VARCHAR, false, 50);

	} 
} 