<?php



class KonversiSubtitleMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.KonversiSubtitleMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.konversi_subtitle');
		$tMap->setPhpName('KonversiSubtitle');

		$tMap->setUseIdGenerator(false);

		$tMap->addColumn('UNIT_ID_LAMA', 'UnitIdLama', 'string', CreoleTypes::VARCHAR, false, 20);

		$tMap->addColumn('KEGIATAN_CODE_LAMA', 'KegiatanCodeLama', 'string', CreoleTypes::VARCHAR, false, 50);

		$tMap->addColumn('SUBTITLE_LAMA', 'SubtitleLama', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('UNIT_ID_BARU', 'UnitIdBaru', 'string', CreoleTypes::VARCHAR, false, 20);

		$tMap->addColumn('KEGIATAN_CODE_BARU', 'KegiatanCodeBaru', 'string', CreoleTypes::VARCHAR, false, 50);

		$tMap->addColumn('SUBTITLE_BARU', 'SubtitleBaru', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('MASUK', 'Masuk', 'boolean', CreoleTypes::BOOLEAN, false, null);

	} 
} 