<?php



class KelompokDinasMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.KelompokDinasMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('kelompok_dinas');
		$tMap->setPhpName('KelompokDinas');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('KELOMPOK_ID', 'KelompokId', 'double', CreoleTypes::NUMERIC, true, 2);

		$tMap->addColumn('KELOMPOK_NAME', 'KelompokName', 'string', CreoleTypes::VARCHAR, false, 150);

		$tMap->addColumn('FRONT_CODE', 'FrontCode', 'string', CreoleTypes::VARCHAR, false, 20);

	} 
} 