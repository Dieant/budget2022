<?php



class IndikatorTujuanKotaMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.IndikatorTujuanKotaMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.indikator_tujuan_kota');
		$tMap->setPhpName('IndikatorTujuanKota');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('UNIT_ID', 'UnitId', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('KODE_KEGIATAN', 'KodeKegiatan', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('INDIKATOR_TUJUAN', 'IndikatorTujuan', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('NILAI', 'Nilai', 'string', CreoleTypes::VARCHAR, false, null);

	} 
} 