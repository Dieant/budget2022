<?php



class KelompokBelanjaMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.KelompokBelanjaMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.kelompok_belanja');
		$tMap->setPhpName('KelompokBelanja');

		$tMap->setUseIdGenerator(false);

		$tMap->addColumn('BELANJA_ID', 'BelanjaId', 'int', CreoleTypes::SMALLINT, false, null);

		$tMap->addColumn('BELANJA_NAME', 'BelanjaName', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('BELANJA_URUTAN', 'BelanjaUrutan', 'int', CreoleTypes::SMALLINT, false, null);

		$tMap->addColumn('BELANJA_CODE', 'BelanjaCode', 'string', CreoleTypes::VARCHAR, false, 20);

	} 
} 