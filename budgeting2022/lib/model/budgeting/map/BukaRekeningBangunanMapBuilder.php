<?php



class BukaRekeningBangunanMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.BukaRekeningBangunanMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.buka_rekening_bangunan');
		$tMap->setPhpName('BukaRekeningBangunan');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('UNIT_ID', 'UnitId', 'string', CreoleTypes::VARCHAR, true, 10);

	} 
} 