<?php



class MasterUserMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.MasterUserMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('master_user');
		$tMap->setPhpName('MasterUser');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('USER_ID', 'UserId', 'string', CreoleTypes::VARCHAR, true, 50);

		$tMap->addColumn('USER_NAME', 'UserName', 'string', CreoleTypes::VARCHAR, false, 50);

		$tMap->addColumn('USER_DEFAULT_PASSWORD', 'UserDefaultPassword', 'string', CreoleTypes::VARCHAR, false, 50);

		$tMap->addColumn('USER_PASSWORD', 'UserPassword', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('IP_ADDRESS', 'IpAddress', 'string', CreoleTypes::VARCHAR, false, 20);

		$tMap->addColumn('WAKTU_ACCESS', 'WaktuAccess', 'int', CreoleTypes::TIMESTAMP, false, null);

		$tMap->addColumn('USER_ENABLE', 'UserEnable', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('NIP', 'Nip', 'string', CreoleTypes::VARCHAR, false, 50);

		$tMap->addColumn('EULA_BUDGETING', 'EulaBudgeting', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('JABATAN', 'Jabatan', 'string', CreoleTypes::VARCHAR, false, 500);

		$tMap->addColumn('EMAIL', 'Email', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('TELEPON', 'Telepon', 'string', CreoleTypes::VARCHAR, false, 50);

		$tMap->addColumn('JENIS_KELAMIN', 'JenisKelamin', 'string', CreoleTypes::VARCHAR, false, 10);

	} 
} 