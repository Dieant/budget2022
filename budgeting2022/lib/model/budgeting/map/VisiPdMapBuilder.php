<?php



class VisiPdMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.VisiPdMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.visi_pd');
		$tMap->setPhpName('VisiPd');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('UNIT_ID', 'UnitId', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('VISI', 'Visi', 'string', CreoleTypes::LONGVARCHAR, false, null);

	} 
} 