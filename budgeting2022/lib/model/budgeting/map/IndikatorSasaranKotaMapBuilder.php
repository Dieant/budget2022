<?php



class IndikatorSasaranKotaMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.IndikatorSasaranKotaMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.indikator_sasaran_kota');
		$tMap->setPhpName('IndikatorSasaranKota');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('UNIT_ID', 'UnitId', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('KODE_KEGIATAN', 'KodeKegiatan', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('INDIKATOR_SASARAN', 'IndikatorSasaran', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('NILAI', 'Nilai', 'string', CreoleTypes::VARCHAR, false, null);

	} 
} 