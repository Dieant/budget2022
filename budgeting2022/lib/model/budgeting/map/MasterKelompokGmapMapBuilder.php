<?php



class MasterKelompokGmapMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.MasterKelompokGmapMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.master_kelompok_gmap');
		$tMap->setPhpName('MasterKelompokGmap');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('ID_KELOMPOK', 'IdKelompok', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('KODE_KELOMPOK', 'KodeKelompok', 'string', CreoleTypes::VARCHAR, false, 20);

		$tMap->addColumn('NAMA_OBJEK', 'NamaObjek', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('TIPE_OBJEK', 'TipeObjek', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('WARNA', 'Warna', 'string', CreoleTypes::VARCHAR, false, 20);

		$tMap->addColumn('REKENING', 'Rekening', 'string', CreoleTypes::VARCHAR, false, 10);

	} 
} 