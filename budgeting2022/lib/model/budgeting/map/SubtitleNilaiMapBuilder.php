<?php



class SubtitleNilaiMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.SubtitleNilaiMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.subtitle_nilai');
		$tMap->setPhpName('SubtitleNilai');

		$tMap->setUseIdGenerator(true);

		$tMap->setPrimaryKeyMethodInfo('ebudget.subtitle_nilai_SEQ');

		$tMap->addColumn('UNIT_ID', 'UnitId', 'string', CreoleTypes::VARCHAR, false, 10);

		$tMap->addColumn('NILAI_1', 'Nilai1', 'int', CreoleTypes::SMALLINT, false, null);

		$tMap->addColumn('NILAI_2', 'Nilai2', 'int', CreoleTypes::SMALLINT, false, null);

		$tMap->addColumn('NILAI_3', 'Nilai3', 'int', CreoleTypes::SMALLINT, false, null);

		$tMap->addColumn('TOTAL', 'Total', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('KEGIATAN_CODE', 'KegiatanCode', 'string', CreoleTypes::VARCHAR, false, 12);

		$tMap->addColumn('SUBTITLE', 'Subtitle', 'string', CreoleTypes::VARCHAR, false, 300);

		$tMap->addColumn('KETERANGAN', 'Keterangan', 'string', CreoleTypes::VARCHAR, false, 300);

		$tMap->addColumn('SUB_ID', 'SubId', 'int', CreoleTypes::INTEGER, true, null);

	} 
} 