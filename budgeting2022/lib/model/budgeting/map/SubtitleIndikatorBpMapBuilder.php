<?php



class SubtitleIndikatorBpMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.SubtitleIndikatorBpMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.subtitle_indikator_bp');
		$tMap->setPhpName('SubtitleIndikatorBp');

		$tMap->setUseIdGenerator(true);

		$tMap->setPrimaryKeyMethodInfo('ebudget.subtitle_indikator_SEQ_8');

		$tMap->addColumn('UNIT_ID', 'UnitId', 'string', CreoleTypes::VARCHAR, false, 10);

		$tMap->addColumn('KEGIATAN_CODE', 'KegiatanCode', 'string', CreoleTypes::VARCHAR, false, 12);

		$tMap->addColumn('SUBTITLE', 'Subtitle', 'string', CreoleTypes::VARCHAR, false, 250);

		$tMap->addColumn('INDIKATOR', 'Indikator', 'string', CreoleTypes::VARCHAR, false, 250);

		$tMap->addColumn('NILAI', 'Nilai', 'double', CreoleTypes::DOUBLE, false, null);

		$tMap->addColumn('SATUAN', 'Satuan', 'string', CreoleTypes::VARCHAR, false, 50);

		$tMap->addColumn('LAST_UPDATE_USER', 'LastUpdateUser', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('LAST_UPDATE_TIME', 'LastUpdateTime', 'int', CreoleTypes::TIMESTAMP, false, null);

		$tMap->addColumn('LAST_UPDATE_IP', 'LastUpdateIp', 'string', CreoleTypes::VARCHAR, false, 20);

		$tMap->addColumn('TAHAP', 'Tahap', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addPrimaryKey('SUB_ID', 'SubId', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('TAHUN', 'Tahun', 'string', CreoleTypes::VARCHAR, false, 5);

		$tMap->addColumn('LOCK_SUBTITLE', 'LockSubtitle', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('PRIORITAS', 'Prioritas', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('CATATAN', 'Catatan', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('LAKILAKI', 'Lakilaki', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('PEREMPUAN', 'Perempuan', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('DEWASA', 'Dewasa', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('ANAK', 'Anak', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('LANSIA', 'Lansia', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('INKLUSI', 'Inklusi', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('GENDER', 'Gender', 'boolean', CreoleTypes::BOOLEAN, false, null);

	} 
} 