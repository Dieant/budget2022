<?php



class RincianBappekoMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.RincianBappekoMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.rincian_bappeko');
		$tMap->setPhpName('RincianBappeko');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('UNIT_ID', 'UnitId', 'string', CreoleTypes::VARCHAR, true, 50);

		$tMap->addPrimaryKey('KODE_KEGIATAN', 'KodeKegiatan', 'string', CreoleTypes::VARCHAR, true, 50);

		$tMap->addColumn('TAHAP', 'Tahap', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('JAWABAN1_DINAS', 'Jawaban1Dinas', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('JAWABAN2_DINAS', 'Jawaban2Dinas', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('CATATAN_DINAS', 'CatatanDinas', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('JAWABAN1_BAPPEKO', 'Jawaban1Bappeko', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('JAWABAN2_BAPPEKO', 'Jawaban2Bappeko', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('CATATAN_BAPPEKO', 'CatatanBappeko', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('STATUS_BUKA', 'StatusBuka', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('UPDATED_AT', 'UpdatedAt', 'int', CreoleTypes::TIMESTAMP, false, null);

	} 
} 