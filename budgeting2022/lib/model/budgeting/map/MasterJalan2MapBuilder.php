<?php



class MasterJalan2MapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.MasterJalan2MapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('master_jalan2');
		$tMap->setPhpName('MasterJalan2');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('NAMA_JALAN', 'NamaJalan', 'string', CreoleTypes::VARCHAR, true, null);

	} 
} 