<?php



class ShsdSurveyMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.ShsdSurveyMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.shsd_survey');
		$tMap->setPhpName('ShsdSurvey');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('SHSD_ID', 'ShsdId', 'string', CreoleTypes::VARCHAR, true, 50);

		$tMap->addColumn('SATUAN', 'Satuan', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('SHSD_NAME', 'ShsdName', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('SHSD_HARGA', 'ShsdHarga', 'double', CreoleTypes::DOUBLE, false, null);

		$tMap->addColumn('SHSD_HARGA_DASAR', 'ShsdHargaDasar', 'double', CreoleTypes::DOUBLE, false, null);

		$tMap->addColumn('SHSD_SHOW', 'ShsdShow', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('SHSD_FAKTOR_INFLASI', 'ShsdFaktorInflasi', 'double', CreoleTypes::NUMERIC, false, 6);

		$tMap->addColumn('SHSD_LOCKED', 'ShsdLocked', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('REKENING_CODE', 'RekeningCode', 'string', CreoleTypes::VARCHAR, false, 300);

		$tMap->addColumn('SHSD_MERK', 'ShsdMerk', 'string', CreoleTypes::VARCHAR, false, 250);

		$tMap->addColumn('NON_PAJAK', 'NonPajak', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('SHSD_ID_OLD', 'ShsdIdOld', 'string', CreoleTypes::VARCHAR, false, 30);

		$tMap->addColumn('NAMA_DASAR', 'NamaDasar', 'string', CreoleTypes::VARCHAR, false, 300);

		$tMap->addColumn('SPEC', 'Spec', 'string', CreoleTypes::VARCHAR, false, 500);

		$tMap->addColumn('HIDDEN_SPEC', 'HiddenSpec', 'string', CreoleTypes::VARCHAR, false, 500);

		$tMap->addColumn('USULAN_SKPD', 'UsulanSkpd', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('IS_POTONG_BPJS', 'IsPotongBpjs', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('IS_IURAN_BPJS', 'IsIuranBpjs', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('TAHAP', 'Tahap', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('IS_SURVEY_BP', 'IsSurveyBp', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('IS_INFLASI', 'IsInflasi', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('TOKO1', 'Toko1', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('TOKO2', 'Toko2', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('TOKO3', 'Toko3', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('MERK1', 'Merk1', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('MERK2', 'Merk2', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('MERK3', 'Merk3', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('HARGA1', 'Harga1', 'double', CreoleTypes::DOUBLE, false, null);

		$tMap->addColumn('HARGA2', 'Harga2', 'double', CreoleTypes::DOUBLE, false, null);

		$tMap->addColumn('HARGA3', 'Harga3', 'double', CreoleTypes::DOUBLE, false, null);

		$tMap->addColumn('TANGGAL_SURVEY', 'TanggalSurvey', 'int', CreoleTypes::TIMESTAMP, false, null);

		$tMap->addColumn('SURVEYOR', 'Surveyor', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('FILE1', 'File1', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('FILE2', 'File2', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('FILE3', 'File3', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('SHSD_HARGA_LELANG', 'ShsdHargaLelang', 'double', CreoleTypes::DOUBLE, false, null);

		$tMap->addColumn('SHSD_HARGA_PAKAI', 'ShsdHargaPakai', 'double', CreoleTypes::DOUBLE, false, null);

		$tMap->addColumn('FILE_LELANG', 'FileLelang', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('FILE1_2', 'File12', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('FILE2_2', 'File22', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('FILE3_2', 'File32', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('KETERANGAN1', 'Keterangan1', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('KETERANGAN2', 'Keterangan2', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('KETERANGAN3', 'Keterangan3', 'string', CreoleTypes::VARCHAR, false, null);

	} 
} 