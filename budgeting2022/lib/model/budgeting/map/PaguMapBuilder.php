<?php



class PaguMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.PaguMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.pagu');
		$tMap->setPhpName('Pagu');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('UNIT_ID', 'UnitId', 'string', CreoleTypes::VARCHAR, true, 8);

		$tMap->addPrimaryKey('KODE_KEGIATAN', 'KodeKegiatan', 'string', CreoleTypes::VARCHAR, true, 20);

		$tMap->addColumn('ALOKASI', 'Alokasi', 'double', CreoleTypes::DOUBLE, false, null);

		$tMap->addColumn('PENYESUAIAN_UK', 'PenyesuaianUk', 'double', CreoleTypes::DOUBLE, false, null);

	} 
} 