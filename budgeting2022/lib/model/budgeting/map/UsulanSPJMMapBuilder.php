<?php



class UsulanSPJMMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.UsulanSPJMMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.usulan_spjm');
		$tMap->setPhpName('UsulanSPJM');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('ID_SPJM', 'IdSpjm', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('TGL_SPJM', 'TglSpjm', 'int', CreoleTypes::TIMESTAMP, false, null);

		$tMap->addColumn('CREATED_AT', 'CreatedAt', 'int', CreoleTypes::TIMESTAMP, false, null);

		$tMap->addColumn('UPDATED_AT', 'UpdatedAt', 'int', CreoleTypes::TIMESTAMP, false, null);

		$tMap->addColumn('PENYELIA', 'Penyelia', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('SKPD', 'Skpd', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('TIPE_USULAN', 'TipeUsulan', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('JENIS_USULAN', 'JenisUsulan', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('JUMLAH_DUKUNGAN', 'JumlahDukungan', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('STATUS_VERIFIKASI', 'StatusVerifikasi', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('STATUS_HAPUS', 'StatusHapus', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('FILEPATH', 'Filepath', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('FILEPATH_PDF', 'FilepathPdf', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('ID_TOKEN', 'IdToken', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('NOMOR_SURAT', 'NomorSurat', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('KOMENTAR_VERIFIKATOR', 'KomentarVerifikator', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('TAHAP', 'Tahap', 'string', CreoleTypes::LONGVARCHAR, false, null);

	} 
} 