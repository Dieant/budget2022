<?php



class GeojsonlokasiWaitinglistMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.GeojsonlokasiWaitinglistMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('gis_ebudget.geojsonlokasi_waitinglist');
		$tMap->setPhpName('GeojsonlokasiWaitinglist');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('IDGEOLOKASI', 'Idgeolokasi', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('UNIT_ID', 'UnitId', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('KEGIATAN_CODE', 'KegiatanCode', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('ID_WAITING', 'IdWaiting', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('SATUAN', 'Satuan', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('VOLUME', 'Volume', 'double', CreoleTypes::DOUBLE, false, null);

		$tMap->addColumn('NILAI_ANGGARAN', 'NilaiAnggaran', 'double', CreoleTypes::DOUBLE, false, null);

		$tMap->addColumn('TAHUN', 'Tahun', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('MLOKASI', 'Mlokasi', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('ID_KELOMPOK', 'IdKelompok', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('GEOJSON', 'Geojson', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('KETERANGAN', 'Keterangan', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('NMUSER', 'Nmuser', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('LEVEL', 'Level', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('KOMPONEN_NAME', 'KomponenName', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('STATUS_HAPUS', 'StatusHapus', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('KETERANGAN_ALAMAT', 'KeteranganAlamat', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('UNIT_NAME', 'UnitName', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('LAST_EDIT_TIME', 'LastEditTime', 'int', CreoleTypes::TIMESTAMP, false, null);

		$tMap->addColumn('LAST_CREATE_TIME', 'LastCreateTime', 'int', CreoleTypes::TIMESTAMP, false, null);

		$tMap->addColumn('KOORDINAT', 'Koordinat', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('LOKASI_KE', 'LokasiKe', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('KODE_DETAIL_KEGIATAN', 'KodeDetailKegiatan', 'string', CreoleTypes::VARCHAR, false, 50);

	} 
} 