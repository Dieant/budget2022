<?php



class SchemaAksesV2MapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.SchemaAksesV2MapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('schema_akses_v2');
		$tMap->setPhpName('SchemaAksesV2');

		$tMap->setUseIdGenerator(false);

		$tMap->addForeignPrimaryKey('USER_ID', 'UserId', 'string' , CreoleTypes::VARCHAR, 'master_user_v2', 'USER_ID', true, 50);

		$tMap->addForeignPrimaryKey('SCHEMA_ID', 'SchemaId', 'int' , CreoleTypes::INTEGER, 'master_schema', 'SCHEMA_ID', true, null);

		$tMap->addForeignKey('LEVEL_ID', 'LevelId', 'int', CreoleTypes::INTEGER, 'user_level', 'LEVEL_ID', false, null);

		$tMap->addColumn('IS_UBAH_PASS', 'IsUbahPass', 'boolean', CreoleTypes::BOOLEAN, false, null);

	} 
} 