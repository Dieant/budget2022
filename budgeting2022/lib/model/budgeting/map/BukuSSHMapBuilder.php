<?php



class BukuSSHMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.BukuSSHMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.buku_ssh');
		$tMap->setPhpName('BukuSSH');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('KODE', 'Kode', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('NAMA', 'Nama', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('MERK', 'Merk', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('SPEC', 'Spec', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('HIDDEN_SPEC', 'HiddenSpec', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('SATUAN', 'Satuan', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('PERUBAHAN', 'Perubahan', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('TANGGAL', 'Tanggal', 'int', CreoleTypes::TIMESTAMP, false, null);

		$tMap->addColumn('PERUBAHAN_REVISI', 'PerubahanRevisi', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('STATUS_HAPUS', 'StatusHapus', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('HARGA', 'Harga', 'double', CreoleTypes::DOUBLE, false, null);

		$tMap->addColumn('TAHUN', 'Tahun', 'string', CreoleTypes::VARCHAR, false, 10);

		$tMap->addColumn('REPLACED_AT', 'ReplacedAt', 'int', CreoleTypes::TIMESTAMP, false, null);

	} 
} 