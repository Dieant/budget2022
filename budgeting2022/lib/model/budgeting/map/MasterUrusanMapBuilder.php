<?php



class MasterUrusanMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.MasterUrusanMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.master_urusan');
		$tMap->setPhpName('MasterUrusan');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('KODE_URUSAN', 'KodeUrusan', 'string', CreoleTypes::VARCHAR, true, 10);

		$tMap->addColumn('NAMA_URUSAN', 'NamaUrusan', 'string', CreoleTypes::VARCHAR, false, 200);

		$tMap->addColumn('RANKING', 'Ranking', 'int', CreoleTypes::SMALLINT, false, null);

	} 
} 