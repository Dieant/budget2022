<?php



class WaitingListPUMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.WaitingListPUMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.waitinglist_pu');
		$tMap->setPhpName('WaitingListPU');

		$tMap->setUseIdGenerator(true);

		$tMap->setPrimaryKeyMethodInfo('ebudget.waitinglist_pu_id_waiting_seq');

		$tMap->addPrimaryKey('ID_WAITING', 'IdWaiting', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('UNIT_ID', 'UnitId', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('KEGIATAN_CODE', 'KegiatanCode', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('SUBTITLE', 'Subtitle', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('KOMPONEN_ID', 'KomponenId', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('KOMPONEN_NAME', 'KomponenName', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('KOMPONEN_LOKASI', 'KomponenLokasi', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('KOMPONEN_HARGA_AWAL', 'KomponenHargaAwal', 'double', CreoleTypes::DOUBLE, false, null);

		$tMap->addColumn('PAJAK', 'Pajak', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('KOMPONEN_SATUAN', 'KomponenSatuan', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('KOMPONEN_REKENING', 'KomponenRekening', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('KOEFISIEN', 'Koefisien', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('VOLUME', 'Volume', 'double', CreoleTypes::DOUBLE, false, null);

		$tMap->addColumn('NILAI_ANGGARAN', 'NilaiAnggaran', 'double', CreoleTypes::DOUBLE, false, null);

		$tMap->addColumn('TAHUN_INPUT', 'TahunInput', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('CREATED_AT', 'CreatedAt', 'int', CreoleTypes::TIMESTAMP, false, null);

		$tMap->addColumn('UPDATED_AT', 'UpdatedAt', 'int', CreoleTypes::TIMESTAMP, false, null);

		$tMap->addColumn('STATUS_HAPUS', 'StatusHapus', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('STATUS_WAITING', 'StatusWaiting', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('PRIORITAS', 'Prioritas', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('KODE_RKA', 'KodeRka', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('USER_PENGAMBIL', 'UserPengambil', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('NILAI_EE', 'NilaiEe', 'double', CreoleTypes::DOUBLE, false, null);

		$tMap->addColumn('KETERANGAN', 'Keterangan', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('KODE_JASMAS', 'KodeJasmas', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('KECAMATAN', 'Kecamatan', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('KELURAHAN', 'Kelurahan', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('IS_MUSRENBANG', 'IsMusrenbang', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('NILAI_ANGGARAN_SEMULA', 'NilaiAnggaranSemula', 'double', CreoleTypes::DOUBLE, false, null);

		$tMap->addColumn('METODE_WAITINGLIST', 'MetodeWaitinglist', 'int', CreoleTypes::INTEGER, false, null);

	} 
} 