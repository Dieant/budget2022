<?php



class PrintKoefisienMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.PrintKoefisienMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.print_koefisien');
		$tMap->setPhpName('PrintKoefisien');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('UNIT_ID', 'UnitId', 'string', CreoleTypes::VARCHAR, true, 20);

		$tMap->addPrimaryKey('KEGIATAN_CODE', 'KegiatanCode', 'string', CreoleTypes::VARCHAR, true, 30);

		$tMap->addPrimaryKey('PRINT_NO', 'PrintNo', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('KETERANGAN', 'Keterangan', 'string', CreoleTypes::VARCHAR, false, 400);

		$tMap->addColumn('TGL_PRINT', 'TglPrint', 'int', CreoleTypes::TIMESTAMP, false, null);

	} 
} 