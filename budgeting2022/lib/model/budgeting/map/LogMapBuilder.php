<?php



class LogMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.LogMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.log');
		$tMap->setPhpName('Log');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('LOG_ID', 'LogId', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('WAKTU_ACCESS', 'WaktuAccess', 'int', CreoleTypes::TIMESTAMP, false, null);

		$tMap->addColumn('IP_ADDRESS', 'IpAddress', 'string', CreoleTypes::VARCHAR, false, 20);

		$tMap->addColumn('LOGIN', 'Login', 'string', CreoleTypes::VARCHAR, false, 20);

		$tMap->addColumn('TABEL_ASAL', 'TabelAsal', 'string', CreoleTypes::VARCHAR, false, 50);

		$tMap->addColumn('ISI1', 'Isi1', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('ISI2', 'Isi2', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('ISI3', 'Isi3', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('ISI4', 'Isi4', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('ISI5', 'Isi5', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('ISI6', 'Isi6', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('ISI7', 'Isi7', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('ISI8', 'Isi8', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('ISI9', 'Isi9', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('ISI10', 'Isi10', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('ISI11', 'Isi11', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('ISI12', 'Isi12', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('ISI13', 'Isi13', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('ISI14', 'Isi14', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('ISI15', 'Isi15', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('ISI16', 'Isi16', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('ISI17', 'Isi17', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('ISI18', 'Isi18', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('ISI19', 'Isi19', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('ISI20', 'Isi20', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('ISI21', 'Isi21', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('ISI22', 'Isi22', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('ISI23', 'Isi23', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('ISI24', 'Isi24', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('ISI25', 'Isi25', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('ISI26', 'Isi26', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('ISI27', 'Isi27', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('ISI28', 'Isi28', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('ISI29', 'Isi29', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('ISI30', 'Isi30', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('ISI31', 'Isi31', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('ISI32', 'Isi32', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('ISI33', 'Isi33', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('ISI34', 'Isi34', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('ISI35', 'Isi35', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('ISI36', 'Isi36', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('ISI37', 'Isi37', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('ISI38', 'Isi38', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('ISI39', 'Isi39', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('ISI40', 'Isi40', 'string', CreoleTypes::VARCHAR, false, 100);

	} 
} 