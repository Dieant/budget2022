<?php



class MasterBidangMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.MasterBidangMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.master_bidang');
		$tMap->setPhpName('MasterBidang');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('KODE_BIDANG', 'KodeBidang', 'string', CreoleTypes::VARCHAR, true, 10);

		$tMap->addColumn('NAMA_BIDANG', 'NamaBidang', 'string', CreoleTypes::VARCHAR, false, 300);

	} 
} 