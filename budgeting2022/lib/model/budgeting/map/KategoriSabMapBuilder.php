<?php



class KategoriSabMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.KategoriSabMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.kategori_sab');
		$tMap->setPhpName('KategoriSab');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('KATEGORI_SAB_ID', 'KategoriSabId', 'string', CreoleTypes::VARCHAR, true, 50);

		$tMap->addColumn('KATEGORI_SAB_NAME', 'KategoriSabName', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('REKENING_CODE', 'RekeningCode', 'string', CreoleTypes::VARCHAR, false, 50);

	} 
} 