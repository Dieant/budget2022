<?php



class MasterKegiatan2MapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.MasterKegiatan2MapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.master_kegiatan2');
		$tMap->setPhpName('MasterKegiatan2');

		$tMap->setUseIdGenerator(true);

		$tMap->setPrimaryKeyMethodInfo('ebudget.master_kegiatan2_SEQ');

		$tMap->addPrimaryKey('KODE_PROGRAM2', 'KodeProgram2', 'string', CreoleTypes::VARCHAR, true, 25);

		$tMap->addPrimaryKey('KODE_KEGIATAN2', 'KodeKegiatan2', 'string', CreoleTypes::VARCHAR, true, 25);

		$tMap->addColumn('NAMA_KEGIATAN2', 'NamaKegiatan2', 'string', CreoleTypes::VARCHAR, false, 300);

		$tMap->addColumn('URUT', 'Urut', 'int', CreoleTypes::INTEGER, true, null);

	} 
} 