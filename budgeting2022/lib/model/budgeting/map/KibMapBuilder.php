<?php



class KibMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.KibMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.kib');
		$tMap->setPhpName('Kib');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('ID', 'Id', 'string', CreoleTypes::VARCHAR, true, 20);

		$tMap->addColumn('TIPE_KIB', 'TipeKib', 'string', CreoleTypes::VARCHAR, false, 50);

		$tMap->addColumn('UNIT_ID', 'UnitId', 'string', CreoleTypes::VARCHAR, false, 5);

		$tMap->addColumn('KODE_LOKASI', 'KodeLokasi', 'string', CreoleTypes::VARCHAR, false, 50);

		$tMap->addColumn('NAMA_LOKASI', 'NamaLokasi', 'string', CreoleTypes::VARCHAR, false, 500);

		$tMap->addColumn('NO_REGISTER', 'NoRegister', 'string', CreoleTypes::VARCHAR, false, 50);

		$tMap->addColumn('KODE_BARANG', 'KodeBarang', 'string', CreoleTypes::VARCHAR, false, 50);

		$tMap->addColumn('NAMA_BARANG', 'NamaBarang', 'string', CreoleTypes::VARCHAR, false, 500);

		$tMap->addColumn('KONDISI', 'Kondisi', 'string', CreoleTypes::VARCHAR, false, 10);

		$tMap->addColumn('MERK', 'Merk', 'string', CreoleTypes::VARCHAR, false, 200);

		$tMap->addColumn('TIPE', 'Tipe', 'string', CreoleTypes::VARCHAR, false, 200);

		$tMap->addColumn('ALAMAT', 'Alamat', 'string', CreoleTypes::VARCHAR, false, 500);

		$tMap->addColumn('KETERANGAN', 'Keterangan', 'string', CreoleTypes::VARCHAR, false, 200);

		$tMap->addColumn('TAHUN', 'Tahun', 'string', CreoleTypes::VARCHAR, false, 5);

	} 
} 