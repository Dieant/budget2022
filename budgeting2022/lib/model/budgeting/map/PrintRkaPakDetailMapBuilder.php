<?php



class PrintRkaPakDetailMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.PrintRkaPakDetailMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.print_rka_pak_detail');
		$tMap->setPhpName('PrintRkaPakDetail');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('ID_PRINT_RKA_PAK', 'IdPrintRkaPak', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('SUBTITLE', 'Subtitle', 'string', CreoleTypes::VARCHAR, false, 300);

		$tMap->addColumn('REKENING_CODE', 'RekeningCode', 'string', CreoleTypes::VARCHAR, false, 20);

		$tMap->addColumn('SUBSUBTITLE', 'Subsubtitle', 'string', CreoleTypes::VARCHAR, false, 300);

		$tMap->addColumn('NAMA_KOMPONEN', 'NamaKomponen', 'string', CreoleTypes::VARCHAR, false, 300);

		$tMap->addColumn('SATUAN', 'Satuan', 'string', CreoleTypes::VARCHAR, false, 50);

		$tMap->addColumn('KOEFISIEN', 'Koefisien', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('HARGA', 'Harga', 'double', CreoleTypes::DOUBLE, false, null);

		$tMap->addColumn('HASIL', 'Hasil', 'double', CreoleTypes::DOUBLE, false, null);

		$tMap->addColumn('PAJAK', 'Pajak', 'int', CreoleTypes::INTEGER, false, null);

	} 
} 