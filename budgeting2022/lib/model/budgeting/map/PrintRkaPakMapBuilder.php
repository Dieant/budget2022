<?php



class PrintRkaPakMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.PrintRkaPakMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.print_rka_pak');
		$tMap->setPhpName('PrintRkaPak');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('UNIT_ID', 'UnitId', 'string', CreoleTypes::VARCHAR, false, 10);

		$tMap->addColumn('KEGIATAN_CODE', 'KegiatanCode', 'string', CreoleTypes::VARCHAR, false, 20);

		$tMap->addColumn('WAKTU', 'Waktu', 'int', CreoleTypes::TIMESTAMP, false, null);

		$tMap->addColumn('TOKEN', 'Token', 'string', CreoleTypes::VARCHAR, false, 100);

	} 
} 