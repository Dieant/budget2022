<?php



class PrintKoefisienDetailMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.PrintKoefisienDetailMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.print_koefisien_detail');
		$tMap->setPhpName('PrintKoefisienDetail');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('UNIT_ID', 'UnitId', 'string', CreoleTypes::VARCHAR, true, 20);

		$tMap->addPrimaryKey('KEGIATAN_CODE', 'KegiatanCode', 'string', CreoleTypes::VARCHAR, true, 30);

		$tMap->addPrimaryKey('PRINT_NO', 'PrintNo', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addPrimaryKey('DETAIL_NO', 'DetailNo', 'string', CreoleTypes::BIGINT, true, null);

		$tMap->addPrimaryKey('REVISI_NO', 'RevisiNo', 'int', CreoleTypes::INTEGER, true, null);

	} 
} 