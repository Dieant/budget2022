<?php



class PembandingKomponenMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.PembandingKomponenMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.pembanding_komponen');
		$tMap->setPhpName('PembandingKomponen');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('ID_PEMBANDING_KEGIATAN', 'IdPembandingKegiatan', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('DETAIL_NO', 'DetailNo', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('REKENING_CODE', 'RekeningCode', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('KOMPONEN_NAME', 'KomponenName', 'string', CreoleTypes::VARCHAR, false, 500);

		$tMap->addColumn('DETAIL_NAME', 'DetailName', 'string', CreoleTypes::VARCHAR, false, 500);

		$tMap->addColumn('VOLUME', 'Volume', 'double', CreoleTypes::DOUBLE, false, null);

		$tMap->addColumn('SATUAN', 'Satuan', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('KETERANGAN_KOEFISIEN', 'KeteranganKoefisien', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('SUBTITLE', 'Subtitle', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('NILAI_ANGGARAN', 'NilaiAnggaran', 'double', CreoleTypes::DOUBLE, false, null);

		$tMap->addColumn('KOMPONEN_HARGA', 'KomponenHarga', 'double', CreoleTypes::DOUBLE, false, null);

	} 
} 