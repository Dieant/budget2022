<?php



class MasterUser2MapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.MasterUser2MapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('master_user2');
		$tMap->setPhpName('MasterUser2');

		$tMap->setUseIdGenerator(false);

		$tMap->addColumn('USERNAME', 'Username', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('PASSWD', 'Passwd', 'string', CreoleTypes::VARCHAR, false, 20);

		$tMap->addColumn('STRLIKE', 'Strlike', 'string', CreoleTypes::VARCHAR, false, 50);

	} 
} 