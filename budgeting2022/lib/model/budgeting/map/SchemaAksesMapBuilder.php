<?php



class SchemaAksesMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.SchemaAksesMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('schema_akses');
		$tMap->setPhpName('SchemaAkses');

		$tMap->setUseIdGenerator(false);

		$tMap->addForeignPrimaryKey('USER_ID', 'UserId', 'string' , CreoleTypes::VARCHAR, 'master_user', 'USER_ID', true, 50);

		$tMap->addForeignPrimaryKey('SCHEMA_ID', 'SchemaId', 'int' , CreoleTypes::INTEGER, 'master_schema', 'SCHEMA_ID', true, null);

		$tMap->addForeignKey('LEVEL_ID', 'LevelId', 'int', CreoleTypes::INTEGER, 'user_level', 'LEVEL_ID', false, null);

	} 
} 