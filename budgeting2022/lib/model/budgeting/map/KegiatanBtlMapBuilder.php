<?php



class KegiatanBtlMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.KegiatanBtlMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.kegiatan_btl');
		$tMap->setPhpName('KegiatanBtl');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('UNIT_ID', 'UnitId', 'string', CreoleTypes::VARCHAR, true, 50);

		$tMap->addPrimaryKey('KODE_KEGIATAN', 'KodeKegiatan', 'string', CreoleTypes::VARCHAR, true, 50);

		$tMap->addPrimaryKey('REKENING_CODE', 'RekeningCode', 'string', CreoleTypes::VARCHAR, true, 100);

		$tMap->addPrimaryKey('TAHAP', 'Tahap', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('NAMA_KEGIATAN', 'NamaKegiatan', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('REKENING_NAME', 'RekeningName', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('CATATAN', 'Catatan', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('SEMULA', 'Semula', 'double', CreoleTypes::DOUBLE, false, null);

		$tMap->addColumn('MENJADI', 'Menjadi', 'double', CreoleTypes::DOUBLE, false, null);

	} 
} 