<?php



class MasterJalanMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.MasterJalanMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('master_jalan');
		$tMap->setPhpName('MasterJalan');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('KODE', 'Kode', 'string', CreoleTypes::VARCHAR, true, 16);

		$tMap->addColumn('NAMA_JALAN', 'NamaJalan', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('NAMA_UJUNG', 'NamaUjung', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('NAMA_PANGKAL', 'NamaPangkal', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('KECAMATAN', 'Kecamatan', 'string', CreoleTypes::VARCHAR, false, 30);

		$tMap->addColumn('KELURAHAN', 'Kelurahan', 'string', CreoleTypes::VARCHAR, false, 30);

	} 
} 