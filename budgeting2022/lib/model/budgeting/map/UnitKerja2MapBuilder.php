<?php



class UnitKerja2MapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.UnitKerja2MapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('unit_kerja2');
		$tMap->setPhpName('UnitKerja2');

		$tMap->setUseIdGenerator(true);

		$tMap->setPrimaryKeyMethodInfo('unit_kerja2_SEQ');

		$tMap->addColumn('UNIT_NAME', 'UnitName', 'string', CreoleTypes::VARCHAR, false, 250);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

	} 
} 