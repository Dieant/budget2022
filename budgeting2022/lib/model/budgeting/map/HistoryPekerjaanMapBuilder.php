<?php



class HistoryPekerjaanMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.HistoryPekerjaanMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('history_pekerjaan');
		$tMap->setPhpName('HistoryPekerjaan');

		$tMap->setUseIdGenerator(false);

		$tMap->addColumn('TAHUN', 'Tahun', 'int', CreoleTypes::SMALLINT, true, null);

		$tMap->addColumn('LOKASI', 'Lokasi', 'string', CreoleTypes::VARCHAR, false, 500);

		$tMap->addColumn('NILAI', 'Nilai', 'double', CreoleTypes::DOUBLE, false, null);

		$tMap->addColumn('KODE', 'Kode', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('NOMOR', 'Nomor', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('VOLUME', 'Volume', 'string', CreoleTypes::VARCHAR, false, null);

	} 
} 