<?php



class AsbFisikMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.AsbFisikMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.asb_fisik');
		$tMap->setPhpName('AsbFisik');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('KOMPONEN_ID', 'KomponenId', 'string', CreoleTypes::VARCHAR, true, 50);

		$tMap->addColumn('SATUAN', 'Satuan', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('KOMPONEN_NAME', 'KomponenName', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('SHSD_ID', 'ShsdId', 'string', CreoleTypes::VARCHAR, false, 50);

		$tMap->addColumn('KOMPONEN_HARGA', 'KomponenHarga', 'double', CreoleTypes::DOUBLE, false, null);

		$tMap->addColumn('KOMPONEN_SHOW', 'KomponenShow', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('IP_ADDRESS', 'IpAddress', 'string', CreoleTypes::VARCHAR, false, 20);

		$tMap->addColumn('WAKTU_ACCESS', 'WaktuAccess', 'int', CreoleTypes::TIMESTAMP, false, null);

		$tMap->addColumn('KOMPONEN_TIPE', 'KomponenTipe', 'string', CreoleTypes::VARCHAR, false, 10);

		$tMap->addColumn('KOMPONEN_CONFIRMED', 'KomponenConfirmed', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('KOMPONEN_NON_PAJAK', 'KomponenNonPajak', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('USER_ID', 'UserId', 'string', CreoleTypes::VARCHAR, false, 30);

		$tMap->addColumn('REKENING', 'Rekening', 'string', CreoleTypes::VARCHAR, false, 300);

		$tMap->addColumn('KELOMPOK', 'Kelompok', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('PEMELIHARAAN', 'Pemeliharaan', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('REK_UPAH', 'RekUpah', 'string', CreoleTypes::VARCHAR, false, 20);

		$tMap->addColumn('REK_BAHAN', 'RekBahan', 'string', CreoleTypes::VARCHAR, false, 20);

		$tMap->addColumn('REK_SEWA', 'RekSewa', 'string', CreoleTypes::VARCHAR, false, 20);

		$tMap->addColumn('DESKRIPSI', 'Deskripsi', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('ASB_LOCKED', 'AsbLocked', 'boolean', CreoleTypes::BOOLEAN, false, null);

	} 
} 