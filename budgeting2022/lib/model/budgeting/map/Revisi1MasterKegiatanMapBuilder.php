<?php



class Revisi1MasterKegiatanMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.Revisi1MasterKegiatanMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.revisi1_master_kegiatan');
		$tMap->setPhpName('Revisi1MasterKegiatan');

		$tMap->setUseIdGenerator(true);

		$tMap->setPrimaryKeyMethodInfo('ebudget.revisi1_master_ke_SEQ_32');

		$tMap->addPrimaryKey('UNIT_ID', 'UnitId', 'string', CreoleTypes::VARCHAR, true, 10);

		$tMap->addPrimaryKey('KODE_KEGIATAN', 'KodeKegiatan', 'string', CreoleTypes::VARCHAR, true, 20);

		$tMap->addColumn('KODE_BIDANG', 'KodeBidang', 'string', CreoleTypes::VARCHAR, false, 2);

		$tMap->addColumn('KODE_URUSAN_WAJIB', 'KodeUrusanWajib', 'string', CreoleTypes::VARCHAR, false, 2);

		$tMap->addColumn('KODE_PROGRAM', 'KodeProgram', 'string', CreoleTypes::VARCHAR, false, 2);

		$tMap->addColumn('KODE_SASARAN', 'KodeSasaran', 'string', CreoleTypes::VARCHAR, false, 5);

		$tMap->addColumn('KODE_INDIKATOR', 'KodeIndikator', 'string', CreoleTypes::VARCHAR, false, 2);

		$tMap->addColumn('ALOKASI_DANA', 'AlokasiDana', 'double', CreoleTypes::DOUBLE, false, null);

		$tMap->addColumn('NAMA_KEGIATAN', 'NamaKegiatan', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('MASUKAN', 'Masukan', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('OUTPUT', 'Output', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('OUTCOME', 'Outcome', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('BENEFIT', 'Benefit', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('IMPACT', 'Impact', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('TIPE', 'Tipe', 'string', CreoleTypes::VARCHAR, false, 10);

		$tMap->addColumn('KEGIATAN_ACTIVE', 'KegiatanActive', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('TO_KEGIATAN_CODE', 'ToKegiatanCode', 'string', CreoleTypes::VARCHAR, false, 12);

		$tMap->addColumn('CATATAN', 'Catatan', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('TARGET_OUTCOME', 'TargetOutcome', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('LOKASI', 'Lokasi', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('JUMLAH_PREV', 'JumlahPrev', 'double', CreoleTypes::DOUBLE, false, null);

		$tMap->addColumn('JUMLAH_NOW', 'JumlahNow', 'double', CreoleTypes::DOUBLE, false, null);

		$tMap->addColumn('JUMLAH_NEXT', 'JumlahNext', 'double', CreoleTypes::DOUBLE, false, null);

		$tMap->addColumn('KODE_PROGRAM2', 'KodeProgram2', 'string', CreoleTypes::VARCHAR, false, 30);

		$tMap->addColumn('KODE_URUSAN', 'KodeUrusan', 'string', CreoleTypes::VARCHAR, false, 10);

		$tMap->addColumn('LAST_UPDATE_USER', 'LastUpdateUser', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('LAST_UPDATE_TIME', 'LastUpdateTime', 'int', CreoleTypes::TIMESTAMP, false, null);

		$tMap->addColumn('LAST_UPDATE_IP', 'LastUpdateIp', 'string', CreoleTypes::VARCHAR, false, 20);

		$tMap->addColumn('TAHAP', 'Tahap', 'string', CreoleTypes::VARCHAR, false, 20);

		$tMap->addColumn('KODE_MISI', 'KodeMisi', 'string', CreoleTypes::VARCHAR, false, 2);

		$tMap->addColumn('KODE_TUJUAN', 'KodeTujuan', 'string', CreoleTypes::VARCHAR, false, 2);

		$tMap->addColumn('RANKING', 'Ranking', 'int', CreoleTypes::SMALLINT, false, null);

		$tMap->addColumn('NOMOR13', 'Nomor13', 'string', CreoleTypes::VARCHAR, false, 4);

		$tMap->addColumn('PPA_NAMA', 'PpaNama', 'string', CreoleTypes::VARCHAR, false, 200);

		$tMap->addColumn('PPA_PANGKAT', 'PpaPangkat', 'string', CreoleTypes::VARCHAR, false, 200);

		$tMap->addColumn('PPA_NIP', 'PpaNip', 'string', CreoleTypes::VARCHAR, false, 30);

		$tMap->addColumn('LANJUTAN', 'Lanjutan', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('USER_ID', 'UserId', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('TAHUN', 'Tahun', 'string', CreoleTypes::VARCHAR, false, 5);

		$tMap->addColumn('TAMBAHAN_PAGU', 'TambahanPagu', 'double', CreoleTypes::DOUBLE, false, null);

		$tMap->addColumn('GENDER', 'Gender', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('KODE_KEG_KEUANGAN', 'KodeKegKeuangan', 'string', CreoleTypes::VARCHAR, false, 15);

		$tMap->addColumn('INDIKATOR', 'Indikator', 'string', CreoleTypes::VARCHAR, false, 300);

		$tMap->addColumn('IS_DAK', 'IsDak', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('KODE_KEGIATAN_ASAL', 'KodeKegiatanAsal', 'string', CreoleTypes::VARCHAR, false, 20);

		$tMap->addColumn('KODE_KEG_KEUANGAN_ASAL', 'KodeKegKeuanganAsal', 'string', CreoleTypes::VARCHAR, false, 20);

		$tMap->addColumn('TH_KE_MULTIYEARS', 'ThKeMultiyears', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('KELOMPOK_SASARAN', 'KelompokSasaran', 'string', CreoleTypes::VARCHAR, false, 500);

		$tMap->addColumn('PAGU_BAPPEKO', 'PaguBappeko', 'double', CreoleTypes::DOUBLE, false, null);

		$tMap->addColumn('KODE_DPA', 'KodeDpa', 'string', CreoleTypes::VARCHAR, false, 500);

		$tMap->addColumn('USER_ID_PPTK', 'UserIdPptk', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('USER_ID_KPA', 'UserIdKpa', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('CATATAN_PEMBAHASAN', 'CatatanPembahasan', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('CATATAN_PENYELIA', 'CatatanPenyelia', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('CATATAN_BAPPEKO', 'CatatanBappeko', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('STATUS_LEVEL', 'StatusLevel', 'int', CreoleTypes::SMALLINT, false, null);

		$tMap->addColumn('IS_TAPD_SETUJU', 'IsTapdSetuju', 'boolean', CreoleTypes::BOOLEAN, true, null);

		$tMap->addColumn('IS_BAPPEKO_SETUJU', 'IsBappekoSetuju', 'boolean', CreoleTypes::BOOLEAN, true, null);

		$tMap->addColumn('IS_PENYELIA_SETUJU', 'IsPenyeliaSetuju', 'boolean', CreoleTypes::BOOLEAN, true, null);

		$tMap->addColumn('IS_PERNAH_RKA', 'IsPernahRka', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('KODE_KEGIATAN_BARU', 'KodeKegiatanBaru', 'string', CreoleTypes::VARCHAR, false, 20);

		$tMap->addColumn('CATATAN_BPKPD', 'CatatanBpkpd', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('UBAH_F1_DINAS', 'UbahF1Dinas', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('UBAH_F1_PENELITI', 'UbahF1Peneliti', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('SISA_LELANG_DINAS', 'SisaLelangDinas', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('SISA_LELANG_PENELITI', 'SisaLelangPeneliti', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('CATATAN_UBAH_F1_DINAS', 'CatatanUbahF1Dinas', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('CATATAN_SISA_LELANG_PENELITI', 'CatatanSisaLelangPeneliti', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('PPTK_APPROVAL', 'PptkApproval', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('KPA_APPROVAL', 'KpaApproval', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('CATATAN_BAGIAN_HUKUM', 'CatatanBagianHukum', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('CATATAN_INSPEKTORAT', 'CatatanInspektorat', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('CATATAN_BADAN_KEPEGAWAIAN', 'CatatanBadanKepegawaian', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('CATATAN_LPPA', 'CatatanLppa', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('IS_BAGIAN_HUKUM_SETUJU', 'IsBagianHukumSetuju', 'boolean', CreoleTypes::BOOLEAN, true, null);

		$tMap->addColumn('IS_INSPEKTORAT_SETUJU', 'IsInspektoratSetuju', 'boolean', CreoleTypes::BOOLEAN, true, null);

		$tMap->addColumn('IS_BADAN_KEPEGAWAIAN_SETUJU', 'IsBadanKepegawaianSetuju', 'boolean', CreoleTypes::BOOLEAN, true, null);

		$tMap->addColumn('IS_LPPA_SETUJU', 'IsLppaSetuju', 'boolean', CreoleTypes::BOOLEAN, true, null);

		$tMap->addColumn('VERIFIKASI_BPKPD', 'VerifikasiBpkpd', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('VERIFIKASI_BAPPEKO', 'VerifikasiBappeko', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('VERIFIKASI_PENYELIA', 'VerifikasiPenyelia', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('VERIFIKASI_BAGIAN_HUKUM', 'VerifikasiBagianHukum', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('VERIFIKASI_INSPEKTORAT', 'VerifikasiInspektorat', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('VERIFIKASI_BADAN_KEPEGAWAIAN', 'VerifikasiBadanKepegawaian', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('VERIFIKASI_LPPA', 'VerifikasiLppa', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('METODE_COUNT', 'MetodeCount', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('CATATAN_BAGIAN_ORGANISASI', 'CatatanBagianOrganisasi', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('IS_BAGIAN_ORGANISASI_SETUJU', 'IsBagianOrganisasiSetuju', 'boolean', CreoleTypes::BOOLEAN, true, null);

		$tMap->addColumn('VERIFIKASI_BAGIAN_ORGANISASI', 'VerifikasiBagianOrganisasi', 'string', CreoleTypes::VARCHAR, false, null);

	} 
} 