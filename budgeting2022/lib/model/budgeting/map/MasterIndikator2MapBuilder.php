<?php



class MasterIndikator2MapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.MasterIndikator2MapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.master_indikator2');
		$tMap->setPhpName('MasterIndikator2');

		$tMap->setUseIdGenerator(true);

		$tMap->setPrimaryKeyMethodInfo('ebudget.master_indikator2_SEQ');

		$tMap->addPrimaryKey('KODE_PROGRAM2', 'KodeProgram2', 'string', CreoleTypes::VARCHAR, true, 25);

		$tMap->addPrimaryKey('PK_INDIKATOR', 'PkIndikator', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('INDIKATOR', 'Indikator', 'string', CreoleTypes::VARCHAR, false, 200);

		$tMap->addColumn('TARGET', 'Target', 'string', CreoleTypes::VARCHAR, false, 500);

		$tMap->addColumn('KODE_PROGRAM_INDIKATOR', 'KodeProgramIndikator', 'string', CreoleTypes::VARCHAR, false, 20);

	} 
} 