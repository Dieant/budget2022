<?php



class KegiatanNilaiMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.KegiatanNilaiMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.kegiatan_nilai');
		$tMap->setPhpName('KegiatanNilai');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('UNIT_ID', 'UnitId', 'string', CreoleTypes::VARCHAR, true, 10);

		$tMap->addColumn('NILAI_1', 'Nilai1', 'int', CreoleTypes::SMALLINT, false, null);

		$tMap->addColumn('NILAI_2', 'Nilai2', 'int', CreoleTypes::SMALLINT, false, null);

		$tMap->addColumn('NILAI_3', 'Nilai3', 'int', CreoleTypes::SMALLINT, false, null);

		$tMap->addColumn('TOTAL', 'Total', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('KETERANGAN', 'Keterangan', 'string', CreoleTypes::VARCHAR, false, 300);

		$tMap->addPrimaryKey('KEGIATAN_CODE', 'KegiatanCode', 'string', CreoleTypes::VARCHAR, true, 20);

		$tMap->addColumn('PRIORITAS', 'Prioritas', 'string', CreoleTypes::VARCHAR, false, 300);

	} 
} 