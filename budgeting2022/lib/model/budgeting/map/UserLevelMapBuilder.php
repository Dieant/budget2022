<?php



class UserLevelMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.UserLevelMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('user_level');
		$tMap->setPhpName('UserLevel');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('LEVEL_ID', 'LevelId', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('LEVEL_NAME', 'LevelName', 'string', CreoleTypes::VARCHAR, false, 100);

	} 
} 