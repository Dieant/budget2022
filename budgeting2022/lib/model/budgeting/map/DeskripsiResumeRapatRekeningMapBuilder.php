<?php



class DeskripsiResumeRapatRekeningMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.DeskripsiResumeRapatRekeningMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.deskripsi_resume_rapat_rekening');
		$tMap->setPhpName('DeskripsiResumeRapatRekening');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('ID_DESKRIPSI_RESUME_RAPAT', 'IdDeskripsiResumeRapat', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addPrimaryKey('KEGIATAN_CODE', 'KegiatanCode', 'string', CreoleTypes::VARCHAR, true, 100);

		$tMap->addPrimaryKey('REKENING_CODE', 'RekeningCode', 'string', CreoleTypes::VARCHAR, true, 100);

		$tMap->addColumn('REKENING_NAME', 'RekeningName', 'string', CreoleTypes::VARCHAR, false, 500);

		$tMap->addColumn('SEMULA', 'Semula', 'double', CreoleTypes::DOUBLE, false, null);

		$tMap->addColumn('MENJADI', 'Menjadi', 'double', CreoleTypes::DOUBLE, false, null);

	} 
} 