<?php



class UserHandleAsliMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.UserHandleAsliMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('user_handle_asli');
		$tMap->setPhpName('UserHandleAsli');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('USER_ID', 'UserId', 'string', CreoleTypes::VARCHAR, true, 20);

		$tMap->addPrimaryKey('SCHEMA_ID', 'SchemaId', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addPrimaryKey('UNIT_ID', 'UnitId', 'string', CreoleTypes::VARCHAR, true, 10);

	} 
} 