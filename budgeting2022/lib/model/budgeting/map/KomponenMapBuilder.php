<?php



class KomponenMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.KomponenMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.komponen');
		$tMap->setPhpName('Komponen');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('KOMPONEN_ID', 'KomponenId', 'string', CreoleTypes::VARCHAR, true, 50);

		$tMap->addColumn('SATUAN', 'Satuan', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('KOMPONEN_NAME', 'KomponenName', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('SHSD_ID', 'ShsdId', 'string', CreoleTypes::VARCHAR, false, 50);

		$tMap->addColumn('KOMPONEN_HARGA', 'KomponenHarga', 'double', CreoleTypes::DOUBLE, false, null);

		$tMap->addColumn('KOMPONEN_SHOW', 'KomponenShow', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('IP_ADDRESS', 'IpAddress', 'string', CreoleTypes::VARCHAR, false, 20);

		$tMap->addColumn('WAKTU_ACCESS', 'WaktuAccess', 'int', CreoleTypes::TIMESTAMP, false, null);

		$tMap->addColumn('KOMPONEN_TIPE', 'KomponenTipe', 'string', CreoleTypes::VARCHAR, false, 10);

		$tMap->addColumn('KOMPONEN_CONFIRMED', 'KomponenConfirmed', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('KOMPONEN_NON_PAJAK', 'KomponenNonPajak', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('USER_ID', 'UserId', 'string', CreoleTypes::VARCHAR, false, 30);

		$tMap->addColumn('REKENING', 'Rekening', 'string', CreoleTypes::VARCHAR, false, 300);

		$tMap->addColumn('KELOMPOK', 'Kelompok', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('PEMELIHARAAN', 'Pemeliharaan', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('REK_UPAH', 'RekUpah', 'string', CreoleTypes::VARCHAR, false, 20);

		$tMap->addColumn('REK_BAHAN', 'RekBahan', 'string', CreoleTypes::VARCHAR, false, 20);

		$tMap->addColumn('REK_SEWA', 'RekSewa', 'string', CreoleTypes::VARCHAR, false, 20);

		$tMap->addColumn('DESKRIPSI', 'Deskripsi', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('STATUS_MASUK', 'StatusMasuk', 'string', CreoleTypes::VARCHAR, false, 10);

		$tMap->addColumn('RKA_MEMBER', 'RkaMember', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('MAINTENANCE', 'Maintenance', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('IS_POTONG_BPJS', 'IsPotongBpjs', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('IS_IURAN_BPJS', 'IsIuranBpjs', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('USULAN_SKPD', 'UsulanSkpd', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('TAHAP', 'Tahap', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('IS_EST_FISIK', 'IsEstFisik', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('AKRUAL_CODE', 'AkrualCode', 'string', CreoleTypes::VARCHAR, false, 50);

		$tMap->addColumn('KODE_AKRUAL_KOMPONEN_PENYUSUN', 'KodeAkrualKomponenPenyusun', 'string', CreoleTypes::VARCHAR, false, 10);

		$tMap->addColumn('KOMPONEN_TIPE2', 'KomponenTipe2', 'string', CreoleTypes::VARCHAR, false, 50);

		$tMap->addColumn('IS_SURVEY_BP', 'IsSurveyBp', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('KOMPONEN_HARGA_BULAT', 'KomponenHargaBulat', 'double', CreoleTypes::DOUBLE, false, null);

		$tMap->addColumn('IS_IURAN_JKN', 'IsIuranJkn', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('IS_IURAN_JKK', 'IsIuranJkk', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('IS_IURAN_JK', 'IsIuranJk', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('IS_NARSUM', 'IsNarsum', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('IS_RAB', 'IsRab', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('IS_BBM', 'IsBbm', 'boolean', CreoleTypes::BOOLEAN, false, null);

	} 
} 