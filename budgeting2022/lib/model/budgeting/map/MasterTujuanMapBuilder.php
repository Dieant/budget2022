<?php



class MasterTujuanMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.MasterTujuanMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.master_tujuan');
		$tMap->setPhpName('MasterTujuan');

		$tMap->setUseIdGenerator(true);

		$tMap->setPrimaryKeyMethodInfo('ebudget.master_tujuan_SEQ');

		$tMap->addColumn('KODE_TUJUAN', 'KodeTujuan', 'string', CreoleTypes::VARCHAR, true, 2);

		$tMap->addColumn('NAMA_TUJUAN', 'NamaTujuan', 'string', CreoleTypes::VARCHAR, false, 200);

		$tMap->addColumn('KODE_MISI', 'KodeMisi', 'string', CreoleTypes::VARCHAR, false, 2);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

	} 
} 