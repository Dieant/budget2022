<?php



class MasterProgramSasaranMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.MasterProgramSasaranMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.master_program_sasaran');
		$tMap->setPhpName('MasterProgramSasaran');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('KODE_SASARAN_INDIKATOR', 'KodeSasaranIndikator', 'string', CreoleTypes::VARCHAR, true, 20);

		$tMap->addPrimaryKey('KODE_PROGRAM_INDIKATOR', 'KodeProgramIndikator', 'string', CreoleTypes::VARCHAR, true, 20);

	} 
} 