<?php



class AkrualKomponenPenyusunanMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.AkrualKomponenPenyusunanMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.akrual_komponen_penyusunan');
		$tMap->setPhpName('AkrualKomponenPenyusunan');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('KODE_AKRUAL_KOMPONEN_PENYUSUN', 'KodeAkrualKomponenPenyusun', 'string', CreoleTypes::VARCHAR, true, 10);

		$tMap->addColumn('NAMA', 'Nama', 'string', CreoleTypes::VARCHAR, false, 100);

	} 
} 