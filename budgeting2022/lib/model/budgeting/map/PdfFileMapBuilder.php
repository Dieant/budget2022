<?php



class PdfFileMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.PdfFileMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.pdf_file');
		$tMap->setPhpName('PdfFile');

		$tMap->setUseIdGenerator(true);

		$tMap->setPrimaryKeyMethodInfo('ebudget.pdf_file_SEQ');

		$tMap->addPrimaryKey('PDF_ID', 'PdfId', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('UNIT_ID', 'UnitId', 'string', CreoleTypes::VARCHAR, true, 20);

		$tMap->addColumn('KEGIATAN_CODE', 'KegiatanCode', 'string', CreoleTypes::VARCHAR, true, 30);

		$tMap->addColumn('SUB_ID', 'SubId', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('SUBTITLE', 'Subtitle', 'string', CreoleTypes::VARCHAR, false, 300);

		$tMap->addColumn('NAMA_ASLI', 'NamaAsli', 'string', CreoleTypes::VARCHAR, false, 200);

		$tMap->addColumn('NAMA_ALIAS', 'NamaAlias', 'string', CreoleTypes::VARCHAR, false, 200);

		$tMap->addColumn('HASH', 'Hash', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('USER_ID', 'UserId', 'string', CreoleTypes::VARCHAR, false, 150);

	} 
} 