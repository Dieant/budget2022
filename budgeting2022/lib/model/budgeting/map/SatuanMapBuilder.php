<?php



class SatuanMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.SatuanMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.satuan');
		$tMap->setPhpName('Satuan');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('SATUAN_ID', 'SatuanId', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('USER_ID', 'UserId', 'string', CreoleTypes::VARCHAR, false, 20);

		$tMap->addColumn('SATUAN_NAME', 'SatuanName', 'string', CreoleTypes::VARCHAR, false, 50);

		$tMap->addColumn('IP_ADDRESS', 'IpAddress', 'string', CreoleTypes::VARCHAR, false, 20);

		$tMap->addColumn('WAKTU_ACCESS', 'WaktuAccess', 'int', CreoleTypes::TIMESTAMP, false, null);

	} 
} 