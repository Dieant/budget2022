<?php



class KomponenDikunciMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.KomponenDikunciMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.komponen_dikunci');
		$tMap->setPhpName('KomponenDikunci');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('UNIT_ID', 'UnitId', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('KEGIATAN_CODE', 'KegiatanCode', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('KOMPONEN_ID', 'KomponenId', 'string', CreoleTypes::LONGVARCHAR, false, null);

	} 
} 