<?php



class MisiPdMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.MisiPdMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.misi_pd');
		$tMap->setPhpName('MisiPd');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('UNIT_ID', 'UnitId', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('KODE_KEGIATAN', 'KodeKegiatan', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('ID_MISI', 'IdMisi', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('MISI', 'Misi', 'string', CreoleTypes::LONGVARCHAR, false, null);

	} 
} 