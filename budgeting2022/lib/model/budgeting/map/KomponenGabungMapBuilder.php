<?php



class KomponenGabungMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.KomponenGabungMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.komponen_gabung');
		$tMap->setPhpName('KomponenGabung');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('KEGIATAN_CODE', 'KegiatanCode', 'string', CreoleTypes::VARCHAR, true, 20);

		$tMap->addPrimaryKey('DETAIL_NO', 'DetailNo', 'int', CreoleTypes::SMALLINT, true, null);

		$tMap->addColumn('REKENING_CODE', 'RekeningCode', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('KOMPONEN_ID', 'KomponenId', 'string', CreoleTypes::VARCHAR, false, 50);

		$tMap->addColumn('DETAIL_NAME', 'DetailName', 'string', CreoleTypes::VARCHAR, false, 150);

		$tMap->addColumn('VOLUME', 'Volume', 'double', CreoleTypes::DOUBLE, false, null);

		$tMap->addColumn('KETERANGAN_KOEFISIEN', 'KeteranganKoefisien', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('SUBTITLE', 'Subtitle', 'string', CreoleTypes::VARCHAR, false, 300);

		$tMap->addColumn('KOMPONEN_HARGA', 'KomponenHarga', 'double', CreoleTypes::DOUBLE, false, null);

		$tMap->addColumn('KOMPONEN_HARGA_AWAL', 'KomponenHargaAwal', 'double', CreoleTypes::DOUBLE, false, null);

		$tMap->addColumn('KOMPONEN_NAME', 'KomponenName', 'string', CreoleTypes::VARCHAR, false, 500);

		$tMap->addColumn('SATUAN', 'Satuan', 'string', CreoleTypes::VARCHAR, false, 50);

		$tMap->addColumn('PAJAK', 'Pajak', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addPrimaryKey('UNIT_ID', 'UnitId', 'string', CreoleTypes::VARCHAR, true, 8);

		$tMap->addColumn('FOR_DETAIL_NO', 'ForDetailNo', 'int', CreoleTypes::INTEGER, false, null);

	} 
} 