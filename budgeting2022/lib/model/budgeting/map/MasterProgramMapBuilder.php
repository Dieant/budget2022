<?php



class MasterProgramMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.MasterProgramMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.master_program');
		$tMap->setPhpName('MasterProgram');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('KODE_PROGRAM', 'KodeProgram', 'string', CreoleTypes::CHAR, true, 2);

		$tMap->addColumn('NAMA_PROGRAM', 'NamaProgram', 'string', CreoleTypes::VARCHAR, false, 200);

		$tMap->addColumn('BAPPEKO', 'Bappeko', 'int', CreoleTypes::SMALLINT, false, null);

		$tMap->addColumn('KODE_TUJUAN', 'KodeTujuan', 'string', CreoleTypes::VARCHAR, false, 10);

		$tMap->addColumn('KODE_LUTFI', 'KodeLutfi', 'string', CreoleTypes::VARCHAR, false, 2);

		$tMap->addColumn('INDIKATOR', 'Indikator', 'string', CreoleTypes::VARCHAR, false, null);

	} 
} 