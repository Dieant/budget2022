<?php



class Revisi5RincianMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.Revisi5RincianMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.revisi5_rincian');
		$tMap->setPhpName('Revisi5Rincian');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('KEGIATAN_CODE', 'KegiatanCode', 'string', CreoleTypes::VARCHAR, true, 20);

		$tMap->addPrimaryKey('TIPE', 'Tipe', 'string', CreoleTypes::VARCHAR, true, 5);

		$tMap->addColumn('RINCIAN_CONFIRMED', 'RincianConfirmed', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('RINCIAN_CHANGED', 'RincianChanged', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('RINCIAN_SELESAI', 'RincianSelesai', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('IP_ADDRESS', 'IpAddress', 'string', CreoleTypes::VARCHAR, false, 20);

		$tMap->addColumn('WAKTU_ACCESS', 'WaktuAccess', 'int', CreoleTypes::TIMESTAMP, false, null);

		$tMap->addColumn('TARGET', 'Target', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addPrimaryKey('UNIT_ID', 'UnitId', 'string', CreoleTypes::VARCHAR, true, 8);

		$tMap->addColumn('RINCIAN_LEVEL', 'RincianLevel', 'int', CreoleTypes::SMALLINT, false, null);

		$tMap->addColumn('LOCK', 'Lock', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('LAST_UPDATE_USER', 'LastUpdateUser', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('LAST_UPDATE_TIME', 'LastUpdateTime', 'int', CreoleTypes::TIMESTAMP, false, null);

		$tMap->addColumn('LAST_UPDATE_IP', 'LastUpdateIp', 'string', CreoleTypes::VARCHAR, false, 20);

		$tMap->addColumn('TAHAP', 'Tahap', 'string', CreoleTypes::VARCHAR, false, 20);

		$tMap->addColumn('TAHUN', 'Tahun', 'string', CreoleTypes::VARCHAR, false, 5);

	} 
} 