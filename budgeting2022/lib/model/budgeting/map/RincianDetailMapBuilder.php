<?php



class RincianDetailMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.RincianDetailMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.rincian_detail');
		$tMap->setPhpName('RincianDetail');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('KEGIATAN_CODE', 'KegiatanCode', 'string', CreoleTypes::VARCHAR, true, 20);

		$tMap->addColumn('TIPE', 'Tipe', 'string', CreoleTypes::VARCHAR, true, 5);

		$tMap->addPrimaryKey('DETAIL_NO', 'DetailNo', 'int', CreoleTypes::SMALLINT, true, null);

		$tMap->addColumn('REKENING_CODE', 'RekeningCode', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('KOMPONEN_ID', 'KomponenId', 'string', CreoleTypes::VARCHAR, false, 50);

		$tMap->addColumn('DETAIL_NAME', 'DetailName', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('VOLUME', 'Volume', 'double', CreoleTypes::DOUBLE, false, null);

		$tMap->addColumn('KETERANGAN_KOEFISIEN', 'KeteranganKoefisien', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('SUBTITLE', 'Subtitle', 'string', CreoleTypes::VARCHAR, false, 300);

		$tMap->addColumn('KOMPONEN_HARGA', 'KomponenHarga', 'double', CreoleTypes::DOUBLE, false, null);

		$tMap->addColumn('KOMPONEN_HARGA_AWAL', 'KomponenHargaAwal', 'double', CreoleTypes::DOUBLE, false, null);

		$tMap->addColumn('KOMPONEN_NAME', 'KomponenName', 'string', CreoleTypes::VARCHAR, false, 500);

		$tMap->addColumn('SATUAN', 'Satuan', 'string', CreoleTypes::VARCHAR, false, 50);

		$tMap->addColumn('PAJAK', 'Pajak', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addPrimaryKey('UNIT_ID', 'UnitId', 'string', CreoleTypes::VARCHAR, true, 8);

		$tMap->addColumn('FROM_SUB_KEGIATAN', 'FromSubKegiatan', 'string', CreoleTypes::VARCHAR, false, 50);

		$tMap->addColumn('SUB', 'Sub', 'string', CreoleTypes::VARCHAR, false, 500);

		$tMap->addColumn('KODE_SUB', 'KodeSub', 'string', CreoleTypes::VARCHAR, false, 30);

		$tMap->addColumn('LAST_UPDATE_USER', 'LastUpdateUser', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('LAST_UPDATE_TIME', 'LastUpdateTime', 'int', CreoleTypes::TIMESTAMP, false, null);

		$tMap->addColumn('LAST_UPDATE_IP', 'LastUpdateIp', 'string', CreoleTypes::VARCHAR, false, 20);

		$tMap->addColumn('TAHAP', 'Tahap', 'string', CreoleTypes::VARCHAR, false, 20);

		$tMap->addColumn('TAHAP_EDIT', 'TahapEdit', 'string', CreoleTypes::VARCHAR, false, 20);

		$tMap->addColumn('TAHAP_NEW', 'TahapNew', 'string', CreoleTypes::VARCHAR, false, 20);

		$tMap->addColumn('STATUS_LELANG', 'StatusLelang', 'string', CreoleTypes::VARCHAR, false, 30);

		$tMap->addColumn('NOMOR_LELANG', 'NomorLelang', 'string', CreoleTypes::VARCHAR, false, 300);

		$tMap->addColumn('KOEFISIEN_SEMULA', 'KoefisienSemula', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('VOLUME_SEMULA', 'VolumeSemula', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('HARGA_SEMULA', 'HargaSemula', 'double', CreoleTypes::DOUBLE, false, null);

		$tMap->addColumn('TOTAL_SEMULA', 'TotalSemula', 'double', CreoleTypes::DOUBLE, false, null);

		$tMap->addColumn('LOCK_SUBTITLE', 'LockSubtitle', 'string', CreoleTypes::VARCHAR, false, 5);

		$tMap->addColumn('STATUS_HAPUS', 'StatusHapus', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('TAHUN', 'Tahun', 'string', CreoleTypes::VARCHAR, false, 5);

		$tMap->addColumn('KODE_LOKASI', 'KodeLokasi', 'string', CreoleTypes::VARCHAR, false, 20);

		$tMap->addColumn('KECAMATAN', 'Kecamatan', 'string', CreoleTypes::VARCHAR, false, 150);

		$tMap->addColumn('REKENING_CODE_ASLI', 'RekeningCodeAsli', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('NOTE_SKPD', 'NoteSkpd', 'string', CreoleTypes::VARCHAR, false, 300);

		$tMap->addColumn('NOTE_PENELITI', 'NotePeneliti', 'string', CreoleTypes::VARCHAR, false, 300);

		$tMap->addColumn('NILAI_ANGGARAN', 'NilaiAnggaran', 'double', CreoleTypes::DOUBLE, false, null);

		$tMap->addColumn('IS_BLUD', 'IsBlud', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('IS_KAPITASI_BPJS', 'IsKapitasiBpjs', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('LOKASI_KECAMATAN', 'LokasiKecamatan', 'string', CreoleTypes::VARCHAR, false, 500);

		$tMap->addColumn('LOKASI_KELURAHAN', 'LokasiKelurahan', 'string', CreoleTypes::VARCHAR, false, 500);

		$tMap->addColumn('OB', 'Ob', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('OB_FROM_ID', 'ObFromId', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('IS_PER_KOMPONEN', 'IsPerKomponen', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('KEGIATAN_CODE_ASAL', 'KegiatanCodeAsal', 'string', CreoleTypes::VARCHAR, false, 20);

		$tMap->addColumn('TH_KE_MULTIYEARS', 'ThKeMultiyears', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('HARGA_SEBELUM_SISA_LELANG', 'HargaSebelumSisaLelang', 'double', CreoleTypes::DOUBLE, false, null);

		$tMap->addColumn('IS_MUSRENBANG', 'IsMusrenbang', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('SUB_ID_ASAL', 'SubIdAsal', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('SUBTITLE_ASAL', 'SubtitleAsal', 'string', CreoleTypes::VARCHAR, false, 300);

		$tMap->addColumn('KODE_SUB_ASAL', 'KodeSubAsal', 'string', CreoleTypes::VARCHAR, false, 30);

		$tMap->addColumn('SUB_ASAL', 'SubAsal', 'string', CreoleTypes::VARCHAR, false, 500);

		$tMap->addColumn('LAST_EDIT_TIME', 'LastEditTime', 'int', CreoleTypes::TIMESTAMP, false, null);

		$tMap->addColumn('IS_POTONG_BPJS', 'IsPotongBpjs', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('IS_IURAN_BPJS', 'IsIuranBpjs', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('STATUS_OB', 'StatusOb', 'int', CreoleTypes::SMALLINT, false, null);

		$tMap->addColumn('OB_PARENT', 'ObParent', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('OB_ALOKASI_BARU', 'ObAlokasiBaru', 'double', CreoleTypes::DOUBLE, false, null);

		$tMap->addColumn('IS_HIBAH', 'IsHibah', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('AKRUAL_CODE', 'AkrualCode', 'string', CreoleTypes::VARCHAR, false, 50);

		$tMap->addColumn('TIPE2', 'Tipe2', 'string', CreoleTypes::VARCHAR, false, 20);

		$tMap->addColumn('STATUS_LEVEL', 'StatusLevel', 'int', CreoleTypes::SMALLINT, false, null);

		$tMap->addColumn('STATUS_LEVEL_TOLAK', 'StatusLevelTolak', 'int', CreoleTypes::SMALLINT, false, null);

		$tMap->addColumn('STATUS_SISIPAN', 'StatusSisipan', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('IS_TAPD_SETUJU', 'IsTapdSetuju', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('IS_BAPPEKO_SETUJU', 'IsBappekoSetuju', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('IS_PENYELIA_SETUJU', 'IsPenyeliaSetuju', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('NOTE_TAPD', 'NoteTapd', 'string', CreoleTypes::VARCHAR, false, 300);

		$tMap->addColumn('NOTE_BAPPEKO', 'NoteBappeko', 'string', CreoleTypes::VARCHAR, false, 300);

		$tMap->addColumn('SATUAN_SEMULA', 'SatuanSemula', 'string', CreoleTypes::VARCHAR, false, 50);

		$tMap->addColumn('ID_LOKASI', 'IdLokasi', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('DETAIL_KEGIATAN', 'DetailKegiatan', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('DETAIL_KEGIATAN_SEMULA', 'DetailKegiatanSemula', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('STATUS_KOMPONEN_BARU', 'StatusKomponenBaru', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('STATUS_KOMPONEN_BERUBAH', 'StatusKomponenBerubah', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('APPROVE_UNLOCK_HARGA', 'ApproveUnlockHarga', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('TIPE_LELANG', 'TipeLelang', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('IS_HPSP', 'IsHpsp', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('IS_DAK', 'IsDak', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('IS_BOS', 'IsBos', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('IS_BOBDA', 'IsBobda', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('IS_NARSUM', 'IsNarsum', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('IS_BAGIAN_HUKUM_SETUJU', 'IsBagianHukumSetuju', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('IS_INSPEKTORAT_SETUJU', 'IsInspektoratSetuju', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('IS_BADAN_KEPEGAWAIAN_SETUJU', 'IsBadanKepegawaianSetuju', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('IS_LPPA_SETUJU', 'IsLppaSetuju', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('PRIORITAS_WALI', 'PrioritasWali', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('IS_OUTPUT', 'IsOutput', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('IS_BAGIAN_ORGANISASI_SETUJU', 'IsBagianOrganisasiSetuju', 'boolean', CreoleTypes::BOOLEAN, true, null);

		$tMap->addColumn('IS_ASISTEN1_SETUJU', 'IsAsisten1Setuju', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('IS_ASISTEN2_SETUJU', 'IsAsisten2Setuju', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('IS_ASISTEN3_SETUJU', 'IsAsisten3Setuju', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('IS_SEKDA_SETUJU', 'IsSekdaSetuju', 'boolean', CreoleTypes::BOOLEAN, false, null);

	} 
} 