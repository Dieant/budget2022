<?php



class UsulanSSHMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.UsulanSSHMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.usulan_ssh');
		$tMap->setPhpName('UsulanSSH');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('ID_USULAN', 'IdUsulan', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('CREATED_AT', 'CreatedAt', 'int', CreoleTypes::TIMESTAMP, false, null);

		$tMap->addColumn('UPDATED_AT', 'UpdatedAt', 'int', CreoleTypes::TIMESTAMP, false, null);

		$tMap->addColumn('NAMA', 'Nama', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('SPEC', 'Spec', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('HIDDEN_SPEC', 'HiddenSpec', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('MEREK', 'Merek', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('SATUAN', 'Satuan', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('HARGA', 'Harga', 'double', CreoleTypes::DOUBLE, false, null);

		$tMap->addColumn('REKENING', 'Rekening', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('PAJAK', 'Pajak', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('KETERANGAN', 'Keterangan', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('KODE_BARANG', 'KodeBarang', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('ID_SPJM', 'IdSpjm', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('STATUS_VERIFIKASI', 'StatusVerifikasi', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('STATUS_HAPUS', 'StatusHapus', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('TIPE_USULAN', 'TipeUsulan', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('UNIT_ID', 'UnitId', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('SHSD_ID', 'ShsdId', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('STATUS_CONFIRMASI_PENYELIA', 'StatusConfirmasiPenyelia', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('KOMENTAR_VERIFIKATOR', 'KomentarVerifikator', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('NAMA_SEBELUM', 'NamaSebelum', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('HARGA_SEBELUM', 'HargaSebelum', 'double', CreoleTypes::DOUBLE, false, null);

		$tMap->addColumn('ALASAN_PERUBAHAN_HARGA', 'AlasanPerubahanHarga', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('IS_PERUBAHAN_HARGA', 'IsPerubahanHarga', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('IS_PERBEDAAN_PENDUKUNG', 'IsPerbedaanPendukung', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('ALASAN_PERBEDAAN_DINAS', 'AlasanPerbedaanDinas', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('ALASAN_PERBEDAAN_PENYELIA', 'AlasanPerbedaanPenyelia', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('HARGA_PENDUKUNG1', 'HargaPendukung1', 'double', CreoleTypes::DOUBLE, false, null);

		$tMap->addColumn('HARGA_PENDUKUNG2', 'HargaPendukung2', 'double', CreoleTypes::DOUBLE, false, null);

		$tMap->addColumn('HARGA_PENDUKUNG3', 'HargaPendukung3', 'double', CreoleTypes::DOUBLE, false, null);

		$tMap->addColumn('TAHAP', 'Tahap', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('STATUS_BARU', 'StatusBaru', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('STATUS_PENDING', 'StatusPending', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('KOMPONEN_TIPE2', 'KomponenTipe2', 'string', CreoleTypes::VARCHAR, false, 50);

	} 
} 