<?php



class DeskripsiResumeRapatKegiatanMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.DeskripsiResumeRapatKegiatanMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.deskripsi_resume_rapat_kegiatan');
		$tMap->setPhpName('DeskripsiResumeRapatKegiatan');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('ID_DESKRIPSI_RESUME_RAPAT', 'IdDeskripsiResumeRapat', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addPrimaryKey('KEGIATAN_CODE', 'KegiatanCode', 'string', CreoleTypes::VARCHAR, true, 100);

		$tMap->addColumn('KEGIATAN_NAME', 'KegiatanName', 'string', CreoleTypes::VARCHAR, false, 500);

		$tMap->addColumn('CATATAN_PEMBAHASAN', 'CatatanPembahasan', 'string', CreoleTypes::VARCHAR, false, null);

	} 
} 