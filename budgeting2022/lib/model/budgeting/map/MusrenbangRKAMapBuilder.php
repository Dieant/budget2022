<?php



class MusrenbangRKAMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.MusrenbangRKAMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.musrenbang_rka');
		$tMap->setPhpName('MusrenbangRKA');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('STATUS_HAPUS', 'StatusHapus', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('UNIT_ID', 'UnitId', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('KEGIATAN_CODE', 'KegiatanCode', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('DETAIL_NO', 'DetailNo', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('ID_MUSRENBANG', 'IdMusrenbang', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('TAHUN_MUSRENBANG', 'TahunMusrenbang', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('KOORDINAT_MUSRENBANG', 'KoordinatMusrenbang', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('VOLUME', 'Volume', 'double', CreoleTypes::DOUBLE, false, null);

		$tMap->addColumn('SATUAN', 'Satuan', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('ANGGARAN', 'Anggaran', 'double', CreoleTypes::DOUBLE, false, null);

		$tMap->addColumn('KOMPONEN_NAME', 'KomponenName', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('KOMPONEN_HARGA', 'KomponenHarga', 'double', CreoleTypes::DOUBLE, false, null);

		$tMap->addColumn('KOMPONEN_PAJAK', 'KomponenPajak', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('DETAIL_NAME', 'DetailName', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('KOMPONEN_REKENING', 'KomponenRekening', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('KOMPONEN_TIPE', 'KomponenTipe', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('SUBTITLE', 'Subtitle', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('KOMPONEN_ID', 'KomponenId', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('CREATED_BY', 'CreatedBy', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('UPDATED_BY', 'UpdatedBy', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('CREATED_AT', 'CreatedAt', 'int', CreoleTypes::TIMESTAMP, false, null);

		$tMap->addColumn('UPDATED_AT', 'UpdatedAt', 'int', CreoleTypes::TIMESTAMP, false, null);

	} 
} 