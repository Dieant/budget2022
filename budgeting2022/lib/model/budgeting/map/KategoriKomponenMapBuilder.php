<?php



class KategoriKomponenMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.KategoriKomponenMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.kategori_komponen');
		$tMap->setPhpName('KategoriKomponen');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('KATEGORI_KOMPONEN_ID', 'KategoriKomponenId', 'string', CreoleTypes::VARCHAR, true, 50);

		$tMap->addColumn('KATEGORI_KOMPONEN_NAME', 'KategoriKomponenName', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('KOMPONEN_TIPE', 'KomponenTipe', 'string', CreoleTypes::VARCHAR, false, 10);

	} 
} 