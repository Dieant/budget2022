<?php



class KategoriEstMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.KategoriEstMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.kategori_est');
		$tMap->setPhpName('KategoriEst');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('KATEGORI_EST_ID', 'KategoriEstId', 'string', CreoleTypes::VARCHAR, true, 50);

		$tMap->addColumn('KATEGORI_EST_NAME', 'KategoriEstName', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('REKENING_CODE', 'RekeningCode', 'string', CreoleTypes::VARCHAR, false, 50);

	} 
} 