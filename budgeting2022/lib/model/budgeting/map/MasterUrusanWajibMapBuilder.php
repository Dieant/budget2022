<?php



class MasterUrusanWajibMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.MasterUrusanWajibMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.master_urusan_wajib');
		$tMap->setPhpName('MasterUrusanWajib');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('KODE_URUSAN_WAJIB', 'KodeUrusanWajib', 'string', CreoleTypes::CHAR, true, 2);

		$tMap->addColumn('NAMA_URUSAN_WAJIB', 'NamaUrusanWajib', 'string', CreoleTypes::VARCHAR, false, 200);

	} 
} 