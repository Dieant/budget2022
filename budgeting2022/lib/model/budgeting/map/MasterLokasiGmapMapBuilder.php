<?php



class MasterLokasiGmapMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.MasterLokasiGmapMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.master_lokasi_gmap');
		$tMap->setPhpName('MasterLokasiGmap');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('NAMA_LOKASI', 'NamaLokasi', 'string', CreoleTypes::VARCHAR, false, 50);

	} 
} 