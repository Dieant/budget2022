<?php



class DeskripsiResumeRapatMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.DeskripsiResumeRapatMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.deskripsi_resume_rapat');
		$tMap->setPhpName('DeskripsiResumeRapat');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('UNIT_ID', 'UnitId', 'string', CreoleTypes::VARCHAR, true, 10);

		$tMap->addColumn('TAHAP', 'Tahap', 'int', CreoleTypes::SMALLINT, true, null);

		$tMap->addColumn('TANGGAL', 'Tanggal', 'int', CreoleTypes::TIMESTAMP, false, null);

		$tMap->addColumn('JAM', 'Jam', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('MENIT', 'Menit', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('ACARA', 'Acara', 'string', CreoleTypes::VARCHAR, false, 200);

		$tMap->addColumn('ACARA2', 'Acara2', 'string', CreoleTypes::VARCHAR, false, 200);

		$tMap->addColumn('TEMPAT', 'Tempat', 'string', CreoleTypes::VARCHAR, false, 200);

		$tMap->addColumn('PIMPINAN', 'Pimpinan', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('CATATAN', 'Catatan', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('TOKEN', 'Token', 'string', CreoleTypes::VARCHAR, false, 500);

	} 
} 