<?php



class MasterPenyeliaMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.MasterPenyeliaMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.master_penyelia');
		$tMap->setPhpName('MasterPenyelia');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('ID_PENYELIA', 'IdPenyelia', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('USERNAME', 'Username', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('USEREMAIL', 'Useremail', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('AKTIF', 'Aktif', 'boolean', CreoleTypes::BOOLEAN, false, null);

	} 
} 