<?php



class AkrualMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.AkrualMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.akrual');
		$tMap->setPhpName('Akrual');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('AKRUAL_CODE', 'AkrualCode', 'string', CreoleTypes::VARCHAR, true, 100);

		$tMap->addColumn('NAMA', 'Nama', 'string', CreoleTypes::VARCHAR, false, 300);

	} 
} 