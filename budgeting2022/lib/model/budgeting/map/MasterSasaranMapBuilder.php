<?php



class MasterSasaranMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.MasterSasaranMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.master_sasaran');
		$tMap->setPhpName('MasterSasaran');

		$tMap->setUseIdGenerator(true);

		$tMap->setPrimaryKeyMethodInfo('ebudget.master_sasaran_SEQ');

		$tMap->addColumn('KODE_SASARAN', 'KodeSasaran', 'string', CreoleTypes::VARCHAR, true, 5);

		$tMap->addColumn('NAMA_SASARAN', 'NamaSasaran', 'string', CreoleTypes::VARCHAR, false, 200);

		$tMap->addColumn('KODE_MISI', 'KodeMisi', 'string', CreoleTypes::VARCHAR, false, 2);

		$tMap->addColumn('KODE_TUJUAN', 'KodeTujuan', 'string', CreoleTypes::VARCHAR, false, 2);

		$tMap->addColumn('INDIKATOR_SASARAN', 'IndikatorSasaran', 'string', CreoleTypes::VARCHAR, false, 300);

		$tMap->addColumn('TARGET', 'Target', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('KODE_SASARAN2', 'KodeSasaran2', 'string', CreoleTypes::VARCHAR, false, 10);

	} 
} 