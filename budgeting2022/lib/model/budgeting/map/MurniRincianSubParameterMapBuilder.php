<?php



class MurniRincianSubParameterMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.MurniRincianSubParameterMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.murni_rincian_sub_parameter');
		$tMap->setPhpName('MurniRincianSubParameter');

		$tMap->setUseIdGenerator(false);

		$tMap->addColumn('UNIT_ID', 'UnitId', 'string', CreoleTypes::VARCHAR, true, 10);

		$tMap->addColumn('KEGIATAN_CODE', 'KegiatanCode', 'string', CreoleTypes::VARCHAR, true, 12);

		$tMap->addColumn('FROM_SUB_KEGIATAN', 'FromSubKegiatan', 'string', CreoleTypes::VARCHAR, true, 20);

		$tMap->addColumn('SUB_KEGIATAN_NAME', 'SubKegiatanName', 'string', CreoleTypes::VARCHAR, true, 300);

		$tMap->addColumn('SUBTITLE', 'Subtitle', 'string', CreoleTypes::VARCHAR, false, 200);

		$tMap->addColumn('DETAIL_NAME', 'DetailName', 'string', CreoleTypes::VARCHAR, false, 200);

		$tMap->addColumn('NEW_SUBTITLE', 'NewSubtitle', 'string', CreoleTypes::VARCHAR, false, 400);

		$tMap->addColumn('PARAM', 'Param', 'string', CreoleTypes::VARCHAR, false, 400);

		$tMap->addColumn('KECAMATAN', 'Kecamatan', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('MAX_NILAI', 'MaxNilai', 'double', CreoleTypes::DOUBLE, false, null);

		$tMap->addColumn('KETERANGAN', 'Keterangan', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('KET_PEMBAGI', 'KetPembagi', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('PEMBAGI', 'Pembagi', 'double', CreoleTypes::DOUBLE, false, null);

		$tMap->addColumn('KODE_SUB', 'KodeSub', 'string', CreoleTypes::VARCHAR, false, 30);

		$tMap->addColumn('TAHUN', 'Tahun', 'string', CreoleTypes::VARCHAR, false, 5);

	} 
} 