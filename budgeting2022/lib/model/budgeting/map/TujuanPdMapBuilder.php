<?php



class TujuanPdMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.TujuanPdMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.tujuan_pd');
		$tMap->setPhpName('TujuanPd');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('ID_TUJUAN', 'IdTujuan', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('TUJUAN_PD', 'TujuanPd', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('UNIT_ID', 'UnitId', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('KODE_KEGIATAN', 'KodeKegiatan', 'string', CreoleTypes::VARCHAR, false, null);

	} 
} 