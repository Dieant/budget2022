<?php



class MasterProgram2MapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.MasterProgram2MapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.master_program2');
		$tMap->setPhpName('MasterProgram2');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('KODE_PROGRAM', 'KodeProgram', 'string', CreoleTypes::CHAR, true, 2);

		$tMap->addPrimaryKey('KODE_PROGRAM2', 'KodeProgram2', 'string', CreoleTypes::VARCHAR, true, 25);

		$tMap->addColumn('NAMA_PROGRAM2', 'NamaProgram2', 'string', CreoleTypes::VARCHAR, false, 300);

		$tMap->addColumn('RANKING', 'Ranking', 'int', CreoleTypes::SMALLINT, false, null);

		$tMap->addColumn('KODE_PROGRAM3', 'KodeProgram3', 'string', CreoleTypes::VARCHAR, false, 20);

	} 
} 