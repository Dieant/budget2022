<?php



class PersonilRkaMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.PersonilRkaMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.personil_rka');
		$tMap->setPhpName('PersonilRka');

		$tMap->setUseIdGenerator(false);

		$tMap->addColumn('NIP', 'Nip', 'string', CreoleTypes::VARCHAR, false, 25);

		$tMap->addColumn('NAMA', 'Nama', 'string', CreoleTypes::VARCHAR, false, 70);

		$tMap->addColumn('UNIT_ID', 'UnitId', 'string', CreoleTypes::VARCHAR, false, 10);

		$tMap->addColumn('KEGIATAN_CODE', 'KegiatanCode', 'string', CreoleTypes::VARCHAR, false, 12);

		$tMap->addColumn('SUBTITLE', 'Subtitle', 'string', CreoleTypes::VARCHAR, false, 70);

	} 
} 