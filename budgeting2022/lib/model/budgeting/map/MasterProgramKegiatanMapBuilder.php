<?php



class MasterProgramKegiatanMapBuilder {

	
	const CLASS_NAME = 'lib.model.budgeting.map.MasterProgramKegiatanMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('budgeting');

		$tMap = $this->dbMap->addTable('ebudget.master_program_kegiatan');
		$tMap->setPhpName('MasterProgramKegiatan');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('UNIT_ID', 'UnitId', 'string', CreoleTypes::VARCHAR, true, 10);

		$tMap->addPrimaryKey('KODE_KEGIATAN', 'KodeKegiatan', 'string', CreoleTypes::VARCHAR, true, 20);

		$tMap->addPrimaryKey('KODE_PROGRAM_INDIKATOR', 'KodeProgramIndikator', 'string', CreoleTypes::VARCHAR, true, 20);

	} 
} 