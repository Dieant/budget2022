<?php

/**
 * Subclass for representing a row from the 'budget2012.revisi1_rincian_detail' table.
 *
 * 
 *
 * @package lib.model.budgeting
 */
class Revisi1RincianDetail extends BaseRevisi1RincianDetail {

    public function getTotalKodeSub($kode_sub) {
        $unit_id = $this->getUnitId();
        $kegiatan_code = $this->getKegiatanCode();
        $kodeSub = $kode_sub;
        $query2 = "select sum(nilai_anggaran) as hasil_kali
                from " . sfConfig::get('app_default_schema') . ".revisi1_rincian_detail
                where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and kode_sub='$kodeSub' and status_hapus=false";
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query2);
        $t = $stmt->executeQuery();
        while ($t->next()) {
            $hasilKali = $t->getString('hasil_kali');
        }
        return $hasilKali;
    }

}
