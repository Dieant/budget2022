<?php

/**
 * Subclass for performing query and update operations on the 'ebudget.dinas_rincian_sub_parameter' table.
 *
 * 
 *
 * @package lib.model.budgeting
 */ 
class DinasRincianSubParameterPeer extends BaseDinasRincianSubParameterPeer
{
}
