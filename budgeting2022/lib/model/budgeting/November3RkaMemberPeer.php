<?php

/**
 * Subclass for performing query and update operations on the 'ebudget.november3_rka_member' table.
 *
 * 
 *
 * @package lib.model.budgeting
 */ 
class November3RkaMemberPeer extends BaseNovember3RkaMemberPeer
{
}
