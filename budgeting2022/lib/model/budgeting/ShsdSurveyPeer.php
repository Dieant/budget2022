<?php

/**
 * Subclass for performing query and update operations on the 'ebudget.shsd_survey' table.
 *
 * 
 *
 * @package lib.model.budgeting
 */ 
class ShsdSurveyPeer extends BaseShsdSurveyPeer
{
}
