<?php

/**
 * Subclass for performing query and update operations on the 'ebudget.log_approval' table.
 *
 * 
 *
 * @package lib.model.budgeting
 */
class LogApprovalPeer extends BaseLogApprovalPeer {

    public function getTahapKegiatan($unit_id, $kode_kegiatan) {
        $c_tahap = new Criteria();
        $c_tahap->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
        $c_tahap->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
        $tahap_kegiatan = DinasMasterKegiatanPeer::doSelectOne($c_tahap);
        return $tahap_kegiatan->getTahap();
    }

}
