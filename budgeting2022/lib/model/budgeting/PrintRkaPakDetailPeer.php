<?php

/**
 * Subclass for performing query and update operations on the 'ebudget.print_rka_pak_detail' table.
 *
 * 
 *
 * @package lib.model.budgeting
 */ 
class PrintRkaPakDetailPeer extends BasePrintRkaPakDetailPeer
{
}
