<?php

/**
 * Subclass for performing query and update operations on the 'master_schema' table.
 *
 * 
 *
 * @package lib.model.budget2010
 */ 
class MasterSchemaPeer extends BaseMasterSchemaPeer
{
}
