<?php

/**
 * Subclass for performing query and update operations on the 'ebudget.pembanding_kegiatan' table.
 *
 * 
 *
 * @package lib.model.budgeting
 */ 
class PembandingKegiatanPeer extends BasePembandingKegiatanPeer
{
	public static function savePembandingUsulan($unit_id, $kode_kegiatan) {
        $con = Propel::getConnection();
        $con->begin();
        try {
            // set_time_limit(0);
            // $x = null;
            //buat id pembanding kegiatan baru
            $id = 0;
            $query = "select max(id) as nilai from " . sfConfig::get('app_default_schema') . ".pembanding_kegiatan";
            $stmt = $con->prepareStatement($query);
            $rs_max = $stmt->executeQuery();
            while ($rs_max->next()) {
                $id = $rs_max->getString('nilai');
            }
            $id+=1;
            // if ($unit_id == '2600' && $kode_kegiatan == '1.1.1.03.05.0002') {
            //     echo "masuk sini";die();
            //     print_r($id);die();
            // }
            
            
            //isi pembanding kegiatan
            $pembanding_kegiatan = new PembandingKegiatan();

            $pembanding_kegiatan->setId($id);
            $pembanding_kegiatan->setUnitId($unit_id);
            $pembanding_kegiatan->setKodeKegiatan($kode_kegiatan);
            $pembanding_kegiatan->setTahap(DinasMasterKegiatanPeer::getTahapKegiatan($unit_id, $kode_kegiatan));
            $pembanding_kegiatan->setUpdatedAt(date('Y-m-d H:i:s'));
            $pembanding_kegiatan->save();


            // $tahap_insert = DinasMasterKegiatanPeer::getTahapKegiatan($unit_id, $kode_kegiatan);
            // $tgl_upd = date('Y-m-d H:i:s');
            // // echo $tgl_upd;die();

            // $ins_pembanding_kegiatan = "insert into " . sfConfig::get('app_default_schema') . ".pembanding_kegiatan values('".$id."','".$unit_id."','".$kode_kegiatan."',".$tahap_insert.",".$tgl_upd.")";
            // echo $ins_pembanding_kegiatan;die();
            // $stmt_ins = $con->prepareStatement($ins_pembanding_kegiatan);
            // $stmt_ins->executeQuery();
            // // $x = $stmt_ins->executeQuery();
            // // if ($x != null) {
            // //     echo $x;die();
            // // }

            // echo "lewat";die();
            //isi pembanding komponen
            $c_komponen = new Criteria();
            $c_komponen->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
            $c_komponen->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
            $c_komponen->add(DinasRincianDetailPeer::STATUS_HAPUS, FALSE);
            //$c_komponen->add(DinasRincianDetailPeer::STATUS_LEVEL, 4, Criteria::GREATER_EQUAL);
            $rs_rd = DinasRincianDetailPeer::doSelect($c_komponen);

            // if ($unit_id == '3000' && $kode_kegiatan == '1.1.2.05.02.0004') {
            //     // echo "masuk sini";die();
            //     print_r($rs_rd);die();
            // }

            foreach ($rs_rd as $rd) {

                // $a = null;
                $pembanding_komponen = new PembandingKomponen();
                
                $pembanding_komponen->setIdPembandingKegiatan($id);
                $pembanding_komponen->setDetailNo($rd->getDetailNo());
                $pembanding_komponen->setRekeningCode($rd->getRekeningCode());
                $pembanding_komponen->setKomponenName($rd->getKomponenName());
                $pembanding_komponen->setDetailName($rd->getDetailName());
                $pembanding_komponen->setKomponenHarga($rd->getKomponenHargaAwal());
                $pembanding_komponen->setVolume($rd->getVolume());
                $pembanding_komponen->setSatuan($rd->getSatuan());
                $pembanding_komponen->setKeteranganKoefisien($rd->getKeteranganKoefisien());
                $pembanding_komponen->setSubtitle($rd->getSubtitle());
                $pembanding_komponen->setNilaiAnggaran($rd->getNilaiAnggaran());
                $pembanding_komponen->save();
                // if ($unit_id == '3000' && $kode_kegiatan == '1.1.2.05.02.0004') {
                //     echo "masuk sini";die();
                //     // print_r($pembanding_komponen);die();
                // }
                // $ins_pembanding_komponen = "insert into " . sfConfig::get('app_default_schema') . ".pembanding_komponen values('".$id."','".$rd->getDetailNo()."','".$rd->getRekeningCode()."','".$rd->getSubtitle()."','".$rd->getKomponenName()."','".$rd->getDetailName()."','".$rd->getKomponenHargaAwal()."','".$rd->getVolume()."','".$rd->getSatuan()."','".$rd->getKeteranganKoefisien()."','".$rd->getNilaiAnggaran()."')";
                // // echo $ins_pembanding_komponen;die();
                // $stmt_ins2 = $con->prepareStatement($ins_pembanding_komponen);
                // $stmt_ins2->executeQuery();
                // // $a = $stmt_ins2->executeQuery();
                // // if ($a != null) {
                // //     echo "insert pembanding_komponen ada";die();
                // // }
            }
                
            $con->commit();
            return TRUE;
        } catch (Exception $ex) {
            // echo "sini";die();
            // if ($unit_id == '3000' && $kode_kegiatan == '1.1.2.05.02.0004') {
            //     echo "masuk sini";die();
            // }
            $con->rollback();
            return FALSE;
        }
    }
}
