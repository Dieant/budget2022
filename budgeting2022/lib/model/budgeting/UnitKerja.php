<?php

/**
 * Subclass for representing a row from the 'unit_kerja' table.
 *
 * 
 *
 * @package lib.model.budget2010
 */ 
class UnitKerja extends BaseUnitKerja
{
    public function __toString() {
        return $this->getUnitName();
    }
    public function getStringUnitKerja($unitId) {
        $c = new Criteria();
        $c->addSelectColumn(UnitKerjaPeer::UNIT_NAME);
        $c->add(UnitKerjaPeer::UNIT_ID, $unitId, Criteria::EQUAL);
        $unitName = UnitKerjaPeer::doSelectOne($c);
        return '--';
    }
}
