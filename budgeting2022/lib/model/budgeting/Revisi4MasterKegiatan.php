<?php

/**
 * Subclass for representing a row from the 'budget2012.revisi4_master_kegiatan' table.
 *
 * 
 *
 * @package lib.model.budgeting
 */ 
class Revisi4MasterKegiatan extends BaseRevisi4MasterKegiatan
{
    public function getKodeNamaKegiatan()
    {
        return $this->getKodeKegiatan().'-'.$this->getNamaKegiatan();
    }
}
