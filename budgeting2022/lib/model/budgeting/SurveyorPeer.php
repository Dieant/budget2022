<?php

/**
 * Subclass for performing query and update operations on the 'ebudget.surveyor' table.
 *
 * 
 *
 * @package lib.model.budgeting
 */ 
class SurveyorPeer extends BaseSurveyorPeer
{
}
