<?php

/**
 * Subclass for representing a row from the 'ebudget.november3_rincian_detail' table.
 *
 * 
 *
 * @package lib.model.budgeting
 */
class November3RincianDetail extends BaseNovember3RincianDetail {

    public function getMaxDetailNo($unit_id, $kegiatan_code) {
        $queryDetailNo = "select max(detail_no) as nilai from " . sfConfig::get('app_default_schema') . ".november3_rincian_detail 
            where unit_id='$unit_id' and kegiatan_code='$kegiatan_code'";
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($queryDetailNo);
        $rs_max = $stmt->executeQuery();
        while ($rs_max->next()) {
            $detail_no = $rs_max->getString('nilai');
        }
        $detail_no+=1;
        return $detail_no;
    }

    public function getBatasPaguPerKegiatan($unit_id, $kegiatan_code, $komponen_id, $pajak, $vol1, $vol2, $vol3, $vol4, $komponen_penyusun, $volumePenyusun) {
        try {
            $unit_id = $unit_id;
            $total_baru = $this->getTotalNilaiBaru($komponen_id, $pajak, $vol1, $vol2, $vol3, $vol4, $komponen_penyusun, $volumePenyusun);

            $total_pagu = $this->getNilaiPagu($unit_id, $kegiatan_code);

            $total_rincianDetail = $this->getNilaiRincianDetail($unit_id, $kegiatan_code);

            $total_baruRincian = $total_baru + $total_rincianDetail;
            if ($total_pagu < $total_baruRincian) {
                $backValue = '1';
            } else {
                $backValue = '0';
            }
        } catch (Exception $exc) {
            $backValue = '1';
        }

        return $backValue;
    }

    public function getBatasPaguPerKegiatanSubKegiatan($unit_id, $kegiatan_code, $perkalian_keoef, $total) {
        $unit_id = $unit_id;
        $total_baru = $perkalian_keoef * $total;
        $total_pagu = $this->getNilaiPagu($unit_id, $kegiatan_code);
        $total_rincianDetail = $this->getNilaiRincianDetail($unit_id, $kegiatan_code);

        $total_baruRincian = $total_baru + $total_rincianDetail;
        if ($total_pagu < $total_baruRincian) {
            $backValue = '1';
        } else {
            $backValue = '0';
        }
        return $backValue;
    }

    public function getBatasPaguPerKegiatanforEdit($unit_id, $kegiatan_code, $detail_no, $volume) {
        try {
            $unit_id = $unit_id;

            $komponen_id = $this->getIsiKomponenId($unit_id, $kegiatan_code, $detail_no);
            $pajak = $this->getIsiPajak($unit_id, $kegiatan_code, $detail_no);
            //nilai komponen setelah di edit
            $total_baru = $this->getTotalNilaiBaruforEdit($komponen_id, $pajak, $volume);
            //nilai komponen sebelum di edit
            $total_saatIni = $this->getTotalNilaiKomponenBdsrDetailNo($unit_id, $kegiatan_code, $detail_no);

            $total_pagu = $this->getNilaiPagu($unit_id, $kegiatan_code);

            $total_rincianDetail = $this->getNilaiRincianDetail($unit_id, $kegiatan_code);
            //print_r($komponen_id.' '.$pajak.' totBaru '.$total_baru.' ');exit;
            //rumus : total baru = total rincian_detail + nilai komponen (setelah di edit) - nilai komponen (sebelum di edit)
            $total_baruRincian = $total_rincianDetail + $total_baru - $total_saatIni;
            //print_r('test dari programer '.$total_rincianDetail.' '.$total_baru.' '.$total_saatIni.' '.$total_pagu);exit;
            if ($total_pagu < $total_baruRincian) {
                $backValue = '1';
            } else {
                $backValue = '0';
            }
        } catch (Exception $exc) {
            $backValue = '1';
        }

        return $backValue;
    }

    public function getBatasPaguPerDinas($unit_id, $komponen_id, $pajak, $vol1, $vol2, $vol3, $vol4, $komponen_penyusun, $volumePenyusun) {
        try {
            $unit_id = $unit_id;
            $total_baru = $this->getTotalNilaiBaru($komponen_id, $pajak, $vol1, $vol2, $vol3, $vol4, $komponen_penyusun, $volumePenyusun);

            $total_pagu = $this->getNilaiPagu($unit_id, $kegiatan_code = null);

            $total_rincianDetail = $this->getNilaiRincianDetail($unit_id, $kegiatan_code = null);

            $total_baruRincian = $total_baru + $total_rincianDetail;
            if ($total_pagu < $total_baruRincian) {
                $backValue = '1';
            } else {
                $backValue = '0';
            }
        } catch (Exception $exc) {
            $backValue = '1';
        }

        return $backValue;
    }

    public function getBatasPaguPerDinasforEdit($unit_id, $kegiatan_code, $detail_no, $volume) {
        try {
            $unit_id = $unit_id;

            $komponen_id = $this->getIsiKomponenId($unit_id, $kegiatan_code, $detail_no);
            $pajak = $this->getIsiPajak($unit_id, $kegiatan_code, $detail_no);
            //nilai komponen setelah di edit
            $total_baru = $this->getTotalNilaiBaruforEdit($komponen_id, $pajak, $volume);
            //nilai komponen sebelum di edit
            $total_saatIni = $this->getTotalNilaiKomponenBdsrDetailNo($unit_id, $kegiatan_code, $detail_no);

            $total_pagu = $this->getNilaiPagu($unit_id, $kegiatan_code = null);

            $total_rincianDetail = $this->getNilaiRincianDetail($unit_id, $kegiatan_code = null);
            //print_r($komponen_id.' '.$pajak.' totBaru '.$total_baru.' ');exit;
            //rumus : total baru = total rincian_detail + nilai komponen (setelah di edit) - nilai komponen (sebelum di edit)
            $total_baruRincian = $total_rincianDetail + $total_baru - $total_saatIni;
            //print_r('test dari programer '.$total_rincianDetail.' '.$total_baru.' '.$total_saatIni.' '.$total_pagu);exit;
            if ($total_pagu < $total_baruRincian) {
                $backValue = '1';
            } else {
                $backValue = '0';
            }
        } catch (Exception $exc) {
            $backValue = '1';
        }
        return $backValue;
    }

    public function getIsiKomponenId($unit_id, $kegiatan_code, $detail_no) {
        $queryRd = "select komponen_id as komponen_id from " . sfConfig::get('app_default_schema') . ".november3_rincian_detail where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and detail_no='$detail_no' and status_hapus=FALSE";
        $con = Propel::getConnection();
        $statement2 = $con->prepareStatement($queryRd);
        $rs_nilai2 = $statement2->executeQuery();
        while ($rs_nilai2->next()) {
            $komponen_id = $rs_nilai2->getString('komponen_id');
        }
        return $komponen_id;
    }

    public function getTotalNilaiKomponenBdsrDetailNo($unit_id, $kegiatan_code, $detail_no) {
        $queryRd = "select nilai_anggaran as nilai from " . sfConfig::get('app_default_schema') . ".november3_rincian_detail where 
                unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and detail_no='$detail_no' and status_hapus=FALSE";

        $con = Propel::getConnection();
        $statement2 = $con->prepareStatement($queryRd);
        $rs_nilai2 = $statement2->executeQuery();
        while ($rs_nilai2->next()) {
            $nilai_rincian_sebelum_edit = $rs_nilai2->getString('nilai');
        }
        return $nilai_rincian_sebelum_edit;
    }

    public function getIsiPajak($unit_id, $kegiatan_code, $detail_no) {
        $queryRd = "select pajak as pajak from " . sfConfig::get('app_default_schema') . ".november3_rincian_detail where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and detail_no='$detail_no' and status_hapus=FALSE";
        $con = Propel::getConnection();
        $statement2 = $con->prepareStatement($queryRd);
        $rs_nilai2 = $statement2->executeQuery();
        while ($rs_nilai2->next()) {
            $pajak = $rs_nilai2->getString('pajak');
        }
        return $pajak;
    }

    public function getTotalNilaiBaru($komponen_id, $pajak, $vol1, $vol2, $vol3, $vol4, $komponen_penyusun, $volumePenyusun) {
        /* print_r($komponen_penyusun);
          var_dump($komponen_penyusun);

          print_r('chekbox');
          var_dump($volumePenyusun);exit; */

        $volume = 0;
        $c = new Criteria();
        $c->add(KomponenPeer::KOMPONEN_ID, $komponen_id);
        $rs_komponen = KomponenPeer::doSelectOne($c);
        if ($rs_komponen) {
            $komponen_harga = $rs_komponen->getKomponenHarga();
            $komponen_name = $rs_komponen->getKomponenName();
            $satuan = $rs_komponen->getSatuan();
        }
        if ($vol1 || $vol2 || $vol3 || $vol4) {
            if ($vol2 == '') {
                $vol2 = 1;
                $volume = $vol1 * $vol2;
            } else if (!$vol2 == '') {
                $volume = $vol1 * $vol2;
            }
            if ($vol3 == '') {
                $vol3 = 1;
                $volume = $volume * $vol3;
            } else if (!$vol3 == '') {
                $volume = $vol1 * $vol2 * $vol3;
            }
            if ($vol4 == '') {
                $vol4 = 1;
                $volume = $volume * $vol4;
            } else if (!$vol4 == '') {
                $volume = $vol1 * $vol2 * $vol3 * $vol4;
            }
        }
        $nilai_komponen = round($komponen_harga * $volume * ((100 + $pajak) / 100));
        //print_r($komponen_id.' '.$komponen_name.' '.$vol1);exit;
        $totalKomponenPenyusun = 0;
        foreach ($komponen_penyusun as $penyusun) {
            $cekPenyusun = new Criteria();
            $cekPenyusun->add(KomponenPeer::KOMPONEN_ID, $penyusun);
            $rs_cekPenyusun = KomponenPeer::doSelect($cekPenyusun);
            foreach ($rs_cekPenyusun as $komPenyusun) {
                if ($komPenyusun->getKomponenNonPajak() == TRUE) {
                    $pajakPenyusun = 0;
                } else {
                    $pajakPenyusun = 10;
                }
                $penyu = $volumePenyusun[$komPenyusun->getKomponenId()] * $komPenyusun->getKomponenHarga() * ((100 + $pajakPenyusun) / 100);
                $totalKomponenPenyusun+=$penyu;
            }
        }
        $totalKomponenBaru = $nilai_komponen + $totalKomponenPenyusun;
        return $totalKomponenBaru;
    }

    public function getTotalNilaiBaruforEdit($komponen_id, $pajak, $volume) {

        $c = new Criteria();
        $c->add(KomponenPeer::KOMPONEN_ID, $komponen_id);
        $rs_komponen = KomponenPeer::doSelectOne($c);
        if ($rs_komponen) {
            $komponen_harga = $rs_komponen->getKomponenHarga();
            $komponen_name = $rs_komponen->getKomponenName();
            $satuan = $rs_komponen->getSatuan();
        }

        $nilai_komponen = round($komponen_harga * $volume * ((100 + $pajak) / 100));
        //print_r($komponen_id.' '.$komponen_name.' '.$vol1);exit;

        $totalKomponenBaru = $nilai_komponen;
        return $totalKomponenBaru;
    }

    public function getNilaiPagu($unit_id, $kegiatan_code) {
        //untuk merubah menjadi nilai pagu pakai query no 1.
        $con = Propel::getConnection();
        if (sfConfig::get('app_fasilitas_paguBayangan') == 'buka') { // menggunakan nilai rincian + paguBayangan #biasanya setalah dari dewan, dan agar dinas tidak melebihi total yang sudah ditetapkan
            if ($kegiatan_code == null) {
                //awal
//                $queryPagu = "select sum(nilai_anggaran) as nilai from " . sfConfig::get('app_default_schema') . ".murni_rincian_detail where unit_id='$unit_id' and status_hapus=false";
                //awal
                $queryPagu = "select sum(alokasi_dana) as nilai from " . sfConfig::get('app_default_schema') . ".november3_master_kegiatan where unit_id='$unit_id'";
                $queryPaguTambahan = "select sum(tambahan_pagu) as nilai from " . sfConfig::get('app_default_schema') . ".november3_master_kegiatan where unit_id='$unit_id'";
            } else {
                //awal
//                $queryPagu = "select sum(nilai_anggaran) as nilai from " . sfConfig::get('app_default_schema') . ".murni_rincian_detail where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and status_hapus=false";
                //awal
                $queryPagu = "select sum(alokasi_dana) as nilai from " . sfConfig::get('app_default_schema') . ".november3_master_kegiatan where unit_id='$unit_id' and kode_kegiatan='$kegiatan_code'";
                $queryPaguTambahan = "select sum(tambahan_pagu) as nilai from " . sfConfig::get('app_default_schema') . ".november3_master_kegiatan where unit_id='$unit_id' and kode_kegiatan='$kegiatan_code'";
            }
            $statement = $con->prepareStatement($queryPaguTambahan);
            $rs_nilaiTambahan = $statement->executeQuery();
            while ($rs_nilaiTambahan->next()) {
                $nilai_paguTambahan = $rs_nilaiTambahan->getString('nilai');
            }
        } else { //berarti paguBayangan ditutup jadi menggunakan pagu asli
            if ($kegiatan_code == null) {
                $queryPagu = "select sum(alokasi_dana) as nilai from " . sfConfig::get('app_default_schema') . ".november3_master_kegiatan where unit_id='$unit_id'";
                $queryPaguTambahan = 0;
            } else {
                $queryPagu = "select sum(alokasi_dana) as nilai from " . sfConfig::get('app_default_schema') . ".november3_master_kegiatan where unit_id='$unit_id' and kode_kegiatan='$kegiatan_code'";
                $queryPaguTambahan = 0;
            }
        }
        $con = Propel::getConnection();
        $statement = $con->prepareStatement($queryPagu);
        $rs_nilai = $statement->executeQuery();
        while ($rs_nilai->next()) {
            $nilai_awalPagu = $rs_nilai->getString('nilai');
        }

        $nilai_awal = $nilai_awalPagu + $nilai_paguTambahan;

        return $nilai_awal;
    }

    public function getNilaiRincianDetail($unit_id, $kegiatan_code) {
        if ($kegiatan_code == null) {
            $queryRd = "select sum(nilai_anggaran) as nilai from " . sfConfig::get('app_default_schema') . ".november3_rincian_detail where unit_id='$unit_id' and status_hapus=FALSE";
        } else {
            $queryRd = "select sum(nilai_anggaran) as nilai from " . sfConfig::get('app_default_schema') . ".november3_rincian_detail where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and status_hapus=FALSE";
        }
        $con = Propel::getConnection();
        $statement2 = $con->prepareStatement($queryRd);
        $rs_nilai2 = $statement2->executeQuery();
        while ($rs_nilai2->next()) {
            $nilai_rincian = $rs_nilai2->getString('nilai');
        }
        return $nilai_rincian;
    }

    public function getBatasPaguPerDinasSubKegiatan($unit_id, $perkalian_keoef, $total) {
        $unit_id = $unit_id;
        $total_baru = $total * $perkalian_keoef;
        $total_pagu = $this->getNilaiPagu($unit_id, $kegiatan_code = null);
        $total_rincianDetail = $this->getNilaiRincianDetail($unit_id, $kegiatan_code = null);

        $total_baruRincian = $total_baru + $total_rincianDetail;
        if ($total_pagu < $total_baruRincian) {
            $backValue = '1';
        } else {
            $backValue = '0';
        }
        return $backValue;
    }

    public function getNilaiPaguDanSelisih($unit_id) {
        $queryPagu = "select sum(alokasi_dana) as nilai from " . sfConfig::get('app_default_schema') . ".november3_master_kegiatan where unit_id='$unit_id'";
        $queryRd = "select sum(nilai_anggaran) as nilai from " . sfConfig::get('app_default_schema') . ".november3_rincian_detail where unit_id='$unit_id' and status_hapus=FALSE";
        $con = Propel::getConnection();
        $statement = $con->prepareStatement($queryPagu);
        $rs_nilai = $statement->executeQuery();
        while ($rs_nilai->next()) {
            $nilai_awal = $rs_nilai->getString('nilai');
        }

        $statement2 = $con->prepareStatement($queryRd);
        $rs_nilai2 = $statement2->executeQuery();
        while ($rs_nilai2->next()) {
            $nilai_rincian = $rs_nilai2->getString('nilai');
        }
        //tolak=1, lolos=0;
        //print_r($queryPagu.'  '.$queryRd);
        //$tam=$queryPagu.'  '.$queryRd;
        if ($nilai_awal < $nilai_rincian) {
            $backValue = '1';
        } else {
            $backValue = '0';
        }
        return $backValue;
    }

    public function getCekNilaiSwakelolaDelivery2($unit_id, $kode_kegiatan, $detail_no) {
        $totNilaiSwakelola = 0;
        $query2 = "select rd.unit_id,rd.kegiatan_code,rd.detail_no,rd.subtitle,rd.komponen_name,rd.kode_sub,
				 de.kode_detail_kegiatan,dk.id,
				 sum(rd.nilai_anggaran) as nilai_budgeting,
				 dk.nilai as nilai_realisasi


				from " . sfConfig::get('app_default_edelivery') . ".detail_swakelola dk,
                                    " . sfConfig::get('app_default_eproject') . ".detail_kegiatan de,
                                    " . sfConfig::get('app_default_schema') . ".november3_rincian_detail rd

				where rd.unit_id='$unit_id' and rd.kegiatan_code='$kode_kegiatan' and rd.detail_no='$detail_no'
				      and rd.volume>0 and rd.status_hapus=FALSE
				      and de.id=dk.detail_kegiatan_id 
                                      and de.kode_detail_kegiatan=rd.unit_id||'.'||rd.kegiatan_code||'.'||rd.detail_no

				group by rd.unit_id,rd.kegiatan_code,rd.detail_no,rd.subtitle,rd.komponen_name,rd.kode_sub,
				      de.kode_detail_kegiatan,dk.id,dk.nilai
				      having dk.nilai >0
				      order by rd.unit_id,rd.kegiatan_code,rd.detail_no";
        //print_r($query2);exit;

        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query2);
        $t = $stmt->executeQuery();
        while ($t->next()) {
            $jumlahRows = $t->getRow();
            if ($t->getString('nilai_realisasi') > 0) {
                $totNilaiSwakelola = $totNilaiSwakelola + $t->getString('nilai_realisasi');
                //print_r($totNilaiSwakelola);exit;
                //$warning=$totNilaiSwakelola;
            }
        }
        return $totNilaiSwakelola;
        // $backvalue = $unit_id.' '.$kode_kegiatan.' '.$detail_no.' '.$sub;
        //return $backvalue;
    }

    public function getCekNilaiSwakelolaDelivery() {
        $totNilaiSwakelola = 0;
        $jumlahRows = 0;
        $unit_id = $this->getUnitId();
        $kegiatan = $this->getKegiatanCode();
        $no = $this->getDetailNo();
        $sub = $this->getSub();
        $query2 = "select rd.unit_id,rd.kegiatan_code,rd.detail_no,rd.subtitle,rd.komponen_name,rd.kode_sub,
				 de.kode_detail_kegiatan,dk.detail_swakelola_id,dk.kontrak_id,
				 sum(rd.volume * rd.komponen_harga_awal * (100 + rd.pajak) / 100) as nilai_budgeting,
				 sum(dk.harga_realisasi*dk.volume_realisasi) as nilai_realisasi,
				 sum(dk.harga_kontrak*dk.volume_kontrak) as nilai_kontrak

				from " . sfConfig::get('app_default_edelivery') . ".detail_komponen dk, " . sfConfig::get('app_default_eproject') . ".detail_kegiatan de, " . sfConfig::get('app_default_schema') . ".november3_rincian_detail rd

				where rd.unit_id='$unit_id' and rd.kegiatan_code='$kegiatan' and rd.detail_no='$no'
				      and rd.subtitle ilike '$sub' and rd.volume>0 and rd.status_hapus=false
				      and de.id=dk.detail_kegiatan_id and substring(de.kode_detail_kegiatan,1,4)=rd.unit_id
				      and substring(de.kode_detail_kegiatan,6,4)=rd.kegiatan_code
				      and (substring(de.kode_detail_kegiatan,11))::integer=rd.detail_no

				group by rd.unit_id,rd.kegiatan_code,rd.detail_no,rd.subtitle,rd.komponen_name,rd.kode_sub,
				      de.kode_detail_kegiatan,dk.detail_swakelola_id,dk.kontrak_id
				      having sum(dk.harga_realisasi*dk.volume_realisasi)>0 or sum(dk.harga_kontrak*dk.volume_kontrak)>0
				      order by rd.unit_id,rd.kegiatan_code,rd.detail_no";
        //print_r($query2);//exit;

        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query2);
        $t = $stmt->executeQuery();
        //print_r($t);

        while ($t->next()) {
            $jumlahRows = $t->getRow();
            if ($t->getString('nilai_realisasi') > 0) {
                $totNilaiSwakelola = $totNilaiSwakelola + $t->getString('nilai_realisasi');
            }
        }
        // $totNilaiSwakelola=100;
        return $totNilaiSwakelola;
    }

    public function getCekNilaiKontrakDelivery2($unit_id, $kode_kegiatan, $detail_no) {
        $totNilaiKontrak = 0;
        $query2 = "select rd.unit_id,rd.kegiatan_code,rd.detail_no,rd.subtitle,rd.komponen_name,rd.kode_sub,
				 de.kode_detail_kegiatan,dk.detail_swakelola_id,dk.kontrak_id,
				 sum(rd.nilai_anggaran) as nilai_budgeting,
				 sum(round(dk.harga_realisasi)) as nilai_kontrak

				from " . sfConfig::get('app_default_edelivery') . ".detail_komponen dk,
                                     " . sfConfig::get('app_default_eproject') . ".detail_kegiatan de,
                                     " . sfConfig::get('app_default_schema') . ".november3_rincian_detail rd

				where rd.unit_id='$unit_id' and rd.kegiatan_code='$kode_kegiatan' and rd.detail_no='$detail_no'
				      and rd.volume>0 and rd.status_hapus=FALSE
				      and de.id=dk.detail_kegiatan_id 
                                      and de.kode_detail_kegiatan=rd.unit_id||'.'||rd.kegiatan_code||'.'||rd.detail_no

				group by rd.unit_id,rd.kegiatan_code,rd.detail_no,rd.subtitle,rd.komponen_name,rd.kode_sub,
				      de.kode_detail_kegiatan,dk.detail_swakelola_id,dk.kontrak_id
				      having  sum(round(dk.harga_realisasi))>0
				      order by rd.unit_id,rd.kegiatan_code,rd.detail_no";
//print_r($query2);exit;

        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query2);
        $t = $stmt->executeQuery();
        while ($t->next()) {
            $jumlahRows = $t->getRow();
            if ($t->getString('nilai_kontrak') > 0) {
                $totNilaiKontrak = $totNilaiKontrak + $t->getString('nilai_kontrak');
                //print_r($totNilaiKontrak);exit;
                //$warning=$totNilaiKontrak;
            }
        }
        return $totNilaiKontrak;
    }

    public function getCekNilaiKontrakDelivery() {
        $totNilaiKontrak = 0;
        $jumlahRows = 0;
        $unit_id = $this->getUnitId();
        $kegiatan = $this->getKegiatanCode();
        $no = $this->getDetailNo();
        $sub = $this->getSub();
        $query2 = "select rd.unit_id,rd.kegiatan_code,rd.detail_no,rd.subtitle,rd.komponen_name,rd.kode_sub,
				 de.kode_detail_kegiatan,dk.detail_swakelola_id,dk.kontrak_id,
				 sum(rd.volume * rd.komponen_harga_awal * (100 + rd.pajak) / 100) as nilai_budgeting,
				 sum(dk.harga_realisasi*dk.volume_realisasi) as nilai_realisasi,
				 sum(dk.harga_kontrak*dk.volume_kontrak) as nilai_kontrak

				from " . sfConfig::get('app_default_edelivery') . ".detail_komponen dk, " . sfConfig::get('app_default_eproject') . ".detail_kegiatan de, " . sfConfig::get('app_default_schema') . ".november3_rincian_detail rd

				where rd.unit_id='$unit_id' and rd.kegiatan_code='$kegiatan' and rd.detail_no='$no'
				      and rd.subtitle ilike '$sub' and rd.volume>0 and rd.status_hapus=false
				      and de.id=dk.detail_kegiatan_id and substring(de.kode_detail_kegiatan,1,4)=rd.unit_id
				      and substring(de.kode_detail_kegiatan,6,4)=rd.kegiatan_code
				      and (substring(de.kode_detail_kegiatan,11))::integer=rd.detail_no

				group by rd.unit_id,rd.kegiatan_code,rd.detail_no,rd.subtitle,rd.komponen_name,rd.kode_sub,
				      de.kode_detail_kegiatan,dk.detail_swakelola_id,dk.kontrak_id
				      having sum(dk.harga_realisasi*dk.volume_realisasi)>0 or sum(dk.harga_kontrak*dk.volume_kontrak)>0
				      order by rd.unit_id,rd.kegiatan_code,rd.detail_no";
        //print_r($query2);//exit;

        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query2);
        $t = $stmt->executeQuery();
        //print_r($t);

        while ($t->next()) {
            $jumlahRows = $t->getRow();
            if ($t->getString('nilai_kontrak') > 0) {
                $totNilaiKontrak = $totNilaiKontrak + $t->getString('nilai_kontrak');
            }
        }
        // $totNilaiKontrak=500;
        return $totNilaiKontrak;
    }

    //irul 14 feb 2014 - cek nilai alokasi
    public function getCekNilaiAlokasiProject($unit_id, $kegiatan_code, $detail_no) {
        $totNilaiAlokasi = 0;

        $query_eproject = "SELECT 
                            s.kode, k.kode, dk.kode_detail_kegiatan, dk.id_budgeting, sum(dp.ALOKASI) as jml
                            from
                            " . sfConfig::get('app_default_eproject') . ".detail_pekerjaan dp,
                            " . sfConfig::get('app_default_eproject') . ".pekerjaan p,
                            " . sfConfig::get('app_default_eproject') . ".detail_kegiatan dk,
                            " . sfConfig::get('app_default_eproject') . ".kegiatan k,
                            " . sfConfig::get('app_default_eproject') . ".skpd s
                            where
                            ((p.STATE<>0) and (p.STATE IS NOT NULL) or p.pernah_realisasi = 1 or p.pernah_konfirmasi = 1) and
                            dp.pekerjaan_id = p.id and p.kegiatan_id = k.id and s.kode = '$unit_id' and k.kode = '$kegiatan_code' and dk.id_budgeting = '$detail_no'
                            and k.skpd_id = s.id and dp.detail_kegiatan_id = dk.id
                            group by s.kode, k.kode, dk.kode_detail_kegiatan, dk.id_budgeting";

        $con = Propel::getConnection();
        $statement = $con->prepareStatement($query_eproject);
        $rs_eprojects = $statement->executeQuery();

        while ($rs_eprojects->next()) {
            $totNilaiAlokasi = $totNilaiAlokasi + $rs_eprojects->getString('jml');
        }
        return $totNilaiAlokasi;
    }

    //irul 14 feb 2014 - cek nilai alokasi

    public function getTotalKodeSub($kode_sub) {
        $unit_id = $this->getUnitId();
        $kegiatan_code = $this->getKegiatanCode();
        $kodeSub = $kode_sub;
        $query2 = "select sum(nilai_anggaran) as hasil_kali
                from " . sfConfig::get('app_default_schema') . ".november3_rincian_detail 
                where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and kode_sub='$kodeSub' and status_hapus=false";
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query2);
        $t = $stmt->executeQuery();
        while ($t->next()) {
            $hasilKali = $t->getString('hasil_kali');
        }
        return $hasilKali;
    }

    public function getTotalSub($sub) {
        $unit_id = $this->getUnitId();
        $kegiatan_code = $this->getKegiatanCode();
        $subKeg = $sub;
        $query2 = $query2 = "select sum(nilai_anggaran) as hasil_kali
                        from " . sfConfig::get('app_default_schema') . ".november3_rincian_detail
                        where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and sub ilike '%$subKeg%' and status_hapus=false";
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query2);
        $t = $stmt->executeQuery();
        while ($t->next()) {
            $hasilKali = $t->getString('hasil_kali');
        }
        return $hasilKali;
    }

///sepertinya tidak dipakai
    public function getCekVolume4cekPaguRincian($unit_id, $kegiatan_code, $detail_no, $volume) {
        $queryPagu = "select sum(alokasi_dana) as nilai from " . sfConfig::get('app_default_schema') . ".november3_master_kegiatan where unit_id='$unit_id'";
        $queryRd = "select sum(volume * komponen_harga_awal * (100 + pajak) / 100) as nilai from " . sfConfig::get('app_default_schema') . ".november3_rincian_detail where unit_id='$unit_id' and status_hapus=FALSE";
        $con = Propel::getConnection();
        $statement0 = $con->prepareStatement($queryPagu);
        $rs_nilai = $statement0->executeQuery();
        while ($rs_nilai->next()) {
            $nilai_awal = $rs_nilai->getString('nilai');
        }

        $statement2 = $con->prepareStatement($queryRd);
        $rs_nilai2 = $statement2->executeQuery();
        while ($rs_nilai2->next()) {
            $nilai_rincian = $rs_nilai2->getString('nilai');
        }

        $querycekVolume = "select volume from " . sfConfig::get('app_default_schema') . ".november3_rincian_detail where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and detail_no='$detail_no' and status_hapus=FALSE";
        $statement = $con->prepareStatement($querycekVolume);
        $rs_volume = $statement->executeQuery();
        while ($rs_volume->next()) {
            $volume_rd = $rs_volume->getString('volume');
        }
        //tolak=1, lolos=0;

        if (($nilai_awal < $nilai_rincian) && ($volume > $volume_rd)) {
            $backValue = '1';
        } else {
            $backValue = '0';
        }
        //$tampung = $nilai_awal.' '.$nilai_rincian.' '.$volume.' '.$volume_rd.' '.$backValue;

        return $backValue;
    }

    //irul 3april 2014 
    public function getCekLelang($unit_id, $kegiatan_code, $detail_no) {
        $i = 0;
        $pekerjaan = $unit_id . '.' . $kegiatan_code . '.' . $detail_no;
        $query = "select vpd.status_lelang as lelang "
                . "from v_pekerjaan_dilelang vpd, " . sfConfig::get('app_default_eproject') . ".detail_pekerjaan dp, " . sfConfig::get('app_default_eproject') . ".detail_kegiatan dk "
                . "where vpd.id = dp.pekerjaan_id and dp.detail_kegiatan_id = dk.id and dk.kode_detail_kegiatan = '$pekerjaan' and vpd.status_lelang ilike '%Berjalan%'";
        $con = Propel::getConnection();
        $statement = $con->prepareStatement($query);
        $rs = $statement->executeQuery();
        while ($rs->next()) {
            $i++;
        }
        if ($i == 0) {
            return 0;
        } else {
            return 1;
        }
    }

    //irul 3april 2014 
    //irul 20april 2014 
    public function getCekNilaiDetKomMCDelivery($unit_id, $kode_kegiatan, $detail_no) {
        $totNilaiKontrak = 0;
        $query2 = "select rd.unit_id,rd.kegiatan_code,rd.detail_no,rd.subtitle,rd.komponen_name,rd.kode_sub,
				 de.kode_detail_kegiatan,dk.detail_swakelola_id,dk.kontrak_id,
				 sum(rd.nilai_anggaran) as nilai_budgeting,
				 sum(round(dk.harga_realisasi)) as nilai_kontrak

				from " . sfConfig::get('app_default_edelivery') . ".detail_komponen dk,
                                     " . sfConfig::get('app_default_eproject') . ".detail_kegiatan de,
                                     " . sfConfig::get('app_default_schema') . ".november3_rincian_detail rd

				where rd.unit_id='$unit_id' and rd.kegiatan_code='$kode_kegiatan' and rd.detail_no='$detail_no'
				      and rd.volume>0 and rd.status_hapus=FALSE
				      and de.id=dk.detail_kegiatan_id 
                                      and de.kode_detail_kegiatan=rd.unit_id||'.'||rd.kegiatan_code||'.'||rd.detail_no

				group by rd.unit_id,rd.kegiatan_code,rd.detail_no,rd.subtitle,rd.komponen_name,rd.kode_sub,
				      de.kode_detail_kegiatan,dk.detail_swakelola_id,dk.kontrak_id
				      having  sum(round(dk.harga_realisasi))>0
				      order by rd.unit_id,rd.kegiatan_code,rd.detail_no";
//print_r($query2);exit;

        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query2);
        $t = $stmt->executeQuery();
        while ($t->next()) {
            $jumlahRows = $t->getRow();
            if ($t->getString('nilai_kontrak') > 0) {
                $totNilaiKontrak = $totNilaiKontrak + $t->getString('nilai_kontrak');
                //print_r($totNilaiKontrak);exit;
                //$warning=$totNilaiKontrak;
            }
        }
        return $totNilaiKontrak;
    }

    //irul 20april 2014 
}
