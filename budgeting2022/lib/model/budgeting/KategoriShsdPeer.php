<?php

/**
 * Subclass for performing query and update operations on the 'budget2010.kategori_shsd' table.
 *
 * 
 *
 * @package lib.model.budget2010
 */
class KategoriShsdPeer extends BaseKategoriShsdPeer {
    public static function getStringKategoriShsdName($kategori_id, $con = null) {
        if ($con === null) {
            $con = Propel::getConnection(self::DATABASE_NAME);
        }
        $c = new Criteria();
        $c->addSelectColumn(self::KATEGORI_SHSD_NAME);
        $c->add(KategoriShsdPeer::KATEGORI_SHSD_ID, $kategori_id, Criteria::EQUAL);
        $rs = KategoriShsdPeer::doSelectRS($c, $con);
        if ($rs->next()) {
            $rt = $rs->get(1);
        }

        return $rt;
    }
}
