<?php

/**
 * Subclass for representing a row from the 'budget2010.master_kegiatan' table.
 *
 * 
 *
 * @package lib.model.budget2010
 */ 
class MasterKegiatan extends BaseMasterKegiatan
{
    
    public function getKodeNamaKegiatan()
    {
        return $this->getKodeKegiatan().'-'.$this->getNamaKegiatan();
    }
}

