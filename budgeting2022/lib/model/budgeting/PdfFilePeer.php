<?php

/**
 * Subclass for performing query and update operations on the 'budget2012.pdf_file' table.
 *
 * 
 *
 * @package lib.model.budgeting
 */ 
class PdfFilePeer extends BasePdfFilePeer
{
}
