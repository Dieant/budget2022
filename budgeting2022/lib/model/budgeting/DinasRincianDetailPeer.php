<?php

/**
 * Subclass for performing query and update operations on the 'ebudget.dinas_rincian_detail' table.
 *
 * 
 *
 * @package lib.model.budgeting
 */
class DinasRincianDetailPeer extends BaseDinasRincianDetailPeer {

    public static function AmbilUrutanAkrual($kode_akrual_komponen) {
        $c = new Criteria();
        $c->add(DinasRincianDetailPeer::AKRUAL_CODE, $kode_akrual_komponen . '%', Criteria::ILIKE);
        $akrual = DinasRincianDetailPeer::doCount($c);
        if ($akrual < 9) {
            return '|0' . ($akrual + 1);
        } else {
            return '|' . ($akrual + 1);
        }
    }

}
