<?php

/**
 * Subclass for performing query and update operations on the 'budget2010.komponen' table.
 *
 * 
 *
 * @package lib.model.budget2010
 */
class KomponenPeer extends BaseKomponenPeer {

    public static function buatKodeNamaAkrual($kodeAkrual) {
        $kode = explode('|', $kodeAkrual);
        $c = new Criteria();
        $c->add(AkrualPeer::AKRUAL_CODE, $kode[0]);
        if ($akrual = AkrualPeer::doSelectOne($c)) {
            return str_replace('|', '.', $kodeAkrual) . ' - ' . $akrual->getNama();
        } else {
            return;
        }
    }

    public static function buatKodeNamaAkrualLima($kodeAkrual) {
        $kode = explode('|', $kodeAkrual);
        $sesuatu = substr($kode[0], 0, 11);
        $c = new Criteria();
        $c->add(AkrualPeer::AKRUAL_CODE, $sesuatu);
        if ($akrual = AkrualPeer::doSelectOne($c))
            return $sesuatu . ' - ' . $akrual->getNama();
        else
            return;
    }

}
