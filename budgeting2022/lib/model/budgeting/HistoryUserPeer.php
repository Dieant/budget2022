<?php

/**
 * Subclass for performing query and update operations on the 'ebudget.history_user' table.
 *
 * 
 *
 * @package lib.model.budgeting
 */ 
class HistoryUserPeer extends BaseHistoryUserPeer
{
}
