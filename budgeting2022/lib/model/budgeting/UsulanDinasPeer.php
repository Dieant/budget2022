<?php

/**
 * Subclass for performing query and update operations on the 'ebudget.usulan_dinas' table.
 *
 * 
 *
 * @package lib.model.budgeting
 */
class UsulanDinasPeer extends BaseUsulanDinasPeer {

    public static function getStringFilepath($id_spjm, $con = null) {
        if ($con === null) {
            $con = Propel::getConnection(self::DATABASE_NAME);
        }
        $c = new Criteria();
        $c->addSelectColumn(self::FILEPATH_RAR);
        $c->add(UsulanSPJMPeer::ID_SPJM, $id_spjm, Criteria::EQUAL);
        $c->addJoin(UsulanDinasPeer::NOMOR_SURAT, UsulanSPJMPeer::NOMOR_SURAT, Criteria::INNER_JOIN);  
        $c->add(UsulanDinasPeer::STATUS_HAPUS, FALSE);
        $rs = UsulanSPJMPeer::doSelectRS($c, $con);
        if ($rs->next()) {
            $rt = $rs->get(1);
        }

        return $rt;
    }

}
