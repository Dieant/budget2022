<?php

/**
 * Subclass for performing query and update operations on the 'unit_kerja' table.
 *
 * 
 *
 * @package lib.model.budget2010
 */
class UnitKerjaPeer extends BaseUnitKerjaPeer {

    public static function getStringUnitKerja($unitId, $con = null) {
        if ($con === null) {
            $con = Propel::getConnection(self::DATABASE_NAME);
        }
        $c = new Criteria();
        $c->addSelectColumn(self::UNIT_NAME);
        $c->add(UnitKerjaPeer::UNIT_ID, $unitId, Criteria::EQUAL);
        $rs = UnitKerjaPeer::doSelectRS($c, $con);
        if ($rs->next()) {
            $rt = $rs->get(1);
        }

        return $rt;
    }

}
