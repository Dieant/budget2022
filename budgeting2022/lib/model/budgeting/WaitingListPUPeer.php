<?php

/**
 * Subclass for performing query and update operations on the 'ebudget.waitinglist_pu' table.
 *
 * 
 *
 * @package lib.model.budgeting
 */ 
class WaitingListPUPeer extends BaseWaitingListPUPeer
{
}
