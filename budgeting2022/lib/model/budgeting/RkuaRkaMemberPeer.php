<?php

/**
 * Subclass for performing query and update operations on the 'ebudget.rkua_rka_member' table.
 *
 * 
 *
 * @package lib.model.budgeting
 */ 
class RkuaRkaMemberPeer extends BaseRkuaRkaMemberPeer
{
}
