<?php

/**
 * Subclass for representing a row from the 'ebudget.akrual' table.
 *
 * 
 *
 * @package lib.model.budgeting
 */ 
class Akrual extends BaseAkrual
{
    public function getAkrualKodeNama() {
        return $this->getAkrualCode() . ' - ' . $this->getNama();
    }
}
