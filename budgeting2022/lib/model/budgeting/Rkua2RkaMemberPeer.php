<?php

/**
 * Subclass for performing query and update operations on the 'ebudget.rkua2_rka_member' table.
 *
 * 
 *
 * @package lib.model.budgeting
 */ 
class Rkua2RkaMemberPeer extends BaseRkua2RkaMemberPeer
{
}
