<?php

/**
 * Subclass for performing query and update operations on the 'ebudget.deskripsi_resume_rapat' table.
 *
 * 
 *
 * @package lib.model.budgeting
 */
class DeskripsiResumeRapatPeer extends BaseDeskripsiResumeRapatPeer {

    public static function saveResumeRapat($unit_id, $kode_kegiatan) {
        $con = Propel::getConnection();
        $tahap = DinasMasterKegiatanPeer::getTahapKegiatan($unit_id, $kode_kegiatan);

        //kegiatan yang akan disimpan ditaruh di array awal
        $kegiatans = array();
        $rekenings = array();

        $c = new Criteria();
        $c->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
        $c->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
        $rs_kegiatan = DinasMasterKegiatanPeer::doSelectOne($c);
        $sisipan = $rs_kegiatan->getIsPernahRka();
        if ($sisipan) {
            $tabel_dpn = 'prev_';
        } else {
            $tabel_dpn = '';
        }

        $data = array(
            'kode' => $rs_kegiatan->getKodeKegiatan(),
            'nama' => $rs_kegiatan->getNamaKegiatan(),
            'catatan_pembahasan' => $rs_kegiatan->getCatatanPembahasan()
        );
        array_push($kegiatans, $data);

        //cek F1 apa tidak
        $query = "select count(*) as jml
                from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail
                where kegiatan_code='$kode_kegiatan' and unit_id='$unit_id' and 
                status_level between 4 and 7 and status_hapus=false";
        $stmt = $con->prepareStatement($query);
        $rs = $stmt->executeQuery();
        while ($rs->next()) {
            $jml_komponen = $rs->getString('jml');
        }
        if ($jml_komponen == 0) {
            $data = array(
                'kegiatan_code' => $kode_kegiatan,
                'rekening_name' => 0,
                'rekening_code' => 0,
                'semula' => 0,
                'menjadi' => 0
            );
            array_push($rekenings, $data);
        } else {
            //ambil kode rekening yang berubah
            $kode_rekenings = array();
            $query = "(
                        select distinct rekening_code
                        from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail
                        where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and status_level between 4 and 7 and status_hapus=false
                    )union(
                        select distinct a.rekening_code
                        from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "rincian_detail a, " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail b
                        where a.unit_id=b.unit_id and a.kegiatan_code=b.kegiatan_code and a.detail_no=b.detail_no and
                        b.unit_id='$unit_id' and b.kegiatan_code='$kode_kegiatan' and b.status_level between 4 and 7 and b.status_hapus=false
                    )";
            $stmt = $con->prepareStatement($query);
            $rs = $stmt->executeQuery();
            while ($rs->next()) {
                $kode_rekenings[] = "'" . $rs->getString('rekening_code') . "'";
            }
            $kumpulan_kode_rekening = implode(',', $kode_rekenings);

            //ambil nilai per rekening
            $query = "select r.rekening_code, r.rekening_name, coalesce(b.nilai,0) as semula, coalesce(a.nilai,0) as menjadi
                    from " . sfConfig::get('app_default_schema') . ".rekening r, (select rekening_code, sum(nilai_anggaran) as nilai
                        from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail
                        where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rekening_code in ($kumpulan_kode_rekening) and status_hapus=false
                        group by rekening_code) a
                    FULL OUTER JOIN (select rekening_code, sum(nilai_anggaran) as nilai
                        from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "rincian_detail
                        where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rekening_code in ($kumpulan_kode_rekening) and status_hapus=false
                        group by rekening_code) b
                    ON a.rekening_code=b.rekening_code
                    where b.rekening_code=r.rekening_code or a.rekening_code=r.rekening_code
                    order by rekening_code";
            $stmt = $con->prepareStatement($query);
            $rs = $stmt->executeQuery();
            while ($rs->next()) {
                $data = array(
                    'kegiatan_code' => $kode_kegiatan,
                    'rekening_name' => $rs->getString('rekening_name'),
                    'rekening_code' => $rs->getString('rekening_code'),
                    'semula' => $rs->getString('semula'),
                    'menjadi' => $rs->getString('menjadi')
                );
                array_push($rekenings, $data);
            }
        }

        //jika Resume Rapat sudah ada sebelumnya -> ambil kegiatan+rekening selain yang akan disimpan
        $c = new Criteria();
        $c->add(DeskripsiResumeRapatPeer::UNIT_ID, $unit_id);
        $c->add(DeskripsiResumeRapatPeer::TAHAP, $tahap);
        $c->addDescendingOrderByColumn(DeskripsiResumeRapatPeer::ID);
        if ($rs_drp = DeskripsiResumeRapatPeer::doSelectOne($c)) {
            $id_max = $rs_drp->getId();

            $c = new Criteria();
            $c->add(DeskripsiResumeRapatKegiatanPeer::ID_DESKRIPSI_RESUME_RAPAT, $id_max);
            $c->add(DeskripsiResumeRapatKegiatanPeer::KEGIATAN_CODE, $kode_kegiatan, Criteria::NOT_EQUAL);
            $rs_drp_kegiatan = DeskripsiResumeRapatKegiatanPeer::doSelect($c);
            foreach ($rs_drp_kegiatan as $kegiatan) {
                $data = array(
                    'kode' => $kegiatan->getKegiatanCode(),
                    'nama' => $kegiatan->getKegiatanName(),
                    'catatan_pembahasan' => $kegiatan->getCatatanPembahasan()
                );
                array_push($kegiatans, $data);
            }

            $c = new Criteria();
            $c->add(DeskripsiResumeRapatRekeningPeer::ID_DESKRIPSI_RESUME_RAPAT, $id_max);
            $c->add(DeskripsiResumeRapatRekeningPeer::KEGIATAN_CODE, $kode_kegiatan, Criteria::NOT_EQUAL);
            $rs_drp_rekening = DeskripsiResumeRapatRekeningPeer::doSelect($c);
            foreach ($rs_drp_rekening as $rekening) {
                $data = array(
                    'kegiatan_code' => $rekening->getKegiatanCode(),
                    'rekening_name' => $rekening->getRekeningName(),
                    'rekening_code' => $rekening->getRekeningCode(),
                    'semula' => $rekening->getSemula(),
                    'menjadi' => $rekening->getMenjadi()
                );
                array_push($rekenings, $data);
            }
        }

        //proses SIMPAN ke resume rapat
        try {
            $con->begin();
            $query_qr = " select max(id) as id from " . sfConfig::get('app_default_schema') . ".deskripsi_resume_rapat";
            $stmt_qr = $con->prepareStatement($query_qr);
            $rs_id = $stmt_qr->executeQuery();
            if ($rs_id->next()) {
                $id_baru = $rs_id->getInt('id') + 1;
            } else {
                $id_baru = 1;
            }
            $token = md5(rand());
            $rs_qr = new DeskripsiResumeRapat();
            $rs_qr->setId($id_baru);
            $rs_qr->setUnitId($unit_id);
            $rs_qr->setTahap($tahap);
            $rs_qr->setToken($token);
            $rs_qr->save();
            //simpan di deskripsi_resume_rapat_kegiatan
            foreach ($kegiatans as $kegiatan) {
                $c_cek = new Criteria();
                $c_cek->add(DeskripsiResumeRapatKegiatanPeer::ID_DESKRIPSI_RESUME_RAPAT, $id_baru);
                $c_cek->add(DeskripsiResumeRapatKegiatanPeer::KEGIATAN_CODE, $kegiatan['kode']);

                $insert_kegiatan = new DeskripsiResumeRapatKegiatan();
                $insert_kegiatan->setIdDeskripsiResumeRapat($id_baru);
                $insert_kegiatan->setKegiatanCode($kegiatan['kode']);
                $insert_kegiatan->setKegiatanName($kegiatan['nama']);
                $insert_kegiatan->setCatatanPembahasan($kegiatan['catatan_pembahasan']);
                $insert_kegiatan->save();
            }

            //simpan di deskripsi_resume_rapat_rekening
            foreach ($rekenings as $rekening) {
                $insert_rekening = new DeskripsiResumeRapatRekening();
                $insert_rekening->setIdDeskripsiResumeRapat($id_baru);
                $insert_rekening->setKegiatanCode($rekening['kegiatan_code']);
                $insert_rekening->setRekeningCode($rekening['rekening_code']);
                $insert_rekening->setRekeningName($rekening['rekening_name']);
                $insert_rekening->setSemula($rekening['semula']);
                $insert_rekening->setMenjadi($rekening['menjadi']);
                $insert_rekening->save();
            }

            $con->commit();
            return TRUE;
        } catch (Exception $ex) {
            $con->rollback();
            return FALSE;
        }
    }

//    public function getTahapKegiatan($unit_id, $kode_kegiatan) {
//        $c_tahap = new Criteria();
//        $c_tahap->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
//        $c_tahap->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
//        $tahap_kegiatan = DinasMasterKegiatanPeer::doSelectOne($c_tahap);
//        if ($tahap_kegiatan->getTahap() == 'murni') {
//            return 0;
//        } elseif ($tahap_kegiatan->getTahap() == 'revisi1') {
//            return 1;
//        } elseif ($tahap_kegiatan->getTahap() == 'revisi2') {
//            return 2;
//        } elseif ($tahap_kegiatan->getTahap() == 'revisi3') {
//            return 3;
//        } elseif ($tahap_kegiatan->getTahap() == 'revisi4') {
//            return 4;
//        } elseif ($tahap_kegiatan->getTahap() == 'revisi5') {
//            return 5;
//        } elseif ($tahap_kegiatan->getTahap() == 'pak') {
//            return 10;
//        }
//        return 0;
//    }
}
