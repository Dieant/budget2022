<?php

/**
 * Subclass for representing a row from the 'budget2012.revisi2_master_kegiatan' table.
 *
 * 
 *
 * @package lib.model.budgeting
 */ 
class Revisi2MasterKegiatan extends BaseRevisi2MasterKegiatan
{
    public function getKodeNamaKegiatan()
    {
        return $this->getKodeKegiatan().'-'.$this->getNamaKegiatan();
    }
}
