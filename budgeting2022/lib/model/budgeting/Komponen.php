<?php

/**
 * Subclass for representing a row from the 'budget2010.komponen' table.
 *
 * 
 *
 * @package lib.model.budget2010
 */ 
class Komponen extends BaseKomponen
{
    public function getNamaRekening()
    {
        $rekening_codes= trim($this->getRekening());
        
        $arr_rekening=explode("/", $rekening_codes);
        $count_arr_rekening= count($arr_rekening);
        $result_arr_rekening='';
        if($rekening_codes == ''){
            $result_arr_rekening='';
        }else{
            for($i=0;$i<$count_arr_rekening;$i++)
            {
               if($i==0){
                    $c_rekening= new Criteria();
                    $c_rekening->add(RekeningPeer::REKENING_CODE,str_replace(',', '', trim($arr_rekening[$i])));
                    $rs_rekening=RekeningPeer::doSelectOne($c_rekening);
                    $result_arr_rekening= trim($arr_rekening[$i]).' ('.$rs_rekening->getRekeningName().')';
               }else if($i!=0){
                    $c_rekening= new Criteria();
                    $c_rekening->add(RekeningPeer::REKENING_CODE,str_replace(',', '', trim($arr_rekening[$i])));
                    $rs_rekening=RekeningPeer::doSelectOne($c_rekening);
                    $result_arr_rekening=$result_arr_rekening.'<br>'.trim($arr_rekening[$i]).' ('.$rs_rekening->getRekeningName().')';
                }
            }
        }
        return $result_arr_rekening;
    } 
}
