<?php

/**
 * Subclass for performing query and update operations on the 'ebudget.upload_bpk' table.
 *
 * 
 *
 * @package lib.model.budgeting
 */ 
class UploadBpkPeer extends BaseUploadBpkPeer
{
}
