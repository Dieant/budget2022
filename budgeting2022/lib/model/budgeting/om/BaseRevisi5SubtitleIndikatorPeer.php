<?php


abstract class BaseRevisi5SubtitleIndikatorPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.revisi5_subtitle_indikator';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.Revisi5SubtitleIndikator';

	
	const NUM_COLUMNS = 22;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const UNIT_ID = 'ebudget.revisi5_subtitle_indikator.UNIT_ID';

	
	const KEGIATAN_CODE = 'ebudget.revisi5_subtitle_indikator.KEGIATAN_CODE';

	
	const SUBTITLE = 'ebudget.revisi5_subtitle_indikator.SUBTITLE';

	
	const INDIKATOR = 'ebudget.revisi5_subtitle_indikator.INDIKATOR';

	
	const NILAI = 'ebudget.revisi5_subtitle_indikator.NILAI';

	
	const SATUAN = 'ebudget.revisi5_subtitle_indikator.SATUAN';

	
	const LAST_UPDATE_USER = 'ebudget.revisi5_subtitle_indikator.LAST_UPDATE_USER';

	
	const LAST_UPDATE_TIME = 'ebudget.revisi5_subtitle_indikator.LAST_UPDATE_TIME';

	
	const LAST_UPDATE_IP = 'ebudget.revisi5_subtitle_indikator.LAST_UPDATE_IP';

	
	const TAHAP = 'ebudget.revisi5_subtitle_indikator.TAHAP';

	
	const SUB_ID = 'ebudget.revisi5_subtitle_indikator.SUB_ID';

	
	const TAHUN = 'ebudget.revisi5_subtitle_indikator.TAHUN';

	
	const LOCK_SUBTITLE = 'ebudget.revisi5_subtitle_indikator.LOCK_SUBTITLE';

	
	const PRIORITAS = 'ebudget.revisi5_subtitle_indikator.PRIORITAS';

	
	const CATATAN = 'ebudget.revisi5_subtitle_indikator.CATATAN';

	
	const LAKILAKI = 'ebudget.revisi5_subtitle_indikator.LAKILAKI';

	
	const PEREMPUAN = 'ebudget.revisi5_subtitle_indikator.PEREMPUAN';

	
	const DEWASA = 'ebudget.revisi5_subtitle_indikator.DEWASA';

	
	const ANAK = 'ebudget.revisi5_subtitle_indikator.ANAK';

	
	const LANSIA = 'ebudget.revisi5_subtitle_indikator.LANSIA';

	
	const INKLUSI = 'ebudget.revisi5_subtitle_indikator.INKLUSI';

	
	const GENDER = 'ebudget.revisi5_subtitle_indikator.GENDER';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('UnitId', 'KegiatanCode', 'Subtitle', 'Indikator', 'Nilai', 'Satuan', 'LastUpdateUser', 'LastUpdateTime', 'LastUpdateIp', 'Tahap', 'SubId', 'Tahun', 'LockSubtitle', 'Prioritas', 'Catatan', 'Lakilaki', 'Perempuan', 'Dewasa', 'Anak', 'Lansia', 'Inklusi', 'Gender', ),
		BasePeer::TYPE_COLNAME => array (Revisi5SubtitleIndikatorPeer::UNIT_ID, Revisi5SubtitleIndikatorPeer::KEGIATAN_CODE, Revisi5SubtitleIndikatorPeer::SUBTITLE, Revisi5SubtitleIndikatorPeer::INDIKATOR, Revisi5SubtitleIndikatorPeer::NILAI, Revisi5SubtitleIndikatorPeer::SATUAN, Revisi5SubtitleIndikatorPeer::LAST_UPDATE_USER, Revisi5SubtitleIndikatorPeer::LAST_UPDATE_TIME, Revisi5SubtitleIndikatorPeer::LAST_UPDATE_IP, Revisi5SubtitleIndikatorPeer::TAHAP, Revisi5SubtitleIndikatorPeer::SUB_ID, Revisi5SubtitleIndikatorPeer::TAHUN, Revisi5SubtitleIndikatorPeer::LOCK_SUBTITLE, Revisi5SubtitleIndikatorPeer::PRIORITAS, Revisi5SubtitleIndikatorPeer::CATATAN, Revisi5SubtitleIndikatorPeer::LAKILAKI, Revisi5SubtitleIndikatorPeer::PEREMPUAN, Revisi5SubtitleIndikatorPeer::DEWASA, Revisi5SubtitleIndikatorPeer::ANAK, Revisi5SubtitleIndikatorPeer::LANSIA, Revisi5SubtitleIndikatorPeer::INKLUSI, Revisi5SubtitleIndikatorPeer::GENDER, ),
		BasePeer::TYPE_FIELDNAME => array ('unit_id', 'kegiatan_code', 'subtitle', 'indikator', 'nilai', 'satuan', 'last_update_user', 'last_update_time', 'last_update_ip', 'tahap', 'sub_id', 'tahun', 'lock_subtitle', 'prioritas', 'catatan', 'lakilaki', 'perempuan', 'dewasa', 'anak', 'lansia', 'inklusi', 'gender', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('UnitId' => 0, 'KegiatanCode' => 1, 'Subtitle' => 2, 'Indikator' => 3, 'Nilai' => 4, 'Satuan' => 5, 'LastUpdateUser' => 6, 'LastUpdateTime' => 7, 'LastUpdateIp' => 8, 'Tahap' => 9, 'SubId' => 10, 'Tahun' => 11, 'LockSubtitle' => 12, 'Prioritas' => 13, 'Catatan' => 14, 'Lakilaki' => 15, 'Perempuan' => 16, 'Dewasa' => 17, 'Anak' => 18, 'Lansia' => 19, 'Inklusi' => 20, 'Gender' => 21, ),
		BasePeer::TYPE_COLNAME => array (Revisi5SubtitleIndikatorPeer::UNIT_ID => 0, Revisi5SubtitleIndikatorPeer::KEGIATAN_CODE => 1, Revisi5SubtitleIndikatorPeer::SUBTITLE => 2, Revisi5SubtitleIndikatorPeer::INDIKATOR => 3, Revisi5SubtitleIndikatorPeer::NILAI => 4, Revisi5SubtitleIndikatorPeer::SATUAN => 5, Revisi5SubtitleIndikatorPeer::LAST_UPDATE_USER => 6, Revisi5SubtitleIndikatorPeer::LAST_UPDATE_TIME => 7, Revisi5SubtitleIndikatorPeer::LAST_UPDATE_IP => 8, Revisi5SubtitleIndikatorPeer::TAHAP => 9, Revisi5SubtitleIndikatorPeer::SUB_ID => 10, Revisi5SubtitleIndikatorPeer::TAHUN => 11, Revisi5SubtitleIndikatorPeer::LOCK_SUBTITLE => 12, Revisi5SubtitleIndikatorPeer::PRIORITAS => 13, Revisi5SubtitleIndikatorPeer::CATATAN => 14, Revisi5SubtitleIndikatorPeer::LAKILAKI => 15, Revisi5SubtitleIndikatorPeer::PEREMPUAN => 16, Revisi5SubtitleIndikatorPeer::DEWASA => 17, Revisi5SubtitleIndikatorPeer::ANAK => 18, Revisi5SubtitleIndikatorPeer::LANSIA => 19, Revisi5SubtitleIndikatorPeer::INKLUSI => 20, Revisi5SubtitleIndikatorPeer::GENDER => 21, ),
		BasePeer::TYPE_FIELDNAME => array ('unit_id' => 0, 'kegiatan_code' => 1, 'subtitle' => 2, 'indikator' => 3, 'nilai' => 4, 'satuan' => 5, 'last_update_user' => 6, 'last_update_time' => 7, 'last_update_ip' => 8, 'tahap' => 9, 'sub_id' => 10, 'tahun' => 11, 'lock_subtitle' => 12, 'prioritas' => 13, 'catatan' => 14, 'lakilaki' => 15, 'perempuan' => 16, 'dewasa' => 17, 'anak' => 18, 'lansia' => 19, 'inklusi' => 20, 'gender' => 21, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/Revisi5SubtitleIndikatorMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.Revisi5SubtitleIndikatorMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = Revisi5SubtitleIndikatorPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(Revisi5SubtitleIndikatorPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(Revisi5SubtitleIndikatorPeer::UNIT_ID);

		$criteria->addSelectColumn(Revisi5SubtitleIndikatorPeer::KEGIATAN_CODE);

		$criteria->addSelectColumn(Revisi5SubtitleIndikatorPeer::SUBTITLE);

		$criteria->addSelectColumn(Revisi5SubtitleIndikatorPeer::INDIKATOR);

		$criteria->addSelectColumn(Revisi5SubtitleIndikatorPeer::NILAI);

		$criteria->addSelectColumn(Revisi5SubtitleIndikatorPeer::SATUAN);

		$criteria->addSelectColumn(Revisi5SubtitleIndikatorPeer::LAST_UPDATE_USER);

		$criteria->addSelectColumn(Revisi5SubtitleIndikatorPeer::LAST_UPDATE_TIME);

		$criteria->addSelectColumn(Revisi5SubtitleIndikatorPeer::LAST_UPDATE_IP);

		$criteria->addSelectColumn(Revisi5SubtitleIndikatorPeer::TAHAP);

		$criteria->addSelectColumn(Revisi5SubtitleIndikatorPeer::SUB_ID);

		$criteria->addSelectColumn(Revisi5SubtitleIndikatorPeer::TAHUN);

		$criteria->addSelectColumn(Revisi5SubtitleIndikatorPeer::LOCK_SUBTITLE);

		$criteria->addSelectColumn(Revisi5SubtitleIndikatorPeer::PRIORITAS);

		$criteria->addSelectColumn(Revisi5SubtitleIndikatorPeer::CATATAN);

		$criteria->addSelectColumn(Revisi5SubtitleIndikatorPeer::LAKILAKI);

		$criteria->addSelectColumn(Revisi5SubtitleIndikatorPeer::PEREMPUAN);

		$criteria->addSelectColumn(Revisi5SubtitleIndikatorPeer::DEWASA);

		$criteria->addSelectColumn(Revisi5SubtitleIndikatorPeer::ANAK);

		$criteria->addSelectColumn(Revisi5SubtitleIndikatorPeer::LANSIA);

		$criteria->addSelectColumn(Revisi5SubtitleIndikatorPeer::INKLUSI);

		$criteria->addSelectColumn(Revisi5SubtitleIndikatorPeer::GENDER);

	}

	const COUNT = 'COUNT(ebudget.revisi5_subtitle_indikator.SUB_ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.revisi5_subtitle_indikator.SUB_ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(Revisi5SubtitleIndikatorPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(Revisi5SubtitleIndikatorPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = Revisi5SubtitleIndikatorPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = Revisi5SubtitleIndikatorPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return Revisi5SubtitleIndikatorPeer::populateObjects(Revisi5SubtitleIndikatorPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			Revisi5SubtitleIndikatorPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = Revisi5SubtitleIndikatorPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return Revisi5SubtitleIndikatorPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}

		$criteria->remove(Revisi5SubtitleIndikatorPeer::SUB_ID); 

				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(Revisi5SubtitleIndikatorPeer::SUB_ID);
			$selectCriteria->add(Revisi5SubtitleIndikatorPeer::SUB_ID, $criteria->remove(Revisi5SubtitleIndikatorPeer::SUB_ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(Revisi5SubtitleIndikatorPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(Revisi5SubtitleIndikatorPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof Revisi5SubtitleIndikator) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(Revisi5SubtitleIndikatorPeer::SUB_ID, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(Revisi5SubtitleIndikator $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(Revisi5SubtitleIndikatorPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(Revisi5SubtitleIndikatorPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(Revisi5SubtitleIndikatorPeer::DATABASE_NAME, Revisi5SubtitleIndikatorPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = Revisi5SubtitleIndikatorPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(Revisi5SubtitleIndikatorPeer::DATABASE_NAME);

		$criteria->add(Revisi5SubtitleIndikatorPeer::SUB_ID, $pk);


		$v = Revisi5SubtitleIndikatorPeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(Revisi5SubtitleIndikatorPeer::SUB_ID, $pks, Criteria::IN);
			$objs = Revisi5SubtitleIndikatorPeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BaseRevisi5SubtitleIndikatorPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/Revisi5SubtitleIndikatorMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.Revisi5SubtitleIndikatorMapBuilder');
}
