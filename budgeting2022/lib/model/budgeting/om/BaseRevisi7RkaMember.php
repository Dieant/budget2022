<?php


abstract class BaseRevisi7RkaMember extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $kode_sub;


	
	protected $unit_id;


	
	protected $kegiatan_code;


	
	protected $komponen_id;


	
	protected $komponen_name;


	
	protected $detail_name;


	
	protected $rekening_asli;


	
	protected $tahun;


	
	protected $detail_no;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getKodeSub()
	{

		return $this->kode_sub;
	}

	
	public function getUnitId()
	{

		return $this->unit_id;
	}

	
	public function getKegiatanCode()
	{

		return $this->kegiatan_code;
	}

	
	public function getKomponenId()
	{

		return $this->komponen_id;
	}

	
	public function getKomponenName()
	{

		return $this->komponen_name;
	}

	
	public function getDetailName()
	{

		return $this->detail_name;
	}

	
	public function getRekeningAsli()
	{

		return $this->rekening_asli;
	}

	
	public function getTahun()
	{

		return $this->tahun;
	}

	
	public function getDetailNo()
	{

		return $this->detail_no;
	}

	
	public function setKodeSub($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_sub !== $v) {
			$this->kode_sub = $v;
			$this->modifiedColumns[] = Revisi7RkaMemberPeer::KODE_SUB;
		}

	} 
	
	public function setUnitId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->unit_id !== $v) {
			$this->unit_id = $v;
			$this->modifiedColumns[] = Revisi7RkaMemberPeer::UNIT_ID;
		}

	} 
	
	public function setKegiatanCode($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kegiatan_code !== $v) {
			$this->kegiatan_code = $v;
			$this->modifiedColumns[] = Revisi7RkaMemberPeer::KEGIATAN_CODE;
		}

	} 
	
	public function setKomponenId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->komponen_id !== $v) {
			$this->komponen_id = $v;
			$this->modifiedColumns[] = Revisi7RkaMemberPeer::KOMPONEN_ID;
		}

	} 
	
	public function setKomponenName($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->komponen_name !== $v) {
			$this->komponen_name = $v;
			$this->modifiedColumns[] = Revisi7RkaMemberPeer::KOMPONEN_NAME;
		}

	} 
	
	public function setDetailName($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->detail_name !== $v) {
			$this->detail_name = $v;
			$this->modifiedColumns[] = Revisi7RkaMemberPeer::DETAIL_NAME;
		}

	} 
	
	public function setRekeningAsli($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->rekening_asli !== $v) {
			$this->rekening_asli = $v;
			$this->modifiedColumns[] = Revisi7RkaMemberPeer::REKENING_ASLI;
		}

	} 
	
	public function setTahun($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->tahun !== $v) {
			$this->tahun = $v;
			$this->modifiedColumns[] = Revisi7RkaMemberPeer::TAHUN;
		}

	} 
	
	public function setDetailNo($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->detail_no !== $v) {
			$this->detail_no = $v;
			$this->modifiedColumns[] = Revisi7RkaMemberPeer::DETAIL_NO;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->kode_sub = $rs->getString($startcol + 0);

			$this->unit_id = $rs->getString($startcol + 1);

			$this->kegiatan_code = $rs->getString($startcol + 2);

			$this->komponen_id = $rs->getString($startcol + 3);

			$this->komponen_name = $rs->getString($startcol + 4);

			$this->detail_name = $rs->getString($startcol + 5);

			$this->rekening_asli = $rs->getString($startcol + 6);

			$this->tahun = $rs->getString($startcol + 7);

			$this->detail_no = $rs->getInt($startcol + 8);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 9; 
		} catch (Exception $e) {
			throw new PropelException("Error populating Revisi7RkaMember object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(Revisi7RkaMemberPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			Revisi7RkaMemberPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(Revisi7RkaMemberPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = Revisi7RkaMemberPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += Revisi7RkaMemberPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = Revisi7RkaMemberPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = Revisi7RkaMemberPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getKodeSub();
				break;
			case 1:
				return $this->getUnitId();
				break;
			case 2:
				return $this->getKegiatanCode();
				break;
			case 3:
				return $this->getKomponenId();
				break;
			case 4:
				return $this->getKomponenName();
				break;
			case 5:
				return $this->getDetailName();
				break;
			case 6:
				return $this->getRekeningAsli();
				break;
			case 7:
				return $this->getTahun();
				break;
			case 8:
				return $this->getDetailNo();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = Revisi7RkaMemberPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getKodeSub(),
			$keys[1] => $this->getUnitId(),
			$keys[2] => $this->getKegiatanCode(),
			$keys[3] => $this->getKomponenId(),
			$keys[4] => $this->getKomponenName(),
			$keys[5] => $this->getDetailName(),
			$keys[6] => $this->getRekeningAsli(),
			$keys[7] => $this->getTahun(),
			$keys[8] => $this->getDetailNo(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = Revisi7RkaMemberPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setKodeSub($value);
				break;
			case 1:
				$this->setUnitId($value);
				break;
			case 2:
				$this->setKegiatanCode($value);
				break;
			case 3:
				$this->setKomponenId($value);
				break;
			case 4:
				$this->setKomponenName($value);
				break;
			case 5:
				$this->setDetailName($value);
				break;
			case 6:
				$this->setRekeningAsli($value);
				break;
			case 7:
				$this->setTahun($value);
				break;
			case 8:
				$this->setDetailNo($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = Revisi7RkaMemberPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setKodeSub($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setUnitId($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setKegiatanCode($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setKomponenId($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setKomponenName($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setDetailName($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setRekeningAsli($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setTahun($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setDetailNo($arr[$keys[8]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(Revisi7RkaMemberPeer::DATABASE_NAME);

		if ($this->isColumnModified(Revisi7RkaMemberPeer::KODE_SUB)) $criteria->add(Revisi7RkaMemberPeer::KODE_SUB, $this->kode_sub);
		if ($this->isColumnModified(Revisi7RkaMemberPeer::UNIT_ID)) $criteria->add(Revisi7RkaMemberPeer::UNIT_ID, $this->unit_id);
		if ($this->isColumnModified(Revisi7RkaMemberPeer::KEGIATAN_CODE)) $criteria->add(Revisi7RkaMemberPeer::KEGIATAN_CODE, $this->kegiatan_code);
		if ($this->isColumnModified(Revisi7RkaMemberPeer::KOMPONEN_ID)) $criteria->add(Revisi7RkaMemberPeer::KOMPONEN_ID, $this->komponen_id);
		if ($this->isColumnModified(Revisi7RkaMemberPeer::KOMPONEN_NAME)) $criteria->add(Revisi7RkaMemberPeer::KOMPONEN_NAME, $this->komponen_name);
		if ($this->isColumnModified(Revisi7RkaMemberPeer::DETAIL_NAME)) $criteria->add(Revisi7RkaMemberPeer::DETAIL_NAME, $this->detail_name);
		if ($this->isColumnModified(Revisi7RkaMemberPeer::REKENING_ASLI)) $criteria->add(Revisi7RkaMemberPeer::REKENING_ASLI, $this->rekening_asli);
		if ($this->isColumnModified(Revisi7RkaMemberPeer::TAHUN)) $criteria->add(Revisi7RkaMemberPeer::TAHUN, $this->tahun);
		if ($this->isColumnModified(Revisi7RkaMemberPeer::DETAIL_NO)) $criteria->add(Revisi7RkaMemberPeer::DETAIL_NO, $this->detail_no);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(Revisi7RkaMemberPeer::DATABASE_NAME);

		$criteria->add(Revisi7RkaMemberPeer::KODE_SUB, $this->kode_sub);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getKodeSub();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setKodeSub($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setUnitId($this->unit_id);

		$copyObj->setKegiatanCode($this->kegiatan_code);

		$copyObj->setKomponenId($this->komponen_id);

		$copyObj->setKomponenName($this->komponen_name);

		$copyObj->setDetailName($this->detail_name);

		$copyObj->setRekeningAsli($this->rekening_asli);

		$copyObj->setTahun($this->tahun);

		$copyObj->setDetailNo($this->detail_no);


		$copyObj->setNew(true);

		$copyObj->setKodeSub(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new Revisi7RkaMemberPeer();
		}
		return self::$peer;
	}

} 