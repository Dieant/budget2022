<?php


abstract class BaseDeskripsiResumeRapatRekening extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id_deskripsi_resume_rapat;


	
	protected $kegiatan_code;


	
	protected $rekening_code;


	
	protected $rekening_name;


	
	protected $semula;


	
	protected $menjadi;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getIdDeskripsiResumeRapat()
	{

		return $this->id_deskripsi_resume_rapat;
	}

	
	public function getKegiatanCode()
	{

		return $this->kegiatan_code;
	}

	
	public function getRekeningCode()
	{

		return $this->rekening_code;
	}

	
	public function getRekeningName()
	{

		return $this->rekening_name;
	}

	
	public function getSemula()
	{

		return $this->semula;
	}

	
	public function getMenjadi()
	{

		return $this->menjadi;
	}

	
	public function setIdDeskripsiResumeRapat($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id_deskripsi_resume_rapat !== $v) {
			$this->id_deskripsi_resume_rapat = $v;
			$this->modifiedColumns[] = DeskripsiResumeRapatRekeningPeer::ID_DESKRIPSI_RESUME_RAPAT;
		}

	} 
	
	public function setKegiatanCode($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kegiatan_code !== $v) {
			$this->kegiatan_code = $v;
			$this->modifiedColumns[] = DeskripsiResumeRapatRekeningPeer::KEGIATAN_CODE;
		}

	} 
	
	public function setRekeningCode($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->rekening_code !== $v) {
			$this->rekening_code = $v;
			$this->modifiedColumns[] = DeskripsiResumeRapatRekeningPeer::REKENING_CODE;
		}

	} 
	
	public function setRekeningName($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->rekening_name !== $v) {
			$this->rekening_name = $v;
			$this->modifiedColumns[] = DeskripsiResumeRapatRekeningPeer::REKENING_NAME;
		}

	} 
	
	public function setSemula($v)
	{

		if ($this->semula !== $v) {
			$this->semula = $v;
			$this->modifiedColumns[] = DeskripsiResumeRapatRekeningPeer::SEMULA;
		}

	} 
	
	public function setMenjadi($v)
	{

		if ($this->menjadi !== $v) {
			$this->menjadi = $v;
			$this->modifiedColumns[] = DeskripsiResumeRapatRekeningPeer::MENJADI;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id_deskripsi_resume_rapat = $rs->getInt($startcol + 0);

			$this->kegiatan_code = $rs->getString($startcol + 1);

			$this->rekening_code = $rs->getString($startcol + 2);

			$this->rekening_name = $rs->getString($startcol + 3);

			$this->semula = $rs->getFloat($startcol + 4);

			$this->menjadi = $rs->getFloat($startcol + 5);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 6; 
		} catch (Exception $e) {
			throw new PropelException("Error populating DeskripsiResumeRapatRekening object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(DeskripsiResumeRapatRekeningPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			DeskripsiResumeRapatRekeningPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(DeskripsiResumeRapatRekeningPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = DeskripsiResumeRapatRekeningPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += DeskripsiResumeRapatRekeningPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = DeskripsiResumeRapatRekeningPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = DeskripsiResumeRapatRekeningPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getIdDeskripsiResumeRapat();
				break;
			case 1:
				return $this->getKegiatanCode();
				break;
			case 2:
				return $this->getRekeningCode();
				break;
			case 3:
				return $this->getRekeningName();
				break;
			case 4:
				return $this->getSemula();
				break;
			case 5:
				return $this->getMenjadi();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = DeskripsiResumeRapatRekeningPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getIdDeskripsiResumeRapat(),
			$keys[1] => $this->getKegiatanCode(),
			$keys[2] => $this->getRekeningCode(),
			$keys[3] => $this->getRekeningName(),
			$keys[4] => $this->getSemula(),
			$keys[5] => $this->getMenjadi(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = DeskripsiResumeRapatRekeningPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setIdDeskripsiResumeRapat($value);
				break;
			case 1:
				$this->setKegiatanCode($value);
				break;
			case 2:
				$this->setRekeningCode($value);
				break;
			case 3:
				$this->setRekeningName($value);
				break;
			case 4:
				$this->setSemula($value);
				break;
			case 5:
				$this->setMenjadi($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = DeskripsiResumeRapatRekeningPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setIdDeskripsiResumeRapat($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setKegiatanCode($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setRekeningCode($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setRekeningName($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setSemula($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setMenjadi($arr[$keys[5]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(DeskripsiResumeRapatRekeningPeer::DATABASE_NAME);

		if ($this->isColumnModified(DeskripsiResumeRapatRekeningPeer::ID_DESKRIPSI_RESUME_RAPAT)) $criteria->add(DeskripsiResumeRapatRekeningPeer::ID_DESKRIPSI_RESUME_RAPAT, $this->id_deskripsi_resume_rapat);
		if ($this->isColumnModified(DeskripsiResumeRapatRekeningPeer::KEGIATAN_CODE)) $criteria->add(DeskripsiResumeRapatRekeningPeer::KEGIATAN_CODE, $this->kegiatan_code);
		if ($this->isColumnModified(DeskripsiResumeRapatRekeningPeer::REKENING_CODE)) $criteria->add(DeskripsiResumeRapatRekeningPeer::REKENING_CODE, $this->rekening_code);
		if ($this->isColumnModified(DeskripsiResumeRapatRekeningPeer::REKENING_NAME)) $criteria->add(DeskripsiResumeRapatRekeningPeer::REKENING_NAME, $this->rekening_name);
		if ($this->isColumnModified(DeskripsiResumeRapatRekeningPeer::SEMULA)) $criteria->add(DeskripsiResumeRapatRekeningPeer::SEMULA, $this->semula);
		if ($this->isColumnModified(DeskripsiResumeRapatRekeningPeer::MENJADI)) $criteria->add(DeskripsiResumeRapatRekeningPeer::MENJADI, $this->menjadi);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(DeskripsiResumeRapatRekeningPeer::DATABASE_NAME);

		$criteria->add(DeskripsiResumeRapatRekeningPeer::ID_DESKRIPSI_RESUME_RAPAT, $this->id_deskripsi_resume_rapat);
		$criteria->add(DeskripsiResumeRapatRekeningPeer::KEGIATAN_CODE, $this->kegiatan_code);
		$criteria->add(DeskripsiResumeRapatRekeningPeer::REKENING_CODE, $this->rekening_code);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		$pks = array();

		$pks[0] = $this->getIdDeskripsiResumeRapat();

		$pks[1] = $this->getKegiatanCode();

		$pks[2] = $this->getRekeningCode();

		return $pks;
	}

	
	public function setPrimaryKey($keys)
	{

		$this->setIdDeskripsiResumeRapat($keys[0]);

		$this->setKegiatanCode($keys[1]);

		$this->setRekeningCode($keys[2]);

	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setRekeningName($this->rekening_name);

		$copyObj->setSemula($this->semula);

		$copyObj->setMenjadi($this->menjadi);


		$copyObj->setNew(true);

		$copyObj->setIdDeskripsiResumeRapat(NULL); 
		$copyObj->setKegiatanCode(NULL); 
		$copyObj->setRekeningCode(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new DeskripsiResumeRapatRekeningPeer();
		}
		return self::$peer;
	}

} 