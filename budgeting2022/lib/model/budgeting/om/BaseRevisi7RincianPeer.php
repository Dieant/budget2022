<?php


abstract class BaseRevisi7RincianPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.revisi7_rincian';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.Revisi7Rincian';

	
	const NUM_COLUMNS = 16;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const KEGIATAN_CODE = 'ebudget.revisi7_rincian.KEGIATAN_CODE';

	
	const TIPE = 'ebudget.revisi7_rincian.TIPE';

	
	const RINCIAN_CONFIRMED = 'ebudget.revisi7_rincian.RINCIAN_CONFIRMED';

	
	const RINCIAN_CHANGED = 'ebudget.revisi7_rincian.RINCIAN_CHANGED';

	
	const RINCIAN_SELESAI = 'ebudget.revisi7_rincian.RINCIAN_SELESAI';

	
	const IP_ADDRESS = 'ebudget.revisi7_rincian.IP_ADDRESS';

	
	const WAKTU_ACCESS = 'ebudget.revisi7_rincian.WAKTU_ACCESS';

	
	const TARGET = 'ebudget.revisi7_rincian.TARGET';

	
	const UNIT_ID = 'ebudget.revisi7_rincian.UNIT_ID';

	
	const RINCIAN_LEVEL = 'ebudget.revisi7_rincian.RINCIAN_LEVEL';

	
	const LOCK = 'ebudget.revisi7_rincian.LOCK';

	
	const LAST_UPDATE_USER = 'ebudget.revisi7_rincian.LAST_UPDATE_USER';

	
	const LAST_UPDATE_TIME = 'ebudget.revisi7_rincian.LAST_UPDATE_TIME';

	
	const LAST_UPDATE_IP = 'ebudget.revisi7_rincian.LAST_UPDATE_IP';

	
	const TAHAP = 'ebudget.revisi7_rincian.TAHAP';

	
	const TAHUN = 'ebudget.revisi7_rincian.TAHUN';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('KegiatanCode', 'Tipe', 'RincianConfirmed', 'RincianChanged', 'RincianSelesai', 'IpAddress', 'WaktuAccess', 'Target', 'UnitId', 'RincianLevel', 'Lock', 'LastUpdateUser', 'LastUpdateTime', 'LastUpdateIp', 'Tahap', 'Tahun', ),
		BasePeer::TYPE_COLNAME => array (Revisi7RincianPeer::KEGIATAN_CODE, Revisi7RincianPeer::TIPE, Revisi7RincianPeer::RINCIAN_CONFIRMED, Revisi7RincianPeer::RINCIAN_CHANGED, Revisi7RincianPeer::RINCIAN_SELESAI, Revisi7RincianPeer::IP_ADDRESS, Revisi7RincianPeer::WAKTU_ACCESS, Revisi7RincianPeer::TARGET, Revisi7RincianPeer::UNIT_ID, Revisi7RincianPeer::RINCIAN_LEVEL, Revisi7RincianPeer::LOCK, Revisi7RincianPeer::LAST_UPDATE_USER, Revisi7RincianPeer::LAST_UPDATE_TIME, Revisi7RincianPeer::LAST_UPDATE_IP, Revisi7RincianPeer::TAHAP, Revisi7RincianPeer::TAHUN, ),
		BasePeer::TYPE_FIELDNAME => array ('kegiatan_code', 'tipe', 'rincian_confirmed', 'rincian_changed', 'rincian_selesai', 'ip_address', 'waktu_access', 'target', 'unit_id', 'rincian_level', 'lock', 'last_update_user', 'last_update_time', 'last_update_ip', 'tahap', 'tahun', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('KegiatanCode' => 0, 'Tipe' => 1, 'RincianConfirmed' => 2, 'RincianChanged' => 3, 'RincianSelesai' => 4, 'IpAddress' => 5, 'WaktuAccess' => 6, 'Target' => 7, 'UnitId' => 8, 'RincianLevel' => 9, 'Lock' => 10, 'LastUpdateUser' => 11, 'LastUpdateTime' => 12, 'LastUpdateIp' => 13, 'Tahap' => 14, 'Tahun' => 15, ),
		BasePeer::TYPE_COLNAME => array (Revisi7RincianPeer::KEGIATAN_CODE => 0, Revisi7RincianPeer::TIPE => 1, Revisi7RincianPeer::RINCIAN_CONFIRMED => 2, Revisi7RincianPeer::RINCIAN_CHANGED => 3, Revisi7RincianPeer::RINCIAN_SELESAI => 4, Revisi7RincianPeer::IP_ADDRESS => 5, Revisi7RincianPeer::WAKTU_ACCESS => 6, Revisi7RincianPeer::TARGET => 7, Revisi7RincianPeer::UNIT_ID => 8, Revisi7RincianPeer::RINCIAN_LEVEL => 9, Revisi7RincianPeer::LOCK => 10, Revisi7RincianPeer::LAST_UPDATE_USER => 11, Revisi7RincianPeer::LAST_UPDATE_TIME => 12, Revisi7RincianPeer::LAST_UPDATE_IP => 13, Revisi7RincianPeer::TAHAP => 14, Revisi7RincianPeer::TAHUN => 15, ),
		BasePeer::TYPE_FIELDNAME => array ('kegiatan_code' => 0, 'tipe' => 1, 'rincian_confirmed' => 2, 'rincian_changed' => 3, 'rincian_selesai' => 4, 'ip_address' => 5, 'waktu_access' => 6, 'target' => 7, 'unit_id' => 8, 'rincian_level' => 9, 'lock' => 10, 'last_update_user' => 11, 'last_update_time' => 12, 'last_update_ip' => 13, 'tahap' => 14, 'tahun' => 15, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/Revisi7RincianMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.Revisi7RincianMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = Revisi7RincianPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(Revisi7RincianPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(Revisi7RincianPeer::KEGIATAN_CODE);

		$criteria->addSelectColumn(Revisi7RincianPeer::TIPE);

		$criteria->addSelectColumn(Revisi7RincianPeer::RINCIAN_CONFIRMED);

		$criteria->addSelectColumn(Revisi7RincianPeer::RINCIAN_CHANGED);

		$criteria->addSelectColumn(Revisi7RincianPeer::RINCIAN_SELESAI);

		$criteria->addSelectColumn(Revisi7RincianPeer::IP_ADDRESS);

		$criteria->addSelectColumn(Revisi7RincianPeer::WAKTU_ACCESS);

		$criteria->addSelectColumn(Revisi7RincianPeer::TARGET);

		$criteria->addSelectColumn(Revisi7RincianPeer::UNIT_ID);

		$criteria->addSelectColumn(Revisi7RincianPeer::RINCIAN_LEVEL);

		$criteria->addSelectColumn(Revisi7RincianPeer::LOCK);

		$criteria->addSelectColumn(Revisi7RincianPeer::LAST_UPDATE_USER);

		$criteria->addSelectColumn(Revisi7RincianPeer::LAST_UPDATE_TIME);

		$criteria->addSelectColumn(Revisi7RincianPeer::LAST_UPDATE_IP);

		$criteria->addSelectColumn(Revisi7RincianPeer::TAHAP);

		$criteria->addSelectColumn(Revisi7RincianPeer::TAHUN);

	}

	const COUNT = 'COUNT(ebudget.revisi7_rincian.KEGIATAN_CODE)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.revisi7_rincian.KEGIATAN_CODE)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(Revisi7RincianPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(Revisi7RincianPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = Revisi7RincianPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = Revisi7RincianPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return Revisi7RincianPeer::populateObjects(Revisi7RincianPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			Revisi7RincianPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = Revisi7RincianPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return Revisi7RincianPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(Revisi7RincianPeer::KEGIATAN_CODE);
			$selectCriteria->add(Revisi7RincianPeer::KEGIATAN_CODE, $criteria->remove(Revisi7RincianPeer::KEGIATAN_CODE), $comparison);

			$comparison = $criteria->getComparison(Revisi7RincianPeer::TIPE);
			$selectCriteria->add(Revisi7RincianPeer::TIPE, $criteria->remove(Revisi7RincianPeer::TIPE), $comparison);

			$comparison = $criteria->getComparison(Revisi7RincianPeer::UNIT_ID);
			$selectCriteria->add(Revisi7RincianPeer::UNIT_ID, $criteria->remove(Revisi7RincianPeer::UNIT_ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(Revisi7RincianPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(Revisi7RincianPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof Revisi7Rincian) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
												if(count($values) == count($values, COUNT_RECURSIVE))
			{
								$values = array($values);
			}
			$vals = array();
			foreach($values as $value)
			{

				$vals[0][] = $value[0];
				$vals[1][] = $value[1];
				$vals[2][] = $value[2];
			}

			$criteria->add(Revisi7RincianPeer::KEGIATAN_CODE, $vals[0], Criteria::IN);
			$criteria->add(Revisi7RincianPeer::TIPE, $vals[1], Criteria::IN);
			$criteria->add(Revisi7RincianPeer::UNIT_ID, $vals[2], Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(Revisi7Rincian $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(Revisi7RincianPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(Revisi7RincianPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(Revisi7RincianPeer::DATABASE_NAME, Revisi7RincianPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = Revisi7RincianPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK( $kegiatan_code, $tipe, $unit_id, $con = null) {
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$criteria = new Criteria();
		$criteria->add(Revisi7RincianPeer::KEGIATAN_CODE, $kegiatan_code);
		$criteria->add(Revisi7RincianPeer::TIPE, $tipe);
		$criteria->add(Revisi7RincianPeer::UNIT_ID, $unit_id);
		$v = Revisi7RincianPeer::doSelect($criteria, $con);

		return !empty($v) ? $v[0] : null;
	}
} 
if (Propel::isInit()) {
			try {
		BaseRevisi7RincianPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/Revisi7RincianMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.Revisi7RincianMapBuilder');
}
