<?php


abstract class BaseShsdSurvey extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $shsd_id;


	
	protected $satuan;


	
	protected $shsd_name;


	
	protected $shsd_harga;


	
	protected $shsd_harga_dasar;


	
	protected $shsd_show;


	
	protected $shsd_faktor_inflasi;


	
	protected $shsd_locked = false;


	
	protected $rekening_code;


	
	protected $shsd_merk;


	
	protected $non_pajak = false;


	
	protected $shsd_id_old;


	
	protected $nama_dasar;


	
	protected $spec;


	
	protected $hidden_spec;


	
	protected $usulan_skpd;


	
	protected $is_potong_bpjs = false;


	
	protected $is_iuran_bpjs = false;


	
	protected $tahap;


	
	protected $is_survey_bp = false;


	
	protected $is_inflasi = true;


	
	protected $toko1;


	
	protected $toko2;


	
	protected $toko3;


	
	protected $merk1;


	
	protected $merk2;


	
	protected $merk3;


	
	protected $harga1;


	
	protected $harga2;


	
	protected $harga3;


	
	protected $tanggal_survey;


	
	protected $surveyor;


	
	protected $file1;


	
	protected $file2;


	
	protected $file3;


	
	protected $shsd_harga_lelang;


	
	protected $shsd_harga_pakai;


	
	protected $file_lelang;


	
	protected $file1_2;


	
	protected $file2_2;


	
	protected $file3_2;


	
	protected $keterangan1;


	
	protected $keterangan2;


	
	protected $keterangan3;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getShsdId()
	{

		return $this->shsd_id;
	}

	
	public function getSatuan()
	{

		return $this->satuan;
	}

	
	public function getShsdName()
	{

		return $this->shsd_name;
	}

	
	public function getShsdHarga()
	{

		return $this->shsd_harga;
	}

	
	public function getShsdHargaDasar()
	{

		return $this->shsd_harga_dasar;
	}

	
	public function getShsdShow()
	{

		return $this->shsd_show;
	}

	
	public function getShsdFaktorInflasi()
	{

		return $this->shsd_faktor_inflasi;
	}

	
	public function getShsdLocked()
	{

		return $this->shsd_locked;
	}

	
	public function getRekeningCode()
	{

		return $this->rekening_code;
	}

	
	public function getShsdMerk()
	{

		return $this->shsd_merk;
	}

	
	public function getNonPajak()
	{

		return $this->non_pajak;
	}

	
	public function getShsdIdOld()
	{

		return $this->shsd_id_old;
	}

	
	public function getNamaDasar()
	{

		return $this->nama_dasar;
	}

	
	public function getSpec()
	{

		return $this->spec;
	}

	
	public function getHiddenSpec()
	{

		return $this->hidden_spec;
	}

	
	public function getUsulanSkpd()
	{

		return $this->usulan_skpd;
	}

	
	public function getIsPotongBpjs()
	{

		return $this->is_potong_bpjs;
	}

	
	public function getIsIuranBpjs()
	{

		return $this->is_iuran_bpjs;
	}

	
	public function getTahap()
	{

		return $this->tahap;
	}

	
	public function getIsSurveyBp()
	{

		return $this->is_survey_bp;
	}

	
	public function getIsInflasi()
	{

		return $this->is_inflasi;
	}

	
	public function getToko1()
	{

		return $this->toko1;
	}

	
	public function getToko2()
	{

		return $this->toko2;
	}

	
	public function getToko3()
	{

		return $this->toko3;
	}

	
	public function getMerk1()
	{

		return $this->merk1;
	}

	
	public function getMerk2()
	{

		return $this->merk2;
	}

	
	public function getMerk3()
	{

		return $this->merk3;
	}

	
	public function getHarga1()
	{

		return $this->harga1;
	}

	
	public function getHarga2()
	{

		return $this->harga2;
	}

	
	public function getHarga3()
	{

		return $this->harga3;
	}

	
	public function getTanggalSurvey($format = 'Y-m-d H:i:s')
	{

		if ($this->tanggal_survey === null || $this->tanggal_survey === '') {
			return null;
		} elseif (!is_int($this->tanggal_survey)) {
						$ts = strtotime($this->tanggal_survey);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [tanggal_survey] as date/time value: " . var_export($this->tanggal_survey, true));
			}
		} else {
			$ts = $this->tanggal_survey;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getSurveyor()
	{

		return $this->surveyor;
	}

	
	public function getFile1()
	{

		return $this->file1;
	}

	
	public function getFile2()
	{

		return $this->file2;
	}

	
	public function getFile3()
	{

		return $this->file3;
	}

	
	public function getShsdHargaLelang()
	{

		return $this->shsd_harga_lelang;
	}

	
	public function getShsdHargaPakai()
	{

		return $this->shsd_harga_pakai;
	}

	
	public function getFileLelang()
	{

		return $this->file_lelang;
	}

	
	public function getFile12()
	{

		return $this->file1_2;
	}

	
	public function getFile22()
	{

		return $this->file2_2;
	}

	
	public function getFile32()
	{

		return $this->file3_2;
	}

	
	public function getKeterangan1()
	{

		return $this->keterangan1;
	}

	
	public function getKeterangan2()
	{

		return $this->keterangan2;
	}

	
	public function getKeterangan3()
	{

		return $this->keterangan3;
	}

	
	public function setShsdId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->shsd_id !== $v) {
			$this->shsd_id = $v;
			$this->modifiedColumns[] = ShsdSurveyPeer::SHSD_ID;
		}

	} 
	
	public function setSatuan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->satuan !== $v) {
			$this->satuan = $v;
			$this->modifiedColumns[] = ShsdSurveyPeer::SATUAN;
		}

	} 
	
	public function setShsdName($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->shsd_name !== $v) {
			$this->shsd_name = $v;
			$this->modifiedColumns[] = ShsdSurveyPeer::SHSD_NAME;
		}

	} 
	
	public function setShsdHarga($v)
	{

		if ($this->shsd_harga !== $v) {
			$this->shsd_harga = $v;
			$this->modifiedColumns[] = ShsdSurveyPeer::SHSD_HARGA;
		}

	} 
	
	public function setShsdHargaDasar($v)
	{

		if ($this->shsd_harga_dasar !== $v) {
			$this->shsd_harga_dasar = $v;
			$this->modifiedColumns[] = ShsdSurveyPeer::SHSD_HARGA_DASAR;
		}

	} 
	
	public function setShsdShow($v)
	{

		if ($this->shsd_show !== $v) {
			$this->shsd_show = $v;
			$this->modifiedColumns[] = ShsdSurveyPeer::SHSD_SHOW;
		}

	} 
	
	public function setShsdFaktorInflasi($v)
	{

		if ($this->shsd_faktor_inflasi !== $v) {
			$this->shsd_faktor_inflasi = $v;
			$this->modifiedColumns[] = ShsdSurveyPeer::SHSD_FAKTOR_INFLASI;
		}

	} 
	
	public function setShsdLocked($v)
	{

		if ($this->shsd_locked !== $v || $v === false) {
			$this->shsd_locked = $v;
			$this->modifiedColumns[] = ShsdSurveyPeer::SHSD_LOCKED;
		}

	} 
	
	public function setRekeningCode($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->rekening_code !== $v) {
			$this->rekening_code = $v;
			$this->modifiedColumns[] = ShsdSurveyPeer::REKENING_CODE;
		}

	} 
	
	public function setShsdMerk($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->shsd_merk !== $v) {
			$this->shsd_merk = $v;
			$this->modifiedColumns[] = ShsdSurveyPeer::SHSD_MERK;
		}

	} 
	
	public function setNonPajak($v)
	{

		if ($this->non_pajak !== $v || $v === false) {
			$this->non_pajak = $v;
			$this->modifiedColumns[] = ShsdSurveyPeer::NON_PAJAK;
		}

	} 
	
	public function setShsdIdOld($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->shsd_id_old !== $v) {
			$this->shsd_id_old = $v;
			$this->modifiedColumns[] = ShsdSurveyPeer::SHSD_ID_OLD;
		}

	} 
	
	public function setNamaDasar($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nama_dasar !== $v) {
			$this->nama_dasar = $v;
			$this->modifiedColumns[] = ShsdSurveyPeer::NAMA_DASAR;
		}

	} 
	
	public function setSpec($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->spec !== $v) {
			$this->spec = $v;
			$this->modifiedColumns[] = ShsdSurveyPeer::SPEC;
		}

	} 
	
	public function setHiddenSpec($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->hidden_spec !== $v) {
			$this->hidden_spec = $v;
			$this->modifiedColumns[] = ShsdSurveyPeer::HIDDEN_SPEC;
		}

	} 
	
	public function setUsulanSkpd($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->usulan_skpd !== $v) {
			$this->usulan_skpd = $v;
			$this->modifiedColumns[] = ShsdSurveyPeer::USULAN_SKPD;
		}

	} 
	
	public function setIsPotongBpjs($v)
	{

		if ($this->is_potong_bpjs !== $v || $v === false) {
			$this->is_potong_bpjs = $v;
			$this->modifiedColumns[] = ShsdSurveyPeer::IS_POTONG_BPJS;
		}

	} 
	
	public function setIsIuranBpjs($v)
	{

		if ($this->is_iuran_bpjs !== $v || $v === false) {
			$this->is_iuran_bpjs = $v;
			$this->modifiedColumns[] = ShsdSurveyPeer::IS_IURAN_BPJS;
		}

	} 
	
	public function setTahap($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->tahap !== $v) {
			$this->tahap = $v;
			$this->modifiedColumns[] = ShsdSurveyPeer::TAHAP;
		}

	} 
	
	public function setIsSurveyBp($v)
	{

		if ($this->is_survey_bp !== $v || $v === false) {
			$this->is_survey_bp = $v;
			$this->modifiedColumns[] = ShsdSurveyPeer::IS_SURVEY_BP;
		}

	} 
	
	public function setIsInflasi($v)
	{

		if ($this->is_inflasi !== $v || $v === true) {
			$this->is_inflasi = $v;
			$this->modifiedColumns[] = ShsdSurveyPeer::IS_INFLASI;
		}

	} 
	
	public function setToko1($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->toko1 !== $v) {
			$this->toko1 = $v;
			$this->modifiedColumns[] = ShsdSurveyPeer::TOKO1;
		}

	} 
	
	public function setToko2($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->toko2 !== $v) {
			$this->toko2 = $v;
			$this->modifiedColumns[] = ShsdSurveyPeer::TOKO2;
		}

	} 
	
	public function setToko3($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->toko3 !== $v) {
			$this->toko3 = $v;
			$this->modifiedColumns[] = ShsdSurveyPeer::TOKO3;
		}

	} 
	
	public function setMerk1($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->merk1 !== $v) {
			$this->merk1 = $v;
			$this->modifiedColumns[] = ShsdSurveyPeer::MERK1;
		}

	} 
	
	public function setMerk2($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->merk2 !== $v) {
			$this->merk2 = $v;
			$this->modifiedColumns[] = ShsdSurveyPeer::MERK2;
		}

	} 
	
	public function setMerk3($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->merk3 !== $v) {
			$this->merk3 = $v;
			$this->modifiedColumns[] = ShsdSurveyPeer::MERK3;
		}

	} 
	
	public function setHarga1($v)
	{

		if ($this->harga1 !== $v) {
			$this->harga1 = $v;
			$this->modifiedColumns[] = ShsdSurveyPeer::HARGA1;
		}

	} 
	
	public function setHarga2($v)
	{

		if ($this->harga2 !== $v) {
			$this->harga2 = $v;
			$this->modifiedColumns[] = ShsdSurveyPeer::HARGA2;
		}

	} 
	
	public function setHarga3($v)
	{

		if ($this->harga3 !== $v) {
			$this->harga3 = $v;
			$this->modifiedColumns[] = ShsdSurveyPeer::HARGA3;
		}

	} 
	
	public function setTanggalSurvey($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [tanggal_survey] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->tanggal_survey !== $ts) {
			$this->tanggal_survey = $ts;
			$this->modifiedColumns[] = ShsdSurveyPeer::TANGGAL_SURVEY;
		}

	} 
	
	public function setSurveyor($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->surveyor !== $v) {
			$this->surveyor = $v;
			$this->modifiedColumns[] = ShsdSurveyPeer::SURVEYOR;
		}

	} 
	
	public function setFile1($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->file1 !== $v) {
			$this->file1 = $v;
			$this->modifiedColumns[] = ShsdSurveyPeer::FILE1;
		}

	} 
	
	public function setFile2($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->file2 !== $v) {
			$this->file2 = $v;
			$this->modifiedColumns[] = ShsdSurveyPeer::FILE2;
		}

	} 
	
	public function setFile3($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->file3 !== $v) {
			$this->file3 = $v;
			$this->modifiedColumns[] = ShsdSurveyPeer::FILE3;
		}

	} 
	
	public function setShsdHargaLelang($v)
	{

		if ($this->shsd_harga_lelang !== $v) {
			$this->shsd_harga_lelang = $v;
			$this->modifiedColumns[] = ShsdSurveyPeer::SHSD_HARGA_LELANG;
		}

	} 
	
	public function setShsdHargaPakai($v)
	{

		if ($this->shsd_harga_pakai !== $v) {
			$this->shsd_harga_pakai = $v;
			$this->modifiedColumns[] = ShsdSurveyPeer::SHSD_HARGA_PAKAI;
		}

	} 
	
	public function setFileLelang($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->file_lelang !== $v) {
			$this->file_lelang = $v;
			$this->modifiedColumns[] = ShsdSurveyPeer::FILE_LELANG;
		}

	} 
	
	public function setFile12($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->file1_2 !== $v) {
			$this->file1_2 = $v;
			$this->modifiedColumns[] = ShsdSurveyPeer::FILE1_2;
		}

	} 
	
	public function setFile22($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->file2_2 !== $v) {
			$this->file2_2 = $v;
			$this->modifiedColumns[] = ShsdSurveyPeer::FILE2_2;
		}

	} 
	
	public function setFile32($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->file3_2 !== $v) {
			$this->file3_2 = $v;
			$this->modifiedColumns[] = ShsdSurveyPeer::FILE3_2;
		}

	} 
	
	public function setKeterangan1($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->keterangan1 !== $v) {
			$this->keterangan1 = $v;
			$this->modifiedColumns[] = ShsdSurveyPeer::KETERANGAN1;
		}

	} 
	
	public function setKeterangan2($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->keterangan2 !== $v) {
			$this->keterangan2 = $v;
			$this->modifiedColumns[] = ShsdSurveyPeer::KETERANGAN2;
		}

	} 
	
	public function setKeterangan3($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->keterangan3 !== $v) {
			$this->keterangan3 = $v;
			$this->modifiedColumns[] = ShsdSurveyPeer::KETERANGAN3;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->shsd_id = $rs->getString($startcol + 0);

			$this->satuan = $rs->getString($startcol + 1);

			$this->shsd_name = $rs->getString($startcol + 2);

			$this->shsd_harga = $rs->getFloat($startcol + 3);

			$this->shsd_harga_dasar = $rs->getFloat($startcol + 4);

			$this->shsd_show = $rs->getBoolean($startcol + 5);

			$this->shsd_faktor_inflasi = $rs->getFloat($startcol + 6);

			$this->shsd_locked = $rs->getBoolean($startcol + 7);

			$this->rekening_code = $rs->getString($startcol + 8);

			$this->shsd_merk = $rs->getString($startcol + 9);

			$this->non_pajak = $rs->getBoolean($startcol + 10);

			$this->shsd_id_old = $rs->getString($startcol + 11);

			$this->nama_dasar = $rs->getString($startcol + 12);

			$this->spec = $rs->getString($startcol + 13);

			$this->hidden_spec = $rs->getString($startcol + 14);

			$this->usulan_skpd = $rs->getString($startcol + 15);

			$this->is_potong_bpjs = $rs->getBoolean($startcol + 16);

			$this->is_iuran_bpjs = $rs->getBoolean($startcol + 17);

			$this->tahap = $rs->getString($startcol + 18);

			$this->is_survey_bp = $rs->getBoolean($startcol + 19);

			$this->is_inflasi = $rs->getBoolean($startcol + 20);

			$this->toko1 = $rs->getString($startcol + 21);

			$this->toko2 = $rs->getString($startcol + 22);

			$this->toko3 = $rs->getString($startcol + 23);

			$this->merk1 = $rs->getString($startcol + 24);

			$this->merk2 = $rs->getString($startcol + 25);

			$this->merk3 = $rs->getString($startcol + 26);

			$this->harga1 = $rs->getFloat($startcol + 27);

			$this->harga2 = $rs->getFloat($startcol + 28);

			$this->harga3 = $rs->getFloat($startcol + 29);

			$this->tanggal_survey = $rs->getTimestamp($startcol + 30, null);

			$this->surveyor = $rs->getString($startcol + 31);

			$this->file1 = $rs->getString($startcol + 32);

			$this->file2 = $rs->getString($startcol + 33);

			$this->file3 = $rs->getString($startcol + 34);

			$this->shsd_harga_lelang = $rs->getFloat($startcol + 35);

			$this->shsd_harga_pakai = $rs->getFloat($startcol + 36);

			$this->file_lelang = $rs->getString($startcol + 37);

			$this->file1_2 = $rs->getString($startcol + 38);

			$this->file2_2 = $rs->getString($startcol + 39);

			$this->file3_2 = $rs->getString($startcol + 40);

			$this->keterangan1 = $rs->getString($startcol + 41);

			$this->keterangan2 = $rs->getString($startcol + 42);

			$this->keterangan3 = $rs->getString($startcol + 43);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 44; 
		} catch (Exception $e) {
			throw new PropelException("Error populating ShsdSurvey object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(ShsdSurveyPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			ShsdSurveyPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(ShsdSurveyPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = ShsdSurveyPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += ShsdSurveyPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = ShsdSurveyPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = ShsdSurveyPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getShsdId();
				break;
			case 1:
				return $this->getSatuan();
				break;
			case 2:
				return $this->getShsdName();
				break;
			case 3:
				return $this->getShsdHarga();
				break;
			case 4:
				return $this->getShsdHargaDasar();
				break;
			case 5:
				return $this->getShsdShow();
				break;
			case 6:
				return $this->getShsdFaktorInflasi();
				break;
			case 7:
				return $this->getShsdLocked();
				break;
			case 8:
				return $this->getRekeningCode();
				break;
			case 9:
				return $this->getShsdMerk();
				break;
			case 10:
				return $this->getNonPajak();
				break;
			case 11:
				return $this->getShsdIdOld();
				break;
			case 12:
				return $this->getNamaDasar();
				break;
			case 13:
				return $this->getSpec();
				break;
			case 14:
				return $this->getHiddenSpec();
				break;
			case 15:
				return $this->getUsulanSkpd();
				break;
			case 16:
				return $this->getIsPotongBpjs();
				break;
			case 17:
				return $this->getIsIuranBpjs();
				break;
			case 18:
				return $this->getTahap();
				break;
			case 19:
				return $this->getIsSurveyBp();
				break;
			case 20:
				return $this->getIsInflasi();
				break;
			case 21:
				return $this->getToko1();
				break;
			case 22:
				return $this->getToko2();
				break;
			case 23:
				return $this->getToko3();
				break;
			case 24:
				return $this->getMerk1();
				break;
			case 25:
				return $this->getMerk2();
				break;
			case 26:
				return $this->getMerk3();
				break;
			case 27:
				return $this->getHarga1();
				break;
			case 28:
				return $this->getHarga2();
				break;
			case 29:
				return $this->getHarga3();
				break;
			case 30:
				return $this->getTanggalSurvey();
				break;
			case 31:
				return $this->getSurveyor();
				break;
			case 32:
				return $this->getFile1();
				break;
			case 33:
				return $this->getFile2();
				break;
			case 34:
				return $this->getFile3();
				break;
			case 35:
				return $this->getShsdHargaLelang();
				break;
			case 36:
				return $this->getShsdHargaPakai();
				break;
			case 37:
				return $this->getFileLelang();
				break;
			case 38:
				return $this->getFile12();
				break;
			case 39:
				return $this->getFile22();
				break;
			case 40:
				return $this->getFile32();
				break;
			case 41:
				return $this->getKeterangan1();
				break;
			case 42:
				return $this->getKeterangan2();
				break;
			case 43:
				return $this->getKeterangan3();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = ShsdSurveyPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getShsdId(),
			$keys[1] => $this->getSatuan(),
			$keys[2] => $this->getShsdName(),
			$keys[3] => $this->getShsdHarga(),
			$keys[4] => $this->getShsdHargaDasar(),
			$keys[5] => $this->getShsdShow(),
			$keys[6] => $this->getShsdFaktorInflasi(),
			$keys[7] => $this->getShsdLocked(),
			$keys[8] => $this->getRekeningCode(),
			$keys[9] => $this->getShsdMerk(),
			$keys[10] => $this->getNonPajak(),
			$keys[11] => $this->getShsdIdOld(),
			$keys[12] => $this->getNamaDasar(),
			$keys[13] => $this->getSpec(),
			$keys[14] => $this->getHiddenSpec(),
			$keys[15] => $this->getUsulanSkpd(),
			$keys[16] => $this->getIsPotongBpjs(),
			$keys[17] => $this->getIsIuranBpjs(),
			$keys[18] => $this->getTahap(),
			$keys[19] => $this->getIsSurveyBp(),
			$keys[20] => $this->getIsInflasi(),
			$keys[21] => $this->getToko1(),
			$keys[22] => $this->getToko2(),
			$keys[23] => $this->getToko3(),
			$keys[24] => $this->getMerk1(),
			$keys[25] => $this->getMerk2(),
			$keys[26] => $this->getMerk3(),
			$keys[27] => $this->getHarga1(),
			$keys[28] => $this->getHarga2(),
			$keys[29] => $this->getHarga3(),
			$keys[30] => $this->getTanggalSurvey(),
			$keys[31] => $this->getSurveyor(),
			$keys[32] => $this->getFile1(),
			$keys[33] => $this->getFile2(),
			$keys[34] => $this->getFile3(),
			$keys[35] => $this->getShsdHargaLelang(),
			$keys[36] => $this->getShsdHargaPakai(),
			$keys[37] => $this->getFileLelang(),
			$keys[38] => $this->getFile12(),
			$keys[39] => $this->getFile22(),
			$keys[40] => $this->getFile32(),
			$keys[41] => $this->getKeterangan1(),
			$keys[42] => $this->getKeterangan2(),
			$keys[43] => $this->getKeterangan3(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = ShsdSurveyPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setShsdId($value);
				break;
			case 1:
				$this->setSatuan($value);
				break;
			case 2:
				$this->setShsdName($value);
				break;
			case 3:
				$this->setShsdHarga($value);
				break;
			case 4:
				$this->setShsdHargaDasar($value);
				break;
			case 5:
				$this->setShsdShow($value);
				break;
			case 6:
				$this->setShsdFaktorInflasi($value);
				break;
			case 7:
				$this->setShsdLocked($value);
				break;
			case 8:
				$this->setRekeningCode($value);
				break;
			case 9:
				$this->setShsdMerk($value);
				break;
			case 10:
				$this->setNonPajak($value);
				break;
			case 11:
				$this->setShsdIdOld($value);
				break;
			case 12:
				$this->setNamaDasar($value);
				break;
			case 13:
				$this->setSpec($value);
				break;
			case 14:
				$this->setHiddenSpec($value);
				break;
			case 15:
				$this->setUsulanSkpd($value);
				break;
			case 16:
				$this->setIsPotongBpjs($value);
				break;
			case 17:
				$this->setIsIuranBpjs($value);
				break;
			case 18:
				$this->setTahap($value);
				break;
			case 19:
				$this->setIsSurveyBp($value);
				break;
			case 20:
				$this->setIsInflasi($value);
				break;
			case 21:
				$this->setToko1($value);
				break;
			case 22:
				$this->setToko2($value);
				break;
			case 23:
				$this->setToko3($value);
				break;
			case 24:
				$this->setMerk1($value);
				break;
			case 25:
				$this->setMerk2($value);
				break;
			case 26:
				$this->setMerk3($value);
				break;
			case 27:
				$this->setHarga1($value);
				break;
			case 28:
				$this->setHarga2($value);
				break;
			case 29:
				$this->setHarga3($value);
				break;
			case 30:
				$this->setTanggalSurvey($value);
				break;
			case 31:
				$this->setSurveyor($value);
				break;
			case 32:
				$this->setFile1($value);
				break;
			case 33:
				$this->setFile2($value);
				break;
			case 34:
				$this->setFile3($value);
				break;
			case 35:
				$this->setShsdHargaLelang($value);
				break;
			case 36:
				$this->setShsdHargaPakai($value);
				break;
			case 37:
				$this->setFileLelang($value);
				break;
			case 38:
				$this->setFile12($value);
				break;
			case 39:
				$this->setFile22($value);
				break;
			case 40:
				$this->setFile32($value);
				break;
			case 41:
				$this->setKeterangan1($value);
				break;
			case 42:
				$this->setKeterangan2($value);
				break;
			case 43:
				$this->setKeterangan3($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = ShsdSurveyPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setShsdId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setSatuan($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setShsdName($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setShsdHarga($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setShsdHargaDasar($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setShsdShow($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setShsdFaktorInflasi($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setShsdLocked($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setRekeningCode($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setShsdMerk($arr[$keys[9]]);
		if (array_key_exists($keys[10], $arr)) $this->setNonPajak($arr[$keys[10]]);
		if (array_key_exists($keys[11], $arr)) $this->setShsdIdOld($arr[$keys[11]]);
		if (array_key_exists($keys[12], $arr)) $this->setNamaDasar($arr[$keys[12]]);
		if (array_key_exists($keys[13], $arr)) $this->setSpec($arr[$keys[13]]);
		if (array_key_exists($keys[14], $arr)) $this->setHiddenSpec($arr[$keys[14]]);
		if (array_key_exists($keys[15], $arr)) $this->setUsulanSkpd($arr[$keys[15]]);
		if (array_key_exists($keys[16], $arr)) $this->setIsPotongBpjs($arr[$keys[16]]);
		if (array_key_exists($keys[17], $arr)) $this->setIsIuranBpjs($arr[$keys[17]]);
		if (array_key_exists($keys[18], $arr)) $this->setTahap($arr[$keys[18]]);
		if (array_key_exists($keys[19], $arr)) $this->setIsSurveyBp($arr[$keys[19]]);
		if (array_key_exists($keys[20], $arr)) $this->setIsInflasi($arr[$keys[20]]);
		if (array_key_exists($keys[21], $arr)) $this->setToko1($arr[$keys[21]]);
		if (array_key_exists($keys[22], $arr)) $this->setToko2($arr[$keys[22]]);
		if (array_key_exists($keys[23], $arr)) $this->setToko3($arr[$keys[23]]);
		if (array_key_exists($keys[24], $arr)) $this->setMerk1($arr[$keys[24]]);
		if (array_key_exists($keys[25], $arr)) $this->setMerk2($arr[$keys[25]]);
		if (array_key_exists($keys[26], $arr)) $this->setMerk3($arr[$keys[26]]);
		if (array_key_exists($keys[27], $arr)) $this->setHarga1($arr[$keys[27]]);
		if (array_key_exists($keys[28], $arr)) $this->setHarga2($arr[$keys[28]]);
		if (array_key_exists($keys[29], $arr)) $this->setHarga3($arr[$keys[29]]);
		if (array_key_exists($keys[30], $arr)) $this->setTanggalSurvey($arr[$keys[30]]);
		if (array_key_exists($keys[31], $arr)) $this->setSurveyor($arr[$keys[31]]);
		if (array_key_exists($keys[32], $arr)) $this->setFile1($arr[$keys[32]]);
		if (array_key_exists($keys[33], $arr)) $this->setFile2($arr[$keys[33]]);
		if (array_key_exists($keys[34], $arr)) $this->setFile3($arr[$keys[34]]);
		if (array_key_exists($keys[35], $arr)) $this->setShsdHargaLelang($arr[$keys[35]]);
		if (array_key_exists($keys[36], $arr)) $this->setShsdHargaPakai($arr[$keys[36]]);
		if (array_key_exists($keys[37], $arr)) $this->setFileLelang($arr[$keys[37]]);
		if (array_key_exists($keys[38], $arr)) $this->setFile12($arr[$keys[38]]);
		if (array_key_exists($keys[39], $arr)) $this->setFile22($arr[$keys[39]]);
		if (array_key_exists($keys[40], $arr)) $this->setFile32($arr[$keys[40]]);
		if (array_key_exists($keys[41], $arr)) $this->setKeterangan1($arr[$keys[41]]);
		if (array_key_exists($keys[42], $arr)) $this->setKeterangan2($arr[$keys[42]]);
		if (array_key_exists($keys[43], $arr)) $this->setKeterangan3($arr[$keys[43]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(ShsdSurveyPeer::DATABASE_NAME);

		if ($this->isColumnModified(ShsdSurveyPeer::SHSD_ID)) $criteria->add(ShsdSurveyPeer::SHSD_ID, $this->shsd_id);
		if ($this->isColumnModified(ShsdSurveyPeer::SATUAN)) $criteria->add(ShsdSurveyPeer::SATUAN, $this->satuan);
		if ($this->isColumnModified(ShsdSurveyPeer::SHSD_NAME)) $criteria->add(ShsdSurveyPeer::SHSD_NAME, $this->shsd_name);
		if ($this->isColumnModified(ShsdSurveyPeer::SHSD_HARGA)) $criteria->add(ShsdSurveyPeer::SHSD_HARGA, $this->shsd_harga);
		if ($this->isColumnModified(ShsdSurveyPeer::SHSD_HARGA_DASAR)) $criteria->add(ShsdSurveyPeer::SHSD_HARGA_DASAR, $this->shsd_harga_dasar);
		if ($this->isColumnModified(ShsdSurveyPeer::SHSD_SHOW)) $criteria->add(ShsdSurveyPeer::SHSD_SHOW, $this->shsd_show);
		if ($this->isColumnModified(ShsdSurveyPeer::SHSD_FAKTOR_INFLASI)) $criteria->add(ShsdSurveyPeer::SHSD_FAKTOR_INFLASI, $this->shsd_faktor_inflasi);
		if ($this->isColumnModified(ShsdSurveyPeer::SHSD_LOCKED)) $criteria->add(ShsdSurveyPeer::SHSD_LOCKED, $this->shsd_locked);
		if ($this->isColumnModified(ShsdSurveyPeer::REKENING_CODE)) $criteria->add(ShsdSurveyPeer::REKENING_CODE, $this->rekening_code);
		if ($this->isColumnModified(ShsdSurveyPeer::SHSD_MERK)) $criteria->add(ShsdSurveyPeer::SHSD_MERK, $this->shsd_merk);
		if ($this->isColumnModified(ShsdSurveyPeer::NON_PAJAK)) $criteria->add(ShsdSurveyPeer::NON_PAJAK, $this->non_pajak);
		if ($this->isColumnModified(ShsdSurveyPeer::SHSD_ID_OLD)) $criteria->add(ShsdSurveyPeer::SHSD_ID_OLD, $this->shsd_id_old);
		if ($this->isColumnModified(ShsdSurveyPeer::NAMA_DASAR)) $criteria->add(ShsdSurveyPeer::NAMA_DASAR, $this->nama_dasar);
		if ($this->isColumnModified(ShsdSurveyPeer::SPEC)) $criteria->add(ShsdSurveyPeer::SPEC, $this->spec);
		if ($this->isColumnModified(ShsdSurveyPeer::HIDDEN_SPEC)) $criteria->add(ShsdSurveyPeer::HIDDEN_SPEC, $this->hidden_spec);
		if ($this->isColumnModified(ShsdSurveyPeer::USULAN_SKPD)) $criteria->add(ShsdSurveyPeer::USULAN_SKPD, $this->usulan_skpd);
		if ($this->isColumnModified(ShsdSurveyPeer::IS_POTONG_BPJS)) $criteria->add(ShsdSurveyPeer::IS_POTONG_BPJS, $this->is_potong_bpjs);
		if ($this->isColumnModified(ShsdSurveyPeer::IS_IURAN_BPJS)) $criteria->add(ShsdSurveyPeer::IS_IURAN_BPJS, $this->is_iuran_bpjs);
		if ($this->isColumnModified(ShsdSurveyPeer::TAHAP)) $criteria->add(ShsdSurveyPeer::TAHAP, $this->tahap);
		if ($this->isColumnModified(ShsdSurveyPeer::IS_SURVEY_BP)) $criteria->add(ShsdSurveyPeer::IS_SURVEY_BP, $this->is_survey_bp);
		if ($this->isColumnModified(ShsdSurveyPeer::IS_INFLASI)) $criteria->add(ShsdSurveyPeer::IS_INFLASI, $this->is_inflasi);
		if ($this->isColumnModified(ShsdSurveyPeer::TOKO1)) $criteria->add(ShsdSurveyPeer::TOKO1, $this->toko1);
		if ($this->isColumnModified(ShsdSurveyPeer::TOKO2)) $criteria->add(ShsdSurveyPeer::TOKO2, $this->toko2);
		if ($this->isColumnModified(ShsdSurveyPeer::TOKO3)) $criteria->add(ShsdSurveyPeer::TOKO3, $this->toko3);
		if ($this->isColumnModified(ShsdSurveyPeer::MERK1)) $criteria->add(ShsdSurveyPeer::MERK1, $this->merk1);
		if ($this->isColumnModified(ShsdSurveyPeer::MERK2)) $criteria->add(ShsdSurveyPeer::MERK2, $this->merk2);
		if ($this->isColumnModified(ShsdSurveyPeer::MERK3)) $criteria->add(ShsdSurveyPeer::MERK3, $this->merk3);
		if ($this->isColumnModified(ShsdSurveyPeer::HARGA1)) $criteria->add(ShsdSurveyPeer::HARGA1, $this->harga1);
		if ($this->isColumnModified(ShsdSurveyPeer::HARGA2)) $criteria->add(ShsdSurveyPeer::HARGA2, $this->harga2);
		if ($this->isColumnModified(ShsdSurveyPeer::HARGA3)) $criteria->add(ShsdSurveyPeer::HARGA3, $this->harga3);
		if ($this->isColumnModified(ShsdSurveyPeer::TANGGAL_SURVEY)) $criteria->add(ShsdSurveyPeer::TANGGAL_SURVEY, $this->tanggal_survey);
		if ($this->isColumnModified(ShsdSurveyPeer::SURVEYOR)) $criteria->add(ShsdSurveyPeer::SURVEYOR, $this->surveyor);
		if ($this->isColumnModified(ShsdSurveyPeer::FILE1)) $criteria->add(ShsdSurveyPeer::FILE1, $this->file1);
		if ($this->isColumnModified(ShsdSurveyPeer::FILE2)) $criteria->add(ShsdSurveyPeer::FILE2, $this->file2);
		if ($this->isColumnModified(ShsdSurveyPeer::FILE3)) $criteria->add(ShsdSurveyPeer::FILE3, $this->file3);
		if ($this->isColumnModified(ShsdSurveyPeer::SHSD_HARGA_LELANG)) $criteria->add(ShsdSurveyPeer::SHSD_HARGA_LELANG, $this->shsd_harga_lelang);
		if ($this->isColumnModified(ShsdSurveyPeer::SHSD_HARGA_PAKAI)) $criteria->add(ShsdSurveyPeer::SHSD_HARGA_PAKAI, $this->shsd_harga_pakai);
		if ($this->isColumnModified(ShsdSurveyPeer::FILE_LELANG)) $criteria->add(ShsdSurveyPeer::FILE_LELANG, $this->file_lelang);
		if ($this->isColumnModified(ShsdSurveyPeer::FILE1_2)) $criteria->add(ShsdSurveyPeer::FILE1_2, $this->file1_2);
		if ($this->isColumnModified(ShsdSurveyPeer::FILE2_2)) $criteria->add(ShsdSurveyPeer::FILE2_2, $this->file2_2);
		if ($this->isColumnModified(ShsdSurveyPeer::FILE3_2)) $criteria->add(ShsdSurveyPeer::FILE3_2, $this->file3_2);
		if ($this->isColumnModified(ShsdSurveyPeer::KETERANGAN1)) $criteria->add(ShsdSurveyPeer::KETERANGAN1, $this->keterangan1);
		if ($this->isColumnModified(ShsdSurveyPeer::KETERANGAN2)) $criteria->add(ShsdSurveyPeer::KETERANGAN2, $this->keterangan2);
		if ($this->isColumnModified(ShsdSurveyPeer::KETERANGAN3)) $criteria->add(ShsdSurveyPeer::KETERANGAN3, $this->keterangan3);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(ShsdSurveyPeer::DATABASE_NAME);

		$criteria->add(ShsdSurveyPeer::SHSD_ID, $this->shsd_id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getShsdId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setShsdId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setSatuan($this->satuan);

		$copyObj->setShsdName($this->shsd_name);

		$copyObj->setShsdHarga($this->shsd_harga);

		$copyObj->setShsdHargaDasar($this->shsd_harga_dasar);

		$copyObj->setShsdShow($this->shsd_show);

		$copyObj->setShsdFaktorInflasi($this->shsd_faktor_inflasi);

		$copyObj->setShsdLocked($this->shsd_locked);

		$copyObj->setRekeningCode($this->rekening_code);

		$copyObj->setShsdMerk($this->shsd_merk);

		$copyObj->setNonPajak($this->non_pajak);

		$copyObj->setShsdIdOld($this->shsd_id_old);

		$copyObj->setNamaDasar($this->nama_dasar);

		$copyObj->setSpec($this->spec);

		$copyObj->setHiddenSpec($this->hidden_spec);

		$copyObj->setUsulanSkpd($this->usulan_skpd);

		$copyObj->setIsPotongBpjs($this->is_potong_bpjs);

		$copyObj->setIsIuranBpjs($this->is_iuran_bpjs);

		$copyObj->setTahap($this->tahap);

		$copyObj->setIsSurveyBp($this->is_survey_bp);

		$copyObj->setIsInflasi($this->is_inflasi);

		$copyObj->setToko1($this->toko1);

		$copyObj->setToko2($this->toko2);

		$copyObj->setToko3($this->toko3);

		$copyObj->setMerk1($this->merk1);

		$copyObj->setMerk2($this->merk2);

		$copyObj->setMerk3($this->merk3);

		$copyObj->setHarga1($this->harga1);

		$copyObj->setHarga2($this->harga2);

		$copyObj->setHarga3($this->harga3);

		$copyObj->setTanggalSurvey($this->tanggal_survey);

		$copyObj->setSurveyor($this->surveyor);

		$copyObj->setFile1($this->file1);

		$copyObj->setFile2($this->file2);

		$copyObj->setFile3($this->file3);

		$copyObj->setShsdHargaLelang($this->shsd_harga_lelang);

		$copyObj->setShsdHargaPakai($this->shsd_harga_pakai);

		$copyObj->setFileLelang($this->file_lelang);

		$copyObj->setFile12($this->file1_2);

		$copyObj->setFile22($this->file2_2);

		$copyObj->setFile32($this->file3_2);

		$copyObj->setKeterangan1($this->keterangan1);

		$copyObj->setKeterangan2($this->keterangan2);

		$copyObj->setKeterangan3($this->keterangan3);


		$copyObj->setNew(true);

		$copyObj->setShsdId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new ShsdSurveyPeer();
		}
		return self::$peer;
	}

} 