<?php


abstract class BaseRekeningDibukaBelanjaDikunci extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id;


	
	protected $belanja_dikunci_id;


	
	protected $unit_id;


	
	protected $kegiatan_code;


	
	protected $rekening_code;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getId()
	{

		return $this->id;
	}

	
	public function getBelanjaDikunciId()
	{

		return $this->belanja_dikunci_id;
	}

	
	public function getUnitId()
	{

		return $this->unit_id;
	}

	
	public function getKegiatanCode()
	{

		return $this->kegiatan_code;
	}

	
	public function getRekeningCode()
	{

		return $this->rekening_code;
	}

	
	public function setId($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = RekeningDibukaBelanjaDikunciPeer::ID;
		}

	} 
	
	public function setBelanjaDikunciId($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->belanja_dikunci_id !== $v) {
			$this->belanja_dikunci_id = $v;
			$this->modifiedColumns[] = RekeningDibukaBelanjaDikunciPeer::BELANJA_DIKUNCI_ID;
		}

	} 
	
	public function setUnitId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->unit_id !== $v) {
			$this->unit_id = $v;
			$this->modifiedColumns[] = RekeningDibukaBelanjaDikunciPeer::UNIT_ID;
		}

	} 
	
	public function setKegiatanCode($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kegiatan_code !== $v) {
			$this->kegiatan_code = $v;
			$this->modifiedColumns[] = RekeningDibukaBelanjaDikunciPeer::KEGIATAN_CODE;
		}

	} 
	
	public function setRekeningCode($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->rekening_code !== $v) {
			$this->rekening_code = $v;
			$this->modifiedColumns[] = RekeningDibukaBelanjaDikunciPeer::REKENING_CODE;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getInt($startcol + 0);

			$this->belanja_dikunci_id = $rs->getInt($startcol + 1);

			$this->unit_id = $rs->getString($startcol + 2);

			$this->kegiatan_code = $rs->getString($startcol + 3);

			$this->rekening_code = $rs->getString($startcol + 4);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 5; 
		} catch (Exception $e) {
			throw new PropelException("Error populating RekeningDibukaBelanjaDikunci object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(RekeningDibukaBelanjaDikunciPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			RekeningDibukaBelanjaDikunciPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(RekeningDibukaBelanjaDikunciPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = RekeningDibukaBelanjaDikunciPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += RekeningDibukaBelanjaDikunciPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = RekeningDibukaBelanjaDikunciPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = RekeningDibukaBelanjaDikunciPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getBelanjaDikunciId();
				break;
			case 2:
				return $this->getUnitId();
				break;
			case 3:
				return $this->getKegiatanCode();
				break;
			case 4:
				return $this->getRekeningCode();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = RekeningDibukaBelanjaDikunciPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getBelanjaDikunciId(),
			$keys[2] => $this->getUnitId(),
			$keys[3] => $this->getKegiatanCode(),
			$keys[4] => $this->getRekeningCode(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = RekeningDibukaBelanjaDikunciPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setBelanjaDikunciId($value);
				break;
			case 2:
				$this->setUnitId($value);
				break;
			case 3:
				$this->setKegiatanCode($value);
				break;
			case 4:
				$this->setRekeningCode($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = RekeningDibukaBelanjaDikunciPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setBelanjaDikunciId($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setUnitId($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setKegiatanCode($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setRekeningCode($arr[$keys[4]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(RekeningDibukaBelanjaDikunciPeer::DATABASE_NAME);

		if ($this->isColumnModified(RekeningDibukaBelanjaDikunciPeer::ID)) $criteria->add(RekeningDibukaBelanjaDikunciPeer::ID, $this->id);
		if ($this->isColumnModified(RekeningDibukaBelanjaDikunciPeer::BELANJA_DIKUNCI_ID)) $criteria->add(RekeningDibukaBelanjaDikunciPeer::BELANJA_DIKUNCI_ID, $this->belanja_dikunci_id);
		if ($this->isColumnModified(RekeningDibukaBelanjaDikunciPeer::UNIT_ID)) $criteria->add(RekeningDibukaBelanjaDikunciPeer::UNIT_ID, $this->unit_id);
		if ($this->isColumnModified(RekeningDibukaBelanjaDikunciPeer::KEGIATAN_CODE)) $criteria->add(RekeningDibukaBelanjaDikunciPeer::KEGIATAN_CODE, $this->kegiatan_code);
		if ($this->isColumnModified(RekeningDibukaBelanjaDikunciPeer::REKENING_CODE)) $criteria->add(RekeningDibukaBelanjaDikunciPeer::REKENING_CODE, $this->rekening_code);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(RekeningDibukaBelanjaDikunciPeer::DATABASE_NAME);

		$criteria->add(RekeningDibukaBelanjaDikunciPeer::ID, $this->id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setBelanjaDikunciId($this->belanja_dikunci_id);

		$copyObj->setUnitId($this->unit_id);

		$copyObj->setKegiatanCode($this->kegiatan_code);

		$copyObj->setRekeningCode($this->rekening_code);


		$copyObj->setNew(true);

		$copyObj->setId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new RekeningDibukaBelanjaDikunciPeer();
		}
		return self::$peer;
	}

} 