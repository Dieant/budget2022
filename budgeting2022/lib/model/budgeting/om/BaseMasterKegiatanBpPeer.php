<?php


abstract class BaseMasterKegiatanBpPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.master_kegiatan_bp';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.MasterKegiatanBp';

	
	const NUM_COLUMNS = 62;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const UNIT_ID = 'ebudget.master_kegiatan_bp.UNIT_ID';

	
	const KODE_KEGIATAN = 'ebudget.master_kegiatan_bp.KODE_KEGIATAN';

	
	const KODE_BIDANG = 'ebudget.master_kegiatan_bp.KODE_BIDANG';

	
	const KODE_URUSAN_WAJIB = 'ebudget.master_kegiatan_bp.KODE_URUSAN_WAJIB';

	
	const KODE_PROGRAM = 'ebudget.master_kegiatan_bp.KODE_PROGRAM';

	
	const KODE_SASARAN = 'ebudget.master_kegiatan_bp.KODE_SASARAN';

	
	const KODE_INDIKATOR = 'ebudget.master_kegiatan_bp.KODE_INDIKATOR';

	
	const ALOKASI_DANA = 'ebudget.master_kegiatan_bp.ALOKASI_DANA';

	
	const NAMA_KEGIATAN = 'ebudget.master_kegiatan_bp.NAMA_KEGIATAN';

	
	const MASUKAN = 'ebudget.master_kegiatan_bp.MASUKAN';

	
	const OUTPUT = 'ebudget.master_kegiatan_bp.OUTPUT';

	
	const OUTCOME = 'ebudget.master_kegiatan_bp.OUTCOME';

	
	const BENEFIT = 'ebudget.master_kegiatan_bp.BENEFIT';

	
	const IMPACT = 'ebudget.master_kegiatan_bp.IMPACT';

	
	const TIPE = 'ebudget.master_kegiatan_bp.TIPE';

	
	const KEGIATAN_ACTIVE = 'ebudget.master_kegiatan_bp.KEGIATAN_ACTIVE';

	
	const TO_KEGIATAN_CODE = 'ebudget.master_kegiatan_bp.TO_KEGIATAN_CODE';

	
	const CATATAN = 'ebudget.master_kegiatan_bp.CATATAN';

	
	const TARGET_OUTCOME = 'ebudget.master_kegiatan_bp.TARGET_OUTCOME';

	
	const LOKASI = 'ebudget.master_kegiatan_bp.LOKASI';

	
	const JUMLAH_PREV = 'ebudget.master_kegiatan_bp.JUMLAH_PREV';

	
	const JUMLAH_NOW = 'ebudget.master_kegiatan_bp.JUMLAH_NOW';

	
	const JUMLAH_NEXT = 'ebudget.master_kegiatan_bp.JUMLAH_NEXT';

	
	const KODE_PROGRAM2 = 'ebudget.master_kegiatan_bp.KODE_PROGRAM2';

	
	const KODE_URUSAN = 'ebudget.master_kegiatan_bp.KODE_URUSAN';

	
	const LAST_UPDATE_USER = 'ebudget.master_kegiatan_bp.LAST_UPDATE_USER';

	
	const LAST_UPDATE_TIME = 'ebudget.master_kegiatan_bp.LAST_UPDATE_TIME';

	
	const LAST_UPDATE_IP = 'ebudget.master_kegiatan_bp.LAST_UPDATE_IP';

	
	const TAHAP = 'ebudget.master_kegiatan_bp.TAHAP';

	
	const KODE_MISI = 'ebudget.master_kegiatan_bp.KODE_MISI';

	
	const KODE_TUJUAN = 'ebudget.master_kegiatan_bp.KODE_TUJUAN';

	
	const RANKING = 'ebudget.master_kegiatan_bp.RANKING';

	
	const NOMOR13 = 'ebudget.master_kegiatan_bp.NOMOR13';

	
	const PPA_NAMA = 'ebudget.master_kegiatan_bp.PPA_NAMA';

	
	const PPA_PANGKAT = 'ebudget.master_kegiatan_bp.PPA_PANGKAT';

	
	const PPA_NIP = 'ebudget.master_kegiatan_bp.PPA_NIP';

	
	const LANJUTAN = 'ebudget.master_kegiatan_bp.LANJUTAN';

	
	const USER_ID = 'ebudget.master_kegiatan_bp.USER_ID';

	
	const ID = 'ebudget.master_kegiatan_bp.ID';

	
	const TAHUN = 'ebudget.master_kegiatan_bp.TAHUN';

	
	const TAMBAHAN_PAGU = 'ebudget.master_kegiatan_bp.TAMBAHAN_PAGU';

	
	const GENDER = 'ebudget.master_kegiatan_bp.GENDER';

	
	const KODE_KEG_KEUANGAN = 'ebudget.master_kegiatan_bp.KODE_KEG_KEUANGAN';

	
	const USER_ID_LAMA = 'ebudget.master_kegiatan_bp.USER_ID_LAMA';

	
	const INDIKATOR = 'ebudget.master_kegiatan_bp.INDIKATOR';

	
	const IS_DAK = 'ebudget.master_kegiatan_bp.IS_DAK';

	
	const KODE_KEGIATAN_ASAL = 'ebudget.master_kegiatan_bp.KODE_KEGIATAN_ASAL';

	
	const KODE_KEG_KEUANGAN_ASAL = 'ebudget.master_kegiatan_bp.KODE_KEG_KEUANGAN_ASAL';

	
	const TH_KE_MULTIYEARS = 'ebudget.master_kegiatan_bp.TH_KE_MULTIYEARS';

	
	const KELOMPOK_SASARAN = 'ebudget.master_kegiatan_bp.KELOMPOK_SASARAN';

	
	const PAGU_BAPPEKO = 'ebudget.master_kegiatan_bp.PAGU_BAPPEKO';

	
	const KODE_DPA = 'ebudget.master_kegiatan_bp.KODE_DPA';

	
	const USER_ID_PPTK = 'ebudget.master_kegiatan_bp.USER_ID_PPTK';

	
	const USER_ID_KPA = 'ebudget.master_kegiatan_bp.USER_ID_KPA';

	
	const CATATAN_PEMBAHASAN = 'ebudget.master_kegiatan_bp.CATATAN_PEMBAHASAN';

	
	const VERIFIKASI_BPKPD = 'ebudget.master_kegiatan_bp.VERIFIKASI_BPKPD';

	
	const VERIFIKASI_BAPPEKO = 'ebudget.master_kegiatan_bp.VERIFIKASI_BAPPEKO';

	
	const VERIFIKASI_PENYELIA = 'ebudget.master_kegiatan_bp.VERIFIKASI_PENYELIA';

	
	const VERIFIKASI_BAGIAN_HUKUM = 'ebudget.master_kegiatan_bp.VERIFIKASI_BAGIAN_HUKUM';

	
	const VERIFIKASI_INSPEKTORAT = 'ebudget.master_kegiatan_bp.VERIFIKASI_INSPEKTORAT';

	
	const VERIFIKASI_BADAN_KEPEGAWAIAN = 'ebudget.master_kegiatan_bp.VERIFIKASI_BADAN_KEPEGAWAIAN';

	
	const VERIFIKASI_LPPA = 'ebudget.master_kegiatan_bp.VERIFIKASI_LPPA';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('UnitId', 'KodeKegiatan', 'KodeBidang', 'KodeUrusanWajib', 'KodeProgram', 'KodeSasaran', 'KodeIndikator', 'AlokasiDana', 'NamaKegiatan', 'Masukan', 'Output', 'Outcome', 'Benefit', 'Impact', 'Tipe', 'KegiatanActive', 'ToKegiatanCode', 'Catatan', 'TargetOutcome', 'Lokasi', 'JumlahPrev', 'JumlahNow', 'JumlahNext', 'KodeProgram2', 'KodeUrusan', 'LastUpdateUser', 'LastUpdateTime', 'LastUpdateIp', 'Tahap', 'KodeMisi', 'KodeTujuan', 'Ranking', 'Nomor13', 'PpaNama', 'PpaPangkat', 'PpaNip', 'Lanjutan', 'UserId', 'Id', 'Tahun', 'TambahanPagu', 'Gender', 'KodeKegKeuangan', 'UserIdLama', 'Indikator', 'IsDak', 'KodeKegiatanAsal', 'KodeKegKeuanganAsal', 'ThKeMultiyears', 'KelompokSasaran', 'PaguBappeko', 'KodeDpa', 'UserIdPptk', 'UserIdKpa', 'CatatanPembahasan', 'VerifikasiBpkpd', 'VerifikasiBappeko', 'VerifikasiPenyelia', 'VerifikasiBagianHukum', 'VerifikasiInspektorat', 'VerifikasiBadanKepegawaian', 'VerifikasiLppa', ),
		BasePeer::TYPE_COLNAME => array (MasterKegiatanBpPeer::UNIT_ID, MasterKegiatanBpPeer::KODE_KEGIATAN, MasterKegiatanBpPeer::KODE_BIDANG, MasterKegiatanBpPeer::KODE_URUSAN_WAJIB, MasterKegiatanBpPeer::KODE_PROGRAM, MasterKegiatanBpPeer::KODE_SASARAN, MasterKegiatanBpPeer::KODE_INDIKATOR, MasterKegiatanBpPeer::ALOKASI_DANA, MasterKegiatanBpPeer::NAMA_KEGIATAN, MasterKegiatanBpPeer::MASUKAN, MasterKegiatanBpPeer::OUTPUT, MasterKegiatanBpPeer::OUTCOME, MasterKegiatanBpPeer::BENEFIT, MasterKegiatanBpPeer::IMPACT, MasterKegiatanBpPeer::TIPE, MasterKegiatanBpPeer::KEGIATAN_ACTIVE, MasterKegiatanBpPeer::TO_KEGIATAN_CODE, MasterKegiatanBpPeer::CATATAN, MasterKegiatanBpPeer::TARGET_OUTCOME, MasterKegiatanBpPeer::LOKASI, MasterKegiatanBpPeer::JUMLAH_PREV, MasterKegiatanBpPeer::JUMLAH_NOW, MasterKegiatanBpPeer::JUMLAH_NEXT, MasterKegiatanBpPeer::KODE_PROGRAM2, MasterKegiatanBpPeer::KODE_URUSAN, MasterKegiatanBpPeer::LAST_UPDATE_USER, MasterKegiatanBpPeer::LAST_UPDATE_TIME, MasterKegiatanBpPeer::LAST_UPDATE_IP, MasterKegiatanBpPeer::TAHAP, MasterKegiatanBpPeer::KODE_MISI, MasterKegiatanBpPeer::KODE_TUJUAN, MasterKegiatanBpPeer::RANKING, MasterKegiatanBpPeer::NOMOR13, MasterKegiatanBpPeer::PPA_NAMA, MasterKegiatanBpPeer::PPA_PANGKAT, MasterKegiatanBpPeer::PPA_NIP, MasterKegiatanBpPeer::LANJUTAN, MasterKegiatanBpPeer::USER_ID, MasterKegiatanBpPeer::ID, MasterKegiatanBpPeer::TAHUN, MasterKegiatanBpPeer::TAMBAHAN_PAGU, MasterKegiatanBpPeer::GENDER, MasterKegiatanBpPeer::KODE_KEG_KEUANGAN, MasterKegiatanBpPeer::USER_ID_LAMA, MasterKegiatanBpPeer::INDIKATOR, MasterKegiatanBpPeer::IS_DAK, MasterKegiatanBpPeer::KODE_KEGIATAN_ASAL, MasterKegiatanBpPeer::KODE_KEG_KEUANGAN_ASAL, MasterKegiatanBpPeer::TH_KE_MULTIYEARS, MasterKegiatanBpPeer::KELOMPOK_SASARAN, MasterKegiatanBpPeer::PAGU_BAPPEKO, MasterKegiatanBpPeer::KODE_DPA, MasterKegiatanBpPeer::USER_ID_PPTK, MasterKegiatanBpPeer::USER_ID_KPA, MasterKegiatanBpPeer::CATATAN_PEMBAHASAN, MasterKegiatanBpPeer::VERIFIKASI_BPKPD, MasterKegiatanBpPeer::VERIFIKASI_BAPPEKO, MasterKegiatanBpPeer::VERIFIKASI_PENYELIA, MasterKegiatanBpPeer::VERIFIKASI_BAGIAN_HUKUM, MasterKegiatanBpPeer::VERIFIKASI_INSPEKTORAT, MasterKegiatanBpPeer::VERIFIKASI_BADAN_KEPEGAWAIAN, MasterKegiatanBpPeer::VERIFIKASI_LPPA, ),
		BasePeer::TYPE_FIELDNAME => array ('unit_id', 'kode_kegiatan', 'kode_bidang', 'kode_urusan_wajib', 'kode_program', 'kode_sasaran', 'kode_indikator', 'alokasi_dana', 'nama_kegiatan', 'masukan', 'output', 'outcome', 'benefit', 'impact', 'tipe', 'kegiatan_active', 'to_kegiatan_code', 'catatan', 'target_outcome', 'lokasi', 'jumlah_prev', 'jumlah_now', 'jumlah_next', 'kode_program2', 'kode_urusan', 'last_update_user', 'last_update_time', 'last_update_ip', 'tahap', 'kode_misi', 'kode_tujuan', 'ranking', 'nomor13', 'ppa_nama', 'ppa_pangkat', 'ppa_nip', 'lanjutan', 'user_id', 'id', 'tahun', 'tambahan_pagu', 'gender', 'kode_keg_keuangan', 'user_id_lama', 'indikator', 'is_dak', 'kode_kegiatan_asal', 'kode_keg_keuangan_asal', 'th_ke_multiyears', 'kelompok_sasaran', 'pagu_bappeko', 'kode_dpa', 'user_id_pptk', 'user_id_kpa', 'catatan_pembahasan', 'verifikasi_bpkpd', 'verifikasi_bappeko', 'verifikasi_penyelia', 'verifikasi_bagian_hukum', 'verifikasi_inspektorat', 'verifikasi_badan_kepegawaian', 'verifikasi_lppa', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('UnitId' => 0, 'KodeKegiatan' => 1, 'KodeBidang' => 2, 'KodeUrusanWajib' => 3, 'KodeProgram' => 4, 'KodeSasaran' => 5, 'KodeIndikator' => 6, 'AlokasiDana' => 7, 'NamaKegiatan' => 8, 'Masukan' => 9, 'Output' => 10, 'Outcome' => 11, 'Benefit' => 12, 'Impact' => 13, 'Tipe' => 14, 'KegiatanActive' => 15, 'ToKegiatanCode' => 16, 'Catatan' => 17, 'TargetOutcome' => 18, 'Lokasi' => 19, 'JumlahPrev' => 20, 'JumlahNow' => 21, 'JumlahNext' => 22, 'KodeProgram2' => 23, 'KodeUrusan' => 24, 'LastUpdateUser' => 25, 'LastUpdateTime' => 26, 'LastUpdateIp' => 27, 'Tahap' => 28, 'KodeMisi' => 29, 'KodeTujuan' => 30, 'Ranking' => 31, 'Nomor13' => 32, 'PpaNama' => 33, 'PpaPangkat' => 34, 'PpaNip' => 35, 'Lanjutan' => 36, 'UserId' => 37, 'Id' => 38, 'Tahun' => 39, 'TambahanPagu' => 40, 'Gender' => 41, 'KodeKegKeuangan' => 42, 'UserIdLama' => 43, 'Indikator' => 44, 'IsDak' => 45, 'KodeKegiatanAsal' => 46, 'KodeKegKeuanganAsal' => 47, 'ThKeMultiyears' => 48, 'KelompokSasaran' => 49, 'PaguBappeko' => 50, 'KodeDpa' => 51, 'UserIdPptk' => 52, 'UserIdKpa' => 53, 'CatatanPembahasan' => 54, 'VerifikasiBpkpd' => 55, 'VerifikasiBappeko' => 56, 'VerifikasiPenyelia' => 57, 'VerifikasiBagianHukum' => 58, 'VerifikasiInspektorat' => 59, 'VerifikasiBadanKepegawaian' => 60, 'VerifikasiLppa' => 61, ),
		BasePeer::TYPE_COLNAME => array (MasterKegiatanBpPeer::UNIT_ID => 0, MasterKegiatanBpPeer::KODE_KEGIATAN => 1, MasterKegiatanBpPeer::KODE_BIDANG => 2, MasterKegiatanBpPeer::KODE_URUSAN_WAJIB => 3, MasterKegiatanBpPeer::KODE_PROGRAM => 4, MasterKegiatanBpPeer::KODE_SASARAN => 5, MasterKegiatanBpPeer::KODE_INDIKATOR => 6, MasterKegiatanBpPeer::ALOKASI_DANA => 7, MasterKegiatanBpPeer::NAMA_KEGIATAN => 8, MasterKegiatanBpPeer::MASUKAN => 9, MasterKegiatanBpPeer::OUTPUT => 10, MasterKegiatanBpPeer::OUTCOME => 11, MasterKegiatanBpPeer::BENEFIT => 12, MasterKegiatanBpPeer::IMPACT => 13, MasterKegiatanBpPeer::TIPE => 14, MasterKegiatanBpPeer::KEGIATAN_ACTIVE => 15, MasterKegiatanBpPeer::TO_KEGIATAN_CODE => 16, MasterKegiatanBpPeer::CATATAN => 17, MasterKegiatanBpPeer::TARGET_OUTCOME => 18, MasterKegiatanBpPeer::LOKASI => 19, MasterKegiatanBpPeer::JUMLAH_PREV => 20, MasterKegiatanBpPeer::JUMLAH_NOW => 21, MasterKegiatanBpPeer::JUMLAH_NEXT => 22, MasterKegiatanBpPeer::KODE_PROGRAM2 => 23, MasterKegiatanBpPeer::KODE_URUSAN => 24, MasterKegiatanBpPeer::LAST_UPDATE_USER => 25, MasterKegiatanBpPeer::LAST_UPDATE_TIME => 26, MasterKegiatanBpPeer::LAST_UPDATE_IP => 27, MasterKegiatanBpPeer::TAHAP => 28, MasterKegiatanBpPeer::KODE_MISI => 29, MasterKegiatanBpPeer::KODE_TUJUAN => 30, MasterKegiatanBpPeer::RANKING => 31, MasterKegiatanBpPeer::NOMOR13 => 32, MasterKegiatanBpPeer::PPA_NAMA => 33, MasterKegiatanBpPeer::PPA_PANGKAT => 34, MasterKegiatanBpPeer::PPA_NIP => 35, MasterKegiatanBpPeer::LANJUTAN => 36, MasterKegiatanBpPeer::USER_ID => 37, MasterKegiatanBpPeer::ID => 38, MasterKegiatanBpPeer::TAHUN => 39, MasterKegiatanBpPeer::TAMBAHAN_PAGU => 40, MasterKegiatanBpPeer::GENDER => 41, MasterKegiatanBpPeer::KODE_KEG_KEUANGAN => 42, MasterKegiatanBpPeer::USER_ID_LAMA => 43, MasterKegiatanBpPeer::INDIKATOR => 44, MasterKegiatanBpPeer::IS_DAK => 45, MasterKegiatanBpPeer::KODE_KEGIATAN_ASAL => 46, MasterKegiatanBpPeer::KODE_KEG_KEUANGAN_ASAL => 47, MasterKegiatanBpPeer::TH_KE_MULTIYEARS => 48, MasterKegiatanBpPeer::KELOMPOK_SASARAN => 49, MasterKegiatanBpPeer::PAGU_BAPPEKO => 50, MasterKegiatanBpPeer::KODE_DPA => 51, MasterKegiatanBpPeer::USER_ID_PPTK => 52, MasterKegiatanBpPeer::USER_ID_KPA => 53, MasterKegiatanBpPeer::CATATAN_PEMBAHASAN => 54, MasterKegiatanBpPeer::VERIFIKASI_BPKPD => 55, MasterKegiatanBpPeer::VERIFIKASI_BAPPEKO => 56, MasterKegiatanBpPeer::VERIFIKASI_PENYELIA => 57, MasterKegiatanBpPeer::VERIFIKASI_BAGIAN_HUKUM => 58, MasterKegiatanBpPeer::VERIFIKASI_INSPEKTORAT => 59, MasterKegiatanBpPeer::VERIFIKASI_BADAN_KEPEGAWAIAN => 60, MasterKegiatanBpPeer::VERIFIKASI_LPPA => 61, ),
		BasePeer::TYPE_FIELDNAME => array ('unit_id' => 0, 'kode_kegiatan' => 1, 'kode_bidang' => 2, 'kode_urusan_wajib' => 3, 'kode_program' => 4, 'kode_sasaran' => 5, 'kode_indikator' => 6, 'alokasi_dana' => 7, 'nama_kegiatan' => 8, 'masukan' => 9, 'output' => 10, 'outcome' => 11, 'benefit' => 12, 'impact' => 13, 'tipe' => 14, 'kegiatan_active' => 15, 'to_kegiatan_code' => 16, 'catatan' => 17, 'target_outcome' => 18, 'lokasi' => 19, 'jumlah_prev' => 20, 'jumlah_now' => 21, 'jumlah_next' => 22, 'kode_program2' => 23, 'kode_urusan' => 24, 'last_update_user' => 25, 'last_update_time' => 26, 'last_update_ip' => 27, 'tahap' => 28, 'kode_misi' => 29, 'kode_tujuan' => 30, 'ranking' => 31, 'nomor13' => 32, 'ppa_nama' => 33, 'ppa_pangkat' => 34, 'ppa_nip' => 35, 'lanjutan' => 36, 'user_id' => 37, 'id' => 38, 'tahun' => 39, 'tambahan_pagu' => 40, 'gender' => 41, 'kode_keg_keuangan' => 42, 'user_id_lama' => 43, 'indikator' => 44, 'is_dak' => 45, 'kode_kegiatan_asal' => 46, 'kode_keg_keuangan_asal' => 47, 'th_ke_multiyears' => 48, 'kelompok_sasaran' => 49, 'pagu_bappeko' => 50, 'kode_dpa' => 51, 'user_id_pptk' => 52, 'user_id_kpa' => 53, 'catatan_pembahasan' => 54, 'verifikasi_bpkpd' => 55, 'verifikasi_bappeko' => 56, 'verifikasi_penyelia' => 57, 'verifikasi_bagian_hukum' => 58, 'verifikasi_inspektorat' => 59, 'verifikasi_badan_kepegawaian' => 60, 'verifikasi_lppa' => 61, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/MasterKegiatanBpMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.MasterKegiatanBpMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = MasterKegiatanBpPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(MasterKegiatanBpPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(MasterKegiatanBpPeer::UNIT_ID);

		$criteria->addSelectColumn(MasterKegiatanBpPeer::KODE_KEGIATAN);

		$criteria->addSelectColumn(MasterKegiatanBpPeer::KODE_BIDANG);

		$criteria->addSelectColumn(MasterKegiatanBpPeer::KODE_URUSAN_WAJIB);

		$criteria->addSelectColumn(MasterKegiatanBpPeer::KODE_PROGRAM);

		$criteria->addSelectColumn(MasterKegiatanBpPeer::KODE_SASARAN);

		$criteria->addSelectColumn(MasterKegiatanBpPeer::KODE_INDIKATOR);

		$criteria->addSelectColumn(MasterKegiatanBpPeer::ALOKASI_DANA);

		$criteria->addSelectColumn(MasterKegiatanBpPeer::NAMA_KEGIATAN);

		$criteria->addSelectColumn(MasterKegiatanBpPeer::MASUKAN);

		$criteria->addSelectColumn(MasterKegiatanBpPeer::OUTPUT);

		$criteria->addSelectColumn(MasterKegiatanBpPeer::OUTCOME);

		$criteria->addSelectColumn(MasterKegiatanBpPeer::BENEFIT);

		$criteria->addSelectColumn(MasterKegiatanBpPeer::IMPACT);

		$criteria->addSelectColumn(MasterKegiatanBpPeer::TIPE);

		$criteria->addSelectColumn(MasterKegiatanBpPeer::KEGIATAN_ACTIVE);

		$criteria->addSelectColumn(MasterKegiatanBpPeer::TO_KEGIATAN_CODE);

		$criteria->addSelectColumn(MasterKegiatanBpPeer::CATATAN);

		$criteria->addSelectColumn(MasterKegiatanBpPeer::TARGET_OUTCOME);

		$criteria->addSelectColumn(MasterKegiatanBpPeer::LOKASI);

		$criteria->addSelectColumn(MasterKegiatanBpPeer::JUMLAH_PREV);

		$criteria->addSelectColumn(MasterKegiatanBpPeer::JUMLAH_NOW);

		$criteria->addSelectColumn(MasterKegiatanBpPeer::JUMLAH_NEXT);

		$criteria->addSelectColumn(MasterKegiatanBpPeer::KODE_PROGRAM2);

		$criteria->addSelectColumn(MasterKegiatanBpPeer::KODE_URUSAN);

		$criteria->addSelectColumn(MasterKegiatanBpPeer::LAST_UPDATE_USER);

		$criteria->addSelectColumn(MasterKegiatanBpPeer::LAST_UPDATE_TIME);

		$criteria->addSelectColumn(MasterKegiatanBpPeer::LAST_UPDATE_IP);

		$criteria->addSelectColumn(MasterKegiatanBpPeer::TAHAP);

		$criteria->addSelectColumn(MasterKegiatanBpPeer::KODE_MISI);

		$criteria->addSelectColumn(MasterKegiatanBpPeer::KODE_TUJUAN);

		$criteria->addSelectColumn(MasterKegiatanBpPeer::RANKING);

		$criteria->addSelectColumn(MasterKegiatanBpPeer::NOMOR13);

		$criteria->addSelectColumn(MasterKegiatanBpPeer::PPA_NAMA);

		$criteria->addSelectColumn(MasterKegiatanBpPeer::PPA_PANGKAT);

		$criteria->addSelectColumn(MasterKegiatanBpPeer::PPA_NIP);

		$criteria->addSelectColumn(MasterKegiatanBpPeer::LANJUTAN);

		$criteria->addSelectColumn(MasterKegiatanBpPeer::USER_ID);

		$criteria->addSelectColumn(MasterKegiatanBpPeer::ID);

		$criteria->addSelectColumn(MasterKegiatanBpPeer::TAHUN);

		$criteria->addSelectColumn(MasterKegiatanBpPeer::TAMBAHAN_PAGU);

		$criteria->addSelectColumn(MasterKegiatanBpPeer::GENDER);

		$criteria->addSelectColumn(MasterKegiatanBpPeer::KODE_KEG_KEUANGAN);

		$criteria->addSelectColumn(MasterKegiatanBpPeer::USER_ID_LAMA);

		$criteria->addSelectColumn(MasterKegiatanBpPeer::INDIKATOR);

		$criteria->addSelectColumn(MasterKegiatanBpPeer::IS_DAK);

		$criteria->addSelectColumn(MasterKegiatanBpPeer::KODE_KEGIATAN_ASAL);

		$criteria->addSelectColumn(MasterKegiatanBpPeer::KODE_KEG_KEUANGAN_ASAL);

		$criteria->addSelectColumn(MasterKegiatanBpPeer::TH_KE_MULTIYEARS);

		$criteria->addSelectColumn(MasterKegiatanBpPeer::KELOMPOK_SASARAN);

		$criteria->addSelectColumn(MasterKegiatanBpPeer::PAGU_BAPPEKO);

		$criteria->addSelectColumn(MasterKegiatanBpPeer::KODE_DPA);

		$criteria->addSelectColumn(MasterKegiatanBpPeer::USER_ID_PPTK);

		$criteria->addSelectColumn(MasterKegiatanBpPeer::USER_ID_KPA);

		$criteria->addSelectColumn(MasterKegiatanBpPeer::CATATAN_PEMBAHASAN);

		$criteria->addSelectColumn(MasterKegiatanBpPeer::VERIFIKASI_BPKPD);

		$criteria->addSelectColumn(MasterKegiatanBpPeer::VERIFIKASI_BAPPEKO);

		$criteria->addSelectColumn(MasterKegiatanBpPeer::VERIFIKASI_PENYELIA);

		$criteria->addSelectColumn(MasterKegiatanBpPeer::VERIFIKASI_BAGIAN_HUKUM);

		$criteria->addSelectColumn(MasterKegiatanBpPeer::VERIFIKASI_INSPEKTORAT);

		$criteria->addSelectColumn(MasterKegiatanBpPeer::VERIFIKASI_BADAN_KEPEGAWAIAN);

		$criteria->addSelectColumn(MasterKegiatanBpPeer::VERIFIKASI_LPPA);

	}

	const COUNT = 'COUNT(ebudget.master_kegiatan_bp.UNIT_ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.master_kegiatan_bp.UNIT_ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(MasterKegiatanBpPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(MasterKegiatanBpPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = MasterKegiatanBpPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = MasterKegiatanBpPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return MasterKegiatanBpPeer::populateObjects(MasterKegiatanBpPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			MasterKegiatanBpPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = MasterKegiatanBpPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return MasterKegiatanBpPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}

		$criteria->remove(MasterKegiatanBpPeer::ID); 

				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(MasterKegiatanBpPeer::UNIT_ID);
			$selectCriteria->add(MasterKegiatanBpPeer::UNIT_ID, $criteria->remove(MasterKegiatanBpPeer::UNIT_ID), $comparison);

			$comparison = $criteria->getComparison(MasterKegiatanBpPeer::KODE_KEGIATAN);
			$selectCriteria->add(MasterKegiatanBpPeer::KODE_KEGIATAN, $criteria->remove(MasterKegiatanBpPeer::KODE_KEGIATAN), $comparison);

			$comparison = $criteria->getComparison(MasterKegiatanBpPeer::ID);
			$selectCriteria->add(MasterKegiatanBpPeer::ID, $criteria->remove(MasterKegiatanBpPeer::ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(MasterKegiatanBpPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(MasterKegiatanBpPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof MasterKegiatanBp) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
												if(count($values) == count($values, COUNT_RECURSIVE))
			{
								$values = array($values);
			}
			$vals = array();
			foreach($values as $value)
			{

				$vals[0][] = $value[0];
				$vals[1][] = $value[1];
				$vals[2][] = $value[2];
			}

			$criteria->add(MasterKegiatanBpPeer::UNIT_ID, $vals[0], Criteria::IN);
			$criteria->add(MasterKegiatanBpPeer::KODE_KEGIATAN, $vals[1], Criteria::IN);
			$criteria->add(MasterKegiatanBpPeer::ID, $vals[2], Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(MasterKegiatanBp $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(MasterKegiatanBpPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(MasterKegiatanBpPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(MasterKegiatanBpPeer::DATABASE_NAME, MasterKegiatanBpPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = MasterKegiatanBpPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK( $unit_id, $kode_kegiatan, $id, $con = null) {
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$criteria = new Criteria();
		$criteria->add(MasterKegiatanBpPeer::UNIT_ID, $unit_id);
		$criteria->add(MasterKegiatanBpPeer::KODE_KEGIATAN, $kode_kegiatan);
		$criteria->add(MasterKegiatanBpPeer::ID, $id);
		$v = MasterKegiatanBpPeer::doSelect($criteria, $con);

		return !empty($v) ? $v[0] : null;
	}
} 
if (Propel::isInit()) {
			try {
		BaseMasterKegiatanBpPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/MasterKegiatanBpMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.MasterKegiatanBpMapBuilder');
}
