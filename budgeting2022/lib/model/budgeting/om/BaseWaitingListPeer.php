<?php


abstract class BaseWaitingListPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.waitinglist';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.WaitingList';

	
	const NUM_COLUMNS = 34;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const ID_WAITING = 'ebudget.waitinglist.ID_WAITING';

	
	const UNIT_ID = 'ebudget.waitinglist.UNIT_ID';

	
	const KEGIATAN_CODE = 'ebudget.waitinglist.KEGIATAN_CODE';

	
	const KEGIATAN_CODE_LAMA = 'ebudget.waitinglist.KEGIATAN_CODE_LAMA';

	
	const SUBTITLE = 'ebudget.waitinglist.SUBTITLE';

	
	const SUBTITLE_LAMA = 'ebudget.waitinglist.SUBTITLE_LAMA';

	
	const KOMPONEN_ID = 'ebudget.waitinglist.KOMPONEN_ID';

	
	const KOMPONEN_ID_LAMA = 'ebudget.waitinglist.KOMPONEN_ID_LAMA';

	
	const KOMPONEN_NAME = 'ebudget.waitinglist.KOMPONEN_NAME';

	
	const KOMPONEN_LOKASI = 'ebudget.waitinglist.KOMPONEN_LOKASI';

	
	const KOMPONEN_HARGA = 'ebudget.waitinglist.KOMPONEN_HARGA';

	
	const KOMPONEN_HARGA_LAMA = 'ebudget.waitinglist.KOMPONEN_HARGA_LAMA';

	
	const PAJAK = 'ebudget.waitinglist.PAJAK';

	
	const KOMPONEN_SATUAN = 'ebudget.waitinglist.KOMPONEN_SATUAN';

	
	const KOMPONEN_REKENING = 'ebudget.waitinglist.KOMPONEN_REKENING';

	
	const KOEFISIEN = 'ebudget.waitinglist.KOEFISIEN';

	
	const VOLUME = 'ebudget.waitinglist.VOLUME';

	
	const NILAI_ANGGARAN = 'ebudget.waitinglist.NILAI_ANGGARAN';

	
	const TAHUN_INPUT = 'ebudget.waitinglist.TAHUN_INPUT';

	
	const CREATED_AT = 'ebudget.waitinglist.CREATED_AT';

	
	const UPDATED_AT = 'ebudget.waitinglist.UPDATED_AT';

	
	const STATUS_HAPUS = 'ebudget.waitinglist.STATUS_HAPUS';

	
	const STATUS_WAITING = 'ebudget.waitinglist.STATUS_WAITING';

	
	const PRIORITAS = 'ebudget.waitinglist.PRIORITAS';

	
	const KODE_RKA = 'ebudget.waitinglist.KODE_RKA';

	
	const USER_PENGAMBIL = 'ebudget.waitinglist.USER_PENGAMBIL';

	
	const NILAI_EE = 'ebudget.waitinglist.NILAI_EE';

	
	const KETERANGAN = 'ebudget.waitinglist.KETERANGAN';

	
	const KODE_JASMAS = 'ebudget.waitinglist.KODE_JASMAS';

	
	const KECAMATAN = 'ebudget.waitinglist.KECAMATAN';

	
	const KELURAHAN = 'ebudget.waitinglist.KELURAHAN';

	
	const IS_MUSRENBANG = 'ebudget.waitinglist.IS_MUSRENBANG';

	
	const NILAI_ANGGARAN_SEMULA = 'ebudget.waitinglist.NILAI_ANGGARAN_SEMULA';

	
	const METODE_WAITINGLIST = 'ebudget.waitinglist.METODE_WAITINGLIST';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('IdWaiting', 'UnitId', 'KegiatanCode', 'KegiatanCodeLama', 'Subtitle', 'SubtitleLama', 'KomponenId', 'KomponenIdLama', 'KomponenName', 'KomponenLokasi', 'KomponenHarga', 'KomponenHargaLama', 'Pajak', 'KomponenSatuan', 'KomponenRekening', 'Koefisien', 'Volume', 'NilaiAnggaran', 'TahunInput', 'CreatedAt', 'UpdatedAt', 'StatusHapus', 'StatusWaiting', 'Prioritas', 'KodeRka', 'UserPengambil', 'NilaiEe', 'Keterangan', 'KodeJasmas', 'Kecamatan', 'Kelurahan', 'IsMusrenbang', 'NilaiAnggaranSemula', 'MetodeWaitinglist', ),
		BasePeer::TYPE_COLNAME => array (WaitingListPeer::ID_WAITING, WaitingListPeer::UNIT_ID, WaitingListPeer::KEGIATAN_CODE, WaitingListPeer::KEGIATAN_CODE_LAMA, WaitingListPeer::SUBTITLE, WaitingListPeer::SUBTITLE_LAMA, WaitingListPeer::KOMPONEN_ID, WaitingListPeer::KOMPONEN_ID_LAMA, WaitingListPeer::KOMPONEN_NAME, WaitingListPeer::KOMPONEN_LOKASI, WaitingListPeer::KOMPONEN_HARGA, WaitingListPeer::KOMPONEN_HARGA_LAMA, WaitingListPeer::PAJAK, WaitingListPeer::KOMPONEN_SATUAN, WaitingListPeer::KOMPONEN_REKENING, WaitingListPeer::KOEFISIEN, WaitingListPeer::VOLUME, WaitingListPeer::NILAI_ANGGARAN, WaitingListPeer::TAHUN_INPUT, WaitingListPeer::CREATED_AT, WaitingListPeer::UPDATED_AT, WaitingListPeer::STATUS_HAPUS, WaitingListPeer::STATUS_WAITING, WaitingListPeer::PRIORITAS, WaitingListPeer::KODE_RKA, WaitingListPeer::USER_PENGAMBIL, WaitingListPeer::NILAI_EE, WaitingListPeer::KETERANGAN, WaitingListPeer::KODE_JASMAS, WaitingListPeer::KECAMATAN, WaitingListPeer::KELURAHAN, WaitingListPeer::IS_MUSRENBANG, WaitingListPeer::NILAI_ANGGARAN_SEMULA, WaitingListPeer::METODE_WAITINGLIST, ),
		BasePeer::TYPE_FIELDNAME => array ('id_waiting', 'unit_id', 'kegiatan_code', 'kegiatan_code_lama', 'subtitle', 'subtitle_lama', 'komponen_id', 'komponen_id_lama', 'komponen_name', 'komponen_lokasi', 'komponen_harga', 'komponen_harga_lama', 'pajak', 'komponen_satuan', 'komponen_rekening', 'koefisien', 'volume', 'nilai_anggaran', 'tahun_input', 'created_at', 'updated_at', 'status_hapus', 'status_waiting', 'prioritas', 'kode_rka', 'user_pengambil', 'nilai_ee', 'keterangan', 'kode_jasmas', 'kecamatan', 'kelurahan', 'is_musrenbang', 'nilai_anggaran_semula', 'metode_waitinglist', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('IdWaiting' => 0, 'UnitId' => 1, 'KegiatanCode' => 2, 'KegiatanCodeLama' => 3, 'Subtitle' => 4, 'SubtitleLama' => 5, 'KomponenId' => 6, 'KomponenIdLama' => 7, 'KomponenName' => 8, 'KomponenLokasi' => 9, 'KomponenHarga' => 10, 'KomponenHargaLama' => 11, 'Pajak' => 12, 'KomponenSatuan' => 13, 'KomponenRekening' => 14, 'Koefisien' => 15, 'Volume' => 16, 'NilaiAnggaran' => 17, 'TahunInput' => 18, 'CreatedAt' => 19, 'UpdatedAt' => 20, 'StatusHapus' => 21, 'StatusWaiting' => 22, 'Prioritas' => 23, 'KodeRka' => 24, 'UserPengambil' => 25, 'NilaiEe' => 26, 'Keterangan' => 27, 'KodeJasmas' => 28, 'Kecamatan' => 29, 'Kelurahan' => 30, 'IsMusrenbang' => 31, 'NilaiAnggaranSemula' => 32, 'MetodeWaitinglist' => 33, ),
		BasePeer::TYPE_COLNAME => array (WaitingListPeer::ID_WAITING => 0, WaitingListPeer::UNIT_ID => 1, WaitingListPeer::KEGIATAN_CODE => 2, WaitingListPeer::KEGIATAN_CODE_LAMA => 3, WaitingListPeer::SUBTITLE => 4, WaitingListPeer::SUBTITLE_LAMA => 5, WaitingListPeer::KOMPONEN_ID => 6, WaitingListPeer::KOMPONEN_ID_LAMA => 7, WaitingListPeer::KOMPONEN_NAME => 8, WaitingListPeer::KOMPONEN_LOKASI => 9, WaitingListPeer::KOMPONEN_HARGA => 10, WaitingListPeer::KOMPONEN_HARGA_LAMA => 11, WaitingListPeer::PAJAK => 12, WaitingListPeer::KOMPONEN_SATUAN => 13, WaitingListPeer::KOMPONEN_REKENING => 14, WaitingListPeer::KOEFISIEN => 15, WaitingListPeer::VOLUME => 16, WaitingListPeer::NILAI_ANGGARAN => 17, WaitingListPeer::TAHUN_INPUT => 18, WaitingListPeer::CREATED_AT => 19, WaitingListPeer::UPDATED_AT => 20, WaitingListPeer::STATUS_HAPUS => 21, WaitingListPeer::STATUS_WAITING => 22, WaitingListPeer::PRIORITAS => 23, WaitingListPeer::KODE_RKA => 24, WaitingListPeer::USER_PENGAMBIL => 25, WaitingListPeer::NILAI_EE => 26, WaitingListPeer::KETERANGAN => 27, WaitingListPeer::KODE_JASMAS => 28, WaitingListPeer::KECAMATAN => 29, WaitingListPeer::KELURAHAN => 30, WaitingListPeer::IS_MUSRENBANG => 31, WaitingListPeer::NILAI_ANGGARAN_SEMULA => 32, WaitingListPeer::METODE_WAITINGLIST => 33, ),
		BasePeer::TYPE_FIELDNAME => array ('id_waiting' => 0, 'unit_id' => 1, 'kegiatan_code' => 2, 'kegiatan_code_lama' => 3, 'subtitle' => 4, 'subtitle_lama' => 5, 'komponen_id' => 6, 'komponen_id_lama' => 7, 'komponen_name' => 8, 'komponen_lokasi' => 9, 'komponen_harga' => 10, 'komponen_harga_lama' => 11, 'pajak' => 12, 'komponen_satuan' => 13, 'komponen_rekening' => 14, 'koefisien' => 15, 'volume' => 16, 'nilai_anggaran' => 17, 'tahun_input' => 18, 'created_at' => 19, 'updated_at' => 20, 'status_hapus' => 21, 'status_waiting' => 22, 'prioritas' => 23, 'kode_rka' => 24, 'user_pengambil' => 25, 'nilai_ee' => 26, 'keterangan' => 27, 'kode_jasmas' => 28, 'kecamatan' => 29, 'kelurahan' => 30, 'is_musrenbang' => 31, 'nilai_anggaran_semula' => 32, 'metode_waitinglist' => 33, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/WaitingListMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.WaitingListMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = WaitingListPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(WaitingListPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(WaitingListPeer::ID_WAITING);

		$criteria->addSelectColumn(WaitingListPeer::UNIT_ID);

		$criteria->addSelectColumn(WaitingListPeer::KEGIATAN_CODE);

		$criteria->addSelectColumn(WaitingListPeer::KEGIATAN_CODE_LAMA);

		$criteria->addSelectColumn(WaitingListPeer::SUBTITLE);

		$criteria->addSelectColumn(WaitingListPeer::SUBTITLE_LAMA);

		$criteria->addSelectColumn(WaitingListPeer::KOMPONEN_ID);

		$criteria->addSelectColumn(WaitingListPeer::KOMPONEN_ID_LAMA);

		$criteria->addSelectColumn(WaitingListPeer::KOMPONEN_NAME);

		$criteria->addSelectColumn(WaitingListPeer::KOMPONEN_LOKASI);

		$criteria->addSelectColumn(WaitingListPeer::KOMPONEN_HARGA);

		$criteria->addSelectColumn(WaitingListPeer::KOMPONEN_HARGA_LAMA);

		$criteria->addSelectColumn(WaitingListPeer::PAJAK);

		$criteria->addSelectColumn(WaitingListPeer::KOMPONEN_SATUAN);

		$criteria->addSelectColumn(WaitingListPeer::KOMPONEN_REKENING);

		$criteria->addSelectColumn(WaitingListPeer::KOEFISIEN);

		$criteria->addSelectColumn(WaitingListPeer::VOLUME);

		$criteria->addSelectColumn(WaitingListPeer::NILAI_ANGGARAN);

		$criteria->addSelectColumn(WaitingListPeer::TAHUN_INPUT);

		$criteria->addSelectColumn(WaitingListPeer::CREATED_AT);

		$criteria->addSelectColumn(WaitingListPeer::UPDATED_AT);

		$criteria->addSelectColumn(WaitingListPeer::STATUS_HAPUS);

		$criteria->addSelectColumn(WaitingListPeer::STATUS_WAITING);

		$criteria->addSelectColumn(WaitingListPeer::PRIORITAS);

		$criteria->addSelectColumn(WaitingListPeer::KODE_RKA);

		$criteria->addSelectColumn(WaitingListPeer::USER_PENGAMBIL);

		$criteria->addSelectColumn(WaitingListPeer::NILAI_EE);

		$criteria->addSelectColumn(WaitingListPeer::KETERANGAN);

		$criteria->addSelectColumn(WaitingListPeer::KODE_JASMAS);

		$criteria->addSelectColumn(WaitingListPeer::KECAMATAN);

		$criteria->addSelectColumn(WaitingListPeer::KELURAHAN);

		$criteria->addSelectColumn(WaitingListPeer::IS_MUSRENBANG);

		$criteria->addSelectColumn(WaitingListPeer::NILAI_ANGGARAN_SEMULA);

		$criteria->addSelectColumn(WaitingListPeer::METODE_WAITINGLIST);

	}

	const COUNT = 'COUNT(ebudget.waitinglist.ID_WAITING)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.waitinglist.ID_WAITING)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(WaitingListPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(WaitingListPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = WaitingListPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = WaitingListPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return WaitingListPeer::populateObjects(WaitingListPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			WaitingListPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = WaitingListPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return WaitingListPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}

		$criteria->remove(WaitingListPeer::ID_WAITING); 

				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(WaitingListPeer::ID_WAITING);
			$selectCriteria->add(WaitingListPeer::ID_WAITING, $criteria->remove(WaitingListPeer::ID_WAITING), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(WaitingListPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(WaitingListPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof WaitingList) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(WaitingListPeer::ID_WAITING, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(WaitingList $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(WaitingListPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(WaitingListPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(WaitingListPeer::DATABASE_NAME, WaitingListPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = WaitingListPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(WaitingListPeer::DATABASE_NAME);

		$criteria->add(WaitingListPeer::ID_WAITING, $pk);


		$v = WaitingListPeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(WaitingListPeer::ID_WAITING, $pks, Criteria::IN);
			$objs = WaitingListPeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BaseWaitingListPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/WaitingListMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.WaitingListMapBuilder');
}
