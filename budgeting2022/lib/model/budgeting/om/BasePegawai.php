<?php


abstract class BasePegawai extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $nip;


	
	protected $nip_lama;


	
	protected $nama;


	
	protected $tempat_lahir;


	
	protected $tanggal_lahir;


	
	protected $nama_pangkat;


	
	protected $nama_golongan;


	
	protected $tmt_pangkat;


	
	protected $tmt_cpns;


	
	protected $jenis_kelamin;


	
	protected $nama_agama;


	
	protected $nama_jabatan;


	
	protected $eselon;


	
	protected $tmt_jabatan;


	
	protected $nama_jurusan_sk;


	
	protected $lulus;


	
	protected $jenis_pegawai;


	
	protected $nama_instansi;


	
	protected $nama_unit_kerja;


	
	protected $nama_unit_kerja2;


	
	protected $unit_id;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getNip()
	{

		return $this->nip;
	}

	
	public function getNipLama()
	{

		return $this->nip_lama;
	}

	
	public function getNama()
	{

		return $this->nama;
	}

	
	public function getTempatLahir()
	{

		return $this->tempat_lahir;
	}

	
	public function getTanggalLahir()
	{

		return $this->tanggal_lahir;
	}

	
	public function getNamaPangkat()
	{

		return $this->nama_pangkat;
	}

	
	public function getNamaGolongan()
	{

		return $this->nama_golongan;
	}

	
	public function getTmtPangkat()
	{

		return $this->tmt_pangkat;
	}

	
	public function getTmtCpns()
	{

		return $this->tmt_cpns;
	}

	
	public function getJenisKelamin()
	{

		return $this->jenis_kelamin;
	}

	
	public function getNamaAgama()
	{

		return $this->nama_agama;
	}

	
	public function getNamaJabatan()
	{

		return $this->nama_jabatan;
	}

	
	public function getEselon()
	{

		return $this->eselon;
	}

	
	public function getTmtJabatan()
	{

		return $this->tmt_jabatan;
	}

	
	public function getNamaJurusanSk()
	{

		return $this->nama_jurusan_sk;
	}

	
	public function getLulus()
	{

		return $this->lulus;
	}

	
	public function getJenisPegawai()
	{

		return $this->jenis_pegawai;
	}

	
	public function getNamaInstansi()
	{

		return $this->nama_instansi;
	}

	
	public function getNamaUnitKerja()
	{

		return $this->nama_unit_kerja;
	}

	
	public function getNamaUnitKerja2()
	{

		return $this->nama_unit_kerja2;
	}

	
	public function getUnitId()
	{

		return $this->unit_id;
	}

	
	public function setNip($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nip !== $v) {
			$this->nip = $v;
			$this->modifiedColumns[] = PegawaiPeer::NIP;
		}

	} 
	
	public function setNipLama($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nip_lama !== $v) {
			$this->nip_lama = $v;
			$this->modifiedColumns[] = PegawaiPeer::NIP_LAMA;
		}

	} 
	
	public function setNama($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nama !== $v) {
			$this->nama = $v;
			$this->modifiedColumns[] = PegawaiPeer::NAMA;
		}

	} 
	
	public function setTempatLahir($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->tempat_lahir !== $v) {
			$this->tempat_lahir = $v;
			$this->modifiedColumns[] = PegawaiPeer::TEMPAT_LAHIR;
		}

	} 
	
	public function setTanggalLahir($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->tanggal_lahir !== $v) {
			$this->tanggal_lahir = $v;
			$this->modifiedColumns[] = PegawaiPeer::TANGGAL_LAHIR;
		}

	} 
	
	public function setNamaPangkat($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nama_pangkat !== $v) {
			$this->nama_pangkat = $v;
			$this->modifiedColumns[] = PegawaiPeer::NAMA_PANGKAT;
		}

	} 
	
	public function setNamaGolongan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nama_golongan !== $v) {
			$this->nama_golongan = $v;
			$this->modifiedColumns[] = PegawaiPeer::NAMA_GOLONGAN;
		}

	} 
	
	public function setTmtPangkat($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->tmt_pangkat !== $v) {
			$this->tmt_pangkat = $v;
			$this->modifiedColumns[] = PegawaiPeer::TMT_PANGKAT;
		}

	} 
	
	public function setTmtCpns($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->tmt_cpns !== $v) {
			$this->tmt_cpns = $v;
			$this->modifiedColumns[] = PegawaiPeer::TMT_CPNS;
		}

	} 
	
	public function setJenisKelamin($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->jenis_kelamin !== $v) {
			$this->jenis_kelamin = $v;
			$this->modifiedColumns[] = PegawaiPeer::JENIS_KELAMIN;
		}

	} 
	
	public function setNamaAgama($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nama_agama !== $v) {
			$this->nama_agama = $v;
			$this->modifiedColumns[] = PegawaiPeer::NAMA_AGAMA;
		}

	} 
	
	public function setNamaJabatan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nama_jabatan !== $v) {
			$this->nama_jabatan = $v;
			$this->modifiedColumns[] = PegawaiPeer::NAMA_JABATAN;
		}

	} 
	
	public function setEselon($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->eselon !== $v) {
			$this->eselon = $v;
			$this->modifiedColumns[] = PegawaiPeer::ESELON;
		}

	} 
	
	public function setTmtJabatan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->tmt_jabatan !== $v) {
			$this->tmt_jabatan = $v;
			$this->modifiedColumns[] = PegawaiPeer::TMT_JABATAN;
		}

	} 
	
	public function setNamaJurusanSk($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nama_jurusan_sk !== $v) {
			$this->nama_jurusan_sk = $v;
			$this->modifiedColumns[] = PegawaiPeer::NAMA_JURUSAN_SK;
		}

	} 
	
	public function setLulus($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->lulus !== $v) {
			$this->lulus = $v;
			$this->modifiedColumns[] = PegawaiPeer::LULUS;
		}

	} 
	
	public function setJenisPegawai($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->jenis_pegawai !== $v) {
			$this->jenis_pegawai = $v;
			$this->modifiedColumns[] = PegawaiPeer::JENIS_PEGAWAI;
		}

	} 
	
	public function setNamaInstansi($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nama_instansi !== $v) {
			$this->nama_instansi = $v;
			$this->modifiedColumns[] = PegawaiPeer::NAMA_INSTANSI;
		}

	} 
	
	public function setNamaUnitKerja($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nama_unit_kerja !== $v) {
			$this->nama_unit_kerja = $v;
			$this->modifiedColumns[] = PegawaiPeer::NAMA_UNIT_KERJA;
		}

	} 
	
	public function setNamaUnitKerja2($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nama_unit_kerja2 !== $v) {
			$this->nama_unit_kerja2 = $v;
			$this->modifiedColumns[] = PegawaiPeer::NAMA_UNIT_KERJA2;
		}

	} 
	
	public function setUnitId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->unit_id !== $v) {
			$this->unit_id = $v;
			$this->modifiedColumns[] = PegawaiPeer::UNIT_ID;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->nip = $rs->getString($startcol + 0);

			$this->nip_lama = $rs->getString($startcol + 1);

			$this->nama = $rs->getString($startcol + 2);

			$this->tempat_lahir = $rs->getString($startcol + 3);

			$this->tanggal_lahir = $rs->getString($startcol + 4);

			$this->nama_pangkat = $rs->getString($startcol + 5);

			$this->nama_golongan = $rs->getString($startcol + 6);

			$this->tmt_pangkat = $rs->getString($startcol + 7);

			$this->tmt_cpns = $rs->getString($startcol + 8);

			$this->jenis_kelamin = $rs->getString($startcol + 9);

			$this->nama_agama = $rs->getString($startcol + 10);

			$this->nama_jabatan = $rs->getString($startcol + 11);

			$this->eselon = $rs->getString($startcol + 12);

			$this->tmt_jabatan = $rs->getString($startcol + 13);

			$this->nama_jurusan_sk = $rs->getString($startcol + 14);

			$this->lulus = $rs->getString($startcol + 15);

			$this->jenis_pegawai = $rs->getString($startcol + 16);

			$this->nama_instansi = $rs->getString($startcol + 17);

			$this->nama_unit_kerja = $rs->getString($startcol + 18);

			$this->nama_unit_kerja2 = $rs->getString($startcol + 19);

			$this->unit_id = $rs->getString($startcol + 20);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 21; 
		} catch (Exception $e) {
			throw new PropelException("Error populating Pegawai object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(PegawaiPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			PegawaiPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(PegawaiPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = PegawaiPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += PegawaiPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = PegawaiPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = PegawaiPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getNip();
				break;
			case 1:
				return $this->getNipLama();
				break;
			case 2:
				return $this->getNama();
				break;
			case 3:
				return $this->getTempatLahir();
				break;
			case 4:
				return $this->getTanggalLahir();
				break;
			case 5:
				return $this->getNamaPangkat();
				break;
			case 6:
				return $this->getNamaGolongan();
				break;
			case 7:
				return $this->getTmtPangkat();
				break;
			case 8:
				return $this->getTmtCpns();
				break;
			case 9:
				return $this->getJenisKelamin();
				break;
			case 10:
				return $this->getNamaAgama();
				break;
			case 11:
				return $this->getNamaJabatan();
				break;
			case 12:
				return $this->getEselon();
				break;
			case 13:
				return $this->getTmtJabatan();
				break;
			case 14:
				return $this->getNamaJurusanSk();
				break;
			case 15:
				return $this->getLulus();
				break;
			case 16:
				return $this->getJenisPegawai();
				break;
			case 17:
				return $this->getNamaInstansi();
				break;
			case 18:
				return $this->getNamaUnitKerja();
				break;
			case 19:
				return $this->getNamaUnitKerja2();
				break;
			case 20:
				return $this->getUnitId();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = PegawaiPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getNip(),
			$keys[1] => $this->getNipLama(),
			$keys[2] => $this->getNama(),
			$keys[3] => $this->getTempatLahir(),
			$keys[4] => $this->getTanggalLahir(),
			$keys[5] => $this->getNamaPangkat(),
			$keys[6] => $this->getNamaGolongan(),
			$keys[7] => $this->getTmtPangkat(),
			$keys[8] => $this->getTmtCpns(),
			$keys[9] => $this->getJenisKelamin(),
			$keys[10] => $this->getNamaAgama(),
			$keys[11] => $this->getNamaJabatan(),
			$keys[12] => $this->getEselon(),
			$keys[13] => $this->getTmtJabatan(),
			$keys[14] => $this->getNamaJurusanSk(),
			$keys[15] => $this->getLulus(),
			$keys[16] => $this->getJenisPegawai(),
			$keys[17] => $this->getNamaInstansi(),
			$keys[18] => $this->getNamaUnitKerja(),
			$keys[19] => $this->getNamaUnitKerja2(),
			$keys[20] => $this->getUnitId(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = PegawaiPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setNip($value);
				break;
			case 1:
				$this->setNipLama($value);
				break;
			case 2:
				$this->setNama($value);
				break;
			case 3:
				$this->setTempatLahir($value);
				break;
			case 4:
				$this->setTanggalLahir($value);
				break;
			case 5:
				$this->setNamaPangkat($value);
				break;
			case 6:
				$this->setNamaGolongan($value);
				break;
			case 7:
				$this->setTmtPangkat($value);
				break;
			case 8:
				$this->setTmtCpns($value);
				break;
			case 9:
				$this->setJenisKelamin($value);
				break;
			case 10:
				$this->setNamaAgama($value);
				break;
			case 11:
				$this->setNamaJabatan($value);
				break;
			case 12:
				$this->setEselon($value);
				break;
			case 13:
				$this->setTmtJabatan($value);
				break;
			case 14:
				$this->setNamaJurusanSk($value);
				break;
			case 15:
				$this->setLulus($value);
				break;
			case 16:
				$this->setJenisPegawai($value);
				break;
			case 17:
				$this->setNamaInstansi($value);
				break;
			case 18:
				$this->setNamaUnitKerja($value);
				break;
			case 19:
				$this->setNamaUnitKerja2($value);
				break;
			case 20:
				$this->setUnitId($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = PegawaiPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setNip($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setNipLama($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setNama($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setTempatLahir($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setTanggalLahir($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setNamaPangkat($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setNamaGolongan($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setTmtPangkat($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setTmtCpns($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setJenisKelamin($arr[$keys[9]]);
		if (array_key_exists($keys[10], $arr)) $this->setNamaAgama($arr[$keys[10]]);
		if (array_key_exists($keys[11], $arr)) $this->setNamaJabatan($arr[$keys[11]]);
		if (array_key_exists($keys[12], $arr)) $this->setEselon($arr[$keys[12]]);
		if (array_key_exists($keys[13], $arr)) $this->setTmtJabatan($arr[$keys[13]]);
		if (array_key_exists($keys[14], $arr)) $this->setNamaJurusanSk($arr[$keys[14]]);
		if (array_key_exists($keys[15], $arr)) $this->setLulus($arr[$keys[15]]);
		if (array_key_exists($keys[16], $arr)) $this->setJenisPegawai($arr[$keys[16]]);
		if (array_key_exists($keys[17], $arr)) $this->setNamaInstansi($arr[$keys[17]]);
		if (array_key_exists($keys[18], $arr)) $this->setNamaUnitKerja($arr[$keys[18]]);
		if (array_key_exists($keys[19], $arr)) $this->setNamaUnitKerja2($arr[$keys[19]]);
		if (array_key_exists($keys[20], $arr)) $this->setUnitId($arr[$keys[20]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(PegawaiPeer::DATABASE_NAME);

		if ($this->isColumnModified(PegawaiPeer::NIP)) $criteria->add(PegawaiPeer::NIP, $this->nip);
		if ($this->isColumnModified(PegawaiPeer::NIP_LAMA)) $criteria->add(PegawaiPeer::NIP_LAMA, $this->nip_lama);
		if ($this->isColumnModified(PegawaiPeer::NAMA)) $criteria->add(PegawaiPeer::NAMA, $this->nama);
		if ($this->isColumnModified(PegawaiPeer::TEMPAT_LAHIR)) $criteria->add(PegawaiPeer::TEMPAT_LAHIR, $this->tempat_lahir);
		if ($this->isColumnModified(PegawaiPeer::TANGGAL_LAHIR)) $criteria->add(PegawaiPeer::TANGGAL_LAHIR, $this->tanggal_lahir);
		if ($this->isColumnModified(PegawaiPeer::NAMA_PANGKAT)) $criteria->add(PegawaiPeer::NAMA_PANGKAT, $this->nama_pangkat);
		if ($this->isColumnModified(PegawaiPeer::NAMA_GOLONGAN)) $criteria->add(PegawaiPeer::NAMA_GOLONGAN, $this->nama_golongan);
		if ($this->isColumnModified(PegawaiPeer::TMT_PANGKAT)) $criteria->add(PegawaiPeer::TMT_PANGKAT, $this->tmt_pangkat);
		if ($this->isColumnModified(PegawaiPeer::TMT_CPNS)) $criteria->add(PegawaiPeer::TMT_CPNS, $this->tmt_cpns);
		if ($this->isColumnModified(PegawaiPeer::JENIS_KELAMIN)) $criteria->add(PegawaiPeer::JENIS_KELAMIN, $this->jenis_kelamin);
		if ($this->isColumnModified(PegawaiPeer::NAMA_AGAMA)) $criteria->add(PegawaiPeer::NAMA_AGAMA, $this->nama_agama);
		if ($this->isColumnModified(PegawaiPeer::NAMA_JABATAN)) $criteria->add(PegawaiPeer::NAMA_JABATAN, $this->nama_jabatan);
		if ($this->isColumnModified(PegawaiPeer::ESELON)) $criteria->add(PegawaiPeer::ESELON, $this->eselon);
		if ($this->isColumnModified(PegawaiPeer::TMT_JABATAN)) $criteria->add(PegawaiPeer::TMT_JABATAN, $this->tmt_jabatan);
		if ($this->isColumnModified(PegawaiPeer::NAMA_JURUSAN_SK)) $criteria->add(PegawaiPeer::NAMA_JURUSAN_SK, $this->nama_jurusan_sk);
		if ($this->isColumnModified(PegawaiPeer::LULUS)) $criteria->add(PegawaiPeer::LULUS, $this->lulus);
		if ($this->isColumnModified(PegawaiPeer::JENIS_PEGAWAI)) $criteria->add(PegawaiPeer::JENIS_PEGAWAI, $this->jenis_pegawai);
		if ($this->isColumnModified(PegawaiPeer::NAMA_INSTANSI)) $criteria->add(PegawaiPeer::NAMA_INSTANSI, $this->nama_instansi);
		if ($this->isColumnModified(PegawaiPeer::NAMA_UNIT_KERJA)) $criteria->add(PegawaiPeer::NAMA_UNIT_KERJA, $this->nama_unit_kerja);
		if ($this->isColumnModified(PegawaiPeer::NAMA_UNIT_KERJA2)) $criteria->add(PegawaiPeer::NAMA_UNIT_KERJA2, $this->nama_unit_kerja2);
		if ($this->isColumnModified(PegawaiPeer::UNIT_ID)) $criteria->add(PegawaiPeer::UNIT_ID, $this->unit_id);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(PegawaiPeer::DATABASE_NAME);


		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return null;
	}

	
	 public function setPrimaryKey($pk)
	 {
		 	 }

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setNip($this->nip);

		$copyObj->setNipLama($this->nip_lama);

		$copyObj->setNama($this->nama);

		$copyObj->setTempatLahir($this->tempat_lahir);

		$copyObj->setTanggalLahir($this->tanggal_lahir);

		$copyObj->setNamaPangkat($this->nama_pangkat);

		$copyObj->setNamaGolongan($this->nama_golongan);

		$copyObj->setTmtPangkat($this->tmt_pangkat);

		$copyObj->setTmtCpns($this->tmt_cpns);

		$copyObj->setJenisKelamin($this->jenis_kelamin);

		$copyObj->setNamaAgama($this->nama_agama);

		$copyObj->setNamaJabatan($this->nama_jabatan);

		$copyObj->setEselon($this->eselon);

		$copyObj->setTmtJabatan($this->tmt_jabatan);

		$copyObj->setNamaJurusanSk($this->nama_jurusan_sk);

		$copyObj->setLulus($this->lulus);

		$copyObj->setJenisPegawai($this->jenis_pegawai);

		$copyObj->setNamaInstansi($this->nama_instansi);

		$copyObj->setNamaUnitKerja($this->nama_unit_kerja);

		$copyObj->setNamaUnitKerja2($this->nama_unit_kerja2);

		$copyObj->setUnitId($this->unit_id);


		$copyObj->setNew(true);

	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new PegawaiPeer();
		}
		return self::$peer;
	}

} 