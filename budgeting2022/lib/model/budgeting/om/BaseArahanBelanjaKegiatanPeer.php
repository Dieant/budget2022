<?php


abstract class BaseArahanBelanjaKegiatanPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.arahan_belanja_kegiatan';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.ArahanBelanjaKegiatan';

	
	const NUM_COLUMNS = 9;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const ID = 'ebudget.arahan_belanja_kegiatan.ID';

	
	const UNIT_ID = 'ebudget.arahan_belanja_kegiatan.UNIT_ID';

	
	const KEGIATAN_CODE = 'ebudget.arahan_belanja_kegiatan.KEGIATAN_CODE';

	
	const KODE_BELANJA = 'ebudget.arahan_belanja_kegiatan.KODE_BELANJA';

	
	const ANGGARAN = 'ebudget.arahan_belanja_kegiatan.ANGGARAN';

	
	const STATUS = 'ebudget.arahan_belanja_kegiatan.STATUS';

	
	const KODE_DETAIL_KEGIATAN = 'ebudget.arahan_belanja_kegiatan.KODE_DETAIL_KEGIATAN';

	
	const CREATED_AT = 'ebudget.arahan_belanja_kegiatan.CREATED_AT';

	
	const UPDATED_AT = 'ebudget.arahan_belanja_kegiatan.UPDATED_AT';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('Id', 'UnitId', 'KegiatanCode', 'KodeBelanja', 'Anggaran', 'Status', 'KodeDetailKegiatan', 'CreatedAt', 'UpdatedAt', ),
		BasePeer::TYPE_COLNAME => array (ArahanBelanjaKegiatanPeer::ID, ArahanBelanjaKegiatanPeer::UNIT_ID, ArahanBelanjaKegiatanPeer::KEGIATAN_CODE, ArahanBelanjaKegiatanPeer::KODE_BELANJA, ArahanBelanjaKegiatanPeer::ANGGARAN, ArahanBelanjaKegiatanPeer::STATUS, ArahanBelanjaKegiatanPeer::KODE_DETAIL_KEGIATAN, ArahanBelanjaKegiatanPeer::CREATED_AT, ArahanBelanjaKegiatanPeer::UPDATED_AT, ),
		BasePeer::TYPE_FIELDNAME => array ('id', 'unit_id', 'kegiatan_code', 'kode_belanja', 'anggaran', 'status', 'kode_detail_kegiatan', 'created_at', 'updated_at', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('Id' => 0, 'UnitId' => 1, 'KegiatanCode' => 2, 'KodeBelanja' => 3, 'Anggaran' => 4, 'Status' => 5, 'KodeDetailKegiatan' => 6, 'CreatedAt' => 7, 'UpdatedAt' => 8, ),
		BasePeer::TYPE_COLNAME => array (ArahanBelanjaKegiatanPeer::ID => 0, ArahanBelanjaKegiatanPeer::UNIT_ID => 1, ArahanBelanjaKegiatanPeer::KEGIATAN_CODE => 2, ArahanBelanjaKegiatanPeer::KODE_BELANJA => 3, ArahanBelanjaKegiatanPeer::ANGGARAN => 4, ArahanBelanjaKegiatanPeer::STATUS => 5, ArahanBelanjaKegiatanPeer::KODE_DETAIL_KEGIATAN => 6, ArahanBelanjaKegiatanPeer::CREATED_AT => 7, ArahanBelanjaKegiatanPeer::UPDATED_AT => 8, ),
		BasePeer::TYPE_FIELDNAME => array ('id' => 0, 'unit_id' => 1, 'kegiatan_code' => 2, 'kode_belanja' => 3, 'anggaran' => 4, 'status' => 5, 'kode_detail_kegiatan' => 6, 'created_at' => 7, 'updated_at' => 8, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/ArahanBelanjaKegiatanMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.ArahanBelanjaKegiatanMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = ArahanBelanjaKegiatanPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(ArahanBelanjaKegiatanPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(ArahanBelanjaKegiatanPeer::ID);

		$criteria->addSelectColumn(ArahanBelanjaKegiatanPeer::UNIT_ID);

		$criteria->addSelectColumn(ArahanBelanjaKegiatanPeer::KEGIATAN_CODE);

		$criteria->addSelectColumn(ArahanBelanjaKegiatanPeer::KODE_BELANJA);

		$criteria->addSelectColumn(ArahanBelanjaKegiatanPeer::ANGGARAN);

		$criteria->addSelectColumn(ArahanBelanjaKegiatanPeer::STATUS);

		$criteria->addSelectColumn(ArahanBelanjaKegiatanPeer::KODE_DETAIL_KEGIATAN);

		$criteria->addSelectColumn(ArahanBelanjaKegiatanPeer::CREATED_AT);

		$criteria->addSelectColumn(ArahanBelanjaKegiatanPeer::UPDATED_AT);

	}

	const COUNT = 'COUNT(ebudget.arahan_belanja_kegiatan.ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.arahan_belanja_kegiatan.ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(ArahanBelanjaKegiatanPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(ArahanBelanjaKegiatanPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = ArahanBelanjaKegiatanPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = ArahanBelanjaKegiatanPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return ArahanBelanjaKegiatanPeer::populateObjects(ArahanBelanjaKegiatanPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			ArahanBelanjaKegiatanPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = ArahanBelanjaKegiatanPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return ArahanBelanjaKegiatanPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(ArahanBelanjaKegiatanPeer::ID);
			$selectCriteria->add(ArahanBelanjaKegiatanPeer::ID, $criteria->remove(ArahanBelanjaKegiatanPeer::ID), $comparison);

			$comparison = $criteria->getComparison(ArahanBelanjaKegiatanPeer::UNIT_ID);
			$selectCriteria->add(ArahanBelanjaKegiatanPeer::UNIT_ID, $criteria->remove(ArahanBelanjaKegiatanPeer::UNIT_ID), $comparison);

			$comparison = $criteria->getComparison(ArahanBelanjaKegiatanPeer::KEGIATAN_CODE);
			$selectCriteria->add(ArahanBelanjaKegiatanPeer::KEGIATAN_CODE, $criteria->remove(ArahanBelanjaKegiatanPeer::KEGIATAN_CODE), $comparison);

			$comparison = $criteria->getComparison(ArahanBelanjaKegiatanPeer::KODE_BELANJA);
			$selectCriteria->add(ArahanBelanjaKegiatanPeer::KODE_BELANJA, $criteria->remove(ArahanBelanjaKegiatanPeer::KODE_BELANJA), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(ArahanBelanjaKegiatanPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(ArahanBelanjaKegiatanPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof ArahanBelanjaKegiatan) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
												if(count($values) == count($values, COUNT_RECURSIVE))
			{
								$values = array($values);
			}
			$vals = array();
			foreach($values as $value)
			{

				$vals[0][] = $value[0];
				$vals[1][] = $value[1];
				$vals[2][] = $value[2];
				$vals[3][] = $value[3];
			}

			$criteria->add(ArahanBelanjaKegiatanPeer::ID, $vals[0], Criteria::IN);
			$criteria->add(ArahanBelanjaKegiatanPeer::UNIT_ID, $vals[1], Criteria::IN);
			$criteria->add(ArahanBelanjaKegiatanPeer::KEGIATAN_CODE, $vals[2], Criteria::IN);
			$criteria->add(ArahanBelanjaKegiatanPeer::KODE_BELANJA, $vals[3], Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(ArahanBelanjaKegiatan $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(ArahanBelanjaKegiatanPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(ArahanBelanjaKegiatanPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(ArahanBelanjaKegiatanPeer::DATABASE_NAME, ArahanBelanjaKegiatanPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = ArahanBelanjaKegiatanPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK( $id, $unit_id, $kegiatan_code, $kode_belanja, $con = null) {
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$criteria = new Criteria();
		$criteria->add(ArahanBelanjaKegiatanPeer::ID, $id);
		$criteria->add(ArahanBelanjaKegiatanPeer::UNIT_ID, $unit_id);
		$criteria->add(ArahanBelanjaKegiatanPeer::KEGIATAN_CODE, $kegiatan_code);
		$criteria->add(ArahanBelanjaKegiatanPeer::KODE_BELANJA, $kode_belanja);
		$v = ArahanBelanjaKegiatanPeer::doSelect($criteria, $con);

		return !empty($v) ? $v[0] : null;
	}
} 
if (Propel::isInit()) {
			try {
		BaseArahanBelanjaKegiatanPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/ArahanBelanjaKegiatanMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.ArahanBelanjaKegiatanMapBuilder');
}
