<?php


abstract class BaseDpaImportbedaPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.dpa_importbeda';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.DpaImportbeda';

	
	const NUM_COLUMNS = 16;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const UNIT_ID = 'ebudget.dpa_importbeda.UNIT_ID';

	
	const KEGIATAN_CODE = 'ebudget.dpa_importbeda.KEGIATAN_CODE';

	
	const DETAIL_NO = 'ebudget.dpa_importbeda.DETAIL_NO';

	
	const NAMA_KEGIATAN = 'ebudget.dpa_importbeda.NAMA_KEGIATAN';

	
	const SUBTITLE = 'ebudget.dpa_importbeda.SUBTITLE';

	
	const REKENING_CODE = 'ebudget.dpa_importbeda.REKENING_CODE';

	
	const REKENING_NAME = 'ebudget.dpa_importbeda.REKENING_NAME';

	
	const KOMPONEN_ID = 'ebudget.dpa_importbeda.KOMPONEN_ID';

	
	const KOMPONEN_NAME = 'ebudget.dpa_importbeda.KOMPONEN_NAME';

	
	const DETAIL_NAME = 'ebudget.dpa_importbeda.DETAIL_NAME';

	
	const KOMPONEN_HARGA_AWAL = 'ebudget.dpa_importbeda.KOMPONEN_HARGA_AWAL';

	
	const KETERANGAN_KOEFISIEN = 'ebudget.dpa_importbeda.KETERANGAN_KOEFISIEN';

	
	const VOLUME = 'ebudget.dpa_importbeda.VOLUME';

	
	const TOTAL = 'ebudget.dpa_importbeda.TOTAL';

	
	const DT_IMPORT = 'ebudget.dpa_importbeda.DT_IMPORT';

	
	const SATUAN = 'ebudget.dpa_importbeda.SATUAN';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('UnitId', 'KegiatanCode', 'DetailNo', 'NamaKegiatan', 'Subtitle', 'RekeningCode', 'RekeningName', 'KomponenId', 'KomponenName', 'DetailName', 'KomponenHargaAwal', 'KeteranganKoefisien', 'Volume', 'Total', 'DtImport', 'Satuan', ),
		BasePeer::TYPE_COLNAME => array (DpaImportbedaPeer::UNIT_ID, DpaImportbedaPeer::KEGIATAN_CODE, DpaImportbedaPeer::DETAIL_NO, DpaImportbedaPeer::NAMA_KEGIATAN, DpaImportbedaPeer::SUBTITLE, DpaImportbedaPeer::REKENING_CODE, DpaImportbedaPeer::REKENING_NAME, DpaImportbedaPeer::KOMPONEN_ID, DpaImportbedaPeer::KOMPONEN_NAME, DpaImportbedaPeer::DETAIL_NAME, DpaImportbedaPeer::KOMPONEN_HARGA_AWAL, DpaImportbedaPeer::KETERANGAN_KOEFISIEN, DpaImportbedaPeer::VOLUME, DpaImportbedaPeer::TOTAL, DpaImportbedaPeer::DT_IMPORT, DpaImportbedaPeer::SATUAN, ),
		BasePeer::TYPE_FIELDNAME => array ('unit_id', 'kegiatan_code', 'detail_no', 'nama_kegiatan', 'subtitle', 'rekening_code', 'rekening_name', 'komponen_id', 'komponen_name', 'detail_name', 'komponen_harga_awal', 'keterangan_koefisien', 'volume', 'total', 'dt_import', 'satuan', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('UnitId' => 0, 'KegiatanCode' => 1, 'DetailNo' => 2, 'NamaKegiatan' => 3, 'Subtitle' => 4, 'RekeningCode' => 5, 'RekeningName' => 6, 'KomponenId' => 7, 'KomponenName' => 8, 'DetailName' => 9, 'KomponenHargaAwal' => 10, 'KeteranganKoefisien' => 11, 'Volume' => 12, 'Total' => 13, 'DtImport' => 14, 'Satuan' => 15, ),
		BasePeer::TYPE_COLNAME => array (DpaImportbedaPeer::UNIT_ID => 0, DpaImportbedaPeer::KEGIATAN_CODE => 1, DpaImportbedaPeer::DETAIL_NO => 2, DpaImportbedaPeer::NAMA_KEGIATAN => 3, DpaImportbedaPeer::SUBTITLE => 4, DpaImportbedaPeer::REKENING_CODE => 5, DpaImportbedaPeer::REKENING_NAME => 6, DpaImportbedaPeer::KOMPONEN_ID => 7, DpaImportbedaPeer::KOMPONEN_NAME => 8, DpaImportbedaPeer::DETAIL_NAME => 9, DpaImportbedaPeer::KOMPONEN_HARGA_AWAL => 10, DpaImportbedaPeer::KETERANGAN_KOEFISIEN => 11, DpaImportbedaPeer::VOLUME => 12, DpaImportbedaPeer::TOTAL => 13, DpaImportbedaPeer::DT_IMPORT => 14, DpaImportbedaPeer::SATUAN => 15, ),
		BasePeer::TYPE_FIELDNAME => array ('unit_id' => 0, 'kegiatan_code' => 1, 'detail_no' => 2, 'nama_kegiatan' => 3, 'subtitle' => 4, 'rekening_code' => 5, 'rekening_name' => 6, 'komponen_id' => 7, 'komponen_name' => 8, 'detail_name' => 9, 'komponen_harga_awal' => 10, 'keterangan_koefisien' => 11, 'volume' => 12, 'total' => 13, 'dt_import' => 14, 'satuan' => 15, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/DpaImportbedaMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.DpaImportbedaMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = DpaImportbedaPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(DpaImportbedaPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(DpaImportbedaPeer::UNIT_ID);

		$criteria->addSelectColumn(DpaImportbedaPeer::KEGIATAN_CODE);

		$criteria->addSelectColumn(DpaImportbedaPeer::DETAIL_NO);

		$criteria->addSelectColumn(DpaImportbedaPeer::NAMA_KEGIATAN);

		$criteria->addSelectColumn(DpaImportbedaPeer::SUBTITLE);

		$criteria->addSelectColumn(DpaImportbedaPeer::REKENING_CODE);

		$criteria->addSelectColumn(DpaImportbedaPeer::REKENING_NAME);

		$criteria->addSelectColumn(DpaImportbedaPeer::KOMPONEN_ID);

		$criteria->addSelectColumn(DpaImportbedaPeer::KOMPONEN_NAME);

		$criteria->addSelectColumn(DpaImportbedaPeer::DETAIL_NAME);

		$criteria->addSelectColumn(DpaImportbedaPeer::KOMPONEN_HARGA_AWAL);

		$criteria->addSelectColumn(DpaImportbedaPeer::KETERANGAN_KOEFISIEN);

		$criteria->addSelectColumn(DpaImportbedaPeer::VOLUME);

		$criteria->addSelectColumn(DpaImportbedaPeer::TOTAL);

		$criteria->addSelectColumn(DpaImportbedaPeer::DT_IMPORT);

		$criteria->addSelectColumn(DpaImportbedaPeer::SATUAN);

	}

	const COUNT = 'COUNT(ebudget.dpa_importbeda.UNIT_ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.dpa_importbeda.UNIT_ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(DpaImportbedaPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(DpaImportbedaPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = DpaImportbedaPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = DpaImportbedaPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return DpaImportbedaPeer::populateObjects(DpaImportbedaPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			DpaImportbedaPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = DpaImportbedaPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return DpaImportbedaPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(DpaImportbedaPeer::UNIT_ID);
			$selectCriteria->add(DpaImportbedaPeer::UNIT_ID, $criteria->remove(DpaImportbedaPeer::UNIT_ID), $comparison);

			$comparison = $criteria->getComparison(DpaImportbedaPeer::KEGIATAN_CODE);
			$selectCriteria->add(DpaImportbedaPeer::KEGIATAN_CODE, $criteria->remove(DpaImportbedaPeer::KEGIATAN_CODE), $comparison);

			$comparison = $criteria->getComparison(DpaImportbedaPeer::DETAIL_NO);
			$selectCriteria->add(DpaImportbedaPeer::DETAIL_NO, $criteria->remove(DpaImportbedaPeer::DETAIL_NO), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(DpaImportbedaPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(DpaImportbedaPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof DpaImportbeda) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
												if(count($values) == count($values, COUNT_RECURSIVE))
			{
								$values = array($values);
			}
			$vals = array();
			foreach($values as $value)
			{

				$vals[0][] = $value[0];
				$vals[1][] = $value[1];
				$vals[2][] = $value[2];
			}

			$criteria->add(DpaImportbedaPeer::UNIT_ID, $vals[0], Criteria::IN);
			$criteria->add(DpaImportbedaPeer::KEGIATAN_CODE, $vals[1], Criteria::IN);
			$criteria->add(DpaImportbedaPeer::DETAIL_NO, $vals[2], Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(DpaImportbeda $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(DpaImportbedaPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(DpaImportbedaPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(DpaImportbedaPeer::DATABASE_NAME, DpaImportbedaPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = DpaImportbedaPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK( $unit_id, $kegiatan_code, $detail_no, $con = null) {
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$criteria = new Criteria();
		$criteria->add(DpaImportbedaPeer::UNIT_ID, $unit_id);
		$criteria->add(DpaImportbedaPeer::KEGIATAN_CODE, $kegiatan_code);
		$criteria->add(DpaImportbedaPeer::DETAIL_NO, $detail_no);
		$v = DpaImportbedaPeer::doSelect($criteria, $con);

		return !empty($v) ? $v[0] : null;
	}
} 
if (Propel::isInit()) {
			try {
		BaseDpaImportbedaPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/DpaImportbedaMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.DpaImportbedaMapBuilder');
}
