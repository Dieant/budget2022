<?php


abstract class BaseHistoryPekerjaan extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $tahun;


	
	protected $lokasi;


	
	protected $nilai;


	
	protected $kode;


	
	protected $nomor;


	
	protected $volume;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getTahun()
	{

		return $this->tahun;
	}

	
	public function getLokasi()
	{

		return $this->lokasi;
	}

	
	public function getNilai()
	{

		return $this->nilai;
	}

	
	public function getKode()
	{

		return $this->kode;
	}

	
	public function getNomor()
	{

		return $this->nomor;
	}

	
	public function getVolume()
	{

		return $this->volume;
	}

	
	public function setTahun($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->tahun !== $v) {
			$this->tahun = $v;
			$this->modifiedColumns[] = HistoryPekerjaanPeer::TAHUN;
		}

	} 
	
	public function setLokasi($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->lokasi !== $v) {
			$this->lokasi = $v;
			$this->modifiedColumns[] = HistoryPekerjaanPeer::LOKASI;
		}

	} 
	
	public function setNilai($v)
	{

		if ($this->nilai !== $v) {
			$this->nilai = $v;
			$this->modifiedColumns[] = HistoryPekerjaanPeer::NILAI;
		}

	} 
	
	public function setKode($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode !== $v) {
			$this->kode = $v;
			$this->modifiedColumns[] = HistoryPekerjaanPeer::KODE;
		}

	} 
	
	public function setNomor($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->nomor !== $v) {
			$this->nomor = $v;
			$this->modifiedColumns[] = HistoryPekerjaanPeer::NOMOR;
		}

	} 
	
	public function setVolume($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->volume !== $v) {
			$this->volume = $v;
			$this->modifiedColumns[] = HistoryPekerjaanPeer::VOLUME;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->tahun = $rs->getInt($startcol + 0);

			$this->lokasi = $rs->getString($startcol + 1);

			$this->nilai = $rs->getFloat($startcol + 2);

			$this->kode = $rs->getString($startcol + 3);

			$this->nomor = $rs->getInt($startcol + 4);

			$this->volume = $rs->getString($startcol + 5);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 6; 
		} catch (Exception $e) {
			throw new PropelException("Error populating HistoryPekerjaan object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(HistoryPekerjaanPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			HistoryPekerjaanPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(HistoryPekerjaanPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = HistoryPekerjaanPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += HistoryPekerjaanPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = HistoryPekerjaanPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = HistoryPekerjaanPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getTahun();
				break;
			case 1:
				return $this->getLokasi();
				break;
			case 2:
				return $this->getNilai();
				break;
			case 3:
				return $this->getKode();
				break;
			case 4:
				return $this->getNomor();
				break;
			case 5:
				return $this->getVolume();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = HistoryPekerjaanPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getTahun(),
			$keys[1] => $this->getLokasi(),
			$keys[2] => $this->getNilai(),
			$keys[3] => $this->getKode(),
			$keys[4] => $this->getNomor(),
			$keys[5] => $this->getVolume(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = HistoryPekerjaanPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setTahun($value);
				break;
			case 1:
				$this->setLokasi($value);
				break;
			case 2:
				$this->setNilai($value);
				break;
			case 3:
				$this->setKode($value);
				break;
			case 4:
				$this->setNomor($value);
				break;
			case 5:
				$this->setVolume($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = HistoryPekerjaanPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setTahun($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setLokasi($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setNilai($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setKode($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setNomor($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setVolume($arr[$keys[5]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(HistoryPekerjaanPeer::DATABASE_NAME);

		if ($this->isColumnModified(HistoryPekerjaanPeer::TAHUN)) $criteria->add(HistoryPekerjaanPeer::TAHUN, $this->tahun);
		if ($this->isColumnModified(HistoryPekerjaanPeer::LOKASI)) $criteria->add(HistoryPekerjaanPeer::LOKASI, $this->lokasi);
		if ($this->isColumnModified(HistoryPekerjaanPeer::NILAI)) $criteria->add(HistoryPekerjaanPeer::NILAI, $this->nilai);
		if ($this->isColumnModified(HistoryPekerjaanPeer::KODE)) $criteria->add(HistoryPekerjaanPeer::KODE, $this->kode);
		if ($this->isColumnModified(HistoryPekerjaanPeer::NOMOR)) $criteria->add(HistoryPekerjaanPeer::NOMOR, $this->nomor);
		if ($this->isColumnModified(HistoryPekerjaanPeer::VOLUME)) $criteria->add(HistoryPekerjaanPeer::VOLUME, $this->volume);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(HistoryPekerjaanPeer::DATABASE_NAME);


		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return null;
	}

	
	 public function setPrimaryKey($pk)
	 {
		 	 }

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setTahun($this->tahun);

		$copyObj->setLokasi($this->lokasi);

		$copyObj->setNilai($this->nilai);

		$copyObj->setKode($this->kode);

		$copyObj->setNomor($this->nomor);

		$copyObj->setVolume($this->volume);


		$copyObj->setNew(true);

	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new HistoryPekerjaanPeer();
		}
		return self::$peer;
	}

} 