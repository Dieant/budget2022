<?php


abstract class BasePrintRkaPakDetail extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id_print_rka_pak;


	
	protected $subtitle;


	
	protected $rekening_code;


	
	protected $subsubtitle;


	
	protected $nama_komponen;


	
	protected $satuan;


	
	protected $koefisien;


	
	protected $harga;


	
	protected $hasil;


	
	protected $pajak;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getIdPrintRkaPak()
	{

		return $this->id_print_rka_pak;
	}

	
	public function getSubtitle()
	{

		return $this->subtitle;
	}

	
	public function getRekeningCode()
	{

		return $this->rekening_code;
	}

	
	public function getSubsubtitle()
	{

		return $this->subsubtitle;
	}

	
	public function getNamaKomponen()
	{

		return $this->nama_komponen;
	}

	
	public function getSatuan()
	{

		return $this->satuan;
	}

	
	public function getKoefisien()
	{

		return $this->koefisien;
	}

	
	public function getHarga()
	{

		return $this->harga;
	}

	
	public function getHasil()
	{

		return $this->hasil;
	}

	
	public function getPajak()
	{

		return $this->pajak;
	}

	
	public function setIdPrintRkaPak($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id_print_rka_pak !== $v) {
			$this->id_print_rka_pak = $v;
			$this->modifiedColumns[] = PrintRkaPakDetailPeer::ID_PRINT_RKA_PAK;
		}

	} 
	
	public function setSubtitle($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->subtitle !== $v) {
			$this->subtitle = $v;
			$this->modifiedColumns[] = PrintRkaPakDetailPeer::SUBTITLE;
		}

	} 
	
	public function setRekeningCode($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->rekening_code !== $v) {
			$this->rekening_code = $v;
			$this->modifiedColumns[] = PrintRkaPakDetailPeer::REKENING_CODE;
		}

	} 
	
	public function setSubsubtitle($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->subsubtitle !== $v) {
			$this->subsubtitle = $v;
			$this->modifiedColumns[] = PrintRkaPakDetailPeer::SUBSUBTITLE;
		}

	} 
	
	public function setNamaKomponen($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nama_komponen !== $v) {
			$this->nama_komponen = $v;
			$this->modifiedColumns[] = PrintRkaPakDetailPeer::NAMA_KOMPONEN;
		}

	} 
	
	public function setSatuan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->satuan !== $v) {
			$this->satuan = $v;
			$this->modifiedColumns[] = PrintRkaPakDetailPeer::SATUAN;
		}

	} 
	
	public function setKoefisien($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->koefisien !== $v) {
			$this->koefisien = $v;
			$this->modifiedColumns[] = PrintRkaPakDetailPeer::KOEFISIEN;
		}

	} 
	
	public function setHarga($v)
	{

		if ($this->harga !== $v) {
			$this->harga = $v;
			$this->modifiedColumns[] = PrintRkaPakDetailPeer::HARGA;
		}

	} 
	
	public function setHasil($v)
	{

		if ($this->hasil !== $v) {
			$this->hasil = $v;
			$this->modifiedColumns[] = PrintRkaPakDetailPeer::HASIL;
		}

	} 
	
	public function setPajak($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->pajak !== $v) {
			$this->pajak = $v;
			$this->modifiedColumns[] = PrintRkaPakDetailPeer::PAJAK;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id_print_rka_pak = $rs->getInt($startcol + 0);

			$this->subtitle = $rs->getString($startcol + 1);

			$this->rekening_code = $rs->getString($startcol + 2);

			$this->subsubtitle = $rs->getString($startcol + 3);

			$this->nama_komponen = $rs->getString($startcol + 4);

			$this->satuan = $rs->getString($startcol + 5);

			$this->koefisien = $rs->getString($startcol + 6);

			$this->harga = $rs->getFloat($startcol + 7);

			$this->hasil = $rs->getFloat($startcol + 8);

			$this->pajak = $rs->getInt($startcol + 9);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 10; 
		} catch (Exception $e) {
			throw new PropelException("Error populating PrintRkaPakDetail object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(PrintRkaPakDetailPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			PrintRkaPakDetailPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(PrintRkaPakDetailPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = PrintRkaPakDetailPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += PrintRkaPakDetailPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = PrintRkaPakDetailPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = PrintRkaPakDetailPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getIdPrintRkaPak();
				break;
			case 1:
				return $this->getSubtitle();
				break;
			case 2:
				return $this->getRekeningCode();
				break;
			case 3:
				return $this->getSubsubtitle();
				break;
			case 4:
				return $this->getNamaKomponen();
				break;
			case 5:
				return $this->getSatuan();
				break;
			case 6:
				return $this->getKoefisien();
				break;
			case 7:
				return $this->getHarga();
				break;
			case 8:
				return $this->getHasil();
				break;
			case 9:
				return $this->getPajak();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = PrintRkaPakDetailPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getIdPrintRkaPak(),
			$keys[1] => $this->getSubtitle(),
			$keys[2] => $this->getRekeningCode(),
			$keys[3] => $this->getSubsubtitle(),
			$keys[4] => $this->getNamaKomponen(),
			$keys[5] => $this->getSatuan(),
			$keys[6] => $this->getKoefisien(),
			$keys[7] => $this->getHarga(),
			$keys[8] => $this->getHasil(),
			$keys[9] => $this->getPajak(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = PrintRkaPakDetailPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setIdPrintRkaPak($value);
				break;
			case 1:
				$this->setSubtitle($value);
				break;
			case 2:
				$this->setRekeningCode($value);
				break;
			case 3:
				$this->setSubsubtitle($value);
				break;
			case 4:
				$this->setNamaKomponen($value);
				break;
			case 5:
				$this->setSatuan($value);
				break;
			case 6:
				$this->setKoefisien($value);
				break;
			case 7:
				$this->setHarga($value);
				break;
			case 8:
				$this->setHasil($value);
				break;
			case 9:
				$this->setPajak($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = PrintRkaPakDetailPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setIdPrintRkaPak($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setSubtitle($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setRekeningCode($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setSubsubtitle($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setNamaKomponen($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setSatuan($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setKoefisien($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setHarga($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setHasil($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setPajak($arr[$keys[9]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(PrintRkaPakDetailPeer::DATABASE_NAME);

		if ($this->isColumnModified(PrintRkaPakDetailPeer::ID_PRINT_RKA_PAK)) $criteria->add(PrintRkaPakDetailPeer::ID_PRINT_RKA_PAK, $this->id_print_rka_pak);
		if ($this->isColumnModified(PrintRkaPakDetailPeer::SUBTITLE)) $criteria->add(PrintRkaPakDetailPeer::SUBTITLE, $this->subtitle);
		if ($this->isColumnModified(PrintRkaPakDetailPeer::REKENING_CODE)) $criteria->add(PrintRkaPakDetailPeer::REKENING_CODE, $this->rekening_code);
		if ($this->isColumnModified(PrintRkaPakDetailPeer::SUBSUBTITLE)) $criteria->add(PrintRkaPakDetailPeer::SUBSUBTITLE, $this->subsubtitle);
		if ($this->isColumnModified(PrintRkaPakDetailPeer::NAMA_KOMPONEN)) $criteria->add(PrintRkaPakDetailPeer::NAMA_KOMPONEN, $this->nama_komponen);
		if ($this->isColumnModified(PrintRkaPakDetailPeer::SATUAN)) $criteria->add(PrintRkaPakDetailPeer::SATUAN, $this->satuan);
		if ($this->isColumnModified(PrintRkaPakDetailPeer::KOEFISIEN)) $criteria->add(PrintRkaPakDetailPeer::KOEFISIEN, $this->koefisien);
		if ($this->isColumnModified(PrintRkaPakDetailPeer::HARGA)) $criteria->add(PrintRkaPakDetailPeer::HARGA, $this->harga);
		if ($this->isColumnModified(PrintRkaPakDetailPeer::HASIL)) $criteria->add(PrintRkaPakDetailPeer::HASIL, $this->hasil);
		if ($this->isColumnModified(PrintRkaPakDetailPeer::PAJAK)) $criteria->add(PrintRkaPakDetailPeer::PAJAK, $this->pajak);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(PrintRkaPakDetailPeer::DATABASE_NAME);

		$criteria->add(PrintRkaPakDetailPeer::ID_PRINT_RKA_PAK, $this->id_print_rka_pak);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getIdPrintRkaPak();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setIdPrintRkaPak($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setSubtitle($this->subtitle);

		$copyObj->setRekeningCode($this->rekening_code);

		$copyObj->setSubsubtitle($this->subsubtitle);

		$copyObj->setNamaKomponen($this->nama_komponen);

		$copyObj->setSatuan($this->satuan);

		$copyObj->setKoefisien($this->koefisien);

		$copyObj->setHarga($this->harga);

		$copyObj->setHasil($this->hasil);

		$copyObj->setPajak($this->pajak);


		$copyObj->setNew(true);

		$copyObj->setIdPrintRkaPak(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new PrintRkaPakDetailPeer();
		}
		return self::$peer;
	}

} 