<?php


abstract class BasePrintKoefisienDetail extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $unit_id;


	
	protected $kegiatan_code;


	
	protected $print_no;


	
	protected $detail_no;


	
	protected $revisi_no;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getUnitId()
	{

		return $this->unit_id;
	}

	
	public function getKegiatanCode()
	{

		return $this->kegiatan_code;
	}

	
	public function getPrintNo()
	{

		return $this->print_no;
	}

	
	public function getDetailNo()
	{

		return $this->detail_no;
	}

	
	public function getRevisiNo()
	{

		return $this->revisi_no;
	}

	
	public function setUnitId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->unit_id !== $v) {
			$this->unit_id = $v;
			$this->modifiedColumns[] = PrintKoefisienDetailPeer::UNIT_ID;
		}

	} 
	
	public function setKegiatanCode($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kegiatan_code !== $v) {
			$this->kegiatan_code = $v;
			$this->modifiedColumns[] = PrintKoefisienDetailPeer::KEGIATAN_CODE;
		}

	} 
	
	public function setPrintNo($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->print_no !== $v) {
			$this->print_no = $v;
			$this->modifiedColumns[] = PrintKoefisienDetailPeer::PRINT_NO;
		}

	} 
	
	public function setDetailNo($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->detail_no !== $v) {
			$this->detail_no = $v;
			$this->modifiedColumns[] = PrintKoefisienDetailPeer::DETAIL_NO;
		}

	} 
	
	public function setRevisiNo($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->revisi_no !== $v) {
			$this->revisi_no = $v;
			$this->modifiedColumns[] = PrintKoefisienDetailPeer::REVISI_NO;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->unit_id = $rs->getString($startcol + 0);

			$this->kegiatan_code = $rs->getString($startcol + 1);

			$this->print_no = $rs->getInt($startcol + 2);

			$this->detail_no = $rs->getString($startcol + 3);

			$this->revisi_no = $rs->getInt($startcol + 4);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 5; 
		} catch (Exception $e) {
			throw new PropelException("Error populating PrintKoefisienDetail object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(PrintKoefisienDetailPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			PrintKoefisienDetailPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(PrintKoefisienDetailPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = PrintKoefisienDetailPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += PrintKoefisienDetailPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = PrintKoefisienDetailPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = PrintKoefisienDetailPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getUnitId();
				break;
			case 1:
				return $this->getKegiatanCode();
				break;
			case 2:
				return $this->getPrintNo();
				break;
			case 3:
				return $this->getDetailNo();
				break;
			case 4:
				return $this->getRevisiNo();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = PrintKoefisienDetailPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getUnitId(),
			$keys[1] => $this->getKegiatanCode(),
			$keys[2] => $this->getPrintNo(),
			$keys[3] => $this->getDetailNo(),
			$keys[4] => $this->getRevisiNo(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = PrintKoefisienDetailPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setUnitId($value);
				break;
			case 1:
				$this->setKegiatanCode($value);
				break;
			case 2:
				$this->setPrintNo($value);
				break;
			case 3:
				$this->setDetailNo($value);
				break;
			case 4:
				$this->setRevisiNo($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = PrintKoefisienDetailPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setUnitId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setKegiatanCode($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setPrintNo($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setDetailNo($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setRevisiNo($arr[$keys[4]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(PrintKoefisienDetailPeer::DATABASE_NAME);

		if ($this->isColumnModified(PrintKoefisienDetailPeer::UNIT_ID)) $criteria->add(PrintKoefisienDetailPeer::UNIT_ID, $this->unit_id);
		if ($this->isColumnModified(PrintKoefisienDetailPeer::KEGIATAN_CODE)) $criteria->add(PrintKoefisienDetailPeer::KEGIATAN_CODE, $this->kegiatan_code);
		if ($this->isColumnModified(PrintKoefisienDetailPeer::PRINT_NO)) $criteria->add(PrintKoefisienDetailPeer::PRINT_NO, $this->print_no);
		if ($this->isColumnModified(PrintKoefisienDetailPeer::DETAIL_NO)) $criteria->add(PrintKoefisienDetailPeer::DETAIL_NO, $this->detail_no);
		if ($this->isColumnModified(PrintKoefisienDetailPeer::REVISI_NO)) $criteria->add(PrintKoefisienDetailPeer::REVISI_NO, $this->revisi_no);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(PrintKoefisienDetailPeer::DATABASE_NAME);

		$criteria->add(PrintKoefisienDetailPeer::UNIT_ID, $this->unit_id);
		$criteria->add(PrintKoefisienDetailPeer::KEGIATAN_CODE, $this->kegiatan_code);
		$criteria->add(PrintKoefisienDetailPeer::PRINT_NO, $this->print_no);
		$criteria->add(PrintKoefisienDetailPeer::DETAIL_NO, $this->detail_no);
		$criteria->add(PrintKoefisienDetailPeer::REVISI_NO, $this->revisi_no);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		$pks = array();

		$pks[0] = $this->getUnitId();

		$pks[1] = $this->getKegiatanCode();

		$pks[2] = $this->getPrintNo();

		$pks[3] = $this->getDetailNo();

		$pks[4] = $this->getRevisiNo();

		return $pks;
	}

	
	public function setPrimaryKey($keys)
	{

		$this->setUnitId($keys[0]);

		$this->setKegiatanCode($keys[1]);

		$this->setPrintNo($keys[2]);

		$this->setDetailNo($keys[3]);

		$this->setRevisiNo($keys[4]);

	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{


		$copyObj->setNew(true);

		$copyObj->setUnitId(NULL); 
		$copyObj->setKegiatanCode(NULL); 
		$copyObj->setPrintNo(NULL); 
		$copyObj->setDetailNo(NULL); 
		$copyObj->setRevisiNo(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new PrintKoefisienDetailPeer();
		}
		return self::$peer;
	}

} 