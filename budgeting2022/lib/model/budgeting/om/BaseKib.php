<?php


abstract class BaseKib extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id;


	
	protected $tipe_kib;


	
	protected $unit_id;


	
	protected $kode_lokasi;


	
	protected $nama_lokasi;


	
	protected $no_register;


	
	protected $kode_barang;


	
	protected $nama_barang;


	
	protected $kondisi;


	
	protected $merk;


	
	protected $tipe;


	
	protected $alamat;


	
	protected $keterangan;


	
	protected $tahun;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getId()
	{

		return $this->id;
	}

	
	public function getTipeKib()
	{

		return $this->tipe_kib;
	}

	
	public function getUnitId()
	{

		return $this->unit_id;
	}

	
	public function getKodeLokasi()
	{

		return $this->kode_lokasi;
	}

	
	public function getNamaLokasi()
	{

		return $this->nama_lokasi;
	}

	
	public function getNoRegister()
	{

		return $this->no_register;
	}

	
	public function getKodeBarang()
	{

		return $this->kode_barang;
	}

	
	public function getNamaBarang()
	{

		return $this->nama_barang;
	}

	
	public function getKondisi()
	{

		return $this->kondisi;
	}

	
	public function getMerk()
	{

		return $this->merk;
	}

	
	public function getTipe()
	{

		return $this->tipe;
	}

	
	public function getAlamat()
	{

		return $this->alamat;
	}

	
	public function getKeterangan()
	{

		return $this->keterangan;
	}

	
	public function getTahun()
	{

		return $this->tahun;
	}

	
	public function setId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = KibPeer::ID;
		}

	} 
	
	public function setTipeKib($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->tipe_kib !== $v) {
			$this->tipe_kib = $v;
			$this->modifiedColumns[] = KibPeer::TIPE_KIB;
		}

	} 
	
	public function setUnitId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->unit_id !== $v) {
			$this->unit_id = $v;
			$this->modifiedColumns[] = KibPeer::UNIT_ID;
		}

	} 
	
	public function setKodeLokasi($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_lokasi !== $v) {
			$this->kode_lokasi = $v;
			$this->modifiedColumns[] = KibPeer::KODE_LOKASI;
		}

	} 
	
	public function setNamaLokasi($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nama_lokasi !== $v) {
			$this->nama_lokasi = $v;
			$this->modifiedColumns[] = KibPeer::NAMA_LOKASI;
		}

	} 
	
	public function setNoRegister($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->no_register !== $v) {
			$this->no_register = $v;
			$this->modifiedColumns[] = KibPeer::NO_REGISTER;
		}

	} 
	
	public function setKodeBarang($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_barang !== $v) {
			$this->kode_barang = $v;
			$this->modifiedColumns[] = KibPeer::KODE_BARANG;
		}

	} 
	
	public function setNamaBarang($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nama_barang !== $v) {
			$this->nama_barang = $v;
			$this->modifiedColumns[] = KibPeer::NAMA_BARANG;
		}

	} 
	
	public function setKondisi($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kondisi !== $v) {
			$this->kondisi = $v;
			$this->modifiedColumns[] = KibPeer::KONDISI;
		}

	} 
	
	public function setMerk($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->merk !== $v) {
			$this->merk = $v;
			$this->modifiedColumns[] = KibPeer::MERK;
		}

	} 
	
	public function setTipe($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->tipe !== $v) {
			$this->tipe = $v;
			$this->modifiedColumns[] = KibPeer::TIPE;
		}

	} 
	
	public function setAlamat($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->alamat !== $v) {
			$this->alamat = $v;
			$this->modifiedColumns[] = KibPeer::ALAMAT;
		}

	} 
	
	public function setKeterangan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->keterangan !== $v) {
			$this->keterangan = $v;
			$this->modifiedColumns[] = KibPeer::KETERANGAN;
		}

	} 
	
	public function setTahun($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->tahun !== $v) {
			$this->tahun = $v;
			$this->modifiedColumns[] = KibPeer::TAHUN;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getString($startcol + 0);

			$this->tipe_kib = $rs->getString($startcol + 1);

			$this->unit_id = $rs->getString($startcol + 2);

			$this->kode_lokasi = $rs->getString($startcol + 3);

			$this->nama_lokasi = $rs->getString($startcol + 4);

			$this->no_register = $rs->getString($startcol + 5);

			$this->kode_barang = $rs->getString($startcol + 6);

			$this->nama_barang = $rs->getString($startcol + 7);

			$this->kondisi = $rs->getString($startcol + 8);

			$this->merk = $rs->getString($startcol + 9);

			$this->tipe = $rs->getString($startcol + 10);

			$this->alamat = $rs->getString($startcol + 11);

			$this->keterangan = $rs->getString($startcol + 12);

			$this->tahun = $rs->getString($startcol + 13);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 14; 
		} catch (Exception $e) {
			throw new PropelException("Error populating Kib object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(KibPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			KibPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(KibPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = KibPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += KibPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = KibPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = KibPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getTipeKib();
				break;
			case 2:
				return $this->getUnitId();
				break;
			case 3:
				return $this->getKodeLokasi();
				break;
			case 4:
				return $this->getNamaLokasi();
				break;
			case 5:
				return $this->getNoRegister();
				break;
			case 6:
				return $this->getKodeBarang();
				break;
			case 7:
				return $this->getNamaBarang();
				break;
			case 8:
				return $this->getKondisi();
				break;
			case 9:
				return $this->getMerk();
				break;
			case 10:
				return $this->getTipe();
				break;
			case 11:
				return $this->getAlamat();
				break;
			case 12:
				return $this->getKeterangan();
				break;
			case 13:
				return $this->getTahun();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = KibPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getTipeKib(),
			$keys[2] => $this->getUnitId(),
			$keys[3] => $this->getKodeLokasi(),
			$keys[4] => $this->getNamaLokasi(),
			$keys[5] => $this->getNoRegister(),
			$keys[6] => $this->getKodeBarang(),
			$keys[7] => $this->getNamaBarang(),
			$keys[8] => $this->getKondisi(),
			$keys[9] => $this->getMerk(),
			$keys[10] => $this->getTipe(),
			$keys[11] => $this->getAlamat(),
			$keys[12] => $this->getKeterangan(),
			$keys[13] => $this->getTahun(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = KibPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setTipeKib($value);
				break;
			case 2:
				$this->setUnitId($value);
				break;
			case 3:
				$this->setKodeLokasi($value);
				break;
			case 4:
				$this->setNamaLokasi($value);
				break;
			case 5:
				$this->setNoRegister($value);
				break;
			case 6:
				$this->setKodeBarang($value);
				break;
			case 7:
				$this->setNamaBarang($value);
				break;
			case 8:
				$this->setKondisi($value);
				break;
			case 9:
				$this->setMerk($value);
				break;
			case 10:
				$this->setTipe($value);
				break;
			case 11:
				$this->setAlamat($value);
				break;
			case 12:
				$this->setKeterangan($value);
				break;
			case 13:
				$this->setTahun($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = KibPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setTipeKib($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setUnitId($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setKodeLokasi($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setNamaLokasi($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setNoRegister($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setKodeBarang($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setNamaBarang($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setKondisi($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setMerk($arr[$keys[9]]);
		if (array_key_exists($keys[10], $arr)) $this->setTipe($arr[$keys[10]]);
		if (array_key_exists($keys[11], $arr)) $this->setAlamat($arr[$keys[11]]);
		if (array_key_exists($keys[12], $arr)) $this->setKeterangan($arr[$keys[12]]);
		if (array_key_exists($keys[13], $arr)) $this->setTahun($arr[$keys[13]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(KibPeer::DATABASE_NAME);

		if ($this->isColumnModified(KibPeer::ID)) $criteria->add(KibPeer::ID, $this->id);
		if ($this->isColumnModified(KibPeer::TIPE_KIB)) $criteria->add(KibPeer::TIPE_KIB, $this->tipe_kib);
		if ($this->isColumnModified(KibPeer::UNIT_ID)) $criteria->add(KibPeer::UNIT_ID, $this->unit_id);
		if ($this->isColumnModified(KibPeer::KODE_LOKASI)) $criteria->add(KibPeer::KODE_LOKASI, $this->kode_lokasi);
		if ($this->isColumnModified(KibPeer::NAMA_LOKASI)) $criteria->add(KibPeer::NAMA_LOKASI, $this->nama_lokasi);
		if ($this->isColumnModified(KibPeer::NO_REGISTER)) $criteria->add(KibPeer::NO_REGISTER, $this->no_register);
		if ($this->isColumnModified(KibPeer::KODE_BARANG)) $criteria->add(KibPeer::KODE_BARANG, $this->kode_barang);
		if ($this->isColumnModified(KibPeer::NAMA_BARANG)) $criteria->add(KibPeer::NAMA_BARANG, $this->nama_barang);
		if ($this->isColumnModified(KibPeer::KONDISI)) $criteria->add(KibPeer::KONDISI, $this->kondisi);
		if ($this->isColumnModified(KibPeer::MERK)) $criteria->add(KibPeer::MERK, $this->merk);
		if ($this->isColumnModified(KibPeer::TIPE)) $criteria->add(KibPeer::TIPE, $this->tipe);
		if ($this->isColumnModified(KibPeer::ALAMAT)) $criteria->add(KibPeer::ALAMAT, $this->alamat);
		if ($this->isColumnModified(KibPeer::KETERANGAN)) $criteria->add(KibPeer::KETERANGAN, $this->keterangan);
		if ($this->isColumnModified(KibPeer::TAHUN)) $criteria->add(KibPeer::TAHUN, $this->tahun);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(KibPeer::DATABASE_NAME);

		$criteria->add(KibPeer::ID, $this->id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setTipeKib($this->tipe_kib);

		$copyObj->setUnitId($this->unit_id);

		$copyObj->setKodeLokasi($this->kode_lokasi);

		$copyObj->setNamaLokasi($this->nama_lokasi);

		$copyObj->setNoRegister($this->no_register);

		$copyObj->setKodeBarang($this->kode_barang);

		$copyObj->setNamaBarang($this->nama_barang);

		$copyObj->setKondisi($this->kondisi);

		$copyObj->setMerk($this->merk);

		$copyObj->setTipe($this->tipe);

		$copyObj->setAlamat($this->alamat);

		$copyObj->setKeterangan($this->keterangan);

		$copyObj->setTahun($this->tahun);


		$copyObj->setNew(true);

		$copyObj->setId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new KibPeer();
		}
		return self::$peer;
	}

} 