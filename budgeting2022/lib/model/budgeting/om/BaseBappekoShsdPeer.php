<?php


abstract class BaseBappekoShsdPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.bappeko_shsd';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.BappekoShsd';

	
	const NUM_COLUMNS = 15;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const SHSD_ID = 'ebudget.bappeko_shsd.SHSD_ID';

	
	const SATUAN = 'ebudget.bappeko_shsd.SATUAN';

	
	const SHSD_NAME = 'ebudget.bappeko_shsd.SHSD_NAME';

	
	const SHSD_HARGA = 'ebudget.bappeko_shsd.SHSD_HARGA';

	
	const SHSD_HARGA_DASAR = 'ebudget.bappeko_shsd.SHSD_HARGA_DASAR';

	
	const SHSD_SHOW = 'ebudget.bappeko_shsd.SHSD_SHOW';

	
	const SHSD_FAKTOR_INFLASI = 'ebudget.bappeko_shsd.SHSD_FAKTOR_INFLASI';

	
	const SHSD_LOCKED = 'ebudget.bappeko_shsd.SHSD_LOCKED';

	
	const REKENING_CODE = 'ebudget.bappeko_shsd.REKENING_CODE';

	
	const SHSD_MERK = 'ebudget.bappeko_shsd.SHSD_MERK';

	
	const NON_PAJAK = 'ebudget.bappeko_shsd.NON_PAJAK';

	
	const SHSD_ID_OLD = 'ebudget.bappeko_shsd.SHSD_ID_OLD';

	
	const NAMA_DASAR = 'ebudget.bappeko_shsd.NAMA_DASAR';

	
	const SPEC = 'ebudget.bappeko_shsd.SPEC';

	
	const HIDDEN_SPEC = 'ebudget.bappeko_shsd.HIDDEN_SPEC';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('ShsdId', 'Satuan', 'ShsdName', 'ShsdHarga', 'ShsdHargaDasar', 'ShsdShow', 'ShsdFaktorInflasi', 'ShsdLocked', 'RekeningCode', 'ShsdMerk', 'NonPajak', 'ShsdIdOld', 'NamaDasar', 'Spec', 'HiddenSpec', ),
		BasePeer::TYPE_COLNAME => array (BappekoShsdPeer::SHSD_ID, BappekoShsdPeer::SATUAN, BappekoShsdPeer::SHSD_NAME, BappekoShsdPeer::SHSD_HARGA, BappekoShsdPeer::SHSD_HARGA_DASAR, BappekoShsdPeer::SHSD_SHOW, BappekoShsdPeer::SHSD_FAKTOR_INFLASI, BappekoShsdPeer::SHSD_LOCKED, BappekoShsdPeer::REKENING_CODE, BappekoShsdPeer::SHSD_MERK, BappekoShsdPeer::NON_PAJAK, BappekoShsdPeer::SHSD_ID_OLD, BappekoShsdPeer::NAMA_DASAR, BappekoShsdPeer::SPEC, BappekoShsdPeer::HIDDEN_SPEC, ),
		BasePeer::TYPE_FIELDNAME => array ('shsd_id', 'satuan', 'shsd_name', 'shsd_harga', 'shsd_harga_dasar', 'shsd_show', 'shsd_faktor_inflasi', 'shsd_locked', 'rekening_code', 'shsd_merk', 'non_pajak', 'shsd_id_old', 'nama_dasar', 'spec', 'hidden_spec', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('ShsdId' => 0, 'Satuan' => 1, 'ShsdName' => 2, 'ShsdHarga' => 3, 'ShsdHargaDasar' => 4, 'ShsdShow' => 5, 'ShsdFaktorInflasi' => 6, 'ShsdLocked' => 7, 'RekeningCode' => 8, 'ShsdMerk' => 9, 'NonPajak' => 10, 'ShsdIdOld' => 11, 'NamaDasar' => 12, 'Spec' => 13, 'HiddenSpec' => 14, ),
		BasePeer::TYPE_COLNAME => array (BappekoShsdPeer::SHSD_ID => 0, BappekoShsdPeer::SATUAN => 1, BappekoShsdPeer::SHSD_NAME => 2, BappekoShsdPeer::SHSD_HARGA => 3, BappekoShsdPeer::SHSD_HARGA_DASAR => 4, BappekoShsdPeer::SHSD_SHOW => 5, BappekoShsdPeer::SHSD_FAKTOR_INFLASI => 6, BappekoShsdPeer::SHSD_LOCKED => 7, BappekoShsdPeer::REKENING_CODE => 8, BappekoShsdPeer::SHSD_MERK => 9, BappekoShsdPeer::NON_PAJAK => 10, BappekoShsdPeer::SHSD_ID_OLD => 11, BappekoShsdPeer::NAMA_DASAR => 12, BappekoShsdPeer::SPEC => 13, BappekoShsdPeer::HIDDEN_SPEC => 14, ),
		BasePeer::TYPE_FIELDNAME => array ('shsd_id' => 0, 'satuan' => 1, 'shsd_name' => 2, 'shsd_harga' => 3, 'shsd_harga_dasar' => 4, 'shsd_show' => 5, 'shsd_faktor_inflasi' => 6, 'shsd_locked' => 7, 'rekening_code' => 8, 'shsd_merk' => 9, 'non_pajak' => 10, 'shsd_id_old' => 11, 'nama_dasar' => 12, 'spec' => 13, 'hidden_spec' => 14, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/BappekoShsdMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.BappekoShsdMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = BappekoShsdPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(BappekoShsdPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(BappekoShsdPeer::SHSD_ID);

		$criteria->addSelectColumn(BappekoShsdPeer::SATUAN);

		$criteria->addSelectColumn(BappekoShsdPeer::SHSD_NAME);

		$criteria->addSelectColumn(BappekoShsdPeer::SHSD_HARGA);

		$criteria->addSelectColumn(BappekoShsdPeer::SHSD_HARGA_DASAR);

		$criteria->addSelectColumn(BappekoShsdPeer::SHSD_SHOW);

		$criteria->addSelectColumn(BappekoShsdPeer::SHSD_FAKTOR_INFLASI);

		$criteria->addSelectColumn(BappekoShsdPeer::SHSD_LOCKED);

		$criteria->addSelectColumn(BappekoShsdPeer::REKENING_CODE);

		$criteria->addSelectColumn(BappekoShsdPeer::SHSD_MERK);

		$criteria->addSelectColumn(BappekoShsdPeer::NON_PAJAK);

		$criteria->addSelectColumn(BappekoShsdPeer::SHSD_ID_OLD);

		$criteria->addSelectColumn(BappekoShsdPeer::NAMA_DASAR);

		$criteria->addSelectColumn(BappekoShsdPeer::SPEC);

		$criteria->addSelectColumn(BappekoShsdPeer::HIDDEN_SPEC);

	}

	const COUNT = 'COUNT(ebudget.bappeko_shsd.SHSD_ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.bappeko_shsd.SHSD_ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(BappekoShsdPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(BappekoShsdPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = BappekoShsdPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = BappekoShsdPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return BappekoShsdPeer::populateObjects(BappekoShsdPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			BappekoShsdPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = BappekoShsdPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return BappekoShsdPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(BappekoShsdPeer::SHSD_ID);
			$selectCriteria->add(BappekoShsdPeer::SHSD_ID, $criteria->remove(BappekoShsdPeer::SHSD_ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(BappekoShsdPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(BappekoShsdPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof BappekoShsd) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(BappekoShsdPeer::SHSD_ID, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(BappekoShsd $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(BappekoShsdPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(BappekoShsdPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(BappekoShsdPeer::DATABASE_NAME, BappekoShsdPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = BappekoShsdPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(BappekoShsdPeer::DATABASE_NAME);

		$criteria->add(BappekoShsdPeer::SHSD_ID, $pk);


		$v = BappekoShsdPeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(BappekoShsdPeer::SHSD_ID, $pks, Criteria::IN);
			$objs = BappekoShsdPeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BaseBappekoShsdPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/BappekoShsdMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.BappekoShsdMapBuilder');
}
