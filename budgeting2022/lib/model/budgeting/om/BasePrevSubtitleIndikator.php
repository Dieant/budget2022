<?php


abstract class BasePrevSubtitleIndikator extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $unit_id;


	
	protected $kegiatan_code;


	
	protected $subtitle;


	
	protected $indikator;


	
	protected $nilai;


	
	protected $satuan;


	
	protected $last_update_user;


	
	protected $last_update_time;


	
	protected $last_update_ip;


	
	protected $tahap;


	
	protected $sub_id;


	
	protected $tahun;


	
	protected $lock_subtitle = false;


	
	protected $prioritas = 0;


	
	protected $gender = false;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getUnitId()
	{

		return $this->unit_id;
	}

	
	public function getKegiatanCode()
	{

		return $this->kegiatan_code;
	}

	
	public function getSubtitle()
	{

		return $this->subtitle;
	}

	
	public function getIndikator()
	{

		return $this->indikator;
	}

	
	public function getNilai()
	{

		return $this->nilai;
	}

	
	public function getSatuan()
	{

		return $this->satuan;
	}

	
	public function getLastUpdateUser()
	{

		return $this->last_update_user;
	}

	
	public function getLastUpdateTime($format = 'Y-m-d H:i:s')
	{

		if ($this->last_update_time === null || $this->last_update_time === '') {
			return null;
		} elseif (!is_int($this->last_update_time)) {
						$ts = strtotime($this->last_update_time);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [last_update_time] as date/time value: " . var_export($this->last_update_time, true));
			}
		} else {
			$ts = $this->last_update_time;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getLastUpdateIp()
	{

		return $this->last_update_ip;
	}

	
	public function getTahap()
	{

		return $this->tahap;
	}

	
	public function getSubId()
	{

		return $this->sub_id;
	}

	
	public function getTahun()
	{

		return $this->tahun;
	}

	
	public function getLockSubtitle()
	{

		return $this->lock_subtitle;
	}

	
	public function getPrioritas()
	{

		return $this->prioritas;
	}

	
	public function getGender()
	{

		return $this->gender;
	}

	
	public function setUnitId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->unit_id !== $v) {
			$this->unit_id = $v;
			$this->modifiedColumns[] = PrevSubtitleIndikatorPeer::UNIT_ID;
		}

	} 
	
	public function setKegiatanCode($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kegiatan_code !== $v) {
			$this->kegiatan_code = $v;
			$this->modifiedColumns[] = PrevSubtitleIndikatorPeer::KEGIATAN_CODE;
		}

	} 
	
	public function setSubtitle($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->subtitle !== $v) {
			$this->subtitle = $v;
			$this->modifiedColumns[] = PrevSubtitleIndikatorPeer::SUBTITLE;
		}

	} 
	
	public function setIndikator($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->indikator !== $v) {
			$this->indikator = $v;
			$this->modifiedColumns[] = PrevSubtitleIndikatorPeer::INDIKATOR;
		}

	} 
	
	public function setNilai($v)
	{

		if ($this->nilai !== $v) {
			$this->nilai = $v;
			$this->modifiedColumns[] = PrevSubtitleIndikatorPeer::NILAI;
		}

	} 
	
	public function setSatuan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->satuan !== $v) {
			$this->satuan = $v;
			$this->modifiedColumns[] = PrevSubtitleIndikatorPeer::SATUAN;
		}

	} 
	
	public function setLastUpdateUser($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->last_update_user !== $v) {
			$this->last_update_user = $v;
			$this->modifiedColumns[] = PrevSubtitleIndikatorPeer::LAST_UPDATE_USER;
		}

	} 
	
	public function setLastUpdateTime($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [last_update_time] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->last_update_time !== $ts) {
			$this->last_update_time = $ts;
			$this->modifiedColumns[] = PrevSubtitleIndikatorPeer::LAST_UPDATE_TIME;
		}

	} 
	
	public function setLastUpdateIp($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->last_update_ip !== $v) {
			$this->last_update_ip = $v;
			$this->modifiedColumns[] = PrevSubtitleIndikatorPeer::LAST_UPDATE_IP;
		}

	} 
	
	public function setTahap($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->tahap !== $v) {
			$this->tahap = $v;
			$this->modifiedColumns[] = PrevSubtitleIndikatorPeer::TAHAP;
		}

	} 
	
	public function setSubId($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->sub_id !== $v) {
			$this->sub_id = $v;
			$this->modifiedColumns[] = PrevSubtitleIndikatorPeer::SUB_ID;
		}

	} 
	
	public function setTahun($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->tahun !== $v) {
			$this->tahun = $v;
			$this->modifiedColumns[] = PrevSubtitleIndikatorPeer::TAHUN;
		}

	} 
	
	public function setLockSubtitle($v)
	{

		if ($this->lock_subtitle !== $v || $v === false) {
			$this->lock_subtitle = $v;
			$this->modifiedColumns[] = PrevSubtitleIndikatorPeer::LOCK_SUBTITLE;
		}

	} 
	
	public function setPrioritas($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->prioritas !== $v || $v === 0) {
			$this->prioritas = $v;
			$this->modifiedColumns[] = PrevSubtitleIndikatorPeer::PRIORITAS;
		}

	} 
	
	public function setGender($v)
	{

		if ($this->gender !== $v || $v === false) {
			$this->gender = $v;
			$this->modifiedColumns[] = PrevSubtitleIndikatorPeer::GENDER;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->unit_id = $rs->getString($startcol + 0);

			$this->kegiatan_code = $rs->getString($startcol + 1);

			$this->subtitle = $rs->getString($startcol + 2);

			$this->indikator = $rs->getString($startcol + 3);

			$this->nilai = $rs->getFloat($startcol + 4);

			$this->satuan = $rs->getString($startcol + 5);

			$this->last_update_user = $rs->getString($startcol + 6);

			$this->last_update_time = $rs->getTimestamp($startcol + 7, null);

			$this->last_update_ip = $rs->getString($startcol + 8);

			$this->tahap = $rs->getString($startcol + 9);

			$this->sub_id = $rs->getInt($startcol + 10);

			$this->tahun = $rs->getString($startcol + 11);

			$this->lock_subtitle = $rs->getBoolean($startcol + 12);

			$this->prioritas = $rs->getInt($startcol + 13);

			$this->gender = $rs->getBoolean($startcol + 14);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 15; 
		} catch (Exception $e) {
			throw new PropelException("Error populating PrevSubtitleIndikator object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(PrevSubtitleIndikatorPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			PrevSubtitleIndikatorPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(PrevSubtitleIndikatorPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = PrevSubtitleIndikatorPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setSubId($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += PrevSubtitleIndikatorPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = PrevSubtitleIndikatorPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = PrevSubtitleIndikatorPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getUnitId();
				break;
			case 1:
				return $this->getKegiatanCode();
				break;
			case 2:
				return $this->getSubtitle();
				break;
			case 3:
				return $this->getIndikator();
				break;
			case 4:
				return $this->getNilai();
				break;
			case 5:
				return $this->getSatuan();
				break;
			case 6:
				return $this->getLastUpdateUser();
				break;
			case 7:
				return $this->getLastUpdateTime();
				break;
			case 8:
				return $this->getLastUpdateIp();
				break;
			case 9:
				return $this->getTahap();
				break;
			case 10:
				return $this->getSubId();
				break;
			case 11:
				return $this->getTahun();
				break;
			case 12:
				return $this->getLockSubtitle();
				break;
			case 13:
				return $this->getPrioritas();
				break;
			case 14:
				return $this->getGender();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = PrevSubtitleIndikatorPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getUnitId(),
			$keys[1] => $this->getKegiatanCode(),
			$keys[2] => $this->getSubtitle(),
			$keys[3] => $this->getIndikator(),
			$keys[4] => $this->getNilai(),
			$keys[5] => $this->getSatuan(),
			$keys[6] => $this->getLastUpdateUser(),
			$keys[7] => $this->getLastUpdateTime(),
			$keys[8] => $this->getLastUpdateIp(),
			$keys[9] => $this->getTahap(),
			$keys[10] => $this->getSubId(),
			$keys[11] => $this->getTahun(),
			$keys[12] => $this->getLockSubtitle(),
			$keys[13] => $this->getPrioritas(),
			$keys[14] => $this->getGender(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = PrevSubtitleIndikatorPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setUnitId($value);
				break;
			case 1:
				$this->setKegiatanCode($value);
				break;
			case 2:
				$this->setSubtitle($value);
				break;
			case 3:
				$this->setIndikator($value);
				break;
			case 4:
				$this->setNilai($value);
				break;
			case 5:
				$this->setSatuan($value);
				break;
			case 6:
				$this->setLastUpdateUser($value);
				break;
			case 7:
				$this->setLastUpdateTime($value);
				break;
			case 8:
				$this->setLastUpdateIp($value);
				break;
			case 9:
				$this->setTahap($value);
				break;
			case 10:
				$this->setSubId($value);
				break;
			case 11:
				$this->setTahun($value);
				break;
			case 12:
				$this->setLockSubtitle($value);
				break;
			case 13:
				$this->setPrioritas($value);
				break;
			case 14:
				$this->setGender($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = PrevSubtitleIndikatorPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setUnitId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setKegiatanCode($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setSubtitle($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setIndikator($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setNilai($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setSatuan($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setLastUpdateUser($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setLastUpdateTime($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setLastUpdateIp($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setTahap($arr[$keys[9]]);
		if (array_key_exists($keys[10], $arr)) $this->setSubId($arr[$keys[10]]);
		if (array_key_exists($keys[11], $arr)) $this->setTahun($arr[$keys[11]]);
		if (array_key_exists($keys[12], $arr)) $this->setLockSubtitle($arr[$keys[12]]);
		if (array_key_exists($keys[13], $arr)) $this->setPrioritas($arr[$keys[13]]);
		if (array_key_exists($keys[14], $arr)) $this->setGender($arr[$keys[14]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(PrevSubtitleIndikatorPeer::DATABASE_NAME);

		if ($this->isColumnModified(PrevSubtitleIndikatorPeer::UNIT_ID)) $criteria->add(PrevSubtitleIndikatorPeer::UNIT_ID, $this->unit_id);
		if ($this->isColumnModified(PrevSubtitleIndikatorPeer::KEGIATAN_CODE)) $criteria->add(PrevSubtitleIndikatorPeer::KEGIATAN_CODE, $this->kegiatan_code);
		if ($this->isColumnModified(PrevSubtitleIndikatorPeer::SUBTITLE)) $criteria->add(PrevSubtitleIndikatorPeer::SUBTITLE, $this->subtitle);
		if ($this->isColumnModified(PrevSubtitleIndikatorPeer::INDIKATOR)) $criteria->add(PrevSubtitleIndikatorPeer::INDIKATOR, $this->indikator);
		if ($this->isColumnModified(PrevSubtitleIndikatorPeer::NILAI)) $criteria->add(PrevSubtitleIndikatorPeer::NILAI, $this->nilai);
		if ($this->isColumnModified(PrevSubtitleIndikatorPeer::SATUAN)) $criteria->add(PrevSubtitleIndikatorPeer::SATUAN, $this->satuan);
		if ($this->isColumnModified(PrevSubtitleIndikatorPeer::LAST_UPDATE_USER)) $criteria->add(PrevSubtitleIndikatorPeer::LAST_UPDATE_USER, $this->last_update_user);
		if ($this->isColumnModified(PrevSubtitleIndikatorPeer::LAST_UPDATE_TIME)) $criteria->add(PrevSubtitleIndikatorPeer::LAST_UPDATE_TIME, $this->last_update_time);
		if ($this->isColumnModified(PrevSubtitleIndikatorPeer::LAST_UPDATE_IP)) $criteria->add(PrevSubtitleIndikatorPeer::LAST_UPDATE_IP, $this->last_update_ip);
		if ($this->isColumnModified(PrevSubtitleIndikatorPeer::TAHAP)) $criteria->add(PrevSubtitleIndikatorPeer::TAHAP, $this->tahap);
		if ($this->isColumnModified(PrevSubtitleIndikatorPeer::SUB_ID)) $criteria->add(PrevSubtitleIndikatorPeer::SUB_ID, $this->sub_id);
		if ($this->isColumnModified(PrevSubtitleIndikatorPeer::TAHUN)) $criteria->add(PrevSubtitleIndikatorPeer::TAHUN, $this->tahun);
		if ($this->isColumnModified(PrevSubtitleIndikatorPeer::LOCK_SUBTITLE)) $criteria->add(PrevSubtitleIndikatorPeer::LOCK_SUBTITLE, $this->lock_subtitle);
		if ($this->isColumnModified(PrevSubtitleIndikatorPeer::PRIORITAS)) $criteria->add(PrevSubtitleIndikatorPeer::PRIORITAS, $this->prioritas);
		if ($this->isColumnModified(PrevSubtitleIndikatorPeer::GENDER)) $criteria->add(PrevSubtitleIndikatorPeer::GENDER, $this->gender);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(PrevSubtitleIndikatorPeer::DATABASE_NAME);

		$criteria->add(PrevSubtitleIndikatorPeer::SUB_ID, $this->sub_id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getSubId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setSubId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setUnitId($this->unit_id);

		$copyObj->setKegiatanCode($this->kegiatan_code);

		$copyObj->setSubtitle($this->subtitle);

		$copyObj->setIndikator($this->indikator);

		$copyObj->setNilai($this->nilai);

		$copyObj->setSatuan($this->satuan);

		$copyObj->setLastUpdateUser($this->last_update_user);

		$copyObj->setLastUpdateTime($this->last_update_time);

		$copyObj->setLastUpdateIp($this->last_update_ip);

		$copyObj->setTahap($this->tahap);

		$copyObj->setTahun($this->tahun);

		$copyObj->setLockSubtitle($this->lock_subtitle);

		$copyObj->setPrioritas($this->prioritas);

		$copyObj->setGender($this->gender);


		$copyObj->setNew(true);

		$copyObj->setSubId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new PrevSubtitleIndikatorPeer();
		}
		return self::$peer;
	}

} 