<?php


abstract class BasePakBukuPutihRincianPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.pak_bukuputih_rincian';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.PakBukuPutihRincian';

	
	const NUM_COLUMNS = 17;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const KEGIATAN_CODE = 'ebudget.pak_bukuputih_rincian.KEGIATAN_CODE';

	
	const TIPE = 'ebudget.pak_bukuputih_rincian.TIPE';

	
	const RINCIAN_CONFIRMED = 'ebudget.pak_bukuputih_rincian.RINCIAN_CONFIRMED';

	
	const RINCIAN_CHANGED = 'ebudget.pak_bukuputih_rincian.RINCIAN_CHANGED';

	
	const RINCIAN_SELESAI = 'ebudget.pak_bukuputih_rincian.RINCIAN_SELESAI';

	
	const IP_ADDRESS = 'ebudget.pak_bukuputih_rincian.IP_ADDRESS';

	
	const WAKTU_ACCESS = 'ebudget.pak_bukuputih_rincian.WAKTU_ACCESS';

	
	const TARGET = 'ebudget.pak_bukuputih_rincian.TARGET';

	
	const UNIT_ID = 'ebudget.pak_bukuputih_rincian.UNIT_ID';

	
	const RINCIAN_LEVEL = 'ebudget.pak_bukuputih_rincian.RINCIAN_LEVEL';

	
	const LOCK = 'ebudget.pak_bukuputih_rincian.LOCK';

	
	const LAST_UPDATE_USER = 'ebudget.pak_bukuputih_rincian.LAST_UPDATE_USER';

	
	const LAST_UPDATE_TIME = 'ebudget.pak_bukuputih_rincian.LAST_UPDATE_TIME';

	
	const LAST_UPDATE_IP = 'ebudget.pak_bukuputih_rincian.LAST_UPDATE_IP';

	
	const TAHAP = 'ebudget.pak_bukuputih_rincian.TAHAP';

	
	const TAHUN = 'ebudget.pak_bukuputih_rincian.TAHUN';

	
	const RINCIAN_REVISI_LEVEL = 'ebudget.pak_bukuputih_rincian.RINCIAN_REVISI_LEVEL';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('KegiatanCode', 'Tipe', 'RincianConfirmed', 'RincianChanged', 'RincianSelesai', 'IpAddress', 'WaktuAccess', 'Target', 'UnitId', 'RincianLevel', 'Lock', 'LastUpdateUser', 'LastUpdateTime', 'LastUpdateIp', 'Tahap', 'Tahun', 'RincianRevisiLevel', ),
		BasePeer::TYPE_COLNAME => array (PakBukuPutihRincianPeer::KEGIATAN_CODE, PakBukuPutihRincianPeer::TIPE, PakBukuPutihRincianPeer::RINCIAN_CONFIRMED, PakBukuPutihRincianPeer::RINCIAN_CHANGED, PakBukuPutihRincianPeer::RINCIAN_SELESAI, PakBukuPutihRincianPeer::IP_ADDRESS, PakBukuPutihRincianPeer::WAKTU_ACCESS, PakBukuPutihRincianPeer::TARGET, PakBukuPutihRincianPeer::UNIT_ID, PakBukuPutihRincianPeer::RINCIAN_LEVEL, PakBukuPutihRincianPeer::LOCK, PakBukuPutihRincianPeer::LAST_UPDATE_USER, PakBukuPutihRincianPeer::LAST_UPDATE_TIME, PakBukuPutihRincianPeer::LAST_UPDATE_IP, PakBukuPutihRincianPeer::TAHAP, PakBukuPutihRincianPeer::TAHUN, PakBukuPutihRincianPeer::RINCIAN_REVISI_LEVEL, ),
		BasePeer::TYPE_FIELDNAME => array ('kegiatan_code', 'tipe', 'rincian_confirmed', 'rincian_changed', 'rincian_selesai', 'ip_address', 'waktu_access', 'target', 'unit_id', 'rincian_level', 'lock', 'last_update_user', 'last_update_time', 'last_update_ip', 'tahap', 'tahun', 'rincian_revisi_level', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('KegiatanCode' => 0, 'Tipe' => 1, 'RincianConfirmed' => 2, 'RincianChanged' => 3, 'RincianSelesai' => 4, 'IpAddress' => 5, 'WaktuAccess' => 6, 'Target' => 7, 'UnitId' => 8, 'RincianLevel' => 9, 'Lock' => 10, 'LastUpdateUser' => 11, 'LastUpdateTime' => 12, 'LastUpdateIp' => 13, 'Tahap' => 14, 'Tahun' => 15, 'RincianRevisiLevel' => 16, ),
		BasePeer::TYPE_COLNAME => array (PakBukuPutihRincianPeer::KEGIATAN_CODE => 0, PakBukuPutihRincianPeer::TIPE => 1, PakBukuPutihRincianPeer::RINCIAN_CONFIRMED => 2, PakBukuPutihRincianPeer::RINCIAN_CHANGED => 3, PakBukuPutihRincianPeer::RINCIAN_SELESAI => 4, PakBukuPutihRincianPeer::IP_ADDRESS => 5, PakBukuPutihRincianPeer::WAKTU_ACCESS => 6, PakBukuPutihRincianPeer::TARGET => 7, PakBukuPutihRincianPeer::UNIT_ID => 8, PakBukuPutihRincianPeer::RINCIAN_LEVEL => 9, PakBukuPutihRincianPeer::LOCK => 10, PakBukuPutihRincianPeer::LAST_UPDATE_USER => 11, PakBukuPutihRincianPeer::LAST_UPDATE_TIME => 12, PakBukuPutihRincianPeer::LAST_UPDATE_IP => 13, PakBukuPutihRincianPeer::TAHAP => 14, PakBukuPutihRincianPeer::TAHUN => 15, PakBukuPutihRincianPeer::RINCIAN_REVISI_LEVEL => 16, ),
		BasePeer::TYPE_FIELDNAME => array ('kegiatan_code' => 0, 'tipe' => 1, 'rincian_confirmed' => 2, 'rincian_changed' => 3, 'rincian_selesai' => 4, 'ip_address' => 5, 'waktu_access' => 6, 'target' => 7, 'unit_id' => 8, 'rincian_level' => 9, 'lock' => 10, 'last_update_user' => 11, 'last_update_time' => 12, 'last_update_ip' => 13, 'tahap' => 14, 'tahun' => 15, 'rincian_revisi_level' => 16, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/PakBukuPutihRincianMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.PakBukuPutihRincianMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = PakBukuPutihRincianPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(PakBukuPutihRincianPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(PakBukuPutihRincianPeer::KEGIATAN_CODE);

		$criteria->addSelectColumn(PakBukuPutihRincianPeer::TIPE);

		$criteria->addSelectColumn(PakBukuPutihRincianPeer::RINCIAN_CONFIRMED);

		$criteria->addSelectColumn(PakBukuPutihRincianPeer::RINCIAN_CHANGED);

		$criteria->addSelectColumn(PakBukuPutihRincianPeer::RINCIAN_SELESAI);

		$criteria->addSelectColumn(PakBukuPutihRincianPeer::IP_ADDRESS);

		$criteria->addSelectColumn(PakBukuPutihRincianPeer::WAKTU_ACCESS);

		$criteria->addSelectColumn(PakBukuPutihRincianPeer::TARGET);

		$criteria->addSelectColumn(PakBukuPutihRincianPeer::UNIT_ID);

		$criteria->addSelectColumn(PakBukuPutihRincianPeer::RINCIAN_LEVEL);

		$criteria->addSelectColumn(PakBukuPutihRincianPeer::LOCK);

		$criteria->addSelectColumn(PakBukuPutihRincianPeer::LAST_UPDATE_USER);

		$criteria->addSelectColumn(PakBukuPutihRincianPeer::LAST_UPDATE_TIME);

		$criteria->addSelectColumn(PakBukuPutihRincianPeer::LAST_UPDATE_IP);

		$criteria->addSelectColumn(PakBukuPutihRincianPeer::TAHAP);

		$criteria->addSelectColumn(PakBukuPutihRincianPeer::TAHUN);

		$criteria->addSelectColumn(PakBukuPutihRincianPeer::RINCIAN_REVISI_LEVEL);

	}

	const COUNT = 'COUNT(ebudget.pak_bukuputih_rincian.KEGIATAN_CODE)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.pak_bukuputih_rincian.KEGIATAN_CODE)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(PakBukuPutihRincianPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(PakBukuPutihRincianPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = PakBukuPutihRincianPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = PakBukuPutihRincianPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return PakBukuPutihRincianPeer::populateObjects(PakBukuPutihRincianPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			PakBukuPutihRincianPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = PakBukuPutihRincianPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return PakBukuPutihRincianPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(PakBukuPutihRincianPeer::KEGIATAN_CODE);
			$selectCriteria->add(PakBukuPutihRincianPeer::KEGIATAN_CODE, $criteria->remove(PakBukuPutihRincianPeer::KEGIATAN_CODE), $comparison);

			$comparison = $criteria->getComparison(PakBukuPutihRincianPeer::TIPE);
			$selectCriteria->add(PakBukuPutihRincianPeer::TIPE, $criteria->remove(PakBukuPutihRincianPeer::TIPE), $comparison);

			$comparison = $criteria->getComparison(PakBukuPutihRincianPeer::UNIT_ID);
			$selectCriteria->add(PakBukuPutihRincianPeer::UNIT_ID, $criteria->remove(PakBukuPutihRincianPeer::UNIT_ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(PakBukuPutihRincianPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(PakBukuPutihRincianPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof PakBukuPutihRincian) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
												if(count($values) == count($values, COUNT_RECURSIVE))
			{
								$values = array($values);
			}
			$vals = array();
			foreach($values as $value)
			{

				$vals[0][] = $value[0];
				$vals[1][] = $value[1];
				$vals[2][] = $value[2];
			}

			$criteria->add(PakBukuPutihRincianPeer::KEGIATAN_CODE, $vals[0], Criteria::IN);
			$criteria->add(PakBukuPutihRincianPeer::TIPE, $vals[1], Criteria::IN);
			$criteria->add(PakBukuPutihRincianPeer::UNIT_ID, $vals[2], Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(PakBukuPutihRincian $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(PakBukuPutihRincianPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(PakBukuPutihRincianPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(PakBukuPutihRincianPeer::DATABASE_NAME, PakBukuPutihRincianPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = PakBukuPutihRincianPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK( $kegiatan_code, $tipe, $unit_id, $con = null) {
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$criteria = new Criteria();
		$criteria->add(PakBukuPutihRincianPeer::KEGIATAN_CODE, $kegiatan_code);
		$criteria->add(PakBukuPutihRincianPeer::TIPE, $tipe);
		$criteria->add(PakBukuPutihRincianPeer::UNIT_ID, $unit_id);
		$v = PakBukuPutihRincianPeer::doSelect($criteria, $con);

		return !empty($v) ? $v[0] : null;
	}
} 
if (Propel::isInit()) {
			try {
		BasePakBukuPutihRincianPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/PakBukuPutihRincianMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.PakBukuPutihRincianMapBuilder');
}
