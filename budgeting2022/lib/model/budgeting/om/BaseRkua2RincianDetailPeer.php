<?php


abstract class BaseRkua2RincianDetailPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.rkua2_rincian_detail';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.Rkua2RincianDetail';

	
	const NUM_COLUMNS = 70;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const KEGIATAN_CODE = 'ebudget.rkua2_rincian_detail.KEGIATAN_CODE';

	
	const TIPE = 'ebudget.rkua2_rincian_detail.TIPE';

	
	const DETAIL_NO = 'ebudget.rkua2_rincian_detail.DETAIL_NO';

	
	const REKENING_CODE = 'ebudget.rkua2_rincian_detail.REKENING_CODE';

	
	const KOMPONEN_ID = 'ebudget.rkua2_rincian_detail.KOMPONEN_ID';

	
	const DETAIL_NAME = 'ebudget.rkua2_rincian_detail.DETAIL_NAME';

	
	const VOLUME = 'ebudget.rkua2_rincian_detail.VOLUME';

	
	const KETERANGAN_KOEFISIEN = 'ebudget.rkua2_rincian_detail.KETERANGAN_KOEFISIEN';

	
	const SUBTITLE = 'ebudget.rkua2_rincian_detail.SUBTITLE';

	
	const KOMPONEN_HARGA = 'ebudget.rkua2_rincian_detail.KOMPONEN_HARGA';

	
	const KOMPONEN_HARGA_AWAL = 'ebudget.rkua2_rincian_detail.KOMPONEN_HARGA_AWAL';

	
	const KOMPONEN_NAME = 'ebudget.rkua2_rincian_detail.KOMPONEN_NAME';

	
	const SATUAN = 'ebudget.rkua2_rincian_detail.SATUAN';

	
	const PAJAK = 'ebudget.rkua2_rincian_detail.PAJAK';

	
	const UNIT_ID = 'ebudget.rkua2_rincian_detail.UNIT_ID';

	
	const FROM_SUB_KEGIATAN = 'ebudget.rkua2_rincian_detail.FROM_SUB_KEGIATAN';

	
	const SUB = 'ebudget.rkua2_rincian_detail.SUB';

	
	const KODE_SUB = 'ebudget.rkua2_rincian_detail.KODE_SUB';

	
	const LAST_UPDATE_USER = 'ebudget.rkua2_rincian_detail.LAST_UPDATE_USER';

	
	const LAST_UPDATE_TIME = 'ebudget.rkua2_rincian_detail.LAST_UPDATE_TIME';

	
	const LAST_UPDATE_IP = 'ebudget.rkua2_rincian_detail.LAST_UPDATE_IP';

	
	const TAHAP = 'ebudget.rkua2_rincian_detail.TAHAP';

	
	const TAHAP_EDIT = 'ebudget.rkua2_rincian_detail.TAHAP_EDIT';

	
	const TAHAP_NEW = 'ebudget.rkua2_rincian_detail.TAHAP_NEW';

	
	const STATUS_LELANG = 'ebudget.rkua2_rincian_detail.STATUS_LELANG';

	
	const NOMOR_LELANG = 'ebudget.rkua2_rincian_detail.NOMOR_LELANG';

	
	const KOEFISIEN_SEMULA = 'ebudget.rkua2_rincian_detail.KOEFISIEN_SEMULA';

	
	const VOLUME_SEMULA = 'ebudget.rkua2_rincian_detail.VOLUME_SEMULA';

	
	const HARGA_SEMULA = 'ebudget.rkua2_rincian_detail.HARGA_SEMULA';

	
	const TOTAL_SEMULA = 'ebudget.rkua2_rincian_detail.TOTAL_SEMULA';

	
	const LOCK_SUBTITLE = 'ebudget.rkua2_rincian_detail.LOCK_SUBTITLE';

	
	const STATUS_HAPUS = 'ebudget.rkua2_rincian_detail.STATUS_HAPUS';

	
	const TAHUN = 'ebudget.rkua2_rincian_detail.TAHUN';

	
	const KODE_LOKASI = 'ebudget.rkua2_rincian_detail.KODE_LOKASI';

	
	const KECAMATAN = 'ebudget.rkua2_rincian_detail.KECAMATAN';

	
	const REKENING_CODE_ASLI = 'ebudget.rkua2_rincian_detail.REKENING_CODE_ASLI';

	
	const NOTE_SKPD = 'ebudget.rkua2_rincian_detail.NOTE_SKPD';

	
	const NOTE_PENELITI = 'ebudget.rkua2_rincian_detail.NOTE_PENELITI';

	
	const NILAI_ANGGARAN = 'ebudget.rkua2_rincian_detail.NILAI_ANGGARAN';

	
	const IS_BLUD = 'ebudget.rkua2_rincian_detail.IS_BLUD';

	
	const LOKASI_KECAMATAN = 'ebudget.rkua2_rincian_detail.LOKASI_KECAMATAN';

	
	const LOKASI_KELURAHAN = 'ebudget.rkua2_rincian_detail.LOKASI_KELURAHAN';

	
	const OB = 'ebudget.rkua2_rincian_detail.OB';

	
	const OB_FROM_ID = 'ebudget.rkua2_rincian_detail.OB_FROM_ID';

	
	const IS_PER_KOMPONEN = 'ebudget.rkua2_rincian_detail.IS_PER_KOMPONEN';

	
	const KEGIATAN_CODE_ASAL = 'ebudget.rkua2_rincian_detail.KEGIATAN_CODE_ASAL';

	
	const TH_KE_MULTIYEARS = 'ebudget.rkua2_rincian_detail.TH_KE_MULTIYEARS';

	
	const HARGA_SEBELUM_SISA_LELANG = 'ebudget.rkua2_rincian_detail.HARGA_SEBELUM_SISA_LELANG';

	
	const IS_MUSRENBANG = 'ebudget.rkua2_rincian_detail.IS_MUSRENBANG';

	
	const SUB_ID_ASAL = 'ebudget.rkua2_rincian_detail.SUB_ID_ASAL';

	
	const SUBTITLE_ASAL = 'ebudget.rkua2_rincian_detail.SUBTITLE_ASAL';

	
	const KODE_SUB_ASAL = 'ebudget.rkua2_rincian_detail.KODE_SUB_ASAL';

	
	const SUB_ASAL = 'ebudget.rkua2_rincian_detail.SUB_ASAL';

	
	const LAST_EDIT_TIME = 'ebudget.rkua2_rincian_detail.LAST_EDIT_TIME';

	
	const IS_POTONG_BPJS = 'ebudget.rkua2_rincian_detail.IS_POTONG_BPJS';

	
	const IS_IURAN_BPJS = 'ebudget.rkua2_rincian_detail.IS_IURAN_BPJS';

	
	const STATUS_OB = 'ebudget.rkua2_rincian_detail.STATUS_OB';

	
	const OB_PARENT = 'ebudget.rkua2_rincian_detail.OB_PARENT';

	
	const OB_ALOKASI_BARU = 'ebudget.rkua2_rincian_detail.OB_ALOKASI_BARU';

	
	const IS_HIBAH = 'ebudget.rkua2_rincian_detail.IS_HIBAH';

	
	const STATUS_LEVEL = 'ebudget.rkua2_rincian_detail.STATUS_LEVEL';

	
	const STATUS_LEVEL_TOLAK = 'ebudget.rkua2_rincian_detail.STATUS_LEVEL_TOLAK';

	
	const STATUS_SISIPAN = 'ebudget.rkua2_rincian_detail.STATUS_SISIPAN';

	
	const IS_TAPD_SETUJU = 'ebudget.rkua2_rincian_detail.IS_TAPD_SETUJU';

	
	const IS_BAPPEKO_SETUJU = 'ebudget.rkua2_rincian_detail.IS_BAPPEKO_SETUJU';

	
	const AKRUAL_CODE = 'ebudget.rkua2_rincian_detail.AKRUAL_CODE';

	
	const TIPE2 = 'ebudget.rkua2_rincian_detail.TIPE2';

	
	const IS_PENYELIA_SETUJU = 'ebudget.rkua2_rincian_detail.IS_PENYELIA_SETUJU';

	
	const NOTE_TAPD = 'ebudget.rkua2_rincian_detail.NOTE_TAPD';

	
	const NOTE_BAPPEKO = 'ebudget.rkua2_rincian_detail.NOTE_BAPPEKO';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('KegiatanCode', 'Tipe', 'DetailNo', 'RekeningCode', 'KomponenId', 'DetailName', 'Volume', 'KeteranganKoefisien', 'Subtitle', 'KomponenHarga', 'KomponenHargaAwal', 'KomponenName', 'Satuan', 'Pajak', 'UnitId', 'FromSubKegiatan', 'Sub', 'KodeSub', 'LastUpdateUser', 'LastUpdateTime', 'LastUpdateIp', 'Tahap', 'TahapEdit', 'TahapNew', 'StatusLelang', 'NomorLelang', 'KoefisienSemula', 'VolumeSemula', 'HargaSemula', 'TotalSemula', 'LockSubtitle', 'StatusHapus', 'Tahun', 'KodeLokasi', 'Kecamatan', 'RekeningCodeAsli', 'NoteSkpd', 'NotePeneliti', 'NilaiAnggaran', 'IsBlud', 'LokasiKecamatan', 'LokasiKelurahan', 'Ob', 'ObFromId', 'IsPerKomponen', 'KegiatanCodeAsal', 'ThKeMultiyears', 'HargaSebelumSisaLelang', 'IsMusrenbang', 'SubIdAsal', 'SubtitleAsal', 'KodeSubAsal', 'SubAsal', 'LastEditTime', 'IsPotongBpjs', 'IsIuranBpjs', 'StatusOb', 'ObParent', 'ObAlokasiBaru', 'IsHibah', 'StatusLevel', 'StatusLevelTolak', 'StatusSisipan', 'IsTapdSetuju', 'IsBappekoSetuju', 'AkrualCode', 'Tipe2', 'IsPenyeliaSetuju', 'NoteTapd', 'NoteBappeko', ),
		BasePeer::TYPE_COLNAME => array (Rkua2RincianDetailPeer::KEGIATAN_CODE, Rkua2RincianDetailPeer::TIPE, Rkua2RincianDetailPeer::DETAIL_NO, Rkua2RincianDetailPeer::REKENING_CODE, Rkua2RincianDetailPeer::KOMPONEN_ID, Rkua2RincianDetailPeer::DETAIL_NAME, Rkua2RincianDetailPeer::VOLUME, Rkua2RincianDetailPeer::KETERANGAN_KOEFISIEN, Rkua2RincianDetailPeer::SUBTITLE, Rkua2RincianDetailPeer::KOMPONEN_HARGA, Rkua2RincianDetailPeer::KOMPONEN_HARGA_AWAL, Rkua2RincianDetailPeer::KOMPONEN_NAME, Rkua2RincianDetailPeer::SATUAN, Rkua2RincianDetailPeer::PAJAK, Rkua2RincianDetailPeer::UNIT_ID, Rkua2RincianDetailPeer::FROM_SUB_KEGIATAN, Rkua2RincianDetailPeer::SUB, Rkua2RincianDetailPeer::KODE_SUB, Rkua2RincianDetailPeer::LAST_UPDATE_USER, Rkua2RincianDetailPeer::LAST_UPDATE_TIME, Rkua2RincianDetailPeer::LAST_UPDATE_IP, Rkua2RincianDetailPeer::TAHAP, Rkua2RincianDetailPeer::TAHAP_EDIT, Rkua2RincianDetailPeer::TAHAP_NEW, Rkua2RincianDetailPeer::STATUS_LELANG, Rkua2RincianDetailPeer::NOMOR_LELANG, Rkua2RincianDetailPeer::KOEFISIEN_SEMULA, Rkua2RincianDetailPeer::VOLUME_SEMULA, Rkua2RincianDetailPeer::HARGA_SEMULA, Rkua2RincianDetailPeer::TOTAL_SEMULA, Rkua2RincianDetailPeer::LOCK_SUBTITLE, Rkua2RincianDetailPeer::STATUS_HAPUS, Rkua2RincianDetailPeer::TAHUN, Rkua2RincianDetailPeer::KODE_LOKASI, Rkua2RincianDetailPeer::KECAMATAN, Rkua2RincianDetailPeer::REKENING_CODE_ASLI, Rkua2RincianDetailPeer::NOTE_SKPD, Rkua2RincianDetailPeer::NOTE_PENELITI, Rkua2RincianDetailPeer::NILAI_ANGGARAN, Rkua2RincianDetailPeer::IS_BLUD, Rkua2RincianDetailPeer::LOKASI_KECAMATAN, Rkua2RincianDetailPeer::LOKASI_KELURAHAN, Rkua2RincianDetailPeer::OB, Rkua2RincianDetailPeer::OB_FROM_ID, Rkua2RincianDetailPeer::IS_PER_KOMPONEN, Rkua2RincianDetailPeer::KEGIATAN_CODE_ASAL, Rkua2RincianDetailPeer::TH_KE_MULTIYEARS, Rkua2RincianDetailPeer::HARGA_SEBELUM_SISA_LELANG, Rkua2RincianDetailPeer::IS_MUSRENBANG, Rkua2RincianDetailPeer::SUB_ID_ASAL, Rkua2RincianDetailPeer::SUBTITLE_ASAL, Rkua2RincianDetailPeer::KODE_SUB_ASAL, Rkua2RincianDetailPeer::SUB_ASAL, Rkua2RincianDetailPeer::LAST_EDIT_TIME, Rkua2RincianDetailPeer::IS_POTONG_BPJS, Rkua2RincianDetailPeer::IS_IURAN_BPJS, Rkua2RincianDetailPeer::STATUS_OB, Rkua2RincianDetailPeer::OB_PARENT, Rkua2RincianDetailPeer::OB_ALOKASI_BARU, Rkua2RincianDetailPeer::IS_HIBAH, Rkua2RincianDetailPeer::STATUS_LEVEL, Rkua2RincianDetailPeer::STATUS_LEVEL_TOLAK, Rkua2RincianDetailPeer::STATUS_SISIPAN, Rkua2RincianDetailPeer::IS_TAPD_SETUJU, Rkua2RincianDetailPeer::IS_BAPPEKO_SETUJU, Rkua2RincianDetailPeer::AKRUAL_CODE, Rkua2RincianDetailPeer::TIPE2, Rkua2RincianDetailPeer::IS_PENYELIA_SETUJU, Rkua2RincianDetailPeer::NOTE_TAPD, Rkua2RincianDetailPeer::NOTE_BAPPEKO, ),
		BasePeer::TYPE_FIELDNAME => array ('kegiatan_code', 'tipe', 'detail_no', 'rekening_code', 'komponen_id', 'detail_name', 'volume', 'keterangan_koefisien', 'subtitle', 'komponen_harga', 'komponen_harga_awal', 'komponen_name', 'satuan', 'pajak', 'unit_id', 'from_sub_kegiatan', 'sub', 'kode_sub', 'last_update_user', 'last_update_time', 'last_update_ip', 'tahap', 'tahap_edit', 'tahap_new', 'status_lelang', 'nomor_lelang', 'koefisien_semula', 'volume_semula', 'harga_semula', 'total_semula', 'lock_subtitle', 'status_hapus', 'tahun', 'kode_lokasi', 'kecamatan', 'rekening_code_asli', 'note_skpd', 'note_peneliti', 'nilai_anggaran', 'is_blud', 'lokasi_kecamatan', 'lokasi_kelurahan', 'ob', 'ob_from_id', 'is_per_komponen', 'kegiatan_code_asal', 'th_ke_multiyears', 'harga_sebelum_sisa_lelang', 'is_musrenbang', 'sub_id_asal', 'subtitle_asal', 'kode_sub_asal', 'sub_asal', 'last_edit_time', 'is_potong_bpjs', 'is_iuran_bpjs', 'status_ob', 'ob_parent', 'ob_alokasi_baru', 'is_hibah', 'status_level', 'status_level_tolak', 'status_sisipan', 'is_tapd_setuju', 'is_bappeko_setuju', 'akrual_code', 'tipe2', 'is_penyelia_setuju', 'note_tapd', 'note_bappeko', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('KegiatanCode' => 0, 'Tipe' => 1, 'DetailNo' => 2, 'RekeningCode' => 3, 'KomponenId' => 4, 'DetailName' => 5, 'Volume' => 6, 'KeteranganKoefisien' => 7, 'Subtitle' => 8, 'KomponenHarga' => 9, 'KomponenHargaAwal' => 10, 'KomponenName' => 11, 'Satuan' => 12, 'Pajak' => 13, 'UnitId' => 14, 'FromSubKegiatan' => 15, 'Sub' => 16, 'KodeSub' => 17, 'LastUpdateUser' => 18, 'LastUpdateTime' => 19, 'LastUpdateIp' => 20, 'Tahap' => 21, 'TahapEdit' => 22, 'TahapNew' => 23, 'StatusLelang' => 24, 'NomorLelang' => 25, 'KoefisienSemula' => 26, 'VolumeSemula' => 27, 'HargaSemula' => 28, 'TotalSemula' => 29, 'LockSubtitle' => 30, 'StatusHapus' => 31, 'Tahun' => 32, 'KodeLokasi' => 33, 'Kecamatan' => 34, 'RekeningCodeAsli' => 35, 'NoteSkpd' => 36, 'NotePeneliti' => 37, 'NilaiAnggaran' => 38, 'IsBlud' => 39, 'LokasiKecamatan' => 40, 'LokasiKelurahan' => 41, 'Ob' => 42, 'ObFromId' => 43, 'IsPerKomponen' => 44, 'KegiatanCodeAsal' => 45, 'ThKeMultiyears' => 46, 'HargaSebelumSisaLelang' => 47, 'IsMusrenbang' => 48, 'SubIdAsal' => 49, 'SubtitleAsal' => 50, 'KodeSubAsal' => 51, 'SubAsal' => 52, 'LastEditTime' => 53, 'IsPotongBpjs' => 54, 'IsIuranBpjs' => 55, 'StatusOb' => 56, 'ObParent' => 57, 'ObAlokasiBaru' => 58, 'IsHibah' => 59, 'StatusLevel' => 60, 'StatusLevelTolak' => 61, 'StatusSisipan' => 62, 'IsTapdSetuju' => 63, 'IsBappekoSetuju' => 64, 'AkrualCode' => 65, 'Tipe2' => 66, 'IsPenyeliaSetuju' => 67, 'NoteTapd' => 68, 'NoteBappeko' => 69, ),
		BasePeer::TYPE_COLNAME => array (Rkua2RincianDetailPeer::KEGIATAN_CODE => 0, Rkua2RincianDetailPeer::TIPE => 1, Rkua2RincianDetailPeer::DETAIL_NO => 2, Rkua2RincianDetailPeer::REKENING_CODE => 3, Rkua2RincianDetailPeer::KOMPONEN_ID => 4, Rkua2RincianDetailPeer::DETAIL_NAME => 5, Rkua2RincianDetailPeer::VOLUME => 6, Rkua2RincianDetailPeer::KETERANGAN_KOEFISIEN => 7, Rkua2RincianDetailPeer::SUBTITLE => 8, Rkua2RincianDetailPeer::KOMPONEN_HARGA => 9, Rkua2RincianDetailPeer::KOMPONEN_HARGA_AWAL => 10, Rkua2RincianDetailPeer::KOMPONEN_NAME => 11, Rkua2RincianDetailPeer::SATUAN => 12, Rkua2RincianDetailPeer::PAJAK => 13, Rkua2RincianDetailPeer::UNIT_ID => 14, Rkua2RincianDetailPeer::FROM_SUB_KEGIATAN => 15, Rkua2RincianDetailPeer::SUB => 16, Rkua2RincianDetailPeer::KODE_SUB => 17, Rkua2RincianDetailPeer::LAST_UPDATE_USER => 18, Rkua2RincianDetailPeer::LAST_UPDATE_TIME => 19, Rkua2RincianDetailPeer::LAST_UPDATE_IP => 20, Rkua2RincianDetailPeer::TAHAP => 21, Rkua2RincianDetailPeer::TAHAP_EDIT => 22, Rkua2RincianDetailPeer::TAHAP_NEW => 23, Rkua2RincianDetailPeer::STATUS_LELANG => 24, Rkua2RincianDetailPeer::NOMOR_LELANG => 25, Rkua2RincianDetailPeer::KOEFISIEN_SEMULA => 26, Rkua2RincianDetailPeer::VOLUME_SEMULA => 27, Rkua2RincianDetailPeer::HARGA_SEMULA => 28, Rkua2RincianDetailPeer::TOTAL_SEMULA => 29, Rkua2RincianDetailPeer::LOCK_SUBTITLE => 30, Rkua2RincianDetailPeer::STATUS_HAPUS => 31, Rkua2RincianDetailPeer::TAHUN => 32, Rkua2RincianDetailPeer::KODE_LOKASI => 33, Rkua2RincianDetailPeer::KECAMATAN => 34, Rkua2RincianDetailPeer::REKENING_CODE_ASLI => 35, Rkua2RincianDetailPeer::NOTE_SKPD => 36, Rkua2RincianDetailPeer::NOTE_PENELITI => 37, Rkua2RincianDetailPeer::NILAI_ANGGARAN => 38, Rkua2RincianDetailPeer::IS_BLUD => 39, Rkua2RincianDetailPeer::LOKASI_KECAMATAN => 40, Rkua2RincianDetailPeer::LOKASI_KELURAHAN => 41, Rkua2RincianDetailPeer::OB => 42, Rkua2RincianDetailPeer::OB_FROM_ID => 43, Rkua2RincianDetailPeer::IS_PER_KOMPONEN => 44, Rkua2RincianDetailPeer::KEGIATAN_CODE_ASAL => 45, Rkua2RincianDetailPeer::TH_KE_MULTIYEARS => 46, Rkua2RincianDetailPeer::HARGA_SEBELUM_SISA_LELANG => 47, Rkua2RincianDetailPeer::IS_MUSRENBANG => 48, Rkua2RincianDetailPeer::SUB_ID_ASAL => 49, Rkua2RincianDetailPeer::SUBTITLE_ASAL => 50, Rkua2RincianDetailPeer::KODE_SUB_ASAL => 51, Rkua2RincianDetailPeer::SUB_ASAL => 52, Rkua2RincianDetailPeer::LAST_EDIT_TIME => 53, Rkua2RincianDetailPeer::IS_POTONG_BPJS => 54, Rkua2RincianDetailPeer::IS_IURAN_BPJS => 55, Rkua2RincianDetailPeer::STATUS_OB => 56, Rkua2RincianDetailPeer::OB_PARENT => 57, Rkua2RincianDetailPeer::OB_ALOKASI_BARU => 58, Rkua2RincianDetailPeer::IS_HIBAH => 59, Rkua2RincianDetailPeer::STATUS_LEVEL => 60, Rkua2RincianDetailPeer::STATUS_LEVEL_TOLAK => 61, Rkua2RincianDetailPeer::STATUS_SISIPAN => 62, Rkua2RincianDetailPeer::IS_TAPD_SETUJU => 63, Rkua2RincianDetailPeer::IS_BAPPEKO_SETUJU => 64, Rkua2RincianDetailPeer::AKRUAL_CODE => 65, Rkua2RincianDetailPeer::TIPE2 => 66, Rkua2RincianDetailPeer::IS_PENYELIA_SETUJU => 67, Rkua2RincianDetailPeer::NOTE_TAPD => 68, Rkua2RincianDetailPeer::NOTE_BAPPEKO => 69, ),
		BasePeer::TYPE_FIELDNAME => array ('kegiatan_code' => 0, 'tipe' => 1, 'detail_no' => 2, 'rekening_code' => 3, 'komponen_id' => 4, 'detail_name' => 5, 'volume' => 6, 'keterangan_koefisien' => 7, 'subtitle' => 8, 'komponen_harga' => 9, 'komponen_harga_awal' => 10, 'komponen_name' => 11, 'satuan' => 12, 'pajak' => 13, 'unit_id' => 14, 'from_sub_kegiatan' => 15, 'sub' => 16, 'kode_sub' => 17, 'last_update_user' => 18, 'last_update_time' => 19, 'last_update_ip' => 20, 'tahap' => 21, 'tahap_edit' => 22, 'tahap_new' => 23, 'status_lelang' => 24, 'nomor_lelang' => 25, 'koefisien_semula' => 26, 'volume_semula' => 27, 'harga_semula' => 28, 'total_semula' => 29, 'lock_subtitle' => 30, 'status_hapus' => 31, 'tahun' => 32, 'kode_lokasi' => 33, 'kecamatan' => 34, 'rekening_code_asli' => 35, 'note_skpd' => 36, 'note_peneliti' => 37, 'nilai_anggaran' => 38, 'is_blud' => 39, 'lokasi_kecamatan' => 40, 'lokasi_kelurahan' => 41, 'ob' => 42, 'ob_from_id' => 43, 'is_per_komponen' => 44, 'kegiatan_code_asal' => 45, 'th_ke_multiyears' => 46, 'harga_sebelum_sisa_lelang' => 47, 'is_musrenbang' => 48, 'sub_id_asal' => 49, 'subtitle_asal' => 50, 'kode_sub_asal' => 51, 'sub_asal' => 52, 'last_edit_time' => 53, 'is_potong_bpjs' => 54, 'is_iuran_bpjs' => 55, 'status_ob' => 56, 'ob_parent' => 57, 'ob_alokasi_baru' => 58, 'is_hibah' => 59, 'status_level' => 60, 'status_level_tolak' => 61, 'status_sisipan' => 62, 'is_tapd_setuju' => 63, 'is_bappeko_setuju' => 64, 'akrual_code' => 65, 'tipe2' => 66, 'is_penyelia_setuju' => 67, 'note_tapd' => 68, 'note_bappeko' => 69, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/Rkua2RincianDetailMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.Rkua2RincianDetailMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = Rkua2RincianDetailPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(Rkua2RincianDetailPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(Rkua2RincianDetailPeer::KEGIATAN_CODE);

		$criteria->addSelectColumn(Rkua2RincianDetailPeer::TIPE);

		$criteria->addSelectColumn(Rkua2RincianDetailPeer::DETAIL_NO);

		$criteria->addSelectColumn(Rkua2RincianDetailPeer::REKENING_CODE);

		$criteria->addSelectColumn(Rkua2RincianDetailPeer::KOMPONEN_ID);

		$criteria->addSelectColumn(Rkua2RincianDetailPeer::DETAIL_NAME);

		$criteria->addSelectColumn(Rkua2RincianDetailPeer::VOLUME);

		$criteria->addSelectColumn(Rkua2RincianDetailPeer::KETERANGAN_KOEFISIEN);

		$criteria->addSelectColumn(Rkua2RincianDetailPeer::SUBTITLE);

		$criteria->addSelectColumn(Rkua2RincianDetailPeer::KOMPONEN_HARGA);

		$criteria->addSelectColumn(Rkua2RincianDetailPeer::KOMPONEN_HARGA_AWAL);

		$criteria->addSelectColumn(Rkua2RincianDetailPeer::KOMPONEN_NAME);

		$criteria->addSelectColumn(Rkua2RincianDetailPeer::SATUAN);

		$criteria->addSelectColumn(Rkua2RincianDetailPeer::PAJAK);

		$criteria->addSelectColumn(Rkua2RincianDetailPeer::UNIT_ID);

		$criteria->addSelectColumn(Rkua2RincianDetailPeer::FROM_SUB_KEGIATAN);

		$criteria->addSelectColumn(Rkua2RincianDetailPeer::SUB);

		$criteria->addSelectColumn(Rkua2RincianDetailPeer::KODE_SUB);

		$criteria->addSelectColumn(Rkua2RincianDetailPeer::LAST_UPDATE_USER);

		$criteria->addSelectColumn(Rkua2RincianDetailPeer::LAST_UPDATE_TIME);

		$criteria->addSelectColumn(Rkua2RincianDetailPeer::LAST_UPDATE_IP);

		$criteria->addSelectColumn(Rkua2RincianDetailPeer::TAHAP);

		$criteria->addSelectColumn(Rkua2RincianDetailPeer::TAHAP_EDIT);

		$criteria->addSelectColumn(Rkua2RincianDetailPeer::TAHAP_NEW);

		$criteria->addSelectColumn(Rkua2RincianDetailPeer::STATUS_LELANG);

		$criteria->addSelectColumn(Rkua2RincianDetailPeer::NOMOR_LELANG);

		$criteria->addSelectColumn(Rkua2RincianDetailPeer::KOEFISIEN_SEMULA);

		$criteria->addSelectColumn(Rkua2RincianDetailPeer::VOLUME_SEMULA);

		$criteria->addSelectColumn(Rkua2RincianDetailPeer::HARGA_SEMULA);

		$criteria->addSelectColumn(Rkua2RincianDetailPeer::TOTAL_SEMULA);

		$criteria->addSelectColumn(Rkua2RincianDetailPeer::LOCK_SUBTITLE);

		$criteria->addSelectColumn(Rkua2RincianDetailPeer::STATUS_HAPUS);

		$criteria->addSelectColumn(Rkua2RincianDetailPeer::TAHUN);

		$criteria->addSelectColumn(Rkua2RincianDetailPeer::KODE_LOKASI);

		$criteria->addSelectColumn(Rkua2RincianDetailPeer::KECAMATAN);

		$criteria->addSelectColumn(Rkua2RincianDetailPeer::REKENING_CODE_ASLI);

		$criteria->addSelectColumn(Rkua2RincianDetailPeer::NOTE_SKPD);

		$criteria->addSelectColumn(Rkua2RincianDetailPeer::NOTE_PENELITI);

		$criteria->addSelectColumn(Rkua2RincianDetailPeer::NILAI_ANGGARAN);

		$criteria->addSelectColumn(Rkua2RincianDetailPeer::IS_BLUD);

		$criteria->addSelectColumn(Rkua2RincianDetailPeer::LOKASI_KECAMATAN);

		$criteria->addSelectColumn(Rkua2RincianDetailPeer::LOKASI_KELURAHAN);

		$criteria->addSelectColumn(Rkua2RincianDetailPeer::OB);

		$criteria->addSelectColumn(Rkua2RincianDetailPeer::OB_FROM_ID);

		$criteria->addSelectColumn(Rkua2RincianDetailPeer::IS_PER_KOMPONEN);

		$criteria->addSelectColumn(Rkua2RincianDetailPeer::KEGIATAN_CODE_ASAL);

		$criteria->addSelectColumn(Rkua2RincianDetailPeer::TH_KE_MULTIYEARS);

		$criteria->addSelectColumn(Rkua2RincianDetailPeer::HARGA_SEBELUM_SISA_LELANG);

		$criteria->addSelectColumn(Rkua2RincianDetailPeer::IS_MUSRENBANG);

		$criteria->addSelectColumn(Rkua2RincianDetailPeer::SUB_ID_ASAL);

		$criteria->addSelectColumn(Rkua2RincianDetailPeer::SUBTITLE_ASAL);

		$criteria->addSelectColumn(Rkua2RincianDetailPeer::KODE_SUB_ASAL);

		$criteria->addSelectColumn(Rkua2RincianDetailPeer::SUB_ASAL);

		$criteria->addSelectColumn(Rkua2RincianDetailPeer::LAST_EDIT_TIME);

		$criteria->addSelectColumn(Rkua2RincianDetailPeer::IS_POTONG_BPJS);

		$criteria->addSelectColumn(Rkua2RincianDetailPeer::IS_IURAN_BPJS);

		$criteria->addSelectColumn(Rkua2RincianDetailPeer::STATUS_OB);

		$criteria->addSelectColumn(Rkua2RincianDetailPeer::OB_PARENT);

		$criteria->addSelectColumn(Rkua2RincianDetailPeer::OB_ALOKASI_BARU);

		$criteria->addSelectColumn(Rkua2RincianDetailPeer::IS_HIBAH);

		$criteria->addSelectColumn(Rkua2RincianDetailPeer::STATUS_LEVEL);

		$criteria->addSelectColumn(Rkua2RincianDetailPeer::STATUS_LEVEL_TOLAK);

		$criteria->addSelectColumn(Rkua2RincianDetailPeer::STATUS_SISIPAN);

		$criteria->addSelectColumn(Rkua2RincianDetailPeer::IS_TAPD_SETUJU);

		$criteria->addSelectColumn(Rkua2RincianDetailPeer::IS_BAPPEKO_SETUJU);

		$criteria->addSelectColumn(Rkua2RincianDetailPeer::AKRUAL_CODE);

		$criteria->addSelectColumn(Rkua2RincianDetailPeer::TIPE2);

		$criteria->addSelectColumn(Rkua2RincianDetailPeer::IS_PENYELIA_SETUJU);

		$criteria->addSelectColumn(Rkua2RincianDetailPeer::NOTE_TAPD);

		$criteria->addSelectColumn(Rkua2RincianDetailPeer::NOTE_BAPPEKO);

	}

	const COUNT = 'COUNT(ebudget.rkua2_rincian_detail.KEGIATAN_CODE)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.rkua2_rincian_detail.KEGIATAN_CODE)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(Rkua2RincianDetailPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(Rkua2RincianDetailPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = Rkua2RincianDetailPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = Rkua2RincianDetailPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return Rkua2RincianDetailPeer::populateObjects(Rkua2RincianDetailPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			Rkua2RincianDetailPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = Rkua2RincianDetailPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return Rkua2RincianDetailPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(Rkua2RincianDetailPeer::KEGIATAN_CODE);
			$selectCriteria->add(Rkua2RincianDetailPeer::KEGIATAN_CODE, $criteria->remove(Rkua2RincianDetailPeer::KEGIATAN_CODE), $comparison);

			$comparison = $criteria->getComparison(Rkua2RincianDetailPeer::DETAIL_NO);
			$selectCriteria->add(Rkua2RincianDetailPeer::DETAIL_NO, $criteria->remove(Rkua2RincianDetailPeer::DETAIL_NO), $comparison);

			$comparison = $criteria->getComparison(Rkua2RincianDetailPeer::UNIT_ID);
			$selectCriteria->add(Rkua2RincianDetailPeer::UNIT_ID, $criteria->remove(Rkua2RincianDetailPeer::UNIT_ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(Rkua2RincianDetailPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(Rkua2RincianDetailPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof Rkua2RincianDetail) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
												if(count($values) == count($values, COUNT_RECURSIVE))
			{
								$values = array($values);
			}
			$vals = array();
			foreach($values as $value)
			{

				$vals[0][] = $value[0];
				$vals[1][] = $value[1];
				$vals[2][] = $value[2];
			}

			$criteria->add(Rkua2RincianDetailPeer::KEGIATAN_CODE, $vals[0], Criteria::IN);
			$criteria->add(Rkua2RincianDetailPeer::DETAIL_NO, $vals[1], Criteria::IN);
			$criteria->add(Rkua2RincianDetailPeer::UNIT_ID, $vals[2], Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(Rkua2RincianDetail $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(Rkua2RincianDetailPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(Rkua2RincianDetailPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(Rkua2RincianDetailPeer::DATABASE_NAME, Rkua2RincianDetailPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = Rkua2RincianDetailPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK( $kegiatan_code, $detail_no, $unit_id, $con = null) {
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$criteria = new Criteria();
		$criteria->add(Rkua2RincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
		$criteria->add(Rkua2RincianDetailPeer::DETAIL_NO, $detail_no);
		$criteria->add(Rkua2RincianDetailPeer::UNIT_ID, $unit_id);
		$v = Rkua2RincianDetailPeer::doSelect($criteria, $con);

		return !empty($v) ? $v[0] : null;
	}
} 
if (Propel::isInit()) {
			try {
		BaseRkua2RincianDetailPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/Rkua2RincianDetailMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.Rkua2RincianDetailMapBuilder');
}
