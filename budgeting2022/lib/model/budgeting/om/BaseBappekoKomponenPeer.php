<?php


abstract class BaseBappekoKomponenPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.bappeko_komponen';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.BappekoKomponen';

	
	const NUM_COLUMNS = 22;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const KOMPONEN_ID = 'ebudget.bappeko_komponen.KOMPONEN_ID';

	
	const SATUAN = 'ebudget.bappeko_komponen.SATUAN';

	
	const KOMPONEN_NAME = 'ebudget.bappeko_komponen.KOMPONEN_NAME';

	
	const SHSD_ID = 'ebudget.bappeko_komponen.SHSD_ID';

	
	const KOMPONEN_HARGA = 'ebudget.bappeko_komponen.KOMPONEN_HARGA';

	
	const KOMPONEN_SHOW = 'ebudget.bappeko_komponen.KOMPONEN_SHOW';

	
	const IP_ADDRESS = 'ebudget.bappeko_komponen.IP_ADDRESS';

	
	const WAKTU_ACCESS = 'ebudget.bappeko_komponen.WAKTU_ACCESS';

	
	const KOMPONEN_TIPE = 'ebudget.bappeko_komponen.KOMPONEN_TIPE';

	
	const KOMPONEN_CONFIRMED = 'ebudget.bappeko_komponen.KOMPONEN_CONFIRMED';

	
	const KOMPONEN_NON_PAJAK = 'ebudget.bappeko_komponen.KOMPONEN_NON_PAJAK';

	
	const USER_ID = 'ebudget.bappeko_komponen.USER_ID';

	
	const REKENING = 'ebudget.bappeko_komponen.REKENING';

	
	const KELOMPOK = 'ebudget.bappeko_komponen.KELOMPOK';

	
	const PEMELIHARAAN = 'ebudget.bappeko_komponen.PEMELIHARAAN';

	
	const REK_UPAH = 'ebudget.bappeko_komponen.REK_UPAH';

	
	const REK_BAHAN = 'ebudget.bappeko_komponen.REK_BAHAN';

	
	const REK_SEWA = 'ebudget.bappeko_komponen.REK_SEWA';

	
	const DESKRIPSI = 'ebudget.bappeko_komponen.DESKRIPSI';

	
	const STATUS_MASUK = 'ebudget.bappeko_komponen.STATUS_MASUK';

	
	const RKA_MEMBER = 'ebudget.bappeko_komponen.RKA_MEMBER';

	
	const MAINTENANCE = 'ebudget.bappeko_komponen.MAINTENANCE';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('KomponenId', 'Satuan', 'KomponenName', 'ShsdId', 'KomponenHarga', 'KomponenShow', 'IpAddress', 'WaktuAccess', 'KomponenTipe', 'KomponenConfirmed', 'KomponenNonPajak', 'UserId', 'Rekening', 'Kelompok', 'Pemeliharaan', 'RekUpah', 'RekBahan', 'RekSewa', 'Deskripsi', 'StatusMasuk', 'RkaMember', 'Maintenance', ),
		BasePeer::TYPE_COLNAME => array (BappekoKomponenPeer::KOMPONEN_ID, BappekoKomponenPeer::SATUAN, BappekoKomponenPeer::KOMPONEN_NAME, BappekoKomponenPeer::SHSD_ID, BappekoKomponenPeer::KOMPONEN_HARGA, BappekoKomponenPeer::KOMPONEN_SHOW, BappekoKomponenPeer::IP_ADDRESS, BappekoKomponenPeer::WAKTU_ACCESS, BappekoKomponenPeer::KOMPONEN_TIPE, BappekoKomponenPeer::KOMPONEN_CONFIRMED, BappekoKomponenPeer::KOMPONEN_NON_PAJAK, BappekoKomponenPeer::USER_ID, BappekoKomponenPeer::REKENING, BappekoKomponenPeer::KELOMPOK, BappekoKomponenPeer::PEMELIHARAAN, BappekoKomponenPeer::REK_UPAH, BappekoKomponenPeer::REK_BAHAN, BappekoKomponenPeer::REK_SEWA, BappekoKomponenPeer::DESKRIPSI, BappekoKomponenPeer::STATUS_MASUK, BappekoKomponenPeer::RKA_MEMBER, BappekoKomponenPeer::MAINTENANCE, ),
		BasePeer::TYPE_FIELDNAME => array ('komponen_id', 'satuan', 'komponen_name', 'shsd_id', 'komponen_harga', 'komponen_show', 'ip_address', 'waktu_access', 'komponen_tipe', 'komponen_confirmed', 'komponen_non_pajak', 'user_id', 'rekening', 'kelompok', 'pemeliharaan', 'rek_upah', 'rek_bahan', 'rek_sewa', 'deskripsi', 'status_masuk', 'rka_member', 'maintenance', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('KomponenId' => 0, 'Satuan' => 1, 'KomponenName' => 2, 'ShsdId' => 3, 'KomponenHarga' => 4, 'KomponenShow' => 5, 'IpAddress' => 6, 'WaktuAccess' => 7, 'KomponenTipe' => 8, 'KomponenConfirmed' => 9, 'KomponenNonPajak' => 10, 'UserId' => 11, 'Rekening' => 12, 'Kelompok' => 13, 'Pemeliharaan' => 14, 'RekUpah' => 15, 'RekBahan' => 16, 'RekSewa' => 17, 'Deskripsi' => 18, 'StatusMasuk' => 19, 'RkaMember' => 20, 'Maintenance' => 21, ),
		BasePeer::TYPE_COLNAME => array (BappekoKomponenPeer::KOMPONEN_ID => 0, BappekoKomponenPeer::SATUAN => 1, BappekoKomponenPeer::KOMPONEN_NAME => 2, BappekoKomponenPeer::SHSD_ID => 3, BappekoKomponenPeer::KOMPONEN_HARGA => 4, BappekoKomponenPeer::KOMPONEN_SHOW => 5, BappekoKomponenPeer::IP_ADDRESS => 6, BappekoKomponenPeer::WAKTU_ACCESS => 7, BappekoKomponenPeer::KOMPONEN_TIPE => 8, BappekoKomponenPeer::KOMPONEN_CONFIRMED => 9, BappekoKomponenPeer::KOMPONEN_NON_PAJAK => 10, BappekoKomponenPeer::USER_ID => 11, BappekoKomponenPeer::REKENING => 12, BappekoKomponenPeer::KELOMPOK => 13, BappekoKomponenPeer::PEMELIHARAAN => 14, BappekoKomponenPeer::REK_UPAH => 15, BappekoKomponenPeer::REK_BAHAN => 16, BappekoKomponenPeer::REK_SEWA => 17, BappekoKomponenPeer::DESKRIPSI => 18, BappekoKomponenPeer::STATUS_MASUK => 19, BappekoKomponenPeer::RKA_MEMBER => 20, BappekoKomponenPeer::MAINTENANCE => 21, ),
		BasePeer::TYPE_FIELDNAME => array ('komponen_id' => 0, 'satuan' => 1, 'komponen_name' => 2, 'shsd_id' => 3, 'komponen_harga' => 4, 'komponen_show' => 5, 'ip_address' => 6, 'waktu_access' => 7, 'komponen_tipe' => 8, 'komponen_confirmed' => 9, 'komponen_non_pajak' => 10, 'user_id' => 11, 'rekening' => 12, 'kelompok' => 13, 'pemeliharaan' => 14, 'rek_upah' => 15, 'rek_bahan' => 16, 'rek_sewa' => 17, 'deskripsi' => 18, 'status_masuk' => 19, 'rka_member' => 20, 'maintenance' => 21, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/BappekoKomponenMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.BappekoKomponenMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = BappekoKomponenPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(BappekoKomponenPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(BappekoKomponenPeer::KOMPONEN_ID);

		$criteria->addSelectColumn(BappekoKomponenPeer::SATUAN);

		$criteria->addSelectColumn(BappekoKomponenPeer::KOMPONEN_NAME);

		$criteria->addSelectColumn(BappekoKomponenPeer::SHSD_ID);

		$criteria->addSelectColumn(BappekoKomponenPeer::KOMPONEN_HARGA);

		$criteria->addSelectColumn(BappekoKomponenPeer::KOMPONEN_SHOW);

		$criteria->addSelectColumn(BappekoKomponenPeer::IP_ADDRESS);

		$criteria->addSelectColumn(BappekoKomponenPeer::WAKTU_ACCESS);

		$criteria->addSelectColumn(BappekoKomponenPeer::KOMPONEN_TIPE);

		$criteria->addSelectColumn(BappekoKomponenPeer::KOMPONEN_CONFIRMED);

		$criteria->addSelectColumn(BappekoKomponenPeer::KOMPONEN_NON_PAJAK);

		$criteria->addSelectColumn(BappekoKomponenPeer::USER_ID);

		$criteria->addSelectColumn(BappekoKomponenPeer::REKENING);

		$criteria->addSelectColumn(BappekoKomponenPeer::KELOMPOK);

		$criteria->addSelectColumn(BappekoKomponenPeer::PEMELIHARAAN);

		$criteria->addSelectColumn(BappekoKomponenPeer::REK_UPAH);

		$criteria->addSelectColumn(BappekoKomponenPeer::REK_BAHAN);

		$criteria->addSelectColumn(BappekoKomponenPeer::REK_SEWA);

		$criteria->addSelectColumn(BappekoKomponenPeer::DESKRIPSI);

		$criteria->addSelectColumn(BappekoKomponenPeer::STATUS_MASUK);

		$criteria->addSelectColumn(BappekoKomponenPeer::RKA_MEMBER);

		$criteria->addSelectColumn(BappekoKomponenPeer::MAINTENANCE);

	}

	const COUNT = 'COUNT(ebudget.bappeko_komponen.KOMPONEN_ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.bappeko_komponen.KOMPONEN_ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(BappekoKomponenPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(BappekoKomponenPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = BappekoKomponenPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = BappekoKomponenPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return BappekoKomponenPeer::populateObjects(BappekoKomponenPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			BappekoKomponenPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = BappekoKomponenPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return BappekoKomponenPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(BappekoKomponenPeer::KOMPONEN_ID);
			$selectCriteria->add(BappekoKomponenPeer::KOMPONEN_ID, $criteria->remove(BappekoKomponenPeer::KOMPONEN_ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(BappekoKomponenPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(BappekoKomponenPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof BappekoKomponen) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(BappekoKomponenPeer::KOMPONEN_ID, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(BappekoKomponen $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(BappekoKomponenPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(BappekoKomponenPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(BappekoKomponenPeer::DATABASE_NAME, BappekoKomponenPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = BappekoKomponenPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(BappekoKomponenPeer::DATABASE_NAME);

		$criteria->add(BappekoKomponenPeer::KOMPONEN_ID, $pk);


		$v = BappekoKomponenPeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(BappekoKomponenPeer::KOMPONEN_ID, $pks, Criteria::IN);
			$objs = BappekoKomponenPeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BaseBappekoKomponenPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/BappekoKomponenMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.BappekoKomponenMapBuilder');
}
