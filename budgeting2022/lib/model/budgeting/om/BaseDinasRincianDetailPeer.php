<?php


abstract class BaseDinasRincianDetailPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.dinas_rincian_detail';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.DinasRincianDetail';

	
	const NUM_COLUMNS = 96;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const KEGIATAN_CODE = 'ebudget.dinas_rincian_detail.KEGIATAN_CODE';

	
	const TIPE = 'ebudget.dinas_rincian_detail.TIPE';

	
	const DETAIL_NO = 'ebudget.dinas_rincian_detail.DETAIL_NO';

	
	const REKENING_CODE = 'ebudget.dinas_rincian_detail.REKENING_CODE';

	
	const KOMPONEN_ID = 'ebudget.dinas_rincian_detail.KOMPONEN_ID';

	
	const DETAIL_NAME = 'ebudget.dinas_rincian_detail.DETAIL_NAME';

	
	const VOLUME = 'ebudget.dinas_rincian_detail.VOLUME';

	
	const KETERANGAN_KOEFISIEN = 'ebudget.dinas_rincian_detail.KETERANGAN_KOEFISIEN';

	
	const SUBTITLE = 'ebudget.dinas_rincian_detail.SUBTITLE';

	
	const KOMPONEN_HARGA = 'ebudget.dinas_rincian_detail.KOMPONEN_HARGA';

	
	const KOMPONEN_HARGA_AWAL = 'ebudget.dinas_rincian_detail.KOMPONEN_HARGA_AWAL';

	
	const KOMPONEN_NAME = 'ebudget.dinas_rincian_detail.KOMPONEN_NAME';

	
	const SATUAN = 'ebudget.dinas_rincian_detail.SATUAN';

	
	const PAJAK = 'ebudget.dinas_rincian_detail.PAJAK';

	
	const UNIT_ID = 'ebudget.dinas_rincian_detail.UNIT_ID';

	
	const FROM_SUB_KEGIATAN = 'ebudget.dinas_rincian_detail.FROM_SUB_KEGIATAN';

	
	const SUB = 'ebudget.dinas_rincian_detail.SUB';

	
	const KODE_SUB = 'ebudget.dinas_rincian_detail.KODE_SUB';

	
	const LAST_UPDATE_USER = 'ebudget.dinas_rincian_detail.LAST_UPDATE_USER';

	
	const LAST_UPDATE_TIME = 'ebudget.dinas_rincian_detail.LAST_UPDATE_TIME';

	
	const LAST_UPDATE_IP = 'ebudget.dinas_rincian_detail.LAST_UPDATE_IP';

	
	const TAHAP = 'ebudget.dinas_rincian_detail.TAHAP';

	
	const TAHAP_EDIT = 'ebudget.dinas_rincian_detail.TAHAP_EDIT';

	
	const TAHAP_NEW = 'ebudget.dinas_rincian_detail.TAHAP_NEW';

	
	const STATUS_LELANG = 'ebudget.dinas_rincian_detail.STATUS_LELANG';

	
	const NOMOR_LELANG = 'ebudget.dinas_rincian_detail.NOMOR_LELANG';

	
	const KOEFISIEN_SEMULA = 'ebudget.dinas_rincian_detail.KOEFISIEN_SEMULA';

	
	const VOLUME_SEMULA = 'ebudget.dinas_rincian_detail.VOLUME_SEMULA';

	
	const HARGA_SEMULA = 'ebudget.dinas_rincian_detail.HARGA_SEMULA';

	
	const TOTAL_SEMULA = 'ebudget.dinas_rincian_detail.TOTAL_SEMULA';

	
	const LOCK_SUBTITLE = 'ebudget.dinas_rincian_detail.LOCK_SUBTITLE';

	
	const STATUS_HAPUS = 'ebudget.dinas_rincian_detail.STATUS_HAPUS';

	
	const TAHUN = 'ebudget.dinas_rincian_detail.TAHUN';

	
	const KODE_LOKASI = 'ebudget.dinas_rincian_detail.KODE_LOKASI';

	
	const KECAMATAN = 'ebudget.dinas_rincian_detail.KECAMATAN';

	
	const REKENING_CODE_ASLI = 'ebudget.dinas_rincian_detail.REKENING_CODE_ASLI';

	
	const NOTE_SKPD = 'ebudget.dinas_rincian_detail.NOTE_SKPD';

	
	const NOTE_PENELITI = 'ebudget.dinas_rincian_detail.NOTE_PENELITI';

	
	const NILAI_ANGGARAN = 'ebudget.dinas_rincian_detail.NILAI_ANGGARAN';

	
	const IS_BLUD = 'ebudget.dinas_rincian_detail.IS_BLUD';

	
	const IS_KAPITASI_BPJS = 'ebudget.dinas_rincian_detail.IS_KAPITASI_BPJS';

	
	const LOKASI_KECAMATAN = 'ebudget.dinas_rincian_detail.LOKASI_KECAMATAN';

	
	const LOKASI_KELURAHAN = 'ebudget.dinas_rincian_detail.LOKASI_KELURAHAN';

	
	const OB = 'ebudget.dinas_rincian_detail.OB';

	
	const OB_FROM_ID = 'ebudget.dinas_rincian_detail.OB_FROM_ID';

	
	const IS_PER_KOMPONEN = 'ebudget.dinas_rincian_detail.IS_PER_KOMPONEN';

	
	const KEGIATAN_CODE_ASAL = 'ebudget.dinas_rincian_detail.KEGIATAN_CODE_ASAL';

	
	const TH_KE_MULTIYEARS = 'ebudget.dinas_rincian_detail.TH_KE_MULTIYEARS';

	
	const HARGA_SEBELUM_SISA_LELANG = 'ebudget.dinas_rincian_detail.HARGA_SEBELUM_SISA_LELANG';

	
	const IS_MUSRENBANG = 'ebudget.dinas_rincian_detail.IS_MUSRENBANG';

	
	const SUB_ID_ASAL = 'ebudget.dinas_rincian_detail.SUB_ID_ASAL';

	
	const SUBTITLE_ASAL = 'ebudget.dinas_rincian_detail.SUBTITLE_ASAL';

	
	const KODE_SUB_ASAL = 'ebudget.dinas_rincian_detail.KODE_SUB_ASAL';

	
	const SUB_ASAL = 'ebudget.dinas_rincian_detail.SUB_ASAL';

	
	const LAST_EDIT_TIME = 'ebudget.dinas_rincian_detail.LAST_EDIT_TIME';

	
	const IS_POTONG_BPJS = 'ebudget.dinas_rincian_detail.IS_POTONG_BPJS';

	
	const IS_IURAN_BPJS = 'ebudget.dinas_rincian_detail.IS_IURAN_BPJS';

	
	const STATUS_OB = 'ebudget.dinas_rincian_detail.STATUS_OB';

	
	const OB_PARENT = 'ebudget.dinas_rincian_detail.OB_PARENT';

	
	const OB_ALOKASI_BARU = 'ebudget.dinas_rincian_detail.OB_ALOKASI_BARU';

	
	const IS_HIBAH = 'ebudget.dinas_rincian_detail.IS_HIBAH';

	
	const STATUS_LEVEL = 'ebudget.dinas_rincian_detail.STATUS_LEVEL';

	
	const STATUS_LEVEL_TOLAK = 'ebudget.dinas_rincian_detail.STATUS_LEVEL_TOLAK';

	
	const STATUS_SISIPAN = 'ebudget.dinas_rincian_detail.STATUS_SISIPAN';

	
	const IS_TAPD_SETUJU = 'ebudget.dinas_rincian_detail.IS_TAPD_SETUJU';

	
	const IS_BAPPEKO_SETUJU = 'ebudget.dinas_rincian_detail.IS_BAPPEKO_SETUJU';

	
	const AKRUAL_CODE = 'ebudget.dinas_rincian_detail.AKRUAL_CODE';

	
	const TIPE2 = 'ebudget.dinas_rincian_detail.TIPE2';

	
	const IS_PENYELIA_SETUJU = 'ebudget.dinas_rincian_detail.IS_PENYELIA_SETUJU';

	
	const NOTE_TAPD = 'ebudget.dinas_rincian_detail.NOTE_TAPD';

	
	const NOTE_BAPPEKO = 'ebudget.dinas_rincian_detail.NOTE_BAPPEKO';

	
	const SATUAN_SEMULA = 'ebudget.dinas_rincian_detail.SATUAN_SEMULA';

	
	const ID_LOKASI = 'ebudget.dinas_rincian_detail.ID_LOKASI';

	
	const DETAIL_KEGIATAN = 'ebudget.dinas_rincian_detail.DETAIL_KEGIATAN';

	
	const DETAIL_KEGIATAN_SEMULA = 'ebudget.dinas_rincian_detail.DETAIL_KEGIATAN_SEMULA';

	
	const STATUS_KOMPONEN_BARU = 'ebudget.dinas_rincian_detail.STATUS_KOMPONEN_BARU';

	
	const STATUS_KOMPONEN_BERUBAH = 'ebudget.dinas_rincian_detail.STATUS_KOMPONEN_BERUBAH';

	
	const APPROVE_UNLOCK_HARGA = 'ebudget.dinas_rincian_detail.APPROVE_UNLOCK_HARGA';

	
	const TIPE_LELANG = 'ebudget.dinas_rincian_detail.TIPE_LELANG';

	
	const IS_HPSP = 'ebudget.dinas_rincian_detail.IS_HPSP';

	
	const IS_DAK = 'ebudget.dinas_rincian_detail.IS_DAK';

	
	const IS_BOS = 'ebudget.dinas_rincian_detail.IS_BOS';

	
	const IS_BOBDA = 'ebudget.dinas_rincian_detail.IS_BOBDA';

	
	const IS_NARSUM = 'ebudget.dinas_rincian_detail.IS_NARSUM';

	
	const IS_BAGIAN_HUKUM_SETUJU = 'ebudget.dinas_rincian_detail.IS_BAGIAN_HUKUM_SETUJU';

	
	const IS_INSPEKTORAT_SETUJU = 'ebudget.dinas_rincian_detail.IS_INSPEKTORAT_SETUJU';

	
	const IS_BADAN_KEPEGAWAIAN_SETUJU = 'ebudget.dinas_rincian_detail.IS_BADAN_KEPEGAWAIAN_SETUJU';

	
	const IS_LPPA_SETUJU = 'ebudget.dinas_rincian_detail.IS_LPPA_SETUJU';

	
	const PRIORITAS_WALI = 'ebudget.dinas_rincian_detail.PRIORITAS_WALI';

	
	const IS_OUTPUT = 'ebudget.dinas_rincian_detail.IS_OUTPUT';

	
	const IS_BAGIAN_ORGANISASI_SETUJU = 'ebudget.dinas_rincian_detail.IS_BAGIAN_ORGANISASI_SETUJU';

	
	const NOTE_KOEFISIEN = 'ebudget.dinas_rincian_detail.NOTE_KOEFISIEN';

	
	const IS_ASISTEN1_SETUJU = 'ebudget.dinas_rincian_detail.IS_ASISTEN1_SETUJU';

	
	const IS_ASISTEN2_SETUJU = 'ebudget.dinas_rincian_detail.IS_ASISTEN2_SETUJU';

	
	const IS_ASISTEN3_SETUJU = 'ebudget.dinas_rincian_detail.IS_ASISTEN3_SETUJU';

	
	const IS_SEKDA_SETUJU = 'ebudget.dinas_rincian_detail.IS_SEKDA_SETUJU';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('KegiatanCode', 'Tipe', 'DetailNo', 'RekeningCode', 'KomponenId', 'DetailName', 'Volume', 'KeteranganKoefisien', 'Subtitle', 'KomponenHarga', 'KomponenHargaAwal', 'KomponenName', 'Satuan', 'Pajak', 'UnitId', 'FromSubKegiatan', 'Sub', 'KodeSub', 'LastUpdateUser', 'LastUpdateTime', 'LastUpdateIp', 'Tahap', 'TahapEdit', 'TahapNew', 'StatusLelang', 'NomorLelang', 'KoefisienSemula', 'VolumeSemula', 'HargaSemula', 'TotalSemula', 'LockSubtitle', 'StatusHapus', 'Tahun', 'KodeLokasi', 'Kecamatan', 'RekeningCodeAsli', 'NoteSkpd', 'NotePeneliti', 'NilaiAnggaran', 'IsBlud', 'IsKapitasiBpjs', 'LokasiKecamatan', 'LokasiKelurahan', 'Ob', 'ObFromId', 'IsPerKomponen', 'KegiatanCodeAsal', 'ThKeMultiyears', 'HargaSebelumSisaLelang', 'IsMusrenbang', 'SubIdAsal', 'SubtitleAsal', 'KodeSubAsal', 'SubAsal', 'LastEditTime', 'IsPotongBpjs', 'IsIuranBpjs', 'StatusOb', 'ObParent', 'ObAlokasiBaru', 'IsHibah', 'StatusLevel', 'StatusLevelTolak', 'StatusSisipan', 'IsTapdSetuju', 'IsBappekoSetuju', 'AkrualCode', 'Tipe2', 'IsPenyeliaSetuju', 'NoteTapd', 'NoteBappeko', 'SatuanSemula', 'IdLokasi', 'DetailKegiatan', 'DetailKegiatanSemula', 'StatusKomponenBaru', 'StatusKomponenBerubah', 'ApproveUnlockHarga', 'TipeLelang', 'IsHpsp', 'IsDak', 'IsBos', 'IsBobda', 'IsNarsum', 'IsBagianHukumSetuju', 'IsInspektoratSetuju', 'IsBadanKepegawaianSetuju', 'IsLppaSetuju', 'PrioritasWali', 'IsOutput', 'IsBagianOrganisasiSetuju', 'NoteKoefisien', 'IsAsisten1Setuju', 'IsAsisten2Setuju', 'IsAsisten3Setuju', 'IsSekdaSetuju', ),
		BasePeer::TYPE_COLNAME => array (DinasRincianDetailPeer::KEGIATAN_CODE, DinasRincianDetailPeer::TIPE, DinasRincianDetailPeer::DETAIL_NO, DinasRincianDetailPeer::REKENING_CODE, DinasRincianDetailPeer::KOMPONEN_ID, DinasRincianDetailPeer::DETAIL_NAME, DinasRincianDetailPeer::VOLUME, DinasRincianDetailPeer::KETERANGAN_KOEFISIEN, DinasRincianDetailPeer::SUBTITLE, DinasRincianDetailPeer::KOMPONEN_HARGA, DinasRincianDetailPeer::KOMPONEN_HARGA_AWAL, DinasRincianDetailPeer::KOMPONEN_NAME, DinasRincianDetailPeer::SATUAN, DinasRincianDetailPeer::PAJAK, DinasRincianDetailPeer::UNIT_ID, DinasRincianDetailPeer::FROM_SUB_KEGIATAN, DinasRincianDetailPeer::SUB, DinasRincianDetailPeer::KODE_SUB, DinasRincianDetailPeer::LAST_UPDATE_USER, DinasRincianDetailPeer::LAST_UPDATE_TIME, DinasRincianDetailPeer::LAST_UPDATE_IP, DinasRincianDetailPeer::TAHAP, DinasRincianDetailPeer::TAHAP_EDIT, DinasRincianDetailPeer::TAHAP_NEW, DinasRincianDetailPeer::STATUS_LELANG, DinasRincianDetailPeer::NOMOR_LELANG, DinasRincianDetailPeer::KOEFISIEN_SEMULA, DinasRincianDetailPeer::VOLUME_SEMULA, DinasRincianDetailPeer::HARGA_SEMULA, DinasRincianDetailPeer::TOTAL_SEMULA, DinasRincianDetailPeer::LOCK_SUBTITLE, DinasRincianDetailPeer::STATUS_HAPUS, DinasRincianDetailPeer::TAHUN, DinasRincianDetailPeer::KODE_LOKASI, DinasRincianDetailPeer::KECAMATAN, DinasRincianDetailPeer::REKENING_CODE_ASLI, DinasRincianDetailPeer::NOTE_SKPD, DinasRincianDetailPeer::NOTE_PENELITI, DinasRincianDetailPeer::NILAI_ANGGARAN, DinasRincianDetailPeer::IS_BLUD, DinasRincianDetailPeer::IS_KAPITASI_BPJS, DinasRincianDetailPeer::LOKASI_KECAMATAN, DinasRincianDetailPeer::LOKASI_KELURAHAN, DinasRincianDetailPeer::OB, DinasRincianDetailPeer::OB_FROM_ID, DinasRincianDetailPeer::IS_PER_KOMPONEN, DinasRincianDetailPeer::KEGIATAN_CODE_ASAL, DinasRincianDetailPeer::TH_KE_MULTIYEARS, DinasRincianDetailPeer::HARGA_SEBELUM_SISA_LELANG, DinasRincianDetailPeer::IS_MUSRENBANG, DinasRincianDetailPeer::SUB_ID_ASAL, DinasRincianDetailPeer::SUBTITLE_ASAL, DinasRincianDetailPeer::KODE_SUB_ASAL, DinasRincianDetailPeer::SUB_ASAL, DinasRincianDetailPeer::LAST_EDIT_TIME, DinasRincianDetailPeer::IS_POTONG_BPJS, DinasRincianDetailPeer::IS_IURAN_BPJS, DinasRincianDetailPeer::STATUS_OB, DinasRincianDetailPeer::OB_PARENT, DinasRincianDetailPeer::OB_ALOKASI_BARU, DinasRincianDetailPeer::IS_HIBAH, DinasRincianDetailPeer::STATUS_LEVEL, DinasRincianDetailPeer::STATUS_LEVEL_TOLAK, DinasRincianDetailPeer::STATUS_SISIPAN, DinasRincianDetailPeer::IS_TAPD_SETUJU, DinasRincianDetailPeer::IS_BAPPEKO_SETUJU, DinasRincianDetailPeer::AKRUAL_CODE, DinasRincianDetailPeer::TIPE2, DinasRincianDetailPeer::IS_PENYELIA_SETUJU, DinasRincianDetailPeer::NOTE_TAPD, DinasRincianDetailPeer::NOTE_BAPPEKO, DinasRincianDetailPeer::SATUAN_SEMULA, DinasRincianDetailPeer::ID_LOKASI, DinasRincianDetailPeer::DETAIL_KEGIATAN, DinasRincianDetailPeer::DETAIL_KEGIATAN_SEMULA, DinasRincianDetailPeer::STATUS_KOMPONEN_BARU, DinasRincianDetailPeer::STATUS_KOMPONEN_BERUBAH, DinasRincianDetailPeer::APPROVE_UNLOCK_HARGA, DinasRincianDetailPeer::TIPE_LELANG, DinasRincianDetailPeer::IS_HPSP, DinasRincianDetailPeer::IS_DAK, DinasRincianDetailPeer::IS_BOS, DinasRincianDetailPeer::IS_BOBDA, DinasRincianDetailPeer::IS_NARSUM, DinasRincianDetailPeer::IS_BAGIAN_HUKUM_SETUJU, DinasRincianDetailPeer::IS_INSPEKTORAT_SETUJU, DinasRincianDetailPeer::IS_BADAN_KEPEGAWAIAN_SETUJU, DinasRincianDetailPeer::IS_LPPA_SETUJU, DinasRincianDetailPeer::PRIORITAS_WALI, DinasRincianDetailPeer::IS_OUTPUT, DinasRincianDetailPeer::IS_BAGIAN_ORGANISASI_SETUJU, DinasRincianDetailPeer::NOTE_KOEFISIEN, DinasRincianDetailPeer::IS_ASISTEN1_SETUJU, DinasRincianDetailPeer::IS_ASISTEN2_SETUJU, DinasRincianDetailPeer::IS_ASISTEN3_SETUJU, DinasRincianDetailPeer::IS_SEKDA_SETUJU, ),
		BasePeer::TYPE_FIELDNAME => array ('kegiatan_code', 'tipe', 'detail_no', 'rekening_code', 'komponen_id', 'detail_name', 'volume', 'keterangan_koefisien', 'subtitle', 'komponen_harga', 'komponen_harga_awal', 'komponen_name', 'satuan', 'pajak', 'unit_id', 'from_sub_kegiatan', 'sub', 'kode_sub', 'last_update_user', 'last_update_time', 'last_update_ip', 'tahap', 'tahap_edit', 'tahap_new', 'status_lelang', 'nomor_lelang', 'koefisien_semula', 'volume_semula', 'harga_semula', 'total_semula', 'lock_subtitle', 'status_hapus', 'tahun', 'kode_lokasi', 'kecamatan', 'rekening_code_asli', 'note_skpd', 'note_peneliti', 'nilai_anggaran', 'is_blud', 'is_kapitasi_bpjs', 'lokasi_kecamatan', 'lokasi_kelurahan', 'ob', 'ob_from_id', 'is_per_komponen', 'kegiatan_code_asal', 'th_ke_multiyears', 'harga_sebelum_sisa_lelang', 'is_musrenbang', 'sub_id_asal', 'subtitle_asal', 'kode_sub_asal', 'sub_asal', 'last_edit_time', 'is_potong_bpjs', 'is_iuran_bpjs', 'status_ob', 'ob_parent', 'ob_alokasi_baru', 'is_hibah', 'status_level', 'status_level_tolak', 'status_sisipan', 'is_tapd_setuju', 'is_bappeko_setuju', 'akrual_code', 'tipe2', 'is_penyelia_setuju', 'note_tapd', 'note_bappeko', 'satuan_semula', 'id_lokasi', 'detail_kegiatan', 'detail_kegiatan_semula', 'status_komponen_baru', 'status_komponen_berubah', 'approve_unlock_harga', 'tipe_lelang', 'is_hpsp', 'is_dak', 'is_bos', 'is_bobda', 'is_narsum', 'is_bagian_hukum_setuju', 'is_inspektorat_setuju', 'is_badan_kepegawaian_setuju', 'is_lppa_setuju', 'prioritas_wali', 'is_output', 'is_bagian_organisasi_setuju', 'note_koefisien', 'is_asisten1_setuju', 'is_asisten2_setuju', 'is_asisten3_setuju', 'is_sekda_setuju', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('KegiatanCode' => 0, 'Tipe' => 1, 'DetailNo' => 2, 'RekeningCode' => 3, 'KomponenId' => 4, 'DetailName' => 5, 'Volume' => 6, 'KeteranganKoefisien' => 7, 'Subtitle' => 8, 'KomponenHarga' => 9, 'KomponenHargaAwal' => 10, 'KomponenName' => 11, 'Satuan' => 12, 'Pajak' => 13, 'UnitId' => 14, 'FromSubKegiatan' => 15, 'Sub' => 16, 'KodeSub' => 17, 'LastUpdateUser' => 18, 'LastUpdateTime' => 19, 'LastUpdateIp' => 20, 'Tahap' => 21, 'TahapEdit' => 22, 'TahapNew' => 23, 'StatusLelang' => 24, 'NomorLelang' => 25, 'KoefisienSemula' => 26, 'VolumeSemula' => 27, 'HargaSemula' => 28, 'TotalSemula' => 29, 'LockSubtitle' => 30, 'StatusHapus' => 31, 'Tahun' => 32, 'KodeLokasi' => 33, 'Kecamatan' => 34, 'RekeningCodeAsli' => 35, 'NoteSkpd' => 36, 'NotePeneliti' => 37, 'NilaiAnggaran' => 38, 'IsBlud' => 39, 'IsKapitasiBpjs' => 40, 'LokasiKecamatan' => 41, 'LokasiKelurahan' => 42, 'Ob' => 43, 'ObFromId' => 44, 'IsPerKomponen' => 45, 'KegiatanCodeAsal' => 46, 'ThKeMultiyears' => 47, 'HargaSebelumSisaLelang' => 48, 'IsMusrenbang' => 49, 'SubIdAsal' => 50, 'SubtitleAsal' => 51, 'KodeSubAsal' => 52, 'SubAsal' => 53, 'LastEditTime' => 54, 'IsPotongBpjs' => 55, 'IsIuranBpjs' => 56, 'StatusOb' => 57, 'ObParent' => 58, 'ObAlokasiBaru' => 59, 'IsHibah' => 60, 'StatusLevel' => 61, 'StatusLevelTolak' => 62, 'StatusSisipan' => 63, 'IsTapdSetuju' => 64, 'IsBappekoSetuju' => 65, 'AkrualCode' => 66, 'Tipe2' => 67, 'IsPenyeliaSetuju' => 68, 'NoteTapd' => 69, 'NoteBappeko' => 70, 'SatuanSemula' => 71, 'IdLokasi' => 72, 'DetailKegiatan' => 73, 'DetailKegiatanSemula' => 74, 'StatusKomponenBaru' => 75, 'StatusKomponenBerubah' => 76, 'ApproveUnlockHarga' => 77, 'TipeLelang' => 78, 'IsHpsp' => 79, 'IsDak' => 80, 'IsBos' => 81, 'IsBobda' => 82, 'IsNarsum' => 83, 'IsBagianHukumSetuju' => 84, 'IsInspektoratSetuju' => 85, 'IsBadanKepegawaianSetuju' => 86, 'IsLppaSetuju' => 87, 'PrioritasWali' => 88, 'IsOutput' => 89, 'IsBagianOrganisasiSetuju' => 90, 'NoteKoefisien' => 91, 'IsAsisten1Setuju' => 92, 'IsAsisten2Setuju' => 93, 'IsAsisten3Setuju' => 94, 'IsSekdaSetuju' => 95, ),
		BasePeer::TYPE_COLNAME => array (DinasRincianDetailPeer::KEGIATAN_CODE => 0, DinasRincianDetailPeer::TIPE => 1, DinasRincianDetailPeer::DETAIL_NO => 2, DinasRincianDetailPeer::REKENING_CODE => 3, DinasRincianDetailPeer::KOMPONEN_ID => 4, DinasRincianDetailPeer::DETAIL_NAME => 5, DinasRincianDetailPeer::VOLUME => 6, DinasRincianDetailPeer::KETERANGAN_KOEFISIEN => 7, DinasRincianDetailPeer::SUBTITLE => 8, DinasRincianDetailPeer::KOMPONEN_HARGA => 9, DinasRincianDetailPeer::KOMPONEN_HARGA_AWAL => 10, DinasRincianDetailPeer::KOMPONEN_NAME => 11, DinasRincianDetailPeer::SATUAN => 12, DinasRincianDetailPeer::PAJAK => 13, DinasRincianDetailPeer::UNIT_ID => 14, DinasRincianDetailPeer::FROM_SUB_KEGIATAN => 15, DinasRincianDetailPeer::SUB => 16, DinasRincianDetailPeer::KODE_SUB => 17, DinasRincianDetailPeer::LAST_UPDATE_USER => 18, DinasRincianDetailPeer::LAST_UPDATE_TIME => 19, DinasRincianDetailPeer::LAST_UPDATE_IP => 20, DinasRincianDetailPeer::TAHAP => 21, DinasRincianDetailPeer::TAHAP_EDIT => 22, DinasRincianDetailPeer::TAHAP_NEW => 23, DinasRincianDetailPeer::STATUS_LELANG => 24, DinasRincianDetailPeer::NOMOR_LELANG => 25, DinasRincianDetailPeer::KOEFISIEN_SEMULA => 26, DinasRincianDetailPeer::VOLUME_SEMULA => 27, DinasRincianDetailPeer::HARGA_SEMULA => 28, DinasRincianDetailPeer::TOTAL_SEMULA => 29, DinasRincianDetailPeer::LOCK_SUBTITLE => 30, DinasRincianDetailPeer::STATUS_HAPUS => 31, DinasRincianDetailPeer::TAHUN => 32, DinasRincianDetailPeer::KODE_LOKASI => 33, DinasRincianDetailPeer::KECAMATAN => 34, DinasRincianDetailPeer::REKENING_CODE_ASLI => 35, DinasRincianDetailPeer::NOTE_SKPD => 36, DinasRincianDetailPeer::NOTE_PENELITI => 37, DinasRincianDetailPeer::NILAI_ANGGARAN => 38, DinasRincianDetailPeer::IS_BLUD => 39, DinasRincianDetailPeer::IS_KAPITASI_BPJS => 40, DinasRincianDetailPeer::LOKASI_KECAMATAN => 41, DinasRincianDetailPeer::LOKASI_KELURAHAN => 42, DinasRincianDetailPeer::OB => 43, DinasRincianDetailPeer::OB_FROM_ID => 44, DinasRincianDetailPeer::IS_PER_KOMPONEN => 45, DinasRincianDetailPeer::KEGIATAN_CODE_ASAL => 46, DinasRincianDetailPeer::TH_KE_MULTIYEARS => 47, DinasRincianDetailPeer::HARGA_SEBELUM_SISA_LELANG => 48, DinasRincianDetailPeer::IS_MUSRENBANG => 49, DinasRincianDetailPeer::SUB_ID_ASAL => 50, DinasRincianDetailPeer::SUBTITLE_ASAL => 51, DinasRincianDetailPeer::KODE_SUB_ASAL => 52, DinasRincianDetailPeer::SUB_ASAL => 53, DinasRincianDetailPeer::LAST_EDIT_TIME => 54, DinasRincianDetailPeer::IS_POTONG_BPJS => 55, DinasRincianDetailPeer::IS_IURAN_BPJS => 56, DinasRincianDetailPeer::STATUS_OB => 57, DinasRincianDetailPeer::OB_PARENT => 58, DinasRincianDetailPeer::OB_ALOKASI_BARU => 59, DinasRincianDetailPeer::IS_HIBAH => 60, DinasRincianDetailPeer::STATUS_LEVEL => 61, DinasRincianDetailPeer::STATUS_LEVEL_TOLAK => 62, DinasRincianDetailPeer::STATUS_SISIPAN => 63, DinasRincianDetailPeer::IS_TAPD_SETUJU => 64, DinasRincianDetailPeer::IS_BAPPEKO_SETUJU => 65, DinasRincianDetailPeer::AKRUAL_CODE => 66, DinasRincianDetailPeer::TIPE2 => 67, DinasRincianDetailPeer::IS_PENYELIA_SETUJU => 68, DinasRincianDetailPeer::NOTE_TAPD => 69, DinasRincianDetailPeer::NOTE_BAPPEKO => 70, DinasRincianDetailPeer::SATUAN_SEMULA => 71, DinasRincianDetailPeer::ID_LOKASI => 72, DinasRincianDetailPeer::DETAIL_KEGIATAN => 73, DinasRincianDetailPeer::DETAIL_KEGIATAN_SEMULA => 74, DinasRincianDetailPeer::STATUS_KOMPONEN_BARU => 75, DinasRincianDetailPeer::STATUS_KOMPONEN_BERUBAH => 76, DinasRincianDetailPeer::APPROVE_UNLOCK_HARGA => 77, DinasRincianDetailPeer::TIPE_LELANG => 78, DinasRincianDetailPeer::IS_HPSP => 79, DinasRincianDetailPeer::IS_DAK => 80, DinasRincianDetailPeer::IS_BOS => 81, DinasRincianDetailPeer::IS_BOBDA => 82, DinasRincianDetailPeer::IS_NARSUM => 83, DinasRincianDetailPeer::IS_BAGIAN_HUKUM_SETUJU => 84, DinasRincianDetailPeer::IS_INSPEKTORAT_SETUJU => 85, DinasRincianDetailPeer::IS_BADAN_KEPEGAWAIAN_SETUJU => 86, DinasRincianDetailPeer::IS_LPPA_SETUJU => 87, DinasRincianDetailPeer::PRIORITAS_WALI => 88, DinasRincianDetailPeer::IS_OUTPUT => 89, DinasRincianDetailPeer::IS_BAGIAN_ORGANISASI_SETUJU => 90, DinasRincianDetailPeer::NOTE_KOEFISIEN => 91, DinasRincianDetailPeer::IS_ASISTEN1_SETUJU => 92, DinasRincianDetailPeer::IS_ASISTEN2_SETUJU => 93, DinasRincianDetailPeer::IS_ASISTEN3_SETUJU => 94, DinasRincianDetailPeer::IS_SEKDA_SETUJU => 95, ),
		BasePeer::TYPE_FIELDNAME => array ('kegiatan_code' => 0, 'tipe' => 1, 'detail_no' => 2, 'rekening_code' => 3, 'komponen_id' => 4, 'detail_name' => 5, 'volume' => 6, 'keterangan_koefisien' => 7, 'subtitle' => 8, 'komponen_harga' => 9, 'komponen_harga_awal' => 10, 'komponen_name' => 11, 'satuan' => 12, 'pajak' => 13, 'unit_id' => 14, 'from_sub_kegiatan' => 15, 'sub' => 16, 'kode_sub' => 17, 'last_update_user' => 18, 'last_update_time' => 19, 'last_update_ip' => 20, 'tahap' => 21, 'tahap_edit' => 22, 'tahap_new' => 23, 'status_lelang' => 24, 'nomor_lelang' => 25, 'koefisien_semula' => 26, 'volume_semula' => 27, 'harga_semula' => 28, 'total_semula' => 29, 'lock_subtitle' => 30, 'status_hapus' => 31, 'tahun' => 32, 'kode_lokasi' => 33, 'kecamatan' => 34, 'rekening_code_asli' => 35, 'note_skpd' => 36, 'note_peneliti' => 37, 'nilai_anggaran' => 38, 'is_blud' => 39, 'is_kapitasi_bpjs' => 40, 'lokasi_kecamatan' => 41, 'lokasi_kelurahan' => 42, 'ob' => 43, 'ob_from_id' => 44, 'is_per_komponen' => 45, 'kegiatan_code_asal' => 46, 'th_ke_multiyears' => 47, 'harga_sebelum_sisa_lelang' => 48, 'is_musrenbang' => 49, 'sub_id_asal' => 50, 'subtitle_asal' => 51, 'kode_sub_asal' => 52, 'sub_asal' => 53, 'last_edit_time' => 54, 'is_potong_bpjs' => 55, 'is_iuran_bpjs' => 56, 'status_ob' => 57, 'ob_parent' => 58, 'ob_alokasi_baru' => 59, 'is_hibah' => 60, 'status_level' => 61, 'status_level_tolak' => 62, 'status_sisipan' => 63, 'is_tapd_setuju' => 64, 'is_bappeko_setuju' => 65, 'akrual_code' => 66, 'tipe2' => 67, 'is_penyelia_setuju' => 68, 'note_tapd' => 69, 'note_bappeko' => 70, 'satuan_semula' => 71, 'id_lokasi' => 72, 'detail_kegiatan' => 73, 'detail_kegiatan_semula' => 74, 'status_komponen_baru' => 75, 'status_komponen_berubah' => 76, 'approve_unlock_harga' => 77, 'tipe_lelang' => 78, 'is_hpsp' => 79, 'is_dak' => 80, 'is_bos' => 81, 'is_bobda' => 82, 'is_narsum' => 83, 'is_bagian_hukum_setuju' => 84, 'is_inspektorat_setuju' => 85, 'is_badan_kepegawaian_setuju' => 86, 'is_lppa_setuju' => 87, 'prioritas_wali' => 88, 'is_output' => 89, 'is_bagian_organisasi_setuju' => 90, 'note_koefisien' => 91, 'is_asisten1_setuju' => 92, 'is_asisten2_setuju' => 93, 'is_asisten3_setuju' => 94, 'is_sekda_setuju' => 95, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/DinasRincianDetailMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.DinasRincianDetailMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = DinasRincianDetailPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(DinasRincianDetailPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(DinasRincianDetailPeer::KEGIATAN_CODE);

		$criteria->addSelectColumn(DinasRincianDetailPeer::TIPE);

		$criteria->addSelectColumn(DinasRincianDetailPeer::DETAIL_NO);

		$criteria->addSelectColumn(DinasRincianDetailPeer::REKENING_CODE);

		$criteria->addSelectColumn(DinasRincianDetailPeer::KOMPONEN_ID);

		$criteria->addSelectColumn(DinasRincianDetailPeer::DETAIL_NAME);

		$criteria->addSelectColumn(DinasRincianDetailPeer::VOLUME);

		$criteria->addSelectColumn(DinasRincianDetailPeer::KETERANGAN_KOEFISIEN);

		$criteria->addSelectColumn(DinasRincianDetailPeer::SUBTITLE);

		$criteria->addSelectColumn(DinasRincianDetailPeer::KOMPONEN_HARGA);

		$criteria->addSelectColumn(DinasRincianDetailPeer::KOMPONEN_HARGA_AWAL);

		$criteria->addSelectColumn(DinasRincianDetailPeer::KOMPONEN_NAME);

		$criteria->addSelectColumn(DinasRincianDetailPeer::SATUAN);

		$criteria->addSelectColumn(DinasRincianDetailPeer::PAJAK);

		$criteria->addSelectColumn(DinasRincianDetailPeer::UNIT_ID);

		$criteria->addSelectColumn(DinasRincianDetailPeer::FROM_SUB_KEGIATAN);

		$criteria->addSelectColumn(DinasRincianDetailPeer::SUB);

		$criteria->addSelectColumn(DinasRincianDetailPeer::KODE_SUB);

		$criteria->addSelectColumn(DinasRincianDetailPeer::LAST_UPDATE_USER);

		$criteria->addSelectColumn(DinasRincianDetailPeer::LAST_UPDATE_TIME);

		$criteria->addSelectColumn(DinasRincianDetailPeer::LAST_UPDATE_IP);

		$criteria->addSelectColumn(DinasRincianDetailPeer::TAHAP);

		$criteria->addSelectColumn(DinasRincianDetailPeer::TAHAP_EDIT);

		$criteria->addSelectColumn(DinasRincianDetailPeer::TAHAP_NEW);

		$criteria->addSelectColumn(DinasRincianDetailPeer::STATUS_LELANG);

		$criteria->addSelectColumn(DinasRincianDetailPeer::NOMOR_LELANG);

		$criteria->addSelectColumn(DinasRincianDetailPeer::KOEFISIEN_SEMULA);

		$criteria->addSelectColumn(DinasRincianDetailPeer::VOLUME_SEMULA);

		$criteria->addSelectColumn(DinasRincianDetailPeer::HARGA_SEMULA);

		$criteria->addSelectColumn(DinasRincianDetailPeer::TOTAL_SEMULA);

		$criteria->addSelectColumn(DinasRincianDetailPeer::LOCK_SUBTITLE);

		$criteria->addSelectColumn(DinasRincianDetailPeer::STATUS_HAPUS);

		$criteria->addSelectColumn(DinasRincianDetailPeer::TAHUN);

		$criteria->addSelectColumn(DinasRincianDetailPeer::KODE_LOKASI);

		$criteria->addSelectColumn(DinasRincianDetailPeer::KECAMATAN);

		$criteria->addSelectColumn(DinasRincianDetailPeer::REKENING_CODE_ASLI);

		$criteria->addSelectColumn(DinasRincianDetailPeer::NOTE_SKPD);

		$criteria->addSelectColumn(DinasRincianDetailPeer::NOTE_PENELITI);

		$criteria->addSelectColumn(DinasRincianDetailPeer::NILAI_ANGGARAN);

		$criteria->addSelectColumn(DinasRincianDetailPeer::IS_BLUD);

		$criteria->addSelectColumn(DinasRincianDetailPeer::IS_KAPITASI_BPJS);

		$criteria->addSelectColumn(DinasRincianDetailPeer::LOKASI_KECAMATAN);

		$criteria->addSelectColumn(DinasRincianDetailPeer::LOKASI_KELURAHAN);

		$criteria->addSelectColumn(DinasRincianDetailPeer::OB);

		$criteria->addSelectColumn(DinasRincianDetailPeer::OB_FROM_ID);

		$criteria->addSelectColumn(DinasRincianDetailPeer::IS_PER_KOMPONEN);

		$criteria->addSelectColumn(DinasRincianDetailPeer::KEGIATAN_CODE_ASAL);

		$criteria->addSelectColumn(DinasRincianDetailPeer::TH_KE_MULTIYEARS);

		$criteria->addSelectColumn(DinasRincianDetailPeer::HARGA_SEBELUM_SISA_LELANG);

		$criteria->addSelectColumn(DinasRincianDetailPeer::IS_MUSRENBANG);

		$criteria->addSelectColumn(DinasRincianDetailPeer::SUB_ID_ASAL);

		$criteria->addSelectColumn(DinasRincianDetailPeer::SUBTITLE_ASAL);

		$criteria->addSelectColumn(DinasRincianDetailPeer::KODE_SUB_ASAL);

		$criteria->addSelectColumn(DinasRincianDetailPeer::SUB_ASAL);

		$criteria->addSelectColumn(DinasRincianDetailPeer::LAST_EDIT_TIME);

		$criteria->addSelectColumn(DinasRincianDetailPeer::IS_POTONG_BPJS);

		$criteria->addSelectColumn(DinasRincianDetailPeer::IS_IURAN_BPJS);

		$criteria->addSelectColumn(DinasRincianDetailPeer::STATUS_OB);

		$criteria->addSelectColumn(DinasRincianDetailPeer::OB_PARENT);

		$criteria->addSelectColumn(DinasRincianDetailPeer::OB_ALOKASI_BARU);

		$criteria->addSelectColumn(DinasRincianDetailPeer::IS_HIBAH);

		$criteria->addSelectColumn(DinasRincianDetailPeer::STATUS_LEVEL);

		$criteria->addSelectColumn(DinasRincianDetailPeer::STATUS_LEVEL_TOLAK);

		$criteria->addSelectColumn(DinasRincianDetailPeer::STATUS_SISIPAN);

		$criteria->addSelectColumn(DinasRincianDetailPeer::IS_TAPD_SETUJU);

		$criteria->addSelectColumn(DinasRincianDetailPeer::IS_BAPPEKO_SETUJU);

		$criteria->addSelectColumn(DinasRincianDetailPeer::AKRUAL_CODE);

		$criteria->addSelectColumn(DinasRincianDetailPeer::TIPE2);

		$criteria->addSelectColumn(DinasRincianDetailPeer::IS_PENYELIA_SETUJU);

		$criteria->addSelectColumn(DinasRincianDetailPeer::NOTE_TAPD);

		$criteria->addSelectColumn(DinasRincianDetailPeer::NOTE_BAPPEKO);

		$criteria->addSelectColumn(DinasRincianDetailPeer::SATUAN_SEMULA);

		$criteria->addSelectColumn(DinasRincianDetailPeer::ID_LOKASI);

		$criteria->addSelectColumn(DinasRincianDetailPeer::DETAIL_KEGIATAN);

		$criteria->addSelectColumn(DinasRincianDetailPeer::DETAIL_KEGIATAN_SEMULA);

		$criteria->addSelectColumn(DinasRincianDetailPeer::STATUS_KOMPONEN_BARU);

		$criteria->addSelectColumn(DinasRincianDetailPeer::STATUS_KOMPONEN_BERUBAH);

		$criteria->addSelectColumn(DinasRincianDetailPeer::APPROVE_UNLOCK_HARGA);

		$criteria->addSelectColumn(DinasRincianDetailPeer::TIPE_LELANG);

		$criteria->addSelectColumn(DinasRincianDetailPeer::IS_HPSP);

		$criteria->addSelectColumn(DinasRincianDetailPeer::IS_DAK);

		$criteria->addSelectColumn(DinasRincianDetailPeer::IS_BOS);

		$criteria->addSelectColumn(DinasRincianDetailPeer::IS_BOBDA);

		$criteria->addSelectColumn(DinasRincianDetailPeer::IS_NARSUM);

		$criteria->addSelectColumn(DinasRincianDetailPeer::IS_BAGIAN_HUKUM_SETUJU);

		$criteria->addSelectColumn(DinasRincianDetailPeer::IS_INSPEKTORAT_SETUJU);

		$criteria->addSelectColumn(DinasRincianDetailPeer::IS_BADAN_KEPEGAWAIAN_SETUJU);

		$criteria->addSelectColumn(DinasRincianDetailPeer::IS_LPPA_SETUJU);

		$criteria->addSelectColumn(DinasRincianDetailPeer::PRIORITAS_WALI);

		$criteria->addSelectColumn(DinasRincianDetailPeer::IS_OUTPUT);

		$criteria->addSelectColumn(DinasRincianDetailPeer::IS_BAGIAN_ORGANISASI_SETUJU);

		$criteria->addSelectColumn(DinasRincianDetailPeer::NOTE_KOEFISIEN);

		$criteria->addSelectColumn(DinasRincianDetailPeer::IS_ASISTEN1_SETUJU);

		$criteria->addSelectColumn(DinasRincianDetailPeer::IS_ASISTEN2_SETUJU);

		$criteria->addSelectColumn(DinasRincianDetailPeer::IS_ASISTEN3_SETUJU);

		$criteria->addSelectColumn(DinasRincianDetailPeer::IS_SEKDA_SETUJU);

	}

	const COUNT = 'COUNT(ebudget.dinas_rincian_detail.KEGIATAN_CODE)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.dinas_rincian_detail.KEGIATAN_CODE)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(DinasRincianDetailPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(DinasRincianDetailPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = DinasRincianDetailPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = DinasRincianDetailPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return DinasRincianDetailPeer::populateObjects(DinasRincianDetailPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			DinasRincianDetailPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = DinasRincianDetailPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return DinasRincianDetailPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(DinasRincianDetailPeer::KEGIATAN_CODE);
			$selectCriteria->add(DinasRincianDetailPeer::KEGIATAN_CODE, $criteria->remove(DinasRincianDetailPeer::KEGIATAN_CODE), $comparison);

			$comparison = $criteria->getComparison(DinasRincianDetailPeer::DETAIL_NO);
			$selectCriteria->add(DinasRincianDetailPeer::DETAIL_NO, $criteria->remove(DinasRincianDetailPeer::DETAIL_NO), $comparison);

			$comparison = $criteria->getComparison(DinasRincianDetailPeer::UNIT_ID);
			$selectCriteria->add(DinasRincianDetailPeer::UNIT_ID, $criteria->remove(DinasRincianDetailPeer::UNIT_ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(DinasRincianDetailPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(DinasRincianDetailPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof DinasRincianDetail) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
												if(count($values) == count($values, COUNT_RECURSIVE))
			{
								$values = array($values);
			}
			$vals = array();
			foreach($values as $value)
			{

				$vals[0][] = $value[0];
				$vals[1][] = $value[1];
				$vals[2][] = $value[2];
			}

			$criteria->add(DinasRincianDetailPeer::KEGIATAN_CODE, $vals[0], Criteria::IN);
			$criteria->add(DinasRincianDetailPeer::DETAIL_NO, $vals[1], Criteria::IN);
			$criteria->add(DinasRincianDetailPeer::UNIT_ID, $vals[2], Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(DinasRincianDetail $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(DinasRincianDetailPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(DinasRincianDetailPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(DinasRincianDetailPeer::DATABASE_NAME, DinasRincianDetailPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = DinasRincianDetailPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK( $kegiatan_code, $detail_no, $unit_id, $con = null) {
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$criteria = new Criteria();
		$criteria->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
		$criteria->add(DinasRincianDetailPeer::DETAIL_NO, $detail_no);
		$criteria->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
		$v = DinasRincianDetailPeer::doSelect($criteria, $con);

		return !empty($v) ? $v[0] : null;
	}
} 
if (Propel::isInit()) {
			try {
		BaseDinasRincianDetailPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/DinasRincianDetailMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.DinasRincianDetailMapBuilder');
}
