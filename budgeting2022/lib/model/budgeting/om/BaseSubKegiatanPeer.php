<?php


abstract class BaseSubKegiatanPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.sub_kegiatan';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.SubKegiatan';

	
	const NUM_COLUMNS = 12;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const SUB_KEGIATAN_ID = 'ebudget.sub_kegiatan.SUB_KEGIATAN_ID';

	
	const SUB_KEGIATAN_NAME = 'ebudget.sub_kegiatan.SUB_KEGIATAN_NAME';

	
	const PARAM = 'ebudget.sub_kegiatan.PARAM';

	
	const SATUAN = 'ebudget.sub_kegiatan.SATUAN';

	
	const STATUS = 'ebudget.sub_kegiatan.STATUS';

	
	const PEMBAGI = 'ebudget.sub_kegiatan.PEMBAGI';

	
	const KETERANGAN = 'ebudget.sub_kegiatan.KETERANGAN';

	
	const UNIT_ID = 'ebudget.sub_kegiatan.UNIT_ID';

	
	const PENELITIAN_SKALA = 'ebudget.sub_kegiatan.PENELITIAN_SKALA';

	
	const PENELITIAN_WAKTU = 'ebudget.sub_kegiatan.PENELITIAN_WAKTU';

	
	const BATAS_NILAI = 'ebudget.sub_kegiatan.BATAS_NILAI';

	
	const TENAGA_AHLI = 'ebudget.sub_kegiatan.TENAGA_AHLI';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('SubKegiatanId', 'SubKegiatanName', 'Param', 'Satuan', 'Status', 'Pembagi', 'Keterangan', 'UnitId', 'PenelitianSkala', 'PenelitianWaktu', 'BatasNilai', 'TenagaAhli', ),
		BasePeer::TYPE_COLNAME => array (SubKegiatanPeer::SUB_KEGIATAN_ID, SubKegiatanPeer::SUB_KEGIATAN_NAME, SubKegiatanPeer::PARAM, SubKegiatanPeer::SATUAN, SubKegiatanPeer::STATUS, SubKegiatanPeer::PEMBAGI, SubKegiatanPeer::KETERANGAN, SubKegiatanPeer::UNIT_ID, SubKegiatanPeer::PENELITIAN_SKALA, SubKegiatanPeer::PENELITIAN_WAKTU, SubKegiatanPeer::BATAS_NILAI, SubKegiatanPeer::TENAGA_AHLI, ),
		BasePeer::TYPE_FIELDNAME => array ('sub_kegiatan_id', 'sub_kegiatan_name', 'param', 'satuan', 'status', 'pembagi', 'keterangan', 'unit_id', 'penelitian_skala', 'penelitian_waktu', 'batas_nilai', 'tenaga_ahli', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('SubKegiatanId' => 0, 'SubKegiatanName' => 1, 'Param' => 2, 'Satuan' => 3, 'Status' => 4, 'Pembagi' => 5, 'Keterangan' => 6, 'UnitId' => 7, 'PenelitianSkala' => 8, 'PenelitianWaktu' => 9, 'BatasNilai' => 10, 'TenagaAhli' => 11, ),
		BasePeer::TYPE_COLNAME => array (SubKegiatanPeer::SUB_KEGIATAN_ID => 0, SubKegiatanPeer::SUB_KEGIATAN_NAME => 1, SubKegiatanPeer::PARAM => 2, SubKegiatanPeer::SATUAN => 3, SubKegiatanPeer::STATUS => 4, SubKegiatanPeer::PEMBAGI => 5, SubKegiatanPeer::KETERANGAN => 6, SubKegiatanPeer::UNIT_ID => 7, SubKegiatanPeer::PENELITIAN_SKALA => 8, SubKegiatanPeer::PENELITIAN_WAKTU => 9, SubKegiatanPeer::BATAS_NILAI => 10, SubKegiatanPeer::TENAGA_AHLI => 11, ),
		BasePeer::TYPE_FIELDNAME => array ('sub_kegiatan_id' => 0, 'sub_kegiatan_name' => 1, 'param' => 2, 'satuan' => 3, 'status' => 4, 'pembagi' => 5, 'keterangan' => 6, 'unit_id' => 7, 'penelitian_skala' => 8, 'penelitian_waktu' => 9, 'batas_nilai' => 10, 'tenaga_ahli' => 11, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/SubKegiatanMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.SubKegiatanMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = SubKegiatanPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(SubKegiatanPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(SubKegiatanPeer::SUB_KEGIATAN_ID);

		$criteria->addSelectColumn(SubKegiatanPeer::SUB_KEGIATAN_NAME);

		$criteria->addSelectColumn(SubKegiatanPeer::PARAM);

		$criteria->addSelectColumn(SubKegiatanPeer::SATUAN);

		$criteria->addSelectColumn(SubKegiatanPeer::STATUS);

		$criteria->addSelectColumn(SubKegiatanPeer::PEMBAGI);

		$criteria->addSelectColumn(SubKegiatanPeer::KETERANGAN);

		$criteria->addSelectColumn(SubKegiatanPeer::UNIT_ID);

		$criteria->addSelectColumn(SubKegiatanPeer::PENELITIAN_SKALA);

		$criteria->addSelectColumn(SubKegiatanPeer::PENELITIAN_WAKTU);

		$criteria->addSelectColumn(SubKegiatanPeer::BATAS_NILAI);

		$criteria->addSelectColumn(SubKegiatanPeer::TENAGA_AHLI);

	}

	const COUNT = 'COUNT(ebudget.sub_kegiatan.SUB_KEGIATAN_ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.sub_kegiatan.SUB_KEGIATAN_ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(SubKegiatanPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(SubKegiatanPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = SubKegiatanPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = SubKegiatanPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return SubKegiatanPeer::populateObjects(SubKegiatanPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			SubKegiatanPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = SubKegiatanPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return SubKegiatanPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(SubKegiatanPeer::SUB_KEGIATAN_ID);
			$selectCriteria->add(SubKegiatanPeer::SUB_KEGIATAN_ID, $criteria->remove(SubKegiatanPeer::SUB_KEGIATAN_ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(SubKegiatanPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(SubKegiatanPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof SubKegiatan) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(SubKegiatanPeer::SUB_KEGIATAN_ID, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(SubKegiatan $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(SubKegiatanPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(SubKegiatanPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(SubKegiatanPeer::DATABASE_NAME, SubKegiatanPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = SubKegiatanPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(SubKegiatanPeer::DATABASE_NAME);

		$criteria->add(SubKegiatanPeer::SUB_KEGIATAN_ID, $pk);


		$v = SubKegiatanPeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(SubKegiatanPeer::SUB_KEGIATAN_ID, $pks, Criteria::IN);
			$objs = SubKegiatanPeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BaseSubKegiatanPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/SubKegiatanMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.SubKegiatanMapBuilder');
}
