<?php


abstract class BaseKelompokDinas extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $kelompok_id;


	
	protected $kelompok_name;


	
	protected $front_code;

	
	protected $collUnitKerjas;

	
	protected $lastUnitKerjaCriteria = null;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getKelompokId()
	{

		return $this->kelompok_id;
	}

	
	public function getKelompokName()
	{

		return $this->kelompok_name;
	}

	
	public function getFrontCode()
	{

		return $this->front_code;
	}

	
	public function setKelompokId($v)
	{

		if ($this->kelompok_id !== $v) {
			$this->kelompok_id = $v;
			$this->modifiedColumns[] = KelompokDinasPeer::KELOMPOK_ID;
		}

	} 
	
	public function setKelompokName($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kelompok_name !== $v) {
			$this->kelompok_name = $v;
			$this->modifiedColumns[] = KelompokDinasPeer::KELOMPOK_NAME;
		}

	} 
	
	public function setFrontCode($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->front_code !== $v) {
			$this->front_code = $v;
			$this->modifiedColumns[] = KelompokDinasPeer::FRONT_CODE;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->kelompok_id = $rs->getFloat($startcol + 0);

			$this->kelompok_name = $rs->getString($startcol + 1);

			$this->front_code = $rs->getString($startcol + 2);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 3; 
		} catch (Exception $e) {
			throw new PropelException("Error populating KelompokDinas object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(KelompokDinasPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			KelompokDinasPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(KelompokDinasPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = KelompokDinasPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += KelompokDinasPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			if ($this->collUnitKerjas !== null) {
				foreach($this->collUnitKerjas as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = KelompokDinasPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}


				if ($this->collUnitKerjas !== null) {
					foreach($this->collUnitKerjas as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}


			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = KelompokDinasPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getKelompokId();
				break;
			case 1:
				return $this->getKelompokName();
				break;
			case 2:
				return $this->getFrontCode();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = KelompokDinasPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getKelompokId(),
			$keys[1] => $this->getKelompokName(),
			$keys[2] => $this->getFrontCode(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = KelompokDinasPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setKelompokId($value);
				break;
			case 1:
				$this->setKelompokName($value);
				break;
			case 2:
				$this->setFrontCode($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = KelompokDinasPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setKelompokId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setKelompokName($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setFrontCode($arr[$keys[2]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(KelompokDinasPeer::DATABASE_NAME);

		if ($this->isColumnModified(KelompokDinasPeer::KELOMPOK_ID)) $criteria->add(KelompokDinasPeer::KELOMPOK_ID, $this->kelompok_id);
		if ($this->isColumnModified(KelompokDinasPeer::KELOMPOK_NAME)) $criteria->add(KelompokDinasPeer::KELOMPOK_NAME, $this->kelompok_name);
		if ($this->isColumnModified(KelompokDinasPeer::FRONT_CODE)) $criteria->add(KelompokDinasPeer::FRONT_CODE, $this->front_code);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(KelompokDinasPeer::DATABASE_NAME);

		$criteria->add(KelompokDinasPeer::KELOMPOK_ID, $this->kelompok_id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getKelompokId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setKelompokId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setKelompokName($this->kelompok_name);

		$copyObj->setFrontCode($this->front_code);


		if ($deepCopy) {
									$copyObj->setNew(false);

			foreach($this->getUnitKerjas() as $relObj) {
				$copyObj->addUnitKerja($relObj->copy($deepCopy));
			}

		} 

		$copyObj->setNew(true);

		$copyObj->setKelompokId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new KelompokDinasPeer();
		}
		return self::$peer;
	}

	
	public function initUnitKerjas()
	{
		if ($this->collUnitKerjas === null) {
			$this->collUnitKerjas = array();
		}
	}

	
	public function getUnitKerjas($criteria = null, $con = null)
	{
				include_once 'lib/model/budgeting/om/BaseUnitKerjaPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collUnitKerjas === null) {
			if ($this->isNew()) {
			   $this->collUnitKerjas = array();
			} else {

				$criteria->add(UnitKerjaPeer::KELOMPOK_ID, $this->getKelompokId());

				UnitKerjaPeer::addSelectColumns($criteria);
				$this->collUnitKerjas = UnitKerjaPeer::doSelect($criteria, $con);
			}
		} else {
						if (!$this->isNew()) {
												

				$criteria->add(UnitKerjaPeer::KELOMPOK_ID, $this->getKelompokId());

				UnitKerjaPeer::addSelectColumns($criteria);
				if (!isset($this->lastUnitKerjaCriteria) || !$this->lastUnitKerjaCriteria->equals($criteria)) {
					$this->collUnitKerjas = UnitKerjaPeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastUnitKerjaCriteria = $criteria;
		return $this->collUnitKerjas;
	}

	
	public function countUnitKerjas($criteria = null, $distinct = false, $con = null)
	{
				include_once 'lib/model/budgeting/om/BaseUnitKerjaPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(UnitKerjaPeer::KELOMPOK_ID, $this->getKelompokId());

		return UnitKerjaPeer::doCount($criteria, $distinct, $con);
	}

	
	public function addUnitKerja(UnitKerja $l)
	{
		$this->collUnitKerjas[] = $l;
		$l->setKelompokDinas($this);
	}

} 