<?php


abstract class BaseUploadBpk extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id;


	
	protected $unit_id;


	
	protected $kegiatan_code;


	
	protected $tipe;


	
	protected $path;


	
	protected $penyelia;


	
	protected $created_at;


	
	protected $updated_at;


	
	protected $catatan;


	
	protected $is_khusus;


	
	protected $status;


	
	protected $catatan_tolak;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getId()
	{

		return $this->id;
	}

	
	public function getUnitId()
	{

		return $this->unit_id;
	}

	
	public function getKegiatanCode()
	{

		return $this->kegiatan_code;
	}

	
	public function getTipe()
	{

		return $this->tipe;
	}

	
	public function getPath()
	{

		return $this->path;
	}

	
	public function getPenyelia()
	{

		return $this->penyelia;
	}

	
	public function getCreatedAt($format = 'Y-m-d H:i:s')
	{

		if ($this->created_at === null || $this->created_at === '') {
			return null;
		} elseif (!is_int($this->created_at)) {
						$ts = strtotime($this->created_at);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [created_at] as date/time value: " . var_export($this->created_at, true));
			}
		} else {
			$ts = $this->created_at;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getUpdatedAt($format = 'Y-m-d H:i:s')
	{

		if ($this->updated_at === null || $this->updated_at === '') {
			return null;
		} elseif (!is_int($this->updated_at)) {
						$ts = strtotime($this->updated_at);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [updated_at] as date/time value: " . var_export($this->updated_at, true));
			}
		} else {
			$ts = $this->updated_at;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getCatatan()
	{

		return $this->catatan;
	}

	
	public function getIsKhusus()
	{

		return $this->is_khusus;
	}

	
	public function getStatus()
	{

		return $this->status;
	}

	
	public function getCatatanTolak()
	{

		return $this->catatan_tolak;
	}

	
	public function setId($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = UploadBpkPeer::ID;
		}

	} 
	
	public function setUnitId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->unit_id !== $v) {
			$this->unit_id = $v;
			$this->modifiedColumns[] = UploadBpkPeer::UNIT_ID;
		}

	} 
	
	public function setKegiatanCode($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kegiatan_code !== $v) {
			$this->kegiatan_code = $v;
			$this->modifiedColumns[] = UploadBpkPeer::KEGIATAN_CODE;
		}

	} 
	
	public function setTipe($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->tipe !== $v) {
			$this->tipe = $v;
			$this->modifiedColumns[] = UploadBpkPeer::TIPE;
		}

	} 
	
	public function setPath($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->path !== $v) {
			$this->path = $v;
			$this->modifiedColumns[] = UploadBpkPeer::PATH;
		}

	} 
	
	public function setPenyelia($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->penyelia !== $v) {
			$this->penyelia = $v;
			$this->modifiedColumns[] = UploadBpkPeer::PENYELIA;
		}

	} 
	
	public function setCreatedAt($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [created_at] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->created_at !== $ts) {
			$this->created_at = $ts;
			$this->modifiedColumns[] = UploadBpkPeer::CREATED_AT;
		}

	} 
	
	public function setUpdatedAt($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [updated_at] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->updated_at !== $ts) {
			$this->updated_at = $ts;
			$this->modifiedColumns[] = UploadBpkPeer::UPDATED_AT;
		}

	} 
	
	public function setCatatan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->catatan !== $v) {
			$this->catatan = $v;
			$this->modifiedColumns[] = UploadBpkPeer::CATATAN;
		}

	} 
	
	public function setIsKhusus($v)
	{

		if ($this->is_khusus !== $v) {
			$this->is_khusus = $v;
			$this->modifiedColumns[] = UploadBpkPeer::IS_KHUSUS;
		}

	} 
	
	public function setStatus($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->status !== $v) {
			$this->status = $v;
			$this->modifiedColumns[] = UploadBpkPeer::STATUS;
		}

	} 
	
	public function setCatatanTolak($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->catatan_tolak !== $v) {
			$this->catatan_tolak = $v;
			$this->modifiedColumns[] = UploadBpkPeer::CATATAN_TOLAK;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getInt($startcol + 0);

			$this->unit_id = $rs->getString($startcol + 1);

			$this->kegiatan_code = $rs->getString($startcol + 2);

			$this->tipe = $rs->getInt($startcol + 3);

			$this->path = $rs->getString($startcol + 4);

			$this->penyelia = $rs->getString($startcol + 5);

			$this->created_at = $rs->getTimestamp($startcol + 6, null);

			$this->updated_at = $rs->getTimestamp($startcol + 7, null);

			$this->catatan = $rs->getString($startcol + 8);

			$this->is_khusus = $rs->getBoolean($startcol + 9);

			$this->status = $rs->getInt($startcol + 10);

			$this->catatan_tolak = $rs->getString($startcol + 11);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 12; 
		} catch (Exception $e) {
			throw new PropelException("Error populating UploadBpk object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(UploadBpkPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			UploadBpkPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
    if ($this->isNew() && !$this->isColumnModified(UploadBpkPeer::CREATED_AT))
    {
      $this->setCreatedAt(time());
    }

    if ($this->isModified() && !$this->isColumnModified(UploadBpkPeer::UPDATED_AT))
    {
      $this->setUpdatedAt(time());
    }

		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(UploadBpkPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = UploadBpkPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += UploadBpkPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = UploadBpkPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = UploadBpkPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getUnitId();
				break;
			case 2:
				return $this->getKegiatanCode();
				break;
			case 3:
				return $this->getTipe();
				break;
			case 4:
				return $this->getPath();
				break;
			case 5:
				return $this->getPenyelia();
				break;
			case 6:
				return $this->getCreatedAt();
				break;
			case 7:
				return $this->getUpdatedAt();
				break;
			case 8:
				return $this->getCatatan();
				break;
			case 9:
				return $this->getIsKhusus();
				break;
			case 10:
				return $this->getStatus();
				break;
			case 11:
				return $this->getCatatanTolak();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = UploadBpkPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getUnitId(),
			$keys[2] => $this->getKegiatanCode(),
			$keys[3] => $this->getTipe(),
			$keys[4] => $this->getPath(),
			$keys[5] => $this->getPenyelia(),
			$keys[6] => $this->getCreatedAt(),
			$keys[7] => $this->getUpdatedAt(),
			$keys[8] => $this->getCatatan(),
			$keys[9] => $this->getIsKhusus(),
			$keys[10] => $this->getStatus(),
			$keys[11] => $this->getCatatanTolak(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = UploadBpkPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setUnitId($value);
				break;
			case 2:
				$this->setKegiatanCode($value);
				break;
			case 3:
				$this->setTipe($value);
				break;
			case 4:
				$this->setPath($value);
				break;
			case 5:
				$this->setPenyelia($value);
				break;
			case 6:
				$this->setCreatedAt($value);
				break;
			case 7:
				$this->setUpdatedAt($value);
				break;
			case 8:
				$this->setCatatan($value);
				break;
			case 9:
				$this->setIsKhusus($value);
				break;
			case 10:
				$this->setStatus($value);
				break;
			case 11:
				$this->setCatatanTolak($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = UploadBpkPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setUnitId($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setKegiatanCode($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setTipe($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setPath($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setPenyelia($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setCreatedAt($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setUpdatedAt($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setCatatan($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setIsKhusus($arr[$keys[9]]);
		if (array_key_exists($keys[10], $arr)) $this->setStatus($arr[$keys[10]]);
		if (array_key_exists($keys[11], $arr)) $this->setCatatanTolak($arr[$keys[11]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(UploadBpkPeer::DATABASE_NAME);

		if ($this->isColumnModified(UploadBpkPeer::ID)) $criteria->add(UploadBpkPeer::ID, $this->id);
		if ($this->isColumnModified(UploadBpkPeer::UNIT_ID)) $criteria->add(UploadBpkPeer::UNIT_ID, $this->unit_id);
		if ($this->isColumnModified(UploadBpkPeer::KEGIATAN_CODE)) $criteria->add(UploadBpkPeer::KEGIATAN_CODE, $this->kegiatan_code);
		if ($this->isColumnModified(UploadBpkPeer::TIPE)) $criteria->add(UploadBpkPeer::TIPE, $this->tipe);
		if ($this->isColumnModified(UploadBpkPeer::PATH)) $criteria->add(UploadBpkPeer::PATH, $this->path);
		if ($this->isColumnModified(UploadBpkPeer::PENYELIA)) $criteria->add(UploadBpkPeer::PENYELIA, $this->penyelia);
		if ($this->isColumnModified(UploadBpkPeer::CREATED_AT)) $criteria->add(UploadBpkPeer::CREATED_AT, $this->created_at);
		if ($this->isColumnModified(UploadBpkPeer::UPDATED_AT)) $criteria->add(UploadBpkPeer::UPDATED_AT, $this->updated_at);
		if ($this->isColumnModified(UploadBpkPeer::CATATAN)) $criteria->add(UploadBpkPeer::CATATAN, $this->catatan);
		if ($this->isColumnModified(UploadBpkPeer::IS_KHUSUS)) $criteria->add(UploadBpkPeer::IS_KHUSUS, $this->is_khusus);
		if ($this->isColumnModified(UploadBpkPeer::STATUS)) $criteria->add(UploadBpkPeer::STATUS, $this->status);
		if ($this->isColumnModified(UploadBpkPeer::CATATAN_TOLAK)) $criteria->add(UploadBpkPeer::CATATAN_TOLAK, $this->catatan_tolak);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(UploadBpkPeer::DATABASE_NAME);

		$criteria->add(UploadBpkPeer::ID, $this->id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setUnitId($this->unit_id);

		$copyObj->setKegiatanCode($this->kegiatan_code);

		$copyObj->setTipe($this->tipe);

		$copyObj->setPath($this->path);

		$copyObj->setPenyelia($this->penyelia);

		$copyObj->setCreatedAt($this->created_at);

		$copyObj->setUpdatedAt($this->updated_at);

		$copyObj->setCatatan($this->catatan);

		$copyObj->setIsKhusus($this->is_khusus);

		$copyObj->setStatus($this->status);

		$copyObj->setCatatanTolak($this->catatan_tolak);


		$copyObj->setNew(true);

		$copyObj->setId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new UploadBpkPeer();
		}
		return self::$peer;
	}

} 