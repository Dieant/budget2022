<?php


abstract class BaseKegiatanBtl extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $unit_id;


	
	protected $kode_kegiatan;


	
	protected $rekening_code;


	
	protected $tahap;


	
	protected $nama_kegiatan;


	
	protected $rekening_name;


	
	protected $catatan;


	
	protected $semula;


	
	protected $menjadi;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getUnitId()
	{

		return $this->unit_id;
	}

	
	public function getKodeKegiatan()
	{

		return $this->kode_kegiatan;
	}

	
	public function getRekeningCode()
	{

		return $this->rekening_code;
	}

	
	public function getTahap()
	{

		return $this->tahap;
	}

	
	public function getNamaKegiatan()
	{

		return $this->nama_kegiatan;
	}

	
	public function getRekeningName()
	{

		return $this->rekening_name;
	}

	
	public function getCatatan()
	{

		return $this->catatan;
	}

	
	public function getSemula()
	{

		return $this->semula;
	}

	
	public function getMenjadi()
	{

		return $this->menjadi;
	}

	
	public function setUnitId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->unit_id !== $v) {
			$this->unit_id = $v;
			$this->modifiedColumns[] = KegiatanBtlPeer::UNIT_ID;
		}

	} 
	
	public function setKodeKegiatan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_kegiatan !== $v) {
			$this->kode_kegiatan = $v;
			$this->modifiedColumns[] = KegiatanBtlPeer::KODE_KEGIATAN;
		}

	} 
	
	public function setRekeningCode($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->rekening_code !== $v) {
			$this->rekening_code = $v;
			$this->modifiedColumns[] = KegiatanBtlPeer::REKENING_CODE;
		}

	} 
	
	public function setTahap($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->tahap !== $v) {
			$this->tahap = $v;
			$this->modifiedColumns[] = KegiatanBtlPeer::TAHAP;
		}

	} 
	
	public function setNamaKegiatan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nama_kegiatan !== $v) {
			$this->nama_kegiatan = $v;
			$this->modifiedColumns[] = KegiatanBtlPeer::NAMA_KEGIATAN;
		}

	} 
	
	public function setRekeningName($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->rekening_name !== $v) {
			$this->rekening_name = $v;
			$this->modifiedColumns[] = KegiatanBtlPeer::REKENING_NAME;
		}

	} 
	
	public function setCatatan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->catatan !== $v) {
			$this->catatan = $v;
			$this->modifiedColumns[] = KegiatanBtlPeer::CATATAN;
		}

	} 
	
	public function setSemula($v)
	{

		if ($this->semula !== $v) {
			$this->semula = $v;
			$this->modifiedColumns[] = KegiatanBtlPeer::SEMULA;
		}

	} 
	
	public function setMenjadi($v)
	{

		if ($this->menjadi !== $v) {
			$this->menjadi = $v;
			$this->modifiedColumns[] = KegiatanBtlPeer::MENJADI;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->unit_id = $rs->getString($startcol + 0);

			$this->kode_kegiatan = $rs->getString($startcol + 1);

			$this->rekening_code = $rs->getString($startcol + 2);

			$this->tahap = $rs->getInt($startcol + 3);

			$this->nama_kegiatan = $rs->getString($startcol + 4);

			$this->rekening_name = $rs->getString($startcol + 5);

			$this->catatan = $rs->getString($startcol + 6);

			$this->semula = $rs->getFloat($startcol + 7);

			$this->menjadi = $rs->getFloat($startcol + 8);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 9; 
		} catch (Exception $e) {
			throw new PropelException("Error populating KegiatanBtl object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(KegiatanBtlPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			KegiatanBtlPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(KegiatanBtlPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = KegiatanBtlPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += KegiatanBtlPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = KegiatanBtlPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = KegiatanBtlPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getUnitId();
				break;
			case 1:
				return $this->getKodeKegiatan();
				break;
			case 2:
				return $this->getRekeningCode();
				break;
			case 3:
				return $this->getTahap();
				break;
			case 4:
				return $this->getNamaKegiatan();
				break;
			case 5:
				return $this->getRekeningName();
				break;
			case 6:
				return $this->getCatatan();
				break;
			case 7:
				return $this->getSemula();
				break;
			case 8:
				return $this->getMenjadi();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = KegiatanBtlPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getUnitId(),
			$keys[1] => $this->getKodeKegiatan(),
			$keys[2] => $this->getRekeningCode(),
			$keys[3] => $this->getTahap(),
			$keys[4] => $this->getNamaKegiatan(),
			$keys[5] => $this->getRekeningName(),
			$keys[6] => $this->getCatatan(),
			$keys[7] => $this->getSemula(),
			$keys[8] => $this->getMenjadi(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = KegiatanBtlPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setUnitId($value);
				break;
			case 1:
				$this->setKodeKegiatan($value);
				break;
			case 2:
				$this->setRekeningCode($value);
				break;
			case 3:
				$this->setTahap($value);
				break;
			case 4:
				$this->setNamaKegiatan($value);
				break;
			case 5:
				$this->setRekeningName($value);
				break;
			case 6:
				$this->setCatatan($value);
				break;
			case 7:
				$this->setSemula($value);
				break;
			case 8:
				$this->setMenjadi($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = KegiatanBtlPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setUnitId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setKodeKegiatan($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setRekeningCode($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setTahap($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setNamaKegiatan($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setRekeningName($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setCatatan($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setSemula($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setMenjadi($arr[$keys[8]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(KegiatanBtlPeer::DATABASE_NAME);

		if ($this->isColumnModified(KegiatanBtlPeer::UNIT_ID)) $criteria->add(KegiatanBtlPeer::UNIT_ID, $this->unit_id);
		if ($this->isColumnModified(KegiatanBtlPeer::KODE_KEGIATAN)) $criteria->add(KegiatanBtlPeer::KODE_KEGIATAN, $this->kode_kegiatan);
		if ($this->isColumnModified(KegiatanBtlPeer::REKENING_CODE)) $criteria->add(KegiatanBtlPeer::REKENING_CODE, $this->rekening_code);
		if ($this->isColumnModified(KegiatanBtlPeer::TAHAP)) $criteria->add(KegiatanBtlPeer::TAHAP, $this->tahap);
		if ($this->isColumnModified(KegiatanBtlPeer::NAMA_KEGIATAN)) $criteria->add(KegiatanBtlPeer::NAMA_KEGIATAN, $this->nama_kegiatan);
		if ($this->isColumnModified(KegiatanBtlPeer::REKENING_NAME)) $criteria->add(KegiatanBtlPeer::REKENING_NAME, $this->rekening_name);
		if ($this->isColumnModified(KegiatanBtlPeer::CATATAN)) $criteria->add(KegiatanBtlPeer::CATATAN, $this->catatan);
		if ($this->isColumnModified(KegiatanBtlPeer::SEMULA)) $criteria->add(KegiatanBtlPeer::SEMULA, $this->semula);
		if ($this->isColumnModified(KegiatanBtlPeer::MENJADI)) $criteria->add(KegiatanBtlPeer::MENJADI, $this->menjadi);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(KegiatanBtlPeer::DATABASE_NAME);

		$criteria->add(KegiatanBtlPeer::UNIT_ID, $this->unit_id);
		$criteria->add(KegiatanBtlPeer::KODE_KEGIATAN, $this->kode_kegiatan);
		$criteria->add(KegiatanBtlPeer::REKENING_CODE, $this->rekening_code);
		$criteria->add(KegiatanBtlPeer::TAHAP, $this->tahap);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		$pks = array();

		$pks[0] = $this->getUnitId();

		$pks[1] = $this->getKodeKegiatan();

		$pks[2] = $this->getRekeningCode();

		$pks[3] = $this->getTahap();

		return $pks;
	}

	
	public function setPrimaryKey($keys)
	{

		$this->setUnitId($keys[0]);

		$this->setKodeKegiatan($keys[1]);

		$this->setRekeningCode($keys[2]);

		$this->setTahap($keys[3]);

	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setNamaKegiatan($this->nama_kegiatan);

		$copyObj->setRekeningName($this->rekening_name);

		$copyObj->setCatatan($this->catatan);

		$copyObj->setSemula($this->semula);

		$copyObj->setMenjadi($this->menjadi);


		$copyObj->setNew(true);

		$copyObj->setUnitId(NULL); 
		$copyObj->setKodeKegiatan(NULL); 
		$copyObj->setRekeningCode(NULL); 
		$copyObj->setTahap(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new KegiatanBtlPeer();
		}
		return self::$peer;
	}

} 