<?php


abstract class BaseBukuPutihMasterKegiatanPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.bukuputih_master_kegiatan';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.BukuPutihMasterKegiatan';

	
	const NUM_COLUMNS = 43;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const UNIT_ID = 'ebudget.bukuputih_master_kegiatan.UNIT_ID';

	
	const KODE_KEGIATAN = 'ebudget.bukuputih_master_kegiatan.KODE_KEGIATAN';

	
	const KODE_BIDANG = 'ebudget.bukuputih_master_kegiatan.KODE_BIDANG';

	
	const KODE_URUSAN_WAJIB = 'ebudget.bukuputih_master_kegiatan.KODE_URUSAN_WAJIB';

	
	const KODE_PROGRAM = 'ebudget.bukuputih_master_kegiatan.KODE_PROGRAM';

	
	const KODE_SASARAN = 'ebudget.bukuputih_master_kegiatan.KODE_SASARAN';

	
	const KODE_INDIKATOR = 'ebudget.bukuputih_master_kegiatan.KODE_INDIKATOR';

	
	const ALOKASI_DANA = 'ebudget.bukuputih_master_kegiatan.ALOKASI_DANA';

	
	const NAMA_KEGIATAN = 'ebudget.bukuputih_master_kegiatan.NAMA_KEGIATAN';

	
	const MASUKAN = 'ebudget.bukuputih_master_kegiatan.MASUKAN';

	
	const OUTPUT = 'ebudget.bukuputih_master_kegiatan.OUTPUT';

	
	const OUTCOME = 'ebudget.bukuputih_master_kegiatan.OUTCOME';

	
	const BENEFIT = 'ebudget.bukuputih_master_kegiatan.BENEFIT';

	
	const IMPACT = 'ebudget.bukuputih_master_kegiatan.IMPACT';

	
	const TIPE = 'ebudget.bukuputih_master_kegiatan.TIPE';

	
	const KEGIATAN_ACTIVE = 'ebudget.bukuputih_master_kegiatan.KEGIATAN_ACTIVE';

	
	const TO_KEGIATAN_CODE = 'ebudget.bukuputih_master_kegiatan.TO_KEGIATAN_CODE';

	
	const CATATAN = 'ebudget.bukuputih_master_kegiatan.CATATAN';

	
	const TARGET_OUTCOME = 'ebudget.bukuputih_master_kegiatan.TARGET_OUTCOME';

	
	const LOKASI = 'ebudget.bukuputih_master_kegiatan.LOKASI';

	
	const JUMLAH_PREV = 'ebudget.bukuputih_master_kegiatan.JUMLAH_PREV';

	
	const JUMLAH_NOW = 'ebudget.bukuputih_master_kegiatan.JUMLAH_NOW';

	
	const JUMLAH_NEXT = 'ebudget.bukuputih_master_kegiatan.JUMLAH_NEXT';

	
	const KODE_PROGRAM2 = 'ebudget.bukuputih_master_kegiatan.KODE_PROGRAM2';

	
	const KODE_URUSAN = 'ebudget.bukuputih_master_kegiatan.KODE_URUSAN';

	
	const LAST_UPDATE_USER = 'ebudget.bukuputih_master_kegiatan.LAST_UPDATE_USER';

	
	const LAST_UPDATE_TIME = 'ebudget.bukuputih_master_kegiatan.LAST_UPDATE_TIME';

	
	const LAST_UPDATE_IP = 'ebudget.bukuputih_master_kegiatan.LAST_UPDATE_IP';

	
	const TAHAP = 'ebudget.bukuputih_master_kegiatan.TAHAP';

	
	const KODE_MISI = 'ebudget.bukuputih_master_kegiatan.KODE_MISI';

	
	const KODE_TUJUAN = 'ebudget.bukuputih_master_kegiatan.KODE_TUJUAN';

	
	const RANKING = 'ebudget.bukuputih_master_kegiatan.RANKING';

	
	const NOMOR13 = 'ebudget.bukuputih_master_kegiatan.NOMOR13';

	
	const PPA_NAMA = 'ebudget.bukuputih_master_kegiatan.PPA_NAMA';

	
	const PPA_PANGKAT = 'ebudget.bukuputih_master_kegiatan.PPA_PANGKAT';

	
	const PPA_NIP = 'ebudget.bukuputih_master_kegiatan.PPA_NIP';

	
	const LANJUTAN = 'ebudget.bukuputih_master_kegiatan.LANJUTAN';

	
	const USER_ID = 'ebudget.bukuputih_master_kegiatan.USER_ID';

	
	const ID = 'ebudget.bukuputih_master_kegiatan.ID';

	
	const TAHUN = 'ebudget.bukuputih_master_kegiatan.TAHUN';

	
	const TAMBAHAN_PAGU = 'ebudget.bukuputih_master_kegiatan.TAMBAHAN_PAGU';

	
	const GENDER = 'ebudget.bukuputih_master_kegiatan.GENDER';

	
	const KODE_KEG_KEUANGAN = 'ebudget.bukuputih_master_kegiatan.KODE_KEG_KEUANGAN';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('UnitId', 'KodeKegiatan', 'KodeBidang', 'KodeUrusanWajib', 'KodeProgram', 'KodeSasaran', 'KodeIndikator', 'AlokasiDana', 'NamaKegiatan', 'Masukan', 'Output', 'Outcome', 'Benefit', 'Impact', 'Tipe', 'KegiatanActive', 'ToKegiatanCode', 'Catatan', 'TargetOutcome', 'Lokasi', 'JumlahPrev', 'JumlahNow', 'JumlahNext', 'KodeProgram2', 'KodeUrusan', 'LastUpdateUser', 'LastUpdateTime', 'LastUpdateIp', 'Tahap', 'KodeMisi', 'KodeTujuan', 'Ranking', 'Nomor13', 'PpaNama', 'PpaPangkat', 'PpaNip', 'Lanjutan', 'UserId', 'Id', 'Tahun', 'TambahanPagu', 'Gender', 'KodeKegKeuangan', ),
		BasePeer::TYPE_COLNAME => array (BukuPutihMasterKegiatanPeer::UNIT_ID, BukuPutihMasterKegiatanPeer::KODE_KEGIATAN, BukuPutihMasterKegiatanPeer::KODE_BIDANG, BukuPutihMasterKegiatanPeer::KODE_URUSAN_WAJIB, BukuPutihMasterKegiatanPeer::KODE_PROGRAM, BukuPutihMasterKegiatanPeer::KODE_SASARAN, BukuPutihMasterKegiatanPeer::KODE_INDIKATOR, BukuPutihMasterKegiatanPeer::ALOKASI_DANA, BukuPutihMasterKegiatanPeer::NAMA_KEGIATAN, BukuPutihMasterKegiatanPeer::MASUKAN, BukuPutihMasterKegiatanPeer::OUTPUT, BukuPutihMasterKegiatanPeer::OUTCOME, BukuPutihMasterKegiatanPeer::BENEFIT, BukuPutihMasterKegiatanPeer::IMPACT, BukuPutihMasterKegiatanPeer::TIPE, BukuPutihMasterKegiatanPeer::KEGIATAN_ACTIVE, BukuPutihMasterKegiatanPeer::TO_KEGIATAN_CODE, BukuPutihMasterKegiatanPeer::CATATAN, BukuPutihMasterKegiatanPeer::TARGET_OUTCOME, BukuPutihMasterKegiatanPeer::LOKASI, BukuPutihMasterKegiatanPeer::JUMLAH_PREV, BukuPutihMasterKegiatanPeer::JUMLAH_NOW, BukuPutihMasterKegiatanPeer::JUMLAH_NEXT, BukuPutihMasterKegiatanPeer::KODE_PROGRAM2, BukuPutihMasterKegiatanPeer::KODE_URUSAN, BukuPutihMasterKegiatanPeer::LAST_UPDATE_USER, BukuPutihMasterKegiatanPeer::LAST_UPDATE_TIME, BukuPutihMasterKegiatanPeer::LAST_UPDATE_IP, BukuPutihMasterKegiatanPeer::TAHAP, BukuPutihMasterKegiatanPeer::KODE_MISI, BukuPutihMasterKegiatanPeer::KODE_TUJUAN, BukuPutihMasterKegiatanPeer::RANKING, BukuPutihMasterKegiatanPeer::NOMOR13, BukuPutihMasterKegiatanPeer::PPA_NAMA, BukuPutihMasterKegiatanPeer::PPA_PANGKAT, BukuPutihMasterKegiatanPeer::PPA_NIP, BukuPutihMasterKegiatanPeer::LANJUTAN, BukuPutihMasterKegiatanPeer::USER_ID, BukuPutihMasterKegiatanPeer::ID, BukuPutihMasterKegiatanPeer::TAHUN, BukuPutihMasterKegiatanPeer::TAMBAHAN_PAGU, BukuPutihMasterKegiatanPeer::GENDER, BukuPutihMasterKegiatanPeer::KODE_KEG_KEUANGAN, ),
		BasePeer::TYPE_FIELDNAME => array ('unit_id', 'kode_kegiatan', 'kode_bidang', 'kode_urusan_wajib', 'kode_program', 'kode_sasaran', 'kode_indikator', 'alokasi_dana', 'nama_kegiatan', 'masukan', 'output', 'outcome', 'benefit', 'impact', 'tipe', 'kegiatan_active', 'to_kegiatan_code', 'catatan', 'target_outcome', 'lokasi', 'jumlah_prev', 'jumlah_now', 'jumlah_next', 'kode_program2', 'kode_urusan', 'last_update_user', 'last_update_time', 'last_update_ip', 'tahap', 'kode_misi', 'kode_tujuan', 'ranking', 'nomor13', 'ppa_nama', 'ppa_pangkat', 'ppa_nip', 'lanjutan', 'user_id', 'id', 'tahun', 'tambahan_pagu', 'gender', 'kode_keg_keuangan', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('UnitId' => 0, 'KodeKegiatan' => 1, 'KodeBidang' => 2, 'KodeUrusanWajib' => 3, 'KodeProgram' => 4, 'KodeSasaran' => 5, 'KodeIndikator' => 6, 'AlokasiDana' => 7, 'NamaKegiatan' => 8, 'Masukan' => 9, 'Output' => 10, 'Outcome' => 11, 'Benefit' => 12, 'Impact' => 13, 'Tipe' => 14, 'KegiatanActive' => 15, 'ToKegiatanCode' => 16, 'Catatan' => 17, 'TargetOutcome' => 18, 'Lokasi' => 19, 'JumlahPrev' => 20, 'JumlahNow' => 21, 'JumlahNext' => 22, 'KodeProgram2' => 23, 'KodeUrusan' => 24, 'LastUpdateUser' => 25, 'LastUpdateTime' => 26, 'LastUpdateIp' => 27, 'Tahap' => 28, 'KodeMisi' => 29, 'KodeTujuan' => 30, 'Ranking' => 31, 'Nomor13' => 32, 'PpaNama' => 33, 'PpaPangkat' => 34, 'PpaNip' => 35, 'Lanjutan' => 36, 'UserId' => 37, 'Id' => 38, 'Tahun' => 39, 'TambahanPagu' => 40, 'Gender' => 41, 'KodeKegKeuangan' => 42, ),
		BasePeer::TYPE_COLNAME => array (BukuPutihMasterKegiatanPeer::UNIT_ID => 0, BukuPutihMasterKegiatanPeer::KODE_KEGIATAN => 1, BukuPutihMasterKegiatanPeer::KODE_BIDANG => 2, BukuPutihMasterKegiatanPeer::KODE_URUSAN_WAJIB => 3, BukuPutihMasterKegiatanPeer::KODE_PROGRAM => 4, BukuPutihMasterKegiatanPeer::KODE_SASARAN => 5, BukuPutihMasterKegiatanPeer::KODE_INDIKATOR => 6, BukuPutihMasterKegiatanPeer::ALOKASI_DANA => 7, BukuPutihMasterKegiatanPeer::NAMA_KEGIATAN => 8, BukuPutihMasterKegiatanPeer::MASUKAN => 9, BukuPutihMasterKegiatanPeer::OUTPUT => 10, BukuPutihMasterKegiatanPeer::OUTCOME => 11, BukuPutihMasterKegiatanPeer::BENEFIT => 12, BukuPutihMasterKegiatanPeer::IMPACT => 13, BukuPutihMasterKegiatanPeer::TIPE => 14, BukuPutihMasterKegiatanPeer::KEGIATAN_ACTIVE => 15, BukuPutihMasterKegiatanPeer::TO_KEGIATAN_CODE => 16, BukuPutihMasterKegiatanPeer::CATATAN => 17, BukuPutihMasterKegiatanPeer::TARGET_OUTCOME => 18, BukuPutihMasterKegiatanPeer::LOKASI => 19, BukuPutihMasterKegiatanPeer::JUMLAH_PREV => 20, BukuPutihMasterKegiatanPeer::JUMLAH_NOW => 21, BukuPutihMasterKegiatanPeer::JUMLAH_NEXT => 22, BukuPutihMasterKegiatanPeer::KODE_PROGRAM2 => 23, BukuPutihMasterKegiatanPeer::KODE_URUSAN => 24, BukuPutihMasterKegiatanPeer::LAST_UPDATE_USER => 25, BukuPutihMasterKegiatanPeer::LAST_UPDATE_TIME => 26, BukuPutihMasterKegiatanPeer::LAST_UPDATE_IP => 27, BukuPutihMasterKegiatanPeer::TAHAP => 28, BukuPutihMasterKegiatanPeer::KODE_MISI => 29, BukuPutihMasterKegiatanPeer::KODE_TUJUAN => 30, BukuPutihMasterKegiatanPeer::RANKING => 31, BukuPutihMasterKegiatanPeer::NOMOR13 => 32, BukuPutihMasterKegiatanPeer::PPA_NAMA => 33, BukuPutihMasterKegiatanPeer::PPA_PANGKAT => 34, BukuPutihMasterKegiatanPeer::PPA_NIP => 35, BukuPutihMasterKegiatanPeer::LANJUTAN => 36, BukuPutihMasterKegiatanPeer::USER_ID => 37, BukuPutihMasterKegiatanPeer::ID => 38, BukuPutihMasterKegiatanPeer::TAHUN => 39, BukuPutihMasterKegiatanPeer::TAMBAHAN_PAGU => 40, BukuPutihMasterKegiatanPeer::GENDER => 41, BukuPutihMasterKegiatanPeer::KODE_KEG_KEUANGAN => 42, ),
		BasePeer::TYPE_FIELDNAME => array ('unit_id' => 0, 'kode_kegiatan' => 1, 'kode_bidang' => 2, 'kode_urusan_wajib' => 3, 'kode_program' => 4, 'kode_sasaran' => 5, 'kode_indikator' => 6, 'alokasi_dana' => 7, 'nama_kegiatan' => 8, 'masukan' => 9, 'output' => 10, 'outcome' => 11, 'benefit' => 12, 'impact' => 13, 'tipe' => 14, 'kegiatan_active' => 15, 'to_kegiatan_code' => 16, 'catatan' => 17, 'target_outcome' => 18, 'lokasi' => 19, 'jumlah_prev' => 20, 'jumlah_now' => 21, 'jumlah_next' => 22, 'kode_program2' => 23, 'kode_urusan' => 24, 'last_update_user' => 25, 'last_update_time' => 26, 'last_update_ip' => 27, 'tahap' => 28, 'kode_misi' => 29, 'kode_tujuan' => 30, 'ranking' => 31, 'nomor13' => 32, 'ppa_nama' => 33, 'ppa_pangkat' => 34, 'ppa_nip' => 35, 'lanjutan' => 36, 'user_id' => 37, 'id' => 38, 'tahun' => 39, 'tambahan_pagu' => 40, 'gender' => 41, 'kode_keg_keuangan' => 42, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/BukuPutihMasterKegiatanMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.BukuPutihMasterKegiatanMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = BukuPutihMasterKegiatanPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(BukuPutihMasterKegiatanPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(BukuPutihMasterKegiatanPeer::UNIT_ID);

		$criteria->addSelectColumn(BukuPutihMasterKegiatanPeer::KODE_KEGIATAN);

		$criteria->addSelectColumn(BukuPutihMasterKegiatanPeer::KODE_BIDANG);

		$criteria->addSelectColumn(BukuPutihMasterKegiatanPeer::KODE_URUSAN_WAJIB);

		$criteria->addSelectColumn(BukuPutihMasterKegiatanPeer::KODE_PROGRAM);

		$criteria->addSelectColumn(BukuPutihMasterKegiatanPeer::KODE_SASARAN);

		$criteria->addSelectColumn(BukuPutihMasterKegiatanPeer::KODE_INDIKATOR);

		$criteria->addSelectColumn(BukuPutihMasterKegiatanPeer::ALOKASI_DANA);

		$criteria->addSelectColumn(BukuPutihMasterKegiatanPeer::NAMA_KEGIATAN);

		$criteria->addSelectColumn(BukuPutihMasterKegiatanPeer::MASUKAN);

		$criteria->addSelectColumn(BukuPutihMasterKegiatanPeer::OUTPUT);

		$criteria->addSelectColumn(BukuPutihMasterKegiatanPeer::OUTCOME);

		$criteria->addSelectColumn(BukuPutihMasterKegiatanPeer::BENEFIT);

		$criteria->addSelectColumn(BukuPutihMasterKegiatanPeer::IMPACT);

		$criteria->addSelectColumn(BukuPutihMasterKegiatanPeer::TIPE);

		$criteria->addSelectColumn(BukuPutihMasterKegiatanPeer::KEGIATAN_ACTIVE);

		$criteria->addSelectColumn(BukuPutihMasterKegiatanPeer::TO_KEGIATAN_CODE);

		$criteria->addSelectColumn(BukuPutihMasterKegiatanPeer::CATATAN);

		$criteria->addSelectColumn(BukuPutihMasterKegiatanPeer::TARGET_OUTCOME);

		$criteria->addSelectColumn(BukuPutihMasterKegiatanPeer::LOKASI);

		$criteria->addSelectColumn(BukuPutihMasterKegiatanPeer::JUMLAH_PREV);

		$criteria->addSelectColumn(BukuPutihMasterKegiatanPeer::JUMLAH_NOW);

		$criteria->addSelectColumn(BukuPutihMasterKegiatanPeer::JUMLAH_NEXT);

		$criteria->addSelectColumn(BukuPutihMasterKegiatanPeer::KODE_PROGRAM2);

		$criteria->addSelectColumn(BukuPutihMasterKegiatanPeer::KODE_URUSAN);

		$criteria->addSelectColumn(BukuPutihMasterKegiatanPeer::LAST_UPDATE_USER);

		$criteria->addSelectColumn(BukuPutihMasterKegiatanPeer::LAST_UPDATE_TIME);

		$criteria->addSelectColumn(BukuPutihMasterKegiatanPeer::LAST_UPDATE_IP);

		$criteria->addSelectColumn(BukuPutihMasterKegiatanPeer::TAHAP);

		$criteria->addSelectColumn(BukuPutihMasterKegiatanPeer::KODE_MISI);

		$criteria->addSelectColumn(BukuPutihMasterKegiatanPeer::KODE_TUJUAN);

		$criteria->addSelectColumn(BukuPutihMasterKegiatanPeer::RANKING);

		$criteria->addSelectColumn(BukuPutihMasterKegiatanPeer::NOMOR13);

		$criteria->addSelectColumn(BukuPutihMasterKegiatanPeer::PPA_NAMA);

		$criteria->addSelectColumn(BukuPutihMasterKegiatanPeer::PPA_PANGKAT);

		$criteria->addSelectColumn(BukuPutihMasterKegiatanPeer::PPA_NIP);

		$criteria->addSelectColumn(BukuPutihMasterKegiatanPeer::LANJUTAN);

		$criteria->addSelectColumn(BukuPutihMasterKegiatanPeer::USER_ID);

		$criteria->addSelectColumn(BukuPutihMasterKegiatanPeer::ID);

		$criteria->addSelectColumn(BukuPutihMasterKegiatanPeer::TAHUN);

		$criteria->addSelectColumn(BukuPutihMasterKegiatanPeer::TAMBAHAN_PAGU);

		$criteria->addSelectColumn(BukuPutihMasterKegiatanPeer::GENDER);

		$criteria->addSelectColumn(BukuPutihMasterKegiatanPeer::KODE_KEG_KEUANGAN);

	}

	const COUNT = 'COUNT(ebudget.bukuputih_master_kegiatan.UNIT_ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.bukuputih_master_kegiatan.UNIT_ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(BukuPutihMasterKegiatanPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(BukuPutihMasterKegiatanPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = BukuPutihMasterKegiatanPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = BukuPutihMasterKegiatanPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return BukuPutihMasterKegiatanPeer::populateObjects(BukuPutihMasterKegiatanPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			BukuPutihMasterKegiatanPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = BukuPutihMasterKegiatanPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return BukuPutihMasterKegiatanPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}

		$criteria->remove(BukuPutihMasterKegiatanPeer::ID); 

				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(BukuPutihMasterKegiatanPeer::UNIT_ID);
			$selectCriteria->add(BukuPutihMasterKegiatanPeer::UNIT_ID, $criteria->remove(BukuPutihMasterKegiatanPeer::UNIT_ID), $comparison);

			$comparison = $criteria->getComparison(BukuPutihMasterKegiatanPeer::KODE_KEGIATAN);
			$selectCriteria->add(BukuPutihMasterKegiatanPeer::KODE_KEGIATAN, $criteria->remove(BukuPutihMasterKegiatanPeer::KODE_KEGIATAN), $comparison);

			$comparison = $criteria->getComparison(BukuPutihMasterKegiatanPeer::ID);
			$selectCriteria->add(BukuPutihMasterKegiatanPeer::ID, $criteria->remove(BukuPutihMasterKegiatanPeer::ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(BukuPutihMasterKegiatanPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(BukuPutihMasterKegiatanPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof BukuPutihMasterKegiatan) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
												if(count($values) == count($values, COUNT_RECURSIVE))
			{
								$values = array($values);
			}
			$vals = array();
			foreach($values as $value)
			{

				$vals[0][] = $value[0];
				$vals[1][] = $value[1];
				$vals[2][] = $value[2];
			}

			$criteria->add(BukuPutihMasterKegiatanPeer::UNIT_ID, $vals[0], Criteria::IN);
			$criteria->add(BukuPutihMasterKegiatanPeer::KODE_KEGIATAN, $vals[1], Criteria::IN);
			$criteria->add(BukuPutihMasterKegiatanPeer::ID, $vals[2], Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(BukuPutihMasterKegiatan $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(BukuPutihMasterKegiatanPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(BukuPutihMasterKegiatanPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(BukuPutihMasterKegiatanPeer::DATABASE_NAME, BukuPutihMasterKegiatanPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = BukuPutihMasterKegiatanPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK( $unit_id, $kode_kegiatan, $id, $con = null) {
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$criteria = new Criteria();
		$criteria->add(BukuPutihMasterKegiatanPeer::UNIT_ID, $unit_id);
		$criteria->add(BukuPutihMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
		$criteria->add(BukuPutihMasterKegiatanPeer::ID, $id);
		$v = BukuPutihMasterKegiatanPeer::doSelect($criteria, $con);

		return !empty($v) ? $v[0] : null;
	}
} 
if (Propel::isInit()) {
			try {
		BaseBukuPutihMasterKegiatanPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/BukuPutihMasterKegiatanMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.BukuPutihMasterKegiatanMapBuilder');
}
