<?php


abstract class BaseBukuProvinsiMasterKegiatanPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.buku_provinsi_master_kegiatan';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.BukuProvinsiMasterKegiatan';

	
	const NUM_COLUMNS = 51;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const UNIT_ID = 'ebudget.buku_provinsi_master_kegiatan.UNIT_ID';

	
	const KODE_KEGIATAN = 'ebudget.buku_provinsi_master_kegiatan.KODE_KEGIATAN';

	
	const KODE_BIDANG = 'ebudget.buku_provinsi_master_kegiatan.KODE_BIDANG';

	
	const KODE_URUSAN_WAJIB = 'ebudget.buku_provinsi_master_kegiatan.KODE_URUSAN_WAJIB';

	
	const KODE_PROGRAM = 'ebudget.buku_provinsi_master_kegiatan.KODE_PROGRAM';

	
	const KODE_SASARAN = 'ebudget.buku_provinsi_master_kegiatan.KODE_SASARAN';

	
	const KODE_INDIKATOR = 'ebudget.buku_provinsi_master_kegiatan.KODE_INDIKATOR';

	
	const ALOKASI_DANA = 'ebudget.buku_provinsi_master_kegiatan.ALOKASI_DANA';

	
	const NAMA_KEGIATAN = 'ebudget.buku_provinsi_master_kegiatan.NAMA_KEGIATAN';

	
	const MASUKAN = 'ebudget.buku_provinsi_master_kegiatan.MASUKAN';

	
	const OUTPUT = 'ebudget.buku_provinsi_master_kegiatan.OUTPUT';

	
	const OUTCOME = 'ebudget.buku_provinsi_master_kegiatan.OUTCOME';

	
	const BENEFIT = 'ebudget.buku_provinsi_master_kegiatan.BENEFIT';

	
	const IMPACT = 'ebudget.buku_provinsi_master_kegiatan.IMPACT';

	
	const TIPE = 'ebudget.buku_provinsi_master_kegiatan.TIPE';

	
	const KEGIATAN_ACTIVE = 'ebudget.buku_provinsi_master_kegiatan.KEGIATAN_ACTIVE';

	
	const TO_KEGIATAN_CODE = 'ebudget.buku_provinsi_master_kegiatan.TO_KEGIATAN_CODE';

	
	const CATATAN = 'ebudget.buku_provinsi_master_kegiatan.CATATAN';

	
	const TARGET_OUTCOME = 'ebudget.buku_provinsi_master_kegiatan.TARGET_OUTCOME';

	
	const LOKASI = 'ebudget.buku_provinsi_master_kegiatan.LOKASI';

	
	const JUMLAH_PREV = 'ebudget.buku_provinsi_master_kegiatan.JUMLAH_PREV';

	
	const JUMLAH_NOW = 'ebudget.buku_provinsi_master_kegiatan.JUMLAH_NOW';

	
	const JUMLAH_NEXT = 'ebudget.buku_provinsi_master_kegiatan.JUMLAH_NEXT';

	
	const KODE_PROGRAM2 = 'ebudget.buku_provinsi_master_kegiatan.KODE_PROGRAM2';

	
	const KODE_URUSAN = 'ebudget.buku_provinsi_master_kegiatan.KODE_URUSAN';

	
	const LAST_UPDATE_USER = 'ebudget.buku_provinsi_master_kegiatan.LAST_UPDATE_USER';

	
	const LAST_UPDATE_TIME = 'ebudget.buku_provinsi_master_kegiatan.LAST_UPDATE_TIME';

	
	const LAST_UPDATE_IP = 'ebudget.buku_provinsi_master_kegiatan.LAST_UPDATE_IP';

	
	const TAHAP = 'ebudget.buku_provinsi_master_kegiatan.TAHAP';

	
	const KODE_MISI = 'ebudget.buku_provinsi_master_kegiatan.KODE_MISI';

	
	const KODE_TUJUAN = 'ebudget.buku_provinsi_master_kegiatan.KODE_TUJUAN';

	
	const RANKING = 'ebudget.buku_provinsi_master_kegiatan.RANKING';

	
	const NOMOR13 = 'ebudget.buku_provinsi_master_kegiatan.NOMOR13';

	
	const PPA_NAMA = 'ebudget.buku_provinsi_master_kegiatan.PPA_NAMA';

	
	const PPA_PANGKAT = 'ebudget.buku_provinsi_master_kegiatan.PPA_PANGKAT';

	
	const PPA_NIP = 'ebudget.buku_provinsi_master_kegiatan.PPA_NIP';

	
	const LANJUTAN = 'ebudget.buku_provinsi_master_kegiatan.LANJUTAN';

	
	const USER_ID = 'ebudget.buku_provinsi_master_kegiatan.USER_ID';

	
	const ID = 'ebudget.buku_provinsi_master_kegiatan.ID';

	
	const TAHUN = 'ebudget.buku_provinsi_master_kegiatan.TAHUN';

	
	const TAMBAHAN_PAGU = 'ebudget.buku_provinsi_master_kegiatan.TAMBAHAN_PAGU';

	
	const GENDER = 'ebudget.buku_provinsi_master_kegiatan.GENDER';

	
	const KODE_KEG_KEUANGAN = 'ebudget.buku_provinsi_master_kegiatan.KODE_KEG_KEUANGAN';

	
	const USER_ID_LAMA = 'ebudget.buku_provinsi_master_kegiatan.USER_ID_LAMA';

	
	const INDIKATOR = 'ebudget.buku_provinsi_master_kegiatan.INDIKATOR';

	
	const IS_DAK = 'ebudget.buku_provinsi_master_kegiatan.IS_DAK';

	
	const KODE_KEGIATAN_ASAL = 'ebudget.buku_provinsi_master_kegiatan.KODE_KEGIATAN_ASAL';

	
	const KODE_KEG_KEUANGAN_ASAL = 'ebudget.buku_provinsi_master_kegiatan.KODE_KEG_KEUANGAN_ASAL';

	
	const TH_KE_MULTIYEARS = 'ebudget.buku_provinsi_master_kegiatan.TH_KE_MULTIYEARS';

	
	const KELOMPOK_SASARAN = 'ebudget.buku_provinsi_master_kegiatan.KELOMPOK_SASARAN';

	
	const PAGU_BAPPEKO = 'ebudget.buku_provinsi_master_kegiatan.PAGU_BAPPEKO';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('UnitId', 'KodeKegiatan', 'KodeBidang', 'KodeUrusanWajib', 'KodeProgram', 'KodeSasaran', 'KodeIndikator', 'AlokasiDana', 'NamaKegiatan', 'Masukan', 'Output', 'Outcome', 'Benefit', 'Impact', 'Tipe', 'KegiatanActive', 'ToKegiatanCode', 'Catatan', 'TargetOutcome', 'Lokasi', 'JumlahPrev', 'JumlahNow', 'JumlahNext', 'KodeProgram2', 'KodeUrusan', 'LastUpdateUser', 'LastUpdateTime', 'LastUpdateIp', 'Tahap', 'KodeMisi', 'KodeTujuan', 'Ranking', 'Nomor13', 'PpaNama', 'PpaPangkat', 'PpaNip', 'Lanjutan', 'UserId', 'Id', 'Tahun', 'TambahanPagu', 'Gender', 'KodeKegKeuangan', 'UserIdLama', 'Indikator', 'IsDak', 'KodeKegiatanAsal', 'KodeKegKeuanganAsal', 'ThKeMultiyears', 'KelompokSasaran', 'PaguBappeko', ),
		BasePeer::TYPE_COLNAME => array (BukuProvinsiMasterKegiatanPeer::UNIT_ID, BukuProvinsiMasterKegiatanPeer::KODE_KEGIATAN, BukuProvinsiMasterKegiatanPeer::KODE_BIDANG, BukuProvinsiMasterKegiatanPeer::KODE_URUSAN_WAJIB, BukuProvinsiMasterKegiatanPeer::KODE_PROGRAM, BukuProvinsiMasterKegiatanPeer::KODE_SASARAN, BukuProvinsiMasterKegiatanPeer::KODE_INDIKATOR, BukuProvinsiMasterKegiatanPeer::ALOKASI_DANA, BukuProvinsiMasterKegiatanPeer::NAMA_KEGIATAN, BukuProvinsiMasterKegiatanPeer::MASUKAN, BukuProvinsiMasterKegiatanPeer::OUTPUT, BukuProvinsiMasterKegiatanPeer::OUTCOME, BukuProvinsiMasterKegiatanPeer::BENEFIT, BukuProvinsiMasterKegiatanPeer::IMPACT, BukuProvinsiMasterKegiatanPeer::TIPE, BukuProvinsiMasterKegiatanPeer::KEGIATAN_ACTIVE, BukuProvinsiMasterKegiatanPeer::TO_KEGIATAN_CODE, BukuProvinsiMasterKegiatanPeer::CATATAN, BukuProvinsiMasterKegiatanPeer::TARGET_OUTCOME, BukuProvinsiMasterKegiatanPeer::LOKASI, BukuProvinsiMasterKegiatanPeer::JUMLAH_PREV, BukuProvinsiMasterKegiatanPeer::JUMLAH_NOW, BukuProvinsiMasterKegiatanPeer::JUMLAH_NEXT, BukuProvinsiMasterKegiatanPeer::KODE_PROGRAM2, BukuProvinsiMasterKegiatanPeer::KODE_URUSAN, BukuProvinsiMasterKegiatanPeer::LAST_UPDATE_USER, BukuProvinsiMasterKegiatanPeer::LAST_UPDATE_TIME, BukuProvinsiMasterKegiatanPeer::LAST_UPDATE_IP, BukuProvinsiMasterKegiatanPeer::TAHAP, BukuProvinsiMasterKegiatanPeer::KODE_MISI, BukuProvinsiMasterKegiatanPeer::KODE_TUJUAN, BukuProvinsiMasterKegiatanPeer::RANKING, BukuProvinsiMasterKegiatanPeer::NOMOR13, BukuProvinsiMasterKegiatanPeer::PPA_NAMA, BukuProvinsiMasterKegiatanPeer::PPA_PANGKAT, BukuProvinsiMasterKegiatanPeer::PPA_NIP, BukuProvinsiMasterKegiatanPeer::LANJUTAN, BukuProvinsiMasterKegiatanPeer::USER_ID, BukuProvinsiMasterKegiatanPeer::ID, BukuProvinsiMasterKegiatanPeer::TAHUN, BukuProvinsiMasterKegiatanPeer::TAMBAHAN_PAGU, BukuProvinsiMasterKegiatanPeer::GENDER, BukuProvinsiMasterKegiatanPeer::KODE_KEG_KEUANGAN, BukuProvinsiMasterKegiatanPeer::USER_ID_LAMA, BukuProvinsiMasterKegiatanPeer::INDIKATOR, BukuProvinsiMasterKegiatanPeer::IS_DAK, BukuProvinsiMasterKegiatanPeer::KODE_KEGIATAN_ASAL, BukuProvinsiMasterKegiatanPeer::KODE_KEG_KEUANGAN_ASAL, BukuProvinsiMasterKegiatanPeer::TH_KE_MULTIYEARS, BukuProvinsiMasterKegiatanPeer::KELOMPOK_SASARAN, BukuProvinsiMasterKegiatanPeer::PAGU_BAPPEKO, ),
		BasePeer::TYPE_FIELDNAME => array ('unit_id', 'kode_kegiatan', 'kode_bidang', 'kode_urusan_wajib', 'kode_program', 'kode_sasaran', 'kode_indikator', 'alokasi_dana', 'nama_kegiatan', 'masukan', 'output', 'outcome', 'benefit', 'impact', 'tipe', 'kegiatan_active', 'to_kegiatan_code', 'catatan', 'target_outcome', 'lokasi', 'jumlah_prev', 'jumlah_now', 'jumlah_next', 'kode_program2', 'kode_urusan', 'last_update_user', 'last_update_time', 'last_update_ip', 'tahap', 'kode_misi', 'kode_tujuan', 'ranking', 'nomor13', 'ppa_nama', 'ppa_pangkat', 'ppa_nip', 'lanjutan', 'user_id', 'id', 'tahun', 'tambahan_pagu', 'gender', 'kode_keg_keuangan', 'user_id_lama', 'indikator', 'is_dak', 'kode_kegiatan_asal', 'kode_keg_keuangan_asal', 'th_ke_multiyears', 'kelompok_sasaran', 'pagu_bappeko', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('UnitId' => 0, 'KodeKegiatan' => 1, 'KodeBidang' => 2, 'KodeUrusanWajib' => 3, 'KodeProgram' => 4, 'KodeSasaran' => 5, 'KodeIndikator' => 6, 'AlokasiDana' => 7, 'NamaKegiatan' => 8, 'Masukan' => 9, 'Output' => 10, 'Outcome' => 11, 'Benefit' => 12, 'Impact' => 13, 'Tipe' => 14, 'KegiatanActive' => 15, 'ToKegiatanCode' => 16, 'Catatan' => 17, 'TargetOutcome' => 18, 'Lokasi' => 19, 'JumlahPrev' => 20, 'JumlahNow' => 21, 'JumlahNext' => 22, 'KodeProgram2' => 23, 'KodeUrusan' => 24, 'LastUpdateUser' => 25, 'LastUpdateTime' => 26, 'LastUpdateIp' => 27, 'Tahap' => 28, 'KodeMisi' => 29, 'KodeTujuan' => 30, 'Ranking' => 31, 'Nomor13' => 32, 'PpaNama' => 33, 'PpaPangkat' => 34, 'PpaNip' => 35, 'Lanjutan' => 36, 'UserId' => 37, 'Id' => 38, 'Tahun' => 39, 'TambahanPagu' => 40, 'Gender' => 41, 'KodeKegKeuangan' => 42, 'UserIdLama' => 43, 'Indikator' => 44, 'IsDak' => 45, 'KodeKegiatanAsal' => 46, 'KodeKegKeuanganAsal' => 47, 'ThKeMultiyears' => 48, 'KelompokSasaran' => 49, 'PaguBappeko' => 50, ),
		BasePeer::TYPE_COLNAME => array (BukuProvinsiMasterKegiatanPeer::UNIT_ID => 0, BukuProvinsiMasterKegiatanPeer::KODE_KEGIATAN => 1, BukuProvinsiMasterKegiatanPeer::KODE_BIDANG => 2, BukuProvinsiMasterKegiatanPeer::KODE_URUSAN_WAJIB => 3, BukuProvinsiMasterKegiatanPeer::KODE_PROGRAM => 4, BukuProvinsiMasterKegiatanPeer::KODE_SASARAN => 5, BukuProvinsiMasterKegiatanPeer::KODE_INDIKATOR => 6, BukuProvinsiMasterKegiatanPeer::ALOKASI_DANA => 7, BukuProvinsiMasterKegiatanPeer::NAMA_KEGIATAN => 8, BukuProvinsiMasterKegiatanPeer::MASUKAN => 9, BukuProvinsiMasterKegiatanPeer::OUTPUT => 10, BukuProvinsiMasterKegiatanPeer::OUTCOME => 11, BukuProvinsiMasterKegiatanPeer::BENEFIT => 12, BukuProvinsiMasterKegiatanPeer::IMPACT => 13, BukuProvinsiMasterKegiatanPeer::TIPE => 14, BukuProvinsiMasterKegiatanPeer::KEGIATAN_ACTIVE => 15, BukuProvinsiMasterKegiatanPeer::TO_KEGIATAN_CODE => 16, BukuProvinsiMasterKegiatanPeer::CATATAN => 17, BukuProvinsiMasterKegiatanPeer::TARGET_OUTCOME => 18, BukuProvinsiMasterKegiatanPeer::LOKASI => 19, BukuProvinsiMasterKegiatanPeer::JUMLAH_PREV => 20, BukuProvinsiMasterKegiatanPeer::JUMLAH_NOW => 21, BukuProvinsiMasterKegiatanPeer::JUMLAH_NEXT => 22, BukuProvinsiMasterKegiatanPeer::KODE_PROGRAM2 => 23, BukuProvinsiMasterKegiatanPeer::KODE_URUSAN => 24, BukuProvinsiMasterKegiatanPeer::LAST_UPDATE_USER => 25, BukuProvinsiMasterKegiatanPeer::LAST_UPDATE_TIME => 26, BukuProvinsiMasterKegiatanPeer::LAST_UPDATE_IP => 27, BukuProvinsiMasterKegiatanPeer::TAHAP => 28, BukuProvinsiMasterKegiatanPeer::KODE_MISI => 29, BukuProvinsiMasterKegiatanPeer::KODE_TUJUAN => 30, BukuProvinsiMasterKegiatanPeer::RANKING => 31, BukuProvinsiMasterKegiatanPeer::NOMOR13 => 32, BukuProvinsiMasterKegiatanPeer::PPA_NAMA => 33, BukuProvinsiMasterKegiatanPeer::PPA_PANGKAT => 34, BukuProvinsiMasterKegiatanPeer::PPA_NIP => 35, BukuProvinsiMasterKegiatanPeer::LANJUTAN => 36, BukuProvinsiMasterKegiatanPeer::USER_ID => 37, BukuProvinsiMasterKegiatanPeer::ID => 38, BukuProvinsiMasterKegiatanPeer::TAHUN => 39, BukuProvinsiMasterKegiatanPeer::TAMBAHAN_PAGU => 40, BukuProvinsiMasterKegiatanPeer::GENDER => 41, BukuProvinsiMasterKegiatanPeer::KODE_KEG_KEUANGAN => 42, BukuProvinsiMasterKegiatanPeer::USER_ID_LAMA => 43, BukuProvinsiMasterKegiatanPeer::INDIKATOR => 44, BukuProvinsiMasterKegiatanPeer::IS_DAK => 45, BukuProvinsiMasterKegiatanPeer::KODE_KEGIATAN_ASAL => 46, BukuProvinsiMasterKegiatanPeer::KODE_KEG_KEUANGAN_ASAL => 47, BukuProvinsiMasterKegiatanPeer::TH_KE_MULTIYEARS => 48, BukuProvinsiMasterKegiatanPeer::KELOMPOK_SASARAN => 49, BukuProvinsiMasterKegiatanPeer::PAGU_BAPPEKO => 50, ),
		BasePeer::TYPE_FIELDNAME => array ('unit_id' => 0, 'kode_kegiatan' => 1, 'kode_bidang' => 2, 'kode_urusan_wajib' => 3, 'kode_program' => 4, 'kode_sasaran' => 5, 'kode_indikator' => 6, 'alokasi_dana' => 7, 'nama_kegiatan' => 8, 'masukan' => 9, 'output' => 10, 'outcome' => 11, 'benefit' => 12, 'impact' => 13, 'tipe' => 14, 'kegiatan_active' => 15, 'to_kegiatan_code' => 16, 'catatan' => 17, 'target_outcome' => 18, 'lokasi' => 19, 'jumlah_prev' => 20, 'jumlah_now' => 21, 'jumlah_next' => 22, 'kode_program2' => 23, 'kode_urusan' => 24, 'last_update_user' => 25, 'last_update_time' => 26, 'last_update_ip' => 27, 'tahap' => 28, 'kode_misi' => 29, 'kode_tujuan' => 30, 'ranking' => 31, 'nomor13' => 32, 'ppa_nama' => 33, 'ppa_pangkat' => 34, 'ppa_nip' => 35, 'lanjutan' => 36, 'user_id' => 37, 'id' => 38, 'tahun' => 39, 'tambahan_pagu' => 40, 'gender' => 41, 'kode_keg_keuangan' => 42, 'user_id_lama' => 43, 'indikator' => 44, 'is_dak' => 45, 'kode_kegiatan_asal' => 46, 'kode_keg_keuangan_asal' => 47, 'th_ke_multiyears' => 48, 'kelompok_sasaran' => 49, 'pagu_bappeko' => 50, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/BukuProvinsiMasterKegiatanMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.BukuProvinsiMasterKegiatanMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = BukuProvinsiMasterKegiatanPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(BukuProvinsiMasterKegiatanPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(BukuProvinsiMasterKegiatanPeer::UNIT_ID);

		$criteria->addSelectColumn(BukuProvinsiMasterKegiatanPeer::KODE_KEGIATAN);

		$criteria->addSelectColumn(BukuProvinsiMasterKegiatanPeer::KODE_BIDANG);

		$criteria->addSelectColumn(BukuProvinsiMasterKegiatanPeer::KODE_URUSAN_WAJIB);

		$criteria->addSelectColumn(BukuProvinsiMasterKegiatanPeer::KODE_PROGRAM);

		$criteria->addSelectColumn(BukuProvinsiMasterKegiatanPeer::KODE_SASARAN);

		$criteria->addSelectColumn(BukuProvinsiMasterKegiatanPeer::KODE_INDIKATOR);

		$criteria->addSelectColumn(BukuProvinsiMasterKegiatanPeer::ALOKASI_DANA);

		$criteria->addSelectColumn(BukuProvinsiMasterKegiatanPeer::NAMA_KEGIATAN);

		$criteria->addSelectColumn(BukuProvinsiMasterKegiatanPeer::MASUKAN);

		$criteria->addSelectColumn(BukuProvinsiMasterKegiatanPeer::OUTPUT);

		$criteria->addSelectColumn(BukuProvinsiMasterKegiatanPeer::OUTCOME);

		$criteria->addSelectColumn(BukuProvinsiMasterKegiatanPeer::BENEFIT);

		$criteria->addSelectColumn(BukuProvinsiMasterKegiatanPeer::IMPACT);

		$criteria->addSelectColumn(BukuProvinsiMasterKegiatanPeer::TIPE);

		$criteria->addSelectColumn(BukuProvinsiMasterKegiatanPeer::KEGIATAN_ACTIVE);

		$criteria->addSelectColumn(BukuProvinsiMasterKegiatanPeer::TO_KEGIATAN_CODE);

		$criteria->addSelectColumn(BukuProvinsiMasterKegiatanPeer::CATATAN);

		$criteria->addSelectColumn(BukuProvinsiMasterKegiatanPeer::TARGET_OUTCOME);

		$criteria->addSelectColumn(BukuProvinsiMasterKegiatanPeer::LOKASI);

		$criteria->addSelectColumn(BukuProvinsiMasterKegiatanPeer::JUMLAH_PREV);

		$criteria->addSelectColumn(BukuProvinsiMasterKegiatanPeer::JUMLAH_NOW);

		$criteria->addSelectColumn(BukuProvinsiMasterKegiatanPeer::JUMLAH_NEXT);

		$criteria->addSelectColumn(BukuProvinsiMasterKegiatanPeer::KODE_PROGRAM2);

		$criteria->addSelectColumn(BukuProvinsiMasterKegiatanPeer::KODE_URUSAN);

		$criteria->addSelectColumn(BukuProvinsiMasterKegiatanPeer::LAST_UPDATE_USER);

		$criteria->addSelectColumn(BukuProvinsiMasterKegiatanPeer::LAST_UPDATE_TIME);

		$criteria->addSelectColumn(BukuProvinsiMasterKegiatanPeer::LAST_UPDATE_IP);

		$criteria->addSelectColumn(BukuProvinsiMasterKegiatanPeer::TAHAP);

		$criteria->addSelectColumn(BukuProvinsiMasterKegiatanPeer::KODE_MISI);

		$criteria->addSelectColumn(BukuProvinsiMasterKegiatanPeer::KODE_TUJUAN);

		$criteria->addSelectColumn(BukuProvinsiMasterKegiatanPeer::RANKING);

		$criteria->addSelectColumn(BukuProvinsiMasterKegiatanPeer::NOMOR13);

		$criteria->addSelectColumn(BukuProvinsiMasterKegiatanPeer::PPA_NAMA);

		$criteria->addSelectColumn(BukuProvinsiMasterKegiatanPeer::PPA_PANGKAT);

		$criteria->addSelectColumn(BukuProvinsiMasterKegiatanPeer::PPA_NIP);

		$criteria->addSelectColumn(BukuProvinsiMasterKegiatanPeer::LANJUTAN);

		$criteria->addSelectColumn(BukuProvinsiMasterKegiatanPeer::USER_ID);

		$criteria->addSelectColumn(BukuProvinsiMasterKegiatanPeer::ID);

		$criteria->addSelectColumn(BukuProvinsiMasterKegiatanPeer::TAHUN);

		$criteria->addSelectColumn(BukuProvinsiMasterKegiatanPeer::TAMBAHAN_PAGU);

		$criteria->addSelectColumn(BukuProvinsiMasterKegiatanPeer::GENDER);

		$criteria->addSelectColumn(BukuProvinsiMasterKegiatanPeer::KODE_KEG_KEUANGAN);

		$criteria->addSelectColumn(BukuProvinsiMasterKegiatanPeer::USER_ID_LAMA);

		$criteria->addSelectColumn(BukuProvinsiMasterKegiatanPeer::INDIKATOR);

		$criteria->addSelectColumn(BukuProvinsiMasterKegiatanPeer::IS_DAK);

		$criteria->addSelectColumn(BukuProvinsiMasterKegiatanPeer::KODE_KEGIATAN_ASAL);

		$criteria->addSelectColumn(BukuProvinsiMasterKegiatanPeer::KODE_KEG_KEUANGAN_ASAL);

		$criteria->addSelectColumn(BukuProvinsiMasterKegiatanPeer::TH_KE_MULTIYEARS);

		$criteria->addSelectColumn(BukuProvinsiMasterKegiatanPeer::KELOMPOK_SASARAN);

		$criteria->addSelectColumn(BukuProvinsiMasterKegiatanPeer::PAGU_BAPPEKO);

	}

	const COUNT = 'COUNT(ebudget.buku_provinsi_master_kegiatan.UNIT_ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.buku_provinsi_master_kegiatan.UNIT_ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(BukuProvinsiMasterKegiatanPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(BukuProvinsiMasterKegiatanPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = BukuProvinsiMasterKegiatanPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = BukuProvinsiMasterKegiatanPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return BukuProvinsiMasterKegiatanPeer::populateObjects(BukuProvinsiMasterKegiatanPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			BukuProvinsiMasterKegiatanPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = BukuProvinsiMasterKegiatanPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return BukuProvinsiMasterKegiatanPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}

		$criteria->remove(BukuProvinsiMasterKegiatanPeer::ID); 

				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(BukuProvinsiMasterKegiatanPeer::UNIT_ID);
			$selectCriteria->add(BukuProvinsiMasterKegiatanPeer::UNIT_ID, $criteria->remove(BukuProvinsiMasterKegiatanPeer::UNIT_ID), $comparison);

			$comparison = $criteria->getComparison(BukuProvinsiMasterKegiatanPeer::KODE_KEGIATAN);
			$selectCriteria->add(BukuProvinsiMasterKegiatanPeer::KODE_KEGIATAN, $criteria->remove(BukuProvinsiMasterKegiatanPeer::KODE_KEGIATAN), $comparison);

			$comparison = $criteria->getComparison(BukuProvinsiMasterKegiatanPeer::ID);
			$selectCriteria->add(BukuProvinsiMasterKegiatanPeer::ID, $criteria->remove(BukuProvinsiMasterKegiatanPeer::ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(BukuProvinsiMasterKegiatanPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(BukuProvinsiMasterKegiatanPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof BukuProvinsiMasterKegiatan) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
												if(count($values) == count($values, COUNT_RECURSIVE))
			{
								$values = array($values);
			}
			$vals = array();
			foreach($values as $value)
			{

				$vals[0][] = $value[0];
				$vals[1][] = $value[1];
				$vals[2][] = $value[2];
			}

			$criteria->add(BukuProvinsiMasterKegiatanPeer::UNIT_ID, $vals[0], Criteria::IN);
			$criteria->add(BukuProvinsiMasterKegiatanPeer::KODE_KEGIATAN, $vals[1], Criteria::IN);
			$criteria->add(BukuProvinsiMasterKegiatanPeer::ID, $vals[2], Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(BukuProvinsiMasterKegiatan $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(BukuProvinsiMasterKegiatanPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(BukuProvinsiMasterKegiatanPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(BukuProvinsiMasterKegiatanPeer::DATABASE_NAME, BukuProvinsiMasterKegiatanPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = BukuProvinsiMasterKegiatanPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK( $unit_id, $kode_kegiatan, $id, $con = null) {
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$criteria = new Criteria();
		$criteria->add(BukuProvinsiMasterKegiatanPeer::UNIT_ID, $unit_id);
		$criteria->add(BukuProvinsiMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
		$criteria->add(BukuProvinsiMasterKegiatanPeer::ID, $id);
		$v = BukuProvinsiMasterKegiatanPeer::doSelect($criteria, $con);

		return !empty($v) ? $v[0] : null;
	}
} 
if (Propel::isInit()) {
			try {
		BaseBukuProvinsiMasterKegiatanPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/BukuProvinsiMasterKegiatanMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.BukuProvinsiMasterKegiatanMapBuilder');
}
