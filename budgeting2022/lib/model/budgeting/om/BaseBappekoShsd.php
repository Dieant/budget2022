<?php


abstract class BaseBappekoShsd extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $shsd_id;


	
	protected $satuan;


	
	protected $shsd_name;


	
	protected $shsd_harga;


	
	protected $shsd_harga_dasar;


	
	protected $shsd_show;


	
	protected $shsd_faktor_inflasi;


	
	protected $shsd_locked = false;


	
	protected $rekening_code;


	
	protected $shsd_merk;


	
	protected $non_pajak = false;


	
	protected $shsd_id_old;


	
	protected $nama_dasar;


	
	protected $spec;


	
	protected $hidden_spec;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getShsdId()
	{

		return $this->shsd_id;
	}

	
	public function getSatuan()
	{

		return $this->satuan;
	}

	
	public function getShsdName()
	{

		return $this->shsd_name;
	}

	
	public function getShsdHarga()
	{

		return $this->shsd_harga;
	}

	
	public function getShsdHargaDasar()
	{

		return $this->shsd_harga_dasar;
	}

	
	public function getShsdShow()
	{

		return $this->shsd_show;
	}

	
	public function getShsdFaktorInflasi()
	{

		return $this->shsd_faktor_inflasi;
	}

	
	public function getShsdLocked()
	{

		return $this->shsd_locked;
	}

	
	public function getRekeningCode()
	{

		return $this->rekening_code;
	}

	
	public function getShsdMerk()
	{

		return $this->shsd_merk;
	}

	
	public function getNonPajak()
	{

		return $this->non_pajak;
	}

	
	public function getShsdIdOld()
	{

		return $this->shsd_id_old;
	}

	
	public function getNamaDasar()
	{

		return $this->nama_dasar;
	}

	
	public function getSpec()
	{

		return $this->spec;
	}

	
	public function getHiddenSpec()
	{

		return $this->hidden_spec;
	}

	
	public function setShsdId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->shsd_id !== $v) {
			$this->shsd_id = $v;
			$this->modifiedColumns[] = BappekoShsdPeer::SHSD_ID;
		}

	} 
	
	public function setSatuan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->satuan !== $v) {
			$this->satuan = $v;
			$this->modifiedColumns[] = BappekoShsdPeer::SATUAN;
		}

	} 
	
	public function setShsdName($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->shsd_name !== $v) {
			$this->shsd_name = $v;
			$this->modifiedColumns[] = BappekoShsdPeer::SHSD_NAME;
		}

	} 
	
	public function setShsdHarga($v)
	{

		if ($this->shsd_harga !== $v) {
			$this->shsd_harga = $v;
			$this->modifiedColumns[] = BappekoShsdPeer::SHSD_HARGA;
		}

	} 
	
	public function setShsdHargaDasar($v)
	{

		if ($this->shsd_harga_dasar !== $v) {
			$this->shsd_harga_dasar = $v;
			$this->modifiedColumns[] = BappekoShsdPeer::SHSD_HARGA_DASAR;
		}

	} 
	
	public function setShsdShow($v)
	{

		if ($this->shsd_show !== $v) {
			$this->shsd_show = $v;
			$this->modifiedColumns[] = BappekoShsdPeer::SHSD_SHOW;
		}

	} 
	
	public function setShsdFaktorInflasi($v)
	{

		if ($this->shsd_faktor_inflasi !== $v) {
			$this->shsd_faktor_inflasi = $v;
			$this->modifiedColumns[] = BappekoShsdPeer::SHSD_FAKTOR_INFLASI;
		}

	} 
	
	public function setShsdLocked($v)
	{

		if ($this->shsd_locked !== $v || $v === false) {
			$this->shsd_locked = $v;
			$this->modifiedColumns[] = BappekoShsdPeer::SHSD_LOCKED;
		}

	} 
	
	public function setRekeningCode($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->rekening_code !== $v) {
			$this->rekening_code = $v;
			$this->modifiedColumns[] = BappekoShsdPeer::REKENING_CODE;
		}

	} 
	
	public function setShsdMerk($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->shsd_merk !== $v) {
			$this->shsd_merk = $v;
			$this->modifiedColumns[] = BappekoShsdPeer::SHSD_MERK;
		}

	} 
	
	public function setNonPajak($v)
	{

		if ($this->non_pajak !== $v || $v === false) {
			$this->non_pajak = $v;
			$this->modifiedColumns[] = BappekoShsdPeer::NON_PAJAK;
		}

	} 
	
	public function setShsdIdOld($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->shsd_id_old !== $v) {
			$this->shsd_id_old = $v;
			$this->modifiedColumns[] = BappekoShsdPeer::SHSD_ID_OLD;
		}

	} 
	
	public function setNamaDasar($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nama_dasar !== $v) {
			$this->nama_dasar = $v;
			$this->modifiedColumns[] = BappekoShsdPeer::NAMA_DASAR;
		}

	} 
	
	public function setSpec($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->spec !== $v) {
			$this->spec = $v;
			$this->modifiedColumns[] = BappekoShsdPeer::SPEC;
		}

	} 
	
	public function setHiddenSpec($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->hidden_spec !== $v) {
			$this->hidden_spec = $v;
			$this->modifiedColumns[] = BappekoShsdPeer::HIDDEN_SPEC;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->shsd_id = $rs->getString($startcol + 0);

			$this->satuan = $rs->getString($startcol + 1);

			$this->shsd_name = $rs->getString($startcol + 2);

			$this->shsd_harga = $rs->getFloat($startcol + 3);

			$this->shsd_harga_dasar = $rs->getFloat($startcol + 4);

			$this->shsd_show = $rs->getBoolean($startcol + 5);

			$this->shsd_faktor_inflasi = $rs->getFloat($startcol + 6);

			$this->shsd_locked = $rs->getBoolean($startcol + 7);

			$this->rekening_code = $rs->getString($startcol + 8);

			$this->shsd_merk = $rs->getString($startcol + 9);

			$this->non_pajak = $rs->getBoolean($startcol + 10);

			$this->shsd_id_old = $rs->getString($startcol + 11);

			$this->nama_dasar = $rs->getString($startcol + 12);

			$this->spec = $rs->getString($startcol + 13);

			$this->hidden_spec = $rs->getString($startcol + 14);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 15; 
		} catch (Exception $e) {
			throw new PropelException("Error populating BappekoShsd object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(BappekoShsdPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			BappekoShsdPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(BappekoShsdPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = BappekoShsdPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += BappekoShsdPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = BappekoShsdPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = BappekoShsdPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getShsdId();
				break;
			case 1:
				return $this->getSatuan();
				break;
			case 2:
				return $this->getShsdName();
				break;
			case 3:
				return $this->getShsdHarga();
				break;
			case 4:
				return $this->getShsdHargaDasar();
				break;
			case 5:
				return $this->getShsdShow();
				break;
			case 6:
				return $this->getShsdFaktorInflasi();
				break;
			case 7:
				return $this->getShsdLocked();
				break;
			case 8:
				return $this->getRekeningCode();
				break;
			case 9:
				return $this->getShsdMerk();
				break;
			case 10:
				return $this->getNonPajak();
				break;
			case 11:
				return $this->getShsdIdOld();
				break;
			case 12:
				return $this->getNamaDasar();
				break;
			case 13:
				return $this->getSpec();
				break;
			case 14:
				return $this->getHiddenSpec();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = BappekoShsdPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getShsdId(),
			$keys[1] => $this->getSatuan(),
			$keys[2] => $this->getShsdName(),
			$keys[3] => $this->getShsdHarga(),
			$keys[4] => $this->getShsdHargaDasar(),
			$keys[5] => $this->getShsdShow(),
			$keys[6] => $this->getShsdFaktorInflasi(),
			$keys[7] => $this->getShsdLocked(),
			$keys[8] => $this->getRekeningCode(),
			$keys[9] => $this->getShsdMerk(),
			$keys[10] => $this->getNonPajak(),
			$keys[11] => $this->getShsdIdOld(),
			$keys[12] => $this->getNamaDasar(),
			$keys[13] => $this->getSpec(),
			$keys[14] => $this->getHiddenSpec(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = BappekoShsdPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setShsdId($value);
				break;
			case 1:
				$this->setSatuan($value);
				break;
			case 2:
				$this->setShsdName($value);
				break;
			case 3:
				$this->setShsdHarga($value);
				break;
			case 4:
				$this->setShsdHargaDasar($value);
				break;
			case 5:
				$this->setShsdShow($value);
				break;
			case 6:
				$this->setShsdFaktorInflasi($value);
				break;
			case 7:
				$this->setShsdLocked($value);
				break;
			case 8:
				$this->setRekeningCode($value);
				break;
			case 9:
				$this->setShsdMerk($value);
				break;
			case 10:
				$this->setNonPajak($value);
				break;
			case 11:
				$this->setShsdIdOld($value);
				break;
			case 12:
				$this->setNamaDasar($value);
				break;
			case 13:
				$this->setSpec($value);
				break;
			case 14:
				$this->setHiddenSpec($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = BappekoShsdPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setShsdId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setSatuan($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setShsdName($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setShsdHarga($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setShsdHargaDasar($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setShsdShow($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setShsdFaktorInflasi($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setShsdLocked($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setRekeningCode($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setShsdMerk($arr[$keys[9]]);
		if (array_key_exists($keys[10], $arr)) $this->setNonPajak($arr[$keys[10]]);
		if (array_key_exists($keys[11], $arr)) $this->setShsdIdOld($arr[$keys[11]]);
		if (array_key_exists($keys[12], $arr)) $this->setNamaDasar($arr[$keys[12]]);
		if (array_key_exists($keys[13], $arr)) $this->setSpec($arr[$keys[13]]);
		if (array_key_exists($keys[14], $arr)) $this->setHiddenSpec($arr[$keys[14]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(BappekoShsdPeer::DATABASE_NAME);

		if ($this->isColumnModified(BappekoShsdPeer::SHSD_ID)) $criteria->add(BappekoShsdPeer::SHSD_ID, $this->shsd_id);
		if ($this->isColumnModified(BappekoShsdPeer::SATUAN)) $criteria->add(BappekoShsdPeer::SATUAN, $this->satuan);
		if ($this->isColumnModified(BappekoShsdPeer::SHSD_NAME)) $criteria->add(BappekoShsdPeer::SHSD_NAME, $this->shsd_name);
		if ($this->isColumnModified(BappekoShsdPeer::SHSD_HARGA)) $criteria->add(BappekoShsdPeer::SHSD_HARGA, $this->shsd_harga);
		if ($this->isColumnModified(BappekoShsdPeer::SHSD_HARGA_DASAR)) $criteria->add(BappekoShsdPeer::SHSD_HARGA_DASAR, $this->shsd_harga_dasar);
		if ($this->isColumnModified(BappekoShsdPeer::SHSD_SHOW)) $criteria->add(BappekoShsdPeer::SHSD_SHOW, $this->shsd_show);
		if ($this->isColumnModified(BappekoShsdPeer::SHSD_FAKTOR_INFLASI)) $criteria->add(BappekoShsdPeer::SHSD_FAKTOR_INFLASI, $this->shsd_faktor_inflasi);
		if ($this->isColumnModified(BappekoShsdPeer::SHSD_LOCKED)) $criteria->add(BappekoShsdPeer::SHSD_LOCKED, $this->shsd_locked);
		if ($this->isColumnModified(BappekoShsdPeer::REKENING_CODE)) $criteria->add(BappekoShsdPeer::REKENING_CODE, $this->rekening_code);
		if ($this->isColumnModified(BappekoShsdPeer::SHSD_MERK)) $criteria->add(BappekoShsdPeer::SHSD_MERK, $this->shsd_merk);
		if ($this->isColumnModified(BappekoShsdPeer::NON_PAJAK)) $criteria->add(BappekoShsdPeer::NON_PAJAK, $this->non_pajak);
		if ($this->isColumnModified(BappekoShsdPeer::SHSD_ID_OLD)) $criteria->add(BappekoShsdPeer::SHSD_ID_OLD, $this->shsd_id_old);
		if ($this->isColumnModified(BappekoShsdPeer::NAMA_DASAR)) $criteria->add(BappekoShsdPeer::NAMA_DASAR, $this->nama_dasar);
		if ($this->isColumnModified(BappekoShsdPeer::SPEC)) $criteria->add(BappekoShsdPeer::SPEC, $this->spec);
		if ($this->isColumnModified(BappekoShsdPeer::HIDDEN_SPEC)) $criteria->add(BappekoShsdPeer::HIDDEN_SPEC, $this->hidden_spec);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(BappekoShsdPeer::DATABASE_NAME);

		$criteria->add(BappekoShsdPeer::SHSD_ID, $this->shsd_id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getShsdId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setShsdId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setSatuan($this->satuan);

		$copyObj->setShsdName($this->shsd_name);

		$copyObj->setShsdHarga($this->shsd_harga);

		$copyObj->setShsdHargaDasar($this->shsd_harga_dasar);

		$copyObj->setShsdShow($this->shsd_show);

		$copyObj->setShsdFaktorInflasi($this->shsd_faktor_inflasi);

		$copyObj->setShsdLocked($this->shsd_locked);

		$copyObj->setRekeningCode($this->rekening_code);

		$copyObj->setShsdMerk($this->shsd_merk);

		$copyObj->setNonPajak($this->non_pajak);

		$copyObj->setShsdIdOld($this->shsd_id_old);

		$copyObj->setNamaDasar($this->nama_dasar);

		$copyObj->setSpec($this->spec);

		$copyObj->setHiddenSpec($this->hidden_spec);


		$copyObj->setNew(true);

		$copyObj->setShsdId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new BappekoShsdPeer();
		}
		return self::$peer;
	}

} 