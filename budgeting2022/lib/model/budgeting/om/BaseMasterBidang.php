<?php


abstract class BaseMasterBidang extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $kode_bidang;


	
	protected $nama_bidang;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getKodeBidang()
	{

		return $this->kode_bidang;
	}

	
	public function getNamaBidang()
	{

		return $this->nama_bidang;
	}

	
	public function setKodeBidang($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_bidang !== $v) {
			$this->kode_bidang = $v;
			$this->modifiedColumns[] = MasterBidangPeer::KODE_BIDANG;
		}

	} 
	
	public function setNamaBidang($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nama_bidang !== $v) {
			$this->nama_bidang = $v;
			$this->modifiedColumns[] = MasterBidangPeer::NAMA_BIDANG;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->kode_bidang = $rs->getString($startcol + 0);

			$this->nama_bidang = $rs->getString($startcol + 1);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 2; 
		} catch (Exception $e) {
			throw new PropelException("Error populating MasterBidang object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(MasterBidangPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			MasterBidangPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(MasterBidangPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = MasterBidangPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += MasterBidangPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = MasterBidangPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = MasterBidangPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getKodeBidang();
				break;
			case 1:
				return $this->getNamaBidang();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = MasterBidangPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getKodeBidang(),
			$keys[1] => $this->getNamaBidang(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = MasterBidangPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setKodeBidang($value);
				break;
			case 1:
				$this->setNamaBidang($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = MasterBidangPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setKodeBidang($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setNamaBidang($arr[$keys[1]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(MasterBidangPeer::DATABASE_NAME);

		if ($this->isColumnModified(MasterBidangPeer::KODE_BIDANG)) $criteria->add(MasterBidangPeer::KODE_BIDANG, $this->kode_bidang);
		if ($this->isColumnModified(MasterBidangPeer::NAMA_BIDANG)) $criteria->add(MasterBidangPeer::NAMA_BIDANG, $this->nama_bidang);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(MasterBidangPeer::DATABASE_NAME);

		$criteria->add(MasterBidangPeer::KODE_BIDANG, $this->kode_bidang);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getKodeBidang();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setKodeBidang($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setNamaBidang($this->nama_bidang);


		$copyObj->setNew(true);

		$copyObj->setKodeBidang(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new MasterBidangPeer();
		}
		return self::$peer;
	}

} 