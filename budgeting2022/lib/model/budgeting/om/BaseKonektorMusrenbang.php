<?php


abstract class BaseKonektorMusrenbang extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id;


	
	protected $kode_devplan;


	
	protected $nama_devplan;


	
	protected $satuan_devplan;


	
	protected $komponen_id;


	
	protected $komponen_rekening;


	
	protected $harga_devplan;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getId()
	{

		return $this->id;
	}

	
	public function getKodeDevplan()
	{

		return $this->kode_devplan;
	}

	
	public function getNamaDevplan()
	{

		return $this->nama_devplan;
	}

	
	public function getSatuanDevplan()
	{

		return $this->satuan_devplan;
	}

	
	public function getKomponenId()
	{

		return $this->komponen_id;
	}

	
	public function getKomponenRekening()
	{

		return $this->komponen_rekening;
	}

	
	public function getHargaDevplan()
	{

		return $this->harga_devplan;
	}

	
	public function setId($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = KonektorMusrenbangPeer::ID;
		}

	} 
	
	public function setKodeDevplan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_devplan !== $v) {
			$this->kode_devplan = $v;
			$this->modifiedColumns[] = KonektorMusrenbangPeer::KODE_DEVPLAN;
		}

	} 
	
	public function setNamaDevplan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nama_devplan !== $v) {
			$this->nama_devplan = $v;
			$this->modifiedColumns[] = KonektorMusrenbangPeer::NAMA_DEVPLAN;
		}

	} 
	
	public function setSatuanDevplan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->satuan_devplan !== $v) {
			$this->satuan_devplan = $v;
			$this->modifiedColumns[] = KonektorMusrenbangPeer::SATUAN_DEVPLAN;
		}

	} 
	
	public function setKomponenId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->komponen_id !== $v) {
			$this->komponen_id = $v;
			$this->modifiedColumns[] = KonektorMusrenbangPeer::KOMPONEN_ID;
		}

	} 
	
	public function setKomponenRekening($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->komponen_rekening !== $v) {
			$this->komponen_rekening = $v;
			$this->modifiedColumns[] = KonektorMusrenbangPeer::KOMPONEN_REKENING;
		}

	} 
	
	public function setHargaDevplan($v)
	{

		if ($this->harga_devplan !== $v) {
			$this->harga_devplan = $v;
			$this->modifiedColumns[] = KonektorMusrenbangPeer::HARGA_DEVPLAN;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getInt($startcol + 0);

			$this->kode_devplan = $rs->getString($startcol + 1);

			$this->nama_devplan = $rs->getString($startcol + 2);

			$this->satuan_devplan = $rs->getString($startcol + 3);

			$this->komponen_id = $rs->getString($startcol + 4);

			$this->komponen_rekening = $rs->getString($startcol + 5);

			$this->harga_devplan = $rs->getFloat($startcol + 6);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 7; 
		} catch (Exception $e) {
			throw new PropelException("Error populating KonektorMusrenbang object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(KonektorMusrenbangPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			KonektorMusrenbangPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(KonektorMusrenbangPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = KonektorMusrenbangPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += KonektorMusrenbangPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = KonektorMusrenbangPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = KonektorMusrenbangPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getKodeDevplan();
				break;
			case 2:
				return $this->getNamaDevplan();
				break;
			case 3:
				return $this->getSatuanDevplan();
				break;
			case 4:
				return $this->getKomponenId();
				break;
			case 5:
				return $this->getKomponenRekening();
				break;
			case 6:
				return $this->getHargaDevplan();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = KonektorMusrenbangPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getKodeDevplan(),
			$keys[2] => $this->getNamaDevplan(),
			$keys[3] => $this->getSatuanDevplan(),
			$keys[4] => $this->getKomponenId(),
			$keys[5] => $this->getKomponenRekening(),
			$keys[6] => $this->getHargaDevplan(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = KonektorMusrenbangPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setKodeDevplan($value);
				break;
			case 2:
				$this->setNamaDevplan($value);
				break;
			case 3:
				$this->setSatuanDevplan($value);
				break;
			case 4:
				$this->setKomponenId($value);
				break;
			case 5:
				$this->setKomponenRekening($value);
				break;
			case 6:
				$this->setHargaDevplan($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = KonektorMusrenbangPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setKodeDevplan($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setNamaDevplan($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setSatuanDevplan($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setKomponenId($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setKomponenRekening($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setHargaDevplan($arr[$keys[6]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(KonektorMusrenbangPeer::DATABASE_NAME);

		if ($this->isColumnModified(KonektorMusrenbangPeer::ID)) $criteria->add(KonektorMusrenbangPeer::ID, $this->id);
		if ($this->isColumnModified(KonektorMusrenbangPeer::KODE_DEVPLAN)) $criteria->add(KonektorMusrenbangPeer::KODE_DEVPLAN, $this->kode_devplan);
		if ($this->isColumnModified(KonektorMusrenbangPeer::NAMA_DEVPLAN)) $criteria->add(KonektorMusrenbangPeer::NAMA_DEVPLAN, $this->nama_devplan);
		if ($this->isColumnModified(KonektorMusrenbangPeer::SATUAN_DEVPLAN)) $criteria->add(KonektorMusrenbangPeer::SATUAN_DEVPLAN, $this->satuan_devplan);
		if ($this->isColumnModified(KonektorMusrenbangPeer::KOMPONEN_ID)) $criteria->add(KonektorMusrenbangPeer::KOMPONEN_ID, $this->komponen_id);
		if ($this->isColumnModified(KonektorMusrenbangPeer::KOMPONEN_REKENING)) $criteria->add(KonektorMusrenbangPeer::KOMPONEN_REKENING, $this->komponen_rekening);
		if ($this->isColumnModified(KonektorMusrenbangPeer::HARGA_DEVPLAN)) $criteria->add(KonektorMusrenbangPeer::HARGA_DEVPLAN, $this->harga_devplan);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(KonektorMusrenbangPeer::DATABASE_NAME);

		$criteria->add(KonektorMusrenbangPeer::ID, $this->id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setKodeDevplan($this->kode_devplan);

		$copyObj->setNamaDevplan($this->nama_devplan);

		$copyObj->setSatuanDevplan($this->satuan_devplan);

		$copyObj->setKomponenId($this->komponen_id);

		$copyObj->setKomponenRekening($this->komponen_rekening);

		$copyObj->setHargaDevplan($this->harga_devplan);


		$copyObj->setNew(true);

		$copyObj->setId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new KonektorMusrenbangPeer();
		}
		return self::$peer;
	}

} 