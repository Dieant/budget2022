<?php


abstract class BaseBappekoRincianDetailPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.bappeko_rincian_detail';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.BappekoRincianDetail';

	
	const NUM_COLUMNS = 38;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const KEGIATAN_CODE = 'ebudget.bappeko_rincian_detail.KEGIATAN_CODE';

	
	const TIPE = 'ebudget.bappeko_rincian_detail.TIPE';

	
	const DETAIL_NO = 'ebudget.bappeko_rincian_detail.DETAIL_NO';

	
	const REKENING_CODE = 'ebudget.bappeko_rincian_detail.REKENING_CODE';

	
	const KOMPONEN_ID = 'ebudget.bappeko_rincian_detail.KOMPONEN_ID';

	
	const DETAIL_NAME = 'ebudget.bappeko_rincian_detail.DETAIL_NAME';

	
	const VOLUME = 'ebudget.bappeko_rincian_detail.VOLUME';

	
	const KETERANGAN_KOEFISIEN = 'ebudget.bappeko_rincian_detail.KETERANGAN_KOEFISIEN';

	
	const SUBTITLE = 'ebudget.bappeko_rincian_detail.SUBTITLE';

	
	const KOMPONEN_HARGA = 'ebudget.bappeko_rincian_detail.KOMPONEN_HARGA';

	
	const KOMPONEN_HARGA_AWAL = 'ebudget.bappeko_rincian_detail.KOMPONEN_HARGA_AWAL';

	
	const KOMPONEN_NAME = 'ebudget.bappeko_rincian_detail.KOMPONEN_NAME';

	
	const SATUAN = 'ebudget.bappeko_rincian_detail.SATUAN';

	
	const PAJAK = 'ebudget.bappeko_rincian_detail.PAJAK';

	
	const UNIT_ID = 'ebudget.bappeko_rincian_detail.UNIT_ID';

	
	const FROM_SUB_KEGIATAN = 'ebudget.bappeko_rincian_detail.FROM_SUB_KEGIATAN';

	
	const SUB = 'ebudget.bappeko_rincian_detail.SUB';

	
	const KODE_SUB = 'ebudget.bappeko_rincian_detail.KODE_SUB';

	
	const LAST_UPDATE_USER = 'ebudget.bappeko_rincian_detail.LAST_UPDATE_USER';

	
	const LAST_UPDATE_TIME = 'ebudget.bappeko_rincian_detail.LAST_UPDATE_TIME';

	
	const LAST_UPDATE_IP = 'ebudget.bappeko_rincian_detail.LAST_UPDATE_IP';

	
	const TAHAP = 'ebudget.bappeko_rincian_detail.TAHAP';

	
	const TAHAP_EDIT = 'ebudget.bappeko_rincian_detail.TAHAP_EDIT';

	
	const TAHAP_NEW = 'ebudget.bappeko_rincian_detail.TAHAP_NEW';

	
	const STATUS_LELANG = 'ebudget.bappeko_rincian_detail.STATUS_LELANG';

	
	const NOMOR_LELANG = 'ebudget.bappeko_rincian_detail.NOMOR_LELANG';

	
	const KOEFISIEN_SEMULA = 'ebudget.bappeko_rincian_detail.KOEFISIEN_SEMULA';

	
	const VOLUME_SEMULA = 'ebudget.bappeko_rincian_detail.VOLUME_SEMULA';

	
	const HARGA_SEMULA = 'ebudget.bappeko_rincian_detail.HARGA_SEMULA';

	
	const TOTAL_SEMULA = 'ebudget.bappeko_rincian_detail.TOTAL_SEMULA';

	
	const LOCK_SUBTITLE = 'ebudget.bappeko_rincian_detail.LOCK_SUBTITLE';

	
	const STATUS_HAPUS = 'ebudget.bappeko_rincian_detail.STATUS_HAPUS';

	
	const TAHUN = 'ebudget.bappeko_rincian_detail.TAHUN';

	
	const KODE_LOKASI = 'ebudget.bappeko_rincian_detail.KODE_LOKASI';

	
	const KECAMATAN = 'ebudget.bappeko_rincian_detail.KECAMATAN';

	
	const REKENING_CODE_ASLI = 'ebudget.bappeko_rincian_detail.REKENING_CODE_ASLI';

	
	const NOTE_SKPD = 'ebudget.bappeko_rincian_detail.NOTE_SKPD';

	
	const NOTE_PENELITI = 'ebudget.bappeko_rincian_detail.NOTE_PENELITI';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('KegiatanCode', 'Tipe', 'DetailNo', 'RekeningCode', 'KomponenId', 'DetailName', 'Volume', 'KeteranganKoefisien', 'Subtitle', 'KomponenHarga', 'KomponenHargaAwal', 'KomponenName', 'Satuan', 'Pajak', 'UnitId', 'FromSubKegiatan', 'Sub', 'KodeSub', 'LastUpdateUser', 'LastUpdateTime', 'LastUpdateIp', 'Tahap', 'TahapEdit', 'TahapNew', 'StatusLelang', 'NomorLelang', 'KoefisienSemula', 'VolumeSemula', 'HargaSemula', 'TotalSemula', 'LockSubtitle', 'StatusHapus', 'Tahun', 'KodeLokasi', 'Kecamatan', 'RekeningCodeAsli', 'NoteSkpd', 'NotePeneliti', ),
		BasePeer::TYPE_COLNAME => array (BappekoRincianDetailPeer::KEGIATAN_CODE, BappekoRincianDetailPeer::TIPE, BappekoRincianDetailPeer::DETAIL_NO, BappekoRincianDetailPeer::REKENING_CODE, BappekoRincianDetailPeer::KOMPONEN_ID, BappekoRincianDetailPeer::DETAIL_NAME, BappekoRincianDetailPeer::VOLUME, BappekoRincianDetailPeer::KETERANGAN_KOEFISIEN, BappekoRincianDetailPeer::SUBTITLE, BappekoRincianDetailPeer::KOMPONEN_HARGA, BappekoRincianDetailPeer::KOMPONEN_HARGA_AWAL, BappekoRincianDetailPeer::KOMPONEN_NAME, BappekoRincianDetailPeer::SATUAN, BappekoRincianDetailPeer::PAJAK, BappekoRincianDetailPeer::UNIT_ID, BappekoRincianDetailPeer::FROM_SUB_KEGIATAN, BappekoRincianDetailPeer::SUB, BappekoRincianDetailPeer::KODE_SUB, BappekoRincianDetailPeer::LAST_UPDATE_USER, BappekoRincianDetailPeer::LAST_UPDATE_TIME, BappekoRincianDetailPeer::LAST_UPDATE_IP, BappekoRincianDetailPeer::TAHAP, BappekoRincianDetailPeer::TAHAP_EDIT, BappekoRincianDetailPeer::TAHAP_NEW, BappekoRincianDetailPeer::STATUS_LELANG, BappekoRincianDetailPeer::NOMOR_LELANG, BappekoRincianDetailPeer::KOEFISIEN_SEMULA, BappekoRincianDetailPeer::VOLUME_SEMULA, BappekoRincianDetailPeer::HARGA_SEMULA, BappekoRincianDetailPeer::TOTAL_SEMULA, BappekoRincianDetailPeer::LOCK_SUBTITLE, BappekoRincianDetailPeer::STATUS_HAPUS, BappekoRincianDetailPeer::TAHUN, BappekoRincianDetailPeer::KODE_LOKASI, BappekoRincianDetailPeer::KECAMATAN, BappekoRincianDetailPeer::REKENING_CODE_ASLI, BappekoRincianDetailPeer::NOTE_SKPD, BappekoRincianDetailPeer::NOTE_PENELITI, ),
		BasePeer::TYPE_FIELDNAME => array ('kegiatan_code', 'tipe', 'detail_no', 'rekening_code', 'komponen_id', 'detail_name', 'volume', 'keterangan_koefisien', 'subtitle', 'komponen_harga', 'komponen_harga_awal', 'komponen_name', 'satuan', 'pajak', 'unit_id', 'from_sub_kegiatan', 'sub', 'kode_sub', 'last_update_user', 'last_update_time', 'last_update_ip', 'tahap', 'tahap_edit', 'tahap_new', 'status_lelang', 'nomor_lelang', 'koefisien_semula', 'volume_semula', 'harga_semula', 'total_semula', 'lock_subtitle', 'status_hapus', 'tahun', 'kode_lokasi', 'kecamatan', 'rekening_code_asli', 'note_skpd', 'note_peneliti', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('KegiatanCode' => 0, 'Tipe' => 1, 'DetailNo' => 2, 'RekeningCode' => 3, 'KomponenId' => 4, 'DetailName' => 5, 'Volume' => 6, 'KeteranganKoefisien' => 7, 'Subtitle' => 8, 'KomponenHarga' => 9, 'KomponenHargaAwal' => 10, 'KomponenName' => 11, 'Satuan' => 12, 'Pajak' => 13, 'UnitId' => 14, 'FromSubKegiatan' => 15, 'Sub' => 16, 'KodeSub' => 17, 'LastUpdateUser' => 18, 'LastUpdateTime' => 19, 'LastUpdateIp' => 20, 'Tahap' => 21, 'TahapEdit' => 22, 'TahapNew' => 23, 'StatusLelang' => 24, 'NomorLelang' => 25, 'KoefisienSemula' => 26, 'VolumeSemula' => 27, 'HargaSemula' => 28, 'TotalSemula' => 29, 'LockSubtitle' => 30, 'StatusHapus' => 31, 'Tahun' => 32, 'KodeLokasi' => 33, 'Kecamatan' => 34, 'RekeningCodeAsli' => 35, 'NoteSkpd' => 36, 'NotePeneliti' => 37, ),
		BasePeer::TYPE_COLNAME => array (BappekoRincianDetailPeer::KEGIATAN_CODE => 0, BappekoRincianDetailPeer::TIPE => 1, BappekoRincianDetailPeer::DETAIL_NO => 2, BappekoRincianDetailPeer::REKENING_CODE => 3, BappekoRincianDetailPeer::KOMPONEN_ID => 4, BappekoRincianDetailPeer::DETAIL_NAME => 5, BappekoRincianDetailPeer::VOLUME => 6, BappekoRincianDetailPeer::KETERANGAN_KOEFISIEN => 7, BappekoRincianDetailPeer::SUBTITLE => 8, BappekoRincianDetailPeer::KOMPONEN_HARGA => 9, BappekoRincianDetailPeer::KOMPONEN_HARGA_AWAL => 10, BappekoRincianDetailPeer::KOMPONEN_NAME => 11, BappekoRincianDetailPeer::SATUAN => 12, BappekoRincianDetailPeer::PAJAK => 13, BappekoRincianDetailPeer::UNIT_ID => 14, BappekoRincianDetailPeer::FROM_SUB_KEGIATAN => 15, BappekoRincianDetailPeer::SUB => 16, BappekoRincianDetailPeer::KODE_SUB => 17, BappekoRincianDetailPeer::LAST_UPDATE_USER => 18, BappekoRincianDetailPeer::LAST_UPDATE_TIME => 19, BappekoRincianDetailPeer::LAST_UPDATE_IP => 20, BappekoRincianDetailPeer::TAHAP => 21, BappekoRincianDetailPeer::TAHAP_EDIT => 22, BappekoRincianDetailPeer::TAHAP_NEW => 23, BappekoRincianDetailPeer::STATUS_LELANG => 24, BappekoRincianDetailPeer::NOMOR_LELANG => 25, BappekoRincianDetailPeer::KOEFISIEN_SEMULA => 26, BappekoRincianDetailPeer::VOLUME_SEMULA => 27, BappekoRincianDetailPeer::HARGA_SEMULA => 28, BappekoRincianDetailPeer::TOTAL_SEMULA => 29, BappekoRincianDetailPeer::LOCK_SUBTITLE => 30, BappekoRincianDetailPeer::STATUS_HAPUS => 31, BappekoRincianDetailPeer::TAHUN => 32, BappekoRincianDetailPeer::KODE_LOKASI => 33, BappekoRincianDetailPeer::KECAMATAN => 34, BappekoRincianDetailPeer::REKENING_CODE_ASLI => 35, BappekoRincianDetailPeer::NOTE_SKPD => 36, BappekoRincianDetailPeer::NOTE_PENELITI => 37, ),
		BasePeer::TYPE_FIELDNAME => array ('kegiatan_code' => 0, 'tipe' => 1, 'detail_no' => 2, 'rekening_code' => 3, 'komponen_id' => 4, 'detail_name' => 5, 'volume' => 6, 'keterangan_koefisien' => 7, 'subtitle' => 8, 'komponen_harga' => 9, 'komponen_harga_awal' => 10, 'komponen_name' => 11, 'satuan' => 12, 'pajak' => 13, 'unit_id' => 14, 'from_sub_kegiatan' => 15, 'sub' => 16, 'kode_sub' => 17, 'last_update_user' => 18, 'last_update_time' => 19, 'last_update_ip' => 20, 'tahap' => 21, 'tahap_edit' => 22, 'tahap_new' => 23, 'status_lelang' => 24, 'nomor_lelang' => 25, 'koefisien_semula' => 26, 'volume_semula' => 27, 'harga_semula' => 28, 'total_semula' => 29, 'lock_subtitle' => 30, 'status_hapus' => 31, 'tahun' => 32, 'kode_lokasi' => 33, 'kecamatan' => 34, 'rekening_code_asli' => 35, 'note_skpd' => 36, 'note_peneliti' => 37, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/BappekoRincianDetailMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.BappekoRincianDetailMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = BappekoRincianDetailPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(BappekoRincianDetailPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(BappekoRincianDetailPeer::KEGIATAN_CODE);

		$criteria->addSelectColumn(BappekoRincianDetailPeer::TIPE);

		$criteria->addSelectColumn(BappekoRincianDetailPeer::DETAIL_NO);

		$criteria->addSelectColumn(BappekoRincianDetailPeer::REKENING_CODE);

		$criteria->addSelectColumn(BappekoRincianDetailPeer::KOMPONEN_ID);

		$criteria->addSelectColumn(BappekoRincianDetailPeer::DETAIL_NAME);

		$criteria->addSelectColumn(BappekoRincianDetailPeer::VOLUME);

		$criteria->addSelectColumn(BappekoRincianDetailPeer::KETERANGAN_KOEFISIEN);

		$criteria->addSelectColumn(BappekoRincianDetailPeer::SUBTITLE);

		$criteria->addSelectColumn(BappekoRincianDetailPeer::KOMPONEN_HARGA);

		$criteria->addSelectColumn(BappekoRincianDetailPeer::KOMPONEN_HARGA_AWAL);

		$criteria->addSelectColumn(BappekoRincianDetailPeer::KOMPONEN_NAME);

		$criteria->addSelectColumn(BappekoRincianDetailPeer::SATUAN);

		$criteria->addSelectColumn(BappekoRincianDetailPeer::PAJAK);

		$criteria->addSelectColumn(BappekoRincianDetailPeer::UNIT_ID);

		$criteria->addSelectColumn(BappekoRincianDetailPeer::FROM_SUB_KEGIATAN);

		$criteria->addSelectColumn(BappekoRincianDetailPeer::SUB);

		$criteria->addSelectColumn(BappekoRincianDetailPeer::KODE_SUB);

		$criteria->addSelectColumn(BappekoRincianDetailPeer::LAST_UPDATE_USER);

		$criteria->addSelectColumn(BappekoRincianDetailPeer::LAST_UPDATE_TIME);

		$criteria->addSelectColumn(BappekoRincianDetailPeer::LAST_UPDATE_IP);

		$criteria->addSelectColumn(BappekoRincianDetailPeer::TAHAP);

		$criteria->addSelectColumn(BappekoRincianDetailPeer::TAHAP_EDIT);

		$criteria->addSelectColumn(BappekoRincianDetailPeer::TAHAP_NEW);

		$criteria->addSelectColumn(BappekoRincianDetailPeer::STATUS_LELANG);

		$criteria->addSelectColumn(BappekoRincianDetailPeer::NOMOR_LELANG);

		$criteria->addSelectColumn(BappekoRincianDetailPeer::KOEFISIEN_SEMULA);

		$criteria->addSelectColumn(BappekoRincianDetailPeer::VOLUME_SEMULA);

		$criteria->addSelectColumn(BappekoRincianDetailPeer::HARGA_SEMULA);

		$criteria->addSelectColumn(BappekoRincianDetailPeer::TOTAL_SEMULA);

		$criteria->addSelectColumn(BappekoRincianDetailPeer::LOCK_SUBTITLE);

		$criteria->addSelectColumn(BappekoRincianDetailPeer::STATUS_HAPUS);

		$criteria->addSelectColumn(BappekoRincianDetailPeer::TAHUN);

		$criteria->addSelectColumn(BappekoRincianDetailPeer::KODE_LOKASI);

		$criteria->addSelectColumn(BappekoRincianDetailPeer::KECAMATAN);

		$criteria->addSelectColumn(BappekoRincianDetailPeer::REKENING_CODE_ASLI);

		$criteria->addSelectColumn(BappekoRincianDetailPeer::NOTE_SKPD);

		$criteria->addSelectColumn(BappekoRincianDetailPeer::NOTE_PENELITI);

	}

	const COUNT = 'COUNT(ebudget.bappeko_rincian_detail.KEGIATAN_CODE)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.bappeko_rincian_detail.KEGIATAN_CODE)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(BappekoRincianDetailPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(BappekoRincianDetailPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = BappekoRincianDetailPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = BappekoRincianDetailPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return BappekoRincianDetailPeer::populateObjects(BappekoRincianDetailPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			BappekoRincianDetailPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = BappekoRincianDetailPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return BappekoRincianDetailPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(BappekoRincianDetailPeer::KEGIATAN_CODE);
			$selectCriteria->add(BappekoRincianDetailPeer::KEGIATAN_CODE, $criteria->remove(BappekoRincianDetailPeer::KEGIATAN_CODE), $comparison);

			$comparison = $criteria->getComparison(BappekoRincianDetailPeer::TIPE);
			$selectCriteria->add(BappekoRincianDetailPeer::TIPE, $criteria->remove(BappekoRincianDetailPeer::TIPE), $comparison);

			$comparison = $criteria->getComparison(BappekoRincianDetailPeer::DETAIL_NO);
			$selectCriteria->add(BappekoRincianDetailPeer::DETAIL_NO, $criteria->remove(BappekoRincianDetailPeer::DETAIL_NO), $comparison);

			$comparison = $criteria->getComparison(BappekoRincianDetailPeer::UNIT_ID);
			$selectCriteria->add(BappekoRincianDetailPeer::UNIT_ID, $criteria->remove(BappekoRincianDetailPeer::UNIT_ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(BappekoRincianDetailPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(BappekoRincianDetailPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof BappekoRincianDetail) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
												if(count($values) == count($values, COUNT_RECURSIVE))
			{
								$values = array($values);
			}
			$vals = array();
			foreach($values as $value)
			{

				$vals[0][] = $value[0];
				$vals[1][] = $value[1];
				$vals[2][] = $value[2];
				$vals[3][] = $value[3];
			}

			$criteria->add(BappekoRincianDetailPeer::KEGIATAN_CODE, $vals[0], Criteria::IN);
			$criteria->add(BappekoRincianDetailPeer::TIPE, $vals[1], Criteria::IN);
			$criteria->add(BappekoRincianDetailPeer::DETAIL_NO, $vals[2], Criteria::IN);
			$criteria->add(BappekoRincianDetailPeer::UNIT_ID, $vals[3], Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(BappekoRincianDetail $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(BappekoRincianDetailPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(BappekoRincianDetailPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(BappekoRincianDetailPeer::DATABASE_NAME, BappekoRincianDetailPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = BappekoRincianDetailPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK( $kegiatan_code, $tipe, $detail_no, $unit_id, $con = null) {
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$criteria = new Criteria();
		$criteria->add(BappekoRincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
		$criteria->add(BappekoRincianDetailPeer::TIPE, $tipe);
		$criteria->add(BappekoRincianDetailPeer::DETAIL_NO, $detail_no);
		$criteria->add(BappekoRincianDetailPeer::UNIT_ID, $unit_id);
		$v = BappekoRincianDetailPeer::doSelect($criteria, $con);

		return !empty($v) ? $v[0] : null;
	}
} 
if (Propel::isInit()) {
			try {
		BaseBappekoRincianDetailPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/BappekoRincianDetailMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.BappekoRincianDetailMapBuilder');
}
