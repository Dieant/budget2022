<?php


abstract class BaseRincianDetailPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.rincian_detail';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.RincianDetail';

	
	const NUM_COLUMNS = 95;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const KEGIATAN_CODE = 'ebudget.rincian_detail.KEGIATAN_CODE';

	
	const TIPE = 'ebudget.rincian_detail.TIPE';

	
	const DETAIL_NO = 'ebudget.rincian_detail.DETAIL_NO';

	
	const REKENING_CODE = 'ebudget.rincian_detail.REKENING_CODE';

	
	const KOMPONEN_ID = 'ebudget.rincian_detail.KOMPONEN_ID';

	
	const DETAIL_NAME = 'ebudget.rincian_detail.DETAIL_NAME';

	
	const VOLUME = 'ebudget.rincian_detail.VOLUME';

	
	const KETERANGAN_KOEFISIEN = 'ebudget.rincian_detail.KETERANGAN_KOEFISIEN';

	
	const SUBTITLE = 'ebudget.rincian_detail.SUBTITLE';

	
	const KOMPONEN_HARGA = 'ebudget.rincian_detail.KOMPONEN_HARGA';

	
	const KOMPONEN_HARGA_AWAL = 'ebudget.rincian_detail.KOMPONEN_HARGA_AWAL';

	
	const KOMPONEN_NAME = 'ebudget.rincian_detail.KOMPONEN_NAME';

	
	const SATUAN = 'ebudget.rincian_detail.SATUAN';

	
	const PAJAK = 'ebudget.rincian_detail.PAJAK';

	
	const UNIT_ID = 'ebudget.rincian_detail.UNIT_ID';

	
	const FROM_SUB_KEGIATAN = 'ebudget.rincian_detail.FROM_SUB_KEGIATAN';

	
	const SUB = 'ebudget.rincian_detail.SUB';

	
	const KODE_SUB = 'ebudget.rincian_detail.KODE_SUB';

	
	const LAST_UPDATE_USER = 'ebudget.rincian_detail.LAST_UPDATE_USER';

	
	const LAST_UPDATE_TIME = 'ebudget.rincian_detail.LAST_UPDATE_TIME';

	
	const LAST_UPDATE_IP = 'ebudget.rincian_detail.LAST_UPDATE_IP';

	
	const TAHAP = 'ebudget.rincian_detail.TAHAP';

	
	const TAHAP_EDIT = 'ebudget.rincian_detail.TAHAP_EDIT';

	
	const TAHAP_NEW = 'ebudget.rincian_detail.TAHAP_NEW';

	
	const STATUS_LELANG = 'ebudget.rincian_detail.STATUS_LELANG';

	
	const NOMOR_LELANG = 'ebudget.rincian_detail.NOMOR_LELANG';

	
	const KOEFISIEN_SEMULA = 'ebudget.rincian_detail.KOEFISIEN_SEMULA';

	
	const VOLUME_SEMULA = 'ebudget.rincian_detail.VOLUME_SEMULA';

	
	const HARGA_SEMULA = 'ebudget.rincian_detail.HARGA_SEMULA';

	
	const TOTAL_SEMULA = 'ebudget.rincian_detail.TOTAL_SEMULA';

	
	const LOCK_SUBTITLE = 'ebudget.rincian_detail.LOCK_SUBTITLE';

	
	const STATUS_HAPUS = 'ebudget.rincian_detail.STATUS_HAPUS';

	
	const TAHUN = 'ebudget.rincian_detail.TAHUN';

	
	const KODE_LOKASI = 'ebudget.rincian_detail.KODE_LOKASI';

	
	const KECAMATAN = 'ebudget.rincian_detail.KECAMATAN';

	
	const REKENING_CODE_ASLI = 'ebudget.rincian_detail.REKENING_CODE_ASLI';

	
	const NOTE_SKPD = 'ebudget.rincian_detail.NOTE_SKPD';

	
	const NOTE_PENELITI = 'ebudget.rincian_detail.NOTE_PENELITI';

	
	const NILAI_ANGGARAN = 'ebudget.rincian_detail.NILAI_ANGGARAN';

	
	const IS_BLUD = 'ebudget.rincian_detail.IS_BLUD';

	
	const IS_KAPITASI_BPJS = 'ebudget.rincian_detail.IS_KAPITASI_BPJS';

	
	const LOKASI_KECAMATAN = 'ebudget.rincian_detail.LOKASI_KECAMATAN';

	
	const LOKASI_KELURAHAN = 'ebudget.rincian_detail.LOKASI_KELURAHAN';

	
	const OB = 'ebudget.rincian_detail.OB';

	
	const OB_FROM_ID = 'ebudget.rincian_detail.OB_FROM_ID';

	
	const IS_PER_KOMPONEN = 'ebudget.rincian_detail.IS_PER_KOMPONEN';

	
	const KEGIATAN_CODE_ASAL = 'ebudget.rincian_detail.KEGIATAN_CODE_ASAL';

	
	const TH_KE_MULTIYEARS = 'ebudget.rincian_detail.TH_KE_MULTIYEARS';

	
	const HARGA_SEBELUM_SISA_LELANG = 'ebudget.rincian_detail.HARGA_SEBELUM_SISA_LELANG';

	
	const IS_MUSRENBANG = 'ebudget.rincian_detail.IS_MUSRENBANG';

	
	const SUB_ID_ASAL = 'ebudget.rincian_detail.SUB_ID_ASAL';

	
	const SUBTITLE_ASAL = 'ebudget.rincian_detail.SUBTITLE_ASAL';

	
	const KODE_SUB_ASAL = 'ebudget.rincian_detail.KODE_SUB_ASAL';

	
	const SUB_ASAL = 'ebudget.rincian_detail.SUB_ASAL';

	
	const LAST_EDIT_TIME = 'ebudget.rincian_detail.LAST_EDIT_TIME';

	
	const IS_POTONG_BPJS = 'ebudget.rincian_detail.IS_POTONG_BPJS';

	
	const IS_IURAN_BPJS = 'ebudget.rincian_detail.IS_IURAN_BPJS';

	
	const STATUS_OB = 'ebudget.rincian_detail.STATUS_OB';

	
	const OB_PARENT = 'ebudget.rincian_detail.OB_PARENT';

	
	const OB_ALOKASI_BARU = 'ebudget.rincian_detail.OB_ALOKASI_BARU';

	
	const IS_HIBAH = 'ebudget.rincian_detail.IS_HIBAH';

	
	const AKRUAL_CODE = 'ebudget.rincian_detail.AKRUAL_CODE';

	
	const TIPE2 = 'ebudget.rincian_detail.TIPE2';

	
	const STATUS_LEVEL = 'ebudget.rincian_detail.STATUS_LEVEL';

	
	const STATUS_LEVEL_TOLAK = 'ebudget.rincian_detail.STATUS_LEVEL_TOLAK';

	
	const STATUS_SISIPAN = 'ebudget.rincian_detail.STATUS_SISIPAN';

	
	const IS_TAPD_SETUJU = 'ebudget.rincian_detail.IS_TAPD_SETUJU';

	
	const IS_BAPPEKO_SETUJU = 'ebudget.rincian_detail.IS_BAPPEKO_SETUJU';

	
	const IS_PENYELIA_SETUJU = 'ebudget.rincian_detail.IS_PENYELIA_SETUJU';

	
	const NOTE_TAPD = 'ebudget.rincian_detail.NOTE_TAPD';

	
	const NOTE_BAPPEKO = 'ebudget.rincian_detail.NOTE_BAPPEKO';

	
	const SATUAN_SEMULA = 'ebudget.rincian_detail.SATUAN_SEMULA';

	
	const ID_LOKASI = 'ebudget.rincian_detail.ID_LOKASI';

	
	const DETAIL_KEGIATAN = 'ebudget.rincian_detail.DETAIL_KEGIATAN';

	
	const DETAIL_KEGIATAN_SEMULA = 'ebudget.rincian_detail.DETAIL_KEGIATAN_SEMULA';

	
	const STATUS_KOMPONEN_BARU = 'ebudget.rincian_detail.STATUS_KOMPONEN_BARU';

	
	const STATUS_KOMPONEN_BERUBAH = 'ebudget.rincian_detail.STATUS_KOMPONEN_BERUBAH';

	
	const APPROVE_UNLOCK_HARGA = 'ebudget.rincian_detail.APPROVE_UNLOCK_HARGA';

	
	const TIPE_LELANG = 'ebudget.rincian_detail.TIPE_LELANG';

	
	const IS_HPSP = 'ebudget.rincian_detail.IS_HPSP';

	
	const IS_DAK = 'ebudget.rincian_detail.IS_DAK';

	
	const IS_BOS = 'ebudget.rincian_detail.IS_BOS';

	
	const IS_BOBDA = 'ebudget.rincian_detail.IS_BOBDA';

	
	const IS_NARSUM = 'ebudget.rincian_detail.IS_NARSUM';

	
	const IS_BAGIAN_HUKUM_SETUJU = 'ebudget.rincian_detail.IS_BAGIAN_HUKUM_SETUJU';

	
	const IS_INSPEKTORAT_SETUJU = 'ebudget.rincian_detail.IS_INSPEKTORAT_SETUJU';

	
	const IS_BADAN_KEPEGAWAIAN_SETUJU = 'ebudget.rincian_detail.IS_BADAN_KEPEGAWAIAN_SETUJU';

	
	const IS_LPPA_SETUJU = 'ebudget.rincian_detail.IS_LPPA_SETUJU';

	
	const PRIORITAS_WALI = 'ebudget.rincian_detail.PRIORITAS_WALI';

	
	const IS_OUTPUT = 'ebudget.rincian_detail.IS_OUTPUT';

	
	const IS_BAGIAN_ORGANISASI_SETUJU = 'ebudget.rincian_detail.IS_BAGIAN_ORGANISASI_SETUJU';

	
	const IS_ASISTEN1_SETUJU = 'ebudget.rincian_detail.IS_ASISTEN1_SETUJU';

	
	const IS_ASISTEN2_SETUJU = 'ebudget.rincian_detail.IS_ASISTEN2_SETUJU';

	
	const IS_ASISTEN3_SETUJU = 'ebudget.rincian_detail.IS_ASISTEN3_SETUJU';

	
	const IS_SEKDA_SETUJU = 'ebudget.rincian_detail.IS_SEKDA_SETUJU';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('KegiatanCode', 'Tipe', 'DetailNo', 'RekeningCode', 'KomponenId', 'DetailName', 'Volume', 'KeteranganKoefisien', 'Subtitle', 'KomponenHarga', 'KomponenHargaAwal', 'KomponenName', 'Satuan', 'Pajak', 'UnitId', 'FromSubKegiatan', 'Sub', 'KodeSub', 'LastUpdateUser', 'LastUpdateTime', 'LastUpdateIp', 'Tahap', 'TahapEdit', 'TahapNew', 'StatusLelang', 'NomorLelang', 'KoefisienSemula', 'VolumeSemula', 'HargaSemula', 'TotalSemula', 'LockSubtitle', 'StatusHapus', 'Tahun', 'KodeLokasi', 'Kecamatan', 'RekeningCodeAsli', 'NoteSkpd', 'NotePeneliti', 'NilaiAnggaran', 'IsBlud', 'IsKapitasiBpjs', 'LokasiKecamatan', 'LokasiKelurahan', 'Ob', 'ObFromId', 'IsPerKomponen', 'KegiatanCodeAsal', 'ThKeMultiyears', 'HargaSebelumSisaLelang', 'IsMusrenbang', 'SubIdAsal', 'SubtitleAsal', 'KodeSubAsal', 'SubAsal', 'LastEditTime', 'IsPotongBpjs', 'IsIuranBpjs', 'StatusOb', 'ObParent', 'ObAlokasiBaru', 'IsHibah', 'AkrualCode', 'Tipe2', 'StatusLevel', 'StatusLevelTolak', 'StatusSisipan', 'IsTapdSetuju', 'IsBappekoSetuju', 'IsPenyeliaSetuju', 'NoteTapd', 'NoteBappeko', 'SatuanSemula', 'IdLokasi', 'DetailKegiatan', 'DetailKegiatanSemula', 'StatusKomponenBaru', 'StatusKomponenBerubah', 'ApproveUnlockHarga', 'TipeLelang', 'IsHpsp', 'IsDak', 'IsBos', 'IsBobda', 'IsNarsum', 'IsBagianHukumSetuju', 'IsInspektoratSetuju', 'IsBadanKepegawaianSetuju', 'IsLppaSetuju', 'PrioritasWali', 'IsOutput', 'IsBagianOrganisasiSetuju', 'IsAsisten1Setuju', 'IsAsisten2Setuju', 'IsAsisten3Setuju', 'IsSekdaSetuju', ),
		BasePeer::TYPE_COLNAME => array (RincianDetailPeer::KEGIATAN_CODE, RincianDetailPeer::TIPE, RincianDetailPeer::DETAIL_NO, RincianDetailPeer::REKENING_CODE, RincianDetailPeer::KOMPONEN_ID, RincianDetailPeer::DETAIL_NAME, RincianDetailPeer::VOLUME, RincianDetailPeer::KETERANGAN_KOEFISIEN, RincianDetailPeer::SUBTITLE, RincianDetailPeer::KOMPONEN_HARGA, RincianDetailPeer::KOMPONEN_HARGA_AWAL, RincianDetailPeer::KOMPONEN_NAME, RincianDetailPeer::SATUAN, RincianDetailPeer::PAJAK, RincianDetailPeer::UNIT_ID, RincianDetailPeer::FROM_SUB_KEGIATAN, RincianDetailPeer::SUB, RincianDetailPeer::KODE_SUB, RincianDetailPeer::LAST_UPDATE_USER, RincianDetailPeer::LAST_UPDATE_TIME, RincianDetailPeer::LAST_UPDATE_IP, RincianDetailPeer::TAHAP, RincianDetailPeer::TAHAP_EDIT, RincianDetailPeer::TAHAP_NEW, RincianDetailPeer::STATUS_LELANG, RincianDetailPeer::NOMOR_LELANG, RincianDetailPeer::KOEFISIEN_SEMULA, RincianDetailPeer::VOLUME_SEMULA, RincianDetailPeer::HARGA_SEMULA, RincianDetailPeer::TOTAL_SEMULA, RincianDetailPeer::LOCK_SUBTITLE, RincianDetailPeer::STATUS_HAPUS, RincianDetailPeer::TAHUN, RincianDetailPeer::KODE_LOKASI, RincianDetailPeer::KECAMATAN, RincianDetailPeer::REKENING_CODE_ASLI, RincianDetailPeer::NOTE_SKPD, RincianDetailPeer::NOTE_PENELITI, RincianDetailPeer::NILAI_ANGGARAN, RincianDetailPeer::IS_BLUD, RincianDetailPeer::IS_KAPITASI_BPJS, RincianDetailPeer::LOKASI_KECAMATAN, RincianDetailPeer::LOKASI_KELURAHAN, RincianDetailPeer::OB, RincianDetailPeer::OB_FROM_ID, RincianDetailPeer::IS_PER_KOMPONEN, RincianDetailPeer::KEGIATAN_CODE_ASAL, RincianDetailPeer::TH_KE_MULTIYEARS, RincianDetailPeer::HARGA_SEBELUM_SISA_LELANG, RincianDetailPeer::IS_MUSRENBANG, RincianDetailPeer::SUB_ID_ASAL, RincianDetailPeer::SUBTITLE_ASAL, RincianDetailPeer::KODE_SUB_ASAL, RincianDetailPeer::SUB_ASAL, RincianDetailPeer::LAST_EDIT_TIME, RincianDetailPeer::IS_POTONG_BPJS, RincianDetailPeer::IS_IURAN_BPJS, RincianDetailPeer::STATUS_OB, RincianDetailPeer::OB_PARENT, RincianDetailPeer::OB_ALOKASI_BARU, RincianDetailPeer::IS_HIBAH, RincianDetailPeer::AKRUAL_CODE, RincianDetailPeer::TIPE2, RincianDetailPeer::STATUS_LEVEL, RincianDetailPeer::STATUS_LEVEL_TOLAK, RincianDetailPeer::STATUS_SISIPAN, RincianDetailPeer::IS_TAPD_SETUJU, RincianDetailPeer::IS_BAPPEKO_SETUJU, RincianDetailPeer::IS_PENYELIA_SETUJU, RincianDetailPeer::NOTE_TAPD, RincianDetailPeer::NOTE_BAPPEKO, RincianDetailPeer::SATUAN_SEMULA, RincianDetailPeer::ID_LOKASI, RincianDetailPeer::DETAIL_KEGIATAN, RincianDetailPeer::DETAIL_KEGIATAN_SEMULA, RincianDetailPeer::STATUS_KOMPONEN_BARU, RincianDetailPeer::STATUS_KOMPONEN_BERUBAH, RincianDetailPeer::APPROVE_UNLOCK_HARGA, RincianDetailPeer::TIPE_LELANG, RincianDetailPeer::IS_HPSP, RincianDetailPeer::IS_DAK, RincianDetailPeer::IS_BOS, RincianDetailPeer::IS_BOBDA, RincianDetailPeer::IS_NARSUM, RincianDetailPeer::IS_BAGIAN_HUKUM_SETUJU, RincianDetailPeer::IS_INSPEKTORAT_SETUJU, RincianDetailPeer::IS_BADAN_KEPEGAWAIAN_SETUJU, RincianDetailPeer::IS_LPPA_SETUJU, RincianDetailPeer::PRIORITAS_WALI, RincianDetailPeer::IS_OUTPUT, RincianDetailPeer::IS_BAGIAN_ORGANISASI_SETUJU, RincianDetailPeer::IS_ASISTEN1_SETUJU, RincianDetailPeer::IS_ASISTEN2_SETUJU, RincianDetailPeer::IS_ASISTEN3_SETUJU, RincianDetailPeer::IS_SEKDA_SETUJU, ),
		BasePeer::TYPE_FIELDNAME => array ('kegiatan_code', 'tipe', 'detail_no', 'rekening_code', 'komponen_id', 'detail_name', 'volume', 'keterangan_koefisien', 'subtitle', 'komponen_harga', 'komponen_harga_awal', 'komponen_name', 'satuan', 'pajak', 'unit_id', 'from_sub_kegiatan', 'sub', 'kode_sub', 'last_update_user', 'last_update_time', 'last_update_ip', 'tahap', 'tahap_edit', 'tahap_new', 'status_lelang', 'nomor_lelang', 'koefisien_semula', 'volume_semula', 'harga_semula', 'total_semula', 'lock_subtitle', 'status_hapus', 'tahun', 'kode_lokasi', 'kecamatan', 'rekening_code_asli', 'note_skpd', 'note_peneliti', 'nilai_anggaran', 'is_blud', 'is_kapitasi_bpjs', 'lokasi_kecamatan', 'lokasi_kelurahan', 'ob', 'ob_from_id', 'is_per_komponen', 'kegiatan_code_asal', 'th_ke_multiyears', 'harga_sebelum_sisa_lelang', 'is_musrenbang', 'sub_id_asal', 'subtitle_asal', 'kode_sub_asal', 'sub_asal', 'last_edit_time', 'is_potong_bpjs', 'is_iuran_bpjs', 'status_ob', 'ob_parent', 'ob_alokasi_baru', 'is_hibah', 'akrual_code', 'tipe2', 'status_level', 'status_level_tolak', 'status_sisipan', 'is_tapd_setuju', 'is_bappeko_setuju', 'is_penyelia_setuju', 'note_tapd', 'note_bappeko', 'satuan_semula', 'id_lokasi', 'detail_kegiatan', 'detail_kegiatan_semula', 'status_komponen_baru', 'status_komponen_berubah', 'approve_unlock_harga', 'tipe_lelang', 'is_hpsp', 'is_dak', 'is_bos', 'is_bobda', 'is_narsum', 'is_bagian_hukum_setuju', 'is_inspektorat_setuju', 'is_badan_kepegawaian_setuju', 'is_lppa_setuju', 'prioritas_wali', 'is_output', 'is_bagian_organisasi_setuju', 'is_asisten1_setuju', 'is_asisten2_setuju', 'is_asisten3_setuju', 'is_sekda_setuju', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('KegiatanCode' => 0, 'Tipe' => 1, 'DetailNo' => 2, 'RekeningCode' => 3, 'KomponenId' => 4, 'DetailName' => 5, 'Volume' => 6, 'KeteranganKoefisien' => 7, 'Subtitle' => 8, 'KomponenHarga' => 9, 'KomponenHargaAwal' => 10, 'KomponenName' => 11, 'Satuan' => 12, 'Pajak' => 13, 'UnitId' => 14, 'FromSubKegiatan' => 15, 'Sub' => 16, 'KodeSub' => 17, 'LastUpdateUser' => 18, 'LastUpdateTime' => 19, 'LastUpdateIp' => 20, 'Tahap' => 21, 'TahapEdit' => 22, 'TahapNew' => 23, 'StatusLelang' => 24, 'NomorLelang' => 25, 'KoefisienSemula' => 26, 'VolumeSemula' => 27, 'HargaSemula' => 28, 'TotalSemula' => 29, 'LockSubtitle' => 30, 'StatusHapus' => 31, 'Tahun' => 32, 'KodeLokasi' => 33, 'Kecamatan' => 34, 'RekeningCodeAsli' => 35, 'NoteSkpd' => 36, 'NotePeneliti' => 37, 'NilaiAnggaran' => 38, 'IsBlud' => 39, 'IsKapitasiBpjs' => 40, 'LokasiKecamatan' => 41, 'LokasiKelurahan' => 42, 'Ob' => 43, 'ObFromId' => 44, 'IsPerKomponen' => 45, 'KegiatanCodeAsal' => 46, 'ThKeMultiyears' => 47, 'HargaSebelumSisaLelang' => 48, 'IsMusrenbang' => 49, 'SubIdAsal' => 50, 'SubtitleAsal' => 51, 'KodeSubAsal' => 52, 'SubAsal' => 53, 'LastEditTime' => 54, 'IsPotongBpjs' => 55, 'IsIuranBpjs' => 56, 'StatusOb' => 57, 'ObParent' => 58, 'ObAlokasiBaru' => 59, 'IsHibah' => 60, 'AkrualCode' => 61, 'Tipe2' => 62, 'StatusLevel' => 63, 'StatusLevelTolak' => 64, 'StatusSisipan' => 65, 'IsTapdSetuju' => 66, 'IsBappekoSetuju' => 67, 'IsPenyeliaSetuju' => 68, 'NoteTapd' => 69, 'NoteBappeko' => 70, 'SatuanSemula' => 71, 'IdLokasi' => 72, 'DetailKegiatan' => 73, 'DetailKegiatanSemula' => 74, 'StatusKomponenBaru' => 75, 'StatusKomponenBerubah' => 76, 'ApproveUnlockHarga' => 77, 'TipeLelang' => 78, 'IsHpsp' => 79, 'IsDak' => 80, 'IsBos' => 81, 'IsBobda' => 82, 'IsNarsum' => 83, 'IsBagianHukumSetuju' => 84, 'IsInspektoratSetuju' => 85, 'IsBadanKepegawaianSetuju' => 86, 'IsLppaSetuju' => 87, 'PrioritasWali' => 88, 'IsOutput' => 89, 'IsBagianOrganisasiSetuju' => 90, 'IsAsisten1Setuju' => 91, 'IsAsisten2Setuju' => 92, 'IsAsisten3Setuju' => 93, 'IsSekdaSetuju' => 94, ),
		BasePeer::TYPE_COLNAME => array (RincianDetailPeer::KEGIATAN_CODE => 0, RincianDetailPeer::TIPE => 1, RincianDetailPeer::DETAIL_NO => 2, RincianDetailPeer::REKENING_CODE => 3, RincianDetailPeer::KOMPONEN_ID => 4, RincianDetailPeer::DETAIL_NAME => 5, RincianDetailPeer::VOLUME => 6, RincianDetailPeer::KETERANGAN_KOEFISIEN => 7, RincianDetailPeer::SUBTITLE => 8, RincianDetailPeer::KOMPONEN_HARGA => 9, RincianDetailPeer::KOMPONEN_HARGA_AWAL => 10, RincianDetailPeer::KOMPONEN_NAME => 11, RincianDetailPeer::SATUAN => 12, RincianDetailPeer::PAJAK => 13, RincianDetailPeer::UNIT_ID => 14, RincianDetailPeer::FROM_SUB_KEGIATAN => 15, RincianDetailPeer::SUB => 16, RincianDetailPeer::KODE_SUB => 17, RincianDetailPeer::LAST_UPDATE_USER => 18, RincianDetailPeer::LAST_UPDATE_TIME => 19, RincianDetailPeer::LAST_UPDATE_IP => 20, RincianDetailPeer::TAHAP => 21, RincianDetailPeer::TAHAP_EDIT => 22, RincianDetailPeer::TAHAP_NEW => 23, RincianDetailPeer::STATUS_LELANG => 24, RincianDetailPeer::NOMOR_LELANG => 25, RincianDetailPeer::KOEFISIEN_SEMULA => 26, RincianDetailPeer::VOLUME_SEMULA => 27, RincianDetailPeer::HARGA_SEMULA => 28, RincianDetailPeer::TOTAL_SEMULA => 29, RincianDetailPeer::LOCK_SUBTITLE => 30, RincianDetailPeer::STATUS_HAPUS => 31, RincianDetailPeer::TAHUN => 32, RincianDetailPeer::KODE_LOKASI => 33, RincianDetailPeer::KECAMATAN => 34, RincianDetailPeer::REKENING_CODE_ASLI => 35, RincianDetailPeer::NOTE_SKPD => 36, RincianDetailPeer::NOTE_PENELITI => 37, RincianDetailPeer::NILAI_ANGGARAN => 38, RincianDetailPeer::IS_BLUD => 39, RincianDetailPeer::IS_KAPITASI_BPJS => 40, RincianDetailPeer::LOKASI_KECAMATAN => 41, RincianDetailPeer::LOKASI_KELURAHAN => 42, RincianDetailPeer::OB => 43, RincianDetailPeer::OB_FROM_ID => 44, RincianDetailPeer::IS_PER_KOMPONEN => 45, RincianDetailPeer::KEGIATAN_CODE_ASAL => 46, RincianDetailPeer::TH_KE_MULTIYEARS => 47, RincianDetailPeer::HARGA_SEBELUM_SISA_LELANG => 48, RincianDetailPeer::IS_MUSRENBANG => 49, RincianDetailPeer::SUB_ID_ASAL => 50, RincianDetailPeer::SUBTITLE_ASAL => 51, RincianDetailPeer::KODE_SUB_ASAL => 52, RincianDetailPeer::SUB_ASAL => 53, RincianDetailPeer::LAST_EDIT_TIME => 54, RincianDetailPeer::IS_POTONG_BPJS => 55, RincianDetailPeer::IS_IURAN_BPJS => 56, RincianDetailPeer::STATUS_OB => 57, RincianDetailPeer::OB_PARENT => 58, RincianDetailPeer::OB_ALOKASI_BARU => 59, RincianDetailPeer::IS_HIBAH => 60, RincianDetailPeer::AKRUAL_CODE => 61, RincianDetailPeer::TIPE2 => 62, RincianDetailPeer::STATUS_LEVEL => 63, RincianDetailPeer::STATUS_LEVEL_TOLAK => 64, RincianDetailPeer::STATUS_SISIPAN => 65, RincianDetailPeer::IS_TAPD_SETUJU => 66, RincianDetailPeer::IS_BAPPEKO_SETUJU => 67, RincianDetailPeer::IS_PENYELIA_SETUJU => 68, RincianDetailPeer::NOTE_TAPD => 69, RincianDetailPeer::NOTE_BAPPEKO => 70, RincianDetailPeer::SATUAN_SEMULA => 71, RincianDetailPeer::ID_LOKASI => 72, RincianDetailPeer::DETAIL_KEGIATAN => 73, RincianDetailPeer::DETAIL_KEGIATAN_SEMULA => 74, RincianDetailPeer::STATUS_KOMPONEN_BARU => 75, RincianDetailPeer::STATUS_KOMPONEN_BERUBAH => 76, RincianDetailPeer::APPROVE_UNLOCK_HARGA => 77, RincianDetailPeer::TIPE_LELANG => 78, RincianDetailPeer::IS_HPSP => 79, RincianDetailPeer::IS_DAK => 80, RincianDetailPeer::IS_BOS => 81, RincianDetailPeer::IS_BOBDA => 82, RincianDetailPeer::IS_NARSUM => 83, RincianDetailPeer::IS_BAGIAN_HUKUM_SETUJU => 84, RincianDetailPeer::IS_INSPEKTORAT_SETUJU => 85, RincianDetailPeer::IS_BADAN_KEPEGAWAIAN_SETUJU => 86, RincianDetailPeer::IS_LPPA_SETUJU => 87, RincianDetailPeer::PRIORITAS_WALI => 88, RincianDetailPeer::IS_OUTPUT => 89, RincianDetailPeer::IS_BAGIAN_ORGANISASI_SETUJU => 90, RincianDetailPeer::IS_ASISTEN1_SETUJU => 91, RincianDetailPeer::IS_ASISTEN2_SETUJU => 92, RincianDetailPeer::IS_ASISTEN3_SETUJU => 93, RincianDetailPeer::IS_SEKDA_SETUJU => 94, ),
		BasePeer::TYPE_FIELDNAME => array ('kegiatan_code' => 0, 'tipe' => 1, 'detail_no' => 2, 'rekening_code' => 3, 'komponen_id' => 4, 'detail_name' => 5, 'volume' => 6, 'keterangan_koefisien' => 7, 'subtitle' => 8, 'komponen_harga' => 9, 'komponen_harga_awal' => 10, 'komponen_name' => 11, 'satuan' => 12, 'pajak' => 13, 'unit_id' => 14, 'from_sub_kegiatan' => 15, 'sub' => 16, 'kode_sub' => 17, 'last_update_user' => 18, 'last_update_time' => 19, 'last_update_ip' => 20, 'tahap' => 21, 'tahap_edit' => 22, 'tahap_new' => 23, 'status_lelang' => 24, 'nomor_lelang' => 25, 'koefisien_semula' => 26, 'volume_semula' => 27, 'harga_semula' => 28, 'total_semula' => 29, 'lock_subtitle' => 30, 'status_hapus' => 31, 'tahun' => 32, 'kode_lokasi' => 33, 'kecamatan' => 34, 'rekening_code_asli' => 35, 'note_skpd' => 36, 'note_peneliti' => 37, 'nilai_anggaran' => 38, 'is_blud' => 39, 'is_kapitasi_bpjs' => 40, 'lokasi_kecamatan' => 41, 'lokasi_kelurahan' => 42, 'ob' => 43, 'ob_from_id' => 44, 'is_per_komponen' => 45, 'kegiatan_code_asal' => 46, 'th_ke_multiyears' => 47, 'harga_sebelum_sisa_lelang' => 48, 'is_musrenbang' => 49, 'sub_id_asal' => 50, 'subtitle_asal' => 51, 'kode_sub_asal' => 52, 'sub_asal' => 53, 'last_edit_time' => 54, 'is_potong_bpjs' => 55, 'is_iuran_bpjs' => 56, 'status_ob' => 57, 'ob_parent' => 58, 'ob_alokasi_baru' => 59, 'is_hibah' => 60, 'akrual_code' => 61, 'tipe2' => 62, 'status_level' => 63, 'status_level_tolak' => 64, 'status_sisipan' => 65, 'is_tapd_setuju' => 66, 'is_bappeko_setuju' => 67, 'is_penyelia_setuju' => 68, 'note_tapd' => 69, 'note_bappeko' => 70, 'satuan_semula' => 71, 'id_lokasi' => 72, 'detail_kegiatan' => 73, 'detail_kegiatan_semula' => 74, 'status_komponen_baru' => 75, 'status_komponen_berubah' => 76, 'approve_unlock_harga' => 77, 'tipe_lelang' => 78, 'is_hpsp' => 79, 'is_dak' => 80, 'is_bos' => 81, 'is_bobda' => 82, 'is_narsum' => 83, 'is_bagian_hukum_setuju' => 84, 'is_inspektorat_setuju' => 85, 'is_badan_kepegawaian_setuju' => 86, 'is_lppa_setuju' => 87, 'prioritas_wali' => 88, 'is_output' => 89, 'is_bagian_organisasi_setuju' => 90, 'is_asisten1_setuju' => 91, 'is_asisten2_setuju' => 92, 'is_asisten3_setuju' => 93, 'is_sekda_setuju' => 94, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/RincianDetailMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.RincianDetailMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = RincianDetailPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(RincianDetailPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(RincianDetailPeer::KEGIATAN_CODE);

		$criteria->addSelectColumn(RincianDetailPeer::TIPE);

		$criteria->addSelectColumn(RincianDetailPeer::DETAIL_NO);

		$criteria->addSelectColumn(RincianDetailPeer::REKENING_CODE);

		$criteria->addSelectColumn(RincianDetailPeer::KOMPONEN_ID);

		$criteria->addSelectColumn(RincianDetailPeer::DETAIL_NAME);

		$criteria->addSelectColumn(RincianDetailPeer::VOLUME);

		$criteria->addSelectColumn(RincianDetailPeer::KETERANGAN_KOEFISIEN);

		$criteria->addSelectColumn(RincianDetailPeer::SUBTITLE);

		$criteria->addSelectColumn(RincianDetailPeer::KOMPONEN_HARGA);

		$criteria->addSelectColumn(RincianDetailPeer::KOMPONEN_HARGA_AWAL);

		$criteria->addSelectColumn(RincianDetailPeer::KOMPONEN_NAME);

		$criteria->addSelectColumn(RincianDetailPeer::SATUAN);

		$criteria->addSelectColumn(RincianDetailPeer::PAJAK);

		$criteria->addSelectColumn(RincianDetailPeer::UNIT_ID);

		$criteria->addSelectColumn(RincianDetailPeer::FROM_SUB_KEGIATAN);

		$criteria->addSelectColumn(RincianDetailPeer::SUB);

		$criteria->addSelectColumn(RincianDetailPeer::KODE_SUB);

		$criteria->addSelectColumn(RincianDetailPeer::LAST_UPDATE_USER);

		$criteria->addSelectColumn(RincianDetailPeer::LAST_UPDATE_TIME);

		$criteria->addSelectColumn(RincianDetailPeer::LAST_UPDATE_IP);

		$criteria->addSelectColumn(RincianDetailPeer::TAHAP);

		$criteria->addSelectColumn(RincianDetailPeer::TAHAP_EDIT);

		$criteria->addSelectColumn(RincianDetailPeer::TAHAP_NEW);

		$criteria->addSelectColumn(RincianDetailPeer::STATUS_LELANG);

		$criteria->addSelectColumn(RincianDetailPeer::NOMOR_LELANG);

		$criteria->addSelectColumn(RincianDetailPeer::KOEFISIEN_SEMULA);

		$criteria->addSelectColumn(RincianDetailPeer::VOLUME_SEMULA);

		$criteria->addSelectColumn(RincianDetailPeer::HARGA_SEMULA);

		$criteria->addSelectColumn(RincianDetailPeer::TOTAL_SEMULA);

		$criteria->addSelectColumn(RincianDetailPeer::LOCK_SUBTITLE);

		$criteria->addSelectColumn(RincianDetailPeer::STATUS_HAPUS);

		$criteria->addSelectColumn(RincianDetailPeer::TAHUN);

		$criteria->addSelectColumn(RincianDetailPeer::KODE_LOKASI);

		$criteria->addSelectColumn(RincianDetailPeer::KECAMATAN);

		$criteria->addSelectColumn(RincianDetailPeer::REKENING_CODE_ASLI);

		$criteria->addSelectColumn(RincianDetailPeer::NOTE_SKPD);

		$criteria->addSelectColumn(RincianDetailPeer::NOTE_PENELITI);

		$criteria->addSelectColumn(RincianDetailPeer::NILAI_ANGGARAN);

		$criteria->addSelectColumn(RincianDetailPeer::IS_BLUD);

		$criteria->addSelectColumn(RincianDetailPeer::IS_KAPITASI_BPJS);

		$criteria->addSelectColumn(RincianDetailPeer::LOKASI_KECAMATAN);

		$criteria->addSelectColumn(RincianDetailPeer::LOKASI_KELURAHAN);

		$criteria->addSelectColumn(RincianDetailPeer::OB);

		$criteria->addSelectColumn(RincianDetailPeer::OB_FROM_ID);

		$criteria->addSelectColumn(RincianDetailPeer::IS_PER_KOMPONEN);

		$criteria->addSelectColumn(RincianDetailPeer::KEGIATAN_CODE_ASAL);

		$criteria->addSelectColumn(RincianDetailPeer::TH_KE_MULTIYEARS);

		$criteria->addSelectColumn(RincianDetailPeer::HARGA_SEBELUM_SISA_LELANG);

		$criteria->addSelectColumn(RincianDetailPeer::IS_MUSRENBANG);

		$criteria->addSelectColumn(RincianDetailPeer::SUB_ID_ASAL);

		$criteria->addSelectColumn(RincianDetailPeer::SUBTITLE_ASAL);

		$criteria->addSelectColumn(RincianDetailPeer::KODE_SUB_ASAL);

		$criteria->addSelectColumn(RincianDetailPeer::SUB_ASAL);

		$criteria->addSelectColumn(RincianDetailPeer::LAST_EDIT_TIME);

		$criteria->addSelectColumn(RincianDetailPeer::IS_POTONG_BPJS);

		$criteria->addSelectColumn(RincianDetailPeer::IS_IURAN_BPJS);

		$criteria->addSelectColumn(RincianDetailPeer::STATUS_OB);

		$criteria->addSelectColumn(RincianDetailPeer::OB_PARENT);

		$criteria->addSelectColumn(RincianDetailPeer::OB_ALOKASI_BARU);

		$criteria->addSelectColumn(RincianDetailPeer::IS_HIBAH);

		$criteria->addSelectColumn(RincianDetailPeer::AKRUAL_CODE);

		$criteria->addSelectColumn(RincianDetailPeer::TIPE2);

		$criteria->addSelectColumn(RincianDetailPeer::STATUS_LEVEL);

		$criteria->addSelectColumn(RincianDetailPeer::STATUS_LEVEL_TOLAK);

		$criteria->addSelectColumn(RincianDetailPeer::STATUS_SISIPAN);

		$criteria->addSelectColumn(RincianDetailPeer::IS_TAPD_SETUJU);

		$criteria->addSelectColumn(RincianDetailPeer::IS_BAPPEKO_SETUJU);

		$criteria->addSelectColumn(RincianDetailPeer::IS_PENYELIA_SETUJU);

		$criteria->addSelectColumn(RincianDetailPeer::NOTE_TAPD);

		$criteria->addSelectColumn(RincianDetailPeer::NOTE_BAPPEKO);

		$criteria->addSelectColumn(RincianDetailPeer::SATUAN_SEMULA);

		$criteria->addSelectColumn(RincianDetailPeer::ID_LOKASI);

		$criteria->addSelectColumn(RincianDetailPeer::DETAIL_KEGIATAN);

		$criteria->addSelectColumn(RincianDetailPeer::DETAIL_KEGIATAN_SEMULA);

		$criteria->addSelectColumn(RincianDetailPeer::STATUS_KOMPONEN_BARU);

		$criteria->addSelectColumn(RincianDetailPeer::STATUS_KOMPONEN_BERUBAH);

		$criteria->addSelectColumn(RincianDetailPeer::APPROVE_UNLOCK_HARGA);

		$criteria->addSelectColumn(RincianDetailPeer::TIPE_LELANG);

		$criteria->addSelectColumn(RincianDetailPeer::IS_HPSP);

		$criteria->addSelectColumn(RincianDetailPeer::IS_DAK);

		$criteria->addSelectColumn(RincianDetailPeer::IS_BOS);

		$criteria->addSelectColumn(RincianDetailPeer::IS_BOBDA);

		$criteria->addSelectColumn(RincianDetailPeer::IS_NARSUM);

		$criteria->addSelectColumn(RincianDetailPeer::IS_BAGIAN_HUKUM_SETUJU);

		$criteria->addSelectColumn(RincianDetailPeer::IS_INSPEKTORAT_SETUJU);

		$criteria->addSelectColumn(RincianDetailPeer::IS_BADAN_KEPEGAWAIAN_SETUJU);

		$criteria->addSelectColumn(RincianDetailPeer::IS_LPPA_SETUJU);

		$criteria->addSelectColumn(RincianDetailPeer::PRIORITAS_WALI);

		$criteria->addSelectColumn(RincianDetailPeer::IS_OUTPUT);

		$criteria->addSelectColumn(RincianDetailPeer::IS_BAGIAN_ORGANISASI_SETUJU);

		$criteria->addSelectColumn(RincianDetailPeer::IS_ASISTEN1_SETUJU);

		$criteria->addSelectColumn(RincianDetailPeer::IS_ASISTEN2_SETUJU);

		$criteria->addSelectColumn(RincianDetailPeer::IS_ASISTEN3_SETUJU);

		$criteria->addSelectColumn(RincianDetailPeer::IS_SEKDA_SETUJU);

	}

	const COUNT = 'COUNT(ebudget.rincian_detail.KEGIATAN_CODE)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.rincian_detail.KEGIATAN_CODE)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(RincianDetailPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(RincianDetailPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = RincianDetailPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = RincianDetailPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return RincianDetailPeer::populateObjects(RincianDetailPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			RincianDetailPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = RincianDetailPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return RincianDetailPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(RincianDetailPeer::KEGIATAN_CODE);
			$selectCriteria->add(RincianDetailPeer::KEGIATAN_CODE, $criteria->remove(RincianDetailPeer::KEGIATAN_CODE), $comparison);

			$comparison = $criteria->getComparison(RincianDetailPeer::DETAIL_NO);
			$selectCriteria->add(RincianDetailPeer::DETAIL_NO, $criteria->remove(RincianDetailPeer::DETAIL_NO), $comparison);

			$comparison = $criteria->getComparison(RincianDetailPeer::UNIT_ID);
			$selectCriteria->add(RincianDetailPeer::UNIT_ID, $criteria->remove(RincianDetailPeer::UNIT_ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(RincianDetailPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(RincianDetailPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof RincianDetail) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
												if(count($values) == count($values, COUNT_RECURSIVE))
			{
								$values = array($values);
			}
			$vals = array();
			foreach($values as $value)
			{

				$vals[0][] = $value[0];
				$vals[1][] = $value[1];
				$vals[2][] = $value[2];
			}

			$criteria->add(RincianDetailPeer::KEGIATAN_CODE, $vals[0], Criteria::IN);
			$criteria->add(RincianDetailPeer::DETAIL_NO, $vals[1], Criteria::IN);
			$criteria->add(RincianDetailPeer::UNIT_ID, $vals[2], Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(RincianDetail $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(RincianDetailPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(RincianDetailPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(RincianDetailPeer::DATABASE_NAME, RincianDetailPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = RincianDetailPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK( $kegiatan_code, $detail_no, $unit_id, $con = null) {
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$criteria = new Criteria();
		$criteria->add(RincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
		$criteria->add(RincianDetailPeer::DETAIL_NO, $detail_no);
		$criteria->add(RincianDetailPeer::UNIT_ID, $unit_id);
		$v = RincianDetailPeer::doSelect($criteria, $con);

		return !empty($v) ? $v[0] : null;
	}
} 
if (Propel::isInit()) {
			try {
		BaseRincianDetailPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/RincianDetailMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.RincianDetailMapBuilder');
}
