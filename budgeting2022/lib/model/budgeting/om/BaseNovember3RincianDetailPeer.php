<?php


abstract class BaseNovember3RincianDetailPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.november3_rincian_detail';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.November3RincianDetail';

	
	const NUM_COLUMNS = 42;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const KEGIATAN_CODE = 'ebudget.november3_rincian_detail.KEGIATAN_CODE';

	
	const TIPE = 'ebudget.november3_rincian_detail.TIPE';

	
	const DETAIL_NO = 'ebudget.november3_rincian_detail.DETAIL_NO';

	
	const REKENING_CODE = 'ebudget.november3_rincian_detail.REKENING_CODE';

	
	const KOMPONEN_ID = 'ebudget.november3_rincian_detail.KOMPONEN_ID';

	
	const DETAIL_NAME = 'ebudget.november3_rincian_detail.DETAIL_NAME';

	
	const VOLUME = 'ebudget.november3_rincian_detail.VOLUME';

	
	const KETERANGAN_KOEFISIEN = 'ebudget.november3_rincian_detail.KETERANGAN_KOEFISIEN';

	
	const SUBTITLE = 'ebudget.november3_rincian_detail.SUBTITLE';

	
	const KOMPONEN_HARGA = 'ebudget.november3_rincian_detail.KOMPONEN_HARGA';

	
	const KOMPONEN_HARGA_AWAL = 'ebudget.november3_rincian_detail.KOMPONEN_HARGA_AWAL';

	
	const KOMPONEN_NAME = 'ebudget.november3_rincian_detail.KOMPONEN_NAME';

	
	const SATUAN = 'ebudget.november3_rincian_detail.SATUAN';

	
	const PAJAK = 'ebudget.november3_rincian_detail.PAJAK';

	
	const UNIT_ID = 'ebudget.november3_rincian_detail.UNIT_ID';

	
	const FROM_SUB_KEGIATAN = 'ebudget.november3_rincian_detail.FROM_SUB_KEGIATAN';

	
	const SUB = 'ebudget.november3_rincian_detail.SUB';

	
	const KODE_SUB = 'ebudget.november3_rincian_detail.KODE_SUB';

	
	const LAST_UPDATE_USER = 'ebudget.november3_rincian_detail.LAST_UPDATE_USER';

	
	const LAST_UPDATE_TIME = 'ebudget.november3_rincian_detail.LAST_UPDATE_TIME';

	
	const LAST_UPDATE_IP = 'ebudget.november3_rincian_detail.LAST_UPDATE_IP';

	
	const TAHAP = 'ebudget.november3_rincian_detail.TAHAP';

	
	const TAHAP_EDIT = 'ebudget.november3_rincian_detail.TAHAP_EDIT';

	
	const TAHAP_NEW = 'ebudget.november3_rincian_detail.TAHAP_NEW';

	
	const STATUS_LELANG = 'ebudget.november3_rincian_detail.STATUS_LELANG';

	
	const NOMOR_LELANG = 'ebudget.november3_rincian_detail.NOMOR_LELANG';

	
	const KOEFISIEN_SEMULA = 'ebudget.november3_rincian_detail.KOEFISIEN_SEMULA';

	
	const VOLUME_SEMULA = 'ebudget.november3_rincian_detail.VOLUME_SEMULA';

	
	const HARGA_SEMULA = 'ebudget.november3_rincian_detail.HARGA_SEMULA';

	
	const TOTAL_SEMULA = 'ebudget.november3_rincian_detail.TOTAL_SEMULA';

	
	const LOCK_SUBTITLE = 'ebudget.november3_rincian_detail.LOCK_SUBTITLE';

	
	const STATUS_HAPUS = 'ebudget.november3_rincian_detail.STATUS_HAPUS';

	
	const TAHUN = 'ebudget.november3_rincian_detail.TAHUN';

	
	const KODE_LOKASI = 'ebudget.november3_rincian_detail.KODE_LOKASI';

	
	const KECAMATAN = 'ebudget.november3_rincian_detail.KECAMATAN';

	
	const REKENING_CODE_ASLI = 'ebudget.november3_rincian_detail.REKENING_CODE_ASLI';

	
	const NOTE_SKPD = 'ebudget.november3_rincian_detail.NOTE_SKPD';

	
	const NOTE_PENELITI = 'ebudget.november3_rincian_detail.NOTE_PENELITI';

	
	const NILAI_ANGGARAN = 'ebudget.november3_rincian_detail.NILAI_ANGGARAN';

	
	const IS_BLUD = 'ebudget.november3_rincian_detail.IS_BLUD';

	
	const LOKASI_KECAMATAN = 'ebudget.november3_rincian_detail.LOKASI_KECAMATAN';

	
	const LOKASI_KELURAHAN = 'ebudget.november3_rincian_detail.LOKASI_KELURAHAN';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('KegiatanCode', 'Tipe', 'DetailNo', 'RekeningCode', 'KomponenId', 'DetailName', 'Volume', 'KeteranganKoefisien', 'Subtitle', 'KomponenHarga', 'KomponenHargaAwal', 'KomponenName', 'Satuan', 'Pajak', 'UnitId', 'FromSubKegiatan', 'Sub', 'KodeSub', 'LastUpdateUser', 'LastUpdateTime', 'LastUpdateIp', 'Tahap', 'TahapEdit', 'TahapNew', 'StatusLelang', 'NomorLelang', 'KoefisienSemula', 'VolumeSemula', 'HargaSemula', 'TotalSemula', 'LockSubtitle', 'StatusHapus', 'Tahun', 'KodeLokasi', 'Kecamatan', 'RekeningCodeAsli', 'NoteSkpd', 'NotePeneliti', 'NilaiAnggaran', 'IsBlud', 'LokasiKecamatan', 'LokasiKelurahan', ),
		BasePeer::TYPE_COLNAME => array (November3RincianDetailPeer::KEGIATAN_CODE, November3RincianDetailPeer::TIPE, November3RincianDetailPeer::DETAIL_NO, November3RincianDetailPeer::REKENING_CODE, November3RincianDetailPeer::KOMPONEN_ID, November3RincianDetailPeer::DETAIL_NAME, November3RincianDetailPeer::VOLUME, November3RincianDetailPeer::KETERANGAN_KOEFISIEN, November3RincianDetailPeer::SUBTITLE, November3RincianDetailPeer::KOMPONEN_HARGA, November3RincianDetailPeer::KOMPONEN_HARGA_AWAL, November3RincianDetailPeer::KOMPONEN_NAME, November3RincianDetailPeer::SATUAN, November3RincianDetailPeer::PAJAK, November3RincianDetailPeer::UNIT_ID, November3RincianDetailPeer::FROM_SUB_KEGIATAN, November3RincianDetailPeer::SUB, November3RincianDetailPeer::KODE_SUB, November3RincianDetailPeer::LAST_UPDATE_USER, November3RincianDetailPeer::LAST_UPDATE_TIME, November3RincianDetailPeer::LAST_UPDATE_IP, November3RincianDetailPeer::TAHAP, November3RincianDetailPeer::TAHAP_EDIT, November3RincianDetailPeer::TAHAP_NEW, November3RincianDetailPeer::STATUS_LELANG, November3RincianDetailPeer::NOMOR_LELANG, November3RincianDetailPeer::KOEFISIEN_SEMULA, November3RincianDetailPeer::VOLUME_SEMULA, November3RincianDetailPeer::HARGA_SEMULA, November3RincianDetailPeer::TOTAL_SEMULA, November3RincianDetailPeer::LOCK_SUBTITLE, November3RincianDetailPeer::STATUS_HAPUS, November3RincianDetailPeer::TAHUN, November3RincianDetailPeer::KODE_LOKASI, November3RincianDetailPeer::KECAMATAN, November3RincianDetailPeer::REKENING_CODE_ASLI, November3RincianDetailPeer::NOTE_SKPD, November3RincianDetailPeer::NOTE_PENELITI, November3RincianDetailPeer::NILAI_ANGGARAN, November3RincianDetailPeer::IS_BLUD, November3RincianDetailPeer::LOKASI_KECAMATAN, November3RincianDetailPeer::LOKASI_KELURAHAN, ),
		BasePeer::TYPE_FIELDNAME => array ('kegiatan_code', 'tipe', 'detail_no', 'rekening_code', 'komponen_id', 'detail_name', 'volume', 'keterangan_koefisien', 'subtitle', 'komponen_harga', 'komponen_harga_awal', 'komponen_name', 'satuan', 'pajak', 'unit_id', 'from_sub_kegiatan', 'sub', 'kode_sub', 'last_update_user', 'last_update_time', 'last_update_ip', 'tahap', 'tahap_edit', 'tahap_new', 'status_lelang', 'nomor_lelang', 'koefisien_semula', 'volume_semula', 'harga_semula', 'total_semula', 'lock_subtitle', 'status_hapus', 'tahun', 'kode_lokasi', 'kecamatan', 'rekening_code_asli', 'note_skpd', 'note_peneliti', 'nilai_anggaran', 'is_blud', 'lokasi_kecamatan', 'lokasi_kelurahan', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('KegiatanCode' => 0, 'Tipe' => 1, 'DetailNo' => 2, 'RekeningCode' => 3, 'KomponenId' => 4, 'DetailName' => 5, 'Volume' => 6, 'KeteranganKoefisien' => 7, 'Subtitle' => 8, 'KomponenHarga' => 9, 'KomponenHargaAwal' => 10, 'KomponenName' => 11, 'Satuan' => 12, 'Pajak' => 13, 'UnitId' => 14, 'FromSubKegiatan' => 15, 'Sub' => 16, 'KodeSub' => 17, 'LastUpdateUser' => 18, 'LastUpdateTime' => 19, 'LastUpdateIp' => 20, 'Tahap' => 21, 'TahapEdit' => 22, 'TahapNew' => 23, 'StatusLelang' => 24, 'NomorLelang' => 25, 'KoefisienSemula' => 26, 'VolumeSemula' => 27, 'HargaSemula' => 28, 'TotalSemula' => 29, 'LockSubtitle' => 30, 'StatusHapus' => 31, 'Tahun' => 32, 'KodeLokasi' => 33, 'Kecamatan' => 34, 'RekeningCodeAsli' => 35, 'NoteSkpd' => 36, 'NotePeneliti' => 37, 'NilaiAnggaran' => 38, 'IsBlud' => 39, 'LokasiKecamatan' => 40, 'LokasiKelurahan' => 41, ),
		BasePeer::TYPE_COLNAME => array (November3RincianDetailPeer::KEGIATAN_CODE => 0, November3RincianDetailPeer::TIPE => 1, November3RincianDetailPeer::DETAIL_NO => 2, November3RincianDetailPeer::REKENING_CODE => 3, November3RincianDetailPeer::KOMPONEN_ID => 4, November3RincianDetailPeer::DETAIL_NAME => 5, November3RincianDetailPeer::VOLUME => 6, November3RincianDetailPeer::KETERANGAN_KOEFISIEN => 7, November3RincianDetailPeer::SUBTITLE => 8, November3RincianDetailPeer::KOMPONEN_HARGA => 9, November3RincianDetailPeer::KOMPONEN_HARGA_AWAL => 10, November3RincianDetailPeer::KOMPONEN_NAME => 11, November3RincianDetailPeer::SATUAN => 12, November3RincianDetailPeer::PAJAK => 13, November3RincianDetailPeer::UNIT_ID => 14, November3RincianDetailPeer::FROM_SUB_KEGIATAN => 15, November3RincianDetailPeer::SUB => 16, November3RincianDetailPeer::KODE_SUB => 17, November3RincianDetailPeer::LAST_UPDATE_USER => 18, November3RincianDetailPeer::LAST_UPDATE_TIME => 19, November3RincianDetailPeer::LAST_UPDATE_IP => 20, November3RincianDetailPeer::TAHAP => 21, November3RincianDetailPeer::TAHAP_EDIT => 22, November3RincianDetailPeer::TAHAP_NEW => 23, November3RincianDetailPeer::STATUS_LELANG => 24, November3RincianDetailPeer::NOMOR_LELANG => 25, November3RincianDetailPeer::KOEFISIEN_SEMULA => 26, November3RincianDetailPeer::VOLUME_SEMULA => 27, November3RincianDetailPeer::HARGA_SEMULA => 28, November3RincianDetailPeer::TOTAL_SEMULA => 29, November3RincianDetailPeer::LOCK_SUBTITLE => 30, November3RincianDetailPeer::STATUS_HAPUS => 31, November3RincianDetailPeer::TAHUN => 32, November3RincianDetailPeer::KODE_LOKASI => 33, November3RincianDetailPeer::KECAMATAN => 34, November3RincianDetailPeer::REKENING_CODE_ASLI => 35, November3RincianDetailPeer::NOTE_SKPD => 36, November3RincianDetailPeer::NOTE_PENELITI => 37, November3RincianDetailPeer::NILAI_ANGGARAN => 38, November3RincianDetailPeer::IS_BLUD => 39, November3RincianDetailPeer::LOKASI_KECAMATAN => 40, November3RincianDetailPeer::LOKASI_KELURAHAN => 41, ),
		BasePeer::TYPE_FIELDNAME => array ('kegiatan_code' => 0, 'tipe' => 1, 'detail_no' => 2, 'rekening_code' => 3, 'komponen_id' => 4, 'detail_name' => 5, 'volume' => 6, 'keterangan_koefisien' => 7, 'subtitle' => 8, 'komponen_harga' => 9, 'komponen_harga_awal' => 10, 'komponen_name' => 11, 'satuan' => 12, 'pajak' => 13, 'unit_id' => 14, 'from_sub_kegiatan' => 15, 'sub' => 16, 'kode_sub' => 17, 'last_update_user' => 18, 'last_update_time' => 19, 'last_update_ip' => 20, 'tahap' => 21, 'tahap_edit' => 22, 'tahap_new' => 23, 'status_lelang' => 24, 'nomor_lelang' => 25, 'koefisien_semula' => 26, 'volume_semula' => 27, 'harga_semula' => 28, 'total_semula' => 29, 'lock_subtitle' => 30, 'status_hapus' => 31, 'tahun' => 32, 'kode_lokasi' => 33, 'kecamatan' => 34, 'rekening_code_asli' => 35, 'note_skpd' => 36, 'note_peneliti' => 37, 'nilai_anggaran' => 38, 'is_blud' => 39, 'lokasi_kecamatan' => 40, 'lokasi_kelurahan' => 41, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/November3RincianDetailMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.November3RincianDetailMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = November3RincianDetailPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(November3RincianDetailPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(November3RincianDetailPeer::KEGIATAN_CODE);

		$criteria->addSelectColumn(November3RincianDetailPeer::TIPE);

		$criteria->addSelectColumn(November3RincianDetailPeer::DETAIL_NO);

		$criteria->addSelectColumn(November3RincianDetailPeer::REKENING_CODE);

		$criteria->addSelectColumn(November3RincianDetailPeer::KOMPONEN_ID);

		$criteria->addSelectColumn(November3RincianDetailPeer::DETAIL_NAME);

		$criteria->addSelectColumn(November3RincianDetailPeer::VOLUME);

		$criteria->addSelectColumn(November3RincianDetailPeer::KETERANGAN_KOEFISIEN);

		$criteria->addSelectColumn(November3RincianDetailPeer::SUBTITLE);

		$criteria->addSelectColumn(November3RincianDetailPeer::KOMPONEN_HARGA);

		$criteria->addSelectColumn(November3RincianDetailPeer::KOMPONEN_HARGA_AWAL);

		$criteria->addSelectColumn(November3RincianDetailPeer::KOMPONEN_NAME);

		$criteria->addSelectColumn(November3RincianDetailPeer::SATUAN);

		$criteria->addSelectColumn(November3RincianDetailPeer::PAJAK);

		$criteria->addSelectColumn(November3RincianDetailPeer::UNIT_ID);

		$criteria->addSelectColumn(November3RincianDetailPeer::FROM_SUB_KEGIATAN);

		$criteria->addSelectColumn(November3RincianDetailPeer::SUB);

		$criteria->addSelectColumn(November3RincianDetailPeer::KODE_SUB);

		$criteria->addSelectColumn(November3RincianDetailPeer::LAST_UPDATE_USER);

		$criteria->addSelectColumn(November3RincianDetailPeer::LAST_UPDATE_TIME);

		$criteria->addSelectColumn(November3RincianDetailPeer::LAST_UPDATE_IP);

		$criteria->addSelectColumn(November3RincianDetailPeer::TAHAP);

		$criteria->addSelectColumn(November3RincianDetailPeer::TAHAP_EDIT);

		$criteria->addSelectColumn(November3RincianDetailPeer::TAHAP_NEW);

		$criteria->addSelectColumn(November3RincianDetailPeer::STATUS_LELANG);

		$criteria->addSelectColumn(November3RincianDetailPeer::NOMOR_LELANG);

		$criteria->addSelectColumn(November3RincianDetailPeer::KOEFISIEN_SEMULA);

		$criteria->addSelectColumn(November3RincianDetailPeer::VOLUME_SEMULA);

		$criteria->addSelectColumn(November3RincianDetailPeer::HARGA_SEMULA);

		$criteria->addSelectColumn(November3RincianDetailPeer::TOTAL_SEMULA);

		$criteria->addSelectColumn(November3RincianDetailPeer::LOCK_SUBTITLE);

		$criteria->addSelectColumn(November3RincianDetailPeer::STATUS_HAPUS);

		$criteria->addSelectColumn(November3RincianDetailPeer::TAHUN);

		$criteria->addSelectColumn(November3RincianDetailPeer::KODE_LOKASI);

		$criteria->addSelectColumn(November3RincianDetailPeer::KECAMATAN);

		$criteria->addSelectColumn(November3RincianDetailPeer::REKENING_CODE_ASLI);

		$criteria->addSelectColumn(November3RincianDetailPeer::NOTE_SKPD);

		$criteria->addSelectColumn(November3RincianDetailPeer::NOTE_PENELITI);

		$criteria->addSelectColumn(November3RincianDetailPeer::NILAI_ANGGARAN);

		$criteria->addSelectColumn(November3RincianDetailPeer::IS_BLUD);

		$criteria->addSelectColumn(November3RincianDetailPeer::LOKASI_KECAMATAN);

		$criteria->addSelectColumn(November3RincianDetailPeer::LOKASI_KELURAHAN);

	}

	const COUNT = 'COUNT(ebudget.november3_rincian_detail.KEGIATAN_CODE)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.november3_rincian_detail.KEGIATAN_CODE)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(November3RincianDetailPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(November3RincianDetailPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = November3RincianDetailPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = November3RincianDetailPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return November3RincianDetailPeer::populateObjects(November3RincianDetailPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			November3RincianDetailPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = November3RincianDetailPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return November3RincianDetailPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(November3RincianDetailPeer::KEGIATAN_CODE);
			$selectCriteria->add(November3RincianDetailPeer::KEGIATAN_CODE, $criteria->remove(November3RincianDetailPeer::KEGIATAN_CODE), $comparison);

			$comparison = $criteria->getComparison(November3RincianDetailPeer::TIPE);
			$selectCriteria->add(November3RincianDetailPeer::TIPE, $criteria->remove(November3RincianDetailPeer::TIPE), $comparison);

			$comparison = $criteria->getComparison(November3RincianDetailPeer::DETAIL_NO);
			$selectCriteria->add(November3RincianDetailPeer::DETAIL_NO, $criteria->remove(November3RincianDetailPeer::DETAIL_NO), $comparison);

			$comparison = $criteria->getComparison(November3RincianDetailPeer::UNIT_ID);
			$selectCriteria->add(November3RincianDetailPeer::UNIT_ID, $criteria->remove(November3RincianDetailPeer::UNIT_ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(November3RincianDetailPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(November3RincianDetailPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof November3RincianDetail) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
												if(count($values) == count($values, COUNT_RECURSIVE))
			{
								$values = array($values);
			}
			$vals = array();
			foreach($values as $value)
			{

				$vals[0][] = $value[0];
				$vals[1][] = $value[1];
				$vals[2][] = $value[2];
				$vals[3][] = $value[3];
			}

			$criteria->add(November3RincianDetailPeer::KEGIATAN_CODE, $vals[0], Criteria::IN);
			$criteria->add(November3RincianDetailPeer::TIPE, $vals[1], Criteria::IN);
			$criteria->add(November3RincianDetailPeer::DETAIL_NO, $vals[2], Criteria::IN);
			$criteria->add(November3RincianDetailPeer::UNIT_ID, $vals[3], Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(November3RincianDetail $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(November3RincianDetailPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(November3RincianDetailPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(November3RincianDetailPeer::DATABASE_NAME, November3RincianDetailPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = November3RincianDetailPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK( $kegiatan_code, $tipe, $detail_no, $unit_id, $con = null) {
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$criteria = new Criteria();
		$criteria->add(November3RincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
		$criteria->add(November3RincianDetailPeer::TIPE, $tipe);
		$criteria->add(November3RincianDetailPeer::DETAIL_NO, $detail_no);
		$criteria->add(November3RincianDetailPeer::UNIT_ID, $unit_id);
		$v = November3RincianDetailPeer::doSelect($criteria, $con);

		return !empty($v) ? $v[0] : null;
	}
} 
if (Propel::isInit()) {
			try {
		BaseNovember3RincianDetailPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/November3RincianDetailMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.November3RincianDetailMapBuilder');
}
