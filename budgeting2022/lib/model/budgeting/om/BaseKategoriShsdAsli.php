<?php


abstract class BaseKategoriShsdAsli extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $kategori_shsd_id;


	
	protected $kategori_shsd_name;


	
	protected $rekening_code;


	
	protected $rekening;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getKategoriShsdId()
	{

		return $this->kategori_shsd_id;
	}

	
	public function getKategoriShsdName()
	{

		return $this->kategori_shsd_name;
	}

	
	public function getRekeningCode()
	{

		return $this->rekening_code;
	}

	
	public function getRekening()
	{

		return $this->rekening;
	}

	
	public function setKategoriShsdId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kategori_shsd_id !== $v) {
			$this->kategori_shsd_id = $v;
			$this->modifiedColumns[] = KategoriShsdAsliPeer::KATEGORI_SHSD_ID;
		}

	} 
	
	public function setKategoriShsdName($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kategori_shsd_name !== $v) {
			$this->kategori_shsd_name = $v;
			$this->modifiedColumns[] = KategoriShsdAsliPeer::KATEGORI_SHSD_NAME;
		}

	} 
	
	public function setRekeningCode($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->rekening_code !== $v) {
			$this->rekening_code = $v;
			$this->modifiedColumns[] = KategoriShsdAsliPeer::REKENING_CODE;
		}

	} 
	
	public function setRekening($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->rekening !== $v) {
			$this->rekening = $v;
			$this->modifiedColumns[] = KategoriShsdAsliPeer::REKENING;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->kategori_shsd_id = $rs->getString($startcol + 0);

			$this->kategori_shsd_name = $rs->getString($startcol + 1);

			$this->rekening_code = $rs->getString($startcol + 2);

			$this->rekening = $rs->getString($startcol + 3);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 4; 
		} catch (Exception $e) {
			throw new PropelException("Error populating KategoriShsdAsli object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(KategoriShsdAsliPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			KategoriShsdAsliPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(KategoriShsdAsliPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = KategoriShsdAsliPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += KategoriShsdAsliPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = KategoriShsdAsliPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = KategoriShsdAsliPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getKategoriShsdId();
				break;
			case 1:
				return $this->getKategoriShsdName();
				break;
			case 2:
				return $this->getRekeningCode();
				break;
			case 3:
				return $this->getRekening();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = KategoriShsdAsliPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getKategoriShsdId(),
			$keys[1] => $this->getKategoriShsdName(),
			$keys[2] => $this->getRekeningCode(),
			$keys[3] => $this->getRekening(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = KategoriShsdAsliPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setKategoriShsdId($value);
				break;
			case 1:
				$this->setKategoriShsdName($value);
				break;
			case 2:
				$this->setRekeningCode($value);
				break;
			case 3:
				$this->setRekening($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = KategoriShsdAsliPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setKategoriShsdId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setKategoriShsdName($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setRekeningCode($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setRekening($arr[$keys[3]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(KategoriShsdAsliPeer::DATABASE_NAME);

		if ($this->isColumnModified(KategoriShsdAsliPeer::KATEGORI_SHSD_ID)) $criteria->add(KategoriShsdAsliPeer::KATEGORI_SHSD_ID, $this->kategori_shsd_id);
		if ($this->isColumnModified(KategoriShsdAsliPeer::KATEGORI_SHSD_NAME)) $criteria->add(KategoriShsdAsliPeer::KATEGORI_SHSD_NAME, $this->kategori_shsd_name);
		if ($this->isColumnModified(KategoriShsdAsliPeer::REKENING_CODE)) $criteria->add(KategoriShsdAsliPeer::REKENING_CODE, $this->rekening_code);
		if ($this->isColumnModified(KategoriShsdAsliPeer::REKENING)) $criteria->add(KategoriShsdAsliPeer::REKENING, $this->rekening);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(KategoriShsdAsliPeer::DATABASE_NAME);


		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return null;
	}

	
	 public function setPrimaryKey($pk)
	 {
		 	 }

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setKategoriShsdId($this->kategori_shsd_id);

		$copyObj->setKategoriShsdName($this->kategori_shsd_name);

		$copyObj->setRekeningCode($this->rekening_code);

		$copyObj->setRekening($this->rekening);


		$copyObj->setNew(true);

	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new KategoriShsdAsliPeer();
		}
		return self::$peer;
	}

} 