<?php


abstract class BaseKategoriShsd extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $kategori_shsd_id;


	
	protected $kategori_shsd_name;


	
	protected $rekening_code;


	
	protected $rekening;


	
	protected $akrual_code;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getKategoriShsdId()
	{

		return $this->kategori_shsd_id;
	}

	
	public function getKategoriShsdName()
	{

		return $this->kategori_shsd_name;
	}

	
	public function getRekeningCode()
	{

		return $this->rekening_code;
	}

	
	public function getRekening()
	{

		return $this->rekening;
	}

	
	public function getAkrualCode()
	{

		return $this->akrual_code;
	}

	
	public function setKategoriShsdId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kategori_shsd_id !== $v) {
			$this->kategori_shsd_id = $v;
			$this->modifiedColumns[] = KategoriShsdPeer::KATEGORI_SHSD_ID;
		}

	} 
	
	public function setKategoriShsdName($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kategori_shsd_name !== $v) {
			$this->kategori_shsd_name = $v;
			$this->modifiedColumns[] = KategoriShsdPeer::KATEGORI_SHSD_NAME;
		}

	} 
	
	public function setRekeningCode($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->rekening_code !== $v) {
			$this->rekening_code = $v;
			$this->modifiedColumns[] = KategoriShsdPeer::REKENING_CODE;
		}

	} 
	
	public function setRekening($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->rekening !== $v) {
			$this->rekening = $v;
			$this->modifiedColumns[] = KategoriShsdPeer::REKENING;
		}

	} 
	
	public function setAkrualCode($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->akrual_code !== $v) {
			$this->akrual_code = $v;
			$this->modifiedColumns[] = KategoriShsdPeer::AKRUAL_CODE;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->kategori_shsd_id = $rs->getString($startcol + 0);

			$this->kategori_shsd_name = $rs->getString($startcol + 1);

			$this->rekening_code = $rs->getString($startcol + 2);

			$this->rekening = $rs->getString($startcol + 3);

			$this->akrual_code = $rs->getString($startcol + 4);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 5; 
		} catch (Exception $e) {
			throw new PropelException("Error populating KategoriShsd object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(KategoriShsdPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			KategoriShsdPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(KategoriShsdPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = KategoriShsdPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += KategoriShsdPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = KategoriShsdPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = KategoriShsdPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getKategoriShsdId();
				break;
			case 1:
				return $this->getKategoriShsdName();
				break;
			case 2:
				return $this->getRekeningCode();
				break;
			case 3:
				return $this->getRekening();
				break;
			case 4:
				return $this->getAkrualCode();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = KategoriShsdPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getKategoriShsdId(),
			$keys[1] => $this->getKategoriShsdName(),
			$keys[2] => $this->getRekeningCode(),
			$keys[3] => $this->getRekening(),
			$keys[4] => $this->getAkrualCode(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = KategoriShsdPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setKategoriShsdId($value);
				break;
			case 1:
				$this->setKategoriShsdName($value);
				break;
			case 2:
				$this->setRekeningCode($value);
				break;
			case 3:
				$this->setRekening($value);
				break;
			case 4:
				$this->setAkrualCode($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = KategoriShsdPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setKategoriShsdId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setKategoriShsdName($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setRekeningCode($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setRekening($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setAkrualCode($arr[$keys[4]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(KategoriShsdPeer::DATABASE_NAME);

		if ($this->isColumnModified(KategoriShsdPeer::KATEGORI_SHSD_ID)) $criteria->add(KategoriShsdPeer::KATEGORI_SHSD_ID, $this->kategori_shsd_id);
		if ($this->isColumnModified(KategoriShsdPeer::KATEGORI_SHSD_NAME)) $criteria->add(KategoriShsdPeer::KATEGORI_SHSD_NAME, $this->kategori_shsd_name);
		if ($this->isColumnModified(KategoriShsdPeer::REKENING_CODE)) $criteria->add(KategoriShsdPeer::REKENING_CODE, $this->rekening_code);
		if ($this->isColumnModified(KategoriShsdPeer::REKENING)) $criteria->add(KategoriShsdPeer::REKENING, $this->rekening);
		if ($this->isColumnModified(KategoriShsdPeer::AKRUAL_CODE)) $criteria->add(KategoriShsdPeer::AKRUAL_CODE, $this->akrual_code);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(KategoriShsdPeer::DATABASE_NAME);


		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return null;
	}

	
	 public function setPrimaryKey($pk)
	 {
		 	 }

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setKategoriShsdId($this->kategori_shsd_id);

		$copyObj->setKategoriShsdName($this->kategori_shsd_name);

		$copyObj->setRekeningCode($this->rekening_code);

		$copyObj->setRekening($this->rekening);

		$copyObj->setAkrualCode($this->akrual_code);


		$copyObj->setNew(true);

	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new KategoriShsdPeer();
		}
		return self::$peer;
	}

} 