<?php


abstract class BaseMasterProgram extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $kode_program;


	
	protected $nama_program;


	
	protected $bappeko;


	
	protected $kode_tujuan;


	
	protected $kode_lutfi;


	
	protected $indikator;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getKodeProgram()
	{

		return $this->kode_program;
	}

	
	public function getNamaProgram()
	{

		return $this->nama_program;
	}

	
	public function getBappeko()
	{

		return $this->bappeko;
	}

	
	public function getKodeTujuan()
	{

		return $this->kode_tujuan;
	}

	
	public function getKodeLutfi()
	{

		return $this->kode_lutfi;
	}

	
	public function getIndikator()
	{

		return $this->indikator;
	}

	
	public function setKodeProgram($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_program !== $v) {
			$this->kode_program = $v;
			$this->modifiedColumns[] = MasterProgramPeer::KODE_PROGRAM;
		}

	} 
	
	public function setNamaProgram($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nama_program !== $v) {
			$this->nama_program = $v;
			$this->modifiedColumns[] = MasterProgramPeer::NAMA_PROGRAM;
		}

	} 
	
	public function setBappeko($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->bappeko !== $v) {
			$this->bappeko = $v;
			$this->modifiedColumns[] = MasterProgramPeer::BAPPEKO;
		}

	} 
	
	public function setKodeTujuan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_tujuan !== $v) {
			$this->kode_tujuan = $v;
			$this->modifiedColumns[] = MasterProgramPeer::KODE_TUJUAN;
		}

	} 
	
	public function setKodeLutfi($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_lutfi !== $v) {
			$this->kode_lutfi = $v;
			$this->modifiedColumns[] = MasterProgramPeer::KODE_LUTFI;
		}

	} 
	
	public function setIndikator($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->indikator !== $v) {
			$this->indikator = $v;
			$this->modifiedColumns[] = MasterProgramPeer::INDIKATOR;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->kode_program = $rs->getString($startcol + 0);

			$this->nama_program = $rs->getString($startcol + 1);

			$this->bappeko = $rs->getInt($startcol + 2);

			$this->kode_tujuan = $rs->getString($startcol + 3);

			$this->kode_lutfi = $rs->getString($startcol + 4);

			$this->indikator = $rs->getString($startcol + 5);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 6; 
		} catch (Exception $e) {
			throw new PropelException("Error populating MasterProgram object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(MasterProgramPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			MasterProgramPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(MasterProgramPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = MasterProgramPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += MasterProgramPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = MasterProgramPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = MasterProgramPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getKodeProgram();
				break;
			case 1:
				return $this->getNamaProgram();
				break;
			case 2:
				return $this->getBappeko();
				break;
			case 3:
				return $this->getKodeTujuan();
				break;
			case 4:
				return $this->getKodeLutfi();
				break;
			case 5:
				return $this->getIndikator();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = MasterProgramPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getKodeProgram(),
			$keys[1] => $this->getNamaProgram(),
			$keys[2] => $this->getBappeko(),
			$keys[3] => $this->getKodeTujuan(),
			$keys[4] => $this->getKodeLutfi(),
			$keys[5] => $this->getIndikator(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = MasterProgramPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setKodeProgram($value);
				break;
			case 1:
				$this->setNamaProgram($value);
				break;
			case 2:
				$this->setBappeko($value);
				break;
			case 3:
				$this->setKodeTujuan($value);
				break;
			case 4:
				$this->setKodeLutfi($value);
				break;
			case 5:
				$this->setIndikator($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = MasterProgramPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setKodeProgram($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setNamaProgram($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setBappeko($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setKodeTujuan($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setKodeLutfi($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setIndikator($arr[$keys[5]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(MasterProgramPeer::DATABASE_NAME);

		if ($this->isColumnModified(MasterProgramPeer::KODE_PROGRAM)) $criteria->add(MasterProgramPeer::KODE_PROGRAM, $this->kode_program);
		if ($this->isColumnModified(MasterProgramPeer::NAMA_PROGRAM)) $criteria->add(MasterProgramPeer::NAMA_PROGRAM, $this->nama_program);
		if ($this->isColumnModified(MasterProgramPeer::BAPPEKO)) $criteria->add(MasterProgramPeer::BAPPEKO, $this->bappeko);
		if ($this->isColumnModified(MasterProgramPeer::KODE_TUJUAN)) $criteria->add(MasterProgramPeer::KODE_TUJUAN, $this->kode_tujuan);
		if ($this->isColumnModified(MasterProgramPeer::KODE_LUTFI)) $criteria->add(MasterProgramPeer::KODE_LUTFI, $this->kode_lutfi);
		if ($this->isColumnModified(MasterProgramPeer::INDIKATOR)) $criteria->add(MasterProgramPeer::INDIKATOR, $this->indikator);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(MasterProgramPeer::DATABASE_NAME);

		$criteria->add(MasterProgramPeer::KODE_PROGRAM, $this->kode_program);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getKodeProgram();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setKodeProgram($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setNamaProgram($this->nama_program);

		$copyObj->setBappeko($this->bappeko);

		$copyObj->setKodeTujuan($this->kode_tujuan);

		$copyObj->setKodeLutfi($this->kode_lutfi);

		$copyObj->setIndikator($this->indikator);


		$copyObj->setNew(true);

		$copyObj->setKodeProgram(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new MasterProgramPeer();
		}
		return self::$peer;
	}

} 