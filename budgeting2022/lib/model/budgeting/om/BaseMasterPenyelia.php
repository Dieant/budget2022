<?php


abstract class BaseMasterPenyelia extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id_penyelia;


	
	protected $username;


	
	protected $useremail;


	
	protected $aktif;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getIdPenyelia()
	{

		return $this->id_penyelia;
	}

	
	public function getUsername()
	{

		return $this->username;
	}

	
	public function getUseremail()
	{

		return $this->useremail;
	}

	
	public function getAktif()
	{

		return $this->aktif;
	}

	
	public function setIdPenyelia($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id_penyelia !== $v) {
			$this->id_penyelia = $v;
			$this->modifiedColumns[] = MasterPenyeliaPeer::ID_PENYELIA;
		}

	} 
	
	public function setUsername($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->username !== $v) {
			$this->username = $v;
			$this->modifiedColumns[] = MasterPenyeliaPeer::USERNAME;
		}

	} 
	
	public function setUseremail($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->useremail !== $v) {
			$this->useremail = $v;
			$this->modifiedColumns[] = MasterPenyeliaPeer::USEREMAIL;
		}

	} 
	
	public function setAktif($v)
	{

		if ($this->aktif !== $v) {
			$this->aktif = $v;
			$this->modifiedColumns[] = MasterPenyeliaPeer::AKTIF;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id_penyelia = $rs->getInt($startcol + 0);

			$this->username = $rs->getString($startcol + 1);

			$this->useremail = $rs->getString($startcol + 2);

			$this->aktif = $rs->getBoolean($startcol + 3);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 4; 
		} catch (Exception $e) {
			throw new PropelException("Error populating MasterPenyelia object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(MasterPenyeliaPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			MasterPenyeliaPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(MasterPenyeliaPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = MasterPenyeliaPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += MasterPenyeliaPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = MasterPenyeliaPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = MasterPenyeliaPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getIdPenyelia();
				break;
			case 1:
				return $this->getUsername();
				break;
			case 2:
				return $this->getUseremail();
				break;
			case 3:
				return $this->getAktif();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = MasterPenyeliaPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getIdPenyelia(),
			$keys[1] => $this->getUsername(),
			$keys[2] => $this->getUseremail(),
			$keys[3] => $this->getAktif(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = MasterPenyeliaPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setIdPenyelia($value);
				break;
			case 1:
				$this->setUsername($value);
				break;
			case 2:
				$this->setUseremail($value);
				break;
			case 3:
				$this->setAktif($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = MasterPenyeliaPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setIdPenyelia($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setUsername($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setUseremail($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setAktif($arr[$keys[3]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(MasterPenyeliaPeer::DATABASE_NAME);

		if ($this->isColumnModified(MasterPenyeliaPeer::ID_PENYELIA)) $criteria->add(MasterPenyeliaPeer::ID_PENYELIA, $this->id_penyelia);
		if ($this->isColumnModified(MasterPenyeliaPeer::USERNAME)) $criteria->add(MasterPenyeliaPeer::USERNAME, $this->username);
		if ($this->isColumnModified(MasterPenyeliaPeer::USEREMAIL)) $criteria->add(MasterPenyeliaPeer::USEREMAIL, $this->useremail);
		if ($this->isColumnModified(MasterPenyeliaPeer::AKTIF)) $criteria->add(MasterPenyeliaPeer::AKTIF, $this->aktif);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(MasterPenyeliaPeer::DATABASE_NAME);

		$criteria->add(MasterPenyeliaPeer::ID_PENYELIA, $this->id_penyelia);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getIdPenyelia();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setIdPenyelia($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setUsername($this->username);

		$copyObj->setUseremail($this->useremail);

		$copyObj->setAktif($this->aktif);


		$copyObj->setNew(true);

		$copyObj->setIdPenyelia(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new MasterPenyeliaPeer();
		}
		return self::$peer;
	}

} 