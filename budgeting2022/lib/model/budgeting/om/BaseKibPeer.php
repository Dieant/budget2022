<?php


abstract class BaseKibPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.kib';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.Kib';

	
	const NUM_COLUMNS = 14;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const ID = 'ebudget.kib.ID';

	
	const TIPE_KIB = 'ebudget.kib.TIPE_KIB';

	
	const UNIT_ID = 'ebudget.kib.UNIT_ID';

	
	const KODE_LOKASI = 'ebudget.kib.KODE_LOKASI';

	
	const NAMA_LOKASI = 'ebudget.kib.NAMA_LOKASI';

	
	const NO_REGISTER = 'ebudget.kib.NO_REGISTER';

	
	const KODE_BARANG = 'ebudget.kib.KODE_BARANG';

	
	const NAMA_BARANG = 'ebudget.kib.NAMA_BARANG';

	
	const KONDISI = 'ebudget.kib.KONDISI';

	
	const MERK = 'ebudget.kib.MERK';

	
	const TIPE = 'ebudget.kib.TIPE';

	
	const ALAMAT = 'ebudget.kib.ALAMAT';

	
	const KETERANGAN = 'ebudget.kib.KETERANGAN';

	
	const TAHUN = 'ebudget.kib.TAHUN';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('Id', 'TipeKib', 'UnitId', 'KodeLokasi', 'NamaLokasi', 'NoRegister', 'KodeBarang', 'NamaBarang', 'Kondisi', 'Merk', 'Tipe', 'Alamat', 'Keterangan', 'Tahun', ),
		BasePeer::TYPE_COLNAME => array (KibPeer::ID, KibPeer::TIPE_KIB, KibPeer::UNIT_ID, KibPeer::KODE_LOKASI, KibPeer::NAMA_LOKASI, KibPeer::NO_REGISTER, KibPeer::KODE_BARANG, KibPeer::NAMA_BARANG, KibPeer::KONDISI, KibPeer::MERK, KibPeer::TIPE, KibPeer::ALAMAT, KibPeer::KETERANGAN, KibPeer::TAHUN, ),
		BasePeer::TYPE_FIELDNAME => array ('id', 'tipe_kib', 'unit_id', 'kode_lokasi', 'nama_lokasi', 'no_register', 'kode_barang', 'nama_barang', 'kondisi', 'merk', 'tipe', 'alamat', 'keterangan', 'tahun', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('Id' => 0, 'TipeKib' => 1, 'UnitId' => 2, 'KodeLokasi' => 3, 'NamaLokasi' => 4, 'NoRegister' => 5, 'KodeBarang' => 6, 'NamaBarang' => 7, 'Kondisi' => 8, 'Merk' => 9, 'Tipe' => 10, 'Alamat' => 11, 'Keterangan' => 12, 'Tahun' => 13, ),
		BasePeer::TYPE_COLNAME => array (KibPeer::ID => 0, KibPeer::TIPE_KIB => 1, KibPeer::UNIT_ID => 2, KibPeer::KODE_LOKASI => 3, KibPeer::NAMA_LOKASI => 4, KibPeer::NO_REGISTER => 5, KibPeer::KODE_BARANG => 6, KibPeer::NAMA_BARANG => 7, KibPeer::KONDISI => 8, KibPeer::MERK => 9, KibPeer::TIPE => 10, KibPeer::ALAMAT => 11, KibPeer::KETERANGAN => 12, KibPeer::TAHUN => 13, ),
		BasePeer::TYPE_FIELDNAME => array ('id' => 0, 'tipe_kib' => 1, 'unit_id' => 2, 'kode_lokasi' => 3, 'nama_lokasi' => 4, 'no_register' => 5, 'kode_barang' => 6, 'nama_barang' => 7, 'kondisi' => 8, 'merk' => 9, 'tipe' => 10, 'alamat' => 11, 'keterangan' => 12, 'tahun' => 13, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/KibMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.KibMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = KibPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(KibPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(KibPeer::ID);

		$criteria->addSelectColumn(KibPeer::TIPE_KIB);

		$criteria->addSelectColumn(KibPeer::UNIT_ID);

		$criteria->addSelectColumn(KibPeer::KODE_LOKASI);

		$criteria->addSelectColumn(KibPeer::NAMA_LOKASI);

		$criteria->addSelectColumn(KibPeer::NO_REGISTER);

		$criteria->addSelectColumn(KibPeer::KODE_BARANG);

		$criteria->addSelectColumn(KibPeer::NAMA_BARANG);

		$criteria->addSelectColumn(KibPeer::KONDISI);

		$criteria->addSelectColumn(KibPeer::MERK);

		$criteria->addSelectColumn(KibPeer::TIPE);

		$criteria->addSelectColumn(KibPeer::ALAMAT);

		$criteria->addSelectColumn(KibPeer::KETERANGAN);

		$criteria->addSelectColumn(KibPeer::TAHUN);

	}

	const COUNT = 'COUNT(ebudget.kib.ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.kib.ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(KibPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(KibPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = KibPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = KibPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return KibPeer::populateObjects(KibPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			KibPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = KibPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return KibPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(KibPeer::ID);
			$selectCriteria->add(KibPeer::ID, $criteria->remove(KibPeer::ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(KibPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(KibPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof Kib) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(KibPeer::ID, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(Kib $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(KibPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(KibPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(KibPeer::DATABASE_NAME, KibPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = KibPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(KibPeer::DATABASE_NAME);

		$criteria->add(KibPeer::ID, $pk);


		$v = KibPeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(KibPeer::ID, $pks, Criteria::IN);
			$objs = KibPeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BaseKibPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/KibMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.KibMapBuilder');
}
