<?php


abstract class BaseUsulanDihapus extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $dari;


	
	protected $pekerjaan;


	
	protected $kecamatan;


	
	protected $kelurahan;


	
	protected $nama;


	
	protected $tipe;


	
	protected $lokasi;


	
	protected $volume;


	
	protected $keterangan;


	
	protected $dana;


	
	protected $skpd;


	
	protected $tahap;


	
	protected $alasan;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getDari()
	{

		return $this->dari;
	}

	
	public function getPekerjaan()
	{

		return $this->pekerjaan;
	}

	
	public function getKecamatan()
	{

		return $this->kecamatan;
	}

	
	public function getKelurahan()
	{

		return $this->kelurahan;
	}

	
	public function getNama()
	{

		return $this->nama;
	}

	
	public function getTipe()
	{

		return $this->tipe;
	}

	
	public function getLokasi()
	{

		return $this->lokasi;
	}

	
	public function getVolume()
	{

		return $this->volume;
	}

	
	public function getKeterangan()
	{

		return $this->keterangan;
	}

	
	public function getDana()
	{

		return $this->dana;
	}

	
	public function getSkpd()
	{

		return $this->skpd;
	}

	
	public function getTahap()
	{

		return $this->tahap;
	}

	
	public function getAlasan()
	{

		return $this->alasan;
	}

	
	public function setDari($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->dari !== $v) {
			$this->dari = $v;
			$this->modifiedColumns[] = UsulanDihapusPeer::DARI;
		}

	} 
	
	public function setPekerjaan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->pekerjaan !== $v) {
			$this->pekerjaan = $v;
			$this->modifiedColumns[] = UsulanDihapusPeer::PEKERJAAN;
		}

	} 
	
	public function setKecamatan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kecamatan !== $v) {
			$this->kecamatan = $v;
			$this->modifiedColumns[] = UsulanDihapusPeer::KECAMATAN;
		}

	} 
	
	public function setKelurahan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kelurahan !== $v) {
			$this->kelurahan = $v;
			$this->modifiedColumns[] = UsulanDihapusPeer::KELURAHAN;
		}

	} 
	
	public function setNama($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nama !== $v) {
			$this->nama = $v;
			$this->modifiedColumns[] = UsulanDihapusPeer::NAMA;
		}

	} 
	
	public function setTipe($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->tipe !== $v) {
			$this->tipe = $v;
			$this->modifiedColumns[] = UsulanDihapusPeer::TIPE;
		}

	} 
	
	public function setLokasi($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->lokasi !== $v) {
			$this->lokasi = $v;
			$this->modifiedColumns[] = UsulanDihapusPeer::LOKASI;
		}

	} 
	
	public function setVolume($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->volume !== $v) {
			$this->volume = $v;
			$this->modifiedColumns[] = UsulanDihapusPeer::VOLUME;
		}

	} 
	
	public function setKeterangan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->keterangan !== $v) {
			$this->keterangan = $v;
			$this->modifiedColumns[] = UsulanDihapusPeer::KETERANGAN;
		}

	} 
	
	public function setDana($v)
	{

		if ($this->dana !== $v) {
			$this->dana = $v;
			$this->modifiedColumns[] = UsulanDihapusPeer::DANA;
		}

	} 
	
	public function setSkpd($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->skpd !== $v) {
			$this->skpd = $v;
			$this->modifiedColumns[] = UsulanDihapusPeer::SKPD;
		}

	} 
	
	public function setTahap($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->tahap !== $v) {
			$this->tahap = $v;
			$this->modifiedColumns[] = UsulanDihapusPeer::TAHAP;
		}

	} 
	
	public function setAlasan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->alasan !== $v) {
			$this->alasan = $v;
			$this->modifiedColumns[] = UsulanDihapusPeer::ALASAN;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->dari = $rs->getString($startcol + 0);

			$this->pekerjaan = $rs->getString($startcol + 1);

			$this->kecamatan = $rs->getString($startcol + 2);

			$this->kelurahan = $rs->getString($startcol + 3);

			$this->nama = $rs->getString($startcol + 4);

			$this->tipe = $rs->getString($startcol + 5);

			$this->lokasi = $rs->getString($startcol + 6);

			$this->volume = $rs->getString($startcol + 7);

			$this->keterangan = $rs->getString($startcol + 8);

			$this->dana = $rs->getFloat($startcol + 9);

			$this->skpd = $rs->getString($startcol + 10);

			$this->tahap = $rs->getInt($startcol + 11);

			$this->alasan = $rs->getString($startcol + 12);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 13; 
		} catch (Exception $e) {
			throw new PropelException("Error populating UsulanDihapus object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(UsulanDihapusPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			UsulanDihapusPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(UsulanDihapusPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = UsulanDihapusPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += UsulanDihapusPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = UsulanDihapusPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = UsulanDihapusPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getDari();
				break;
			case 1:
				return $this->getPekerjaan();
				break;
			case 2:
				return $this->getKecamatan();
				break;
			case 3:
				return $this->getKelurahan();
				break;
			case 4:
				return $this->getNama();
				break;
			case 5:
				return $this->getTipe();
				break;
			case 6:
				return $this->getLokasi();
				break;
			case 7:
				return $this->getVolume();
				break;
			case 8:
				return $this->getKeterangan();
				break;
			case 9:
				return $this->getDana();
				break;
			case 10:
				return $this->getSkpd();
				break;
			case 11:
				return $this->getTahap();
				break;
			case 12:
				return $this->getAlasan();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = UsulanDihapusPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getDari(),
			$keys[1] => $this->getPekerjaan(),
			$keys[2] => $this->getKecamatan(),
			$keys[3] => $this->getKelurahan(),
			$keys[4] => $this->getNama(),
			$keys[5] => $this->getTipe(),
			$keys[6] => $this->getLokasi(),
			$keys[7] => $this->getVolume(),
			$keys[8] => $this->getKeterangan(),
			$keys[9] => $this->getDana(),
			$keys[10] => $this->getSkpd(),
			$keys[11] => $this->getTahap(),
			$keys[12] => $this->getAlasan(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = UsulanDihapusPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setDari($value);
				break;
			case 1:
				$this->setPekerjaan($value);
				break;
			case 2:
				$this->setKecamatan($value);
				break;
			case 3:
				$this->setKelurahan($value);
				break;
			case 4:
				$this->setNama($value);
				break;
			case 5:
				$this->setTipe($value);
				break;
			case 6:
				$this->setLokasi($value);
				break;
			case 7:
				$this->setVolume($value);
				break;
			case 8:
				$this->setKeterangan($value);
				break;
			case 9:
				$this->setDana($value);
				break;
			case 10:
				$this->setSkpd($value);
				break;
			case 11:
				$this->setTahap($value);
				break;
			case 12:
				$this->setAlasan($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = UsulanDihapusPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setDari($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setPekerjaan($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setKecamatan($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setKelurahan($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setNama($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setTipe($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setLokasi($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setVolume($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setKeterangan($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setDana($arr[$keys[9]]);
		if (array_key_exists($keys[10], $arr)) $this->setSkpd($arr[$keys[10]]);
		if (array_key_exists($keys[11], $arr)) $this->setTahap($arr[$keys[11]]);
		if (array_key_exists($keys[12], $arr)) $this->setAlasan($arr[$keys[12]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(UsulanDihapusPeer::DATABASE_NAME);

		if ($this->isColumnModified(UsulanDihapusPeer::DARI)) $criteria->add(UsulanDihapusPeer::DARI, $this->dari);
		if ($this->isColumnModified(UsulanDihapusPeer::PEKERJAAN)) $criteria->add(UsulanDihapusPeer::PEKERJAAN, $this->pekerjaan);
		if ($this->isColumnModified(UsulanDihapusPeer::KECAMATAN)) $criteria->add(UsulanDihapusPeer::KECAMATAN, $this->kecamatan);
		if ($this->isColumnModified(UsulanDihapusPeer::KELURAHAN)) $criteria->add(UsulanDihapusPeer::KELURAHAN, $this->kelurahan);
		if ($this->isColumnModified(UsulanDihapusPeer::NAMA)) $criteria->add(UsulanDihapusPeer::NAMA, $this->nama);
		if ($this->isColumnModified(UsulanDihapusPeer::TIPE)) $criteria->add(UsulanDihapusPeer::TIPE, $this->tipe);
		if ($this->isColumnModified(UsulanDihapusPeer::LOKASI)) $criteria->add(UsulanDihapusPeer::LOKASI, $this->lokasi);
		if ($this->isColumnModified(UsulanDihapusPeer::VOLUME)) $criteria->add(UsulanDihapusPeer::VOLUME, $this->volume);
		if ($this->isColumnModified(UsulanDihapusPeer::KETERANGAN)) $criteria->add(UsulanDihapusPeer::KETERANGAN, $this->keterangan);
		if ($this->isColumnModified(UsulanDihapusPeer::DANA)) $criteria->add(UsulanDihapusPeer::DANA, $this->dana);
		if ($this->isColumnModified(UsulanDihapusPeer::SKPD)) $criteria->add(UsulanDihapusPeer::SKPD, $this->skpd);
		if ($this->isColumnModified(UsulanDihapusPeer::TAHAP)) $criteria->add(UsulanDihapusPeer::TAHAP, $this->tahap);
		if ($this->isColumnModified(UsulanDihapusPeer::ALASAN)) $criteria->add(UsulanDihapusPeer::ALASAN, $this->alasan);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(UsulanDihapusPeer::DATABASE_NAME);


		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return null;
	}

	
	 public function setPrimaryKey($pk)
	 {
		 	 }

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setDari($this->dari);

		$copyObj->setPekerjaan($this->pekerjaan);

		$copyObj->setKecamatan($this->kecamatan);

		$copyObj->setKelurahan($this->kelurahan);

		$copyObj->setNama($this->nama);

		$copyObj->setTipe($this->tipe);

		$copyObj->setLokasi($this->lokasi);

		$copyObj->setVolume($this->volume);

		$copyObj->setKeterangan($this->keterangan);

		$copyObj->setDana($this->dana);

		$copyObj->setSkpd($this->skpd);

		$copyObj->setTahap($this->tahap);

		$copyObj->setAlasan($this->alasan);


		$copyObj->setNew(true);

	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new UsulanDihapusPeer();
		}
		return self::$peer;
	}

} 