<?php


abstract class BaseMasterProgram2Peer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.master_program2';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.MasterProgram2';

	
	const NUM_COLUMNS = 5;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const KODE_PROGRAM = 'ebudget.master_program2.KODE_PROGRAM';

	
	const KODE_PROGRAM2 = 'ebudget.master_program2.KODE_PROGRAM2';

	
	const NAMA_PROGRAM2 = 'ebudget.master_program2.NAMA_PROGRAM2';

	
	const RANKING = 'ebudget.master_program2.RANKING';

	
	const KODE_PROGRAM3 = 'ebudget.master_program2.KODE_PROGRAM3';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('KodeProgram', 'KodeProgram2', 'NamaProgram2', 'Ranking', 'KodeProgram3', ),
		BasePeer::TYPE_COLNAME => array (MasterProgram2Peer::KODE_PROGRAM, MasterProgram2Peer::KODE_PROGRAM2, MasterProgram2Peer::NAMA_PROGRAM2, MasterProgram2Peer::RANKING, MasterProgram2Peer::KODE_PROGRAM3, ),
		BasePeer::TYPE_FIELDNAME => array ('kode_program', 'kode_program2', 'nama_program2', 'ranking', 'kode_program3', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('KodeProgram' => 0, 'KodeProgram2' => 1, 'NamaProgram2' => 2, 'Ranking' => 3, 'KodeProgram3' => 4, ),
		BasePeer::TYPE_COLNAME => array (MasterProgram2Peer::KODE_PROGRAM => 0, MasterProgram2Peer::KODE_PROGRAM2 => 1, MasterProgram2Peer::NAMA_PROGRAM2 => 2, MasterProgram2Peer::RANKING => 3, MasterProgram2Peer::KODE_PROGRAM3 => 4, ),
		BasePeer::TYPE_FIELDNAME => array ('kode_program' => 0, 'kode_program2' => 1, 'nama_program2' => 2, 'ranking' => 3, 'kode_program3' => 4, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/MasterProgram2MapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.MasterProgram2MapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = MasterProgram2Peer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(MasterProgram2Peer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(MasterProgram2Peer::KODE_PROGRAM);

		$criteria->addSelectColumn(MasterProgram2Peer::KODE_PROGRAM2);

		$criteria->addSelectColumn(MasterProgram2Peer::NAMA_PROGRAM2);

		$criteria->addSelectColumn(MasterProgram2Peer::RANKING);

		$criteria->addSelectColumn(MasterProgram2Peer::KODE_PROGRAM3);

	}

	const COUNT = 'COUNT(ebudget.master_program2.KODE_PROGRAM)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.master_program2.KODE_PROGRAM)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(MasterProgram2Peer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(MasterProgram2Peer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = MasterProgram2Peer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = MasterProgram2Peer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return MasterProgram2Peer::populateObjects(MasterProgram2Peer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			MasterProgram2Peer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = MasterProgram2Peer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return MasterProgram2Peer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(MasterProgram2Peer::KODE_PROGRAM);
			$selectCriteria->add(MasterProgram2Peer::KODE_PROGRAM, $criteria->remove(MasterProgram2Peer::KODE_PROGRAM), $comparison);

			$comparison = $criteria->getComparison(MasterProgram2Peer::KODE_PROGRAM2);
			$selectCriteria->add(MasterProgram2Peer::KODE_PROGRAM2, $criteria->remove(MasterProgram2Peer::KODE_PROGRAM2), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(MasterProgram2Peer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(MasterProgram2Peer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof MasterProgram2) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
												if(count($values) == count($values, COUNT_RECURSIVE))
			{
								$values = array($values);
			}
			$vals = array();
			foreach($values as $value)
			{

				$vals[0][] = $value[0];
				$vals[1][] = $value[1];
			}

			$criteria->add(MasterProgram2Peer::KODE_PROGRAM, $vals[0], Criteria::IN);
			$criteria->add(MasterProgram2Peer::KODE_PROGRAM2, $vals[1], Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(MasterProgram2 $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(MasterProgram2Peer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(MasterProgram2Peer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(MasterProgram2Peer::DATABASE_NAME, MasterProgram2Peer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = MasterProgram2Peer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK( $kode_program, $kode_program2, $con = null) {
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$criteria = new Criteria();
		$criteria->add(MasterProgram2Peer::KODE_PROGRAM, $kode_program);
		$criteria->add(MasterProgram2Peer::KODE_PROGRAM2, $kode_program2);
		$v = MasterProgram2Peer::doSelect($criteria, $con);

		return !empty($v) ? $v[0] : null;
	}
} 
if (Propel::isInit()) {
			try {
		BaseMasterProgram2Peer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/MasterProgram2MapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.MasterProgram2MapBuilder');
}
