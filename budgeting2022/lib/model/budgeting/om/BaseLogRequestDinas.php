<?php


abstract class BaseLogRequestDinas extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id;


	
	protected $unit_id;


	
	protected $kegiatan_code;


	
	protected $tipe;


	
	protected $path;


	
	protected $created_at;


	
	protected $updated_at;


	
	protected $catatan;


	
	protected $is_khusus;


	
	protected $status;


	
	protected $catatan_tolak;


	
	protected $user_id;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getId()
	{

		return $this->id;
	}

	
	public function getUnitId()
	{

		return $this->unit_id;
	}

	
	public function getKegiatanCode()
	{

		return $this->kegiatan_code;
	}

	
	public function getTipe()
	{

		return $this->tipe;
	}

	
	public function getPath()
	{

		return $this->path;
	}

	
	public function getCreatedAt($format = 'Y-m-d H:i:s')
	{

		if ($this->created_at === null || $this->created_at === '') {
			return null;
		} elseif (!is_int($this->created_at)) {
						$ts = strtotime($this->created_at);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [created_at] as date/time value: " . var_export($this->created_at, true));
			}
		} else {
			$ts = $this->created_at;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getUpdatedAt($format = 'Y-m-d H:i:s')
	{

		if ($this->updated_at === null || $this->updated_at === '') {
			return null;
		} elseif (!is_int($this->updated_at)) {
						$ts = strtotime($this->updated_at);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [updated_at] as date/time value: " . var_export($this->updated_at, true));
			}
		} else {
			$ts = $this->updated_at;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getCatatan()
	{

		return $this->catatan;
	}

	
	public function getIsKhusus()
	{

		return $this->is_khusus;
	}

	
	public function getStatus()
	{

		return $this->status;
	}

	
	public function getCatatanTolak()
	{

		return $this->catatan_tolak;
	}

	
	public function getUserId()
	{

		return $this->user_id;
	}

	
	public function setId($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = LogRequestDinasPeer::ID;
		}

	} 
	
	public function setUnitId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->unit_id !== $v) {
			$this->unit_id = $v;
			$this->modifiedColumns[] = LogRequestDinasPeer::UNIT_ID;
		}

	} 
	
	public function setKegiatanCode($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kegiatan_code !== $v) {
			$this->kegiatan_code = $v;
			$this->modifiedColumns[] = LogRequestDinasPeer::KEGIATAN_CODE;
		}

	} 
	
	public function setTipe($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->tipe !== $v) {
			$this->tipe = $v;
			$this->modifiedColumns[] = LogRequestDinasPeer::TIPE;
		}

	} 
	
	public function setPath($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->path !== $v) {
			$this->path = $v;
			$this->modifiedColumns[] = LogRequestDinasPeer::PATH;
		}

	} 
	
	public function setCreatedAt($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [created_at] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->created_at !== $ts) {
			$this->created_at = $ts;
			$this->modifiedColumns[] = LogRequestDinasPeer::CREATED_AT;
		}

	} 
	
	public function setUpdatedAt($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [updated_at] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->updated_at !== $ts) {
			$this->updated_at = $ts;
			$this->modifiedColumns[] = LogRequestDinasPeer::UPDATED_AT;
		}

	} 
	
	public function setCatatan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->catatan !== $v) {
			$this->catatan = $v;
			$this->modifiedColumns[] = LogRequestDinasPeer::CATATAN;
		}

	} 
	
	public function setIsKhusus($v)
	{

		if ($this->is_khusus !== $v) {
			$this->is_khusus = $v;
			$this->modifiedColumns[] = LogRequestDinasPeer::IS_KHUSUS;
		}

	} 
	
	public function setStatus($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->status !== $v) {
			$this->status = $v;
			$this->modifiedColumns[] = LogRequestDinasPeer::STATUS;
		}

	} 
	
	public function setCatatanTolak($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->catatan_tolak !== $v) {
			$this->catatan_tolak = $v;
			$this->modifiedColumns[] = LogRequestDinasPeer::CATATAN_TOLAK;
		}

	} 
	
	public function setUserId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->user_id !== $v) {
			$this->user_id = $v;
			$this->modifiedColumns[] = LogRequestDinasPeer::USER_ID;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getInt($startcol + 0);

			$this->unit_id = $rs->getString($startcol + 1);

			$this->kegiatan_code = $rs->getString($startcol + 2);

			$this->tipe = $rs->getInt($startcol + 3);

			$this->path = $rs->getString($startcol + 4);

			$this->created_at = $rs->getTimestamp($startcol + 5, null);

			$this->updated_at = $rs->getTimestamp($startcol + 6, null);

			$this->catatan = $rs->getString($startcol + 7);

			$this->is_khusus = $rs->getBoolean($startcol + 8);

			$this->status = $rs->getInt($startcol + 9);

			$this->catatan_tolak = $rs->getString($startcol + 10);

			$this->user_id = $rs->getString($startcol + 11);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 12; 
		} catch (Exception $e) {
			throw new PropelException("Error populating LogRequestDinas object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(LogRequestDinasPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			LogRequestDinasPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
    if ($this->isNew() && !$this->isColumnModified(LogRequestDinasPeer::CREATED_AT))
    {
      $this->setCreatedAt(time());
    }

    if ($this->isModified() && !$this->isColumnModified(LogRequestDinasPeer::UPDATED_AT))
    {
      $this->setUpdatedAt(time());
    }

		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(LogRequestDinasPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = LogRequestDinasPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += LogRequestDinasPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = LogRequestDinasPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = LogRequestDinasPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getUnitId();
				break;
			case 2:
				return $this->getKegiatanCode();
				break;
			case 3:
				return $this->getTipe();
				break;
			case 4:
				return $this->getPath();
				break;
			case 5:
				return $this->getCreatedAt();
				break;
			case 6:
				return $this->getUpdatedAt();
				break;
			case 7:
				return $this->getCatatan();
				break;
			case 8:
				return $this->getIsKhusus();
				break;
			case 9:
				return $this->getStatus();
				break;
			case 10:
				return $this->getCatatanTolak();
				break;
			case 11:
				return $this->getUserId();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = LogRequestDinasPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getUnitId(),
			$keys[2] => $this->getKegiatanCode(),
			$keys[3] => $this->getTipe(),
			$keys[4] => $this->getPath(),
			$keys[5] => $this->getCreatedAt(),
			$keys[6] => $this->getUpdatedAt(),
			$keys[7] => $this->getCatatan(),
			$keys[8] => $this->getIsKhusus(),
			$keys[9] => $this->getStatus(),
			$keys[10] => $this->getCatatanTolak(),
			$keys[11] => $this->getUserId(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = LogRequestDinasPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setUnitId($value);
				break;
			case 2:
				$this->setKegiatanCode($value);
				break;
			case 3:
				$this->setTipe($value);
				break;
			case 4:
				$this->setPath($value);
				break;
			case 5:
				$this->setCreatedAt($value);
				break;
			case 6:
				$this->setUpdatedAt($value);
				break;
			case 7:
				$this->setCatatan($value);
				break;
			case 8:
				$this->setIsKhusus($value);
				break;
			case 9:
				$this->setStatus($value);
				break;
			case 10:
				$this->setCatatanTolak($value);
				break;
			case 11:
				$this->setUserId($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = LogRequestDinasPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setUnitId($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setKegiatanCode($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setTipe($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setPath($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setCreatedAt($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setUpdatedAt($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setCatatan($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setIsKhusus($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setStatus($arr[$keys[9]]);
		if (array_key_exists($keys[10], $arr)) $this->setCatatanTolak($arr[$keys[10]]);
		if (array_key_exists($keys[11], $arr)) $this->setUserId($arr[$keys[11]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(LogRequestDinasPeer::DATABASE_NAME);

		if ($this->isColumnModified(LogRequestDinasPeer::ID)) $criteria->add(LogRequestDinasPeer::ID, $this->id);
		if ($this->isColumnModified(LogRequestDinasPeer::UNIT_ID)) $criteria->add(LogRequestDinasPeer::UNIT_ID, $this->unit_id);
		if ($this->isColumnModified(LogRequestDinasPeer::KEGIATAN_CODE)) $criteria->add(LogRequestDinasPeer::KEGIATAN_CODE, $this->kegiatan_code);
		if ($this->isColumnModified(LogRequestDinasPeer::TIPE)) $criteria->add(LogRequestDinasPeer::TIPE, $this->tipe);
		if ($this->isColumnModified(LogRequestDinasPeer::PATH)) $criteria->add(LogRequestDinasPeer::PATH, $this->path);
		if ($this->isColumnModified(LogRequestDinasPeer::CREATED_AT)) $criteria->add(LogRequestDinasPeer::CREATED_AT, $this->created_at);
		if ($this->isColumnModified(LogRequestDinasPeer::UPDATED_AT)) $criteria->add(LogRequestDinasPeer::UPDATED_AT, $this->updated_at);
		if ($this->isColumnModified(LogRequestDinasPeer::CATATAN)) $criteria->add(LogRequestDinasPeer::CATATAN, $this->catatan);
		if ($this->isColumnModified(LogRequestDinasPeer::IS_KHUSUS)) $criteria->add(LogRequestDinasPeer::IS_KHUSUS, $this->is_khusus);
		if ($this->isColumnModified(LogRequestDinasPeer::STATUS)) $criteria->add(LogRequestDinasPeer::STATUS, $this->status);
		if ($this->isColumnModified(LogRequestDinasPeer::CATATAN_TOLAK)) $criteria->add(LogRequestDinasPeer::CATATAN_TOLAK, $this->catatan_tolak);
		if ($this->isColumnModified(LogRequestDinasPeer::USER_ID)) $criteria->add(LogRequestDinasPeer::USER_ID, $this->user_id);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(LogRequestDinasPeer::DATABASE_NAME);

		$criteria->add(LogRequestDinasPeer::ID, $this->id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setUnitId($this->unit_id);

		$copyObj->setKegiatanCode($this->kegiatan_code);

		$copyObj->setTipe($this->tipe);

		$copyObj->setPath($this->path);

		$copyObj->setCreatedAt($this->created_at);

		$copyObj->setUpdatedAt($this->updated_at);

		$copyObj->setCatatan($this->catatan);

		$copyObj->setIsKhusus($this->is_khusus);

		$copyObj->setStatus($this->status);

		$copyObj->setCatatanTolak($this->catatan_tolak);

		$copyObj->setUserId($this->user_id);


		$copyObj->setNew(true);

		$copyObj->setId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new LogRequestDinasPeer();
		}
		return self::$peer;
	}

} 