<?php


abstract class BaseKomponen2Peer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.komponen2';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.Komponen2';

	
	const NUM_COLUMNS = 19;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const KOMPONEN_ID = 'ebudget.komponen2.KOMPONEN_ID';

	
	const SATUAN = 'ebudget.komponen2.SATUAN';

	
	const KOMPONEN_NAME = 'ebudget.komponen2.KOMPONEN_NAME';

	
	const SHSD_ID = 'ebudget.komponen2.SHSD_ID';

	
	const KOMPONEN_HARGA = 'ebudget.komponen2.KOMPONEN_HARGA';

	
	const KOMPONEN_SHOW = 'ebudget.komponen2.KOMPONEN_SHOW';

	
	const IP_ADDRESS = 'ebudget.komponen2.IP_ADDRESS';

	
	const WAKTU_ACCESS = 'ebudget.komponen2.WAKTU_ACCESS';

	
	const KOMPONEN_TIPE = 'ebudget.komponen2.KOMPONEN_TIPE';

	
	const KOMPONEN_CONFIRMED = 'ebudget.komponen2.KOMPONEN_CONFIRMED';

	
	const KOMPONEN_NON_PAJAK = 'ebudget.komponen2.KOMPONEN_NON_PAJAK';

	
	const USER_ID = 'ebudget.komponen2.USER_ID';

	
	const REKENING = 'ebudget.komponen2.REKENING';

	
	const KELOMPOK = 'ebudget.komponen2.KELOMPOK';

	
	const PEMELIHARAAN = 'ebudget.komponen2.PEMELIHARAAN';

	
	const REK_UPAH = 'ebudget.komponen2.REK_UPAH';

	
	const REK_BAHAN = 'ebudget.komponen2.REK_BAHAN';

	
	const REK_SEWA = 'ebudget.komponen2.REK_SEWA';

	
	const DESKRIPSI = 'ebudget.komponen2.DESKRIPSI';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('KomponenId', 'Satuan', 'KomponenName', 'ShsdId', 'KomponenHarga', 'KomponenShow', 'IpAddress', 'WaktuAccess', 'KomponenTipe', 'KomponenConfirmed', 'KomponenNonPajak', 'UserId', 'Rekening', 'Kelompok', 'Pemeliharaan', 'RekUpah', 'RekBahan', 'RekSewa', 'Deskripsi', ),
		BasePeer::TYPE_COLNAME => array (Komponen2Peer::KOMPONEN_ID, Komponen2Peer::SATUAN, Komponen2Peer::KOMPONEN_NAME, Komponen2Peer::SHSD_ID, Komponen2Peer::KOMPONEN_HARGA, Komponen2Peer::KOMPONEN_SHOW, Komponen2Peer::IP_ADDRESS, Komponen2Peer::WAKTU_ACCESS, Komponen2Peer::KOMPONEN_TIPE, Komponen2Peer::KOMPONEN_CONFIRMED, Komponen2Peer::KOMPONEN_NON_PAJAK, Komponen2Peer::USER_ID, Komponen2Peer::REKENING, Komponen2Peer::KELOMPOK, Komponen2Peer::PEMELIHARAAN, Komponen2Peer::REK_UPAH, Komponen2Peer::REK_BAHAN, Komponen2Peer::REK_SEWA, Komponen2Peer::DESKRIPSI, ),
		BasePeer::TYPE_FIELDNAME => array ('komponen_id', 'satuan', 'komponen_name', 'shsd_id', 'komponen_harga', 'komponen_show', 'ip_address', 'waktu_access', 'komponen_tipe', 'komponen_confirmed', 'komponen_non_pajak', 'user_id', 'rekening', 'kelompok', 'pemeliharaan', 'rek_upah', 'rek_bahan', 'rek_sewa', 'deskripsi', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('KomponenId' => 0, 'Satuan' => 1, 'KomponenName' => 2, 'ShsdId' => 3, 'KomponenHarga' => 4, 'KomponenShow' => 5, 'IpAddress' => 6, 'WaktuAccess' => 7, 'KomponenTipe' => 8, 'KomponenConfirmed' => 9, 'KomponenNonPajak' => 10, 'UserId' => 11, 'Rekening' => 12, 'Kelompok' => 13, 'Pemeliharaan' => 14, 'RekUpah' => 15, 'RekBahan' => 16, 'RekSewa' => 17, 'Deskripsi' => 18, ),
		BasePeer::TYPE_COLNAME => array (Komponen2Peer::KOMPONEN_ID => 0, Komponen2Peer::SATUAN => 1, Komponen2Peer::KOMPONEN_NAME => 2, Komponen2Peer::SHSD_ID => 3, Komponen2Peer::KOMPONEN_HARGA => 4, Komponen2Peer::KOMPONEN_SHOW => 5, Komponen2Peer::IP_ADDRESS => 6, Komponen2Peer::WAKTU_ACCESS => 7, Komponen2Peer::KOMPONEN_TIPE => 8, Komponen2Peer::KOMPONEN_CONFIRMED => 9, Komponen2Peer::KOMPONEN_NON_PAJAK => 10, Komponen2Peer::USER_ID => 11, Komponen2Peer::REKENING => 12, Komponen2Peer::KELOMPOK => 13, Komponen2Peer::PEMELIHARAAN => 14, Komponen2Peer::REK_UPAH => 15, Komponen2Peer::REK_BAHAN => 16, Komponen2Peer::REK_SEWA => 17, Komponen2Peer::DESKRIPSI => 18, ),
		BasePeer::TYPE_FIELDNAME => array ('komponen_id' => 0, 'satuan' => 1, 'komponen_name' => 2, 'shsd_id' => 3, 'komponen_harga' => 4, 'komponen_show' => 5, 'ip_address' => 6, 'waktu_access' => 7, 'komponen_tipe' => 8, 'komponen_confirmed' => 9, 'komponen_non_pajak' => 10, 'user_id' => 11, 'rekening' => 12, 'kelompok' => 13, 'pemeliharaan' => 14, 'rek_upah' => 15, 'rek_bahan' => 16, 'rek_sewa' => 17, 'deskripsi' => 18, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/Komponen2MapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.Komponen2MapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = Komponen2Peer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(Komponen2Peer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(Komponen2Peer::KOMPONEN_ID);

		$criteria->addSelectColumn(Komponen2Peer::SATUAN);

		$criteria->addSelectColumn(Komponen2Peer::KOMPONEN_NAME);

		$criteria->addSelectColumn(Komponen2Peer::SHSD_ID);

		$criteria->addSelectColumn(Komponen2Peer::KOMPONEN_HARGA);

		$criteria->addSelectColumn(Komponen2Peer::KOMPONEN_SHOW);

		$criteria->addSelectColumn(Komponen2Peer::IP_ADDRESS);

		$criteria->addSelectColumn(Komponen2Peer::WAKTU_ACCESS);

		$criteria->addSelectColumn(Komponen2Peer::KOMPONEN_TIPE);

		$criteria->addSelectColumn(Komponen2Peer::KOMPONEN_CONFIRMED);

		$criteria->addSelectColumn(Komponen2Peer::KOMPONEN_NON_PAJAK);

		$criteria->addSelectColumn(Komponen2Peer::USER_ID);

		$criteria->addSelectColumn(Komponen2Peer::REKENING);

		$criteria->addSelectColumn(Komponen2Peer::KELOMPOK);

		$criteria->addSelectColumn(Komponen2Peer::PEMELIHARAAN);

		$criteria->addSelectColumn(Komponen2Peer::REK_UPAH);

		$criteria->addSelectColumn(Komponen2Peer::REK_BAHAN);

		$criteria->addSelectColumn(Komponen2Peer::REK_SEWA);

		$criteria->addSelectColumn(Komponen2Peer::DESKRIPSI);

	}

	const COUNT = 'COUNT(ebudget.komponen2.KOMPONEN_ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.komponen2.KOMPONEN_ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(Komponen2Peer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(Komponen2Peer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = Komponen2Peer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = Komponen2Peer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return Komponen2Peer::populateObjects(Komponen2Peer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			Komponen2Peer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = Komponen2Peer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return Komponen2Peer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(Komponen2Peer::KOMPONEN_ID);
			$selectCriteria->add(Komponen2Peer::KOMPONEN_ID, $criteria->remove(Komponen2Peer::KOMPONEN_ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(Komponen2Peer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(Komponen2Peer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof Komponen2) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(Komponen2Peer::KOMPONEN_ID, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(Komponen2 $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(Komponen2Peer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(Komponen2Peer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(Komponen2Peer::DATABASE_NAME, Komponen2Peer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = Komponen2Peer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(Komponen2Peer::DATABASE_NAME);

		$criteria->add(Komponen2Peer::KOMPONEN_ID, $pk);


		$v = Komponen2Peer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(Komponen2Peer::KOMPONEN_ID, $pks, Criteria::IN);
			$objs = Komponen2Peer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BaseKomponen2Peer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/Komponen2MapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.Komponen2MapBuilder');
}
