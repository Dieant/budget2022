<?php


abstract class BaseBappekoMasterKegiatanPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.bappeko_master_kegiatan';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.BappekoMasterKegiatan';

	
	const NUM_COLUMNS = 43;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const UNIT_ID = 'ebudget.bappeko_master_kegiatan.UNIT_ID';

	
	const KODE_KEGIATAN = 'ebudget.bappeko_master_kegiatan.KODE_KEGIATAN';

	
	const KODE_BIDANG = 'ebudget.bappeko_master_kegiatan.KODE_BIDANG';

	
	const KODE_URUSAN_WAJIB = 'ebudget.bappeko_master_kegiatan.KODE_URUSAN_WAJIB';

	
	const KODE_PROGRAM = 'ebudget.bappeko_master_kegiatan.KODE_PROGRAM';

	
	const KODE_SASARAN = 'ebudget.bappeko_master_kegiatan.KODE_SASARAN';

	
	const KODE_INDIKATOR = 'ebudget.bappeko_master_kegiatan.KODE_INDIKATOR';

	
	const ALOKASI_DANA = 'ebudget.bappeko_master_kegiatan.ALOKASI_DANA';

	
	const NAMA_KEGIATAN = 'ebudget.bappeko_master_kegiatan.NAMA_KEGIATAN';

	
	const MASUKAN = 'ebudget.bappeko_master_kegiatan.MASUKAN';

	
	const OUTPUT = 'ebudget.bappeko_master_kegiatan.OUTPUT';

	
	const OUTCOME = 'ebudget.bappeko_master_kegiatan.OUTCOME';

	
	const BENEFIT = 'ebudget.bappeko_master_kegiatan.BENEFIT';

	
	const IMPACT = 'ebudget.bappeko_master_kegiatan.IMPACT';

	
	const TIPE = 'ebudget.bappeko_master_kegiatan.TIPE';

	
	const KEGIATAN_ACTIVE = 'ebudget.bappeko_master_kegiatan.KEGIATAN_ACTIVE';

	
	const TO_KEGIATAN_CODE = 'ebudget.bappeko_master_kegiatan.TO_KEGIATAN_CODE';

	
	const CATATAN = 'ebudget.bappeko_master_kegiatan.CATATAN';

	
	const TARGET_OUTCOME = 'ebudget.bappeko_master_kegiatan.TARGET_OUTCOME';

	
	const LOKASI = 'ebudget.bappeko_master_kegiatan.LOKASI';

	
	const JUMLAH_PREV = 'ebudget.bappeko_master_kegiatan.JUMLAH_PREV';

	
	const JUMLAH_NOW = 'ebudget.bappeko_master_kegiatan.JUMLAH_NOW';

	
	const JUMLAH_NEXT = 'ebudget.bappeko_master_kegiatan.JUMLAH_NEXT';

	
	const KODE_PROGRAM2 = 'ebudget.bappeko_master_kegiatan.KODE_PROGRAM2';

	
	const KODE_URUSAN = 'ebudget.bappeko_master_kegiatan.KODE_URUSAN';

	
	const LAST_UPDATE_USER = 'ebudget.bappeko_master_kegiatan.LAST_UPDATE_USER';

	
	const LAST_UPDATE_TIME = 'ebudget.bappeko_master_kegiatan.LAST_UPDATE_TIME';

	
	const LAST_UPDATE_IP = 'ebudget.bappeko_master_kegiatan.LAST_UPDATE_IP';

	
	const TAHAP = 'ebudget.bappeko_master_kegiatan.TAHAP';

	
	const KODE_MISI = 'ebudget.bappeko_master_kegiatan.KODE_MISI';

	
	const KODE_TUJUAN = 'ebudget.bappeko_master_kegiatan.KODE_TUJUAN';

	
	const RANKING = 'ebudget.bappeko_master_kegiatan.RANKING';

	
	const NOMOR13 = 'ebudget.bappeko_master_kegiatan.NOMOR13';

	
	const PPA_NAMA = 'ebudget.bappeko_master_kegiatan.PPA_NAMA';

	
	const PPA_PANGKAT = 'ebudget.bappeko_master_kegiatan.PPA_PANGKAT';

	
	const PPA_NIP = 'ebudget.bappeko_master_kegiatan.PPA_NIP';

	
	const LANJUTAN = 'ebudget.bappeko_master_kegiatan.LANJUTAN';

	
	const USER_ID = 'ebudget.bappeko_master_kegiatan.USER_ID';

	
	const ID = 'ebudget.bappeko_master_kegiatan.ID';

	
	const TAHUN = 'ebudget.bappeko_master_kegiatan.TAHUN';

	
	const TAMBAHAN_PAGU = 'ebudget.bappeko_master_kegiatan.TAMBAHAN_PAGU';

	
	const GENDER = 'ebudget.bappeko_master_kegiatan.GENDER';

	
	const KODE_KEG_KEUANGAN = 'ebudget.bappeko_master_kegiatan.KODE_KEG_KEUANGAN';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('UnitId', 'KodeKegiatan', 'KodeBidang', 'KodeUrusanWajib', 'KodeProgram', 'KodeSasaran', 'KodeIndikator', 'AlokasiDana', 'NamaKegiatan', 'Masukan', 'Output', 'Outcome', 'Benefit', 'Impact', 'Tipe', 'KegiatanActive', 'ToKegiatanCode', 'Catatan', 'TargetOutcome', 'Lokasi', 'JumlahPrev', 'JumlahNow', 'JumlahNext', 'KodeProgram2', 'KodeUrusan', 'LastUpdateUser', 'LastUpdateTime', 'LastUpdateIp', 'Tahap', 'KodeMisi', 'KodeTujuan', 'Ranking', 'Nomor13', 'PpaNama', 'PpaPangkat', 'PpaNip', 'Lanjutan', 'UserId', 'Id', 'Tahun', 'TambahanPagu', 'Gender', 'KodeKegKeuangan', ),
		BasePeer::TYPE_COLNAME => array (BappekoMasterKegiatanPeer::UNIT_ID, BappekoMasterKegiatanPeer::KODE_KEGIATAN, BappekoMasterKegiatanPeer::KODE_BIDANG, BappekoMasterKegiatanPeer::KODE_URUSAN_WAJIB, BappekoMasterKegiatanPeer::KODE_PROGRAM, BappekoMasterKegiatanPeer::KODE_SASARAN, BappekoMasterKegiatanPeer::KODE_INDIKATOR, BappekoMasterKegiatanPeer::ALOKASI_DANA, BappekoMasterKegiatanPeer::NAMA_KEGIATAN, BappekoMasterKegiatanPeer::MASUKAN, BappekoMasterKegiatanPeer::OUTPUT, BappekoMasterKegiatanPeer::OUTCOME, BappekoMasterKegiatanPeer::BENEFIT, BappekoMasterKegiatanPeer::IMPACT, BappekoMasterKegiatanPeer::TIPE, BappekoMasterKegiatanPeer::KEGIATAN_ACTIVE, BappekoMasterKegiatanPeer::TO_KEGIATAN_CODE, BappekoMasterKegiatanPeer::CATATAN, BappekoMasterKegiatanPeer::TARGET_OUTCOME, BappekoMasterKegiatanPeer::LOKASI, BappekoMasterKegiatanPeer::JUMLAH_PREV, BappekoMasterKegiatanPeer::JUMLAH_NOW, BappekoMasterKegiatanPeer::JUMLAH_NEXT, BappekoMasterKegiatanPeer::KODE_PROGRAM2, BappekoMasterKegiatanPeer::KODE_URUSAN, BappekoMasterKegiatanPeer::LAST_UPDATE_USER, BappekoMasterKegiatanPeer::LAST_UPDATE_TIME, BappekoMasterKegiatanPeer::LAST_UPDATE_IP, BappekoMasterKegiatanPeer::TAHAP, BappekoMasterKegiatanPeer::KODE_MISI, BappekoMasterKegiatanPeer::KODE_TUJUAN, BappekoMasterKegiatanPeer::RANKING, BappekoMasterKegiatanPeer::NOMOR13, BappekoMasterKegiatanPeer::PPA_NAMA, BappekoMasterKegiatanPeer::PPA_PANGKAT, BappekoMasterKegiatanPeer::PPA_NIP, BappekoMasterKegiatanPeer::LANJUTAN, BappekoMasterKegiatanPeer::USER_ID, BappekoMasterKegiatanPeer::ID, BappekoMasterKegiatanPeer::TAHUN, BappekoMasterKegiatanPeer::TAMBAHAN_PAGU, BappekoMasterKegiatanPeer::GENDER, BappekoMasterKegiatanPeer::KODE_KEG_KEUANGAN, ),
		BasePeer::TYPE_FIELDNAME => array ('unit_id', 'kode_kegiatan', 'kode_bidang', 'kode_urusan_wajib', 'kode_program', 'kode_sasaran', 'kode_indikator', 'alokasi_dana', 'nama_kegiatan', 'masukan', 'output', 'outcome', 'benefit', 'impact', 'tipe', 'kegiatan_active', 'to_kegiatan_code', 'catatan', 'target_outcome', 'lokasi', 'jumlah_prev', 'jumlah_now', 'jumlah_next', 'kode_program2', 'kode_urusan', 'last_update_user', 'last_update_time', 'last_update_ip', 'tahap', 'kode_misi', 'kode_tujuan', 'ranking', 'nomor13', 'ppa_nama', 'ppa_pangkat', 'ppa_nip', 'lanjutan', 'user_id', 'id', 'tahun', 'tambahan_pagu', 'gender', 'kode_keg_keuangan', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('UnitId' => 0, 'KodeKegiatan' => 1, 'KodeBidang' => 2, 'KodeUrusanWajib' => 3, 'KodeProgram' => 4, 'KodeSasaran' => 5, 'KodeIndikator' => 6, 'AlokasiDana' => 7, 'NamaKegiatan' => 8, 'Masukan' => 9, 'Output' => 10, 'Outcome' => 11, 'Benefit' => 12, 'Impact' => 13, 'Tipe' => 14, 'KegiatanActive' => 15, 'ToKegiatanCode' => 16, 'Catatan' => 17, 'TargetOutcome' => 18, 'Lokasi' => 19, 'JumlahPrev' => 20, 'JumlahNow' => 21, 'JumlahNext' => 22, 'KodeProgram2' => 23, 'KodeUrusan' => 24, 'LastUpdateUser' => 25, 'LastUpdateTime' => 26, 'LastUpdateIp' => 27, 'Tahap' => 28, 'KodeMisi' => 29, 'KodeTujuan' => 30, 'Ranking' => 31, 'Nomor13' => 32, 'PpaNama' => 33, 'PpaPangkat' => 34, 'PpaNip' => 35, 'Lanjutan' => 36, 'UserId' => 37, 'Id' => 38, 'Tahun' => 39, 'TambahanPagu' => 40, 'Gender' => 41, 'KodeKegKeuangan' => 42, ),
		BasePeer::TYPE_COLNAME => array (BappekoMasterKegiatanPeer::UNIT_ID => 0, BappekoMasterKegiatanPeer::KODE_KEGIATAN => 1, BappekoMasterKegiatanPeer::KODE_BIDANG => 2, BappekoMasterKegiatanPeer::KODE_URUSAN_WAJIB => 3, BappekoMasterKegiatanPeer::KODE_PROGRAM => 4, BappekoMasterKegiatanPeer::KODE_SASARAN => 5, BappekoMasterKegiatanPeer::KODE_INDIKATOR => 6, BappekoMasterKegiatanPeer::ALOKASI_DANA => 7, BappekoMasterKegiatanPeer::NAMA_KEGIATAN => 8, BappekoMasterKegiatanPeer::MASUKAN => 9, BappekoMasterKegiatanPeer::OUTPUT => 10, BappekoMasterKegiatanPeer::OUTCOME => 11, BappekoMasterKegiatanPeer::BENEFIT => 12, BappekoMasterKegiatanPeer::IMPACT => 13, BappekoMasterKegiatanPeer::TIPE => 14, BappekoMasterKegiatanPeer::KEGIATAN_ACTIVE => 15, BappekoMasterKegiatanPeer::TO_KEGIATAN_CODE => 16, BappekoMasterKegiatanPeer::CATATAN => 17, BappekoMasterKegiatanPeer::TARGET_OUTCOME => 18, BappekoMasterKegiatanPeer::LOKASI => 19, BappekoMasterKegiatanPeer::JUMLAH_PREV => 20, BappekoMasterKegiatanPeer::JUMLAH_NOW => 21, BappekoMasterKegiatanPeer::JUMLAH_NEXT => 22, BappekoMasterKegiatanPeer::KODE_PROGRAM2 => 23, BappekoMasterKegiatanPeer::KODE_URUSAN => 24, BappekoMasterKegiatanPeer::LAST_UPDATE_USER => 25, BappekoMasterKegiatanPeer::LAST_UPDATE_TIME => 26, BappekoMasterKegiatanPeer::LAST_UPDATE_IP => 27, BappekoMasterKegiatanPeer::TAHAP => 28, BappekoMasterKegiatanPeer::KODE_MISI => 29, BappekoMasterKegiatanPeer::KODE_TUJUAN => 30, BappekoMasterKegiatanPeer::RANKING => 31, BappekoMasterKegiatanPeer::NOMOR13 => 32, BappekoMasterKegiatanPeer::PPA_NAMA => 33, BappekoMasterKegiatanPeer::PPA_PANGKAT => 34, BappekoMasterKegiatanPeer::PPA_NIP => 35, BappekoMasterKegiatanPeer::LANJUTAN => 36, BappekoMasterKegiatanPeer::USER_ID => 37, BappekoMasterKegiatanPeer::ID => 38, BappekoMasterKegiatanPeer::TAHUN => 39, BappekoMasterKegiatanPeer::TAMBAHAN_PAGU => 40, BappekoMasterKegiatanPeer::GENDER => 41, BappekoMasterKegiatanPeer::KODE_KEG_KEUANGAN => 42, ),
		BasePeer::TYPE_FIELDNAME => array ('unit_id' => 0, 'kode_kegiatan' => 1, 'kode_bidang' => 2, 'kode_urusan_wajib' => 3, 'kode_program' => 4, 'kode_sasaran' => 5, 'kode_indikator' => 6, 'alokasi_dana' => 7, 'nama_kegiatan' => 8, 'masukan' => 9, 'output' => 10, 'outcome' => 11, 'benefit' => 12, 'impact' => 13, 'tipe' => 14, 'kegiatan_active' => 15, 'to_kegiatan_code' => 16, 'catatan' => 17, 'target_outcome' => 18, 'lokasi' => 19, 'jumlah_prev' => 20, 'jumlah_now' => 21, 'jumlah_next' => 22, 'kode_program2' => 23, 'kode_urusan' => 24, 'last_update_user' => 25, 'last_update_time' => 26, 'last_update_ip' => 27, 'tahap' => 28, 'kode_misi' => 29, 'kode_tujuan' => 30, 'ranking' => 31, 'nomor13' => 32, 'ppa_nama' => 33, 'ppa_pangkat' => 34, 'ppa_nip' => 35, 'lanjutan' => 36, 'user_id' => 37, 'id' => 38, 'tahun' => 39, 'tambahan_pagu' => 40, 'gender' => 41, 'kode_keg_keuangan' => 42, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/BappekoMasterKegiatanMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.BappekoMasterKegiatanMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = BappekoMasterKegiatanPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(BappekoMasterKegiatanPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(BappekoMasterKegiatanPeer::UNIT_ID);

		$criteria->addSelectColumn(BappekoMasterKegiatanPeer::KODE_KEGIATAN);

		$criteria->addSelectColumn(BappekoMasterKegiatanPeer::KODE_BIDANG);

		$criteria->addSelectColumn(BappekoMasterKegiatanPeer::KODE_URUSAN_WAJIB);

		$criteria->addSelectColumn(BappekoMasterKegiatanPeer::KODE_PROGRAM);

		$criteria->addSelectColumn(BappekoMasterKegiatanPeer::KODE_SASARAN);

		$criteria->addSelectColumn(BappekoMasterKegiatanPeer::KODE_INDIKATOR);

		$criteria->addSelectColumn(BappekoMasterKegiatanPeer::ALOKASI_DANA);

		$criteria->addSelectColumn(BappekoMasterKegiatanPeer::NAMA_KEGIATAN);

		$criteria->addSelectColumn(BappekoMasterKegiatanPeer::MASUKAN);

		$criteria->addSelectColumn(BappekoMasterKegiatanPeer::OUTPUT);

		$criteria->addSelectColumn(BappekoMasterKegiatanPeer::OUTCOME);

		$criteria->addSelectColumn(BappekoMasterKegiatanPeer::BENEFIT);

		$criteria->addSelectColumn(BappekoMasterKegiatanPeer::IMPACT);

		$criteria->addSelectColumn(BappekoMasterKegiatanPeer::TIPE);

		$criteria->addSelectColumn(BappekoMasterKegiatanPeer::KEGIATAN_ACTIVE);

		$criteria->addSelectColumn(BappekoMasterKegiatanPeer::TO_KEGIATAN_CODE);

		$criteria->addSelectColumn(BappekoMasterKegiatanPeer::CATATAN);

		$criteria->addSelectColumn(BappekoMasterKegiatanPeer::TARGET_OUTCOME);

		$criteria->addSelectColumn(BappekoMasterKegiatanPeer::LOKASI);

		$criteria->addSelectColumn(BappekoMasterKegiatanPeer::JUMLAH_PREV);

		$criteria->addSelectColumn(BappekoMasterKegiatanPeer::JUMLAH_NOW);

		$criteria->addSelectColumn(BappekoMasterKegiatanPeer::JUMLAH_NEXT);

		$criteria->addSelectColumn(BappekoMasterKegiatanPeer::KODE_PROGRAM2);

		$criteria->addSelectColumn(BappekoMasterKegiatanPeer::KODE_URUSAN);

		$criteria->addSelectColumn(BappekoMasterKegiatanPeer::LAST_UPDATE_USER);

		$criteria->addSelectColumn(BappekoMasterKegiatanPeer::LAST_UPDATE_TIME);

		$criteria->addSelectColumn(BappekoMasterKegiatanPeer::LAST_UPDATE_IP);

		$criteria->addSelectColumn(BappekoMasterKegiatanPeer::TAHAP);

		$criteria->addSelectColumn(BappekoMasterKegiatanPeer::KODE_MISI);

		$criteria->addSelectColumn(BappekoMasterKegiatanPeer::KODE_TUJUAN);

		$criteria->addSelectColumn(BappekoMasterKegiatanPeer::RANKING);

		$criteria->addSelectColumn(BappekoMasterKegiatanPeer::NOMOR13);

		$criteria->addSelectColumn(BappekoMasterKegiatanPeer::PPA_NAMA);

		$criteria->addSelectColumn(BappekoMasterKegiatanPeer::PPA_PANGKAT);

		$criteria->addSelectColumn(BappekoMasterKegiatanPeer::PPA_NIP);

		$criteria->addSelectColumn(BappekoMasterKegiatanPeer::LANJUTAN);

		$criteria->addSelectColumn(BappekoMasterKegiatanPeer::USER_ID);

		$criteria->addSelectColumn(BappekoMasterKegiatanPeer::ID);

		$criteria->addSelectColumn(BappekoMasterKegiatanPeer::TAHUN);

		$criteria->addSelectColumn(BappekoMasterKegiatanPeer::TAMBAHAN_PAGU);

		$criteria->addSelectColumn(BappekoMasterKegiatanPeer::GENDER);

		$criteria->addSelectColumn(BappekoMasterKegiatanPeer::KODE_KEG_KEUANGAN);

	}

	const COUNT = 'COUNT(ebudget.bappeko_master_kegiatan.UNIT_ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.bappeko_master_kegiatan.UNIT_ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(BappekoMasterKegiatanPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(BappekoMasterKegiatanPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = BappekoMasterKegiatanPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = BappekoMasterKegiatanPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return BappekoMasterKegiatanPeer::populateObjects(BappekoMasterKegiatanPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			BappekoMasterKegiatanPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = BappekoMasterKegiatanPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return BappekoMasterKegiatanPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}

		$criteria->remove(BappekoMasterKegiatanPeer::ID); 

				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(BappekoMasterKegiatanPeer::UNIT_ID);
			$selectCriteria->add(BappekoMasterKegiatanPeer::UNIT_ID, $criteria->remove(BappekoMasterKegiatanPeer::UNIT_ID), $comparison);

			$comparison = $criteria->getComparison(BappekoMasterKegiatanPeer::KODE_KEGIATAN);
			$selectCriteria->add(BappekoMasterKegiatanPeer::KODE_KEGIATAN, $criteria->remove(BappekoMasterKegiatanPeer::KODE_KEGIATAN), $comparison);

			$comparison = $criteria->getComparison(BappekoMasterKegiatanPeer::ID);
			$selectCriteria->add(BappekoMasterKegiatanPeer::ID, $criteria->remove(BappekoMasterKegiatanPeer::ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(BappekoMasterKegiatanPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(BappekoMasterKegiatanPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof BappekoMasterKegiatan) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
												if(count($values) == count($values, COUNT_RECURSIVE))
			{
								$values = array($values);
			}
			$vals = array();
			foreach($values as $value)
			{

				$vals[0][] = $value[0];
				$vals[1][] = $value[1];
				$vals[2][] = $value[2];
			}

			$criteria->add(BappekoMasterKegiatanPeer::UNIT_ID, $vals[0], Criteria::IN);
			$criteria->add(BappekoMasterKegiatanPeer::KODE_KEGIATAN, $vals[1], Criteria::IN);
			$criteria->add(BappekoMasterKegiatanPeer::ID, $vals[2], Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(BappekoMasterKegiatan $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(BappekoMasterKegiatanPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(BappekoMasterKegiatanPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(BappekoMasterKegiatanPeer::DATABASE_NAME, BappekoMasterKegiatanPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = BappekoMasterKegiatanPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK( $unit_id, $kode_kegiatan, $id, $con = null) {
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$criteria = new Criteria();
		$criteria->add(BappekoMasterKegiatanPeer::UNIT_ID, $unit_id);
		$criteria->add(BappekoMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
		$criteria->add(BappekoMasterKegiatanPeer::ID, $id);
		$v = BappekoMasterKegiatanPeer::doSelect($criteria, $con);

		return !empty($v) ? $v[0] : null;
	}
} 
if (Propel::isInit()) {
			try {
		BaseBappekoMasterKegiatanPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/BappekoMasterKegiatanMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.BappekoMasterKegiatanMapBuilder');
}
