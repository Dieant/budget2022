<?php


abstract class BaseIndikatorProgramKota extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id;


	
	protected $unit_id;


	
	protected $kode_kegiatan;


	
	protected $indikator_program;


	
	protected $nilai;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getId()
	{

		return $this->id;
	}

	
	public function getUnitId()
	{

		return $this->unit_id;
	}

	
	public function getKodeKegiatan()
	{

		return $this->kode_kegiatan;
	}

	
	public function getIndikatorProgram()
	{

		return $this->indikator_program;
	}

	
	public function getNilai()
	{

		return $this->nilai;
	}

	
	public function setId($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = IndikatorProgramKotaPeer::ID;
		}

	} 
	
	public function setUnitId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->unit_id !== $v) {
			$this->unit_id = $v;
			$this->modifiedColumns[] = IndikatorProgramKotaPeer::UNIT_ID;
		}

	} 
	
	public function setKodeKegiatan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_kegiatan !== $v) {
			$this->kode_kegiatan = $v;
			$this->modifiedColumns[] = IndikatorProgramKotaPeer::KODE_KEGIATAN;
		}

	} 
	
	public function setIndikatorProgram($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->indikator_program !== $v) {
			$this->indikator_program = $v;
			$this->modifiedColumns[] = IndikatorProgramKotaPeer::INDIKATOR_PROGRAM;
		}

	} 
	
	public function setNilai($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nilai !== $v) {
			$this->nilai = $v;
			$this->modifiedColumns[] = IndikatorProgramKotaPeer::NILAI;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getInt($startcol + 0);

			$this->unit_id = $rs->getString($startcol + 1);

			$this->kode_kegiatan = $rs->getString($startcol + 2);

			$this->indikator_program = $rs->getString($startcol + 3);

			$this->nilai = $rs->getString($startcol + 4);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 5; 
		} catch (Exception $e) {
			throw new PropelException("Error populating IndikatorProgramKota object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(IndikatorProgramKotaPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			IndikatorProgramKotaPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(IndikatorProgramKotaPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = IndikatorProgramKotaPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += IndikatorProgramKotaPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = IndikatorProgramKotaPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = IndikatorProgramKotaPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getUnitId();
				break;
			case 2:
				return $this->getKodeKegiatan();
				break;
			case 3:
				return $this->getIndikatorProgram();
				break;
			case 4:
				return $this->getNilai();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = IndikatorProgramKotaPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getUnitId(),
			$keys[2] => $this->getKodeKegiatan(),
			$keys[3] => $this->getIndikatorProgram(),
			$keys[4] => $this->getNilai(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = IndikatorProgramKotaPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setUnitId($value);
				break;
			case 2:
				$this->setKodeKegiatan($value);
				break;
			case 3:
				$this->setIndikatorProgram($value);
				break;
			case 4:
				$this->setNilai($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = IndikatorProgramKotaPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setUnitId($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setKodeKegiatan($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setIndikatorProgram($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setNilai($arr[$keys[4]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(IndikatorProgramKotaPeer::DATABASE_NAME);

		if ($this->isColumnModified(IndikatorProgramKotaPeer::ID)) $criteria->add(IndikatorProgramKotaPeer::ID, $this->id);
		if ($this->isColumnModified(IndikatorProgramKotaPeer::UNIT_ID)) $criteria->add(IndikatorProgramKotaPeer::UNIT_ID, $this->unit_id);
		if ($this->isColumnModified(IndikatorProgramKotaPeer::KODE_KEGIATAN)) $criteria->add(IndikatorProgramKotaPeer::KODE_KEGIATAN, $this->kode_kegiatan);
		if ($this->isColumnModified(IndikatorProgramKotaPeer::INDIKATOR_PROGRAM)) $criteria->add(IndikatorProgramKotaPeer::INDIKATOR_PROGRAM, $this->indikator_program);
		if ($this->isColumnModified(IndikatorProgramKotaPeer::NILAI)) $criteria->add(IndikatorProgramKotaPeer::NILAI, $this->nilai);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(IndikatorProgramKotaPeer::DATABASE_NAME);

		$criteria->add(IndikatorProgramKotaPeer::ID, $this->id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setUnitId($this->unit_id);

		$copyObj->setKodeKegiatan($this->kode_kegiatan);

		$copyObj->setIndikatorProgram($this->indikator_program);

		$copyObj->setNilai($this->nilai);


		$copyObj->setNew(true);

		$copyObj->setId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new IndikatorProgramKotaPeer();
		}
		return self::$peer;
	}

} 