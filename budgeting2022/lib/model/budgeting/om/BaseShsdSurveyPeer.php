<?php


abstract class BaseShsdSurveyPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.shsd_survey';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.ShsdSurvey';

	
	const NUM_COLUMNS = 44;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const SHSD_ID = 'ebudget.shsd_survey.SHSD_ID';

	
	const SATUAN = 'ebudget.shsd_survey.SATUAN';

	
	const SHSD_NAME = 'ebudget.shsd_survey.SHSD_NAME';

	
	const SHSD_HARGA = 'ebudget.shsd_survey.SHSD_HARGA';

	
	const SHSD_HARGA_DASAR = 'ebudget.shsd_survey.SHSD_HARGA_DASAR';

	
	const SHSD_SHOW = 'ebudget.shsd_survey.SHSD_SHOW';

	
	const SHSD_FAKTOR_INFLASI = 'ebudget.shsd_survey.SHSD_FAKTOR_INFLASI';

	
	const SHSD_LOCKED = 'ebudget.shsd_survey.SHSD_LOCKED';

	
	const REKENING_CODE = 'ebudget.shsd_survey.REKENING_CODE';

	
	const SHSD_MERK = 'ebudget.shsd_survey.SHSD_MERK';

	
	const NON_PAJAK = 'ebudget.shsd_survey.NON_PAJAK';

	
	const SHSD_ID_OLD = 'ebudget.shsd_survey.SHSD_ID_OLD';

	
	const NAMA_DASAR = 'ebudget.shsd_survey.NAMA_DASAR';

	
	const SPEC = 'ebudget.shsd_survey.SPEC';

	
	const HIDDEN_SPEC = 'ebudget.shsd_survey.HIDDEN_SPEC';

	
	const USULAN_SKPD = 'ebudget.shsd_survey.USULAN_SKPD';

	
	const IS_POTONG_BPJS = 'ebudget.shsd_survey.IS_POTONG_BPJS';

	
	const IS_IURAN_BPJS = 'ebudget.shsd_survey.IS_IURAN_BPJS';

	
	const TAHAP = 'ebudget.shsd_survey.TAHAP';

	
	const IS_SURVEY_BP = 'ebudget.shsd_survey.IS_SURVEY_BP';

	
	const IS_INFLASI = 'ebudget.shsd_survey.IS_INFLASI';

	
	const TOKO1 = 'ebudget.shsd_survey.TOKO1';

	
	const TOKO2 = 'ebudget.shsd_survey.TOKO2';

	
	const TOKO3 = 'ebudget.shsd_survey.TOKO3';

	
	const MERK1 = 'ebudget.shsd_survey.MERK1';

	
	const MERK2 = 'ebudget.shsd_survey.MERK2';

	
	const MERK3 = 'ebudget.shsd_survey.MERK3';

	
	const HARGA1 = 'ebudget.shsd_survey.HARGA1';

	
	const HARGA2 = 'ebudget.shsd_survey.HARGA2';

	
	const HARGA3 = 'ebudget.shsd_survey.HARGA3';

	
	const TANGGAL_SURVEY = 'ebudget.shsd_survey.TANGGAL_SURVEY';

	
	const SURVEYOR = 'ebudget.shsd_survey.SURVEYOR';

	
	const FILE1 = 'ebudget.shsd_survey.FILE1';

	
	const FILE2 = 'ebudget.shsd_survey.FILE2';

	
	const FILE3 = 'ebudget.shsd_survey.FILE3';

	
	const SHSD_HARGA_LELANG = 'ebudget.shsd_survey.SHSD_HARGA_LELANG';

	
	const SHSD_HARGA_PAKAI = 'ebudget.shsd_survey.SHSD_HARGA_PAKAI';

	
	const FILE_LELANG = 'ebudget.shsd_survey.FILE_LELANG';

	
	const FILE1_2 = 'ebudget.shsd_survey.FILE1_2';

	
	const FILE2_2 = 'ebudget.shsd_survey.FILE2_2';

	
	const FILE3_2 = 'ebudget.shsd_survey.FILE3_2';

	
	const KETERANGAN1 = 'ebudget.shsd_survey.KETERANGAN1';

	
	const KETERANGAN2 = 'ebudget.shsd_survey.KETERANGAN2';

	
	const KETERANGAN3 = 'ebudget.shsd_survey.KETERANGAN3';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('ShsdId', 'Satuan', 'ShsdName', 'ShsdHarga', 'ShsdHargaDasar', 'ShsdShow', 'ShsdFaktorInflasi', 'ShsdLocked', 'RekeningCode', 'ShsdMerk', 'NonPajak', 'ShsdIdOld', 'NamaDasar', 'Spec', 'HiddenSpec', 'UsulanSkpd', 'IsPotongBpjs', 'IsIuranBpjs', 'Tahap', 'IsSurveyBp', 'IsInflasi', 'Toko1', 'Toko2', 'Toko3', 'Merk1', 'Merk2', 'Merk3', 'Harga1', 'Harga2', 'Harga3', 'TanggalSurvey', 'Surveyor', 'File1', 'File2', 'File3', 'ShsdHargaLelang', 'ShsdHargaPakai', 'FileLelang', 'File12', 'File22', 'File32', 'Keterangan1', 'Keterangan2', 'Keterangan3', ),
		BasePeer::TYPE_COLNAME => array (ShsdSurveyPeer::SHSD_ID, ShsdSurveyPeer::SATUAN, ShsdSurveyPeer::SHSD_NAME, ShsdSurveyPeer::SHSD_HARGA, ShsdSurveyPeer::SHSD_HARGA_DASAR, ShsdSurveyPeer::SHSD_SHOW, ShsdSurveyPeer::SHSD_FAKTOR_INFLASI, ShsdSurveyPeer::SHSD_LOCKED, ShsdSurveyPeer::REKENING_CODE, ShsdSurveyPeer::SHSD_MERK, ShsdSurveyPeer::NON_PAJAK, ShsdSurveyPeer::SHSD_ID_OLD, ShsdSurveyPeer::NAMA_DASAR, ShsdSurveyPeer::SPEC, ShsdSurveyPeer::HIDDEN_SPEC, ShsdSurveyPeer::USULAN_SKPD, ShsdSurveyPeer::IS_POTONG_BPJS, ShsdSurveyPeer::IS_IURAN_BPJS, ShsdSurveyPeer::TAHAP, ShsdSurveyPeer::IS_SURVEY_BP, ShsdSurveyPeer::IS_INFLASI, ShsdSurveyPeer::TOKO1, ShsdSurveyPeer::TOKO2, ShsdSurveyPeer::TOKO3, ShsdSurveyPeer::MERK1, ShsdSurveyPeer::MERK2, ShsdSurveyPeer::MERK3, ShsdSurveyPeer::HARGA1, ShsdSurveyPeer::HARGA2, ShsdSurveyPeer::HARGA3, ShsdSurveyPeer::TANGGAL_SURVEY, ShsdSurveyPeer::SURVEYOR, ShsdSurveyPeer::FILE1, ShsdSurveyPeer::FILE2, ShsdSurveyPeer::FILE3, ShsdSurveyPeer::SHSD_HARGA_LELANG, ShsdSurveyPeer::SHSD_HARGA_PAKAI, ShsdSurveyPeer::FILE_LELANG, ShsdSurveyPeer::FILE1_2, ShsdSurveyPeer::FILE2_2, ShsdSurveyPeer::FILE3_2, ShsdSurveyPeer::KETERANGAN1, ShsdSurveyPeer::KETERANGAN2, ShsdSurveyPeer::KETERANGAN3, ),
		BasePeer::TYPE_FIELDNAME => array ('shsd_id', 'satuan', 'shsd_name', 'shsd_harga', 'shsd_harga_dasar', 'shsd_show', 'shsd_faktor_inflasi', 'shsd_locked', 'rekening_code', 'shsd_merk', 'non_pajak', 'shsd_id_old', 'nama_dasar', 'spec', 'hidden_spec', 'usulan_skpd', 'is_potong_bpjs', 'is_iuran_bpjs', 'tahap', 'is_survey_bp', 'is_inflasi', 'toko1', 'toko2', 'toko3', 'merk1', 'merk2', 'merk3', 'harga1', 'harga2', 'harga3', 'tanggal_survey', 'surveyor', 'file1', 'file2', 'file3', 'shsd_harga_lelang', 'shsd_harga_pakai', 'file_lelang', 'file1_2', 'file2_2', 'file3_2', 'keterangan1', 'keterangan2', 'keterangan3', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('ShsdId' => 0, 'Satuan' => 1, 'ShsdName' => 2, 'ShsdHarga' => 3, 'ShsdHargaDasar' => 4, 'ShsdShow' => 5, 'ShsdFaktorInflasi' => 6, 'ShsdLocked' => 7, 'RekeningCode' => 8, 'ShsdMerk' => 9, 'NonPajak' => 10, 'ShsdIdOld' => 11, 'NamaDasar' => 12, 'Spec' => 13, 'HiddenSpec' => 14, 'UsulanSkpd' => 15, 'IsPotongBpjs' => 16, 'IsIuranBpjs' => 17, 'Tahap' => 18, 'IsSurveyBp' => 19, 'IsInflasi' => 20, 'Toko1' => 21, 'Toko2' => 22, 'Toko3' => 23, 'Merk1' => 24, 'Merk2' => 25, 'Merk3' => 26, 'Harga1' => 27, 'Harga2' => 28, 'Harga3' => 29, 'TanggalSurvey' => 30, 'Surveyor' => 31, 'File1' => 32, 'File2' => 33, 'File3' => 34, 'ShsdHargaLelang' => 35, 'ShsdHargaPakai' => 36, 'FileLelang' => 37, 'File12' => 38, 'File22' => 39, 'File32' => 40, 'Keterangan1' => 41, 'Keterangan2' => 42, 'Keterangan3' => 43, ),
		BasePeer::TYPE_COLNAME => array (ShsdSurveyPeer::SHSD_ID => 0, ShsdSurveyPeer::SATUAN => 1, ShsdSurveyPeer::SHSD_NAME => 2, ShsdSurveyPeer::SHSD_HARGA => 3, ShsdSurveyPeer::SHSD_HARGA_DASAR => 4, ShsdSurveyPeer::SHSD_SHOW => 5, ShsdSurveyPeer::SHSD_FAKTOR_INFLASI => 6, ShsdSurveyPeer::SHSD_LOCKED => 7, ShsdSurveyPeer::REKENING_CODE => 8, ShsdSurveyPeer::SHSD_MERK => 9, ShsdSurveyPeer::NON_PAJAK => 10, ShsdSurveyPeer::SHSD_ID_OLD => 11, ShsdSurveyPeer::NAMA_DASAR => 12, ShsdSurveyPeer::SPEC => 13, ShsdSurveyPeer::HIDDEN_SPEC => 14, ShsdSurveyPeer::USULAN_SKPD => 15, ShsdSurveyPeer::IS_POTONG_BPJS => 16, ShsdSurveyPeer::IS_IURAN_BPJS => 17, ShsdSurveyPeer::TAHAP => 18, ShsdSurveyPeer::IS_SURVEY_BP => 19, ShsdSurveyPeer::IS_INFLASI => 20, ShsdSurveyPeer::TOKO1 => 21, ShsdSurveyPeer::TOKO2 => 22, ShsdSurveyPeer::TOKO3 => 23, ShsdSurveyPeer::MERK1 => 24, ShsdSurveyPeer::MERK2 => 25, ShsdSurveyPeer::MERK3 => 26, ShsdSurveyPeer::HARGA1 => 27, ShsdSurveyPeer::HARGA2 => 28, ShsdSurveyPeer::HARGA3 => 29, ShsdSurveyPeer::TANGGAL_SURVEY => 30, ShsdSurveyPeer::SURVEYOR => 31, ShsdSurveyPeer::FILE1 => 32, ShsdSurveyPeer::FILE2 => 33, ShsdSurveyPeer::FILE3 => 34, ShsdSurveyPeer::SHSD_HARGA_LELANG => 35, ShsdSurveyPeer::SHSD_HARGA_PAKAI => 36, ShsdSurveyPeer::FILE_LELANG => 37, ShsdSurveyPeer::FILE1_2 => 38, ShsdSurveyPeer::FILE2_2 => 39, ShsdSurveyPeer::FILE3_2 => 40, ShsdSurveyPeer::KETERANGAN1 => 41, ShsdSurveyPeer::KETERANGAN2 => 42, ShsdSurveyPeer::KETERANGAN3 => 43, ),
		BasePeer::TYPE_FIELDNAME => array ('shsd_id' => 0, 'satuan' => 1, 'shsd_name' => 2, 'shsd_harga' => 3, 'shsd_harga_dasar' => 4, 'shsd_show' => 5, 'shsd_faktor_inflasi' => 6, 'shsd_locked' => 7, 'rekening_code' => 8, 'shsd_merk' => 9, 'non_pajak' => 10, 'shsd_id_old' => 11, 'nama_dasar' => 12, 'spec' => 13, 'hidden_spec' => 14, 'usulan_skpd' => 15, 'is_potong_bpjs' => 16, 'is_iuran_bpjs' => 17, 'tahap' => 18, 'is_survey_bp' => 19, 'is_inflasi' => 20, 'toko1' => 21, 'toko2' => 22, 'toko3' => 23, 'merk1' => 24, 'merk2' => 25, 'merk3' => 26, 'harga1' => 27, 'harga2' => 28, 'harga3' => 29, 'tanggal_survey' => 30, 'surveyor' => 31, 'file1' => 32, 'file2' => 33, 'file3' => 34, 'shsd_harga_lelang' => 35, 'shsd_harga_pakai' => 36, 'file_lelang' => 37, 'file1_2' => 38, 'file2_2' => 39, 'file3_2' => 40, 'keterangan1' => 41, 'keterangan2' => 42, 'keterangan3' => 43, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/ShsdSurveyMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.ShsdSurveyMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = ShsdSurveyPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(ShsdSurveyPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(ShsdSurveyPeer::SHSD_ID);

		$criteria->addSelectColumn(ShsdSurveyPeer::SATUAN);

		$criteria->addSelectColumn(ShsdSurveyPeer::SHSD_NAME);

		$criteria->addSelectColumn(ShsdSurveyPeer::SHSD_HARGA);

		$criteria->addSelectColumn(ShsdSurveyPeer::SHSD_HARGA_DASAR);

		$criteria->addSelectColumn(ShsdSurveyPeer::SHSD_SHOW);

		$criteria->addSelectColumn(ShsdSurveyPeer::SHSD_FAKTOR_INFLASI);

		$criteria->addSelectColumn(ShsdSurveyPeer::SHSD_LOCKED);

		$criteria->addSelectColumn(ShsdSurveyPeer::REKENING_CODE);

		$criteria->addSelectColumn(ShsdSurveyPeer::SHSD_MERK);

		$criteria->addSelectColumn(ShsdSurveyPeer::NON_PAJAK);

		$criteria->addSelectColumn(ShsdSurveyPeer::SHSD_ID_OLD);

		$criteria->addSelectColumn(ShsdSurveyPeer::NAMA_DASAR);

		$criteria->addSelectColumn(ShsdSurveyPeer::SPEC);

		$criteria->addSelectColumn(ShsdSurveyPeer::HIDDEN_SPEC);

		$criteria->addSelectColumn(ShsdSurveyPeer::USULAN_SKPD);

		$criteria->addSelectColumn(ShsdSurveyPeer::IS_POTONG_BPJS);

		$criteria->addSelectColumn(ShsdSurveyPeer::IS_IURAN_BPJS);

		$criteria->addSelectColumn(ShsdSurveyPeer::TAHAP);

		$criteria->addSelectColumn(ShsdSurveyPeer::IS_SURVEY_BP);

		$criteria->addSelectColumn(ShsdSurveyPeer::IS_INFLASI);

		$criteria->addSelectColumn(ShsdSurveyPeer::TOKO1);

		$criteria->addSelectColumn(ShsdSurveyPeer::TOKO2);

		$criteria->addSelectColumn(ShsdSurveyPeer::TOKO3);

		$criteria->addSelectColumn(ShsdSurveyPeer::MERK1);

		$criteria->addSelectColumn(ShsdSurveyPeer::MERK2);

		$criteria->addSelectColumn(ShsdSurveyPeer::MERK3);

		$criteria->addSelectColumn(ShsdSurveyPeer::HARGA1);

		$criteria->addSelectColumn(ShsdSurveyPeer::HARGA2);

		$criteria->addSelectColumn(ShsdSurveyPeer::HARGA3);

		$criteria->addSelectColumn(ShsdSurveyPeer::TANGGAL_SURVEY);

		$criteria->addSelectColumn(ShsdSurveyPeer::SURVEYOR);

		$criteria->addSelectColumn(ShsdSurveyPeer::FILE1);

		$criteria->addSelectColumn(ShsdSurveyPeer::FILE2);

		$criteria->addSelectColumn(ShsdSurveyPeer::FILE3);

		$criteria->addSelectColumn(ShsdSurveyPeer::SHSD_HARGA_LELANG);

		$criteria->addSelectColumn(ShsdSurveyPeer::SHSD_HARGA_PAKAI);

		$criteria->addSelectColumn(ShsdSurveyPeer::FILE_LELANG);

		$criteria->addSelectColumn(ShsdSurveyPeer::FILE1_2);

		$criteria->addSelectColumn(ShsdSurveyPeer::FILE2_2);

		$criteria->addSelectColumn(ShsdSurveyPeer::FILE3_2);

		$criteria->addSelectColumn(ShsdSurveyPeer::KETERANGAN1);

		$criteria->addSelectColumn(ShsdSurveyPeer::KETERANGAN2);

		$criteria->addSelectColumn(ShsdSurveyPeer::KETERANGAN3);

	}

	const COUNT = 'COUNT(ebudget.shsd_survey.SHSD_ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.shsd_survey.SHSD_ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(ShsdSurveyPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(ShsdSurveyPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = ShsdSurveyPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = ShsdSurveyPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return ShsdSurveyPeer::populateObjects(ShsdSurveyPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			ShsdSurveyPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = ShsdSurveyPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return ShsdSurveyPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(ShsdSurveyPeer::SHSD_ID);
			$selectCriteria->add(ShsdSurveyPeer::SHSD_ID, $criteria->remove(ShsdSurveyPeer::SHSD_ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(ShsdSurveyPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(ShsdSurveyPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof ShsdSurvey) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(ShsdSurveyPeer::SHSD_ID, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(ShsdSurvey $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(ShsdSurveyPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(ShsdSurveyPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(ShsdSurveyPeer::DATABASE_NAME, ShsdSurveyPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = ShsdSurveyPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(ShsdSurveyPeer::DATABASE_NAME);

		$criteria->add(ShsdSurveyPeer::SHSD_ID, $pk);


		$v = ShsdSurveyPeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(ShsdSurveyPeer::SHSD_ID, $pks, Criteria::IN);
			$objs = ShsdSurveyPeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BaseShsdSurveyPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/ShsdSurveyMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.ShsdSurveyMapBuilder');
}
