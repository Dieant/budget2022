<?php


abstract class BaseLogApprovalPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.log_approval';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.LogApproval';

	
	const NUM_COLUMNS = 9;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const ID = 'ebudget.log_approval.ID';

	
	const UNIT_ID = 'ebudget.log_approval.UNIT_ID';

	
	const KEGIATAN_CODE = 'ebudget.log_approval.KEGIATAN_CODE';

	
	const USER_ID = 'ebudget.log_approval.USER_ID';

	
	const WAKTU = 'ebudget.log_approval.WAKTU';

	
	const KUMPULAN_DETAIL_NO = 'ebudget.log_approval.KUMPULAN_DETAIL_NO';

	
	const SEBAGAI = 'ebudget.log_approval.SEBAGAI';

	
	const TAHAP = 'ebudget.log_approval.TAHAP';

	
	const CATATAN = 'ebudget.log_approval.CATATAN';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('Id', 'UnitId', 'KegiatanCode', 'UserId', 'Waktu', 'KumpulanDetailNo', 'Sebagai', 'Tahap', 'Catatan', ),
		BasePeer::TYPE_COLNAME => array (LogApprovalPeer::ID, LogApprovalPeer::UNIT_ID, LogApprovalPeer::KEGIATAN_CODE, LogApprovalPeer::USER_ID, LogApprovalPeer::WAKTU, LogApprovalPeer::KUMPULAN_DETAIL_NO, LogApprovalPeer::SEBAGAI, LogApprovalPeer::TAHAP, LogApprovalPeer::CATATAN, ),
		BasePeer::TYPE_FIELDNAME => array ('id', 'unit_id', 'kegiatan_code', 'user_id', 'waktu', 'kumpulan_detail_no', 'sebagai', 'tahap', 'catatan', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('Id' => 0, 'UnitId' => 1, 'KegiatanCode' => 2, 'UserId' => 3, 'Waktu' => 4, 'KumpulanDetailNo' => 5, 'Sebagai' => 6, 'Tahap' => 7, 'Catatan' => 8, ),
		BasePeer::TYPE_COLNAME => array (LogApprovalPeer::ID => 0, LogApprovalPeer::UNIT_ID => 1, LogApprovalPeer::KEGIATAN_CODE => 2, LogApprovalPeer::USER_ID => 3, LogApprovalPeer::WAKTU => 4, LogApprovalPeer::KUMPULAN_DETAIL_NO => 5, LogApprovalPeer::SEBAGAI => 6, LogApprovalPeer::TAHAP => 7, LogApprovalPeer::CATATAN => 8, ),
		BasePeer::TYPE_FIELDNAME => array ('id' => 0, 'unit_id' => 1, 'kegiatan_code' => 2, 'user_id' => 3, 'waktu' => 4, 'kumpulan_detail_no' => 5, 'sebagai' => 6, 'tahap' => 7, 'catatan' => 8, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/LogApprovalMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.LogApprovalMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = LogApprovalPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(LogApprovalPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(LogApprovalPeer::ID);

		$criteria->addSelectColumn(LogApprovalPeer::UNIT_ID);

		$criteria->addSelectColumn(LogApprovalPeer::KEGIATAN_CODE);

		$criteria->addSelectColumn(LogApprovalPeer::USER_ID);

		$criteria->addSelectColumn(LogApprovalPeer::WAKTU);

		$criteria->addSelectColumn(LogApprovalPeer::KUMPULAN_DETAIL_NO);

		$criteria->addSelectColumn(LogApprovalPeer::SEBAGAI);

		$criteria->addSelectColumn(LogApprovalPeer::TAHAP);

		$criteria->addSelectColumn(LogApprovalPeer::CATATAN);

	}

	const COUNT = 'COUNT(ebudget.log_approval.ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.log_approval.ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(LogApprovalPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(LogApprovalPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = LogApprovalPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = LogApprovalPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return LogApprovalPeer::populateObjects(LogApprovalPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			LogApprovalPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = LogApprovalPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return LogApprovalPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(LogApprovalPeer::ID);
			$selectCriteria->add(LogApprovalPeer::ID, $criteria->remove(LogApprovalPeer::ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(LogApprovalPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(LogApprovalPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof LogApproval) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(LogApprovalPeer::ID, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(LogApproval $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(LogApprovalPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(LogApprovalPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(LogApprovalPeer::DATABASE_NAME, LogApprovalPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = LogApprovalPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(LogApprovalPeer::DATABASE_NAME);

		$criteria->add(LogApprovalPeer::ID, $pk);


		$v = LogApprovalPeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(LogApprovalPeer::ID, $pks, Criteria::IN);
			$objs = LogApprovalPeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BaseLogApprovalPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/LogApprovalMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.LogApprovalMapBuilder');
}
