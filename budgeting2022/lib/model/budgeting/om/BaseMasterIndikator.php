<?php


abstract class BaseMasterIndikator extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $kode_program;


	
	protected $kode_indikator;


	
	protected $nama_indikator;


	
	protected $kode_indikator2;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getKodeProgram()
	{

		return $this->kode_program;
	}

	
	public function getKodeIndikator()
	{

		return $this->kode_indikator;
	}

	
	public function getNamaIndikator()
	{

		return $this->nama_indikator;
	}

	
	public function getKodeIndikator2()
	{

		return $this->kode_indikator2;
	}

	
	public function setKodeProgram($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_program !== $v) {
			$this->kode_program = $v;
			$this->modifiedColumns[] = MasterIndikatorPeer::KODE_PROGRAM;
		}

	} 
	
	public function setKodeIndikator($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_indikator !== $v) {
			$this->kode_indikator = $v;
			$this->modifiedColumns[] = MasterIndikatorPeer::KODE_INDIKATOR;
		}

	} 
	
	public function setNamaIndikator($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nama_indikator !== $v) {
			$this->nama_indikator = $v;
			$this->modifiedColumns[] = MasterIndikatorPeer::NAMA_INDIKATOR;
		}

	} 
	
	public function setKodeIndikator2($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_indikator2 !== $v) {
			$this->kode_indikator2 = $v;
			$this->modifiedColumns[] = MasterIndikatorPeer::KODE_INDIKATOR2;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->kode_program = $rs->getString($startcol + 0);

			$this->kode_indikator = $rs->getString($startcol + 1);

			$this->nama_indikator = $rs->getString($startcol + 2);

			$this->kode_indikator2 = $rs->getString($startcol + 3);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 4; 
		} catch (Exception $e) {
			throw new PropelException("Error populating MasterIndikator object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(MasterIndikatorPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			MasterIndikatorPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(MasterIndikatorPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = MasterIndikatorPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += MasterIndikatorPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = MasterIndikatorPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = MasterIndikatorPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getKodeProgram();
				break;
			case 1:
				return $this->getKodeIndikator();
				break;
			case 2:
				return $this->getNamaIndikator();
				break;
			case 3:
				return $this->getKodeIndikator2();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = MasterIndikatorPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getKodeProgram(),
			$keys[1] => $this->getKodeIndikator(),
			$keys[2] => $this->getNamaIndikator(),
			$keys[3] => $this->getKodeIndikator2(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = MasterIndikatorPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setKodeProgram($value);
				break;
			case 1:
				$this->setKodeIndikator($value);
				break;
			case 2:
				$this->setNamaIndikator($value);
				break;
			case 3:
				$this->setKodeIndikator2($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = MasterIndikatorPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setKodeProgram($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setKodeIndikator($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setNamaIndikator($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setKodeIndikator2($arr[$keys[3]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(MasterIndikatorPeer::DATABASE_NAME);

		if ($this->isColumnModified(MasterIndikatorPeer::KODE_PROGRAM)) $criteria->add(MasterIndikatorPeer::KODE_PROGRAM, $this->kode_program);
		if ($this->isColumnModified(MasterIndikatorPeer::KODE_INDIKATOR)) $criteria->add(MasterIndikatorPeer::KODE_INDIKATOR, $this->kode_indikator);
		if ($this->isColumnModified(MasterIndikatorPeer::NAMA_INDIKATOR)) $criteria->add(MasterIndikatorPeer::NAMA_INDIKATOR, $this->nama_indikator);
		if ($this->isColumnModified(MasterIndikatorPeer::KODE_INDIKATOR2)) $criteria->add(MasterIndikatorPeer::KODE_INDIKATOR2, $this->kode_indikator2);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(MasterIndikatorPeer::DATABASE_NAME);

		$criteria->add(MasterIndikatorPeer::KODE_PROGRAM, $this->kode_program);
		$criteria->add(MasterIndikatorPeer::KODE_INDIKATOR, $this->kode_indikator);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		$pks = array();

		$pks[0] = $this->getKodeProgram();

		$pks[1] = $this->getKodeIndikator();

		return $pks;
	}

	
	public function setPrimaryKey($keys)
	{

		$this->setKodeProgram($keys[0]);

		$this->setKodeIndikator($keys[1]);

	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setNamaIndikator($this->nama_indikator);

		$copyObj->setKodeIndikator2($this->kode_indikator2);


		$copyObj->setNew(true);

		$copyObj->setKodeProgram(NULL); 
		$copyObj->setKodeIndikator(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new MasterIndikatorPeer();
		}
		return self::$peer;
	}

} 