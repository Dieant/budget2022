<?php


abstract class BaseKelompokBelanja extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $belanja_id;


	
	protected $belanja_name;


	
	protected $belanja_urutan;


	
	protected $belanja_code;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getBelanjaId()
	{

		return $this->belanja_id;
	}

	
	public function getBelanjaName()
	{

		return $this->belanja_name;
	}

	
	public function getBelanjaUrutan()
	{

		return $this->belanja_urutan;
	}

	
	public function getBelanjaCode()
	{

		return $this->belanja_code;
	}

	
	public function setBelanjaId($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->belanja_id !== $v) {
			$this->belanja_id = $v;
			$this->modifiedColumns[] = KelompokBelanjaPeer::BELANJA_ID;
		}

	} 
	
	public function setBelanjaName($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->belanja_name !== $v) {
			$this->belanja_name = $v;
			$this->modifiedColumns[] = KelompokBelanjaPeer::BELANJA_NAME;
		}

	} 
	
	public function setBelanjaUrutan($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->belanja_urutan !== $v) {
			$this->belanja_urutan = $v;
			$this->modifiedColumns[] = KelompokBelanjaPeer::BELANJA_URUTAN;
		}

	} 
	
	public function setBelanjaCode($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->belanja_code !== $v) {
			$this->belanja_code = $v;
			$this->modifiedColumns[] = KelompokBelanjaPeer::BELANJA_CODE;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->belanja_id = $rs->getInt($startcol + 0);

			$this->belanja_name = $rs->getString($startcol + 1);

			$this->belanja_urutan = $rs->getInt($startcol + 2);

			$this->belanja_code = $rs->getString($startcol + 3);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 4; 
		} catch (Exception $e) {
			throw new PropelException("Error populating KelompokBelanja object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(KelompokBelanjaPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			KelompokBelanjaPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(KelompokBelanjaPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = KelompokBelanjaPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += KelompokBelanjaPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = KelompokBelanjaPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = KelompokBelanjaPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getBelanjaId();
				break;
			case 1:
				return $this->getBelanjaName();
				break;
			case 2:
				return $this->getBelanjaUrutan();
				break;
			case 3:
				return $this->getBelanjaCode();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = KelompokBelanjaPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getBelanjaId(),
			$keys[1] => $this->getBelanjaName(),
			$keys[2] => $this->getBelanjaUrutan(),
			$keys[3] => $this->getBelanjaCode(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = KelompokBelanjaPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setBelanjaId($value);
				break;
			case 1:
				$this->setBelanjaName($value);
				break;
			case 2:
				$this->setBelanjaUrutan($value);
				break;
			case 3:
				$this->setBelanjaCode($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = KelompokBelanjaPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setBelanjaId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setBelanjaName($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setBelanjaUrutan($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setBelanjaCode($arr[$keys[3]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(KelompokBelanjaPeer::DATABASE_NAME);

		if ($this->isColumnModified(KelompokBelanjaPeer::BELANJA_ID)) $criteria->add(KelompokBelanjaPeer::BELANJA_ID, $this->belanja_id);
		if ($this->isColumnModified(KelompokBelanjaPeer::BELANJA_NAME)) $criteria->add(KelompokBelanjaPeer::BELANJA_NAME, $this->belanja_name);
		if ($this->isColumnModified(KelompokBelanjaPeer::BELANJA_URUTAN)) $criteria->add(KelompokBelanjaPeer::BELANJA_URUTAN, $this->belanja_urutan);
		if ($this->isColumnModified(KelompokBelanjaPeer::BELANJA_CODE)) $criteria->add(KelompokBelanjaPeer::BELANJA_CODE, $this->belanja_code);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(KelompokBelanjaPeer::DATABASE_NAME);


		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return null;
	}

	
	 public function setPrimaryKey($pk)
	 {
		 	 }

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setBelanjaId($this->belanja_id);

		$copyObj->setBelanjaName($this->belanja_name);

		$copyObj->setBelanjaUrutan($this->belanja_urutan);

		$copyObj->setBelanjaCode($this->belanja_code);


		$copyObj->setNew(true);

	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new KelompokBelanjaPeer();
		}
		return self::$peer;
	}

} 