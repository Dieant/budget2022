<?php


abstract class BaseRkuaSubtitleIndikatorPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.rkua_subtitle_indikator';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.RkuaSubtitleIndikator';

	
	const NUM_COLUMNS = 22;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const UNIT_ID = 'ebudget.rkua_subtitle_indikator.UNIT_ID';

	
	const KEGIATAN_CODE = 'ebudget.rkua_subtitle_indikator.KEGIATAN_CODE';

	
	const SUBTITLE = 'ebudget.rkua_subtitle_indikator.SUBTITLE';

	
	const INDIKATOR = 'ebudget.rkua_subtitle_indikator.INDIKATOR';

	
	const NILAI = 'ebudget.rkua_subtitle_indikator.NILAI';

	
	const SATUAN = 'ebudget.rkua_subtitle_indikator.SATUAN';

	
	const LAST_UPDATE_USER = 'ebudget.rkua_subtitle_indikator.LAST_UPDATE_USER';

	
	const LAST_UPDATE_TIME = 'ebudget.rkua_subtitle_indikator.LAST_UPDATE_TIME';

	
	const LAST_UPDATE_IP = 'ebudget.rkua_subtitle_indikator.LAST_UPDATE_IP';

	
	const TAHAP = 'ebudget.rkua_subtitle_indikator.TAHAP';

	
	const SUB_ID = 'ebudget.rkua_subtitle_indikator.SUB_ID';

	
	const TAHUN = 'ebudget.rkua_subtitle_indikator.TAHUN';

	
	const LOCK_SUBTITLE = 'ebudget.rkua_subtitle_indikator.LOCK_SUBTITLE';

	
	const PRIORITAS = 'ebudget.rkua_subtitle_indikator.PRIORITAS';

	
	const CATATAN = 'ebudget.rkua_subtitle_indikator.CATATAN';

	
	const LAKILAKI = 'ebudget.rkua_subtitle_indikator.LAKILAKI';

	
	const PEREMPUAN = 'ebudget.rkua_subtitle_indikator.PEREMPUAN';

	
	const DEWASA = 'ebudget.rkua_subtitle_indikator.DEWASA';

	
	const ANAK = 'ebudget.rkua_subtitle_indikator.ANAK';

	
	const LANSIA = 'ebudget.rkua_subtitle_indikator.LANSIA';

	
	const INKLUSI = 'ebudget.rkua_subtitle_indikator.INKLUSI';

	
	const GENDER = 'ebudget.rkua_subtitle_indikator.GENDER';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('UnitId', 'KegiatanCode', 'Subtitle', 'Indikator', 'Nilai', 'Satuan', 'LastUpdateUser', 'LastUpdateTime', 'LastUpdateIp', 'Tahap', 'SubId', 'Tahun', 'LockSubtitle', 'Prioritas', 'Catatan', 'Lakilaki', 'Perempuan', 'Dewasa', 'Anak', 'Lansia', 'Inklusi', 'Gender', ),
		BasePeer::TYPE_COLNAME => array (RkuaSubtitleIndikatorPeer::UNIT_ID, RkuaSubtitleIndikatorPeer::KEGIATAN_CODE, RkuaSubtitleIndikatorPeer::SUBTITLE, RkuaSubtitleIndikatorPeer::INDIKATOR, RkuaSubtitleIndikatorPeer::NILAI, RkuaSubtitleIndikatorPeer::SATUAN, RkuaSubtitleIndikatorPeer::LAST_UPDATE_USER, RkuaSubtitleIndikatorPeer::LAST_UPDATE_TIME, RkuaSubtitleIndikatorPeer::LAST_UPDATE_IP, RkuaSubtitleIndikatorPeer::TAHAP, RkuaSubtitleIndikatorPeer::SUB_ID, RkuaSubtitleIndikatorPeer::TAHUN, RkuaSubtitleIndikatorPeer::LOCK_SUBTITLE, RkuaSubtitleIndikatorPeer::PRIORITAS, RkuaSubtitleIndikatorPeer::CATATAN, RkuaSubtitleIndikatorPeer::LAKILAKI, RkuaSubtitleIndikatorPeer::PEREMPUAN, RkuaSubtitleIndikatorPeer::DEWASA, RkuaSubtitleIndikatorPeer::ANAK, RkuaSubtitleIndikatorPeer::LANSIA, RkuaSubtitleIndikatorPeer::INKLUSI, RkuaSubtitleIndikatorPeer::GENDER, ),
		BasePeer::TYPE_FIELDNAME => array ('unit_id', 'kegiatan_code', 'subtitle', 'indikator', 'nilai', 'satuan', 'last_update_user', 'last_update_time', 'last_update_ip', 'tahap', 'sub_id', 'tahun', 'lock_subtitle', 'prioritas', 'catatan', 'lakilaki', 'perempuan', 'dewasa', 'anak', 'lansia', 'inklusi', 'gender', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('UnitId' => 0, 'KegiatanCode' => 1, 'Subtitle' => 2, 'Indikator' => 3, 'Nilai' => 4, 'Satuan' => 5, 'LastUpdateUser' => 6, 'LastUpdateTime' => 7, 'LastUpdateIp' => 8, 'Tahap' => 9, 'SubId' => 10, 'Tahun' => 11, 'LockSubtitle' => 12, 'Prioritas' => 13, 'Catatan' => 14, 'Lakilaki' => 15, 'Perempuan' => 16, 'Dewasa' => 17, 'Anak' => 18, 'Lansia' => 19, 'Inklusi' => 20, 'Gender' => 21, ),
		BasePeer::TYPE_COLNAME => array (RkuaSubtitleIndikatorPeer::UNIT_ID => 0, RkuaSubtitleIndikatorPeer::KEGIATAN_CODE => 1, RkuaSubtitleIndikatorPeer::SUBTITLE => 2, RkuaSubtitleIndikatorPeer::INDIKATOR => 3, RkuaSubtitleIndikatorPeer::NILAI => 4, RkuaSubtitleIndikatorPeer::SATUAN => 5, RkuaSubtitleIndikatorPeer::LAST_UPDATE_USER => 6, RkuaSubtitleIndikatorPeer::LAST_UPDATE_TIME => 7, RkuaSubtitleIndikatorPeer::LAST_UPDATE_IP => 8, RkuaSubtitleIndikatorPeer::TAHAP => 9, RkuaSubtitleIndikatorPeer::SUB_ID => 10, RkuaSubtitleIndikatorPeer::TAHUN => 11, RkuaSubtitleIndikatorPeer::LOCK_SUBTITLE => 12, RkuaSubtitleIndikatorPeer::PRIORITAS => 13, RkuaSubtitleIndikatorPeer::CATATAN => 14, RkuaSubtitleIndikatorPeer::LAKILAKI => 15, RkuaSubtitleIndikatorPeer::PEREMPUAN => 16, RkuaSubtitleIndikatorPeer::DEWASA => 17, RkuaSubtitleIndikatorPeer::ANAK => 18, RkuaSubtitleIndikatorPeer::LANSIA => 19, RkuaSubtitleIndikatorPeer::INKLUSI => 20, RkuaSubtitleIndikatorPeer::GENDER => 21, ),
		BasePeer::TYPE_FIELDNAME => array ('unit_id' => 0, 'kegiatan_code' => 1, 'subtitle' => 2, 'indikator' => 3, 'nilai' => 4, 'satuan' => 5, 'last_update_user' => 6, 'last_update_time' => 7, 'last_update_ip' => 8, 'tahap' => 9, 'sub_id' => 10, 'tahun' => 11, 'lock_subtitle' => 12, 'prioritas' => 13, 'catatan' => 14, 'lakilaki' => 15, 'perempuan' => 16, 'dewasa' => 17, 'anak' => 18, 'lansia' => 19, 'inklusi' => 20, 'gender' => 21, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/RkuaSubtitleIndikatorMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.RkuaSubtitleIndikatorMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = RkuaSubtitleIndikatorPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(RkuaSubtitleIndikatorPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(RkuaSubtitleIndikatorPeer::UNIT_ID);

		$criteria->addSelectColumn(RkuaSubtitleIndikatorPeer::KEGIATAN_CODE);

		$criteria->addSelectColumn(RkuaSubtitleIndikatorPeer::SUBTITLE);

		$criteria->addSelectColumn(RkuaSubtitleIndikatorPeer::INDIKATOR);

		$criteria->addSelectColumn(RkuaSubtitleIndikatorPeer::NILAI);

		$criteria->addSelectColumn(RkuaSubtitleIndikatorPeer::SATUAN);

		$criteria->addSelectColumn(RkuaSubtitleIndikatorPeer::LAST_UPDATE_USER);

		$criteria->addSelectColumn(RkuaSubtitleIndikatorPeer::LAST_UPDATE_TIME);

		$criteria->addSelectColumn(RkuaSubtitleIndikatorPeer::LAST_UPDATE_IP);

		$criteria->addSelectColumn(RkuaSubtitleIndikatorPeer::TAHAP);

		$criteria->addSelectColumn(RkuaSubtitleIndikatorPeer::SUB_ID);

		$criteria->addSelectColumn(RkuaSubtitleIndikatorPeer::TAHUN);

		$criteria->addSelectColumn(RkuaSubtitleIndikatorPeer::LOCK_SUBTITLE);

		$criteria->addSelectColumn(RkuaSubtitleIndikatorPeer::PRIORITAS);

		$criteria->addSelectColumn(RkuaSubtitleIndikatorPeer::CATATAN);

		$criteria->addSelectColumn(RkuaSubtitleIndikatorPeer::LAKILAKI);

		$criteria->addSelectColumn(RkuaSubtitleIndikatorPeer::PEREMPUAN);

		$criteria->addSelectColumn(RkuaSubtitleIndikatorPeer::DEWASA);

		$criteria->addSelectColumn(RkuaSubtitleIndikatorPeer::ANAK);

		$criteria->addSelectColumn(RkuaSubtitleIndikatorPeer::LANSIA);

		$criteria->addSelectColumn(RkuaSubtitleIndikatorPeer::INKLUSI);

		$criteria->addSelectColumn(RkuaSubtitleIndikatorPeer::GENDER);

	}

	const COUNT = 'COUNT(ebudget.rkua_subtitle_indikator.SUB_ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.rkua_subtitle_indikator.SUB_ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(RkuaSubtitleIndikatorPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(RkuaSubtitleIndikatorPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = RkuaSubtitleIndikatorPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = RkuaSubtitleIndikatorPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return RkuaSubtitleIndikatorPeer::populateObjects(RkuaSubtitleIndikatorPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			RkuaSubtitleIndikatorPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = RkuaSubtitleIndikatorPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return RkuaSubtitleIndikatorPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}

		$criteria->remove(RkuaSubtitleIndikatorPeer::SUB_ID); 

				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(RkuaSubtitleIndikatorPeer::SUB_ID);
			$selectCriteria->add(RkuaSubtitleIndikatorPeer::SUB_ID, $criteria->remove(RkuaSubtitleIndikatorPeer::SUB_ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(RkuaSubtitleIndikatorPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(RkuaSubtitleIndikatorPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof RkuaSubtitleIndikator) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(RkuaSubtitleIndikatorPeer::SUB_ID, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(RkuaSubtitleIndikator $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(RkuaSubtitleIndikatorPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(RkuaSubtitleIndikatorPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(RkuaSubtitleIndikatorPeer::DATABASE_NAME, RkuaSubtitleIndikatorPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = RkuaSubtitleIndikatorPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(RkuaSubtitleIndikatorPeer::DATABASE_NAME);

		$criteria->add(RkuaSubtitleIndikatorPeer::SUB_ID, $pk);


		$v = RkuaSubtitleIndikatorPeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(RkuaSubtitleIndikatorPeer::SUB_ID, $pks, Criteria::IN);
			$objs = RkuaSubtitleIndikatorPeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BaseRkuaSubtitleIndikatorPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/RkuaSubtitleIndikatorMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.RkuaSubtitleIndikatorMapBuilder');
}
