<?php


abstract class BaseBappekoRincianSubParameter extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $unit_id;


	
	protected $kegiatan_code;


	
	protected $from_sub_kegiatan;


	
	protected $sub_kegiatan_name;


	
	protected $subtitle;


	
	protected $detail_name;


	
	protected $new_subtitle;


	
	protected $param;


	
	protected $kecamatan;


	
	protected $max_nilai;


	
	protected $keterangan;


	
	protected $ket_pembagi;


	
	protected $pembagi;


	
	protected $kode_sub;


	
	protected $tahun;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getUnitId()
	{

		return $this->unit_id;
	}

	
	public function getKegiatanCode()
	{

		return $this->kegiatan_code;
	}

	
	public function getFromSubKegiatan()
	{

		return $this->from_sub_kegiatan;
	}

	
	public function getSubKegiatanName()
	{

		return $this->sub_kegiatan_name;
	}

	
	public function getSubtitle()
	{

		return $this->subtitle;
	}

	
	public function getDetailName()
	{

		return $this->detail_name;
	}

	
	public function getNewSubtitle()
	{

		return $this->new_subtitle;
	}

	
	public function getParam()
	{

		return $this->param;
	}

	
	public function getKecamatan()
	{

		return $this->kecamatan;
	}

	
	public function getMaxNilai()
	{

		return $this->max_nilai;
	}

	
	public function getKeterangan()
	{

		return $this->keterangan;
	}

	
	public function getKetPembagi()
	{

		return $this->ket_pembagi;
	}

	
	public function getPembagi()
	{

		return $this->pembagi;
	}

	
	public function getKodeSub()
	{

		return $this->kode_sub;
	}

	
	public function getTahun()
	{

		return $this->tahun;
	}

	
	public function setUnitId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->unit_id !== $v) {
			$this->unit_id = $v;
			$this->modifiedColumns[] = BappekoRincianSubParameterPeer::UNIT_ID;
		}

	} 
	
	public function setKegiatanCode($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kegiatan_code !== $v) {
			$this->kegiatan_code = $v;
			$this->modifiedColumns[] = BappekoRincianSubParameterPeer::KEGIATAN_CODE;
		}

	} 
	
	public function setFromSubKegiatan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->from_sub_kegiatan !== $v) {
			$this->from_sub_kegiatan = $v;
			$this->modifiedColumns[] = BappekoRincianSubParameterPeer::FROM_SUB_KEGIATAN;
		}

	} 
	
	public function setSubKegiatanName($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->sub_kegiatan_name !== $v) {
			$this->sub_kegiatan_name = $v;
			$this->modifiedColumns[] = BappekoRincianSubParameterPeer::SUB_KEGIATAN_NAME;
		}

	} 
	
	public function setSubtitle($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->subtitle !== $v) {
			$this->subtitle = $v;
			$this->modifiedColumns[] = BappekoRincianSubParameterPeer::SUBTITLE;
		}

	} 
	
	public function setDetailName($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->detail_name !== $v) {
			$this->detail_name = $v;
			$this->modifiedColumns[] = BappekoRincianSubParameterPeer::DETAIL_NAME;
		}

	} 
	
	public function setNewSubtitle($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->new_subtitle !== $v) {
			$this->new_subtitle = $v;
			$this->modifiedColumns[] = BappekoRincianSubParameterPeer::NEW_SUBTITLE;
		}

	} 
	
	public function setParam($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->param !== $v) {
			$this->param = $v;
			$this->modifiedColumns[] = BappekoRincianSubParameterPeer::PARAM;
		}

	} 
	
	public function setKecamatan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kecamatan !== $v) {
			$this->kecamatan = $v;
			$this->modifiedColumns[] = BappekoRincianSubParameterPeer::KECAMATAN;
		}

	} 
	
	public function setMaxNilai($v)
	{

		if ($this->max_nilai !== $v) {
			$this->max_nilai = $v;
			$this->modifiedColumns[] = BappekoRincianSubParameterPeer::MAX_NILAI;
		}

	} 
	
	public function setKeterangan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->keterangan !== $v) {
			$this->keterangan = $v;
			$this->modifiedColumns[] = BappekoRincianSubParameterPeer::KETERANGAN;
		}

	} 
	
	public function setKetPembagi($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->ket_pembagi !== $v) {
			$this->ket_pembagi = $v;
			$this->modifiedColumns[] = BappekoRincianSubParameterPeer::KET_PEMBAGI;
		}

	} 
	
	public function setPembagi($v)
	{

		if ($this->pembagi !== $v) {
			$this->pembagi = $v;
			$this->modifiedColumns[] = BappekoRincianSubParameterPeer::PEMBAGI;
		}

	} 
	
	public function setKodeSub($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_sub !== $v) {
			$this->kode_sub = $v;
			$this->modifiedColumns[] = BappekoRincianSubParameterPeer::KODE_SUB;
		}

	} 
	
	public function setTahun($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->tahun !== $v) {
			$this->tahun = $v;
			$this->modifiedColumns[] = BappekoRincianSubParameterPeer::TAHUN;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->unit_id = $rs->getString($startcol + 0);

			$this->kegiatan_code = $rs->getString($startcol + 1);

			$this->from_sub_kegiatan = $rs->getString($startcol + 2);

			$this->sub_kegiatan_name = $rs->getString($startcol + 3);

			$this->subtitle = $rs->getString($startcol + 4);

			$this->detail_name = $rs->getString($startcol + 5);

			$this->new_subtitle = $rs->getString($startcol + 6);

			$this->param = $rs->getString($startcol + 7);

			$this->kecamatan = $rs->getString($startcol + 8);

			$this->max_nilai = $rs->getFloat($startcol + 9);

			$this->keterangan = $rs->getString($startcol + 10);

			$this->ket_pembagi = $rs->getString($startcol + 11);

			$this->pembagi = $rs->getFloat($startcol + 12);

			$this->kode_sub = $rs->getString($startcol + 13);

			$this->tahun = $rs->getString($startcol + 14);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 15; 
		} catch (Exception $e) {
			throw new PropelException("Error populating BappekoRincianSubParameter object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(BappekoRincianSubParameterPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			BappekoRincianSubParameterPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(BappekoRincianSubParameterPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = BappekoRincianSubParameterPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += BappekoRincianSubParameterPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = BappekoRincianSubParameterPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = BappekoRincianSubParameterPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getUnitId();
				break;
			case 1:
				return $this->getKegiatanCode();
				break;
			case 2:
				return $this->getFromSubKegiatan();
				break;
			case 3:
				return $this->getSubKegiatanName();
				break;
			case 4:
				return $this->getSubtitle();
				break;
			case 5:
				return $this->getDetailName();
				break;
			case 6:
				return $this->getNewSubtitle();
				break;
			case 7:
				return $this->getParam();
				break;
			case 8:
				return $this->getKecamatan();
				break;
			case 9:
				return $this->getMaxNilai();
				break;
			case 10:
				return $this->getKeterangan();
				break;
			case 11:
				return $this->getKetPembagi();
				break;
			case 12:
				return $this->getPembagi();
				break;
			case 13:
				return $this->getKodeSub();
				break;
			case 14:
				return $this->getTahun();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = BappekoRincianSubParameterPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getUnitId(),
			$keys[1] => $this->getKegiatanCode(),
			$keys[2] => $this->getFromSubKegiatan(),
			$keys[3] => $this->getSubKegiatanName(),
			$keys[4] => $this->getSubtitle(),
			$keys[5] => $this->getDetailName(),
			$keys[6] => $this->getNewSubtitle(),
			$keys[7] => $this->getParam(),
			$keys[8] => $this->getKecamatan(),
			$keys[9] => $this->getMaxNilai(),
			$keys[10] => $this->getKeterangan(),
			$keys[11] => $this->getKetPembagi(),
			$keys[12] => $this->getPembagi(),
			$keys[13] => $this->getKodeSub(),
			$keys[14] => $this->getTahun(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = BappekoRincianSubParameterPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setUnitId($value);
				break;
			case 1:
				$this->setKegiatanCode($value);
				break;
			case 2:
				$this->setFromSubKegiatan($value);
				break;
			case 3:
				$this->setSubKegiatanName($value);
				break;
			case 4:
				$this->setSubtitle($value);
				break;
			case 5:
				$this->setDetailName($value);
				break;
			case 6:
				$this->setNewSubtitle($value);
				break;
			case 7:
				$this->setParam($value);
				break;
			case 8:
				$this->setKecamatan($value);
				break;
			case 9:
				$this->setMaxNilai($value);
				break;
			case 10:
				$this->setKeterangan($value);
				break;
			case 11:
				$this->setKetPembagi($value);
				break;
			case 12:
				$this->setPembagi($value);
				break;
			case 13:
				$this->setKodeSub($value);
				break;
			case 14:
				$this->setTahun($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = BappekoRincianSubParameterPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setUnitId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setKegiatanCode($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setFromSubKegiatan($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setSubKegiatanName($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setSubtitle($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setDetailName($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setNewSubtitle($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setParam($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setKecamatan($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setMaxNilai($arr[$keys[9]]);
		if (array_key_exists($keys[10], $arr)) $this->setKeterangan($arr[$keys[10]]);
		if (array_key_exists($keys[11], $arr)) $this->setKetPembagi($arr[$keys[11]]);
		if (array_key_exists($keys[12], $arr)) $this->setPembagi($arr[$keys[12]]);
		if (array_key_exists($keys[13], $arr)) $this->setKodeSub($arr[$keys[13]]);
		if (array_key_exists($keys[14], $arr)) $this->setTahun($arr[$keys[14]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(BappekoRincianSubParameterPeer::DATABASE_NAME);

		if ($this->isColumnModified(BappekoRincianSubParameterPeer::UNIT_ID)) $criteria->add(BappekoRincianSubParameterPeer::UNIT_ID, $this->unit_id);
		if ($this->isColumnModified(BappekoRincianSubParameterPeer::KEGIATAN_CODE)) $criteria->add(BappekoRincianSubParameterPeer::KEGIATAN_CODE, $this->kegiatan_code);
		if ($this->isColumnModified(BappekoRincianSubParameterPeer::FROM_SUB_KEGIATAN)) $criteria->add(BappekoRincianSubParameterPeer::FROM_SUB_KEGIATAN, $this->from_sub_kegiatan);
		if ($this->isColumnModified(BappekoRincianSubParameterPeer::SUB_KEGIATAN_NAME)) $criteria->add(BappekoRincianSubParameterPeer::SUB_KEGIATAN_NAME, $this->sub_kegiatan_name);
		if ($this->isColumnModified(BappekoRincianSubParameterPeer::SUBTITLE)) $criteria->add(BappekoRincianSubParameterPeer::SUBTITLE, $this->subtitle);
		if ($this->isColumnModified(BappekoRincianSubParameterPeer::DETAIL_NAME)) $criteria->add(BappekoRincianSubParameterPeer::DETAIL_NAME, $this->detail_name);
		if ($this->isColumnModified(BappekoRincianSubParameterPeer::NEW_SUBTITLE)) $criteria->add(BappekoRincianSubParameterPeer::NEW_SUBTITLE, $this->new_subtitle);
		if ($this->isColumnModified(BappekoRincianSubParameterPeer::PARAM)) $criteria->add(BappekoRincianSubParameterPeer::PARAM, $this->param);
		if ($this->isColumnModified(BappekoRincianSubParameterPeer::KECAMATAN)) $criteria->add(BappekoRincianSubParameterPeer::KECAMATAN, $this->kecamatan);
		if ($this->isColumnModified(BappekoRincianSubParameterPeer::MAX_NILAI)) $criteria->add(BappekoRincianSubParameterPeer::MAX_NILAI, $this->max_nilai);
		if ($this->isColumnModified(BappekoRincianSubParameterPeer::KETERANGAN)) $criteria->add(BappekoRincianSubParameterPeer::KETERANGAN, $this->keterangan);
		if ($this->isColumnModified(BappekoRincianSubParameterPeer::KET_PEMBAGI)) $criteria->add(BappekoRincianSubParameterPeer::KET_PEMBAGI, $this->ket_pembagi);
		if ($this->isColumnModified(BappekoRincianSubParameterPeer::PEMBAGI)) $criteria->add(BappekoRincianSubParameterPeer::PEMBAGI, $this->pembagi);
		if ($this->isColumnModified(BappekoRincianSubParameterPeer::KODE_SUB)) $criteria->add(BappekoRincianSubParameterPeer::KODE_SUB, $this->kode_sub);
		if ($this->isColumnModified(BappekoRincianSubParameterPeer::TAHUN)) $criteria->add(BappekoRincianSubParameterPeer::TAHUN, $this->tahun);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(BappekoRincianSubParameterPeer::DATABASE_NAME);


		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return null;
	}

	
	 public function setPrimaryKey($pk)
	 {
		 	 }

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setUnitId($this->unit_id);

		$copyObj->setKegiatanCode($this->kegiatan_code);

		$copyObj->setFromSubKegiatan($this->from_sub_kegiatan);

		$copyObj->setSubKegiatanName($this->sub_kegiatan_name);

		$copyObj->setSubtitle($this->subtitle);

		$copyObj->setDetailName($this->detail_name);

		$copyObj->setNewSubtitle($this->new_subtitle);

		$copyObj->setParam($this->param);

		$copyObj->setKecamatan($this->kecamatan);

		$copyObj->setMaxNilai($this->max_nilai);

		$copyObj->setKeterangan($this->keterangan);

		$copyObj->setKetPembagi($this->ket_pembagi);

		$copyObj->setPembagi($this->pembagi);

		$copyObj->setKodeSub($this->kode_sub);

		$copyObj->setTahun($this->tahun);


		$copyObj->setNew(true);

	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new BappekoRincianSubParameterPeer();
		}
		return self::$peer;
	}

} 