<?php


abstract class BaseRincianPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.rincian';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.Rincian';

	
	const NUM_COLUMNS = 17;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const KEGIATAN_CODE = 'ebudget.rincian.KEGIATAN_CODE';

	
	const TIPE = 'ebudget.rincian.TIPE';

	
	const RINCIAN_CONFIRMED = 'ebudget.rincian.RINCIAN_CONFIRMED';

	
	const RINCIAN_CHANGED = 'ebudget.rincian.RINCIAN_CHANGED';

	
	const RINCIAN_SELESAI = 'ebudget.rincian.RINCIAN_SELESAI';

	
	const IP_ADDRESS = 'ebudget.rincian.IP_ADDRESS';

	
	const WAKTU_ACCESS = 'ebudget.rincian.WAKTU_ACCESS';

	
	const TARGET = 'ebudget.rincian.TARGET';

	
	const UNIT_ID = 'ebudget.rincian.UNIT_ID';

	
	const RINCIAN_LEVEL = 'ebudget.rincian.RINCIAN_LEVEL';

	
	const LOCK = 'ebudget.rincian.LOCK';

	
	const LAST_UPDATE_USER = 'ebudget.rincian.LAST_UPDATE_USER';

	
	const LAST_UPDATE_TIME = 'ebudget.rincian.LAST_UPDATE_TIME';

	
	const LAST_UPDATE_IP = 'ebudget.rincian.LAST_UPDATE_IP';

	
	const TAHAP = 'ebudget.rincian.TAHAP';

	
	const TAHUN = 'ebudget.rincian.TAHUN';

	
	const RINCIAN_REVISI_LEVEL = 'ebudget.rincian.RINCIAN_REVISI_LEVEL';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('KegiatanCode', 'Tipe', 'RincianConfirmed', 'RincianChanged', 'RincianSelesai', 'IpAddress', 'WaktuAccess', 'Target', 'UnitId', 'RincianLevel', 'Lock', 'LastUpdateUser', 'LastUpdateTime', 'LastUpdateIp', 'Tahap', 'Tahun', 'RincianRevisiLevel', ),
		BasePeer::TYPE_COLNAME => array (RincianPeer::KEGIATAN_CODE, RincianPeer::TIPE, RincianPeer::RINCIAN_CONFIRMED, RincianPeer::RINCIAN_CHANGED, RincianPeer::RINCIAN_SELESAI, RincianPeer::IP_ADDRESS, RincianPeer::WAKTU_ACCESS, RincianPeer::TARGET, RincianPeer::UNIT_ID, RincianPeer::RINCIAN_LEVEL, RincianPeer::LOCK, RincianPeer::LAST_UPDATE_USER, RincianPeer::LAST_UPDATE_TIME, RincianPeer::LAST_UPDATE_IP, RincianPeer::TAHAP, RincianPeer::TAHUN, RincianPeer::RINCIAN_REVISI_LEVEL, ),
		BasePeer::TYPE_FIELDNAME => array ('kegiatan_code', 'tipe', 'rincian_confirmed', 'rincian_changed', 'rincian_selesai', 'ip_address', 'waktu_access', 'target', 'unit_id', 'rincian_level', 'lock', 'last_update_user', 'last_update_time', 'last_update_ip', 'tahap', 'tahun', 'rincian_revisi_level', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('KegiatanCode' => 0, 'Tipe' => 1, 'RincianConfirmed' => 2, 'RincianChanged' => 3, 'RincianSelesai' => 4, 'IpAddress' => 5, 'WaktuAccess' => 6, 'Target' => 7, 'UnitId' => 8, 'RincianLevel' => 9, 'Lock' => 10, 'LastUpdateUser' => 11, 'LastUpdateTime' => 12, 'LastUpdateIp' => 13, 'Tahap' => 14, 'Tahun' => 15, 'RincianRevisiLevel' => 16, ),
		BasePeer::TYPE_COLNAME => array (RincianPeer::KEGIATAN_CODE => 0, RincianPeer::TIPE => 1, RincianPeer::RINCIAN_CONFIRMED => 2, RincianPeer::RINCIAN_CHANGED => 3, RincianPeer::RINCIAN_SELESAI => 4, RincianPeer::IP_ADDRESS => 5, RincianPeer::WAKTU_ACCESS => 6, RincianPeer::TARGET => 7, RincianPeer::UNIT_ID => 8, RincianPeer::RINCIAN_LEVEL => 9, RincianPeer::LOCK => 10, RincianPeer::LAST_UPDATE_USER => 11, RincianPeer::LAST_UPDATE_TIME => 12, RincianPeer::LAST_UPDATE_IP => 13, RincianPeer::TAHAP => 14, RincianPeer::TAHUN => 15, RincianPeer::RINCIAN_REVISI_LEVEL => 16, ),
		BasePeer::TYPE_FIELDNAME => array ('kegiatan_code' => 0, 'tipe' => 1, 'rincian_confirmed' => 2, 'rincian_changed' => 3, 'rincian_selesai' => 4, 'ip_address' => 5, 'waktu_access' => 6, 'target' => 7, 'unit_id' => 8, 'rincian_level' => 9, 'lock' => 10, 'last_update_user' => 11, 'last_update_time' => 12, 'last_update_ip' => 13, 'tahap' => 14, 'tahun' => 15, 'rincian_revisi_level' => 16, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/RincianMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.RincianMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = RincianPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(RincianPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(RincianPeer::KEGIATAN_CODE);

		$criteria->addSelectColumn(RincianPeer::TIPE);

		$criteria->addSelectColumn(RincianPeer::RINCIAN_CONFIRMED);

		$criteria->addSelectColumn(RincianPeer::RINCIAN_CHANGED);

		$criteria->addSelectColumn(RincianPeer::RINCIAN_SELESAI);

		$criteria->addSelectColumn(RincianPeer::IP_ADDRESS);

		$criteria->addSelectColumn(RincianPeer::WAKTU_ACCESS);

		$criteria->addSelectColumn(RincianPeer::TARGET);

		$criteria->addSelectColumn(RincianPeer::UNIT_ID);

		$criteria->addSelectColumn(RincianPeer::RINCIAN_LEVEL);

		$criteria->addSelectColumn(RincianPeer::LOCK);

		$criteria->addSelectColumn(RincianPeer::LAST_UPDATE_USER);

		$criteria->addSelectColumn(RincianPeer::LAST_UPDATE_TIME);

		$criteria->addSelectColumn(RincianPeer::LAST_UPDATE_IP);

		$criteria->addSelectColumn(RincianPeer::TAHAP);

		$criteria->addSelectColumn(RincianPeer::TAHUN);

		$criteria->addSelectColumn(RincianPeer::RINCIAN_REVISI_LEVEL);

	}

	const COUNT = 'COUNT(ebudget.rincian.KEGIATAN_CODE)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.rincian.KEGIATAN_CODE)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(RincianPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(RincianPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = RincianPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = RincianPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return RincianPeer::populateObjects(RincianPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			RincianPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = RincianPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return RincianPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(RincianPeer::KEGIATAN_CODE);
			$selectCriteria->add(RincianPeer::KEGIATAN_CODE, $criteria->remove(RincianPeer::KEGIATAN_CODE), $comparison);

			$comparison = $criteria->getComparison(RincianPeer::TIPE);
			$selectCriteria->add(RincianPeer::TIPE, $criteria->remove(RincianPeer::TIPE), $comparison);

			$comparison = $criteria->getComparison(RincianPeer::UNIT_ID);
			$selectCriteria->add(RincianPeer::UNIT_ID, $criteria->remove(RincianPeer::UNIT_ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(RincianPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(RincianPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof Rincian) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
												if(count($values) == count($values, COUNT_RECURSIVE))
			{
								$values = array($values);
			}
			$vals = array();
			foreach($values as $value)
			{

				$vals[0][] = $value[0];
				$vals[1][] = $value[1];
				$vals[2][] = $value[2];
			}

			$criteria->add(RincianPeer::KEGIATAN_CODE, $vals[0], Criteria::IN);
			$criteria->add(RincianPeer::TIPE, $vals[1], Criteria::IN);
			$criteria->add(RincianPeer::UNIT_ID, $vals[2], Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(Rincian $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(RincianPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(RincianPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(RincianPeer::DATABASE_NAME, RincianPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = RincianPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK( $kegiatan_code, $tipe, $unit_id, $con = null) {
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$criteria = new Criteria();
		$criteria->add(RincianPeer::KEGIATAN_CODE, $kegiatan_code);
		$criteria->add(RincianPeer::TIPE, $tipe);
		$criteria->add(RincianPeer::UNIT_ID, $unit_id);
		$v = RincianPeer::doSelect($criteria, $con);

		return !empty($v) ? $v[0] : null;
	}
} 
if (Propel::isInit()) {
			try {
		BaseRincianPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/RincianMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.RincianMapBuilder');
}
