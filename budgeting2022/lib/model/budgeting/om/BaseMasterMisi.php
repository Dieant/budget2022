<?php


abstract class BaseMasterMisi extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $kode_misi;


	
	protected $nama_misi;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getKodeMisi()
	{

		return $this->kode_misi;
	}

	
	public function getNamaMisi()
	{

		return $this->nama_misi;
	}

	
	public function setKodeMisi($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_misi !== $v) {
			$this->kode_misi = $v;
			$this->modifiedColumns[] = MasterMisiPeer::KODE_MISI;
		}

	} 
	
	public function setNamaMisi($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nama_misi !== $v) {
			$this->nama_misi = $v;
			$this->modifiedColumns[] = MasterMisiPeer::NAMA_MISI;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->kode_misi = $rs->getString($startcol + 0);

			$this->nama_misi = $rs->getString($startcol + 1);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 2; 
		} catch (Exception $e) {
			throw new PropelException("Error populating MasterMisi object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(MasterMisiPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			MasterMisiPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(MasterMisiPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = MasterMisiPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += MasterMisiPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = MasterMisiPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = MasterMisiPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getKodeMisi();
				break;
			case 1:
				return $this->getNamaMisi();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = MasterMisiPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getKodeMisi(),
			$keys[1] => $this->getNamaMisi(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = MasterMisiPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setKodeMisi($value);
				break;
			case 1:
				$this->setNamaMisi($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = MasterMisiPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setKodeMisi($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setNamaMisi($arr[$keys[1]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(MasterMisiPeer::DATABASE_NAME);

		if ($this->isColumnModified(MasterMisiPeer::KODE_MISI)) $criteria->add(MasterMisiPeer::KODE_MISI, $this->kode_misi);
		if ($this->isColumnModified(MasterMisiPeer::NAMA_MISI)) $criteria->add(MasterMisiPeer::NAMA_MISI, $this->nama_misi);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(MasterMisiPeer::DATABASE_NAME);

		$criteria->add(MasterMisiPeer::KODE_MISI, $this->kode_misi);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getKodeMisi();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setKodeMisi($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setNamaMisi($this->nama_misi);


		$copyObj->setNew(true);

		$copyObj->setKodeMisi(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new MasterMisiPeer();
		}
		return self::$peer;
	}

} 