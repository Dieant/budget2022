<?php


abstract class BaseAsbFisikPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.asb_fisik';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.AsbFisik';

	
	const NUM_COLUMNS = 20;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const KOMPONEN_ID = 'ebudget.asb_fisik.KOMPONEN_ID';

	
	const SATUAN = 'ebudget.asb_fisik.SATUAN';

	
	const KOMPONEN_NAME = 'ebudget.asb_fisik.KOMPONEN_NAME';

	
	const SHSD_ID = 'ebudget.asb_fisik.SHSD_ID';

	
	const KOMPONEN_HARGA = 'ebudget.asb_fisik.KOMPONEN_HARGA';

	
	const KOMPONEN_SHOW = 'ebudget.asb_fisik.KOMPONEN_SHOW';

	
	const IP_ADDRESS = 'ebudget.asb_fisik.IP_ADDRESS';

	
	const WAKTU_ACCESS = 'ebudget.asb_fisik.WAKTU_ACCESS';

	
	const KOMPONEN_TIPE = 'ebudget.asb_fisik.KOMPONEN_TIPE';

	
	const KOMPONEN_CONFIRMED = 'ebudget.asb_fisik.KOMPONEN_CONFIRMED';

	
	const KOMPONEN_NON_PAJAK = 'ebudget.asb_fisik.KOMPONEN_NON_PAJAK';

	
	const USER_ID = 'ebudget.asb_fisik.USER_ID';

	
	const REKENING = 'ebudget.asb_fisik.REKENING';

	
	const KELOMPOK = 'ebudget.asb_fisik.KELOMPOK';

	
	const PEMELIHARAAN = 'ebudget.asb_fisik.PEMELIHARAAN';

	
	const REK_UPAH = 'ebudget.asb_fisik.REK_UPAH';

	
	const REK_BAHAN = 'ebudget.asb_fisik.REK_BAHAN';

	
	const REK_SEWA = 'ebudget.asb_fisik.REK_SEWA';

	
	const DESKRIPSI = 'ebudget.asb_fisik.DESKRIPSI';

	
	const ASB_LOCKED = 'ebudget.asb_fisik.ASB_LOCKED';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('KomponenId', 'Satuan', 'KomponenName', 'ShsdId', 'KomponenHarga', 'KomponenShow', 'IpAddress', 'WaktuAccess', 'KomponenTipe', 'KomponenConfirmed', 'KomponenNonPajak', 'UserId', 'Rekening', 'Kelompok', 'Pemeliharaan', 'RekUpah', 'RekBahan', 'RekSewa', 'Deskripsi', 'AsbLocked', ),
		BasePeer::TYPE_COLNAME => array (AsbFisikPeer::KOMPONEN_ID, AsbFisikPeer::SATUAN, AsbFisikPeer::KOMPONEN_NAME, AsbFisikPeer::SHSD_ID, AsbFisikPeer::KOMPONEN_HARGA, AsbFisikPeer::KOMPONEN_SHOW, AsbFisikPeer::IP_ADDRESS, AsbFisikPeer::WAKTU_ACCESS, AsbFisikPeer::KOMPONEN_TIPE, AsbFisikPeer::KOMPONEN_CONFIRMED, AsbFisikPeer::KOMPONEN_NON_PAJAK, AsbFisikPeer::USER_ID, AsbFisikPeer::REKENING, AsbFisikPeer::KELOMPOK, AsbFisikPeer::PEMELIHARAAN, AsbFisikPeer::REK_UPAH, AsbFisikPeer::REK_BAHAN, AsbFisikPeer::REK_SEWA, AsbFisikPeer::DESKRIPSI, AsbFisikPeer::ASB_LOCKED, ),
		BasePeer::TYPE_FIELDNAME => array ('komponen_id', 'satuan', 'komponen_name', 'shsd_id', 'komponen_harga', 'komponen_show', 'ip_address', 'waktu_access', 'komponen_tipe', 'komponen_confirmed', 'komponen_non_pajak', 'user_id', 'rekening', 'kelompok', 'pemeliharaan', 'rek_upah', 'rek_bahan', 'rek_sewa', 'deskripsi', 'asb_locked', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('KomponenId' => 0, 'Satuan' => 1, 'KomponenName' => 2, 'ShsdId' => 3, 'KomponenHarga' => 4, 'KomponenShow' => 5, 'IpAddress' => 6, 'WaktuAccess' => 7, 'KomponenTipe' => 8, 'KomponenConfirmed' => 9, 'KomponenNonPajak' => 10, 'UserId' => 11, 'Rekening' => 12, 'Kelompok' => 13, 'Pemeliharaan' => 14, 'RekUpah' => 15, 'RekBahan' => 16, 'RekSewa' => 17, 'Deskripsi' => 18, 'AsbLocked' => 19, ),
		BasePeer::TYPE_COLNAME => array (AsbFisikPeer::KOMPONEN_ID => 0, AsbFisikPeer::SATUAN => 1, AsbFisikPeer::KOMPONEN_NAME => 2, AsbFisikPeer::SHSD_ID => 3, AsbFisikPeer::KOMPONEN_HARGA => 4, AsbFisikPeer::KOMPONEN_SHOW => 5, AsbFisikPeer::IP_ADDRESS => 6, AsbFisikPeer::WAKTU_ACCESS => 7, AsbFisikPeer::KOMPONEN_TIPE => 8, AsbFisikPeer::KOMPONEN_CONFIRMED => 9, AsbFisikPeer::KOMPONEN_NON_PAJAK => 10, AsbFisikPeer::USER_ID => 11, AsbFisikPeer::REKENING => 12, AsbFisikPeer::KELOMPOK => 13, AsbFisikPeer::PEMELIHARAAN => 14, AsbFisikPeer::REK_UPAH => 15, AsbFisikPeer::REK_BAHAN => 16, AsbFisikPeer::REK_SEWA => 17, AsbFisikPeer::DESKRIPSI => 18, AsbFisikPeer::ASB_LOCKED => 19, ),
		BasePeer::TYPE_FIELDNAME => array ('komponen_id' => 0, 'satuan' => 1, 'komponen_name' => 2, 'shsd_id' => 3, 'komponen_harga' => 4, 'komponen_show' => 5, 'ip_address' => 6, 'waktu_access' => 7, 'komponen_tipe' => 8, 'komponen_confirmed' => 9, 'komponen_non_pajak' => 10, 'user_id' => 11, 'rekening' => 12, 'kelompok' => 13, 'pemeliharaan' => 14, 'rek_upah' => 15, 'rek_bahan' => 16, 'rek_sewa' => 17, 'deskripsi' => 18, 'asb_locked' => 19, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/AsbFisikMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.AsbFisikMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = AsbFisikPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(AsbFisikPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(AsbFisikPeer::KOMPONEN_ID);

		$criteria->addSelectColumn(AsbFisikPeer::SATUAN);

		$criteria->addSelectColumn(AsbFisikPeer::KOMPONEN_NAME);

		$criteria->addSelectColumn(AsbFisikPeer::SHSD_ID);

		$criteria->addSelectColumn(AsbFisikPeer::KOMPONEN_HARGA);

		$criteria->addSelectColumn(AsbFisikPeer::KOMPONEN_SHOW);

		$criteria->addSelectColumn(AsbFisikPeer::IP_ADDRESS);

		$criteria->addSelectColumn(AsbFisikPeer::WAKTU_ACCESS);

		$criteria->addSelectColumn(AsbFisikPeer::KOMPONEN_TIPE);

		$criteria->addSelectColumn(AsbFisikPeer::KOMPONEN_CONFIRMED);

		$criteria->addSelectColumn(AsbFisikPeer::KOMPONEN_NON_PAJAK);

		$criteria->addSelectColumn(AsbFisikPeer::USER_ID);

		$criteria->addSelectColumn(AsbFisikPeer::REKENING);

		$criteria->addSelectColumn(AsbFisikPeer::KELOMPOK);

		$criteria->addSelectColumn(AsbFisikPeer::PEMELIHARAAN);

		$criteria->addSelectColumn(AsbFisikPeer::REK_UPAH);

		$criteria->addSelectColumn(AsbFisikPeer::REK_BAHAN);

		$criteria->addSelectColumn(AsbFisikPeer::REK_SEWA);

		$criteria->addSelectColumn(AsbFisikPeer::DESKRIPSI);

		$criteria->addSelectColumn(AsbFisikPeer::ASB_LOCKED);

	}

	const COUNT = 'COUNT(ebudget.asb_fisik.KOMPONEN_ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.asb_fisik.KOMPONEN_ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(AsbFisikPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(AsbFisikPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = AsbFisikPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = AsbFisikPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return AsbFisikPeer::populateObjects(AsbFisikPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			AsbFisikPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = AsbFisikPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return AsbFisikPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(AsbFisikPeer::KOMPONEN_ID);
			$selectCriteria->add(AsbFisikPeer::KOMPONEN_ID, $criteria->remove(AsbFisikPeer::KOMPONEN_ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(AsbFisikPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(AsbFisikPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof AsbFisik) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(AsbFisikPeer::KOMPONEN_ID, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(AsbFisik $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(AsbFisikPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(AsbFisikPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(AsbFisikPeer::DATABASE_NAME, AsbFisikPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = AsbFisikPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(AsbFisikPeer::DATABASE_NAME);

		$criteria->add(AsbFisikPeer::KOMPONEN_ID, $pk);


		$v = AsbFisikPeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(AsbFisikPeer::KOMPONEN_ID, $pks, Criteria::IN);
			$objs = AsbFisikPeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BaseAsbFisikPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/AsbFisikMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.AsbFisikMapBuilder');
}
