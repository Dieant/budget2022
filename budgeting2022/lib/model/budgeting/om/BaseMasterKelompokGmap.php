<?php


abstract class BaseMasterKelompokGmap extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id_kelompok;


	
	protected $kode_kelompok;


	
	protected $nama_objek;


	
	protected $tipe_objek;


	
	protected $warna;


	
	protected $rekening;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getIdKelompok()
	{

		return $this->id_kelompok;
	}

	
	public function getKodeKelompok()
	{

		return $this->kode_kelompok;
	}

	
	public function getNamaObjek()
	{

		return $this->nama_objek;
	}

	
	public function getTipeObjek()
	{

		return $this->tipe_objek;
	}

	
	public function getWarna()
	{

		return $this->warna;
	}

	
	public function getRekening()
	{

		return $this->rekening;
	}

	
	public function setIdKelompok($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id_kelompok !== $v) {
			$this->id_kelompok = $v;
			$this->modifiedColumns[] = MasterKelompokGmapPeer::ID_KELOMPOK;
		}

	} 
	
	public function setKodeKelompok($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_kelompok !== $v) {
			$this->kode_kelompok = $v;
			$this->modifiedColumns[] = MasterKelompokGmapPeer::KODE_KELOMPOK;
		}

	} 
	
	public function setNamaObjek($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nama_objek !== $v) {
			$this->nama_objek = $v;
			$this->modifiedColumns[] = MasterKelompokGmapPeer::NAMA_OBJEK;
		}

	} 
	
	public function setTipeObjek($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->tipe_objek !== $v) {
			$this->tipe_objek = $v;
			$this->modifiedColumns[] = MasterKelompokGmapPeer::TIPE_OBJEK;
		}

	} 
	
	public function setWarna($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->warna !== $v) {
			$this->warna = $v;
			$this->modifiedColumns[] = MasterKelompokGmapPeer::WARNA;
		}

	} 
	
	public function setRekening($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->rekening !== $v) {
			$this->rekening = $v;
			$this->modifiedColumns[] = MasterKelompokGmapPeer::REKENING;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id_kelompok = $rs->getInt($startcol + 0);

			$this->kode_kelompok = $rs->getString($startcol + 1);

			$this->nama_objek = $rs->getString($startcol + 2);

			$this->tipe_objek = $rs->getInt($startcol + 3);

			$this->warna = $rs->getString($startcol + 4);

			$this->rekening = $rs->getString($startcol + 5);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 6; 
		} catch (Exception $e) {
			throw new PropelException("Error populating MasterKelompokGmap object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(MasterKelompokGmapPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			MasterKelompokGmapPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(MasterKelompokGmapPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = MasterKelompokGmapPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += MasterKelompokGmapPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = MasterKelompokGmapPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = MasterKelompokGmapPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getIdKelompok();
				break;
			case 1:
				return $this->getKodeKelompok();
				break;
			case 2:
				return $this->getNamaObjek();
				break;
			case 3:
				return $this->getTipeObjek();
				break;
			case 4:
				return $this->getWarna();
				break;
			case 5:
				return $this->getRekening();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = MasterKelompokGmapPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getIdKelompok(),
			$keys[1] => $this->getKodeKelompok(),
			$keys[2] => $this->getNamaObjek(),
			$keys[3] => $this->getTipeObjek(),
			$keys[4] => $this->getWarna(),
			$keys[5] => $this->getRekening(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = MasterKelompokGmapPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setIdKelompok($value);
				break;
			case 1:
				$this->setKodeKelompok($value);
				break;
			case 2:
				$this->setNamaObjek($value);
				break;
			case 3:
				$this->setTipeObjek($value);
				break;
			case 4:
				$this->setWarna($value);
				break;
			case 5:
				$this->setRekening($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = MasterKelompokGmapPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setIdKelompok($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setKodeKelompok($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setNamaObjek($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setTipeObjek($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setWarna($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setRekening($arr[$keys[5]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(MasterKelompokGmapPeer::DATABASE_NAME);

		if ($this->isColumnModified(MasterKelompokGmapPeer::ID_KELOMPOK)) $criteria->add(MasterKelompokGmapPeer::ID_KELOMPOK, $this->id_kelompok);
		if ($this->isColumnModified(MasterKelompokGmapPeer::KODE_KELOMPOK)) $criteria->add(MasterKelompokGmapPeer::KODE_KELOMPOK, $this->kode_kelompok);
		if ($this->isColumnModified(MasterKelompokGmapPeer::NAMA_OBJEK)) $criteria->add(MasterKelompokGmapPeer::NAMA_OBJEK, $this->nama_objek);
		if ($this->isColumnModified(MasterKelompokGmapPeer::TIPE_OBJEK)) $criteria->add(MasterKelompokGmapPeer::TIPE_OBJEK, $this->tipe_objek);
		if ($this->isColumnModified(MasterKelompokGmapPeer::WARNA)) $criteria->add(MasterKelompokGmapPeer::WARNA, $this->warna);
		if ($this->isColumnModified(MasterKelompokGmapPeer::REKENING)) $criteria->add(MasterKelompokGmapPeer::REKENING, $this->rekening);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(MasterKelompokGmapPeer::DATABASE_NAME);

		$criteria->add(MasterKelompokGmapPeer::ID_KELOMPOK, $this->id_kelompok);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getIdKelompok();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setIdKelompok($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setKodeKelompok($this->kode_kelompok);

		$copyObj->setNamaObjek($this->nama_objek);

		$copyObj->setTipeObjek($this->tipe_objek);

		$copyObj->setWarna($this->warna);

		$copyObj->setRekening($this->rekening);


		$copyObj->setNew(true);

		$copyObj->setIdKelompok(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new MasterKelompokGmapPeer();
		}
		return self::$peer;
	}

} 