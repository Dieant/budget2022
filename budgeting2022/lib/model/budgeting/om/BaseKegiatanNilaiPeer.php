<?php


abstract class BaseKegiatanNilaiPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.kegiatan_nilai';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.KegiatanNilai';

	
	const NUM_COLUMNS = 8;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const UNIT_ID = 'ebudget.kegiatan_nilai.UNIT_ID';

	
	const NILAI_1 = 'ebudget.kegiatan_nilai.NILAI_1';

	
	const NILAI_2 = 'ebudget.kegiatan_nilai.NILAI_2';

	
	const NILAI_3 = 'ebudget.kegiatan_nilai.NILAI_3';

	
	const TOTAL = 'ebudget.kegiatan_nilai.TOTAL';

	
	const KETERANGAN = 'ebudget.kegiatan_nilai.KETERANGAN';

	
	const KEGIATAN_CODE = 'ebudget.kegiatan_nilai.KEGIATAN_CODE';

	
	const PRIORITAS = 'ebudget.kegiatan_nilai.PRIORITAS';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('UnitId', 'Nilai1', 'Nilai2', 'Nilai3', 'Total', 'Keterangan', 'KegiatanCode', 'Prioritas', ),
		BasePeer::TYPE_COLNAME => array (KegiatanNilaiPeer::UNIT_ID, KegiatanNilaiPeer::NILAI_1, KegiatanNilaiPeer::NILAI_2, KegiatanNilaiPeer::NILAI_3, KegiatanNilaiPeer::TOTAL, KegiatanNilaiPeer::KETERANGAN, KegiatanNilaiPeer::KEGIATAN_CODE, KegiatanNilaiPeer::PRIORITAS, ),
		BasePeer::TYPE_FIELDNAME => array ('unit_id', 'nilai_1', 'nilai_2', 'nilai_3', 'total', 'keterangan', 'kegiatan_code', 'prioritas', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('UnitId' => 0, 'Nilai1' => 1, 'Nilai2' => 2, 'Nilai3' => 3, 'Total' => 4, 'Keterangan' => 5, 'KegiatanCode' => 6, 'Prioritas' => 7, ),
		BasePeer::TYPE_COLNAME => array (KegiatanNilaiPeer::UNIT_ID => 0, KegiatanNilaiPeer::NILAI_1 => 1, KegiatanNilaiPeer::NILAI_2 => 2, KegiatanNilaiPeer::NILAI_3 => 3, KegiatanNilaiPeer::TOTAL => 4, KegiatanNilaiPeer::KETERANGAN => 5, KegiatanNilaiPeer::KEGIATAN_CODE => 6, KegiatanNilaiPeer::PRIORITAS => 7, ),
		BasePeer::TYPE_FIELDNAME => array ('unit_id' => 0, 'nilai_1' => 1, 'nilai_2' => 2, 'nilai_3' => 3, 'total' => 4, 'keterangan' => 5, 'kegiatan_code' => 6, 'prioritas' => 7, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/KegiatanNilaiMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.KegiatanNilaiMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = KegiatanNilaiPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(KegiatanNilaiPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(KegiatanNilaiPeer::UNIT_ID);

		$criteria->addSelectColumn(KegiatanNilaiPeer::NILAI_1);

		$criteria->addSelectColumn(KegiatanNilaiPeer::NILAI_2);

		$criteria->addSelectColumn(KegiatanNilaiPeer::NILAI_3);

		$criteria->addSelectColumn(KegiatanNilaiPeer::TOTAL);

		$criteria->addSelectColumn(KegiatanNilaiPeer::KETERANGAN);

		$criteria->addSelectColumn(KegiatanNilaiPeer::KEGIATAN_CODE);

		$criteria->addSelectColumn(KegiatanNilaiPeer::PRIORITAS);

	}

	const COUNT = 'COUNT(ebudget.kegiatan_nilai.UNIT_ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.kegiatan_nilai.UNIT_ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(KegiatanNilaiPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(KegiatanNilaiPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = KegiatanNilaiPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = KegiatanNilaiPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return KegiatanNilaiPeer::populateObjects(KegiatanNilaiPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			KegiatanNilaiPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = KegiatanNilaiPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return KegiatanNilaiPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(KegiatanNilaiPeer::UNIT_ID);
			$selectCriteria->add(KegiatanNilaiPeer::UNIT_ID, $criteria->remove(KegiatanNilaiPeer::UNIT_ID), $comparison);

			$comparison = $criteria->getComparison(KegiatanNilaiPeer::KEGIATAN_CODE);
			$selectCriteria->add(KegiatanNilaiPeer::KEGIATAN_CODE, $criteria->remove(KegiatanNilaiPeer::KEGIATAN_CODE), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(KegiatanNilaiPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(KegiatanNilaiPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof KegiatanNilai) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
												if(count($values) == count($values, COUNT_RECURSIVE))
			{
								$values = array($values);
			}
			$vals = array();
			foreach($values as $value)
			{

				$vals[0][] = $value[0];
				$vals[1][] = $value[1];
			}

			$criteria->add(KegiatanNilaiPeer::UNIT_ID, $vals[0], Criteria::IN);
			$criteria->add(KegiatanNilaiPeer::KEGIATAN_CODE, $vals[1], Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(KegiatanNilai $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(KegiatanNilaiPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(KegiatanNilaiPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(KegiatanNilaiPeer::DATABASE_NAME, KegiatanNilaiPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = KegiatanNilaiPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK( $unit_id, $kegiatan_code, $con = null) {
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$criteria = new Criteria();
		$criteria->add(KegiatanNilaiPeer::UNIT_ID, $unit_id);
		$criteria->add(KegiatanNilaiPeer::KEGIATAN_CODE, $kegiatan_code);
		$v = KegiatanNilaiPeer::doSelect($criteria, $con);

		return !empty($v) ? $v[0] : null;
	}
} 
if (Propel::isInit()) {
			try {
		BaseKegiatanNilaiPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/KegiatanNilaiMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.KegiatanNilaiMapBuilder');
}
