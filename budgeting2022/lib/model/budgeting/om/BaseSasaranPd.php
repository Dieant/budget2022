<?php


abstract class BaseSasaranPd extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id;


	
	protected $id_sasaran;


	
	protected $sasaran_pd;


	
	protected $unit_id;


	
	protected $kode_kegiatan;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getId()
	{

		return $this->id;
	}

	
	public function getIdSasaran()
	{

		return $this->id_sasaran;
	}

	
	public function getSasaranPd()
	{

		return $this->sasaran_pd;
	}

	
	public function getUnitId()
	{

		return $this->unit_id;
	}

	
	public function getKodeKegiatan()
	{

		return $this->kode_kegiatan;
	}

	
	public function setId($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = SasaranPdPeer::ID;
		}

	} 
	
	public function setIdSasaran($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->id_sasaran !== $v) {
			$this->id_sasaran = $v;
			$this->modifiedColumns[] = SasaranPdPeer::ID_SASARAN;
		}

	} 
	
	public function setSasaranPd($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->sasaran_pd !== $v) {
			$this->sasaran_pd = $v;
			$this->modifiedColumns[] = SasaranPdPeer::SASARAN_PD;
		}

	} 
	
	public function setUnitId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->unit_id !== $v) {
			$this->unit_id = $v;
			$this->modifiedColumns[] = SasaranPdPeer::UNIT_ID;
		}

	} 
	
	public function setKodeKegiatan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_kegiatan !== $v) {
			$this->kode_kegiatan = $v;
			$this->modifiedColumns[] = SasaranPdPeer::KODE_KEGIATAN;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getInt($startcol + 0);

			$this->id_sasaran = $rs->getString($startcol + 1);

			$this->sasaran_pd = $rs->getString($startcol + 2);

			$this->unit_id = $rs->getString($startcol + 3);

			$this->kode_kegiatan = $rs->getString($startcol + 4);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 5; 
		} catch (Exception $e) {
			throw new PropelException("Error populating SasaranPd object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(SasaranPdPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			SasaranPdPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(SasaranPdPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = SasaranPdPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += SasaranPdPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = SasaranPdPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = SasaranPdPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getIdSasaran();
				break;
			case 2:
				return $this->getSasaranPd();
				break;
			case 3:
				return $this->getUnitId();
				break;
			case 4:
				return $this->getKodeKegiatan();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = SasaranPdPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getIdSasaran(),
			$keys[2] => $this->getSasaranPd(),
			$keys[3] => $this->getUnitId(),
			$keys[4] => $this->getKodeKegiatan(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = SasaranPdPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setIdSasaran($value);
				break;
			case 2:
				$this->setSasaranPd($value);
				break;
			case 3:
				$this->setUnitId($value);
				break;
			case 4:
				$this->setKodeKegiatan($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = SasaranPdPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setIdSasaran($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setSasaranPd($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setUnitId($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setKodeKegiatan($arr[$keys[4]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(SasaranPdPeer::DATABASE_NAME);

		if ($this->isColumnModified(SasaranPdPeer::ID)) $criteria->add(SasaranPdPeer::ID, $this->id);
		if ($this->isColumnModified(SasaranPdPeer::ID_SASARAN)) $criteria->add(SasaranPdPeer::ID_SASARAN, $this->id_sasaran);
		if ($this->isColumnModified(SasaranPdPeer::SASARAN_PD)) $criteria->add(SasaranPdPeer::SASARAN_PD, $this->sasaran_pd);
		if ($this->isColumnModified(SasaranPdPeer::UNIT_ID)) $criteria->add(SasaranPdPeer::UNIT_ID, $this->unit_id);
		if ($this->isColumnModified(SasaranPdPeer::KODE_KEGIATAN)) $criteria->add(SasaranPdPeer::KODE_KEGIATAN, $this->kode_kegiatan);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(SasaranPdPeer::DATABASE_NAME);

		$criteria->add(SasaranPdPeer::ID, $this->id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setIdSasaran($this->id_sasaran);

		$copyObj->setSasaranPd($this->sasaran_pd);

		$copyObj->setUnitId($this->unit_id);

		$copyObj->setKodeKegiatan($this->kode_kegiatan);


		$copyObj->setNew(true);

		$copyObj->setId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new SasaranPdPeer();
		}
		return self::$peer;
	}

} 