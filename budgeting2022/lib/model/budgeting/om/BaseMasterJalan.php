<?php


abstract class BaseMasterJalan extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $kode;


	
	protected $nama_jalan;


	
	protected $nama_ujung;


	
	protected $nama_pangkal;


	
	protected $kecamatan;


	
	protected $kelurahan;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getKode()
	{

		return $this->kode;
	}

	
	public function getNamaJalan()
	{

		return $this->nama_jalan;
	}

	
	public function getNamaUjung()
	{

		return $this->nama_ujung;
	}

	
	public function getNamaPangkal()
	{

		return $this->nama_pangkal;
	}

	
	public function getKecamatan()
	{

		return $this->kecamatan;
	}

	
	public function getKelurahan()
	{

		return $this->kelurahan;
	}

	
	public function setKode($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode !== $v) {
			$this->kode = $v;
			$this->modifiedColumns[] = MasterJalanPeer::KODE;
		}

	} 
	
	public function setNamaJalan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nama_jalan !== $v) {
			$this->nama_jalan = $v;
			$this->modifiedColumns[] = MasterJalanPeer::NAMA_JALAN;
		}

	} 
	
	public function setNamaUjung($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nama_ujung !== $v) {
			$this->nama_ujung = $v;
			$this->modifiedColumns[] = MasterJalanPeer::NAMA_UJUNG;
		}

	} 
	
	public function setNamaPangkal($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nama_pangkal !== $v) {
			$this->nama_pangkal = $v;
			$this->modifiedColumns[] = MasterJalanPeer::NAMA_PANGKAL;
		}

	} 
	
	public function setKecamatan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kecamatan !== $v) {
			$this->kecamatan = $v;
			$this->modifiedColumns[] = MasterJalanPeer::KECAMATAN;
		}

	} 
	
	public function setKelurahan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kelurahan !== $v) {
			$this->kelurahan = $v;
			$this->modifiedColumns[] = MasterJalanPeer::KELURAHAN;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->kode = $rs->getString($startcol + 0);

			$this->nama_jalan = $rs->getString($startcol + 1);

			$this->nama_ujung = $rs->getString($startcol + 2);

			$this->nama_pangkal = $rs->getString($startcol + 3);

			$this->kecamatan = $rs->getString($startcol + 4);

			$this->kelurahan = $rs->getString($startcol + 5);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 6; 
		} catch (Exception $e) {
			throw new PropelException("Error populating MasterJalan object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(MasterJalanPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			MasterJalanPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(MasterJalanPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = MasterJalanPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += MasterJalanPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = MasterJalanPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = MasterJalanPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getKode();
				break;
			case 1:
				return $this->getNamaJalan();
				break;
			case 2:
				return $this->getNamaUjung();
				break;
			case 3:
				return $this->getNamaPangkal();
				break;
			case 4:
				return $this->getKecamatan();
				break;
			case 5:
				return $this->getKelurahan();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = MasterJalanPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getKode(),
			$keys[1] => $this->getNamaJalan(),
			$keys[2] => $this->getNamaUjung(),
			$keys[3] => $this->getNamaPangkal(),
			$keys[4] => $this->getKecamatan(),
			$keys[5] => $this->getKelurahan(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = MasterJalanPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setKode($value);
				break;
			case 1:
				$this->setNamaJalan($value);
				break;
			case 2:
				$this->setNamaUjung($value);
				break;
			case 3:
				$this->setNamaPangkal($value);
				break;
			case 4:
				$this->setKecamatan($value);
				break;
			case 5:
				$this->setKelurahan($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = MasterJalanPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setKode($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setNamaJalan($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setNamaUjung($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setNamaPangkal($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setKecamatan($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setKelurahan($arr[$keys[5]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(MasterJalanPeer::DATABASE_NAME);

		if ($this->isColumnModified(MasterJalanPeer::KODE)) $criteria->add(MasterJalanPeer::KODE, $this->kode);
		if ($this->isColumnModified(MasterJalanPeer::NAMA_JALAN)) $criteria->add(MasterJalanPeer::NAMA_JALAN, $this->nama_jalan);
		if ($this->isColumnModified(MasterJalanPeer::NAMA_UJUNG)) $criteria->add(MasterJalanPeer::NAMA_UJUNG, $this->nama_ujung);
		if ($this->isColumnModified(MasterJalanPeer::NAMA_PANGKAL)) $criteria->add(MasterJalanPeer::NAMA_PANGKAL, $this->nama_pangkal);
		if ($this->isColumnModified(MasterJalanPeer::KECAMATAN)) $criteria->add(MasterJalanPeer::KECAMATAN, $this->kecamatan);
		if ($this->isColumnModified(MasterJalanPeer::KELURAHAN)) $criteria->add(MasterJalanPeer::KELURAHAN, $this->kelurahan);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(MasterJalanPeer::DATABASE_NAME);

		$criteria->add(MasterJalanPeer::KODE, $this->kode);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getKode();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setKode($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setNamaJalan($this->nama_jalan);

		$copyObj->setNamaUjung($this->nama_ujung);

		$copyObj->setNamaPangkal($this->nama_pangkal);

		$copyObj->setKecamatan($this->kecamatan);

		$copyObj->setKelurahan($this->kelurahan);


		$copyObj->setNew(true);

		$copyObj->setKode(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new MasterJalanPeer();
		}
		return self::$peer;
	}

} 