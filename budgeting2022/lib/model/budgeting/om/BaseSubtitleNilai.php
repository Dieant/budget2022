<?php


abstract class BaseSubtitleNilai extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $unit_id;


	
	protected $nilai_1;


	
	protected $nilai_2;


	
	protected $nilai_3;


	
	protected $total;


	
	protected $kegiatan_code;


	
	protected $subtitle;


	
	protected $keterangan;


	
	protected $sub_id;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getUnitId()
	{

		return $this->unit_id;
	}

	
	public function getNilai1()
	{

		return $this->nilai_1;
	}

	
	public function getNilai2()
	{

		return $this->nilai_2;
	}

	
	public function getNilai3()
	{

		return $this->nilai_3;
	}

	
	public function getTotal()
	{

		return $this->total;
	}

	
	public function getKegiatanCode()
	{

		return $this->kegiatan_code;
	}

	
	public function getSubtitle()
	{

		return $this->subtitle;
	}

	
	public function getKeterangan()
	{

		return $this->keterangan;
	}

	
	public function getSubId()
	{

		return $this->sub_id;
	}

	
	public function setUnitId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->unit_id !== $v) {
			$this->unit_id = $v;
			$this->modifiedColumns[] = SubtitleNilaiPeer::UNIT_ID;
		}

	} 
	
	public function setNilai1($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->nilai_1 !== $v) {
			$this->nilai_1 = $v;
			$this->modifiedColumns[] = SubtitleNilaiPeer::NILAI_1;
		}

	} 
	
	public function setNilai2($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->nilai_2 !== $v) {
			$this->nilai_2 = $v;
			$this->modifiedColumns[] = SubtitleNilaiPeer::NILAI_2;
		}

	} 
	
	public function setNilai3($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->nilai_3 !== $v) {
			$this->nilai_3 = $v;
			$this->modifiedColumns[] = SubtitleNilaiPeer::NILAI_3;
		}

	} 
	
	public function setTotal($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->total !== $v) {
			$this->total = $v;
			$this->modifiedColumns[] = SubtitleNilaiPeer::TOTAL;
		}

	} 
	
	public function setKegiatanCode($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kegiatan_code !== $v) {
			$this->kegiatan_code = $v;
			$this->modifiedColumns[] = SubtitleNilaiPeer::KEGIATAN_CODE;
		}

	} 
	
	public function setSubtitle($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->subtitle !== $v) {
			$this->subtitle = $v;
			$this->modifiedColumns[] = SubtitleNilaiPeer::SUBTITLE;
		}

	} 
	
	public function setKeterangan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->keterangan !== $v) {
			$this->keterangan = $v;
			$this->modifiedColumns[] = SubtitleNilaiPeer::KETERANGAN;
		}

	} 
	
	public function setSubId($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->sub_id !== $v) {
			$this->sub_id = $v;
			$this->modifiedColumns[] = SubtitleNilaiPeer::SUB_ID;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->unit_id = $rs->getString($startcol + 0);

			$this->nilai_1 = $rs->getInt($startcol + 1);

			$this->nilai_2 = $rs->getInt($startcol + 2);

			$this->nilai_3 = $rs->getInt($startcol + 3);

			$this->total = $rs->getInt($startcol + 4);

			$this->kegiatan_code = $rs->getString($startcol + 5);

			$this->subtitle = $rs->getString($startcol + 6);

			$this->keterangan = $rs->getString($startcol + 7);

			$this->sub_id = $rs->getInt($startcol + 8);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 9; 
		} catch (Exception $e) {
			throw new PropelException("Error populating SubtitleNilai object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(SubtitleNilaiPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			SubtitleNilaiPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(SubtitleNilaiPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = SubtitleNilaiPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += SubtitleNilaiPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = SubtitleNilaiPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = SubtitleNilaiPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getUnitId();
				break;
			case 1:
				return $this->getNilai1();
				break;
			case 2:
				return $this->getNilai2();
				break;
			case 3:
				return $this->getNilai3();
				break;
			case 4:
				return $this->getTotal();
				break;
			case 5:
				return $this->getKegiatanCode();
				break;
			case 6:
				return $this->getSubtitle();
				break;
			case 7:
				return $this->getKeterangan();
				break;
			case 8:
				return $this->getSubId();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = SubtitleNilaiPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getUnitId(),
			$keys[1] => $this->getNilai1(),
			$keys[2] => $this->getNilai2(),
			$keys[3] => $this->getNilai3(),
			$keys[4] => $this->getTotal(),
			$keys[5] => $this->getKegiatanCode(),
			$keys[6] => $this->getSubtitle(),
			$keys[7] => $this->getKeterangan(),
			$keys[8] => $this->getSubId(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = SubtitleNilaiPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setUnitId($value);
				break;
			case 1:
				$this->setNilai1($value);
				break;
			case 2:
				$this->setNilai2($value);
				break;
			case 3:
				$this->setNilai3($value);
				break;
			case 4:
				$this->setTotal($value);
				break;
			case 5:
				$this->setKegiatanCode($value);
				break;
			case 6:
				$this->setSubtitle($value);
				break;
			case 7:
				$this->setKeterangan($value);
				break;
			case 8:
				$this->setSubId($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = SubtitleNilaiPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setUnitId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setNilai1($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setNilai2($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setNilai3($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setTotal($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setKegiatanCode($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setSubtitle($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setKeterangan($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setSubId($arr[$keys[8]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(SubtitleNilaiPeer::DATABASE_NAME);

		if ($this->isColumnModified(SubtitleNilaiPeer::UNIT_ID)) $criteria->add(SubtitleNilaiPeer::UNIT_ID, $this->unit_id);
		if ($this->isColumnModified(SubtitleNilaiPeer::NILAI_1)) $criteria->add(SubtitleNilaiPeer::NILAI_1, $this->nilai_1);
		if ($this->isColumnModified(SubtitleNilaiPeer::NILAI_2)) $criteria->add(SubtitleNilaiPeer::NILAI_2, $this->nilai_2);
		if ($this->isColumnModified(SubtitleNilaiPeer::NILAI_3)) $criteria->add(SubtitleNilaiPeer::NILAI_3, $this->nilai_3);
		if ($this->isColumnModified(SubtitleNilaiPeer::TOTAL)) $criteria->add(SubtitleNilaiPeer::TOTAL, $this->total);
		if ($this->isColumnModified(SubtitleNilaiPeer::KEGIATAN_CODE)) $criteria->add(SubtitleNilaiPeer::KEGIATAN_CODE, $this->kegiatan_code);
		if ($this->isColumnModified(SubtitleNilaiPeer::SUBTITLE)) $criteria->add(SubtitleNilaiPeer::SUBTITLE, $this->subtitle);
		if ($this->isColumnModified(SubtitleNilaiPeer::KETERANGAN)) $criteria->add(SubtitleNilaiPeer::KETERANGAN, $this->keterangan);
		if ($this->isColumnModified(SubtitleNilaiPeer::SUB_ID)) $criteria->add(SubtitleNilaiPeer::SUB_ID, $this->sub_id);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(SubtitleNilaiPeer::DATABASE_NAME);


		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return null;
	}

	
	 public function setPrimaryKey($pk)
	 {
		 	 }

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setUnitId($this->unit_id);

		$copyObj->setNilai1($this->nilai_1);

		$copyObj->setNilai2($this->nilai_2);

		$copyObj->setNilai3($this->nilai_3);

		$copyObj->setTotal($this->total);

		$copyObj->setKegiatanCode($this->kegiatan_code);

		$copyObj->setSubtitle($this->subtitle);

		$copyObj->setKeterangan($this->keterangan);

		$copyObj->setSubId($this->sub_id);


		$copyObj->setNew(true);

	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new SubtitleNilaiPeer();
		}
		return self::$peer;
	}

} 