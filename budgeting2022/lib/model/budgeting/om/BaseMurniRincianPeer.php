<?php


abstract class BaseMurniRincianPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.murni_rincian';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.MurniRincian';

	
	const NUM_COLUMNS = 16;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const KEGIATAN_CODE = 'ebudget.murni_rincian.KEGIATAN_CODE';

	
	const TIPE = 'ebudget.murni_rincian.TIPE';

	
	const RINCIAN_CONFIRMED = 'ebudget.murni_rincian.RINCIAN_CONFIRMED';

	
	const RINCIAN_CHANGED = 'ebudget.murni_rincian.RINCIAN_CHANGED';

	
	const RINCIAN_SELESAI = 'ebudget.murni_rincian.RINCIAN_SELESAI';

	
	const IP_ADDRESS = 'ebudget.murni_rincian.IP_ADDRESS';

	
	const WAKTU_ACCESS = 'ebudget.murni_rincian.WAKTU_ACCESS';

	
	const TARGET = 'ebudget.murni_rincian.TARGET';

	
	const UNIT_ID = 'ebudget.murni_rincian.UNIT_ID';

	
	const RINCIAN_LEVEL = 'ebudget.murni_rincian.RINCIAN_LEVEL';

	
	const LOCK = 'ebudget.murni_rincian.LOCK';

	
	const LAST_UPDATE_USER = 'ebudget.murni_rincian.LAST_UPDATE_USER';

	
	const LAST_UPDATE_TIME = 'ebudget.murni_rincian.LAST_UPDATE_TIME';

	
	const LAST_UPDATE_IP = 'ebudget.murni_rincian.LAST_UPDATE_IP';

	
	const TAHAP = 'ebudget.murni_rincian.TAHAP';

	
	const TAHUN = 'ebudget.murni_rincian.TAHUN';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('KegiatanCode', 'Tipe', 'RincianConfirmed', 'RincianChanged', 'RincianSelesai', 'IpAddress', 'WaktuAccess', 'Target', 'UnitId', 'RincianLevel', 'Lock', 'LastUpdateUser', 'LastUpdateTime', 'LastUpdateIp', 'Tahap', 'Tahun', ),
		BasePeer::TYPE_COLNAME => array (MurniRincianPeer::KEGIATAN_CODE, MurniRincianPeer::TIPE, MurniRincianPeer::RINCIAN_CONFIRMED, MurniRincianPeer::RINCIAN_CHANGED, MurniRincianPeer::RINCIAN_SELESAI, MurniRincianPeer::IP_ADDRESS, MurniRincianPeer::WAKTU_ACCESS, MurniRincianPeer::TARGET, MurniRincianPeer::UNIT_ID, MurniRincianPeer::RINCIAN_LEVEL, MurniRincianPeer::LOCK, MurniRincianPeer::LAST_UPDATE_USER, MurniRincianPeer::LAST_UPDATE_TIME, MurniRincianPeer::LAST_UPDATE_IP, MurniRincianPeer::TAHAP, MurniRincianPeer::TAHUN, ),
		BasePeer::TYPE_FIELDNAME => array ('kegiatan_code', 'tipe', 'rincian_confirmed', 'rincian_changed', 'rincian_selesai', 'ip_address', 'waktu_access', 'target', 'unit_id', 'rincian_level', 'lock', 'last_update_user', 'last_update_time', 'last_update_ip', 'tahap', 'tahun', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('KegiatanCode' => 0, 'Tipe' => 1, 'RincianConfirmed' => 2, 'RincianChanged' => 3, 'RincianSelesai' => 4, 'IpAddress' => 5, 'WaktuAccess' => 6, 'Target' => 7, 'UnitId' => 8, 'RincianLevel' => 9, 'Lock' => 10, 'LastUpdateUser' => 11, 'LastUpdateTime' => 12, 'LastUpdateIp' => 13, 'Tahap' => 14, 'Tahun' => 15, ),
		BasePeer::TYPE_COLNAME => array (MurniRincianPeer::KEGIATAN_CODE => 0, MurniRincianPeer::TIPE => 1, MurniRincianPeer::RINCIAN_CONFIRMED => 2, MurniRincianPeer::RINCIAN_CHANGED => 3, MurniRincianPeer::RINCIAN_SELESAI => 4, MurniRincianPeer::IP_ADDRESS => 5, MurniRincianPeer::WAKTU_ACCESS => 6, MurniRincianPeer::TARGET => 7, MurniRincianPeer::UNIT_ID => 8, MurniRincianPeer::RINCIAN_LEVEL => 9, MurniRincianPeer::LOCK => 10, MurniRincianPeer::LAST_UPDATE_USER => 11, MurniRincianPeer::LAST_UPDATE_TIME => 12, MurniRincianPeer::LAST_UPDATE_IP => 13, MurniRincianPeer::TAHAP => 14, MurniRincianPeer::TAHUN => 15, ),
		BasePeer::TYPE_FIELDNAME => array ('kegiatan_code' => 0, 'tipe' => 1, 'rincian_confirmed' => 2, 'rincian_changed' => 3, 'rincian_selesai' => 4, 'ip_address' => 5, 'waktu_access' => 6, 'target' => 7, 'unit_id' => 8, 'rincian_level' => 9, 'lock' => 10, 'last_update_user' => 11, 'last_update_time' => 12, 'last_update_ip' => 13, 'tahap' => 14, 'tahun' => 15, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/MurniRincianMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.MurniRincianMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = MurniRincianPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(MurniRincianPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(MurniRincianPeer::KEGIATAN_CODE);

		$criteria->addSelectColumn(MurniRincianPeer::TIPE);

		$criteria->addSelectColumn(MurniRincianPeer::RINCIAN_CONFIRMED);

		$criteria->addSelectColumn(MurniRincianPeer::RINCIAN_CHANGED);

		$criteria->addSelectColumn(MurniRincianPeer::RINCIAN_SELESAI);

		$criteria->addSelectColumn(MurniRincianPeer::IP_ADDRESS);

		$criteria->addSelectColumn(MurniRincianPeer::WAKTU_ACCESS);

		$criteria->addSelectColumn(MurniRincianPeer::TARGET);

		$criteria->addSelectColumn(MurniRincianPeer::UNIT_ID);

		$criteria->addSelectColumn(MurniRincianPeer::RINCIAN_LEVEL);

		$criteria->addSelectColumn(MurniRincianPeer::LOCK);

		$criteria->addSelectColumn(MurniRincianPeer::LAST_UPDATE_USER);

		$criteria->addSelectColumn(MurniRincianPeer::LAST_UPDATE_TIME);

		$criteria->addSelectColumn(MurniRincianPeer::LAST_UPDATE_IP);

		$criteria->addSelectColumn(MurniRincianPeer::TAHAP);

		$criteria->addSelectColumn(MurniRincianPeer::TAHUN);

	}

	const COUNT = 'COUNT(ebudget.murni_rincian.KEGIATAN_CODE)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.murni_rincian.KEGIATAN_CODE)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(MurniRincianPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(MurniRincianPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = MurniRincianPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = MurniRincianPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return MurniRincianPeer::populateObjects(MurniRincianPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			MurniRincianPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = MurniRincianPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return MurniRincianPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(MurniRincianPeer::KEGIATAN_CODE);
			$selectCriteria->add(MurniRincianPeer::KEGIATAN_CODE, $criteria->remove(MurniRincianPeer::KEGIATAN_CODE), $comparison);

			$comparison = $criteria->getComparison(MurniRincianPeer::TIPE);
			$selectCriteria->add(MurniRincianPeer::TIPE, $criteria->remove(MurniRincianPeer::TIPE), $comparison);

			$comparison = $criteria->getComparison(MurniRincianPeer::UNIT_ID);
			$selectCriteria->add(MurniRincianPeer::UNIT_ID, $criteria->remove(MurniRincianPeer::UNIT_ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(MurniRincianPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(MurniRincianPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof MurniRincian) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
												if(count($values) == count($values, COUNT_RECURSIVE))
			{
								$values = array($values);
			}
			$vals = array();
			foreach($values as $value)
			{

				$vals[0][] = $value[0];
				$vals[1][] = $value[1];
				$vals[2][] = $value[2];
			}

			$criteria->add(MurniRincianPeer::KEGIATAN_CODE, $vals[0], Criteria::IN);
			$criteria->add(MurniRincianPeer::TIPE, $vals[1], Criteria::IN);
			$criteria->add(MurniRincianPeer::UNIT_ID, $vals[2], Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(MurniRincian $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(MurniRincianPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(MurniRincianPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		return BasePeer::doValidate(MurniRincianPeer::DATABASE_NAME, MurniRincianPeer::TABLE_NAME, $columns);
	}

	
	public static function retrieveByPK( $kegiatan_code, $tipe, $unit_id, $con = null) {
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$criteria = new Criteria();
		$criteria->add(MurniRincianPeer::KEGIATAN_CODE, $kegiatan_code);
		$criteria->add(MurniRincianPeer::TIPE, $tipe);
		$criteria->add(MurniRincianPeer::UNIT_ID, $unit_id);
		$v = MurniRincianPeer::doSelect($criteria, $con);

		return !empty($v) ? $v[0] : null;
	}
} 
if (Propel::isInit()) {
			try {
		BaseMurniRincianPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/MurniRincianMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.MurniRincianMapBuilder');
}
