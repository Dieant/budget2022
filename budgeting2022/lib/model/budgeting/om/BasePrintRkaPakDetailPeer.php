<?php


abstract class BasePrintRkaPakDetailPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.print_rka_pak_detail';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.PrintRkaPakDetail';

	
	const NUM_COLUMNS = 10;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const ID_PRINT_RKA_PAK = 'ebudget.print_rka_pak_detail.ID_PRINT_RKA_PAK';

	
	const SUBTITLE = 'ebudget.print_rka_pak_detail.SUBTITLE';

	
	const REKENING_CODE = 'ebudget.print_rka_pak_detail.REKENING_CODE';

	
	const SUBSUBTITLE = 'ebudget.print_rka_pak_detail.SUBSUBTITLE';

	
	const NAMA_KOMPONEN = 'ebudget.print_rka_pak_detail.NAMA_KOMPONEN';

	
	const SATUAN = 'ebudget.print_rka_pak_detail.SATUAN';

	
	const KOEFISIEN = 'ebudget.print_rka_pak_detail.KOEFISIEN';

	
	const HARGA = 'ebudget.print_rka_pak_detail.HARGA';

	
	const HASIL = 'ebudget.print_rka_pak_detail.HASIL';

	
	const PAJAK = 'ebudget.print_rka_pak_detail.PAJAK';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('IdPrintRkaPak', 'Subtitle', 'RekeningCode', 'Subsubtitle', 'NamaKomponen', 'Satuan', 'Koefisien', 'Harga', 'Hasil', 'Pajak', ),
		BasePeer::TYPE_COLNAME => array (PrintRkaPakDetailPeer::ID_PRINT_RKA_PAK, PrintRkaPakDetailPeer::SUBTITLE, PrintRkaPakDetailPeer::REKENING_CODE, PrintRkaPakDetailPeer::SUBSUBTITLE, PrintRkaPakDetailPeer::NAMA_KOMPONEN, PrintRkaPakDetailPeer::SATUAN, PrintRkaPakDetailPeer::KOEFISIEN, PrintRkaPakDetailPeer::HARGA, PrintRkaPakDetailPeer::HASIL, PrintRkaPakDetailPeer::PAJAK, ),
		BasePeer::TYPE_FIELDNAME => array ('id_print_rka_pak', 'subtitle', 'rekening_code', 'subsubtitle', 'nama_komponen', 'satuan', 'koefisien', 'harga', 'hasil', 'pajak', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('IdPrintRkaPak' => 0, 'Subtitle' => 1, 'RekeningCode' => 2, 'Subsubtitle' => 3, 'NamaKomponen' => 4, 'Satuan' => 5, 'Koefisien' => 6, 'Harga' => 7, 'Hasil' => 8, 'Pajak' => 9, ),
		BasePeer::TYPE_COLNAME => array (PrintRkaPakDetailPeer::ID_PRINT_RKA_PAK => 0, PrintRkaPakDetailPeer::SUBTITLE => 1, PrintRkaPakDetailPeer::REKENING_CODE => 2, PrintRkaPakDetailPeer::SUBSUBTITLE => 3, PrintRkaPakDetailPeer::NAMA_KOMPONEN => 4, PrintRkaPakDetailPeer::SATUAN => 5, PrintRkaPakDetailPeer::KOEFISIEN => 6, PrintRkaPakDetailPeer::HARGA => 7, PrintRkaPakDetailPeer::HASIL => 8, PrintRkaPakDetailPeer::PAJAK => 9, ),
		BasePeer::TYPE_FIELDNAME => array ('id_print_rka_pak' => 0, 'subtitle' => 1, 'rekening_code' => 2, 'subsubtitle' => 3, 'nama_komponen' => 4, 'satuan' => 5, 'koefisien' => 6, 'harga' => 7, 'hasil' => 8, 'pajak' => 9, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/PrintRkaPakDetailMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.PrintRkaPakDetailMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = PrintRkaPakDetailPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(PrintRkaPakDetailPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(PrintRkaPakDetailPeer::ID_PRINT_RKA_PAK);

		$criteria->addSelectColumn(PrintRkaPakDetailPeer::SUBTITLE);

		$criteria->addSelectColumn(PrintRkaPakDetailPeer::REKENING_CODE);

		$criteria->addSelectColumn(PrintRkaPakDetailPeer::SUBSUBTITLE);

		$criteria->addSelectColumn(PrintRkaPakDetailPeer::NAMA_KOMPONEN);

		$criteria->addSelectColumn(PrintRkaPakDetailPeer::SATUAN);

		$criteria->addSelectColumn(PrintRkaPakDetailPeer::KOEFISIEN);

		$criteria->addSelectColumn(PrintRkaPakDetailPeer::HARGA);

		$criteria->addSelectColumn(PrintRkaPakDetailPeer::HASIL);

		$criteria->addSelectColumn(PrintRkaPakDetailPeer::PAJAK);

	}

	const COUNT = 'COUNT(ebudget.print_rka_pak_detail.ID_PRINT_RKA_PAK)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.print_rka_pak_detail.ID_PRINT_RKA_PAK)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(PrintRkaPakDetailPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(PrintRkaPakDetailPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = PrintRkaPakDetailPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = PrintRkaPakDetailPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return PrintRkaPakDetailPeer::populateObjects(PrintRkaPakDetailPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			PrintRkaPakDetailPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = PrintRkaPakDetailPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return PrintRkaPakDetailPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(PrintRkaPakDetailPeer::ID_PRINT_RKA_PAK);
			$selectCriteria->add(PrintRkaPakDetailPeer::ID_PRINT_RKA_PAK, $criteria->remove(PrintRkaPakDetailPeer::ID_PRINT_RKA_PAK), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(PrintRkaPakDetailPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(PrintRkaPakDetailPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof PrintRkaPakDetail) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(PrintRkaPakDetailPeer::ID_PRINT_RKA_PAK, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(PrintRkaPakDetail $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(PrintRkaPakDetailPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(PrintRkaPakDetailPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(PrintRkaPakDetailPeer::DATABASE_NAME, PrintRkaPakDetailPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = PrintRkaPakDetailPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(PrintRkaPakDetailPeer::DATABASE_NAME);

		$criteria->add(PrintRkaPakDetailPeer::ID_PRINT_RKA_PAK, $pk);


		$v = PrintRkaPakDetailPeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(PrintRkaPakDetailPeer::ID_PRINT_RKA_PAK, $pks, Criteria::IN);
			$objs = PrintRkaPakDetailPeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BasePrintRkaPakDetailPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/PrintRkaPakDetailMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.PrintRkaPakDetailMapBuilder');
}
