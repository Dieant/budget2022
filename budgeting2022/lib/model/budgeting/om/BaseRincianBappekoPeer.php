<?php


abstract class BaseRincianBappekoPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.rincian_bappeko';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.RincianBappeko';

	
	const NUM_COLUMNS = 11;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const UNIT_ID = 'ebudget.rincian_bappeko.UNIT_ID';

	
	const KODE_KEGIATAN = 'ebudget.rincian_bappeko.KODE_KEGIATAN';

	
	const TAHAP = 'ebudget.rincian_bappeko.TAHAP';

	
	const JAWABAN1_DINAS = 'ebudget.rincian_bappeko.JAWABAN1_DINAS';

	
	const JAWABAN2_DINAS = 'ebudget.rincian_bappeko.JAWABAN2_DINAS';

	
	const CATATAN_DINAS = 'ebudget.rincian_bappeko.CATATAN_DINAS';

	
	const JAWABAN1_BAPPEKO = 'ebudget.rincian_bappeko.JAWABAN1_BAPPEKO';

	
	const JAWABAN2_BAPPEKO = 'ebudget.rincian_bappeko.JAWABAN2_BAPPEKO';

	
	const CATATAN_BAPPEKO = 'ebudget.rincian_bappeko.CATATAN_BAPPEKO';

	
	const STATUS_BUKA = 'ebudget.rincian_bappeko.STATUS_BUKA';

	
	const UPDATED_AT = 'ebudget.rincian_bappeko.UPDATED_AT';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('UnitId', 'KodeKegiatan', 'Tahap', 'Jawaban1Dinas', 'Jawaban2Dinas', 'CatatanDinas', 'Jawaban1Bappeko', 'Jawaban2Bappeko', 'CatatanBappeko', 'StatusBuka', 'UpdatedAt', ),
		BasePeer::TYPE_COLNAME => array (RincianBappekoPeer::UNIT_ID, RincianBappekoPeer::KODE_KEGIATAN, RincianBappekoPeer::TAHAP, RincianBappekoPeer::JAWABAN1_DINAS, RincianBappekoPeer::JAWABAN2_DINAS, RincianBappekoPeer::CATATAN_DINAS, RincianBappekoPeer::JAWABAN1_BAPPEKO, RincianBappekoPeer::JAWABAN2_BAPPEKO, RincianBappekoPeer::CATATAN_BAPPEKO, RincianBappekoPeer::STATUS_BUKA, RincianBappekoPeer::UPDATED_AT, ),
		BasePeer::TYPE_FIELDNAME => array ('unit_id', 'kode_kegiatan', 'tahap', 'jawaban1_dinas', 'jawaban2_dinas', 'catatan_dinas', 'jawaban1_bappeko', 'jawaban2_bappeko', 'catatan_bappeko', 'status_buka', 'updated_at', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('UnitId' => 0, 'KodeKegiatan' => 1, 'Tahap' => 2, 'Jawaban1Dinas' => 3, 'Jawaban2Dinas' => 4, 'CatatanDinas' => 5, 'Jawaban1Bappeko' => 6, 'Jawaban2Bappeko' => 7, 'CatatanBappeko' => 8, 'StatusBuka' => 9, 'UpdatedAt' => 10, ),
		BasePeer::TYPE_COLNAME => array (RincianBappekoPeer::UNIT_ID => 0, RincianBappekoPeer::KODE_KEGIATAN => 1, RincianBappekoPeer::TAHAP => 2, RincianBappekoPeer::JAWABAN1_DINAS => 3, RincianBappekoPeer::JAWABAN2_DINAS => 4, RincianBappekoPeer::CATATAN_DINAS => 5, RincianBappekoPeer::JAWABAN1_BAPPEKO => 6, RincianBappekoPeer::JAWABAN2_BAPPEKO => 7, RincianBappekoPeer::CATATAN_BAPPEKO => 8, RincianBappekoPeer::STATUS_BUKA => 9, RincianBappekoPeer::UPDATED_AT => 10, ),
		BasePeer::TYPE_FIELDNAME => array ('unit_id' => 0, 'kode_kegiatan' => 1, 'tahap' => 2, 'jawaban1_dinas' => 3, 'jawaban2_dinas' => 4, 'catatan_dinas' => 5, 'jawaban1_bappeko' => 6, 'jawaban2_bappeko' => 7, 'catatan_bappeko' => 8, 'status_buka' => 9, 'updated_at' => 10, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/RincianBappekoMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.RincianBappekoMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = RincianBappekoPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(RincianBappekoPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(RincianBappekoPeer::UNIT_ID);

		$criteria->addSelectColumn(RincianBappekoPeer::KODE_KEGIATAN);

		$criteria->addSelectColumn(RincianBappekoPeer::TAHAP);

		$criteria->addSelectColumn(RincianBappekoPeer::JAWABAN1_DINAS);

		$criteria->addSelectColumn(RincianBappekoPeer::JAWABAN2_DINAS);

		$criteria->addSelectColumn(RincianBappekoPeer::CATATAN_DINAS);

		$criteria->addSelectColumn(RincianBappekoPeer::JAWABAN1_BAPPEKO);

		$criteria->addSelectColumn(RincianBappekoPeer::JAWABAN2_BAPPEKO);

		$criteria->addSelectColumn(RincianBappekoPeer::CATATAN_BAPPEKO);

		$criteria->addSelectColumn(RincianBappekoPeer::STATUS_BUKA);

		$criteria->addSelectColumn(RincianBappekoPeer::UPDATED_AT);

	}

	const COUNT = 'COUNT(ebudget.rincian_bappeko.UNIT_ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.rincian_bappeko.UNIT_ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(RincianBappekoPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(RincianBappekoPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = RincianBappekoPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = RincianBappekoPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return RincianBappekoPeer::populateObjects(RincianBappekoPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			RincianBappekoPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = RincianBappekoPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return RincianBappekoPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(RincianBappekoPeer::UNIT_ID);
			$selectCriteria->add(RincianBappekoPeer::UNIT_ID, $criteria->remove(RincianBappekoPeer::UNIT_ID), $comparison);

			$comparison = $criteria->getComparison(RincianBappekoPeer::KODE_KEGIATAN);
			$selectCriteria->add(RincianBappekoPeer::KODE_KEGIATAN, $criteria->remove(RincianBappekoPeer::KODE_KEGIATAN), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(RincianBappekoPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(RincianBappekoPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof RincianBappeko) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
												if(count($values) == count($values, COUNT_RECURSIVE))
			{
								$values = array($values);
			}
			$vals = array();
			foreach($values as $value)
			{

				$vals[0][] = $value[0];
				$vals[1][] = $value[1];
			}

			$criteria->add(RincianBappekoPeer::UNIT_ID, $vals[0], Criteria::IN);
			$criteria->add(RincianBappekoPeer::KODE_KEGIATAN, $vals[1], Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(RincianBappeko $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(RincianBappekoPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(RincianBappekoPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(RincianBappekoPeer::DATABASE_NAME, RincianBappekoPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = RincianBappekoPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK( $unit_id, $kode_kegiatan, $con = null) {
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$criteria = new Criteria();
		$criteria->add(RincianBappekoPeer::UNIT_ID, $unit_id);
		$criteria->add(RincianBappekoPeer::KODE_KEGIATAN, $kode_kegiatan);
		$v = RincianBappekoPeer::doSelect($criteria, $con);

		return !empty($v) ? $v[0] : null;
	}
} 
if (Propel::isInit()) {
			try {
		BaseRincianBappekoPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/RincianBappekoMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.RincianBappekoMapBuilder');
}
