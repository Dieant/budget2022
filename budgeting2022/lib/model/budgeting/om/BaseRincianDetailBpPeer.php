<?php


abstract class BaseRincianDetailBpPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.rincian_detail_bp';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.RincianDetailBp';

	
	const NUM_COLUMNS = 77;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const KEGIATAN_CODE = 'ebudget.rincian_detail_bp.KEGIATAN_CODE';

	
	const TIPE = 'ebudget.rincian_detail_bp.TIPE';

	
	const DETAIL_NO = 'ebudget.rincian_detail_bp.DETAIL_NO';

	
	const REKENING_CODE = 'ebudget.rincian_detail_bp.REKENING_CODE';

	
	const KOMPONEN_ID = 'ebudget.rincian_detail_bp.KOMPONEN_ID';

	
	const DETAIL_NAME = 'ebudget.rincian_detail_bp.DETAIL_NAME';

	
	const VOLUME = 'ebudget.rincian_detail_bp.VOLUME';

	
	const KETERANGAN_KOEFISIEN = 'ebudget.rincian_detail_bp.KETERANGAN_KOEFISIEN';

	
	const SUBTITLE = 'ebudget.rincian_detail_bp.SUBTITLE';

	
	const KOMPONEN_HARGA = 'ebudget.rincian_detail_bp.KOMPONEN_HARGA';

	
	const KOMPONEN_HARGA_AWAL = 'ebudget.rincian_detail_bp.KOMPONEN_HARGA_AWAL';

	
	const KOMPONEN_NAME = 'ebudget.rincian_detail_bp.KOMPONEN_NAME';

	
	const SATUAN = 'ebudget.rincian_detail_bp.SATUAN';

	
	const PAJAK = 'ebudget.rincian_detail_bp.PAJAK';

	
	const UNIT_ID = 'ebudget.rincian_detail_bp.UNIT_ID';

	
	const FROM_SUB_KEGIATAN = 'ebudget.rincian_detail_bp.FROM_SUB_KEGIATAN';

	
	const SUB = 'ebudget.rincian_detail_bp.SUB';

	
	const KODE_SUB = 'ebudget.rincian_detail_bp.KODE_SUB';

	
	const LAST_UPDATE_USER = 'ebudget.rincian_detail_bp.LAST_UPDATE_USER';

	
	const LAST_UPDATE_TIME = 'ebudget.rincian_detail_bp.LAST_UPDATE_TIME';

	
	const LAST_UPDATE_IP = 'ebudget.rincian_detail_bp.LAST_UPDATE_IP';

	
	const TAHAP = 'ebudget.rincian_detail_bp.TAHAP';

	
	const TAHAP_EDIT = 'ebudget.rincian_detail_bp.TAHAP_EDIT';

	
	const TAHAP_NEW = 'ebudget.rincian_detail_bp.TAHAP_NEW';

	
	const STATUS_LELANG = 'ebudget.rincian_detail_bp.STATUS_LELANG';

	
	const NOMOR_LELANG = 'ebudget.rincian_detail_bp.NOMOR_LELANG';

	
	const KOEFISIEN_SEMULA = 'ebudget.rincian_detail_bp.KOEFISIEN_SEMULA';

	
	const VOLUME_SEMULA = 'ebudget.rincian_detail_bp.VOLUME_SEMULA';

	
	const HARGA_SEMULA = 'ebudget.rincian_detail_bp.HARGA_SEMULA';

	
	const TOTAL_SEMULA = 'ebudget.rincian_detail_bp.TOTAL_SEMULA';

	
	const LOCK_SUBTITLE = 'ebudget.rincian_detail_bp.LOCK_SUBTITLE';

	
	const STATUS_HAPUS = 'ebudget.rincian_detail_bp.STATUS_HAPUS';

	
	const TAHUN = 'ebudget.rincian_detail_bp.TAHUN';

	
	const KODE_LOKASI = 'ebudget.rincian_detail_bp.KODE_LOKASI';

	
	const KECAMATAN = 'ebudget.rincian_detail_bp.KECAMATAN';

	
	const REKENING_CODE_ASLI = 'ebudget.rincian_detail_bp.REKENING_CODE_ASLI';

	
	const NOTE_SKPD = 'ebudget.rincian_detail_bp.NOTE_SKPD';

	
	const NOTE_PENELITI = 'ebudget.rincian_detail_bp.NOTE_PENELITI';

	
	const NILAI_ANGGARAN = 'ebudget.rincian_detail_bp.NILAI_ANGGARAN';

	
	const IS_BLUD = 'ebudget.rincian_detail_bp.IS_BLUD';

	
	const LOKASI_KECAMATAN = 'ebudget.rincian_detail_bp.LOKASI_KECAMATAN';

	
	const LOKASI_KELURAHAN = 'ebudget.rincian_detail_bp.LOKASI_KELURAHAN';

	
	const OB = 'ebudget.rincian_detail_bp.OB';

	
	const OB_FROM_ID = 'ebudget.rincian_detail_bp.OB_FROM_ID';

	
	const IS_PER_KOMPONEN = 'ebudget.rincian_detail_bp.IS_PER_KOMPONEN';

	
	const KEGIATAN_CODE_ASAL = 'ebudget.rincian_detail_bp.KEGIATAN_CODE_ASAL';

	
	const TH_KE_MULTIYEARS = 'ebudget.rincian_detail_bp.TH_KE_MULTIYEARS';

	
	const HARGA_SEBELUM_SISA_LELANG = 'ebudget.rincian_detail_bp.HARGA_SEBELUM_SISA_LELANG';

	
	const IS_MUSRENBANG = 'ebudget.rincian_detail_bp.IS_MUSRENBANG';

	
	const SUB_ID_ASAL = 'ebudget.rincian_detail_bp.SUB_ID_ASAL';

	
	const SUBTITLE_ASAL = 'ebudget.rincian_detail_bp.SUBTITLE_ASAL';

	
	const KODE_SUB_ASAL = 'ebudget.rincian_detail_bp.KODE_SUB_ASAL';

	
	const SUB_ASAL = 'ebudget.rincian_detail_bp.SUB_ASAL';

	
	const LAST_EDIT_TIME = 'ebudget.rincian_detail_bp.LAST_EDIT_TIME';

	
	const IS_POTONG_BPJS = 'ebudget.rincian_detail_bp.IS_POTONG_BPJS';

	
	const IS_IURAN_BPJS = 'ebudget.rincian_detail_bp.IS_IURAN_BPJS';

	
	const STATUS_OB = 'ebudget.rincian_detail_bp.STATUS_OB';

	
	const OB_PARENT = 'ebudget.rincian_detail_bp.OB_PARENT';

	
	const OB_ALOKASI_BARU = 'ebudget.rincian_detail_bp.OB_ALOKASI_BARU';

	
	const IS_HIBAH = 'ebudget.rincian_detail_bp.IS_HIBAH';

	
	const AKRUAL_CODE = 'ebudget.rincian_detail_bp.AKRUAL_CODE';

	
	const TIPE2 = 'ebudget.rincian_detail_bp.TIPE2';

	
	const STATUS_LEVEL = 'ebudget.rincian_detail_bp.STATUS_LEVEL';

	
	const STATUS_LEVEL_TOLAK = 'ebudget.rincian_detail_bp.STATUS_LEVEL_TOLAK';

	
	const STATUS_SISIPAN = 'ebudget.rincian_detail_bp.STATUS_SISIPAN';

	
	const IS_TAPD_SETUJU = 'ebudget.rincian_detail_bp.IS_TAPD_SETUJU';

	
	const IS_BAPPEKO_SETUJU = 'ebudget.rincian_detail_bp.IS_BAPPEKO_SETUJU';

	
	const IS_PENYELIA_SETUJU = 'ebudget.rincian_detail_bp.IS_PENYELIA_SETUJU';

	
	const NOTE_TAPD = 'ebudget.rincian_detail_bp.NOTE_TAPD';

	
	const NOTE_BAPPEKO = 'ebudget.rincian_detail_bp.NOTE_BAPPEKO';

	
	const SATUAN_SEMULA = 'ebudget.rincian_detail_bp.SATUAN_SEMULA';

	
	const ID_LOKASI = 'ebudget.rincian_detail_bp.ID_LOKASI';

	
	const DETAIL_KEGIATAN = 'ebudget.rincian_detail_bp.DETAIL_KEGIATAN';

	
	const DETAIL_KEGIATAN_SEMULA = 'ebudget.rincian_detail_bp.DETAIL_KEGIATAN_SEMULA';

	
	const STATUS_KOMPONEN_BARU = 'ebudget.rincian_detail_bp.STATUS_KOMPONEN_BARU';

	
	const STATUS_KOMPONEN_BERUBAH = 'ebudget.rincian_detail_bp.STATUS_KOMPONEN_BERUBAH';

	
	const APPROVE_UNLOCK_HARGA = 'ebudget.rincian_detail_bp.APPROVE_UNLOCK_HARGA';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('KegiatanCode', 'Tipe', 'DetailNo', 'RekeningCode', 'KomponenId', 'DetailName', 'Volume', 'KeteranganKoefisien', 'Subtitle', 'KomponenHarga', 'KomponenHargaAwal', 'KomponenName', 'Satuan', 'Pajak', 'UnitId', 'FromSubKegiatan', 'Sub', 'KodeSub', 'LastUpdateUser', 'LastUpdateTime', 'LastUpdateIp', 'Tahap', 'TahapEdit', 'TahapNew', 'StatusLelang', 'NomorLelang', 'KoefisienSemula', 'VolumeSemula', 'HargaSemula', 'TotalSemula', 'LockSubtitle', 'StatusHapus', 'Tahun', 'KodeLokasi', 'Kecamatan', 'RekeningCodeAsli', 'NoteSkpd', 'NotePeneliti', 'NilaiAnggaran', 'IsBlud', 'LokasiKecamatan', 'LokasiKelurahan', 'Ob', 'ObFromId', 'IsPerKomponen', 'KegiatanCodeAsal', 'ThKeMultiyears', 'HargaSebelumSisaLelang', 'IsMusrenbang', 'SubIdAsal', 'SubtitleAsal', 'KodeSubAsal', 'SubAsal', 'LastEditTime', 'IsPotongBpjs', 'IsIuranBpjs', 'StatusOb', 'ObParent', 'ObAlokasiBaru', 'IsHibah', 'AkrualCode', 'Tipe2', 'StatusLevel', 'StatusLevelTolak', 'StatusSisipan', 'IsTapdSetuju', 'IsBappekoSetuju', 'IsPenyeliaSetuju', 'NoteTapd', 'NoteBappeko', 'SatuanSemula', 'IdLokasi', 'DetailKegiatan', 'DetailKegiatanSemula', 'StatusKomponenBaru', 'StatusKomponenBerubah', 'ApproveUnlockHarga', ),
		BasePeer::TYPE_COLNAME => array (RincianDetailBpPeer::KEGIATAN_CODE, RincianDetailBpPeer::TIPE, RincianDetailBpPeer::DETAIL_NO, RincianDetailBpPeer::REKENING_CODE, RincianDetailBpPeer::KOMPONEN_ID, RincianDetailBpPeer::DETAIL_NAME, RincianDetailBpPeer::VOLUME, RincianDetailBpPeer::KETERANGAN_KOEFISIEN, RincianDetailBpPeer::SUBTITLE, RincianDetailBpPeer::KOMPONEN_HARGA, RincianDetailBpPeer::KOMPONEN_HARGA_AWAL, RincianDetailBpPeer::KOMPONEN_NAME, RincianDetailBpPeer::SATUAN, RincianDetailBpPeer::PAJAK, RincianDetailBpPeer::UNIT_ID, RincianDetailBpPeer::FROM_SUB_KEGIATAN, RincianDetailBpPeer::SUB, RincianDetailBpPeer::KODE_SUB, RincianDetailBpPeer::LAST_UPDATE_USER, RincianDetailBpPeer::LAST_UPDATE_TIME, RincianDetailBpPeer::LAST_UPDATE_IP, RincianDetailBpPeer::TAHAP, RincianDetailBpPeer::TAHAP_EDIT, RincianDetailBpPeer::TAHAP_NEW, RincianDetailBpPeer::STATUS_LELANG, RincianDetailBpPeer::NOMOR_LELANG, RincianDetailBpPeer::KOEFISIEN_SEMULA, RincianDetailBpPeer::VOLUME_SEMULA, RincianDetailBpPeer::HARGA_SEMULA, RincianDetailBpPeer::TOTAL_SEMULA, RincianDetailBpPeer::LOCK_SUBTITLE, RincianDetailBpPeer::STATUS_HAPUS, RincianDetailBpPeer::TAHUN, RincianDetailBpPeer::KODE_LOKASI, RincianDetailBpPeer::KECAMATAN, RincianDetailBpPeer::REKENING_CODE_ASLI, RincianDetailBpPeer::NOTE_SKPD, RincianDetailBpPeer::NOTE_PENELITI, RincianDetailBpPeer::NILAI_ANGGARAN, RincianDetailBpPeer::IS_BLUD, RincianDetailBpPeer::LOKASI_KECAMATAN, RincianDetailBpPeer::LOKASI_KELURAHAN, RincianDetailBpPeer::OB, RincianDetailBpPeer::OB_FROM_ID, RincianDetailBpPeer::IS_PER_KOMPONEN, RincianDetailBpPeer::KEGIATAN_CODE_ASAL, RincianDetailBpPeer::TH_KE_MULTIYEARS, RincianDetailBpPeer::HARGA_SEBELUM_SISA_LELANG, RincianDetailBpPeer::IS_MUSRENBANG, RincianDetailBpPeer::SUB_ID_ASAL, RincianDetailBpPeer::SUBTITLE_ASAL, RincianDetailBpPeer::KODE_SUB_ASAL, RincianDetailBpPeer::SUB_ASAL, RincianDetailBpPeer::LAST_EDIT_TIME, RincianDetailBpPeer::IS_POTONG_BPJS, RincianDetailBpPeer::IS_IURAN_BPJS, RincianDetailBpPeer::STATUS_OB, RincianDetailBpPeer::OB_PARENT, RincianDetailBpPeer::OB_ALOKASI_BARU, RincianDetailBpPeer::IS_HIBAH, RincianDetailBpPeer::AKRUAL_CODE, RincianDetailBpPeer::TIPE2, RincianDetailBpPeer::STATUS_LEVEL, RincianDetailBpPeer::STATUS_LEVEL_TOLAK, RincianDetailBpPeer::STATUS_SISIPAN, RincianDetailBpPeer::IS_TAPD_SETUJU, RincianDetailBpPeer::IS_BAPPEKO_SETUJU, RincianDetailBpPeer::IS_PENYELIA_SETUJU, RincianDetailBpPeer::NOTE_TAPD, RincianDetailBpPeer::NOTE_BAPPEKO, RincianDetailBpPeer::SATUAN_SEMULA, RincianDetailBpPeer::ID_LOKASI, RincianDetailBpPeer::DETAIL_KEGIATAN, RincianDetailBpPeer::DETAIL_KEGIATAN_SEMULA, RincianDetailBpPeer::STATUS_KOMPONEN_BARU, RincianDetailBpPeer::STATUS_KOMPONEN_BERUBAH, RincianDetailBpPeer::APPROVE_UNLOCK_HARGA, ),
		BasePeer::TYPE_FIELDNAME => array ('kegiatan_code', 'tipe', 'detail_no', 'rekening_code', 'komponen_id', 'detail_name', 'volume', 'keterangan_koefisien', 'subtitle', 'komponen_harga', 'komponen_harga_awal', 'komponen_name', 'satuan', 'pajak', 'unit_id', 'from_sub_kegiatan', 'sub', 'kode_sub', 'last_update_user', 'last_update_time', 'last_update_ip', 'tahap', 'tahap_edit', 'tahap_new', 'status_lelang', 'nomor_lelang', 'koefisien_semula', 'volume_semula', 'harga_semula', 'total_semula', 'lock_subtitle', 'status_hapus', 'tahun', 'kode_lokasi', 'kecamatan', 'rekening_code_asli', 'note_skpd', 'note_peneliti', 'nilai_anggaran', 'is_blud', 'lokasi_kecamatan', 'lokasi_kelurahan', 'ob', 'ob_from_id', 'is_per_komponen', 'kegiatan_code_asal', 'th_ke_multiyears', 'harga_sebelum_sisa_lelang', 'is_musrenbang', 'sub_id_asal', 'subtitle_asal', 'kode_sub_asal', 'sub_asal', 'last_edit_time', 'is_potong_bpjs', 'is_iuran_bpjs', 'status_ob', 'ob_parent', 'ob_alokasi_baru', 'is_hibah', 'akrual_code', 'tipe2', 'status_level', 'status_level_tolak', 'status_sisipan', 'is_tapd_setuju', 'is_bappeko_setuju', 'is_penyelia_setuju', 'note_tapd', 'note_bappeko', 'satuan_semula', 'id_lokasi', 'detail_kegiatan', 'detail_kegiatan_semula', 'status_komponen_baru', 'status_komponen_berubah', 'approve_unlock_harga', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('KegiatanCode' => 0, 'Tipe' => 1, 'DetailNo' => 2, 'RekeningCode' => 3, 'KomponenId' => 4, 'DetailName' => 5, 'Volume' => 6, 'KeteranganKoefisien' => 7, 'Subtitle' => 8, 'KomponenHarga' => 9, 'KomponenHargaAwal' => 10, 'KomponenName' => 11, 'Satuan' => 12, 'Pajak' => 13, 'UnitId' => 14, 'FromSubKegiatan' => 15, 'Sub' => 16, 'KodeSub' => 17, 'LastUpdateUser' => 18, 'LastUpdateTime' => 19, 'LastUpdateIp' => 20, 'Tahap' => 21, 'TahapEdit' => 22, 'TahapNew' => 23, 'StatusLelang' => 24, 'NomorLelang' => 25, 'KoefisienSemula' => 26, 'VolumeSemula' => 27, 'HargaSemula' => 28, 'TotalSemula' => 29, 'LockSubtitle' => 30, 'StatusHapus' => 31, 'Tahun' => 32, 'KodeLokasi' => 33, 'Kecamatan' => 34, 'RekeningCodeAsli' => 35, 'NoteSkpd' => 36, 'NotePeneliti' => 37, 'NilaiAnggaran' => 38, 'IsBlud' => 39, 'LokasiKecamatan' => 40, 'LokasiKelurahan' => 41, 'Ob' => 42, 'ObFromId' => 43, 'IsPerKomponen' => 44, 'KegiatanCodeAsal' => 45, 'ThKeMultiyears' => 46, 'HargaSebelumSisaLelang' => 47, 'IsMusrenbang' => 48, 'SubIdAsal' => 49, 'SubtitleAsal' => 50, 'KodeSubAsal' => 51, 'SubAsal' => 52, 'LastEditTime' => 53, 'IsPotongBpjs' => 54, 'IsIuranBpjs' => 55, 'StatusOb' => 56, 'ObParent' => 57, 'ObAlokasiBaru' => 58, 'IsHibah' => 59, 'AkrualCode' => 60, 'Tipe2' => 61, 'StatusLevel' => 62, 'StatusLevelTolak' => 63, 'StatusSisipan' => 64, 'IsTapdSetuju' => 65, 'IsBappekoSetuju' => 66, 'IsPenyeliaSetuju' => 67, 'NoteTapd' => 68, 'NoteBappeko' => 69, 'SatuanSemula' => 70, 'IdLokasi' => 71, 'DetailKegiatan' => 72, 'DetailKegiatanSemula' => 73, 'StatusKomponenBaru' => 74, 'StatusKomponenBerubah' => 75, 'ApproveUnlockHarga' => 76, ),
		BasePeer::TYPE_COLNAME => array (RincianDetailBpPeer::KEGIATAN_CODE => 0, RincianDetailBpPeer::TIPE => 1, RincianDetailBpPeer::DETAIL_NO => 2, RincianDetailBpPeer::REKENING_CODE => 3, RincianDetailBpPeer::KOMPONEN_ID => 4, RincianDetailBpPeer::DETAIL_NAME => 5, RincianDetailBpPeer::VOLUME => 6, RincianDetailBpPeer::KETERANGAN_KOEFISIEN => 7, RincianDetailBpPeer::SUBTITLE => 8, RincianDetailBpPeer::KOMPONEN_HARGA => 9, RincianDetailBpPeer::KOMPONEN_HARGA_AWAL => 10, RincianDetailBpPeer::KOMPONEN_NAME => 11, RincianDetailBpPeer::SATUAN => 12, RincianDetailBpPeer::PAJAK => 13, RincianDetailBpPeer::UNIT_ID => 14, RincianDetailBpPeer::FROM_SUB_KEGIATAN => 15, RincianDetailBpPeer::SUB => 16, RincianDetailBpPeer::KODE_SUB => 17, RincianDetailBpPeer::LAST_UPDATE_USER => 18, RincianDetailBpPeer::LAST_UPDATE_TIME => 19, RincianDetailBpPeer::LAST_UPDATE_IP => 20, RincianDetailBpPeer::TAHAP => 21, RincianDetailBpPeer::TAHAP_EDIT => 22, RincianDetailBpPeer::TAHAP_NEW => 23, RincianDetailBpPeer::STATUS_LELANG => 24, RincianDetailBpPeer::NOMOR_LELANG => 25, RincianDetailBpPeer::KOEFISIEN_SEMULA => 26, RincianDetailBpPeer::VOLUME_SEMULA => 27, RincianDetailBpPeer::HARGA_SEMULA => 28, RincianDetailBpPeer::TOTAL_SEMULA => 29, RincianDetailBpPeer::LOCK_SUBTITLE => 30, RincianDetailBpPeer::STATUS_HAPUS => 31, RincianDetailBpPeer::TAHUN => 32, RincianDetailBpPeer::KODE_LOKASI => 33, RincianDetailBpPeer::KECAMATAN => 34, RincianDetailBpPeer::REKENING_CODE_ASLI => 35, RincianDetailBpPeer::NOTE_SKPD => 36, RincianDetailBpPeer::NOTE_PENELITI => 37, RincianDetailBpPeer::NILAI_ANGGARAN => 38, RincianDetailBpPeer::IS_BLUD => 39, RincianDetailBpPeer::LOKASI_KECAMATAN => 40, RincianDetailBpPeer::LOKASI_KELURAHAN => 41, RincianDetailBpPeer::OB => 42, RincianDetailBpPeer::OB_FROM_ID => 43, RincianDetailBpPeer::IS_PER_KOMPONEN => 44, RincianDetailBpPeer::KEGIATAN_CODE_ASAL => 45, RincianDetailBpPeer::TH_KE_MULTIYEARS => 46, RincianDetailBpPeer::HARGA_SEBELUM_SISA_LELANG => 47, RincianDetailBpPeer::IS_MUSRENBANG => 48, RincianDetailBpPeer::SUB_ID_ASAL => 49, RincianDetailBpPeer::SUBTITLE_ASAL => 50, RincianDetailBpPeer::KODE_SUB_ASAL => 51, RincianDetailBpPeer::SUB_ASAL => 52, RincianDetailBpPeer::LAST_EDIT_TIME => 53, RincianDetailBpPeer::IS_POTONG_BPJS => 54, RincianDetailBpPeer::IS_IURAN_BPJS => 55, RincianDetailBpPeer::STATUS_OB => 56, RincianDetailBpPeer::OB_PARENT => 57, RincianDetailBpPeer::OB_ALOKASI_BARU => 58, RincianDetailBpPeer::IS_HIBAH => 59, RincianDetailBpPeer::AKRUAL_CODE => 60, RincianDetailBpPeer::TIPE2 => 61, RincianDetailBpPeer::STATUS_LEVEL => 62, RincianDetailBpPeer::STATUS_LEVEL_TOLAK => 63, RincianDetailBpPeer::STATUS_SISIPAN => 64, RincianDetailBpPeer::IS_TAPD_SETUJU => 65, RincianDetailBpPeer::IS_BAPPEKO_SETUJU => 66, RincianDetailBpPeer::IS_PENYELIA_SETUJU => 67, RincianDetailBpPeer::NOTE_TAPD => 68, RincianDetailBpPeer::NOTE_BAPPEKO => 69, RincianDetailBpPeer::SATUAN_SEMULA => 70, RincianDetailBpPeer::ID_LOKASI => 71, RincianDetailBpPeer::DETAIL_KEGIATAN => 72, RincianDetailBpPeer::DETAIL_KEGIATAN_SEMULA => 73, RincianDetailBpPeer::STATUS_KOMPONEN_BARU => 74, RincianDetailBpPeer::STATUS_KOMPONEN_BERUBAH => 75, RincianDetailBpPeer::APPROVE_UNLOCK_HARGA => 76, ),
		BasePeer::TYPE_FIELDNAME => array ('kegiatan_code' => 0, 'tipe' => 1, 'detail_no' => 2, 'rekening_code' => 3, 'komponen_id' => 4, 'detail_name' => 5, 'volume' => 6, 'keterangan_koefisien' => 7, 'subtitle' => 8, 'komponen_harga' => 9, 'komponen_harga_awal' => 10, 'komponen_name' => 11, 'satuan' => 12, 'pajak' => 13, 'unit_id' => 14, 'from_sub_kegiatan' => 15, 'sub' => 16, 'kode_sub' => 17, 'last_update_user' => 18, 'last_update_time' => 19, 'last_update_ip' => 20, 'tahap' => 21, 'tahap_edit' => 22, 'tahap_new' => 23, 'status_lelang' => 24, 'nomor_lelang' => 25, 'koefisien_semula' => 26, 'volume_semula' => 27, 'harga_semula' => 28, 'total_semula' => 29, 'lock_subtitle' => 30, 'status_hapus' => 31, 'tahun' => 32, 'kode_lokasi' => 33, 'kecamatan' => 34, 'rekening_code_asli' => 35, 'note_skpd' => 36, 'note_peneliti' => 37, 'nilai_anggaran' => 38, 'is_blud' => 39, 'lokasi_kecamatan' => 40, 'lokasi_kelurahan' => 41, 'ob' => 42, 'ob_from_id' => 43, 'is_per_komponen' => 44, 'kegiatan_code_asal' => 45, 'th_ke_multiyears' => 46, 'harga_sebelum_sisa_lelang' => 47, 'is_musrenbang' => 48, 'sub_id_asal' => 49, 'subtitle_asal' => 50, 'kode_sub_asal' => 51, 'sub_asal' => 52, 'last_edit_time' => 53, 'is_potong_bpjs' => 54, 'is_iuran_bpjs' => 55, 'status_ob' => 56, 'ob_parent' => 57, 'ob_alokasi_baru' => 58, 'is_hibah' => 59, 'akrual_code' => 60, 'tipe2' => 61, 'status_level' => 62, 'status_level_tolak' => 63, 'status_sisipan' => 64, 'is_tapd_setuju' => 65, 'is_bappeko_setuju' => 66, 'is_penyelia_setuju' => 67, 'note_tapd' => 68, 'note_bappeko' => 69, 'satuan_semula' => 70, 'id_lokasi' => 71, 'detail_kegiatan' => 72, 'detail_kegiatan_semula' => 73, 'status_komponen_baru' => 74, 'status_komponen_berubah' => 75, 'approve_unlock_harga' => 76, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/RincianDetailBpMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.RincianDetailBpMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = RincianDetailBpPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(RincianDetailBpPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(RincianDetailBpPeer::KEGIATAN_CODE);

		$criteria->addSelectColumn(RincianDetailBpPeer::TIPE);

		$criteria->addSelectColumn(RincianDetailBpPeer::DETAIL_NO);

		$criteria->addSelectColumn(RincianDetailBpPeer::REKENING_CODE);

		$criteria->addSelectColumn(RincianDetailBpPeer::KOMPONEN_ID);

		$criteria->addSelectColumn(RincianDetailBpPeer::DETAIL_NAME);

		$criteria->addSelectColumn(RincianDetailBpPeer::VOLUME);

		$criteria->addSelectColumn(RincianDetailBpPeer::KETERANGAN_KOEFISIEN);

		$criteria->addSelectColumn(RincianDetailBpPeer::SUBTITLE);

		$criteria->addSelectColumn(RincianDetailBpPeer::KOMPONEN_HARGA);

		$criteria->addSelectColumn(RincianDetailBpPeer::KOMPONEN_HARGA_AWAL);

		$criteria->addSelectColumn(RincianDetailBpPeer::KOMPONEN_NAME);

		$criteria->addSelectColumn(RincianDetailBpPeer::SATUAN);

		$criteria->addSelectColumn(RincianDetailBpPeer::PAJAK);

		$criteria->addSelectColumn(RincianDetailBpPeer::UNIT_ID);

		$criteria->addSelectColumn(RincianDetailBpPeer::FROM_SUB_KEGIATAN);

		$criteria->addSelectColumn(RincianDetailBpPeer::SUB);

		$criteria->addSelectColumn(RincianDetailBpPeer::KODE_SUB);

		$criteria->addSelectColumn(RincianDetailBpPeer::LAST_UPDATE_USER);

		$criteria->addSelectColumn(RincianDetailBpPeer::LAST_UPDATE_TIME);

		$criteria->addSelectColumn(RincianDetailBpPeer::LAST_UPDATE_IP);

		$criteria->addSelectColumn(RincianDetailBpPeer::TAHAP);

		$criteria->addSelectColumn(RincianDetailBpPeer::TAHAP_EDIT);

		$criteria->addSelectColumn(RincianDetailBpPeer::TAHAP_NEW);

		$criteria->addSelectColumn(RincianDetailBpPeer::STATUS_LELANG);

		$criteria->addSelectColumn(RincianDetailBpPeer::NOMOR_LELANG);

		$criteria->addSelectColumn(RincianDetailBpPeer::KOEFISIEN_SEMULA);

		$criteria->addSelectColumn(RincianDetailBpPeer::VOLUME_SEMULA);

		$criteria->addSelectColumn(RincianDetailBpPeer::HARGA_SEMULA);

		$criteria->addSelectColumn(RincianDetailBpPeer::TOTAL_SEMULA);

		$criteria->addSelectColumn(RincianDetailBpPeer::LOCK_SUBTITLE);

		$criteria->addSelectColumn(RincianDetailBpPeer::STATUS_HAPUS);

		$criteria->addSelectColumn(RincianDetailBpPeer::TAHUN);

		$criteria->addSelectColumn(RincianDetailBpPeer::KODE_LOKASI);

		$criteria->addSelectColumn(RincianDetailBpPeer::KECAMATAN);

		$criteria->addSelectColumn(RincianDetailBpPeer::REKENING_CODE_ASLI);

		$criteria->addSelectColumn(RincianDetailBpPeer::NOTE_SKPD);

		$criteria->addSelectColumn(RincianDetailBpPeer::NOTE_PENELITI);

		$criteria->addSelectColumn(RincianDetailBpPeer::NILAI_ANGGARAN);

		$criteria->addSelectColumn(RincianDetailBpPeer::IS_BLUD);

		$criteria->addSelectColumn(RincianDetailBpPeer::LOKASI_KECAMATAN);

		$criteria->addSelectColumn(RincianDetailBpPeer::LOKASI_KELURAHAN);

		$criteria->addSelectColumn(RincianDetailBpPeer::OB);

		$criteria->addSelectColumn(RincianDetailBpPeer::OB_FROM_ID);

		$criteria->addSelectColumn(RincianDetailBpPeer::IS_PER_KOMPONEN);

		$criteria->addSelectColumn(RincianDetailBpPeer::KEGIATAN_CODE_ASAL);

		$criteria->addSelectColumn(RincianDetailBpPeer::TH_KE_MULTIYEARS);

		$criteria->addSelectColumn(RincianDetailBpPeer::HARGA_SEBELUM_SISA_LELANG);

		$criteria->addSelectColumn(RincianDetailBpPeer::IS_MUSRENBANG);

		$criteria->addSelectColumn(RincianDetailBpPeer::SUB_ID_ASAL);

		$criteria->addSelectColumn(RincianDetailBpPeer::SUBTITLE_ASAL);

		$criteria->addSelectColumn(RincianDetailBpPeer::KODE_SUB_ASAL);

		$criteria->addSelectColumn(RincianDetailBpPeer::SUB_ASAL);

		$criteria->addSelectColumn(RincianDetailBpPeer::LAST_EDIT_TIME);

		$criteria->addSelectColumn(RincianDetailBpPeer::IS_POTONG_BPJS);

		$criteria->addSelectColumn(RincianDetailBpPeer::IS_IURAN_BPJS);

		$criteria->addSelectColumn(RincianDetailBpPeer::STATUS_OB);

		$criteria->addSelectColumn(RincianDetailBpPeer::OB_PARENT);

		$criteria->addSelectColumn(RincianDetailBpPeer::OB_ALOKASI_BARU);

		$criteria->addSelectColumn(RincianDetailBpPeer::IS_HIBAH);

		$criteria->addSelectColumn(RincianDetailBpPeer::AKRUAL_CODE);

		$criteria->addSelectColumn(RincianDetailBpPeer::TIPE2);

		$criteria->addSelectColumn(RincianDetailBpPeer::STATUS_LEVEL);

		$criteria->addSelectColumn(RincianDetailBpPeer::STATUS_LEVEL_TOLAK);

		$criteria->addSelectColumn(RincianDetailBpPeer::STATUS_SISIPAN);

		$criteria->addSelectColumn(RincianDetailBpPeer::IS_TAPD_SETUJU);

		$criteria->addSelectColumn(RincianDetailBpPeer::IS_BAPPEKO_SETUJU);

		$criteria->addSelectColumn(RincianDetailBpPeer::IS_PENYELIA_SETUJU);

		$criteria->addSelectColumn(RincianDetailBpPeer::NOTE_TAPD);

		$criteria->addSelectColumn(RincianDetailBpPeer::NOTE_BAPPEKO);

		$criteria->addSelectColumn(RincianDetailBpPeer::SATUAN_SEMULA);

		$criteria->addSelectColumn(RincianDetailBpPeer::ID_LOKASI);

		$criteria->addSelectColumn(RincianDetailBpPeer::DETAIL_KEGIATAN);

		$criteria->addSelectColumn(RincianDetailBpPeer::DETAIL_KEGIATAN_SEMULA);

		$criteria->addSelectColumn(RincianDetailBpPeer::STATUS_KOMPONEN_BARU);

		$criteria->addSelectColumn(RincianDetailBpPeer::STATUS_KOMPONEN_BERUBAH);

		$criteria->addSelectColumn(RincianDetailBpPeer::APPROVE_UNLOCK_HARGA);

	}

	const COUNT = 'COUNT(ebudget.rincian_detail_bp.KEGIATAN_CODE)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.rincian_detail_bp.KEGIATAN_CODE)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(RincianDetailBpPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(RincianDetailBpPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = RincianDetailBpPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = RincianDetailBpPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return RincianDetailBpPeer::populateObjects(RincianDetailBpPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			RincianDetailBpPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = RincianDetailBpPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return RincianDetailBpPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(RincianDetailBpPeer::KEGIATAN_CODE);
			$selectCriteria->add(RincianDetailBpPeer::KEGIATAN_CODE, $criteria->remove(RincianDetailBpPeer::KEGIATAN_CODE), $comparison);

			$comparison = $criteria->getComparison(RincianDetailBpPeer::DETAIL_NO);
			$selectCriteria->add(RincianDetailBpPeer::DETAIL_NO, $criteria->remove(RincianDetailBpPeer::DETAIL_NO), $comparison);

			$comparison = $criteria->getComparison(RincianDetailBpPeer::UNIT_ID);
			$selectCriteria->add(RincianDetailBpPeer::UNIT_ID, $criteria->remove(RincianDetailBpPeer::UNIT_ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(RincianDetailBpPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(RincianDetailBpPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof RincianDetailBp) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
												if(count($values) == count($values, COUNT_RECURSIVE))
			{
								$values = array($values);
			}
			$vals = array();
			foreach($values as $value)
			{

				$vals[0][] = $value[0];
				$vals[1][] = $value[1];
				$vals[2][] = $value[2];
			}

			$criteria->add(RincianDetailBpPeer::KEGIATAN_CODE, $vals[0], Criteria::IN);
			$criteria->add(RincianDetailBpPeer::DETAIL_NO, $vals[1], Criteria::IN);
			$criteria->add(RincianDetailBpPeer::UNIT_ID, $vals[2], Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(RincianDetailBp $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(RincianDetailBpPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(RincianDetailBpPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(RincianDetailBpPeer::DATABASE_NAME, RincianDetailBpPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = RincianDetailBpPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK( $kegiatan_code, $detail_no, $unit_id, $con = null) {
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$criteria = new Criteria();
		$criteria->add(RincianDetailBpPeer::KEGIATAN_CODE, $kegiatan_code);
		$criteria->add(RincianDetailBpPeer::DETAIL_NO, $detail_no);
		$criteria->add(RincianDetailBpPeer::UNIT_ID, $unit_id);
		$v = RincianDetailBpPeer::doSelect($criteria, $con);

		return !empty($v) ? $v[0] : null;
	}
} 
if (Propel::isInit()) {
			try {
		BaseRincianDetailBpPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/RincianDetailBpMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.RincianDetailBpMapBuilder');
}
