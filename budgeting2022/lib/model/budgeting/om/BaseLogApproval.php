<?php


abstract class BaseLogApproval extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id;


	
	protected $unit_id;


	
	protected $kegiatan_code;


	
	protected $user_id;


	
	protected $waktu;


	
	protected $kumpulan_detail_no;


	
	protected $sebagai;


	
	protected $tahap;


	
	protected $catatan;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getId()
	{

		return $this->id;
	}

	
	public function getUnitId()
	{

		return $this->unit_id;
	}

	
	public function getKegiatanCode()
	{

		return $this->kegiatan_code;
	}

	
	public function getUserId()
	{

		return $this->user_id;
	}

	
	public function getWaktu($format = 'Y-m-d H:i:s')
	{

		if ($this->waktu === null || $this->waktu === '') {
			return null;
		} elseif (!is_int($this->waktu)) {
						$ts = strtotime($this->waktu);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [waktu] as date/time value: " . var_export($this->waktu, true));
			}
		} else {
			$ts = $this->waktu;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getKumpulanDetailNo()
	{

		return $this->kumpulan_detail_no;
	}

	
	public function getSebagai()
	{

		return $this->sebagai;
	}

	
	public function getTahap()
	{

		return $this->tahap;
	}

	
	public function getCatatan()
	{

		return $this->catatan;
	}

	
	public function setId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = LogApprovalPeer::ID;
		}

	} 
	
	public function setUnitId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->unit_id !== $v) {
			$this->unit_id = $v;
			$this->modifiedColumns[] = LogApprovalPeer::UNIT_ID;
		}

	} 
	
	public function setKegiatanCode($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kegiatan_code !== $v) {
			$this->kegiatan_code = $v;
			$this->modifiedColumns[] = LogApprovalPeer::KEGIATAN_CODE;
		}

	} 
	
	public function setUserId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->user_id !== $v) {
			$this->user_id = $v;
			$this->modifiedColumns[] = LogApprovalPeer::USER_ID;
		}

	} 
	
	public function setWaktu($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [waktu] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->waktu !== $ts) {
			$this->waktu = $ts;
			$this->modifiedColumns[] = LogApprovalPeer::WAKTU;
		}

	} 
	
	public function setKumpulanDetailNo($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kumpulan_detail_no !== $v) {
			$this->kumpulan_detail_no = $v;
			$this->modifiedColumns[] = LogApprovalPeer::KUMPULAN_DETAIL_NO;
		}

	} 
	
	public function setSebagai($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->sebagai !== $v) {
			$this->sebagai = $v;
			$this->modifiedColumns[] = LogApprovalPeer::SEBAGAI;
		}

	} 
	
	public function setTahap($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->tahap !== $v) {
			$this->tahap = $v;
			$this->modifiedColumns[] = LogApprovalPeer::TAHAP;
		}

	} 
	
	public function setCatatan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->catatan !== $v) {
			$this->catatan = $v;
			$this->modifiedColumns[] = LogApprovalPeer::CATATAN;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getString($startcol + 0);

			$this->unit_id = $rs->getString($startcol + 1);

			$this->kegiatan_code = $rs->getString($startcol + 2);

			$this->user_id = $rs->getString($startcol + 3);

			$this->waktu = $rs->getTimestamp($startcol + 4, null);

			$this->kumpulan_detail_no = $rs->getString($startcol + 5);

			$this->sebagai = $rs->getString($startcol + 6);

			$this->tahap = $rs->getString($startcol + 7);

			$this->catatan = $rs->getString($startcol + 8);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 9; 
		} catch (Exception $e) {
			throw new PropelException("Error populating LogApproval object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(LogApprovalPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			LogApprovalPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(LogApprovalPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = LogApprovalPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += LogApprovalPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = LogApprovalPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = LogApprovalPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getUnitId();
				break;
			case 2:
				return $this->getKegiatanCode();
				break;
			case 3:
				return $this->getUserId();
				break;
			case 4:
				return $this->getWaktu();
				break;
			case 5:
				return $this->getKumpulanDetailNo();
				break;
			case 6:
				return $this->getSebagai();
				break;
			case 7:
				return $this->getTahap();
				break;
			case 8:
				return $this->getCatatan();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = LogApprovalPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getUnitId(),
			$keys[2] => $this->getKegiatanCode(),
			$keys[3] => $this->getUserId(),
			$keys[4] => $this->getWaktu(),
			$keys[5] => $this->getKumpulanDetailNo(),
			$keys[6] => $this->getSebagai(),
			$keys[7] => $this->getTahap(),
			$keys[8] => $this->getCatatan(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = LogApprovalPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setUnitId($value);
				break;
			case 2:
				$this->setKegiatanCode($value);
				break;
			case 3:
				$this->setUserId($value);
				break;
			case 4:
				$this->setWaktu($value);
				break;
			case 5:
				$this->setKumpulanDetailNo($value);
				break;
			case 6:
				$this->setSebagai($value);
				break;
			case 7:
				$this->setTahap($value);
				break;
			case 8:
				$this->setCatatan($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = LogApprovalPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setUnitId($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setKegiatanCode($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setUserId($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setWaktu($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setKumpulanDetailNo($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setSebagai($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setTahap($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setCatatan($arr[$keys[8]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(LogApprovalPeer::DATABASE_NAME);

		if ($this->isColumnModified(LogApprovalPeer::ID)) $criteria->add(LogApprovalPeer::ID, $this->id);
		if ($this->isColumnModified(LogApprovalPeer::UNIT_ID)) $criteria->add(LogApprovalPeer::UNIT_ID, $this->unit_id);
		if ($this->isColumnModified(LogApprovalPeer::KEGIATAN_CODE)) $criteria->add(LogApprovalPeer::KEGIATAN_CODE, $this->kegiatan_code);
		if ($this->isColumnModified(LogApprovalPeer::USER_ID)) $criteria->add(LogApprovalPeer::USER_ID, $this->user_id);
		if ($this->isColumnModified(LogApprovalPeer::WAKTU)) $criteria->add(LogApprovalPeer::WAKTU, $this->waktu);
		if ($this->isColumnModified(LogApprovalPeer::KUMPULAN_DETAIL_NO)) $criteria->add(LogApprovalPeer::KUMPULAN_DETAIL_NO, $this->kumpulan_detail_no);
		if ($this->isColumnModified(LogApprovalPeer::SEBAGAI)) $criteria->add(LogApprovalPeer::SEBAGAI, $this->sebagai);
		if ($this->isColumnModified(LogApprovalPeer::TAHAP)) $criteria->add(LogApprovalPeer::TAHAP, $this->tahap);
		if ($this->isColumnModified(LogApprovalPeer::CATATAN)) $criteria->add(LogApprovalPeer::CATATAN, $this->catatan);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(LogApprovalPeer::DATABASE_NAME);

		$criteria->add(LogApprovalPeer::ID, $this->id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setUnitId($this->unit_id);

		$copyObj->setKegiatanCode($this->kegiatan_code);

		$copyObj->setUserId($this->user_id);

		$copyObj->setWaktu($this->waktu);

		$copyObj->setKumpulanDetailNo($this->kumpulan_detail_no);

		$copyObj->setSebagai($this->sebagai);

		$copyObj->setTahap($this->tahap);

		$copyObj->setCatatan($this->catatan);


		$copyObj->setNew(true);

		$copyObj->setId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new LogApprovalPeer();
		}
		return self::$peer;
	}

} 