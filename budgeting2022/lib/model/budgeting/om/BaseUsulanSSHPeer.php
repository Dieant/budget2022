<?php


abstract class BaseUsulanSSHPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.usulan_ssh';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.UsulanSSH';

	
	const NUM_COLUMNS = 35;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const ID_USULAN = 'ebudget.usulan_ssh.ID_USULAN';

	
	const CREATED_AT = 'ebudget.usulan_ssh.CREATED_AT';

	
	const UPDATED_AT = 'ebudget.usulan_ssh.UPDATED_AT';

	
	const NAMA = 'ebudget.usulan_ssh.NAMA';

	
	const SPEC = 'ebudget.usulan_ssh.SPEC';

	
	const HIDDEN_SPEC = 'ebudget.usulan_ssh.HIDDEN_SPEC';

	
	const MEREK = 'ebudget.usulan_ssh.MEREK';

	
	const SATUAN = 'ebudget.usulan_ssh.SATUAN';

	
	const HARGA = 'ebudget.usulan_ssh.HARGA';

	
	const REKENING = 'ebudget.usulan_ssh.REKENING';

	
	const PAJAK = 'ebudget.usulan_ssh.PAJAK';

	
	const KETERANGAN = 'ebudget.usulan_ssh.KETERANGAN';

	
	const KODE_BARANG = 'ebudget.usulan_ssh.KODE_BARANG';

	
	const ID_SPJM = 'ebudget.usulan_ssh.ID_SPJM';

	
	const STATUS_VERIFIKASI = 'ebudget.usulan_ssh.STATUS_VERIFIKASI';

	
	const STATUS_HAPUS = 'ebudget.usulan_ssh.STATUS_HAPUS';

	
	const TIPE_USULAN = 'ebudget.usulan_ssh.TIPE_USULAN';

	
	const UNIT_ID = 'ebudget.usulan_ssh.UNIT_ID';

	
	const SHSD_ID = 'ebudget.usulan_ssh.SHSD_ID';

	
	const STATUS_CONFIRMASI_PENYELIA = 'ebudget.usulan_ssh.STATUS_CONFIRMASI_PENYELIA';

	
	const KOMENTAR_VERIFIKATOR = 'ebudget.usulan_ssh.KOMENTAR_VERIFIKATOR';

	
	const NAMA_SEBELUM = 'ebudget.usulan_ssh.NAMA_SEBELUM';

	
	const HARGA_SEBELUM = 'ebudget.usulan_ssh.HARGA_SEBELUM';

	
	const ALASAN_PERUBAHAN_HARGA = 'ebudget.usulan_ssh.ALASAN_PERUBAHAN_HARGA';

	
	const IS_PERUBAHAN_HARGA = 'ebudget.usulan_ssh.IS_PERUBAHAN_HARGA';

	
	const IS_PERBEDAAN_PENDUKUNG = 'ebudget.usulan_ssh.IS_PERBEDAAN_PENDUKUNG';

	
	const ALASAN_PERBEDAAN_DINAS = 'ebudget.usulan_ssh.ALASAN_PERBEDAAN_DINAS';

	
	const ALASAN_PERBEDAAN_PENYELIA = 'ebudget.usulan_ssh.ALASAN_PERBEDAAN_PENYELIA';

	
	const HARGA_PENDUKUNG1 = 'ebudget.usulan_ssh.HARGA_PENDUKUNG1';

	
	const HARGA_PENDUKUNG2 = 'ebudget.usulan_ssh.HARGA_PENDUKUNG2';

	
	const HARGA_PENDUKUNG3 = 'ebudget.usulan_ssh.HARGA_PENDUKUNG3';

	
	const TAHAP = 'ebudget.usulan_ssh.TAHAP';

	
	const STATUS_BARU = 'ebudget.usulan_ssh.STATUS_BARU';

	
	const STATUS_PENDING = 'ebudget.usulan_ssh.STATUS_PENDING';

	
	const KOMPONEN_TIPE2 = 'ebudget.usulan_ssh.KOMPONEN_TIPE2';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('IdUsulan', 'CreatedAt', 'UpdatedAt', 'Nama', 'Spec', 'HiddenSpec', 'Merek', 'Satuan', 'Harga', 'Rekening', 'Pajak', 'Keterangan', 'KodeBarang', 'IdSpjm', 'StatusVerifikasi', 'StatusHapus', 'TipeUsulan', 'UnitId', 'ShsdId', 'StatusConfirmasiPenyelia', 'KomentarVerifikator', 'NamaSebelum', 'HargaSebelum', 'AlasanPerubahanHarga', 'IsPerubahanHarga', 'IsPerbedaanPendukung', 'AlasanPerbedaanDinas', 'AlasanPerbedaanPenyelia', 'HargaPendukung1', 'HargaPendukung2', 'HargaPendukung3', 'Tahap', 'StatusBaru', 'StatusPending', 'KomponenTipe2', ),
		BasePeer::TYPE_COLNAME => array (UsulanSSHPeer::ID_USULAN, UsulanSSHPeer::CREATED_AT, UsulanSSHPeer::UPDATED_AT, UsulanSSHPeer::NAMA, UsulanSSHPeer::SPEC, UsulanSSHPeer::HIDDEN_SPEC, UsulanSSHPeer::MEREK, UsulanSSHPeer::SATUAN, UsulanSSHPeer::HARGA, UsulanSSHPeer::REKENING, UsulanSSHPeer::PAJAK, UsulanSSHPeer::KETERANGAN, UsulanSSHPeer::KODE_BARANG, UsulanSSHPeer::ID_SPJM, UsulanSSHPeer::STATUS_VERIFIKASI, UsulanSSHPeer::STATUS_HAPUS, UsulanSSHPeer::TIPE_USULAN, UsulanSSHPeer::UNIT_ID, UsulanSSHPeer::SHSD_ID, UsulanSSHPeer::STATUS_CONFIRMASI_PENYELIA, UsulanSSHPeer::KOMENTAR_VERIFIKATOR, UsulanSSHPeer::NAMA_SEBELUM, UsulanSSHPeer::HARGA_SEBELUM, UsulanSSHPeer::ALASAN_PERUBAHAN_HARGA, UsulanSSHPeer::IS_PERUBAHAN_HARGA, UsulanSSHPeer::IS_PERBEDAAN_PENDUKUNG, UsulanSSHPeer::ALASAN_PERBEDAAN_DINAS, UsulanSSHPeer::ALASAN_PERBEDAAN_PENYELIA, UsulanSSHPeer::HARGA_PENDUKUNG1, UsulanSSHPeer::HARGA_PENDUKUNG2, UsulanSSHPeer::HARGA_PENDUKUNG3, UsulanSSHPeer::TAHAP, UsulanSSHPeer::STATUS_BARU, UsulanSSHPeer::STATUS_PENDING, UsulanSSHPeer::KOMPONEN_TIPE2, ),
		BasePeer::TYPE_FIELDNAME => array ('id_usulan', 'created_at', 'updated_at', 'nama', 'spec', 'hidden_spec', 'merek', 'satuan', 'harga', 'rekening', 'pajak', 'keterangan', 'kode_barang', 'id_spjm', 'status_verifikasi', 'status_hapus', 'tipe_usulan', 'unit_id', 'shsd_id', 'status_confirmasi_penyelia', 'komentar_verifikator', 'nama_sebelum', 'harga_sebelum', 'alasan_perubahan_harga', 'is_perubahan_harga', 'is_perbedaan_pendukung', 'alasan_perbedaan_dinas', 'alasan_perbedaan_penyelia', 'harga_pendukung1', 'harga_pendukung2', 'harga_pendukung3', 'tahap', 'status_baru', 'status_pending', 'komponen_tipe2', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('IdUsulan' => 0, 'CreatedAt' => 1, 'UpdatedAt' => 2, 'Nama' => 3, 'Spec' => 4, 'HiddenSpec' => 5, 'Merek' => 6, 'Satuan' => 7, 'Harga' => 8, 'Rekening' => 9, 'Pajak' => 10, 'Keterangan' => 11, 'KodeBarang' => 12, 'IdSpjm' => 13, 'StatusVerifikasi' => 14, 'StatusHapus' => 15, 'TipeUsulan' => 16, 'UnitId' => 17, 'ShsdId' => 18, 'StatusConfirmasiPenyelia' => 19, 'KomentarVerifikator' => 20, 'NamaSebelum' => 21, 'HargaSebelum' => 22, 'AlasanPerubahanHarga' => 23, 'IsPerubahanHarga' => 24, 'IsPerbedaanPendukung' => 25, 'AlasanPerbedaanDinas' => 26, 'AlasanPerbedaanPenyelia' => 27, 'HargaPendukung1' => 28, 'HargaPendukung2' => 29, 'HargaPendukung3' => 30, 'Tahap' => 31, 'StatusBaru' => 32, 'StatusPending' => 33, 'KomponenTipe2' => 34, ),
		BasePeer::TYPE_COLNAME => array (UsulanSSHPeer::ID_USULAN => 0, UsulanSSHPeer::CREATED_AT => 1, UsulanSSHPeer::UPDATED_AT => 2, UsulanSSHPeer::NAMA => 3, UsulanSSHPeer::SPEC => 4, UsulanSSHPeer::HIDDEN_SPEC => 5, UsulanSSHPeer::MEREK => 6, UsulanSSHPeer::SATUAN => 7, UsulanSSHPeer::HARGA => 8, UsulanSSHPeer::REKENING => 9, UsulanSSHPeer::PAJAK => 10, UsulanSSHPeer::KETERANGAN => 11, UsulanSSHPeer::KODE_BARANG => 12, UsulanSSHPeer::ID_SPJM => 13, UsulanSSHPeer::STATUS_VERIFIKASI => 14, UsulanSSHPeer::STATUS_HAPUS => 15, UsulanSSHPeer::TIPE_USULAN => 16, UsulanSSHPeer::UNIT_ID => 17, UsulanSSHPeer::SHSD_ID => 18, UsulanSSHPeer::STATUS_CONFIRMASI_PENYELIA => 19, UsulanSSHPeer::KOMENTAR_VERIFIKATOR => 20, UsulanSSHPeer::NAMA_SEBELUM => 21, UsulanSSHPeer::HARGA_SEBELUM => 22, UsulanSSHPeer::ALASAN_PERUBAHAN_HARGA => 23, UsulanSSHPeer::IS_PERUBAHAN_HARGA => 24, UsulanSSHPeer::IS_PERBEDAAN_PENDUKUNG => 25, UsulanSSHPeer::ALASAN_PERBEDAAN_DINAS => 26, UsulanSSHPeer::ALASAN_PERBEDAAN_PENYELIA => 27, UsulanSSHPeer::HARGA_PENDUKUNG1 => 28, UsulanSSHPeer::HARGA_PENDUKUNG2 => 29, UsulanSSHPeer::HARGA_PENDUKUNG3 => 30, UsulanSSHPeer::TAHAP => 31, UsulanSSHPeer::STATUS_BARU => 32, UsulanSSHPeer::STATUS_PENDING => 33, UsulanSSHPeer::KOMPONEN_TIPE2 => 34, ),
		BasePeer::TYPE_FIELDNAME => array ('id_usulan' => 0, 'created_at' => 1, 'updated_at' => 2, 'nama' => 3, 'spec' => 4, 'hidden_spec' => 5, 'merek' => 6, 'satuan' => 7, 'harga' => 8, 'rekening' => 9, 'pajak' => 10, 'keterangan' => 11, 'kode_barang' => 12, 'id_spjm' => 13, 'status_verifikasi' => 14, 'status_hapus' => 15, 'tipe_usulan' => 16, 'unit_id' => 17, 'shsd_id' => 18, 'status_confirmasi_penyelia' => 19, 'komentar_verifikator' => 20, 'nama_sebelum' => 21, 'harga_sebelum' => 22, 'alasan_perubahan_harga' => 23, 'is_perubahan_harga' => 24, 'is_perbedaan_pendukung' => 25, 'alasan_perbedaan_dinas' => 26, 'alasan_perbedaan_penyelia' => 27, 'harga_pendukung1' => 28, 'harga_pendukung2' => 29, 'harga_pendukung3' => 30, 'tahap' => 31, 'status_baru' => 32, 'status_pending' => 33, 'komponen_tipe2' => 34, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/UsulanSSHMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.UsulanSSHMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = UsulanSSHPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(UsulanSSHPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(UsulanSSHPeer::ID_USULAN);

		$criteria->addSelectColumn(UsulanSSHPeer::CREATED_AT);

		$criteria->addSelectColumn(UsulanSSHPeer::UPDATED_AT);

		$criteria->addSelectColumn(UsulanSSHPeer::NAMA);

		$criteria->addSelectColumn(UsulanSSHPeer::SPEC);

		$criteria->addSelectColumn(UsulanSSHPeer::HIDDEN_SPEC);

		$criteria->addSelectColumn(UsulanSSHPeer::MEREK);

		$criteria->addSelectColumn(UsulanSSHPeer::SATUAN);

		$criteria->addSelectColumn(UsulanSSHPeer::HARGA);

		$criteria->addSelectColumn(UsulanSSHPeer::REKENING);

		$criteria->addSelectColumn(UsulanSSHPeer::PAJAK);

		$criteria->addSelectColumn(UsulanSSHPeer::KETERANGAN);

		$criteria->addSelectColumn(UsulanSSHPeer::KODE_BARANG);

		$criteria->addSelectColumn(UsulanSSHPeer::ID_SPJM);

		$criteria->addSelectColumn(UsulanSSHPeer::STATUS_VERIFIKASI);

		$criteria->addSelectColumn(UsulanSSHPeer::STATUS_HAPUS);

		$criteria->addSelectColumn(UsulanSSHPeer::TIPE_USULAN);

		$criteria->addSelectColumn(UsulanSSHPeer::UNIT_ID);

		$criteria->addSelectColumn(UsulanSSHPeer::SHSD_ID);

		$criteria->addSelectColumn(UsulanSSHPeer::STATUS_CONFIRMASI_PENYELIA);

		$criteria->addSelectColumn(UsulanSSHPeer::KOMENTAR_VERIFIKATOR);

		$criteria->addSelectColumn(UsulanSSHPeer::NAMA_SEBELUM);

		$criteria->addSelectColumn(UsulanSSHPeer::HARGA_SEBELUM);

		$criteria->addSelectColumn(UsulanSSHPeer::ALASAN_PERUBAHAN_HARGA);

		$criteria->addSelectColumn(UsulanSSHPeer::IS_PERUBAHAN_HARGA);

		$criteria->addSelectColumn(UsulanSSHPeer::IS_PERBEDAAN_PENDUKUNG);

		$criteria->addSelectColumn(UsulanSSHPeer::ALASAN_PERBEDAAN_DINAS);

		$criteria->addSelectColumn(UsulanSSHPeer::ALASAN_PERBEDAAN_PENYELIA);

		$criteria->addSelectColumn(UsulanSSHPeer::HARGA_PENDUKUNG1);

		$criteria->addSelectColumn(UsulanSSHPeer::HARGA_PENDUKUNG2);

		$criteria->addSelectColumn(UsulanSSHPeer::HARGA_PENDUKUNG3);

		$criteria->addSelectColumn(UsulanSSHPeer::TAHAP);

		$criteria->addSelectColumn(UsulanSSHPeer::STATUS_BARU);

		$criteria->addSelectColumn(UsulanSSHPeer::STATUS_PENDING);

		$criteria->addSelectColumn(UsulanSSHPeer::KOMPONEN_TIPE2);

	}

	const COUNT = 'COUNT(ebudget.usulan_ssh.ID_USULAN)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.usulan_ssh.ID_USULAN)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(UsulanSSHPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(UsulanSSHPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = UsulanSSHPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = UsulanSSHPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return UsulanSSHPeer::populateObjects(UsulanSSHPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			UsulanSSHPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = UsulanSSHPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return UsulanSSHPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(UsulanSSHPeer::ID_USULAN);
			$selectCriteria->add(UsulanSSHPeer::ID_USULAN, $criteria->remove(UsulanSSHPeer::ID_USULAN), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(UsulanSSHPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(UsulanSSHPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof UsulanSSH) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(UsulanSSHPeer::ID_USULAN, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(UsulanSSH $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(UsulanSSHPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(UsulanSSHPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(UsulanSSHPeer::DATABASE_NAME, UsulanSSHPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = UsulanSSHPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(UsulanSSHPeer::DATABASE_NAME);

		$criteria->add(UsulanSSHPeer::ID_USULAN, $pk);


		$v = UsulanSSHPeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(UsulanSSHPeer::ID_USULAN, $pks, Criteria::IN);
			$objs = UsulanSSHPeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BaseUsulanSSHPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/UsulanSSHMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.UsulanSSHMapBuilder');
}
