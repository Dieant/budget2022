<?php


abstract class BaseWaitingList extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id_waiting;


	
	protected $unit_id;


	
	protected $kegiatan_code;


	
	protected $kegiatan_code_lama;


	
	protected $subtitle;


	
	protected $subtitle_lama;


	
	protected $komponen_id;


	
	protected $komponen_id_lama;


	
	protected $komponen_name;


	
	protected $komponen_lokasi;


	
	protected $komponen_harga;


	
	protected $komponen_harga_lama;


	
	protected $pajak;


	
	protected $komponen_satuan;


	
	protected $komponen_rekening;


	
	protected $koefisien;


	
	protected $volume;


	
	protected $nilai_anggaran;


	
	protected $tahun_input;


	
	protected $created_at;


	
	protected $updated_at;


	
	protected $status_hapus;


	
	protected $status_waiting;


	
	protected $prioritas;


	
	protected $kode_rka;


	
	protected $user_pengambil;


	
	protected $nilai_ee;


	
	protected $keterangan;


	
	protected $kode_jasmas;


	
	protected $kecamatan;


	
	protected $kelurahan;


	
	protected $is_musrenbang;


	
	protected $nilai_anggaran_semula;


	
	protected $metode_waitinglist;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getIdWaiting()
	{

		return $this->id_waiting;
	}

	
	public function getUnitId()
	{

		return $this->unit_id;
	}

	
	public function getKegiatanCode()
	{

		return $this->kegiatan_code;
	}

	
	public function getKegiatanCodeLama()
	{

		return $this->kegiatan_code_lama;
	}

	
	public function getSubtitle()
	{

		return $this->subtitle;
	}

	
	public function getSubtitleLama()
	{

		return $this->subtitle_lama;
	}

	
	public function getKomponenId()
	{

		return $this->komponen_id;
	}

	
	public function getKomponenIdLama()
	{

		return $this->komponen_id_lama;
	}

	
	public function getKomponenName()
	{

		return $this->komponen_name;
	}

	
	public function getKomponenLokasi()
	{

		return $this->komponen_lokasi;
	}

	
	public function getKomponenHarga()
	{

		return $this->komponen_harga;
	}

	
	public function getKomponenHargaLama()
	{

		return $this->komponen_harga_lama;
	}

	
	public function getPajak()
	{

		return $this->pajak;
	}

	
	public function getKomponenSatuan()
	{

		return $this->komponen_satuan;
	}

	
	public function getKomponenRekening()
	{

		return $this->komponen_rekening;
	}

	
	public function getKoefisien()
	{

		return $this->koefisien;
	}

	
	public function getVolume()
	{

		return $this->volume;
	}

	
	public function getNilaiAnggaran()
	{

		return $this->nilai_anggaran;
	}

	
	public function getTahunInput()
	{

		return $this->tahun_input;
	}

	
	public function getCreatedAt($format = 'Y-m-d H:i:s')
	{

		if ($this->created_at === null || $this->created_at === '') {
			return null;
		} elseif (!is_int($this->created_at)) {
						$ts = strtotime($this->created_at);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [created_at] as date/time value: " . var_export($this->created_at, true));
			}
		} else {
			$ts = $this->created_at;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getUpdatedAt($format = 'Y-m-d H:i:s')
	{

		if ($this->updated_at === null || $this->updated_at === '') {
			return null;
		} elseif (!is_int($this->updated_at)) {
						$ts = strtotime($this->updated_at);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [updated_at] as date/time value: " . var_export($this->updated_at, true));
			}
		} else {
			$ts = $this->updated_at;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getStatusHapus()
	{

		return $this->status_hapus;
	}

	
	public function getStatusWaiting()
	{

		return $this->status_waiting;
	}

	
	public function getPrioritas()
	{

		return $this->prioritas;
	}

	
	public function getKodeRka()
	{

		return $this->kode_rka;
	}

	
	public function getUserPengambil()
	{

		return $this->user_pengambil;
	}

	
	public function getNilaiEe()
	{

		return $this->nilai_ee;
	}

	
	public function getKeterangan()
	{

		return $this->keterangan;
	}

	
	public function getKodeJasmas()
	{

		return $this->kode_jasmas;
	}

	
	public function getKecamatan()
	{

		return $this->kecamatan;
	}

	
	public function getKelurahan()
	{

		return $this->kelurahan;
	}

	
	public function getIsMusrenbang()
	{

		return $this->is_musrenbang;
	}

	
	public function getNilaiAnggaranSemula()
	{

		return $this->nilai_anggaran_semula;
	}

	
	public function getMetodeWaitinglist()
	{

		return $this->metode_waitinglist;
	}

	
	public function setIdWaiting($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id_waiting !== $v) {
			$this->id_waiting = $v;
			$this->modifiedColumns[] = WaitingListPeer::ID_WAITING;
		}

	} 
	
	public function setUnitId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->unit_id !== $v) {
			$this->unit_id = $v;
			$this->modifiedColumns[] = WaitingListPeer::UNIT_ID;
		}

	} 
	
	public function setKegiatanCode($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kegiatan_code !== $v) {
			$this->kegiatan_code = $v;
			$this->modifiedColumns[] = WaitingListPeer::KEGIATAN_CODE;
		}

	} 
	
	public function setKegiatanCodeLama($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kegiatan_code_lama !== $v) {
			$this->kegiatan_code_lama = $v;
			$this->modifiedColumns[] = WaitingListPeer::KEGIATAN_CODE_LAMA;
		}

	} 
	
	public function setSubtitle($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->subtitle !== $v) {
			$this->subtitle = $v;
			$this->modifiedColumns[] = WaitingListPeer::SUBTITLE;
		}

	} 
	
	public function setSubtitleLama($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->subtitle_lama !== $v) {
			$this->subtitle_lama = $v;
			$this->modifiedColumns[] = WaitingListPeer::SUBTITLE_LAMA;
		}

	} 
	
	public function setKomponenId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->komponen_id !== $v) {
			$this->komponen_id = $v;
			$this->modifiedColumns[] = WaitingListPeer::KOMPONEN_ID;
		}

	} 
	
	public function setKomponenIdLama($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->komponen_id_lama !== $v) {
			$this->komponen_id_lama = $v;
			$this->modifiedColumns[] = WaitingListPeer::KOMPONEN_ID_LAMA;
		}

	} 
	
	public function setKomponenName($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->komponen_name !== $v) {
			$this->komponen_name = $v;
			$this->modifiedColumns[] = WaitingListPeer::KOMPONEN_NAME;
		}

	} 
	
	public function setKomponenLokasi($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->komponen_lokasi !== $v) {
			$this->komponen_lokasi = $v;
			$this->modifiedColumns[] = WaitingListPeer::KOMPONEN_LOKASI;
		}

	} 
	
	public function setKomponenHarga($v)
	{

		if ($this->komponen_harga !== $v) {
			$this->komponen_harga = $v;
			$this->modifiedColumns[] = WaitingListPeer::KOMPONEN_HARGA;
		}

	} 
	
	public function setKomponenHargaLama($v)
	{

		if ($this->komponen_harga_lama !== $v) {
			$this->komponen_harga_lama = $v;
			$this->modifiedColumns[] = WaitingListPeer::KOMPONEN_HARGA_LAMA;
		}

	} 
	
	public function setPajak($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->pajak !== $v) {
			$this->pajak = $v;
			$this->modifiedColumns[] = WaitingListPeer::PAJAK;
		}

	} 
	
	public function setKomponenSatuan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->komponen_satuan !== $v) {
			$this->komponen_satuan = $v;
			$this->modifiedColumns[] = WaitingListPeer::KOMPONEN_SATUAN;
		}

	} 
	
	public function setKomponenRekening($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->komponen_rekening !== $v) {
			$this->komponen_rekening = $v;
			$this->modifiedColumns[] = WaitingListPeer::KOMPONEN_REKENING;
		}

	} 
	
	public function setKoefisien($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->koefisien !== $v) {
			$this->koefisien = $v;
			$this->modifiedColumns[] = WaitingListPeer::KOEFISIEN;
		}

	} 
	
	public function setVolume($v)
	{

		if ($this->volume !== $v) {
			$this->volume = $v;
			$this->modifiedColumns[] = WaitingListPeer::VOLUME;
		}

	} 
	
	public function setNilaiAnggaran($v)
	{

		if ($this->nilai_anggaran !== $v) {
			$this->nilai_anggaran = $v;
			$this->modifiedColumns[] = WaitingListPeer::NILAI_ANGGARAN;
		}

	} 
	
	public function setTahunInput($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->tahun_input !== $v) {
			$this->tahun_input = $v;
			$this->modifiedColumns[] = WaitingListPeer::TAHUN_INPUT;
		}

	} 
	
	public function setCreatedAt($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [created_at] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->created_at !== $ts) {
			$this->created_at = $ts;
			$this->modifiedColumns[] = WaitingListPeer::CREATED_AT;
		}

	} 
	
	public function setUpdatedAt($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [updated_at] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->updated_at !== $ts) {
			$this->updated_at = $ts;
			$this->modifiedColumns[] = WaitingListPeer::UPDATED_AT;
		}

	} 
	
	public function setStatusHapus($v)
	{

		if ($this->status_hapus !== $v) {
			$this->status_hapus = $v;
			$this->modifiedColumns[] = WaitingListPeer::STATUS_HAPUS;
		}

	} 
	
	public function setStatusWaiting($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->status_waiting !== $v) {
			$this->status_waiting = $v;
			$this->modifiedColumns[] = WaitingListPeer::STATUS_WAITING;
		}

	} 
	
	public function setPrioritas($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->prioritas !== $v) {
			$this->prioritas = $v;
			$this->modifiedColumns[] = WaitingListPeer::PRIORITAS;
		}

	} 
	
	public function setKodeRka($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_rka !== $v) {
			$this->kode_rka = $v;
			$this->modifiedColumns[] = WaitingListPeer::KODE_RKA;
		}

	} 
	
	public function setUserPengambil($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->user_pengambil !== $v) {
			$this->user_pengambil = $v;
			$this->modifiedColumns[] = WaitingListPeer::USER_PENGAMBIL;
		}

	} 
	
	public function setNilaiEe($v)
	{

		if ($this->nilai_ee !== $v) {
			$this->nilai_ee = $v;
			$this->modifiedColumns[] = WaitingListPeer::NILAI_EE;
		}

	} 
	
	public function setKeterangan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->keterangan !== $v) {
			$this->keterangan = $v;
			$this->modifiedColumns[] = WaitingListPeer::KETERANGAN;
		}

	} 
	
	public function setKodeJasmas($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_jasmas !== $v) {
			$this->kode_jasmas = $v;
			$this->modifiedColumns[] = WaitingListPeer::KODE_JASMAS;
		}

	} 
	
	public function setKecamatan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kecamatan !== $v) {
			$this->kecamatan = $v;
			$this->modifiedColumns[] = WaitingListPeer::KECAMATAN;
		}

	} 
	
	public function setKelurahan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kelurahan !== $v) {
			$this->kelurahan = $v;
			$this->modifiedColumns[] = WaitingListPeer::KELURAHAN;
		}

	} 
	
	public function setIsMusrenbang($v)
	{

		if ($this->is_musrenbang !== $v) {
			$this->is_musrenbang = $v;
			$this->modifiedColumns[] = WaitingListPeer::IS_MUSRENBANG;
		}

	} 
	
	public function setNilaiAnggaranSemula($v)
	{

		if ($this->nilai_anggaran_semula !== $v) {
			$this->nilai_anggaran_semula = $v;
			$this->modifiedColumns[] = WaitingListPeer::NILAI_ANGGARAN_SEMULA;
		}

	} 
	
	public function setMetodeWaitinglist($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->metode_waitinglist !== $v) {
			$this->metode_waitinglist = $v;
			$this->modifiedColumns[] = WaitingListPeer::METODE_WAITINGLIST;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id_waiting = $rs->getInt($startcol + 0);

			$this->unit_id = $rs->getString($startcol + 1);

			$this->kegiatan_code = $rs->getString($startcol + 2);

			$this->kegiatan_code_lama = $rs->getString($startcol + 3);

			$this->subtitle = $rs->getString($startcol + 4);

			$this->subtitle_lama = $rs->getString($startcol + 5);

			$this->komponen_id = $rs->getString($startcol + 6);

			$this->komponen_id_lama = $rs->getString($startcol + 7);

			$this->komponen_name = $rs->getString($startcol + 8);

			$this->komponen_lokasi = $rs->getString($startcol + 9);

			$this->komponen_harga = $rs->getFloat($startcol + 10);

			$this->komponen_harga_lama = $rs->getFloat($startcol + 11);

			$this->pajak = $rs->getInt($startcol + 12);

			$this->komponen_satuan = $rs->getString($startcol + 13);

			$this->komponen_rekening = $rs->getString($startcol + 14);

			$this->koefisien = $rs->getString($startcol + 15);

			$this->volume = $rs->getFloat($startcol + 16);

			$this->nilai_anggaran = $rs->getFloat($startcol + 17);

			$this->tahun_input = $rs->getString($startcol + 18);

			$this->created_at = $rs->getTimestamp($startcol + 19, null);

			$this->updated_at = $rs->getTimestamp($startcol + 20, null);

			$this->status_hapus = $rs->getBoolean($startcol + 21);

			$this->status_waiting = $rs->getInt($startcol + 22);

			$this->prioritas = $rs->getInt($startcol + 23);

			$this->kode_rka = $rs->getString($startcol + 24);

			$this->user_pengambil = $rs->getString($startcol + 25);

			$this->nilai_ee = $rs->getFloat($startcol + 26);

			$this->keterangan = $rs->getString($startcol + 27);

			$this->kode_jasmas = $rs->getString($startcol + 28);

			$this->kecamatan = $rs->getString($startcol + 29);

			$this->kelurahan = $rs->getString($startcol + 30);

			$this->is_musrenbang = $rs->getBoolean($startcol + 31);

			$this->nilai_anggaran_semula = $rs->getFloat($startcol + 32);

			$this->metode_waitinglist = $rs->getInt($startcol + 33);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 34; 
		} catch (Exception $e) {
			throw new PropelException("Error populating WaitingList object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(WaitingListPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			WaitingListPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
    if ($this->isNew() && !$this->isColumnModified(WaitingListPeer::CREATED_AT))
    {
      $this->setCreatedAt(time());
    }

    if ($this->isModified() && !$this->isColumnModified(WaitingListPeer::UPDATED_AT))
    {
      $this->setUpdatedAt(time());
    }

		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(WaitingListPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = WaitingListPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setIdWaiting($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += WaitingListPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = WaitingListPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = WaitingListPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getIdWaiting();
				break;
			case 1:
				return $this->getUnitId();
				break;
			case 2:
				return $this->getKegiatanCode();
				break;
			case 3:
				return $this->getKegiatanCodeLama();
				break;
			case 4:
				return $this->getSubtitle();
				break;
			case 5:
				return $this->getSubtitleLama();
				break;
			case 6:
				return $this->getKomponenId();
				break;
			case 7:
				return $this->getKomponenIdLama();
				break;
			case 8:
				return $this->getKomponenName();
				break;
			case 9:
				return $this->getKomponenLokasi();
				break;
			case 10:
				return $this->getKomponenHarga();
				break;
			case 11:
				return $this->getKomponenHargaLama();
				break;
			case 12:
				return $this->getPajak();
				break;
			case 13:
				return $this->getKomponenSatuan();
				break;
			case 14:
				return $this->getKomponenRekening();
				break;
			case 15:
				return $this->getKoefisien();
				break;
			case 16:
				return $this->getVolume();
				break;
			case 17:
				return $this->getNilaiAnggaran();
				break;
			case 18:
				return $this->getTahunInput();
				break;
			case 19:
				return $this->getCreatedAt();
				break;
			case 20:
				return $this->getUpdatedAt();
				break;
			case 21:
				return $this->getStatusHapus();
				break;
			case 22:
				return $this->getStatusWaiting();
				break;
			case 23:
				return $this->getPrioritas();
				break;
			case 24:
				return $this->getKodeRka();
				break;
			case 25:
				return $this->getUserPengambil();
				break;
			case 26:
				return $this->getNilaiEe();
				break;
			case 27:
				return $this->getKeterangan();
				break;
			case 28:
				return $this->getKodeJasmas();
				break;
			case 29:
				return $this->getKecamatan();
				break;
			case 30:
				return $this->getKelurahan();
				break;
			case 31:
				return $this->getIsMusrenbang();
				break;
			case 32:
				return $this->getNilaiAnggaranSemula();
				break;
			case 33:
				return $this->getMetodeWaitinglist();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = WaitingListPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getIdWaiting(),
			$keys[1] => $this->getUnitId(),
			$keys[2] => $this->getKegiatanCode(),
			$keys[3] => $this->getKegiatanCodeLama(),
			$keys[4] => $this->getSubtitle(),
			$keys[5] => $this->getSubtitleLama(),
			$keys[6] => $this->getKomponenId(),
			$keys[7] => $this->getKomponenIdLama(),
			$keys[8] => $this->getKomponenName(),
			$keys[9] => $this->getKomponenLokasi(),
			$keys[10] => $this->getKomponenHarga(),
			$keys[11] => $this->getKomponenHargaLama(),
			$keys[12] => $this->getPajak(),
			$keys[13] => $this->getKomponenSatuan(),
			$keys[14] => $this->getKomponenRekening(),
			$keys[15] => $this->getKoefisien(),
			$keys[16] => $this->getVolume(),
			$keys[17] => $this->getNilaiAnggaran(),
			$keys[18] => $this->getTahunInput(),
			$keys[19] => $this->getCreatedAt(),
			$keys[20] => $this->getUpdatedAt(),
			$keys[21] => $this->getStatusHapus(),
			$keys[22] => $this->getStatusWaiting(),
			$keys[23] => $this->getPrioritas(),
			$keys[24] => $this->getKodeRka(),
			$keys[25] => $this->getUserPengambil(),
			$keys[26] => $this->getNilaiEe(),
			$keys[27] => $this->getKeterangan(),
			$keys[28] => $this->getKodeJasmas(),
			$keys[29] => $this->getKecamatan(),
			$keys[30] => $this->getKelurahan(),
			$keys[31] => $this->getIsMusrenbang(),
			$keys[32] => $this->getNilaiAnggaranSemula(),
			$keys[33] => $this->getMetodeWaitinglist(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = WaitingListPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setIdWaiting($value);
				break;
			case 1:
				$this->setUnitId($value);
				break;
			case 2:
				$this->setKegiatanCode($value);
				break;
			case 3:
				$this->setKegiatanCodeLama($value);
				break;
			case 4:
				$this->setSubtitle($value);
				break;
			case 5:
				$this->setSubtitleLama($value);
				break;
			case 6:
				$this->setKomponenId($value);
				break;
			case 7:
				$this->setKomponenIdLama($value);
				break;
			case 8:
				$this->setKomponenName($value);
				break;
			case 9:
				$this->setKomponenLokasi($value);
				break;
			case 10:
				$this->setKomponenHarga($value);
				break;
			case 11:
				$this->setKomponenHargaLama($value);
				break;
			case 12:
				$this->setPajak($value);
				break;
			case 13:
				$this->setKomponenSatuan($value);
				break;
			case 14:
				$this->setKomponenRekening($value);
				break;
			case 15:
				$this->setKoefisien($value);
				break;
			case 16:
				$this->setVolume($value);
				break;
			case 17:
				$this->setNilaiAnggaran($value);
				break;
			case 18:
				$this->setTahunInput($value);
				break;
			case 19:
				$this->setCreatedAt($value);
				break;
			case 20:
				$this->setUpdatedAt($value);
				break;
			case 21:
				$this->setStatusHapus($value);
				break;
			case 22:
				$this->setStatusWaiting($value);
				break;
			case 23:
				$this->setPrioritas($value);
				break;
			case 24:
				$this->setKodeRka($value);
				break;
			case 25:
				$this->setUserPengambil($value);
				break;
			case 26:
				$this->setNilaiEe($value);
				break;
			case 27:
				$this->setKeterangan($value);
				break;
			case 28:
				$this->setKodeJasmas($value);
				break;
			case 29:
				$this->setKecamatan($value);
				break;
			case 30:
				$this->setKelurahan($value);
				break;
			case 31:
				$this->setIsMusrenbang($value);
				break;
			case 32:
				$this->setNilaiAnggaranSemula($value);
				break;
			case 33:
				$this->setMetodeWaitinglist($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = WaitingListPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setIdWaiting($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setUnitId($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setKegiatanCode($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setKegiatanCodeLama($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setSubtitle($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setSubtitleLama($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setKomponenId($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setKomponenIdLama($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setKomponenName($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setKomponenLokasi($arr[$keys[9]]);
		if (array_key_exists($keys[10], $arr)) $this->setKomponenHarga($arr[$keys[10]]);
		if (array_key_exists($keys[11], $arr)) $this->setKomponenHargaLama($arr[$keys[11]]);
		if (array_key_exists($keys[12], $arr)) $this->setPajak($arr[$keys[12]]);
		if (array_key_exists($keys[13], $arr)) $this->setKomponenSatuan($arr[$keys[13]]);
		if (array_key_exists($keys[14], $arr)) $this->setKomponenRekening($arr[$keys[14]]);
		if (array_key_exists($keys[15], $arr)) $this->setKoefisien($arr[$keys[15]]);
		if (array_key_exists($keys[16], $arr)) $this->setVolume($arr[$keys[16]]);
		if (array_key_exists($keys[17], $arr)) $this->setNilaiAnggaran($arr[$keys[17]]);
		if (array_key_exists($keys[18], $arr)) $this->setTahunInput($arr[$keys[18]]);
		if (array_key_exists($keys[19], $arr)) $this->setCreatedAt($arr[$keys[19]]);
		if (array_key_exists($keys[20], $arr)) $this->setUpdatedAt($arr[$keys[20]]);
		if (array_key_exists($keys[21], $arr)) $this->setStatusHapus($arr[$keys[21]]);
		if (array_key_exists($keys[22], $arr)) $this->setStatusWaiting($arr[$keys[22]]);
		if (array_key_exists($keys[23], $arr)) $this->setPrioritas($arr[$keys[23]]);
		if (array_key_exists($keys[24], $arr)) $this->setKodeRka($arr[$keys[24]]);
		if (array_key_exists($keys[25], $arr)) $this->setUserPengambil($arr[$keys[25]]);
		if (array_key_exists($keys[26], $arr)) $this->setNilaiEe($arr[$keys[26]]);
		if (array_key_exists($keys[27], $arr)) $this->setKeterangan($arr[$keys[27]]);
		if (array_key_exists($keys[28], $arr)) $this->setKodeJasmas($arr[$keys[28]]);
		if (array_key_exists($keys[29], $arr)) $this->setKecamatan($arr[$keys[29]]);
		if (array_key_exists($keys[30], $arr)) $this->setKelurahan($arr[$keys[30]]);
		if (array_key_exists($keys[31], $arr)) $this->setIsMusrenbang($arr[$keys[31]]);
		if (array_key_exists($keys[32], $arr)) $this->setNilaiAnggaranSemula($arr[$keys[32]]);
		if (array_key_exists($keys[33], $arr)) $this->setMetodeWaitinglist($arr[$keys[33]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(WaitingListPeer::DATABASE_NAME);

		if ($this->isColumnModified(WaitingListPeer::ID_WAITING)) $criteria->add(WaitingListPeer::ID_WAITING, $this->id_waiting);
		if ($this->isColumnModified(WaitingListPeer::UNIT_ID)) $criteria->add(WaitingListPeer::UNIT_ID, $this->unit_id);
		if ($this->isColumnModified(WaitingListPeer::KEGIATAN_CODE)) $criteria->add(WaitingListPeer::KEGIATAN_CODE, $this->kegiatan_code);
		if ($this->isColumnModified(WaitingListPeer::KEGIATAN_CODE_LAMA)) $criteria->add(WaitingListPeer::KEGIATAN_CODE_LAMA, $this->kegiatan_code_lama);
		if ($this->isColumnModified(WaitingListPeer::SUBTITLE)) $criteria->add(WaitingListPeer::SUBTITLE, $this->subtitle);
		if ($this->isColumnModified(WaitingListPeer::SUBTITLE_LAMA)) $criteria->add(WaitingListPeer::SUBTITLE_LAMA, $this->subtitle_lama);
		if ($this->isColumnModified(WaitingListPeer::KOMPONEN_ID)) $criteria->add(WaitingListPeer::KOMPONEN_ID, $this->komponen_id);
		if ($this->isColumnModified(WaitingListPeer::KOMPONEN_ID_LAMA)) $criteria->add(WaitingListPeer::KOMPONEN_ID_LAMA, $this->komponen_id_lama);
		if ($this->isColumnModified(WaitingListPeer::KOMPONEN_NAME)) $criteria->add(WaitingListPeer::KOMPONEN_NAME, $this->komponen_name);
		if ($this->isColumnModified(WaitingListPeer::KOMPONEN_LOKASI)) $criteria->add(WaitingListPeer::KOMPONEN_LOKASI, $this->komponen_lokasi);
		if ($this->isColumnModified(WaitingListPeer::KOMPONEN_HARGA)) $criteria->add(WaitingListPeer::KOMPONEN_HARGA, $this->komponen_harga);
		if ($this->isColumnModified(WaitingListPeer::KOMPONEN_HARGA_LAMA)) $criteria->add(WaitingListPeer::KOMPONEN_HARGA_LAMA, $this->komponen_harga_lama);
		if ($this->isColumnModified(WaitingListPeer::PAJAK)) $criteria->add(WaitingListPeer::PAJAK, $this->pajak);
		if ($this->isColumnModified(WaitingListPeer::KOMPONEN_SATUAN)) $criteria->add(WaitingListPeer::KOMPONEN_SATUAN, $this->komponen_satuan);
		if ($this->isColumnModified(WaitingListPeer::KOMPONEN_REKENING)) $criteria->add(WaitingListPeer::KOMPONEN_REKENING, $this->komponen_rekening);
		if ($this->isColumnModified(WaitingListPeer::KOEFISIEN)) $criteria->add(WaitingListPeer::KOEFISIEN, $this->koefisien);
		if ($this->isColumnModified(WaitingListPeer::VOLUME)) $criteria->add(WaitingListPeer::VOLUME, $this->volume);
		if ($this->isColumnModified(WaitingListPeer::NILAI_ANGGARAN)) $criteria->add(WaitingListPeer::NILAI_ANGGARAN, $this->nilai_anggaran);
		if ($this->isColumnModified(WaitingListPeer::TAHUN_INPUT)) $criteria->add(WaitingListPeer::TAHUN_INPUT, $this->tahun_input);
		if ($this->isColumnModified(WaitingListPeer::CREATED_AT)) $criteria->add(WaitingListPeer::CREATED_AT, $this->created_at);
		if ($this->isColumnModified(WaitingListPeer::UPDATED_AT)) $criteria->add(WaitingListPeer::UPDATED_AT, $this->updated_at);
		if ($this->isColumnModified(WaitingListPeer::STATUS_HAPUS)) $criteria->add(WaitingListPeer::STATUS_HAPUS, $this->status_hapus);
		if ($this->isColumnModified(WaitingListPeer::STATUS_WAITING)) $criteria->add(WaitingListPeer::STATUS_WAITING, $this->status_waiting);
		if ($this->isColumnModified(WaitingListPeer::PRIORITAS)) $criteria->add(WaitingListPeer::PRIORITAS, $this->prioritas);
		if ($this->isColumnModified(WaitingListPeer::KODE_RKA)) $criteria->add(WaitingListPeer::KODE_RKA, $this->kode_rka);
		if ($this->isColumnModified(WaitingListPeer::USER_PENGAMBIL)) $criteria->add(WaitingListPeer::USER_PENGAMBIL, $this->user_pengambil);
		if ($this->isColumnModified(WaitingListPeer::NILAI_EE)) $criteria->add(WaitingListPeer::NILAI_EE, $this->nilai_ee);
		if ($this->isColumnModified(WaitingListPeer::KETERANGAN)) $criteria->add(WaitingListPeer::KETERANGAN, $this->keterangan);
		if ($this->isColumnModified(WaitingListPeer::KODE_JASMAS)) $criteria->add(WaitingListPeer::KODE_JASMAS, $this->kode_jasmas);
		if ($this->isColumnModified(WaitingListPeer::KECAMATAN)) $criteria->add(WaitingListPeer::KECAMATAN, $this->kecamatan);
		if ($this->isColumnModified(WaitingListPeer::KELURAHAN)) $criteria->add(WaitingListPeer::KELURAHAN, $this->kelurahan);
		if ($this->isColumnModified(WaitingListPeer::IS_MUSRENBANG)) $criteria->add(WaitingListPeer::IS_MUSRENBANG, $this->is_musrenbang);
		if ($this->isColumnModified(WaitingListPeer::NILAI_ANGGARAN_SEMULA)) $criteria->add(WaitingListPeer::NILAI_ANGGARAN_SEMULA, $this->nilai_anggaran_semula);
		if ($this->isColumnModified(WaitingListPeer::METODE_WAITINGLIST)) $criteria->add(WaitingListPeer::METODE_WAITINGLIST, $this->metode_waitinglist);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(WaitingListPeer::DATABASE_NAME);

		$criteria->add(WaitingListPeer::ID_WAITING, $this->id_waiting);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getIdWaiting();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setIdWaiting($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setUnitId($this->unit_id);

		$copyObj->setKegiatanCode($this->kegiatan_code);

		$copyObj->setKegiatanCodeLama($this->kegiatan_code_lama);

		$copyObj->setSubtitle($this->subtitle);

		$copyObj->setSubtitleLama($this->subtitle_lama);

		$copyObj->setKomponenId($this->komponen_id);

		$copyObj->setKomponenIdLama($this->komponen_id_lama);

		$copyObj->setKomponenName($this->komponen_name);

		$copyObj->setKomponenLokasi($this->komponen_lokasi);

		$copyObj->setKomponenHarga($this->komponen_harga);

		$copyObj->setKomponenHargaLama($this->komponen_harga_lama);

		$copyObj->setPajak($this->pajak);

		$copyObj->setKomponenSatuan($this->komponen_satuan);

		$copyObj->setKomponenRekening($this->komponen_rekening);

		$copyObj->setKoefisien($this->koefisien);

		$copyObj->setVolume($this->volume);

		$copyObj->setNilaiAnggaran($this->nilai_anggaran);

		$copyObj->setTahunInput($this->tahun_input);

		$copyObj->setCreatedAt($this->created_at);

		$copyObj->setUpdatedAt($this->updated_at);

		$copyObj->setStatusHapus($this->status_hapus);

		$copyObj->setStatusWaiting($this->status_waiting);

		$copyObj->setPrioritas($this->prioritas);

		$copyObj->setKodeRka($this->kode_rka);

		$copyObj->setUserPengambil($this->user_pengambil);

		$copyObj->setNilaiEe($this->nilai_ee);

		$copyObj->setKeterangan($this->keterangan);

		$copyObj->setKodeJasmas($this->kode_jasmas);

		$copyObj->setKecamatan($this->kecamatan);

		$copyObj->setKelurahan($this->kelurahan);

		$copyObj->setIsMusrenbang($this->is_musrenbang);

		$copyObj->setNilaiAnggaranSemula($this->nilai_anggaran_semula);

		$copyObj->setMetodeWaitinglist($this->metode_waitinglist);


		$copyObj->setNew(true);

		$copyObj->setIdWaiting(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new WaitingListPeer();
		}
		return self::$peer;
	}

} 