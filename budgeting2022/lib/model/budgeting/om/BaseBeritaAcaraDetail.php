<?php


abstract class BaseBeritaAcaraDetail extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id_berita_acara;


	
	protected $detail_no;


	
	protected $belanja;


	
	protected $rekening_code;


	
	protected $rekening_code_semula;


	
	protected $subtitle;


	
	protected $semula_komponen;


	
	protected $menjadi_komponen;


	
	protected $semula_satuan;


	
	protected $menjadi_satuan;


	
	protected $semula_koefisien;


	
	protected $menjadi_koefisien;


	
	protected $semula_harga;


	
	protected $menjadi_harga;


	
	protected $semula_hasil;


	
	protected $menjadi_hasil;


	
	protected $semula_pajak;


	
	protected $menjadi_pajak;


	
	protected $semula_total;


	
	protected $menjadi_total;


	
	protected $catatan;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getIdBeritaAcara()
	{

		return $this->id_berita_acara;
	}

	
	public function getDetailNo()
	{

		return $this->detail_no;
	}

	
	public function getBelanja()
	{

		return $this->belanja;
	}

	
	public function getRekeningCode()
	{

		return $this->rekening_code;
	}

	
	public function getRekeningCodeSemula()
	{

		return $this->rekening_code_semula;
	}

	
	public function getSubtitle()
	{

		return $this->subtitle;
	}

	
	public function getSemulaKomponen()
	{

		return $this->semula_komponen;
	}

	
	public function getMenjadiKomponen()
	{

		return $this->menjadi_komponen;
	}

	
	public function getSemulaSatuan()
	{

		return $this->semula_satuan;
	}

	
	public function getMenjadiSatuan()
	{

		return $this->menjadi_satuan;
	}

	
	public function getSemulaKoefisien()
	{

		return $this->semula_koefisien;
	}

	
	public function getMenjadiKoefisien()
	{

		return $this->menjadi_koefisien;
	}

	
	public function getSemulaHarga()
	{

		return $this->semula_harga;
	}

	
	public function getMenjadiHarga()
	{

		return $this->menjadi_harga;
	}

	
	public function getSemulaHasil()
	{

		return $this->semula_hasil;
	}

	
	public function getMenjadiHasil()
	{

		return $this->menjadi_hasil;
	}

	
	public function getSemulaPajak()
	{

		return $this->semula_pajak;
	}

	
	public function getMenjadiPajak()
	{

		return $this->menjadi_pajak;
	}

	
	public function getSemulaTotal()
	{

		return $this->semula_total;
	}

	
	public function getMenjadiTotal()
	{

		return $this->menjadi_total;
	}

	
	public function getCatatan()
	{

		return $this->catatan;
	}

	
	public function setIdBeritaAcara($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id_berita_acara !== $v) {
			$this->id_berita_acara = $v;
			$this->modifiedColumns[] = BeritaAcaraDetailPeer::ID_BERITA_ACARA;
		}

	} 
	
	public function setDetailNo($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->detail_no !== $v) {
			$this->detail_no = $v;
			$this->modifiedColumns[] = BeritaAcaraDetailPeer::DETAIL_NO;
		}

	} 
	
	public function setBelanja($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->belanja !== $v) {
			$this->belanja = $v;
			$this->modifiedColumns[] = BeritaAcaraDetailPeer::BELANJA;
		}

	} 
	
	public function setRekeningCode($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->rekening_code !== $v) {
			$this->rekening_code = $v;
			$this->modifiedColumns[] = BeritaAcaraDetailPeer::REKENING_CODE;
		}

	} 
	
	public function setRekeningCodeSemula($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->rekening_code_semula !== $v) {
			$this->rekening_code_semula = $v;
			$this->modifiedColumns[] = BeritaAcaraDetailPeer::REKENING_CODE_SEMULA;
		}

	} 
	
	public function setSubtitle($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->subtitle !== $v) {
			$this->subtitle = $v;
			$this->modifiedColumns[] = BeritaAcaraDetailPeer::SUBTITLE;
		}

	} 
	
	public function setSemulaKomponen($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->semula_komponen !== $v) {
			$this->semula_komponen = $v;
			$this->modifiedColumns[] = BeritaAcaraDetailPeer::SEMULA_KOMPONEN;
		}

	} 
	
	public function setMenjadiKomponen($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->menjadi_komponen !== $v) {
			$this->menjadi_komponen = $v;
			$this->modifiedColumns[] = BeritaAcaraDetailPeer::MENJADI_KOMPONEN;
		}

	} 
	
	public function setSemulaSatuan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->semula_satuan !== $v) {
			$this->semula_satuan = $v;
			$this->modifiedColumns[] = BeritaAcaraDetailPeer::SEMULA_SATUAN;
		}

	} 
	
	public function setMenjadiSatuan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->menjadi_satuan !== $v) {
			$this->menjadi_satuan = $v;
			$this->modifiedColumns[] = BeritaAcaraDetailPeer::MENJADI_SATUAN;
		}

	} 
	
	public function setSemulaKoefisien($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->semula_koefisien !== $v) {
			$this->semula_koefisien = $v;
			$this->modifiedColumns[] = BeritaAcaraDetailPeer::SEMULA_KOEFISIEN;
		}

	} 
	
	public function setMenjadiKoefisien($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->menjadi_koefisien !== $v) {
			$this->menjadi_koefisien = $v;
			$this->modifiedColumns[] = BeritaAcaraDetailPeer::MENJADI_KOEFISIEN;
		}

	} 
	
	public function setSemulaHarga($v)
	{

		if ($this->semula_harga !== $v) {
			$this->semula_harga = $v;
			$this->modifiedColumns[] = BeritaAcaraDetailPeer::SEMULA_HARGA;
		}

	} 
	
	public function setMenjadiHarga($v)
	{

		if ($this->menjadi_harga !== $v) {
			$this->menjadi_harga = $v;
			$this->modifiedColumns[] = BeritaAcaraDetailPeer::MENJADI_HARGA;
		}

	} 
	
	public function setSemulaHasil($v)
	{

		if ($this->semula_hasil !== $v) {
			$this->semula_hasil = $v;
			$this->modifiedColumns[] = BeritaAcaraDetailPeer::SEMULA_HASIL;
		}

	} 
	
	public function setMenjadiHasil($v)
	{

		if ($this->menjadi_hasil !== $v) {
			$this->menjadi_hasil = $v;
			$this->modifiedColumns[] = BeritaAcaraDetailPeer::MENJADI_HASIL;
		}

	} 
	
	public function setSemulaPajak($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->semula_pajak !== $v) {
			$this->semula_pajak = $v;
			$this->modifiedColumns[] = BeritaAcaraDetailPeer::SEMULA_PAJAK;
		}

	} 
	
	public function setMenjadiPajak($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->menjadi_pajak !== $v) {
			$this->menjadi_pajak = $v;
			$this->modifiedColumns[] = BeritaAcaraDetailPeer::MENJADI_PAJAK;
		}

	} 
	
	public function setSemulaTotal($v)
	{

		if ($this->semula_total !== $v) {
			$this->semula_total = $v;
			$this->modifiedColumns[] = BeritaAcaraDetailPeer::SEMULA_TOTAL;
		}

	} 
	
	public function setMenjadiTotal($v)
	{

		if ($this->menjadi_total !== $v) {
			$this->menjadi_total = $v;
			$this->modifiedColumns[] = BeritaAcaraDetailPeer::MENJADI_TOTAL;
		}

	} 
	
	public function setCatatan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->catatan !== $v) {
			$this->catatan = $v;
			$this->modifiedColumns[] = BeritaAcaraDetailPeer::CATATAN;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id_berita_acara = $rs->getInt($startcol + 0);

			$this->detail_no = $rs->getInt($startcol + 1);

			$this->belanja = $rs->getString($startcol + 2);

			$this->rekening_code = $rs->getString($startcol + 3);

			$this->rekening_code_semula = $rs->getString($startcol + 4);

			$this->subtitle = $rs->getString($startcol + 5);

			$this->semula_komponen = $rs->getString($startcol + 6);

			$this->menjadi_komponen = $rs->getString($startcol + 7);

			$this->semula_satuan = $rs->getString($startcol + 8);

			$this->menjadi_satuan = $rs->getString($startcol + 9);

			$this->semula_koefisien = $rs->getString($startcol + 10);

			$this->menjadi_koefisien = $rs->getString($startcol + 11);

			$this->semula_harga = $rs->getFloat($startcol + 12);

			$this->menjadi_harga = $rs->getFloat($startcol + 13);

			$this->semula_hasil = $rs->getFloat($startcol + 14);

			$this->menjadi_hasil = $rs->getFloat($startcol + 15);

			$this->semula_pajak = $rs->getInt($startcol + 16);

			$this->menjadi_pajak = $rs->getInt($startcol + 17);

			$this->semula_total = $rs->getFloat($startcol + 18);

			$this->menjadi_total = $rs->getFloat($startcol + 19);

			$this->catatan = $rs->getString($startcol + 20);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 21; 
		} catch (Exception $e) {
			throw new PropelException("Error populating BeritaAcaraDetail object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(BeritaAcaraDetailPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			BeritaAcaraDetailPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(BeritaAcaraDetailPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = BeritaAcaraDetailPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += BeritaAcaraDetailPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = BeritaAcaraDetailPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = BeritaAcaraDetailPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getIdBeritaAcara();
				break;
			case 1:
				return $this->getDetailNo();
				break;
			case 2:
				return $this->getBelanja();
				break;
			case 3:
				return $this->getRekeningCode();
				break;
			case 4:
				return $this->getRekeningCodeSemula();
				break;
			case 5:
				return $this->getSubtitle();
				break;
			case 6:
				return $this->getSemulaKomponen();
				break;
			case 7:
				return $this->getMenjadiKomponen();
				break;
			case 8:
				return $this->getSemulaSatuan();
				break;
			case 9:
				return $this->getMenjadiSatuan();
				break;
			case 10:
				return $this->getSemulaKoefisien();
				break;
			case 11:
				return $this->getMenjadiKoefisien();
				break;
			case 12:
				return $this->getSemulaHarga();
				break;
			case 13:
				return $this->getMenjadiHarga();
				break;
			case 14:
				return $this->getSemulaHasil();
				break;
			case 15:
				return $this->getMenjadiHasil();
				break;
			case 16:
				return $this->getSemulaPajak();
				break;
			case 17:
				return $this->getMenjadiPajak();
				break;
			case 18:
				return $this->getSemulaTotal();
				break;
			case 19:
				return $this->getMenjadiTotal();
				break;
			case 20:
				return $this->getCatatan();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = BeritaAcaraDetailPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getIdBeritaAcara(),
			$keys[1] => $this->getDetailNo(),
			$keys[2] => $this->getBelanja(),
			$keys[3] => $this->getRekeningCode(),
			$keys[4] => $this->getRekeningCodeSemula(),
			$keys[5] => $this->getSubtitle(),
			$keys[6] => $this->getSemulaKomponen(),
			$keys[7] => $this->getMenjadiKomponen(),
			$keys[8] => $this->getSemulaSatuan(),
			$keys[9] => $this->getMenjadiSatuan(),
			$keys[10] => $this->getSemulaKoefisien(),
			$keys[11] => $this->getMenjadiKoefisien(),
			$keys[12] => $this->getSemulaHarga(),
			$keys[13] => $this->getMenjadiHarga(),
			$keys[14] => $this->getSemulaHasil(),
			$keys[15] => $this->getMenjadiHasil(),
			$keys[16] => $this->getSemulaPajak(),
			$keys[17] => $this->getMenjadiPajak(),
			$keys[18] => $this->getSemulaTotal(),
			$keys[19] => $this->getMenjadiTotal(),
			$keys[20] => $this->getCatatan(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = BeritaAcaraDetailPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setIdBeritaAcara($value);
				break;
			case 1:
				$this->setDetailNo($value);
				break;
			case 2:
				$this->setBelanja($value);
				break;
			case 3:
				$this->setRekeningCode($value);
				break;
			case 4:
				$this->setRekeningCodeSemula($value);
				break;
			case 5:
				$this->setSubtitle($value);
				break;
			case 6:
				$this->setSemulaKomponen($value);
				break;
			case 7:
				$this->setMenjadiKomponen($value);
				break;
			case 8:
				$this->setSemulaSatuan($value);
				break;
			case 9:
				$this->setMenjadiSatuan($value);
				break;
			case 10:
				$this->setSemulaKoefisien($value);
				break;
			case 11:
				$this->setMenjadiKoefisien($value);
				break;
			case 12:
				$this->setSemulaHarga($value);
				break;
			case 13:
				$this->setMenjadiHarga($value);
				break;
			case 14:
				$this->setSemulaHasil($value);
				break;
			case 15:
				$this->setMenjadiHasil($value);
				break;
			case 16:
				$this->setSemulaPajak($value);
				break;
			case 17:
				$this->setMenjadiPajak($value);
				break;
			case 18:
				$this->setSemulaTotal($value);
				break;
			case 19:
				$this->setMenjadiTotal($value);
				break;
			case 20:
				$this->setCatatan($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = BeritaAcaraDetailPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setIdBeritaAcara($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setDetailNo($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setBelanja($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setRekeningCode($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setRekeningCodeSemula($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setSubtitle($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setSemulaKomponen($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setMenjadiKomponen($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setSemulaSatuan($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setMenjadiSatuan($arr[$keys[9]]);
		if (array_key_exists($keys[10], $arr)) $this->setSemulaKoefisien($arr[$keys[10]]);
		if (array_key_exists($keys[11], $arr)) $this->setMenjadiKoefisien($arr[$keys[11]]);
		if (array_key_exists($keys[12], $arr)) $this->setSemulaHarga($arr[$keys[12]]);
		if (array_key_exists($keys[13], $arr)) $this->setMenjadiHarga($arr[$keys[13]]);
		if (array_key_exists($keys[14], $arr)) $this->setSemulaHasil($arr[$keys[14]]);
		if (array_key_exists($keys[15], $arr)) $this->setMenjadiHasil($arr[$keys[15]]);
		if (array_key_exists($keys[16], $arr)) $this->setSemulaPajak($arr[$keys[16]]);
		if (array_key_exists($keys[17], $arr)) $this->setMenjadiPajak($arr[$keys[17]]);
		if (array_key_exists($keys[18], $arr)) $this->setSemulaTotal($arr[$keys[18]]);
		if (array_key_exists($keys[19], $arr)) $this->setMenjadiTotal($arr[$keys[19]]);
		if (array_key_exists($keys[20], $arr)) $this->setCatatan($arr[$keys[20]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(BeritaAcaraDetailPeer::DATABASE_NAME);

		if ($this->isColumnModified(BeritaAcaraDetailPeer::ID_BERITA_ACARA)) $criteria->add(BeritaAcaraDetailPeer::ID_BERITA_ACARA, $this->id_berita_acara);
		if ($this->isColumnModified(BeritaAcaraDetailPeer::DETAIL_NO)) $criteria->add(BeritaAcaraDetailPeer::DETAIL_NO, $this->detail_no);
		if ($this->isColumnModified(BeritaAcaraDetailPeer::BELANJA)) $criteria->add(BeritaAcaraDetailPeer::BELANJA, $this->belanja);
		if ($this->isColumnModified(BeritaAcaraDetailPeer::REKENING_CODE)) $criteria->add(BeritaAcaraDetailPeer::REKENING_CODE, $this->rekening_code);
		if ($this->isColumnModified(BeritaAcaraDetailPeer::REKENING_CODE_SEMULA)) $criteria->add(BeritaAcaraDetailPeer::REKENING_CODE_SEMULA, $this->rekening_code_semula);
		if ($this->isColumnModified(BeritaAcaraDetailPeer::SUBTITLE)) $criteria->add(BeritaAcaraDetailPeer::SUBTITLE, $this->subtitle);
		if ($this->isColumnModified(BeritaAcaraDetailPeer::SEMULA_KOMPONEN)) $criteria->add(BeritaAcaraDetailPeer::SEMULA_KOMPONEN, $this->semula_komponen);
		if ($this->isColumnModified(BeritaAcaraDetailPeer::MENJADI_KOMPONEN)) $criteria->add(BeritaAcaraDetailPeer::MENJADI_KOMPONEN, $this->menjadi_komponen);
		if ($this->isColumnModified(BeritaAcaraDetailPeer::SEMULA_SATUAN)) $criteria->add(BeritaAcaraDetailPeer::SEMULA_SATUAN, $this->semula_satuan);
		if ($this->isColumnModified(BeritaAcaraDetailPeer::MENJADI_SATUAN)) $criteria->add(BeritaAcaraDetailPeer::MENJADI_SATUAN, $this->menjadi_satuan);
		if ($this->isColumnModified(BeritaAcaraDetailPeer::SEMULA_KOEFISIEN)) $criteria->add(BeritaAcaraDetailPeer::SEMULA_KOEFISIEN, $this->semula_koefisien);
		if ($this->isColumnModified(BeritaAcaraDetailPeer::MENJADI_KOEFISIEN)) $criteria->add(BeritaAcaraDetailPeer::MENJADI_KOEFISIEN, $this->menjadi_koefisien);
		if ($this->isColumnModified(BeritaAcaraDetailPeer::SEMULA_HARGA)) $criteria->add(BeritaAcaraDetailPeer::SEMULA_HARGA, $this->semula_harga);
		if ($this->isColumnModified(BeritaAcaraDetailPeer::MENJADI_HARGA)) $criteria->add(BeritaAcaraDetailPeer::MENJADI_HARGA, $this->menjadi_harga);
		if ($this->isColumnModified(BeritaAcaraDetailPeer::SEMULA_HASIL)) $criteria->add(BeritaAcaraDetailPeer::SEMULA_HASIL, $this->semula_hasil);
		if ($this->isColumnModified(BeritaAcaraDetailPeer::MENJADI_HASIL)) $criteria->add(BeritaAcaraDetailPeer::MENJADI_HASIL, $this->menjadi_hasil);
		if ($this->isColumnModified(BeritaAcaraDetailPeer::SEMULA_PAJAK)) $criteria->add(BeritaAcaraDetailPeer::SEMULA_PAJAK, $this->semula_pajak);
		if ($this->isColumnModified(BeritaAcaraDetailPeer::MENJADI_PAJAK)) $criteria->add(BeritaAcaraDetailPeer::MENJADI_PAJAK, $this->menjadi_pajak);
		if ($this->isColumnModified(BeritaAcaraDetailPeer::SEMULA_TOTAL)) $criteria->add(BeritaAcaraDetailPeer::SEMULA_TOTAL, $this->semula_total);
		if ($this->isColumnModified(BeritaAcaraDetailPeer::MENJADI_TOTAL)) $criteria->add(BeritaAcaraDetailPeer::MENJADI_TOTAL, $this->menjadi_total);
		if ($this->isColumnModified(BeritaAcaraDetailPeer::CATATAN)) $criteria->add(BeritaAcaraDetailPeer::CATATAN, $this->catatan);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(BeritaAcaraDetailPeer::DATABASE_NAME);

		$criteria->add(BeritaAcaraDetailPeer::ID_BERITA_ACARA, $this->id_berita_acara);
		$criteria->add(BeritaAcaraDetailPeer::DETAIL_NO, $this->detail_no);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		$pks = array();

		$pks[0] = $this->getIdBeritaAcara();

		$pks[1] = $this->getDetailNo();

		return $pks;
	}

	
	public function setPrimaryKey($keys)
	{

		$this->setIdBeritaAcara($keys[0]);

		$this->setDetailNo($keys[1]);

	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setBelanja($this->belanja);

		$copyObj->setRekeningCode($this->rekening_code);

		$copyObj->setRekeningCodeSemula($this->rekening_code_semula);

		$copyObj->setSubtitle($this->subtitle);

		$copyObj->setSemulaKomponen($this->semula_komponen);

		$copyObj->setMenjadiKomponen($this->menjadi_komponen);

		$copyObj->setSemulaSatuan($this->semula_satuan);

		$copyObj->setMenjadiSatuan($this->menjadi_satuan);

		$copyObj->setSemulaKoefisien($this->semula_koefisien);

		$copyObj->setMenjadiKoefisien($this->menjadi_koefisien);

		$copyObj->setSemulaHarga($this->semula_harga);

		$copyObj->setMenjadiHarga($this->menjadi_harga);

		$copyObj->setSemulaHasil($this->semula_hasil);

		$copyObj->setMenjadiHasil($this->menjadi_hasil);

		$copyObj->setSemulaPajak($this->semula_pajak);

		$copyObj->setMenjadiPajak($this->menjadi_pajak);

		$copyObj->setSemulaTotal($this->semula_total);

		$copyObj->setMenjadiTotal($this->menjadi_total);

		$copyObj->setCatatan($this->catatan);


		$copyObj->setNew(true);

		$copyObj->setIdBeritaAcara(NULL); 
		$copyObj->setDetailNo(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new BeritaAcaraDetailPeer();
		}
		return self::$peer;
	}

} 