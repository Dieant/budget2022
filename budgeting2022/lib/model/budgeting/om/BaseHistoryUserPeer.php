<?php


abstract class BaseHistoryUserPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.history_user';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.HistoryUser';

	
	const NUM_COLUMNS = 9;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const ID = 'ebudget.history_user.ID';

	
	const USERNAME = 'ebudget.history_user.USERNAME';

	
	const IP = 'ebudget.history_user.IP';

	
	const TIME_ACT = 'ebudget.history_user.TIME_ACT';

	
	const UNIT_ID = 'ebudget.history_user.UNIT_ID';

	
	const KEGIATAN_CODE = 'ebudget.history_user.KEGIATAN_CODE';

	
	const DETAIL_NO = 'ebudget.history_user.DETAIL_NO';

	
	const DESCRIPTION = 'ebudget.history_user.DESCRIPTION';

	
	const AKSI = 'ebudget.history_user.AKSI';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('Id', 'Username', 'Ip', 'TimeAct', 'UnitId', 'KegiatanCode', 'DetailNo', 'Description', 'Aksi', ),
		BasePeer::TYPE_COLNAME => array (HistoryUserPeer::ID, HistoryUserPeer::USERNAME, HistoryUserPeer::IP, HistoryUserPeer::TIME_ACT, HistoryUserPeer::UNIT_ID, HistoryUserPeer::KEGIATAN_CODE, HistoryUserPeer::DETAIL_NO, HistoryUserPeer::DESCRIPTION, HistoryUserPeer::AKSI, ),
		BasePeer::TYPE_FIELDNAME => array ('id', 'username', 'ip', 'time_act', 'unit_id', 'kegiatan_code', 'detail_no', 'description', 'aksi', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('Id' => 0, 'Username' => 1, 'Ip' => 2, 'TimeAct' => 3, 'UnitId' => 4, 'KegiatanCode' => 5, 'DetailNo' => 6, 'Description' => 7, 'Aksi' => 8, ),
		BasePeer::TYPE_COLNAME => array (HistoryUserPeer::ID => 0, HistoryUserPeer::USERNAME => 1, HistoryUserPeer::IP => 2, HistoryUserPeer::TIME_ACT => 3, HistoryUserPeer::UNIT_ID => 4, HistoryUserPeer::KEGIATAN_CODE => 5, HistoryUserPeer::DETAIL_NO => 6, HistoryUserPeer::DESCRIPTION => 7, HistoryUserPeer::AKSI => 8, ),
		BasePeer::TYPE_FIELDNAME => array ('id' => 0, 'username' => 1, 'ip' => 2, 'time_act' => 3, 'unit_id' => 4, 'kegiatan_code' => 5, 'detail_no' => 6, 'description' => 7, 'aksi' => 8, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/HistoryUserMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.HistoryUserMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = HistoryUserPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(HistoryUserPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(HistoryUserPeer::ID);

		$criteria->addSelectColumn(HistoryUserPeer::USERNAME);

		$criteria->addSelectColumn(HistoryUserPeer::IP);

		$criteria->addSelectColumn(HistoryUserPeer::TIME_ACT);

		$criteria->addSelectColumn(HistoryUserPeer::UNIT_ID);

		$criteria->addSelectColumn(HistoryUserPeer::KEGIATAN_CODE);

		$criteria->addSelectColumn(HistoryUserPeer::DETAIL_NO);

		$criteria->addSelectColumn(HistoryUserPeer::DESCRIPTION);

		$criteria->addSelectColumn(HistoryUserPeer::AKSI);

	}

	const COUNT = 'COUNT(ebudget.history_user.ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.history_user.ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(HistoryUserPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(HistoryUserPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = HistoryUserPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = HistoryUserPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return HistoryUserPeer::populateObjects(HistoryUserPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			HistoryUserPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = HistoryUserPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return HistoryUserPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(HistoryUserPeer::ID);
			$selectCriteria->add(HistoryUserPeer::ID, $criteria->remove(HistoryUserPeer::ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(HistoryUserPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(HistoryUserPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof HistoryUser) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(HistoryUserPeer::ID, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(HistoryUser $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(HistoryUserPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(HistoryUserPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(HistoryUserPeer::DATABASE_NAME, HistoryUserPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = HistoryUserPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(HistoryUserPeer::DATABASE_NAME);

		$criteria->add(HistoryUserPeer::ID, $pk);


		$v = HistoryUserPeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(HistoryUserPeer::ID, $pks, Criteria::IN);
			$objs = HistoryUserPeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BaseHistoryUserPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/HistoryUserMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.HistoryUserMapBuilder');
}
