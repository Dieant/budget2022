<?php


abstract class BaseMasterSmaPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'master_sma';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.MasterSma';

	
	const NUM_COLUMNS = 7;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const KODE = 'master_sma.KODE';

	
	const NAMA_SMA = 'master_sma.NAMA_SMA';

	
	const ALAMAT = 'master_sma.ALAMAT';

	
	const STATUS = 'master_sma.STATUS';

	
	const KECAMATAN = 'master_sma.KECAMATAN';

	
	const KELURAHAN = 'master_sma.KELURAHAN';

	
	const KODE_JALAN = 'master_sma.KODE_JALAN';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('Kode', 'NamaSma', 'Alamat', 'Status', 'Kecamatan', 'Kelurahan', 'KodeJalan', ),
		BasePeer::TYPE_COLNAME => array (MasterSmaPeer::KODE, MasterSmaPeer::NAMA_SMA, MasterSmaPeer::ALAMAT, MasterSmaPeer::STATUS, MasterSmaPeer::KECAMATAN, MasterSmaPeer::KELURAHAN, MasterSmaPeer::KODE_JALAN, ),
		BasePeer::TYPE_FIELDNAME => array ('kode', 'nama_sma', 'alamat', 'status', 'kecamatan', 'kelurahan', 'kode_jalan', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('Kode' => 0, 'NamaSma' => 1, 'Alamat' => 2, 'Status' => 3, 'Kecamatan' => 4, 'Kelurahan' => 5, 'KodeJalan' => 6, ),
		BasePeer::TYPE_COLNAME => array (MasterSmaPeer::KODE => 0, MasterSmaPeer::NAMA_SMA => 1, MasterSmaPeer::ALAMAT => 2, MasterSmaPeer::STATUS => 3, MasterSmaPeer::KECAMATAN => 4, MasterSmaPeer::KELURAHAN => 5, MasterSmaPeer::KODE_JALAN => 6, ),
		BasePeer::TYPE_FIELDNAME => array ('kode' => 0, 'nama_sma' => 1, 'alamat' => 2, 'status' => 3, 'kecamatan' => 4, 'kelurahan' => 5, 'kode_jalan' => 6, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/MasterSmaMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.MasterSmaMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = MasterSmaPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(MasterSmaPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(MasterSmaPeer::KODE);

		$criteria->addSelectColumn(MasterSmaPeer::NAMA_SMA);

		$criteria->addSelectColumn(MasterSmaPeer::ALAMAT);

		$criteria->addSelectColumn(MasterSmaPeer::STATUS);

		$criteria->addSelectColumn(MasterSmaPeer::KECAMATAN);

		$criteria->addSelectColumn(MasterSmaPeer::KELURAHAN);

		$criteria->addSelectColumn(MasterSmaPeer::KODE_JALAN);

	}

	const COUNT = 'COUNT(master_sma.KODE)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT master_sma.KODE)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(MasterSmaPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(MasterSmaPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = MasterSmaPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = MasterSmaPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return MasterSmaPeer::populateObjects(MasterSmaPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			MasterSmaPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = MasterSmaPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return MasterSmaPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(MasterSmaPeer::KODE);
			$selectCriteria->add(MasterSmaPeer::KODE, $criteria->remove(MasterSmaPeer::KODE), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(MasterSmaPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(MasterSmaPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof MasterSma) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(MasterSmaPeer::KODE, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(MasterSma $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(MasterSmaPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(MasterSmaPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(MasterSmaPeer::DATABASE_NAME, MasterSmaPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = MasterSmaPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(MasterSmaPeer::DATABASE_NAME);

		$criteria->add(MasterSmaPeer::KODE, $pk);


		$v = MasterSmaPeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(MasterSmaPeer::KODE, $pks, Criteria::IN);
			$objs = MasterSmaPeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BaseMasterSmaPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/MasterSmaMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.MasterSmaMapBuilder');
}
