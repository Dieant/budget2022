<?php


abstract class BaseMusrenbangRKAPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.musrenbang_rka';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.MusrenbangRKA';

	
	const NUM_COLUMNS = 23;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const ID = 'ebudget.musrenbang_rka.ID';

	
	const STATUS_HAPUS = 'ebudget.musrenbang_rka.STATUS_HAPUS';

	
	const UNIT_ID = 'ebudget.musrenbang_rka.UNIT_ID';

	
	const KEGIATAN_CODE = 'ebudget.musrenbang_rka.KEGIATAN_CODE';

	
	const DETAIL_NO = 'ebudget.musrenbang_rka.DETAIL_NO';

	
	const ID_MUSRENBANG = 'ebudget.musrenbang_rka.ID_MUSRENBANG';

	
	const TAHUN_MUSRENBANG = 'ebudget.musrenbang_rka.TAHUN_MUSRENBANG';

	
	const KOORDINAT_MUSRENBANG = 'ebudget.musrenbang_rka.KOORDINAT_MUSRENBANG';

	
	const VOLUME = 'ebudget.musrenbang_rka.VOLUME';

	
	const SATUAN = 'ebudget.musrenbang_rka.SATUAN';

	
	const ANGGARAN = 'ebudget.musrenbang_rka.ANGGARAN';

	
	const KOMPONEN_NAME = 'ebudget.musrenbang_rka.KOMPONEN_NAME';

	
	const KOMPONEN_HARGA = 'ebudget.musrenbang_rka.KOMPONEN_HARGA';

	
	const KOMPONEN_PAJAK = 'ebudget.musrenbang_rka.KOMPONEN_PAJAK';

	
	const DETAIL_NAME = 'ebudget.musrenbang_rka.DETAIL_NAME';

	
	const KOMPONEN_REKENING = 'ebudget.musrenbang_rka.KOMPONEN_REKENING';

	
	const KOMPONEN_TIPE = 'ebudget.musrenbang_rka.KOMPONEN_TIPE';

	
	const SUBTITLE = 'ebudget.musrenbang_rka.SUBTITLE';

	
	const KOMPONEN_ID = 'ebudget.musrenbang_rka.KOMPONEN_ID';

	
	const CREATED_BY = 'ebudget.musrenbang_rka.CREATED_BY';

	
	const UPDATED_BY = 'ebudget.musrenbang_rka.UPDATED_BY';

	
	const CREATED_AT = 'ebudget.musrenbang_rka.CREATED_AT';

	
	const UPDATED_AT = 'ebudget.musrenbang_rka.UPDATED_AT';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('Id', 'StatusHapus', 'UnitId', 'KegiatanCode', 'DetailNo', 'IdMusrenbang', 'TahunMusrenbang', 'KoordinatMusrenbang', 'Volume', 'Satuan', 'Anggaran', 'KomponenName', 'KomponenHarga', 'KomponenPajak', 'DetailName', 'KomponenRekening', 'KomponenTipe', 'Subtitle', 'KomponenId', 'CreatedBy', 'UpdatedBy', 'CreatedAt', 'UpdatedAt', ),
		BasePeer::TYPE_COLNAME => array (MusrenbangRKAPeer::ID, MusrenbangRKAPeer::STATUS_HAPUS, MusrenbangRKAPeer::UNIT_ID, MusrenbangRKAPeer::KEGIATAN_CODE, MusrenbangRKAPeer::DETAIL_NO, MusrenbangRKAPeer::ID_MUSRENBANG, MusrenbangRKAPeer::TAHUN_MUSRENBANG, MusrenbangRKAPeer::KOORDINAT_MUSRENBANG, MusrenbangRKAPeer::VOLUME, MusrenbangRKAPeer::SATUAN, MusrenbangRKAPeer::ANGGARAN, MusrenbangRKAPeer::KOMPONEN_NAME, MusrenbangRKAPeer::KOMPONEN_HARGA, MusrenbangRKAPeer::KOMPONEN_PAJAK, MusrenbangRKAPeer::DETAIL_NAME, MusrenbangRKAPeer::KOMPONEN_REKENING, MusrenbangRKAPeer::KOMPONEN_TIPE, MusrenbangRKAPeer::SUBTITLE, MusrenbangRKAPeer::KOMPONEN_ID, MusrenbangRKAPeer::CREATED_BY, MusrenbangRKAPeer::UPDATED_BY, MusrenbangRKAPeer::CREATED_AT, MusrenbangRKAPeer::UPDATED_AT, ),
		BasePeer::TYPE_FIELDNAME => array ('id', 'status_hapus', 'unit_id', 'kegiatan_code', 'detail_no', 'id_musrenbang', 'tahun_musrenbang', 'koordinat_musrenbang', 'volume', 'satuan', 'anggaran', 'komponen_name', 'komponen_harga', 'komponen_pajak', 'detail_name', 'komponen_rekening', 'komponen_tipe', 'subtitle', 'komponen_id', 'created_by', 'updated_by', 'created_at', 'updated_at', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('Id' => 0, 'StatusHapus' => 1, 'UnitId' => 2, 'KegiatanCode' => 3, 'DetailNo' => 4, 'IdMusrenbang' => 5, 'TahunMusrenbang' => 6, 'KoordinatMusrenbang' => 7, 'Volume' => 8, 'Satuan' => 9, 'Anggaran' => 10, 'KomponenName' => 11, 'KomponenHarga' => 12, 'KomponenPajak' => 13, 'DetailName' => 14, 'KomponenRekening' => 15, 'KomponenTipe' => 16, 'Subtitle' => 17, 'KomponenId' => 18, 'CreatedBy' => 19, 'UpdatedBy' => 20, 'CreatedAt' => 21, 'UpdatedAt' => 22, ),
		BasePeer::TYPE_COLNAME => array (MusrenbangRKAPeer::ID => 0, MusrenbangRKAPeer::STATUS_HAPUS => 1, MusrenbangRKAPeer::UNIT_ID => 2, MusrenbangRKAPeer::KEGIATAN_CODE => 3, MusrenbangRKAPeer::DETAIL_NO => 4, MusrenbangRKAPeer::ID_MUSRENBANG => 5, MusrenbangRKAPeer::TAHUN_MUSRENBANG => 6, MusrenbangRKAPeer::KOORDINAT_MUSRENBANG => 7, MusrenbangRKAPeer::VOLUME => 8, MusrenbangRKAPeer::SATUAN => 9, MusrenbangRKAPeer::ANGGARAN => 10, MusrenbangRKAPeer::KOMPONEN_NAME => 11, MusrenbangRKAPeer::KOMPONEN_HARGA => 12, MusrenbangRKAPeer::KOMPONEN_PAJAK => 13, MusrenbangRKAPeer::DETAIL_NAME => 14, MusrenbangRKAPeer::KOMPONEN_REKENING => 15, MusrenbangRKAPeer::KOMPONEN_TIPE => 16, MusrenbangRKAPeer::SUBTITLE => 17, MusrenbangRKAPeer::KOMPONEN_ID => 18, MusrenbangRKAPeer::CREATED_BY => 19, MusrenbangRKAPeer::UPDATED_BY => 20, MusrenbangRKAPeer::CREATED_AT => 21, MusrenbangRKAPeer::UPDATED_AT => 22, ),
		BasePeer::TYPE_FIELDNAME => array ('id' => 0, 'status_hapus' => 1, 'unit_id' => 2, 'kegiatan_code' => 3, 'detail_no' => 4, 'id_musrenbang' => 5, 'tahun_musrenbang' => 6, 'koordinat_musrenbang' => 7, 'volume' => 8, 'satuan' => 9, 'anggaran' => 10, 'komponen_name' => 11, 'komponen_harga' => 12, 'komponen_pajak' => 13, 'detail_name' => 14, 'komponen_rekening' => 15, 'komponen_tipe' => 16, 'subtitle' => 17, 'komponen_id' => 18, 'created_by' => 19, 'updated_by' => 20, 'created_at' => 21, 'updated_at' => 22, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/MusrenbangRKAMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.MusrenbangRKAMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = MusrenbangRKAPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(MusrenbangRKAPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(MusrenbangRKAPeer::ID);

		$criteria->addSelectColumn(MusrenbangRKAPeer::STATUS_HAPUS);

		$criteria->addSelectColumn(MusrenbangRKAPeer::UNIT_ID);

		$criteria->addSelectColumn(MusrenbangRKAPeer::KEGIATAN_CODE);

		$criteria->addSelectColumn(MusrenbangRKAPeer::DETAIL_NO);

		$criteria->addSelectColumn(MusrenbangRKAPeer::ID_MUSRENBANG);

		$criteria->addSelectColumn(MusrenbangRKAPeer::TAHUN_MUSRENBANG);

		$criteria->addSelectColumn(MusrenbangRKAPeer::KOORDINAT_MUSRENBANG);

		$criteria->addSelectColumn(MusrenbangRKAPeer::VOLUME);

		$criteria->addSelectColumn(MusrenbangRKAPeer::SATUAN);

		$criteria->addSelectColumn(MusrenbangRKAPeer::ANGGARAN);

		$criteria->addSelectColumn(MusrenbangRKAPeer::KOMPONEN_NAME);

		$criteria->addSelectColumn(MusrenbangRKAPeer::KOMPONEN_HARGA);

		$criteria->addSelectColumn(MusrenbangRKAPeer::KOMPONEN_PAJAK);

		$criteria->addSelectColumn(MusrenbangRKAPeer::DETAIL_NAME);

		$criteria->addSelectColumn(MusrenbangRKAPeer::KOMPONEN_REKENING);

		$criteria->addSelectColumn(MusrenbangRKAPeer::KOMPONEN_TIPE);

		$criteria->addSelectColumn(MusrenbangRKAPeer::SUBTITLE);

		$criteria->addSelectColumn(MusrenbangRKAPeer::KOMPONEN_ID);

		$criteria->addSelectColumn(MusrenbangRKAPeer::CREATED_BY);

		$criteria->addSelectColumn(MusrenbangRKAPeer::UPDATED_BY);

		$criteria->addSelectColumn(MusrenbangRKAPeer::CREATED_AT);

		$criteria->addSelectColumn(MusrenbangRKAPeer::UPDATED_AT);

	}

	const COUNT = 'COUNT(ebudget.musrenbang_rka.ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.musrenbang_rka.ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(MusrenbangRKAPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(MusrenbangRKAPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = MusrenbangRKAPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = MusrenbangRKAPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return MusrenbangRKAPeer::populateObjects(MusrenbangRKAPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			MusrenbangRKAPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = MusrenbangRKAPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return MusrenbangRKAPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(MusrenbangRKAPeer::ID);
			$selectCriteria->add(MusrenbangRKAPeer::ID, $criteria->remove(MusrenbangRKAPeer::ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(MusrenbangRKAPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(MusrenbangRKAPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof MusrenbangRKA) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(MusrenbangRKAPeer::ID, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(MusrenbangRKA $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(MusrenbangRKAPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(MusrenbangRKAPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(MusrenbangRKAPeer::DATABASE_NAME, MusrenbangRKAPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = MusrenbangRKAPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(MusrenbangRKAPeer::DATABASE_NAME);

		$criteria->add(MusrenbangRKAPeer::ID, $pk);


		$v = MusrenbangRKAPeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(MusrenbangRKAPeer::ID, $pks, Criteria::IN);
			$objs = MusrenbangRKAPeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BaseMusrenbangRKAPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/MusrenbangRKAMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.MusrenbangRKAMapBuilder');
}
