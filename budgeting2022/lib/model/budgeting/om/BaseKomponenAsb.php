<?php


abstract class BaseKomponenAsb extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $komponen_id;


	
	protected $satuan;


	
	protected $komponen_name;


	
	protected $shsd_id;


	
	protected $komponen_harga;


	
	protected $komponen_show = true;


	
	protected $ip_address;


	
	protected $waktu_access;


	
	protected $komponen_tipe;


	
	protected $komponen_confirmed = false;


	
	protected $komponen_non_pajak = false;


	
	protected $user_id;


	
	protected $rekening;


	
	protected $kelompok;


	
	protected $pemeliharaan = 0;


	
	protected $rek_upah;


	
	protected $rek_bahan;


	
	protected $rek_sewa;


	
	protected $deskripsi;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getKomponenId()
	{

		return $this->komponen_id;
	}

	
	public function getSatuan()
	{

		return $this->satuan;
	}

	
	public function getKomponenName()
	{

		return $this->komponen_name;
	}

	
	public function getShsdId()
	{

		return $this->shsd_id;
	}

	
	public function getKomponenHarga()
	{

		return $this->komponen_harga;
	}

	
	public function getKomponenShow()
	{

		return $this->komponen_show;
	}

	
	public function getIpAddress()
	{

		return $this->ip_address;
	}

	
	public function getWaktuAccess($format = 'Y-m-d H:i:s')
	{

		if ($this->waktu_access === null || $this->waktu_access === '') {
			return null;
		} elseif (!is_int($this->waktu_access)) {
						$ts = strtotime($this->waktu_access);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [waktu_access] as date/time value: " . var_export($this->waktu_access, true));
			}
		} else {
			$ts = $this->waktu_access;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getKomponenTipe()
	{

		return $this->komponen_tipe;
	}

	
	public function getKomponenConfirmed()
	{

		return $this->komponen_confirmed;
	}

	
	public function getKomponenNonPajak()
	{

		return $this->komponen_non_pajak;
	}

	
	public function getUserId()
	{

		return $this->user_id;
	}

	
	public function getRekening()
	{

		return $this->rekening;
	}

	
	public function getKelompok()
	{

		return $this->kelompok;
	}

	
	public function getPemeliharaan()
	{

		return $this->pemeliharaan;
	}

	
	public function getRekUpah()
	{

		return $this->rek_upah;
	}

	
	public function getRekBahan()
	{

		return $this->rek_bahan;
	}

	
	public function getRekSewa()
	{

		return $this->rek_sewa;
	}

	
	public function getDeskripsi()
	{

		return $this->deskripsi;
	}

	
	public function setKomponenId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->komponen_id !== $v) {
			$this->komponen_id = $v;
			$this->modifiedColumns[] = KomponenAsbPeer::KOMPONEN_ID;
		}

	} 
	
	public function setSatuan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->satuan !== $v) {
			$this->satuan = $v;
			$this->modifiedColumns[] = KomponenAsbPeer::SATUAN;
		}

	} 
	
	public function setKomponenName($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->komponen_name !== $v) {
			$this->komponen_name = $v;
			$this->modifiedColumns[] = KomponenAsbPeer::KOMPONEN_NAME;
		}

	} 
	
	public function setShsdId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->shsd_id !== $v) {
			$this->shsd_id = $v;
			$this->modifiedColumns[] = KomponenAsbPeer::SHSD_ID;
		}

	} 
	
	public function setKomponenHarga($v)
	{

		if ($this->komponen_harga !== $v) {
			$this->komponen_harga = $v;
			$this->modifiedColumns[] = KomponenAsbPeer::KOMPONEN_HARGA;
		}

	} 
	
	public function setKomponenShow($v)
	{

		if ($this->komponen_show !== $v || $v === true) {
			$this->komponen_show = $v;
			$this->modifiedColumns[] = KomponenAsbPeer::KOMPONEN_SHOW;
		}

	} 
	
	public function setIpAddress($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->ip_address !== $v) {
			$this->ip_address = $v;
			$this->modifiedColumns[] = KomponenAsbPeer::IP_ADDRESS;
		}

	} 
	
	public function setWaktuAccess($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [waktu_access] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->waktu_access !== $ts) {
			$this->waktu_access = $ts;
			$this->modifiedColumns[] = KomponenAsbPeer::WAKTU_ACCESS;
		}

	} 
	
	public function setKomponenTipe($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->komponen_tipe !== $v) {
			$this->komponen_tipe = $v;
			$this->modifiedColumns[] = KomponenAsbPeer::KOMPONEN_TIPE;
		}

	} 
	
	public function setKomponenConfirmed($v)
	{

		if ($this->komponen_confirmed !== $v || $v === false) {
			$this->komponen_confirmed = $v;
			$this->modifiedColumns[] = KomponenAsbPeer::KOMPONEN_CONFIRMED;
		}

	} 
	
	public function setKomponenNonPajak($v)
	{

		if ($this->komponen_non_pajak !== $v || $v === false) {
			$this->komponen_non_pajak = $v;
			$this->modifiedColumns[] = KomponenAsbPeer::KOMPONEN_NON_PAJAK;
		}

	} 
	
	public function setUserId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->user_id !== $v) {
			$this->user_id = $v;
			$this->modifiedColumns[] = KomponenAsbPeer::USER_ID;
		}

	} 
	
	public function setRekening($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->rekening !== $v) {
			$this->rekening = $v;
			$this->modifiedColumns[] = KomponenAsbPeer::REKENING;
		}

	} 
	
	public function setKelompok($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kelompok !== $v) {
			$this->kelompok = $v;
			$this->modifiedColumns[] = KomponenAsbPeer::KELOMPOK;
		}

	} 
	
	public function setPemeliharaan($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->pemeliharaan !== $v || $v === 0) {
			$this->pemeliharaan = $v;
			$this->modifiedColumns[] = KomponenAsbPeer::PEMELIHARAAN;
		}

	} 
	
	public function setRekUpah($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->rek_upah !== $v) {
			$this->rek_upah = $v;
			$this->modifiedColumns[] = KomponenAsbPeer::REK_UPAH;
		}

	} 
	
	public function setRekBahan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->rek_bahan !== $v) {
			$this->rek_bahan = $v;
			$this->modifiedColumns[] = KomponenAsbPeer::REK_BAHAN;
		}

	} 
	
	public function setRekSewa($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->rek_sewa !== $v) {
			$this->rek_sewa = $v;
			$this->modifiedColumns[] = KomponenAsbPeer::REK_SEWA;
		}

	} 
	
	public function setDeskripsi($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->deskripsi !== $v) {
			$this->deskripsi = $v;
			$this->modifiedColumns[] = KomponenAsbPeer::DESKRIPSI;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->komponen_id = $rs->getString($startcol + 0);

			$this->satuan = $rs->getString($startcol + 1);

			$this->komponen_name = $rs->getString($startcol + 2);

			$this->shsd_id = $rs->getString($startcol + 3);

			$this->komponen_harga = $rs->getFloat($startcol + 4);

			$this->komponen_show = $rs->getBoolean($startcol + 5);

			$this->ip_address = $rs->getString($startcol + 6);

			$this->waktu_access = $rs->getTimestamp($startcol + 7, null);

			$this->komponen_tipe = $rs->getString($startcol + 8);

			$this->komponen_confirmed = $rs->getBoolean($startcol + 9);

			$this->komponen_non_pajak = $rs->getBoolean($startcol + 10);

			$this->user_id = $rs->getString($startcol + 11);

			$this->rekening = $rs->getString($startcol + 12);

			$this->kelompok = $rs->getString($startcol + 13);

			$this->pemeliharaan = $rs->getInt($startcol + 14);

			$this->rek_upah = $rs->getString($startcol + 15);

			$this->rek_bahan = $rs->getString($startcol + 16);

			$this->rek_sewa = $rs->getString($startcol + 17);

			$this->deskripsi = $rs->getString($startcol + 18);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 19; 
		} catch (Exception $e) {
			throw new PropelException("Error populating KomponenAsb object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(KomponenAsbPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			KomponenAsbPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(KomponenAsbPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = KomponenAsbPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += KomponenAsbPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = KomponenAsbPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = KomponenAsbPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getKomponenId();
				break;
			case 1:
				return $this->getSatuan();
				break;
			case 2:
				return $this->getKomponenName();
				break;
			case 3:
				return $this->getShsdId();
				break;
			case 4:
				return $this->getKomponenHarga();
				break;
			case 5:
				return $this->getKomponenShow();
				break;
			case 6:
				return $this->getIpAddress();
				break;
			case 7:
				return $this->getWaktuAccess();
				break;
			case 8:
				return $this->getKomponenTipe();
				break;
			case 9:
				return $this->getKomponenConfirmed();
				break;
			case 10:
				return $this->getKomponenNonPajak();
				break;
			case 11:
				return $this->getUserId();
				break;
			case 12:
				return $this->getRekening();
				break;
			case 13:
				return $this->getKelompok();
				break;
			case 14:
				return $this->getPemeliharaan();
				break;
			case 15:
				return $this->getRekUpah();
				break;
			case 16:
				return $this->getRekBahan();
				break;
			case 17:
				return $this->getRekSewa();
				break;
			case 18:
				return $this->getDeskripsi();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = KomponenAsbPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getKomponenId(),
			$keys[1] => $this->getSatuan(),
			$keys[2] => $this->getKomponenName(),
			$keys[3] => $this->getShsdId(),
			$keys[4] => $this->getKomponenHarga(),
			$keys[5] => $this->getKomponenShow(),
			$keys[6] => $this->getIpAddress(),
			$keys[7] => $this->getWaktuAccess(),
			$keys[8] => $this->getKomponenTipe(),
			$keys[9] => $this->getKomponenConfirmed(),
			$keys[10] => $this->getKomponenNonPajak(),
			$keys[11] => $this->getUserId(),
			$keys[12] => $this->getRekening(),
			$keys[13] => $this->getKelompok(),
			$keys[14] => $this->getPemeliharaan(),
			$keys[15] => $this->getRekUpah(),
			$keys[16] => $this->getRekBahan(),
			$keys[17] => $this->getRekSewa(),
			$keys[18] => $this->getDeskripsi(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = KomponenAsbPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setKomponenId($value);
				break;
			case 1:
				$this->setSatuan($value);
				break;
			case 2:
				$this->setKomponenName($value);
				break;
			case 3:
				$this->setShsdId($value);
				break;
			case 4:
				$this->setKomponenHarga($value);
				break;
			case 5:
				$this->setKomponenShow($value);
				break;
			case 6:
				$this->setIpAddress($value);
				break;
			case 7:
				$this->setWaktuAccess($value);
				break;
			case 8:
				$this->setKomponenTipe($value);
				break;
			case 9:
				$this->setKomponenConfirmed($value);
				break;
			case 10:
				$this->setKomponenNonPajak($value);
				break;
			case 11:
				$this->setUserId($value);
				break;
			case 12:
				$this->setRekening($value);
				break;
			case 13:
				$this->setKelompok($value);
				break;
			case 14:
				$this->setPemeliharaan($value);
				break;
			case 15:
				$this->setRekUpah($value);
				break;
			case 16:
				$this->setRekBahan($value);
				break;
			case 17:
				$this->setRekSewa($value);
				break;
			case 18:
				$this->setDeskripsi($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = KomponenAsbPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setKomponenId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setSatuan($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setKomponenName($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setShsdId($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setKomponenHarga($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setKomponenShow($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setIpAddress($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setWaktuAccess($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setKomponenTipe($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setKomponenConfirmed($arr[$keys[9]]);
		if (array_key_exists($keys[10], $arr)) $this->setKomponenNonPajak($arr[$keys[10]]);
		if (array_key_exists($keys[11], $arr)) $this->setUserId($arr[$keys[11]]);
		if (array_key_exists($keys[12], $arr)) $this->setRekening($arr[$keys[12]]);
		if (array_key_exists($keys[13], $arr)) $this->setKelompok($arr[$keys[13]]);
		if (array_key_exists($keys[14], $arr)) $this->setPemeliharaan($arr[$keys[14]]);
		if (array_key_exists($keys[15], $arr)) $this->setRekUpah($arr[$keys[15]]);
		if (array_key_exists($keys[16], $arr)) $this->setRekBahan($arr[$keys[16]]);
		if (array_key_exists($keys[17], $arr)) $this->setRekSewa($arr[$keys[17]]);
		if (array_key_exists($keys[18], $arr)) $this->setDeskripsi($arr[$keys[18]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(KomponenAsbPeer::DATABASE_NAME);

		if ($this->isColumnModified(KomponenAsbPeer::KOMPONEN_ID)) $criteria->add(KomponenAsbPeer::KOMPONEN_ID, $this->komponen_id);
		if ($this->isColumnModified(KomponenAsbPeer::SATUAN)) $criteria->add(KomponenAsbPeer::SATUAN, $this->satuan);
		if ($this->isColumnModified(KomponenAsbPeer::KOMPONEN_NAME)) $criteria->add(KomponenAsbPeer::KOMPONEN_NAME, $this->komponen_name);
		if ($this->isColumnModified(KomponenAsbPeer::SHSD_ID)) $criteria->add(KomponenAsbPeer::SHSD_ID, $this->shsd_id);
		if ($this->isColumnModified(KomponenAsbPeer::KOMPONEN_HARGA)) $criteria->add(KomponenAsbPeer::KOMPONEN_HARGA, $this->komponen_harga);
		if ($this->isColumnModified(KomponenAsbPeer::KOMPONEN_SHOW)) $criteria->add(KomponenAsbPeer::KOMPONEN_SHOW, $this->komponen_show);
		if ($this->isColumnModified(KomponenAsbPeer::IP_ADDRESS)) $criteria->add(KomponenAsbPeer::IP_ADDRESS, $this->ip_address);
		if ($this->isColumnModified(KomponenAsbPeer::WAKTU_ACCESS)) $criteria->add(KomponenAsbPeer::WAKTU_ACCESS, $this->waktu_access);
		if ($this->isColumnModified(KomponenAsbPeer::KOMPONEN_TIPE)) $criteria->add(KomponenAsbPeer::KOMPONEN_TIPE, $this->komponen_tipe);
		if ($this->isColumnModified(KomponenAsbPeer::KOMPONEN_CONFIRMED)) $criteria->add(KomponenAsbPeer::KOMPONEN_CONFIRMED, $this->komponen_confirmed);
		if ($this->isColumnModified(KomponenAsbPeer::KOMPONEN_NON_PAJAK)) $criteria->add(KomponenAsbPeer::KOMPONEN_NON_PAJAK, $this->komponen_non_pajak);
		if ($this->isColumnModified(KomponenAsbPeer::USER_ID)) $criteria->add(KomponenAsbPeer::USER_ID, $this->user_id);
		if ($this->isColumnModified(KomponenAsbPeer::REKENING)) $criteria->add(KomponenAsbPeer::REKENING, $this->rekening);
		if ($this->isColumnModified(KomponenAsbPeer::KELOMPOK)) $criteria->add(KomponenAsbPeer::KELOMPOK, $this->kelompok);
		if ($this->isColumnModified(KomponenAsbPeer::PEMELIHARAAN)) $criteria->add(KomponenAsbPeer::PEMELIHARAAN, $this->pemeliharaan);
		if ($this->isColumnModified(KomponenAsbPeer::REK_UPAH)) $criteria->add(KomponenAsbPeer::REK_UPAH, $this->rek_upah);
		if ($this->isColumnModified(KomponenAsbPeer::REK_BAHAN)) $criteria->add(KomponenAsbPeer::REK_BAHAN, $this->rek_bahan);
		if ($this->isColumnModified(KomponenAsbPeer::REK_SEWA)) $criteria->add(KomponenAsbPeer::REK_SEWA, $this->rek_sewa);
		if ($this->isColumnModified(KomponenAsbPeer::DESKRIPSI)) $criteria->add(KomponenAsbPeer::DESKRIPSI, $this->deskripsi);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(KomponenAsbPeer::DATABASE_NAME);

		$criteria->add(KomponenAsbPeer::KOMPONEN_ID, $this->komponen_id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getKomponenId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setKomponenId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setSatuan($this->satuan);

		$copyObj->setKomponenName($this->komponen_name);

		$copyObj->setShsdId($this->shsd_id);

		$copyObj->setKomponenHarga($this->komponen_harga);

		$copyObj->setKomponenShow($this->komponen_show);

		$copyObj->setIpAddress($this->ip_address);

		$copyObj->setWaktuAccess($this->waktu_access);

		$copyObj->setKomponenTipe($this->komponen_tipe);

		$copyObj->setKomponenConfirmed($this->komponen_confirmed);

		$copyObj->setKomponenNonPajak($this->komponen_non_pajak);

		$copyObj->setUserId($this->user_id);

		$copyObj->setRekening($this->rekening);

		$copyObj->setKelompok($this->kelompok);

		$copyObj->setPemeliharaan($this->pemeliharaan);

		$copyObj->setRekUpah($this->rek_upah);

		$copyObj->setRekBahan($this->rek_bahan);

		$copyObj->setRekSewa($this->rek_sewa);

		$copyObj->setDeskripsi($this->deskripsi);


		$copyObj->setNew(true);

		$copyObj->setKomponenId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new KomponenAsbPeer();
		}
		return self::$peer;
	}

} 