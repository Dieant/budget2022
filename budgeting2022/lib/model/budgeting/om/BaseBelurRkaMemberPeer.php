<?php


abstract class BaseBelurRkaMemberPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.belur_rka_member';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.BelurRkaMember';

	
	const NUM_COLUMNS = 9;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const KODE_SUB = 'ebudget.belur_rka_member.KODE_SUB';

	
	const UNIT_ID = 'ebudget.belur_rka_member.UNIT_ID';

	
	const KEGIATAN_CODE = 'ebudget.belur_rka_member.KEGIATAN_CODE';

	
	const KOMPONEN_ID = 'ebudget.belur_rka_member.KOMPONEN_ID';

	
	const KOMPONEN_NAME = 'ebudget.belur_rka_member.KOMPONEN_NAME';

	
	const DETAIL_NAME = 'ebudget.belur_rka_member.DETAIL_NAME';

	
	const REKENING_ASLI = 'ebudget.belur_rka_member.REKENING_ASLI';

	
	const TAHUN = 'ebudget.belur_rka_member.TAHUN';

	
	const DETAIL_NO = 'ebudget.belur_rka_member.DETAIL_NO';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('KodeSub', 'UnitId', 'KegiatanCode', 'KomponenId', 'KomponenName', 'DetailName', 'RekeningAsli', 'Tahun', 'DetailNo', ),
		BasePeer::TYPE_COLNAME => array (BelurRkaMemberPeer::KODE_SUB, BelurRkaMemberPeer::UNIT_ID, BelurRkaMemberPeer::KEGIATAN_CODE, BelurRkaMemberPeer::KOMPONEN_ID, BelurRkaMemberPeer::KOMPONEN_NAME, BelurRkaMemberPeer::DETAIL_NAME, BelurRkaMemberPeer::REKENING_ASLI, BelurRkaMemberPeer::TAHUN, BelurRkaMemberPeer::DETAIL_NO, ),
		BasePeer::TYPE_FIELDNAME => array ('kode_sub', 'unit_id', 'kegiatan_code', 'komponen_id', 'komponen_name', 'detail_name', 'rekening_asli', 'tahun', 'detail_no', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('KodeSub' => 0, 'UnitId' => 1, 'KegiatanCode' => 2, 'KomponenId' => 3, 'KomponenName' => 4, 'DetailName' => 5, 'RekeningAsli' => 6, 'Tahun' => 7, 'DetailNo' => 8, ),
		BasePeer::TYPE_COLNAME => array (BelurRkaMemberPeer::KODE_SUB => 0, BelurRkaMemberPeer::UNIT_ID => 1, BelurRkaMemberPeer::KEGIATAN_CODE => 2, BelurRkaMemberPeer::KOMPONEN_ID => 3, BelurRkaMemberPeer::KOMPONEN_NAME => 4, BelurRkaMemberPeer::DETAIL_NAME => 5, BelurRkaMemberPeer::REKENING_ASLI => 6, BelurRkaMemberPeer::TAHUN => 7, BelurRkaMemberPeer::DETAIL_NO => 8, ),
		BasePeer::TYPE_FIELDNAME => array ('kode_sub' => 0, 'unit_id' => 1, 'kegiatan_code' => 2, 'komponen_id' => 3, 'komponen_name' => 4, 'detail_name' => 5, 'rekening_asli' => 6, 'tahun' => 7, 'detail_no' => 8, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/BelurRkaMemberMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.BelurRkaMemberMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = BelurRkaMemberPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(BelurRkaMemberPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(BelurRkaMemberPeer::KODE_SUB);

		$criteria->addSelectColumn(BelurRkaMemberPeer::UNIT_ID);

		$criteria->addSelectColumn(BelurRkaMemberPeer::KEGIATAN_CODE);

		$criteria->addSelectColumn(BelurRkaMemberPeer::KOMPONEN_ID);

		$criteria->addSelectColumn(BelurRkaMemberPeer::KOMPONEN_NAME);

		$criteria->addSelectColumn(BelurRkaMemberPeer::DETAIL_NAME);

		$criteria->addSelectColumn(BelurRkaMemberPeer::REKENING_ASLI);

		$criteria->addSelectColumn(BelurRkaMemberPeer::TAHUN);

		$criteria->addSelectColumn(BelurRkaMemberPeer::DETAIL_NO);

	}

	const COUNT = 'COUNT(ebudget.belur_rka_member.KODE_SUB)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.belur_rka_member.KODE_SUB)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(BelurRkaMemberPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(BelurRkaMemberPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = BelurRkaMemberPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = BelurRkaMemberPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return BelurRkaMemberPeer::populateObjects(BelurRkaMemberPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			BelurRkaMemberPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = BelurRkaMemberPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return BelurRkaMemberPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(BelurRkaMemberPeer::KODE_SUB);
			$selectCriteria->add(BelurRkaMemberPeer::KODE_SUB, $criteria->remove(BelurRkaMemberPeer::KODE_SUB), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(BelurRkaMemberPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(BelurRkaMemberPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof BelurRkaMember) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(BelurRkaMemberPeer::KODE_SUB, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(BelurRkaMember $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(BelurRkaMemberPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(BelurRkaMemberPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(BelurRkaMemberPeer::DATABASE_NAME, BelurRkaMemberPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = BelurRkaMemberPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(BelurRkaMemberPeer::DATABASE_NAME);

		$criteria->add(BelurRkaMemberPeer::KODE_SUB, $pk);


		$v = BelurRkaMemberPeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(BelurRkaMemberPeer::KODE_SUB, $pks, Criteria::IN);
			$objs = BelurRkaMemberPeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BaseBelurRkaMemberPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/BelurRkaMemberMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.BelurRkaMemberMapBuilder');
}
