<?php


abstract class BaseMasterTujuan extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $kode_tujuan;


	
	protected $nama_tujuan;


	
	protected $kode_misi;


	
	protected $id;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getKodeTujuan()
	{

		return $this->kode_tujuan;
	}

	
	public function getNamaTujuan()
	{

		return $this->nama_tujuan;
	}

	
	public function getKodeMisi()
	{

		return $this->kode_misi;
	}

	
	public function getId()
	{

		return $this->id;
	}

	
	public function setKodeTujuan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_tujuan !== $v) {
			$this->kode_tujuan = $v;
			$this->modifiedColumns[] = MasterTujuanPeer::KODE_TUJUAN;
		}

	} 
	
	public function setNamaTujuan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nama_tujuan !== $v) {
			$this->nama_tujuan = $v;
			$this->modifiedColumns[] = MasterTujuanPeer::NAMA_TUJUAN;
		}

	} 
	
	public function setKodeMisi($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_misi !== $v) {
			$this->kode_misi = $v;
			$this->modifiedColumns[] = MasterTujuanPeer::KODE_MISI;
		}

	} 
	
	public function setId($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = MasterTujuanPeer::ID;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->kode_tujuan = $rs->getString($startcol + 0);

			$this->nama_tujuan = $rs->getString($startcol + 1);

			$this->kode_misi = $rs->getString($startcol + 2);

			$this->id = $rs->getInt($startcol + 3);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 4; 
		} catch (Exception $e) {
			throw new PropelException("Error populating MasterTujuan object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(MasterTujuanPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			MasterTujuanPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(MasterTujuanPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = MasterTujuanPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setId($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += MasterTujuanPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = MasterTujuanPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = MasterTujuanPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getKodeTujuan();
				break;
			case 1:
				return $this->getNamaTujuan();
				break;
			case 2:
				return $this->getKodeMisi();
				break;
			case 3:
				return $this->getId();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = MasterTujuanPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getKodeTujuan(),
			$keys[1] => $this->getNamaTujuan(),
			$keys[2] => $this->getKodeMisi(),
			$keys[3] => $this->getId(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = MasterTujuanPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setKodeTujuan($value);
				break;
			case 1:
				$this->setNamaTujuan($value);
				break;
			case 2:
				$this->setKodeMisi($value);
				break;
			case 3:
				$this->setId($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = MasterTujuanPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setKodeTujuan($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setNamaTujuan($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setKodeMisi($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setId($arr[$keys[3]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(MasterTujuanPeer::DATABASE_NAME);

		if ($this->isColumnModified(MasterTujuanPeer::KODE_TUJUAN)) $criteria->add(MasterTujuanPeer::KODE_TUJUAN, $this->kode_tujuan);
		if ($this->isColumnModified(MasterTujuanPeer::NAMA_TUJUAN)) $criteria->add(MasterTujuanPeer::NAMA_TUJUAN, $this->nama_tujuan);
		if ($this->isColumnModified(MasterTujuanPeer::KODE_MISI)) $criteria->add(MasterTujuanPeer::KODE_MISI, $this->kode_misi);
		if ($this->isColumnModified(MasterTujuanPeer::ID)) $criteria->add(MasterTujuanPeer::ID, $this->id);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(MasterTujuanPeer::DATABASE_NAME);

		$criteria->add(MasterTujuanPeer::ID, $this->id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setKodeTujuan($this->kode_tujuan);

		$copyObj->setNamaTujuan($this->nama_tujuan);

		$copyObj->setKodeMisi($this->kode_misi);


		$copyObj->setNew(true);

		$copyObj->setId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new MasterTujuanPeer();
		}
		return self::$peer;
	}

} 