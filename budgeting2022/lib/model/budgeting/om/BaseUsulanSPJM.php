<?php


abstract class BaseUsulanSPJM extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id_spjm;


	
	protected $tgl_spjm;


	
	protected $created_at;


	
	protected $updated_at;


	
	protected $penyelia;


	
	protected $skpd;


	
	protected $tipe_usulan;


	
	protected $jenis_usulan;


	
	protected $jumlah_dukungan;


	
	protected $status_verifikasi;


	
	protected $status_hapus;


	
	protected $filepath;


	
	protected $filepath_pdf;


	
	protected $id_token;


	
	protected $nomor_surat;


	
	protected $komentar_verifikator;


	
	protected $tahap;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getIdSpjm()
	{

		return $this->id_spjm;
	}

	
	public function getTglSpjm($format = 'Y-m-d H:i:s')
	{

		if ($this->tgl_spjm === null || $this->tgl_spjm === '') {
			return null;
		} elseif (!is_int($this->tgl_spjm)) {
						$ts = strtotime($this->tgl_spjm);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [tgl_spjm] as date/time value: " . var_export($this->tgl_spjm, true));
			}
		} else {
			$ts = $this->tgl_spjm;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getCreatedAt($format = 'Y-m-d H:i:s')
	{

		if ($this->created_at === null || $this->created_at === '') {
			return null;
		} elseif (!is_int($this->created_at)) {
						$ts = strtotime($this->created_at);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [created_at] as date/time value: " . var_export($this->created_at, true));
			}
		} else {
			$ts = $this->created_at;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getUpdatedAt($format = 'Y-m-d H:i:s')
	{

		if ($this->updated_at === null || $this->updated_at === '') {
			return null;
		} elseif (!is_int($this->updated_at)) {
						$ts = strtotime($this->updated_at);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [updated_at] as date/time value: " . var_export($this->updated_at, true));
			}
		} else {
			$ts = $this->updated_at;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getPenyelia()
	{

		return $this->penyelia;
	}

	
	public function getSkpd()
	{

		return $this->skpd;
	}

	
	public function getTipeUsulan()
	{

		return $this->tipe_usulan;
	}

	
	public function getJenisUsulan()
	{

		return $this->jenis_usulan;
	}

	
	public function getJumlahDukungan()
	{

		return $this->jumlah_dukungan;
	}

	
	public function getStatusVerifikasi()
	{

		return $this->status_verifikasi;
	}

	
	public function getStatusHapus()
	{

		return $this->status_hapus;
	}

	
	public function getFilepath()
	{

		return $this->filepath;
	}

	
	public function getFilepathPdf()
	{

		return $this->filepath_pdf;
	}

	
	public function getIdToken()
	{

		return $this->id_token;
	}

	
	public function getNomorSurat()
	{

		return $this->nomor_surat;
	}

	
	public function getKomentarVerifikator()
	{

		return $this->komentar_verifikator;
	}

	
	public function getTahap()
	{

		return $this->tahap;
	}

	
	public function setIdSpjm($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id_spjm !== $v) {
			$this->id_spjm = $v;
			$this->modifiedColumns[] = UsulanSPJMPeer::ID_SPJM;
		}

	} 
	
	public function setTglSpjm($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [tgl_spjm] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->tgl_spjm !== $ts) {
			$this->tgl_spjm = $ts;
			$this->modifiedColumns[] = UsulanSPJMPeer::TGL_SPJM;
		}

	} 
	
	public function setCreatedAt($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [created_at] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->created_at !== $ts) {
			$this->created_at = $ts;
			$this->modifiedColumns[] = UsulanSPJMPeer::CREATED_AT;
		}

	} 
	
	public function setUpdatedAt($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [updated_at] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->updated_at !== $ts) {
			$this->updated_at = $ts;
			$this->modifiedColumns[] = UsulanSPJMPeer::UPDATED_AT;
		}

	} 
	
	public function setPenyelia($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->penyelia !== $v) {
			$this->penyelia = $v;
			$this->modifiedColumns[] = UsulanSPJMPeer::PENYELIA;
		}

	} 
	
	public function setSkpd($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->skpd !== $v) {
			$this->skpd = $v;
			$this->modifiedColumns[] = UsulanSPJMPeer::SKPD;
		}

	} 
	
	public function setTipeUsulan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->tipe_usulan !== $v) {
			$this->tipe_usulan = $v;
			$this->modifiedColumns[] = UsulanSPJMPeer::TIPE_USULAN;
		}

	} 
	
	public function setJenisUsulan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->jenis_usulan !== $v) {
			$this->jenis_usulan = $v;
			$this->modifiedColumns[] = UsulanSPJMPeer::JENIS_USULAN;
		}

	} 
	
	public function setJumlahDukungan($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->jumlah_dukungan !== $v) {
			$this->jumlah_dukungan = $v;
			$this->modifiedColumns[] = UsulanSPJMPeer::JUMLAH_DUKUNGAN;
		}

	} 
	
	public function setStatusVerifikasi($v)
	{

		if ($this->status_verifikasi !== $v) {
			$this->status_verifikasi = $v;
			$this->modifiedColumns[] = UsulanSPJMPeer::STATUS_VERIFIKASI;
		}

	} 
	
	public function setStatusHapus($v)
	{

		if ($this->status_hapus !== $v) {
			$this->status_hapus = $v;
			$this->modifiedColumns[] = UsulanSPJMPeer::STATUS_HAPUS;
		}

	} 
	
	public function setFilepath($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->filepath !== $v) {
			$this->filepath = $v;
			$this->modifiedColumns[] = UsulanSPJMPeer::FILEPATH;
		}

	} 
	
	public function setFilepathPdf($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->filepath_pdf !== $v) {
			$this->filepath_pdf = $v;
			$this->modifiedColumns[] = UsulanSPJMPeer::FILEPATH_PDF;
		}

	} 
	
	public function setIdToken($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id_token !== $v) {
			$this->id_token = $v;
			$this->modifiedColumns[] = UsulanSPJMPeer::ID_TOKEN;
		}

	} 
	
	public function setNomorSurat($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nomor_surat !== $v) {
			$this->nomor_surat = $v;
			$this->modifiedColumns[] = UsulanSPJMPeer::NOMOR_SURAT;
		}

	} 
	
	public function setKomentarVerifikator($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->komentar_verifikator !== $v) {
			$this->komentar_verifikator = $v;
			$this->modifiedColumns[] = UsulanSPJMPeer::KOMENTAR_VERIFIKATOR;
		}

	} 
	
	public function setTahap($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->tahap !== $v) {
			$this->tahap = $v;
			$this->modifiedColumns[] = UsulanSPJMPeer::TAHAP;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id_spjm = $rs->getInt($startcol + 0);

			$this->tgl_spjm = $rs->getTimestamp($startcol + 1, null);

			$this->created_at = $rs->getTimestamp($startcol + 2, null);

			$this->updated_at = $rs->getTimestamp($startcol + 3, null);

			$this->penyelia = $rs->getString($startcol + 4);

			$this->skpd = $rs->getString($startcol + 5);

			$this->tipe_usulan = $rs->getString($startcol + 6);

			$this->jenis_usulan = $rs->getString($startcol + 7);

			$this->jumlah_dukungan = $rs->getInt($startcol + 8);

			$this->status_verifikasi = $rs->getBoolean($startcol + 9);

			$this->status_hapus = $rs->getBoolean($startcol + 10);

			$this->filepath = $rs->getString($startcol + 11);

			$this->filepath_pdf = $rs->getString($startcol + 12);

			$this->id_token = $rs->getInt($startcol + 13);

			$this->nomor_surat = $rs->getString($startcol + 14);

			$this->komentar_verifikator = $rs->getString($startcol + 15);

			$this->tahap = $rs->getString($startcol + 16);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 17; 
		} catch (Exception $e) {
			throw new PropelException("Error populating UsulanSPJM object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(UsulanSPJMPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			UsulanSPJMPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
    if ($this->isNew() && !$this->isColumnModified(UsulanSPJMPeer::CREATED_AT))
    {
      $this->setCreatedAt(time());
    }

    if ($this->isModified() && !$this->isColumnModified(UsulanSPJMPeer::UPDATED_AT))
    {
      $this->setUpdatedAt(time());
    }

		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(UsulanSPJMPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = UsulanSPJMPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += UsulanSPJMPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = UsulanSPJMPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = UsulanSPJMPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getIdSpjm();
				break;
			case 1:
				return $this->getTglSpjm();
				break;
			case 2:
				return $this->getCreatedAt();
				break;
			case 3:
				return $this->getUpdatedAt();
				break;
			case 4:
				return $this->getPenyelia();
				break;
			case 5:
				return $this->getSkpd();
				break;
			case 6:
				return $this->getTipeUsulan();
				break;
			case 7:
				return $this->getJenisUsulan();
				break;
			case 8:
				return $this->getJumlahDukungan();
				break;
			case 9:
				return $this->getStatusVerifikasi();
				break;
			case 10:
				return $this->getStatusHapus();
				break;
			case 11:
				return $this->getFilepath();
				break;
			case 12:
				return $this->getFilepathPdf();
				break;
			case 13:
				return $this->getIdToken();
				break;
			case 14:
				return $this->getNomorSurat();
				break;
			case 15:
				return $this->getKomentarVerifikator();
				break;
			case 16:
				return $this->getTahap();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = UsulanSPJMPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getIdSpjm(),
			$keys[1] => $this->getTglSpjm(),
			$keys[2] => $this->getCreatedAt(),
			$keys[3] => $this->getUpdatedAt(),
			$keys[4] => $this->getPenyelia(),
			$keys[5] => $this->getSkpd(),
			$keys[6] => $this->getTipeUsulan(),
			$keys[7] => $this->getJenisUsulan(),
			$keys[8] => $this->getJumlahDukungan(),
			$keys[9] => $this->getStatusVerifikasi(),
			$keys[10] => $this->getStatusHapus(),
			$keys[11] => $this->getFilepath(),
			$keys[12] => $this->getFilepathPdf(),
			$keys[13] => $this->getIdToken(),
			$keys[14] => $this->getNomorSurat(),
			$keys[15] => $this->getKomentarVerifikator(),
			$keys[16] => $this->getTahap(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = UsulanSPJMPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setIdSpjm($value);
				break;
			case 1:
				$this->setTglSpjm($value);
				break;
			case 2:
				$this->setCreatedAt($value);
				break;
			case 3:
				$this->setUpdatedAt($value);
				break;
			case 4:
				$this->setPenyelia($value);
				break;
			case 5:
				$this->setSkpd($value);
				break;
			case 6:
				$this->setTipeUsulan($value);
				break;
			case 7:
				$this->setJenisUsulan($value);
				break;
			case 8:
				$this->setJumlahDukungan($value);
				break;
			case 9:
				$this->setStatusVerifikasi($value);
				break;
			case 10:
				$this->setStatusHapus($value);
				break;
			case 11:
				$this->setFilepath($value);
				break;
			case 12:
				$this->setFilepathPdf($value);
				break;
			case 13:
				$this->setIdToken($value);
				break;
			case 14:
				$this->setNomorSurat($value);
				break;
			case 15:
				$this->setKomentarVerifikator($value);
				break;
			case 16:
				$this->setTahap($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = UsulanSPJMPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setIdSpjm($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setTglSpjm($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setCreatedAt($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setUpdatedAt($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setPenyelia($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setSkpd($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setTipeUsulan($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setJenisUsulan($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setJumlahDukungan($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setStatusVerifikasi($arr[$keys[9]]);
		if (array_key_exists($keys[10], $arr)) $this->setStatusHapus($arr[$keys[10]]);
		if (array_key_exists($keys[11], $arr)) $this->setFilepath($arr[$keys[11]]);
		if (array_key_exists($keys[12], $arr)) $this->setFilepathPdf($arr[$keys[12]]);
		if (array_key_exists($keys[13], $arr)) $this->setIdToken($arr[$keys[13]]);
		if (array_key_exists($keys[14], $arr)) $this->setNomorSurat($arr[$keys[14]]);
		if (array_key_exists($keys[15], $arr)) $this->setKomentarVerifikator($arr[$keys[15]]);
		if (array_key_exists($keys[16], $arr)) $this->setTahap($arr[$keys[16]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(UsulanSPJMPeer::DATABASE_NAME);

		if ($this->isColumnModified(UsulanSPJMPeer::ID_SPJM)) $criteria->add(UsulanSPJMPeer::ID_SPJM, $this->id_spjm);
		if ($this->isColumnModified(UsulanSPJMPeer::TGL_SPJM)) $criteria->add(UsulanSPJMPeer::TGL_SPJM, $this->tgl_spjm);
		if ($this->isColumnModified(UsulanSPJMPeer::CREATED_AT)) $criteria->add(UsulanSPJMPeer::CREATED_AT, $this->created_at);
		if ($this->isColumnModified(UsulanSPJMPeer::UPDATED_AT)) $criteria->add(UsulanSPJMPeer::UPDATED_AT, $this->updated_at);
		if ($this->isColumnModified(UsulanSPJMPeer::PENYELIA)) $criteria->add(UsulanSPJMPeer::PENYELIA, $this->penyelia);
		if ($this->isColumnModified(UsulanSPJMPeer::SKPD)) $criteria->add(UsulanSPJMPeer::SKPD, $this->skpd);
		if ($this->isColumnModified(UsulanSPJMPeer::TIPE_USULAN)) $criteria->add(UsulanSPJMPeer::TIPE_USULAN, $this->tipe_usulan);
		if ($this->isColumnModified(UsulanSPJMPeer::JENIS_USULAN)) $criteria->add(UsulanSPJMPeer::JENIS_USULAN, $this->jenis_usulan);
		if ($this->isColumnModified(UsulanSPJMPeer::JUMLAH_DUKUNGAN)) $criteria->add(UsulanSPJMPeer::JUMLAH_DUKUNGAN, $this->jumlah_dukungan);
		if ($this->isColumnModified(UsulanSPJMPeer::STATUS_VERIFIKASI)) $criteria->add(UsulanSPJMPeer::STATUS_VERIFIKASI, $this->status_verifikasi);
		if ($this->isColumnModified(UsulanSPJMPeer::STATUS_HAPUS)) $criteria->add(UsulanSPJMPeer::STATUS_HAPUS, $this->status_hapus);
		if ($this->isColumnModified(UsulanSPJMPeer::FILEPATH)) $criteria->add(UsulanSPJMPeer::FILEPATH, $this->filepath);
		if ($this->isColumnModified(UsulanSPJMPeer::FILEPATH_PDF)) $criteria->add(UsulanSPJMPeer::FILEPATH_PDF, $this->filepath_pdf);
		if ($this->isColumnModified(UsulanSPJMPeer::ID_TOKEN)) $criteria->add(UsulanSPJMPeer::ID_TOKEN, $this->id_token);
		if ($this->isColumnModified(UsulanSPJMPeer::NOMOR_SURAT)) $criteria->add(UsulanSPJMPeer::NOMOR_SURAT, $this->nomor_surat);
		if ($this->isColumnModified(UsulanSPJMPeer::KOMENTAR_VERIFIKATOR)) $criteria->add(UsulanSPJMPeer::KOMENTAR_VERIFIKATOR, $this->komentar_verifikator);
		if ($this->isColumnModified(UsulanSPJMPeer::TAHAP)) $criteria->add(UsulanSPJMPeer::TAHAP, $this->tahap);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(UsulanSPJMPeer::DATABASE_NAME);

		$criteria->add(UsulanSPJMPeer::ID_SPJM, $this->id_spjm);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getIdSpjm();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setIdSpjm($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setTglSpjm($this->tgl_spjm);

		$copyObj->setCreatedAt($this->created_at);

		$copyObj->setUpdatedAt($this->updated_at);

		$copyObj->setPenyelia($this->penyelia);

		$copyObj->setSkpd($this->skpd);

		$copyObj->setTipeUsulan($this->tipe_usulan);

		$copyObj->setJenisUsulan($this->jenis_usulan);

		$copyObj->setJumlahDukungan($this->jumlah_dukungan);

		$copyObj->setStatusVerifikasi($this->status_verifikasi);

		$copyObj->setStatusHapus($this->status_hapus);

		$copyObj->setFilepath($this->filepath);

		$copyObj->setFilepathPdf($this->filepath_pdf);

		$copyObj->setIdToken($this->id_token);

		$copyObj->setNomorSurat($this->nomor_surat);

		$copyObj->setKomentarVerifikator($this->komentar_verifikator);

		$copyObj->setTahap($this->tahap);


		$copyObj->setNew(true);

		$copyObj->setIdSpjm(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new UsulanSPJMPeer();
		}
		return self::$peer;
	}

} 