<?php


abstract class BaseSchemaAkses extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $user_id;


	
	protected $schema_id;


	
	protected $level_id;

	
	protected $aMasterUser;

	
	protected $aMasterSchema;

	
	protected $aUserLevel;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getUserId()
	{

		return $this->user_id;
	}

	
	public function getSchemaId()
	{

		return $this->schema_id;
	}

	
	public function getLevelId()
	{

		return $this->level_id;
	}

	
	public function setUserId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->user_id !== $v) {
			$this->user_id = $v;
			$this->modifiedColumns[] = SchemaAksesPeer::USER_ID;
		}

		if ($this->aMasterUser !== null && $this->aMasterUser->getUserId() !== $v) {
			$this->aMasterUser = null;
		}

	} 
	
	public function setSchemaId($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->schema_id !== $v) {
			$this->schema_id = $v;
			$this->modifiedColumns[] = SchemaAksesPeer::SCHEMA_ID;
		}

		if ($this->aMasterSchema !== null && $this->aMasterSchema->getSchemaId() !== $v) {
			$this->aMasterSchema = null;
		}

	} 
	
	public function setLevelId($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->level_id !== $v) {
			$this->level_id = $v;
			$this->modifiedColumns[] = SchemaAksesPeer::LEVEL_ID;
		}

		if ($this->aUserLevel !== null && $this->aUserLevel->getLevelId() !== $v) {
			$this->aUserLevel = null;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->user_id = $rs->getString($startcol + 0);

			$this->schema_id = $rs->getInt($startcol + 1);

			$this->level_id = $rs->getInt($startcol + 2);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 3; 
		} catch (Exception $e) {
			throw new PropelException("Error populating SchemaAkses object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(SchemaAksesPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			SchemaAksesPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(SchemaAksesPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


												
			if ($this->aMasterUser !== null) {
				if ($this->aMasterUser->isModified()) {
					$affectedRows += $this->aMasterUser->save($con);
				}
				$this->setMasterUser($this->aMasterUser);
			}

			if ($this->aMasterSchema !== null) {
				if ($this->aMasterSchema->isModified()) {
					$affectedRows += $this->aMasterSchema->save($con);
				}
				$this->setMasterSchema($this->aMasterSchema);
			}

			if ($this->aUserLevel !== null) {
				if ($this->aUserLevel->isModified()) {
					$affectedRows += $this->aUserLevel->save($con);
				}
				$this->setUserLevel($this->aUserLevel);
			}


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = SchemaAksesPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += SchemaAksesPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


												
			if ($this->aMasterUser !== null) {
				if (!$this->aMasterUser->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aMasterUser->getValidationFailures());
				}
			}

			if ($this->aMasterSchema !== null) {
				if (!$this->aMasterSchema->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aMasterSchema->getValidationFailures());
				}
			}

			if ($this->aUserLevel !== null) {
				if (!$this->aUserLevel->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aUserLevel->getValidationFailures());
				}
			}


			if (($retval = SchemaAksesPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = SchemaAksesPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getUserId();
				break;
			case 1:
				return $this->getSchemaId();
				break;
			case 2:
				return $this->getLevelId();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = SchemaAksesPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getUserId(),
			$keys[1] => $this->getSchemaId(),
			$keys[2] => $this->getLevelId(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = SchemaAksesPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setUserId($value);
				break;
			case 1:
				$this->setSchemaId($value);
				break;
			case 2:
				$this->setLevelId($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = SchemaAksesPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setUserId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setSchemaId($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setLevelId($arr[$keys[2]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(SchemaAksesPeer::DATABASE_NAME);

		if ($this->isColumnModified(SchemaAksesPeer::USER_ID)) $criteria->add(SchemaAksesPeer::USER_ID, $this->user_id);
		if ($this->isColumnModified(SchemaAksesPeer::SCHEMA_ID)) $criteria->add(SchemaAksesPeer::SCHEMA_ID, $this->schema_id);
		if ($this->isColumnModified(SchemaAksesPeer::LEVEL_ID)) $criteria->add(SchemaAksesPeer::LEVEL_ID, $this->level_id);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(SchemaAksesPeer::DATABASE_NAME);

		$criteria->add(SchemaAksesPeer::USER_ID, $this->user_id);
		$criteria->add(SchemaAksesPeer::SCHEMA_ID, $this->schema_id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		$pks = array();

		$pks[0] = $this->getUserId();

		$pks[1] = $this->getSchemaId();

		return $pks;
	}

	
	public function setPrimaryKey($keys)
	{

		$this->setUserId($keys[0]);

		$this->setSchemaId($keys[1]);

	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setLevelId($this->level_id);


		$copyObj->setNew(true);

		$copyObj->setUserId(NULL); 
		$copyObj->setSchemaId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new SchemaAksesPeer();
		}
		return self::$peer;
	}

	
	public function setMasterUser($v)
	{


		if ($v === null) {
			$this->setUserId(NULL);
		} else {
			$this->setUserId($v->getUserId());
		}


		$this->aMasterUser = $v;
	}


	
	public function getMasterUser($con = null)
	{
		if ($this->aMasterUser === null && (($this->user_id !== "" && $this->user_id !== null))) {
						include_once 'lib/model/budgeting/om/BaseMasterUserPeer.php';

			$this->aMasterUser = MasterUserPeer::retrieveByPK($this->user_id, $con);

			
		}
		return $this->aMasterUser;
	}

	
	public function setMasterSchema($v)
	{


		if ($v === null) {
			$this->setSchemaId(NULL);
		} else {
			$this->setSchemaId($v->getSchemaId());
		}


		$this->aMasterSchema = $v;
	}


	
	public function getMasterSchema($con = null)
	{
		if ($this->aMasterSchema === null && ($this->schema_id !== null)) {
						include_once 'lib/model/budgeting/om/BaseMasterSchemaPeer.php';

			$this->aMasterSchema = MasterSchemaPeer::retrieveByPK($this->schema_id, $con);

			
		}
		return $this->aMasterSchema;
	}

	
	public function setUserLevel($v)
	{


		if ($v === null) {
			$this->setLevelId(NULL);
		} else {
			$this->setLevelId($v->getLevelId());
		}


		$this->aUserLevel = $v;
	}


	
	public function getUserLevel($con = null)
	{
		if ($this->aUserLevel === null && ($this->level_id !== null)) {
						include_once 'lib/model/budgeting/om/BaseUserLevelPeer.php';

			$this->aUserLevel = UserLevelPeer::retrieveByPK($this->level_id, $con);

			
		}
		return $this->aUserLevel;
	}

} 