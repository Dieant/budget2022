<?php


abstract class BaseMurniBukuBiruMasterKegiatan extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $unit_id;


	
	protected $kode_kegiatan;


	
	protected $kode_bidang;


	
	protected $kode_urusan_wajib;


	
	protected $kode_program;


	
	protected $kode_sasaran;


	
	protected $kode_indikator;


	
	protected $alokasi_dana;


	
	protected $nama_kegiatan;


	
	protected $masukan;


	
	protected $output;


	
	protected $outcome;


	
	protected $benefit;


	
	protected $impact;


	
	protected $tipe;


	
	protected $kegiatan_active = false;


	
	protected $to_kegiatan_code;


	
	protected $catatan;


	
	protected $target_outcome;


	
	protected $lokasi;


	
	protected $jumlah_prev;


	
	protected $jumlah_now;


	
	protected $jumlah_next;


	
	protected $kode_program2;


	
	protected $kode_urusan;


	
	protected $last_update_user;


	
	protected $last_update_time;


	
	protected $last_update_ip;


	
	protected $tahap;


	
	protected $kode_misi;


	
	protected $kode_tujuan;


	
	protected $ranking;


	
	protected $nomor13;


	
	protected $ppa_nama;


	
	protected $ppa_pangkat;


	
	protected $ppa_nip;


	
	protected $lanjutan;


	
	protected $user_id;


	
	protected $id;


	
	protected $tahun;


	
	protected $tambahan_pagu;


	
	protected $gender = false;


	
	protected $kode_keg_keuangan;


	
	protected $user_id_lama;


	
	protected $indikator;


	
	protected $is_dak;


	
	protected $kode_kegiatan_asal;


	
	protected $kode_keg_keuangan_asal;


	
	protected $th_ke_multiyears;


	
	protected $kelompok_sasaran;


	
	protected $pagu_bappeko;


	
	protected $kode_dpa;


	
	protected $user_id_pptk;


	
	protected $user_id_kpa;


	
	protected $catatan_pembahasan;


	
	protected $catatan_penyelia;


	
	protected $catatan_bappeko;


	
	protected $status_level;


	
	protected $is_tapd_setuju = false;


	
	protected $is_bappeko_setuju = false;


	
	protected $is_penyelia_setuju = false;


	
	protected $is_pernah_rka = false;


	
	protected $kode_kegiatan_baru;


	
	protected $verifikasi_bpkpd;


	
	protected $verifikasi_bappeko;


	
	protected $verifikasi_penyelia;


	
	protected $verifikasi_bagian_hukum;


	
	protected $verifikasi_inspektorat;


	
	protected $verifikasi_badan_kepegawaian;


	
	protected $verifikasi_lppa;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getUnitId()
	{

		return $this->unit_id;
	}

	
	public function getKodeKegiatan()
	{

		return $this->kode_kegiatan;
	}

	
	public function getKodeBidang()
	{

		return $this->kode_bidang;
	}

	
	public function getKodeUrusanWajib()
	{

		return $this->kode_urusan_wajib;
	}

	
	public function getKodeProgram()
	{

		return $this->kode_program;
	}

	
	public function getKodeSasaran()
	{

		return $this->kode_sasaran;
	}

	
	public function getKodeIndikator()
	{

		return $this->kode_indikator;
	}

	
	public function getAlokasiDana()
	{

		return $this->alokasi_dana;
	}

	
	public function getNamaKegiatan()
	{

		return $this->nama_kegiatan;
	}

	
	public function getMasukan()
	{

		return $this->masukan;
	}

	
	public function getOutput()
	{

		return $this->output;
	}

	
	public function getOutcome()
	{

		return $this->outcome;
	}

	
	public function getBenefit()
	{

		return $this->benefit;
	}

	
	public function getImpact()
	{

		return $this->impact;
	}

	
	public function getTipe()
	{

		return $this->tipe;
	}

	
	public function getKegiatanActive()
	{

		return $this->kegiatan_active;
	}

	
	public function getToKegiatanCode()
	{

		return $this->to_kegiatan_code;
	}

	
	public function getCatatan()
	{

		return $this->catatan;
	}

	
	public function getTargetOutcome()
	{

		return $this->target_outcome;
	}

	
	public function getLokasi()
	{

		return $this->lokasi;
	}

	
	public function getJumlahPrev()
	{

		return $this->jumlah_prev;
	}

	
	public function getJumlahNow()
	{

		return $this->jumlah_now;
	}

	
	public function getJumlahNext()
	{

		return $this->jumlah_next;
	}

	
	public function getKodeProgram2()
	{

		return $this->kode_program2;
	}

	
	public function getKodeUrusan()
	{

		return $this->kode_urusan;
	}

	
	public function getLastUpdateUser()
	{

		return $this->last_update_user;
	}

	
	public function getLastUpdateTime($format = 'Y-m-d H:i:s')
	{

		if ($this->last_update_time === null || $this->last_update_time === '') {
			return null;
		} elseif (!is_int($this->last_update_time)) {
						$ts = strtotime($this->last_update_time);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [last_update_time] as date/time value: " . var_export($this->last_update_time, true));
			}
		} else {
			$ts = $this->last_update_time;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getLastUpdateIp()
	{

		return $this->last_update_ip;
	}

	
	public function getTahap()
	{

		return $this->tahap;
	}

	
	public function getKodeMisi()
	{

		return $this->kode_misi;
	}

	
	public function getKodeTujuan()
	{

		return $this->kode_tujuan;
	}

	
	public function getRanking()
	{

		return $this->ranking;
	}

	
	public function getNomor13()
	{

		return $this->nomor13;
	}

	
	public function getPpaNama()
	{

		return $this->ppa_nama;
	}

	
	public function getPpaPangkat()
	{

		return $this->ppa_pangkat;
	}

	
	public function getPpaNip()
	{

		return $this->ppa_nip;
	}

	
	public function getLanjutan()
	{

		return $this->lanjutan;
	}

	
	public function getUserId()
	{

		return $this->user_id;
	}

	
	public function getId()
	{

		return $this->id;
	}

	
	public function getTahun()
	{

		return $this->tahun;
	}

	
	public function getTambahanPagu()
	{

		return $this->tambahan_pagu;
	}

	
	public function getGender()
	{

		return $this->gender;
	}

	
	public function getKodeKegKeuangan()
	{

		return $this->kode_keg_keuangan;
	}

	
	public function getUserIdLama()
	{

		return $this->user_id_lama;
	}

	
	public function getIndikator()
	{

		return $this->indikator;
	}

	
	public function getIsDak()
	{

		return $this->is_dak;
	}

	
	public function getKodeKegiatanAsal()
	{

		return $this->kode_kegiatan_asal;
	}

	
	public function getKodeKegKeuanganAsal()
	{

		return $this->kode_keg_keuangan_asal;
	}

	
	public function getThKeMultiyears()
	{

		return $this->th_ke_multiyears;
	}

	
	public function getKelompokSasaran()
	{

		return $this->kelompok_sasaran;
	}

	
	public function getPaguBappeko()
	{

		return $this->pagu_bappeko;
	}

	
	public function getKodeDpa()
	{

		return $this->kode_dpa;
	}

	
	public function getUserIdPptk()
	{

		return $this->user_id_pptk;
	}

	
	public function getUserIdKpa()
	{

		return $this->user_id_kpa;
	}

	
	public function getCatatanPembahasan()
	{

		return $this->catatan_pembahasan;
	}

	
	public function getCatatanPenyelia()
	{

		return $this->catatan_penyelia;
	}

	
	public function getCatatanBappeko()
	{

		return $this->catatan_bappeko;
	}

	
	public function getStatusLevel()
	{

		return $this->status_level;
	}

	
	public function getIsTapdSetuju()
	{

		return $this->is_tapd_setuju;
	}

	
	public function getIsBappekoSetuju()
	{

		return $this->is_bappeko_setuju;
	}

	
	public function getIsPenyeliaSetuju()
	{

		return $this->is_penyelia_setuju;
	}

	
	public function getIsPernahRka()
	{

		return $this->is_pernah_rka;
	}

	
	public function getKodeKegiatanBaru()
	{

		return $this->kode_kegiatan_baru;
	}

	
	public function getVerifikasiBpkpd()
	{

		return $this->verifikasi_bpkpd;
	}

	
	public function getVerifikasiBappeko()
	{

		return $this->verifikasi_bappeko;
	}

	
	public function getVerifikasiPenyelia()
	{

		return $this->verifikasi_penyelia;
	}

	
	public function getVerifikasiBagianHukum()
	{

		return $this->verifikasi_bagian_hukum;
	}

	
	public function getVerifikasiInspektorat()
	{

		return $this->verifikasi_inspektorat;
	}

	
	public function getVerifikasiBadanKepegawaian()
	{

		return $this->verifikasi_badan_kepegawaian;
	}

	
	public function getVerifikasiLppa()
	{

		return $this->verifikasi_lppa;
	}

	
	public function setUnitId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->unit_id !== $v) {
			$this->unit_id = $v;
			$this->modifiedColumns[] = MurniBukuBiruMasterKegiatanPeer::UNIT_ID;
		}

	} 
	
	public function setKodeKegiatan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_kegiatan !== $v) {
			$this->kode_kegiatan = $v;
			$this->modifiedColumns[] = MurniBukuBiruMasterKegiatanPeer::KODE_KEGIATAN;
		}

	} 
	
	public function setKodeBidang($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_bidang !== $v) {
			$this->kode_bidang = $v;
			$this->modifiedColumns[] = MurniBukuBiruMasterKegiatanPeer::KODE_BIDANG;
		}

	} 
	
	public function setKodeUrusanWajib($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_urusan_wajib !== $v) {
			$this->kode_urusan_wajib = $v;
			$this->modifiedColumns[] = MurniBukuBiruMasterKegiatanPeer::KODE_URUSAN_WAJIB;
		}

	} 
	
	public function setKodeProgram($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_program !== $v) {
			$this->kode_program = $v;
			$this->modifiedColumns[] = MurniBukuBiruMasterKegiatanPeer::KODE_PROGRAM;
		}

	} 
	
	public function setKodeSasaran($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_sasaran !== $v) {
			$this->kode_sasaran = $v;
			$this->modifiedColumns[] = MurniBukuBiruMasterKegiatanPeer::KODE_SASARAN;
		}

	} 
	
	public function setKodeIndikator($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_indikator !== $v) {
			$this->kode_indikator = $v;
			$this->modifiedColumns[] = MurniBukuBiruMasterKegiatanPeer::KODE_INDIKATOR;
		}

	} 
	
	public function setAlokasiDana($v)
	{

		if ($this->alokasi_dana !== $v) {
			$this->alokasi_dana = $v;
			$this->modifiedColumns[] = MurniBukuBiruMasterKegiatanPeer::ALOKASI_DANA;
		}

	} 
	
	public function setNamaKegiatan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nama_kegiatan !== $v) {
			$this->nama_kegiatan = $v;
			$this->modifiedColumns[] = MurniBukuBiruMasterKegiatanPeer::NAMA_KEGIATAN;
		}

	} 
	
	public function setMasukan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->masukan !== $v) {
			$this->masukan = $v;
			$this->modifiedColumns[] = MurniBukuBiruMasterKegiatanPeer::MASUKAN;
		}

	} 
	
	public function setOutput($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->output !== $v) {
			$this->output = $v;
			$this->modifiedColumns[] = MurniBukuBiruMasterKegiatanPeer::OUTPUT;
		}

	} 
	
	public function setOutcome($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->outcome !== $v) {
			$this->outcome = $v;
			$this->modifiedColumns[] = MurniBukuBiruMasterKegiatanPeer::OUTCOME;
		}

	} 
	
	public function setBenefit($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->benefit !== $v) {
			$this->benefit = $v;
			$this->modifiedColumns[] = MurniBukuBiruMasterKegiatanPeer::BENEFIT;
		}

	} 
	
	public function setImpact($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->impact !== $v) {
			$this->impact = $v;
			$this->modifiedColumns[] = MurniBukuBiruMasterKegiatanPeer::IMPACT;
		}

	} 
	
	public function setTipe($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->tipe !== $v) {
			$this->tipe = $v;
			$this->modifiedColumns[] = MurniBukuBiruMasterKegiatanPeer::TIPE;
		}

	} 
	
	public function setKegiatanActive($v)
	{

		if ($this->kegiatan_active !== $v || $v === false) {
			$this->kegiatan_active = $v;
			$this->modifiedColumns[] = MurniBukuBiruMasterKegiatanPeer::KEGIATAN_ACTIVE;
		}

	} 
	
	public function setToKegiatanCode($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->to_kegiatan_code !== $v) {
			$this->to_kegiatan_code = $v;
			$this->modifiedColumns[] = MurniBukuBiruMasterKegiatanPeer::TO_KEGIATAN_CODE;
		}

	} 
	
	public function setCatatan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->catatan !== $v) {
			$this->catatan = $v;
			$this->modifiedColumns[] = MurniBukuBiruMasterKegiatanPeer::CATATAN;
		}

	} 
	
	public function setTargetOutcome($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->target_outcome !== $v) {
			$this->target_outcome = $v;
			$this->modifiedColumns[] = MurniBukuBiruMasterKegiatanPeer::TARGET_OUTCOME;
		}

	} 
	
	public function setLokasi($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->lokasi !== $v) {
			$this->lokasi = $v;
			$this->modifiedColumns[] = MurniBukuBiruMasterKegiatanPeer::LOKASI;
		}

	} 
	
	public function setJumlahPrev($v)
	{

		if ($this->jumlah_prev !== $v) {
			$this->jumlah_prev = $v;
			$this->modifiedColumns[] = MurniBukuBiruMasterKegiatanPeer::JUMLAH_PREV;
		}

	} 
	
	public function setJumlahNow($v)
	{

		if ($this->jumlah_now !== $v) {
			$this->jumlah_now = $v;
			$this->modifiedColumns[] = MurniBukuBiruMasterKegiatanPeer::JUMLAH_NOW;
		}

	} 
	
	public function setJumlahNext($v)
	{

		if ($this->jumlah_next !== $v) {
			$this->jumlah_next = $v;
			$this->modifiedColumns[] = MurniBukuBiruMasterKegiatanPeer::JUMLAH_NEXT;
		}

	} 
	
	public function setKodeProgram2($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_program2 !== $v) {
			$this->kode_program2 = $v;
			$this->modifiedColumns[] = MurniBukuBiruMasterKegiatanPeer::KODE_PROGRAM2;
		}

	} 
	
	public function setKodeUrusan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_urusan !== $v) {
			$this->kode_urusan = $v;
			$this->modifiedColumns[] = MurniBukuBiruMasterKegiatanPeer::KODE_URUSAN;
		}

	} 
	
	public function setLastUpdateUser($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->last_update_user !== $v) {
			$this->last_update_user = $v;
			$this->modifiedColumns[] = MurniBukuBiruMasterKegiatanPeer::LAST_UPDATE_USER;
		}

	} 
	
	public function setLastUpdateTime($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [last_update_time] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->last_update_time !== $ts) {
			$this->last_update_time = $ts;
			$this->modifiedColumns[] = MurniBukuBiruMasterKegiatanPeer::LAST_UPDATE_TIME;
		}

	} 
	
	public function setLastUpdateIp($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->last_update_ip !== $v) {
			$this->last_update_ip = $v;
			$this->modifiedColumns[] = MurniBukuBiruMasterKegiatanPeer::LAST_UPDATE_IP;
		}

	} 
	
	public function setTahap($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->tahap !== $v) {
			$this->tahap = $v;
			$this->modifiedColumns[] = MurniBukuBiruMasterKegiatanPeer::TAHAP;
		}

	} 
	
	public function setKodeMisi($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_misi !== $v) {
			$this->kode_misi = $v;
			$this->modifiedColumns[] = MurniBukuBiruMasterKegiatanPeer::KODE_MISI;
		}

	} 
	
	public function setKodeTujuan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_tujuan !== $v) {
			$this->kode_tujuan = $v;
			$this->modifiedColumns[] = MurniBukuBiruMasterKegiatanPeer::KODE_TUJUAN;
		}

	} 
	
	public function setRanking($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->ranking !== $v) {
			$this->ranking = $v;
			$this->modifiedColumns[] = MurniBukuBiruMasterKegiatanPeer::RANKING;
		}

	} 
	
	public function setNomor13($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nomor13 !== $v) {
			$this->nomor13 = $v;
			$this->modifiedColumns[] = MurniBukuBiruMasterKegiatanPeer::NOMOR13;
		}

	} 
	
	public function setPpaNama($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->ppa_nama !== $v) {
			$this->ppa_nama = $v;
			$this->modifiedColumns[] = MurniBukuBiruMasterKegiatanPeer::PPA_NAMA;
		}

	} 
	
	public function setPpaPangkat($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->ppa_pangkat !== $v) {
			$this->ppa_pangkat = $v;
			$this->modifiedColumns[] = MurniBukuBiruMasterKegiatanPeer::PPA_PANGKAT;
		}

	} 
	
	public function setPpaNip($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->ppa_nip !== $v) {
			$this->ppa_nip = $v;
			$this->modifiedColumns[] = MurniBukuBiruMasterKegiatanPeer::PPA_NIP;
		}

	} 
	
	public function setLanjutan($v)
	{

		if ($this->lanjutan !== $v) {
			$this->lanjutan = $v;
			$this->modifiedColumns[] = MurniBukuBiruMasterKegiatanPeer::LANJUTAN;
		}

	} 
	
	public function setUserId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->user_id !== $v) {
			$this->user_id = $v;
			$this->modifiedColumns[] = MurniBukuBiruMasterKegiatanPeer::USER_ID;
		}

	} 
	
	public function setId($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = MurniBukuBiruMasterKegiatanPeer::ID;
		}

	} 
	
	public function setTahun($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->tahun !== $v) {
			$this->tahun = $v;
			$this->modifiedColumns[] = MurniBukuBiruMasterKegiatanPeer::TAHUN;
		}

	} 
	
	public function setTambahanPagu($v)
	{

		if ($this->tambahan_pagu !== $v) {
			$this->tambahan_pagu = $v;
			$this->modifiedColumns[] = MurniBukuBiruMasterKegiatanPeer::TAMBAHAN_PAGU;
		}

	} 
	
	public function setGender($v)
	{

		if ($this->gender !== $v || $v === false) {
			$this->gender = $v;
			$this->modifiedColumns[] = MurniBukuBiruMasterKegiatanPeer::GENDER;
		}

	} 
	
	public function setKodeKegKeuangan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_keg_keuangan !== $v) {
			$this->kode_keg_keuangan = $v;
			$this->modifiedColumns[] = MurniBukuBiruMasterKegiatanPeer::KODE_KEG_KEUANGAN;
		}

	} 
	
	public function setUserIdLama($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->user_id_lama !== $v) {
			$this->user_id_lama = $v;
			$this->modifiedColumns[] = MurniBukuBiruMasterKegiatanPeer::USER_ID_LAMA;
		}

	} 
	
	public function setIndikator($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->indikator !== $v) {
			$this->indikator = $v;
			$this->modifiedColumns[] = MurniBukuBiruMasterKegiatanPeer::INDIKATOR;
		}

	} 
	
	public function setIsDak($v)
	{

		if ($this->is_dak !== $v) {
			$this->is_dak = $v;
			$this->modifiedColumns[] = MurniBukuBiruMasterKegiatanPeer::IS_DAK;
		}

	} 
	
	public function setKodeKegiatanAsal($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_kegiatan_asal !== $v) {
			$this->kode_kegiatan_asal = $v;
			$this->modifiedColumns[] = MurniBukuBiruMasterKegiatanPeer::KODE_KEGIATAN_ASAL;
		}

	} 
	
	public function setKodeKegKeuanganAsal($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_keg_keuangan_asal !== $v) {
			$this->kode_keg_keuangan_asal = $v;
			$this->modifiedColumns[] = MurniBukuBiruMasterKegiatanPeer::KODE_KEG_KEUANGAN_ASAL;
		}

	} 
	
	public function setThKeMultiyears($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->th_ke_multiyears !== $v) {
			$this->th_ke_multiyears = $v;
			$this->modifiedColumns[] = MurniBukuBiruMasterKegiatanPeer::TH_KE_MULTIYEARS;
		}

	} 
	
	public function setKelompokSasaran($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kelompok_sasaran !== $v) {
			$this->kelompok_sasaran = $v;
			$this->modifiedColumns[] = MurniBukuBiruMasterKegiatanPeer::KELOMPOK_SASARAN;
		}

	} 
	
	public function setPaguBappeko($v)
	{

		if ($this->pagu_bappeko !== $v) {
			$this->pagu_bappeko = $v;
			$this->modifiedColumns[] = MurniBukuBiruMasterKegiatanPeer::PAGU_BAPPEKO;
		}

	} 
	
	public function setKodeDpa($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_dpa !== $v) {
			$this->kode_dpa = $v;
			$this->modifiedColumns[] = MurniBukuBiruMasterKegiatanPeer::KODE_DPA;
		}

	} 
	
	public function setUserIdPptk($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->user_id_pptk !== $v) {
			$this->user_id_pptk = $v;
			$this->modifiedColumns[] = MurniBukuBiruMasterKegiatanPeer::USER_ID_PPTK;
		}

	} 
	
	public function setUserIdKpa($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->user_id_kpa !== $v) {
			$this->user_id_kpa = $v;
			$this->modifiedColumns[] = MurniBukuBiruMasterKegiatanPeer::USER_ID_KPA;
		}

	} 
	
	public function setCatatanPembahasan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->catatan_pembahasan !== $v) {
			$this->catatan_pembahasan = $v;
			$this->modifiedColumns[] = MurniBukuBiruMasterKegiatanPeer::CATATAN_PEMBAHASAN;
		}

	} 
	
	public function setCatatanPenyelia($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->catatan_penyelia !== $v) {
			$this->catatan_penyelia = $v;
			$this->modifiedColumns[] = MurniBukuBiruMasterKegiatanPeer::CATATAN_PENYELIA;
		}

	} 
	
	public function setCatatanBappeko($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->catatan_bappeko !== $v) {
			$this->catatan_bappeko = $v;
			$this->modifiedColumns[] = MurniBukuBiruMasterKegiatanPeer::CATATAN_BAPPEKO;
		}

	} 
	
	public function setStatusLevel($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->status_level !== $v) {
			$this->status_level = $v;
			$this->modifiedColumns[] = MurniBukuBiruMasterKegiatanPeer::STATUS_LEVEL;
		}

	} 
	
	public function setIsTapdSetuju($v)
	{

		if ($this->is_tapd_setuju !== $v || $v === false) {
			$this->is_tapd_setuju = $v;
			$this->modifiedColumns[] = MurniBukuBiruMasterKegiatanPeer::IS_TAPD_SETUJU;
		}

	} 
	
	public function setIsBappekoSetuju($v)
	{

		if ($this->is_bappeko_setuju !== $v || $v === false) {
			$this->is_bappeko_setuju = $v;
			$this->modifiedColumns[] = MurniBukuBiruMasterKegiatanPeer::IS_BAPPEKO_SETUJU;
		}

	} 
	
	public function setIsPenyeliaSetuju($v)
	{

		if ($this->is_penyelia_setuju !== $v || $v === false) {
			$this->is_penyelia_setuju = $v;
			$this->modifiedColumns[] = MurniBukuBiruMasterKegiatanPeer::IS_PENYELIA_SETUJU;
		}

	} 
	
	public function setIsPernahRka($v)
	{

		if ($this->is_pernah_rka !== $v || $v === false) {
			$this->is_pernah_rka = $v;
			$this->modifiedColumns[] = MurniBukuBiruMasterKegiatanPeer::IS_PERNAH_RKA;
		}

	} 
	
	public function setKodeKegiatanBaru($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_kegiatan_baru !== $v) {
			$this->kode_kegiatan_baru = $v;
			$this->modifiedColumns[] = MurniBukuBiruMasterKegiatanPeer::KODE_KEGIATAN_BARU;
		}

	} 
	
	public function setVerifikasiBpkpd($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->verifikasi_bpkpd !== $v) {
			$this->verifikasi_bpkpd = $v;
			$this->modifiedColumns[] = MurniBukuBiruMasterKegiatanPeer::VERIFIKASI_BPKPD;
		}

	} 
	
	public function setVerifikasiBappeko($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->verifikasi_bappeko !== $v) {
			$this->verifikasi_bappeko = $v;
			$this->modifiedColumns[] = MurniBukuBiruMasterKegiatanPeer::VERIFIKASI_BAPPEKO;
		}

	} 
	
	public function setVerifikasiPenyelia($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->verifikasi_penyelia !== $v) {
			$this->verifikasi_penyelia = $v;
			$this->modifiedColumns[] = MurniBukuBiruMasterKegiatanPeer::VERIFIKASI_PENYELIA;
		}

	} 
	
	public function setVerifikasiBagianHukum($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->verifikasi_bagian_hukum !== $v) {
			$this->verifikasi_bagian_hukum = $v;
			$this->modifiedColumns[] = MurniBukuBiruMasterKegiatanPeer::VERIFIKASI_BAGIAN_HUKUM;
		}

	} 
	
	public function setVerifikasiInspektorat($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->verifikasi_inspektorat !== $v) {
			$this->verifikasi_inspektorat = $v;
			$this->modifiedColumns[] = MurniBukuBiruMasterKegiatanPeer::VERIFIKASI_INSPEKTORAT;
		}

	} 
	
	public function setVerifikasiBadanKepegawaian($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->verifikasi_badan_kepegawaian !== $v) {
			$this->verifikasi_badan_kepegawaian = $v;
			$this->modifiedColumns[] = MurniBukuBiruMasterKegiatanPeer::VERIFIKASI_BADAN_KEPEGAWAIAN;
		}

	} 
	
	public function setVerifikasiLppa($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->verifikasi_lppa !== $v) {
			$this->verifikasi_lppa = $v;
			$this->modifiedColumns[] = MurniBukuBiruMasterKegiatanPeer::VERIFIKASI_LPPA;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->unit_id = $rs->getString($startcol + 0);

			$this->kode_kegiatan = $rs->getString($startcol + 1);

			$this->kode_bidang = $rs->getString($startcol + 2);

			$this->kode_urusan_wajib = $rs->getString($startcol + 3);

			$this->kode_program = $rs->getString($startcol + 4);

			$this->kode_sasaran = $rs->getString($startcol + 5);

			$this->kode_indikator = $rs->getString($startcol + 6);

			$this->alokasi_dana = $rs->getFloat($startcol + 7);

			$this->nama_kegiatan = $rs->getString($startcol + 8);

			$this->masukan = $rs->getString($startcol + 9);

			$this->output = $rs->getString($startcol + 10);

			$this->outcome = $rs->getString($startcol + 11);

			$this->benefit = $rs->getString($startcol + 12);

			$this->impact = $rs->getString($startcol + 13);

			$this->tipe = $rs->getString($startcol + 14);

			$this->kegiatan_active = $rs->getBoolean($startcol + 15);

			$this->to_kegiatan_code = $rs->getString($startcol + 16);

			$this->catatan = $rs->getString($startcol + 17);

			$this->target_outcome = $rs->getString($startcol + 18);

			$this->lokasi = $rs->getString($startcol + 19);

			$this->jumlah_prev = $rs->getFloat($startcol + 20);

			$this->jumlah_now = $rs->getFloat($startcol + 21);

			$this->jumlah_next = $rs->getFloat($startcol + 22);

			$this->kode_program2 = $rs->getString($startcol + 23);

			$this->kode_urusan = $rs->getString($startcol + 24);

			$this->last_update_user = $rs->getString($startcol + 25);

			$this->last_update_time = $rs->getTimestamp($startcol + 26, null);

			$this->last_update_ip = $rs->getString($startcol + 27);

			$this->tahap = $rs->getString($startcol + 28);

			$this->kode_misi = $rs->getString($startcol + 29);

			$this->kode_tujuan = $rs->getString($startcol + 30);

			$this->ranking = $rs->getInt($startcol + 31);

			$this->nomor13 = $rs->getString($startcol + 32);

			$this->ppa_nama = $rs->getString($startcol + 33);

			$this->ppa_pangkat = $rs->getString($startcol + 34);

			$this->ppa_nip = $rs->getString($startcol + 35);

			$this->lanjutan = $rs->getBoolean($startcol + 36);

			$this->user_id = $rs->getString($startcol + 37);

			$this->id = $rs->getInt($startcol + 38);

			$this->tahun = $rs->getString($startcol + 39);

			$this->tambahan_pagu = $rs->getFloat($startcol + 40);

			$this->gender = $rs->getBoolean($startcol + 41);

			$this->kode_keg_keuangan = $rs->getString($startcol + 42);

			$this->user_id_lama = $rs->getString($startcol + 43);

			$this->indikator = $rs->getString($startcol + 44);

			$this->is_dak = $rs->getBoolean($startcol + 45);

			$this->kode_kegiatan_asal = $rs->getString($startcol + 46);

			$this->kode_keg_keuangan_asal = $rs->getString($startcol + 47);

			$this->th_ke_multiyears = $rs->getInt($startcol + 48);

			$this->kelompok_sasaran = $rs->getString($startcol + 49);

			$this->pagu_bappeko = $rs->getFloat($startcol + 50);

			$this->kode_dpa = $rs->getString($startcol + 51);

			$this->user_id_pptk = $rs->getString($startcol + 52);

			$this->user_id_kpa = $rs->getString($startcol + 53);

			$this->catatan_pembahasan = $rs->getString($startcol + 54);

			$this->catatan_penyelia = $rs->getString($startcol + 55);

			$this->catatan_bappeko = $rs->getString($startcol + 56);

			$this->status_level = $rs->getInt($startcol + 57);

			$this->is_tapd_setuju = $rs->getBoolean($startcol + 58);

			$this->is_bappeko_setuju = $rs->getBoolean($startcol + 59);

			$this->is_penyelia_setuju = $rs->getBoolean($startcol + 60);

			$this->is_pernah_rka = $rs->getBoolean($startcol + 61);

			$this->kode_kegiatan_baru = $rs->getString($startcol + 62);

			$this->verifikasi_bpkpd = $rs->getString($startcol + 63);

			$this->verifikasi_bappeko = $rs->getString($startcol + 64);

			$this->verifikasi_penyelia = $rs->getString($startcol + 65);

			$this->verifikasi_bagian_hukum = $rs->getString($startcol + 66);

			$this->verifikasi_inspektorat = $rs->getString($startcol + 67);

			$this->verifikasi_badan_kepegawaian = $rs->getString($startcol + 68);

			$this->verifikasi_lppa = $rs->getString($startcol + 69);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 70; 
		} catch (Exception $e) {
			throw new PropelException("Error populating MurniBukuBiruMasterKegiatan object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(MurniBukuBiruMasterKegiatanPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			MurniBukuBiruMasterKegiatanPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(MurniBukuBiruMasterKegiatanPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = MurniBukuBiruMasterKegiatanPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setId($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += MurniBukuBiruMasterKegiatanPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = MurniBukuBiruMasterKegiatanPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = MurniBukuBiruMasterKegiatanPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getUnitId();
				break;
			case 1:
				return $this->getKodeKegiatan();
				break;
			case 2:
				return $this->getKodeBidang();
				break;
			case 3:
				return $this->getKodeUrusanWajib();
				break;
			case 4:
				return $this->getKodeProgram();
				break;
			case 5:
				return $this->getKodeSasaran();
				break;
			case 6:
				return $this->getKodeIndikator();
				break;
			case 7:
				return $this->getAlokasiDana();
				break;
			case 8:
				return $this->getNamaKegiatan();
				break;
			case 9:
				return $this->getMasukan();
				break;
			case 10:
				return $this->getOutput();
				break;
			case 11:
				return $this->getOutcome();
				break;
			case 12:
				return $this->getBenefit();
				break;
			case 13:
				return $this->getImpact();
				break;
			case 14:
				return $this->getTipe();
				break;
			case 15:
				return $this->getKegiatanActive();
				break;
			case 16:
				return $this->getToKegiatanCode();
				break;
			case 17:
				return $this->getCatatan();
				break;
			case 18:
				return $this->getTargetOutcome();
				break;
			case 19:
				return $this->getLokasi();
				break;
			case 20:
				return $this->getJumlahPrev();
				break;
			case 21:
				return $this->getJumlahNow();
				break;
			case 22:
				return $this->getJumlahNext();
				break;
			case 23:
				return $this->getKodeProgram2();
				break;
			case 24:
				return $this->getKodeUrusan();
				break;
			case 25:
				return $this->getLastUpdateUser();
				break;
			case 26:
				return $this->getLastUpdateTime();
				break;
			case 27:
				return $this->getLastUpdateIp();
				break;
			case 28:
				return $this->getTahap();
				break;
			case 29:
				return $this->getKodeMisi();
				break;
			case 30:
				return $this->getKodeTujuan();
				break;
			case 31:
				return $this->getRanking();
				break;
			case 32:
				return $this->getNomor13();
				break;
			case 33:
				return $this->getPpaNama();
				break;
			case 34:
				return $this->getPpaPangkat();
				break;
			case 35:
				return $this->getPpaNip();
				break;
			case 36:
				return $this->getLanjutan();
				break;
			case 37:
				return $this->getUserId();
				break;
			case 38:
				return $this->getId();
				break;
			case 39:
				return $this->getTahun();
				break;
			case 40:
				return $this->getTambahanPagu();
				break;
			case 41:
				return $this->getGender();
				break;
			case 42:
				return $this->getKodeKegKeuangan();
				break;
			case 43:
				return $this->getUserIdLama();
				break;
			case 44:
				return $this->getIndikator();
				break;
			case 45:
				return $this->getIsDak();
				break;
			case 46:
				return $this->getKodeKegiatanAsal();
				break;
			case 47:
				return $this->getKodeKegKeuanganAsal();
				break;
			case 48:
				return $this->getThKeMultiyears();
				break;
			case 49:
				return $this->getKelompokSasaran();
				break;
			case 50:
				return $this->getPaguBappeko();
				break;
			case 51:
				return $this->getKodeDpa();
				break;
			case 52:
				return $this->getUserIdPptk();
				break;
			case 53:
				return $this->getUserIdKpa();
				break;
			case 54:
				return $this->getCatatanPembahasan();
				break;
			case 55:
				return $this->getCatatanPenyelia();
				break;
			case 56:
				return $this->getCatatanBappeko();
				break;
			case 57:
				return $this->getStatusLevel();
				break;
			case 58:
				return $this->getIsTapdSetuju();
				break;
			case 59:
				return $this->getIsBappekoSetuju();
				break;
			case 60:
				return $this->getIsPenyeliaSetuju();
				break;
			case 61:
				return $this->getIsPernahRka();
				break;
			case 62:
				return $this->getKodeKegiatanBaru();
				break;
			case 63:
				return $this->getVerifikasiBpkpd();
				break;
			case 64:
				return $this->getVerifikasiBappeko();
				break;
			case 65:
				return $this->getVerifikasiPenyelia();
				break;
			case 66:
				return $this->getVerifikasiBagianHukum();
				break;
			case 67:
				return $this->getVerifikasiInspektorat();
				break;
			case 68:
				return $this->getVerifikasiBadanKepegawaian();
				break;
			case 69:
				return $this->getVerifikasiLppa();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = MurniBukuBiruMasterKegiatanPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getUnitId(),
			$keys[1] => $this->getKodeKegiatan(),
			$keys[2] => $this->getKodeBidang(),
			$keys[3] => $this->getKodeUrusanWajib(),
			$keys[4] => $this->getKodeProgram(),
			$keys[5] => $this->getKodeSasaran(),
			$keys[6] => $this->getKodeIndikator(),
			$keys[7] => $this->getAlokasiDana(),
			$keys[8] => $this->getNamaKegiatan(),
			$keys[9] => $this->getMasukan(),
			$keys[10] => $this->getOutput(),
			$keys[11] => $this->getOutcome(),
			$keys[12] => $this->getBenefit(),
			$keys[13] => $this->getImpact(),
			$keys[14] => $this->getTipe(),
			$keys[15] => $this->getKegiatanActive(),
			$keys[16] => $this->getToKegiatanCode(),
			$keys[17] => $this->getCatatan(),
			$keys[18] => $this->getTargetOutcome(),
			$keys[19] => $this->getLokasi(),
			$keys[20] => $this->getJumlahPrev(),
			$keys[21] => $this->getJumlahNow(),
			$keys[22] => $this->getJumlahNext(),
			$keys[23] => $this->getKodeProgram2(),
			$keys[24] => $this->getKodeUrusan(),
			$keys[25] => $this->getLastUpdateUser(),
			$keys[26] => $this->getLastUpdateTime(),
			$keys[27] => $this->getLastUpdateIp(),
			$keys[28] => $this->getTahap(),
			$keys[29] => $this->getKodeMisi(),
			$keys[30] => $this->getKodeTujuan(),
			$keys[31] => $this->getRanking(),
			$keys[32] => $this->getNomor13(),
			$keys[33] => $this->getPpaNama(),
			$keys[34] => $this->getPpaPangkat(),
			$keys[35] => $this->getPpaNip(),
			$keys[36] => $this->getLanjutan(),
			$keys[37] => $this->getUserId(),
			$keys[38] => $this->getId(),
			$keys[39] => $this->getTahun(),
			$keys[40] => $this->getTambahanPagu(),
			$keys[41] => $this->getGender(),
			$keys[42] => $this->getKodeKegKeuangan(),
			$keys[43] => $this->getUserIdLama(),
			$keys[44] => $this->getIndikator(),
			$keys[45] => $this->getIsDak(),
			$keys[46] => $this->getKodeKegiatanAsal(),
			$keys[47] => $this->getKodeKegKeuanganAsal(),
			$keys[48] => $this->getThKeMultiyears(),
			$keys[49] => $this->getKelompokSasaran(),
			$keys[50] => $this->getPaguBappeko(),
			$keys[51] => $this->getKodeDpa(),
			$keys[52] => $this->getUserIdPptk(),
			$keys[53] => $this->getUserIdKpa(),
			$keys[54] => $this->getCatatanPembahasan(),
			$keys[55] => $this->getCatatanPenyelia(),
			$keys[56] => $this->getCatatanBappeko(),
			$keys[57] => $this->getStatusLevel(),
			$keys[58] => $this->getIsTapdSetuju(),
			$keys[59] => $this->getIsBappekoSetuju(),
			$keys[60] => $this->getIsPenyeliaSetuju(),
			$keys[61] => $this->getIsPernahRka(),
			$keys[62] => $this->getKodeKegiatanBaru(),
			$keys[63] => $this->getVerifikasiBpkpd(),
			$keys[64] => $this->getVerifikasiBappeko(),
			$keys[65] => $this->getVerifikasiPenyelia(),
			$keys[66] => $this->getVerifikasiBagianHukum(),
			$keys[67] => $this->getVerifikasiInspektorat(),
			$keys[68] => $this->getVerifikasiBadanKepegawaian(),
			$keys[69] => $this->getVerifikasiLppa(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = MurniBukuBiruMasterKegiatanPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setUnitId($value);
				break;
			case 1:
				$this->setKodeKegiatan($value);
				break;
			case 2:
				$this->setKodeBidang($value);
				break;
			case 3:
				$this->setKodeUrusanWajib($value);
				break;
			case 4:
				$this->setKodeProgram($value);
				break;
			case 5:
				$this->setKodeSasaran($value);
				break;
			case 6:
				$this->setKodeIndikator($value);
				break;
			case 7:
				$this->setAlokasiDana($value);
				break;
			case 8:
				$this->setNamaKegiatan($value);
				break;
			case 9:
				$this->setMasukan($value);
				break;
			case 10:
				$this->setOutput($value);
				break;
			case 11:
				$this->setOutcome($value);
				break;
			case 12:
				$this->setBenefit($value);
				break;
			case 13:
				$this->setImpact($value);
				break;
			case 14:
				$this->setTipe($value);
				break;
			case 15:
				$this->setKegiatanActive($value);
				break;
			case 16:
				$this->setToKegiatanCode($value);
				break;
			case 17:
				$this->setCatatan($value);
				break;
			case 18:
				$this->setTargetOutcome($value);
				break;
			case 19:
				$this->setLokasi($value);
				break;
			case 20:
				$this->setJumlahPrev($value);
				break;
			case 21:
				$this->setJumlahNow($value);
				break;
			case 22:
				$this->setJumlahNext($value);
				break;
			case 23:
				$this->setKodeProgram2($value);
				break;
			case 24:
				$this->setKodeUrusan($value);
				break;
			case 25:
				$this->setLastUpdateUser($value);
				break;
			case 26:
				$this->setLastUpdateTime($value);
				break;
			case 27:
				$this->setLastUpdateIp($value);
				break;
			case 28:
				$this->setTahap($value);
				break;
			case 29:
				$this->setKodeMisi($value);
				break;
			case 30:
				$this->setKodeTujuan($value);
				break;
			case 31:
				$this->setRanking($value);
				break;
			case 32:
				$this->setNomor13($value);
				break;
			case 33:
				$this->setPpaNama($value);
				break;
			case 34:
				$this->setPpaPangkat($value);
				break;
			case 35:
				$this->setPpaNip($value);
				break;
			case 36:
				$this->setLanjutan($value);
				break;
			case 37:
				$this->setUserId($value);
				break;
			case 38:
				$this->setId($value);
				break;
			case 39:
				$this->setTahun($value);
				break;
			case 40:
				$this->setTambahanPagu($value);
				break;
			case 41:
				$this->setGender($value);
				break;
			case 42:
				$this->setKodeKegKeuangan($value);
				break;
			case 43:
				$this->setUserIdLama($value);
				break;
			case 44:
				$this->setIndikator($value);
				break;
			case 45:
				$this->setIsDak($value);
				break;
			case 46:
				$this->setKodeKegiatanAsal($value);
				break;
			case 47:
				$this->setKodeKegKeuanganAsal($value);
				break;
			case 48:
				$this->setThKeMultiyears($value);
				break;
			case 49:
				$this->setKelompokSasaran($value);
				break;
			case 50:
				$this->setPaguBappeko($value);
				break;
			case 51:
				$this->setKodeDpa($value);
				break;
			case 52:
				$this->setUserIdPptk($value);
				break;
			case 53:
				$this->setUserIdKpa($value);
				break;
			case 54:
				$this->setCatatanPembahasan($value);
				break;
			case 55:
				$this->setCatatanPenyelia($value);
				break;
			case 56:
				$this->setCatatanBappeko($value);
				break;
			case 57:
				$this->setStatusLevel($value);
				break;
			case 58:
				$this->setIsTapdSetuju($value);
				break;
			case 59:
				$this->setIsBappekoSetuju($value);
				break;
			case 60:
				$this->setIsPenyeliaSetuju($value);
				break;
			case 61:
				$this->setIsPernahRka($value);
				break;
			case 62:
				$this->setKodeKegiatanBaru($value);
				break;
			case 63:
				$this->setVerifikasiBpkpd($value);
				break;
			case 64:
				$this->setVerifikasiBappeko($value);
				break;
			case 65:
				$this->setVerifikasiPenyelia($value);
				break;
			case 66:
				$this->setVerifikasiBagianHukum($value);
				break;
			case 67:
				$this->setVerifikasiInspektorat($value);
				break;
			case 68:
				$this->setVerifikasiBadanKepegawaian($value);
				break;
			case 69:
				$this->setVerifikasiLppa($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = MurniBukuBiruMasterKegiatanPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setUnitId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setKodeKegiatan($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setKodeBidang($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setKodeUrusanWajib($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setKodeProgram($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setKodeSasaran($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setKodeIndikator($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setAlokasiDana($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setNamaKegiatan($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setMasukan($arr[$keys[9]]);
		if (array_key_exists($keys[10], $arr)) $this->setOutput($arr[$keys[10]]);
		if (array_key_exists($keys[11], $arr)) $this->setOutcome($arr[$keys[11]]);
		if (array_key_exists($keys[12], $arr)) $this->setBenefit($arr[$keys[12]]);
		if (array_key_exists($keys[13], $arr)) $this->setImpact($arr[$keys[13]]);
		if (array_key_exists($keys[14], $arr)) $this->setTipe($arr[$keys[14]]);
		if (array_key_exists($keys[15], $arr)) $this->setKegiatanActive($arr[$keys[15]]);
		if (array_key_exists($keys[16], $arr)) $this->setToKegiatanCode($arr[$keys[16]]);
		if (array_key_exists($keys[17], $arr)) $this->setCatatan($arr[$keys[17]]);
		if (array_key_exists($keys[18], $arr)) $this->setTargetOutcome($arr[$keys[18]]);
		if (array_key_exists($keys[19], $arr)) $this->setLokasi($arr[$keys[19]]);
		if (array_key_exists($keys[20], $arr)) $this->setJumlahPrev($arr[$keys[20]]);
		if (array_key_exists($keys[21], $arr)) $this->setJumlahNow($arr[$keys[21]]);
		if (array_key_exists($keys[22], $arr)) $this->setJumlahNext($arr[$keys[22]]);
		if (array_key_exists($keys[23], $arr)) $this->setKodeProgram2($arr[$keys[23]]);
		if (array_key_exists($keys[24], $arr)) $this->setKodeUrusan($arr[$keys[24]]);
		if (array_key_exists($keys[25], $arr)) $this->setLastUpdateUser($arr[$keys[25]]);
		if (array_key_exists($keys[26], $arr)) $this->setLastUpdateTime($arr[$keys[26]]);
		if (array_key_exists($keys[27], $arr)) $this->setLastUpdateIp($arr[$keys[27]]);
		if (array_key_exists($keys[28], $arr)) $this->setTahap($arr[$keys[28]]);
		if (array_key_exists($keys[29], $arr)) $this->setKodeMisi($arr[$keys[29]]);
		if (array_key_exists($keys[30], $arr)) $this->setKodeTujuan($arr[$keys[30]]);
		if (array_key_exists($keys[31], $arr)) $this->setRanking($arr[$keys[31]]);
		if (array_key_exists($keys[32], $arr)) $this->setNomor13($arr[$keys[32]]);
		if (array_key_exists($keys[33], $arr)) $this->setPpaNama($arr[$keys[33]]);
		if (array_key_exists($keys[34], $arr)) $this->setPpaPangkat($arr[$keys[34]]);
		if (array_key_exists($keys[35], $arr)) $this->setPpaNip($arr[$keys[35]]);
		if (array_key_exists($keys[36], $arr)) $this->setLanjutan($arr[$keys[36]]);
		if (array_key_exists($keys[37], $arr)) $this->setUserId($arr[$keys[37]]);
		if (array_key_exists($keys[38], $arr)) $this->setId($arr[$keys[38]]);
		if (array_key_exists($keys[39], $arr)) $this->setTahun($arr[$keys[39]]);
		if (array_key_exists($keys[40], $arr)) $this->setTambahanPagu($arr[$keys[40]]);
		if (array_key_exists($keys[41], $arr)) $this->setGender($arr[$keys[41]]);
		if (array_key_exists($keys[42], $arr)) $this->setKodeKegKeuangan($arr[$keys[42]]);
		if (array_key_exists($keys[43], $arr)) $this->setUserIdLama($arr[$keys[43]]);
		if (array_key_exists($keys[44], $arr)) $this->setIndikator($arr[$keys[44]]);
		if (array_key_exists($keys[45], $arr)) $this->setIsDak($arr[$keys[45]]);
		if (array_key_exists($keys[46], $arr)) $this->setKodeKegiatanAsal($arr[$keys[46]]);
		if (array_key_exists($keys[47], $arr)) $this->setKodeKegKeuanganAsal($arr[$keys[47]]);
		if (array_key_exists($keys[48], $arr)) $this->setThKeMultiyears($arr[$keys[48]]);
		if (array_key_exists($keys[49], $arr)) $this->setKelompokSasaran($arr[$keys[49]]);
		if (array_key_exists($keys[50], $arr)) $this->setPaguBappeko($arr[$keys[50]]);
		if (array_key_exists($keys[51], $arr)) $this->setKodeDpa($arr[$keys[51]]);
		if (array_key_exists($keys[52], $arr)) $this->setUserIdPptk($arr[$keys[52]]);
		if (array_key_exists($keys[53], $arr)) $this->setUserIdKpa($arr[$keys[53]]);
		if (array_key_exists($keys[54], $arr)) $this->setCatatanPembahasan($arr[$keys[54]]);
		if (array_key_exists($keys[55], $arr)) $this->setCatatanPenyelia($arr[$keys[55]]);
		if (array_key_exists($keys[56], $arr)) $this->setCatatanBappeko($arr[$keys[56]]);
		if (array_key_exists($keys[57], $arr)) $this->setStatusLevel($arr[$keys[57]]);
		if (array_key_exists($keys[58], $arr)) $this->setIsTapdSetuju($arr[$keys[58]]);
		if (array_key_exists($keys[59], $arr)) $this->setIsBappekoSetuju($arr[$keys[59]]);
		if (array_key_exists($keys[60], $arr)) $this->setIsPenyeliaSetuju($arr[$keys[60]]);
		if (array_key_exists($keys[61], $arr)) $this->setIsPernahRka($arr[$keys[61]]);
		if (array_key_exists($keys[62], $arr)) $this->setKodeKegiatanBaru($arr[$keys[62]]);
		if (array_key_exists($keys[63], $arr)) $this->setVerifikasiBpkpd($arr[$keys[63]]);
		if (array_key_exists($keys[64], $arr)) $this->setVerifikasiBappeko($arr[$keys[64]]);
		if (array_key_exists($keys[65], $arr)) $this->setVerifikasiPenyelia($arr[$keys[65]]);
		if (array_key_exists($keys[66], $arr)) $this->setVerifikasiBagianHukum($arr[$keys[66]]);
		if (array_key_exists($keys[67], $arr)) $this->setVerifikasiInspektorat($arr[$keys[67]]);
		if (array_key_exists($keys[68], $arr)) $this->setVerifikasiBadanKepegawaian($arr[$keys[68]]);
		if (array_key_exists($keys[69], $arr)) $this->setVerifikasiLppa($arr[$keys[69]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(MurniBukuBiruMasterKegiatanPeer::DATABASE_NAME);

		if ($this->isColumnModified(MurniBukuBiruMasterKegiatanPeer::UNIT_ID)) $criteria->add(MurniBukuBiruMasterKegiatanPeer::UNIT_ID, $this->unit_id);
		if ($this->isColumnModified(MurniBukuBiruMasterKegiatanPeer::KODE_KEGIATAN)) $criteria->add(MurniBukuBiruMasterKegiatanPeer::KODE_KEGIATAN, $this->kode_kegiatan);
		if ($this->isColumnModified(MurniBukuBiruMasterKegiatanPeer::KODE_BIDANG)) $criteria->add(MurniBukuBiruMasterKegiatanPeer::KODE_BIDANG, $this->kode_bidang);
		if ($this->isColumnModified(MurniBukuBiruMasterKegiatanPeer::KODE_URUSAN_WAJIB)) $criteria->add(MurniBukuBiruMasterKegiatanPeer::KODE_URUSAN_WAJIB, $this->kode_urusan_wajib);
		if ($this->isColumnModified(MurniBukuBiruMasterKegiatanPeer::KODE_PROGRAM)) $criteria->add(MurniBukuBiruMasterKegiatanPeer::KODE_PROGRAM, $this->kode_program);
		if ($this->isColumnModified(MurniBukuBiruMasterKegiatanPeer::KODE_SASARAN)) $criteria->add(MurniBukuBiruMasterKegiatanPeer::KODE_SASARAN, $this->kode_sasaran);
		if ($this->isColumnModified(MurniBukuBiruMasterKegiatanPeer::KODE_INDIKATOR)) $criteria->add(MurniBukuBiruMasterKegiatanPeer::KODE_INDIKATOR, $this->kode_indikator);
		if ($this->isColumnModified(MurniBukuBiruMasterKegiatanPeer::ALOKASI_DANA)) $criteria->add(MurniBukuBiruMasterKegiatanPeer::ALOKASI_DANA, $this->alokasi_dana);
		if ($this->isColumnModified(MurniBukuBiruMasterKegiatanPeer::NAMA_KEGIATAN)) $criteria->add(MurniBukuBiruMasterKegiatanPeer::NAMA_KEGIATAN, $this->nama_kegiatan);
		if ($this->isColumnModified(MurniBukuBiruMasterKegiatanPeer::MASUKAN)) $criteria->add(MurniBukuBiruMasterKegiatanPeer::MASUKAN, $this->masukan);
		if ($this->isColumnModified(MurniBukuBiruMasterKegiatanPeer::OUTPUT)) $criteria->add(MurniBukuBiruMasterKegiatanPeer::OUTPUT, $this->output);
		if ($this->isColumnModified(MurniBukuBiruMasterKegiatanPeer::OUTCOME)) $criteria->add(MurniBukuBiruMasterKegiatanPeer::OUTCOME, $this->outcome);
		if ($this->isColumnModified(MurniBukuBiruMasterKegiatanPeer::BENEFIT)) $criteria->add(MurniBukuBiruMasterKegiatanPeer::BENEFIT, $this->benefit);
		if ($this->isColumnModified(MurniBukuBiruMasterKegiatanPeer::IMPACT)) $criteria->add(MurniBukuBiruMasterKegiatanPeer::IMPACT, $this->impact);
		if ($this->isColumnModified(MurniBukuBiruMasterKegiatanPeer::TIPE)) $criteria->add(MurniBukuBiruMasterKegiatanPeer::TIPE, $this->tipe);
		if ($this->isColumnModified(MurniBukuBiruMasterKegiatanPeer::KEGIATAN_ACTIVE)) $criteria->add(MurniBukuBiruMasterKegiatanPeer::KEGIATAN_ACTIVE, $this->kegiatan_active);
		if ($this->isColumnModified(MurniBukuBiruMasterKegiatanPeer::TO_KEGIATAN_CODE)) $criteria->add(MurniBukuBiruMasterKegiatanPeer::TO_KEGIATAN_CODE, $this->to_kegiatan_code);
		if ($this->isColumnModified(MurniBukuBiruMasterKegiatanPeer::CATATAN)) $criteria->add(MurniBukuBiruMasterKegiatanPeer::CATATAN, $this->catatan);
		if ($this->isColumnModified(MurniBukuBiruMasterKegiatanPeer::TARGET_OUTCOME)) $criteria->add(MurniBukuBiruMasterKegiatanPeer::TARGET_OUTCOME, $this->target_outcome);
		if ($this->isColumnModified(MurniBukuBiruMasterKegiatanPeer::LOKASI)) $criteria->add(MurniBukuBiruMasterKegiatanPeer::LOKASI, $this->lokasi);
		if ($this->isColumnModified(MurniBukuBiruMasterKegiatanPeer::JUMLAH_PREV)) $criteria->add(MurniBukuBiruMasterKegiatanPeer::JUMLAH_PREV, $this->jumlah_prev);
		if ($this->isColumnModified(MurniBukuBiruMasterKegiatanPeer::JUMLAH_NOW)) $criteria->add(MurniBukuBiruMasterKegiatanPeer::JUMLAH_NOW, $this->jumlah_now);
		if ($this->isColumnModified(MurniBukuBiruMasterKegiatanPeer::JUMLAH_NEXT)) $criteria->add(MurniBukuBiruMasterKegiatanPeer::JUMLAH_NEXT, $this->jumlah_next);
		if ($this->isColumnModified(MurniBukuBiruMasterKegiatanPeer::KODE_PROGRAM2)) $criteria->add(MurniBukuBiruMasterKegiatanPeer::KODE_PROGRAM2, $this->kode_program2);
		if ($this->isColumnModified(MurniBukuBiruMasterKegiatanPeer::KODE_URUSAN)) $criteria->add(MurniBukuBiruMasterKegiatanPeer::KODE_URUSAN, $this->kode_urusan);
		if ($this->isColumnModified(MurniBukuBiruMasterKegiatanPeer::LAST_UPDATE_USER)) $criteria->add(MurniBukuBiruMasterKegiatanPeer::LAST_UPDATE_USER, $this->last_update_user);
		if ($this->isColumnModified(MurniBukuBiruMasterKegiatanPeer::LAST_UPDATE_TIME)) $criteria->add(MurniBukuBiruMasterKegiatanPeer::LAST_UPDATE_TIME, $this->last_update_time);
		if ($this->isColumnModified(MurniBukuBiruMasterKegiatanPeer::LAST_UPDATE_IP)) $criteria->add(MurniBukuBiruMasterKegiatanPeer::LAST_UPDATE_IP, $this->last_update_ip);
		if ($this->isColumnModified(MurniBukuBiruMasterKegiatanPeer::TAHAP)) $criteria->add(MurniBukuBiruMasterKegiatanPeer::TAHAP, $this->tahap);
		if ($this->isColumnModified(MurniBukuBiruMasterKegiatanPeer::KODE_MISI)) $criteria->add(MurniBukuBiruMasterKegiatanPeer::KODE_MISI, $this->kode_misi);
		if ($this->isColumnModified(MurniBukuBiruMasterKegiatanPeer::KODE_TUJUAN)) $criteria->add(MurniBukuBiruMasterKegiatanPeer::KODE_TUJUAN, $this->kode_tujuan);
		if ($this->isColumnModified(MurniBukuBiruMasterKegiatanPeer::RANKING)) $criteria->add(MurniBukuBiruMasterKegiatanPeer::RANKING, $this->ranking);
		if ($this->isColumnModified(MurniBukuBiruMasterKegiatanPeer::NOMOR13)) $criteria->add(MurniBukuBiruMasterKegiatanPeer::NOMOR13, $this->nomor13);
		if ($this->isColumnModified(MurniBukuBiruMasterKegiatanPeer::PPA_NAMA)) $criteria->add(MurniBukuBiruMasterKegiatanPeer::PPA_NAMA, $this->ppa_nama);
		if ($this->isColumnModified(MurniBukuBiruMasterKegiatanPeer::PPA_PANGKAT)) $criteria->add(MurniBukuBiruMasterKegiatanPeer::PPA_PANGKAT, $this->ppa_pangkat);
		if ($this->isColumnModified(MurniBukuBiruMasterKegiatanPeer::PPA_NIP)) $criteria->add(MurniBukuBiruMasterKegiatanPeer::PPA_NIP, $this->ppa_nip);
		if ($this->isColumnModified(MurniBukuBiruMasterKegiatanPeer::LANJUTAN)) $criteria->add(MurniBukuBiruMasterKegiatanPeer::LANJUTAN, $this->lanjutan);
		if ($this->isColumnModified(MurniBukuBiruMasterKegiatanPeer::USER_ID)) $criteria->add(MurniBukuBiruMasterKegiatanPeer::USER_ID, $this->user_id);
		if ($this->isColumnModified(MurniBukuBiruMasterKegiatanPeer::ID)) $criteria->add(MurniBukuBiruMasterKegiatanPeer::ID, $this->id);
		if ($this->isColumnModified(MurniBukuBiruMasterKegiatanPeer::TAHUN)) $criteria->add(MurniBukuBiruMasterKegiatanPeer::TAHUN, $this->tahun);
		if ($this->isColumnModified(MurniBukuBiruMasterKegiatanPeer::TAMBAHAN_PAGU)) $criteria->add(MurniBukuBiruMasterKegiatanPeer::TAMBAHAN_PAGU, $this->tambahan_pagu);
		if ($this->isColumnModified(MurniBukuBiruMasterKegiatanPeer::GENDER)) $criteria->add(MurniBukuBiruMasterKegiatanPeer::GENDER, $this->gender);
		if ($this->isColumnModified(MurniBukuBiruMasterKegiatanPeer::KODE_KEG_KEUANGAN)) $criteria->add(MurniBukuBiruMasterKegiatanPeer::KODE_KEG_KEUANGAN, $this->kode_keg_keuangan);
		if ($this->isColumnModified(MurniBukuBiruMasterKegiatanPeer::USER_ID_LAMA)) $criteria->add(MurniBukuBiruMasterKegiatanPeer::USER_ID_LAMA, $this->user_id_lama);
		if ($this->isColumnModified(MurniBukuBiruMasterKegiatanPeer::INDIKATOR)) $criteria->add(MurniBukuBiruMasterKegiatanPeer::INDIKATOR, $this->indikator);
		if ($this->isColumnModified(MurniBukuBiruMasterKegiatanPeer::IS_DAK)) $criteria->add(MurniBukuBiruMasterKegiatanPeer::IS_DAK, $this->is_dak);
		if ($this->isColumnModified(MurniBukuBiruMasterKegiatanPeer::KODE_KEGIATAN_ASAL)) $criteria->add(MurniBukuBiruMasterKegiatanPeer::KODE_KEGIATAN_ASAL, $this->kode_kegiatan_asal);
		if ($this->isColumnModified(MurniBukuBiruMasterKegiatanPeer::KODE_KEG_KEUANGAN_ASAL)) $criteria->add(MurniBukuBiruMasterKegiatanPeer::KODE_KEG_KEUANGAN_ASAL, $this->kode_keg_keuangan_asal);
		if ($this->isColumnModified(MurniBukuBiruMasterKegiatanPeer::TH_KE_MULTIYEARS)) $criteria->add(MurniBukuBiruMasterKegiatanPeer::TH_KE_MULTIYEARS, $this->th_ke_multiyears);
		if ($this->isColumnModified(MurniBukuBiruMasterKegiatanPeer::KELOMPOK_SASARAN)) $criteria->add(MurniBukuBiruMasterKegiatanPeer::KELOMPOK_SASARAN, $this->kelompok_sasaran);
		if ($this->isColumnModified(MurniBukuBiruMasterKegiatanPeer::PAGU_BAPPEKO)) $criteria->add(MurniBukuBiruMasterKegiatanPeer::PAGU_BAPPEKO, $this->pagu_bappeko);
		if ($this->isColumnModified(MurniBukuBiruMasterKegiatanPeer::KODE_DPA)) $criteria->add(MurniBukuBiruMasterKegiatanPeer::KODE_DPA, $this->kode_dpa);
		if ($this->isColumnModified(MurniBukuBiruMasterKegiatanPeer::USER_ID_PPTK)) $criteria->add(MurniBukuBiruMasterKegiatanPeer::USER_ID_PPTK, $this->user_id_pptk);
		if ($this->isColumnModified(MurniBukuBiruMasterKegiatanPeer::USER_ID_KPA)) $criteria->add(MurniBukuBiruMasterKegiatanPeer::USER_ID_KPA, $this->user_id_kpa);
		if ($this->isColumnModified(MurniBukuBiruMasterKegiatanPeer::CATATAN_PEMBAHASAN)) $criteria->add(MurniBukuBiruMasterKegiatanPeer::CATATAN_PEMBAHASAN, $this->catatan_pembahasan);
		if ($this->isColumnModified(MurniBukuBiruMasterKegiatanPeer::CATATAN_PENYELIA)) $criteria->add(MurniBukuBiruMasterKegiatanPeer::CATATAN_PENYELIA, $this->catatan_penyelia);
		if ($this->isColumnModified(MurniBukuBiruMasterKegiatanPeer::CATATAN_BAPPEKO)) $criteria->add(MurniBukuBiruMasterKegiatanPeer::CATATAN_BAPPEKO, $this->catatan_bappeko);
		if ($this->isColumnModified(MurniBukuBiruMasterKegiatanPeer::STATUS_LEVEL)) $criteria->add(MurniBukuBiruMasterKegiatanPeer::STATUS_LEVEL, $this->status_level);
		if ($this->isColumnModified(MurniBukuBiruMasterKegiatanPeer::IS_TAPD_SETUJU)) $criteria->add(MurniBukuBiruMasterKegiatanPeer::IS_TAPD_SETUJU, $this->is_tapd_setuju);
		if ($this->isColumnModified(MurniBukuBiruMasterKegiatanPeer::IS_BAPPEKO_SETUJU)) $criteria->add(MurniBukuBiruMasterKegiatanPeer::IS_BAPPEKO_SETUJU, $this->is_bappeko_setuju);
		if ($this->isColumnModified(MurniBukuBiruMasterKegiatanPeer::IS_PENYELIA_SETUJU)) $criteria->add(MurniBukuBiruMasterKegiatanPeer::IS_PENYELIA_SETUJU, $this->is_penyelia_setuju);
		if ($this->isColumnModified(MurniBukuBiruMasterKegiatanPeer::IS_PERNAH_RKA)) $criteria->add(MurniBukuBiruMasterKegiatanPeer::IS_PERNAH_RKA, $this->is_pernah_rka);
		if ($this->isColumnModified(MurniBukuBiruMasterKegiatanPeer::KODE_KEGIATAN_BARU)) $criteria->add(MurniBukuBiruMasterKegiatanPeer::KODE_KEGIATAN_BARU, $this->kode_kegiatan_baru);
		if ($this->isColumnModified(MurniBukuBiruMasterKegiatanPeer::VERIFIKASI_BPKPD)) $criteria->add(MurniBukuBiruMasterKegiatanPeer::VERIFIKASI_BPKPD, $this->verifikasi_bpkpd);
		if ($this->isColumnModified(MurniBukuBiruMasterKegiatanPeer::VERIFIKASI_BAPPEKO)) $criteria->add(MurniBukuBiruMasterKegiatanPeer::VERIFIKASI_BAPPEKO, $this->verifikasi_bappeko);
		if ($this->isColumnModified(MurniBukuBiruMasterKegiatanPeer::VERIFIKASI_PENYELIA)) $criteria->add(MurniBukuBiruMasterKegiatanPeer::VERIFIKASI_PENYELIA, $this->verifikasi_penyelia);
		if ($this->isColumnModified(MurniBukuBiruMasterKegiatanPeer::VERIFIKASI_BAGIAN_HUKUM)) $criteria->add(MurniBukuBiruMasterKegiatanPeer::VERIFIKASI_BAGIAN_HUKUM, $this->verifikasi_bagian_hukum);
		if ($this->isColumnModified(MurniBukuBiruMasterKegiatanPeer::VERIFIKASI_INSPEKTORAT)) $criteria->add(MurniBukuBiruMasterKegiatanPeer::VERIFIKASI_INSPEKTORAT, $this->verifikasi_inspektorat);
		if ($this->isColumnModified(MurniBukuBiruMasterKegiatanPeer::VERIFIKASI_BADAN_KEPEGAWAIAN)) $criteria->add(MurniBukuBiruMasterKegiatanPeer::VERIFIKASI_BADAN_KEPEGAWAIAN, $this->verifikasi_badan_kepegawaian);
		if ($this->isColumnModified(MurniBukuBiruMasterKegiatanPeer::VERIFIKASI_LPPA)) $criteria->add(MurniBukuBiruMasterKegiatanPeer::VERIFIKASI_LPPA, $this->verifikasi_lppa);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(MurniBukuBiruMasterKegiatanPeer::DATABASE_NAME);

		$criteria->add(MurniBukuBiruMasterKegiatanPeer::UNIT_ID, $this->unit_id);
		$criteria->add(MurniBukuBiruMasterKegiatanPeer::KODE_KEGIATAN, $this->kode_kegiatan);
		$criteria->add(MurniBukuBiruMasterKegiatanPeer::ID, $this->id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		$pks = array();

		$pks[0] = $this->getUnitId();

		$pks[1] = $this->getKodeKegiatan();

		$pks[2] = $this->getId();

		return $pks;
	}

	
	public function setPrimaryKey($keys)
	{

		$this->setUnitId($keys[0]);

		$this->setKodeKegiatan($keys[1]);

		$this->setId($keys[2]);

	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setKodeBidang($this->kode_bidang);

		$copyObj->setKodeUrusanWajib($this->kode_urusan_wajib);

		$copyObj->setKodeProgram($this->kode_program);

		$copyObj->setKodeSasaran($this->kode_sasaran);

		$copyObj->setKodeIndikator($this->kode_indikator);

		$copyObj->setAlokasiDana($this->alokasi_dana);

		$copyObj->setNamaKegiatan($this->nama_kegiatan);

		$copyObj->setMasukan($this->masukan);

		$copyObj->setOutput($this->output);

		$copyObj->setOutcome($this->outcome);

		$copyObj->setBenefit($this->benefit);

		$copyObj->setImpact($this->impact);

		$copyObj->setTipe($this->tipe);

		$copyObj->setKegiatanActive($this->kegiatan_active);

		$copyObj->setToKegiatanCode($this->to_kegiatan_code);

		$copyObj->setCatatan($this->catatan);

		$copyObj->setTargetOutcome($this->target_outcome);

		$copyObj->setLokasi($this->lokasi);

		$copyObj->setJumlahPrev($this->jumlah_prev);

		$copyObj->setJumlahNow($this->jumlah_now);

		$copyObj->setJumlahNext($this->jumlah_next);

		$copyObj->setKodeProgram2($this->kode_program2);

		$copyObj->setKodeUrusan($this->kode_urusan);

		$copyObj->setLastUpdateUser($this->last_update_user);

		$copyObj->setLastUpdateTime($this->last_update_time);

		$copyObj->setLastUpdateIp($this->last_update_ip);

		$copyObj->setTahap($this->tahap);

		$copyObj->setKodeMisi($this->kode_misi);

		$copyObj->setKodeTujuan($this->kode_tujuan);

		$copyObj->setRanking($this->ranking);

		$copyObj->setNomor13($this->nomor13);

		$copyObj->setPpaNama($this->ppa_nama);

		$copyObj->setPpaPangkat($this->ppa_pangkat);

		$copyObj->setPpaNip($this->ppa_nip);

		$copyObj->setLanjutan($this->lanjutan);

		$copyObj->setUserId($this->user_id);

		$copyObj->setTahun($this->tahun);

		$copyObj->setTambahanPagu($this->tambahan_pagu);

		$copyObj->setGender($this->gender);

		$copyObj->setKodeKegKeuangan($this->kode_keg_keuangan);

		$copyObj->setUserIdLama($this->user_id_lama);

		$copyObj->setIndikator($this->indikator);

		$copyObj->setIsDak($this->is_dak);

		$copyObj->setKodeKegiatanAsal($this->kode_kegiatan_asal);

		$copyObj->setKodeKegKeuanganAsal($this->kode_keg_keuangan_asal);

		$copyObj->setThKeMultiyears($this->th_ke_multiyears);

		$copyObj->setKelompokSasaran($this->kelompok_sasaran);

		$copyObj->setPaguBappeko($this->pagu_bappeko);

		$copyObj->setKodeDpa($this->kode_dpa);

		$copyObj->setUserIdPptk($this->user_id_pptk);

		$copyObj->setUserIdKpa($this->user_id_kpa);

		$copyObj->setCatatanPembahasan($this->catatan_pembahasan);

		$copyObj->setCatatanPenyelia($this->catatan_penyelia);

		$copyObj->setCatatanBappeko($this->catatan_bappeko);

		$copyObj->setStatusLevel($this->status_level);

		$copyObj->setIsTapdSetuju($this->is_tapd_setuju);

		$copyObj->setIsBappekoSetuju($this->is_bappeko_setuju);

		$copyObj->setIsPenyeliaSetuju($this->is_penyelia_setuju);

		$copyObj->setIsPernahRka($this->is_pernah_rka);

		$copyObj->setKodeKegiatanBaru($this->kode_kegiatan_baru);

		$copyObj->setVerifikasiBpkpd($this->verifikasi_bpkpd);

		$copyObj->setVerifikasiBappeko($this->verifikasi_bappeko);

		$copyObj->setVerifikasiPenyelia($this->verifikasi_penyelia);

		$copyObj->setVerifikasiBagianHukum($this->verifikasi_bagian_hukum);

		$copyObj->setVerifikasiInspektorat($this->verifikasi_inspektorat);

		$copyObj->setVerifikasiBadanKepegawaian($this->verifikasi_badan_kepegawaian);

		$copyObj->setVerifikasiLppa($this->verifikasi_lppa);


		$copyObj->setNew(true);

		$copyObj->setUnitId(NULL); 
		$copyObj->setKodeKegiatan(NULL); 
		$copyObj->setId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new MurniBukuBiruMasterKegiatanPeer();
		}
		return self::$peer;
	}

} 