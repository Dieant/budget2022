<?php


abstract class BaseKomponenGabungPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.komponen_gabung';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.KomponenGabung';

	
	const NUM_COLUMNS = 15;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const KEGIATAN_CODE = 'ebudget.komponen_gabung.KEGIATAN_CODE';

	
	const DETAIL_NO = 'ebudget.komponen_gabung.DETAIL_NO';

	
	const REKENING_CODE = 'ebudget.komponen_gabung.REKENING_CODE';

	
	const KOMPONEN_ID = 'ebudget.komponen_gabung.KOMPONEN_ID';

	
	const DETAIL_NAME = 'ebudget.komponen_gabung.DETAIL_NAME';

	
	const VOLUME = 'ebudget.komponen_gabung.VOLUME';

	
	const KETERANGAN_KOEFISIEN = 'ebudget.komponen_gabung.KETERANGAN_KOEFISIEN';

	
	const SUBTITLE = 'ebudget.komponen_gabung.SUBTITLE';

	
	const KOMPONEN_HARGA = 'ebudget.komponen_gabung.KOMPONEN_HARGA';

	
	const KOMPONEN_HARGA_AWAL = 'ebudget.komponen_gabung.KOMPONEN_HARGA_AWAL';

	
	const KOMPONEN_NAME = 'ebudget.komponen_gabung.KOMPONEN_NAME';

	
	const SATUAN = 'ebudget.komponen_gabung.SATUAN';

	
	const PAJAK = 'ebudget.komponen_gabung.PAJAK';

	
	const UNIT_ID = 'ebudget.komponen_gabung.UNIT_ID';

	
	const FOR_DETAIL_NO = 'ebudget.komponen_gabung.FOR_DETAIL_NO';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('KegiatanCode', 'DetailNo', 'RekeningCode', 'KomponenId', 'DetailName', 'Volume', 'KeteranganKoefisien', 'Subtitle', 'KomponenHarga', 'KomponenHargaAwal', 'KomponenName', 'Satuan', 'Pajak', 'UnitId', 'ForDetailNo', ),
		BasePeer::TYPE_COLNAME => array (KomponenGabungPeer::KEGIATAN_CODE, KomponenGabungPeer::DETAIL_NO, KomponenGabungPeer::REKENING_CODE, KomponenGabungPeer::KOMPONEN_ID, KomponenGabungPeer::DETAIL_NAME, KomponenGabungPeer::VOLUME, KomponenGabungPeer::KETERANGAN_KOEFISIEN, KomponenGabungPeer::SUBTITLE, KomponenGabungPeer::KOMPONEN_HARGA, KomponenGabungPeer::KOMPONEN_HARGA_AWAL, KomponenGabungPeer::KOMPONEN_NAME, KomponenGabungPeer::SATUAN, KomponenGabungPeer::PAJAK, KomponenGabungPeer::UNIT_ID, KomponenGabungPeer::FOR_DETAIL_NO, ),
		BasePeer::TYPE_FIELDNAME => array ('kegiatan_code', 'detail_no', 'rekening_code', 'komponen_id', 'detail_name', 'volume', 'keterangan_koefisien', 'subtitle', 'komponen_harga', 'komponen_harga_awal', 'komponen_name', 'satuan', 'pajak', 'unit_id', 'for_detail_no', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('KegiatanCode' => 0, 'DetailNo' => 1, 'RekeningCode' => 2, 'KomponenId' => 3, 'DetailName' => 4, 'Volume' => 5, 'KeteranganKoefisien' => 6, 'Subtitle' => 7, 'KomponenHarga' => 8, 'KomponenHargaAwal' => 9, 'KomponenName' => 10, 'Satuan' => 11, 'Pajak' => 12, 'UnitId' => 13, 'ForDetailNo' => 14, ),
		BasePeer::TYPE_COLNAME => array (KomponenGabungPeer::KEGIATAN_CODE => 0, KomponenGabungPeer::DETAIL_NO => 1, KomponenGabungPeer::REKENING_CODE => 2, KomponenGabungPeer::KOMPONEN_ID => 3, KomponenGabungPeer::DETAIL_NAME => 4, KomponenGabungPeer::VOLUME => 5, KomponenGabungPeer::KETERANGAN_KOEFISIEN => 6, KomponenGabungPeer::SUBTITLE => 7, KomponenGabungPeer::KOMPONEN_HARGA => 8, KomponenGabungPeer::KOMPONEN_HARGA_AWAL => 9, KomponenGabungPeer::KOMPONEN_NAME => 10, KomponenGabungPeer::SATUAN => 11, KomponenGabungPeer::PAJAK => 12, KomponenGabungPeer::UNIT_ID => 13, KomponenGabungPeer::FOR_DETAIL_NO => 14, ),
		BasePeer::TYPE_FIELDNAME => array ('kegiatan_code' => 0, 'detail_no' => 1, 'rekening_code' => 2, 'komponen_id' => 3, 'detail_name' => 4, 'volume' => 5, 'keterangan_koefisien' => 6, 'subtitle' => 7, 'komponen_harga' => 8, 'komponen_harga_awal' => 9, 'komponen_name' => 10, 'satuan' => 11, 'pajak' => 12, 'unit_id' => 13, 'for_detail_no' => 14, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/KomponenGabungMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.KomponenGabungMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = KomponenGabungPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(KomponenGabungPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(KomponenGabungPeer::KEGIATAN_CODE);

		$criteria->addSelectColumn(KomponenGabungPeer::DETAIL_NO);

		$criteria->addSelectColumn(KomponenGabungPeer::REKENING_CODE);

		$criteria->addSelectColumn(KomponenGabungPeer::KOMPONEN_ID);

		$criteria->addSelectColumn(KomponenGabungPeer::DETAIL_NAME);

		$criteria->addSelectColumn(KomponenGabungPeer::VOLUME);

		$criteria->addSelectColumn(KomponenGabungPeer::KETERANGAN_KOEFISIEN);

		$criteria->addSelectColumn(KomponenGabungPeer::SUBTITLE);

		$criteria->addSelectColumn(KomponenGabungPeer::KOMPONEN_HARGA);

		$criteria->addSelectColumn(KomponenGabungPeer::KOMPONEN_HARGA_AWAL);

		$criteria->addSelectColumn(KomponenGabungPeer::KOMPONEN_NAME);

		$criteria->addSelectColumn(KomponenGabungPeer::SATUAN);

		$criteria->addSelectColumn(KomponenGabungPeer::PAJAK);

		$criteria->addSelectColumn(KomponenGabungPeer::UNIT_ID);

		$criteria->addSelectColumn(KomponenGabungPeer::FOR_DETAIL_NO);

	}

	const COUNT = 'COUNT(ebudget.komponen_gabung.KEGIATAN_CODE)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.komponen_gabung.KEGIATAN_CODE)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(KomponenGabungPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(KomponenGabungPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = KomponenGabungPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = KomponenGabungPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return KomponenGabungPeer::populateObjects(KomponenGabungPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			KomponenGabungPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = KomponenGabungPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return KomponenGabungPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(KomponenGabungPeer::KEGIATAN_CODE);
			$selectCriteria->add(KomponenGabungPeer::KEGIATAN_CODE, $criteria->remove(KomponenGabungPeer::KEGIATAN_CODE), $comparison);

			$comparison = $criteria->getComparison(KomponenGabungPeer::DETAIL_NO);
			$selectCriteria->add(KomponenGabungPeer::DETAIL_NO, $criteria->remove(KomponenGabungPeer::DETAIL_NO), $comparison);

			$comparison = $criteria->getComparison(KomponenGabungPeer::UNIT_ID);
			$selectCriteria->add(KomponenGabungPeer::UNIT_ID, $criteria->remove(KomponenGabungPeer::UNIT_ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(KomponenGabungPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(KomponenGabungPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof KomponenGabung) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
												if(count($values) == count($values, COUNT_RECURSIVE))
			{
								$values = array($values);
			}
			$vals = array();
			foreach($values as $value)
			{

				$vals[0][] = $value[0];
				$vals[1][] = $value[1];
				$vals[2][] = $value[2];
			}

			$criteria->add(KomponenGabungPeer::KEGIATAN_CODE, $vals[0], Criteria::IN);
			$criteria->add(KomponenGabungPeer::DETAIL_NO, $vals[1], Criteria::IN);
			$criteria->add(KomponenGabungPeer::UNIT_ID, $vals[2], Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(KomponenGabung $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(KomponenGabungPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(KomponenGabungPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(KomponenGabungPeer::DATABASE_NAME, KomponenGabungPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = KomponenGabungPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK( $kegiatan_code, $detail_no, $unit_id, $con = null) {
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$criteria = new Criteria();
		$criteria->add(KomponenGabungPeer::KEGIATAN_CODE, $kegiatan_code);
		$criteria->add(KomponenGabungPeer::DETAIL_NO, $detail_no);
		$criteria->add(KomponenGabungPeer::UNIT_ID, $unit_id);
		$v = KomponenGabungPeer::doSelect($criteria, $con);

		return !empty($v) ? $v[0] : null;
	}
} 
if (Propel::isInit()) {
			try {
		BaseKomponenGabungPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/KomponenGabungMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.KomponenGabungMapBuilder');
}
