<?php


abstract class BaseSubtitleIndikatorPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.subtitle_indikator';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.SubtitleIndikator';

	
	const NUM_COLUMNS = 27;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const UNIT_ID = 'ebudget.subtitle_indikator.UNIT_ID';

	
	const KEGIATAN_CODE = 'ebudget.subtitle_indikator.KEGIATAN_CODE';

	
	const SUBTITLE = 'ebudget.subtitle_indikator.SUBTITLE';

	
	const INDIKATOR = 'ebudget.subtitle_indikator.INDIKATOR';

	
	const NILAI = 'ebudget.subtitle_indikator.NILAI';

	
	const SATUAN = 'ebudget.subtitle_indikator.SATUAN';

	
	const LAST_UPDATE_USER = 'ebudget.subtitle_indikator.LAST_UPDATE_USER';

	
	const LAST_UPDATE_TIME = 'ebudget.subtitle_indikator.LAST_UPDATE_TIME';

	
	const LAST_UPDATE_IP = 'ebudget.subtitle_indikator.LAST_UPDATE_IP';

	
	const TAHAP = 'ebudget.subtitle_indikator.TAHAP';

	
	const SUB_ID = 'ebudget.subtitle_indikator.SUB_ID';

	
	const TAHUN = 'ebudget.subtitle_indikator.TAHUN';

	
	const LOCK_SUBTITLE = 'ebudget.subtitle_indikator.LOCK_SUBTITLE';

	
	const PRIORITAS = 'ebudget.subtitle_indikator.PRIORITAS';

	
	const CATATAN = 'ebudget.subtitle_indikator.CATATAN';

	
	const LAKILAKI = 'ebudget.subtitle_indikator.LAKILAKI';

	
	const PEREMPUAN = 'ebudget.subtitle_indikator.PEREMPUAN';

	
	const DEWASA = 'ebudget.subtitle_indikator.DEWASA';

	
	const ANAK = 'ebudget.subtitle_indikator.ANAK';

	
	const LANSIA = 'ebudget.subtitle_indikator.LANSIA';

	
	const INKLUSI = 'ebudget.subtitle_indikator.INKLUSI';

	
	const GENDER = 'ebudget.subtitle_indikator.GENDER';

	
	const ALOKASI_DANA = 'ebudget.subtitle_indikator.ALOKASI_DANA';

	
	const IS_NOL_ANGGARAN = 'ebudget.subtitle_indikator.IS_NOL_ANGGARAN';

	
	const INDIKATOR_CM = 'ebudget.subtitle_indikator.INDIKATOR_CM';

	
	const NILAI_CM = 'ebudget.subtitle_indikator.NILAI_CM';

	
	const SATUAN_CM = 'ebudget.subtitle_indikator.SATUAN_CM';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('UnitId', 'KegiatanCode', 'Subtitle', 'Indikator', 'Nilai', 'Satuan', 'LastUpdateUser', 'LastUpdateTime', 'LastUpdateIp', 'Tahap', 'SubId', 'Tahun', 'LockSubtitle', 'Prioritas', 'Catatan', 'Lakilaki', 'Perempuan', 'Dewasa', 'Anak', 'Lansia', 'Inklusi', 'Gender', 'AlokasiDana', 'IsNolAnggaran', 'IndikatorCm', 'NilaiCm', 'SatuanCm', ),
		BasePeer::TYPE_COLNAME => array (SubtitleIndikatorPeer::UNIT_ID, SubtitleIndikatorPeer::KEGIATAN_CODE, SubtitleIndikatorPeer::SUBTITLE, SubtitleIndikatorPeer::INDIKATOR, SubtitleIndikatorPeer::NILAI, SubtitleIndikatorPeer::SATUAN, SubtitleIndikatorPeer::LAST_UPDATE_USER, SubtitleIndikatorPeer::LAST_UPDATE_TIME, SubtitleIndikatorPeer::LAST_UPDATE_IP, SubtitleIndikatorPeer::TAHAP, SubtitleIndikatorPeer::SUB_ID, SubtitleIndikatorPeer::TAHUN, SubtitleIndikatorPeer::LOCK_SUBTITLE, SubtitleIndikatorPeer::PRIORITAS, SubtitleIndikatorPeer::CATATAN, SubtitleIndikatorPeer::LAKILAKI, SubtitleIndikatorPeer::PEREMPUAN, SubtitleIndikatorPeer::DEWASA, SubtitleIndikatorPeer::ANAK, SubtitleIndikatorPeer::LANSIA, SubtitleIndikatorPeer::INKLUSI, SubtitleIndikatorPeer::GENDER, SubtitleIndikatorPeer::ALOKASI_DANA, SubtitleIndikatorPeer::IS_NOL_ANGGARAN, SubtitleIndikatorPeer::INDIKATOR_CM, SubtitleIndikatorPeer::NILAI_CM, SubtitleIndikatorPeer::SATUAN_CM, ),
		BasePeer::TYPE_FIELDNAME => array ('unit_id', 'kegiatan_code', 'subtitle', 'indikator', 'nilai', 'satuan', 'last_update_user', 'last_update_time', 'last_update_ip', 'tahap', 'sub_id', 'tahun', 'lock_subtitle', 'prioritas', 'catatan', 'lakilaki', 'perempuan', 'dewasa', 'anak', 'lansia', 'inklusi', 'gender', 'alokasi_dana', 'is_nol_anggaran', 'indikator_cm', 'nilai_cm', 'satuan_cm', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('UnitId' => 0, 'KegiatanCode' => 1, 'Subtitle' => 2, 'Indikator' => 3, 'Nilai' => 4, 'Satuan' => 5, 'LastUpdateUser' => 6, 'LastUpdateTime' => 7, 'LastUpdateIp' => 8, 'Tahap' => 9, 'SubId' => 10, 'Tahun' => 11, 'LockSubtitle' => 12, 'Prioritas' => 13, 'Catatan' => 14, 'Lakilaki' => 15, 'Perempuan' => 16, 'Dewasa' => 17, 'Anak' => 18, 'Lansia' => 19, 'Inklusi' => 20, 'Gender' => 21, 'AlokasiDana' => 22, 'IsNolAnggaran' => 23, 'IndikatorCm' => 24, 'NilaiCm' => 25, 'SatuanCm' => 26, ),
		BasePeer::TYPE_COLNAME => array (SubtitleIndikatorPeer::UNIT_ID => 0, SubtitleIndikatorPeer::KEGIATAN_CODE => 1, SubtitleIndikatorPeer::SUBTITLE => 2, SubtitleIndikatorPeer::INDIKATOR => 3, SubtitleIndikatorPeer::NILAI => 4, SubtitleIndikatorPeer::SATUAN => 5, SubtitleIndikatorPeer::LAST_UPDATE_USER => 6, SubtitleIndikatorPeer::LAST_UPDATE_TIME => 7, SubtitleIndikatorPeer::LAST_UPDATE_IP => 8, SubtitleIndikatorPeer::TAHAP => 9, SubtitleIndikatorPeer::SUB_ID => 10, SubtitleIndikatorPeer::TAHUN => 11, SubtitleIndikatorPeer::LOCK_SUBTITLE => 12, SubtitleIndikatorPeer::PRIORITAS => 13, SubtitleIndikatorPeer::CATATAN => 14, SubtitleIndikatorPeer::LAKILAKI => 15, SubtitleIndikatorPeer::PEREMPUAN => 16, SubtitleIndikatorPeer::DEWASA => 17, SubtitleIndikatorPeer::ANAK => 18, SubtitleIndikatorPeer::LANSIA => 19, SubtitleIndikatorPeer::INKLUSI => 20, SubtitleIndikatorPeer::GENDER => 21, SubtitleIndikatorPeer::ALOKASI_DANA => 22, SubtitleIndikatorPeer::IS_NOL_ANGGARAN => 23, SubtitleIndikatorPeer::INDIKATOR_CM => 24, SubtitleIndikatorPeer::NILAI_CM => 25, SubtitleIndikatorPeer::SATUAN_CM => 26, ),
		BasePeer::TYPE_FIELDNAME => array ('unit_id' => 0, 'kegiatan_code' => 1, 'subtitle' => 2, 'indikator' => 3, 'nilai' => 4, 'satuan' => 5, 'last_update_user' => 6, 'last_update_time' => 7, 'last_update_ip' => 8, 'tahap' => 9, 'sub_id' => 10, 'tahun' => 11, 'lock_subtitle' => 12, 'prioritas' => 13, 'catatan' => 14, 'lakilaki' => 15, 'perempuan' => 16, 'dewasa' => 17, 'anak' => 18, 'lansia' => 19, 'inklusi' => 20, 'gender' => 21, 'alokasi_dana' => 22, 'is_nol_anggaran' => 23, 'indikator_cm' => 24, 'nilai_cm' => 25, 'satuan_cm' => 26, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/SubtitleIndikatorMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.SubtitleIndikatorMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = SubtitleIndikatorPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(SubtitleIndikatorPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(SubtitleIndikatorPeer::UNIT_ID);

		$criteria->addSelectColumn(SubtitleIndikatorPeer::KEGIATAN_CODE);

		$criteria->addSelectColumn(SubtitleIndikatorPeer::SUBTITLE);

		$criteria->addSelectColumn(SubtitleIndikatorPeer::INDIKATOR);

		$criteria->addSelectColumn(SubtitleIndikatorPeer::NILAI);

		$criteria->addSelectColumn(SubtitleIndikatorPeer::SATUAN);

		$criteria->addSelectColumn(SubtitleIndikatorPeer::LAST_UPDATE_USER);

		$criteria->addSelectColumn(SubtitleIndikatorPeer::LAST_UPDATE_TIME);

		$criteria->addSelectColumn(SubtitleIndikatorPeer::LAST_UPDATE_IP);

		$criteria->addSelectColumn(SubtitleIndikatorPeer::TAHAP);

		$criteria->addSelectColumn(SubtitleIndikatorPeer::SUB_ID);

		$criteria->addSelectColumn(SubtitleIndikatorPeer::TAHUN);

		$criteria->addSelectColumn(SubtitleIndikatorPeer::LOCK_SUBTITLE);

		$criteria->addSelectColumn(SubtitleIndikatorPeer::PRIORITAS);

		$criteria->addSelectColumn(SubtitleIndikatorPeer::CATATAN);

		$criteria->addSelectColumn(SubtitleIndikatorPeer::LAKILAKI);

		$criteria->addSelectColumn(SubtitleIndikatorPeer::PEREMPUAN);

		$criteria->addSelectColumn(SubtitleIndikatorPeer::DEWASA);

		$criteria->addSelectColumn(SubtitleIndikatorPeer::ANAK);

		$criteria->addSelectColumn(SubtitleIndikatorPeer::LANSIA);

		$criteria->addSelectColumn(SubtitleIndikatorPeer::INKLUSI);

		$criteria->addSelectColumn(SubtitleIndikatorPeer::GENDER);

		$criteria->addSelectColumn(SubtitleIndikatorPeer::ALOKASI_DANA);

		$criteria->addSelectColumn(SubtitleIndikatorPeer::IS_NOL_ANGGARAN);

		$criteria->addSelectColumn(SubtitleIndikatorPeer::INDIKATOR_CM);

		$criteria->addSelectColumn(SubtitleIndikatorPeer::NILAI_CM);

		$criteria->addSelectColumn(SubtitleIndikatorPeer::SATUAN_CM);

	}

	const COUNT = 'COUNT(ebudget.subtitle_indikator.SUB_ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.subtitle_indikator.SUB_ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(SubtitleIndikatorPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(SubtitleIndikatorPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = SubtitleIndikatorPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = SubtitleIndikatorPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return SubtitleIndikatorPeer::populateObjects(SubtitleIndikatorPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			SubtitleIndikatorPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = SubtitleIndikatorPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return SubtitleIndikatorPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}

		$criteria->remove(SubtitleIndikatorPeer::SUB_ID); 

				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(SubtitleIndikatorPeer::SUB_ID);
			$selectCriteria->add(SubtitleIndikatorPeer::SUB_ID, $criteria->remove(SubtitleIndikatorPeer::SUB_ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(SubtitleIndikatorPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(SubtitleIndikatorPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof SubtitleIndikator) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(SubtitleIndikatorPeer::SUB_ID, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(SubtitleIndikator $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(SubtitleIndikatorPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(SubtitleIndikatorPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(SubtitleIndikatorPeer::DATABASE_NAME, SubtitleIndikatorPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = SubtitleIndikatorPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(SubtitleIndikatorPeer::DATABASE_NAME);

		$criteria->add(SubtitleIndikatorPeer::SUB_ID, $pk);


		$v = SubtitleIndikatorPeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(SubtitleIndikatorPeer::SUB_ID, $pks, Criteria::IN);
			$objs = SubtitleIndikatorPeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BaseSubtitleIndikatorPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/SubtitleIndikatorMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.SubtitleIndikatorMapBuilder');
}
