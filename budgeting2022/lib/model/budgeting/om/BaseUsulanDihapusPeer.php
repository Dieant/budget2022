<?php


abstract class BaseUsulanDihapusPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'usulan_dihapus';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.UsulanDihapus';

	
	const NUM_COLUMNS = 13;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const DARI = 'usulan_dihapus.DARI';

	
	const PEKERJAAN = 'usulan_dihapus.PEKERJAAN';

	
	const KECAMATAN = 'usulan_dihapus.KECAMATAN';

	
	const KELURAHAN = 'usulan_dihapus.KELURAHAN';

	
	const NAMA = 'usulan_dihapus.NAMA';

	
	const TIPE = 'usulan_dihapus.TIPE';

	
	const LOKASI = 'usulan_dihapus.LOKASI';

	
	const VOLUME = 'usulan_dihapus.VOLUME';

	
	const KETERANGAN = 'usulan_dihapus.KETERANGAN';

	
	const DANA = 'usulan_dihapus.DANA';

	
	const SKPD = 'usulan_dihapus.SKPD';

	
	const TAHAP = 'usulan_dihapus.TAHAP';

	
	const ALASAN = 'usulan_dihapus.ALASAN';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('Dari', 'Pekerjaan', 'Kecamatan', 'Kelurahan', 'Nama', 'Tipe', 'Lokasi', 'Volume', 'Keterangan', 'Dana', 'Skpd', 'Tahap', 'Alasan', ),
		BasePeer::TYPE_COLNAME => array (UsulanDihapusPeer::DARI, UsulanDihapusPeer::PEKERJAAN, UsulanDihapusPeer::KECAMATAN, UsulanDihapusPeer::KELURAHAN, UsulanDihapusPeer::NAMA, UsulanDihapusPeer::TIPE, UsulanDihapusPeer::LOKASI, UsulanDihapusPeer::VOLUME, UsulanDihapusPeer::KETERANGAN, UsulanDihapusPeer::DANA, UsulanDihapusPeer::SKPD, UsulanDihapusPeer::TAHAP, UsulanDihapusPeer::ALASAN, ),
		BasePeer::TYPE_FIELDNAME => array ('dari', 'pekerjaan', 'kecamatan', 'kelurahan', 'nama', 'tipe', 'lokasi', 'volume', 'keterangan', 'dana', 'skpd', 'tahap', 'alasan', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('Dari' => 0, 'Pekerjaan' => 1, 'Kecamatan' => 2, 'Kelurahan' => 3, 'Nama' => 4, 'Tipe' => 5, 'Lokasi' => 6, 'Volume' => 7, 'Keterangan' => 8, 'Dana' => 9, 'Skpd' => 10, 'Tahap' => 11, 'Alasan' => 12, ),
		BasePeer::TYPE_COLNAME => array (UsulanDihapusPeer::DARI => 0, UsulanDihapusPeer::PEKERJAAN => 1, UsulanDihapusPeer::KECAMATAN => 2, UsulanDihapusPeer::KELURAHAN => 3, UsulanDihapusPeer::NAMA => 4, UsulanDihapusPeer::TIPE => 5, UsulanDihapusPeer::LOKASI => 6, UsulanDihapusPeer::VOLUME => 7, UsulanDihapusPeer::KETERANGAN => 8, UsulanDihapusPeer::DANA => 9, UsulanDihapusPeer::SKPD => 10, UsulanDihapusPeer::TAHAP => 11, UsulanDihapusPeer::ALASAN => 12, ),
		BasePeer::TYPE_FIELDNAME => array ('dari' => 0, 'pekerjaan' => 1, 'kecamatan' => 2, 'kelurahan' => 3, 'nama' => 4, 'tipe' => 5, 'lokasi' => 6, 'volume' => 7, 'keterangan' => 8, 'dana' => 9, 'skpd' => 10, 'tahap' => 11, 'alasan' => 12, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/UsulanDihapusMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.UsulanDihapusMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = UsulanDihapusPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(UsulanDihapusPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(UsulanDihapusPeer::DARI);

		$criteria->addSelectColumn(UsulanDihapusPeer::PEKERJAAN);

		$criteria->addSelectColumn(UsulanDihapusPeer::KECAMATAN);

		$criteria->addSelectColumn(UsulanDihapusPeer::KELURAHAN);

		$criteria->addSelectColumn(UsulanDihapusPeer::NAMA);

		$criteria->addSelectColumn(UsulanDihapusPeer::TIPE);

		$criteria->addSelectColumn(UsulanDihapusPeer::LOKASI);

		$criteria->addSelectColumn(UsulanDihapusPeer::VOLUME);

		$criteria->addSelectColumn(UsulanDihapusPeer::KETERANGAN);

		$criteria->addSelectColumn(UsulanDihapusPeer::DANA);

		$criteria->addSelectColumn(UsulanDihapusPeer::SKPD);

		$criteria->addSelectColumn(UsulanDihapusPeer::TAHAP);

		$criteria->addSelectColumn(UsulanDihapusPeer::ALASAN);

	}

	const COUNT = 'COUNT(*)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT *)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(UsulanDihapusPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(UsulanDihapusPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = UsulanDihapusPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = UsulanDihapusPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return UsulanDihapusPeer::populateObjects(UsulanDihapusPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			UsulanDihapusPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = UsulanDihapusPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return UsulanDihapusPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(UsulanDihapusPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(UsulanDihapusPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof UsulanDihapus) {

			$criteria = $values->buildCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
												if(count($values) == count($values, COUNT_RECURSIVE))
			{
								$values = array($values);
			}
			$vals = array();
			foreach($values as $value)
			{

			}

		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(UsulanDihapus $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(UsulanDihapusPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(UsulanDihapusPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(UsulanDihapusPeer::DATABASE_NAME, UsulanDihapusPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = UsulanDihapusPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

} 
if (Propel::isInit()) {
			try {
		BaseUsulanDihapusPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/UsulanDihapusMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.UsulanDihapusMapBuilder');
}
