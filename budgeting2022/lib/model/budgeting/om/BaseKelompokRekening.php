<?php


abstract class BaseKelompokRekening extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $rekening_code;


	
	protected $rekening_name;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getRekeningCode()
	{

		return $this->rekening_code;
	}

	
	public function getRekeningName()
	{

		return $this->rekening_name;
	}

	
	public function setRekeningCode($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->rekening_code !== $v) {
			$this->rekening_code = $v;
			$this->modifiedColumns[] = KelompokRekeningPeer::REKENING_CODE;
		}

	} 
	
	public function setRekeningName($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->rekening_name !== $v) {
			$this->rekening_name = $v;
			$this->modifiedColumns[] = KelompokRekeningPeer::REKENING_NAME;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->rekening_code = $rs->getString($startcol + 0);

			$this->rekening_name = $rs->getString($startcol + 1);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 2; 
		} catch (Exception $e) {
			throw new PropelException("Error populating KelompokRekening object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(KelompokRekeningPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			KelompokRekeningPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(KelompokRekeningPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = KelompokRekeningPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += KelompokRekeningPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = KelompokRekeningPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = KelompokRekeningPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getRekeningCode();
				break;
			case 1:
				return $this->getRekeningName();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = KelompokRekeningPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getRekeningCode(),
			$keys[1] => $this->getRekeningName(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = KelompokRekeningPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setRekeningCode($value);
				break;
			case 1:
				$this->setRekeningName($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = KelompokRekeningPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setRekeningCode($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setRekeningName($arr[$keys[1]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(KelompokRekeningPeer::DATABASE_NAME);

		if ($this->isColumnModified(KelompokRekeningPeer::REKENING_CODE)) $criteria->add(KelompokRekeningPeer::REKENING_CODE, $this->rekening_code);
		if ($this->isColumnModified(KelompokRekeningPeer::REKENING_NAME)) $criteria->add(KelompokRekeningPeer::REKENING_NAME, $this->rekening_name);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(KelompokRekeningPeer::DATABASE_NAME);

		$criteria->add(KelompokRekeningPeer::REKENING_CODE, $this->rekening_code);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getRekeningCode();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setRekeningCode($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setRekeningName($this->rekening_name);


		$copyObj->setNew(true);

		$copyObj->setRekeningCode(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new KelompokRekeningPeer();
		}
		return self::$peer;
	}

} 