<?php


abstract class BaseDpaImport2Peer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.dpa_import2';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.DpaImport2';

	
	const NUM_COLUMNS = 16;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const UNIT_ID = 'ebudget.dpa_import2.UNIT_ID';

	
	const KEGIATAN_CODE = 'ebudget.dpa_import2.KEGIATAN_CODE';

	
	const DETAIL_NO = 'ebudget.dpa_import2.DETAIL_NO';

	
	const NAMA_KEGIATAN = 'ebudget.dpa_import2.NAMA_KEGIATAN';

	
	const SUBTITLE = 'ebudget.dpa_import2.SUBTITLE';

	
	const REKENING_CODE = 'ebudget.dpa_import2.REKENING_CODE';

	
	const REKENING_NAME = 'ebudget.dpa_import2.REKENING_NAME';

	
	const KOMPONEN_ID = 'ebudget.dpa_import2.KOMPONEN_ID';

	
	const KOMPONEN_NAME = 'ebudget.dpa_import2.KOMPONEN_NAME';

	
	const DETAIL_NAME = 'ebudget.dpa_import2.DETAIL_NAME';

	
	const KOMPONEN_HARGA_AWAL = 'ebudget.dpa_import2.KOMPONEN_HARGA_AWAL';

	
	const KETERANGAN_KOEFISIEN = 'ebudget.dpa_import2.KETERANGAN_KOEFISIEN';

	
	const VOLUME = 'ebudget.dpa_import2.VOLUME';

	
	const TOTAL = 'ebudget.dpa_import2.TOTAL';

	
	const DT_IMPORT = 'ebudget.dpa_import2.DT_IMPORT';

	
	const SATUAN = 'ebudget.dpa_import2.SATUAN';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('UnitId', 'KegiatanCode', 'DetailNo', 'NamaKegiatan', 'Subtitle', 'RekeningCode', 'RekeningName', 'KomponenId', 'KomponenName', 'DetailName', 'KomponenHargaAwal', 'KeteranganKoefisien', 'Volume', 'Total', 'DtImport', 'Satuan', ),
		BasePeer::TYPE_COLNAME => array (DpaImport2Peer::UNIT_ID, DpaImport2Peer::KEGIATAN_CODE, DpaImport2Peer::DETAIL_NO, DpaImport2Peer::NAMA_KEGIATAN, DpaImport2Peer::SUBTITLE, DpaImport2Peer::REKENING_CODE, DpaImport2Peer::REKENING_NAME, DpaImport2Peer::KOMPONEN_ID, DpaImport2Peer::KOMPONEN_NAME, DpaImport2Peer::DETAIL_NAME, DpaImport2Peer::KOMPONEN_HARGA_AWAL, DpaImport2Peer::KETERANGAN_KOEFISIEN, DpaImport2Peer::VOLUME, DpaImport2Peer::TOTAL, DpaImport2Peer::DT_IMPORT, DpaImport2Peer::SATUAN, ),
		BasePeer::TYPE_FIELDNAME => array ('unit_id', 'kegiatan_code', 'detail_no', 'nama_kegiatan', 'subtitle', 'rekening_code', 'rekening_name', 'komponen_id', 'komponen_name', 'detail_name', 'komponen_harga_awal', 'keterangan_koefisien', 'volume', 'total', 'dt_import', 'satuan', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('UnitId' => 0, 'KegiatanCode' => 1, 'DetailNo' => 2, 'NamaKegiatan' => 3, 'Subtitle' => 4, 'RekeningCode' => 5, 'RekeningName' => 6, 'KomponenId' => 7, 'KomponenName' => 8, 'DetailName' => 9, 'KomponenHargaAwal' => 10, 'KeteranganKoefisien' => 11, 'Volume' => 12, 'Total' => 13, 'DtImport' => 14, 'Satuan' => 15, ),
		BasePeer::TYPE_COLNAME => array (DpaImport2Peer::UNIT_ID => 0, DpaImport2Peer::KEGIATAN_CODE => 1, DpaImport2Peer::DETAIL_NO => 2, DpaImport2Peer::NAMA_KEGIATAN => 3, DpaImport2Peer::SUBTITLE => 4, DpaImport2Peer::REKENING_CODE => 5, DpaImport2Peer::REKENING_NAME => 6, DpaImport2Peer::KOMPONEN_ID => 7, DpaImport2Peer::KOMPONEN_NAME => 8, DpaImport2Peer::DETAIL_NAME => 9, DpaImport2Peer::KOMPONEN_HARGA_AWAL => 10, DpaImport2Peer::KETERANGAN_KOEFISIEN => 11, DpaImport2Peer::VOLUME => 12, DpaImport2Peer::TOTAL => 13, DpaImport2Peer::DT_IMPORT => 14, DpaImport2Peer::SATUAN => 15, ),
		BasePeer::TYPE_FIELDNAME => array ('unit_id' => 0, 'kegiatan_code' => 1, 'detail_no' => 2, 'nama_kegiatan' => 3, 'subtitle' => 4, 'rekening_code' => 5, 'rekening_name' => 6, 'komponen_id' => 7, 'komponen_name' => 8, 'detail_name' => 9, 'komponen_harga_awal' => 10, 'keterangan_koefisien' => 11, 'volume' => 12, 'total' => 13, 'dt_import' => 14, 'satuan' => 15, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/DpaImport2MapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.DpaImport2MapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = DpaImport2Peer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(DpaImport2Peer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(DpaImport2Peer::UNIT_ID);

		$criteria->addSelectColumn(DpaImport2Peer::KEGIATAN_CODE);

		$criteria->addSelectColumn(DpaImport2Peer::DETAIL_NO);

		$criteria->addSelectColumn(DpaImport2Peer::NAMA_KEGIATAN);

		$criteria->addSelectColumn(DpaImport2Peer::SUBTITLE);

		$criteria->addSelectColumn(DpaImport2Peer::REKENING_CODE);

		$criteria->addSelectColumn(DpaImport2Peer::REKENING_NAME);

		$criteria->addSelectColumn(DpaImport2Peer::KOMPONEN_ID);

		$criteria->addSelectColumn(DpaImport2Peer::KOMPONEN_NAME);

		$criteria->addSelectColumn(DpaImport2Peer::DETAIL_NAME);

		$criteria->addSelectColumn(DpaImport2Peer::KOMPONEN_HARGA_AWAL);

		$criteria->addSelectColumn(DpaImport2Peer::KETERANGAN_KOEFISIEN);

		$criteria->addSelectColumn(DpaImport2Peer::VOLUME);

		$criteria->addSelectColumn(DpaImport2Peer::TOTAL);

		$criteria->addSelectColumn(DpaImport2Peer::DT_IMPORT);

		$criteria->addSelectColumn(DpaImport2Peer::SATUAN);

	}

	const COUNT = 'COUNT(ebudget.dpa_import2.UNIT_ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.dpa_import2.UNIT_ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(DpaImport2Peer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(DpaImport2Peer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = DpaImport2Peer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = DpaImport2Peer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return DpaImport2Peer::populateObjects(DpaImport2Peer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			DpaImport2Peer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = DpaImport2Peer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return DpaImport2Peer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(DpaImport2Peer::UNIT_ID);
			$selectCriteria->add(DpaImport2Peer::UNIT_ID, $criteria->remove(DpaImport2Peer::UNIT_ID), $comparison);

			$comparison = $criteria->getComparison(DpaImport2Peer::KEGIATAN_CODE);
			$selectCriteria->add(DpaImport2Peer::KEGIATAN_CODE, $criteria->remove(DpaImport2Peer::KEGIATAN_CODE), $comparison);

			$comparison = $criteria->getComparison(DpaImport2Peer::DETAIL_NO);
			$selectCriteria->add(DpaImport2Peer::DETAIL_NO, $criteria->remove(DpaImport2Peer::DETAIL_NO), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(DpaImport2Peer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(DpaImport2Peer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof DpaImport2) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
												if(count($values) == count($values, COUNT_RECURSIVE))
			{
								$values = array($values);
			}
			$vals = array();
			foreach($values as $value)
			{

				$vals[0][] = $value[0];
				$vals[1][] = $value[1];
				$vals[2][] = $value[2];
			}

			$criteria->add(DpaImport2Peer::UNIT_ID, $vals[0], Criteria::IN);
			$criteria->add(DpaImport2Peer::KEGIATAN_CODE, $vals[1], Criteria::IN);
			$criteria->add(DpaImport2Peer::DETAIL_NO, $vals[2], Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(DpaImport2 $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(DpaImport2Peer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(DpaImport2Peer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(DpaImport2Peer::DATABASE_NAME, DpaImport2Peer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = DpaImport2Peer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK( $unit_id, $kegiatan_code, $detail_no, $con = null) {
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$criteria = new Criteria();
		$criteria->add(DpaImport2Peer::UNIT_ID, $unit_id);
		$criteria->add(DpaImport2Peer::KEGIATAN_CODE, $kegiatan_code);
		$criteria->add(DpaImport2Peer::DETAIL_NO, $detail_no);
		$v = DpaImport2Peer::doSelect($criteria, $con);

		return !empty($v) ? $v[0] : null;
	}
} 
if (Propel::isInit()) {
			try {
		BaseDpaImport2Peer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/DpaImport2MapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.DpaImport2MapBuilder');
}
