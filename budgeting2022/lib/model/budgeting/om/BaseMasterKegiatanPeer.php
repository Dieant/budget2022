<?php


abstract class BaseMasterKegiatanPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.master_kegiatan';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.MasterKegiatan';

	
	const NUM_COLUMNS = 101;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const UNIT_ID = 'ebudget.master_kegiatan.UNIT_ID';

	
	const KODE_KEGIATAN = 'ebudget.master_kegiatan.KODE_KEGIATAN';

	
	const KODE_BIDANG = 'ebudget.master_kegiatan.KODE_BIDANG';

	
	const KODE_URUSAN_WAJIB = 'ebudget.master_kegiatan.KODE_URUSAN_WAJIB';

	
	const KODE_PROGRAM = 'ebudget.master_kegiatan.KODE_PROGRAM';

	
	const KODE_SASARAN = 'ebudget.master_kegiatan.KODE_SASARAN';

	
	const KODE_INDIKATOR = 'ebudget.master_kegiatan.KODE_INDIKATOR';

	
	const ALOKASI_DANA = 'ebudget.master_kegiatan.ALOKASI_DANA';

	
	const NAMA_KEGIATAN = 'ebudget.master_kegiatan.NAMA_KEGIATAN';

	
	const MASUKAN = 'ebudget.master_kegiatan.MASUKAN';

	
	const OUTPUT = 'ebudget.master_kegiatan.OUTPUT';

	
	const OUTCOME = 'ebudget.master_kegiatan.OUTCOME';

	
	const BENEFIT = 'ebudget.master_kegiatan.BENEFIT';

	
	const IMPACT = 'ebudget.master_kegiatan.IMPACT';

	
	const TIPE = 'ebudget.master_kegiatan.TIPE';

	
	const KEGIATAN_ACTIVE = 'ebudget.master_kegiatan.KEGIATAN_ACTIVE';

	
	const TO_KEGIATAN_CODE = 'ebudget.master_kegiatan.TO_KEGIATAN_CODE';

	
	const CATATAN = 'ebudget.master_kegiatan.CATATAN';

	
	const TARGET_OUTCOME = 'ebudget.master_kegiatan.TARGET_OUTCOME';

	
	const LOKASI = 'ebudget.master_kegiatan.LOKASI';

	
	const JUMLAH_PREV = 'ebudget.master_kegiatan.JUMLAH_PREV';

	
	const JUMLAH_NOW = 'ebudget.master_kegiatan.JUMLAH_NOW';

	
	const JUMLAH_NEXT = 'ebudget.master_kegiatan.JUMLAH_NEXT';

	
	const KODE_PROGRAM2 = 'ebudget.master_kegiatan.KODE_PROGRAM2';

	
	const KODE_URUSAN = 'ebudget.master_kegiatan.KODE_URUSAN';

	
	const LAST_UPDATE_USER = 'ebudget.master_kegiatan.LAST_UPDATE_USER';

	
	const LAST_UPDATE_TIME = 'ebudget.master_kegiatan.LAST_UPDATE_TIME';

	
	const LAST_UPDATE_IP = 'ebudget.master_kegiatan.LAST_UPDATE_IP';

	
	const TAHAP = 'ebudget.master_kegiatan.TAHAP';

	
	const KODE_MISI = 'ebudget.master_kegiatan.KODE_MISI';

	
	const KODE_TUJUAN = 'ebudget.master_kegiatan.KODE_TUJUAN';

	
	const RANKING = 'ebudget.master_kegiatan.RANKING';

	
	const NOMOR13 = 'ebudget.master_kegiatan.NOMOR13';

	
	const PPA_NAMA = 'ebudget.master_kegiatan.PPA_NAMA';

	
	const PPA_PANGKAT = 'ebudget.master_kegiatan.PPA_PANGKAT';

	
	const PPA_NIP = 'ebudget.master_kegiatan.PPA_NIP';

	
	const LANJUTAN = 'ebudget.master_kegiatan.LANJUTAN';

	
	const USER_ID = 'ebudget.master_kegiatan.USER_ID';

	
	const ID = 'ebudget.master_kegiatan.ID';

	
	const TAHUN = 'ebudget.master_kegiatan.TAHUN';

	
	const TAMBAHAN_PAGU = 'ebudget.master_kegiatan.TAMBAHAN_PAGU';

	
	const GENDER = 'ebudget.master_kegiatan.GENDER';

	
	const KODE_KEG_KEUANGAN = 'ebudget.master_kegiatan.KODE_KEG_KEUANGAN';

	
	const USER_ID_LAMA = 'ebudget.master_kegiatan.USER_ID_LAMA';

	
	const INDIKATOR = 'ebudget.master_kegiatan.INDIKATOR';

	
	const IS_DAK = 'ebudget.master_kegiatan.IS_DAK';

	
	const KODE_KEGIATAN_ASAL = 'ebudget.master_kegiatan.KODE_KEGIATAN_ASAL';

	
	const KODE_KEG_KEUANGAN_ASAL = 'ebudget.master_kegiatan.KODE_KEG_KEUANGAN_ASAL';

	
	const TH_KE_MULTIYEARS = 'ebudget.master_kegiatan.TH_KE_MULTIYEARS';

	
	const KELOMPOK_SASARAN = 'ebudget.master_kegiatan.KELOMPOK_SASARAN';

	
	const PAGU_BAPPEKO = 'ebudget.master_kegiatan.PAGU_BAPPEKO';

	
	const KODE_DPA = 'ebudget.master_kegiatan.KODE_DPA';

	
	const USER_ID_PPTK = 'ebudget.master_kegiatan.USER_ID_PPTK';

	
	const USER_ID_KPA = 'ebudget.master_kegiatan.USER_ID_KPA';

	
	const CATATAN_PEMBAHASAN = 'ebudget.master_kegiatan.CATATAN_PEMBAHASAN';

	
	const IS_TAPD_SETUJU = 'ebudget.master_kegiatan.IS_TAPD_SETUJU';

	
	const IS_BAPPEKO_SETUJU = 'ebudget.master_kegiatan.IS_BAPPEKO_SETUJU';

	
	const IS_PENYELIA_SETUJU = 'ebudget.master_kegiatan.IS_PENYELIA_SETUJU';

	
	const IS_PERNAH_RKA = 'ebudget.master_kegiatan.IS_PERNAH_RKA';

	
	const KODE_KEGIATAN_BARU = 'ebudget.master_kegiatan.KODE_KEGIATAN_BARU';

	
	const CATATAN_BPKPD = 'ebudget.master_kegiatan.CATATAN_BPKPD';

	
	const UBAH_F1_DINAS = 'ebudget.master_kegiatan.UBAH_F1_DINAS';

	
	const UBAH_F1_PENELITI = 'ebudget.master_kegiatan.UBAH_F1_PENELITI';

	
	const SISA_LELANG_DINAS = 'ebudget.master_kegiatan.SISA_LELANG_DINAS';

	
	const SISA_LELANG_PENELITI = 'ebudget.master_kegiatan.SISA_LELANG_PENELITI';

	
	const CATATAN_UBAH_F1_DINAS = 'ebudget.master_kegiatan.CATATAN_UBAH_F1_DINAS';

	
	const CATATAN_SISA_LELANG_PENELITI = 'ebudget.master_kegiatan.CATATAN_SISA_LELANG_PENELITI';

	
	const PPTK_APPROVAL = 'ebudget.master_kegiatan.PPTK_APPROVAL';

	
	const KPA_APPROVAL = 'ebudget.master_kegiatan.KPA_APPROVAL';

	
	const CATATAN_BAGIAN_HUKUM = 'ebudget.master_kegiatan.CATATAN_BAGIAN_HUKUM';

	
	const CATATAN_INSPEKTORAT = 'ebudget.master_kegiatan.CATATAN_INSPEKTORAT';

	
	const CATATAN_BADAN_KEPEGAWAIAN = 'ebudget.master_kegiatan.CATATAN_BADAN_KEPEGAWAIAN';

	
	const CATATAN_LPPA = 'ebudget.master_kegiatan.CATATAN_LPPA';

	
	const IS_BAGIAN_HUKUM_SETUJU = 'ebudget.master_kegiatan.IS_BAGIAN_HUKUM_SETUJU';

	
	const IS_INSPEKTORAT_SETUJU = 'ebudget.master_kegiatan.IS_INSPEKTORAT_SETUJU';

	
	const IS_BADAN_KEPEGAWAIAN_SETUJU = 'ebudget.master_kegiatan.IS_BADAN_KEPEGAWAIAN_SETUJU';

	
	const IS_LPPA_SETUJU = 'ebudget.master_kegiatan.IS_LPPA_SETUJU';

	
	const VERIFIKASI_BPKPD = 'ebudget.master_kegiatan.VERIFIKASI_BPKPD';

	
	const VERIFIKASI_BAPPEKO = 'ebudget.master_kegiatan.VERIFIKASI_BAPPEKO';

	
	const VERIFIKASI_PENYELIA = 'ebudget.master_kegiatan.VERIFIKASI_PENYELIA';

	
	const VERIFIKASI_BAGIAN_HUKUM = 'ebudget.master_kegiatan.VERIFIKASI_BAGIAN_HUKUM';

	
	const VERIFIKASI_INSPEKTORAT = 'ebudget.master_kegiatan.VERIFIKASI_INSPEKTORAT';

	
	const VERIFIKASI_BADAN_KEPEGAWAIAN = 'ebudget.master_kegiatan.VERIFIKASI_BADAN_KEPEGAWAIAN';

	
	const VERIFIKASI_LPPA = 'ebudget.master_kegiatan.VERIFIKASI_LPPA';

	
	const METODE_COUNT = 'ebudget.master_kegiatan.METODE_COUNT';

	
	const CATATAN_BAGIAN_ORGANISASI = 'ebudget.master_kegiatan.CATATAN_BAGIAN_ORGANISASI';

	
	const IS_BAGIAN_ORGANISASI_SETUJU = 'ebudget.master_kegiatan.IS_BAGIAN_ORGANISASI_SETUJU';

	
	const VERIFIKASI_BAGIAN_ORGANISASI = 'ebudget.master_kegiatan.VERIFIKASI_BAGIAN_ORGANISASI';

	
	const CATATAN_ASISTEN1 = 'ebudget.master_kegiatan.CATATAN_ASISTEN1';

	
	const IS_ASISTEN1_SETUJU = 'ebudget.master_kegiatan.IS_ASISTEN1_SETUJU';

	
	const VERIFIKASI_ASISTEN1 = 'ebudget.master_kegiatan.VERIFIKASI_ASISTEN1';

	
	const CATATAN_ASISTEN2 = 'ebudget.master_kegiatan.CATATAN_ASISTEN2';

	
	const IS_ASISTEN2_SETUJU = 'ebudget.master_kegiatan.IS_ASISTEN2_SETUJU';

	
	const VERIFIKASI_ASISTEN2 = 'ebudget.master_kegiatan.VERIFIKASI_ASISTEN2';

	
	const CATATAN_ASISTEN3 = 'ebudget.master_kegiatan.CATATAN_ASISTEN3';

	
	const IS_ASISTEN3_SETUJU = 'ebudget.master_kegiatan.IS_ASISTEN3_SETUJU';

	
	const VERIFIKASI_ASISTEN3 = 'ebudget.master_kegiatan.VERIFIKASI_ASISTEN3';

	
	const CATATAN_SEKDA = 'ebudget.master_kegiatan.CATATAN_SEKDA';

	
	const IS_SEKDA_SETUJU = 'ebudget.master_kegiatan.IS_SEKDA_SETUJU';

	
	const VERIFIKASI_SEKDA = 'ebudget.master_kegiatan.VERIFIKASI_SEKDA';

	
	const VERIFIKASI_ASISTEN = 'ebudget.master_kegiatan.VERIFIKASI_ASISTEN';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('UnitId', 'KodeKegiatan', 'KodeBidang', 'KodeUrusanWajib', 'KodeProgram', 'KodeSasaran', 'KodeIndikator', 'AlokasiDana', 'NamaKegiatan', 'Masukan', 'Output', 'Outcome', 'Benefit', 'Impact', 'Tipe', 'KegiatanActive', 'ToKegiatanCode', 'Catatan', 'TargetOutcome', 'Lokasi', 'JumlahPrev', 'JumlahNow', 'JumlahNext', 'KodeProgram2', 'KodeUrusan', 'LastUpdateUser', 'LastUpdateTime', 'LastUpdateIp', 'Tahap', 'KodeMisi', 'KodeTujuan', 'Ranking', 'Nomor13', 'PpaNama', 'PpaPangkat', 'PpaNip', 'Lanjutan', 'UserId', 'Id', 'Tahun', 'TambahanPagu', 'Gender', 'KodeKegKeuangan', 'UserIdLama', 'Indikator', 'IsDak', 'KodeKegiatanAsal', 'KodeKegKeuanganAsal', 'ThKeMultiyears', 'KelompokSasaran', 'PaguBappeko', 'KodeDpa', 'UserIdPptk', 'UserIdKpa', 'CatatanPembahasan', 'IsTapdSetuju', 'IsBappekoSetuju', 'IsPenyeliaSetuju', 'IsPernahRka', 'KodeKegiatanBaru', 'CatatanBpkpd', 'UbahF1Dinas', 'UbahF1Peneliti', 'SisaLelangDinas', 'SisaLelangPeneliti', 'CatatanUbahF1Dinas', 'CatatanSisaLelangPeneliti', 'PptkApproval', 'KpaApproval', 'CatatanBagianHukum', 'CatatanInspektorat', 'CatatanBadanKepegawaian', 'CatatanLppa', 'IsBagianHukumSetuju', 'IsInspektoratSetuju', 'IsBadanKepegawaianSetuju', 'IsLppaSetuju', 'VerifikasiBpkpd', 'VerifikasiBappeko', 'VerifikasiPenyelia', 'VerifikasiBagianHukum', 'VerifikasiInspektorat', 'VerifikasiBadanKepegawaian', 'VerifikasiLppa', 'MetodeCount', 'CatatanBagianOrganisasi', 'IsBagianOrganisasiSetuju', 'VerifikasiBagianOrganisasi', 'CatatanAsisten1', 'IsAsisten1Setuju', 'VerifikasiAsisten1', 'CatatanAsisten2', 'IsAsisten2Setuju', 'VerifikasiAsisten2', 'CatatanAsisten3', 'IsAsisten3Setuju', 'VerifikasiAsisten3', 'CatatanSekda', 'IsSekdaSetuju', 'VerifikasiSekda', 'VerifikasiAsisten', ),
		BasePeer::TYPE_COLNAME => array (MasterKegiatanPeer::UNIT_ID, MasterKegiatanPeer::KODE_KEGIATAN, MasterKegiatanPeer::KODE_BIDANG, MasterKegiatanPeer::KODE_URUSAN_WAJIB, MasterKegiatanPeer::KODE_PROGRAM, MasterKegiatanPeer::KODE_SASARAN, MasterKegiatanPeer::KODE_INDIKATOR, MasterKegiatanPeer::ALOKASI_DANA, MasterKegiatanPeer::NAMA_KEGIATAN, MasterKegiatanPeer::MASUKAN, MasterKegiatanPeer::OUTPUT, MasterKegiatanPeer::OUTCOME, MasterKegiatanPeer::BENEFIT, MasterKegiatanPeer::IMPACT, MasterKegiatanPeer::TIPE, MasterKegiatanPeer::KEGIATAN_ACTIVE, MasterKegiatanPeer::TO_KEGIATAN_CODE, MasterKegiatanPeer::CATATAN, MasterKegiatanPeer::TARGET_OUTCOME, MasterKegiatanPeer::LOKASI, MasterKegiatanPeer::JUMLAH_PREV, MasterKegiatanPeer::JUMLAH_NOW, MasterKegiatanPeer::JUMLAH_NEXT, MasterKegiatanPeer::KODE_PROGRAM2, MasterKegiatanPeer::KODE_URUSAN, MasterKegiatanPeer::LAST_UPDATE_USER, MasterKegiatanPeer::LAST_UPDATE_TIME, MasterKegiatanPeer::LAST_UPDATE_IP, MasterKegiatanPeer::TAHAP, MasterKegiatanPeer::KODE_MISI, MasterKegiatanPeer::KODE_TUJUAN, MasterKegiatanPeer::RANKING, MasterKegiatanPeer::NOMOR13, MasterKegiatanPeer::PPA_NAMA, MasterKegiatanPeer::PPA_PANGKAT, MasterKegiatanPeer::PPA_NIP, MasterKegiatanPeer::LANJUTAN, MasterKegiatanPeer::USER_ID, MasterKegiatanPeer::ID, MasterKegiatanPeer::TAHUN, MasterKegiatanPeer::TAMBAHAN_PAGU, MasterKegiatanPeer::GENDER, MasterKegiatanPeer::KODE_KEG_KEUANGAN, MasterKegiatanPeer::USER_ID_LAMA, MasterKegiatanPeer::INDIKATOR, MasterKegiatanPeer::IS_DAK, MasterKegiatanPeer::KODE_KEGIATAN_ASAL, MasterKegiatanPeer::KODE_KEG_KEUANGAN_ASAL, MasterKegiatanPeer::TH_KE_MULTIYEARS, MasterKegiatanPeer::KELOMPOK_SASARAN, MasterKegiatanPeer::PAGU_BAPPEKO, MasterKegiatanPeer::KODE_DPA, MasterKegiatanPeer::USER_ID_PPTK, MasterKegiatanPeer::USER_ID_KPA, MasterKegiatanPeer::CATATAN_PEMBAHASAN, MasterKegiatanPeer::IS_TAPD_SETUJU, MasterKegiatanPeer::IS_BAPPEKO_SETUJU, MasterKegiatanPeer::IS_PENYELIA_SETUJU, MasterKegiatanPeer::IS_PERNAH_RKA, MasterKegiatanPeer::KODE_KEGIATAN_BARU, MasterKegiatanPeer::CATATAN_BPKPD, MasterKegiatanPeer::UBAH_F1_DINAS, MasterKegiatanPeer::UBAH_F1_PENELITI, MasterKegiatanPeer::SISA_LELANG_DINAS, MasterKegiatanPeer::SISA_LELANG_PENELITI, MasterKegiatanPeer::CATATAN_UBAH_F1_DINAS, MasterKegiatanPeer::CATATAN_SISA_LELANG_PENELITI, MasterKegiatanPeer::PPTK_APPROVAL, MasterKegiatanPeer::KPA_APPROVAL, MasterKegiatanPeer::CATATAN_BAGIAN_HUKUM, MasterKegiatanPeer::CATATAN_INSPEKTORAT, MasterKegiatanPeer::CATATAN_BADAN_KEPEGAWAIAN, MasterKegiatanPeer::CATATAN_LPPA, MasterKegiatanPeer::IS_BAGIAN_HUKUM_SETUJU, MasterKegiatanPeer::IS_INSPEKTORAT_SETUJU, MasterKegiatanPeer::IS_BADAN_KEPEGAWAIAN_SETUJU, MasterKegiatanPeer::IS_LPPA_SETUJU, MasterKegiatanPeer::VERIFIKASI_BPKPD, MasterKegiatanPeer::VERIFIKASI_BAPPEKO, MasterKegiatanPeer::VERIFIKASI_PENYELIA, MasterKegiatanPeer::VERIFIKASI_BAGIAN_HUKUM, MasterKegiatanPeer::VERIFIKASI_INSPEKTORAT, MasterKegiatanPeer::VERIFIKASI_BADAN_KEPEGAWAIAN, MasterKegiatanPeer::VERIFIKASI_LPPA, MasterKegiatanPeer::METODE_COUNT, MasterKegiatanPeer::CATATAN_BAGIAN_ORGANISASI, MasterKegiatanPeer::IS_BAGIAN_ORGANISASI_SETUJU, MasterKegiatanPeer::VERIFIKASI_BAGIAN_ORGANISASI, MasterKegiatanPeer::CATATAN_ASISTEN1, MasterKegiatanPeer::IS_ASISTEN1_SETUJU, MasterKegiatanPeer::VERIFIKASI_ASISTEN1, MasterKegiatanPeer::CATATAN_ASISTEN2, MasterKegiatanPeer::IS_ASISTEN2_SETUJU, MasterKegiatanPeer::VERIFIKASI_ASISTEN2, MasterKegiatanPeer::CATATAN_ASISTEN3, MasterKegiatanPeer::IS_ASISTEN3_SETUJU, MasterKegiatanPeer::VERIFIKASI_ASISTEN3, MasterKegiatanPeer::CATATAN_SEKDA, MasterKegiatanPeer::IS_SEKDA_SETUJU, MasterKegiatanPeer::VERIFIKASI_SEKDA, MasterKegiatanPeer::VERIFIKASI_ASISTEN, ),
		BasePeer::TYPE_FIELDNAME => array ('unit_id', 'kode_kegiatan', 'kode_bidang', 'kode_urusan_wajib', 'kode_program', 'kode_sasaran', 'kode_indikator', 'alokasi_dana', 'nama_kegiatan', 'masukan', 'output', 'outcome', 'benefit', 'impact', 'tipe', 'kegiatan_active', 'to_kegiatan_code', 'catatan', 'target_outcome', 'lokasi', 'jumlah_prev', 'jumlah_now', 'jumlah_next', 'kode_program2', 'kode_urusan', 'last_update_user', 'last_update_time', 'last_update_ip', 'tahap', 'kode_misi', 'kode_tujuan', 'ranking', 'nomor13', 'ppa_nama', 'ppa_pangkat', 'ppa_nip', 'lanjutan', 'user_id', 'id', 'tahun', 'tambahan_pagu', 'gender', 'kode_keg_keuangan', 'user_id_lama', 'indikator', 'is_dak', 'kode_kegiatan_asal', 'kode_keg_keuangan_asal', 'th_ke_multiyears', 'kelompok_sasaran', 'pagu_bappeko', 'kode_dpa', 'user_id_pptk', 'user_id_kpa', 'catatan_pembahasan', 'is_tapd_setuju', 'is_bappeko_setuju', 'is_penyelia_setuju', 'is_pernah_rka', 'kode_kegiatan_baru', 'catatan_bpkpd', 'ubah_f1_dinas', 'ubah_f1_peneliti', 'sisa_lelang_dinas', 'sisa_lelang_peneliti', 'catatan_ubah_f1_dinas', 'catatan_sisa_lelang_peneliti', 'pptk_approval', 'kpa_approval', 'catatan_bagian_hukum', 'catatan_inspektorat', 'catatan_badan_kepegawaian', 'catatan_lppa', 'is_bagian_hukum_setuju', 'is_inspektorat_setuju', 'is_badan_kepegawaian_setuju', 'is_lppa_setuju', 'verifikasi_bpkpd', 'verifikasi_bappeko', 'verifikasi_penyelia', 'verifikasi_bagian_hukum', 'verifikasi_inspektorat', 'verifikasi_badan_kepegawaian', 'verifikasi_lppa', 'metode_count', 'catatan_bagian_organisasi', 'is_bagian_organisasi_setuju', 'verifikasi_bagian_organisasi', 'catatan_asisten1', 'is_asisten1_setuju', 'verifikasi_asisten1', 'catatan_asisten2', 'is_asisten2_setuju', 'verifikasi_asisten2', 'catatan_asisten3', 'is_asisten3_setuju', 'verifikasi_asisten3', 'catatan_sekda', 'is_sekda_setuju', 'verifikasi_sekda', 'verifikasi_asisten', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('UnitId' => 0, 'KodeKegiatan' => 1, 'KodeBidang' => 2, 'KodeUrusanWajib' => 3, 'KodeProgram' => 4, 'KodeSasaran' => 5, 'KodeIndikator' => 6, 'AlokasiDana' => 7, 'NamaKegiatan' => 8, 'Masukan' => 9, 'Output' => 10, 'Outcome' => 11, 'Benefit' => 12, 'Impact' => 13, 'Tipe' => 14, 'KegiatanActive' => 15, 'ToKegiatanCode' => 16, 'Catatan' => 17, 'TargetOutcome' => 18, 'Lokasi' => 19, 'JumlahPrev' => 20, 'JumlahNow' => 21, 'JumlahNext' => 22, 'KodeProgram2' => 23, 'KodeUrusan' => 24, 'LastUpdateUser' => 25, 'LastUpdateTime' => 26, 'LastUpdateIp' => 27, 'Tahap' => 28, 'KodeMisi' => 29, 'KodeTujuan' => 30, 'Ranking' => 31, 'Nomor13' => 32, 'PpaNama' => 33, 'PpaPangkat' => 34, 'PpaNip' => 35, 'Lanjutan' => 36, 'UserId' => 37, 'Id' => 38, 'Tahun' => 39, 'TambahanPagu' => 40, 'Gender' => 41, 'KodeKegKeuangan' => 42, 'UserIdLama' => 43, 'Indikator' => 44, 'IsDak' => 45, 'KodeKegiatanAsal' => 46, 'KodeKegKeuanganAsal' => 47, 'ThKeMultiyears' => 48, 'KelompokSasaran' => 49, 'PaguBappeko' => 50, 'KodeDpa' => 51, 'UserIdPptk' => 52, 'UserIdKpa' => 53, 'CatatanPembahasan' => 54, 'IsTapdSetuju' => 55, 'IsBappekoSetuju' => 56, 'IsPenyeliaSetuju' => 57, 'IsPernahRka' => 58, 'KodeKegiatanBaru' => 59, 'CatatanBpkpd' => 60, 'UbahF1Dinas' => 61, 'UbahF1Peneliti' => 62, 'SisaLelangDinas' => 63, 'SisaLelangPeneliti' => 64, 'CatatanUbahF1Dinas' => 65, 'CatatanSisaLelangPeneliti' => 66, 'PptkApproval' => 67, 'KpaApproval' => 68, 'CatatanBagianHukum' => 69, 'CatatanInspektorat' => 70, 'CatatanBadanKepegawaian' => 71, 'CatatanLppa' => 72, 'IsBagianHukumSetuju' => 73, 'IsInspektoratSetuju' => 74, 'IsBadanKepegawaianSetuju' => 75, 'IsLppaSetuju' => 76, 'VerifikasiBpkpd' => 77, 'VerifikasiBappeko' => 78, 'VerifikasiPenyelia' => 79, 'VerifikasiBagianHukum' => 80, 'VerifikasiInspektorat' => 81, 'VerifikasiBadanKepegawaian' => 82, 'VerifikasiLppa' => 83, 'MetodeCount' => 84, 'CatatanBagianOrganisasi' => 85, 'IsBagianOrganisasiSetuju' => 86, 'VerifikasiBagianOrganisasi' => 87, 'CatatanAsisten1' => 88, 'IsAsisten1Setuju' => 89, 'VerifikasiAsisten1' => 90, 'CatatanAsisten2' => 91, 'IsAsisten2Setuju' => 92, 'VerifikasiAsisten2' => 93, 'CatatanAsisten3' => 94, 'IsAsisten3Setuju' => 95, 'VerifikasiAsisten3' => 96, 'CatatanSekda' => 97, 'IsSekdaSetuju' => 98, 'VerifikasiSekda' => 99, 'VerifikasiAsisten' => 100, ),
		BasePeer::TYPE_COLNAME => array (MasterKegiatanPeer::UNIT_ID => 0, MasterKegiatanPeer::KODE_KEGIATAN => 1, MasterKegiatanPeer::KODE_BIDANG => 2, MasterKegiatanPeer::KODE_URUSAN_WAJIB => 3, MasterKegiatanPeer::KODE_PROGRAM => 4, MasterKegiatanPeer::KODE_SASARAN => 5, MasterKegiatanPeer::KODE_INDIKATOR => 6, MasterKegiatanPeer::ALOKASI_DANA => 7, MasterKegiatanPeer::NAMA_KEGIATAN => 8, MasterKegiatanPeer::MASUKAN => 9, MasterKegiatanPeer::OUTPUT => 10, MasterKegiatanPeer::OUTCOME => 11, MasterKegiatanPeer::BENEFIT => 12, MasterKegiatanPeer::IMPACT => 13, MasterKegiatanPeer::TIPE => 14, MasterKegiatanPeer::KEGIATAN_ACTIVE => 15, MasterKegiatanPeer::TO_KEGIATAN_CODE => 16, MasterKegiatanPeer::CATATAN => 17, MasterKegiatanPeer::TARGET_OUTCOME => 18, MasterKegiatanPeer::LOKASI => 19, MasterKegiatanPeer::JUMLAH_PREV => 20, MasterKegiatanPeer::JUMLAH_NOW => 21, MasterKegiatanPeer::JUMLAH_NEXT => 22, MasterKegiatanPeer::KODE_PROGRAM2 => 23, MasterKegiatanPeer::KODE_URUSAN => 24, MasterKegiatanPeer::LAST_UPDATE_USER => 25, MasterKegiatanPeer::LAST_UPDATE_TIME => 26, MasterKegiatanPeer::LAST_UPDATE_IP => 27, MasterKegiatanPeer::TAHAP => 28, MasterKegiatanPeer::KODE_MISI => 29, MasterKegiatanPeer::KODE_TUJUAN => 30, MasterKegiatanPeer::RANKING => 31, MasterKegiatanPeer::NOMOR13 => 32, MasterKegiatanPeer::PPA_NAMA => 33, MasterKegiatanPeer::PPA_PANGKAT => 34, MasterKegiatanPeer::PPA_NIP => 35, MasterKegiatanPeer::LANJUTAN => 36, MasterKegiatanPeer::USER_ID => 37, MasterKegiatanPeer::ID => 38, MasterKegiatanPeer::TAHUN => 39, MasterKegiatanPeer::TAMBAHAN_PAGU => 40, MasterKegiatanPeer::GENDER => 41, MasterKegiatanPeer::KODE_KEG_KEUANGAN => 42, MasterKegiatanPeer::USER_ID_LAMA => 43, MasterKegiatanPeer::INDIKATOR => 44, MasterKegiatanPeer::IS_DAK => 45, MasterKegiatanPeer::KODE_KEGIATAN_ASAL => 46, MasterKegiatanPeer::KODE_KEG_KEUANGAN_ASAL => 47, MasterKegiatanPeer::TH_KE_MULTIYEARS => 48, MasterKegiatanPeer::KELOMPOK_SASARAN => 49, MasterKegiatanPeer::PAGU_BAPPEKO => 50, MasterKegiatanPeer::KODE_DPA => 51, MasterKegiatanPeer::USER_ID_PPTK => 52, MasterKegiatanPeer::USER_ID_KPA => 53, MasterKegiatanPeer::CATATAN_PEMBAHASAN => 54, MasterKegiatanPeer::IS_TAPD_SETUJU => 55, MasterKegiatanPeer::IS_BAPPEKO_SETUJU => 56, MasterKegiatanPeer::IS_PENYELIA_SETUJU => 57, MasterKegiatanPeer::IS_PERNAH_RKA => 58, MasterKegiatanPeer::KODE_KEGIATAN_BARU => 59, MasterKegiatanPeer::CATATAN_BPKPD => 60, MasterKegiatanPeer::UBAH_F1_DINAS => 61, MasterKegiatanPeer::UBAH_F1_PENELITI => 62, MasterKegiatanPeer::SISA_LELANG_DINAS => 63, MasterKegiatanPeer::SISA_LELANG_PENELITI => 64, MasterKegiatanPeer::CATATAN_UBAH_F1_DINAS => 65, MasterKegiatanPeer::CATATAN_SISA_LELANG_PENELITI => 66, MasterKegiatanPeer::PPTK_APPROVAL => 67, MasterKegiatanPeer::KPA_APPROVAL => 68, MasterKegiatanPeer::CATATAN_BAGIAN_HUKUM => 69, MasterKegiatanPeer::CATATAN_INSPEKTORAT => 70, MasterKegiatanPeer::CATATAN_BADAN_KEPEGAWAIAN => 71, MasterKegiatanPeer::CATATAN_LPPA => 72, MasterKegiatanPeer::IS_BAGIAN_HUKUM_SETUJU => 73, MasterKegiatanPeer::IS_INSPEKTORAT_SETUJU => 74, MasterKegiatanPeer::IS_BADAN_KEPEGAWAIAN_SETUJU => 75, MasterKegiatanPeer::IS_LPPA_SETUJU => 76, MasterKegiatanPeer::VERIFIKASI_BPKPD => 77, MasterKegiatanPeer::VERIFIKASI_BAPPEKO => 78, MasterKegiatanPeer::VERIFIKASI_PENYELIA => 79, MasterKegiatanPeer::VERIFIKASI_BAGIAN_HUKUM => 80, MasterKegiatanPeer::VERIFIKASI_INSPEKTORAT => 81, MasterKegiatanPeer::VERIFIKASI_BADAN_KEPEGAWAIAN => 82, MasterKegiatanPeer::VERIFIKASI_LPPA => 83, MasterKegiatanPeer::METODE_COUNT => 84, MasterKegiatanPeer::CATATAN_BAGIAN_ORGANISASI => 85, MasterKegiatanPeer::IS_BAGIAN_ORGANISASI_SETUJU => 86, MasterKegiatanPeer::VERIFIKASI_BAGIAN_ORGANISASI => 87, MasterKegiatanPeer::CATATAN_ASISTEN1 => 88, MasterKegiatanPeer::IS_ASISTEN1_SETUJU => 89, MasterKegiatanPeer::VERIFIKASI_ASISTEN1 => 90, MasterKegiatanPeer::CATATAN_ASISTEN2 => 91, MasterKegiatanPeer::IS_ASISTEN2_SETUJU => 92, MasterKegiatanPeer::VERIFIKASI_ASISTEN2 => 93, MasterKegiatanPeer::CATATAN_ASISTEN3 => 94, MasterKegiatanPeer::IS_ASISTEN3_SETUJU => 95, MasterKegiatanPeer::VERIFIKASI_ASISTEN3 => 96, MasterKegiatanPeer::CATATAN_SEKDA => 97, MasterKegiatanPeer::IS_SEKDA_SETUJU => 98, MasterKegiatanPeer::VERIFIKASI_SEKDA => 99, MasterKegiatanPeer::VERIFIKASI_ASISTEN => 100, ),
		BasePeer::TYPE_FIELDNAME => array ('unit_id' => 0, 'kode_kegiatan' => 1, 'kode_bidang' => 2, 'kode_urusan_wajib' => 3, 'kode_program' => 4, 'kode_sasaran' => 5, 'kode_indikator' => 6, 'alokasi_dana' => 7, 'nama_kegiatan' => 8, 'masukan' => 9, 'output' => 10, 'outcome' => 11, 'benefit' => 12, 'impact' => 13, 'tipe' => 14, 'kegiatan_active' => 15, 'to_kegiatan_code' => 16, 'catatan' => 17, 'target_outcome' => 18, 'lokasi' => 19, 'jumlah_prev' => 20, 'jumlah_now' => 21, 'jumlah_next' => 22, 'kode_program2' => 23, 'kode_urusan' => 24, 'last_update_user' => 25, 'last_update_time' => 26, 'last_update_ip' => 27, 'tahap' => 28, 'kode_misi' => 29, 'kode_tujuan' => 30, 'ranking' => 31, 'nomor13' => 32, 'ppa_nama' => 33, 'ppa_pangkat' => 34, 'ppa_nip' => 35, 'lanjutan' => 36, 'user_id' => 37, 'id' => 38, 'tahun' => 39, 'tambahan_pagu' => 40, 'gender' => 41, 'kode_keg_keuangan' => 42, 'user_id_lama' => 43, 'indikator' => 44, 'is_dak' => 45, 'kode_kegiatan_asal' => 46, 'kode_keg_keuangan_asal' => 47, 'th_ke_multiyears' => 48, 'kelompok_sasaran' => 49, 'pagu_bappeko' => 50, 'kode_dpa' => 51, 'user_id_pptk' => 52, 'user_id_kpa' => 53, 'catatan_pembahasan' => 54, 'is_tapd_setuju' => 55, 'is_bappeko_setuju' => 56, 'is_penyelia_setuju' => 57, 'is_pernah_rka' => 58, 'kode_kegiatan_baru' => 59, 'catatan_bpkpd' => 60, 'ubah_f1_dinas' => 61, 'ubah_f1_peneliti' => 62, 'sisa_lelang_dinas' => 63, 'sisa_lelang_peneliti' => 64, 'catatan_ubah_f1_dinas' => 65, 'catatan_sisa_lelang_peneliti' => 66, 'pptk_approval' => 67, 'kpa_approval' => 68, 'catatan_bagian_hukum' => 69, 'catatan_inspektorat' => 70, 'catatan_badan_kepegawaian' => 71, 'catatan_lppa' => 72, 'is_bagian_hukum_setuju' => 73, 'is_inspektorat_setuju' => 74, 'is_badan_kepegawaian_setuju' => 75, 'is_lppa_setuju' => 76, 'verifikasi_bpkpd' => 77, 'verifikasi_bappeko' => 78, 'verifikasi_penyelia' => 79, 'verifikasi_bagian_hukum' => 80, 'verifikasi_inspektorat' => 81, 'verifikasi_badan_kepegawaian' => 82, 'verifikasi_lppa' => 83, 'metode_count' => 84, 'catatan_bagian_organisasi' => 85, 'is_bagian_organisasi_setuju' => 86, 'verifikasi_bagian_organisasi' => 87, 'catatan_asisten1' => 88, 'is_asisten1_setuju' => 89, 'verifikasi_asisten1' => 90, 'catatan_asisten2' => 91, 'is_asisten2_setuju' => 92, 'verifikasi_asisten2' => 93, 'catatan_asisten3' => 94, 'is_asisten3_setuju' => 95, 'verifikasi_asisten3' => 96, 'catatan_sekda' => 97, 'is_sekda_setuju' => 98, 'verifikasi_sekda' => 99, 'verifikasi_asisten' => 100, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/MasterKegiatanMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.MasterKegiatanMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = MasterKegiatanPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(MasterKegiatanPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(MasterKegiatanPeer::UNIT_ID);

		$criteria->addSelectColumn(MasterKegiatanPeer::KODE_KEGIATAN);

		$criteria->addSelectColumn(MasterKegiatanPeer::KODE_BIDANG);

		$criteria->addSelectColumn(MasterKegiatanPeer::KODE_URUSAN_WAJIB);

		$criteria->addSelectColumn(MasterKegiatanPeer::KODE_PROGRAM);

		$criteria->addSelectColumn(MasterKegiatanPeer::KODE_SASARAN);

		$criteria->addSelectColumn(MasterKegiatanPeer::KODE_INDIKATOR);

		$criteria->addSelectColumn(MasterKegiatanPeer::ALOKASI_DANA);

		$criteria->addSelectColumn(MasterKegiatanPeer::NAMA_KEGIATAN);

		$criteria->addSelectColumn(MasterKegiatanPeer::MASUKAN);

		$criteria->addSelectColumn(MasterKegiatanPeer::OUTPUT);

		$criteria->addSelectColumn(MasterKegiatanPeer::OUTCOME);

		$criteria->addSelectColumn(MasterKegiatanPeer::BENEFIT);

		$criteria->addSelectColumn(MasterKegiatanPeer::IMPACT);

		$criteria->addSelectColumn(MasterKegiatanPeer::TIPE);

		$criteria->addSelectColumn(MasterKegiatanPeer::KEGIATAN_ACTIVE);

		$criteria->addSelectColumn(MasterKegiatanPeer::TO_KEGIATAN_CODE);

		$criteria->addSelectColumn(MasterKegiatanPeer::CATATAN);

		$criteria->addSelectColumn(MasterKegiatanPeer::TARGET_OUTCOME);

		$criteria->addSelectColumn(MasterKegiatanPeer::LOKASI);

		$criteria->addSelectColumn(MasterKegiatanPeer::JUMLAH_PREV);

		$criteria->addSelectColumn(MasterKegiatanPeer::JUMLAH_NOW);

		$criteria->addSelectColumn(MasterKegiatanPeer::JUMLAH_NEXT);

		$criteria->addSelectColumn(MasterKegiatanPeer::KODE_PROGRAM2);

		$criteria->addSelectColumn(MasterKegiatanPeer::KODE_URUSAN);

		$criteria->addSelectColumn(MasterKegiatanPeer::LAST_UPDATE_USER);

		$criteria->addSelectColumn(MasterKegiatanPeer::LAST_UPDATE_TIME);

		$criteria->addSelectColumn(MasterKegiatanPeer::LAST_UPDATE_IP);

		$criteria->addSelectColumn(MasterKegiatanPeer::TAHAP);

		$criteria->addSelectColumn(MasterKegiatanPeer::KODE_MISI);

		$criteria->addSelectColumn(MasterKegiatanPeer::KODE_TUJUAN);

		$criteria->addSelectColumn(MasterKegiatanPeer::RANKING);

		$criteria->addSelectColumn(MasterKegiatanPeer::NOMOR13);

		$criteria->addSelectColumn(MasterKegiatanPeer::PPA_NAMA);

		$criteria->addSelectColumn(MasterKegiatanPeer::PPA_PANGKAT);

		$criteria->addSelectColumn(MasterKegiatanPeer::PPA_NIP);

		$criteria->addSelectColumn(MasterKegiatanPeer::LANJUTAN);

		$criteria->addSelectColumn(MasterKegiatanPeer::USER_ID);

		$criteria->addSelectColumn(MasterKegiatanPeer::ID);

		$criteria->addSelectColumn(MasterKegiatanPeer::TAHUN);

		$criteria->addSelectColumn(MasterKegiatanPeer::TAMBAHAN_PAGU);

		$criteria->addSelectColumn(MasterKegiatanPeer::GENDER);

		$criteria->addSelectColumn(MasterKegiatanPeer::KODE_KEG_KEUANGAN);

		$criteria->addSelectColumn(MasterKegiatanPeer::USER_ID_LAMA);

		$criteria->addSelectColumn(MasterKegiatanPeer::INDIKATOR);

		$criteria->addSelectColumn(MasterKegiatanPeer::IS_DAK);

		$criteria->addSelectColumn(MasterKegiatanPeer::KODE_KEGIATAN_ASAL);

		$criteria->addSelectColumn(MasterKegiatanPeer::KODE_KEG_KEUANGAN_ASAL);

		$criteria->addSelectColumn(MasterKegiatanPeer::TH_KE_MULTIYEARS);

		$criteria->addSelectColumn(MasterKegiatanPeer::KELOMPOK_SASARAN);

		$criteria->addSelectColumn(MasterKegiatanPeer::PAGU_BAPPEKO);

		$criteria->addSelectColumn(MasterKegiatanPeer::KODE_DPA);

		$criteria->addSelectColumn(MasterKegiatanPeer::USER_ID_PPTK);

		$criteria->addSelectColumn(MasterKegiatanPeer::USER_ID_KPA);

		$criteria->addSelectColumn(MasterKegiatanPeer::CATATAN_PEMBAHASAN);

		$criteria->addSelectColumn(MasterKegiatanPeer::IS_TAPD_SETUJU);

		$criteria->addSelectColumn(MasterKegiatanPeer::IS_BAPPEKO_SETUJU);

		$criteria->addSelectColumn(MasterKegiatanPeer::IS_PENYELIA_SETUJU);

		$criteria->addSelectColumn(MasterKegiatanPeer::IS_PERNAH_RKA);

		$criteria->addSelectColumn(MasterKegiatanPeer::KODE_KEGIATAN_BARU);

		$criteria->addSelectColumn(MasterKegiatanPeer::CATATAN_BPKPD);

		$criteria->addSelectColumn(MasterKegiatanPeer::UBAH_F1_DINAS);

		$criteria->addSelectColumn(MasterKegiatanPeer::UBAH_F1_PENELITI);

		$criteria->addSelectColumn(MasterKegiatanPeer::SISA_LELANG_DINAS);

		$criteria->addSelectColumn(MasterKegiatanPeer::SISA_LELANG_PENELITI);

		$criteria->addSelectColumn(MasterKegiatanPeer::CATATAN_UBAH_F1_DINAS);

		$criteria->addSelectColumn(MasterKegiatanPeer::CATATAN_SISA_LELANG_PENELITI);

		$criteria->addSelectColumn(MasterKegiatanPeer::PPTK_APPROVAL);

		$criteria->addSelectColumn(MasterKegiatanPeer::KPA_APPROVAL);

		$criteria->addSelectColumn(MasterKegiatanPeer::CATATAN_BAGIAN_HUKUM);

		$criteria->addSelectColumn(MasterKegiatanPeer::CATATAN_INSPEKTORAT);

		$criteria->addSelectColumn(MasterKegiatanPeer::CATATAN_BADAN_KEPEGAWAIAN);

		$criteria->addSelectColumn(MasterKegiatanPeer::CATATAN_LPPA);

		$criteria->addSelectColumn(MasterKegiatanPeer::IS_BAGIAN_HUKUM_SETUJU);

		$criteria->addSelectColumn(MasterKegiatanPeer::IS_INSPEKTORAT_SETUJU);

		$criteria->addSelectColumn(MasterKegiatanPeer::IS_BADAN_KEPEGAWAIAN_SETUJU);

		$criteria->addSelectColumn(MasterKegiatanPeer::IS_LPPA_SETUJU);

		$criteria->addSelectColumn(MasterKegiatanPeer::VERIFIKASI_BPKPD);

		$criteria->addSelectColumn(MasterKegiatanPeer::VERIFIKASI_BAPPEKO);

		$criteria->addSelectColumn(MasterKegiatanPeer::VERIFIKASI_PENYELIA);

		$criteria->addSelectColumn(MasterKegiatanPeer::VERIFIKASI_BAGIAN_HUKUM);

		$criteria->addSelectColumn(MasterKegiatanPeer::VERIFIKASI_INSPEKTORAT);

		$criteria->addSelectColumn(MasterKegiatanPeer::VERIFIKASI_BADAN_KEPEGAWAIAN);

		$criteria->addSelectColumn(MasterKegiatanPeer::VERIFIKASI_LPPA);

		$criteria->addSelectColumn(MasterKegiatanPeer::METODE_COUNT);

		$criteria->addSelectColumn(MasterKegiatanPeer::CATATAN_BAGIAN_ORGANISASI);

		$criteria->addSelectColumn(MasterKegiatanPeer::IS_BAGIAN_ORGANISASI_SETUJU);

		$criteria->addSelectColumn(MasterKegiatanPeer::VERIFIKASI_BAGIAN_ORGANISASI);

		$criteria->addSelectColumn(MasterKegiatanPeer::CATATAN_ASISTEN1);

		$criteria->addSelectColumn(MasterKegiatanPeer::IS_ASISTEN1_SETUJU);

		$criteria->addSelectColumn(MasterKegiatanPeer::VERIFIKASI_ASISTEN1);

		$criteria->addSelectColumn(MasterKegiatanPeer::CATATAN_ASISTEN2);

		$criteria->addSelectColumn(MasterKegiatanPeer::IS_ASISTEN2_SETUJU);

		$criteria->addSelectColumn(MasterKegiatanPeer::VERIFIKASI_ASISTEN2);

		$criteria->addSelectColumn(MasterKegiatanPeer::CATATAN_ASISTEN3);

		$criteria->addSelectColumn(MasterKegiatanPeer::IS_ASISTEN3_SETUJU);

		$criteria->addSelectColumn(MasterKegiatanPeer::VERIFIKASI_ASISTEN3);

		$criteria->addSelectColumn(MasterKegiatanPeer::CATATAN_SEKDA);

		$criteria->addSelectColumn(MasterKegiatanPeer::IS_SEKDA_SETUJU);

		$criteria->addSelectColumn(MasterKegiatanPeer::VERIFIKASI_SEKDA);

		$criteria->addSelectColumn(MasterKegiatanPeer::VERIFIKASI_ASISTEN);

	}

	const COUNT = 'COUNT(ebudget.master_kegiatan.UNIT_ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.master_kegiatan.UNIT_ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(MasterKegiatanPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(MasterKegiatanPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = MasterKegiatanPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = MasterKegiatanPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return MasterKegiatanPeer::populateObjects(MasterKegiatanPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			MasterKegiatanPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = MasterKegiatanPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return MasterKegiatanPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}

		$criteria->remove(MasterKegiatanPeer::ID); 

				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(MasterKegiatanPeer::UNIT_ID);
			$selectCriteria->add(MasterKegiatanPeer::UNIT_ID, $criteria->remove(MasterKegiatanPeer::UNIT_ID), $comparison);

			$comparison = $criteria->getComparison(MasterKegiatanPeer::KODE_KEGIATAN);
			$selectCriteria->add(MasterKegiatanPeer::KODE_KEGIATAN, $criteria->remove(MasterKegiatanPeer::KODE_KEGIATAN), $comparison);

			$comparison = $criteria->getComparison(MasterKegiatanPeer::ID);
			$selectCriteria->add(MasterKegiatanPeer::ID, $criteria->remove(MasterKegiatanPeer::ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(MasterKegiatanPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(MasterKegiatanPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof MasterKegiatan) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
												if(count($values) == count($values, COUNT_RECURSIVE))
			{
								$values = array($values);
			}
			$vals = array();
			foreach($values as $value)
			{

				$vals[0][] = $value[0];
				$vals[1][] = $value[1];
				$vals[2][] = $value[2];
			}

			$criteria->add(MasterKegiatanPeer::UNIT_ID, $vals[0], Criteria::IN);
			$criteria->add(MasterKegiatanPeer::KODE_KEGIATAN, $vals[1], Criteria::IN);
			$criteria->add(MasterKegiatanPeer::ID, $vals[2], Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(MasterKegiatan $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(MasterKegiatanPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(MasterKegiatanPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(MasterKegiatanPeer::DATABASE_NAME, MasterKegiatanPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = MasterKegiatanPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK( $unit_id, $kode_kegiatan, $id, $con = null) {
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$criteria = new Criteria();
		$criteria->add(MasterKegiatanPeer::UNIT_ID, $unit_id);
		$criteria->add(MasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
		$criteria->add(MasterKegiatanPeer::ID, $id);
		$v = MasterKegiatanPeer::doSelect($criteria, $con);

		return !empty($v) ? $v[0] : null;
	}
} 
if (Propel::isInit()) {
			try {
		BaseMasterKegiatanPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/MasterKegiatanMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.MasterKegiatanMapBuilder');
}
