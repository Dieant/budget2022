<?php


abstract class BaseWaitingListPU extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id_waiting;


	
	protected $unit_id;


	
	protected $kegiatan_code;


	
	protected $subtitle;


	
	protected $komponen_id;


	
	protected $komponen_name;


	
	protected $komponen_lokasi;


	
	protected $komponen_harga_awal;


	
	protected $pajak;


	
	protected $komponen_satuan;


	
	protected $komponen_rekening;


	
	protected $koefisien;


	
	protected $volume;


	
	protected $nilai_anggaran;


	
	protected $tahun_input;


	
	protected $created_at;


	
	protected $updated_at;


	
	protected $status_hapus;


	
	protected $status_waiting;


	
	protected $prioritas;


	
	protected $kode_rka;


	
	protected $user_pengambil;


	
	protected $nilai_ee;


	
	protected $keterangan;


	
	protected $kode_jasmas;


	
	protected $kecamatan;


	
	protected $kelurahan;


	
	protected $is_musrenbang;


	
	protected $nilai_anggaran_semula;


	
	protected $metode_waitinglist;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getIdWaiting()
	{

		return $this->id_waiting;
	}

	
	public function getUnitId()
	{

		return $this->unit_id;
	}

	
	public function getKegiatanCode()
	{

		return $this->kegiatan_code;
	}

	
	public function getSubtitle()
	{

		return $this->subtitle;
	}

	
	public function getKomponenId()
	{

		return $this->komponen_id;
	}

	
	public function getKomponenName()
	{

		return $this->komponen_name;
	}

	
	public function getKomponenLokasi()
	{

		return $this->komponen_lokasi;
	}

	
	public function getKomponenHargaAwal()
	{

		return $this->komponen_harga_awal;
	}

	
	public function getPajak()
	{

		return $this->pajak;
	}

	
	public function getKomponenSatuan()
	{

		return $this->komponen_satuan;
	}

	
	public function getKomponenRekening()
	{

		return $this->komponen_rekening;
	}

	
	public function getKoefisien()
	{

		return $this->koefisien;
	}

	
	public function getVolume()
	{

		return $this->volume;
	}

	
	public function getNilaiAnggaran()
	{

		return $this->nilai_anggaran;
	}

	
	public function getTahunInput()
	{

		return $this->tahun_input;
	}

	
	public function getCreatedAt($format = 'Y-m-d H:i:s')
	{

		if ($this->created_at === null || $this->created_at === '') {
			return null;
		} elseif (!is_int($this->created_at)) {
						$ts = strtotime($this->created_at);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [created_at] as date/time value: " . var_export($this->created_at, true));
			}
		} else {
			$ts = $this->created_at;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getUpdatedAt($format = 'Y-m-d H:i:s')
	{

		if ($this->updated_at === null || $this->updated_at === '') {
			return null;
		} elseif (!is_int($this->updated_at)) {
						$ts = strtotime($this->updated_at);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [updated_at] as date/time value: " . var_export($this->updated_at, true));
			}
		} else {
			$ts = $this->updated_at;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getStatusHapus()
	{

		return $this->status_hapus;
	}

	
	public function getStatusWaiting()
	{

		return $this->status_waiting;
	}

	
	public function getPrioritas()
	{

		return $this->prioritas;
	}

	
	public function getKodeRka()
	{

		return $this->kode_rka;
	}

	
	public function getUserPengambil()
	{

		return $this->user_pengambil;
	}

	
	public function getNilaiEe()
	{

		return $this->nilai_ee;
	}

	
	public function getKeterangan()
	{

		return $this->keterangan;
	}

	
	public function getKodeJasmas()
	{

		return $this->kode_jasmas;
	}

	
	public function getKecamatan()
	{

		return $this->kecamatan;
	}

	
	public function getKelurahan()
	{

		return $this->kelurahan;
	}

	
	public function getIsMusrenbang()
	{

		return $this->is_musrenbang;
	}

	
	public function getNilaiAnggaranSemula()
	{

		return $this->nilai_anggaran_semula;
	}

	
	public function getMetodeWaitinglist()
	{

		return $this->metode_waitinglist;
	}

	
	public function setIdWaiting($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id_waiting !== $v) {
			$this->id_waiting = $v;
			$this->modifiedColumns[] = WaitingListPUPeer::ID_WAITING;
		}

	} 
	
	public function setUnitId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->unit_id !== $v) {
			$this->unit_id = $v;
			$this->modifiedColumns[] = WaitingListPUPeer::UNIT_ID;
		}

	} 
	
	public function setKegiatanCode($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kegiatan_code !== $v) {
			$this->kegiatan_code = $v;
			$this->modifiedColumns[] = WaitingListPUPeer::KEGIATAN_CODE;
		}

	} 
	
	public function setSubtitle($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->subtitle !== $v) {
			$this->subtitle = $v;
			$this->modifiedColumns[] = WaitingListPUPeer::SUBTITLE;
		}

	} 
	
	public function setKomponenId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->komponen_id !== $v) {
			$this->komponen_id = $v;
			$this->modifiedColumns[] = WaitingListPUPeer::KOMPONEN_ID;
		}

	} 
	
	public function setKomponenName($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->komponen_name !== $v) {
			$this->komponen_name = $v;
			$this->modifiedColumns[] = WaitingListPUPeer::KOMPONEN_NAME;
		}

	} 
	
	public function setKomponenLokasi($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->komponen_lokasi !== $v) {
			$this->komponen_lokasi = $v;
			$this->modifiedColumns[] = WaitingListPUPeer::KOMPONEN_LOKASI;
		}

	} 
	
	public function setKomponenHargaAwal($v)
	{

		if ($this->komponen_harga_awal !== $v) {
			$this->komponen_harga_awal = $v;
			$this->modifiedColumns[] = WaitingListPUPeer::KOMPONEN_HARGA_AWAL;
		}

	} 
	
	public function setPajak($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->pajak !== $v) {
			$this->pajak = $v;
			$this->modifiedColumns[] = WaitingListPUPeer::PAJAK;
		}

	} 
	
	public function setKomponenSatuan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->komponen_satuan !== $v) {
			$this->komponen_satuan = $v;
			$this->modifiedColumns[] = WaitingListPUPeer::KOMPONEN_SATUAN;
		}

	} 
	
	public function setKomponenRekening($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->komponen_rekening !== $v) {
			$this->komponen_rekening = $v;
			$this->modifiedColumns[] = WaitingListPUPeer::KOMPONEN_REKENING;
		}

	} 
	
	public function setKoefisien($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->koefisien !== $v) {
			$this->koefisien = $v;
			$this->modifiedColumns[] = WaitingListPUPeer::KOEFISIEN;
		}

	} 
	
	public function setVolume($v)
	{

		if ($this->volume !== $v) {
			$this->volume = $v;
			$this->modifiedColumns[] = WaitingListPUPeer::VOLUME;
		}

	} 
	
	public function setNilaiAnggaran($v)
	{

		if ($this->nilai_anggaran !== $v) {
			$this->nilai_anggaran = $v;
			$this->modifiedColumns[] = WaitingListPUPeer::NILAI_ANGGARAN;
		}

	} 
	
	public function setTahunInput($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->tahun_input !== $v) {
			$this->tahun_input = $v;
			$this->modifiedColumns[] = WaitingListPUPeer::TAHUN_INPUT;
		}

	} 
	
	public function setCreatedAt($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [created_at] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->created_at !== $ts) {
			$this->created_at = $ts;
			$this->modifiedColumns[] = WaitingListPUPeer::CREATED_AT;
		}

	} 
	
	public function setUpdatedAt($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [updated_at] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->updated_at !== $ts) {
			$this->updated_at = $ts;
			$this->modifiedColumns[] = WaitingListPUPeer::UPDATED_AT;
		}

	} 
	
	public function setStatusHapus($v)
	{

		if ($this->status_hapus !== $v) {
			$this->status_hapus = $v;
			$this->modifiedColumns[] = WaitingListPUPeer::STATUS_HAPUS;
		}

	} 
	
	public function setStatusWaiting($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->status_waiting !== $v) {
			$this->status_waiting = $v;
			$this->modifiedColumns[] = WaitingListPUPeer::STATUS_WAITING;
		}

	} 
	
	public function setPrioritas($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->prioritas !== $v) {
			$this->prioritas = $v;
			$this->modifiedColumns[] = WaitingListPUPeer::PRIORITAS;
		}

	} 
	
	public function setKodeRka($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_rka !== $v) {
			$this->kode_rka = $v;
			$this->modifiedColumns[] = WaitingListPUPeer::KODE_RKA;
		}

	} 
	
	public function setUserPengambil($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->user_pengambil !== $v) {
			$this->user_pengambil = $v;
			$this->modifiedColumns[] = WaitingListPUPeer::USER_PENGAMBIL;
		}

	} 
	
	public function setNilaiEe($v)
	{

		if ($this->nilai_ee !== $v) {
			$this->nilai_ee = $v;
			$this->modifiedColumns[] = WaitingListPUPeer::NILAI_EE;
		}

	} 
	
	public function setKeterangan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->keterangan !== $v) {
			$this->keterangan = $v;
			$this->modifiedColumns[] = WaitingListPUPeer::KETERANGAN;
		}

	} 
	
	public function setKodeJasmas($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_jasmas !== $v) {
			$this->kode_jasmas = $v;
			$this->modifiedColumns[] = WaitingListPUPeer::KODE_JASMAS;
		}

	} 
	
	public function setKecamatan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kecamatan !== $v) {
			$this->kecamatan = $v;
			$this->modifiedColumns[] = WaitingListPUPeer::KECAMATAN;
		}

	} 
	
	public function setKelurahan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kelurahan !== $v) {
			$this->kelurahan = $v;
			$this->modifiedColumns[] = WaitingListPUPeer::KELURAHAN;
		}

	} 
	
	public function setIsMusrenbang($v)
	{

		if ($this->is_musrenbang !== $v) {
			$this->is_musrenbang = $v;
			$this->modifiedColumns[] = WaitingListPUPeer::IS_MUSRENBANG;
		}

	} 
	
	public function setNilaiAnggaranSemula($v)
	{

		if ($this->nilai_anggaran_semula !== $v) {
			$this->nilai_anggaran_semula = $v;
			$this->modifiedColumns[] = WaitingListPUPeer::NILAI_ANGGARAN_SEMULA;
		}

	} 
	
	public function setMetodeWaitinglist($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->metode_waitinglist !== $v) {
			$this->metode_waitinglist = $v;
			$this->modifiedColumns[] = WaitingListPUPeer::METODE_WAITINGLIST;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id_waiting = $rs->getInt($startcol + 0);

			$this->unit_id = $rs->getString($startcol + 1);

			$this->kegiatan_code = $rs->getString($startcol + 2);

			$this->subtitle = $rs->getString($startcol + 3);

			$this->komponen_id = $rs->getString($startcol + 4);

			$this->komponen_name = $rs->getString($startcol + 5);

			$this->komponen_lokasi = $rs->getString($startcol + 6);

			$this->komponen_harga_awal = $rs->getFloat($startcol + 7);

			$this->pajak = $rs->getInt($startcol + 8);

			$this->komponen_satuan = $rs->getString($startcol + 9);

			$this->komponen_rekening = $rs->getString($startcol + 10);

			$this->koefisien = $rs->getString($startcol + 11);

			$this->volume = $rs->getFloat($startcol + 12);

			$this->nilai_anggaran = $rs->getFloat($startcol + 13);

			$this->tahun_input = $rs->getString($startcol + 14);

			$this->created_at = $rs->getTimestamp($startcol + 15, null);

			$this->updated_at = $rs->getTimestamp($startcol + 16, null);

			$this->status_hapus = $rs->getBoolean($startcol + 17);

			$this->status_waiting = $rs->getInt($startcol + 18);

			$this->prioritas = $rs->getInt($startcol + 19);

			$this->kode_rka = $rs->getString($startcol + 20);

			$this->user_pengambil = $rs->getString($startcol + 21);

			$this->nilai_ee = $rs->getFloat($startcol + 22);

			$this->keterangan = $rs->getString($startcol + 23);

			$this->kode_jasmas = $rs->getString($startcol + 24);

			$this->kecamatan = $rs->getString($startcol + 25);

			$this->kelurahan = $rs->getString($startcol + 26);

			$this->is_musrenbang = $rs->getBoolean($startcol + 27);

			$this->nilai_anggaran_semula = $rs->getFloat($startcol + 28);

			$this->metode_waitinglist = $rs->getInt($startcol + 29);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 30; 
		} catch (Exception $e) {
			throw new PropelException("Error populating WaitingListPU object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(WaitingListPUPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			WaitingListPUPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
    if ($this->isNew() && !$this->isColumnModified(WaitingListPUPeer::CREATED_AT))
    {
      $this->setCreatedAt(time());
    }

    if ($this->isModified() && !$this->isColumnModified(WaitingListPUPeer::UPDATED_AT))
    {
      $this->setUpdatedAt(time());
    }

		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(WaitingListPUPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = WaitingListPUPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setIdWaiting($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += WaitingListPUPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = WaitingListPUPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = WaitingListPUPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getIdWaiting();
				break;
			case 1:
				return $this->getUnitId();
				break;
			case 2:
				return $this->getKegiatanCode();
				break;
			case 3:
				return $this->getSubtitle();
				break;
			case 4:
				return $this->getKomponenId();
				break;
			case 5:
				return $this->getKomponenName();
				break;
			case 6:
				return $this->getKomponenLokasi();
				break;
			case 7:
				return $this->getKomponenHargaAwal();
				break;
			case 8:
				return $this->getPajak();
				break;
			case 9:
				return $this->getKomponenSatuan();
				break;
			case 10:
				return $this->getKomponenRekening();
				break;
			case 11:
				return $this->getKoefisien();
				break;
			case 12:
				return $this->getVolume();
				break;
			case 13:
				return $this->getNilaiAnggaran();
				break;
			case 14:
				return $this->getTahunInput();
				break;
			case 15:
				return $this->getCreatedAt();
				break;
			case 16:
				return $this->getUpdatedAt();
				break;
			case 17:
				return $this->getStatusHapus();
				break;
			case 18:
				return $this->getStatusWaiting();
				break;
			case 19:
				return $this->getPrioritas();
				break;
			case 20:
				return $this->getKodeRka();
				break;
			case 21:
				return $this->getUserPengambil();
				break;
			case 22:
				return $this->getNilaiEe();
				break;
			case 23:
				return $this->getKeterangan();
				break;
			case 24:
				return $this->getKodeJasmas();
				break;
			case 25:
				return $this->getKecamatan();
				break;
			case 26:
				return $this->getKelurahan();
				break;
			case 27:
				return $this->getIsMusrenbang();
				break;
			case 28:
				return $this->getNilaiAnggaranSemula();
				break;
			case 29:
				return $this->getMetodeWaitinglist();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = WaitingListPUPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getIdWaiting(),
			$keys[1] => $this->getUnitId(),
			$keys[2] => $this->getKegiatanCode(),
			$keys[3] => $this->getSubtitle(),
			$keys[4] => $this->getKomponenId(),
			$keys[5] => $this->getKomponenName(),
			$keys[6] => $this->getKomponenLokasi(),
			$keys[7] => $this->getKomponenHargaAwal(),
			$keys[8] => $this->getPajak(),
			$keys[9] => $this->getKomponenSatuan(),
			$keys[10] => $this->getKomponenRekening(),
			$keys[11] => $this->getKoefisien(),
			$keys[12] => $this->getVolume(),
			$keys[13] => $this->getNilaiAnggaran(),
			$keys[14] => $this->getTahunInput(),
			$keys[15] => $this->getCreatedAt(),
			$keys[16] => $this->getUpdatedAt(),
			$keys[17] => $this->getStatusHapus(),
			$keys[18] => $this->getStatusWaiting(),
			$keys[19] => $this->getPrioritas(),
			$keys[20] => $this->getKodeRka(),
			$keys[21] => $this->getUserPengambil(),
			$keys[22] => $this->getNilaiEe(),
			$keys[23] => $this->getKeterangan(),
			$keys[24] => $this->getKodeJasmas(),
			$keys[25] => $this->getKecamatan(),
			$keys[26] => $this->getKelurahan(),
			$keys[27] => $this->getIsMusrenbang(),
			$keys[28] => $this->getNilaiAnggaranSemula(),
			$keys[29] => $this->getMetodeWaitinglist(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = WaitingListPUPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setIdWaiting($value);
				break;
			case 1:
				$this->setUnitId($value);
				break;
			case 2:
				$this->setKegiatanCode($value);
				break;
			case 3:
				$this->setSubtitle($value);
				break;
			case 4:
				$this->setKomponenId($value);
				break;
			case 5:
				$this->setKomponenName($value);
				break;
			case 6:
				$this->setKomponenLokasi($value);
				break;
			case 7:
				$this->setKomponenHargaAwal($value);
				break;
			case 8:
				$this->setPajak($value);
				break;
			case 9:
				$this->setKomponenSatuan($value);
				break;
			case 10:
				$this->setKomponenRekening($value);
				break;
			case 11:
				$this->setKoefisien($value);
				break;
			case 12:
				$this->setVolume($value);
				break;
			case 13:
				$this->setNilaiAnggaran($value);
				break;
			case 14:
				$this->setTahunInput($value);
				break;
			case 15:
				$this->setCreatedAt($value);
				break;
			case 16:
				$this->setUpdatedAt($value);
				break;
			case 17:
				$this->setStatusHapus($value);
				break;
			case 18:
				$this->setStatusWaiting($value);
				break;
			case 19:
				$this->setPrioritas($value);
				break;
			case 20:
				$this->setKodeRka($value);
				break;
			case 21:
				$this->setUserPengambil($value);
				break;
			case 22:
				$this->setNilaiEe($value);
				break;
			case 23:
				$this->setKeterangan($value);
				break;
			case 24:
				$this->setKodeJasmas($value);
				break;
			case 25:
				$this->setKecamatan($value);
				break;
			case 26:
				$this->setKelurahan($value);
				break;
			case 27:
				$this->setIsMusrenbang($value);
				break;
			case 28:
				$this->setNilaiAnggaranSemula($value);
				break;
			case 29:
				$this->setMetodeWaitinglist($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = WaitingListPUPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setIdWaiting($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setUnitId($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setKegiatanCode($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setSubtitle($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setKomponenId($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setKomponenName($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setKomponenLokasi($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setKomponenHargaAwal($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setPajak($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setKomponenSatuan($arr[$keys[9]]);
		if (array_key_exists($keys[10], $arr)) $this->setKomponenRekening($arr[$keys[10]]);
		if (array_key_exists($keys[11], $arr)) $this->setKoefisien($arr[$keys[11]]);
		if (array_key_exists($keys[12], $arr)) $this->setVolume($arr[$keys[12]]);
		if (array_key_exists($keys[13], $arr)) $this->setNilaiAnggaran($arr[$keys[13]]);
		if (array_key_exists($keys[14], $arr)) $this->setTahunInput($arr[$keys[14]]);
		if (array_key_exists($keys[15], $arr)) $this->setCreatedAt($arr[$keys[15]]);
		if (array_key_exists($keys[16], $arr)) $this->setUpdatedAt($arr[$keys[16]]);
		if (array_key_exists($keys[17], $arr)) $this->setStatusHapus($arr[$keys[17]]);
		if (array_key_exists($keys[18], $arr)) $this->setStatusWaiting($arr[$keys[18]]);
		if (array_key_exists($keys[19], $arr)) $this->setPrioritas($arr[$keys[19]]);
		if (array_key_exists($keys[20], $arr)) $this->setKodeRka($arr[$keys[20]]);
		if (array_key_exists($keys[21], $arr)) $this->setUserPengambil($arr[$keys[21]]);
		if (array_key_exists($keys[22], $arr)) $this->setNilaiEe($arr[$keys[22]]);
		if (array_key_exists($keys[23], $arr)) $this->setKeterangan($arr[$keys[23]]);
		if (array_key_exists($keys[24], $arr)) $this->setKodeJasmas($arr[$keys[24]]);
		if (array_key_exists($keys[25], $arr)) $this->setKecamatan($arr[$keys[25]]);
		if (array_key_exists($keys[26], $arr)) $this->setKelurahan($arr[$keys[26]]);
		if (array_key_exists($keys[27], $arr)) $this->setIsMusrenbang($arr[$keys[27]]);
		if (array_key_exists($keys[28], $arr)) $this->setNilaiAnggaranSemula($arr[$keys[28]]);
		if (array_key_exists($keys[29], $arr)) $this->setMetodeWaitinglist($arr[$keys[29]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(WaitingListPUPeer::DATABASE_NAME);

		if ($this->isColumnModified(WaitingListPUPeer::ID_WAITING)) $criteria->add(WaitingListPUPeer::ID_WAITING, $this->id_waiting);
		if ($this->isColumnModified(WaitingListPUPeer::UNIT_ID)) $criteria->add(WaitingListPUPeer::UNIT_ID, $this->unit_id);
		if ($this->isColumnModified(WaitingListPUPeer::KEGIATAN_CODE)) $criteria->add(WaitingListPUPeer::KEGIATAN_CODE, $this->kegiatan_code);
		if ($this->isColumnModified(WaitingListPUPeer::SUBTITLE)) $criteria->add(WaitingListPUPeer::SUBTITLE, $this->subtitle);
		if ($this->isColumnModified(WaitingListPUPeer::KOMPONEN_ID)) $criteria->add(WaitingListPUPeer::KOMPONEN_ID, $this->komponen_id);
		if ($this->isColumnModified(WaitingListPUPeer::KOMPONEN_NAME)) $criteria->add(WaitingListPUPeer::KOMPONEN_NAME, $this->komponen_name);
		if ($this->isColumnModified(WaitingListPUPeer::KOMPONEN_LOKASI)) $criteria->add(WaitingListPUPeer::KOMPONEN_LOKASI, $this->komponen_lokasi);
		if ($this->isColumnModified(WaitingListPUPeer::KOMPONEN_HARGA_AWAL)) $criteria->add(WaitingListPUPeer::KOMPONEN_HARGA_AWAL, $this->komponen_harga_awal);
		if ($this->isColumnModified(WaitingListPUPeer::PAJAK)) $criteria->add(WaitingListPUPeer::PAJAK, $this->pajak);
		if ($this->isColumnModified(WaitingListPUPeer::KOMPONEN_SATUAN)) $criteria->add(WaitingListPUPeer::KOMPONEN_SATUAN, $this->komponen_satuan);
		if ($this->isColumnModified(WaitingListPUPeer::KOMPONEN_REKENING)) $criteria->add(WaitingListPUPeer::KOMPONEN_REKENING, $this->komponen_rekening);
		if ($this->isColumnModified(WaitingListPUPeer::KOEFISIEN)) $criteria->add(WaitingListPUPeer::KOEFISIEN, $this->koefisien);
		if ($this->isColumnModified(WaitingListPUPeer::VOLUME)) $criteria->add(WaitingListPUPeer::VOLUME, $this->volume);
		if ($this->isColumnModified(WaitingListPUPeer::NILAI_ANGGARAN)) $criteria->add(WaitingListPUPeer::NILAI_ANGGARAN, $this->nilai_anggaran);
		if ($this->isColumnModified(WaitingListPUPeer::TAHUN_INPUT)) $criteria->add(WaitingListPUPeer::TAHUN_INPUT, $this->tahun_input);
		if ($this->isColumnModified(WaitingListPUPeer::CREATED_AT)) $criteria->add(WaitingListPUPeer::CREATED_AT, $this->created_at);
		if ($this->isColumnModified(WaitingListPUPeer::UPDATED_AT)) $criteria->add(WaitingListPUPeer::UPDATED_AT, $this->updated_at);
		if ($this->isColumnModified(WaitingListPUPeer::STATUS_HAPUS)) $criteria->add(WaitingListPUPeer::STATUS_HAPUS, $this->status_hapus);
		if ($this->isColumnModified(WaitingListPUPeer::STATUS_WAITING)) $criteria->add(WaitingListPUPeer::STATUS_WAITING, $this->status_waiting);
		if ($this->isColumnModified(WaitingListPUPeer::PRIORITAS)) $criteria->add(WaitingListPUPeer::PRIORITAS, $this->prioritas);
		if ($this->isColumnModified(WaitingListPUPeer::KODE_RKA)) $criteria->add(WaitingListPUPeer::KODE_RKA, $this->kode_rka);
		if ($this->isColumnModified(WaitingListPUPeer::USER_PENGAMBIL)) $criteria->add(WaitingListPUPeer::USER_PENGAMBIL, $this->user_pengambil);
		if ($this->isColumnModified(WaitingListPUPeer::NILAI_EE)) $criteria->add(WaitingListPUPeer::NILAI_EE, $this->nilai_ee);
		if ($this->isColumnModified(WaitingListPUPeer::KETERANGAN)) $criteria->add(WaitingListPUPeer::KETERANGAN, $this->keterangan);
		if ($this->isColumnModified(WaitingListPUPeer::KODE_JASMAS)) $criteria->add(WaitingListPUPeer::KODE_JASMAS, $this->kode_jasmas);
		if ($this->isColumnModified(WaitingListPUPeer::KECAMATAN)) $criteria->add(WaitingListPUPeer::KECAMATAN, $this->kecamatan);
		if ($this->isColumnModified(WaitingListPUPeer::KELURAHAN)) $criteria->add(WaitingListPUPeer::KELURAHAN, $this->kelurahan);
		if ($this->isColumnModified(WaitingListPUPeer::IS_MUSRENBANG)) $criteria->add(WaitingListPUPeer::IS_MUSRENBANG, $this->is_musrenbang);
		if ($this->isColumnModified(WaitingListPUPeer::NILAI_ANGGARAN_SEMULA)) $criteria->add(WaitingListPUPeer::NILAI_ANGGARAN_SEMULA, $this->nilai_anggaran_semula);
		if ($this->isColumnModified(WaitingListPUPeer::METODE_WAITINGLIST)) $criteria->add(WaitingListPUPeer::METODE_WAITINGLIST, $this->metode_waitinglist);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(WaitingListPUPeer::DATABASE_NAME);

		$criteria->add(WaitingListPUPeer::ID_WAITING, $this->id_waiting);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getIdWaiting();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setIdWaiting($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setUnitId($this->unit_id);

		$copyObj->setKegiatanCode($this->kegiatan_code);

		$copyObj->setSubtitle($this->subtitle);

		$copyObj->setKomponenId($this->komponen_id);

		$copyObj->setKomponenName($this->komponen_name);

		$copyObj->setKomponenLokasi($this->komponen_lokasi);

		$copyObj->setKomponenHargaAwal($this->komponen_harga_awal);

		$copyObj->setPajak($this->pajak);

		$copyObj->setKomponenSatuan($this->komponen_satuan);

		$copyObj->setKomponenRekening($this->komponen_rekening);

		$copyObj->setKoefisien($this->koefisien);

		$copyObj->setVolume($this->volume);

		$copyObj->setNilaiAnggaran($this->nilai_anggaran);

		$copyObj->setTahunInput($this->tahun_input);

		$copyObj->setCreatedAt($this->created_at);

		$copyObj->setUpdatedAt($this->updated_at);

		$copyObj->setStatusHapus($this->status_hapus);

		$copyObj->setStatusWaiting($this->status_waiting);

		$copyObj->setPrioritas($this->prioritas);

		$copyObj->setKodeRka($this->kode_rka);

		$copyObj->setUserPengambil($this->user_pengambil);

		$copyObj->setNilaiEe($this->nilai_ee);

		$copyObj->setKeterangan($this->keterangan);

		$copyObj->setKodeJasmas($this->kode_jasmas);

		$copyObj->setKecamatan($this->kecamatan);

		$copyObj->setKelurahan($this->kelurahan);

		$copyObj->setIsMusrenbang($this->is_musrenbang);

		$copyObj->setNilaiAnggaranSemula($this->nilai_anggaran_semula);

		$copyObj->setMetodeWaitinglist($this->metode_waitinglist);


		$copyObj->setNew(true);

		$copyObj->setIdWaiting(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new WaitingListPUPeer();
		}
		return self::$peer;
	}

} 