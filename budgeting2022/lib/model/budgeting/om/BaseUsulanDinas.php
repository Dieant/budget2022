<?php


abstract class BaseUsulanDinas extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id_usulan;


	
	protected $tgl_usulan;


	
	protected $created_at;


	
	protected $updated_at;


	
	protected $penyelia;


	
	protected $skpd;


	
	protected $jenis_usulan;


	
	protected $jumlah_dukungan;


	
	protected $jumlah_usulan;


	
	protected $status_verifikasi;


	
	protected $status_hapus;


	
	protected $filepath;


	
	protected $filepath_rar;


	
	protected $komentar_verifikator;


	
	protected $nomor_surat;


	
	protected $status_tolak;


	
	protected $keterangan_dinas;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getIdUsulan()
	{

		return $this->id_usulan;
	}

	
	public function getTglUsulan($format = 'Y-m-d H:i:s')
	{

		if ($this->tgl_usulan === null || $this->tgl_usulan === '') {
			return null;
		} elseif (!is_int($this->tgl_usulan)) {
						$ts = strtotime($this->tgl_usulan);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [tgl_usulan] as date/time value: " . var_export($this->tgl_usulan, true));
			}
		} else {
			$ts = $this->tgl_usulan;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getCreatedAt($format = 'Y-m-d H:i:s')
	{

		if ($this->created_at === null || $this->created_at === '') {
			return null;
		} elseif (!is_int($this->created_at)) {
						$ts = strtotime($this->created_at);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [created_at] as date/time value: " . var_export($this->created_at, true));
			}
		} else {
			$ts = $this->created_at;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getUpdatedAt($format = 'Y-m-d H:i:s')
	{

		if ($this->updated_at === null || $this->updated_at === '') {
			return null;
		} elseif (!is_int($this->updated_at)) {
						$ts = strtotime($this->updated_at);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [updated_at] as date/time value: " . var_export($this->updated_at, true));
			}
		} else {
			$ts = $this->updated_at;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getPenyelia()
	{

		return $this->penyelia;
	}

	
	public function getSkpd()
	{

		return $this->skpd;
	}

	
	public function getJenisUsulan()
	{

		return $this->jenis_usulan;
	}

	
	public function getJumlahDukungan()
	{

		return $this->jumlah_dukungan;
	}

	
	public function getJumlahUsulan()
	{

		return $this->jumlah_usulan;
	}

	
	public function getStatusVerifikasi()
	{

		return $this->status_verifikasi;
	}

	
	public function getStatusHapus()
	{

		return $this->status_hapus;
	}

	
	public function getFilepath()
	{

		return $this->filepath;
	}

	
	public function getFilepathRar()
	{

		return $this->filepath_rar;
	}

	
	public function getKomentarVerifikator()
	{

		return $this->komentar_verifikator;
	}

	
	public function getNomorSurat()
	{

		return $this->nomor_surat;
	}

	
	public function getStatusTolak()
	{

		return $this->status_tolak;
	}

	
	public function getKeteranganDinas()
	{

		return $this->keterangan_dinas;
	}

	
	public function setIdUsulan($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id_usulan !== $v) {
			$this->id_usulan = $v;
			$this->modifiedColumns[] = UsulanDinasPeer::ID_USULAN;
		}

	} 
	
	public function setTglUsulan($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [tgl_usulan] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->tgl_usulan !== $ts) {
			$this->tgl_usulan = $ts;
			$this->modifiedColumns[] = UsulanDinasPeer::TGL_USULAN;
		}

	} 
	
	public function setCreatedAt($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [created_at] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->created_at !== $ts) {
			$this->created_at = $ts;
			$this->modifiedColumns[] = UsulanDinasPeer::CREATED_AT;
		}

	} 
	
	public function setUpdatedAt($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [updated_at] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->updated_at !== $ts) {
			$this->updated_at = $ts;
			$this->modifiedColumns[] = UsulanDinasPeer::UPDATED_AT;
		}

	} 
	
	public function setPenyelia($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->penyelia !== $v) {
			$this->penyelia = $v;
			$this->modifiedColumns[] = UsulanDinasPeer::PENYELIA;
		}

	} 
	
	public function setSkpd($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->skpd !== $v) {
			$this->skpd = $v;
			$this->modifiedColumns[] = UsulanDinasPeer::SKPD;
		}

	} 
	
	public function setJenisUsulan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->jenis_usulan !== $v) {
			$this->jenis_usulan = $v;
			$this->modifiedColumns[] = UsulanDinasPeer::JENIS_USULAN;
		}

	} 
	
	public function setJumlahDukungan($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->jumlah_dukungan !== $v) {
			$this->jumlah_dukungan = $v;
			$this->modifiedColumns[] = UsulanDinasPeer::JUMLAH_DUKUNGAN;
		}

	} 
	
	public function setJumlahUsulan($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->jumlah_usulan !== $v) {
			$this->jumlah_usulan = $v;
			$this->modifiedColumns[] = UsulanDinasPeer::JUMLAH_USULAN;
		}

	} 
	
	public function setStatusVerifikasi($v)
	{

		if ($this->status_verifikasi !== $v) {
			$this->status_verifikasi = $v;
			$this->modifiedColumns[] = UsulanDinasPeer::STATUS_VERIFIKASI;
		}

	} 
	
	public function setStatusHapus($v)
	{

		if ($this->status_hapus !== $v) {
			$this->status_hapus = $v;
			$this->modifiedColumns[] = UsulanDinasPeer::STATUS_HAPUS;
		}

	} 
	
	public function setFilepath($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->filepath !== $v) {
			$this->filepath = $v;
			$this->modifiedColumns[] = UsulanDinasPeer::FILEPATH;
		}

	} 
	
	public function setFilepathRar($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->filepath_rar !== $v) {
			$this->filepath_rar = $v;
			$this->modifiedColumns[] = UsulanDinasPeer::FILEPATH_RAR;
		}

	} 
	
	public function setKomentarVerifikator($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->komentar_verifikator !== $v) {
			$this->komentar_verifikator = $v;
			$this->modifiedColumns[] = UsulanDinasPeer::KOMENTAR_VERIFIKATOR;
		}

	} 
	
	public function setNomorSurat($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nomor_surat !== $v) {
			$this->nomor_surat = $v;
			$this->modifiedColumns[] = UsulanDinasPeer::NOMOR_SURAT;
		}

	} 
	
	public function setStatusTolak($v)
	{

		if ($this->status_tolak !== $v) {
			$this->status_tolak = $v;
			$this->modifiedColumns[] = UsulanDinasPeer::STATUS_TOLAK;
		}

	} 
	
	public function setKeteranganDinas($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->keterangan_dinas !== $v) {
			$this->keterangan_dinas = $v;
			$this->modifiedColumns[] = UsulanDinasPeer::KETERANGAN_DINAS;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id_usulan = $rs->getInt($startcol + 0);

			$this->tgl_usulan = $rs->getTimestamp($startcol + 1, null);

			$this->created_at = $rs->getTimestamp($startcol + 2, null);

			$this->updated_at = $rs->getTimestamp($startcol + 3, null);

			$this->penyelia = $rs->getString($startcol + 4);

			$this->skpd = $rs->getString($startcol + 5);

			$this->jenis_usulan = $rs->getString($startcol + 6);

			$this->jumlah_dukungan = $rs->getInt($startcol + 7);

			$this->jumlah_usulan = $rs->getInt($startcol + 8);

			$this->status_verifikasi = $rs->getBoolean($startcol + 9);

			$this->status_hapus = $rs->getBoolean($startcol + 10);

			$this->filepath = $rs->getString($startcol + 11);

			$this->filepath_rar = $rs->getString($startcol + 12);

			$this->komentar_verifikator = $rs->getString($startcol + 13);

			$this->nomor_surat = $rs->getString($startcol + 14);

			$this->status_tolak = $rs->getBoolean($startcol + 15);

			$this->keterangan_dinas = $rs->getString($startcol + 16);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 17; 
		} catch (Exception $e) {
			throw new PropelException("Error populating UsulanDinas object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(UsulanDinasPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			UsulanDinasPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
    if ($this->isNew() && !$this->isColumnModified(UsulanDinasPeer::CREATED_AT))
    {
      $this->setCreatedAt(time());
    }

    if ($this->isModified() && !$this->isColumnModified(UsulanDinasPeer::UPDATED_AT))
    {
      $this->setUpdatedAt(time());
    }

		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(UsulanDinasPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = UsulanDinasPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += UsulanDinasPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = UsulanDinasPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = UsulanDinasPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getIdUsulan();
				break;
			case 1:
				return $this->getTglUsulan();
				break;
			case 2:
				return $this->getCreatedAt();
				break;
			case 3:
				return $this->getUpdatedAt();
				break;
			case 4:
				return $this->getPenyelia();
				break;
			case 5:
				return $this->getSkpd();
				break;
			case 6:
				return $this->getJenisUsulan();
				break;
			case 7:
				return $this->getJumlahDukungan();
				break;
			case 8:
				return $this->getJumlahUsulan();
				break;
			case 9:
				return $this->getStatusVerifikasi();
				break;
			case 10:
				return $this->getStatusHapus();
				break;
			case 11:
				return $this->getFilepath();
				break;
			case 12:
				return $this->getFilepathRar();
				break;
			case 13:
				return $this->getKomentarVerifikator();
				break;
			case 14:
				return $this->getNomorSurat();
				break;
			case 15:
				return $this->getStatusTolak();
				break;
			case 16:
				return $this->getKeteranganDinas();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = UsulanDinasPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getIdUsulan(),
			$keys[1] => $this->getTglUsulan(),
			$keys[2] => $this->getCreatedAt(),
			$keys[3] => $this->getUpdatedAt(),
			$keys[4] => $this->getPenyelia(),
			$keys[5] => $this->getSkpd(),
			$keys[6] => $this->getJenisUsulan(),
			$keys[7] => $this->getJumlahDukungan(),
			$keys[8] => $this->getJumlahUsulan(),
			$keys[9] => $this->getStatusVerifikasi(),
			$keys[10] => $this->getStatusHapus(),
			$keys[11] => $this->getFilepath(),
			$keys[12] => $this->getFilepathRar(),
			$keys[13] => $this->getKomentarVerifikator(),
			$keys[14] => $this->getNomorSurat(),
			$keys[15] => $this->getStatusTolak(),
			$keys[16] => $this->getKeteranganDinas(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = UsulanDinasPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setIdUsulan($value);
				break;
			case 1:
				$this->setTglUsulan($value);
				break;
			case 2:
				$this->setCreatedAt($value);
				break;
			case 3:
				$this->setUpdatedAt($value);
				break;
			case 4:
				$this->setPenyelia($value);
				break;
			case 5:
				$this->setSkpd($value);
				break;
			case 6:
				$this->setJenisUsulan($value);
				break;
			case 7:
				$this->setJumlahDukungan($value);
				break;
			case 8:
				$this->setJumlahUsulan($value);
				break;
			case 9:
				$this->setStatusVerifikasi($value);
				break;
			case 10:
				$this->setStatusHapus($value);
				break;
			case 11:
				$this->setFilepath($value);
				break;
			case 12:
				$this->setFilepathRar($value);
				break;
			case 13:
				$this->setKomentarVerifikator($value);
				break;
			case 14:
				$this->setNomorSurat($value);
				break;
			case 15:
				$this->setStatusTolak($value);
				break;
			case 16:
				$this->setKeteranganDinas($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = UsulanDinasPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setIdUsulan($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setTglUsulan($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setCreatedAt($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setUpdatedAt($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setPenyelia($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setSkpd($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setJenisUsulan($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setJumlahDukungan($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setJumlahUsulan($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setStatusVerifikasi($arr[$keys[9]]);
		if (array_key_exists($keys[10], $arr)) $this->setStatusHapus($arr[$keys[10]]);
		if (array_key_exists($keys[11], $arr)) $this->setFilepath($arr[$keys[11]]);
		if (array_key_exists($keys[12], $arr)) $this->setFilepathRar($arr[$keys[12]]);
		if (array_key_exists($keys[13], $arr)) $this->setKomentarVerifikator($arr[$keys[13]]);
		if (array_key_exists($keys[14], $arr)) $this->setNomorSurat($arr[$keys[14]]);
		if (array_key_exists($keys[15], $arr)) $this->setStatusTolak($arr[$keys[15]]);
		if (array_key_exists($keys[16], $arr)) $this->setKeteranganDinas($arr[$keys[16]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(UsulanDinasPeer::DATABASE_NAME);

		if ($this->isColumnModified(UsulanDinasPeer::ID_USULAN)) $criteria->add(UsulanDinasPeer::ID_USULAN, $this->id_usulan);
		if ($this->isColumnModified(UsulanDinasPeer::TGL_USULAN)) $criteria->add(UsulanDinasPeer::TGL_USULAN, $this->tgl_usulan);
		if ($this->isColumnModified(UsulanDinasPeer::CREATED_AT)) $criteria->add(UsulanDinasPeer::CREATED_AT, $this->created_at);
		if ($this->isColumnModified(UsulanDinasPeer::UPDATED_AT)) $criteria->add(UsulanDinasPeer::UPDATED_AT, $this->updated_at);
		if ($this->isColumnModified(UsulanDinasPeer::PENYELIA)) $criteria->add(UsulanDinasPeer::PENYELIA, $this->penyelia);
		if ($this->isColumnModified(UsulanDinasPeer::SKPD)) $criteria->add(UsulanDinasPeer::SKPD, $this->skpd);
		if ($this->isColumnModified(UsulanDinasPeer::JENIS_USULAN)) $criteria->add(UsulanDinasPeer::JENIS_USULAN, $this->jenis_usulan);
		if ($this->isColumnModified(UsulanDinasPeer::JUMLAH_DUKUNGAN)) $criteria->add(UsulanDinasPeer::JUMLAH_DUKUNGAN, $this->jumlah_dukungan);
		if ($this->isColumnModified(UsulanDinasPeer::JUMLAH_USULAN)) $criteria->add(UsulanDinasPeer::JUMLAH_USULAN, $this->jumlah_usulan);
		if ($this->isColumnModified(UsulanDinasPeer::STATUS_VERIFIKASI)) $criteria->add(UsulanDinasPeer::STATUS_VERIFIKASI, $this->status_verifikasi);
		if ($this->isColumnModified(UsulanDinasPeer::STATUS_HAPUS)) $criteria->add(UsulanDinasPeer::STATUS_HAPUS, $this->status_hapus);
		if ($this->isColumnModified(UsulanDinasPeer::FILEPATH)) $criteria->add(UsulanDinasPeer::FILEPATH, $this->filepath);
		if ($this->isColumnModified(UsulanDinasPeer::FILEPATH_RAR)) $criteria->add(UsulanDinasPeer::FILEPATH_RAR, $this->filepath_rar);
		if ($this->isColumnModified(UsulanDinasPeer::KOMENTAR_VERIFIKATOR)) $criteria->add(UsulanDinasPeer::KOMENTAR_VERIFIKATOR, $this->komentar_verifikator);
		if ($this->isColumnModified(UsulanDinasPeer::NOMOR_SURAT)) $criteria->add(UsulanDinasPeer::NOMOR_SURAT, $this->nomor_surat);
		if ($this->isColumnModified(UsulanDinasPeer::STATUS_TOLAK)) $criteria->add(UsulanDinasPeer::STATUS_TOLAK, $this->status_tolak);
		if ($this->isColumnModified(UsulanDinasPeer::KETERANGAN_DINAS)) $criteria->add(UsulanDinasPeer::KETERANGAN_DINAS, $this->keterangan_dinas);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(UsulanDinasPeer::DATABASE_NAME);

		$criteria->add(UsulanDinasPeer::ID_USULAN, $this->id_usulan);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getIdUsulan();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setIdUsulan($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setTglUsulan($this->tgl_usulan);

		$copyObj->setCreatedAt($this->created_at);

		$copyObj->setUpdatedAt($this->updated_at);

		$copyObj->setPenyelia($this->penyelia);

		$copyObj->setSkpd($this->skpd);

		$copyObj->setJenisUsulan($this->jenis_usulan);

		$copyObj->setJumlahDukungan($this->jumlah_dukungan);

		$copyObj->setJumlahUsulan($this->jumlah_usulan);

		$copyObj->setStatusVerifikasi($this->status_verifikasi);

		$copyObj->setStatusHapus($this->status_hapus);

		$copyObj->setFilepath($this->filepath);

		$copyObj->setFilepathRar($this->filepath_rar);

		$copyObj->setKomentarVerifikator($this->komentar_verifikator);

		$copyObj->setNomorSurat($this->nomor_surat);

		$copyObj->setStatusTolak($this->status_tolak);

		$copyObj->setKeteranganDinas($this->keterangan_dinas);


		$copyObj->setNew(true);

		$copyObj->setIdUsulan(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new UsulanDinasPeer();
		}
		return self::$peer;
	}

} 