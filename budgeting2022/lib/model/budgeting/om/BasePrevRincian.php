<?php


abstract class BasePrevRincian extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $kegiatan_code;


	
	protected $tipe;


	
	protected $rincian_confirmed = false;


	
	protected $rincian_changed = false;


	
	protected $rincian_selesai = false;


	
	protected $ip_address;


	
	protected $waktu_access;


	
	protected $target;


	
	protected $unit_id;


	
	protected $rincian_level = 1;


	
	protected $lock = false;


	
	protected $last_update_user;


	
	protected $last_update_time;


	
	protected $last_update_ip;


	
	protected $tahap;


	
	protected $tahun;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getKegiatanCode()
	{

		return $this->kegiatan_code;
	}

	
	public function getTipe()
	{

		return $this->tipe;
	}

	
	public function getRincianConfirmed()
	{

		return $this->rincian_confirmed;
	}

	
	public function getRincianChanged()
	{

		return $this->rincian_changed;
	}

	
	public function getRincianSelesai()
	{

		return $this->rincian_selesai;
	}

	
	public function getIpAddress()
	{

		return $this->ip_address;
	}

	
	public function getWaktuAccess($format = 'Y-m-d H:i:s')
	{

		if ($this->waktu_access === null || $this->waktu_access === '') {
			return null;
		} elseif (!is_int($this->waktu_access)) {
						$ts = strtotime($this->waktu_access);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [waktu_access] as date/time value: " . var_export($this->waktu_access, true));
			}
		} else {
			$ts = $this->waktu_access;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getTarget()
	{

		return $this->target;
	}

	
	public function getUnitId()
	{

		return $this->unit_id;
	}

	
	public function getRincianLevel()
	{

		return $this->rincian_level;
	}

	
	public function getLock()
	{

		return $this->lock;
	}

	
	public function getLastUpdateUser()
	{

		return $this->last_update_user;
	}

	
	public function getLastUpdateTime($format = 'Y-m-d H:i:s')
	{

		if ($this->last_update_time === null || $this->last_update_time === '') {
			return null;
		} elseif (!is_int($this->last_update_time)) {
						$ts = strtotime($this->last_update_time);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [last_update_time] as date/time value: " . var_export($this->last_update_time, true));
			}
		} else {
			$ts = $this->last_update_time;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getLastUpdateIp()
	{

		return $this->last_update_ip;
	}

	
	public function getTahap()
	{

		return $this->tahap;
	}

	
	public function getTahun()
	{

		return $this->tahun;
	}

	
	public function setKegiatanCode($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kegiatan_code !== $v) {
			$this->kegiatan_code = $v;
			$this->modifiedColumns[] = PrevRincianPeer::KEGIATAN_CODE;
		}

	} 
	
	public function setTipe($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->tipe !== $v) {
			$this->tipe = $v;
			$this->modifiedColumns[] = PrevRincianPeer::TIPE;
		}

	} 
	
	public function setRincianConfirmed($v)
	{

		if ($this->rincian_confirmed !== $v || $v === false) {
			$this->rincian_confirmed = $v;
			$this->modifiedColumns[] = PrevRincianPeer::RINCIAN_CONFIRMED;
		}

	} 
	
	public function setRincianChanged($v)
	{

		if ($this->rincian_changed !== $v || $v === false) {
			$this->rincian_changed = $v;
			$this->modifiedColumns[] = PrevRincianPeer::RINCIAN_CHANGED;
		}

	} 
	
	public function setRincianSelesai($v)
	{

		if ($this->rincian_selesai !== $v || $v === false) {
			$this->rincian_selesai = $v;
			$this->modifiedColumns[] = PrevRincianPeer::RINCIAN_SELESAI;
		}

	} 
	
	public function setIpAddress($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->ip_address !== $v) {
			$this->ip_address = $v;
			$this->modifiedColumns[] = PrevRincianPeer::IP_ADDRESS;
		}

	} 
	
	public function setWaktuAccess($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [waktu_access] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->waktu_access !== $ts) {
			$this->waktu_access = $ts;
			$this->modifiedColumns[] = PrevRincianPeer::WAKTU_ACCESS;
		}

	} 
	
	public function setTarget($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->target !== $v) {
			$this->target = $v;
			$this->modifiedColumns[] = PrevRincianPeer::TARGET;
		}

	} 
	
	public function setUnitId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->unit_id !== $v) {
			$this->unit_id = $v;
			$this->modifiedColumns[] = PrevRincianPeer::UNIT_ID;
		}

	} 
	
	public function setRincianLevel($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->rincian_level !== $v || $v === 1) {
			$this->rincian_level = $v;
			$this->modifiedColumns[] = PrevRincianPeer::RINCIAN_LEVEL;
		}

	} 
	
	public function setLock($v)
	{

		if ($this->lock !== $v || $v === false) {
			$this->lock = $v;
			$this->modifiedColumns[] = PrevRincianPeer::LOCK;
		}

	} 
	
	public function setLastUpdateUser($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->last_update_user !== $v) {
			$this->last_update_user = $v;
			$this->modifiedColumns[] = PrevRincianPeer::LAST_UPDATE_USER;
		}

	} 
	
	public function setLastUpdateTime($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [last_update_time] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->last_update_time !== $ts) {
			$this->last_update_time = $ts;
			$this->modifiedColumns[] = PrevRincianPeer::LAST_UPDATE_TIME;
		}

	} 
	
	public function setLastUpdateIp($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->last_update_ip !== $v) {
			$this->last_update_ip = $v;
			$this->modifiedColumns[] = PrevRincianPeer::LAST_UPDATE_IP;
		}

	} 
	
	public function setTahap($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->tahap !== $v) {
			$this->tahap = $v;
			$this->modifiedColumns[] = PrevRincianPeer::TAHAP;
		}

	} 
	
	public function setTahun($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->tahun !== $v) {
			$this->tahun = $v;
			$this->modifiedColumns[] = PrevRincianPeer::TAHUN;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->kegiatan_code = $rs->getString($startcol + 0);

			$this->tipe = $rs->getString($startcol + 1);

			$this->rincian_confirmed = $rs->getBoolean($startcol + 2);

			$this->rincian_changed = $rs->getBoolean($startcol + 3);

			$this->rincian_selesai = $rs->getBoolean($startcol + 4);

			$this->ip_address = $rs->getString($startcol + 5);

			$this->waktu_access = $rs->getTimestamp($startcol + 6, null);

			$this->target = $rs->getString($startcol + 7);

			$this->unit_id = $rs->getString($startcol + 8);

			$this->rincian_level = $rs->getInt($startcol + 9);

			$this->lock = $rs->getBoolean($startcol + 10);

			$this->last_update_user = $rs->getString($startcol + 11);

			$this->last_update_time = $rs->getTimestamp($startcol + 12, null);

			$this->last_update_ip = $rs->getString($startcol + 13);

			$this->tahap = $rs->getString($startcol + 14);

			$this->tahun = $rs->getString($startcol + 15);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 16; 
		} catch (Exception $e) {
			throw new PropelException("Error populating PrevRincian object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(PrevRincianPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			PrevRincianPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(PrevRincianPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = PrevRincianPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += PrevRincianPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = PrevRincianPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = PrevRincianPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getKegiatanCode();
				break;
			case 1:
				return $this->getTipe();
				break;
			case 2:
				return $this->getRincianConfirmed();
				break;
			case 3:
				return $this->getRincianChanged();
				break;
			case 4:
				return $this->getRincianSelesai();
				break;
			case 5:
				return $this->getIpAddress();
				break;
			case 6:
				return $this->getWaktuAccess();
				break;
			case 7:
				return $this->getTarget();
				break;
			case 8:
				return $this->getUnitId();
				break;
			case 9:
				return $this->getRincianLevel();
				break;
			case 10:
				return $this->getLock();
				break;
			case 11:
				return $this->getLastUpdateUser();
				break;
			case 12:
				return $this->getLastUpdateTime();
				break;
			case 13:
				return $this->getLastUpdateIp();
				break;
			case 14:
				return $this->getTahap();
				break;
			case 15:
				return $this->getTahun();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = PrevRincianPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getKegiatanCode(),
			$keys[1] => $this->getTipe(),
			$keys[2] => $this->getRincianConfirmed(),
			$keys[3] => $this->getRincianChanged(),
			$keys[4] => $this->getRincianSelesai(),
			$keys[5] => $this->getIpAddress(),
			$keys[6] => $this->getWaktuAccess(),
			$keys[7] => $this->getTarget(),
			$keys[8] => $this->getUnitId(),
			$keys[9] => $this->getRincianLevel(),
			$keys[10] => $this->getLock(),
			$keys[11] => $this->getLastUpdateUser(),
			$keys[12] => $this->getLastUpdateTime(),
			$keys[13] => $this->getLastUpdateIp(),
			$keys[14] => $this->getTahap(),
			$keys[15] => $this->getTahun(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = PrevRincianPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setKegiatanCode($value);
				break;
			case 1:
				$this->setTipe($value);
				break;
			case 2:
				$this->setRincianConfirmed($value);
				break;
			case 3:
				$this->setRincianChanged($value);
				break;
			case 4:
				$this->setRincianSelesai($value);
				break;
			case 5:
				$this->setIpAddress($value);
				break;
			case 6:
				$this->setWaktuAccess($value);
				break;
			case 7:
				$this->setTarget($value);
				break;
			case 8:
				$this->setUnitId($value);
				break;
			case 9:
				$this->setRincianLevel($value);
				break;
			case 10:
				$this->setLock($value);
				break;
			case 11:
				$this->setLastUpdateUser($value);
				break;
			case 12:
				$this->setLastUpdateTime($value);
				break;
			case 13:
				$this->setLastUpdateIp($value);
				break;
			case 14:
				$this->setTahap($value);
				break;
			case 15:
				$this->setTahun($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = PrevRincianPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setKegiatanCode($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setTipe($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setRincianConfirmed($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setRincianChanged($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setRincianSelesai($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setIpAddress($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setWaktuAccess($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setTarget($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setUnitId($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setRincianLevel($arr[$keys[9]]);
		if (array_key_exists($keys[10], $arr)) $this->setLock($arr[$keys[10]]);
		if (array_key_exists($keys[11], $arr)) $this->setLastUpdateUser($arr[$keys[11]]);
		if (array_key_exists($keys[12], $arr)) $this->setLastUpdateTime($arr[$keys[12]]);
		if (array_key_exists($keys[13], $arr)) $this->setLastUpdateIp($arr[$keys[13]]);
		if (array_key_exists($keys[14], $arr)) $this->setTahap($arr[$keys[14]]);
		if (array_key_exists($keys[15], $arr)) $this->setTahun($arr[$keys[15]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(PrevRincianPeer::DATABASE_NAME);

		if ($this->isColumnModified(PrevRincianPeer::KEGIATAN_CODE)) $criteria->add(PrevRincianPeer::KEGIATAN_CODE, $this->kegiatan_code);
		if ($this->isColumnModified(PrevRincianPeer::TIPE)) $criteria->add(PrevRincianPeer::TIPE, $this->tipe);
		if ($this->isColumnModified(PrevRincianPeer::RINCIAN_CONFIRMED)) $criteria->add(PrevRincianPeer::RINCIAN_CONFIRMED, $this->rincian_confirmed);
		if ($this->isColumnModified(PrevRincianPeer::RINCIAN_CHANGED)) $criteria->add(PrevRincianPeer::RINCIAN_CHANGED, $this->rincian_changed);
		if ($this->isColumnModified(PrevRincianPeer::RINCIAN_SELESAI)) $criteria->add(PrevRincianPeer::RINCIAN_SELESAI, $this->rincian_selesai);
		if ($this->isColumnModified(PrevRincianPeer::IP_ADDRESS)) $criteria->add(PrevRincianPeer::IP_ADDRESS, $this->ip_address);
		if ($this->isColumnModified(PrevRincianPeer::WAKTU_ACCESS)) $criteria->add(PrevRincianPeer::WAKTU_ACCESS, $this->waktu_access);
		if ($this->isColumnModified(PrevRincianPeer::TARGET)) $criteria->add(PrevRincianPeer::TARGET, $this->target);
		if ($this->isColumnModified(PrevRincianPeer::UNIT_ID)) $criteria->add(PrevRincianPeer::UNIT_ID, $this->unit_id);
		if ($this->isColumnModified(PrevRincianPeer::RINCIAN_LEVEL)) $criteria->add(PrevRincianPeer::RINCIAN_LEVEL, $this->rincian_level);
		if ($this->isColumnModified(PrevRincianPeer::LOCK)) $criteria->add(PrevRincianPeer::LOCK, $this->lock);
		if ($this->isColumnModified(PrevRincianPeer::LAST_UPDATE_USER)) $criteria->add(PrevRincianPeer::LAST_UPDATE_USER, $this->last_update_user);
		if ($this->isColumnModified(PrevRincianPeer::LAST_UPDATE_TIME)) $criteria->add(PrevRincianPeer::LAST_UPDATE_TIME, $this->last_update_time);
		if ($this->isColumnModified(PrevRincianPeer::LAST_UPDATE_IP)) $criteria->add(PrevRincianPeer::LAST_UPDATE_IP, $this->last_update_ip);
		if ($this->isColumnModified(PrevRincianPeer::TAHAP)) $criteria->add(PrevRincianPeer::TAHAP, $this->tahap);
		if ($this->isColumnModified(PrevRincianPeer::TAHUN)) $criteria->add(PrevRincianPeer::TAHUN, $this->tahun);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(PrevRincianPeer::DATABASE_NAME);

		$criteria->add(PrevRincianPeer::KEGIATAN_CODE, $this->kegiatan_code);
		$criteria->add(PrevRincianPeer::TIPE, $this->tipe);
		$criteria->add(PrevRincianPeer::UNIT_ID, $this->unit_id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		$pks = array();

		$pks[0] = $this->getKegiatanCode();

		$pks[1] = $this->getTipe();

		$pks[2] = $this->getUnitId();

		return $pks;
	}

	
	public function setPrimaryKey($keys)
	{

		$this->setKegiatanCode($keys[0]);

		$this->setTipe($keys[1]);

		$this->setUnitId($keys[2]);

	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setRincianConfirmed($this->rincian_confirmed);

		$copyObj->setRincianChanged($this->rincian_changed);

		$copyObj->setRincianSelesai($this->rincian_selesai);

		$copyObj->setIpAddress($this->ip_address);

		$copyObj->setWaktuAccess($this->waktu_access);

		$copyObj->setTarget($this->target);

		$copyObj->setRincianLevel($this->rincian_level);

		$copyObj->setLock($this->lock);

		$copyObj->setLastUpdateUser($this->last_update_user);

		$copyObj->setLastUpdateTime($this->last_update_time);

		$copyObj->setLastUpdateIp($this->last_update_ip);

		$copyObj->setTahap($this->tahap);

		$copyObj->setTahun($this->tahun);


		$copyObj->setNew(true);

		$copyObj->setKegiatanCode(NULL); 
		$copyObj->setTipe(NULL); 
		$copyObj->setUnitId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new PrevRincianPeer();
		}
		return self::$peer;
	}

} 