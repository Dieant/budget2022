<?php


abstract class BaseBelurMasterKegiatanPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.belur_master_kegiatan';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.BelurMasterKegiatan';

	
	const NUM_COLUMNS = 43;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const UNIT_ID = 'ebudget.belur_master_kegiatan.UNIT_ID';

	
	const KODE_KEGIATAN = 'ebudget.belur_master_kegiatan.KODE_KEGIATAN';

	
	const KODE_BIDANG = 'ebudget.belur_master_kegiatan.KODE_BIDANG';

	
	const KODE_URUSAN_WAJIB = 'ebudget.belur_master_kegiatan.KODE_URUSAN_WAJIB';

	
	const KODE_PROGRAM = 'ebudget.belur_master_kegiatan.KODE_PROGRAM';

	
	const KODE_SASARAN = 'ebudget.belur_master_kegiatan.KODE_SASARAN';

	
	const KODE_INDIKATOR = 'ebudget.belur_master_kegiatan.KODE_INDIKATOR';

	
	const ALOKASI_DANA = 'ebudget.belur_master_kegiatan.ALOKASI_DANA';

	
	const NAMA_KEGIATAN = 'ebudget.belur_master_kegiatan.NAMA_KEGIATAN';

	
	const MASUKAN = 'ebudget.belur_master_kegiatan.MASUKAN';

	
	const OUTPUT = 'ebudget.belur_master_kegiatan.OUTPUT';

	
	const OUTCOME = 'ebudget.belur_master_kegiatan.OUTCOME';

	
	const BENEFIT = 'ebudget.belur_master_kegiatan.BENEFIT';

	
	const IMPACT = 'ebudget.belur_master_kegiatan.IMPACT';

	
	const TIPE = 'ebudget.belur_master_kegiatan.TIPE';

	
	const KEGIATAN_ACTIVE = 'ebudget.belur_master_kegiatan.KEGIATAN_ACTIVE';

	
	const TO_KEGIATAN_CODE = 'ebudget.belur_master_kegiatan.TO_KEGIATAN_CODE';

	
	const CATATAN = 'ebudget.belur_master_kegiatan.CATATAN';

	
	const TARGET_OUTCOME = 'ebudget.belur_master_kegiatan.TARGET_OUTCOME';

	
	const LOKASI = 'ebudget.belur_master_kegiatan.LOKASI';

	
	const JUMLAH_PREV = 'ebudget.belur_master_kegiatan.JUMLAH_PREV';

	
	const JUMLAH_NOW = 'ebudget.belur_master_kegiatan.JUMLAH_NOW';

	
	const JUMLAH_NEXT = 'ebudget.belur_master_kegiatan.JUMLAH_NEXT';

	
	const KODE_PROGRAM2 = 'ebudget.belur_master_kegiatan.KODE_PROGRAM2';

	
	const KODE_URUSAN = 'ebudget.belur_master_kegiatan.KODE_URUSAN';

	
	const LAST_UPDATE_USER = 'ebudget.belur_master_kegiatan.LAST_UPDATE_USER';

	
	const LAST_UPDATE_TIME = 'ebudget.belur_master_kegiatan.LAST_UPDATE_TIME';

	
	const LAST_UPDATE_IP = 'ebudget.belur_master_kegiatan.LAST_UPDATE_IP';

	
	const TAHAP = 'ebudget.belur_master_kegiatan.TAHAP';

	
	const KODE_MISI = 'ebudget.belur_master_kegiatan.KODE_MISI';

	
	const KODE_TUJUAN = 'ebudget.belur_master_kegiatan.KODE_TUJUAN';

	
	const RANKING = 'ebudget.belur_master_kegiatan.RANKING';

	
	const NOMOR13 = 'ebudget.belur_master_kegiatan.NOMOR13';

	
	const PPA_NAMA = 'ebudget.belur_master_kegiatan.PPA_NAMA';

	
	const PPA_PANGKAT = 'ebudget.belur_master_kegiatan.PPA_PANGKAT';

	
	const PPA_NIP = 'ebudget.belur_master_kegiatan.PPA_NIP';

	
	const LANJUTAN = 'ebudget.belur_master_kegiatan.LANJUTAN';

	
	const USER_ID = 'ebudget.belur_master_kegiatan.USER_ID';

	
	const ID = 'ebudget.belur_master_kegiatan.ID';

	
	const TAHUN = 'ebudget.belur_master_kegiatan.TAHUN';

	
	const TAMBAHAN_PAGU = 'ebudget.belur_master_kegiatan.TAMBAHAN_PAGU';

	
	const GENDER = 'ebudget.belur_master_kegiatan.GENDER';

	
	const KODE_KEG_KEUANGAN = 'ebudget.belur_master_kegiatan.KODE_KEG_KEUANGAN';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('UnitId', 'KodeKegiatan', 'KodeBidang', 'KodeUrusanWajib', 'KodeProgram', 'KodeSasaran', 'KodeIndikator', 'AlokasiDana', 'NamaKegiatan', 'Masukan', 'Output', 'Outcome', 'Benefit', 'Impact', 'Tipe', 'KegiatanActive', 'ToKegiatanCode', 'Catatan', 'TargetOutcome', 'Lokasi', 'JumlahPrev', 'JumlahNow', 'JumlahNext', 'KodeProgram2', 'KodeUrusan', 'LastUpdateUser', 'LastUpdateTime', 'LastUpdateIp', 'Tahap', 'KodeMisi', 'KodeTujuan', 'Ranking', 'Nomor13', 'PpaNama', 'PpaPangkat', 'PpaNip', 'Lanjutan', 'UserId', 'Id', 'Tahun', 'TambahanPagu', 'Gender', 'KodeKegKeuangan', ),
		BasePeer::TYPE_COLNAME => array (BelurMasterKegiatanPeer::UNIT_ID, BelurMasterKegiatanPeer::KODE_KEGIATAN, BelurMasterKegiatanPeer::KODE_BIDANG, BelurMasterKegiatanPeer::KODE_URUSAN_WAJIB, BelurMasterKegiatanPeer::KODE_PROGRAM, BelurMasterKegiatanPeer::KODE_SASARAN, BelurMasterKegiatanPeer::KODE_INDIKATOR, BelurMasterKegiatanPeer::ALOKASI_DANA, BelurMasterKegiatanPeer::NAMA_KEGIATAN, BelurMasterKegiatanPeer::MASUKAN, BelurMasterKegiatanPeer::OUTPUT, BelurMasterKegiatanPeer::OUTCOME, BelurMasterKegiatanPeer::BENEFIT, BelurMasterKegiatanPeer::IMPACT, BelurMasterKegiatanPeer::TIPE, BelurMasterKegiatanPeer::KEGIATAN_ACTIVE, BelurMasterKegiatanPeer::TO_KEGIATAN_CODE, BelurMasterKegiatanPeer::CATATAN, BelurMasterKegiatanPeer::TARGET_OUTCOME, BelurMasterKegiatanPeer::LOKASI, BelurMasterKegiatanPeer::JUMLAH_PREV, BelurMasterKegiatanPeer::JUMLAH_NOW, BelurMasterKegiatanPeer::JUMLAH_NEXT, BelurMasterKegiatanPeer::KODE_PROGRAM2, BelurMasterKegiatanPeer::KODE_URUSAN, BelurMasterKegiatanPeer::LAST_UPDATE_USER, BelurMasterKegiatanPeer::LAST_UPDATE_TIME, BelurMasterKegiatanPeer::LAST_UPDATE_IP, BelurMasterKegiatanPeer::TAHAP, BelurMasterKegiatanPeer::KODE_MISI, BelurMasterKegiatanPeer::KODE_TUJUAN, BelurMasterKegiatanPeer::RANKING, BelurMasterKegiatanPeer::NOMOR13, BelurMasterKegiatanPeer::PPA_NAMA, BelurMasterKegiatanPeer::PPA_PANGKAT, BelurMasterKegiatanPeer::PPA_NIP, BelurMasterKegiatanPeer::LANJUTAN, BelurMasterKegiatanPeer::USER_ID, BelurMasterKegiatanPeer::ID, BelurMasterKegiatanPeer::TAHUN, BelurMasterKegiatanPeer::TAMBAHAN_PAGU, BelurMasterKegiatanPeer::GENDER, BelurMasterKegiatanPeer::KODE_KEG_KEUANGAN, ),
		BasePeer::TYPE_FIELDNAME => array ('unit_id', 'kode_kegiatan', 'kode_bidang', 'kode_urusan_wajib', 'kode_program', 'kode_sasaran', 'kode_indikator', 'alokasi_dana', 'nama_kegiatan', 'masukan', 'output', 'outcome', 'benefit', 'impact', 'tipe', 'kegiatan_active', 'to_kegiatan_code', 'catatan', 'target_outcome', 'lokasi', 'jumlah_prev', 'jumlah_now', 'jumlah_next', 'kode_program2', 'kode_urusan', 'last_update_user', 'last_update_time', 'last_update_ip', 'tahap', 'kode_misi', 'kode_tujuan', 'ranking', 'nomor13', 'ppa_nama', 'ppa_pangkat', 'ppa_nip', 'lanjutan', 'user_id', 'id', 'tahun', 'tambahan_pagu', 'gender', 'kode_keg_keuangan', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('UnitId' => 0, 'KodeKegiatan' => 1, 'KodeBidang' => 2, 'KodeUrusanWajib' => 3, 'KodeProgram' => 4, 'KodeSasaran' => 5, 'KodeIndikator' => 6, 'AlokasiDana' => 7, 'NamaKegiatan' => 8, 'Masukan' => 9, 'Output' => 10, 'Outcome' => 11, 'Benefit' => 12, 'Impact' => 13, 'Tipe' => 14, 'KegiatanActive' => 15, 'ToKegiatanCode' => 16, 'Catatan' => 17, 'TargetOutcome' => 18, 'Lokasi' => 19, 'JumlahPrev' => 20, 'JumlahNow' => 21, 'JumlahNext' => 22, 'KodeProgram2' => 23, 'KodeUrusan' => 24, 'LastUpdateUser' => 25, 'LastUpdateTime' => 26, 'LastUpdateIp' => 27, 'Tahap' => 28, 'KodeMisi' => 29, 'KodeTujuan' => 30, 'Ranking' => 31, 'Nomor13' => 32, 'PpaNama' => 33, 'PpaPangkat' => 34, 'PpaNip' => 35, 'Lanjutan' => 36, 'UserId' => 37, 'Id' => 38, 'Tahun' => 39, 'TambahanPagu' => 40, 'Gender' => 41, 'KodeKegKeuangan' => 42, ),
		BasePeer::TYPE_COLNAME => array (BelurMasterKegiatanPeer::UNIT_ID => 0, BelurMasterKegiatanPeer::KODE_KEGIATAN => 1, BelurMasterKegiatanPeer::KODE_BIDANG => 2, BelurMasterKegiatanPeer::KODE_URUSAN_WAJIB => 3, BelurMasterKegiatanPeer::KODE_PROGRAM => 4, BelurMasterKegiatanPeer::KODE_SASARAN => 5, BelurMasterKegiatanPeer::KODE_INDIKATOR => 6, BelurMasterKegiatanPeer::ALOKASI_DANA => 7, BelurMasterKegiatanPeer::NAMA_KEGIATAN => 8, BelurMasterKegiatanPeer::MASUKAN => 9, BelurMasterKegiatanPeer::OUTPUT => 10, BelurMasterKegiatanPeer::OUTCOME => 11, BelurMasterKegiatanPeer::BENEFIT => 12, BelurMasterKegiatanPeer::IMPACT => 13, BelurMasterKegiatanPeer::TIPE => 14, BelurMasterKegiatanPeer::KEGIATAN_ACTIVE => 15, BelurMasterKegiatanPeer::TO_KEGIATAN_CODE => 16, BelurMasterKegiatanPeer::CATATAN => 17, BelurMasterKegiatanPeer::TARGET_OUTCOME => 18, BelurMasterKegiatanPeer::LOKASI => 19, BelurMasterKegiatanPeer::JUMLAH_PREV => 20, BelurMasterKegiatanPeer::JUMLAH_NOW => 21, BelurMasterKegiatanPeer::JUMLAH_NEXT => 22, BelurMasterKegiatanPeer::KODE_PROGRAM2 => 23, BelurMasterKegiatanPeer::KODE_URUSAN => 24, BelurMasterKegiatanPeer::LAST_UPDATE_USER => 25, BelurMasterKegiatanPeer::LAST_UPDATE_TIME => 26, BelurMasterKegiatanPeer::LAST_UPDATE_IP => 27, BelurMasterKegiatanPeer::TAHAP => 28, BelurMasterKegiatanPeer::KODE_MISI => 29, BelurMasterKegiatanPeer::KODE_TUJUAN => 30, BelurMasterKegiatanPeer::RANKING => 31, BelurMasterKegiatanPeer::NOMOR13 => 32, BelurMasterKegiatanPeer::PPA_NAMA => 33, BelurMasterKegiatanPeer::PPA_PANGKAT => 34, BelurMasterKegiatanPeer::PPA_NIP => 35, BelurMasterKegiatanPeer::LANJUTAN => 36, BelurMasterKegiatanPeer::USER_ID => 37, BelurMasterKegiatanPeer::ID => 38, BelurMasterKegiatanPeer::TAHUN => 39, BelurMasterKegiatanPeer::TAMBAHAN_PAGU => 40, BelurMasterKegiatanPeer::GENDER => 41, BelurMasterKegiatanPeer::KODE_KEG_KEUANGAN => 42, ),
		BasePeer::TYPE_FIELDNAME => array ('unit_id' => 0, 'kode_kegiatan' => 1, 'kode_bidang' => 2, 'kode_urusan_wajib' => 3, 'kode_program' => 4, 'kode_sasaran' => 5, 'kode_indikator' => 6, 'alokasi_dana' => 7, 'nama_kegiatan' => 8, 'masukan' => 9, 'output' => 10, 'outcome' => 11, 'benefit' => 12, 'impact' => 13, 'tipe' => 14, 'kegiatan_active' => 15, 'to_kegiatan_code' => 16, 'catatan' => 17, 'target_outcome' => 18, 'lokasi' => 19, 'jumlah_prev' => 20, 'jumlah_now' => 21, 'jumlah_next' => 22, 'kode_program2' => 23, 'kode_urusan' => 24, 'last_update_user' => 25, 'last_update_time' => 26, 'last_update_ip' => 27, 'tahap' => 28, 'kode_misi' => 29, 'kode_tujuan' => 30, 'ranking' => 31, 'nomor13' => 32, 'ppa_nama' => 33, 'ppa_pangkat' => 34, 'ppa_nip' => 35, 'lanjutan' => 36, 'user_id' => 37, 'id' => 38, 'tahun' => 39, 'tambahan_pagu' => 40, 'gender' => 41, 'kode_keg_keuangan' => 42, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/BelurMasterKegiatanMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.BelurMasterKegiatanMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = BelurMasterKegiatanPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(BelurMasterKegiatanPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(BelurMasterKegiatanPeer::UNIT_ID);

		$criteria->addSelectColumn(BelurMasterKegiatanPeer::KODE_KEGIATAN);

		$criteria->addSelectColumn(BelurMasterKegiatanPeer::KODE_BIDANG);

		$criteria->addSelectColumn(BelurMasterKegiatanPeer::KODE_URUSAN_WAJIB);

		$criteria->addSelectColumn(BelurMasterKegiatanPeer::KODE_PROGRAM);

		$criteria->addSelectColumn(BelurMasterKegiatanPeer::KODE_SASARAN);

		$criteria->addSelectColumn(BelurMasterKegiatanPeer::KODE_INDIKATOR);

		$criteria->addSelectColumn(BelurMasterKegiatanPeer::ALOKASI_DANA);

		$criteria->addSelectColumn(BelurMasterKegiatanPeer::NAMA_KEGIATAN);

		$criteria->addSelectColumn(BelurMasterKegiatanPeer::MASUKAN);

		$criteria->addSelectColumn(BelurMasterKegiatanPeer::OUTPUT);

		$criteria->addSelectColumn(BelurMasterKegiatanPeer::OUTCOME);

		$criteria->addSelectColumn(BelurMasterKegiatanPeer::BENEFIT);

		$criteria->addSelectColumn(BelurMasterKegiatanPeer::IMPACT);

		$criteria->addSelectColumn(BelurMasterKegiatanPeer::TIPE);

		$criteria->addSelectColumn(BelurMasterKegiatanPeer::KEGIATAN_ACTIVE);

		$criteria->addSelectColumn(BelurMasterKegiatanPeer::TO_KEGIATAN_CODE);

		$criteria->addSelectColumn(BelurMasterKegiatanPeer::CATATAN);

		$criteria->addSelectColumn(BelurMasterKegiatanPeer::TARGET_OUTCOME);

		$criteria->addSelectColumn(BelurMasterKegiatanPeer::LOKASI);

		$criteria->addSelectColumn(BelurMasterKegiatanPeer::JUMLAH_PREV);

		$criteria->addSelectColumn(BelurMasterKegiatanPeer::JUMLAH_NOW);

		$criteria->addSelectColumn(BelurMasterKegiatanPeer::JUMLAH_NEXT);

		$criteria->addSelectColumn(BelurMasterKegiatanPeer::KODE_PROGRAM2);

		$criteria->addSelectColumn(BelurMasterKegiatanPeer::KODE_URUSAN);

		$criteria->addSelectColumn(BelurMasterKegiatanPeer::LAST_UPDATE_USER);

		$criteria->addSelectColumn(BelurMasterKegiatanPeer::LAST_UPDATE_TIME);

		$criteria->addSelectColumn(BelurMasterKegiatanPeer::LAST_UPDATE_IP);

		$criteria->addSelectColumn(BelurMasterKegiatanPeer::TAHAP);

		$criteria->addSelectColumn(BelurMasterKegiatanPeer::KODE_MISI);

		$criteria->addSelectColumn(BelurMasterKegiatanPeer::KODE_TUJUAN);

		$criteria->addSelectColumn(BelurMasterKegiatanPeer::RANKING);

		$criteria->addSelectColumn(BelurMasterKegiatanPeer::NOMOR13);

		$criteria->addSelectColumn(BelurMasterKegiatanPeer::PPA_NAMA);

		$criteria->addSelectColumn(BelurMasterKegiatanPeer::PPA_PANGKAT);

		$criteria->addSelectColumn(BelurMasterKegiatanPeer::PPA_NIP);

		$criteria->addSelectColumn(BelurMasterKegiatanPeer::LANJUTAN);

		$criteria->addSelectColumn(BelurMasterKegiatanPeer::USER_ID);

		$criteria->addSelectColumn(BelurMasterKegiatanPeer::ID);

		$criteria->addSelectColumn(BelurMasterKegiatanPeer::TAHUN);

		$criteria->addSelectColumn(BelurMasterKegiatanPeer::TAMBAHAN_PAGU);

		$criteria->addSelectColumn(BelurMasterKegiatanPeer::GENDER);

		$criteria->addSelectColumn(BelurMasterKegiatanPeer::KODE_KEG_KEUANGAN);

	}

	const COUNT = 'COUNT(ebudget.belur_master_kegiatan.UNIT_ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.belur_master_kegiatan.UNIT_ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(BelurMasterKegiatanPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(BelurMasterKegiatanPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = BelurMasterKegiatanPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = BelurMasterKegiatanPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return BelurMasterKegiatanPeer::populateObjects(BelurMasterKegiatanPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			BelurMasterKegiatanPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = BelurMasterKegiatanPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return BelurMasterKegiatanPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}

		$criteria->remove(BelurMasterKegiatanPeer::ID); 

				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(BelurMasterKegiatanPeer::UNIT_ID);
			$selectCriteria->add(BelurMasterKegiatanPeer::UNIT_ID, $criteria->remove(BelurMasterKegiatanPeer::UNIT_ID), $comparison);

			$comparison = $criteria->getComparison(BelurMasterKegiatanPeer::KODE_KEGIATAN);
			$selectCriteria->add(BelurMasterKegiatanPeer::KODE_KEGIATAN, $criteria->remove(BelurMasterKegiatanPeer::KODE_KEGIATAN), $comparison);

			$comparison = $criteria->getComparison(BelurMasterKegiatanPeer::ID);
			$selectCriteria->add(BelurMasterKegiatanPeer::ID, $criteria->remove(BelurMasterKegiatanPeer::ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(BelurMasterKegiatanPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(BelurMasterKegiatanPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof BelurMasterKegiatan) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
												if(count($values) == count($values, COUNT_RECURSIVE))
			{
								$values = array($values);
			}
			$vals = array();
			foreach($values as $value)
			{

				$vals[0][] = $value[0];
				$vals[1][] = $value[1];
				$vals[2][] = $value[2];
			}

			$criteria->add(BelurMasterKegiatanPeer::UNIT_ID, $vals[0], Criteria::IN);
			$criteria->add(BelurMasterKegiatanPeer::KODE_KEGIATAN, $vals[1], Criteria::IN);
			$criteria->add(BelurMasterKegiatanPeer::ID, $vals[2], Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(BelurMasterKegiatan $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(BelurMasterKegiatanPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(BelurMasterKegiatanPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(BelurMasterKegiatanPeer::DATABASE_NAME, BelurMasterKegiatanPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = BelurMasterKegiatanPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK( $unit_id, $kode_kegiatan, $id, $con = null) {
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$criteria = new Criteria();
		$criteria->add(BelurMasterKegiatanPeer::UNIT_ID, $unit_id);
		$criteria->add(BelurMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
		$criteria->add(BelurMasterKegiatanPeer::ID, $id);
		$v = BelurMasterKegiatanPeer::doSelect($criteria, $con);

		return !empty($v) ? $v[0] : null;
	}
} 
if (Propel::isInit()) {
			try {
		BaseBelurMasterKegiatanPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/BelurMasterKegiatanMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.BelurMasterKegiatanMapBuilder');
}
