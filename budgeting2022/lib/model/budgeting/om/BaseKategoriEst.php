<?php


abstract class BaseKategoriEst extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $kategori_est_id;


	
	protected $kategori_est_name;


	
	protected $rekening_code;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getKategoriEstId()
	{

		return $this->kategori_est_id;
	}

	
	public function getKategoriEstName()
	{

		return $this->kategori_est_name;
	}

	
	public function getRekeningCode()
	{

		return $this->rekening_code;
	}

	
	public function setKategoriEstId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kategori_est_id !== $v) {
			$this->kategori_est_id = $v;
			$this->modifiedColumns[] = KategoriEstPeer::KATEGORI_EST_ID;
		}

	} 
	
	public function setKategoriEstName($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kategori_est_name !== $v) {
			$this->kategori_est_name = $v;
			$this->modifiedColumns[] = KategoriEstPeer::KATEGORI_EST_NAME;
		}

	} 
	
	public function setRekeningCode($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->rekening_code !== $v) {
			$this->rekening_code = $v;
			$this->modifiedColumns[] = KategoriEstPeer::REKENING_CODE;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->kategori_est_id = $rs->getString($startcol + 0);

			$this->kategori_est_name = $rs->getString($startcol + 1);

			$this->rekening_code = $rs->getString($startcol + 2);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 3; 
		} catch (Exception $e) {
			throw new PropelException("Error populating KategoriEst object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(KategoriEstPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			KategoriEstPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(KategoriEstPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = KategoriEstPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += KategoriEstPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = KategoriEstPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = KategoriEstPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getKategoriEstId();
				break;
			case 1:
				return $this->getKategoriEstName();
				break;
			case 2:
				return $this->getRekeningCode();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = KategoriEstPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getKategoriEstId(),
			$keys[1] => $this->getKategoriEstName(),
			$keys[2] => $this->getRekeningCode(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = KategoriEstPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setKategoriEstId($value);
				break;
			case 1:
				$this->setKategoriEstName($value);
				break;
			case 2:
				$this->setRekeningCode($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = KategoriEstPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setKategoriEstId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setKategoriEstName($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setRekeningCode($arr[$keys[2]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(KategoriEstPeer::DATABASE_NAME);

		if ($this->isColumnModified(KategoriEstPeer::KATEGORI_EST_ID)) $criteria->add(KategoriEstPeer::KATEGORI_EST_ID, $this->kategori_est_id);
		if ($this->isColumnModified(KategoriEstPeer::KATEGORI_EST_NAME)) $criteria->add(KategoriEstPeer::KATEGORI_EST_NAME, $this->kategori_est_name);
		if ($this->isColumnModified(KategoriEstPeer::REKENING_CODE)) $criteria->add(KategoriEstPeer::REKENING_CODE, $this->rekening_code);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(KategoriEstPeer::DATABASE_NAME);

		$criteria->add(KategoriEstPeer::KATEGORI_EST_ID, $this->kategori_est_id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getKategoriEstId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setKategoriEstId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setKategoriEstName($this->kategori_est_name);

		$copyObj->setRekeningCode($this->rekening_code);


		$copyObj->setNew(true);

		$copyObj->setKategoriEstId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new KategoriEstPeer();
		}
		return self::$peer;
	}

} 