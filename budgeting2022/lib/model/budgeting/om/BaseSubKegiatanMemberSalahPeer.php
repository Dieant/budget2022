<?php


abstract class BaseSubKegiatanMemberSalahPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.sub_kegiatan_member_salah';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.SubKegiatanMemberSalah';

	
	const NUM_COLUMNS = 17;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const SUB_KEGIATAN_ID = 'ebudget.sub_kegiatan_member_salah.SUB_KEGIATAN_ID';

	
	const DETAIL_NO = 'ebudget.sub_kegiatan_member_salah.DETAIL_NO';

	
	const KOMPONEN_ID = 'ebudget.sub_kegiatan_member_salah.KOMPONEN_ID';

	
	const REKENING_CODE = 'ebudget.sub_kegiatan_member_salah.REKENING_CODE';

	
	const DETAIL_NAME = 'ebudget.sub_kegiatan_member_salah.DETAIL_NAME';

	
	const VOLUME = 'ebudget.sub_kegiatan_member_salah.VOLUME';

	
	const KETERANGAN_KOEFISIEN = 'ebudget.sub_kegiatan_member_salah.KETERANGAN_KOEFISIEN';

	
	const KOEFISIEN = 'ebudget.sub_kegiatan_member_salah.KOEFISIEN';

	
	const SUBTITLE = 'ebudget.sub_kegiatan_member_salah.SUBTITLE';

	
	const KOMPONEN_HARGA = 'ebudget.sub_kegiatan_member_salah.KOMPONEN_HARGA';

	
	const KOMPONEN_HARGA_AWAL = 'ebudget.sub_kegiatan_member_salah.KOMPONEN_HARGA_AWAL';

	
	const KOMPONEN_NAME = 'ebudget.sub_kegiatan_member_salah.KOMPONEN_NAME';

	
	const SATUAN = 'ebudget.sub_kegiatan_member_salah.SATUAN';

	
	const PAJAK = 'ebudget.sub_kegiatan_member_salah.PAJAK';

	
	const PARAM = 'ebudget.sub_kegiatan_member_salah.PARAM';

	
	const FROM_KEGIATAN_MEMBER = 'ebudget.sub_kegiatan_member_salah.FROM_KEGIATAN_MEMBER';

	
	const TEMP_KOMPONEN_ID = 'ebudget.sub_kegiatan_member_salah.TEMP_KOMPONEN_ID';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('SubKegiatanId', 'DetailNo', 'KomponenId', 'RekeningCode', 'DetailName', 'Volume', 'KeteranganKoefisien', 'Koefisien', 'Subtitle', 'KomponenHarga', 'KomponenHargaAwal', 'KomponenName', 'Satuan', 'Pajak', 'Param', 'FromKegiatanMember', 'TempKomponenId', ),
		BasePeer::TYPE_COLNAME => array (SubKegiatanMemberSalahPeer::SUB_KEGIATAN_ID, SubKegiatanMemberSalahPeer::DETAIL_NO, SubKegiatanMemberSalahPeer::KOMPONEN_ID, SubKegiatanMemberSalahPeer::REKENING_CODE, SubKegiatanMemberSalahPeer::DETAIL_NAME, SubKegiatanMemberSalahPeer::VOLUME, SubKegiatanMemberSalahPeer::KETERANGAN_KOEFISIEN, SubKegiatanMemberSalahPeer::KOEFISIEN, SubKegiatanMemberSalahPeer::SUBTITLE, SubKegiatanMemberSalahPeer::KOMPONEN_HARGA, SubKegiatanMemberSalahPeer::KOMPONEN_HARGA_AWAL, SubKegiatanMemberSalahPeer::KOMPONEN_NAME, SubKegiatanMemberSalahPeer::SATUAN, SubKegiatanMemberSalahPeer::PAJAK, SubKegiatanMemberSalahPeer::PARAM, SubKegiatanMemberSalahPeer::FROM_KEGIATAN_MEMBER, SubKegiatanMemberSalahPeer::TEMP_KOMPONEN_ID, ),
		BasePeer::TYPE_FIELDNAME => array ('sub_kegiatan_id', 'detail_no', 'komponen_id', 'rekening_code', 'detail_name', 'volume', 'keterangan_koefisien', 'koefisien', 'subtitle', 'komponen_harga', 'komponen_harga_awal', 'komponen_name', 'satuan', 'pajak', 'param', 'from_kegiatan_member', 'temp_komponen_id', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('SubKegiatanId' => 0, 'DetailNo' => 1, 'KomponenId' => 2, 'RekeningCode' => 3, 'DetailName' => 4, 'Volume' => 5, 'KeteranganKoefisien' => 6, 'Koefisien' => 7, 'Subtitle' => 8, 'KomponenHarga' => 9, 'KomponenHargaAwal' => 10, 'KomponenName' => 11, 'Satuan' => 12, 'Pajak' => 13, 'Param' => 14, 'FromKegiatanMember' => 15, 'TempKomponenId' => 16, ),
		BasePeer::TYPE_COLNAME => array (SubKegiatanMemberSalahPeer::SUB_KEGIATAN_ID => 0, SubKegiatanMemberSalahPeer::DETAIL_NO => 1, SubKegiatanMemberSalahPeer::KOMPONEN_ID => 2, SubKegiatanMemberSalahPeer::REKENING_CODE => 3, SubKegiatanMemberSalahPeer::DETAIL_NAME => 4, SubKegiatanMemberSalahPeer::VOLUME => 5, SubKegiatanMemberSalahPeer::KETERANGAN_KOEFISIEN => 6, SubKegiatanMemberSalahPeer::KOEFISIEN => 7, SubKegiatanMemberSalahPeer::SUBTITLE => 8, SubKegiatanMemberSalahPeer::KOMPONEN_HARGA => 9, SubKegiatanMemberSalahPeer::KOMPONEN_HARGA_AWAL => 10, SubKegiatanMemberSalahPeer::KOMPONEN_NAME => 11, SubKegiatanMemberSalahPeer::SATUAN => 12, SubKegiatanMemberSalahPeer::PAJAK => 13, SubKegiatanMemberSalahPeer::PARAM => 14, SubKegiatanMemberSalahPeer::FROM_KEGIATAN_MEMBER => 15, SubKegiatanMemberSalahPeer::TEMP_KOMPONEN_ID => 16, ),
		BasePeer::TYPE_FIELDNAME => array ('sub_kegiatan_id' => 0, 'detail_no' => 1, 'komponen_id' => 2, 'rekening_code' => 3, 'detail_name' => 4, 'volume' => 5, 'keterangan_koefisien' => 6, 'koefisien' => 7, 'subtitle' => 8, 'komponen_harga' => 9, 'komponen_harga_awal' => 10, 'komponen_name' => 11, 'satuan' => 12, 'pajak' => 13, 'param' => 14, 'from_kegiatan_member' => 15, 'temp_komponen_id' => 16, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/SubKegiatanMemberSalahMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.SubKegiatanMemberSalahMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = SubKegiatanMemberSalahPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(SubKegiatanMemberSalahPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(SubKegiatanMemberSalahPeer::SUB_KEGIATAN_ID);

		$criteria->addSelectColumn(SubKegiatanMemberSalahPeer::DETAIL_NO);

		$criteria->addSelectColumn(SubKegiatanMemberSalahPeer::KOMPONEN_ID);

		$criteria->addSelectColumn(SubKegiatanMemberSalahPeer::REKENING_CODE);

		$criteria->addSelectColumn(SubKegiatanMemberSalahPeer::DETAIL_NAME);

		$criteria->addSelectColumn(SubKegiatanMemberSalahPeer::VOLUME);

		$criteria->addSelectColumn(SubKegiatanMemberSalahPeer::KETERANGAN_KOEFISIEN);

		$criteria->addSelectColumn(SubKegiatanMemberSalahPeer::KOEFISIEN);

		$criteria->addSelectColumn(SubKegiatanMemberSalahPeer::SUBTITLE);

		$criteria->addSelectColumn(SubKegiatanMemberSalahPeer::KOMPONEN_HARGA);

		$criteria->addSelectColumn(SubKegiatanMemberSalahPeer::KOMPONEN_HARGA_AWAL);

		$criteria->addSelectColumn(SubKegiatanMemberSalahPeer::KOMPONEN_NAME);

		$criteria->addSelectColumn(SubKegiatanMemberSalahPeer::SATUAN);

		$criteria->addSelectColumn(SubKegiatanMemberSalahPeer::PAJAK);

		$criteria->addSelectColumn(SubKegiatanMemberSalahPeer::PARAM);

		$criteria->addSelectColumn(SubKegiatanMemberSalahPeer::FROM_KEGIATAN_MEMBER);

		$criteria->addSelectColumn(SubKegiatanMemberSalahPeer::TEMP_KOMPONEN_ID);

	}

	const COUNT = 'COUNT(*)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT *)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(SubKegiatanMemberSalahPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(SubKegiatanMemberSalahPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = SubKegiatanMemberSalahPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = SubKegiatanMemberSalahPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return SubKegiatanMemberSalahPeer::populateObjects(SubKegiatanMemberSalahPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			SubKegiatanMemberSalahPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = SubKegiatanMemberSalahPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return SubKegiatanMemberSalahPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(SubKegiatanMemberSalahPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(SubKegiatanMemberSalahPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof SubKegiatanMemberSalah) {

			$criteria = $values->buildCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
												if(count($values) == count($values, COUNT_RECURSIVE))
			{
								$values = array($values);
			}
			$vals = array();
			foreach($values as $value)
			{

			}

		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(SubKegiatanMemberSalah $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(SubKegiatanMemberSalahPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(SubKegiatanMemberSalahPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(SubKegiatanMemberSalahPeer::DATABASE_NAME, SubKegiatanMemberSalahPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = SubKegiatanMemberSalahPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

} 
if (Propel::isInit()) {
			try {
		BaseSubKegiatanMemberSalahPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/SubKegiatanMemberSalahMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.SubKegiatanMemberSalahMapBuilder');
}
