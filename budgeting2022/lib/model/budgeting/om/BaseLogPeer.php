<?php


abstract class BaseLogPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.log';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.Log';

	
	const NUM_COLUMNS = 45;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const LOG_ID = 'ebudget.log.LOG_ID';

	
	const WAKTU_ACCESS = 'ebudget.log.WAKTU_ACCESS';

	
	const IP_ADDRESS = 'ebudget.log.IP_ADDRESS';

	
	const LOGIN = 'ebudget.log.LOGIN';

	
	const TABEL_ASAL = 'ebudget.log.TABEL_ASAL';

	
	const ISI1 = 'ebudget.log.ISI1';

	
	const ISI2 = 'ebudget.log.ISI2';

	
	const ISI3 = 'ebudget.log.ISI3';

	
	const ISI4 = 'ebudget.log.ISI4';

	
	const ISI5 = 'ebudget.log.ISI5';

	
	const ISI6 = 'ebudget.log.ISI6';

	
	const ISI7 = 'ebudget.log.ISI7';

	
	const ISI8 = 'ebudget.log.ISI8';

	
	const ISI9 = 'ebudget.log.ISI9';

	
	const ISI10 = 'ebudget.log.ISI10';

	
	const ISI11 = 'ebudget.log.ISI11';

	
	const ISI12 = 'ebudget.log.ISI12';

	
	const ISI13 = 'ebudget.log.ISI13';

	
	const ISI14 = 'ebudget.log.ISI14';

	
	const ISI15 = 'ebudget.log.ISI15';

	
	const ISI16 = 'ebudget.log.ISI16';

	
	const ISI17 = 'ebudget.log.ISI17';

	
	const ISI18 = 'ebudget.log.ISI18';

	
	const ISI19 = 'ebudget.log.ISI19';

	
	const ISI20 = 'ebudget.log.ISI20';

	
	const ISI21 = 'ebudget.log.ISI21';

	
	const ISI22 = 'ebudget.log.ISI22';

	
	const ISI23 = 'ebudget.log.ISI23';

	
	const ISI24 = 'ebudget.log.ISI24';

	
	const ISI25 = 'ebudget.log.ISI25';

	
	const ISI26 = 'ebudget.log.ISI26';

	
	const ISI27 = 'ebudget.log.ISI27';

	
	const ISI28 = 'ebudget.log.ISI28';

	
	const ISI29 = 'ebudget.log.ISI29';

	
	const ISI30 = 'ebudget.log.ISI30';

	
	const ISI31 = 'ebudget.log.ISI31';

	
	const ISI32 = 'ebudget.log.ISI32';

	
	const ISI33 = 'ebudget.log.ISI33';

	
	const ISI34 = 'ebudget.log.ISI34';

	
	const ISI35 = 'ebudget.log.ISI35';

	
	const ISI36 = 'ebudget.log.ISI36';

	
	const ISI37 = 'ebudget.log.ISI37';

	
	const ISI38 = 'ebudget.log.ISI38';

	
	const ISI39 = 'ebudget.log.ISI39';

	
	const ISI40 = 'ebudget.log.ISI40';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('LogId', 'WaktuAccess', 'IpAddress', 'Login', 'TabelAsal', 'Isi1', 'Isi2', 'Isi3', 'Isi4', 'Isi5', 'Isi6', 'Isi7', 'Isi8', 'Isi9', 'Isi10', 'Isi11', 'Isi12', 'Isi13', 'Isi14', 'Isi15', 'Isi16', 'Isi17', 'Isi18', 'Isi19', 'Isi20', 'Isi21', 'Isi22', 'Isi23', 'Isi24', 'Isi25', 'Isi26', 'Isi27', 'Isi28', 'Isi29', 'Isi30', 'Isi31', 'Isi32', 'Isi33', 'Isi34', 'Isi35', 'Isi36', 'Isi37', 'Isi38', 'Isi39', 'Isi40', ),
		BasePeer::TYPE_COLNAME => array (LogPeer::LOG_ID, LogPeer::WAKTU_ACCESS, LogPeer::IP_ADDRESS, LogPeer::LOGIN, LogPeer::TABEL_ASAL, LogPeer::ISI1, LogPeer::ISI2, LogPeer::ISI3, LogPeer::ISI4, LogPeer::ISI5, LogPeer::ISI6, LogPeer::ISI7, LogPeer::ISI8, LogPeer::ISI9, LogPeer::ISI10, LogPeer::ISI11, LogPeer::ISI12, LogPeer::ISI13, LogPeer::ISI14, LogPeer::ISI15, LogPeer::ISI16, LogPeer::ISI17, LogPeer::ISI18, LogPeer::ISI19, LogPeer::ISI20, LogPeer::ISI21, LogPeer::ISI22, LogPeer::ISI23, LogPeer::ISI24, LogPeer::ISI25, LogPeer::ISI26, LogPeer::ISI27, LogPeer::ISI28, LogPeer::ISI29, LogPeer::ISI30, LogPeer::ISI31, LogPeer::ISI32, LogPeer::ISI33, LogPeer::ISI34, LogPeer::ISI35, LogPeer::ISI36, LogPeer::ISI37, LogPeer::ISI38, LogPeer::ISI39, LogPeer::ISI40, ),
		BasePeer::TYPE_FIELDNAME => array ('log_id', 'waktu_access', 'ip_address', 'login', 'tabel_asal', 'isi1', 'isi2', 'isi3', 'isi4', 'isi5', 'isi6', 'isi7', 'isi8', 'isi9', 'isi10', 'isi11', 'isi12', 'isi13', 'isi14', 'isi15', 'isi16', 'isi17', 'isi18', 'isi19', 'isi20', 'isi21', 'isi22', 'isi23', 'isi24', 'isi25', 'isi26', 'isi27', 'isi28', 'isi29', 'isi30', 'isi31', 'isi32', 'isi33', 'isi34', 'isi35', 'isi36', 'isi37', 'isi38', 'isi39', 'isi40', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('LogId' => 0, 'WaktuAccess' => 1, 'IpAddress' => 2, 'Login' => 3, 'TabelAsal' => 4, 'Isi1' => 5, 'Isi2' => 6, 'Isi3' => 7, 'Isi4' => 8, 'Isi5' => 9, 'Isi6' => 10, 'Isi7' => 11, 'Isi8' => 12, 'Isi9' => 13, 'Isi10' => 14, 'Isi11' => 15, 'Isi12' => 16, 'Isi13' => 17, 'Isi14' => 18, 'Isi15' => 19, 'Isi16' => 20, 'Isi17' => 21, 'Isi18' => 22, 'Isi19' => 23, 'Isi20' => 24, 'Isi21' => 25, 'Isi22' => 26, 'Isi23' => 27, 'Isi24' => 28, 'Isi25' => 29, 'Isi26' => 30, 'Isi27' => 31, 'Isi28' => 32, 'Isi29' => 33, 'Isi30' => 34, 'Isi31' => 35, 'Isi32' => 36, 'Isi33' => 37, 'Isi34' => 38, 'Isi35' => 39, 'Isi36' => 40, 'Isi37' => 41, 'Isi38' => 42, 'Isi39' => 43, 'Isi40' => 44, ),
		BasePeer::TYPE_COLNAME => array (LogPeer::LOG_ID => 0, LogPeer::WAKTU_ACCESS => 1, LogPeer::IP_ADDRESS => 2, LogPeer::LOGIN => 3, LogPeer::TABEL_ASAL => 4, LogPeer::ISI1 => 5, LogPeer::ISI2 => 6, LogPeer::ISI3 => 7, LogPeer::ISI4 => 8, LogPeer::ISI5 => 9, LogPeer::ISI6 => 10, LogPeer::ISI7 => 11, LogPeer::ISI8 => 12, LogPeer::ISI9 => 13, LogPeer::ISI10 => 14, LogPeer::ISI11 => 15, LogPeer::ISI12 => 16, LogPeer::ISI13 => 17, LogPeer::ISI14 => 18, LogPeer::ISI15 => 19, LogPeer::ISI16 => 20, LogPeer::ISI17 => 21, LogPeer::ISI18 => 22, LogPeer::ISI19 => 23, LogPeer::ISI20 => 24, LogPeer::ISI21 => 25, LogPeer::ISI22 => 26, LogPeer::ISI23 => 27, LogPeer::ISI24 => 28, LogPeer::ISI25 => 29, LogPeer::ISI26 => 30, LogPeer::ISI27 => 31, LogPeer::ISI28 => 32, LogPeer::ISI29 => 33, LogPeer::ISI30 => 34, LogPeer::ISI31 => 35, LogPeer::ISI32 => 36, LogPeer::ISI33 => 37, LogPeer::ISI34 => 38, LogPeer::ISI35 => 39, LogPeer::ISI36 => 40, LogPeer::ISI37 => 41, LogPeer::ISI38 => 42, LogPeer::ISI39 => 43, LogPeer::ISI40 => 44, ),
		BasePeer::TYPE_FIELDNAME => array ('log_id' => 0, 'waktu_access' => 1, 'ip_address' => 2, 'login' => 3, 'tabel_asal' => 4, 'isi1' => 5, 'isi2' => 6, 'isi3' => 7, 'isi4' => 8, 'isi5' => 9, 'isi6' => 10, 'isi7' => 11, 'isi8' => 12, 'isi9' => 13, 'isi10' => 14, 'isi11' => 15, 'isi12' => 16, 'isi13' => 17, 'isi14' => 18, 'isi15' => 19, 'isi16' => 20, 'isi17' => 21, 'isi18' => 22, 'isi19' => 23, 'isi20' => 24, 'isi21' => 25, 'isi22' => 26, 'isi23' => 27, 'isi24' => 28, 'isi25' => 29, 'isi26' => 30, 'isi27' => 31, 'isi28' => 32, 'isi29' => 33, 'isi30' => 34, 'isi31' => 35, 'isi32' => 36, 'isi33' => 37, 'isi34' => 38, 'isi35' => 39, 'isi36' => 40, 'isi37' => 41, 'isi38' => 42, 'isi39' => 43, 'isi40' => 44, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/LogMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.LogMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = LogPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(LogPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(LogPeer::LOG_ID);

		$criteria->addSelectColumn(LogPeer::WAKTU_ACCESS);

		$criteria->addSelectColumn(LogPeer::IP_ADDRESS);

		$criteria->addSelectColumn(LogPeer::LOGIN);

		$criteria->addSelectColumn(LogPeer::TABEL_ASAL);

		$criteria->addSelectColumn(LogPeer::ISI1);

		$criteria->addSelectColumn(LogPeer::ISI2);

		$criteria->addSelectColumn(LogPeer::ISI3);

		$criteria->addSelectColumn(LogPeer::ISI4);

		$criteria->addSelectColumn(LogPeer::ISI5);

		$criteria->addSelectColumn(LogPeer::ISI6);

		$criteria->addSelectColumn(LogPeer::ISI7);

		$criteria->addSelectColumn(LogPeer::ISI8);

		$criteria->addSelectColumn(LogPeer::ISI9);

		$criteria->addSelectColumn(LogPeer::ISI10);

		$criteria->addSelectColumn(LogPeer::ISI11);

		$criteria->addSelectColumn(LogPeer::ISI12);

		$criteria->addSelectColumn(LogPeer::ISI13);

		$criteria->addSelectColumn(LogPeer::ISI14);

		$criteria->addSelectColumn(LogPeer::ISI15);

		$criteria->addSelectColumn(LogPeer::ISI16);

		$criteria->addSelectColumn(LogPeer::ISI17);

		$criteria->addSelectColumn(LogPeer::ISI18);

		$criteria->addSelectColumn(LogPeer::ISI19);

		$criteria->addSelectColumn(LogPeer::ISI20);

		$criteria->addSelectColumn(LogPeer::ISI21);

		$criteria->addSelectColumn(LogPeer::ISI22);

		$criteria->addSelectColumn(LogPeer::ISI23);

		$criteria->addSelectColumn(LogPeer::ISI24);

		$criteria->addSelectColumn(LogPeer::ISI25);

		$criteria->addSelectColumn(LogPeer::ISI26);

		$criteria->addSelectColumn(LogPeer::ISI27);

		$criteria->addSelectColumn(LogPeer::ISI28);

		$criteria->addSelectColumn(LogPeer::ISI29);

		$criteria->addSelectColumn(LogPeer::ISI30);

		$criteria->addSelectColumn(LogPeer::ISI31);

		$criteria->addSelectColumn(LogPeer::ISI32);

		$criteria->addSelectColumn(LogPeer::ISI33);

		$criteria->addSelectColumn(LogPeer::ISI34);

		$criteria->addSelectColumn(LogPeer::ISI35);

		$criteria->addSelectColumn(LogPeer::ISI36);

		$criteria->addSelectColumn(LogPeer::ISI37);

		$criteria->addSelectColumn(LogPeer::ISI38);

		$criteria->addSelectColumn(LogPeer::ISI39);

		$criteria->addSelectColumn(LogPeer::ISI40);

	}

	const COUNT = 'COUNT(ebudget.log.LOG_ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.log.LOG_ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(LogPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(LogPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = LogPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = LogPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return LogPeer::populateObjects(LogPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			LogPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = LogPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return LogPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(LogPeer::LOG_ID);
			$selectCriteria->add(LogPeer::LOG_ID, $criteria->remove(LogPeer::LOG_ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(LogPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(LogPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof Log) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(LogPeer::LOG_ID, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(Log $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(LogPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(LogPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(LogPeer::DATABASE_NAME, LogPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = LogPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(LogPeer::DATABASE_NAME);

		$criteria->add(LogPeer::LOG_ID, $pk);


		$v = LogPeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(LogPeer::LOG_ID, $pks, Criteria::IN);
			$objs = LogPeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BaseLogPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/LogMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.LogMapBuilder');
}
