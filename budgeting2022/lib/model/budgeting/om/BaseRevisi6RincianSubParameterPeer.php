<?php


abstract class BaseRevisi6RincianSubParameterPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.revisi6_rincian_sub_parameter';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.Revisi6RincianSubParameter';

	
	const NUM_COLUMNS = 15;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const UNIT_ID = 'ebudget.revisi6_rincian_sub_parameter.UNIT_ID';

	
	const KEGIATAN_CODE = 'ebudget.revisi6_rincian_sub_parameter.KEGIATAN_CODE';

	
	const FROM_SUB_KEGIATAN = 'ebudget.revisi6_rincian_sub_parameter.FROM_SUB_KEGIATAN';

	
	const SUB_KEGIATAN_NAME = 'ebudget.revisi6_rincian_sub_parameter.SUB_KEGIATAN_NAME';

	
	const SUBTITLE = 'ebudget.revisi6_rincian_sub_parameter.SUBTITLE';

	
	const DETAIL_NAME = 'ebudget.revisi6_rincian_sub_parameter.DETAIL_NAME';

	
	const NEW_SUBTITLE = 'ebudget.revisi6_rincian_sub_parameter.NEW_SUBTITLE';

	
	const PARAM = 'ebudget.revisi6_rincian_sub_parameter.PARAM';

	
	const KECAMATAN = 'ebudget.revisi6_rincian_sub_parameter.KECAMATAN';

	
	const MAX_NILAI = 'ebudget.revisi6_rincian_sub_parameter.MAX_NILAI';

	
	const KETERANGAN = 'ebudget.revisi6_rincian_sub_parameter.KETERANGAN';

	
	const KET_PEMBAGI = 'ebudget.revisi6_rincian_sub_parameter.KET_PEMBAGI';

	
	const PEMBAGI = 'ebudget.revisi6_rincian_sub_parameter.PEMBAGI';

	
	const KODE_SUB = 'ebudget.revisi6_rincian_sub_parameter.KODE_SUB';

	
	const TAHUN = 'ebudget.revisi6_rincian_sub_parameter.TAHUN';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('UnitId', 'KegiatanCode', 'FromSubKegiatan', 'SubKegiatanName', 'Subtitle', 'DetailName', 'NewSubtitle', 'Param', 'Kecamatan', 'MaxNilai', 'Keterangan', 'KetPembagi', 'Pembagi', 'KodeSub', 'Tahun', ),
		BasePeer::TYPE_COLNAME => array (Revisi6RincianSubParameterPeer::UNIT_ID, Revisi6RincianSubParameterPeer::KEGIATAN_CODE, Revisi6RincianSubParameterPeer::FROM_SUB_KEGIATAN, Revisi6RincianSubParameterPeer::SUB_KEGIATAN_NAME, Revisi6RincianSubParameterPeer::SUBTITLE, Revisi6RincianSubParameterPeer::DETAIL_NAME, Revisi6RincianSubParameterPeer::NEW_SUBTITLE, Revisi6RincianSubParameterPeer::PARAM, Revisi6RincianSubParameterPeer::KECAMATAN, Revisi6RincianSubParameterPeer::MAX_NILAI, Revisi6RincianSubParameterPeer::KETERANGAN, Revisi6RincianSubParameterPeer::KET_PEMBAGI, Revisi6RincianSubParameterPeer::PEMBAGI, Revisi6RincianSubParameterPeer::KODE_SUB, Revisi6RincianSubParameterPeer::TAHUN, ),
		BasePeer::TYPE_FIELDNAME => array ('unit_id', 'kegiatan_code', 'from_sub_kegiatan', 'sub_kegiatan_name', 'subtitle', 'detail_name', 'new_subtitle', 'param', 'kecamatan', 'max_nilai', 'keterangan', 'ket_pembagi', 'pembagi', 'kode_sub', 'tahun', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('UnitId' => 0, 'KegiatanCode' => 1, 'FromSubKegiatan' => 2, 'SubKegiatanName' => 3, 'Subtitle' => 4, 'DetailName' => 5, 'NewSubtitle' => 6, 'Param' => 7, 'Kecamatan' => 8, 'MaxNilai' => 9, 'Keterangan' => 10, 'KetPembagi' => 11, 'Pembagi' => 12, 'KodeSub' => 13, 'Tahun' => 14, ),
		BasePeer::TYPE_COLNAME => array (Revisi6RincianSubParameterPeer::UNIT_ID => 0, Revisi6RincianSubParameterPeer::KEGIATAN_CODE => 1, Revisi6RincianSubParameterPeer::FROM_SUB_KEGIATAN => 2, Revisi6RincianSubParameterPeer::SUB_KEGIATAN_NAME => 3, Revisi6RincianSubParameterPeer::SUBTITLE => 4, Revisi6RincianSubParameterPeer::DETAIL_NAME => 5, Revisi6RincianSubParameterPeer::NEW_SUBTITLE => 6, Revisi6RincianSubParameterPeer::PARAM => 7, Revisi6RincianSubParameterPeer::KECAMATAN => 8, Revisi6RincianSubParameterPeer::MAX_NILAI => 9, Revisi6RincianSubParameterPeer::KETERANGAN => 10, Revisi6RincianSubParameterPeer::KET_PEMBAGI => 11, Revisi6RincianSubParameterPeer::PEMBAGI => 12, Revisi6RincianSubParameterPeer::KODE_SUB => 13, Revisi6RincianSubParameterPeer::TAHUN => 14, ),
		BasePeer::TYPE_FIELDNAME => array ('unit_id' => 0, 'kegiatan_code' => 1, 'from_sub_kegiatan' => 2, 'sub_kegiatan_name' => 3, 'subtitle' => 4, 'detail_name' => 5, 'new_subtitle' => 6, 'param' => 7, 'kecamatan' => 8, 'max_nilai' => 9, 'keterangan' => 10, 'ket_pembagi' => 11, 'pembagi' => 12, 'kode_sub' => 13, 'tahun' => 14, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/Revisi6RincianSubParameterMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.Revisi6RincianSubParameterMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = Revisi6RincianSubParameterPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(Revisi6RincianSubParameterPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(Revisi6RincianSubParameterPeer::UNIT_ID);

		$criteria->addSelectColumn(Revisi6RincianSubParameterPeer::KEGIATAN_CODE);

		$criteria->addSelectColumn(Revisi6RincianSubParameterPeer::FROM_SUB_KEGIATAN);

		$criteria->addSelectColumn(Revisi6RincianSubParameterPeer::SUB_KEGIATAN_NAME);

		$criteria->addSelectColumn(Revisi6RincianSubParameterPeer::SUBTITLE);

		$criteria->addSelectColumn(Revisi6RincianSubParameterPeer::DETAIL_NAME);

		$criteria->addSelectColumn(Revisi6RincianSubParameterPeer::NEW_SUBTITLE);

		$criteria->addSelectColumn(Revisi6RincianSubParameterPeer::PARAM);

		$criteria->addSelectColumn(Revisi6RincianSubParameterPeer::KECAMATAN);

		$criteria->addSelectColumn(Revisi6RincianSubParameterPeer::MAX_NILAI);

		$criteria->addSelectColumn(Revisi6RincianSubParameterPeer::KETERANGAN);

		$criteria->addSelectColumn(Revisi6RincianSubParameterPeer::KET_PEMBAGI);

		$criteria->addSelectColumn(Revisi6RincianSubParameterPeer::PEMBAGI);

		$criteria->addSelectColumn(Revisi6RincianSubParameterPeer::KODE_SUB);

		$criteria->addSelectColumn(Revisi6RincianSubParameterPeer::TAHUN);

	}

	const COUNT = 'COUNT(*)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT *)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(Revisi6RincianSubParameterPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(Revisi6RincianSubParameterPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = Revisi6RincianSubParameterPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = Revisi6RincianSubParameterPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return Revisi6RincianSubParameterPeer::populateObjects(Revisi6RincianSubParameterPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			Revisi6RincianSubParameterPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = Revisi6RincianSubParameterPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return Revisi6RincianSubParameterPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(Revisi6RincianSubParameterPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(Revisi6RincianSubParameterPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof Revisi6RincianSubParameter) {

			$criteria = $values->buildCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
												if(count($values) == count($values, COUNT_RECURSIVE))
			{
								$values = array($values);
			}
			$vals = array();
			foreach($values as $value)
			{

			}

		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(Revisi6RincianSubParameter $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(Revisi6RincianSubParameterPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(Revisi6RincianSubParameterPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(Revisi6RincianSubParameterPeer::DATABASE_NAME, Revisi6RincianSubParameterPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = Revisi6RincianSubParameterPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

} 
if (Propel::isInit()) {
			try {
		BaseRevisi6RincianSubParameterPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/Revisi6RincianSubParameterMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.Revisi6RincianSubParameterMapBuilder');
}
