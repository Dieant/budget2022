<?php


abstract class BaseDeskripsiResumeRapat extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id;


	
	protected $unit_id;


	
	protected $tahap;


	
	protected $tanggal;


	
	protected $jam;


	
	protected $menit;


	
	protected $acara;


	
	protected $acara2;


	
	protected $tempat;


	
	protected $pimpinan;


	
	protected $catatan;


	
	protected $token;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getId()
	{

		return $this->id;
	}

	
	public function getUnitId()
	{

		return $this->unit_id;
	}

	
	public function getTahap()
	{

		return $this->tahap;
	}

	
	public function getTanggal($format = 'Y-m-d H:i:s')
	{

		if ($this->tanggal === null || $this->tanggal === '') {
			return null;
		} elseif (!is_int($this->tanggal)) {
						$ts = strtotime($this->tanggal);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [tanggal] as date/time value: " . var_export($this->tanggal, true));
			}
		} else {
			$ts = $this->tanggal;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getJam()
	{

		return $this->jam;
	}

	
	public function getMenit()
	{

		return $this->menit;
	}

	
	public function getAcara()
	{

		return $this->acara;
	}

	
	public function getAcara2()
	{

		return $this->acara2;
	}

	
	public function getTempat()
	{

		return $this->tempat;
	}

	
	public function getPimpinan()
	{

		return $this->pimpinan;
	}

	
	public function getCatatan()
	{

		return $this->catatan;
	}

	
	public function getToken()
	{

		return $this->token;
	}

	
	public function setId($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = DeskripsiResumeRapatPeer::ID;
		}

	} 
	
	public function setUnitId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->unit_id !== $v) {
			$this->unit_id = $v;
			$this->modifiedColumns[] = DeskripsiResumeRapatPeer::UNIT_ID;
		}

	} 
	
	public function setTahap($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->tahap !== $v) {
			$this->tahap = $v;
			$this->modifiedColumns[] = DeskripsiResumeRapatPeer::TAHAP;
		}

	} 
	
	public function setTanggal($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [tanggal] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->tanggal !== $ts) {
			$this->tanggal = $ts;
			$this->modifiedColumns[] = DeskripsiResumeRapatPeer::TANGGAL;
		}

	} 
	
	public function setJam($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->jam !== $v) {
			$this->jam = $v;
			$this->modifiedColumns[] = DeskripsiResumeRapatPeer::JAM;
		}

	} 
	
	public function setMenit($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->menit !== $v) {
			$this->menit = $v;
			$this->modifiedColumns[] = DeskripsiResumeRapatPeer::MENIT;
		}

	} 
	
	public function setAcara($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->acara !== $v) {
			$this->acara = $v;
			$this->modifiedColumns[] = DeskripsiResumeRapatPeer::ACARA;
		}

	} 
	
	public function setAcara2($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->acara2 !== $v) {
			$this->acara2 = $v;
			$this->modifiedColumns[] = DeskripsiResumeRapatPeer::ACARA2;
		}

	} 
	
	public function setTempat($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->tempat !== $v) {
			$this->tempat = $v;
			$this->modifiedColumns[] = DeskripsiResumeRapatPeer::TEMPAT;
		}

	} 
	
	public function setPimpinan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->pimpinan !== $v) {
			$this->pimpinan = $v;
			$this->modifiedColumns[] = DeskripsiResumeRapatPeer::PIMPINAN;
		}

	} 
	
	public function setCatatan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->catatan !== $v) {
			$this->catatan = $v;
			$this->modifiedColumns[] = DeskripsiResumeRapatPeer::CATATAN;
		}

	} 
	
	public function setToken($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->token !== $v) {
			$this->token = $v;
			$this->modifiedColumns[] = DeskripsiResumeRapatPeer::TOKEN;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getInt($startcol + 0);

			$this->unit_id = $rs->getString($startcol + 1);

			$this->tahap = $rs->getInt($startcol + 2);

			$this->tanggal = $rs->getTimestamp($startcol + 3, null);

			$this->jam = $rs->getInt($startcol + 4);

			$this->menit = $rs->getInt($startcol + 5);

			$this->acara = $rs->getString($startcol + 6);

			$this->acara2 = $rs->getString($startcol + 7);

			$this->tempat = $rs->getString($startcol + 8);

			$this->pimpinan = $rs->getString($startcol + 9);

			$this->catatan = $rs->getString($startcol + 10);

			$this->token = $rs->getString($startcol + 11);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 12; 
		} catch (Exception $e) {
			throw new PropelException("Error populating DeskripsiResumeRapat object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(DeskripsiResumeRapatPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			DeskripsiResumeRapatPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(DeskripsiResumeRapatPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = DeskripsiResumeRapatPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += DeskripsiResumeRapatPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = DeskripsiResumeRapatPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = DeskripsiResumeRapatPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getUnitId();
				break;
			case 2:
				return $this->getTahap();
				break;
			case 3:
				return $this->getTanggal();
				break;
			case 4:
				return $this->getJam();
				break;
			case 5:
				return $this->getMenit();
				break;
			case 6:
				return $this->getAcara();
				break;
			case 7:
				return $this->getAcara2();
				break;
			case 8:
				return $this->getTempat();
				break;
			case 9:
				return $this->getPimpinan();
				break;
			case 10:
				return $this->getCatatan();
				break;
			case 11:
				return $this->getToken();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = DeskripsiResumeRapatPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getUnitId(),
			$keys[2] => $this->getTahap(),
			$keys[3] => $this->getTanggal(),
			$keys[4] => $this->getJam(),
			$keys[5] => $this->getMenit(),
			$keys[6] => $this->getAcara(),
			$keys[7] => $this->getAcara2(),
			$keys[8] => $this->getTempat(),
			$keys[9] => $this->getPimpinan(),
			$keys[10] => $this->getCatatan(),
			$keys[11] => $this->getToken(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = DeskripsiResumeRapatPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setUnitId($value);
				break;
			case 2:
				$this->setTahap($value);
				break;
			case 3:
				$this->setTanggal($value);
				break;
			case 4:
				$this->setJam($value);
				break;
			case 5:
				$this->setMenit($value);
				break;
			case 6:
				$this->setAcara($value);
				break;
			case 7:
				$this->setAcara2($value);
				break;
			case 8:
				$this->setTempat($value);
				break;
			case 9:
				$this->setPimpinan($value);
				break;
			case 10:
				$this->setCatatan($value);
				break;
			case 11:
				$this->setToken($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = DeskripsiResumeRapatPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setUnitId($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setTahap($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setTanggal($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setJam($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setMenit($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setAcara($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setAcara2($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setTempat($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setPimpinan($arr[$keys[9]]);
		if (array_key_exists($keys[10], $arr)) $this->setCatatan($arr[$keys[10]]);
		if (array_key_exists($keys[11], $arr)) $this->setToken($arr[$keys[11]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(DeskripsiResumeRapatPeer::DATABASE_NAME);

		if ($this->isColumnModified(DeskripsiResumeRapatPeer::ID)) $criteria->add(DeskripsiResumeRapatPeer::ID, $this->id);
		if ($this->isColumnModified(DeskripsiResumeRapatPeer::UNIT_ID)) $criteria->add(DeskripsiResumeRapatPeer::UNIT_ID, $this->unit_id);
		if ($this->isColumnModified(DeskripsiResumeRapatPeer::TAHAP)) $criteria->add(DeskripsiResumeRapatPeer::TAHAP, $this->tahap);
		if ($this->isColumnModified(DeskripsiResumeRapatPeer::TANGGAL)) $criteria->add(DeskripsiResumeRapatPeer::TANGGAL, $this->tanggal);
		if ($this->isColumnModified(DeskripsiResumeRapatPeer::JAM)) $criteria->add(DeskripsiResumeRapatPeer::JAM, $this->jam);
		if ($this->isColumnModified(DeskripsiResumeRapatPeer::MENIT)) $criteria->add(DeskripsiResumeRapatPeer::MENIT, $this->menit);
		if ($this->isColumnModified(DeskripsiResumeRapatPeer::ACARA)) $criteria->add(DeskripsiResumeRapatPeer::ACARA, $this->acara);
		if ($this->isColumnModified(DeskripsiResumeRapatPeer::ACARA2)) $criteria->add(DeskripsiResumeRapatPeer::ACARA2, $this->acara2);
		if ($this->isColumnModified(DeskripsiResumeRapatPeer::TEMPAT)) $criteria->add(DeskripsiResumeRapatPeer::TEMPAT, $this->tempat);
		if ($this->isColumnModified(DeskripsiResumeRapatPeer::PIMPINAN)) $criteria->add(DeskripsiResumeRapatPeer::PIMPINAN, $this->pimpinan);
		if ($this->isColumnModified(DeskripsiResumeRapatPeer::CATATAN)) $criteria->add(DeskripsiResumeRapatPeer::CATATAN, $this->catatan);
		if ($this->isColumnModified(DeskripsiResumeRapatPeer::TOKEN)) $criteria->add(DeskripsiResumeRapatPeer::TOKEN, $this->token);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(DeskripsiResumeRapatPeer::DATABASE_NAME);

		$criteria->add(DeskripsiResumeRapatPeer::ID, $this->id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setUnitId($this->unit_id);

		$copyObj->setTahap($this->tahap);

		$copyObj->setTanggal($this->tanggal);

		$copyObj->setJam($this->jam);

		$copyObj->setMenit($this->menit);

		$copyObj->setAcara($this->acara);

		$copyObj->setAcara2($this->acara2);

		$copyObj->setTempat($this->tempat);

		$copyObj->setPimpinan($this->pimpinan);

		$copyObj->setCatatan($this->catatan);

		$copyObj->setToken($this->token);


		$copyObj->setNew(true);

		$copyObj->setId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new DeskripsiResumeRapatPeer();
		}
		return self::$peer;
	}

} 