<?php


abstract class BaseMasterLokasiPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'master_lokasi';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.MasterLokasi';

	
	const NUM_COLUMNS = 15;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const ID_LOKASI = 'master_lokasi.ID_LOKASI';

	
	const TAHUN = 'master_lokasi.TAHUN';

	
	const STATUS_HAPUS = 'master_lokasi.STATUS_HAPUS';

	
	const JALAN = 'master_lokasi.JALAN';

	
	const GANG = 'master_lokasi.GANG';

	
	const NOMOR = 'master_lokasi.NOMOR';

	
	const RW = 'master_lokasi.RW';

	
	const RT = 'master_lokasi.RT';

	
	const KETERANGAN = 'master_lokasi.KETERANGAN';

	
	const TEMPAT = 'master_lokasi.TEMPAT';

	
	const KECAMATAN = 'master_lokasi.KECAMATAN';

	
	const KELURAHAN = 'master_lokasi.KELURAHAN';

	
	const LOKASI = 'master_lokasi.LOKASI';

	
	const STATUS_VERIFIKASI = 'master_lokasi.STATUS_VERIFIKASI';

	
	const USULAN_SKPD = 'master_lokasi.USULAN_SKPD';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('IdLokasi', 'Tahun', 'StatusHapus', 'Jalan', 'Gang', 'Nomor', 'Rw', 'Rt', 'Keterangan', 'Tempat', 'Kecamatan', 'Kelurahan', 'Lokasi', 'StatusVerifikasi', 'UsulanSkpd', ),
		BasePeer::TYPE_COLNAME => array (MasterLokasiPeer::ID_LOKASI, MasterLokasiPeer::TAHUN, MasterLokasiPeer::STATUS_HAPUS, MasterLokasiPeer::JALAN, MasterLokasiPeer::GANG, MasterLokasiPeer::NOMOR, MasterLokasiPeer::RW, MasterLokasiPeer::RT, MasterLokasiPeer::KETERANGAN, MasterLokasiPeer::TEMPAT, MasterLokasiPeer::KECAMATAN, MasterLokasiPeer::KELURAHAN, MasterLokasiPeer::LOKASI, MasterLokasiPeer::STATUS_VERIFIKASI, MasterLokasiPeer::USULAN_SKPD, ),
		BasePeer::TYPE_FIELDNAME => array ('id_lokasi', 'tahun', 'status_hapus', 'jalan', 'gang', 'nomor', 'rw', 'rt', 'keterangan', 'tempat', 'kecamatan', 'kelurahan', 'lokasi', 'status_verifikasi', 'usulan_skpd', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('IdLokasi' => 0, 'Tahun' => 1, 'StatusHapus' => 2, 'Jalan' => 3, 'Gang' => 4, 'Nomor' => 5, 'Rw' => 6, 'Rt' => 7, 'Keterangan' => 8, 'Tempat' => 9, 'Kecamatan' => 10, 'Kelurahan' => 11, 'Lokasi' => 12, 'StatusVerifikasi' => 13, 'UsulanSkpd' => 14, ),
		BasePeer::TYPE_COLNAME => array (MasterLokasiPeer::ID_LOKASI => 0, MasterLokasiPeer::TAHUN => 1, MasterLokasiPeer::STATUS_HAPUS => 2, MasterLokasiPeer::JALAN => 3, MasterLokasiPeer::GANG => 4, MasterLokasiPeer::NOMOR => 5, MasterLokasiPeer::RW => 6, MasterLokasiPeer::RT => 7, MasterLokasiPeer::KETERANGAN => 8, MasterLokasiPeer::TEMPAT => 9, MasterLokasiPeer::KECAMATAN => 10, MasterLokasiPeer::KELURAHAN => 11, MasterLokasiPeer::LOKASI => 12, MasterLokasiPeer::STATUS_VERIFIKASI => 13, MasterLokasiPeer::USULAN_SKPD => 14, ),
		BasePeer::TYPE_FIELDNAME => array ('id_lokasi' => 0, 'tahun' => 1, 'status_hapus' => 2, 'jalan' => 3, 'gang' => 4, 'nomor' => 5, 'rw' => 6, 'rt' => 7, 'keterangan' => 8, 'tempat' => 9, 'kecamatan' => 10, 'kelurahan' => 11, 'lokasi' => 12, 'status_verifikasi' => 13, 'usulan_skpd' => 14, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/MasterLokasiMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.MasterLokasiMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = MasterLokasiPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(MasterLokasiPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(MasterLokasiPeer::ID_LOKASI);

		$criteria->addSelectColumn(MasterLokasiPeer::TAHUN);

		$criteria->addSelectColumn(MasterLokasiPeer::STATUS_HAPUS);

		$criteria->addSelectColumn(MasterLokasiPeer::JALAN);

		$criteria->addSelectColumn(MasterLokasiPeer::GANG);

		$criteria->addSelectColumn(MasterLokasiPeer::NOMOR);

		$criteria->addSelectColumn(MasterLokasiPeer::RW);

		$criteria->addSelectColumn(MasterLokasiPeer::RT);

		$criteria->addSelectColumn(MasterLokasiPeer::KETERANGAN);

		$criteria->addSelectColumn(MasterLokasiPeer::TEMPAT);

		$criteria->addSelectColumn(MasterLokasiPeer::KECAMATAN);

		$criteria->addSelectColumn(MasterLokasiPeer::KELURAHAN);

		$criteria->addSelectColumn(MasterLokasiPeer::LOKASI);

		$criteria->addSelectColumn(MasterLokasiPeer::STATUS_VERIFIKASI);

		$criteria->addSelectColumn(MasterLokasiPeer::USULAN_SKPD);

	}

	const COUNT = 'COUNT(master_lokasi.ID_LOKASI)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT master_lokasi.ID_LOKASI)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(MasterLokasiPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(MasterLokasiPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = MasterLokasiPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = MasterLokasiPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return MasterLokasiPeer::populateObjects(MasterLokasiPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			MasterLokasiPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = MasterLokasiPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return MasterLokasiPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(MasterLokasiPeer::ID_LOKASI);
			$selectCriteria->add(MasterLokasiPeer::ID_LOKASI, $criteria->remove(MasterLokasiPeer::ID_LOKASI), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(MasterLokasiPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(MasterLokasiPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof MasterLokasi) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(MasterLokasiPeer::ID_LOKASI, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(MasterLokasi $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(MasterLokasiPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(MasterLokasiPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(MasterLokasiPeer::DATABASE_NAME, MasterLokasiPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = MasterLokasiPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(MasterLokasiPeer::DATABASE_NAME);

		$criteria->add(MasterLokasiPeer::ID_LOKASI, $pk);


		$v = MasterLokasiPeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(MasterLokasiPeer::ID_LOKASI, $pks, Criteria::IN);
			$objs = MasterLokasiPeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BaseMasterLokasiPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/MasterLokasiMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.MasterLokasiMapBuilder');
}
