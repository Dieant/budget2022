<?php


abstract class BaseGeojsonlokasiWaitinglist extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $idgeolokasi;


	
	protected $unit_id;


	
	protected $kegiatan_code;


	
	protected $id_waiting;


	
	protected $satuan;


	
	protected $volume;


	
	protected $nilai_anggaran;


	
	protected $tahun;


	
	protected $mlokasi;


	
	protected $id_kelompok;


	
	protected $geojson;


	
	protected $keterangan;


	
	protected $nmuser;


	
	protected $level;


	
	protected $komponen_name;


	
	protected $status_hapus;


	
	protected $keterangan_alamat;


	
	protected $unit_name;


	
	protected $last_edit_time;


	
	protected $last_create_time;


	
	protected $koordinat;


	
	protected $lokasi_ke;


	
	protected $kode_detail_kegiatan;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getIdgeolokasi()
	{

		return $this->idgeolokasi;
	}

	
	public function getUnitId()
	{

		return $this->unit_id;
	}

	
	public function getKegiatanCode()
	{

		return $this->kegiatan_code;
	}

	
	public function getIdWaiting()
	{

		return $this->id_waiting;
	}

	
	public function getSatuan()
	{

		return $this->satuan;
	}

	
	public function getVolume()
	{

		return $this->volume;
	}

	
	public function getNilaiAnggaran()
	{

		return $this->nilai_anggaran;
	}

	
	public function getTahun()
	{

		return $this->tahun;
	}

	
	public function getMlokasi()
	{

		return $this->mlokasi;
	}

	
	public function getIdKelompok()
	{

		return $this->id_kelompok;
	}

	
	public function getGeojson()
	{

		return $this->geojson;
	}

	
	public function getKeterangan()
	{

		return $this->keterangan;
	}

	
	public function getNmuser()
	{

		return $this->nmuser;
	}

	
	public function getLevel()
	{

		return $this->level;
	}

	
	public function getKomponenName()
	{

		return $this->komponen_name;
	}

	
	public function getStatusHapus()
	{

		return $this->status_hapus;
	}

	
	public function getKeteranganAlamat()
	{

		return $this->keterangan_alamat;
	}

	
	public function getUnitName()
	{

		return $this->unit_name;
	}

	
	public function getLastEditTime($format = 'Y-m-d H:i:s')
	{

		if ($this->last_edit_time === null || $this->last_edit_time === '') {
			return null;
		} elseif (!is_int($this->last_edit_time)) {
						$ts = strtotime($this->last_edit_time);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [last_edit_time] as date/time value: " . var_export($this->last_edit_time, true));
			}
		} else {
			$ts = $this->last_edit_time;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getLastCreateTime($format = 'Y-m-d H:i:s')
	{

		if ($this->last_create_time === null || $this->last_create_time === '') {
			return null;
		} elseif (!is_int($this->last_create_time)) {
						$ts = strtotime($this->last_create_time);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [last_create_time] as date/time value: " . var_export($this->last_create_time, true));
			}
		} else {
			$ts = $this->last_create_time;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getKoordinat()
	{

		return $this->koordinat;
	}

	
	public function getLokasiKe()
	{

		return $this->lokasi_ke;
	}

	
	public function getKodeDetailKegiatan()
	{

		return $this->kode_detail_kegiatan;
	}

	
	public function setIdgeolokasi($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->idgeolokasi !== $v) {
			$this->idgeolokasi = $v;
			$this->modifiedColumns[] = GeojsonlokasiWaitinglistPeer::IDGEOLOKASI;
		}

	} 
	
	public function setUnitId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->unit_id !== $v) {
			$this->unit_id = $v;
			$this->modifiedColumns[] = GeojsonlokasiWaitinglistPeer::UNIT_ID;
		}

	} 
	
	public function setKegiatanCode($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kegiatan_code !== $v) {
			$this->kegiatan_code = $v;
			$this->modifiedColumns[] = GeojsonlokasiWaitinglistPeer::KEGIATAN_CODE;
		}

	} 
	
	public function setIdWaiting($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id_waiting !== $v) {
			$this->id_waiting = $v;
			$this->modifiedColumns[] = GeojsonlokasiWaitinglistPeer::ID_WAITING;
		}

	} 
	
	public function setSatuan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->satuan !== $v) {
			$this->satuan = $v;
			$this->modifiedColumns[] = GeojsonlokasiWaitinglistPeer::SATUAN;
		}

	} 
	
	public function setVolume($v)
	{

		if ($this->volume !== $v) {
			$this->volume = $v;
			$this->modifiedColumns[] = GeojsonlokasiWaitinglistPeer::VOLUME;
		}

	} 
	
	public function setNilaiAnggaran($v)
	{

		if ($this->nilai_anggaran !== $v) {
			$this->nilai_anggaran = $v;
			$this->modifiedColumns[] = GeojsonlokasiWaitinglistPeer::NILAI_ANGGARAN;
		}

	} 
	
	public function setTahun($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->tahun !== $v) {
			$this->tahun = $v;
			$this->modifiedColumns[] = GeojsonlokasiWaitinglistPeer::TAHUN;
		}

	} 
	
	public function setMlokasi($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->mlokasi !== $v) {
			$this->mlokasi = $v;
			$this->modifiedColumns[] = GeojsonlokasiWaitinglistPeer::MLOKASI;
		}

	} 
	
	public function setIdKelompok($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id_kelompok !== $v) {
			$this->id_kelompok = $v;
			$this->modifiedColumns[] = GeojsonlokasiWaitinglistPeer::ID_KELOMPOK;
		}

	} 
	
	public function setGeojson($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->geojson !== $v) {
			$this->geojson = $v;
			$this->modifiedColumns[] = GeojsonlokasiWaitinglistPeer::GEOJSON;
		}

	} 
	
	public function setKeterangan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->keterangan !== $v) {
			$this->keterangan = $v;
			$this->modifiedColumns[] = GeojsonlokasiWaitinglistPeer::KETERANGAN;
		}

	} 
	
	public function setNmuser($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nmuser !== $v) {
			$this->nmuser = $v;
			$this->modifiedColumns[] = GeojsonlokasiWaitinglistPeer::NMUSER;
		}

	} 
	
	public function setLevel($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->level !== $v) {
			$this->level = $v;
			$this->modifiedColumns[] = GeojsonlokasiWaitinglistPeer::LEVEL;
		}

	} 
	
	public function setKomponenName($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->komponen_name !== $v) {
			$this->komponen_name = $v;
			$this->modifiedColumns[] = GeojsonlokasiWaitinglistPeer::KOMPONEN_NAME;
		}

	} 
	
	public function setStatusHapus($v)
	{

		if ($this->status_hapus !== $v) {
			$this->status_hapus = $v;
			$this->modifiedColumns[] = GeojsonlokasiWaitinglistPeer::STATUS_HAPUS;
		}

	} 
	
	public function setKeteranganAlamat($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->keterangan_alamat !== $v) {
			$this->keterangan_alamat = $v;
			$this->modifiedColumns[] = GeojsonlokasiWaitinglistPeer::KETERANGAN_ALAMAT;
		}

	} 
	
	public function setUnitName($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->unit_name !== $v) {
			$this->unit_name = $v;
			$this->modifiedColumns[] = GeojsonlokasiWaitinglistPeer::UNIT_NAME;
		}

	} 
	
	public function setLastEditTime($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [last_edit_time] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->last_edit_time !== $ts) {
			$this->last_edit_time = $ts;
			$this->modifiedColumns[] = GeojsonlokasiWaitinglistPeer::LAST_EDIT_TIME;
		}

	} 
	
	public function setLastCreateTime($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [last_create_time] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->last_create_time !== $ts) {
			$this->last_create_time = $ts;
			$this->modifiedColumns[] = GeojsonlokasiWaitinglistPeer::LAST_CREATE_TIME;
		}

	} 
	
	public function setKoordinat($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->koordinat !== $v) {
			$this->koordinat = $v;
			$this->modifiedColumns[] = GeojsonlokasiWaitinglistPeer::KOORDINAT;
		}

	} 
	
	public function setLokasiKe($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->lokasi_ke !== $v) {
			$this->lokasi_ke = $v;
			$this->modifiedColumns[] = GeojsonlokasiWaitinglistPeer::LOKASI_KE;
		}

	} 
	
	public function setKodeDetailKegiatan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_detail_kegiatan !== $v) {
			$this->kode_detail_kegiatan = $v;
			$this->modifiedColumns[] = GeojsonlokasiWaitinglistPeer::KODE_DETAIL_KEGIATAN;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->idgeolokasi = $rs->getInt($startcol + 0);

			$this->unit_id = $rs->getString($startcol + 1);

			$this->kegiatan_code = $rs->getString($startcol + 2);

			$this->id_waiting = $rs->getInt($startcol + 3);

			$this->satuan = $rs->getString($startcol + 4);

			$this->volume = $rs->getFloat($startcol + 5);

			$this->nilai_anggaran = $rs->getFloat($startcol + 6);

			$this->tahun = $rs->getString($startcol + 7);

			$this->mlokasi = $rs->getString($startcol + 8);

			$this->id_kelompok = $rs->getInt($startcol + 9);

			$this->geojson = $rs->getString($startcol + 10);

			$this->keterangan = $rs->getString($startcol + 11);

			$this->nmuser = $rs->getString($startcol + 12);

			$this->level = $rs->getInt($startcol + 13);

			$this->komponen_name = $rs->getString($startcol + 14);

			$this->status_hapus = $rs->getBoolean($startcol + 15);

			$this->keterangan_alamat = $rs->getString($startcol + 16);

			$this->unit_name = $rs->getString($startcol + 17);

			$this->last_edit_time = $rs->getTimestamp($startcol + 18, null);

			$this->last_create_time = $rs->getTimestamp($startcol + 19, null);

			$this->koordinat = $rs->getString($startcol + 20);

			$this->lokasi_ke = $rs->getInt($startcol + 21);

			$this->kode_detail_kegiatan = $rs->getString($startcol + 22);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 23; 
		} catch (Exception $e) {
			throw new PropelException("Error populating GeojsonlokasiWaitinglist object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(GeojsonlokasiWaitinglistPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			GeojsonlokasiWaitinglistPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(GeojsonlokasiWaitinglistPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = GeojsonlokasiWaitinglistPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += GeojsonlokasiWaitinglistPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = GeojsonlokasiWaitinglistPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = GeojsonlokasiWaitinglistPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getIdgeolokasi();
				break;
			case 1:
				return $this->getUnitId();
				break;
			case 2:
				return $this->getKegiatanCode();
				break;
			case 3:
				return $this->getIdWaiting();
				break;
			case 4:
				return $this->getSatuan();
				break;
			case 5:
				return $this->getVolume();
				break;
			case 6:
				return $this->getNilaiAnggaran();
				break;
			case 7:
				return $this->getTahun();
				break;
			case 8:
				return $this->getMlokasi();
				break;
			case 9:
				return $this->getIdKelompok();
				break;
			case 10:
				return $this->getGeojson();
				break;
			case 11:
				return $this->getKeterangan();
				break;
			case 12:
				return $this->getNmuser();
				break;
			case 13:
				return $this->getLevel();
				break;
			case 14:
				return $this->getKomponenName();
				break;
			case 15:
				return $this->getStatusHapus();
				break;
			case 16:
				return $this->getKeteranganAlamat();
				break;
			case 17:
				return $this->getUnitName();
				break;
			case 18:
				return $this->getLastEditTime();
				break;
			case 19:
				return $this->getLastCreateTime();
				break;
			case 20:
				return $this->getKoordinat();
				break;
			case 21:
				return $this->getLokasiKe();
				break;
			case 22:
				return $this->getKodeDetailKegiatan();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = GeojsonlokasiWaitinglistPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getIdgeolokasi(),
			$keys[1] => $this->getUnitId(),
			$keys[2] => $this->getKegiatanCode(),
			$keys[3] => $this->getIdWaiting(),
			$keys[4] => $this->getSatuan(),
			$keys[5] => $this->getVolume(),
			$keys[6] => $this->getNilaiAnggaran(),
			$keys[7] => $this->getTahun(),
			$keys[8] => $this->getMlokasi(),
			$keys[9] => $this->getIdKelompok(),
			$keys[10] => $this->getGeojson(),
			$keys[11] => $this->getKeterangan(),
			$keys[12] => $this->getNmuser(),
			$keys[13] => $this->getLevel(),
			$keys[14] => $this->getKomponenName(),
			$keys[15] => $this->getStatusHapus(),
			$keys[16] => $this->getKeteranganAlamat(),
			$keys[17] => $this->getUnitName(),
			$keys[18] => $this->getLastEditTime(),
			$keys[19] => $this->getLastCreateTime(),
			$keys[20] => $this->getKoordinat(),
			$keys[21] => $this->getLokasiKe(),
			$keys[22] => $this->getKodeDetailKegiatan(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = GeojsonlokasiWaitinglistPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setIdgeolokasi($value);
				break;
			case 1:
				$this->setUnitId($value);
				break;
			case 2:
				$this->setKegiatanCode($value);
				break;
			case 3:
				$this->setIdWaiting($value);
				break;
			case 4:
				$this->setSatuan($value);
				break;
			case 5:
				$this->setVolume($value);
				break;
			case 6:
				$this->setNilaiAnggaran($value);
				break;
			case 7:
				$this->setTahun($value);
				break;
			case 8:
				$this->setMlokasi($value);
				break;
			case 9:
				$this->setIdKelompok($value);
				break;
			case 10:
				$this->setGeojson($value);
				break;
			case 11:
				$this->setKeterangan($value);
				break;
			case 12:
				$this->setNmuser($value);
				break;
			case 13:
				$this->setLevel($value);
				break;
			case 14:
				$this->setKomponenName($value);
				break;
			case 15:
				$this->setStatusHapus($value);
				break;
			case 16:
				$this->setKeteranganAlamat($value);
				break;
			case 17:
				$this->setUnitName($value);
				break;
			case 18:
				$this->setLastEditTime($value);
				break;
			case 19:
				$this->setLastCreateTime($value);
				break;
			case 20:
				$this->setKoordinat($value);
				break;
			case 21:
				$this->setLokasiKe($value);
				break;
			case 22:
				$this->setKodeDetailKegiatan($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = GeojsonlokasiWaitinglistPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setIdgeolokasi($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setUnitId($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setKegiatanCode($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setIdWaiting($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setSatuan($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setVolume($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setNilaiAnggaran($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setTahun($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setMlokasi($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setIdKelompok($arr[$keys[9]]);
		if (array_key_exists($keys[10], $arr)) $this->setGeojson($arr[$keys[10]]);
		if (array_key_exists($keys[11], $arr)) $this->setKeterangan($arr[$keys[11]]);
		if (array_key_exists($keys[12], $arr)) $this->setNmuser($arr[$keys[12]]);
		if (array_key_exists($keys[13], $arr)) $this->setLevel($arr[$keys[13]]);
		if (array_key_exists($keys[14], $arr)) $this->setKomponenName($arr[$keys[14]]);
		if (array_key_exists($keys[15], $arr)) $this->setStatusHapus($arr[$keys[15]]);
		if (array_key_exists($keys[16], $arr)) $this->setKeteranganAlamat($arr[$keys[16]]);
		if (array_key_exists($keys[17], $arr)) $this->setUnitName($arr[$keys[17]]);
		if (array_key_exists($keys[18], $arr)) $this->setLastEditTime($arr[$keys[18]]);
		if (array_key_exists($keys[19], $arr)) $this->setLastCreateTime($arr[$keys[19]]);
		if (array_key_exists($keys[20], $arr)) $this->setKoordinat($arr[$keys[20]]);
		if (array_key_exists($keys[21], $arr)) $this->setLokasiKe($arr[$keys[21]]);
		if (array_key_exists($keys[22], $arr)) $this->setKodeDetailKegiatan($arr[$keys[22]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(GeojsonlokasiWaitinglistPeer::DATABASE_NAME);

		if ($this->isColumnModified(GeojsonlokasiWaitinglistPeer::IDGEOLOKASI)) $criteria->add(GeojsonlokasiWaitinglistPeer::IDGEOLOKASI, $this->idgeolokasi);
		if ($this->isColumnModified(GeojsonlokasiWaitinglistPeer::UNIT_ID)) $criteria->add(GeojsonlokasiWaitinglistPeer::UNIT_ID, $this->unit_id);
		if ($this->isColumnModified(GeojsonlokasiWaitinglistPeer::KEGIATAN_CODE)) $criteria->add(GeojsonlokasiWaitinglistPeer::KEGIATAN_CODE, $this->kegiatan_code);
		if ($this->isColumnModified(GeojsonlokasiWaitinglistPeer::ID_WAITING)) $criteria->add(GeojsonlokasiWaitinglistPeer::ID_WAITING, $this->id_waiting);
		if ($this->isColumnModified(GeojsonlokasiWaitinglistPeer::SATUAN)) $criteria->add(GeojsonlokasiWaitinglistPeer::SATUAN, $this->satuan);
		if ($this->isColumnModified(GeojsonlokasiWaitinglistPeer::VOLUME)) $criteria->add(GeojsonlokasiWaitinglistPeer::VOLUME, $this->volume);
		if ($this->isColumnModified(GeojsonlokasiWaitinglistPeer::NILAI_ANGGARAN)) $criteria->add(GeojsonlokasiWaitinglistPeer::NILAI_ANGGARAN, $this->nilai_anggaran);
		if ($this->isColumnModified(GeojsonlokasiWaitinglistPeer::TAHUN)) $criteria->add(GeojsonlokasiWaitinglistPeer::TAHUN, $this->tahun);
		if ($this->isColumnModified(GeojsonlokasiWaitinglistPeer::MLOKASI)) $criteria->add(GeojsonlokasiWaitinglistPeer::MLOKASI, $this->mlokasi);
		if ($this->isColumnModified(GeojsonlokasiWaitinglistPeer::ID_KELOMPOK)) $criteria->add(GeojsonlokasiWaitinglistPeer::ID_KELOMPOK, $this->id_kelompok);
		if ($this->isColumnModified(GeojsonlokasiWaitinglistPeer::GEOJSON)) $criteria->add(GeojsonlokasiWaitinglistPeer::GEOJSON, $this->geojson);
		if ($this->isColumnModified(GeojsonlokasiWaitinglistPeer::KETERANGAN)) $criteria->add(GeojsonlokasiWaitinglistPeer::KETERANGAN, $this->keterangan);
		if ($this->isColumnModified(GeojsonlokasiWaitinglistPeer::NMUSER)) $criteria->add(GeojsonlokasiWaitinglistPeer::NMUSER, $this->nmuser);
		if ($this->isColumnModified(GeojsonlokasiWaitinglistPeer::LEVEL)) $criteria->add(GeojsonlokasiWaitinglistPeer::LEVEL, $this->level);
		if ($this->isColumnModified(GeojsonlokasiWaitinglistPeer::KOMPONEN_NAME)) $criteria->add(GeojsonlokasiWaitinglistPeer::KOMPONEN_NAME, $this->komponen_name);
		if ($this->isColumnModified(GeojsonlokasiWaitinglistPeer::STATUS_HAPUS)) $criteria->add(GeojsonlokasiWaitinglistPeer::STATUS_HAPUS, $this->status_hapus);
		if ($this->isColumnModified(GeojsonlokasiWaitinglistPeer::KETERANGAN_ALAMAT)) $criteria->add(GeojsonlokasiWaitinglistPeer::KETERANGAN_ALAMAT, $this->keterangan_alamat);
		if ($this->isColumnModified(GeojsonlokasiWaitinglistPeer::UNIT_NAME)) $criteria->add(GeojsonlokasiWaitinglistPeer::UNIT_NAME, $this->unit_name);
		if ($this->isColumnModified(GeojsonlokasiWaitinglistPeer::LAST_EDIT_TIME)) $criteria->add(GeojsonlokasiWaitinglistPeer::LAST_EDIT_TIME, $this->last_edit_time);
		if ($this->isColumnModified(GeojsonlokasiWaitinglistPeer::LAST_CREATE_TIME)) $criteria->add(GeojsonlokasiWaitinglistPeer::LAST_CREATE_TIME, $this->last_create_time);
		if ($this->isColumnModified(GeojsonlokasiWaitinglistPeer::KOORDINAT)) $criteria->add(GeojsonlokasiWaitinglistPeer::KOORDINAT, $this->koordinat);
		if ($this->isColumnModified(GeojsonlokasiWaitinglistPeer::LOKASI_KE)) $criteria->add(GeojsonlokasiWaitinglistPeer::LOKASI_KE, $this->lokasi_ke);
		if ($this->isColumnModified(GeojsonlokasiWaitinglistPeer::KODE_DETAIL_KEGIATAN)) $criteria->add(GeojsonlokasiWaitinglistPeer::KODE_DETAIL_KEGIATAN, $this->kode_detail_kegiatan);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(GeojsonlokasiWaitinglistPeer::DATABASE_NAME);

		$criteria->add(GeojsonlokasiWaitinglistPeer::IDGEOLOKASI, $this->idgeolokasi);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getIdgeolokasi();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setIdgeolokasi($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setUnitId($this->unit_id);

		$copyObj->setKegiatanCode($this->kegiatan_code);

		$copyObj->setIdWaiting($this->id_waiting);

		$copyObj->setSatuan($this->satuan);

		$copyObj->setVolume($this->volume);

		$copyObj->setNilaiAnggaran($this->nilai_anggaran);

		$copyObj->setTahun($this->tahun);

		$copyObj->setMlokasi($this->mlokasi);

		$copyObj->setIdKelompok($this->id_kelompok);

		$copyObj->setGeojson($this->geojson);

		$copyObj->setKeterangan($this->keterangan);

		$copyObj->setNmuser($this->nmuser);

		$copyObj->setLevel($this->level);

		$copyObj->setKomponenName($this->komponen_name);

		$copyObj->setStatusHapus($this->status_hapus);

		$copyObj->setKeteranganAlamat($this->keterangan_alamat);

		$copyObj->setUnitName($this->unit_name);

		$copyObj->setLastEditTime($this->last_edit_time);

		$copyObj->setLastCreateTime($this->last_create_time);

		$copyObj->setKoordinat($this->koordinat);

		$copyObj->setLokasiKe($this->lokasi_ke);

		$copyObj->setKodeDetailKegiatan($this->kode_detail_kegiatan);


		$copyObj->setNew(true);

		$copyObj->setIdgeolokasi(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new GeojsonlokasiWaitinglistPeer();
		}
		return self::$peer;
	}

} 