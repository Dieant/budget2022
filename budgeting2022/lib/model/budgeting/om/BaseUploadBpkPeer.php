<?php


abstract class BaseUploadBpkPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.log_request_penyelia';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.UploadBpk';

	
	const NUM_COLUMNS = 12;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const ID = 'ebudget.log_request_penyelia.ID';

	
	const UNIT_ID = 'ebudget.log_request_penyelia.UNIT_ID';

	
	const KEGIATAN_CODE = 'ebudget.log_request_penyelia.KEGIATAN_CODE';

	
	const TIPE = 'ebudget.log_request_penyelia.TIPE';

	
	const PATH = 'ebudget.log_request_penyelia.PATH';

	
	const PENYELIA = 'ebudget.log_request_penyelia.PENYELIA';

	
	const CREATED_AT = 'ebudget.log_request_penyelia.CREATED_AT';

	
	const UPDATED_AT = 'ebudget.log_request_penyelia.UPDATED_AT';

	
	const CATATAN = 'ebudget.log_request_penyelia.CATATAN';

	
	const IS_KHUSUS = 'ebudget.log_request_penyelia.IS_KHUSUS';

	
	const STATUS = 'ebudget.log_request_penyelia.STATUS';

	
	const CATATAN_TOLAK = 'ebudget.log_request_penyelia.CATATAN_TOLAK';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('Id', 'UnitId', 'KegiatanCode', 'Tipe', 'Path', 'Penyelia', 'CreatedAt', 'UpdatedAt', 'Catatan', 'IsKhusus', 'Status', 'CatatanTolak', ),
		BasePeer::TYPE_COLNAME => array (UploadBpkPeer::ID, UploadBpkPeer::UNIT_ID, UploadBpkPeer::KEGIATAN_CODE, UploadBpkPeer::TIPE, UploadBpkPeer::PATH, UploadBpkPeer::PENYELIA, UploadBpkPeer::CREATED_AT, UploadBpkPeer::UPDATED_AT, UploadBpkPeer::CATATAN, UploadBpkPeer::IS_KHUSUS, UploadBpkPeer::STATUS, UploadBpkPeer::CATATAN_TOLAK, ),
		BasePeer::TYPE_FIELDNAME => array ('id', 'unit_id', 'kegiatan_code', 'tipe', 'path', 'penyelia', 'created_at', 'updated_at', 'catatan', 'is_khusus', 'status', 'catatan_tolak', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('Id' => 0, 'UnitId' => 1, 'KegiatanCode' => 2, 'Tipe' => 3, 'Path' => 4, 'Penyelia' => 5, 'CreatedAt' => 6, 'UpdatedAt' => 7, 'Catatan' => 8, 'IsKhusus' => 9, 'Status' => 10, 'CatatanTolak' => 11, ),
		BasePeer::TYPE_COLNAME => array (UploadBpkPeer::ID => 0, UploadBpkPeer::UNIT_ID => 1, UploadBpkPeer::KEGIATAN_CODE => 2, UploadBpkPeer::TIPE => 3, UploadBpkPeer::PATH => 4, UploadBpkPeer::PENYELIA => 5, UploadBpkPeer::CREATED_AT => 6, UploadBpkPeer::UPDATED_AT => 7, UploadBpkPeer::CATATAN => 8, UploadBpkPeer::IS_KHUSUS => 9, UploadBpkPeer::STATUS => 10, UploadBpkPeer::CATATAN_TOLAK => 11, ),
		BasePeer::TYPE_FIELDNAME => array ('id' => 0, 'unit_id' => 1, 'kegiatan_code' => 2, 'tipe' => 3, 'path' => 4, 'penyelia' => 5, 'created_at' => 6, 'updated_at' => 7, 'catatan' => 8, 'is_khusus' => 9, 'status' => 10, 'catatan_tolak' => 11, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/UploadBpkMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.UploadBpkMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = UploadBpkPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(UploadBpkPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(UploadBpkPeer::ID);

		$criteria->addSelectColumn(UploadBpkPeer::UNIT_ID);

		$criteria->addSelectColumn(UploadBpkPeer::KEGIATAN_CODE);

		$criteria->addSelectColumn(UploadBpkPeer::TIPE);

		$criteria->addSelectColumn(UploadBpkPeer::PATH);

		$criteria->addSelectColumn(UploadBpkPeer::PENYELIA);

		$criteria->addSelectColumn(UploadBpkPeer::CREATED_AT);

		$criteria->addSelectColumn(UploadBpkPeer::UPDATED_AT);

		$criteria->addSelectColumn(UploadBpkPeer::CATATAN);

		$criteria->addSelectColumn(UploadBpkPeer::IS_KHUSUS);

		$criteria->addSelectColumn(UploadBpkPeer::STATUS);

		$criteria->addSelectColumn(UploadBpkPeer::CATATAN_TOLAK);

	}

	const COUNT = 'COUNT(ebudget.log_request_penyelia.ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.log_request_penyelia.ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(UploadBpkPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(UploadBpkPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = UploadBpkPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = UploadBpkPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return UploadBpkPeer::populateObjects(UploadBpkPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			UploadBpkPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = UploadBpkPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return UploadBpkPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(UploadBpkPeer::ID);
			$selectCriteria->add(UploadBpkPeer::ID, $criteria->remove(UploadBpkPeer::ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(UploadBpkPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(UploadBpkPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof UploadBpk) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(UploadBpkPeer::ID, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(UploadBpk $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(UploadBpkPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(UploadBpkPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(UploadBpkPeer::DATABASE_NAME, UploadBpkPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = UploadBpkPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(UploadBpkPeer::DATABASE_NAME);

		$criteria->add(UploadBpkPeer::ID, $pk);


		$v = UploadBpkPeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(UploadBpkPeer::ID, $pks, Criteria::IN);
			$objs = UploadBpkPeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BaseUploadBpkPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/UploadBpkMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.UploadBpkMapBuilder');
}
