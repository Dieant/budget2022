<?php


abstract class BaseUsulanVerifikasi extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id_verifikasi;


	
	protected $shsd_id;


	
	protected $shsd_name;


	
	protected $harga;


	
	protected $nama;


	
	protected $spec;


	
	protected $hidden_spec;


	
	protected $merek;


	
	protected $satuan;


	
	protected $rekening;


	
	protected $pajak;


	
	protected $keterangan;


	
	protected $id_spjm;


	
	protected $id_usulan;


	
	protected $created_at;


	
	protected $updated_at;


	
	protected $unit_id;


	
	protected $tipe;


	
	protected $tahap;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getIdVerifikasi()
	{

		return $this->id_verifikasi;
	}

	
	public function getShsdId()
	{

		return $this->shsd_id;
	}

	
	public function getShsdName()
	{

		return $this->shsd_name;
	}

	
	public function getHarga()
	{

		return $this->harga;
	}

	
	public function getNama()
	{

		return $this->nama;
	}

	
	public function getSpec()
	{

		return $this->spec;
	}

	
	public function getHiddenSpec()
	{

		return $this->hidden_spec;
	}

	
	public function getMerek()
	{

		return $this->merek;
	}

	
	public function getSatuan()
	{

		return $this->satuan;
	}

	
	public function getRekening()
	{

		return $this->rekening;
	}

	
	public function getPajak()
	{

		return $this->pajak;
	}

	
	public function getKeterangan()
	{

		return $this->keterangan;
	}

	
	public function getIdSpjm()
	{

		return $this->id_spjm;
	}

	
	public function getIdUsulan()
	{

		return $this->id_usulan;
	}

	
	public function getCreatedAt($format = 'Y-m-d H:i:s')
	{

		if ($this->created_at === null || $this->created_at === '') {
			return null;
		} elseif (!is_int($this->created_at)) {
						$ts = strtotime($this->created_at);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [created_at] as date/time value: " . var_export($this->created_at, true));
			}
		} else {
			$ts = $this->created_at;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getUpdatedAt($format = 'Y-m-d H:i:s')
	{

		if ($this->updated_at === null || $this->updated_at === '') {
			return null;
		} elseif (!is_int($this->updated_at)) {
						$ts = strtotime($this->updated_at);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [updated_at] as date/time value: " . var_export($this->updated_at, true));
			}
		} else {
			$ts = $this->updated_at;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getUnitId()
	{

		return $this->unit_id;
	}

	
	public function getTipe()
	{

		return $this->tipe;
	}

	
	public function getTahap()
	{

		return $this->tahap;
	}

	
	public function setIdVerifikasi($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id_verifikasi !== $v) {
			$this->id_verifikasi = $v;
			$this->modifiedColumns[] = UsulanVerifikasiPeer::ID_VERIFIKASI;
		}

	} 
	
	public function setShsdId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->shsd_id !== $v) {
			$this->shsd_id = $v;
			$this->modifiedColumns[] = UsulanVerifikasiPeer::SHSD_ID;
		}

	} 
	
	public function setShsdName($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->shsd_name !== $v) {
			$this->shsd_name = $v;
			$this->modifiedColumns[] = UsulanVerifikasiPeer::SHSD_NAME;
		}

	} 
	
	public function setHarga($v)
	{

		if ($this->harga !== $v) {
			$this->harga = $v;
			$this->modifiedColumns[] = UsulanVerifikasiPeer::HARGA;
		}

	} 
	
	public function setNama($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nama !== $v) {
			$this->nama = $v;
			$this->modifiedColumns[] = UsulanVerifikasiPeer::NAMA;
		}

	} 
	
	public function setSpec($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->spec !== $v) {
			$this->spec = $v;
			$this->modifiedColumns[] = UsulanVerifikasiPeer::SPEC;
		}

	} 
	
	public function setHiddenSpec($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->hidden_spec !== $v) {
			$this->hidden_spec = $v;
			$this->modifiedColumns[] = UsulanVerifikasiPeer::HIDDEN_SPEC;
		}

	} 
	
	public function setMerek($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->merek !== $v) {
			$this->merek = $v;
			$this->modifiedColumns[] = UsulanVerifikasiPeer::MEREK;
		}

	} 
	
	public function setSatuan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->satuan !== $v) {
			$this->satuan = $v;
			$this->modifiedColumns[] = UsulanVerifikasiPeer::SATUAN;
		}

	} 
	
	public function setRekening($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->rekening !== $v) {
			$this->rekening = $v;
			$this->modifiedColumns[] = UsulanVerifikasiPeer::REKENING;
		}

	} 
	
	public function setPajak($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->pajak !== $v) {
			$this->pajak = $v;
			$this->modifiedColumns[] = UsulanVerifikasiPeer::PAJAK;
		}

	} 
	
	public function setKeterangan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->keterangan !== $v) {
			$this->keterangan = $v;
			$this->modifiedColumns[] = UsulanVerifikasiPeer::KETERANGAN;
		}

	} 
	
	public function setIdSpjm($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id_spjm !== $v) {
			$this->id_spjm = $v;
			$this->modifiedColumns[] = UsulanVerifikasiPeer::ID_SPJM;
		}

	} 
	
	public function setIdUsulan($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id_usulan !== $v) {
			$this->id_usulan = $v;
			$this->modifiedColumns[] = UsulanVerifikasiPeer::ID_USULAN;
		}

	} 
	
	public function setCreatedAt($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [created_at] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->created_at !== $ts) {
			$this->created_at = $ts;
			$this->modifiedColumns[] = UsulanVerifikasiPeer::CREATED_AT;
		}

	} 
	
	public function setUpdatedAt($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [updated_at] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->updated_at !== $ts) {
			$this->updated_at = $ts;
			$this->modifiedColumns[] = UsulanVerifikasiPeer::UPDATED_AT;
		}

	} 
	
	public function setUnitId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->unit_id !== $v) {
			$this->unit_id = $v;
			$this->modifiedColumns[] = UsulanVerifikasiPeer::UNIT_ID;
		}

	} 
	
	public function setTipe($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->tipe !== $v) {
			$this->tipe = $v;
			$this->modifiedColumns[] = UsulanVerifikasiPeer::TIPE;
		}

	} 
	
	public function setTahap($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->tahap !== $v) {
			$this->tahap = $v;
			$this->modifiedColumns[] = UsulanVerifikasiPeer::TAHAP;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id_verifikasi = $rs->getInt($startcol + 0);

			$this->shsd_id = $rs->getString($startcol + 1);

			$this->shsd_name = $rs->getString($startcol + 2);

			$this->harga = $rs->getFloat($startcol + 3);

			$this->nama = $rs->getString($startcol + 4);

			$this->spec = $rs->getString($startcol + 5);

			$this->hidden_spec = $rs->getString($startcol + 6);

			$this->merek = $rs->getString($startcol + 7);

			$this->satuan = $rs->getString($startcol + 8);

			$this->rekening = $rs->getString($startcol + 9);

			$this->pajak = $rs->getInt($startcol + 10);

			$this->keterangan = $rs->getString($startcol + 11);

			$this->id_spjm = $rs->getInt($startcol + 12);

			$this->id_usulan = $rs->getInt($startcol + 13);

			$this->created_at = $rs->getTimestamp($startcol + 14, null);

			$this->updated_at = $rs->getTimestamp($startcol + 15, null);

			$this->unit_id = $rs->getString($startcol + 16);

			$this->tipe = $rs->getString($startcol + 17);

			$this->tahap = $rs->getString($startcol + 18);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 19; 
		} catch (Exception $e) {
			throw new PropelException("Error populating UsulanVerifikasi object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(UsulanVerifikasiPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			UsulanVerifikasiPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
    if ($this->isNew() && !$this->isColumnModified(UsulanVerifikasiPeer::CREATED_AT))
    {
      $this->setCreatedAt(time());
    }

    if ($this->isModified() && !$this->isColumnModified(UsulanVerifikasiPeer::UPDATED_AT))
    {
      $this->setUpdatedAt(time());
    }

		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(UsulanVerifikasiPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = UsulanVerifikasiPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += UsulanVerifikasiPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = UsulanVerifikasiPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = UsulanVerifikasiPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getIdVerifikasi();
				break;
			case 1:
				return $this->getShsdId();
				break;
			case 2:
				return $this->getShsdName();
				break;
			case 3:
				return $this->getHarga();
				break;
			case 4:
				return $this->getNama();
				break;
			case 5:
				return $this->getSpec();
				break;
			case 6:
				return $this->getHiddenSpec();
				break;
			case 7:
				return $this->getMerek();
				break;
			case 8:
				return $this->getSatuan();
				break;
			case 9:
				return $this->getRekening();
				break;
			case 10:
				return $this->getPajak();
				break;
			case 11:
				return $this->getKeterangan();
				break;
			case 12:
				return $this->getIdSpjm();
				break;
			case 13:
				return $this->getIdUsulan();
				break;
			case 14:
				return $this->getCreatedAt();
				break;
			case 15:
				return $this->getUpdatedAt();
				break;
			case 16:
				return $this->getUnitId();
				break;
			case 17:
				return $this->getTipe();
				break;
			case 18:
				return $this->getTahap();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = UsulanVerifikasiPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getIdVerifikasi(),
			$keys[1] => $this->getShsdId(),
			$keys[2] => $this->getShsdName(),
			$keys[3] => $this->getHarga(),
			$keys[4] => $this->getNama(),
			$keys[5] => $this->getSpec(),
			$keys[6] => $this->getHiddenSpec(),
			$keys[7] => $this->getMerek(),
			$keys[8] => $this->getSatuan(),
			$keys[9] => $this->getRekening(),
			$keys[10] => $this->getPajak(),
			$keys[11] => $this->getKeterangan(),
			$keys[12] => $this->getIdSpjm(),
			$keys[13] => $this->getIdUsulan(),
			$keys[14] => $this->getCreatedAt(),
			$keys[15] => $this->getUpdatedAt(),
			$keys[16] => $this->getUnitId(),
			$keys[17] => $this->getTipe(),
			$keys[18] => $this->getTahap(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = UsulanVerifikasiPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setIdVerifikasi($value);
				break;
			case 1:
				$this->setShsdId($value);
				break;
			case 2:
				$this->setShsdName($value);
				break;
			case 3:
				$this->setHarga($value);
				break;
			case 4:
				$this->setNama($value);
				break;
			case 5:
				$this->setSpec($value);
				break;
			case 6:
				$this->setHiddenSpec($value);
				break;
			case 7:
				$this->setMerek($value);
				break;
			case 8:
				$this->setSatuan($value);
				break;
			case 9:
				$this->setRekening($value);
				break;
			case 10:
				$this->setPajak($value);
				break;
			case 11:
				$this->setKeterangan($value);
				break;
			case 12:
				$this->setIdSpjm($value);
				break;
			case 13:
				$this->setIdUsulan($value);
				break;
			case 14:
				$this->setCreatedAt($value);
				break;
			case 15:
				$this->setUpdatedAt($value);
				break;
			case 16:
				$this->setUnitId($value);
				break;
			case 17:
				$this->setTipe($value);
				break;
			case 18:
				$this->setTahap($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = UsulanVerifikasiPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setIdVerifikasi($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setShsdId($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setShsdName($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setHarga($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setNama($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setSpec($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setHiddenSpec($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setMerek($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setSatuan($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setRekening($arr[$keys[9]]);
		if (array_key_exists($keys[10], $arr)) $this->setPajak($arr[$keys[10]]);
		if (array_key_exists($keys[11], $arr)) $this->setKeterangan($arr[$keys[11]]);
		if (array_key_exists($keys[12], $arr)) $this->setIdSpjm($arr[$keys[12]]);
		if (array_key_exists($keys[13], $arr)) $this->setIdUsulan($arr[$keys[13]]);
		if (array_key_exists($keys[14], $arr)) $this->setCreatedAt($arr[$keys[14]]);
		if (array_key_exists($keys[15], $arr)) $this->setUpdatedAt($arr[$keys[15]]);
		if (array_key_exists($keys[16], $arr)) $this->setUnitId($arr[$keys[16]]);
		if (array_key_exists($keys[17], $arr)) $this->setTipe($arr[$keys[17]]);
		if (array_key_exists($keys[18], $arr)) $this->setTahap($arr[$keys[18]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(UsulanVerifikasiPeer::DATABASE_NAME);

		if ($this->isColumnModified(UsulanVerifikasiPeer::ID_VERIFIKASI)) $criteria->add(UsulanVerifikasiPeer::ID_VERIFIKASI, $this->id_verifikasi);
		if ($this->isColumnModified(UsulanVerifikasiPeer::SHSD_ID)) $criteria->add(UsulanVerifikasiPeer::SHSD_ID, $this->shsd_id);
		if ($this->isColumnModified(UsulanVerifikasiPeer::SHSD_NAME)) $criteria->add(UsulanVerifikasiPeer::SHSD_NAME, $this->shsd_name);
		if ($this->isColumnModified(UsulanVerifikasiPeer::HARGA)) $criteria->add(UsulanVerifikasiPeer::HARGA, $this->harga);
		if ($this->isColumnModified(UsulanVerifikasiPeer::NAMA)) $criteria->add(UsulanVerifikasiPeer::NAMA, $this->nama);
		if ($this->isColumnModified(UsulanVerifikasiPeer::SPEC)) $criteria->add(UsulanVerifikasiPeer::SPEC, $this->spec);
		if ($this->isColumnModified(UsulanVerifikasiPeer::HIDDEN_SPEC)) $criteria->add(UsulanVerifikasiPeer::HIDDEN_SPEC, $this->hidden_spec);
		if ($this->isColumnModified(UsulanVerifikasiPeer::MEREK)) $criteria->add(UsulanVerifikasiPeer::MEREK, $this->merek);
		if ($this->isColumnModified(UsulanVerifikasiPeer::SATUAN)) $criteria->add(UsulanVerifikasiPeer::SATUAN, $this->satuan);
		if ($this->isColumnModified(UsulanVerifikasiPeer::REKENING)) $criteria->add(UsulanVerifikasiPeer::REKENING, $this->rekening);
		if ($this->isColumnModified(UsulanVerifikasiPeer::PAJAK)) $criteria->add(UsulanVerifikasiPeer::PAJAK, $this->pajak);
		if ($this->isColumnModified(UsulanVerifikasiPeer::KETERANGAN)) $criteria->add(UsulanVerifikasiPeer::KETERANGAN, $this->keterangan);
		if ($this->isColumnModified(UsulanVerifikasiPeer::ID_SPJM)) $criteria->add(UsulanVerifikasiPeer::ID_SPJM, $this->id_spjm);
		if ($this->isColumnModified(UsulanVerifikasiPeer::ID_USULAN)) $criteria->add(UsulanVerifikasiPeer::ID_USULAN, $this->id_usulan);
		if ($this->isColumnModified(UsulanVerifikasiPeer::CREATED_AT)) $criteria->add(UsulanVerifikasiPeer::CREATED_AT, $this->created_at);
		if ($this->isColumnModified(UsulanVerifikasiPeer::UPDATED_AT)) $criteria->add(UsulanVerifikasiPeer::UPDATED_AT, $this->updated_at);
		if ($this->isColumnModified(UsulanVerifikasiPeer::UNIT_ID)) $criteria->add(UsulanVerifikasiPeer::UNIT_ID, $this->unit_id);
		if ($this->isColumnModified(UsulanVerifikasiPeer::TIPE)) $criteria->add(UsulanVerifikasiPeer::TIPE, $this->tipe);
		if ($this->isColumnModified(UsulanVerifikasiPeer::TAHAP)) $criteria->add(UsulanVerifikasiPeer::TAHAP, $this->tahap);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(UsulanVerifikasiPeer::DATABASE_NAME);

		$criteria->add(UsulanVerifikasiPeer::ID_VERIFIKASI, $this->id_verifikasi);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getIdVerifikasi();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setIdVerifikasi($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setShsdId($this->shsd_id);

		$copyObj->setShsdName($this->shsd_name);

		$copyObj->setHarga($this->harga);

		$copyObj->setNama($this->nama);

		$copyObj->setSpec($this->spec);

		$copyObj->setHiddenSpec($this->hidden_spec);

		$copyObj->setMerek($this->merek);

		$copyObj->setSatuan($this->satuan);

		$copyObj->setRekening($this->rekening);

		$copyObj->setPajak($this->pajak);

		$copyObj->setKeterangan($this->keterangan);

		$copyObj->setIdSpjm($this->id_spjm);

		$copyObj->setIdUsulan($this->id_usulan);

		$copyObj->setCreatedAt($this->created_at);

		$copyObj->setUpdatedAt($this->updated_at);

		$copyObj->setUnitId($this->unit_id);

		$copyObj->setTipe($this->tipe);

		$copyObj->setTahap($this->tahap);


		$copyObj->setNew(true);

		$copyObj->setIdVerifikasi(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new UsulanVerifikasiPeer();
		}
		return self::$peer;
	}

} 