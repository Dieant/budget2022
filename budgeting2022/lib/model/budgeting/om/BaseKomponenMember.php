<?php


abstract class BaseKomponenMember extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $komponen_id;


	
	protected $member_id;


	
	protected $satuan;


	
	protected $member_name;


	
	protected $member_harga;


	
	protected $koefisien;


	
	protected $member_total;


	
	protected $ip_address;


	
	protected $waktu_access;


	
	protected $member_tipe;


	
	protected $subtitle;


	
	protected $member_no;


	
	protected $tipe;


	
	protected $komponen_tipe;


	
	protected $from_id;


	
	protected $from_koef;


	
	protected $hspk_name;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getKomponenId()
	{

		return $this->komponen_id;
	}

	
	public function getMemberId()
	{

		return $this->member_id;
	}

	
	public function getSatuan()
	{

		return $this->satuan;
	}

	
	public function getMemberName()
	{

		return $this->member_name;
	}

	
	public function getMemberHarga()
	{

		return $this->member_harga;
	}

	
	public function getKoefisien()
	{

		return $this->koefisien;
	}

	
	public function getMemberTotal()
	{

		return $this->member_total;
	}

	
	public function getIpAddress()
	{

		return $this->ip_address;
	}

	
	public function getWaktuAccess($format = 'Y-m-d H:i:s')
	{

		if ($this->waktu_access === null || $this->waktu_access === '') {
			return null;
		} elseif (!is_int($this->waktu_access)) {
						$ts = strtotime($this->waktu_access);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [waktu_access] as date/time value: " . var_export($this->waktu_access, true));
			}
		} else {
			$ts = $this->waktu_access;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getMemberTipe()
	{

		return $this->member_tipe;
	}

	
	public function getSubtitle()
	{

		return $this->subtitle;
	}

	
	public function getMemberNo()
	{

		return $this->member_no;
	}

	
	public function getTipe()
	{

		return $this->tipe;
	}

	
	public function getKomponenTipe()
	{

		return $this->komponen_tipe;
	}

	
	public function getFromId()
	{

		return $this->from_id;
	}

	
	public function getFromKoef()
	{

		return $this->from_koef;
	}

	
	public function getHspkName()
	{

		return $this->hspk_name;
	}

	
	public function setKomponenId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->komponen_id !== $v) {
			$this->komponen_id = $v;
			$this->modifiedColumns[] = KomponenMemberPeer::KOMPONEN_ID;
		}

	} 
	
	public function setMemberId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->member_id !== $v) {
			$this->member_id = $v;
			$this->modifiedColumns[] = KomponenMemberPeer::MEMBER_ID;
		}

	} 
	
	public function setSatuan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->satuan !== $v) {
			$this->satuan = $v;
			$this->modifiedColumns[] = KomponenMemberPeer::SATUAN;
		}

	} 
	
	public function setMemberName($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->member_name !== $v) {
			$this->member_name = $v;
			$this->modifiedColumns[] = KomponenMemberPeer::MEMBER_NAME;
		}

	} 
	
	public function setMemberHarga($v)
	{

		if ($this->member_harga !== $v) {
			$this->member_harga = $v;
			$this->modifiedColumns[] = KomponenMemberPeer::MEMBER_HARGA;
		}

	} 
	
	public function setKoefisien($v)
	{

		if ($this->koefisien !== $v) {
			$this->koefisien = $v;
			$this->modifiedColumns[] = KomponenMemberPeer::KOEFISIEN;
		}

	} 
	
	public function setMemberTotal($v)
	{

		if ($this->member_total !== $v) {
			$this->member_total = $v;
			$this->modifiedColumns[] = KomponenMemberPeer::MEMBER_TOTAL;
		}

	} 
	
	public function setIpAddress($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->ip_address !== $v) {
			$this->ip_address = $v;
			$this->modifiedColumns[] = KomponenMemberPeer::IP_ADDRESS;
		}

	} 
	
	public function setWaktuAccess($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [waktu_access] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->waktu_access !== $ts) {
			$this->waktu_access = $ts;
			$this->modifiedColumns[] = KomponenMemberPeer::WAKTU_ACCESS;
		}

	} 
	
	public function setMemberTipe($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->member_tipe !== $v) {
			$this->member_tipe = $v;
			$this->modifiedColumns[] = KomponenMemberPeer::MEMBER_TIPE;
		}

	} 
	
	public function setSubtitle($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->subtitle !== $v) {
			$this->subtitle = $v;
			$this->modifiedColumns[] = KomponenMemberPeer::SUBTITLE;
		}

	} 
	
	public function setMemberNo($v)
	{

		if ($this->member_no !== $v) {
			$this->member_no = $v;
			$this->modifiedColumns[] = KomponenMemberPeer::MEMBER_NO;
		}

	} 
	
	public function setTipe($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->tipe !== $v) {
			$this->tipe = $v;
			$this->modifiedColumns[] = KomponenMemberPeer::TIPE;
		}

	} 
	
	public function setKomponenTipe($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->komponen_tipe !== $v) {
			$this->komponen_tipe = $v;
			$this->modifiedColumns[] = KomponenMemberPeer::KOMPONEN_TIPE;
		}

	} 
	
	public function setFromId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->from_id !== $v) {
			$this->from_id = $v;
			$this->modifiedColumns[] = KomponenMemberPeer::FROM_ID;
		}

	} 
	
	public function setFromKoef($v)
	{

		if ($this->from_koef !== $v) {
			$this->from_koef = $v;
			$this->modifiedColumns[] = KomponenMemberPeer::FROM_KOEF;
		}

	} 
	
	public function setHspkName($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->hspk_name !== $v) {
			$this->hspk_name = $v;
			$this->modifiedColumns[] = KomponenMemberPeer::HSPK_NAME;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->komponen_id = $rs->getString($startcol + 0);

			$this->member_id = $rs->getString($startcol + 1);

			$this->satuan = $rs->getString($startcol + 2);

			$this->member_name = $rs->getString($startcol + 3);

			$this->member_harga = $rs->getFloat($startcol + 4);

			$this->koefisien = $rs->getFloat($startcol + 5);

			$this->member_total = $rs->getFloat($startcol + 6);

			$this->ip_address = $rs->getString($startcol + 7);

			$this->waktu_access = $rs->getTimestamp($startcol + 8, null);

			$this->member_tipe = $rs->getString($startcol + 9);

			$this->subtitle = $rs->getString($startcol + 10);

			$this->member_no = $rs->getFloat($startcol + 11);

			$this->tipe = $rs->getString($startcol + 12);

			$this->komponen_tipe = $rs->getString($startcol + 13);

			$this->from_id = $rs->getString($startcol + 14);

			$this->from_koef = $rs->getFloat($startcol + 15);

			$this->hspk_name = $rs->getString($startcol + 16);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 17; 
		} catch (Exception $e) {
			throw new PropelException("Error populating KomponenMember object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(KomponenMemberPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			KomponenMemberPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(KomponenMemberPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = KomponenMemberPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += KomponenMemberPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = KomponenMemberPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = KomponenMemberPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getKomponenId();
				break;
			case 1:
				return $this->getMemberId();
				break;
			case 2:
				return $this->getSatuan();
				break;
			case 3:
				return $this->getMemberName();
				break;
			case 4:
				return $this->getMemberHarga();
				break;
			case 5:
				return $this->getKoefisien();
				break;
			case 6:
				return $this->getMemberTotal();
				break;
			case 7:
				return $this->getIpAddress();
				break;
			case 8:
				return $this->getWaktuAccess();
				break;
			case 9:
				return $this->getMemberTipe();
				break;
			case 10:
				return $this->getSubtitle();
				break;
			case 11:
				return $this->getMemberNo();
				break;
			case 12:
				return $this->getTipe();
				break;
			case 13:
				return $this->getKomponenTipe();
				break;
			case 14:
				return $this->getFromId();
				break;
			case 15:
				return $this->getFromKoef();
				break;
			case 16:
				return $this->getHspkName();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = KomponenMemberPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getKomponenId(),
			$keys[1] => $this->getMemberId(),
			$keys[2] => $this->getSatuan(),
			$keys[3] => $this->getMemberName(),
			$keys[4] => $this->getMemberHarga(),
			$keys[5] => $this->getKoefisien(),
			$keys[6] => $this->getMemberTotal(),
			$keys[7] => $this->getIpAddress(),
			$keys[8] => $this->getWaktuAccess(),
			$keys[9] => $this->getMemberTipe(),
			$keys[10] => $this->getSubtitle(),
			$keys[11] => $this->getMemberNo(),
			$keys[12] => $this->getTipe(),
			$keys[13] => $this->getKomponenTipe(),
			$keys[14] => $this->getFromId(),
			$keys[15] => $this->getFromKoef(),
			$keys[16] => $this->getHspkName(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = KomponenMemberPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setKomponenId($value);
				break;
			case 1:
				$this->setMemberId($value);
				break;
			case 2:
				$this->setSatuan($value);
				break;
			case 3:
				$this->setMemberName($value);
				break;
			case 4:
				$this->setMemberHarga($value);
				break;
			case 5:
				$this->setKoefisien($value);
				break;
			case 6:
				$this->setMemberTotal($value);
				break;
			case 7:
				$this->setIpAddress($value);
				break;
			case 8:
				$this->setWaktuAccess($value);
				break;
			case 9:
				$this->setMemberTipe($value);
				break;
			case 10:
				$this->setSubtitle($value);
				break;
			case 11:
				$this->setMemberNo($value);
				break;
			case 12:
				$this->setTipe($value);
				break;
			case 13:
				$this->setKomponenTipe($value);
				break;
			case 14:
				$this->setFromId($value);
				break;
			case 15:
				$this->setFromKoef($value);
				break;
			case 16:
				$this->setHspkName($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = KomponenMemberPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setKomponenId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setMemberId($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setSatuan($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setMemberName($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setMemberHarga($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setKoefisien($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setMemberTotal($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setIpAddress($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setWaktuAccess($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setMemberTipe($arr[$keys[9]]);
		if (array_key_exists($keys[10], $arr)) $this->setSubtitle($arr[$keys[10]]);
		if (array_key_exists($keys[11], $arr)) $this->setMemberNo($arr[$keys[11]]);
		if (array_key_exists($keys[12], $arr)) $this->setTipe($arr[$keys[12]]);
		if (array_key_exists($keys[13], $arr)) $this->setKomponenTipe($arr[$keys[13]]);
		if (array_key_exists($keys[14], $arr)) $this->setFromId($arr[$keys[14]]);
		if (array_key_exists($keys[15], $arr)) $this->setFromKoef($arr[$keys[15]]);
		if (array_key_exists($keys[16], $arr)) $this->setHspkName($arr[$keys[16]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(KomponenMemberPeer::DATABASE_NAME);

		if ($this->isColumnModified(KomponenMemberPeer::KOMPONEN_ID)) $criteria->add(KomponenMemberPeer::KOMPONEN_ID, $this->komponen_id);
		if ($this->isColumnModified(KomponenMemberPeer::MEMBER_ID)) $criteria->add(KomponenMemberPeer::MEMBER_ID, $this->member_id);
		if ($this->isColumnModified(KomponenMemberPeer::SATUAN)) $criteria->add(KomponenMemberPeer::SATUAN, $this->satuan);
		if ($this->isColumnModified(KomponenMemberPeer::MEMBER_NAME)) $criteria->add(KomponenMemberPeer::MEMBER_NAME, $this->member_name);
		if ($this->isColumnModified(KomponenMemberPeer::MEMBER_HARGA)) $criteria->add(KomponenMemberPeer::MEMBER_HARGA, $this->member_harga);
		if ($this->isColumnModified(KomponenMemberPeer::KOEFISIEN)) $criteria->add(KomponenMemberPeer::KOEFISIEN, $this->koefisien);
		if ($this->isColumnModified(KomponenMemberPeer::MEMBER_TOTAL)) $criteria->add(KomponenMemberPeer::MEMBER_TOTAL, $this->member_total);
		if ($this->isColumnModified(KomponenMemberPeer::IP_ADDRESS)) $criteria->add(KomponenMemberPeer::IP_ADDRESS, $this->ip_address);
		if ($this->isColumnModified(KomponenMemberPeer::WAKTU_ACCESS)) $criteria->add(KomponenMemberPeer::WAKTU_ACCESS, $this->waktu_access);
		if ($this->isColumnModified(KomponenMemberPeer::MEMBER_TIPE)) $criteria->add(KomponenMemberPeer::MEMBER_TIPE, $this->member_tipe);
		if ($this->isColumnModified(KomponenMemberPeer::SUBTITLE)) $criteria->add(KomponenMemberPeer::SUBTITLE, $this->subtitle);
		if ($this->isColumnModified(KomponenMemberPeer::MEMBER_NO)) $criteria->add(KomponenMemberPeer::MEMBER_NO, $this->member_no);
		if ($this->isColumnModified(KomponenMemberPeer::TIPE)) $criteria->add(KomponenMemberPeer::TIPE, $this->tipe);
		if ($this->isColumnModified(KomponenMemberPeer::KOMPONEN_TIPE)) $criteria->add(KomponenMemberPeer::KOMPONEN_TIPE, $this->komponen_tipe);
		if ($this->isColumnModified(KomponenMemberPeer::FROM_ID)) $criteria->add(KomponenMemberPeer::FROM_ID, $this->from_id);
		if ($this->isColumnModified(KomponenMemberPeer::FROM_KOEF)) $criteria->add(KomponenMemberPeer::FROM_KOEF, $this->from_koef);
		if ($this->isColumnModified(KomponenMemberPeer::HSPK_NAME)) $criteria->add(KomponenMemberPeer::HSPK_NAME, $this->hspk_name);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(KomponenMemberPeer::DATABASE_NAME);


		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return null;
	}

	
	 public function setPrimaryKey($pk)
	 {
		 	 }

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setKomponenId($this->komponen_id);

		$copyObj->setMemberId($this->member_id);

		$copyObj->setSatuan($this->satuan);

		$copyObj->setMemberName($this->member_name);

		$copyObj->setMemberHarga($this->member_harga);

		$copyObj->setKoefisien($this->koefisien);

		$copyObj->setMemberTotal($this->member_total);

		$copyObj->setIpAddress($this->ip_address);

		$copyObj->setWaktuAccess($this->waktu_access);

		$copyObj->setMemberTipe($this->member_tipe);

		$copyObj->setSubtitle($this->subtitle);

		$copyObj->setMemberNo($this->member_no);

		$copyObj->setTipe($this->tipe);

		$copyObj->setKomponenTipe($this->komponen_tipe);

		$copyObj->setFromId($this->from_id);

		$copyObj->setFromKoef($this->from_koef);

		$copyObj->setHspkName($this->hspk_name);


		$copyObj->setNew(true);

	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new KomponenMemberPeer();
		}
		return self::$peer;
	}

} 