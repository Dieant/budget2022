<?php


abstract class BaseRevisi1MasterKegiatanPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.revisi1_master_kegiatan';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.Revisi1MasterKegiatan';

	
	const NUM_COLUMNS = 90;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const UNIT_ID = 'ebudget.revisi1_master_kegiatan.UNIT_ID';

	
	const KODE_KEGIATAN = 'ebudget.revisi1_master_kegiatan.KODE_KEGIATAN';

	
	const KODE_BIDANG = 'ebudget.revisi1_master_kegiatan.KODE_BIDANG';

	
	const KODE_URUSAN_WAJIB = 'ebudget.revisi1_master_kegiatan.KODE_URUSAN_WAJIB';

	
	const KODE_PROGRAM = 'ebudget.revisi1_master_kegiatan.KODE_PROGRAM';

	
	const KODE_SASARAN = 'ebudget.revisi1_master_kegiatan.KODE_SASARAN';

	
	const KODE_INDIKATOR = 'ebudget.revisi1_master_kegiatan.KODE_INDIKATOR';

	
	const ALOKASI_DANA = 'ebudget.revisi1_master_kegiatan.ALOKASI_DANA';

	
	const NAMA_KEGIATAN = 'ebudget.revisi1_master_kegiatan.NAMA_KEGIATAN';

	
	const MASUKAN = 'ebudget.revisi1_master_kegiatan.MASUKAN';

	
	const OUTPUT = 'ebudget.revisi1_master_kegiatan.OUTPUT';

	
	const OUTCOME = 'ebudget.revisi1_master_kegiatan.OUTCOME';

	
	const BENEFIT = 'ebudget.revisi1_master_kegiatan.BENEFIT';

	
	const IMPACT = 'ebudget.revisi1_master_kegiatan.IMPACT';

	
	const TIPE = 'ebudget.revisi1_master_kegiatan.TIPE';

	
	const KEGIATAN_ACTIVE = 'ebudget.revisi1_master_kegiatan.KEGIATAN_ACTIVE';

	
	const TO_KEGIATAN_CODE = 'ebudget.revisi1_master_kegiatan.TO_KEGIATAN_CODE';

	
	const CATATAN = 'ebudget.revisi1_master_kegiatan.CATATAN';

	
	const TARGET_OUTCOME = 'ebudget.revisi1_master_kegiatan.TARGET_OUTCOME';

	
	const LOKASI = 'ebudget.revisi1_master_kegiatan.LOKASI';

	
	const JUMLAH_PREV = 'ebudget.revisi1_master_kegiatan.JUMLAH_PREV';

	
	const JUMLAH_NOW = 'ebudget.revisi1_master_kegiatan.JUMLAH_NOW';

	
	const JUMLAH_NEXT = 'ebudget.revisi1_master_kegiatan.JUMLAH_NEXT';

	
	const KODE_PROGRAM2 = 'ebudget.revisi1_master_kegiatan.KODE_PROGRAM2';

	
	const KODE_URUSAN = 'ebudget.revisi1_master_kegiatan.KODE_URUSAN';

	
	const LAST_UPDATE_USER = 'ebudget.revisi1_master_kegiatan.LAST_UPDATE_USER';

	
	const LAST_UPDATE_TIME = 'ebudget.revisi1_master_kegiatan.LAST_UPDATE_TIME';

	
	const LAST_UPDATE_IP = 'ebudget.revisi1_master_kegiatan.LAST_UPDATE_IP';

	
	const TAHAP = 'ebudget.revisi1_master_kegiatan.TAHAP';

	
	const KODE_MISI = 'ebudget.revisi1_master_kegiatan.KODE_MISI';

	
	const KODE_TUJUAN = 'ebudget.revisi1_master_kegiatan.KODE_TUJUAN';

	
	const RANKING = 'ebudget.revisi1_master_kegiatan.RANKING';

	
	const NOMOR13 = 'ebudget.revisi1_master_kegiatan.NOMOR13';

	
	const PPA_NAMA = 'ebudget.revisi1_master_kegiatan.PPA_NAMA';

	
	const PPA_PANGKAT = 'ebudget.revisi1_master_kegiatan.PPA_PANGKAT';

	
	const PPA_NIP = 'ebudget.revisi1_master_kegiatan.PPA_NIP';

	
	const LANJUTAN = 'ebudget.revisi1_master_kegiatan.LANJUTAN';

	
	const USER_ID = 'ebudget.revisi1_master_kegiatan.USER_ID';

	
	const ID = 'ebudget.revisi1_master_kegiatan.ID';

	
	const TAHUN = 'ebudget.revisi1_master_kegiatan.TAHUN';

	
	const TAMBAHAN_PAGU = 'ebudget.revisi1_master_kegiatan.TAMBAHAN_PAGU';

	
	const GENDER = 'ebudget.revisi1_master_kegiatan.GENDER';

	
	const KODE_KEG_KEUANGAN = 'ebudget.revisi1_master_kegiatan.KODE_KEG_KEUANGAN';

	
	const INDIKATOR = 'ebudget.revisi1_master_kegiatan.INDIKATOR';

	
	const IS_DAK = 'ebudget.revisi1_master_kegiatan.IS_DAK';

	
	const KODE_KEGIATAN_ASAL = 'ebudget.revisi1_master_kegiatan.KODE_KEGIATAN_ASAL';

	
	const KODE_KEG_KEUANGAN_ASAL = 'ebudget.revisi1_master_kegiatan.KODE_KEG_KEUANGAN_ASAL';

	
	const TH_KE_MULTIYEARS = 'ebudget.revisi1_master_kegiatan.TH_KE_MULTIYEARS';

	
	const KELOMPOK_SASARAN = 'ebudget.revisi1_master_kegiatan.KELOMPOK_SASARAN';

	
	const PAGU_BAPPEKO = 'ebudget.revisi1_master_kegiatan.PAGU_BAPPEKO';

	
	const KODE_DPA = 'ebudget.revisi1_master_kegiatan.KODE_DPA';

	
	const USER_ID_PPTK = 'ebudget.revisi1_master_kegiatan.USER_ID_PPTK';

	
	const USER_ID_KPA = 'ebudget.revisi1_master_kegiatan.USER_ID_KPA';

	
	const CATATAN_PEMBAHASAN = 'ebudget.revisi1_master_kegiatan.CATATAN_PEMBAHASAN';

	
	const CATATAN_PENYELIA = 'ebudget.revisi1_master_kegiatan.CATATAN_PENYELIA';

	
	const CATATAN_BAPPEKO = 'ebudget.revisi1_master_kegiatan.CATATAN_BAPPEKO';

	
	const STATUS_LEVEL = 'ebudget.revisi1_master_kegiatan.STATUS_LEVEL';

	
	const IS_TAPD_SETUJU = 'ebudget.revisi1_master_kegiatan.IS_TAPD_SETUJU';

	
	const IS_BAPPEKO_SETUJU = 'ebudget.revisi1_master_kegiatan.IS_BAPPEKO_SETUJU';

	
	const IS_PENYELIA_SETUJU = 'ebudget.revisi1_master_kegiatan.IS_PENYELIA_SETUJU';

	
	const IS_PERNAH_RKA = 'ebudget.revisi1_master_kegiatan.IS_PERNAH_RKA';

	
	const KODE_KEGIATAN_BARU = 'ebudget.revisi1_master_kegiatan.KODE_KEGIATAN_BARU';

	
	const CATATAN_BPKPD = 'ebudget.revisi1_master_kegiatan.CATATAN_BPKPD';

	
	const UBAH_F1_DINAS = 'ebudget.revisi1_master_kegiatan.UBAH_F1_DINAS';

	
	const UBAH_F1_PENELITI = 'ebudget.revisi1_master_kegiatan.UBAH_F1_PENELITI';

	
	const SISA_LELANG_DINAS = 'ebudget.revisi1_master_kegiatan.SISA_LELANG_DINAS';

	
	const SISA_LELANG_PENELITI = 'ebudget.revisi1_master_kegiatan.SISA_LELANG_PENELITI';

	
	const CATATAN_UBAH_F1_DINAS = 'ebudget.revisi1_master_kegiatan.CATATAN_UBAH_F1_DINAS';

	
	const CATATAN_SISA_LELANG_PENELITI = 'ebudget.revisi1_master_kegiatan.CATATAN_SISA_LELANG_PENELITI';

	
	const PPTK_APPROVAL = 'ebudget.revisi1_master_kegiatan.PPTK_APPROVAL';

	
	const KPA_APPROVAL = 'ebudget.revisi1_master_kegiatan.KPA_APPROVAL';

	
	const CATATAN_BAGIAN_HUKUM = 'ebudget.revisi1_master_kegiatan.CATATAN_BAGIAN_HUKUM';

	
	const CATATAN_INSPEKTORAT = 'ebudget.revisi1_master_kegiatan.CATATAN_INSPEKTORAT';

	
	const CATATAN_BADAN_KEPEGAWAIAN = 'ebudget.revisi1_master_kegiatan.CATATAN_BADAN_KEPEGAWAIAN';

	
	const CATATAN_LPPA = 'ebudget.revisi1_master_kegiatan.CATATAN_LPPA';

	
	const IS_BAGIAN_HUKUM_SETUJU = 'ebudget.revisi1_master_kegiatan.IS_BAGIAN_HUKUM_SETUJU';

	
	const IS_INSPEKTORAT_SETUJU = 'ebudget.revisi1_master_kegiatan.IS_INSPEKTORAT_SETUJU';

	
	const IS_BADAN_KEPEGAWAIAN_SETUJU = 'ebudget.revisi1_master_kegiatan.IS_BADAN_KEPEGAWAIAN_SETUJU';

	
	const IS_LPPA_SETUJU = 'ebudget.revisi1_master_kegiatan.IS_LPPA_SETUJU';

	
	const VERIFIKASI_BPKPD = 'ebudget.revisi1_master_kegiatan.VERIFIKASI_BPKPD';

	
	const VERIFIKASI_BAPPEKO = 'ebudget.revisi1_master_kegiatan.VERIFIKASI_BAPPEKO';

	
	const VERIFIKASI_PENYELIA = 'ebudget.revisi1_master_kegiatan.VERIFIKASI_PENYELIA';

	
	const VERIFIKASI_BAGIAN_HUKUM = 'ebudget.revisi1_master_kegiatan.VERIFIKASI_BAGIAN_HUKUM';

	
	const VERIFIKASI_INSPEKTORAT = 'ebudget.revisi1_master_kegiatan.VERIFIKASI_INSPEKTORAT';

	
	const VERIFIKASI_BADAN_KEPEGAWAIAN = 'ebudget.revisi1_master_kegiatan.VERIFIKASI_BADAN_KEPEGAWAIAN';

	
	const VERIFIKASI_LPPA = 'ebudget.revisi1_master_kegiatan.VERIFIKASI_LPPA';

	
	const METODE_COUNT = 'ebudget.revisi1_master_kegiatan.METODE_COUNT';

	
	const CATATAN_BAGIAN_ORGANISASI = 'ebudget.revisi1_master_kegiatan.CATATAN_BAGIAN_ORGANISASI';

	
	const IS_BAGIAN_ORGANISASI_SETUJU = 'ebudget.revisi1_master_kegiatan.IS_BAGIAN_ORGANISASI_SETUJU';

	
	const VERIFIKASI_BAGIAN_ORGANISASI = 'ebudget.revisi1_master_kegiatan.VERIFIKASI_BAGIAN_ORGANISASI';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('UnitId', 'KodeKegiatan', 'KodeBidang', 'KodeUrusanWajib', 'KodeProgram', 'KodeSasaran', 'KodeIndikator', 'AlokasiDana', 'NamaKegiatan', 'Masukan', 'Output', 'Outcome', 'Benefit', 'Impact', 'Tipe', 'KegiatanActive', 'ToKegiatanCode', 'Catatan', 'TargetOutcome', 'Lokasi', 'JumlahPrev', 'JumlahNow', 'JumlahNext', 'KodeProgram2', 'KodeUrusan', 'LastUpdateUser', 'LastUpdateTime', 'LastUpdateIp', 'Tahap', 'KodeMisi', 'KodeTujuan', 'Ranking', 'Nomor13', 'PpaNama', 'PpaPangkat', 'PpaNip', 'Lanjutan', 'UserId', 'Id', 'Tahun', 'TambahanPagu', 'Gender', 'KodeKegKeuangan', 'Indikator', 'IsDak', 'KodeKegiatanAsal', 'KodeKegKeuanganAsal', 'ThKeMultiyears', 'KelompokSasaran', 'PaguBappeko', 'KodeDpa', 'UserIdPptk', 'UserIdKpa', 'CatatanPembahasan', 'CatatanPenyelia', 'CatatanBappeko', 'StatusLevel', 'IsTapdSetuju', 'IsBappekoSetuju', 'IsPenyeliaSetuju', 'IsPernahRka', 'KodeKegiatanBaru', 'CatatanBpkpd', 'UbahF1Dinas', 'UbahF1Peneliti', 'SisaLelangDinas', 'SisaLelangPeneliti', 'CatatanUbahF1Dinas', 'CatatanSisaLelangPeneliti', 'PptkApproval', 'KpaApproval', 'CatatanBagianHukum', 'CatatanInspektorat', 'CatatanBadanKepegawaian', 'CatatanLppa', 'IsBagianHukumSetuju', 'IsInspektoratSetuju', 'IsBadanKepegawaianSetuju', 'IsLppaSetuju', 'VerifikasiBpkpd', 'VerifikasiBappeko', 'VerifikasiPenyelia', 'VerifikasiBagianHukum', 'VerifikasiInspektorat', 'VerifikasiBadanKepegawaian', 'VerifikasiLppa', 'MetodeCount', 'CatatanBagianOrganisasi', 'IsBagianOrganisasiSetuju', 'VerifikasiBagianOrganisasi', ),
		BasePeer::TYPE_COLNAME => array (Revisi1MasterKegiatanPeer::UNIT_ID, Revisi1MasterKegiatanPeer::KODE_KEGIATAN, Revisi1MasterKegiatanPeer::KODE_BIDANG, Revisi1MasterKegiatanPeer::KODE_URUSAN_WAJIB, Revisi1MasterKegiatanPeer::KODE_PROGRAM, Revisi1MasterKegiatanPeer::KODE_SASARAN, Revisi1MasterKegiatanPeer::KODE_INDIKATOR, Revisi1MasterKegiatanPeer::ALOKASI_DANA, Revisi1MasterKegiatanPeer::NAMA_KEGIATAN, Revisi1MasterKegiatanPeer::MASUKAN, Revisi1MasterKegiatanPeer::OUTPUT, Revisi1MasterKegiatanPeer::OUTCOME, Revisi1MasterKegiatanPeer::BENEFIT, Revisi1MasterKegiatanPeer::IMPACT, Revisi1MasterKegiatanPeer::TIPE, Revisi1MasterKegiatanPeer::KEGIATAN_ACTIVE, Revisi1MasterKegiatanPeer::TO_KEGIATAN_CODE, Revisi1MasterKegiatanPeer::CATATAN, Revisi1MasterKegiatanPeer::TARGET_OUTCOME, Revisi1MasterKegiatanPeer::LOKASI, Revisi1MasterKegiatanPeer::JUMLAH_PREV, Revisi1MasterKegiatanPeer::JUMLAH_NOW, Revisi1MasterKegiatanPeer::JUMLAH_NEXT, Revisi1MasterKegiatanPeer::KODE_PROGRAM2, Revisi1MasterKegiatanPeer::KODE_URUSAN, Revisi1MasterKegiatanPeer::LAST_UPDATE_USER, Revisi1MasterKegiatanPeer::LAST_UPDATE_TIME, Revisi1MasterKegiatanPeer::LAST_UPDATE_IP, Revisi1MasterKegiatanPeer::TAHAP, Revisi1MasterKegiatanPeer::KODE_MISI, Revisi1MasterKegiatanPeer::KODE_TUJUAN, Revisi1MasterKegiatanPeer::RANKING, Revisi1MasterKegiatanPeer::NOMOR13, Revisi1MasterKegiatanPeer::PPA_NAMA, Revisi1MasterKegiatanPeer::PPA_PANGKAT, Revisi1MasterKegiatanPeer::PPA_NIP, Revisi1MasterKegiatanPeer::LANJUTAN, Revisi1MasterKegiatanPeer::USER_ID, Revisi1MasterKegiatanPeer::ID, Revisi1MasterKegiatanPeer::TAHUN, Revisi1MasterKegiatanPeer::TAMBAHAN_PAGU, Revisi1MasterKegiatanPeer::GENDER, Revisi1MasterKegiatanPeer::KODE_KEG_KEUANGAN, Revisi1MasterKegiatanPeer::INDIKATOR, Revisi1MasterKegiatanPeer::IS_DAK, Revisi1MasterKegiatanPeer::KODE_KEGIATAN_ASAL, Revisi1MasterKegiatanPeer::KODE_KEG_KEUANGAN_ASAL, Revisi1MasterKegiatanPeer::TH_KE_MULTIYEARS, Revisi1MasterKegiatanPeer::KELOMPOK_SASARAN, Revisi1MasterKegiatanPeer::PAGU_BAPPEKO, Revisi1MasterKegiatanPeer::KODE_DPA, Revisi1MasterKegiatanPeer::USER_ID_PPTK, Revisi1MasterKegiatanPeer::USER_ID_KPA, Revisi1MasterKegiatanPeer::CATATAN_PEMBAHASAN, Revisi1MasterKegiatanPeer::CATATAN_PENYELIA, Revisi1MasterKegiatanPeer::CATATAN_BAPPEKO, Revisi1MasterKegiatanPeer::STATUS_LEVEL, Revisi1MasterKegiatanPeer::IS_TAPD_SETUJU, Revisi1MasterKegiatanPeer::IS_BAPPEKO_SETUJU, Revisi1MasterKegiatanPeer::IS_PENYELIA_SETUJU, Revisi1MasterKegiatanPeer::IS_PERNAH_RKA, Revisi1MasterKegiatanPeer::KODE_KEGIATAN_BARU, Revisi1MasterKegiatanPeer::CATATAN_BPKPD, Revisi1MasterKegiatanPeer::UBAH_F1_DINAS, Revisi1MasterKegiatanPeer::UBAH_F1_PENELITI, Revisi1MasterKegiatanPeer::SISA_LELANG_DINAS, Revisi1MasterKegiatanPeer::SISA_LELANG_PENELITI, Revisi1MasterKegiatanPeer::CATATAN_UBAH_F1_DINAS, Revisi1MasterKegiatanPeer::CATATAN_SISA_LELANG_PENELITI, Revisi1MasterKegiatanPeer::PPTK_APPROVAL, Revisi1MasterKegiatanPeer::KPA_APPROVAL, Revisi1MasterKegiatanPeer::CATATAN_BAGIAN_HUKUM, Revisi1MasterKegiatanPeer::CATATAN_INSPEKTORAT, Revisi1MasterKegiatanPeer::CATATAN_BADAN_KEPEGAWAIAN, Revisi1MasterKegiatanPeer::CATATAN_LPPA, Revisi1MasterKegiatanPeer::IS_BAGIAN_HUKUM_SETUJU, Revisi1MasterKegiatanPeer::IS_INSPEKTORAT_SETUJU, Revisi1MasterKegiatanPeer::IS_BADAN_KEPEGAWAIAN_SETUJU, Revisi1MasterKegiatanPeer::IS_LPPA_SETUJU, Revisi1MasterKegiatanPeer::VERIFIKASI_BPKPD, Revisi1MasterKegiatanPeer::VERIFIKASI_BAPPEKO, Revisi1MasterKegiatanPeer::VERIFIKASI_PENYELIA, Revisi1MasterKegiatanPeer::VERIFIKASI_BAGIAN_HUKUM, Revisi1MasterKegiatanPeer::VERIFIKASI_INSPEKTORAT, Revisi1MasterKegiatanPeer::VERIFIKASI_BADAN_KEPEGAWAIAN, Revisi1MasterKegiatanPeer::VERIFIKASI_LPPA, Revisi1MasterKegiatanPeer::METODE_COUNT, Revisi1MasterKegiatanPeer::CATATAN_BAGIAN_ORGANISASI, Revisi1MasterKegiatanPeer::IS_BAGIAN_ORGANISASI_SETUJU, Revisi1MasterKegiatanPeer::VERIFIKASI_BAGIAN_ORGANISASI, ),
		BasePeer::TYPE_FIELDNAME => array ('unit_id', 'kode_kegiatan', 'kode_bidang', 'kode_urusan_wajib', 'kode_program', 'kode_sasaran', 'kode_indikator', 'alokasi_dana', 'nama_kegiatan', 'masukan', 'output', 'outcome', 'benefit', 'impact', 'tipe', 'kegiatan_active', 'to_kegiatan_code', 'catatan', 'target_outcome', 'lokasi', 'jumlah_prev', 'jumlah_now', 'jumlah_next', 'kode_program2', 'kode_urusan', 'last_update_user', 'last_update_time', 'last_update_ip', 'tahap', 'kode_misi', 'kode_tujuan', 'ranking', 'nomor13', 'ppa_nama', 'ppa_pangkat', 'ppa_nip', 'lanjutan', 'user_id', 'id', 'tahun', 'tambahan_pagu', 'gender', 'kode_keg_keuangan', 'indikator', 'is_dak', 'kode_kegiatan_asal', 'kode_keg_keuangan_asal', 'th_ke_multiyears', 'kelompok_sasaran', 'pagu_bappeko', 'kode_dpa', 'user_id_pptk', 'user_id_kpa', 'catatan_pembahasan', 'catatan_penyelia', 'catatan_bappeko', 'status_level', 'is_tapd_setuju', 'is_bappeko_setuju', 'is_penyelia_setuju', 'is_pernah_rka', 'kode_kegiatan_baru', 'catatan_bpkpd', 'ubah_f1_dinas', 'ubah_f1_peneliti', 'sisa_lelang_dinas', 'sisa_lelang_peneliti', 'catatan_ubah_f1_dinas', 'catatan_sisa_lelang_peneliti', 'pptk_approval', 'kpa_approval', 'catatan_bagian_hukum', 'catatan_inspektorat', 'catatan_badan_kepegawaian', 'catatan_lppa', 'is_bagian_hukum_setuju', 'is_inspektorat_setuju', 'is_badan_kepegawaian_setuju', 'is_lppa_setuju', 'verifikasi_bpkpd', 'verifikasi_bappeko', 'verifikasi_penyelia', 'verifikasi_bagian_hukum', 'verifikasi_inspektorat', 'verifikasi_badan_kepegawaian', 'verifikasi_lppa', 'metode_count', 'catatan_bagian_organisasi', 'is_bagian_organisasi_setuju', 'verifikasi_bagian_organisasi', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('UnitId' => 0, 'KodeKegiatan' => 1, 'KodeBidang' => 2, 'KodeUrusanWajib' => 3, 'KodeProgram' => 4, 'KodeSasaran' => 5, 'KodeIndikator' => 6, 'AlokasiDana' => 7, 'NamaKegiatan' => 8, 'Masukan' => 9, 'Output' => 10, 'Outcome' => 11, 'Benefit' => 12, 'Impact' => 13, 'Tipe' => 14, 'KegiatanActive' => 15, 'ToKegiatanCode' => 16, 'Catatan' => 17, 'TargetOutcome' => 18, 'Lokasi' => 19, 'JumlahPrev' => 20, 'JumlahNow' => 21, 'JumlahNext' => 22, 'KodeProgram2' => 23, 'KodeUrusan' => 24, 'LastUpdateUser' => 25, 'LastUpdateTime' => 26, 'LastUpdateIp' => 27, 'Tahap' => 28, 'KodeMisi' => 29, 'KodeTujuan' => 30, 'Ranking' => 31, 'Nomor13' => 32, 'PpaNama' => 33, 'PpaPangkat' => 34, 'PpaNip' => 35, 'Lanjutan' => 36, 'UserId' => 37, 'Id' => 38, 'Tahun' => 39, 'TambahanPagu' => 40, 'Gender' => 41, 'KodeKegKeuangan' => 42, 'Indikator' => 43, 'IsDak' => 44, 'KodeKegiatanAsal' => 45, 'KodeKegKeuanganAsal' => 46, 'ThKeMultiyears' => 47, 'KelompokSasaran' => 48, 'PaguBappeko' => 49, 'KodeDpa' => 50, 'UserIdPptk' => 51, 'UserIdKpa' => 52, 'CatatanPembahasan' => 53, 'CatatanPenyelia' => 54, 'CatatanBappeko' => 55, 'StatusLevel' => 56, 'IsTapdSetuju' => 57, 'IsBappekoSetuju' => 58, 'IsPenyeliaSetuju' => 59, 'IsPernahRka' => 60, 'KodeKegiatanBaru' => 61, 'CatatanBpkpd' => 62, 'UbahF1Dinas' => 63, 'UbahF1Peneliti' => 64, 'SisaLelangDinas' => 65, 'SisaLelangPeneliti' => 66, 'CatatanUbahF1Dinas' => 67, 'CatatanSisaLelangPeneliti' => 68, 'PptkApproval' => 69, 'KpaApproval' => 70, 'CatatanBagianHukum' => 71, 'CatatanInspektorat' => 72, 'CatatanBadanKepegawaian' => 73, 'CatatanLppa' => 74, 'IsBagianHukumSetuju' => 75, 'IsInspektoratSetuju' => 76, 'IsBadanKepegawaianSetuju' => 77, 'IsLppaSetuju' => 78, 'VerifikasiBpkpd' => 79, 'VerifikasiBappeko' => 80, 'VerifikasiPenyelia' => 81, 'VerifikasiBagianHukum' => 82, 'VerifikasiInspektorat' => 83, 'VerifikasiBadanKepegawaian' => 84, 'VerifikasiLppa' => 85, 'MetodeCount' => 86, 'CatatanBagianOrganisasi' => 87, 'IsBagianOrganisasiSetuju' => 88, 'VerifikasiBagianOrganisasi' => 89, ),
		BasePeer::TYPE_COLNAME => array (Revisi1MasterKegiatanPeer::UNIT_ID => 0, Revisi1MasterKegiatanPeer::KODE_KEGIATAN => 1, Revisi1MasterKegiatanPeer::KODE_BIDANG => 2, Revisi1MasterKegiatanPeer::KODE_URUSAN_WAJIB => 3, Revisi1MasterKegiatanPeer::KODE_PROGRAM => 4, Revisi1MasterKegiatanPeer::KODE_SASARAN => 5, Revisi1MasterKegiatanPeer::KODE_INDIKATOR => 6, Revisi1MasterKegiatanPeer::ALOKASI_DANA => 7, Revisi1MasterKegiatanPeer::NAMA_KEGIATAN => 8, Revisi1MasterKegiatanPeer::MASUKAN => 9, Revisi1MasterKegiatanPeer::OUTPUT => 10, Revisi1MasterKegiatanPeer::OUTCOME => 11, Revisi1MasterKegiatanPeer::BENEFIT => 12, Revisi1MasterKegiatanPeer::IMPACT => 13, Revisi1MasterKegiatanPeer::TIPE => 14, Revisi1MasterKegiatanPeer::KEGIATAN_ACTIVE => 15, Revisi1MasterKegiatanPeer::TO_KEGIATAN_CODE => 16, Revisi1MasterKegiatanPeer::CATATAN => 17, Revisi1MasterKegiatanPeer::TARGET_OUTCOME => 18, Revisi1MasterKegiatanPeer::LOKASI => 19, Revisi1MasterKegiatanPeer::JUMLAH_PREV => 20, Revisi1MasterKegiatanPeer::JUMLAH_NOW => 21, Revisi1MasterKegiatanPeer::JUMLAH_NEXT => 22, Revisi1MasterKegiatanPeer::KODE_PROGRAM2 => 23, Revisi1MasterKegiatanPeer::KODE_URUSAN => 24, Revisi1MasterKegiatanPeer::LAST_UPDATE_USER => 25, Revisi1MasterKegiatanPeer::LAST_UPDATE_TIME => 26, Revisi1MasterKegiatanPeer::LAST_UPDATE_IP => 27, Revisi1MasterKegiatanPeer::TAHAP => 28, Revisi1MasterKegiatanPeer::KODE_MISI => 29, Revisi1MasterKegiatanPeer::KODE_TUJUAN => 30, Revisi1MasterKegiatanPeer::RANKING => 31, Revisi1MasterKegiatanPeer::NOMOR13 => 32, Revisi1MasterKegiatanPeer::PPA_NAMA => 33, Revisi1MasterKegiatanPeer::PPA_PANGKAT => 34, Revisi1MasterKegiatanPeer::PPA_NIP => 35, Revisi1MasterKegiatanPeer::LANJUTAN => 36, Revisi1MasterKegiatanPeer::USER_ID => 37, Revisi1MasterKegiatanPeer::ID => 38, Revisi1MasterKegiatanPeer::TAHUN => 39, Revisi1MasterKegiatanPeer::TAMBAHAN_PAGU => 40, Revisi1MasterKegiatanPeer::GENDER => 41, Revisi1MasterKegiatanPeer::KODE_KEG_KEUANGAN => 42, Revisi1MasterKegiatanPeer::INDIKATOR => 43, Revisi1MasterKegiatanPeer::IS_DAK => 44, Revisi1MasterKegiatanPeer::KODE_KEGIATAN_ASAL => 45, Revisi1MasterKegiatanPeer::KODE_KEG_KEUANGAN_ASAL => 46, Revisi1MasterKegiatanPeer::TH_KE_MULTIYEARS => 47, Revisi1MasterKegiatanPeer::KELOMPOK_SASARAN => 48, Revisi1MasterKegiatanPeer::PAGU_BAPPEKO => 49, Revisi1MasterKegiatanPeer::KODE_DPA => 50, Revisi1MasterKegiatanPeer::USER_ID_PPTK => 51, Revisi1MasterKegiatanPeer::USER_ID_KPA => 52, Revisi1MasterKegiatanPeer::CATATAN_PEMBAHASAN => 53, Revisi1MasterKegiatanPeer::CATATAN_PENYELIA => 54, Revisi1MasterKegiatanPeer::CATATAN_BAPPEKO => 55, Revisi1MasterKegiatanPeer::STATUS_LEVEL => 56, Revisi1MasterKegiatanPeer::IS_TAPD_SETUJU => 57, Revisi1MasterKegiatanPeer::IS_BAPPEKO_SETUJU => 58, Revisi1MasterKegiatanPeer::IS_PENYELIA_SETUJU => 59, Revisi1MasterKegiatanPeer::IS_PERNAH_RKA => 60, Revisi1MasterKegiatanPeer::KODE_KEGIATAN_BARU => 61, Revisi1MasterKegiatanPeer::CATATAN_BPKPD => 62, Revisi1MasterKegiatanPeer::UBAH_F1_DINAS => 63, Revisi1MasterKegiatanPeer::UBAH_F1_PENELITI => 64, Revisi1MasterKegiatanPeer::SISA_LELANG_DINAS => 65, Revisi1MasterKegiatanPeer::SISA_LELANG_PENELITI => 66, Revisi1MasterKegiatanPeer::CATATAN_UBAH_F1_DINAS => 67, Revisi1MasterKegiatanPeer::CATATAN_SISA_LELANG_PENELITI => 68, Revisi1MasterKegiatanPeer::PPTK_APPROVAL => 69, Revisi1MasterKegiatanPeer::KPA_APPROVAL => 70, Revisi1MasterKegiatanPeer::CATATAN_BAGIAN_HUKUM => 71, Revisi1MasterKegiatanPeer::CATATAN_INSPEKTORAT => 72, Revisi1MasterKegiatanPeer::CATATAN_BADAN_KEPEGAWAIAN => 73, Revisi1MasterKegiatanPeer::CATATAN_LPPA => 74, Revisi1MasterKegiatanPeer::IS_BAGIAN_HUKUM_SETUJU => 75, Revisi1MasterKegiatanPeer::IS_INSPEKTORAT_SETUJU => 76, Revisi1MasterKegiatanPeer::IS_BADAN_KEPEGAWAIAN_SETUJU => 77, Revisi1MasterKegiatanPeer::IS_LPPA_SETUJU => 78, Revisi1MasterKegiatanPeer::VERIFIKASI_BPKPD => 79, Revisi1MasterKegiatanPeer::VERIFIKASI_BAPPEKO => 80, Revisi1MasterKegiatanPeer::VERIFIKASI_PENYELIA => 81, Revisi1MasterKegiatanPeer::VERIFIKASI_BAGIAN_HUKUM => 82, Revisi1MasterKegiatanPeer::VERIFIKASI_INSPEKTORAT => 83, Revisi1MasterKegiatanPeer::VERIFIKASI_BADAN_KEPEGAWAIAN => 84, Revisi1MasterKegiatanPeer::VERIFIKASI_LPPA => 85, Revisi1MasterKegiatanPeer::METODE_COUNT => 86, Revisi1MasterKegiatanPeer::CATATAN_BAGIAN_ORGANISASI => 87, Revisi1MasterKegiatanPeer::IS_BAGIAN_ORGANISASI_SETUJU => 88, Revisi1MasterKegiatanPeer::VERIFIKASI_BAGIAN_ORGANISASI => 89, ),
		BasePeer::TYPE_FIELDNAME => array ('unit_id' => 0, 'kode_kegiatan' => 1, 'kode_bidang' => 2, 'kode_urusan_wajib' => 3, 'kode_program' => 4, 'kode_sasaran' => 5, 'kode_indikator' => 6, 'alokasi_dana' => 7, 'nama_kegiatan' => 8, 'masukan' => 9, 'output' => 10, 'outcome' => 11, 'benefit' => 12, 'impact' => 13, 'tipe' => 14, 'kegiatan_active' => 15, 'to_kegiatan_code' => 16, 'catatan' => 17, 'target_outcome' => 18, 'lokasi' => 19, 'jumlah_prev' => 20, 'jumlah_now' => 21, 'jumlah_next' => 22, 'kode_program2' => 23, 'kode_urusan' => 24, 'last_update_user' => 25, 'last_update_time' => 26, 'last_update_ip' => 27, 'tahap' => 28, 'kode_misi' => 29, 'kode_tujuan' => 30, 'ranking' => 31, 'nomor13' => 32, 'ppa_nama' => 33, 'ppa_pangkat' => 34, 'ppa_nip' => 35, 'lanjutan' => 36, 'user_id' => 37, 'id' => 38, 'tahun' => 39, 'tambahan_pagu' => 40, 'gender' => 41, 'kode_keg_keuangan' => 42, 'indikator' => 43, 'is_dak' => 44, 'kode_kegiatan_asal' => 45, 'kode_keg_keuangan_asal' => 46, 'th_ke_multiyears' => 47, 'kelompok_sasaran' => 48, 'pagu_bappeko' => 49, 'kode_dpa' => 50, 'user_id_pptk' => 51, 'user_id_kpa' => 52, 'catatan_pembahasan' => 53, 'catatan_penyelia' => 54, 'catatan_bappeko' => 55, 'status_level' => 56, 'is_tapd_setuju' => 57, 'is_bappeko_setuju' => 58, 'is_penyelia_setuju' => 59, 'is_pernah_rka' => 60, 'kode_kegiatan_baru' => 61, 'catatan_bpkpd' => 62, 'ubah_f1_dinas' => 63, 'ubah_f1_peneliti' => 64, 'sisa_lelang_dinas' => 65, 'sisa_lelang_peneliti' => 66, 'catatan_ubah_f1_dinas' => 67, 'catatan_sisa_lelang_peneliti' => 68, 'pptk_approval' => 69, 'kpa_approval' => 70, 'catatan_bagian_hukum' => 71, 'catatan_inspektorat' => 72, 'catatan_badan_kepegawaian' => 73, 'catatan_lppa' => 74, 'is_bagian_hukum_setuju' => 75, 'is_inspektorat_setuju' => 76, 'is_badan_kepegawaian_setuju' => 77, 'is_lppa_setuju' => 78, 'verifikasi_bpkpd' => 79, 'verifikasi_bappeko' => 80, 'verifikasi_penyelia' => 81, 'verifikasi_bagian_hukum' => 82, 'verifikasi_inspektorat' => 83, 'verifikasi_badan_kepegawaian' => 84, 'verifikasi_lppa' => 85, 'metode_count' => 86, 'catatan_bagian_organisasi' => 87, 'is_bagian_organisasi_setuju' => 88, 'verifikasi_bagian_organisasi' => 89, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/Revisi1MasterKegiatanMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.Revisi1MasterKegiatanMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = Revisi1MasterKegiatanPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(Revisi1MasterKegiatanPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::UNIT_ID);

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::KODE_KEGIATAN);

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::KODE_BIDANG);

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::KODE_URUSAN_WAJIB);

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::KODE_PROGRAM);

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::KODE_SASARAN);

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::KODE_INDIKATOR);

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::ALOKASI_DANA);

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::NAMA_KEGIATAN);

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::MASUKAN);

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::OUTPUT);

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::OUTCOME);

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::BENEFIT);

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::IMPACT);

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::TIPE);

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::KEGIATAN_ACTIVE);

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::TO_KEGIATAN_CODE);

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::CATATAN);

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::TARGET_OUTCOME);

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::LOKASI);

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::JUMLAH_PREV);

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::JUMLAH_NOW);

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::JUMLAH_NEXT);

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::KODE_PROGRAM2);

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::KODE_URUSAN);

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::LAST_UPDATE_USER);

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::LAST_UPDATE_TIME);

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::LAST_UPDATE_IP);

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::TAHAP);

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::KODE_MISI);

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::KODE_TUJUAN);

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::RANKING);

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::NOMOR13);

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::PPA_NAMA);

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::PPA_PANGKAT);

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::PPA_NIP);

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::LANJUTAN);

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::USER_ID);

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::ID);

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::TAHUN);

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::TAMBAHAN_PAGU);

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::GENDER);

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::KODE_KEG_KEUANGAN);

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::INDIKATOR);

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::IS_DAK);

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::KODE_KEGIATAN_ASAL);

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::KODE_KEG_KEUANGAN_ASAL);

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::TH_KE_MULTIYEARS);

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::KELOMPOK_SASARAN);

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::PAGU_BAPPEKO);

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::KODE_DPA);

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::USER_ID_PPTK);

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::USER_ID_KPA);

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::CATATAN_PEMBAHASAN);

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::CATATAN_PENYELIA);

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::CATATAN_BAPPEKO);

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::STATUS_LEVEL);

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::IS_TAPD_SETUJU);

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::IS_BAPPEKO_SETUJU);

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::IS_PENYELIA_SETUJU);

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::IS_PERNAH_RKA);

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::KODE_KEGIATAN_BARU);

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::CATATAN_BPKPD);

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::UBAH_F1_DINAS);

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::UBAH_F1_PENELITI);

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::SISA_LELANG_DINAS);

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::SISA_LELANG_PENELITI);

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::CATATAN_UBAH_F1_DINAS);

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::CATATAN_SISA_LELANG_PENELITI);

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::PPTK_APPROVAL);

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::KPA_APPROVAL);

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::CATATAN_BAGIAN_HUKUM);

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::CATATAN_INSPEKTORAT);

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::CATATAN_BADAN_KEPEGAWAIAN);

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::CATATAN_LPPA);

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::IS_BAGIAN_HUKUM_SETUJU);

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::IS_INSPEKTORAT_SETUJU);

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::IS_BADAN_KEPEGAWAIAN_SETUJU);

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::IS_LPPA_SETUJU);

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::VERIFIKASI_BPKPD);

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::VERIFIKASI_BAPPEKO);

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::VERIFIKASI_PENYELIA);

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::VERIFIKASI_BAGIAN_HUKUM);

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::VERIFIKASI_INSPEKTORAT);

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::VERIFIKASI_BADAN_KEPEGAWAIAN);

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::VERIFIKASI_LPPA);

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::METODE_COUNT);

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::CATATAN_BAGIAN_ORGANISASI);

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::IS_BAGIAN_ORGANISASI_SETUJU);

		$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::VERIFIKASI_BAGIAN_ORGANISASI);

	}

	const COUNT = 'COUNT(ebudget.revisi1_master_kegiatan.UNIT_ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.revisi1_master_kegiatan.UNIT_ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(Revisi1MasterKegiatanPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = Revisi1MasterKegiatanPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = Revisi1MasterKegiatanPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return Revisi1MasterKegiatanPeer::populateObjects(Revisi1MasterKegiatanPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			Revisi1MasterKegiatanPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = Revisi1MasterKegiatanPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return Revisi1MasterKegiatanPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}

		$criteria->remove(Revisi1MasterKegiatanPeer::ID); 

				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(Revisi1MasterKegiatanPeer::UNIT_ID);
			$selectCriteria->add(Revisi1MasterKegiatanPeer::UNIT_ID, $criteria->remove(Revisi1MasterKegiatanPeer::UNIT_ID), $comparison);

			$comparison = $criteria->getComparison(Revisi1MasterKegiatanPeer::KODE_KEGIATAN);
			$selectCriteria->add(Revisi1MasterKegiatanPeer::KODE_KEGIATAN, $criteria->remove(Revisi1MasterKegiatanPeer::KODE_KEGIATAN), $comparison);

			$comparison = $criteria->getComparison(Revisi1MasterKegiatanPeer::ID);
			$selectCriteria->add(Revisi1MasterKegiatanPeer::ID, $criteria->remove(Revisi1MasterKegiatanPeer::ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(Revisi1MasterKegiatanPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(Revisi1MasterKegiatanPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof Revisi1MasterKegiatan) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
												if(count($values) == count($values, COUNT_RECURSIVE))
			{
								$values = array($values);
			}
			$vals = array();
			foreach($values as $value)
			{

				$vals[0][] = $value[0];
				$vals[1][] = $value[1];
				$vals[2][] = $value[2];
			}

			$criteria->add(Revisi1MasterKegiatanPeer::UNIT_ID, $vals[0], Criteria::IN);
			$criteria->add(Revisi1MasterKegiatanPeer::KODE_KEGIATAN, $vals[1], Criteria::IN);
			$criteria->add(Revisi1MasterKegiatanPeer::ID, $vals[2], Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(Revisi1MasterKegiatan $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(Revisi1MasterKegiatanPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(Revisi1MasterKegiatanPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(Revisi1MasterKegiatanPeer::DATABASE_NAME, Revisi1MasterKegiatanPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = Revisi1MasterKegiatanPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK( $unit_id, $kode_kegiatan, $id, $con = null) {
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$criteria = new Criteria();
		$criteria->add(Revisi1MasterKegiatanPeer::UNIT_ID, $unit_id);
		$criteria->add(Revisi1MasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
		$criteria->add(Revisi1MasterKegiatanPeer::ID, $id);
		$v = Revisi1MasterKegiatanPeer::doSelect($criteria, $con);

		return !empty($v) ? $v[0] : null;
	}
} 
if (Propel::isInit()) {
			try {
		BaseRevisi1MasterKegiatanPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/Revisi1MasterKegiatanMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.Revisi1MasterKegiatanMapBuilder');
}
