<?php


abstract class BasePegawaiPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'pegawai';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.Pegawai';

	
	const NUM_COLUMNS = 21;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const NIP = 'pegawai.NIP';

	
	const NIP_LAMA = 'pegawai.NIP_LAMA';

	
	const NAMA = 'pegawai.NAMA';

	
	const TEMPAT_LAHIR = 'pegawai.TEMPAT_LAHIR';

	
	const TANGGAL_LAHIR = 'pegawai.TANGGAL_LAHIR';

	
	const NAMA_PANGKAT = 'pegawai.NAMA_PANGKAT';

	
	const NAMA_GOLONGAN = 'pegawai.NAMA_GOLONGAN';

	
	const TMT_PANGKAT = 'pegawai.TMT_PANGKAT';

	
	const TMT_CPNS = 'pegawai.TMT_CPNS';

	
	const JENIS_KELAMIN = 'pegawai.JENIS_KELAMIN';

	
	const NAMA_AGAMA = 'pegawai.NAMA_AGAMA';

	
	const NAMA_JABATAN = 'pegawai.NAMA_JABATAN';

	
	const ESELON = 'pegawai.ESELON';

	
	const TMT_JABATAN = 'pegawai.TMT_JABATAN';

	
	const NAMA_JURUSAN_SK = 'pegawai.NAMA_JURUSAN_SK';

	
	const LULUS = 'pegawai.LULUS';

	
	const JENIS_PEGAWAI = 'pegawai.JENIS_PEGAWAI';

	
	const NAMA_INSTANSI = 'pegawai.NAMA_INSTANSI';

	
	const NAMA_UNIT_KERJA = 'pegawai.NAMA_UNIT_KERJA';

	
	const NAMA_UNIT_KERJA2 = 'pegawai.NAMA_UNIT_KERJA2';

	
	const UNIT_ID = 'pegawai.UNIT_ID';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('Nip', 'NipLama', 'Nama', 'TempatLahir', 'TanggalLahir', 'NamaPangkat', 'NamaGolongan', 'TmtPangkat', 'TmtCpns', 'JenisKelamin', 'NamaAgama', 'NamaJabatan', 'Eselon', 'TmtJabatan', 'NamaJurusanSk', 'Lulus', 'JenisPegawai', 'NamaInstansi', 'NamaUnitKerja', 'NamaUnitKerja2', 'UnitId', ),
		BasePeer::TYPE_COLNAME => array (PegawaiPeer::NIP, PegawaiPeer::NIP_LAMA, PegawaiPeer::NAMA, PegawaiPeer::TEMPAT_LAHIR, PegawaiPeer::TANGGAL_LAHIR, PegawaiPeer::NAMA_PANGKAT, PegawaiPeer::NAMA_GOLONGAN, PegawaiPeer::TMT_PANGKAT, PegawaiPeer::TMT_CPNS, PegawaiPeer::JENIS_KELAMIN, PegawaiPeer::NAMA_AGAMA, PegawaiPeer::NAMA_JABATAN, PegawaiPeer::ESELON, PegawaiPeer::TMT_JABATAN, PegawaiPeer::NAMA_JURUSAN_SK, PegawaiPeer::LULUS, PegawaiPeer::JENIS_PEGAWAI, PegawaiPeer::NAMA_INSTANSI, PegawaiPeer::NAMA_UNIT_KERJA, PegawaiPeer::NAMA_UNIT_KERJA2, PegawaiPeer::UNIT_ID, ),
		BasePeer::TYPE_FIELDNAME => array ('nip', 'nip_lama', 'nama', 'tempat_lahir', 'tanggal_lahir', 'nama_pangkat', 'nama_golongan', 'tmt_pangkat', 'tmt_cpns', 'jenis_kelamin', 'nama_agama', 'nama_jabatan', 'eselon', 'tmt_jabatan', 'nama_jurusan_sk', 'lulus', 'jenis_pegawai', 'nama_instansi', 'nama_unit_kerja', 'nama_unit_kerja2', 'unit_id', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('Nip' => 0, 'NipLama' => 1, 'Nama' => 2, 'TempatLahir' => 3, 'TanggalLahir' => 4, 'NamaPangkat' => 5, 'NamaGolongan' => 6, 'TmtPangkat' => 7, 'TmtCpns' => 8, 'JenisKelamin' => 9, 'NamaAgama' => 10, 'NamaJabatan' => 11, 'Eselon' => 12, 'TmtJabatan' => 13, 'NamaJurusanSk' => 14, 'Lulus' => 15, 'JenisPegawai' => 16, 'NamaInstansi' => 17, 'NamaUnitKerja' => 18, 'NamaUnitKerja2' => 19, 'UnitId' => 20, ),
		BasePeer::TYPE_COLNAME => array (PegawaiPeer::NIP => 0, PegawaiPeer::NIP_LAMA => 1, PegawaiPeer::NAMA => 2, PegawaiPeer::TEMPAT_LAHIR => 3, PegawaiPeer::TANGGAL_LAHIR => 4, PegawaiPeer::NAMA_PANGKAT => 5, PegawaiPeer::NAMA_GOLONGAN => 6, PegawaiPeer::TMT_PANGKAT => 7, PegawaiPeer::TMT_CPNS => 8, PegawaiPeer::JENIS_KELAMIN => 9, PegawaiPeer::NAMA_AGAMA => 10, PegawaiPeer::NAMA_JABATAN => 11, PegawaiPeer::ESELON => 12, PegawaiPeer::TMT_JABATAN => 13, PegawaiPeer::NAMA_JURUSAN_SK => 14, PegawaiPeer::LULUS => 15, PegawaiPeer::JENIS_PEGAWAI => 16, PegawaiPeer::NAMA_INSTANSI => 17, PegawaiPeer::NAMA_UNIT_KERJA => 18, PegawaiPeer::NAMA_UNIT_KERJA2 => 19, PegawaiPeer::UNIT_ID => 20, ),
		BasePeer::TYPE_FIELDNAME => array ('nip' => 0, 'nip_lama' => 1, 'nama' => 2, 'tempat_lahir' => 3, 'tanggal_lahir' => 4, 'nama_pangkat' => 5, 'nama_golongan' => 6, 'tmt_pangkat' => 7, 'tmt_cpns' => 8, 'jenis_kelamin' => 9, 'nama_agama' => 10, 'nama_jabatan' => 11, 'eselon' => 12, 'tmt_jabatan' => 13, 'nama_jurusan_sk' => 14, 'lulus' => 15, 'jenis_pegawai' => 16, 'nama_instansi' => 17, 'nama_unit_kerja' => 18, 'nama_unit_kerja2' => 19, 'unit_id' => 20, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/PegawaiMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.PegawaiMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = PegawaiPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(PegawaiPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(PegawaiPeer::NIP);

		$criteria->addSelectColumn(PegawaiPeer::NIP_LAMA);

		$criteria->addSelectColumn(PegawaiPeer::NAMA);

		$criteria->addSelectColumn(PegawaiPeer::TEMPAT_LAHIR);

		$criteria->addSelectColumn(PegawaiPeer::TANGGAL_LAHIR);

		$criteria->addSelectColumn(PegawaiPeer::NAMA_PANGKAT);

		$criteria->addSelectColumn(PegawaiPeer::NAMA_GOLONGAN);

		$criteria->addSelectColumn(PegawaiPeer::TMT_PANGKAT);

		$criteria->addSelectColumn(PegawaiPeer::TMT_CPNS);

		$criteria->addSelectColumn(PegawaiPeer::JENIS_KELAMIN);

		$criteria->addSelectColumn(PegawaiPeer::NAMA_AGAMA);

		$criteria->addSelectColumn(PegawaiPeer::NAMA_JABATAN);

		$criteria->addSelectColumn(PegawaiPeer::ESELON);

		$criteria->addSelectColumn(PegawaiPeer::TMT_JABATAN);

		$criteria->addSelectColumn(PegawaiPeer::NAMA_JURUSAN_SK);

		$criteria->addSelectColumn(PegawaiPeer::LULUS);

		$criteria->addSelectColumn(PegawaiPeer::JENIS_PEGAWAI);

		$criteria->addSelectColumn(PegawaiPeer::NAMA_INSTANSI);

		$criteria->addSelectColumn(PegawaiPeer::NAMA_UNIT_KERJA);

		$criteria->addSelectColumn(PegawaiPeer::NAMA_UNIT_KERJA2);

		$criteria->addSelectColumn(PegawaiPeer::UNIT_ID);

	}

	const COUNT = 'COUNT(*)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT *)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(PegawaiPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(PegawaiPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = PegawaiPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = PegawaiPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return PegawaiPeer::populateObjects(PegawaiPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			PegawaiPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = PegawaiPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return PegawaiPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(PegawaiPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(PegawaiPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof Pegawai) {

			$criteria = $values->buildCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
												if(count($values) == count($values, COUNT_RECURSIVE))
			{
								$values = array($values);
			}
			$vals = array();
			foreach($values as $value)
			{

			}

		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(Pegawai $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(PegawaiPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(PegawaiPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(PegawaiPeer::DATABASE_NAME, PegawaiPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = PegawaiPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

} 
if (Propel::isInit()) {
			try {
		BasePegawaiPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/PegawaiMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.PegawaiMapBuilder');
}
