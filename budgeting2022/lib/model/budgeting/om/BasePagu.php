<?php


abstract class BasePagu extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $unit_id;


	
	protected $kode_kegiatan;


	
	protected $alokasi;


	
	protected $penyesuaian_uk;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getUnitId()
	{

		return $this->unit_id;
	}

	
	public function getKodeKegiatan()
	{

		return $this->kode_kegiatan;
	}

	
	public function getAlokasi()
	{

		return $this->alokasi;
	}

	
	public function getPenyesuaianUk()
	{

		return $this->penyesuaian_uk;
	}

	
	public function setUnitId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->unit_id !== $v) {
			$this->unit_id = $v;
			$this->modifiedColumns[] = PaguPeer::UNIT_ID;
		}

	} 
	
	public function setKodeKegiatan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_kegiatan !== $v) {
			$this->kode_kegiatan = $v;
			$this->modifiedColumns[] = PaguPeer::KODE_KEGIATAN;
		}

	} 
	
	public function setAlokasi($v)
	{

		if ($this->alokasi !== $v) {
			$this->alokasi = $v;
			$this->modifiedColumns[] = PaguPeer::ALOKASI;
		}

	} 
	
	public function setPenyesuaianUk($v)
	{

		if ($this->penyesuaian_uk !== $v) {
			$this->penyesuaian_uk = $v;
			$this->modifiedColumns[] = PaguPeer::PENYESUAIAN_UK;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->unit_id = $rs->getString($startcol + 0);

			$this->kode_kegiatan = $rs->getString($startcol + 1);

			$this->alokasi = $rs->getFloat($startcol + 2);

			$this->penyesuaian_uk = $rs->getFloat($startcol + 3);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 4; 
		} catch (Exception $e) {
			throw new PropelException("Error populating Pagu object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(PaguPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			PaguPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(PaguPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = PaguPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += PaguPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = PaguPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = PaguPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getUnitId();
				break;
			case 1:
				return $this->getKodeKegiatan();
				break;
			case 2:
				return $this->getAlokasi();
				break;
			case 3:
				return $this->getPenyesuaianUk();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = PaguPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getUnitId(),
			$keys[1] => $this->getKodeKegiatan(),
			$keys[2] => $this->getAlokasi(),
			$keys[3] => $this->getPenyesuaianUk(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = PaguPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setUnitId($value);
				break;
			case 1:
				$this->setKodeKegiatan($value);
				break;
			case 2:
				$this->setAlokasi($value);
				break;
			case 3:
				$this->setPenyesuaianUk($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = PaguPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setUnitId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setKodeKegiatan($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setAlokasi($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setPenyesuaianUk($arr[$keys[3]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(PaguPeer::DATABASE_NAME);

		if ($this->isColumnModified(PaguPeer::UNIT_ID)) $criteria->add(PaguPeer::UNIT_ID, $this->unit_id);
		if ($this->isColumnModified(PaguPeer::KODE_KEGIATAN)) $criteria->add(PaguPeer::KODE_KEGIATAN, $this->kode_kegiatan);
		if ($this->isColumnModified(PaguPeer::ALOKASI)) $criteria->add(PaguPeer::ALOKASI, $this->alokasi);
		if ($this->isColumnModified(PaguPeer::PENYESUAIAN_UK)) $criteria->add(PaguPeer::PENYESUAIAN_UK, $this->penyesuaian_uk);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(PaguPeer::DATABASE_NAME);

		$criteria->add(PaguPeer::UNIT_ID, $this->unit_id);
		$criteria->add(PaguPeer::KODE_KEGIATAN, $this->kode_kegiatan);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		$pks = array();

		$pks[0] = $this->getUnitId();

		$pks[1] = $this->getKodeKegiatan();

		return $pks;
	}

	
	public function setPrimaryKey($keys)
	{

		$this->setUnitId($keys[0]);

		$this->setKodeKegiatan($keys[1]);

	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setAlokasi($this->alokasi);

		$copyObj->setPenyesuaianUk($this->penyesuaian_uk);


		$copyObj->setNew(true);

		$copyObj->setUnitId(NULL); 
		$copyObj->setKodeKegiatan(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new PaguPeer();
		}
		return self::$peer;
	}

} 