<?php


abstract class BasePdfFile extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $pdf_id;


	
	protected $unit_id;


	
	protected $kegiatan_code;


	
	protected $sub_id;


	
	protected $subtitle;


	
	protected $nama_asli;


	
	protected $nama_alias;


	
	protected $hash;


	
	protected $user_id;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getPdfId()
	{

		return $this->pdf_id;
	}

	
	public function getUnitId()
	{

		return $this->unit_id;
	}

	
	public function getKegiatanCode()
	{

		return $this->kegiatan_code;
	}

	
	public function getSubId()
	{

		return $this->sub_id;
	}

	
	public function getSubtitle()
	{

		return $this->subtitle;
	}

	
	public function getNamaAsli()
	{

		return $this->nama_asli;
	}

	
	public function getNamaAlias()
	{

		return $this->nama_alias;
	}

	
	public function getHash()
	{

		return $this->hash;
	}

	
	public function getUserId()
	{

		return $this->user_id;
	}

	
	public function setPdfId($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->pdf_id !== $v) {
			$this->pdf_id = $v;
			$this->modifiedColumns[] = PdfFilePeer::PDF_ID;
		}

	} 
	
	public function setUnitId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->unit_id !== $v) {
			$this->unit_id = $v;
			$this->modifiedColumns[] = PdfFilePeer::UNIT_ID;
		}

	} 
	
	public function setKegiatanCode($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kegiatan_code !== $v) {
			$this->kegiatan_code = $v;
			$this->modifiedColumns[] = PdfFilePeer::KEGIATAN_CODE;
		}

	} 
	
	public function setSubId($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->sub_id !== $v) {
			$this->sub_id = $v;
			$this->modifiedColumns[] = PdfFilePeer::SUB_ID;
		}

	} 
	
	public function setSubtitle($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->subtitle !== $v) {
			$this->subtitle = $v;
			$this->modifiedColumns[] = PdfFilePeer::SUBTITLE;
		}

	} 
	
	public function setNamaAsli($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nama_asli !== $v) {
			$this->nama_asli = $v;
			$this->modifiedColumns[] = PdfFilePeer::NAMA_ASLI;
		}

	} 
	
	public function setNamaAlias($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nama_alias !== $v) {
			$this->nama_alias = $v;
			$this->modifiedColumns[] = PdfFilePeer::NAMA_ALIAS;
		}

	} 
	
	public function setHash($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->hash !== $v) {
			$this->hash = $v;
			$this->modifiedColumns[] = PdfFilePeer::HASH;
		}

	} 
	
	public function setUserId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->user_id !== $v) {
			$this->user_id = $v;
			$this->modifiedColumns[] = PdfFilePeer::USER_ID;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->pdf_id = $rs->getInt($startcol + 0);

			$this->unit_id = $rs->getString($startcol + 1);

			$this->kegiatan_code = $rs->getString($startcol + 2);

			$this->sub_id = $rs->getInt($startcol + 3);

			$this->subtitle = $rs->getString($startcol + 4);

			$this->nama_asli = $rs->getString($startcol + 5);

			$this->nama_alias = $rs->getString($startcol + 6);

			$this->hash = $rs->getString($startcol + 7);

			$this->user_id = $rs->getString($startcol + 8);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 9; 
		} catch (Exception $e) {
			throw new PropelException("Error populating PdfFile object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(PdfFilePeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			PdfFilePeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(PdfFilePeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = PdfFilePeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setPdfId($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += PdfFilePeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = PdfFilePeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = PdfFilePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getPdfId();
				break;
			case 1:
				return $this->getUnitId();
				break;
			case 2:
				return $this->getKegiatanCode();
				break;
			case 3:
				return $this->getSubId();
				break;
			case 4:
				return $this->getSubtitle();
				break;
			case 5:
				return $this->getNamaAsli();
				break;
			case 6:
				return $this->getNamaAlias();
				break;
			case 7:
				return $this->getHash();
				break;
			case 8:
				return $this->getUserId();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = PdfFilePeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getPdfId(),
			$keys[1] => $this->getUnitId(),
			$keys[2] => $this->getKegiatanCode(),
			$keys[3] => $this->getSubId(),
			$keys[4] => $this->getSubtitle(),
			$keys[5] => $this->getNamaAsli(),
			$keys[6] => $this->getNamaAlias(),
			$keys[7] => $this->getHash(),
			$keys[8] => $this->getUserId(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = PdfFilePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setPdfId($value);
				break;
			case 1:
				$this->setUnitId($value);
				break;
			case 2:
				$this->setKegiatanCode($value);
				break;
			case 3:
				$this->setSubId($value);
				break;
			case 4:
				$this->setSubtitle($value);
				break;
			case 5:
				$this->setNamaAsli($value);
				break;
			case 6:
				$this->setNamaAlias($value);
				break;
			case 7:
				$this->setHash($value);
				break;
			case 8:
				$this->setUserId($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = PdfFilePeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setPdfId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setUnitId($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setKegiatanCode($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setSubId($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setSubtitle($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setNamaAsli($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setNamaAlias($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setHash($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setUserId($arr[$keys[8]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(PdfFilePeer::DATABASE_NAME);

		if ($this->isColumnModified(PdfFilePeer::PDF_ID)) $criteria->add(PdfFilePeer::PDF_ID, $this->pdf_id);
		if ($this->isColumnModified(PdfFilePeer::UNIT_ID)) $criteria->add(PdfFilePeer::UNIT_ID, $this->unit_id);
		if ($this->isColumnModified(PdfFilePeer::KEGIATAN_CODE)) $criteria->add(PdfFilePeer::KEGIATAN_CODE, $this->kegiatan_code);
		if ($this->isColumnModified(PdfFilePeer::SUB_ID)) $criteria->add(PdfFilePeer::SUB_ID, $this->sub_id);
		if ($this->isColumnModified(PdfFilePeer::SUBTITLE)) $criteria->add(PdfFilePeer::SUBTITLE, $this->subtitle);
		if ($this->isColumnModified(PdfFilePeer::NAMA_ASLI)) $criteria->add(PdfFilePeer::NAMA_ASLI, $this->nama_asli);
		if ($this->isColumnModified(PdfFilePeer::NAMA_ALIAS)) $criteria->add(PdfFilePeer::NAMA_ALIAS, $this->nama_alias);
		if ($this->isColumnModified(PdfFilePeer::HASH)) $criteria->add(PdfFilePeer::HASH, $this->hash);
		if ($this->isColumnModified(PdfFilePeer::USER_ID)) $criteria->add(PdfFilePeer::USER_ID, $this->user_id);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(PdfFilePeer::DATABASE_NAME);

		$criteria->add(PdfFilePeer::PDF_ID, $this->pdf_id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getPdfId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setPdfId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setUnitId($this->unit_id);

		$copyObj->setKegiatanCode($this->kegiatan_code);

		$copyObj->setSubId($this->sub_id);

		$copyObj->setSubtitle($this->subtitle);

		$copyObj->setNamaAsli($this->nama_asli);

		$copyObj->setNamaAlias($this->nama_alias);

		$copyObj->setHash($this->hash);

		$copyObj->setUserId($this->user_id);


		$copyObj->setNew(true);

		$copyObj->setPdfId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new PdfFilePeer();
		}
		return self::$peer;
	}

} 