<?php


abstract class BaseBeritaAcaraRevisi extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $unit_id;


	
	protected $kegiatan_code;


	
	protected $nomor;


	
	protected $kesimpulan;


	
	protected $dasar1;


	
	protected $dasar2;


	
	protected $dasar3;


	
	protected $dasar4;


	
	protected $dasar5;


	
	protected $dasar6;


	
	protected $dasar7;


	
	protected $dasar8;


	
	protected $dasar9;


	
	protected $dasar10;


	
	protected $pertimbangan1;


	
	protected $pertimbangan2;


	
	protected $pertimbangan3;


	
	protected $pertimbangan4;


	
	protected $pertimbangan5;


	
	protected $pertimbangan6;


	
	protected $pertimbangan7;


	
	protected $pertimbangan8;


	
	protected $pertimbangan9;


	
	protected $pertimbangan10;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getUnitId()
	{

		return $this->unit_id;
	}

	
	public function getKegiatanCode()
	{

		return $this->kegiatan_code;
	}

	
	public function getNomor()
	{

		return $this->nomor;
	}

	
	public function getKesimpulan()
	{

		return $this->kesimpulan;
	}

	
	public function getDasar1()
	{

		return $this->dasar1;
	}

	
	public function getDasar2()
	{

		return $this->dasar2;
	}

	
	public function getDasar3()
	{

		return $this->dasar3;
	}

	
	public function getDasar4()
	{

		return $this->dasar4;
	}

	
	public function getDasar5()
	{

		return $this->dasar5;
	}

	
	public function getDasar6()
	{

		return $this->dasar6;
	}

	
	public function getDasar7()
	{

		return $this->dasar7;
	}

	
	public function getDasar8()
	{

		return $this->dasar8;
	}

	
	public function getDasar9()
	{

		return $this->dasar9;
	}

	
	public function getDasar10()
	{

		return $this->dasar10;
	}

	
	public function getPertimbangan1()
	{

		return $this->pertimbangan1;
	}

	
	public function getPertimbangan2()
	{

		return $this->pertimbangan2;
	}

	
	public function getPertimbangan3()
	{

		return $this->pertimbangan3;
	}

	
	public function getPertimbangan4()
	{

		return $this->pertimbangan4;
	}

	
	public function getPertimbangan5()
	{

		return $this->pertimbangan5;
	}

	
	public function getPertimbangan6()
	{

		return $this->pertimbangan6;
	}

	
	public function getPertimbangan7()
	{

		return $this->pertimbangan7;
	}

	
	public function getPertimbangan8()
	{

		return $this->pertimbangan8;
	}

	
	public function getPertimbangan9()
	{

		return $this->pertimbangan9;
	}

	
	public function getPertimbangan10()
	{

		return $this->pertimbangan10;
	}

	
	public function setUnitId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->unit_id !== $v) {
			$this->unit_id = $v;
			$this->modifiedColumns[] = BeritaAcaraRevisiPeer::UNIT_ID;
		}

	} 
	
	public function setKegiatanCode($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kegiatan_code !== $v) {
			$this->kegiatan_code = $v;
			$this->modifiedColumns[] = BeritaAcaraRevisiPeer::KEGIATAN_CODE;
		}

	} 
	
	public function setNomor($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nomor !== $v) {
			$this->nomor = $v;
			$this->modifiedColumns[] = BeritaAcaraRevisiPeer::NOMOR;
		}

	} 
	
	public function setKesimpulan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kesimpulan !== $v) {
			$this->kesimpulan = $v;
			$this->modifiedColumns[] = BeritaAcaraRevisiPeer::KESIMPULAN;
		}

	} 
	
	public function setDasar1($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->dasar1 !== $v) {
			$this->dasar1 = $v;
			$this->modifiedColumns[] = BeritaAcaraRevisiPeer::DASAR1;
		}

	} 
	
	public function setDasar2($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->dasar2 !== $v) {
			$this->dasar2 = $v;
			$this->modifiedColumns[] = BeritaAcaraRevisiPeer::DASAR2;
		}

	} 
	
	public function setDasar3($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->dasar3 !== $v) {
			$this->dasar3 = $v;
			$this->modifiedColumns[] = BeritaAcaraRevisiPeer::DASAR3;
		}

	} 
	
	public function setDasar4($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->dasar4 !== $v) {
			$this->dasar4 = $v;
			$this->modifiedColumns[] = BeritaAcaraRevisiPeer::DASAR4;
		}

	} 
	
	public function setDasar5($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->dasar5 !== $v) {
			$this->dasar5 = $v;
			$this->modifiedColumns[] = BeritaAcaraRevisiPeer::DASAR5;
		}

	} 
	
	public function setDasar6($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->dasar6 !== $v) {
			$this->dasar6 = $v;
			$this->modifiedColumns[] = BeritaAcaraRevisiPeer::DASAR6;
		}

	} 
	
	public function setDasar7($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->dasar7 !== $v) {
			$this->dasar7 = $v;
			$this->modifiedColumns[] = BeritaAcaraRevisiPeer::DASAR7;
		}

	} 
	
	public function setDasar8($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->dasar8 !== $v) {
			$this->dasar8 = $v;
			$this->modifiedColumns[] = BeritaAcaraRevisiPeer::DASAR8;
		}

	} 
	
	public function setDasar9($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->dasar9 !== $v) {
			$this->dasar9 = $v;
			$this->modifiedColumns[] = BeritaAcaraRevisiPeer::DASAR9;
		}

	} 
	
	public function setDasar10($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->dasar10 !== $v) {
			$this->dasar10 = $v;
			$this->modifiedColumns[] = BeritaAcaraRevisiPeer::DASAR10;
		}

	} 
	
	public function setPertimbangan1($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->pertimbangan1 !== $v) {
			$this->pertimbangan1 = $v;
			$this->modifiedColumns[] = BeritaAcaraRevisiPeer::PERTIMBANGAN1;
		}

	} 
	
	public function setPertimbangan2($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->pertimbangan2 !== $v) {
			$this->pertimbangan2 = $v;
			$this->modifiedColumns[] = BeritaAcaraRevisiPeer::PERTIMBANGAN2;
		}

	} 
	
	public function setPertimbangan3($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->pertimbangan3 !== $v) {
			$this->pertimbangan3 = $v;
			$this->modifiedColumns[] = BeritaAcaraRevisiPeer::PERTIMBANGAN3;
		}

	} 
	
	public function setPertimbangan4($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->pertimbangan4 !== $v) {
			$this->pertimbangan4 = $v;
			$this->modifiedColumns[] = BeritaAcaraRevisiPeer::PERTIMBANGAN4;
		}

	} 
	
	public function setPertimbangan5($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->pertimbangan5 !== $v) {
			$this->pertimbangan5 = $v;
			$this->modifiedColumns[] = BeritaAcaraRevisiPeer::PERTIMBANGAN5;
		}

	} 
	
	public function setPertimbangan6($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->pertimbangan6 !== $v) {
			$this->pertimbangan6 = $v;
			$this->modifiedColumns[] = BeritaAcaraRevisiPeer::PERTIMBANGAN6;
		}

	} 
	
	public function setPertimbangan7($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->pertimbangan7 !== $v) {
			$this->pertimbangan7 = $v;
			$this->modifiedColumns[] = BeritaAcaraRevisiPeer::PERTIMBANGAN7;
		}

	} 
	
	public function setPertimbangan8($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->pertimbangan8 !== $v) {
			$this->pertimbangan8 = $v;
			$this->modifiedColumns[] = BeritaAcaraRevisiPeer::PERTIMBANGAN8;
		}

	} 
	
	public function setPertimbangan9($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->pertimbangan9 !== $v) {
			$this->pertimbangan9 = $v;
			$this->modifiedColumns[] = BeritaAcaraRevisiPeer::PERTIMBANGAN9;
		}

	} 
	
	public function setPertimbangan10($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->pertimbangan10 !== $v) {
			$this->pertimbangan10 = $v;
			$this->modifiedColumns[] = BeritaAcaraRevisiPeer::PERTIMBANGAN10;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->unit_id = $rs->getString($startcol + 0);

			$this->kegiatan_code = $rs->getString($startcol + 1);

			$this->nomor = $rs->getString($startcol + 2);

			$this->kesimpulan = $rs->getString($startcol + 3);

			$this->dasar1 = $rs->getString($startcol + 4);

			$this->dasar2 = $rs->getString($startcol + 5);

			$this->dasar3 = $rs->getString($startcol + 6);

			$this->dasar4 = $rs->getString($startcol + 7);

			$this->dasar5 = $rs->getString($startcol + 8);

			$this->dasar6 = $rs->getString($startcol + 9);

			$this->dasar7 = $rs->getString($startcol + 10);

			$this->dasar8 = $rs->getString($startcol + 11);

			$this->dasar9 = $rs->getString($startcol + 12);

			$this->dasar10 = $rs->getString($startcol + 13);

			$this->pertimbangan1 = $rs->getString($startcol + 14);

			$this->pertimbangan2 = $rs->getString($startcol + 15);

			$this->pertimbangan3 = $rs->getString($startcol + 16);

			$this->pertimbangan4 = $rs->getString($startcol + 17);

			$this->pertimbangan5 = $rs->getString($startcol + 18);

			$this->pertimbangan6 = $rs->getString($startcol + 19);

			$this->pertimbangan7 = $rs->getString($startcol + 20);

			$this->pertimbangan8 = $rs->getString($startcol + 21);

			$this->pertimbangan9 = $rs->getString($startcol + 22);

			$this->pertimbangan10 = $rs->getString($startcol + 23);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 24; 
		} catch (Exception $e) {
			throw new PropelException("Error populating BeritaAcaraRevisi object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(BeritaAcaraRevisiPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			BeritaAcaraRevisiPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(BeritaAcaraRevisiPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = BeritaAcaraRevisiPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += BeritaAcaraRevisiPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = BeritaAcaraRevisiPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = BeritaAcaraRevisiPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getUnitId();
				break;
			case 1:
				return $this->getKegiatanCode();
				break;
			case 2:
				return $this->getNomor();
				break;
			case 3:
				return $this->getKesimpulan();
				break;
			case 4:
				return $this->getDasar1();
				break;
			case 5:
				return $this->getDasar2();
				break;
			case 6:
				return $this->getDasar3();
				break;
			case 7:
				return $this->getDasar4();
				break;
			case 8:
				return $this->getDasar5();
				break;
			case 9:
				return $this->getDasar6();
				break;
			case 10:
				return $this->getDasar7();
				break;
			case 11:
				return $this->getDasar8();
				break;
			case 12:
				return $this->getDasar9();
				break;
			case 13:
				return $this->getDasar10();
				break;
			case 14:
				return $this->getPertimbangan1();
				break;
			case 15:
				return $this->getPertimbangan2();
				break;
			case 16:
				return $this->getPertimbangan3();
				break;
			case 17:
				return $this->getPertimbangan4();
				break;
			case 18:
				return $this->getPertimbangan5();
				break;
			case 19:
				return $this->getPertimbangan6();
				break;
			case 20:
				return $this->getPertimbangan7();
				break;
			case 21:
				return $this->getPertimbangan8();
				break;
			case 22:
				return $this->getPertimbangan9();
				break;
			case 23:
				return $this->getPertimbangan10();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = BeritaAcaraRevisiPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getUnitId(),
			$keys[1] => $this->getKegiatanCode(),
			$keys[2] => $this->getNomor(),
			$keys[3] => $this->getKesimpulan(),
			$keys[4] => $this->getDasar1(),
			$keys[5] => $this->getDasar2(),
			$keys[6] => $this->getDasar3(),
			$keys[7] => $this->getDasar4(),
			$keys[8] => $this->getDasar5(),
			$keys[9] => $this->getDasar6(),
			$keys[10] => $this->getDasar7(),
			$keys[11] => $this->getDasar8(),
			$keys[12] => $this->getDasar9(),
			$keys[13] => $this->getDasar10(),
			$keys[14] => $this->getPertimbangan1(),
			$keys[15] => $this->getPertimbangan2(),
			$keys[16] => $this->getPertimbangan3(),
			$keys[17] => $this->getPertimbangan4(),
			$keys[18] => $this->getPertimbangan5(),
			$keys[19] => $this->getPertimbangan6(),
			$keys[20] => $this->getPertimbangan7(),
			$keys[21] => $this->getPertimbangan8(),
			$keys[22] => $this->getPertimbangan9(),
			$keys[23] => $this->getPertimbangan10(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = BeritaAcaraRevisiPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setUnitId($value);
				break;
			case 1:
				$this->setKegiatanCode($value);
				break;
			case 2:
				$this->setNomor($value);
				break;
			case 3:
				$this->setKesimpulan($value);
				break;
			case 4:
				$this->setDasar1($value);
				break;
			case 5:
				$this->setDasar2($value);
				break;
			case 6:
				$this->setDasar3($value);
				break;
			case 7:
				$this->setDasar4($value);
				break;
			case 8:
				$this->setDasar5($value);
				break;
			case 9:
				$this->setDasar6($value);
				break;
			case 10:
				$this->setDasar7($value);
				break;
			case 11:
				$this->setDasar8($value);
				break;
			case 12:
				$this->setDasar9($value);
				break;
			case 13:
				$this->setDasar10($value);
				break;
			case 14:
				$this->setPertimbangan1($value);
				break;
			case 15:
				$this->setPertimbangan2($value);
				break;
			case 16:
				$this->setPertimbangan3($value);
				break;
			case 17:
				$this->setPertimbangan4($value);
				break;
			case 18:
				$this->setPertimbangan5($value);
				break;
			case 19:
				$this->setPertimbangan6($value);
				break;
			case 20:
				$this->setPertimbangan7($value);
				break;
			case 21:
				$this->setPertimbangan8($value);
				break;
			case 22:
				$this->setPertimbangan9($value);
				break;
			case 23:
				$this->setPertimbangan10($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = BeritaAcaraRevisiPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setUnitId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setKegiatanCode($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setNomor($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setKesimpulan($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setDasar1($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setDasar2($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setDasar3($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setDasar4($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setDasar5($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setDasar6($arr[$keys[9]]);
		if (array_key_exists($keys[10], $arr)) $this->setDasar7($arr[$keys[10]]);
		if (array_key_exists($keys[11], $arr)) $this->setDasar8($arr[$keys[11]]);
		if (array_key_exists($keys[12], $arr)) $this->setDasar9($arr[$keys[12]]);
		if (array_key_exists($keys[13], $arr)) $this->setDasar10($arr[$keys[13]]);
		if (array_key_exists($keys[14], $arr)) $this->setPertimbangan1($arr[$keys[14]]);
		if (array_key_exists($keys[15], $arr)) $this->setPertimbangan2($arr[$keys[15]]);
		if (array_key_exists($keys[16], $arr)) $this->setPertimbangan3($arr[$keys[16]]);
		if (array_key_exists($keys[17], $arr)) $this->setPertimbangan4($arr[$keys[17]]);
		if (array_key_exists($keys[18], $arr)) $this->setPertimbangan5($arr[$keys[18]]);
		if (array_key_exists($keys[19], $arr)) $this->setPertimbangan6($arr[$keys[19]]);
		if (array_key_exists($keys[20], $arr)) $this->setPertimbangan7($arr[$keys[20]]);
		if (array_key_exists($keys[21], $arr)) $this->setPertimbangan8($arr[$keys[21]]);
		if (array_key_exists($keys[22], $arr)) $this->setPertimbangan9($arr[$keys[22]]);
		if (array_key_exists($keys[23], $arr)) $this->setPertimbangan10($arr[$keys[23]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(BeritaAcaraRevisiPeer::DATABASE_NAME);

		if ($this->isColumnModified(BeritaAcaraRevisiPeer::UNIT_ID)) $criteria->add(BeritaAcaraRevisiPeer::UNIT_ID, $this->unit_id);
		if ($this->isColumnModified(BeritaAcaraRevisiPeer::KEGIATAN_CODE)) $criteria->add(BeritaAcaraRevisiPeer::KEGIATAN_CODE, $this->kegiatan_code);
		if ($this->isColumnModified(BeritaAcaraRevisiPeer::NOMOR)) $criteria->add(BeritaAcaraRevisiPeer::NOMOR, $this->nomor);
		if ($this->isColumnModified(BeritaAcaraRevisiPeer::KESIMPULAN)) $criteria->add(BeritaAcaraRevisiPeer::KESIMPULAN, $this->kesimpulan);
		if ($this->isColumnModified(BeritaAcaraRevisiPeer::DASAR1)) $criteria->add(BeritaAcaraRevisiPeer::DASAR1, $this->dasar1);
		if ($this->isColumnModified(BeritaAcaraRevisiPeer::DASAR2)) $criteria->add(BeritaAcaraRevisiPeer::DASAR2, $this->dasar2);
		if ($this->isColumnModified(BeritaAcaraRevisiPeer::DASAR3)) $criteria->add(BeritaAcaraRevisiPeer::DASAR3, $this->dasar3);
		if ($this->isColumnModified(BeritaAcaraRevisiPeer::DASAR4)) $criteria->add(BeritaAcaraRevisiPeer::DASAR4, $this->dasar4);
		if ($this->isColumnModified(BeritaAcaraRevisiPeer::DASAR5)) $criteria->add(BeritaAcaraRevisiPeer::DASAR5, $this->dasar5);
		if ($this->isColumnModified(BeritaAcaraRevisiPeer::DASAR6)) $criteria->add(BeritaAcaraRevisiPeer::DASAR6, $this->dasar6);
		if ($this->isColumnModified(BeritaAcaraRevisiPeer::DASAR7)) $criteria->add(BeritaAcaraRevisiPeer::DASAR7, $this->dasar7);
		if ($this->isColumnModified(BeritaAcaraRevisiPeer::DASAR8)) $criteria->add(BeritaAcaraRevisiPeer::DASAR8, $this->dasar8);
		if ($this->isColumnModified(BeritaAcaraRevisiPeer::DASAR9)) $criteria->add(BeritaAcaraRevisiPeer::DASAR9, $this->dasar9);
		if ($this->isColumnModified(BeritaAcaraRevisiPeer::DASAR10)) $criteria->add(BeritaAcaraRevisiPeer::DASAR10, $this->dasar10);
		if ($this->isColumnModified(BeritaAcaraRevisiPeer::PERTIMBANGAN1)) $criteria->add(BeritaAcaraRevisiPeer::PERTIMBANGAN1, $this->pertimbangan1);
		if ($this->isColumnModified(BeritaAcaraRevisiPeer::PERTIMBANGAN2)) $criteria->add(BeritaAcaraRevisiPeer::PERTIMBANGAN2, $this->pertimbangan2);
		if ($this->isColumnModified(BeritaAcaraRevisiPeer::PERTIMBANGAN3)) $criteria->add(BeritaAcaraRevisiPeer::PERTIMBANGAN3, $this->pertimbangan3);
		if ($this->isColumnModified(BeritaAcaraRevisiPeer::PERTIMBANGAN4)) $criteria->add(BeritaAcaraRevisiPeer::PERTIMBANGAN4, $this->pertimbangan4);
		if ($this->isColumnModified(BeritaAcaraRevisiPeer::PERTIMBANGAN5)) $criteria->add(BeritaAcaraRevisiPeer::PERTIMBANGAN5, $this->pertimbangan5);
		if ($this->isColumnModified(BeritaAcaraRevisiPeer::PERTIMBANGAN6)) $criteria->add(BeritaAcaraRevisiPeer::PERTIMBANGAN6, $this->pertimbangan6);
		if ($this->isColumnModified(BeritaAcaraRevisiPeer::PERTIMBANGAN7)) $criteria->add(BeritaAcaraRevisiPeer::PERTIMBANGAN7, $this->pertimbangan7);
		if ($this->isColumnModified(BeritaAcaraRevisiPeer::PERTIMBANGAN8)) $criteria->add(BeritaAcaraRevisiPeer::PERTIMBANGAN8, $this->pertimbangan8);
		if ($this->isColumnModified(BeritaAcaraRevisiPeer::PERTIMBANGAN9)) $criteria->add(BeritaAcaraRevisiPeer::PERTIMBANGAN9, $this->pertimbangan9);
		if ($this->isColumnModified(BeritaAcaraRevisiPeer::PERTIMBANGAN10)) $criteria->add(BeritaAcaraRevisiPeer::PERTIMBANGAN10, $this->pertimbangan10);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(BeritaAcaraRevisiPeer::DATABASE_NAME);


		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return null;
	}

	
	 public function setPrimaryKey($pk)
	 {
		 	 }

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setUnitId($this->unit_id);

		$copyObj->setKegiatanCode($this->kegiatan_code);

		$copyObj->setNomor($this->nomor);

		$copyObj->setKesimpulan($this->kesimpulan);

		$copyObj->setDasar1($this->dasar1);

		$copyObj->setDasar2($this->dasar2);

		$copyObj->setDasar3($this->dasar3);

		$copyObj->setDasar4($this->dasar4);

		$copyObj->setDasar5($this->dasar5);

		$copyObj->setDasar6($this->dasar6);

		$copyObj->setDasar7($this->dasar7);

		$copyObj->setDasar8($this->dasar8);

		$copyObj->setDasar9($this->dasar9);

		$copyObj->setDasar10($this->dasar10);

		$copyObj->setPertimbangan1($this->pertimbangan1);

		$copyObj->setPertimbangan2($this->pertimbangan2);

		$copyObj->setPertimbangan3($this->pertimbangan3);

		$copyObj->setPertimbangan4($this->pertimbangan4);

		$copyObj->setPertimbangan5($this->pertimbangan5);

		$copyObj->setPertimbangan6($this->pertimbangan6);

		$copyObj->setPertimbangan7($this->pertimbangan7);

		$copyObj->setPertimbangan8($this->pertimbangan8);

		$copyObj->setPertimbangan9($this->pertimbangan9);

		$copyObj->setPertimbangan10($this->pertimbangan10);


		$copyObj->setNew(true);

	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new BeritaAcaraRevisiPeer();
		}
		return self::$peer;
	}

} 