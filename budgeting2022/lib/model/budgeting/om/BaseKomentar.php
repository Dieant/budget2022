<?php


abstract class BaseKomentar extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $no;


	
	protected $judul;


	
	protected $isi;


	
	protected $user_id;


	
	protected $created_at;


	
	protected $updated_at;


	
	protected $locked = false;


	
	protected $tipe;

	
	protected $aMasterUser;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getNo()
	{

		return $this->no;
	}

	
	public function getJudul()
	{

		return $this->judul;
	}

	
	public function getIsi()
	{

		return $this->isi;
	}

	
	public function getUserId()
	{

		return $this->user_id;
	}

	
	public function getCreatedAt($format = 'Y-m-d H:i:s')
	{

		if ($this->created_at === null || $this->created_at === '') {
			return null;
		} elseif (!is_int($this->created_at)) {
						$ts = strtotime($this->created_at);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [created_at] as date/time value: " . var_export($this->created_at, true));
			}
		} else {
			$ts = $this->created_at;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getUpdatedAt($format = 'Y-m-d H:i:s')
	{

		if ($this->updated_at === null || $this->updated_at === '') {
			return null;
		} elseif (!is_int($this->updated_at)) {
						$ts = strtotime($this->updated_at);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [updated_at] as date/time value: " . var_export($this->updated_at, true));
			}
		} else {
			$ts = $this->updated_at;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getLocked()
	{

		return $this->locked;
	}

	
	public function getTipe()
	{

		return $this->tipe;
	}

	
	public function setNo($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->no !== $v) {
			$this->no = $v;
			$this->modifiedColumns[] = KomentarPeer::NO;
		}

	} 
	
	public function setJudul($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->judul !== $v) {
			$this->judul = $v;
			$this->modifiedColumns[] = KomentarPeer::JUDUL;
		}

	} 
	
	public function setIsi($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->isi !== $v) {
			$this->isi = $v;
			$this->modifiedColumns[] = KomentarPeer::ISI;
		}

	} 
	
	public function setUserId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->user_id !== $v) {
			$this->user_id = $v;
			$this->modifiedColumns[] = KomentarPeer::USER_ID;
		}

		if ($this->aMasterUser !== null && $this->aMasterUser->getUserId() !== $v) {
			$this->aMasterUser = null;
		}

	} 
	
	public function setCreatedAt($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [created_at] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->created_at !== $ts) {
			$this->created_at = $ts;
			$this->modifiedColumns[] = KomentarPeer::CREATED_AT;
		}

	} 
	
	public function setUpdatedAt($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [updated_at] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->updated_at !== $ts) {
			$this->updated_at = $ts;
			$this->modifiedColumns[] = KomentarPeer::UPDATED_AT;
		}

	} 
	
	public function setLocked($v)
	{

		if ($this->locked !== $v || $v === false) {
			$this->locked = $v;
			$this->modifiedColumns[] = KomentarPeer::LOCKED;
		}

	} 
	
	public function setTipe($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->tipe !== $v) {
			$this->tipe = $v;
			$this->modifiedColumns[] = KomentarPeer::TIPE;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->no = $rs->getInt($startcol + 0);

			$this->judul = $rs->getString($startcol + 1);

			$this->isi = $rs->getString($startcol + 2);

			$this->user_id = $rs->getString($startcol + 3);

			$this->created_at = $rs->getTimestamp($startcol + 4, null);

			$this->updated_at = $rs->getTimestamp($startcol + 5, null);

			$this->locked = $rs->getBoolean($startcol + 6);

			$this->tipe = $rs->getInt($startcol + 7);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 8; 
		} catch (Exception $e) {
			throw new PropelException("Error populating Komentar object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(KomentarPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			KomentarPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
    if ($this->isNew() && !$this->isColumnModified(KomentarPeer::CREATED_AT))
    {
      $this->setCreatedAt(time());
    }

    if ($this->isModified() && !$this->isColumnModified(KomentarPeer::UPDATED_AT))
    {
      $this->setUpdatedAt(time());
    }

		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(KomentarPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


												
			if ($this->aMasterUser !== null) {
				if ($this->aMasterUser->isModified()) {
					$affectedRows += $this->aMasterUser->save($con);
				}
				$this->setMasterUser($this->aMasterUser);
			}


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = KomentarPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNo($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += KomentarPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


												
			if ($this->aMasterUser !== null) {
				if (!$this->aMasterUser->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aMasterUser->getValidationFailures());
				}
			}


			if (($retval = KomentarPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = KomentarPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getNo();
				break;
			case 1:
				return $this->getJudul();
				break;
			case 2:
				return $this->getIsi();
				break;
			case 3:
				return $this->getUserId();
				break;
			case 4:
				return $this->getCreatedAt();
				break;
			case 5:
				return $this->getUpdatedAt();
				break;
			case 6:
				return $this->getLocked();
				break;
			case 7:
				return $this->getTipe();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = KomentarPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getNo(),
			$keys[1] => $this->getJudul(),
			$keys[2] => $this->getIsi(),
			$keys[3] => $this->getUserId(),
			$keys[4] => $this->getCreatedAt(),
			$keys[5] => $this->getUpdatedAt(),
			$keys[6] => $this->getLocked(),
			$keys[7] => $this->getTipe(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = KomentarPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setNo($value);
				break;
			case 1:
				$this->setJudul($value);
				break;
			case 2:
				$this->setIsi($value);
				break;
			case 3:
				$this->setUserId($value);
				break;
			case 4:
				$this->setCreatedAt($value);
				break;
			case 5:
				$this->setUpdatedAt($value);
				break;
			case 6:
				$this->setLocked($value);
				break;
			case 7:
				$this->setTipe($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = KomentarPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setNo($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setJudul($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setIsi($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setUserId($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setCreatedAt($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setUpdatedAt($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setLocked($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setTipe($arr[$keys[7]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(KomentarPeer::DATABASE_NAME);

		if ($this->isColumnModified(KomentarPeer::NO)) $criteria->add(KomentarPeer::NO, $this->no);
		if ($this->isColumnModified(KomentarPeer::JUDUL)) $criteria->add(KomentarPeer::JUDUL, $this->judul);
		if ($this->isColumnModified(KomentarPeer::ISI)) $criteria->add(KomentarPeer::ISI, $this->isi);
		if ($this->isColumnModified(KomentarPeer::USER_ID)) $criteria->add(KomentarPeer::USER_ID, $this->user_id);
		if ($this->isColumnModified(KomentarPeer::CREATED_AT)) $criteria->add(KomentarPeer::CREATED_AT, $this->created_at);
		if ($this->isColumnModified(KomentarPeer::UPDATED_AT)) $criteria->add(KomentarPeer::UPDATED_AT, $this->updated_at);
		if ($this->isColumnModified(KomentarPeer::LOCKED)) $criteria->add(KomentarPeer::LOCKED, $this->locked);
		if ($this->isColumnModified(KomentarPeer::TIPE)) $criteria->add(KomentarPeer::TIPE, $this->tipe);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(KomentarPeer::DATABASE_NAME);

		$criteria->add(KomentarPeer::NO, $this->no);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getNo();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setNo($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setJudul($this->judul);

		$copyObj->setIsi($this->isi);

		$copyObj->setUserId($this->user_id);

		$copyObj->setCreatedAt($this->created_at);

		$copyObj->setUpdatedAt($this->updated_at);

		$copyObj->setLocked($this->locked);

		$copyObj->setTipe($this->tipe);


		$copyObj->setNew(true);

		$copyObj->setNo(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new KomentarPeer();
		}
		return self::$peer;
	}

	
	public function setMasterUser($v)
	{


		if ($v === null) {
			$this->setUserId(NULL);
		} else {
			$this->setUserId($v->getUserId());
		}


		$this->aMasterUser = $v;
	}


	
	public function getMasterUser($con = null)
	{
		if ($this->aMasterUser === null && (($this->user_id !== "" && $this->user_id !== null))) {
						include_once 'lib/model/budgeting/om/BaseMasterUserPeer.php';

			$this->aMasterUser = MasterUserPeer::retrieveByPK($this->user_id, $con);

			
		}
		return $this->aMasterUser;
	}

} 