<?php


abstract class BaseRekening extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $rekening_code;


	
	protected $user_id;


	
	protected $belanja_id;


	
	protected $rekening_name;


	
	protected $rekening_ppn;


	
	protected $rekening_pph;


	
	protected $ip_address;


	
	protected $waktu_access;


	
	protected $akrual_code;


	
	protected $akrual_konstruksi;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getRekeningCode()
	{

		return $this->rekening_code;
	}

	
	public function getUserId()
	{

		return $this->user_id;
	}

	
	public function getBelanjaId()
	{

		return $this->belanja_id;
	}

	
	public function getRekeningName()
	{

		return $this->rekening_name;
	}

	
	public function getRekeningPpn()
	{

		return $this->rekening_ppn;
	}

	
	public function getRekeningPph()
	{

		return $this->rekening_pph;
	}

	
	public function getIpAddress()
	{

		return $this->ip_address;
	}

	
	public function getWaktuAccess($format = 'Y-m-d H:i:s')
	{

		if ($this->waktu_access === null || $this->waktu_access === '') {
			return null;
		} elseif (!is_int($this->waktu_access)) {
						$ts = strtotime($this->waktu_access);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [waktu_access] as date/time value: " . var_export($this->waktu_access, true));
			}
		} else {
			$ts = $this->waktu_access;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getAkrualCode()
	{

		return $this->akrual_code;
	}

	
	public function getAkrualKonstruksi()
	{

		return $this->akrual_konstruksi;
	}

	
	public function setRekeningCode($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->rekening_code !== $v) {
			$this->rekening_code = $v;
			$this->modifiedColumns[] = RekeningPeer::REKENING_CODE;
		}

	} 
	
	public function setUserId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->user_id !== $v) {
			$this->user_id = $v;
			$this->modifiedColumns[] = RekeningPeer::USER_ID;
		}

	} 
	
	public function setBelanjaId($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->belanja_id !== $v) {
			$this->belanja_id = $v;
			$this->modifiedColumns[] = RekeningPeer::BELANJA_ID;
		}

	} 
	
	public function setRekeningName($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->rekening_name !== $v) {
			$this->rekening_name = $v;
			$this->modifiedColumns[] = RekeningPeer::REKENING_NAME;
		}

	} 
	
	public function setRekeningPpn($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->rekening_ppn !== $v) {
			$this->rekening_ppn = $v;
			$this->modifiedColumns[] = RekeningPeer::REKENING_PPN;
		}

	} 
	
	public function setRekeningPph($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->rekening_pph !== $v) {
			$this->rekening_pph = $v;
			$this->modifiedColumns[] = RekeningPeer::REKENING_PPH;
		}

	} 
	
	public function setIpAddress($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->ip_address !== $v) {
			$this->ip_address = $v;
			$this->modifiedColumns[] = RekeningPeer::IP_ADDRESS;
		}

	} 
	
	public function setWaktuAccess($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [waktu_access] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->waktu_access !== $ts) {
			$this->waktu_access = $ts;
			$this->modifiedColumns[] = RekeningPeer::WAKTU_ACCESS;
		}

	} 
	
	public function setAkrualCode($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->akrual_code !== $v) {
			$this->akrual_code = $v;
			$this->modifiedColumns[] = RekeningPeer::AKRUAL_CODE;
		}

	} 
	
	public function setAkrualKonstruksi($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->akrual_konstruksi !== $v) {
			$this->akrual_konstruksi = $v;
			$this->modifiedColumns[] = RekeningPeer::AKRUAL_KONSTRUKSI;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->rekening_code = $rs->getString($startcol + 0);

			$this->user_id = $rs->getString($startcol + 1);

			$this->belanja_id = $rs->getInt($startcol + 2);

			$this->rekening_name = $rs->getString($startcol + 3);

			$this->rekening_ppn = $rs->getInt($startcol + 4);

			$this->rekening_pph = $rs->getInt($startcol + 5);

			$this->ip_address = $rs->getString($startcol + 6);

			$this->waktu_access = $rs->getTimestamp($startcol + 7, null);

			$this->akrual_code = $rs->getString($startcol + 8);

			$this->akrual_konstruksi = $rs->getString($startcol + 9);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 10; 
		} catch (Exception $e) {
			throw new PropelException("Error populating Rekening object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(RekeningPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			RekeningPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(RekeningPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = RekeningPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += RekeningPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = RekeningPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = RekeningPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getRekeningCode();
				break;
			case 1:
				return $this->getUserId();
				break;
			case 2:
				return $this->getBelanjaId();
				break;
			case 3:
				return $this->getRekeningName();
				break;
			case 4:
				return $this->getRekeningPpn();
				break;
			case 5:
				return $this->getRekeningPph();
				break;
			case 6:
				return $this->getIpAddress();
				break;
			case 7:
				return $this->getWaktuAccess();
				break;
			case 8:
				return $this->getAkrualCode();
				break;
			case 9:
				return $this->getAkrualKonstruksi();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = RekeningPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getRekeningCode(),
			$keys[1] => $this->getUserId(),
			$keys[2] => $this->getBelanjaId(),
			$keys[3] => $this->getRekeningName(),
			$keys[4] => $this->getRekeningPpn(),
			$keys[5] => $this->getRekeningPph(),
			$keys[6] => $this->getIpAddress(),
			$keys[7] => $this->getWaktuAccess(),
			$keys[8] => $this->getAkrualCode(),
			$keys[9] => $this->getAkrualKonstruksi(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = RekeningPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setRekeningCode($value);
				break;
			case 1:
				$this->setUserId($value);
				break;
			case 2:
				$this->setBelanjaId($value);
				break;
			case 3:
				$this->setRekeningName($value);
				break;
			case 4:
				$this->setRekeningPpn($value);
				break;
			case 5:
				$this->setRekeningPph($value);
				break;
			case 6:
				$this->setIpAddress($value);
				break;
			case 7:
				$this->setWaktuAccess($value);
				break;
			case 8:
				$this->setAkrualCode($value);
				break;
			case 9:
				$this->setAkrualKonstruksi($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = RekeningPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setRekeningCode($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setUserId($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setBelanjaId($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setRekeningName($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setRekeningPpn($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setRekeningPph($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setIpAddress($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setWaktuAccess($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setAkrualCode($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setAkrualKonstruksi($arr[$keys[9]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(RekeningPeer::DATABASE_NAME);

		if ($this->isColumnModified(RekeningPeer::REKENING_CODE)) $criteria->add(RekeningPeer::REKENING_CODE, $this->rekening_code);
		if ($this->isColumnModified(RekeningPeer::USER_ID)) $criteria->add(RekeningPeer::USER_ID, $this->user_id);
		if ($this->isColumnModified(RekeningPeer::BELANJA_ID)) $criteria->add(RekeningPeer::BELANJA_ID, $this->belanja_id);
		if ($this->isColumnModified(RekeningPeer::REKENING_NAME)) $criteria->add(RekeningPeer::REKENING_NAME, $this->rekening_name);
		if ($this->isColumnModified(RekeningPeer::REKENING_PPN)) $criteria->add(RekeningPeer::REKENING_PPN, $this->rekening_ppn);
		if ($this->isColumnModified(RekeningPeer::REKENING_PPH)) $criteria->add(RekeningPeer::REKENING_PPH, $this->rekening_pph);
		if ($this->isColumnModified(RekeningPeer::IP_ADDRESS)) $criteria->add(RekeningPeer::IP_ADDRESS, $this->ip_address);
		if ($this->isColumnModified(RekeningPeer::WAKTU_ACCESS)) $criteria->add(RekeningPeer::WAKTU_ACCESS, $this->waktu_access);
		if ($this->isColumnModified(RekeningPeer::AKRUAL_CODE)) $criteria->add(RekeningPeer::AKRUAL_CODE, $this->akrual_code);
		if ($this->isColumnModified(RekeningPeer::AKRUAL_KONSTRUKSI)) $criteria->add(RekeningPeer::AKRUAL_KONSTRUKSI, $this->akrual_konstruksi);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(RekeningPeer::DATABASE_NAME);

		$criteria->add(RekeningPeer::REKENING_CODE, $this->rekening_code);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getRekeningCode();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setRekeningCode($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setUserId($this->user_id);

		$copyObj->setBelanjaId($this->belanja_id);

		$copyObj->setRekeningName($this->rekening_name);

		$copyObj->setRekeningPpn($this->rekening_ppn);

		$copyObj->setRekeningPph($this->rekening_pph);

		$copyObj->setIpAddress($this->ip_address);

		$copyObj->setWaktuAccess($this->waktu_access);

		$copyObj->setAkrualCode($this->akrual_code);

		$copyObj->setAkrualKonstruksi($this->akrual_konstruksi);


		$copyObj->setNew(true);

		$copyObj->setRekeningCode(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new RekeningPeer();
		}
		return self::$peer;
	}

} 