<?php


abstract class BaseUsulan3Peer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'usulan3';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.Usulan3';

	
	const NUM_COLUMNS = 12;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const DARI = 'usulan3.DARI';

	
	const PEKERJAAN = 'usulan3.PEKERJAAN';

	
	const KECAMATAN = 'usulan3.KECAMATAN';

	
	const KELURAHAN = 'usulan3.KELURAHAN';

	
	const NAMA = 'usulan3.NAMA';

	
	const TIPE = 'usulan3.TIPE';

	
	const LOKASI = 'usulan3.LOKASI';

	
	const VOLUME = 'usulan3.VOLUME';

	
	const KETERANGAN = 'usulan3.KETERANGAN';

	
	const DANA = 'usulan3.DANA';

	
	const SKPD = 'usulan3.SKPD';

	
	const TAHAP = 'usulan3.TAHAP';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('Dari', 'Pekerjaan', 'Kecamatan', 'Kelurahan', 'Nama', 'Tipe', 'Lokasi', 'Volume', 'Keterangan', 'Dana', 'Skpd', 'Tahap', ),
		BasePeer::TYPE_COLNAME => array (Usulan3Peer::DARI, Usulan3Peer::PEKERJAAN, Usulan3Peer::KECAMATAN, Usulan3Peer::KELURAHAN, Usulan3Peer::NAMA, Usulan3Peer::TIPE, Usulan3Peer::LOKASI, Usulan3Peer::VOLUME, Usulan3Peer::KETERANGAN, Usulan3Peer::DANA, Usulan3Peer::SKPD, Usulan3Peer::TAHAP, ),
		BasePeer::TYPE_FIELDNAME => array ('dari', 'pekerjaan', 'kecamatan', 'kelurahan', 'nama', 'tipe', 'lokasi', 'volume', 'keterangan', 'dana', 'skpd', 'tahap', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('Dari' => 0, 'Pekerjaan' => 1, 'Kecamatan' => 2, 'Kelurahan' => 3, 'Nama' => 4, 'Tipe' => 5, 'Lokasi' => 6, 'Volume' => 7, 'Keterangan' => 8, 'Dana' => 9, 'Skpd' => 10, 'Tahap' => 11, ),
		BasePeer::TYPE_COLNAME => array (Usulan3Peer::DARI => 0, Usulan3Peer::PEKERJAAN => 1, Usulan3Peer::KECAMATAN => 2, Usulan3Peer::KELURAHAN => 3, Usulan3Peer::NAMA => 4, Usulan3Peer::TIPE => 5, Usulan3Peer::LOKASI => 6, Usulan3Peer::VOLUME => 7, Usulan3Peer::KETERANGAN => 8, Usulan3Peer::DANA => 9, Usulan3Peer::SKPD => 10, Usulan3Peer::TAHAP => 11, ),
		BasePeer::TYPE_FIELDNAME => array ('dari' => 0, 'pekerjaan' => 1, 'kecamatan' => 2, 'kelurahan' => 3, 'nama' => 4, 'tipe' => 5, 'lokasi' => 6, 'volume' => 7, 'keterangan' => 8, 'dana' => 9, 'skpd' => 10, 'tahap' => 11, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/Usulan3MapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.Usulan3MapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = Usulan3Peer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(Usulan3Peer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(Usulan3Peer::DARI);

		$criteria->addSelectColumn(Usulan3Peer::PEKERJAAN);

		$criteria->addSelectColumn(Usulan3Peer::KECAMATAN);

		$criteria->addSelectColumn(Usulan3Peer::KELURAHAN);

		$criteria->addSelectColumn(Usulan3Peer::NAMA);

		$criteria->addSelectColumn(Usulan3Peer::TIPE);

		$criteria->addSelectColumn(Usulan3Peer::LOKASI);

		$criteria->addSelectColumn(Usulan3Peer::VOLUME);

		$criteria->addSelectColumn(Usulan3Peer::KETERANGAN);

		$criteria->addSelectColumn(Usulan3Peer::DANA);

		$criteria->addSelectColumn(Usulan3Peer::SKPD);

		$criteria->addSelectColumn(Usulan3Peer::TAHAP);

	}

	const COUNT = 'COUNT(*)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT *)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(Usulan3Peer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(Usulan3Peer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = Usulan3Peer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = Usulan3Peer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return Usulan3Peer::populateObjects(Usulan3Peer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			Usulan3Peer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = Usulan3Peer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return Usulan3Peer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(Usulan3Peer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(Usulan3Peer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof Usulan3) {

			$criteria = $values->buildCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
												if(count($values) == count($values, COUNT_RECURSIVE))
			{
								$values = array($values);
			}
			$vals = array();
			foreach($values as $value)
			{

			}

		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(Usulan3 $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(Usulan3Peer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(Usulan3Peer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(Usulan3Peer::DATABASE_NAME, Usulan3Peer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = Usulan3Peer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

} 
if (Propel::isInit()) {
			try {
		BaseUsulan3Peer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/Usulan3MapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.Usulan3MapBuilder');
}
