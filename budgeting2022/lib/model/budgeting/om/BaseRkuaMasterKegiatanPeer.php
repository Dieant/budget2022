<?php


abstract class BaseRkuaMasterKegiatanPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.rkua_master_kegiatan';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.RkuaMasterKegiatan';

	
	const NUM_COLUMNS = 63;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const UNIT_ID = 'ebudget.rkua_master_kegiatan.UNIT_ID';

	
	const KODE_KEGIATAN = 'ebudget.rkua_master_kegiatan.KODE_KEGIATAN';

	
	const KODE_BIDANG = 'ebudget.rkua_master_kegiatan.KODE_BIDANG';

	
	const KODE_URUSAN_WAJIB = 'ebudget.rkua_master_kegiatan.KODE_URUSAN_WAJIB';

	
	const KODE_PROGRAM = 'ebudget.rkua_master_kegiatan.KODE_PROGRAM';

	
	const KODE_SASARAN = 'ebudget.rkua_master_kegiatan.KODE_SASARAN';

	
	const KODE_INDIKATOR = 'ebudget.rkua_master_kegiatan.KODE_INDIKATOR';

	
	const ALOKASI_DANA = 'ebudget.rkua_master_kegiatan.ALOKASI_DANA';

	
	const NAMA_KEGIATAN = 'ebudget.rkua_master_kegiatan.NAMA_KEGIATAN';

	
	const MASUKAN = 'ebudget.rkua_master_kegiatan.MASUKAN';

	
	const OUTPUT = 'ebudget.rkua_master_kegiatan.OUTPUT';

	
	const OUTCOME = 'ebudget.rkua_master_kegiatan.OUTCOME';

	
	const BENEFIT = 'ebudget.rkua_master_kegiatan.BENEFIT';

	
	const IMPACT = 'ebudget.rkua_master_kegiatan.IMPACT';

	
	const TIPE = 'ebudget.rkua_master_kegiatan.TIPE';

	
	const KEGIATAN_ACTIVE = 'ebudget.rkua_master_kegiatan.KEGIATAN_ACTIVE';

	
	const TO_KEGIATAN_CODE = 'ebudget.rkua_master_kegiatan.TO_KEGIATAN_CODE';

	
	const CATATAN = 'ebudget.rkua_master_kegiatan.CATATAN';

	
	const TARGET_OUTCOME = 'ebudget.rkua_master_kegiatan.TARGET_OUTCOME';

	
	const LOKASI = 'ebudget.rkua_master_kegiatan.LOKASI';

	
	const JUMLAH_PREV = 'ebudget.rkua_master_kegiatan.JUMLAH_PREV';

	
	const JUMLAH_NOW = 'ebudget.rkua_master_kegiatan.JUMLAH_NOW';

	
	const JUMLAH_NEXT = 'ebudget.rkua_master_kegiatan.JUMLAH_NEXT';

	
	const KODE_PROGRAM2 = 'ebudget.rkua_master_kegiatan.KODE_PROGRAM2';

	
	const KODE_URUSAN = 'ebudget.rkua_master_kegiatan.KODE_URUSAN';

	
	const LAST_UPDATE_USER = 'ebudget.rkua_master_kegiatan.LAST_UPDATE_USER';

	
	const LAST_UPDATE_TIME = 'ebudget.rkua_master_kegiatan.LAST_UPDATE_TIME';

	
	const LAST_UPDATE_IP = 'ebudget.rkua_master_kegiatan.LAST_UPDATE_IP';

	
	const TAHAP = 'ebudget.rkua_master_kegiatan.TAHAP';

	
	const KODE_MISI = 'ebudget.rkua_master_kegiatan.KODE_MISI';

	
	const KODE_TUJUAN = 'ebudget.rkua_master_kegiatan.KODE_TUJUAN';

	
	const RANKING = 'ebudget.rkua_master_kegiatan.RANKING';

	
	const NOMOR13 = 'ebudget.rkua_master_kegiatan.NOMOR13';

	
	const PPA_NAMA = 'ebudget.rkua_master_kegiatan.PPA_NAMA';

	
	const PPA_PANGKAT = 'ebudget.rkua_master_kegiatan.PPA_PANGKAT';

	
	const PPA_NIP = 'ebudget.rkua_master_kegiatan.PPA_NIP';

	
	const LANJUTAN = 'ebudget.rkua_master_kegiatan.LANJUTAN';

	
	const USER_ID = 'ebudget.rkua_master_kegiatan.USER_ID';

	
	const ID = 'ebudget.rkua_master_kegiatan.ID';

	
	const TAHUN = 'ebudget.rkua_master_kegiatan.TAHUN';

	
	const TAMBAHAN_PAGU = 'ebudget.rkua_master_kegiatan.TAMBAHAN_PAGU';

	
	const GENDER = 'ebudget.rkua_master_kegiatan.GENDER';

	
	const KODE_KEG_KEUANGAN = 'ebudget.rkua_master_kegiatan.KODE_KEG_KEUANGAN';

	
	const USER_ID_LAMA = 'ebudget.rkua_master_kegiatan.USER_ID_LAMA';

	
	const INDIKATOR = 'ebudget.rkua_master_kegiatan.INDIKATOR';

	
	const IS_DAK = 'ebudget.rkua_master_kegiatan.IS_DAK';

	
	const KODE_KEGIATAN_ASAL = 'ebudget.rkua_master_kegiatan.KODE_KEGIATAN_ASAL';

	
	const KODE_KEG_KEUANGAN_ASAL = 'ebudget.rkua_master_kegiatan.KODE_KEG_KEUANGAN_ASAL';

	
	const TH_KE_MULTIYEARS = 'ebudget.rkua_master_kegiatan.TH_KE_MULTIYEARS';

	
	const KELOMPOK_SASARAN = 'ebudget.rkua_master_kegiatan.KELOMPOK_SASARAN';

	
	const PAGU_BAPPEKO = 'ebudget.rkua_master_kegiatan.PAGU_BAPPEKO';

	
	const KODE_DPA = 'ebudget.rkua_master_kegiatan.KODE_DPA';

	
	const USER_ID_PPTK = 'ebudget.rkua_master_kegiatan.USER_ID_PPTK';

	
	const USER_ID_KPA = 'ebudget.rkua_master_kegiatan.USER_ID_KPA';

	
	const CATATAN_PEMBAHASAN = 'ebudget.rkua_master_kegiatan.CATATAN_PEMBAHASAN';

	
	const CATATAN_PENYELIA = 'ebudget.rkua_master_kegiatan.CATATAN_PENYELIA';

	
	const CATATAN_BAPPEKO = 'ebudget.rkua_master_kegiatan.CATATAN_BAPPEKO';

	
	const STATUS_LEVEL = 'ebudget.rkua_master_kegiatan.STATUS_LEVEL';

	
	const IS_TAPD_SETUJU = 'ebudget.rkua_master_kegiatan.IS_TAPD_SETUJU';

	
	const IS_BAPPEKO_SETUJU = 'ebudget.rkua_master_kegiatan.IS_BAPPEKO_SETUJU';

	
	const IS_PENYELIA_SETUJU = 'ebudget.rkua_master_kegiatan.IS_PENYELIA_SETUJU';

	
	const IS_PERNAH_RKA = 'ebudget.rkua_master_kegiatan.IS_PERNAH_RKA';

	
	const KODE_KEGIATAN_BARU = 'ebudget.rkua_master_kegiatan.KODE_KEGIATAN_BARU';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('UnitId', 'KodeKegiatan', 'KodeBidang', 'KodeUrusanWajib', 'KodeProgram', 'KodeSasaran', 'KodeIndikator', 'AlokasiDana', 'NamaKegiatan', 'Masukan', 'Output', 'Outcome', 'Benefit', 'Impact', 'Tipe', 'KegiatanActive', 'ToKegiatanCode', 'Catatan', 'TargetOutcome', 'Lokasi', 'JumlahPrev', 'JumlahNow', 'JumlahNext', 'KodeProgram2', 'KodeUrusan', 'LastUpdateUser', 'LastUpdateTime', 'LastUpdateIp', 'Tahap', 'KodeMisi', 'KodeTujuan', 'Ranking', 'Nomor13', 'PpaNama', 'PpaPangkat', 'PpaNip', 'Lanjutan', 'UserId', 'Id', 'Tahun', 'TambahanPagu', 'Gender', 'KodeKegKeuangan', 'UserIdLama', 'Indikator', 'IsDak', 'KodeKegiatanAsal', 'KodeKegKeuanganAsal', 'ThKeMultiyears', 'KelompokSasaran', 'PaguBappeko', 'KodeDpa', 'UserIdPptk', 'UserIdKpa', 'CatatanPembahasan', 'CatatanPenyelia', 'CatatanBappeko', 'StatusLevel', 'IsTapdSetuju', 'IsBappekoSetuju', 'IsPenyeliaSetuju', 'IsPernahRka', 'KodeKegiatanBaru', ),
		BasePeer::TYPE_COLNAME => array (RkuaMasterKegiatanPeer::UNIT_ID, RkuaMasterKegiatanPeer::KODE_KEGIATAN, RkuaMasterKegiatanPeer::KODE_BIDANG, RkuaMasterKegiatanPeer::KODE_URUSAN_WAJIB, RkuaMasterKegiatanPeer::KODE_PROGRAM, RkuaMasterKegiatanPeer::KODE_SASARAN, RkuaMasterKegiatanPeer::KODE_INDIKATOR, RkuaMasterKegiatanPeer::ALOKASI_DANA, RkuaMasterKegiatanPeer::NAMA_KEGIATAN, RkuaMasterKegiatanPeer::MASUKAN, RkuaMasterKegiatanPeer::OUTPUT, RkuaMasterKegiatanPeer::OUTCOME, RkuaMasterKegiatanPeer::BENEFIT, RkuaMasterKegiatanPeer::IMPACT, RkuaMasterKegiatanPeer::TIPE, RkuaMasterKegiatanPeer::KEGIATAN_ACTIVE, RkuaMasterKegiatanPeer::TO_KEGIATAN_CODE, RkuaMasterKegiatanPeer::CATATAN, RkuaMasterKegiatanPeer::TARGET_OUTCOME, RkuaMasterKegiatanPeer::LOKASI, RkuaMasterKegiatanPeer::JUMLAH_PREV, RkuaMasterKegiatanPeer::JUMLAH_NOW, RkuaMasterKegiatanPeer::JUMLAH_NEXT, RkuaMasterKegiatanPeer::KODE_PROGRAM2, RkuaMasterKegiatanPeer::KODE_URUSAN, RkuaMasterKegiatanPeer::LAST_UPDATE_USER, RkuaMasterKegiatanPeer::LAST_UPDATE_TIME, RkuaMasterKegiatanPeer::LAST_UPDATE_IP, RkuaMasterKegiatanPeer::TAHAP, RkuaMasterKegiatanPeer::KODE_MISI, RkuaMasterKegiatanPeer::KODE_TUJUAN, RkuaMasterKegiatanPeer::RANKING, RkuaMasterKegiatanPeer::NOMOR13, RkuaMasterKegiatanPeer::PPA_NAMA, RkuaMasterKegiatanPeer::PPA_PANGKAT, RkuaMasterKegiatanPeer::PPA_NIP, RkuaMasterKegiatanPeer::LANJUTAN, RkuaMasterKegiatanPeer::USER_ID, RkuaMasterKegiatanPeer::ID, RkuaMasterKegiatanPeer::TAHUN, RkuaMasterKegiatanPeer::TAMBAHAN_PAGU, RkuaMasterKegiatanPeer::GENDER, RkuaMasterKegiatanPeer::KODE_KEG_KEUANGAN, RkuaMasterKegiatanPeer::USER_ID_LAMA, RkuaMasterKegiatanPeer::INDIKATOR, RkuaMasterKegiatanPeer::IS_DAK, RkuaMasterKegiatanPeer::KODE_KEGIATAN_ASAL, RkuaMasterKegiatanPeer::KODE_KEG_KEUANGAN_ASAL, RkuaMasterKegiatanPeer::TH_KE_MULTIYEARS, RkuaMasterKegiatanPeer::KELOMPOK_SASARAN, RkuaMasterKegiatanPeer::PAGU_BAPPEKO, RkuaMasterKegiatanPeer::KODE_DPA, RkuaMasterKegiatanPeer::USER_ID_PPTK, RkuaMasterKegiatanPeer::USER_ID_KPA, RkuaMasterKegiatanPeer::CATATAN_PEMBAHASAN, RkuaMasterKegiatanPeer::CATATAN_PENYELIA, RkuaMasterKegiatanPeer::CATATAN_BAPPEKO, RkuaMasterKegiatanPeer::STATUS_LEVEL, RkuaMasterKegiatanPeer::IS_TAPD_SETUJU, RkuaMasterKegiatanPeer::IS_BAPPEKO_SETUJU, RkuaMasterKegiatanPeer::IS_PENYELIA_SETUJU, RkuaMasterKegiatanPeer::IS_PERNAH_RKA, RkuaMasterKegiatanPeer::KODE_KEGIATAN_BARU, ),
		BasePeer::TYPE_FIELDNAME => array ('unit_id', 'kode_kegiatan', 'kode_bidang', 'kode_urusan_wajib', 'kode_program', 'kode_sasaran', 'kode_indikator', 'alokasi_dana', 'nama_kegiatan', 'masukan', 'output', 'outcome', 'benefit', 'impact', 'tipe', 'kegiatan_active', 'to_kegiatan_code', 'catatan', 'target_outcome', 'lokasi', 'jumlah_prev', 'jumlah_now', 'jumlah_next', 'kode_program2', 'kode_urusan', 'last_update_user', 'last_update_time', 'last_update_ip', 'tahap', 'kode_misi', 'kode_tujuan', 'ranking', 'nomor13', 'ppa_nama', 'ppa_pangkat', 'ppa_nip', 'lanjutan', 'user_id', 'id', 'tahun', 'tambahan_pagu', 'gender', 'kode_keg_keuangan', 'user_id_lama', 'indikator', 'is_dak', 'kode_kegiatan_asal', 'kode_keg_keuangan_asal', 'th_ke_multiyears', 'kelompok_sasaran', 'pagu_bappeko', 'kode_dpa', 'user_id_pptk', 'user_id_kpa', 'catatan_pembahasan', 'catatan_penyelia', 'catatan_bappeko', 'status_level', 'is_tapd_setuju', 'is_bappeko_setuju', 'is_penyelia_setuju', 'is_pernah_rka', 'kode_kegiatan_baru', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('UnitId' => 0, 'KodeKegiatan' => 1, 'KodeBidang' => 2, 'KodeUrusanWajib' => 3, 'KodeProgram' => 4, 'KodeSasaran' => 5, 'KodeIndikator' => 6, 'AlokasiDana' => 7, 'NamaKegiatan' => 8, 'Masukan' => 9, 'Output' => 10, 'Outcome' => 11, 'Benefit' => 12, 'Impact' => 13, 'Tipe' => 14, 'KegiatanActive' => 15, 'ToKegiatanCode' => 16, 'Catatan' => 17, 'TargetOutcome' => 18, 'Lokasi' => 19, 'JumlahPrev' => 20, 'JumlahNow' => 21, 'JumlahNext' => 22, 'KodeProgram2' => 23, 'KodeUrusan' => 24, 'LastUpdateUser' => 25, 'LastUpdateTime' => 26, 'LastUpdateIp' => 27, 'Tahap' => 28, 'KodeMisi' => 29, 'KodeTujuan' => 30, 'Ranking' => 31, 'Nomor13' => 32, 'PpaNama' => 33, 'PpaPangkat' => 34, 'PpaNip' => 35, 'Lanjutan' => 36, 'UserId' => 37, 'Id' => 38, 'Tahun' => 39, 'TambahanPagu' => 40, 'Gender' => 41, 'KodeKegKeuangan' => 42, 'UserIdLama' => 43, 'Indikator' => 44, 'IsDak' => 45, 'KodeKegiatanAsal' => 46, 'KodeKegKeuanganAsal' => 47, 'ThKeMultiyears' => 48, 'KelompokSasaran' => 49, 'PaguBappeko' => 50, 'KodeDpa' => 51, 'UserIdPptk' => 52, 'UserIdKpa' => 53, 'CatatanPembahasan' => 54, 'CatatanPenyelia' => 55, 'CatatanBappeko' => 56, 'StatusLevel' => 57, 'IsTapdSetuju' => 58, 'IsBappekoSetuju' => 59, 'IsPenyeliaSetuju' => 60, 'IsPernahRka' => 61, 'KodeKegiatanBaru' => 62, ),
		BasePeer::TYPE_COLNAME => array (RkuaMasterKegiatanPeer::UNIT_ID => 0, RkuaMasterKegiatanPeer::KODE_KEGIATAN => 1, RkuaMasterKegiatanPeer::KODE_BIDANG => 2, RkuaMasterKegiatanPeer::KODE_URUSAN_WAJIB => 3, RkuaMasterKegiatanPeer::KODE_PROGRAM => 4, RkuaMasterKegiatanPeer::KODE_SASARAN => 5, RkuaMasterKegiatanPeer::KODE_INDIKATOR => 6, RkuaMasterKegiatanPeer::ALOKASI_DANA => 7, RkuaMasterKegiatanPeer::NAMA_KEGIATAN => 8, RkuaMasterKegiatanPeer::MASUKAN => 9, RkuaMasterKegiatanPeer::OUTPUT => 10, RkuaMasterKegiatanPeer::OUTCOME => 11, RkuaMasterKegiatanPeer::BENEFIT => 12, RkuaMasterKegiatanPeer::IMPACT => 13, RkuaMasterKegiatanPeer::TIPE => 14, RkuaMasterKegiatanPeer::KEGIATAN_ACTIVE => 15, RkuaMasterKegiatanPeer::TO_KEGIATAN_CODE => 16, RkuaMasterKegiatanPeer::CATATAN => 17, RkuaMasterKegiatanPeer::TARGET_OUTCOME => 18, RkuaMasterKegiatanPeer::LOKASI => 19, RkuaMasterKegiatanPeer::JUMLAH_PREV => 20, RkuaMasterKegiatanPeer::JUMLAH_NOW => 21, RkuaMasterKegiatanPeer::JUMLAH_NEXT => 22, RkuaMasterKegiatanPeer::KODE_PROGRAM2 => 23, RkuaMasterKegiatanPeer::KODE_URUSAN => 24, RkuaMasterKegiatanPeer::LAST_UPDATE_USER => 25, RkuaMasterKegiatanPeer::LAST_UPDATE_TIME => 26, RkuaMasterKegiatanPeer::LAST_UPDATE_IP => 27, RkuaMasterKegiatanPeer::TAHAP => 28, RkuaMasterKegiatanPeer::KODE_MISI => 29, RkuaMasterKegiatanPeer::KODE_TUJUAN => 30, RkuaMasterKegiatanPeer::RANKING => 31, RkuaMasterKegiatanPeer::NOMOR13 => 32, RkuaMasterKegiatanPeer::PPA_NAMA => 33, RkuaMasterKegiatanPeer::PPA_PANGKAT => 34, RkuaMasterKegiatanPeer::PPA_NIP => 35, RkuaMasterKegiatanPeer::LANJUTAN => 36, RkuaMasterKegiatanPeer::USER_ID => 37, RkuaMasterKegiatanPeer::ID => 38, RkuaMasterKegiatanPeer::TAHUN => 39, RkuaMasterKegiatanPeer::TAMBAHAN_PAGU => 40, RkuaMasterKegiatanPeer::GENDER => 41, RkuaMasterKegiatanPeer::KODE_KEG_KEUANGAN => 42, RkuaMasterKegiatanPeer::USER_ID_LAMA => 43, RkuaMasterKegiatanPeer::INDIKATOR => 44, RkuaMasterKegiatanPeer::IS_DAK => 45, RkuaMasterKegiatanPeer::KODE_KEGIATAN_ASAL => 46, RkuaMasterKegiatanPeer::KODE_KEG_KEUANGAN_ASAL => 47, RkuaMasterKegiatanPeer::TH_KE_MULTIYEARS => 48, RkuaMasterKegiatanPeer::KELOMPOK_SASARAN => 49, RkuaMasterKegiatanPeer::PAGU_BAPPEKO => 50, RkuaMasterKegiatanPeer::KODE_DPA => 51, RkuaMasterKegiatanPeer::USER_ID_PPTK => 52, RkuaMasterKegiatanPeer::USER_ID_KPA => 53, RkuaMasterKegiatanPeer::CATATAN_PEMBAHASAN => 54, RkuaMasterKegiatanPeer::CATATAN_PENYELIA => 55, RkuaMasterKegiatanPeer::CATATAN_BAPPEKO => 56, RkuaMasterKegiatanPeer::STATUS_LEVEL => 57, RkuaMasterKegiatanPeer::IS_TAPD_SETUJU => 58, RkuaMasterKegiatanPeer::IS_BAPPEKO_SETUJU => 59, RkuaMasterKegiatanPeer::IS_PENYELIA_SETUJU => 60, RkuaMasterKegiatanPeer::IS_PERNAH_RKA => 61, RkuaMasterKegiatanPeer::KODE_KEGIATAN_BARU => 62, ),
		BasePeer::TYPE_FIELDNAME => array ('unit_id' => 0, 'kode_kegiatan' => 1, 'kode_bidang' => 2, 'kode_urusan_wajib' => 3, 'kode_program' => 4, 'kode_sasaran' => 5, 'kode_indikator' => 6, 'alokasi_dana' => 7, 'nama_kegiatan' => 8, 'masukan' => 9, 'output' => 10, 'outcome' => 11, 'benefit' => 12, 'impact' => 13, 'tipe' => 14, 'kegiatan_active' => 15, 'to_kegiatan_code' => 16, 'catatan' => 17, 'target_outcome' => 18, 'lokasi' => 19, 'jumlah_prev' => 20, 'jumlah_now' => 21, 'jumlah_next' => 22, 'kode_program2' => 23, 'kode_urusan' => 24, 'last_update_user' => 25, 'last_update_time' => 26, 'last_update_ip' => 27, 'tahap' => 28, 'kode_misi' => 29, 'kode_tujuan' => 30, 'ranking' => 31, 'nomor13' => 32, 'ppa_nama' => 33, 'ppa_pangkat' => 34, 'ppa_nip' => 35, 'lanjutan' => 36, 'user_id' => 37, 'id' => 38, 'tahun' => 39, 'tambahan_pagu' => 40, 'gender' => 41, 'kode_keg_keuangan' => 42, 'user_id_lama' => 43, 'indikator' => 44, 'is_dak' => 45, 'kode_kegiatan_asal' => 46, 'kode_keg_keuangan_asal' => 47, 'th_ke_multiyears' => 48, 'kelompok_sasaran' => 49, 'pagu_bappeko' => 50, 'kode_dpa' => 51, 'user_id_pptk' => 52, 'user_id_kpa' => 53, 'catatan_pembahasan' => 54, 'catatan_penyelia' => 55, 'catatan_bappeko' => 56, 'status_level' => 57, 'is_tapd_setuju' => 58, 'is_bappeko_setuju' => 59, 'is_penyelia_setuju' => 60, 'is_pernah_rka' => 61, 'kode_kegiatan_baru' => 62, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/RkuaMasterKegiatanMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.RkuaMasterKegiatanMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = RkuaMasterKegiatanPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(RkuaMasterKegiatanPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(RkuaMasterKegiatanPeer::UNIT_ID);

		$criteria->addSelectColumn(RkuaMasterKegiatanPeer::KODE_KEGIATAN);

		$criteria->addSelectColumn(RkuaMasterKegiatanPeer::KODE_BIDANG);

		$criteria->addSelectColumn(RkuaMasterKegiatanPeer::KODE_URUSAN_WAJIB);

		$criteria->addSelectColumn(RkuaMasterKegiatanPeer::KODE_PROGRAM);

		$criteria->addSelectColumn(RkuaMasterKegiatanPeer::KODE_SASARAN);

		$criteria->addSelectColumn(RkuaMasterKegiatanPeer::KODE_INDIKATOR);

		$criteria->addSelectColumn(RkuaMasterKegiatanPeer::ALOKASI_DANA);

		$criteria->addSelectColumn(RkuaMasterKegiatanPeer::NAMA_KEGIATAN);

		$criteria->addSelectColumn(RkuaMasterKegiatanPeer::MASUKAN);

		$criteria->addSelectColumn(RkuaMasterKegiatanPeer::OUTPUT);

		$criteria->addSelectColumn(RkuaMasterKegiatanPeer::OUTCOME);

		$criteria->addSelectColumn(RkuaMasterKegiatanPeer::BENEFIT);

		$criteria->addSelectColumn(RkuaMasterKegiatanPeer::IMPACT);

		$criteria->addSelectColumn(RkuaMasterKegiatanPeer::TIPE);

		$criteria->addSelectColumn(RkuaMasterKegiatanPeer::KEGIATAN_ACTIVE);

		$criteria->addSelectColumn(RkuaMasterKegiatanPeer::TO_KEGIATAN_CODE);

		$criteria->addSelectColumn(RkuaMasterKegiatanPeer::CATATAN);

		$criteria->addSelectColumn(RkuaMasterKegiatanPeer::TARGET_OUTCOME);

		$criteria->addSelectColumn(RkuaMasterKegiatanPeer::LOKASI);

		$criteria->addSelectColumn(RkuaMasterKegiatanPeer::JUMLAH_PREV);

		$criteria->addSelectColumn(RkuaMasterKegiatanPeer::JUMLAH_NOW);

		$criteria->addSelectColumn(RkuaMasterKegiatanPeer::JUMLAH_NEXT);

		$criteria->addSelectColumn(RkuaMasterKegiatanPeer::KODE_PROGRAM2);

		$criteria->addSelectColumn(RkuaMasterKegiatanPeer::KODE_URUSAN);

		$criteria->addSelectColumn(RkuaMasterKegiatanPeer::LAST_UPDATE_USER);

		$criteria->addSelectColumn(RkuaMasterKegiatanPeer::LAST_UPDATE_TIME);

		$criteria->addSelectColumn(RkuaMasterKegiatanPeer::LAST_UPDATE_IP);

		$criteria->addSelectColumn(RkuaMasterKegiatanPeer::TAHAP);

		$criteria->addSelectColumn(RkuaMasterKegiatanPeer::KODE_MISI);

		$criteria->addSelectColumn(RkuaMasterKegiatanPeer::KODE_TUJUAN);

		$criteria->addSelectColumn(RkuaMasterKegiatanPeer::RANKING);

		$criteria->addSelectColumn(RkuaMasterKegiatanPeer::NOMOR13);

		$criteria->addSelectColumn(RkuaMasterKegiatanPeer::PPA_NAMA);

		$criteria->addSelectColumn(RkuaMasterKegiatanPeer::PPA_PANGKAT);

		$criteria->addSelectColumn(RkuaMasterKegiatanPeer::PPA_NIP);

		$criteria->addSelectColumn(RkuaMasterKegiatanPeer::LANJUTAN);

		$criteria->addSelectColumn(RkuaMasterKegiatanPeer::USER_ID);

		$criteria->addSelectColumn(RkuaMasterKegiatanPeer::ID);

		$criteria->addSelectColumn(RkuaMasterKegiatanPeer::TAHUN);

		$criteria->addSelectColumn(RkuaMasterKegiatanPeer::TAMBAHAN_PAGU);

		$criteria->addSelectColumn(RkuaMasterKegiatanPeer::GENDER);

		$criteria->addSelectColumn(RkuaMasterKegiatanPeer::KODE_KEG_KEUANGAN);

		$criteria->addSelectColumn(RkuaMasterKegiatanPeer::USER_ID_LAMA);

		$criteria->addSelectColumn(RkuaMasterKegiatanPeer::INDIKATOR);

		$criteria->addSelectColumn(RkuaMasterKegiatanPeer::IS_DAK);

		$criteria->addSelectColumn(RkuaMasterKegiatanPeer::KODE_KEGIATAN_ASAL);

		$criteria->addSelectColumn(RkuaMasterKegiatanPeer::KODE_KEG_KEUANGAN_ASAL);

		$criteria->addSelectColumn(RkuaMasterKegiatanPeer::TH_KE_MULTIYEARS);

		$criteria->addSelectColumn(RkuaMasterKegiatanPeer::KELOMPOK_SASARAN);

		$criteria->addSelectColumn(RkuaMasterKegiatanPeer::PAGU_BAPPEKO);

		$criteria->addSelectColumn(RkuaMasterKegiatanPeer::KODE_DPA);

		$criteria->addSelectColumn(RkuaMasterKegiatanPeer::USER_ID_PPTK);

		$criteria->addSelectColumn(RkuaMasterKegiatanPeer::USER_ID_KPA);

		$criteria->addSelectColumn(RkuaMasterKegiatanPeer::CATATAN_PEMBAHASAN);

		$criteria->addSelectColumn(RkuaMasterKegiatanPeer::CATATAN_PENYELIA);

		$criteria->addSelectColumn(RkuaMasterKegiatanPeer::CATATAN_BAPPEKO);

		$criteria->addSelectColumn(RkuaMasterKegiatanPeer::STATUS_LEVEL);

		$criteria->addSelectColumn(RkuaMasterKegiatanPeer::IS_TAPD_SETUJU);

		$criteria->addSelectColumn(RkuaMasterKegiatanPeer::IS_BAPPEKO_SETUJU);

		$criteria->addSelectColumn(RkuaMasterKegiatanPeer::IS_PENYELIA_SETUJU);

		$criteria->addSelectColumn(RkuaMasterKegiatanPeer::IS_PERNAH_RKA);

		$criteria->addSelectColumn(RkuaMasterKegiatanPeer::KODE_KEGIATAN_BARU);

	}

	const COUNT = 'COUNT(ebudget.rkua_master_kegiatan.UNIT_ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.rkua_master_kegiatan.UNIT_ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(RkuaMasterKegiatanPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(RkuaMasterKegiatanPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = RkuaMasterKegiatanPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = RkuaMasterKegiatanPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return RkuaMasterKegiatanPeer::populateObjects(RkuaMasterKegiatanPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			RkuaMasterKegiatanPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = RkuaMasterKegiatanPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return RkuaMasterKegiatanPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}

		$criteria->remove(RkuaMasterKegiatanPeer::ID); 

				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(RkuaMasterKegiatanPeer::UNIT_ID);
			$selectCriteria->add(RkuaMasterKegiatanPeer::UNIT_ID, $criteria->remove(RkuaMasterKegiatanPeer::UNIT_ID), $comparison);

			$comparison = $criteria->getComparison(RkuaMasterKegiatanPeer::KODE_KEGIATAN);
			$selectCriteria->add(RkuaMasterKegiatanPeer::KODE_KEGIATAN, $criteria->remove(RkuaMasterKegiatanPeer::KODE_KEGIATAN), $comparison);

			$comparison = $criteria->getComparison(RkuaMasterKegiatanPeer::ID);
			$selectCriteria->add(RkuaMasterKegiatanPeer::ID, $criteria->remove(RkuaMasterKegiatanPeer::ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(RkuaMasterKegiatanPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(RkuaMasterKegiatanPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof RkuaMasterKegiatan) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
												if(count($values) == count($values, COUNT_RECURSIVE))
			{
								$values = array($values);
			}
			$vals = array();
			foreach($values as $value)
			{

				$vals[0][] = $value[0];
				$vals[1][] = $value[1];
				$vals[2][] = $value[2];
			}

			$criteria->add(RkuaMasterKegiatanPeer::UNIT_ID, $vals[0], Criteria::IN);
			$criteria->add(RkuaMasterKegiatanPeer::KODE_KEGIATAN, $vals[1], Criteria::IN);
			$criteria->add(RkuaMasterKegiatanPeer::ID, $vals[2], Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(RkuaMasterKegiatan $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(RkuaMasterKegiatanPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(RkuaMasterKegiatanPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(RkuaMasterKegiatanPeer::DATABASE_NAME, RkuaMasterKegiatanPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = RkuaMasterKegiatanPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK( $unit_id, $kode_kegiatan, $id, $con = null) {
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$criteria = new Criteria();
		$criteria->add(RkuaMasterKegiatanPeer::UNIT_ID, $unit_id);
		$criteria->add(RkuaMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
		$criteria->add(RkuaMasterKegiatanPeer::ID, $id);
		$v = RkuaMasterKegiatanPeer::doSelect($criteria, $con);

		return !empty($v) ? $v[0] : null;
	}
} 
if (Propel::isInit()) {
			try {
		BaseRkuaMasterKegiatanPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/RkuaMasterKegiatanMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.RkuaMasterKegiatanMapBuilder');
}
