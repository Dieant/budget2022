<?php


abstract class BaseMurniRincianDetail extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $kegiatan_code;


	
	protected $tipe;


	
	protected $detail_no;


	
	protected $rekening_code;


	
	protected $komponen_id;


	
	protected $detail_name;


	
	protected $volume;


	
	protected $keterangan_koefisien;


	
	protected $subtitle;


	
	protected $komponen_harga;


	
	protected $komponen_harga_awal;


	
	protected $komponen_name;


	
	protected $satuan;


	
	protected $pajak = 0;


	
	protected $unit_id;


	
	protected $from_sub_kegiatan;


	
	protected $sub;


	
	protected $kode_sub;


	
	protected $last_update_user;


	
	protected $last_update_time;


	
	protected $last_update_ip;


	
	protected $tahap;


	
	protected $tahap_edit;


	
	protected $tahap_new;


	
	protected $status_lelang;


	
	protected $nomor_lelang;


	
	protected $koefisien_semula;


	
	protected $volume_semula;


	
	protected $harga_semula;


	
	protected $total_semula;


	
	protected $lock_subtitle;


	
	protected $status_hapus = false;


	
	protected $tahun;


	
	protected $kode_lokasi;


	
	protected $kecamatan;


	
	protected $rekening_code_asli;


	
	protected $note_skpd;


	
	protected $note_peneliti;


	
	protected $nilai_anggaran;


	
	protected $is_blud;


	
	protected $lokasi_kecamatan;


	
	protected $lokasi_kelurahan;


	
	protected $ob;


	
	protected $ob_from_id;


	
	protected $is_per_komponen;


	
	protected $kegiatan_code_asal;


	
	protected $th_ke_multiyears;


	
	protected $harga_sebelum_sisa_lelang;


	
	protected $is_musrenbang = false;


	
	protected $sub_id_asal;


	
	protected $subtitle_asal;


	
	protected $kode_sub_asal;


	
	protected $sub_asal;


	
	protected $last_edit_time;


	
	protected $is_potong_bpjs = false;


	
	protected $is_iuran_bpjs = false;


	
	protected $status_ob = 0;


	
	protected $ob_parent;


	
	protected $ob_alokasi_baru;


	
	protected $is_hibah = false;


	
	protected $status_level = 0;


	
	protected $status_level_tolak;


	
	protected $status_sisipan = false;


	
	protected $is_tapd_setuju = false;


	
	protected $is_bappeko_setuju = false;


	
	protected $akrual_code;


	
	protected $tipe2;


	
	protected $is_penyelia_setuju = false;


	
	protected $note_tapd;


	
	protected $note_bappeko;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getKegiatanCode()
	{

		return $this->kegiatan_code;
	}

	
	public function getTipe()
	{

		return $this->tipe;
	}

	
	public function getDetailNo()
	{

		return $this->detail_no;
	}

	
	public function getRekeningCode()
	{

		return $this->rekening_code;
	}

	
	public function getKomponenId()
	{

		return $this->komponen_id;
	}

	
	public function getDetailName()
	{

		return $this->detail_name;
	}

	
	public function getVolume()
	{

		return $this->volume;
	}

	
	public function getKeteranganKoefisien()
	{

		return $this->keterangan_koefisien;
	}

	
	public function getSubtitle()
	{

		return $this->subtitle;
	}

	
	public function getKomponenHarga()
	{

		return $this->komponen_harga;
	}

	
	public function getKomponenHargaAwal()
	{

		return $this->komponen_harga_awal;
	}

	
	public function getKomponenName()
	{

		return $this->komponen_name;
	}

	
	public function getSatuan()
	{

		return $this->satuan;
	}

	
	public function getPajak()
	{

		return $this->pajak;
	}

	
	public function getUnitId()
	{

		return $this->unit_id;
	}

	
	public function getFromSubKegiatan()
	{

		return $this->from_sub_kegiatan;
	}

	
	public function getSub()
	{

		return $this->sub;
	}

	
	public function getKodeSub()
	{

		return $this->kode_sub;
	}

	
	public function getLastUpdateUser()
	{

		return $this->last_update_user;
	}

	
	public function getLastUpdateTime($format = 'Y-m-d H:i:s')
	{

		if ($this->last_update_time === null || $this->last_update_time === '') {
			return null;
		} elseif (!is_int($this->last_update_time)) {
						$ts = strtotime($this->last_update_time);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [last_update_time] as date/time value: " . var_export($this->last_update_time, true));
			}
		} else {
			$ts = $this->last_update_time;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getLastUpdateIp()
	{

		return $this->last_update_ip;
	}

	
	public function getTahap()
	{

		return $this->tahap;
	}

	
	public function getTahapEdit()
	{

		return $this->tahap_edit;
	}

	
	public function getTahapNew()
	{

		return $this->tahap_new;
	}

	
	public function getStatusLelang()
	{

		return $this->status_lelang;
	}

	
	public function getNomorLelang()
	{

		return $this->nomor_lelang;
	}

	
	public function getKoefisienSemula()
	{

		return $this->koefisien_semula;
	}

	
	public function getVolumeSemula()
	{

		return $this->volume_semula;
	}

	
	public function getHargaSemula()
	{

		return $this->harga_semula;
	}

	
	public function getTotalSemula()
	{

		return $this->total_semula;
	}

	
	public function getLockSubtitle()
	{

		return $this->lock_subtitle;
	}

	
	public function getStatusHapus()
	{

		return $this->status_hapus;
	}

	
	public function getTahun()
	{

		return $this->tahun;
	}

	
	public function getKodeLokasi()
	{

		return $this->kode_lokasi;
	}

	
	public function getKecamatan()
	{

		return $this->kecamatan;
	}

	
	public function getRekeningCodeAsli()
	{

		return $this->rekening_code_asli;
	}

	
	public function getNoteSkpd()
	{

		return $this->note_skpd;
	}

	
	public function getNotePeneliti()
	{

		return $this->note_peneliti;
	}

	
	public function getNilaiAnggaran()
	{

		return $this->nilai_anggaran;
	}

	
	public function getIsBlud()
	{

		return $this->is_blud;
	}

	
	public function getLokasiKecamatan()
	{

		return $this->lokasi_kecamatan;
	}

	
	public function getLokasiKelurahan()
	{

		return $this->lokasi_kelurahan;
	}

	
	public function getOb()
	{

		return $this->ob;
	}

	
	public function getObFromId()
	{

		return $this->ob_from_id;
	}

	
	public function getIsPerKomponen()
	{

		return $this->is_per_komponen;
	}

	
	public function getKegiatanCodeAsal()
	{

		return $this->kegiatan_code_asal;
	}

	
	public function getThKeMultiyears()
	{

		return $this->th_ke_multiyears;
	}

	
	public function getHargaSebelumSisaLelang()
	{

		return $this->harga_sebelum_sisa_lelang;
	}

	
	public function getIsMusrenbang()
	{

		return $this->is_musrenbang;
	}

	
	public function getSubIdAsal()
	{

		return $this->sub_id_asal;
	}

	
	public function getSubtitleAsal()
	{

		return $this->subtitle_asal;
	}

	
	public function getKodeSubAsal()
	{

		return $this->kode_sub_asal;
	}

	
	public function getSubAsal()
	{

		return $this->sub_asal;
	}

	
	public function getLastEditTime($format = 'Y-m-d H:i:s')
	{

		if ($this->last_edit_time === null || $this->last_edit_time === '') {
			return null;
		} elseif (!is_int($this->last_edit_time)) {
						$ts = strtotime($this->last_edit_time);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [last_edit_time] as date/time value: " . var_export($this->last_edit_time, true));
			}
		} else {
			$ts = $this->last_edit_time;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getIsPotongBpjs()
	{

		return $this->is_potong_bpjs;
	}

	
	public function getIsIuranBpjs()
	{

		return $this->is_iuran_bpjs;
	}

	
	public function getStatusOb()
	{

		return $this->status_ob;
	}

	
	public function getObParent()
	{

		return $this->ob_parent;
	}

	
	public function getObAlokasiBaru()
	{

		return $this->ob_alokasi_baru;
	}

	
	public function getIsHibah()
	{

		return $this->is_hibah;
	}

	
	public function getStatusLevel()
	{

		return $this->status_level;
	}

	
	public function getStatusLevelTolak()
	{

		return $this->status_level_tolak;
	}

	
	public function getStatusSisipan()
	{

		return $this->status_sisipan;
	}

	
	public function getIsTapdSetuju()
	{

		return $this->is_tapd_setuju;
	}

	
	public function getIsBappekoSetuju()
	{

		return $this->is_bappeko_setuju;
	}

	
	public function getAkrualCode()
	{

		return $this->akrual_code;
	}

	
	public function getTipe2()
	{

		return $this->tipe2;
	}

	
	public function getIsPenyeliaSetuju()
	{

		return $this->is_penyelia_setuju;
	}

	
	public function getNoteTapd()
	{

		return $this->note_tapd;
	}

	
	public function getNoteBappeko()
	{

		return $this->note_bappeko;
	}

	
	public function setKegiatanCode($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kegiatan_code !== $v) {
			$this->kegiatan_code = $v;
			$this->modifiedColumns[] = MurniRincianDetailPeer::KEGIATAN_CODE;
		}

	} 
	
	public function setTipe($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->tipe !== $v) {
			$this->tipe = $v;
			$this->modifiedColumns[] = MurniRincianDetailPeer::TIPE;
		}

	} 
	
	public function setDetailNo($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->detail_no !== $v) {
			$this->detail_no = $v;
			$this->modifiedColumns[] = MurniRincianDetailPeer::DETAIL_NO;
		}

	} 
	
	public function setRekeningCode($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->rekening_code !== $v) {
			$this->rekening_code = $v;
			$this->modifiedColumns[] = MurniRincianDetailPeer::REKENING_CODE;
		}

	} 
	
	public function setKomponenId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->komponen_id !== $v) {
			$this->komponen_id = $v;
			$this->modifiedColumns[] = MurniRincianDetailPeer::KOMPONEN_ID;
		}

	} 
	
	public function setDetailName($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->detail_name !== $v) {
			$this->detail_name = $v;
			$this->modifiedColumns[] = MurniRincianDetailPeer::DETAIL_NAME;
		}

	} 
	
	public function setVolume($v)
	{

		if ($this->volume !== $v) {
			$this->volume = $v;
			$this->modifiedColumns[] = MurniRincianDetailPeer::VOLUME;
		}

	} 
	
	public function setKeteranganKoefisien($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->keterangan_koefisien !== $v) {
			$this->keterangan_koefisien = $v;
			$this->modifiedColumns[] = MurniRincianDetailPeer::KETERANGAN_KOEFISIEN;
		}

	} 
	
	public function setSubtitle($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->subtitle !== $v) {
			$this->subtitle = $v;
			$this->modifiedColumns[] = MurniRincianDetailPeer::SUBTITLE;
		}

	} 
	
	public function setKomponenHarga($v)
	{

		if ($this->komponen_harga !== $v) {
			$this->komponen_harga = $v;
			$this->modifiedColumns[] = MurniRincianDetailPeer::KOMPONEN_HARGA;
		}

	} 
	
	public function setKomponenHargaAwal($v)
	{

		if ($this->komponen_harga_awal !== $v) {
			$this->komponen_harga_awal = $v;
			$this->modifiedColumns[] = MurniRincianDetailPeer::KOMPONEN_HARGA_AWAL;
		}

	} 
	
	public function setKomponenName($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->komponen_name !== $v) {
			$this->komponen_name = $v;
			$this->modifiedColumns[] = MurniRincianDetailPeer::KOMPONEN_NAME;
		}

	} 
	
	public function setSatuan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->satuan !== $v) {
			$this->satuan = $v;
			$this->modifiedColumns[] = MurniRincianDetailPeer::SATUAN;
		}

	} 
	
	public function setPajak($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->pajak !== $v || $v === 0) {
			$this->pajak = $v;
			$this->modifiedColumns[] = MurniRincianDetailPeer::PAJAK;
		}

	} 
	
	public function setUnitId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->unit_id !== $v) {
			$this->unit_id = $v;
			$this->modifiedColumns[] = MurniRincianDetailPeer::UNIT_ID;
		}

	} 
	
	public function setFromSubKegiatan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->from_sub_kegiatan !== $v) {
			$this->from_sub_kegiatan = $v;
			$this->modifiedColumns[] = MurniRincianDetailPeer::FROM_SUB_KEGIATAN;
		}

	} 
	
	public function setSub($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->sub !== $v) {
			$this->sub = $v;
			$this->modifiedColumns[] = MurniRincianDetailPeer::SUB;
		}

	} 
	
	public function setKodeSub($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_sub !== $v) {
			$this->kode_sub = $v;
			$this->modifiedColumns[] = MurniRincianDetailPeer::KODE_SUB;
		}

	} 
	
	public function setLastUpdateUser($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->last_update_user !== $v) {
			$this->last_update_user = $v;
			$this->modifiedColumns[] = MurniRincianDetailPeer::LAST_UPDATE_USER;
		}

	} 
	
	public function setLastUpdateTime($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [last_update_time] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->last_update_time !== $ts) {
			$this->last_update_time = $ts;
			$this->modifiedColumns[] = MurniRincianDetailPeer::LAST_UPDATE_TIME;
		}

	} 
	
	public function setLastUpdateIp($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->last_update_ip !== $v) {
			$this->last_update_ip = $v;
			$this->modifiedColumns[] = MurniRincianDetailPeer::LAST_UPDATE_IP;
		}

	} 
	
	public function setTahap($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->tahap !== $v) {
			$this->tahap = $v;
			$this->modifiedColumns[] = MurniRincianDetailPeer::TAHAP;
		}

	} 
	
	public function setTahapEdit($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->tahap_edit !== $v) {
			$this->tahap_edit = $v;
			$this->modifiedColumns[] = MurniRincianDetailPeer::TAHAP_EDIT;
		}

	} 
	
	public function setTahapNew($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->tahap_new !== $v) {
			$this->tahap_new = $v;
			$this->modifiedColumns[] = MurniRincianDetailPeer::TAHAP_NEW;
		}

	} 
	
	public function setStatusLelang($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->status_lelang !== $v) {
			$this->status_lelang = $v;
			$this->modifiedColumns[] = MurniRincianDetailPeer::STATUS_LELANG;
		}

	} 
	
	public function setNomorLelang($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nomor_lelang !== $v) {
			$this->nomor_lelang = $v;
			$this->modifiedColumns[] = MurniRincianDetailPeer::NOMOR_LELANG;
		}

	} 
	
	public function setKoefisienSemula($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->koefisien_semula !== $v) {
			$this->koefisien_semula = $v;
			$this->modifiedColumns[] = MurniRincianDetailPeer::KOEFISIEN_SEMULA;
		}

	} 
	
	public function setVolumeSemula($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->volume_semula !== $v) {
			$this->volume_semula = $v;
			$this->modifiedColumns[] = MurniRincianDetailPeer::VOLUME_SEMULA;
		}

	} 
	
	public function setHargaSemula($v)
	{

		if ($this->harga_semula !== $v) {
			$this->harga_semula = $v;
			$this->modifiedColumns[] = MurniRincianDetailPeer::HARGA_SEMULA;
		}

	} 
	
	public function setTotalSemula($v)
	{

		if ($this->total_semula !== $v) {
			$this->total_semula = $v;
			$this->modifiedColumns[] = MurniRincianDetailPeer::TOTAL_SEMULA;
		}

	} 
	
	public function setLockSubtitle($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->lock_subtitle !== $v) {
			$this->lock_subtitle = $v;
			$this->modifiedColumns[] = MurniRincianDetailPeer::LOCK_SUBTITLE;
		}

	} 
	
	public function setStatusHapus($v)
	{

		if ($this->status_hapus !== $v || $v === false) {
			$this->status_hapus = $v;
			$this->modifiedColumns[] = MurniRincianDetailPeer::STATUS_HAPUS;
		}

	} 
	
	public function setTahun($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->tahun !== $v) {
			$this->tahun = $v;
			$this->modifiedColumns[] = MurniRincianDetailPeer::TAHUN;
		}

	} 
	
	public function setKodeLokasi($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_lokasi !== $v) {
			$this->kode_lokasi = $v;
			$this->modifiedColumns[] = MurniRincianDetailPeer::KODE_LOKASI;
		}

	} 
	
	public function setKecamatan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kecamatan !== $v) {
			$this->kecamatan = $v;
			$this->modifiedColumns[] = MurniRincianDetailPeer::KECAMATAN;
		}

	} 
	
	public function setRekeningCodeAsli($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->rekening_code_asli !== $v) {
			$this->rekening_code_asli = $v;
			$this->modifiedColumns[] = MurniRincianDetailPeer::REKENING_CODE_ASLI;
		}

	} 
	
	public function setNoteSkpd($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->note_skpd !== $v) {
			$this->note_skpd = $v;
			$this->modifiedColumns[] = MurniRincianDetailPeer::NOTE_SKPD;
		}

	} 
	
	public function setNotePeneliti($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->note_peneliti !== $v) {
			$this->note_peneliti = $v;
			$this->modifiedColumns[] = MurniRincianDetailPeer::NOTE_PENELITI;
		}

	} 
	
	public function setNilaiAnggaran($v)
	{

		if ($this->nilai_anggaran !== $v) {
			$this->nilai_anggaran = $v;
			$this->modifiedColumns[] = MurniRincianDetailPeer::NILAI_ANGGARAN;
		}

	} 
	
	public function setIsBlud($v)
	{

		if ($this->is_blud !== $v) {
			$this->is_blud = $v;
			$this->modifiedColumns[] = MurniRincianDetailPeer::IS_BLUD;
		}

	} 
	
	public function setLokasiKecamatan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->lokasi_kecamatan !== $v) {
			$this->lokasi_kecamatan = $v;
			$this->modifiedColumns[] = MurniRincianDetailPeer::LOKASI_KECAMATAN;
		}

	} 
	
	public function setLokasiKelurahan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->lokasi_kelurahan !== $v) {
			$this->lokasi_kelurahan = $v;
			$this->modifiedColumns[] = MurniRincianDetailPeer::LOKASI_KELURAHAN;
		}

	} 
	
	public function setOb($v)
	{

		if ($this->ob !== $v) {
			$this->ob = $v;
			$this->modifiedColumns[] = MurniRincianDetailPeer::OB;
		}

	} 
	
	public function setObFromId($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->ob_from_id !== $v) {
			$this->ob_from_id = $v;
			$this->modifiedColumns[] = MurniRincianDetailPeer::OB_FROM_ID;
		}

	} 
	
	public function setIsPerKomponen($v)
	{

		if ($this->is_per_komponen !== $v) {
			$this->is_per_komponen = $v;
			$this->modifiedColumns[] = MurniRincianDetailPeer::IS_PER_KOMPONEN;
		}

	} 
	
	public function setKegiatanCodeAsal($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kegiatan_code_asal !== $v) {
			$this->kegiatan_code_asal = $v;
			$this->modifiedColumns[] = MurniRincianDetailPeer::KEGIATAN_CODE_ASAL;
		}

	} 
	
	public function setThKeMultiyears($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->th_ke_multiyears !== $v) {
			$this->th_ke_multiyears = $v;
			$this->modifiedColumns[] = MurniRincianDetailPeer::TH_KE_MULTIYEARS;
		}

	} 
	
	public function setHargaSebelumSisaLelang($v)
	{

		if ($this->harga_sebelum_sisa_lelang !== $v) {
			$this->harga_sebelum_sisa_lelang = $v;
			$this->modifiedColumns[] = MurniRincianDetailPeer::HARGA_SEBELUM_SISA_LELANG;
		}

	} 
	
	public function setIsMusrenbang($v)
	{

		if ($this->is_musrenbang !== $v || $v === false) {
			$this->is_musrenbang = $v;
			$this->modifiedColumns[] = MurniRincianDetailPeer::IS_MUSRENBANG;
		}

	} 
	
	public function setSubIdAsal($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->sub_id_asal !== $v) {
			$this->sub_id_asal = $v;
			$this->modifiedColumns[] = MurniRincianDetailPeer::SUB_ID_ASAL;
		}

	} 
	
	public function setSubtitleAsal($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->subtitle_asal !== $v) {
			$this->subtitle_asal = $v;
			$this->modifiedColumns[] = MurniRincianDetailPeer::SUBTITLE_ASAL;
		}

	} 
	
	public function setKodeSubAsal($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_sub_asal !== $v) {
			$this->kode_sub_asal = $v;
			$this->modifiedColumns[] = MurniRincianDetailPeer::KODE_SUB_ASAL;
		}

	} 
	
	public function setSubAsal($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->sub_asal !== $v) {
			$this->sub_asal = $v;
			$this->modifiedColumns[] = MurniRincianDetailPeer::SUB_ASAL;
		}

	} 
	
	public function setLastEditTime($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [last_edit_time] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->last_edit_time !== $ts) {
			$this->last_edit_time = $ts;
			$this->modifiedColumns[] = MurniRincianDetailPeer::LAST_EDIT_TIME;
		}

	} 
	
	public function setIsPotongBpjs($v)
	{

		if ($this->is_potong_bpjs !== $v || $v === false) {
			$this->is_potong_bpjs = $v;
			$this->modifiedColumns[] = MurniRincianDetailPeer::IS_POTONG_BPJS;
		}

	} 
	
	public function setIsIuranBpjs($v)
	{

		if ($this->is_iuran_bpjs !== $v || $v === false) {
			$this->is_iuran_bpjs = $v;
			$this->modifiedColumns[] = MurniRincianDetailPeer::IS_IURAN_BPJS;
		}

	} 
	
	public function setStatusOb($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->status_ob !== $v || $v === 0) {
			$this->status_ob = $v;
			$this->modifiedColumns[] = MurniRincianDetailPeer::STATUS_OB;
		}

	} 
	
	public function setObParent($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->ob_parent !== $v) {
			$this->ob_parent = $v;
			$this->modifiedColumns[] = MurniRincianDetailPeer::OB_PARENT;
		}

	} 
	
	public function setObAlokasiBaru($v)
	{

		if ($this->ob_alokasi_baru !== $v) {
			$this->ob_alokasi_baru = $v;
			$this->modifiedColumns[] = MurniRincianDetailPeer::OB_ALOKASI_BARU;
		}

	} 
	
	public function setIsHibah($v)
	{

		if ($this->is_hibah !== $v || $v === false) {
			$this->is_hibah = $v;
			$this->modifiedColumns[] = MurniRincianDetailPeer::IS_HIBAH;
		}

	} 
	
	public function setStatusLevel($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->status_level !== $v || $v === 0) {
			$this->status_level = $v;
			$this->modifiedColumns[] = MurniRincianDetailPeer::STATUS_LEVEL;
		}

	} 
	
	public function setStatusLevelTolak($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->status_level_tolak !== $v) {
			$this->status_level_tolak = $v;
			$this->modifiedColumns[] = MurniRincianDetailPeer::STATUS_LEVEL_TOLAK;
		}

	} 
	
	public function setStatusSisipan($v)
	{

		if ($this->status_sisipan !== $v || $v === false) {
			$this->status_sisipan = $v;
			$this->modifiedColumns[] = MurniRincianDetailPeer::STATUS_SISIPAN;
		}

	} 
	
	public function setIsTapdSetuju($v)
	{

		if ($this->is_tapd_setuju !== $v || $v === false) {
			$this->is_tapd_setuju = $v;
			$this->modifiedColumns[] = MurniRincianDetailPeer::IS_TAPD_SETUJU;
		}

	} 
	
	public function setIsBappekoSetuju($v)
	{

		if ($this->is_bappeko_setuju !== $v || $v === false) {
			$this->is_bappeko_setuju = $v;
			$this->modifiedColumns[] = MurniRincianDetailPeer::IS_BAPPEKO_SETUJU;
		}

	} 
	
	public function setAkrualCode($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->akrual_code !== $v) {
			$this->akrual_code = $v;
			$this->modifiedColumns[] = MurniRincianDetailPeer::AKRUAL_CODE;
		}

	} 
	
	public function setTipe2($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->tipe2 !== $v) {
			$this->tipe2 = $v;
			$this->modifiedColumns[] = MurniRincianDetailPeer::TIPE2;
		}

	} 
	
	public function setIsPenyeliaSetuju($v)
	{

		if ($this->is_penyelia_setuju !== $v || $v === false) {
			$this->is_penyelia_setuju = $v;
			$this->modifiedColumns[] = MurniRincianDetailPeer::IS_PENYELIA_SETUJU;
		}

	} 
	
	public function setNoteTapd($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->note_tapd !== $v) {
			$this->note_tapd = $v;
			$this->modifiedColumns[] = MurniRincianDetailPeer::NOTE_TAPD;
		}

	} 
	
	public function setNoteBappeko($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->note_bappeko !== $v) {
			$this->note_bappeko = $v;
			$this->modifiedColumns[] = MurniRincianDetailPeer::NOTE_BAPPEKO;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->kegiatan_code = $rs->getString($startcol + 0);

			$this->tipe = $rs->getString($startcol + 1);

			$this->detail_no = $rs->getInt($startcol + 2);

			$this->rekening_code = $rs->getString($startcol + 3);

			$this->komponen_id = $rs->getString($startcol + 4);

			$this->detail_name = $rs->getString($startcol + 5);

			$this->volume = $rs->getFloat($startcol + 6);

			$this->keterangan_koefisien = $rs->getString($startcol + 7);

			$this->subtitle = $rs->getString($startcol + 8);

			$this->komponen_harga = $rs->getFloat($startcol + 9);

			$this->komponen_harga_awal = $rs->getFloat($startcol + 10);

			$this->komponen_name = $rs->getString($startcol + 11);

			$this->satuan = $rs->getString($startcol + 12);

			$this->pajak = $rs->getInt($startcol + 13);

			$this->unit_id = $rs->getString($startcol + 14);

			$this->from_sub_kegiatan = $rs->getString($startcol + 15);

			$this->sub = $rs->getString($startcol + 16);

			$this->kode_sub = $rs->getString($startcol + 17);

			$this->last_update_user = $rs->getString($startcol + 18);

			$this->last_update_time = $rs->getTimestamp($startcol + 19, null);

			$this->last_update_ip = $rs->getString($startcol + 20);

			$this->tahap = $rs->getString($startcol + 21);

			$this->tahap_edit = $rs->getString($startcol + 22);

			$this->tahap_new = $rs->getString($startcol + 23);

			$this->status_lelang = $rs->getString($startcol + 24);

			$this->nomor_lelang = $rs->getString($startcol + 25);

			$this->koefisien_semula = $rs->getString($startcol + 26);

			$this->volume_semula = $rs->getInt($startcol + 27);

			$this->harga_semula = $rs->getFloat($startcol + 28);

			$this->total_semula = $rs->getFloat($startcol + 29);

			$this->lock_subtitle = $rs->getString($startcol + 30);

			$this->status_hapus = $rs->getBoolean($startcol + 31);

			$this->tahun = $rs->getString($startcol + 32);

			$this->kode_lokasi = $rs->getString($startcol + 33);

			$this->kecamatan = $rs->getString($startcol + 34);

			$this->rekening_code_asli = $rs->getString($startcol + 35);

			$this->note_skpd = $rs->getString($startcol + 36);

			$this->note_peneliti = $rs->getString($startcol + 37);

			$this->nilai_anggaran = $rs->getFloat($startcol + 38);

			$this->is_blud = $rs->getBoolean($startcol + 39);

			$this->lokasi_kecamatan = $rs->getString($startcol + 40);

			$this->lokasi_kelurahan = $rs->getString($startcol + 41);

			$this->ob = $rs->getBoolean($startcol + 42);

			$this->ob_from_id = $rs->getInt($startcol + 43);

			$this->is_per_komponen = $rs->getBoolean($startcol + 44);

			$this->kegiatan_code_asal = $rs->getString($startcol + 45);

			$this->th_ke_multiyears = $rs->getInt($startcol + 46);

			$this->harga_sebelum_sisa_lelang = $rs->getFloat($startcol + 47);

			$this->is_musrenbang = $rs->getBoolean($startcol + 48);

			$this->sub_id_asal = $rs->getInt($startcol + 49);

			$this->subtitle_asal = $rs->getString($startcol + 50);

			$this->kode_sub_asal = $rs->getString($startcol + 51);

			$this->sub_asal = $rs->getString($startcol + 52);

			$this->last_edit_time = $rs->getTimestamp($startcol + 53, null);

			$this->is_potong_bpjs = $rs->getBoolean($startcol + 54);

			$this->is_iuran_bpjs = $rs->getBoolean($startcol + 55);

			$this->status_ob = $rs->getInt($startcol + 56);

			$this->ob_parent = $rs->getString($startcol + 57);

			$this->ob_alokasi_baru = $rs->getFloat($startcol + 58);

			$this->is_hibah = $rs->getBoolean($startcol + 59);

			$this->status_level = $rs->getInt($startcol + 60);

			$this->status_level_tolak = $rs->getInt($startcol + 61);

			$this->status_sisipan = $rs->getBoolean($startcol + 62);

			$this->is_tapd_setuju = $rs->getBoolean($startcol + 63);

			$this->is_bappeko_setuju = $rs->getBoolean($startcol + 64);

			$this->akrual_code = $rs->getString($startcol + 65);

			$this->tipe2 = $rs->getString($startcol + 66);

			$this->is_penyelia_setuju = $rs->getBoolean($startcol + 67);

			$this->note_tapd = $rs->getString($startcol + 68);

			$this->note_bappeko = $rs->getString($startcol + 69);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 70; 
		} catch (Exception $e) {
			throw new PropelException("Error populating MurniRincianDetail object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(MurniRincianDetailPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			MurniRincianDetailPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(MurniRincianDetailPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = MurniRincianDetailPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += MurniRincianDetailPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = MurniRincianDetailPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = MurniRincianDetailPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getKegiatanCode();
				break;
			case 1:
				return $this->getTipe();
				break;
			case 2:
				return $this->getDetailNo();
				break;
			case 3:
				return $this->getRekeningCode();
				break;
			case 4:
				return $this->getKomponenId();
				break;
			case 5:
				return $this->getDetailName();
				break;
			case 6:
				return $this->getVolume();
				break;
			case 7:
				return $this->getKeteranganKoefisien();
				break;
			case 8:
				return $this->getSubtitle();
				break;
			case 9:
				return $this->getKomponenHarga();
				break;
			case 10:
				return $this->getKomponenHargaAwal();
				break;
			case 11:
				return $this->getKomponenName();
				break;
			case 12:
				return $this->getSatuan();
				break;
			case 13:
				return $this->getPajak();
				break;
			case 14:
				return $this->getUnitId();
				break;
			case 15:
				return $this->getFromSubKegiatan();
				break;
			case 16:
				return $this->getSub();
				break;
			case 17:
				return $this->getKodeSub();
				break;
			case 18:
				return $this->getLastUpdateUser();
				break;
			case 19:
				return $this->getLastUpdateTime();
				break;
			case 20:
				return $this->getLastUpdateIp();
				break;
			case 21:
				return $this->getTahap();
				break;
			case 22:
				return $this->getTahapEdit();
				break;
			case 23:
				return $this->getTahapNew();
				break;
			case 24:
				return $this->getStatusLelang();
				break;
			case 25:
				return $this->getNomorLelang();
				break;
			case 26:
				return $this->getKoefisienSemula();
				break;
			case 27:
				return $this->getVolumeSemula();
				break;
			case 28:
				return $this->getHargaSemula();
				break;
			case 29:
				return $this->getTotalSemula();
				break;
			case 30:
				return $this->getLockSubtitle();
				break;
			case 31:
				return $this->getStatusHapus();
				break;
			case 32:
				return $this->getTahun();
				break;
			case 33:
				return $this->getKodeLokasi();
				break;
			case 34:
				return $this->getKecamatan();
				break;
			case 35:
				return $this->getRekeningCodeAsli();
				break;
			case 36:
				return $this->getNoteSkpd();
				break;
			case 37:
				return $this->getNotePeneliti();
				break;
			case 38:
				return $this->getNilaiAnggaran();
				break;
			case 39:
				return $this->getIsBlud();
				break;
			case 40:
				return $this->getLokasiKecamatan();
				break;
			case 41:
				return $this->getLokasiKelurahan();
				break;
			case 42:
				return $this->getOb();
				break;
			case 43:
				return $this->getObFromId();
				break;
			case 44:
				return $this->getIsPerKomponen();
				break;
			case 45:
				return $this->getKegiatanCodeAsal();
				break;
			case 46:
				return $this->getThKeMultiyears();
				break;
			case 47:
				return $this->getHargaSebelumSisaLelang();
				break;
			case 48:
				return $this->getIsMusrenbang();
				break;
			case 49:
				return $this->getSubIdAsal();
				break;
			case 50:
				return $this->getSubtitleAsal();
				break;
			case 51:
				return $this->getKodeSubAsal();
				break;
			case 52:
				return $this->getSubAsal();
				break;
			case 53:
				return $this->getLastEditTime();
				break;
			case 54:
				return $this->getIsPotongBpjs();
				break;
			case 55:
				return $this->getIsIuranBpjs();
				break;
			case 56:
				return $this->getStatusOb();
				break;
			case 57:
				return $this->getObParent();
				break;
			case 58:
				return $this->getObAlokasiBaru();
				break;
			case 59:
				return $this->getIsHibah();
				break;
			case 60:
				return $this->getStatusLevel();
				break;
			case 61:
				return $this->getStatusLevelTolak();
				break;
			case 62:
				return $this->getStatusSisipan();
				break;
			case 63:
				return $this->getIsTapdSetuju();
				break;
			case 64:
				return $this->getIsBappekoSetuju();
				break;
			case 65:
				return $this->getAkrualCode();
				break;
			case 66:
				return $this->getTipe2();
				break;
			case 67:
				return $this->getIsPenyeliaSetuju();
				break;
			case 68:
				return $this->getNoteTapd();
				break;
			case 69:
				return $this->getNoteBappeko();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = MurniRincianDetailPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getKegiatanCode(),
			$keys[1] => $this->getTipe(),
			$keys[2] => $this->getDetailNo(),
			$keys[3] => $this->getRekeningCode(),
			$keys[4] => $this->getKomponenId(),
			$keys[5] => $this->getDetailName(),
			$keys[6] => $this->getVolume(),
			$keys[7] => $this->getKeteranganKoefisien(),
			$keys[8] => $this->getSubtitle(),
			$keys[9] => $this->getKomponenHarga(),
			$keys[10] => $this->getKomponenHargaAwal(),
			$keys[11] => $this->getKomponenName(),
			$keys[12] => $this->getSatuan(),
			$keys[13] => $this->getPajak(),
			$keys[14] => $this->getUnitId(),
			$keys[15] => $this->getFromSubKegiatan(),
			$keys[16] => $this->getSub(),
			$keys[17] => $this->getKodeSub(),
			$keys[18] => $this->getLastUpdateUser(),
			$keys[19] => $this->getLastUpdateTime(),
			$keys[20] => $this->getLastUpdateIp(),
			$keys[21] => $this->getTahap(),
			$keys[22] => $this->getTahapEdit(),
			$keys[23] => $this->getTahapNew(),
			$keys[24] => $this->getStatusLelang(),
			$keys[25] => $this->getNomorLelang(),
			$keys[26] => $this->getKoefisienSemula(),
			$keys[27] => $this->getVolumeSemula(),
			$keys[28] => $this->getHargaSemula(),
			$keys[29] => $this->getTotalSemula(),
			$keys[30] => $this->getLockSubtitle(),
			$keys[31] => $this->getStatusHapus(),
			$keys[32] => $this->getTahun(),
			$keys[33] => $this->getKodeLokasi(),
			$keys[34] => $this->getKecamatan(),
			$keys[35] => $this->getRekeningCodeAsli(),
			$keys[36] => $this->getNoteSkpd(),
			$keys[37] => $this->getNotePeneliti(),
			$keys[38] => $this->getNilaiAnggaran(),
			$keys[39] => $this->getIsBlud(),
			$keys[40] => $this->getLokasiKecamatan(),
			$keys[41] => $this->getLokasiKelurahan(),
			$keys[42] => $this->getOb(),
			$keys[43] => $this->getObFromId(),
			$keys[44] => $this->getIsPerKomponen(),
			$keys[45] => $this->getKegiatanCodeAsal(),
			$keys[46] => $this->getThKeMultiyears(),
			$keys[47] => $this->getHargaSebelumSisaLelang(),
			$keys[48] => $this->getIsMusrenbang(),
			$keys[49] => $this->getSubIdAsal(),
			$keys[50] => $this->getSubtitleAsal(),
			$keys[51] => $this->getKodeSubAsal(),
			$keys[52] => $this->getSubAsal(),
			$keys[53] => $this->getLastEditTime(),
			$keys[54] => $this->getIsPotongBpjs(),
			$keys[55] => $this->getIsIuranBpjs(),
			$keys[56] => $this->getStatusOb(),
			$keys[57] => $this->getObParent(),
			$keys[58] => $this->getObAlokasiBaru(),
			$keys[59] => $this->getIsHibah(),
			$keys[60] => $this->getStatusLevel(),
			$keys[61] => $this->getStatusLevelTolak(),
			$keys[62] => $this->getStatusSisipan(),
			$keys[63] => $this->getIsTapdSetuju(),
			$keys[64] => $this->getIsBappekoSetuju(),
			$keys[65] => $this->getAkrualCode(),
			$keys[66] => $this->getTipe2(),
			$keys[67] => $this->getIsPenyeliaSetuju(),
			$keys[68] => $this->getNoteTapd(),
			$keys[69] => $this->getNoteBappeko(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = MurniRincianDetailPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setKegiatanCode($value);
				break;
			case 1:
				$this->setTipe($value);
				break;
			case 2:
				$this->setDetailNo($value);
				break;
			case 3:
				$this->setRekeningCode($value);
				break;
			case 4:
				$this->setKomponenId($value);
				break;
			case 5:
				$this->setDetailName($value);
				break;
			case 6:
				$this->setVolume($value);
				break;
			case 7:
				$this->setKeteranganKoefisien($value);
				break;
			case 8:
				$this->setSubtitle($value);
				break;
			case 9:
				$this->setKomponenHarga($value);
				break;
			case 10:
				$this->setKomponenHargaAwal($value);
				break;
			case 11:
				$this->setKomponenName($value);
				break;
			case 12:
				$this->setSatuan($value);
				break;
			case 13:
				$this->setPajak($value);
				break;
			case 14:
				$this->setUnitId($value);
				break;
			case 15:
				$this->setFromSubKegiatan($value);
				break;
			case 16:
				$this->setSub($value);
				break;
			case 17:
				$this->setKodeSub($value);
				break;
			case 18:
				$this->setLastUpdateUser($value);
				break;
			case 19:
				$this->setLastUpdateTime($value);
				break;
			case 20:
				$this->setLastUpdateIp($value);
				break;
			case 21:
				$this->setTahap($value);
				break;
			case 22:
				$this->setTahapEdit($value);
				break;
			case 23:
				$this->setTahapNew($value);
				break;
			case 24:
				$this->setStatusLelang($value);
				break;
			case 25:
				$this->setNomorLelang($value);
				break;
			case 26:
				$this->setKoefisienSemula($value);
				break;
			case 27:
				$this->setVolumeSemula($value);
				break;
			case 28:
				$this->setHargaSemula($value);
				break;
			case 29:
				$this->setTotalSemula($value);
				break;
			case 30:
				$this->setLockSubtitle($value);
				break;
			case 31:
				$this->setStatusHapus($value);
				break;
			case 32:
				$this->setTahun($value);
				break;
			case 33:
				$this->setKodeLokasi($value);
				break;
			case 34:
				$this->setKecamatan($value);
				break;
			case 35:
				$this->setRekeningCodeAsli($value);
				break;
			case 36:
				$this->setNoteSkpd($value);
				break;
			case 37:
				$this->setNotePeneliti($value);
				break;
			case 38:
				$this->setNilaiAnggaran($value);
				break;
			case 39:
				$this->setIsBlud($value);
				break;
			case 40:
				$this->setLokasiKecamatan($value);
				break;
			case 41:
				$this->setLokasiKelurahan($value);
				break;
			case 42:
				$this->setOb($value);
				break;
			case 43:
				$this->setObFromId($value);
				break;
			case 44:
				$this->setIsPerKomponen($value);
				break;
			case 45:
				$this->setKegiatanCodeAsal($value);
				break;
			case 46:
				$this->setThKeMultiyears($value);
				break;
			case 47:
				$this->setHargaSebelumSisaLelang($value);
				break;
			case 48:
				$this->setIsMusrenbang($value);
				break;
			case 49:
				$this->setSubIdAsal($value);
				break;
			case 50:
				$this->setSubtitleAsal($value);
				break;
			case 51:
				$this->setKodeSubAsal($value);
				break;
			case 52:
				$this->setSubAsal($value);
				break;
			case 53:
				$this->setLastEditTime($value);
				break;
			case 54:
				$this->setIsPotongBpjs($value);
				break;
			case 55:
				$this->setIsIuranBpjs($value);
				break;
			case 56:
				$this->setStatusOb($value);
				break;
			case 57:
				$this->setObParent($value);
				break;
			case 58:
				$this->setObAlokasiBaru($value);
				break;
			case 59:
				$this->setIsHibah($value);
				break;
			case 60:
				$this->setStatusLevel($value);
				break;
			case 61:
				$this->setStatusLevelTolak($value);
				break;
			case 62:
				$this->setStatusSisipan($value);
				break;
			case 63:
				$this->setIsTapdSetuju($value);
				break;
			case 64:
				$this->setIsBappekoSetuju($value);
				break;
			case 65:
				$this->setAkrualCode($value);
				break;
			case 66:
				$this->setTipe2($value);
				break;
			case 67:
				$this->setIsPenyeliaSetuju($value);
				break;
			case 68:
				$this->setNoteTapd($value);
				break;
			case 69:
				$this->setNoteBappeko($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = MurniRincianDetailPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setKegiatanCode($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setTipe($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setDetailNo($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setRekeningCode($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setKomponenId($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setDetailName($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setVolume($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setKeteranganKoefisien($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setSubtitle($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setKomponenHarga($arr[$keys[9]]);
		if (array_key_exists($keys[10], $arr)) $this->setKomponenHargaAwal($arr[$keys[10]]);
		if (array_key_exists($keys[11], $arr)) $this->setKomponenName($arr[$keys[11]]);
		if (array_key_exists($keys[12], $arr)) $this->setSatuan($arr[$keys[12]]);
		if (array_key_exists($keys[13], $arr)) $this->setPajak($arr[$keys[13]]);
		if (array_key_exists($keys[14], $arr)) $this->setUnitId($arr[$keys[14]]);
		if (array_key_exists($keys[15], $arr)) $this->setFromSubKegiatan($arr[$keys[15]]);
		if (array_key_exists($keys[16], $arr)) $this->setSub($arr[$keys[16]]);
		if (array_key_exists($keys[17], $arr)) $this->setKodeSub($arr[$keys[17]]);
		if (array_key_exists($keys[18], $arr)) $this->setLastUpdateUser($arr[$keys[18]]);
		if (array_key_exists($keys[19], $arr)) $this->setLastUpdateTime($arr[$keys[19]]);
		if (array_key_exists($keys[20], $arr)) $this->setLastUpdateIp($arr[$keys[20]]);
		if (array_key_exists($keys[21], $arr)) $this->setTahap($arr[$keys[21]]);
		if (array_key_exists($keys[22], $arr)) $this->setTahapEdit($arr[$keys[22]]);
		if (array_key_exists($keys[23], $arr)) $this->setTahapNew($arr[$keys[23]]);
		if (array_key_exists($keys[24], $arr)) $this->setStatusLelang($arr[$keys[24]]);
		if (array_key_exists($keys[25], $arr)) $this->setNomorLelang($arr[$keys[25]]);
		if (array_key_exists($keys[26], $arr)) $this->setKoefisienSemula($arr[$keys[26]]);
		if (array_key_exists($keys[27], $arr)) $this->setVolumeSemula($arr[$keys[27]]);
		if (array_key_exists($keys[28], $arr)) $this->setHargaSemula($arr[$keys[28]]);
		if (array_key_exists($keys[29], $arr)) $this->setTotalSemula($arr[$keys[29]]);
		if (array_key_exists($keys[30], $arr)) $this->setLockSubtitle($arr[$keys[30]]);
		if (array_key_exists($keys[31], $arr)) $this->setStatusHapus($arr[$keys[31]]);
		if (array_key_exists($keys[32], $arr)) $this->setTahun($arr[$keys[32]]);
		if (array_key_exists($keys[33], $arr)) $this->setKodeLokasi($arr[$keys[33]]);
		if (array_key_exists($keys[34], $arr)) $this->setKecamatan($arr[$keys[34]]);
		if (array_key_exists($keys[35], $arr)) $this->setRekeningCodeAsli($arr[$keys[35]]);
		if (array_key_exists($keys[36], $arr)) $this->setNoteSkpd($arr[$keys[36]]);
		if (array_key_exists($keys[37], $arr)) $this->setNotePeneliti($arr[$keys[37]]);
		if (array_key_exists($keys[38], $arr)) $this->setNilaiAnggaran($arr[$keys[38]]);
		if (array_key_exists($keys[39], $arr)) $this->setIsBlud($arr[$keys[39]]);
		if (array_key_exists($keys[40], $arr)) $this->setLokasiKecamatan($arr[$keys[40]]);
		if (array_key_exists($keys[41], $arr)) $this->setLokasiKelurahan($arr[$keys[41]]);
		if (array_key_exists($keys[42], $arr)) $this->setOb($arr[$keys[42]]);
		if (array_key_exists($keys[43], $arr)) $this->setObFromId($arr[$keys[43]]);
		if (array_key_exists($keys[44], $arr)) $this->setIsPerKomponen($arr[$keys[44]]);
		if (array_key_exists($keys[45], $arr)) $this->setKegiatanCodeAsal($arr[$keys[45]]);
		if (array_key_exists($keys[46], $arr)) $this->setThKeMultiyears($arr[$keys[46]]);
		if (array_key_exists($keys[47], $arr)) $this->setHargaSebelumSisaLelang($arr[$keys[47]]);
		if (array_key_exists($keys[48], $arr)) $this->setIsMusrenbang($arr[$keys[48]]);
		if (array_key_exists($keys[49], $arr)) $this->setSubIdAsal($arr[$keys[49]]);
		if (array_key_exists($keys[50], $arr)) $this->setSubtitleAsal($arr[$keys[50]]);
		if (array_key_exists($keys[51], $arr)) $this->setKodeSubAsal($arr[$keys[51]]);
		if (array_key_exists($keys[52], $arr)) $this->setSubAsal($arr[$keys[52]]);
		if (array_key_exists($keys[53], $arr)) $this->setLastEditTime($arr[$keys[53]]);
		if (array_key_exists($keys[54], $arr)) $this->setIsPotongBpjs($arr[$keys[54]]);
		if (array_key_exists($keys[55], $arr)) $this->setIsIuranBpjs($arr[$keys[55]]);
		if (array_key_exists($keys[56], $arr)) $this->setStatusOb($arr[$keys[56]]);
		if (array_key_exists($keys[57], $arr)) $this->setObParent($arr[$keys[57]]);
		if (array_key_exists($keys[58], $arr)) $this->setObAlokasiBaru($arr[$keys[58]]);
		if (array_key_exists($keys[59], $arr)) $this->setIsHibah($arr[$keys[59]]);
		if (array_key_exists($keys[60], $arr)) $this->setStatusLevel($arr[$keys[60]]);
		if (array_key_exists($keys[61], $arr)) $this->setStatusLevelTolak($arr[$keys[61]]);
		if (array_key_exists($keys[62], $arr)) $this->setStatusSisipan($arr[$keys[62]]);
		if (array_key_exists($keys[63], $arr)) $this->setIsTapdSetuju($arr[$keys[63]]);
		if (array_key_exists($keys[64], $arr)) $this->setIsBappekoSetuju($arr[$keys[64]]);
		if (array_key_exists($keys[65], $arr)) $this->setAkrualCode($arr[$keys[65]]);
		if (array_key_exists($keys[66], $arr)) $this->setTipe2($arr[$keys[66]]);
		if (array_key_exists($keys[67], $arr)) $this->setIsPenyeliaSetuju($arr[$keys[67]]);
		if (array_key_exists($keys[68], $arr)) $this->setNoteTapd($arr[$keys[68]]);
		if (array_key_exists($keys[69], $arr)) $this->setNoteBappeko($arr[$keys[69]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(MurniRincianDetailPeer::DATABASE_NAME);

		if ($this->isColumnModified(MurniRincianDetailPeer::KEGIATAN_CODE)) $criteria->add(MurniRincianDetailPeer::KEGIATAN_CODE, $this->kegiatan_code);
		if ($this->isColumnModified(MurniRincianDetailPeer::TIPE)) $criteria->add(MurniRincianDetailPeer::TIPE, $this->tipe);
		if ($this->isColumnModified(MurniRincianDetailPeer::DETAIL_NO)) $criteria->add(MurniRincianDetailPeer::DETAIL_NO, $this->detail_no);
		if ($this->isColumnModified(MurniRincianDetailPeer::REKENING_CODE)) $criteria->add(MurniRincianDetailPeer::REKENING_CODE, $this->rekening_code);
		if ($this->isColumnModified(MurniRincianDetailPeer::KOMPONEN_ID)) $criteria->add(MurniRincianDetailPeer::KOMPONEN_ID, $this->komponen_id);
		if ($this->isColumnModified(MurniRincianDetailPeer::DETAIL_NAME)) $criteria->add(MurniRincianDetailPeer::DETAIL_NAME, $this->detail_name);
		if ($this->isColumnModified(MurniRincianDetailPeer::VOLUME)) $criteria->add(MurniRincianDetailPeer::VOLUME, $this->volume);
		if ($this->isColumnModified(MurniRincianDetailPeer::KETERANGAN_KOEFISIEN)) $criteria->add(MurniRincianDetailPeer::KETERANGAN_KOEFISIEN, $this->keterangan_koefisien);
		if ($this->isColumnModified(MurniRincianDetailPeer::SUBTITLE)) $criteria->add(MurniRincianDetailPeer::SUBTITLE, $this->subtitle);
		if ($this->isColumnModified(MurniRincianDetailPeer::KOMPONEN_HARGA)) $criteria->add(MurniRincianDetailPeer::KOMPONEN_HARGA, $this->komponen_harga);
		if ($this->isColumnModified(MurniRincianDetailPeer::KOMPONEN_HARGA_AWAL)) $criteria->add(MurniRincianDetailPeer::KOMPONEN_HARGA_AWAL, $this->komponen_harga_awal);
		if ($this->isColumnModified(MurniRincianDetailPeer::KOMPONEN_NAME)) $criteria->add(MurniRincianDetailPeer::KOMPONEN_NAME, $this->komponen_name);
		if ($this->isColumnModified(MurniRincianDetailPeer::SATUAN)) $criteria->add(MurniRincianDetailPeer::SATUAN, $this->satuan);
		if ($this->isColumnModified(MurniRincianDetailPeer::PAJAK)) $criteria->add(MurniRincianDetailPeer::PAJAK, $this->pajak);
		if ($this->isColumnModified(MurniRincianDetailPeer::UNIT_ID)) $criteria->add(MurniRincianDetailPeer::UNIT_ID, $this->unit_id);
		if ($this->isColumnModified(MurniRincianDetailPeer::FROM_SUB_KEGIATAN)) $criteria->add(MurniRincianDetailPeer::FROM_SUB_KEGIATAN, $this->from_sub_kegiatan);
		if ($this->isColumnModified(MurniRincianDetailPeer::SUB)) $criteria->add(MurniRincianDetailPeer::SUB, $this->sub);
		if ($this->isColumnModified(MurniRincianDetailPeer::KODE_SUB)) $criteria->add(MurniRincianDetailPeer::KODE_SUB, $this->kode_sub);
		if ($this->isColumnModified(MurniRincianDetailPeer::LAST_UPDATE_USER)) $criteria->add(MurniRincianDetailPeer::LAST_UPDATE_USER, $this->last_update_user);
		if ($this->isColumnModified(MurniRincianDetailPeer::LAST_UPDATE_TIME)) $criteria->add(MurniRincianDetailPeer::LAST_UPDATE_TIME, $this->last_update_time);
		if ($this->isColumnModified(MurniRincianDetailPeer::LAST_UPDATE_IP)) $criteria->add(MurniRincianDetailPeer::LAST_UPDATE_IP, $this->last_update_ip);
		if ($this->isColumnModified(MurniRincianDetailPeer::TAHAP)) $criteria->add(MurniRincianDetailPeer::TAHAP, $this->tahap);
		if ($this->isColumnModified(MurniRincianDetailPeer::TAHAP_EDIT)) $criteria->add(MurniRincianDetailPeer::TAHAP_EDIT, $this->tahap_edit);
		if ($this->isColumnModified(MurniRincianDetailPeer::TAHAP_NEW)) $criteria->add(MurniRincianDetailPeer::TAHAP_NEW, $this->tahap_new);
		if ($this->isColumnModified(MurniRincianDetailPeer::STATUS_LELANG)) $criteria->add(MurniRincianDetailPeer::STATUS_LELANG, $this->status_lelang);
		if ($this->isColumnModified(MurniRincianDetailPeer::NOMOR_LELANG)) $criteria->add(MurniRincianDetailPeer::NOMOR_LELANG, $this->nomor_lelang);
		if ($this->isColumnModified(MurniRincianDetailPeer::KOEFISIEN_SEMULA)) $criteria->add(MurniRincianDetailPeer::KOEFISIEN_SEMULA, $this->koefisien_semula);
		if ($this->isColumnModified(MurniRincianDetailPeer::VOLUME_SEMULA)) $criteria->add(MurniRincianDetailPeer::VOLUME_SEMULA, $this->volume_semula);
		if ($this->isColumnModified(MurniRincianDetailPeer::HARGA_SEMULA)) $criteria->add(MurniRincianDetailPeer::HARGA_SEMULA, $this->harga_semula);
		if ($this->isColumnModified(MurniRincianDetailPeer::TOTAL_SEMULA)) $criteria->add(MurniRincianDetailPeer::TOTAL_SEMULA, $this->total_semula);
		if ($this->isColumnModified(MurniRincianDetailPeer::LOCK_SUBTITLE)) $criteria->add(MurniRincianDetailPeer::LOCK_SUBTITLE, $this->lock_subtitle);
		if ($this->isColumnModified(MurniRincianDetailPeer::STATUS_HAPUS)) $criteria->add(MurniRincianDetailPeer::STATUS_HAPUS, $this->status_hapus);
		if ($this->isColumnModified(MurniRincianDetailPeer::TAHUN)) $criteria->add(MurniRincianDetailPeer::TAHUN, $this->tahun);
		if ($this->isColumnModified(MurniRincianDetailPeer::KODE_LOKASI)) $criteria->add(MurniRincianDetailPeer::KODE_LOKASI, $this->kode_lokasi);
		if ($this->isColumnModified(MurniRincianDetailPeer::KECAMATAN)) $criteria->add(MurniRincianDetailPeer::KECAMATAN, $this->kecamatan);
		if ($this->isColumnModified(MurniRincianDetailPeer::REKENING_CODE_ASLI)) $criteria->add(MurniRincianDetailPeer::REKENING_CODE_ASLI, $this->rekening_code_asli);
		if ($this->isColumnModified(MurniRincianDetailPeer::NOTE_SKPD)) $criteria->add(MurniRincianDetailPeer::NOTE_SKPD, $this->note_skpd);
		if ($this->isColumnModified(MurniRincianDetailPeer::NOTE_PENELITI)) $criteria->add(MurniRincianDetailPeer::NOTE_PENELITI, $this->note_peneliti);
		if ($this->isColumnModified(MurniRincianDetailPeer::NILAI_ANGGARAN)) $criteria->add(MurniRincianDetailPeer::NILAI_ANGGARAN, $this->nilai_anggaran);
		if ($this->isColumnModified(MurniRincianDetailPeer::IS_BLUD)) $criteria->add(MurniRincianDetailPeer::IS_BLUD, $this->is_blud);
		if ($this->isColumnModified(MurniRincianDetailPeer::LOKASI_KECAMATAN)) $criteria->add(MurniRincianDetailPeer::LOKASI_KECAMATAN, $this->lokasi_kecamatan);
		if ($this->isColumnModified(MurniRincianDetailPeer::LOKASI_KELURAHAN)) $criteria->add(MurniRincianDetailPeer::LOKASI_KELURAHAN, $this->lokasi_kelurahan);
		if ($this->isColumnModified(MurniRincianDetailPeer::OB)) $criteria->add(MurniRincianDetailPeer::OB, $this->ob);
		if ($this->isColumnModified(MurniRincianDetailPeer::OB_FROM_ID)) $criteria->add(MurniRincianDetailPeer::OB_FROM_ID, $this->ob_from_id);
		if ($this->isColumnModified(MurniRincianDetailPeer::IS_PER_KOMPONEN)) $criteria->add(MurniRincianDetailPeer::IS_PER_KOMPONEN, $this->is_per_komponen);
		if ($this->isColumnModified(MurniRincianDetailPeer::KEGIATAN_CODE_ASAL)) $criteria->add(MurniRincianDetailPeer::KEGIATAN_CODE_ASAL, $this->kegiatan_code_asal);
		if ($this->isColumnModified(MurniRincianDetailPeer::TH_KE_MULTIYEARS)) $criteria->add(MurniRincianDetailPeer::TH_KE_MULTIYEARS, $this->th_ke_multiyears);
		if ($this->isColumnModified(MurniRincianDetailPeer::HARGA_SEBELUM_SISA_LELANG)) $criteria->add(MurniRincianDetailPeer::HARGA_SEBELUM_SISA_LELANG, $this->harga_sebelum_sisa_lelang);
		if ($this->isColumnModified(MurniRincianDetailPeer::IS_MUSRENBANG)) $criteria->add(MurniRincianDetailPeer::IS_MUSRENBANG, $this->is_musrenbang);
		if ($this->isColumnModified(MurniRincianDetailPeer::SUB_ID_ASAL)) $criteria->add(MurniRincianDetailPeer::SUB_ID_ASAL, $this->sub_id_asal);
		if ($this->isColumnModified(MurniRincianDetailPeer::SUBTITLE_ASAL)) $criteria->add(MurniRincianDetailPeer::SUBTITLE_ASAL, $this->subtitle_asal);
		if ($this->isColumnModified(MurniRincianDetailPeer::KODE_SUB_ASAL)) $criteria->add(MurniRincianDetailPeer::KODE_SUB_ASAL, $this->kode_sub_asal);
		if ($this->isColumnModified(MurniRincianDetailPeer::SUB_ASAL)) $criteria->add(MurniRincianDetailPeer::SUB_ASAL, $this->sub_asal);
		if ($this->isColumnModified(MurniRincianDetailPeer::LAST_EDIT_TIME)) $criteria->add(MurniRincianDetailPeer::LAST_EDIT_TIME, $this->last_edit_time);
		if ($this->isColumnModified(MurniRincianDetailPeer::IS_POTONG_BPJS)) $criteria->add(MurniRincianDetailPeer::IS_POTONG_BPJS, $this->is_potong_bpjs);
		if ($this->isColumnModified(MurniRincianDetailPeer::IS_IURAN_BPJS)) $criteria->add(MurniRincianDetailPeer::IS_IURAN_BPJS, $this->is_iuran_bpjs);
		if ($this->isColumnModified(MurniRincianDetailPeer::STATUS_OB)) $criteria->add(MurniRincianDetailPeer::STATUS_OB, $this->status_ob);
		if ($this->isColumnModified(MurniRincianDetailPeer::OB_PARENT)) $criteria->add(MurniRincianDetailPeer::OB_PARENT, $this->ob_parent);
		if ($this->isColumnModified(MurniRincianDetailPeer::OB_ALOKASI_BARU)) $criteria->add(MurniRincianDetailPeer::OB_ALOKASI_BARU, $this->ob_alokasi_baru);
		if ($this->isColumnModified(MurniRincianDetailPeer::IS_HIBAH)) $criteria->add(MurniRincianDetailPeer::IS_HIBAH, $this->is_hibah);
		if ($this->isColumnModified(MurniRincianDetailPeer::STATUS_LEVEL)) $criteria->add(MurniRincianDetailPeer::STATUS_LEVEL, $this->status_level);
		if ($this->isColumnModified(MurniRincianDetailPeer::STATUS_LEVEL_TOLAK)) $criteria->add(MurniRincianDetailPeer::STATUS_LEVEL_TOLAK, $this->status_level_tolak);
		if ($this->isColumnModified(MurniRincianDetailPeer::STATUS_SISIPAN)) $criteria->add(MurniRincianDetailPeer::STATUS_SISIPAN, $this->status_sisipan);
		if ($this->isColumnModified(MurniRincianDetailPeer::IS_TAPD_SETUJU)) $criteria->add(MurniRincianDetailPeer::IS_TAPD_SETUJU, $this->is_tapd_setuju);
		if ($this->isColumnModified(MurniRincianDetailPeer::IS_BAPPEKO_SETUJU)) $criteria->add(MurniRincianDetailPeer::IS_BAPPEKO_SETUJU, $this->is_bappeko_setuju);
		if ($this->isColumnModified(MurniRincianDetailPeer::AKRUAL_CODE)) $criteria->add(MurniRincianDetailPeer::AKRUAL_CODE, $this->akrual_code);
		if ($this->isColumnModified(MurniRincianDetailPeer::TIPE2)) $criteria->add(MurniRincianDetailPeer::TIPE2, $this->tipe2);
		if ($this->isColumnModified(MurniRincianDetailPeer::IS_PENYELIA_SETUJU)) $criteria->add(MurniRincianDetailPeer::IS_PENYELIA_SETUJU, $this->is_penyelia_setuju);
		if ($this->isColumnModified(MurniRincianDetailPeer::NOTE_TAPD)) $criteria->add(MurniRincianDetailPeer::NOTE_TAPD, $this->note_tapd);
		if ($this->isColumnModified(MurniRincianDetailPeer::NOTE_BAPPEKO)) $criteria->add(MurniRincianDetailPeer::NOTE_BAPPEKO, $this->note_bappeko);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(MurniRincianDetailPeer::DATABASE_NAME);

		$criteria->add(MurniRincianDetailPeer::KEGIATAN_CODE, $this->kegiatan_code);
		$criteria->add(MurniRincianDetailPeer::TIPE, $this->tipe);
		$criteria->add(MurniRincianDetailPeer::DETAIL_NO, $this->detail_no);
		$criteria->add(MurniRincianDetailPeer::UNIT_ID, $this->unit_id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		$pks = array();

		$pks[0] = $this->getKegiatanCode();

		$pks[1] = $this->getTipe();

		$pks[2] = $this->getDetailNo();

		$pks[3] = $this->getUnitId();

		return $pks;
	}

	
	public function setPrimaryKey($keys)
	{

		$this->setKegiatanCode($keys[0]);

		$this->setTipe($keys[1]);

		$this->setDetailNo($keys[2]);

		$this->setUnitId($keys[3]);

	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setRekeningCode($this->rekening_code);

		$copyObj->setKomponenId($this->komponen_id);

		$copyObj->setDetailName($this->detail_name);

		$copyObj->setVolume($this->volume);

		$copyObj->setKeteranganKoefisien($this->keterangan_koefisien);

		$copyObj->setSubtitle($this->subtitle);

		$copyObj->setKomponenHarga($this->komponen_harga);

		$copyObj->setKomponenHargaAwal($this->komponen_harga_awal);

		$copyObj->setKomponenName($this->komponen_name);

		$copyObj->setSatuan($this->satuan);

		$copyObj->setPajak($this->pajak);

		$copyObj->setFromSubKegiatan($this->from_sub_kegiatan);

		$copyObj->setSub($this->sub);

		$copyObj->setKodeSub($this->kode_sub);

		$copyObj->setLastUpdateUser($this->last_update_user);

		$copyObj->setLastUpdateTime($this->last_update_time);

		$copyObj->setLastUpdateIp($this->last_update_ip);

		$copyObj->setTahap($this->tahap);

		$copyObj->setTahapEdit($this->tahap_edit);

		$copyObj->setTahapNew($this->tahap_new);

		$copyObj->setStatusLelang($this->status_lelang);

		$copyObj->setNomorLelang($this->nomor_lelang);

		$copyObj->setKoefisienSemula($this->koefisien_semula);

		$copyObj->setVolumeSemula($this->volume_semula);

		$copyObj->setHargaSemula($this->harga_semula);

		$copyObj->setTotalSemula($this->total_semula);

		$copyObj->setLockSubtitle($this->lock_subtitle);

		$copyObj->setStatusHapus($this->status_hapus);

		$copyObj->setTahun($this->tahun);

		$copyObj->setKodeLokasi($this->kode_lokasi);

		$copyObj->setKecamatan($this->kecamatan);

		$copyObj->setRekeningCodeAsli($this->rekening_code_asli);

		$copyObj->setNoteSkpd($this->note_skpd);

		$copyObj->setNotePeneliti($this->note_peneliti);

		$copyObj->setNilaiAnggaran($this->nilai_anggaran);

		$copyObj->setIsBlud($this->is_blud);

		$copyObj->setLokasiKecamatan($this->lokasi_kecamatan);

		$copyObj->setLokasiKelurahan($this->lokasi_kelurahan);

		$copyObj->setOb($this->ob);

		$copyObj->setObFromId($this->ob_from_id);

		$copyObj->setIsPerKomponen($this->is_per_komponen);

		$copyObj->setKegiatanCodeAsal($this->kegiatan_code_asal);

		$copyObj->setThKeMultiyears($this->th_ke_multiyears);

		$copyObj->setHargaSebelumSisaLelang($this->harga_sebelum_sisa_lelang);

		$copyObj->setIsMusrenbang($this->is_musrenbang);

		$copyObj->setSubIdAsal($this->sub_id_asal);

		$copyObj->setSubtitleAsal($this->subtitle_asal);

		$copyObj->setKodeSubAsal($this->kode_sub_asal);

		$copyObj->setSubAsal($this->sub_asal);

		$copyObj->setLastEditTime($this->last_edit_time);

		$copyObj->setIsPotongBpjs($this->is_potong_bpjs);

		$copyObj->setIsIuranBpjs($this->is_iuran_bpjs);

		$copyObj->setStatusOb($this->status_ob);

		$copyObj->setObParent($this->ob_parent);

		$copyObj->setObAlokasiBaru($this->ob_alokasi_baru);

		$copyObj->setIsHibah($this->is_hibah);

		$copyObj->setStatusLevel($this->status_level);

		$copyObj->setStatusLevelTolak($this->status_level_tolak);

		$copyObj->setStatusSisipan($this->status_sisipan);

		$copyObj->setIsTapdSetuju($this->is_tapd_setuju);

		$copyObj->setIsBappekoSetuju($this->is_bappeko_setuju);

		$copyObj->setAkrualCode($this->akrual_code);

		$copyObj->setTipe2($this->tipe2);

		$copyObj->setIsPenyeliaSetuju($this->is_penyelia_setuju);

		$copyObj->setNoteTapd($this->note_tapd);

		$copyObj->setNoteBappeko($this->note_bappeko);


		$copyObj->setNew(true);

		$copyObj->setKegiatanCode(NULL); 
		$copyObj->setTipe(NULL); 
		$copyObj->setDetailNo(NULL); 
		$copyObj->setUnitId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new MurniRincianDetailPeer();
		}
		return self::$peer;
	}

} 