<?php


abstract class BaseDpaImportbeda extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $unit_id;


	
	protected $kegiatan_code;


	
	protected $detail_no;


	
	protected $nama_kegiatan;


	
	protected $subtitle;


	
	protected $rekening_code;


	
	protected $rekening_name;


	
	protected $komponen_id;


	
	protected $komponen_name;


	
	protected $detail_name;


	
	protected $komponen_harga_awal;


	
	protected $keterangan_koefisien;


	
	protected $volume;


	
	protected $total;


	
	protected $dt_import;


	
	protected $satuan;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getUnitId()
	{

		return $this->unit_id;
	}

	
	public function getKegiatanCode()
	{

		return $this->kegiatan_code;
	}

	
	public function getDetailNo()
	{

		return $this->detail_no;
	}

	
	public function getNamaKegiatan()
	{

		return $this->nama_kegiatan;
	}

	
	public function getSubtitle()
	{

		return $this->subtitle;
	}

	
	public function getRekeningCode()
	{

		return $this->rekening_code;
	}

	
	public function getRekeningName()
	{

		return $this->rekening_name;
	}

	
	public function getKomponenId()
	{

		return $this->komponen_id;
	}

	
	public function getKomponenName()
	{

		return $this->komponen_name;
	}

	
	public function getDetailName()
	{

		return $this->detail_name;
	}

	
	public function getKomponenHargaAwal()
	{

		return $this->komponen_harga_awal;
	}

	
	public function getKeteranganKoefisien()
	{

		return $this->keterangan_koefisien;
	}

	
	public function getVolume()
	{

		return $this->volume;
	}

	
	public function getTotal()
	{

		return $this->total;
	}

	
	public function getDtImport($format = 'Y-m-d H:i:s')
	{

		if ($this->dt_import === null || $this->dt_import === '') {
			return null;
		} elseif (!is_int($this->dt_import)) {
						$ts = strtotime($this->dt_import);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [dt_import] as date/time value: " . var_export($this->dt_import, true));
			}
		} else {
			$ts = $this->dt_import;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getSatuan()
	{

		return $this->satuan;
	}

	
	public function setUnitId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->unit_id !== $v) {
			$this->unit_id = $v;
			$this->modifiedColumns[] = DpaImportbedaPeer::UNIT_ID;
		}

	} 
	
	public function setKegiatanCode($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kegiatan_code !== $v) {
			$this->kegiatan_code = $v;
			$this->modifiedColumns[] = DpaImportbedaPeer::KEGIATAN_CODE;
		}

	} 
	
	public function setDetailNo($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->detail_no !== $v) {
			$this->detail_no = $v;
			$this->modifiedColumns[] = DpaImportbedaPeer::DETAIL_NO;
		}

	} 
	
	public function setNamaKegiatan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nama_kegiatan !== $v) {
			$this->nama_kegiatan = $v;
			$this->modifiedColumns[] = DpaImportbedaPeer::NAMA_KEGIATAN;
		}

	} 
	
	public function setSubtitle($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->subtitle !== $v) {
			$this->subtitle = $v;
			$this->modifiedColumns[] = DpaImportbedaPeer::SUBTITLE;
		}

	} 
	
	public function setRekeningCode($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->rekening_code !== $v) {
			$this->rekening_code = $v;
			$this->modifiedColumns[] = DpaImportbedaPeer::REKENING_CODE;
		}

	} 
	
	public function setRekeningName($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->rekening_name !== $v) {
			$this->rekening_name = $v;
			$this->modifiedColumns[] = DpaImportbedaPeer::REKENING_NAME;
		}

	} 
	
	public function setKomponenId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->komponen_id !== $v) {
			$this->komponen_id = $v;
			$this->modifiedColumns[] = DpaImportbedaPeer::KOMPONEN_ID;
		}

	} 
	
	public function setKomponenName($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->komponen_name !== $v) {
			$this->komponen_name = $v;
			$this->modifiedColumns[] = DpaImportbedaPeer::KOMPONEN_NAME;
		}

	} 
	
	public function setDetailName($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->detail_name !== $v) {
			$this->detail_name = $v;
			$this->modifiedColumns[] = DpaImportbedaPeer::DETAIL_NAME;
		}

	} 
	
	public function setKomponenHargaAwal($v)
	{

		if ($this->komponen_harga_awal !== $v) {
			$this->komponen_harga_awal = $v;
			$this->modifiedColumns[] = DpaImportbedaPeer::KOMPONEN_HARGA_AWAL;
		}

	} 
	
	public function setKeteranganKoefisien($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->keterangan_koefisien !== $v) {
			$this->keterangan_koefisien = $v;
			$this->modifiedColumns[] = DpaImportbedaPeer::KETERANGAN_KOEFISIEN;
		}

	} 
	
	public function setVolume($v)
	{

		if ($this->volume !== $v) {
			$this->volume = $v;
			$this->modifiedColumns[] = DpaImportbedaPeer::VOLUME;
		}

	} 
	
	public function setTotal($v)
	{

		if ($this->total !== $v) {
			$this->total = $v;
			$this->modifiedColumns[] = DpaImportbedaPeer::TOTAL;
		}

	} 
	
	public function setDtImport($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [dt_import] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->dt_import !== $ts) {
			$this->dt_import = $ts;
			$this->modifiedColumns[] = DpaImportbedaPeer::DT_IMPORT;
		}

	} 
	
	public function setSatuan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->satuan !== $v) {
			$this->satuan = $v;
			$this->modifiedColumns[] = DpaImportbedaPeer::SATUAN;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->unit_id = $rs->getString($startcol + 0);

			$this->kegiatan_code = $rs->getString($startcol + 1);

			$this->detail_no = $rs->getInt($startcol + 2);

			$this->nama_kegiatan = $rs->getString($startcol + 3);

			$this->subtitle = $rs->getString($startcol + 4);

			$this->rekening_code = $rs->getString($startcol + 5);

			$this->rekening_name = $rs->getString($startcol + 6);

			$this->komponen_id = $rs->getString($startcol + 7);

			$this->komponen_name = $rs->getString($startcol + 8);

			$this->detail_name = $rs->getString($startcol + 9);

			$this->komponen_harga_awal = $rs->getFloat($startcol + 10);

			$this->keterangan_koefisien = $rs->getString($startcol + 11);

			$this->volume = $rs->getFloat($startcol + 12);

			$this->total = $rs->getFloat($startcol + 13);

			$this->dt_import = $rs->getTimestamp($startcol + 14, null);

			$this->satuan = $rs->getString($startcol + 15);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 16; 
		} catch (Exception $e) {
			throw new PropelException("Error populating DpaImportbeda object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(DpaImportbedaPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			DpaImportbedaPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(DpaImportbedaPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = DpaImportbedaPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += DpaImportbedaPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = DpaImportbedaPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = DpaImportbedaPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getUnitId();
				break;
			case 1:
				return $this->getKegiatanCode();
				break;
			case 2:
				return $this->getDetailNo();
				break;
			case 3:
				return $this->getNamaKegiatan();
				break;
			case 4:
				return $this->getSubtitle();
				break;
			case 5:
				return $this->getRekeningCode();
				break;
			case 6:
				return $this->getRekeningName();
				break;
			case 7:
				return $this->getKomponenId();
				break;
			case 8:
				return $this->getKomponenName();
				break;
			case 9:
				return $this->getDetailName();
				break;
			case 10:
				return $this->getKomponenHargaAwal();
				break;
			case 11:
				return $this->getKeteranganKoefisien();
				break;
			case 12:
				return $this->getVolume();
				break;
			case 13:
				return $this->getTotal();
				break;
			case 14:
				return $this->getDtImport();
				break;
			case 15:
				return $this->getSatuan();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = DpaImportbedaPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getUnitId(),
			$keys[1] => $this->getKegiatanCode(),
			$keys[2] => $this->getDetailNo(),
			$keys[3] => $this->getNamaKegiatan(),
			$keys[4] => $this->getSubtitle(),
			$keys[5] => $this->getRekeningCode(),
			$keys[6] => $this->getRekeningName(),
			$keys[7] => $this->getKomponenId(),
			$keys[8] => $this->getKomponenName(),
			$keys[9] => $this->getDetailName(),
			$keys[10] => $this->getKomponenHargaAwal(),
			$keys[11] => $this->getKeteranganKoefisien(),
			$keys[12] => $this->getVolume(),
			$keys[13] => $this->getTotal(),
			$keys[14] => $this->getDtImport(),
			$keys[15] => $this->getSatuan(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = DpaImportbedaPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setUnitId($value);
				break;
			case 1:
				$this->setKegiatanCode($value);
				break;
			case 2:
				$this->setDetailNo($value);
				break;
			case 3:
				$this->setNamaKegiatan($value);
				break;
			case 4:
				$this->setSubtitle($value);
				break;
			case 5:
				$this->setRekeningCode($value);
				break;
			case 6:
				$this->setRekeningName($value);
				break;
			case 7:
				$this->setKomponenId($value);
				break;
			case 8:
				$this->setKomponenName($value);
				break;
			case 9:
				$this->setDetailName($value);
				break;
			case 10:
				$this->setKomponenHargaAwal($value);
				break;
			case 11:
				$this->setKeteranganKoefisien($value);
				break;
			case 12:
				$this->setVolume($value);
				break;
			case 13:
				$this->setTotal($value);
				break;
			case 14:
				$this->setDtImport($value);
				break;
			case 15:
				$this->setSatuan($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = DpaImportbedaPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setUnitId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setKegiatanCode($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setDetailNo($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setNamaKegiatan($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setSubtitle($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setRekeningCode($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setRekeningName($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setKomponenId($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setKomponenName($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setDetailName($arr[$keys[9]]);
		if (array_key_exists($keys[10], $arr)) $this->setKomponenHargaAwal($arr[$keys[10]]);
		if (array_key_exists($keys[11], $arr)) $this->setKeteranganKoefisien($arr[$keys[11]]);
		if (array_key_exists($keys[12], $arr)) $this->setVolume($arr[$keys[12]]);
		if (array_key_exists($keys[13], $arr)) $this->setTotal($arr[$keys[13]]);
		if (array_key_exists($keys[14], $arr)) $this->setDtImport($arr[$keys[14]]);
		if (array_key_exists($keys[15], $arr)) $this->setSatuan($arr[$keys[15]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(DpaImportbedaPeer::DATABASE_NAME);

		if ($this->isColumnModified(DpaImportbedaPeer::UNIT_ID)) $criteria->add(DpaImportbedaPeer::UNIT_ID, $this->unit_id);
		if ($this->isColumnModified(DpaImportbedaPeer::KEGIATAN_CODE)) $criteria->add(DpaImportbedaPeer::KEGIATAN_CODE, $this->kegiatan_code);
		if ($this->isColumnModified(DpaImportbedaPeer::DETAIL_NO)) $criteria->add(DpaImportbedaPeer::DETAIL_NO, $this->detail_no);
		if ($this->isColumnModified(DpaImportbedaPeer::NAMA_KEGIATAN)) $criteria->add(DpaImportbedaPeer::NAMA_KEGIATAN, $this->nama_kegiatan);
		if ($this->isColumnModified(DpaImportbedaPeer::SUBTITLE)) $criteria->add(DpaImportbedaPeer::SUBTITLE, $this->subtitle);
		if ($this->isColumnModified(DpaImportbedaPeer::REKENING_CODE)) $criteria->add(DpaImportbedaPeer::REKENING_CODE, $this->rekening_code);
		if ($this->isColumnModified(DpaImportbedaPeer::REKENING_NAME)) $criteria->add(DpaImportbedaPeer::REKENING_NAME, $this->rekening_name);
		if ($this->isColumnModified(DpaImportbedaPeer::KOMPONEN_ID)) $criteria->add(DpaImportbedaPeer::KOMPONEN_ID, $this->komponen_id);
		if ($this->isColumnModified(DpaImportbedaPeer::KOMPONEN_NAME)) $criteria->add(DpaImportbedaPeer::KOMPONEN_NAME, $this->komponen_name);
		if ($this->isColumnModified(DpaImportbedaPeer::DETAIL_NAME)) $criteria->add(DpaImportbedaPeer::DETAIL_NAME, $this->detail_name);
		if ($this->isColumnModified(DpaImportbedaPeer::KOMPONEN_HARGA_AWAL)) $criteria->add(DpaImportbedaPeer::KOMPONEN_HARGA_AWAL, $this->komponen_harga_awal);
		if ($this->isColumnModified(DpaImportbedaPeer::KETERANGAN_KOEFISIEN)) $criteria->add(DpaImportbedaPeer::KETERANGAN_KOEFISIEN, $this->keterangan_koefisien);
		if ($this->isColumnModified(DpaImportbedaPeer::VOLUME)) $criteria->add(DpaImportbedaPeer::VOLUME, $this->volume);
		if ($this->isColumnModified(DpaImportbedaPeer::TOTAL)) $criteria->add(DpaImportbedaPeer::TOTAL, $this->total);
		if ($this->isColumnModified(DpaImportbedaPeer::DT_IMPORT)) $criteria->add(DpaImportbedaPeer::DT_IMPORT, $this->dt_import);
		if ($this->isColumnModified(DpaImportbedaPeer::SATUAN)) $criteria->add(DpaImportbedaPeer::SATUAN, $this->satuan);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(DpaImportbedaPeer::DATABASE_NAME);

		$criteria->add(DpaImportbedaPeer::UNIT_ID, $this->unit_id);
		$criteria->add(DpaImportbedaPeer::KEGIATAN_CODE, $this->kegiatan_code);
		$criteria->add(DpaImportbedaPeer::DETAIL_NO, $this->detail_no);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		$pks = array();

		$pks[0] = $this->getUnitId();

		$pks[1] = $this->getKegiatanCode();

		$pks[2] = $this->getDetailNo();

		return $pks;
	}

	
	public function setPrimaryKey($keys)
	{

		$this->setUnitId($keys[0]);

		$this->setKegiatanCode($keys[1]);

		$this->setDetailNo($keys[2]);

	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setNamaKegiatan($this->nama_kegiatan);

		$copyObj->setSubtitle($this->subtitle);

		$copyObj->setRekeningCode($this->rekening_code);

		$copyObj->setRekeningName($this->rekening_name);

		$copyObj->setKomponenId($this->komponen_id);

		$copyObj->setKomponenName($this->komponen_name);

		$copyObj->setDetailName($this->detail_name);

		$copyObj->setKomponenHargaAwal($this->komponen_harga_awal);

		$copyObj->setKeteranganKoefisien($this->keterangan_koefisien);

		$copyObj->setVolume($this->volume);

		$copyObj->setTotal($this->total);

		$copyObj->setDtImport($this->dt_import);

		$copyObj->setSatuan($this->satuan);


		$copyObj->setNew(true);

		$copyObj->setUnitId(NULL); 
		$copyObj->setKegiatanCode(NULL); 
		$copyObj->setDetailNo(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new DpaImportbedaPeer();
		}
		return self::$peer;
	}

} 