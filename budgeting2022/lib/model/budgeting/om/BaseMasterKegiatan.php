<?php


abstract class BaseMasterKegiatan extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $unit_id;


	
	protected $kode_kegiatan;


	
	protected $kode_bidang;


	
	protected $kode_urusan_wajib;


	
	protected $kode_program;


	
	protected $kode_sasaran;


	
	protected $kode_indikator;


	
	protected $alokasi_dana;


	
	protected $nama_kegiatan;


	
	protected $masukan;


	
	protected $output;


	
	protected $outcome;


	
	protected $benefit;


	
	protected $impact;


	
	protected $tipe;


	
	protected $kegiatan_active = false;


	
	protected $to_kegiatan_code;


	
	protected $catatan;


	
	protected $target_outcome;


	
	protected $lokasi;


	
	protected $jumlah_prev;


	
	protected $jumlah_now;


	
	protected $jumlah_next;


	
	protected $kode_program2;


	
	protected $kode_urusan;


	
	protected $last_update_user;


	
	protected $last_update_time;


	
	protected $last_update_ip;


	
	protected $tahap;


	
	protected $kode_misi;


	
	protected $kode_tujuan;


	
	protected $ranking;


	
	protected $nomor13;


	
	protected $ppa_nama;


	
	protected $ppa_pangkat;


	
	protected $ppa_nip;


	
	protected $lanjutan;


	
	protected $user_id;


	
	protected $id;


	
	protected $tahun;


	
	protected $tambahan_pagu;


	
	protected $gender = false;


	
	protected $kode_keg_keuangan;


	
	protected $user_id_lama;


	
	protected $indikator;


	
	protected $is_dak;


	
	protected $kode_kegiatan_asal;


	
	protected $kode_keg_keuangan_asal;


	
	protected $th_ke_multiyears;


	
	protected $kelompok_sasaran;


	
	protected $pagu_bappeko;


	
	protected $kode_dpa;


	
	protected $user_id_pptk;


	
	protected $user_id_kpa;


	
	protected $catatan_pembahasan;


	
	protected $is_tapd_setuju = false;


	
	protected $is_bappeko_setuju = false;


	
	protected $is_penyelia_setuju = false;


	
	protected $is_pernah_rka = false;


	
	protected $kode_kegiatan_baru;


	
	protected $catatan_bpkpd;


	
	protected $ubah_f1_dinas;


	
	protected $ubah_f1_peneliti;


	
	protected $sisa_lelang_dinas;


	
	protected $sisa_lelang_peneliti;


	
	protected $catatan_ubah_f1_dinas;


	
	protected $catatan_sisa_lelang_peneliti;


	
	protected $pptk_approval;


	
	protected $kpa_approval;


	
	protected $catatan_bagian_hukum;


	
	protected $catatan_inspektorat;


	
	protected $catatan_badan_kepegawaian;


	
	protected $catatan_lppa;


	
	protected $is_bagian_hukum_setuju = false;


	
	protected $is_inspektorat_setuju = false;


	
	protected $is_badan_kepegawaian_setuju = false;


	
	protected $is_lppa_setuju = false;


	
	protected $verifikasi_bpkpd;


	
	protected $verifikasi_bappeko;


	
	protected $verifikasi_penyelia;


	
	protected $verifikasi_bagian_hukum;


	
	protected $verifikasi_inspektorat;


	
	protected $verifikasi_badan_kepegawaian;


	
	protected $verifikasi_lppa;


	
	protected $metode_count;


	
	protected $catatan_bagian_organisasi;


	
	protected $is_bagian_organisasi_setuju = false;


	
	protected $verifikasi_bagian_organisasi;


	
	protected $catatan_asisten1;


	
	protected $is_asisten1_setuju = false;


	
	protected $verifikasi_asisten1;


	
	protected $catatan_asisten2;


	
	protected $is_asisten2_setuju = false;


	
	protected $verifikasi_asisten2;


	
	protected $catatan_asisten3;


	
	protected $is_asisten3_setuju = false;


	
	protected $verifikasi_asisten3;


	
	protected $catatan_sekda;


	
	protected $is_sekda_setuju = false;


	
	protected $verifikasi_sekda;


	
	protected $verifikasi_asisten;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getUnitId()
	{

		return $this->unit_id;
	}

	
	public function getKodeKegiatan()
	{

		return $this->kode_kegiatan;
	}

	
	public function getKodeBidang()
	{

		return $this->kode_bidang;
	}

	
	public function getKodeUrusanWajib()
	{

		return $this->kode_urusan_wajib;
	}

	
	public function getKodeProgram()
	{

		return $this->kode_program;
	}

	
	public function getKodeSasaran()
	{

		return $this->kode_sasaran;
	}

	
	public function getKodeIndikator()
	{

		return $this->kode_indikator;
	}

	
	public function getAlokasiDana()
	{

		return $this->alokasi_dana;
	}

	
	public function getNamaKegiatan()
	{

		return $this->nama_kegiatan;
	}

	
	public function getMasukan()
	{

		return $this->masukan;
	}

	
	public function getOutput()
	{

		return $this->output;
	}

	
	public function getOutcome()
	{

		return $this->outcome;
	}

	
	public function getBenefit()
	{

		return $this->benefit;
	}

	
	public function getImpact()
	{

		return $this->impact;
	}

	
	public function getTipe()
	{

		return $this->tipe;
	}

	
	public function getKegiatanActive()
	{

		return $this->kegiatan_active;
	}

	
	public function getToKegiatanCode()
	{

		return $this->to_kegiatan_code;
	}

	
	public function getCatatan()
	{

		return $this->catatan;
	}

	
	public function getTargetOutcome()
	{

		return $this->target_outcome;
	}

	
	public function getLokasi()
	{

		return $this->lokasi;
	}

	
	public function getJumlahPrev()
	{

		return $this->jumlah_prev;
	}

	
	public function getJumlahNow()
	{

		return $this->jumlah_now;
	}

	
	public function getJumlahNext()
	{

		return $this->jumlah_next;
	}

	
	public function getKodeProgram2()
	{

		return $this->kode_program2;
	}

	
	public function getKodeUrusan()
	{

		return $this->kode_urusan;
	}

	
	public function getLastUpdateUser()
	{

		return $this->last_update_user;
	}

	
	public function getLastUpdateTime($format = 'Y-m-d H:i:s')
	{

		if ($this->last_update_time === null || $this->last_update_time === '') {
			return null;
		} elseif (!is_int($this->last_update_time)) {
						$ts = strtotime($this->last_update_time);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [last_update_time] as date/time value: " . var_export($this->last_update_time, true));
			}
		} else {
			$ts = $this->last_update_time;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getLastUpdateIp()
	{

		return $this->last_update_ip;
	}

	
	public function getTahap()
	{

		return $this->tahap;
	}

	
	public function getKodeMisi()
	{

		return $this->kode_misi;
	}

	
	public function getKodeTujuan()
	{

		return $this->kode_tujuan;
	}

	
	public function getRanking()
	{

		return $this->ranking;
	}

	
	public function getNomor13()
	{

		return $this->nomor13;
	}

	
	public function getPpaNama()
	{

		return $this->ppa_nama;
	}

	
	public function getPpaPangkat()
	{

		return $this->ppa_pangkat;
	}

	
	public function getPpaNip()
	{

		return $this->ppa_nip;
	}

	
	public function getLanjutan()
	{

		return $this->lanjutan;
	}

	
	public function getUserId()
	{

		return $this->user_id;
	}

	
	public function getId()
	{

		return $this->id;
	}

	
	public function getTahun()
	{

		return $this->tahun;
	}

	
	public function getTambahanPagu()
	{

		return $this->tambahan_pagu;
	}

	
	public function getGender()
	{

		return $this->gender;
	}

	
	public function getKodeKegKeuangan()
	{

		return $this->kode_keg_keuangan;
	}

	
	public function getUserIdLama()
	{

		return $this->user_id_lama;
	}

	
	public function getIndikator()
	{

		return $this->indikator;
	}

	
	public function getIsDak()
	{

		return $this->is_dak;
	}

	
	public function getKodeKegiatanAsal()
	{

		return $this->kode_kegiatan_asal;
	}

	
	public function getKodeKegKeuanganAsal()
	{

		return $this->kode_keg_keuangan_asal;
	}

	
	public function getThKeMultiyears()
	{

		return $this->th_ke_multiyears;
	}

	
	public function getKelompokSasaran()
	{

		return $this->kelompok_sasaran;
	}

	
	public function getPaguBappeko()
	{

		return $this->pagu_bappeko;
	}

	
	public function getKodeDpa()
	{

		return $this->kode_dpa;
	}

	
	public function getUserIdPptk()
	{

		return $this->user_id_pptk;
	}

	
	public function getUserIdKpa()
	{

		return $this->user_id_kpa;
	}

	
	public function getCatatanPembahasan()
	{

		return $this->catatan_pembahasan;
	}

	
	public function getIsTapdSetuju()
	{

		return $this->is_tapd_setuju;
	}

	
	public function getIsBappekoSetuju()
	{

		return $this->is_bappeko_setuju;
	}

	
	public function getIsPenyeliaSetuju()
	{

		return $this->is_penyelia_setuju;
	}

	
	public function getIsPernahRka()
	{

		return $this->is_pernah_rka;
	}

	
	public function getKodeKegiatanBaru()
	{

		return $this->kode_kegiatan_baru;
	}

	
	public function getCatatanBpkpd()
	{

		return $this->catatan_bpkpd;
	}

	
	public function getUbahF1Dinas()
	{

		return $this->ubah_f1_dinas;
	}

	
	public function getUbahF1Peneliti()
	{

		return $this->ubah_f1_peneliti;
	}

	
	public function getSisaLelangDinas()
	{

		return $this->sisa_lelang_dinas;
	}

	
	public function getSisaLelangPeneliti()
	{

		return $this->sisa_lelang_peneliti;
	}

	
	public function getCatatanUbahF1Dinas()
	{

		return $this->catatan_ubah_f1_dinas;
	}

	
	public function getCatatanSisaLelangPeneliti()
	{

		return $this->catatan_sisa_lelang_peneliti;
	}

	
	public function getPptkApproval()
	{

		return $this->pptk_approval;
	}

	
	public function getKpaApproval()
	{

		return $this->kpa_approval;
	}

	
	public function getCatatanBagianHukum()
	{

		return $this->catatan_bagian_hukum;
	}

	
	public function getCatatanInspektorat()
	{

		return $this->catatan_inspektorat;
	}

	
	public function getCatatanBadanKepegawaian()
	{

		return $this->catatan_badan_kepegawaian;
	}

	
	public function getCatatanLppa()
	{

		return $this->catatan_lppa;
	}

	
	public function getIsBagianHukumSetuju()
	{

		return $this->is_bagian_hukum_setuju;
	}

	
	public function getIsInspektoratSetuju()
	{

		return $this->is_inspektorat_setuju;
	}

	
	public function getIsBadanKepegawaianSetuju()
	{

		return $this->is_badan_kepegawaian_setuju;
	}

	
	public function getIsLppaSetuju()
	{

		return $this->is_lppa_setuju;
	}

	
	public function getVerifikasiBpkpd()
	{

		return $this->verifikasi_bpkpd;
	}

	
	public function getVerifikasiBappeko()
	{

		return $this->verifikasi_bappeko;
	}

	
	public function getVerifikasiPenyelia()
	{

		return $this->verifikasi_penyelia;
	}

	
	public function getVerifikasiBagianHukum()
	{

		return $this->verifikasi_bagian_hukum;
	}

	
	public function getVerifikasiInspektorat()
	{

		return $this->verifikasi_inspektorat;
	}

	
	public function getVerifikasiBadanKepegawaian()
	{

		return $this->verifikasi_badan_kepegawaian;
	}

	
	public function getVerifikasiLppa()
	{

		return $this->verifikasi_lppa;
	}

	
	public function getMetodeCount()
	{

		return $this->metode_count;
	}

	
	public function getCatatanBagianOrganisasi()
	{

		return $this->catatan_bagian_organisasi;
	}

	
	public function getIsBagianOrganisasiSetuju()
	{

		return $this->is_bagian_organisasi_setuju;
	}

	
	public function getVerifikasiBagianOrganisasi()
	{

		return $this->verifikasi_bagian_organisasi;
	}

	
	public function getCatatanAsisten1()
	{

		return $this->catatan_asisten1;
	}

	
	public function getIsAsisten1Setuju()
	{

		return $this->is_asisten1_setuju;
	}

	
	public function getVerifikasiAsisten1()
	{

		return $this->verifikasi_asisten1;
	}

	
	public function getCatatanAsisten2()
	{

		return $this->catatan_asisten2;
	}

	
	public function getIsAsisten2Setuju()
	{

		return $this->is_asisten2_setuju;
	}

	
	public function getVerifikasiAsisten2()
	{

		return $this->verifikasi_asisten2;
	}

	
	public function getCatatanAsisten3()
	{

		return $this->catatan_asisten3;
	}

	
	public function getIsAsisten3Setuju()
	{

		return $this->is_asisten3_setuju;
	}

	
	public function getVerifikasiAsisten3()
	{

		return $this->verifikasi_asisten3;
	}

	
	public function getCatatanSekda()
	{

		return $this->catatan_sekda;
	}

	
	public function getIsSekdaSetuju()
	{

		return $this->is_sekda_setuju;
	}

	
	public function getVerifikasiSekda()
	{

		return $this->verifikasi_sekda;
	}

	
	public function getVerifikasiAsisten()
	{

		return $this->verifikasi_asisten;
	}

	
	public function setUnitId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->unit_id !== $v) {
			$this->unit_id = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::UNIT_ID;
		}

	} 
	
	public function setKodeKegiatan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_kegiatan !== $v) {
			$this->kode_kegiatan = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::KODE_KEGIATAN;
		}

	} 
	
	public function setKodeBidang($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_bidang !== $v) {
			$this->kode_bidang = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::KODE_BIDANG;
		}

	} 
	
	public function setKodeUrusanWajib($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_urusan_wajib !== $v) {
			$this->kode_urusan_wajib = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::KODE_URUSAN_WAJIB;
		}

	} 
	
	public function setKodeProgram($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_program !== $v) {
			$this->kode_program = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::KODE_PROGRAM;
		}

	} 
	
	public function setKodeSasaran($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_sasaran !== $v) {
			$this->kode_sasaran = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::KODE_SASARAN;
		}

	} 
	
	public function setKodeIndikator($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_indikator !== $v) {
			$this->kode_indikator = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::KODE_INDIKATOR;
		}

	} 
	
	public function setAlokasiDana($v)
	{

		if ($this->alokasi_dana !== $v) {
			$this->alokasi_dana = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::ALOKASI_DANA;
		}

	} 
	
	public function setNamaKegiatan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nama_kegiatan !== $v) {
			$this->nama_kegiatan = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::NAMA_KEGIATAN;
		}

	} 
	
	public function setMasukan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->masukan !== $v) {
			$this->masukan = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::MASUKAN;
		}

	} 
	
	public function setOutput($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->output !== $v) {
			$this->output = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::OUTPUT;
		}

	} 
	
	public function setOutcome($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->outcome !== $v) {
			$this->outcome = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::OUTCOME;
		}

	} 
	
	public function setBenefit($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->benefit !== $v) {
			$this->benefit = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::BENEFIT;
		}

	} 
	
	public function setImpact($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->impact !== $v) {
			$this->impact = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::IMPACT;
		}

	} 
	
	public function setTipe($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->tipe !== $v) {
			$this->tipe = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::TIPE;
		}

	} 
	
	public function setKegiatanActive($v)
	{

		if ($this->kegiatan_active !== $v || $v === false) {
			$this->kegiatan_active = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::KEGIATAN_ACTIVE;
		}

	} 
	
	public function setToKegiatanCode($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->to_kegiatan_code !== $v) {
			$this->to_kegiatan_code = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::TO_KEGIATAN_CODE;
		}

	} 
	
	public function setCatatan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->catatan !== $v) {
			$this->catatan = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::CATATAN;
		}

	} 
	
	public function setTargetOutcome($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->target_outcome !== $v) {
			$this->target_outcome = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::TARGET_OUTCOME;
		}

	} 
	
	public function setLokasi($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->lokasi !== $v) {
			$this->lokasi = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::LOKASI;
		}

	} 
	
	public function setJumlahPrev($v)
	{

		if ($this->jumlah_prev !== $v) {
			$this->jumlah_prev = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::JUMLAH_PREV;
		}

	} 
	
	public function setJumlahNow($v)
	{

		if ($this->jumlah_now !== $v) {
			$this->jumlah_now = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::JUMLAH_NOW;
		}

	} 
	
	public function setJumlahNext($v)
	{

		if ($this->jumlah_next !== $v) {
			$this->jumlah_next = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::JUMLAH_NEXT;
		}

	} 
	
	public function setKodeProgram2($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_program2 !== $v) {
			$this->kode_program2 = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::KODE_PROGRAM2;
		}

	} 
	
	public function setKodeUrusan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_urusan !== $v) {
			$this->kode_urusan = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::KODE_URUSAN;
		}

	} 
	
	public function setLastUpdateUser($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->last_update_user !== $v) {
			$this->last_update_user = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::LAST_UPDATE_USER;
		}

	} 
	
	public function setLastUpdateTime($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [last_update_time] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->last_update_time !== $ts) {
			$this->last_update_time = $ts;
			$this->modifiedColumns[] = MasterKegiatanPeer::LAST_UPDATE_TIME;
		}

	} 
	
	public function setLastUpdateIp($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->last_update_ip !== $v) {
			$this->last_update_ip = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::LAST_UPDATE_IP;
		}

	} 
	
	public function setTahap($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->tahap !== $v) {
			$this->tahap = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::TAHAP;
		}

	} 
	
	public function setKodeMisi($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_misi !== $v) {
			$this->kode_misi = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::KODE_MISI;
		}

	} 
	
	public function setKodeTujuan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_tujuan !== $v) {
			$this->kode_tujuan = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::KODE_TUJUAN;
		}

	} 
	
	public function setRanking($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->ranking !== $v) {
			$this->ranking = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::RANKING;
		}

	} 
	
	public function setNomor13($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nomor13 !== $v) {
			$this->nomor13 = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::NOMOR13;
		}

	} 
	
	public function setPpaNama($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->ppa_nama !== $v) {
			$this->ppa_nama = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::PPA_NAMA;
		}

	} 
	
	public function setPpaPangkat($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->ppa_pangkat !== $v) {
			$this->ppa_pangkat = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::PPA_PANGKAT;
		}

	} 
	
	public function setPpaNip($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->ppa_nip !== $v) {
			$this->ppa_nip = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::PPA_NIP;
		}

	} 
	
	public function setLanjutan($v)
	{

		if ($this->lanjutan !== $v) {
			$this->lanjutan = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::LANJUTAN;
		}

	} 
	
	public function setUserId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->user_id !== $v) {
			$this->user_id = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::USER_ID;
		}

	} 
	
	public function setId($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::ID;
		}

	} 
	
	public function setTahun($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->tahun !== $v) {
			$this->tahun = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::TAHUN;
		}

	} 
	
	public function setTambahanPagu($v)
	{

		if ($this->tambahan_pagu !== $v) {
			$this->tambahan_pagu = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::TAMBAHAN_PAGU;
		}

	} 
	
	public function setGender($v)
	{

		if ($this->gender !== $v || $v === false) {
			$this->gender = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::GENDER;
		}

	} 
	
	public function setKodeKegKeuangan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_keg_keuangan !== $v) {
			$this->kode_keg_keuangan = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::KODE_KEG_KEUANGAN;
		}

	} 
	
	public function setUserIdLama($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->user_id_lama !== $v) {
			$this->user_id_lama = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::USER_ID_LAMA;
		}

	} 
	
	public function setIndikator($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->indikator !== $v) {
			$this->indikator = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::INDIKATOR;
		}

	} 
	
	public function setIsDak($v)
	{

		if ($this->is_dak !== $v) {
			$this->is_dak = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::IS_DAK;
		}

	} 
	
	public function setKodeKegiatanAsal($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_kegiatan_asal !== $v) {
			$this->kode_kegiatan_asal = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::KODE_KEGIATAN_ASAL;
		}

	} 
	
	public function setKodeKegKeuanganAsal($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_keg_keuangan_asal !== $v) {
			$this->kode_keg_keuangan_asal = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::KODE_KEG_KEUANGAN_ASAL;
		}

	} 
	
	public function setThKeMultiyears($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->th_ke_multiyears !== $v) {
			$this->th_ke_multiyears = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::TH_KE_MULTIYEARS;
		}

	} 
	
	public function setKelompokSasaran($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kelompok_sasaran !== $v) {
			$this->kelompok_sasaran = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::KELOMPOK_SASARAN;
		}

	} 
	
	public function setPaguBappeko($v)
	{

		if ($this->pagu_bappeko !== $v) {
			$this->pagu_bappeko = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::PAGU_BAPPEKO;
		}

	} 
	
	public function setKodeDpa($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_dpa !== $v) {
			$this->kode_dpa = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::KODE_DPA;
		}

	} 
	
	public function setUserIdPptk($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->user_id_pptk !== $v) {
			$this->user_id_pptk = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::USER_ID_PPTK;
		}

	} 
	
	public function setUserIdKpa($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->user_id_kpa !== $v) {
			$this->user_id_kpa = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::USER_ID_KPA;
		}

	} 
	
	public function setCatatanPembahasan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->catatan_pembahasan !== $v) {
			$this->catatan_pembahasan = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::CATATAN_PEMBAHASAN;
		}

	} 
	
	public function setIsTapdSetuju($v)
	{

		if ($this->is_tapd_setuju !== $v || $v === false) {
			$this->is_tapd_setuju = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::IS_TAPD_SETUJU;
		}

	} 
	
	public function setIsBappekoSetuju($v)
	{

		if ($this->is_bappeko_setuju !== $v || $v === false) {
			$this->is_bappeko_setuju = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::IS_BAPPEKO_SETUJU;
		}

	} 
	
	public function setIsPenyeliaSetuju($v)
	{

		if ($this->is_penyelia_setuju !== $v || $v === false) {
			$this->is_penyelia_setuju = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::IS_PENYELIA_SETUJU;
		}

	} 
	
	public function setIsPernahRka($v)
	{

		if ($this->is_pernah_rka !== $v || $v === false) {
			$this->is_pernah_rka = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::IS_PERNAH_RKA;
		}

	} 
	
	public function setKodeKegiatanBaru($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_kegiatan_baru !== $v) {
			$this->kode_kegiatan_baru = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::KODE_KEGIATAN_BARU;
		}

	} 
	
	public function setCatatanBpkpd($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->catatan_bpkpd !== $v) {
			$this->catatan_bpkpd = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::CATATAN_BPKPD;
		}

	} 
	
	public function setUbahF1Dinas($v)
	{

		if ($this->ubah_f1_dinas !== $v) {
			$this->ubah_f1_dinas = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::UBAH_F1_DINAS;
		}

	} 
	
	public function setUbahF1Peneliti($v)
	{

		if ($this->ubah_f1_peneliti !== $v) {
			$this->ubah_f1_peneliti = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::UBAH_F1_PENELITI;
		}

	} 
	
	public function setSisaLelangDinas($v)
	{

		if ($this->sisa_lelang_dinas !== $v) {
			$this->sisa_lelang_dinas = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::SISA_LELANG_DINAS;
		}

	} 
	
	public function setSisaLelangPeneliti($v)
	{

		if ($this->sisa_lelang_peneliti !== $v) {
			$this->sisa_lelang_peneliti = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::SISA_LELANG_PENELITI;
		}

	} 
	
	public function setCatatanUbahF1Dinas($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->catatan_ubah_f1_dinas !== $v) {
			$this->catatan_ubah_f1_dinas = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::CATATAN_UBAH_F1_DINAS;
		}

	} 
	
	public function setCatatanSisaLelangPeneliti($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->catatan_sisa_lelang_peneliti !== $v) {
			$this->catatan_sisa_lelang_peneliti = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::CATATAN_SISA_LELANG_PENELITI;
		}

	} 
	
	public function setPptkApproval($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->pptk_approval !== $v) {
			$this->pptk_approval = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::PPTK_APPROVAL;
		}

	} 
	
	public function setKpaApproval($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kpa_approval !== $v) {
			$this->kpa_approval = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::KPA_APPROVAL;
		}

	} 
	
	public function setCatatanBagianHukum($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->catatan_bagian_hukum !== $v) {
			$this->catatan_bagian_hukum = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::CATATAN_BAGIAN_HUKUM;
		}

	} 
	
	public function setCatatanInspektorat($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->catatan_inspektorat !== $v) {
			$this->catatan_inspektorat = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::CATATAN_INSPEKTORAT;
		}

	} 
	
	public function setCatatanBadanKepegawaian($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->catatan_badan_kepegawaian !== $v) {
			$this->catatan_badan_kepegawaian = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::CATATAN_BADAN_KEPEGAWAIAN;
		}

	} 
	
	public function setCatatanLppa($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->catatan_lppa !== $v) {
			$this->catatan_lppa = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::CATATAN_LPPA;
		}

	} 
	
	public function setIsBagianHukumSetuju($v)
	{

		if ($this->is_bagian_hukum_setuju !== $v || $v === false) {
			$this->is_bagian_hukum_setuju = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::IS_BAGIAN_HUKUM_SETUJU;
		}

	} 
	
	public function setIsInspektoratSetuju($v)
	{

		if ($this->is_inspektorat_setuju !== $v || $v === false) {
			$this->is_inspektorat_setuju = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::IS_INSPEKTORAT_SETUJU;
		}

	} 
	
	public function setIsBadanKepegawaianSetuju($v)
	{

		if ($this->is_badan_kepegawaian_setuju !== $v || $v === false) {
			$this->is_badan_kepegawaian_setuju = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::IS_BADAN_KEPEGAWAIAN_SETUJU;
		}

	} 
	
	public function setIsLppaSetuju($v)
	{

		if ($this->is_lppa_setuju !== $v || $v === false) {
			$this->is_lppa_setuju = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::IS_LPPA_SETUJU;
		}

	} 
	
	public function setVerifikasiBpkpd($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->verifikasi_bpkpd !== $v) {
			$this->verifikasi_bpkpd = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::VERIFIKASI_BPKPD;
		}

	} 
	
	public function setVerifikasiBappeko($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->verifikasi_bappeko !== $v) {
			$this->verifikasi_bappeko = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::VERIFIKASI_BAPPEKO;
		}

	} 
	
	public function setVerifikasiPenyelia($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->verifikasi_penyelia !== $v) {
			$this->verifikasi_penyelia = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::VERIFIKASI_PENYELIA;
		}

	} 
	
	public function setVerifikasiBagianHukum($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->verifikasi_bagian_hukum !== $v) {
			$this->verifikasi_bagian_hukum = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::VERIFIKASI_BAGIAN_HUKUM;
		}

	} 
	
	public function setVerifikasiInspektorat($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->verifikasi_inspektorat !== $v) {
			$this->verifikasi_inspektorat = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::VERIFIKASI_INSPEKTORAT;
		}

	} 
	
	public function setVerifikasiBadanKepegawaian($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->verifikasi_badan_kepegawaian !== $v) {
			$this->verifikasi_badan_kepegawaian = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::VERIFIKASI_BADAN_KEPEGAWAIAN;
		}

	} 
	
	public function setVerifikasiLppa($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->verifikasi_lppa !== $v) {
			$this->verifikasi_lppa = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::VERIFIKASI_LPPA;
		}

	} 
	
	public function setMetodeCount($v)
	{

		if ($this->metode_count !== $v) {
			$this->metode_count = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::METODE_COUNT;
		}

	} 
	
	public function setCatatanBagianOrganisasi($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->catatan_bagian_organisasi !== $v) {
			$this->catatan_bagian_organisasi = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::CATATAN_BAGIAN_ORGANISASI;
		}

	} 
	
	public function setIsBagianOrganisasiSetuju($v)
	{

		if ($this->is_bagian_organisasi_setuju !== $v || $v === false) {
			$this->is_bagian_organisasi_setuju = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::IS_BAGIAN_ORGANISASI_SETUJU;
		}

	} 
	
	public function setVerifikasiBagianOrganisasi($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->verifikasi_bagian_organisasi !== $v) {
			$this->verifikasi_bagian_organisasi = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::VERIFIKASI_BAGIAN_ORGANISASI;
		}

	} 
	
	public function setCatatanAsisten1($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->catatan_asisten1 !== $v) {
			$this->catatan_asisten1 = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::CATATAN_ASISTEN1;
		}

	} 
	
	public function setIsAsisten1Setuju($v)
	{

		if ($this->is_asisten1_setuju !== $v || $v === false) {
			$this->is_asisten1_setuju = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::IS_ASISTEN1_SETUJU;
		}

	} 
	
	public function setVerifikasiAsisten1($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->verifikasi_asisten1 !== $v) {
			$this->verifikasi_asisten1 = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::VERIFIKASI_ASISTEN1;
		}

	} 
	
	public function setCatatanAsisten2($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->catatan_asisten2 !== $v) {
			$this->catatan_asisten2 = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::CATATAN_ASISTEN2;
		}

	} 
	
	public function setIsAsisten2Setuju($v)
	{

		if ($this->is_asisten2_setuju !== $v || $v === false) {
			$this->is_asisten2_setuju = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::IS_ASISTEN2_SETUJU;
		}

	} 
	
	public function setVerifikasiAsisten2($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->verifikasi_asisten2 !== $v) {
			$this->verifikasi_asisten2 = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::VERIFIKASI_ASISTEN2;
		}

	} 
	
	public function setCatatanAsisten3($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->catatan_asisten3 !== $v) {
			$this->catatan_asisten3 = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::CATATAN_ASISTEN3;
		}

	} 
	
	public function setIsAsisten3Setuju($v)
	{

		if ($this->is_asisten3_setuju !== $v || $v === false) {
			$this->is_asisten3_setuju = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::IS_ASISTEN3_SETUJU;
		}

	} 
	
	public function setVerifikasiAsisten3($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->verifikasi_asisten3 !== $v) {
			$this->verifikasi_asisten3 = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::VERIFIKASI_ASISTEN3;
		}

	} 
	
	public function setCatatanSekda($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->catatan_sekda !== $v) {
			$this->catatan_sekda = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::CATATAN_SEKDA;
		}

	} 
	
	public function setIsSekdaSetuju($v)
	{

		if ($this->is_sekda_setuju !== $v || $v === false) {
			$this->is_sekda_setuju = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::IS_SEKDA_SETUJU;
		}

	} 
	
	public function setVerifikasiSekda($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->verifikasi_sekda !== $v) {
			$this->verifikasi_sekda = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::VERIFIKASI_SEKDA;
		}

	} 
	
	public function setVerifikasiAsisten($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->verifikasi_asisten !== $v) {
			$this->verifikasi_asisten = $v;
			$this->modifiedColumns[] = MasterKegiatanPeer::VERIFIKASI_ASISTEN;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->unit_id = $rs->getString($startcol + 0);

			$this->kode_kegiatan = $rs->getString($startcol + 1);

			$this->kode_bidang = $rs->getString($startcol + 2);

			$this->kode_urusan_wajib = $rs->getString($startcol + 3);

			$this->kode_program = $rs->getString($startcol + 4);

			$this->kode_sasaran = $rs->getString($startcol + 5);

			$this->kode_indikator = $rs->getString($startcol + 6);

			$this->alokasi_dana = $rs->getFloat($startcol + 7);

			$this->nama_kegiatan = $rs->getString($startcol + 8);

			$this->masukan = $rs->getString($startcol + 9);

			$this->output = $rs->getString($startcol + 10);

			$this->outcome = $rs->getString($startcol + 11);

			$this->benefit = $rs->getString($startcol + 12);

			$this->impact = $rs->getString($startcol + 13);

			$this->tipe = $rs->getString($startcol + 14);

			$this->kegiatan_active = $rs->getBoolean($startcol + 15);

			$this->to_kegiatan_code = $rs->getString($startcol + 16);

			$this->catatan = $rs->getString($startcol + 17);

			$this->target_outcome = $rs->getString($startcol + 18);

			$this->lokasi = $rs->getString($startcol + 19);

			$this->jumlah_prev = $rs->getFloat($startcol + 20);

			$this->jumlah_now = $rs->getFloat($startcol + 21);

			$this->jumlah_next = $rs->getFloat($startcol + 22);

			$this->kode_program2 = $rs->getString($startcol + 23);

			$this->kode_urusan = $rs->getString($startcol + 24);

			$this->last_update_user = $rs->getString($startcol + 25);

			$this->last_update_time = $rs->getTimestamp($startcol + 26, null);

			$this->last_update_ip = $rs->getString($startcol + 27);

			$this->tahap = $rs->getString($startcol + 28);

			$this->kode_misi = $rs->getString($startcol + 29);

			$this->kode_tujuan = $rs->getString($startcol + 30);

			$this->ranking = $rs->getInt($startcol + 31);

			$this->nomor13 = $rs->getString($startcol + 32);

			$this->ppa_nama = $rs->getString($startcol + 33);

			$this->ppa_pangkat = $rs->getString($startcol + 34);

			$this->ppa_nip = $rs->getString($startcol + 35);

			$this->lanjutan = $rs->getBoolean($startcol + 36);

			$this->user_id = $rs->getString($startcol + 37);

			$this->id = $rs->getInt($startcol + 38);

			$this->tahun = $rs->getString($startcol + 39);

			$this->tambahan_pagu = $rs->getFloat($startcol + 40);

			$this->gender = $rs->getBoolean($startcol + 41);

			$this->kode_keg_keuangan = $rs->getString($startcol + 42);

			$this->user_id_lama = $rs->getString($startcol + 43);

			$this->indikator = $rs->getString($startcol + 44);

			$this->is_dak = $rs->getBoolean($startcol + 45);

			$this->kode_kegiatan_asal = $rs->getString($startcol + 46);

			$this->kode_keg_keuangan_asal = $rs->getString($startcol + 47);

			$this->th_ke_multiyears = $rs->getInt($startcol + 48);

			$this->kelompok_sasaran = $rs->getString($startcol + 49);

			$this->pagu_bappeko = $rs->getFloat($startcol + 50);

			$this->kode_dpa = $rs->getString($startcol + 51);

			$this->user_id_pptk = $rs->getString($startcol + 52);

			$this->user_id_kpa = $rs->getString($startcol + 53);

			$this->catatan_pembahasan = $rs->getString($startcol + 54);

			$this->is_tapd_setuju = $rs->getBoolean($startcol + 55);

			$this->is_bappeko_setuju = $rs->getBoolean($startcol + 56);

			$this->is_penyelia_setuju = $rs->getBoolean($startcol + 57);

			$this->is_pernah_rka = $rs->getBoolean($startcol + 58);

			$this->kode_kegiatan_baru = $rs->getString($startcol + 59);

			$this->catatan_bpkpd = $rs->getString($startcol + 60);

			$this->ubah_f1_dinas = $rs->getBoolean($startcol + 61);

			$this->ubah_f1_peneliti = $rs->getBoolean($startcol + 62);

			$this->sisa_lelang_dinas = $rs->getBoolean($startcol + 63);

			$this->sisa_lelang_peneliti = $rs->getBoolean($startcol + 64);

			$this->catatan_ubah_f1_dinas = $rs->getString($startcol + 65);

			$this->catatan_sisa_lelang_peneliti = $rs->getString($startcol + 66);

			$this->pptk_approval = $rs->getString($startcol + 67);

			$this->kpa_approval = $rs->getString($startcol + 68);

			$this->catatan_bagian_hukum = $rs->getString($startcol + 69);

			$this->catatan_inspektorat = $rs->getString($startcol + 70);

			$this->catatan_badan_kepegawaian = $rs->getString($startcol + 71);

			$this->catatan_lppa = $rs->getString($startcol + 72);

			$this->is_bagian_hukum_setuju = $rs->getBoolean($startcol + 73);

			$this->is_inspektorat_setuju = $rs->getBoolean($startcol + 74);

			$this->is_badan_kepegawaian_setuju = $rs->getBoolean($startcol + 75);

			$this->is_lppa_setuju = $rs->getBoolean($startcol + 76);

			$this->verifikasi_bpkpd = $rs->getString($startcol + 77);

			$this->verifikasi_bappeko = $rs->getString($startcol + 78);

			$this->verifikasi_penyelia = $rs->getString($startcol + 79);

			$this->verifikasi_bagian_hukum = $rs->getString($startcol + 80);

			$this->verifikasi_inspektorat = $rs->getString($startcol + 81);

			$this->verifikasi_badan_kepegawaian = $rs->getString($startcol + 82);

			$this->verifikasi_lppa = $rs->getString($startcol + 83);

			$this->metode_count = $rs->getBoolean($startcol + 84);

			$this->catatan_bagian_organisasi = $rs->getString($startcol + 85);

			$this->is_bagian_organisasi_setuju = $rs->getBoolean($startcol + 86);

			$this->verifikasi_bagian_organisasi = $rs->getString($startcol + 87);

			$this->catatan_asisten1 = $rs->getString($startcol + 88);

			$this->is_asisten1_setuju = $rs->getBoolean($startcol + 89);

			$this->verifikasi_asisten1 = $rs->getString($startcol + 90);

			$this->catatan_asisten2 = $rs->getString($startcol + 91);

			$this->is_asisten2_setuju = $rs->getBoolean($startcol + 92);

			$this->verifikasi_asisten2 = $rs->getString($startcol + 93);

			$this->catatan_asisten3 = $rs->getString($startcol + 94);

			$this->is_asisten3_setuju = $rs->getBoolean($startcol + 95);

			$this->verifikasi_asisten3 = $rs->getString($startcol + 96);

			$this->catatan_sekda = $rs->getString($startcol + 97);

			$this->is_sekda_setuju = $rs->getBoolean($startcol + 98);

			$this->verifikasi_sekda = $rs->getString($startcol + 99);

			$this->verifikasi_asisten = $rs->getString($startcol + 100);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 101; 
		} catch (Exception $e) {
			throw new PropelException("Error populating MasterKegiatan object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(MasterKegiatanPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			MasterKegiatanPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(MasterKegiatanPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = MasterKegiatanPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setId($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += MasterKegiatanPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = MasterKegiatanPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = MasterKegiatanPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getUnitId();
				break;
			case 1:
				return $this->getKodeKegiatan();
				break;
			case 2:
				return $this->getKodeBidang();
				break;
			case 3:
				return $this->getKodeUrusanWajib();
				break;
			case 4:
				return $this->getKodeProgram();
				break;
			case 5:
				return $this->getKodeSasaran();
				break;
			case 6:
				return $this->getKodeIndikator();
				break;
			case 7:
				return $this->getAlokasiDana();
				break;
			case 8:
				return $this->getNamaKegiatan();
				break;
			case 9:
				return $this->getMasukan();
				break;
			case 10:
				return $this->getOutput();
				break;
			case 11:
				return $this->getOutcome();
				break;
			case 12:
				return $this->getBenefit();
				break;
			case 13:
				return $this->getImpact();
				break;
			case 14:
				return $this->getTipe();
				break;
			case 15:
				return $this->getKegiatanActive();
				break;
			case 16:
				return $this->getToKegiatanCode();
				break;
			case 17:
				return $this->getCatatan();
				break;
			case 18:
				return $this->getTargetOutcome();
				break;
			case 19:
				return $this->getLokasi();
				break;
			case 20:
				return $this->getJumlahPrev();
				break;
			case 21:
				return $this->getJumlahNow();
				break;
			case 22:
				return $this->getJumlahNext();
				break;
			case 23:
				return $this->getKodeProgram2();
				break;
			case 24:
				return $this->getKodeUrusan();
				break;
			case 25:
				return $this->getLastUpdateUser();
				break;
			case 26:
				return $this->getLastUpdateTime();
				break;
			case 27:
				return $this->getLastUpdateIp();
				break;
			case 28:
				return $this->getTahap();
				break;
			case 29:
				return $this->getKodeMisi();
				break;
			case 30:
				return $this->getKodeTujuan();
				break;
			case 31:
				return $this->getRanking();
				break;
			case 32:
				return $this->getNomor13();
				break;
			case 33:
				return $this->getPpaNama();
				break;
			case 34:
				return $this->getPpaPangkat();
				break;
			case 35:
				return $this->getPpaNip();
				break;
			case 36:
				return $this->getLanjutan();
				break;
			case 37:
				return $this->getUserId();
				break;
			case 38:
				return $this->getId();
				break;
			case 39:
				return $this->getTahun();
				break;
			case 40:
				return $this->getTambahanPagu();
				break;
			case 41:
				return $this->getGender();
				break;
			case 42:
				return $this->getKodeKegKeuangan();
				break;
			case 43:
				return $this->getUserIdLama();
				break;
			case 44:
				return $this->getIndikator();
				break;
			case 45:
				return $this->getIsDak();
				break;
			case 46:
				return $this->getKodeKegiatanAsal();
				break;
			case 47:
				return $this->getKodeKegKeuanganAsal();
				break;
			case 48:
				return $this->getThKeMultiyears();
				break;
			case 49:
				return $this->getKelompokSasaran();
				break;
			case 50:
				return $this->getPaguBappeko();
				break;
			case 51:
				return $this->getKodeDpa();
				break;
			case 52:
				return $this->getUserIdPptk();
				break;
			case 53:
				return $this->getUserIdKpa();
				break;
			case 54:
				return $this->getCatatanPembahasan();
				break;
			case 55:
				return $this->getIsTapdSetuju();
				break;
			case 56:
				return $this->getIsBappekoSetuju();
				break;
			case 57:
				return $this->getIsPenyeliaSetuju();
				break;
			case 58:
				return $this->getIsPernahRka();
				break;
			case 59:
				return $this->getKodeKegiatanBaru();
				break;
			case 60:
				return $this->getCatatanBpkpd();
				break;
			case 61:
				return $this->getUbahF1Dinas();
				break;
			case 62:
				return $this->getUbahF1Peneliti();
				break;
			case 63:
				return $this->getSisaLelangDinas();
				break;
			case 64:
				return $this->getSisaLelangPeneliti();
				break;
			case 65:
				return $this->getCatatanUbahF1Dinas();
				break;
			case 66:
				return $this->getCatatanSisaLelangPeneliti();
				break;
			case 67:
				return $this->getPptkApproval();
				break;
			case 68:
				return $this->getKpaApproval();
				break;
			case 69:
				return $this->getCatatanBagianHukum();
				break;
			case 70:
				return $this->getCatatanInspektorat();
				break;
			case 71:
				return $this->getCatatanBadanKepegawaian();
				break;
			case 72:
				return $this->getCatatanLppa();
				break;
			case 73:
				return $this->getIsBagianHukumSetuju();
				break;
			case 74:
				return $this->getIsInspektoratSetuju();
				break;
			case 75:
				return $this->getIsBadanKepegawaianSetuju();
				break;
			case 76:
				return $this->getIsLppaSetuju();
				break;
			case 77:
				return $this->getVerifikasiBpkpd();
				break;
			case 78:
				return $this->getVerifikasiBappeko();
				break;
			case 79:
				return $this->getVerifikasiPenyelia();
				break;
			case 80:
				return $this->getVerifikasiBagianHukum();
				break;
			case 81:
				return $this->getVerifikasiInspektorat();
				break;
			case 82:
				return $this->getVerifikasiBadanKepegawaian();
				break;
			case 83:
				return $this->getVerifikasiLppa();
				break;
			case 84:
				return $this->getMetodeCount();
				break;
			case 85:
				return $this->getCatatanBagianOrganisasi();
				break;
			case 86:
				return $this->getIsBagianOrganisasiSetuju();
				break;
			case 87:
				return $this->getVerifikasiBagianOrganisasi();
				break;
			case 88:
				return $this->getCatatanAsisten1();
				break;
			case 89:
				return $this->getIsAsisten1Setuju();
				break;
			case 90:
				return $this->getVerifikasiAsisten1();
				break;
			case 91:
				return $this->getCatatanAsisten2();
				break;
			case 92:
				return $this->getIsAsisten2Setuju();
				break;
			case 93:
				return $this->getVerifikasiAsisten2();
				break;
			case 94:
				return $this->getCatatanAsisten3();
				break;
			case 95:
				return $this->getIsAsisten3Setuju();
				break;
			case 96:
				return $this->getVerifikasiAsisten3();
				break;
			case 97:
				return $this->getCatatanSekda();
				break;
			case 98:
				return $this->getIsSekdaSetuju();
				break;
			case 99:
				return $this->getVerifikasiSekda();
				break;
			case 100:
				return $this->getVerifikasiAsisten();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = MasterKegiatanPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getUnitId(),
			$keys[1] => $this->getKodeKegiatan(),
			$keys[2] => $this->getKodeBidang(),
			$keys[3] => $this->getKodeUrusanWajib(),
			$keys[4] => $this->getKodeProgram(),
			$keys[5] => $this->getKodeSasaran(),
			$keys[6] => $this->getKodeIndikator(),
			$keys[7] => $this->getAlokasiDana(),
			$keys[8] => $this->getNamaKegiatan(),
			$keys[9] => $this->getMasukan(),
			$keys[10] => $this->getOutput(),
			$keys[11] => $this->getOutcome(),
			$keys[12] => $this->getBenefit(),
			$keys[13] => $this->getImpact(),
			$keys[14] => $this->getTipe(),
			$keys[15] => $this->getKegiatanActive(),
			$keys[16] => $this->getToKegiatanCode(),
			$keys[17] => $this->getCatatan(),
			$keys[18] => $this->getTargetOutcome(),
			$keys[19] => $this->getLokasi(),
			$keys[20] => $this->getJumlahPrev(),
			$keys[21] => $this->getJumlahNow(),
			$keys[22] => $this->getJumlahNext(),
			$keys[23] => $this->getKodeProgram2(),
			$keys[24] => $this->getKodeUrusan(),
			$keys[25] => $this->getLastUpdateUser(),
			$keys[26] => $this->getLastUpdateTime(),
			$keys[27] => $this->getLastUpdateIp(),
			$keys[28] => $this->getTahap(),
			$keys[29] => $this->getKodeMisi(),
			$keys[30] => $this->getKodeTujuan(),
			$keys[31] => $this->getRanking(),
			$keys[32] => $this->getNomor13(),
			$keys[33] => $this->getPpaNama(),
			$keys[34] => $this->getPpaPangkat(),
			$keys[35] => $this->getPpaNip(),
			$keys[36] => $this->getLanjutan(),
			$keys[37] => $this->getUserId(),
			$keys[38] => $this->getId(),
			$keys[39] => $this->getTahun(),
			$keys[40] => $this->getTambahanPagu(),
			$keys[41] => $this->getGender(),
			$keys[42] => $this->getKodeKegKeuangan(),
			$keys[43] => $this->getUserIdLama(),
			$keys[44] => $this->getIndikator(),
			$keys[45] => $this->getIsDak(),
			$keys[46] => $this->getKodeKegiatanAsal(),
			$keys[47] => $this->getKodeKegKeuanganAsal(),
			$keys[48] => $this->getThKeMultiyears(),
			$keys[49] => $this->getKelompokSasaran(),
			$keys[50] => $this->getPaguBappeko(),
			$keys[51] => $this->getKodeDpa(),
			$keys[52] => $this->getUserIdPptk(),
			$keys[53] => $this->getUserIdKpa(),
			$keys[54] => $this->getCatatanPembahasan(),
			$keys[55] => $this->getIsTapdSetuju(),
			$keys[56] => $this->getIsBappekoSetuju(),
			$keys[57] => $this->getIsPenyeliaSetuju(),
			$keys[58] => $this->getIsPernahRka(),
			$keys[59] => $this->getKodeKegiatanBaru(),
			$keys[60] => $this->getCatatanBpkpd(),
			$keys[61] => $this->getUbahF1Dinas(),
			$keys[62] => $this->getUbahF1Peneliti(),
			$keys[63] => $this->getSisaLelangDinas(),
			$keys[64] => $this->getSisaLelangPeneliti(),
			$keys[65] => $this->getCatatanUbahF1Dinas(),
			$keys[66] => $this->getCatatanSisaLelangPeneliti(),
			$keys[67] => $this->getPptkApproval(),
			$keys[68] => $this->getKpaApproval(),
			$keys[69] => $this->getCatatanBagianHukum(),
			$keys[70] => $this->getCatatanInspektorat(),
			$keys[71] => $this->getCatatanBadanKepegawaian(),
			$keys[72] => $this->getCatatanLppa(),
			$keys[73] => $this->getIsBagianHukumSetuju(),
			$keys[74] => $this->getIsInspektoratSetuju(),
			$keys[75] => $this->getIsBadanKepegawaianSetuju(),
			$keys[76] => $this->getIsLppaSetuju(),
			$keys[77] => $this->getVerifikasiBpkpd(),
			$keys[78] => $this->getVerifikasiBappeko(),
			$keys[79] => $this->getVerifikasiPenyelia(),
			$keys[80] => $this->getVerifikasiBagianHukum(),
			$keys[81] => $this->getVerifikasiInspektorat(),
			$keys[82] => $this->getVerifikasiBadanKepegawaian(),
			$keys[83] => $this->getVerifikasiLppa(),
			$keys[84] => $this->getMetodeCount(),
			$keys[85] => $this->getCatatanBagianOrganisasi(),
			$keys[86] => $this->getIsBagianOrganisasiSetuju(),
			$keys[87] => $this->getVerifikasiBagianOrganisasi(),
			$keys[88] => $this->getCatatanAsisten1(),
			$keys[89] => $this->getIsAsisten1Setuju(),
			$keys[90] => $this->getVerifikasiAsisten1(),
			$keys[91] => $this->getCatatanAsisten2(),
			$keys[92] => $this->getIsAsisten2Setuju(),
			$keys[93] => $this->getVerifikasiAsisten2(),
			$keys[94] => $this->getCatatanAsisten3(),
			$keys[95] => $this->getIsAsisten3Setuju(),
			$keys[96] => $this->getVerifikasiAsisten3(),
			$keys[97] => $this->getCatatanSekda(),
			$keys[98] => $this->getIsSekdaSetuju(),
			$keys[99] => $this->getVerifikasiSekda(),
			$keys[100] => $this->getVerifikasiAsisten(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = MasterKegiatanPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setUnitId($value);
				break;
			case 1:
				$this->setKodeKegiatan($value);
				break;
			case 2:
				$this->setKodeBidang($value);
				break;
			case 3:
				$this->setKodeUrusanWajib($value);
				break;
			case 4:
				$this->setKodeProgram($value);
				break;
			case 5:
				$this->setKodeSasaran($value);
				break;
			case 6:
				$this->setKodeIndikator($value);
				break;
			case 7:
				$this->setAlokasiDana($value);
				break;
			case 8:
				$this->setNamaKegiatan($value);
				break;
			case 9:
				$this->setMasukan($value);
				break;
			case 10:
				$this->setOutput($value);
				break;
			case 11:
				$this->setOutcome($value);
				break;
			case 12:
				$this->setBenefit($value);
				break;
			case 13:
				$this->setImpact($value);
				break;
			case 14:
				$this->setTipe($value);
				break;
			case 15:
				$this->setKegiatanActive($value);
				break;
			case 16:
				$this->setToKegiatanCode($value);
				break;
			case 17:
				$this->setCatatan($value);
				break;
			case 18:
				$this->setTargetOutcome($value);
				break;
			case 19:
				$this->setLokasi($value);
				break;
			case 20:
				$this->setJumlahPrev($value);
				break;
			case 21:
				$this->setJumlahNow($value);
				break;
			case 22:
				$this->setJumlahNext($value);
				break;
			case 23:
				$this->setKodeProgram2($value);
				break;
			case 24:
				$this->setKodeUrusan($value);
				break;
			case 25:
				$this->setLastUpdateUser($value);
				break;
			case 26:
				$this->setLastUpdateTime($value);
				break;
			case 27:
				$this->setLastUpdateIp($value);
				break;
			case 28:
				$this->setTahap($value);
				break;
			case 29:
				$this->setKodeMisi($value);
				break;
			case 30:
				$this->setKodeTujuan($value);
				break;
			case 31:
				$this->setRanking($value);
				break;
			case 32:
				$this->setNomor13($value);
				break;
			case 33:
				$this->setPpaNama($value);
				break;
			case 34:
				$this->setPpaPangkat($value);
				break;
			case 35:
				$this->setPpaNip($value);
				break;
			case 36:
				$this->setLanjutan($value);
				break;
			case 37:
				$this->setUserId($value);
				break;
			case 38:
				$this->setId($value);
				break;
			case 39:
				$this->setTahun($value);
				break;
			case 40:
				$this->setTambahanPagu($value);
				break;
			case 41:
				$this->setGender($value);
				break;
			case 42:
				$this->setKodeKegKeuangan($value);
				break;
			case 43:
				$this->setUserIdLama($value);
				break;
			case 44:
				$this->setIndikator($value);
				break;
			case 45:
				$this->setIsDak($value);
				break;
			case 46:
				$this->setKodeKegiatanAsal($value);
				break;
			case 47:
				$this->setKodeKegKeuanganAsal($value);
				break;
			case 48:
				$this->setThKeMultiyears($value);
				break;
			case 49:
				$this->setKelompokSasaran($value);
				break;
			case 50:
				$this->setPaguBappeko($value);
				break;
			case 51:
				$this->setKodeDpa($value);
				break;
			case 52:
				$this->setUserIdPptk($value);
				break;
			case 53:
				$this->setUserIdKpa($value);
				break;
			case 54:
				$this->setCatatanPembahasan($value);
				break;
			case 55:
				$this->setIsTapdSetuju($value);
				break;
			case 56:
				$this->setIsBappekoSetuju($value);
				break;
			case 57:
				$this->setIsPenyeliaSetuju($value);
				break;
			case 58:
				$this->setIsPernahRka($value);
				break;
			case 59:
				$this->setKodeKegiatanBaru($value);
				break;
			case 60:
				$this->setCatatanBpkpd($value);
				break;
			case 61:
				$this->setUbahF1Dinas($value);
				break;
			case 62:
				$this->setUbahF1Peneliti($value);
				break;
			case 63:
				$this->setSisaLelangDinas($value);
				break;
			case 64:
				$this->setSisaLelangPeneliti($value);
				break;
			case 65:
				$this->setCatatanUbahF1Dinas($value);
				break;
			case 66:
				$this->setCatatanSisaLelangPeneliti($value);
				break;
			case 67:
				$this->setPptkApproval($value);
				break;
			case 68:
				$this->setKpaApproval($value);
				break;
			case 69:
				$this->setCatatanBagianHukum($value);
				break;
			case 70:
				$this->setCatatanInspektorat($value);
				break;
			case 71:
				$this->setCatatanBadanKepegawaian($value);
				break;
			case 72:
				$this->setCatatanLppa($value);
				break;
			case 73:
				$this->setIsBagianHukumSetuju($value);
				break;
			case 74:
				$this->setIsInspektoratSetuju($value);
				break;
			case 75:
				$this->setIsBadanKepegawaianSetuju($value);
				break;
			case 76:
				$this->setIsLppaSetuju($value);
				break;
			case 77:
				$this->setVerifikasiBpkpd($value);
				break;
			case 78:
				$this->setVerifikasiBappeko($value);
				break;
			case 79:
				$this->setVerifikasiPenyelia($value);
				break;
			case 80:
				$this->setVerifikasiBagianHukum($value);
				break;
			case 81:
				$this->setVerifikasiInspektorat($value);
				break;
			case 82:
				$this->setVerifikasiBadanKepegawaian($value);
				break;
			case 83:
				$this->setVerifikasiLppa($value);
				break;
			case 84:
				$this->setMetodeCount($value);
				break;
			case 85:
				$this->setCatatanBagianOrganisasi($value);
				break;
			case 86:
				$this->setIsBagianOrganisasiSetuju($value);
				break;
			case 87:
				$this->setVerifikasiBagianOrganisasi($value);
				break;
			case 88:
				$this->setCatatanAsisten1($value);
				break;
			case 89:
				$this->setIsAsisten1Setuju($value);
				break;
			case 90:
				$this->setVerifikasiAsisten1($value);
				break;
			case 91:
				$this->setCatatanAsisten2($value);
				break;
			case 92:
				$this->setIsAsisten2Setuju($value);
				break;
			case 93:
				$this->setVerifikasiAsisten2($value);
				break;
			case 94:
				$this->setCatatanAsisten3($value);
				break;
			case 95:
				$this->setIsAsisten3Setuju($value);
				break;
			case 96:
				$this->setVerifikasiAsisten3($value);
				break;
			case 97:
				$this->setCatatanSekda($value);
				break;
			case 98:
				$this->setIsSekdaSetuju($value);
				break;
			case 99:
				$this->setVerifikasiSekda($value);
				break;
			case 100:
				$this->setVerifikasiAsisten($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = MasterKegiatanPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setUnitId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setKodeKegiatan($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setKodeBidang($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setKodeUrusanWajib($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setKodeProgram($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setKodeSasaran($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setKodeIndikator($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setAlokasiDana($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setNamaKegiatan($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setMasukan($arr[$keys[9]]);
		if (array_key_exists($keys[10], $arr)) $this->setOutput($arr[$keys[10]]);
		if (array_key_exists($keys[11], $arr)) $this->setOutcome($arr[$keys[11]]);
		if (array_key_exists($keys[12], $arr)) $this->setBenefit($arr[$keys[12]]);
		if (array_key_exists($keys[13], $arr)) $this->setImpact($arr[$keys[13]]);
		if (array_key_exists($keys[14], $arr)) $this->setTipe($arr[$keys[14]]);
		if (array_key_exists($keys[15], $arr)) $this->setKegiatanActive($arr[$keys[15]]);
		if (array_key_exists($keys[16], $arr)) $this->setToKegiatanCode($arr[$keys[16]]);
		if (array_key_exists($keys[17], $arr)) $this->setCatatan($arr[$keys[17]]);
		if (array_key_exists($keys[18], $arr)) $this->setTargetOutcome($arr[$keys[18]]);
		if (array_key_exists($keys[19], $arr)) $this->setLokasi($arr[$keys[19]]);
		if (array_key_exists($keys[20], $arr)) $this->setJumlahPrev($arr[$keys[20]]);
		if (array_key_exists($keys[21], $arr)) $this->setJumlahNow($arr[$keys[21]]);
		if (array_key_exists($keys[22], $arr)) $this->setJumlahNext($arr[$keys[22]]);
		if (array_key_exists($keys[23], $arr)) $this->setKodeProgram2($arr[$keys[23]]);
		if (array_key_exists($keys[24], $arr)) $this->setKodeUrusan($arr[$keys[24]]);
		if (array_key_exists($keys[25], $arr)) $this->setLastUpdateUser($arr[$keys[25]]);
		if (array_key_exists($keys[26], $arr)) $this->setLastUpdateTime($arr[$keys[26]]);
		if (array_key_exists($keys[27], $arr)) $this->setLastUpdateIp($arr[$keys[27]]);
		if (array_key_exists($keys[28], $arr)) $this->setTahap($arr[$keys[28]]);
		if (array_key_exists($keys[29], $arr)) $this->setKodeMisi($arr[$keys[29]]);
		if (array_key_exists($keys[30], $arr)) $this->setKodeTujuan($arr[$keys[30]]);
		if (array_key_exists($keys[31], $arr)) $this->setRanking($arr[$keys[31]]);
		if (array_key_exists($keys[32], $arr)) $this->setNomor13($arr[$keys[32]]);
		if (array_key_exists($keys[33], $arr)) $this->setPpaNama($arr[$keys[33]]);
		if (array_key_exists($keys[34], $arr)) $this->setPpaPangkat($arr[$keys[34]]);
		if (array_key_exists($keys[35], $arr)) $this->setPpaNip($arr[$keys[35]]);
		if (array_key_exists($keys[36], $arr)) $this->setLanjutan($arr[$keys[36]]);
		if (array_key_exists($keys[37], $arr)) $this->setUserId($arr[$keys[37]]);
		if (array_key_exists($keys[38], $arr)) $this->setId($arr[$keys[38]]);
		if (array_key_exists($keys[39], $arr)) $this->setTahun($arr[$keys[39]]);
		if (array_key_exists($keys[40], $arr)) $this->setTambahanPagu($arr[$keys[40]]);
		if (array_key_exists($keys[41], $arr)) $this->setGender($arr[$keys[41]]);
		if (array_key_exists($keys[42], $arr)) $this->setKodeKegKeuangan($arr[$keys[42]]);
		if (array_key_exists($keys[43], $arr)) $this->setUserIdLama($arr[$keys[43]]);
		if (array_key_exists($keys[44], $arr)) $this->setIndikator($arr[$keys[44]]);
		if (array_key_exists($keys[45], $arr)) $this->setIsDak($arr[$keys[45]]);
		if (array_key_exists($keys[46], $arr)) $this->setKodeKegiatanAsal($arr[$keys[46]]);
		if (array_key_exists($keys[47], $arr)) $this->setKodeKegKeuanganAsal($arr[$keys[47]]);
		if (array_key_exists($keys[48], $arr)) $this->setThKeMultiyears($arr[$keys[48]]);
		if (array_key_exists($keys[49], $arr)) $this->setKelompokSasaran($arr[$keys[49]]);
		if (array_key_exists($keys[50], $arr)) $this->setPaguBappeko($arr[$keys[50]]);
		if (array_key_exists($keys[51], $arr)) $this->setKodeDpa($arr[$keys[51]]);
		if (array_key_exists($keys[52], $arr)) $this->setUserIdPptk($arr[$keys[52]]);
		if (array_key_exists($keys[53], $arr)) $this->setUserIdKpa($arr[$keys[53]]);
		if (array_key_exists($keys[54], $arr)) $this->setCatatanPembahasan($arr[$keys[54]]);
		if (array_key_exists($keys[55], $arr)) $this->setIsTapdSetuju($arr[$keys[55]]);
		if (array_key_exists($keys[56], $arr)) $this->setIsBappekoSetuju($arr[$keys[56]]);
		if (array_key_exists($keys[57], $arr)) $this->setIsPenyeliaSetuju($arr[$keys[57]]);
		if (array_key_exists($keys[58], $arr)) $this->setIsPernahRka($arr[$keys[58]]);
		if (array_key_exists($keys[59], $arr)) $this->setKodeKegiatanBaru($arr[$keys[59]]);
		if (array_key_exists($keys[60], $arr)) $this->setCatatanBpkpd($arr[$keys[60]]);
		if (array_key_exists($keys[61], $arr)) $this->setUbahF1Dinas($arr[$keys[61]]);
		if (array_key_exists($keys[62], $arr)) $this->setUbahF1Peneliti($arr[$keys[62]]);
		if (array_key_exists($keys[63], $arr)) $this->setSisaLelangDinas($arr[$keys[63]]);
		if (array_key_exists($keys[64], $arr)) $this->setSisaLelangPeneliti($arr[$keys[64]]);
		if (array_key_exists($keys[65], $arr)) $this->setCatatanUbahF1Dinas($arr[$keys[65]]);
		if (array_key_exists($keys[66], $arr)) $this->setCatatanSisaLelangPeneliti($arr[$keys[66]]);
		if (array_key_exists($keys[67], $arr)) $this->setPptkApproval($arr[$keys[67]]);
		if (array_key_exists($keys[68], $arr)) $this->setKpaApproval($arr[$keys[68]]);
		if (array_key_exists($keys[69], $arr)) $this->setCatatanBagianHukum($arr[$keys[69]]);
		if (array_key_exists($keys[70], $arr)) $this->setCatatanInspektorat($arr[$keys[70]]);
		if (array_key_exists($keys[71], $arr)) $this->setCatatanBadanKepegawaian($arr[$keys[71]]);
		if (array_key_exists($keys[72], $arr)) $this->setCatatanLppa($arr[$keys[72]]);
		if (array_key_exists($keys[73], $arr)) $this->setIsBagianHukumSetuju($arr[$keys[73]]);
		if (array_key_exists($keys[74], $arr)) $this->setIsInspektoratSetuju($arr[$keys[74]]);
		if (array_key_exists($keys[75], $arr)) $this->setIsBadanKepegawaianSetuju($arr[$keys[75]]);
		if (array_key_exists($keys[76], $arr)) $this->setIsLppaSetuju($arr[$keys[76]]);
		if (array_key_exists($keys[77], $arr)) $this->setVerifikasiBpkpd($arr[$keys[77]]);
		if (array_key_exists($keys[78], $arr)) $this->setVerifikasiBappeko($arr[$keys[78]]);
		if (array_key_exists($keys[79], $arr)) $this->setVerifikasiPenyelia($arr[$keys[79]]);
		if (array_key_exists($keys[80], $arr)) $this->setVerifikasiBagianHukum($arr[$keys[80]]);
		if (array_key_exists($keys[81], $arr)) $this->setVerifikasiInspektorat($arr[$keys[81]]);
		if (array_key_exists($keys[82], $arr)) $this->setVerifikasiBadanKepegawaian($arr[$keys[82]]);
		if (array_key_exists($keys[83], $arr)) $this->setVerifikasiLppa($arr[$keys[83]]);
		if (array_key_exists($keys[84], $arr)) $this->setMetodeCount($arr[$keys[84]]);
		if (array_key_exists($keys[85], $arr)) $this->setCatatanBagianOrganisasi($arr[$keys[85]]);
		if (array_key_exists($keys[86], $arr)) $this->setIsBagianOrganisasiSetuju($arr[$keys[86]]);
		if (array_key_exists($keys[87], $arr)) $this->setVerifikasiBagianOrganisasi($arr[$keys[87]]);
		if (array_key_exists($keys[88], $arr)) $this->setCatatanAsisten1($arr[$keys[88]]);
		if (array_key_exists($keys[89], $arr)) $this->setIsAsisten1Setuju($arr[$keys[89]]);
		if (array_key_exists($keys[90], $arr)) $this->setVerifikasiAsisten1($arr[$keys[90]]);
		if (array_key_exists($keys[91], $arr)) $this->setCatatanAsisten2($arr[$keys[91]]);
		if (array_key_exists($keys[92], $arr)) $this->setIsAsisten2Setuju($arr[$keys[92]]);
		if (array_key_exists($keys[93], $arr)) $this->setVerifikasiAsisten2($arr[$keys[93]]);
		if (array_key_exists($keys[94], $arr)) $this->setCatatanAsisten3($arr[$keys[94]]);
		if (array_key_exists($keys[95], $arr)) $this->setIsAsisten3Setuju($arr[$keys[95]]);
		if (array_key_exists($keys[96], $arr)) $this->setVerifikasiAsisten3($arr[$keys[96]]);
		if (array_key_exists($keys[97], $arr)) $this->setCatatanSekda($arr[$keys[97]]);
		if (array_key_exists($keys[98], $arr)) $this->setIsSekdaSetuju($arr[$keys[98]]);
		if (array_key_exists($keys[99], $arr)) $this->setVerifikasiSekda($arr[$keys[99]]);
		if (array_key_exists($keys[100], $arr)) $this->setVerifikasiAsisten($arr[$keys[100]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(MasterKegiatanPeer::DATABASE_NAME);

		if ($this->isColumnModified(MasterKegiatanPeer::UNIT_ID)) $criteria->add(MasterKegiatanPeer::UNIT_ID, $this->unit_id);
		if ($this->isColumnModified(MasterKegiatanPeer::KODE_KEGIATAN)) $criteria->add(MasterKegiatanPeer::KODE_KEGIATAN, $this->kode_kegiatan);
		if ($this->isColumnModified(MasterKegiatanPeer::KODE_BIDANG)) $criteria->add(MasterKegiatanPeer::KODE_BIDANG, $this->kode_bidang);
		if ($this->isColumnModified(MasterKegiatanPeer::KODE_URUSAN_WAJIB)) $criteria->add(MasterKegiatanPeer::KODE_URUSAN_WAJIB, $this->kode_urusan_wajib);
		if ($this->isColumnModified(MasterKegiatanPeer::KODE_PROGRAM)) $criteria->add(MasterKegiatanPeer::KODE_PROGRAM, $this->kode_program);
		if ($this->isColumnModified(MasterKegiatanPeer::KODE_SASARAN)) $criteria->add(MasterKegiatanPeer::KODE_SASARAN, $this->kode_sasaran);
		if ($this->isColumnModified(MasterKegiatanPeer::KODE_INDIKATOR)) $criteria->add(MasterKegiatanPeer::KODE_INDIKATOR, $this->kode_indikator);
		if ($this->isColumnModified(MasterKegiatanPeer::ALOKASI_DANA)) $criteria->add(MasterKegiatanPeer::ALOKASI_DANA, $this->alokasi_dana);
		if ($this->isColumnModified(MasterKegiatanPeer::NAMA_KEGIATAN)) $criteria->add(MasterKegiatanPeer::NAMA_KEGIATAN, $this->nama_kegiatan);
		if ($this->isColumnModified(MasterKegiatanPeer::MASUKAN)) $criteria->add(MasterKegiatanPeer::MASUKAN, $this->masukan);
		if ($this->isColumnModified(MasterKegiatanPeer::OUTPUT)) $criteria->add(MasterKegiatanPeer::OUTPUT, $this->output);
		if ($this->isColumnModified(MasterKegiatanPeer::OUTCOME)) $criteria->add(MasterKegiatanPeer::OUTCOME, $this->outcome);
		if ($this->isColumnModified(MasterKegiatanPeer::BENEFIT)) $criteria->add(MasterKegiatanPeer::BENEFIT, $this->benefit);
		if ($this->isColumnModified(MasterKegiatanPeer::IMPACT)) $criteria->add(MasterKegiatanPeer::IMPACT, $this->impact);
		if ($this->isColumnModified(MasterKegiatanPeer::TIPE)) $criteria->add(MasterKegiatanPeer::TIPE, $this->tipe);
		if ($this->isColumnModified(MasterKegiatanPeer::KEGIATAN_ACTIVE)) $criteria->add(MasterKegiatanPeer::KEGIATAN_ACTIVE, $this->kegiatan_active);
		if ($this->isColumnModified(MasterKegiatanPeer::TO_KEGIATAN_CODE)) $criteria->add(MasterKegiatanPeer::TO_KEGIATAN_CODE, $this->to_kegiatan_code);
		if ($this->isColumnModified(MasterKegiatanPeer::CATATAN)) $criteria->add(MasterKegiatanPeer::CATATAN, $this->catatan);
		if ($this->isColumnModified(MasterKegiatanPeer::TARGET_OUTCOME)) $criteria->add(MasterKegiatanPeer::TARGET_OUTCOME, $this->target_outcome);
		if ($this->isColumnModified(MasterKegiatanPeer::LOKASI)) $criteria->add(MasterKegiatanPeer::LOKASI, $this->lokasi);
		if ($this->isColumnModified(MasterKegiatanPeer::JUMLAH_PREV)) $criteria->add(MasterKegiatanPeer::JUMLAH_PREV, $this->jumlah_prev);
		if ($this->isColumnModified(MasterKegiatanPeer::JUMLAH_NOW)) $criteria->add(MasterKegiatanPeer::JUMLAH_NOW, $this->jumlah_now);
		if ($this->isColumnModified(MasterKegiatanPeer::JUMLAH_NEXT)) $criteria->add(MasterKegiatanPeer::JUMLAH_NEXT, $this->jumlah_next);
		if ($this->isColumnModified(MasterKegiatanPeer::KODE_PROGRAM2)) $criteria->add(MasterKegiatanPeer::KODE_PROGRAM2, $this->kode_program2);
		if ($this->isColumnModified(MasterKegiatanPeer::KODE_URUSAN)) $criteria->add(MasterKegiatanPeer::KODE_URUSAN, $this->kode_urusan);
		if ($this->isColumnModified(MasterKegiatanPeer::LAST_UPDATE_USER)) $criteria->add(MasterKegiatanPeer::LAST_UPDATE_USER, $this->last_update_user);
		if ($this->isColumnModified(MasterKegiatanPeer::LAST_UPDATE_TIME)) $criteria->add(MasterKegiatanPeer::LAST_UPDATE_TIME, $this->last_update_time);
		if ($this->isColumnModified(MasterKegiatanPeer::LAST_UPDATE_IP)) $criteria->add(MasterKegiatanPeer::LAST_UPDATE_IP, $this->last_update_ip);
		if ($this->isColumnModified(MasterKegiatanPeer::TAHAP)) $criteria->add(MasterKegiatanPeer::TAHAP, $this->tahap);
		if ($this->isColumnModified(MasterKegiatanPeer::KODE_MISI)) $criteria->add(MasterKegiatanPeer::KODE_MISI, $this->kode_misi);
		if ($this->isColumnModified(MasterKegiatanPeer::KODE_TUJUAN)) $criteria->add(MasterKegiatanPeer::KODE_TUJUAN, $this->kode_tujuan);
		if ($this->isColumnModified(MasterKegiatanPeer::RANKING)) $criteria->add(MasterKegiatanPeer::RANKING, $this->ranking);
		if ($this->isColumnModified(MasterKegiatanPeer::NOMOR13)) $criteria->add(MasterKegiatanPeer::NOMOR13, $this->nomor13);
		if ($this->isColumnModified(MasterKegiatanPeer::PPA_NAMA)) $criteria->add(MasterKegiatanPeer::PPA_NAMA, $this->ppa_nama);
		if ($this->isColumnModified(MasterKegiatanPeer::PPA_PANGKAT)) $criteria->add(MasterKegiatanPeer::PPA_PANGKAT, $this->ppa_pangkat);
		if ($this->isColumnModified(MasterKegiatanPeer::PPA_NIP)) $criteria->add(MasterKegiatanPeer::PPA_NIP, $this->ppa_nip);
		if ($this->isColumnModified(MasterKegiatanPeer::LANJUTAN)) $criteria->add(MasterKegiatanPeer::LANJUTAN, $this->lanjutan);
		if ($this->isColumnModified(MasterKegiatanPeer::USER_ID)) $criteria->add(MasterKegiatanPeer::USER_ID, $this->user_id);
		if ($this->isColumnModified(MasterKegiatanPeer::ID)) $criteria->add(MasterKegiatanPeer::ID, $this->id);
		if ($this->isColumnModified(MasterKegiatanPeer::TAHUN)) $criteria->add(MasterKegiatanPeer::TAHUN, $this->tahun);
		if ($this->isColumnModified(MasterKegiatanPeer::TAMBAHAN_PAGU)) $criteria->add(MasterKegiatanPeer::TAMBAHAN_PAGU, $this->tambahan_pagu);
		if ($this->isColumnModified(MasterKegiatanPeer::GENDER)) $criteria->add(MasterKegiatanPeer::GENDER, $this->gender);
		if ($this->isColumnModified(MasterKegiatanPeer::KODE_KEG_KEUANGAN)) $criteria->add(MasterKegiatanPeer::KODE_KEG_KEUANGAN, $this->kode_keg_keuangan);
		if ($this->isColumnModified(MasterKegiatanPeer::USER_ID_LAMA)) $criteria->add(MasterKegiatanPeer::USER_ID_LAMA, $this->user_id_lama);
		if ($this->isColumnModified(MasterKegiatanPeer::INDIKATOR)) $criteria->add(MasterKegiatanPeer::INDIKATOR, $this->indikator);
		if ($this->isColumnModified(MasterKegiatanPeer::IS_DAK)) $criteria->add(MasterKegiatanPeer::IS_DAK, $this->is_dak);
		if ($this->isColumnModified(MasterKegiatanPeer::KODE_KEGIATAN_ASAL)) $criteria->add(MasterKegiatanPeer::KODE_KEGIATAN_ASAL, $this->kode_kegiatan_asal);
		if ($this->isColumnModified(MasterKegiatanPeer::KODE_KEG_KEUANGAN_ASAL)) $criteria->add(MasterKegiatanPeer::KODE_KEG_KEUANGAN_ASAL, $this->kode_keg_keuangan_asal);
		if ($this->isColumnModified(MasterKegiatanPeer::TH_KE_MULTIYEARS)) $criteria->add(MasterKegiatanPeer::TH_KE_MULTIYEARS, $this->th_ke_multiyears);
		if ($this->isColumnModified(MasterKegiatanPeer::KELOMPOK_SASARAN)) $criteria->add(MasterKegiatanPeer::KELOMPOK_SASARAN, $this->kelompok_sasaran);
		if ($this->isColumnModified(MasterKegiatanPeer::PAGU_BAPPEKO)) $criteria->add(MasterKegiatanPeer::PAGU_BAPPEKO, $this->pagu_bappeko);
		if ($this->isColumnModified(MasterKegiatanPeer::KODE_DPA)) $criteria->add(MasterKegiatanPeer::KODE_DPA, $this->kode_dpa);
		if ($this->isColumnModified(MasterKegiatanPeer::USER_ID_PPTK)) $criteria->add(MasterKegiatanPeer::USER_ID_PPTK, $this->user_id_pptk);
		if ($this->isColumnModified(MasterKegiatanPeer::USER_ID_KPA)) $criteria->add(MasterKegiatanPeer::USER_ID_KPA, $this->user_id_kpa);
		if ($this->isColumnModified(MasterKegiatanPeer::CATATAN_PEMBAHASAN)) $criteria->add(MasterKegiatanPeer::CATATAN_PEMBAHASAN, $this->catatan_pembahasan);
		if ($this->isColumnModified(MasterKegiatanPeer::IS_TAPD_SETUJU)) $criteria->add(MasterKegiatanPeer::IS_TAPD_SETUJU, $this->is_tapd_setuju);
		if ($this->isColumnModified(MasterKegiatanPeer::IS_BAPPEKO_SETUJU)) $criteria->add(MasterKegiatanPeer::IS_BAPPEKO_SETUJU, $this->is_bappeko_setuju);
		if ($this->isColumnModified(MasterKegiatanPeer::IS_PENYELIA_SETUJU)) $criteria->add(MasterKegiatanPeer::IS_PENYELIA_SETUJU, $this->is_penyelia_setuju);
		if ($this->isColumnModified(MasterKegiatanPeer::IS_PERNAH_RKA)) $criteria->add(MasterKegiatanPeer::IS_PERNAH_RKA, $this->is_pernah_rka);
		if ($this->isColumnModified(MasterKegiatanPeer::KODE_KEGIATAN_BARU)) $criteria->add(MasterKegiatanPeer::KODE_KEGIATAN_BARU, $this->kode_kegiatan_baru);
		if ($this->isColumnModified(MasterKegiatanPeer::CATATAN_BPKPD)) $criteria->add(MasterKegiatanPeer::CATATAN_BPKPD, $this->catatan_bpkpd);
		if ($this->isColumnModified(MasterKegiatanPeer::UBAH_F1_DINAS)) $criteria->add(MasterKegiatanPeer::UBAH_F1_DINAS, $this->ubah_f1_dinas);
		if ($this->isColumnModified(MasterKegiatanPeer::UBAH_F1_PENELITI)) $criteria->add(MasterKegiatanPeer::UBAH_F1_PENELITI, $this->ubah_f1_peneliti);
		if ($this->isColumnModified(MasterKegiatanPeer::SISA_LELANG_DINAS)) $criteria->add(MasterKegiatanPeer::SISA_LELANG_DINAS, $this->sisa_lelang_dinas);
		if ($this->isColumnModified(MasterKegiatanPeer::SISA_LELANG_PENELITI)) $criteria->add(MasterKegiatanPeer::SISA_LELANG_PENELITI, $this->sisa_lelang_peneliti);
		if ($this->isColumnModified(MasterKegiatanPeer::CATATAN_UBAH_F1_DINAS)) $criteria->add(MasterKegiatanPeer::CATATAN_UBAH_F1_DINAS, $this->catatan_ubah_f1_dinas);
		if ($this->isColumnModified(MasterKegiatanPeer::CATATAN_SISA_LELANG_PENELITI)) $criteria->add(MasterKegiatanPeer::CATATAN_SISA_LELANG_PENELITI, $this->catatan_sisa_lelang_peneliti);
		if ($this->isColumnModified(MasterKegiatanPeer::PPTK_APPROVAL)) $criteria->add(MasterKegiatanPeer::PPTK_APPROVAL, $this->pptk_approval);
		if ($this->isColumnModified(MasterKegiatanPeer::KPA_APPROVAL)) $criteria->add(MasterKegiatanPeer::KPA_APPROVAL, $this->kpa_approval);
		if ($this->isColumnModified(MasterKegiatanPeer::CATATAN_BAGIAN_HUKUM)) $criteria->add(MasterKegiatanPeer::CATATAN_BAGIAN_HUKUM, $this->catatan_bagian_hukum);
		if ($this->isColumnModified(MasterKegiatanPeer::CATATAN_INSPEKTORAT)) $criteria->add(MasterKegiatanPeer::CATATAN_INSPEKTORAT, $this->catatan_inspektorat);
		if ($this->isColumnModified(MasterKegiatanPeer::CATATAN_BADAN_KEPEGAWAIAN)) $criteria->add(MasterKegiatanPeer::CATATAN_BADAN_KEPEGAWAIAN, $this->catatan_badan_kepegawaian);
		if ($this->isColumnModified(MasterKegiatanPeer::CATATAN_LPPA)) $criteria->add(MasterKegiatanPeer::CATATAN_LPPA, $this->catatan_lppa);
		if ($this->isColumnModified(MasterKegiatanPeer::IS_BAGIAN_HUKUM_SETUJU)) $criteria->add(MasterKegiatanPeer::IS_BAGIAN_HUKUM_SETUJU, $this->is_bagian_hukum_setuju);
		if ($this->isColumnModified(MasterKegiatanPeer::IS_INSPEKTORAT_SETUJU)) $criteria->add(MasterKegiatanPeer::IS_INSPEKTORAT_SETUJU, $this->is_inspektorat_setuju);
		if ($this->isColumnModified(MasterKegiatanPeer::IS_BADAN_KEPEGAWAIAN_SETUJU)) $criteria->add(MasterKegiatanPeer::IS_BADAN_KEPEGAWAIAN_SETUJU, $this->is_badan_kepegawaian_setuju);
		if ($this->isColumnModified(MasterKegiatanPeer::IS_LPPA_SETUJU)) $criteria->add(MasterKegiatanPeer::IS_LPPA_SETUJU, $this->is_lppa_setuju);
		if ($this->isColumnModified(MasterKegiatanPeer::VERIFIKASI_BPKPD)) $criteria->add(MasterKegiatanPeer::VERIFIKASI_BPKPD, $this->verifikasi_bpkpd);
		if ($this->isColumnModified(MasterKegiatanPeer::VERIFIKASI_BAPPEKO)) $criteria->add(MasterKegiatanPeer::VERIFIKASI_BAPPEKO, $this->verifikasi_bappeko);
		if ($this->isColumnModified(MasterKegiatanPeer::VERIFIKASI_PENYELIA)) $criteria->add(MasterKegiatanPeer::VERIFIKASI_PENYELIA, $this->verifikasi_penyelia);
		if ($this->isColumnModified(MasterKegiatanPeer::VERIFIKASI_BAGIAN_HUKUM)) $criteria->add(MasterKegiatanPeer::VERIFIKASI_BAGIAN_HUKUM, $this->verifikasi_bagian_hukum);
		if ($this->isColumnModified(MasterKegiatanPeer::VERIFIKASI_INSPEKTORAT)) $criteria->add(MasterKegiatanPeer::VERIFIKASI_INSPEKTORAT, $this->verifikasi_inspektorat);
		if ($this->isColumnModified(MasterKegiatanPeer::VERIFIKASI_BADAN_KEPEGAWAIAN)) $criteria->add(MasterKegiatanPeer::VERIFIKASI_BADAN_KEPEGAWAIAN, $this->verifikasi_badan_kepegawaian);
		if ($this->isColumnModified(MasterKegiatanPeer::VERIFIKASI_LPPA)) $criteria->add(MasterKegiatanPeer::VERIFIKASI_LPPA, $this->verifikasi_lppa);
		if ($this->isColumnModified(MasterKegiatanPeer::METODE_COUNT)) $criteria->add(MasterKegiatanPeer::METODE_COUNT, $this->metode_count);
		if ($this->isColumnModified(MasterKegiatanPeer::CATATAN_BAGIAN_ORGANISASI)) $criteria->add(MasterKegiatanPeer::CATATAN_BAGIAN_ORGANISASI, $this->catatan_bagian_organisasi);
		if ($this->isColumnModified(MasterKegiatanPeer::IS_BAGIAN_ORGANISASI_SETUJU)) $criteria->add(MasterKegiatanPeer::IS_BAGIAN_ORGANISASI_SETUJU, $this->is_bagian_organisasi_setuju);
		if ($this->isColumnModified(MasterKegiatanPeer::VERIFIKASI_BAGIAN_ORGANISASI)) $criteria->add(MasterKegiatanPeer::VERIFIKASI_BAGIAN_ORGANISASI, $this->verifikasi_bagian_organisasi);
		if ($this->isColumnModified(MasterKegiatanPeer::CATATAN_ASISTEN1)) $criteria->add(MasterKegiatanPeer::CATATAN_ASISTEN1, $this->catatan_asisten1);
		if ($this->isColumnModified(MasterKegiatanPeer::IS_ASISTEN1_SETUJU)) $criteria->add(MasterKegiatanPeer::IS_ASISTEN1_SETUJU, $this->is_asisten1_setuju);
		if ($this->isColumnModified(MasterKegiatanPeer::VERIFIKASI_ASISTEN1)) $criteria->add(MasterKegiatanPeer::VERIFIKASI_ASISTEN1, $this->verifikasi_asisten1);
		if ($this->isColumnModified(MasterKegiatanPeer::CATATAN_ASISTEN2)) $criteria->add(MasterKegiatanPeer::CATATAN_ASISTEN2, $this->catatan_asisten2);
		if ($this->isColumnModified(MasterKegiatanPeer::IS_ASISTEN2_SETUJU)) $criteria->add(MasterKegiatanPeer::IS_ASISTEN2_SETUJU, $this->is_asisten2_setuju);
		if ($this->isColumnModified(MasterKegiatanPeer::VERIFIKASI_ASISTEN2)) $criteria->add(MasterKegiatanPeer::VERIFIKASI_ASISTEN2, $this->verifikasi_asisten2);
		if ($this->isColumnModified(MasterKegiatanPeer::CATATAN_ASISTEN3)) $criteria->add(MasterKegiatanPeer::CATATAN_ASISTEN3, $this->catatan_asisten3);
		if ($this->isColumnModified(MasterKegiatanPeer::IS_ASISTEN3_SETUJU)) $criteria->add(MasterKegiatanPeer::IS_ASISTEN3_SETUJU, $this->is_asisten3_setuju);
		if ($this->isColumnModified(MasterKegiatanPeer::VERIFIKASI_ASISTEN3)) $criteria->add(MasterKegiatanPeer::VERIFIKASI_ASISTEN3, $this->verifikasi_asisten3);
		if ($this->isColumnModified(MasterKegiatanPeer::CATATAN_SEKDA)) $criteria->add(MasterKegiatanPeer::CATATAN_SEKDA, $this->catatan_sekda);
		if ($this->isColumnModified(MasterKegiatanPeer::IS_SEKDA_SETUJU)) $criteria->add(MasterKegiatanPeer::IS_SEKDA_SETUJU, $this->is_sekda_setuju);
		if ($this->isColumnModified(MasterKegiatanPeer::VERIFIKASI_SEKDA)) $criteria->add(MasterKegiatanPeer::VERIFIKASI_SEKDA, $this->verifikasi_sekda);
		if ($this->isColumnModified(MasterKegiatanPeer::VERIFIKASI_ASISTEN)) $criteria->add(MasterKegiatanPeer::VERIFIKASI_ASISTEN, $this->verifikasi_asisten);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(MasterKegiatanPeer::DATABASE_NAME);

		$criteria->add(MasterKegiatanPeer::UNIT_ID, $this->unit_id);
		$criteria->add(MasterKegiatanPeer::KODE_KEGIATAN, $this->kode_kegiatan);
		$criteria->add(MasterKegiatanPeer::ID, $this->id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		$pks = array();

		$pks[0] = $this->getUnitId();

		$pks[1] = $this->getKodeKegiatan();

		$pks[2] = $this->getId();

		return $pks;
	}

	
	public function setPrimaryKey($keys)
	{

		$this->setUnitId($keys[0]);

		$this->setKodeKegiatan($keys[1]);

		$this->setId($keys[2]);

	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setKodeBidang($this->kode_bidang);

		$copyObj->setKodeUrusanWajib($this->kode_urusan_wajib);

		$copyObj->setKodeProgram($this->kode_program);

		$copyObj->setKodeSasaran($this->kode_sasaran);

		$copyObj->setKodeIndikator($this->kode_indikator);

		$copyObj->setAlokasiDana($this->alokasi_dana);

		$copyObj->setNamaKegiatan($this->nama_kegiatan);

		$copyObj->setMasukan($this->masukan);

		$copyObj->setOutput($this->output);

		$copyObj->setOutcome($this->outcome);

		$copyObj->setBenefit($this->benefit);

		$copyObj->setImpact($this->impact);

		$copyObj->setTipe($this->tipe);

		$copyObj->setKegiatanActive($this->kegiatan_active);

		$copyObj->setToKegiatanCode($this->to_kegiatan_code);

		$copyObj->setCatatan($this->catatan);

		$copyObj->setTargetOutcome($this->target_outcome);

		$copyObj->setLokasi($this->lokasi);

		$copyObj->setJumlahPrev($this->jumlah_prev);

		$copyObj->setJumlahNow($this->jumlah_now);

		$copyObj->setJumlahNext($this->jumlah_next);

		$copyObj->setKodeProgram2($this->kode_program2);

		$copyObj->setKodeUrusan($this->kode_urusan);

		$copyObj->setLastUpdateUser($this->last_update_user);

		$copyObj->setLastUpdateTime($this->last_update_time);

		$copyObj->setLastUpdateIp($this->last_update_ip);

		$copyObj->setTahap($this->tahap);

		$copyObj->setKodeMisi($this->kode_misi);

		$copyObj->setKodeTujuan($this->kode_tujuan);

		$copyObj->setRanking($this->ranking);

		$copyObj->setNomor13($this->nomor13);

		$copyObj->setPpaNama($this->ppa_nama);

		$copyObj->setPpaPangkat($this->ppa_pangkat);

		$copyObj->setPpaNip($this->ppa_nip);

		$copyObj->setLanjutan($this->lanjutan);

		$copyObj->setUserId($this->user_id);

		$copyObj->setTahun($this->tahun);

		$copyObj->setTambahanPagu($this->tambahan_pagu);

		$copyObj->setGender($this->gender);

		$copyObj->setKodeKegKeuangan($this->kode_keg_keuangan);

		$copyObj->setUserIdLama($this->user_id_lama);

		$copyObj->setIndikator($this->indikator);

		$copyObj->setIsDak($this->is_dak);

		$copyObj->setKodeKegiatanAsal($this->kode_kegiatan_asal);

		$copyObj->setKodeKegKeuanganAsal($this->kode_keg_keuangan_asal);

		$copyObj->setThKeMultiyears($this->th_ke_multiyears);

		$copyObj->setKelompokSasaran($this->kelompok_sasaran);

		$copyObj->setPaguBappeko($this->pagu_bappeko);

		$copyObj->setKodeDpa($this->kode_dpa);

		$copyObj->setUserIdPptk($this->user_id_pptk);

		$copyObj->setUserIdKpa($this->user_id_kpa);

		$copyObj->setCatatanPembahasan($this->catatan_pembahasan);

		$copyObj->setIsTapdSetuju($this->is_tapd_setuju);

		$copyObj->setIsBappekoSetuju($this->is_bappeko_setuju);

		$copyObj->setIsPenyeliaSetuju($this->is_penyelia_setuju);

		$copyObj->setIsPernahRka($this->is_pernah_rka);

		$copyObj->setKodeKegiatanBaru($this->kode_kegiatan_baru);

		$copyObj->setCatatanBpkpd($this->catatan_bpkpd);

		$copyObj->setUbahF1Dinas($this->ubah_f1_dinas);

		$copyObj->setUbahF1Peneliti($this->ubah_f1_peneliti);

		$copyObj->setSisaLelangDinas($this->sisa_lelang_dinas);

		$copyObj->setSisaLelangPeneliti($this->sisa_lelang_peneliti);

		$copyObj->setCatatanUbahF1Dinas($this->catatan_ubah_f1_dinas);

		$copyObj->setCatatanSisaLelangPeneliti($this->catatan_sisa_lelang_peneliti);

		$copyObj->setPptkApproval($this->pptk_approval);

		$copyObj->setKpaApproval($this->kpa_approval);

		$copyObj->setCatatanBagianHukum($this->catatan_bagian_hukum);

		$copyObj->setCatatanInspektorat($this->catatan_inspektorat);

		$copyObj->setCatatanBadanKepegawaian($this->catatan_badan_kepegawaian);

		$copyObj->setCatatanLppa($this->catatan_lppa);

		$copyObj->setIsBagianHukumSetuju($this->is_bagian_hukum_setuju);

		$copyObj->setIsInspektoratSetuju($this->is_inspektorat_setuju);

		$copyObj->setIsBadanKepegawaianSetuju($this->is_badan_kepegawaian_setuju);

		$copyObj->setIsLppaSetuju($this->is_lppa_setuju);

		$copyObj->setVerifikasiBpkpd($this->verifikasi_bpkpd);

		$copyObj->setVerifikasiBappeko($this->verifikasi_bappeko);

		$copyObj->setVerifikasiPenyelia($this->verifikasi_penyelia);

		$copyObj->setVerifikasiBagianHukum($this->verifikasi_bagian_hukum);

		$copyObj->setVerifikasiInspektorat($this->verifikasi_inspektorat);

		$copyObj->setVerifikasiBadanKepegawaian($this->verifikasi_badan_kepegawaian);

		$copyObj->setVerifikasiLppa($this->verifikasi_lppa);

		$copyObj->setMetodeCount($this->metode_count);

		$copyObj->setCatatanBagianOrganisasi($this->catatan_bagian_organisasi);

		$copyObj->setIsBagianOrganisasiSetuju($this->is_bagian_organisasi_setuju);

		$copyObj->setVerifikasiBagianOrganisasi($this->verifikasi_bagian_organisasi);

		$copyObj->setCatatanAsisten1($this->catatan_asisten1);

		$copyObj->setIsAsisten1Setuju($this->is_asisten1_setuju);

		$copyObj->setVerifikasiAsisten1($this->verifikasi_asisten1);

		$copyObj->setCatatanAsisten2($this->catatan_asisten2);

		$copyObj->setIsAsisten2Setuju($this->is_asisten2_setuju);

		$copyObj->setVerifikasiAsisten2($this->verifikasi_asisten2);

		$copyObj->setCatatanAsisten3($this->catatan_asisten3);

		$copyObj->setIsAsisten3Setuju($this->is_asisten3_setuju);

		$copyObj->setVerifikasiAsisten3($this->verifikasi_asisten3);

		$copyObj->setCatatanSekda($this->catatan_sekda);

		$copyObj->setIsSekdaSetuju($this->is_sekda_setuju);

		$copyObj->setVerifikasiSekda($this->verifikasi_sekda);

		$copyObj->setVerifikasiAsisten($this->verifikasi_asisten);


		$copyObj->setNew(true);

		$copyObj->setUnitId(NULL); 
		$copyObj->setKodeKegiatan(NULL); 
		$copyObj->setId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new MasterKegiatanPeer();
		}
		return self::$peer;
	}

} 