<?php


abstract class BaseKonektorMusrenbangPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.konektor_musrenbang';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.KonektorMusrenbang';

	
	const NUM_COLUMNS = 7;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const ID = 'ebudget.konektor_musrenbang.ID';

	
	const KODE_DEVPLAN = 'ebudget.konektor_musrenbang.KODE_DEVPLAN';

	
	const NAMA_DEVPLAN = 'ebudget.konektor_musrenbang.NAMA_DEVPLAN';

	
	const SATUAN_DEVPLAN = 'ebudget.konektor_musrenbang.SATUAN_DEVPLAN';

	
	const KOMPONEN_ID = 'ebudget.konektor_musrenbang.KOMPONEN_ID';

	
	const KOMPONEN_REKENING = 'ebudget.konektor_musrenbang.KOMPONEN_REKENING';

	
	const HARGA_DEVPLAN = 'ebudget.konektor_musrenbang.HARGA_DEVPLAN';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('Id', 'KodeDevplan', 'NamaDevplan', 'SatuanDevplan', 'KomponenId', 'KomponenRekening', 'HargaDevplan', ),
		BasePeer::TYPE_COLNAME => array (KonektorMusrenbangPeer::ID, KonektorMusrenbangPeer::KODE_DEVPLAN, KonektorMusrenbangPeer::NAMA_DEVPLAN, KonektorMusrenbangPeer::SATUAN_DEVPLAN, KonektorMusrenbangPeer::KOMPONEN_ID, KonektorMusrenbangPeer::KOMPONEN_REKENING, KonektorMusrenbangPeer::HARGA_DEVPLAN, ),
		BasePeer::TYPE_FIELDNAME => array ('id', 'kode_devplan', 'nama_devplan', 'satuan_devplan', 'komponen_id', 'komponen_rekening', 'harga_devplan', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('Id' => 0, 'KodeDevplan' => 1, 'NamaDevplan' => 2, 'SatuanDevplan' => 3, 'KomponenId' => 4, 'KomponenRekening' => 5, 'HargaDevplan' => 6, ),
		BasePeer::TYPE_COLNAME => array (KonektorMusrenbangPeer::ID => 0, KonektorMusrenbangPeer::KODE_DEVPLAN => 1, KonektorMusrenbangPeer::NAMA_DEVPLAN => 2, KonektorMusrenbangPeer::SATUAN_DEVPLAN => 3, KonektorMusrenbangPeer::KOMPONEN_ID => 4, KonektorMusrenbangPeer::KOMPONEN_REKENING => 5, KonektorMusrenbangPeer::HARGA_DEVPLAN => 6, ),
		BasePeer::TYPE_FIELDNAME => array ('id' => 0, 'kode_devplan' => 1, 'nama_devplan' => 2, 'satuan_devplan' => 3, 'komponen_id' => 4, 'komponen_rekening' => 5, 'harga_devplan' => 6, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/KonektorMusrenbangMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.KonektorMusrenbangMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = KonektorMusrenbangPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(KonektorMusrenbangPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(KonektorMusrenbangPeer::ID);

		$criteria->addSelectColumn(KonektorMusrenbangPeer::KODE_DEVPLAN);

		$criteria->addSelectColumn(KonektorMusrenbangPeer::NAMA_DEVPLAN);

		$criteria->addSelectColumn(KonektorMusrenbangPeer::SATUAN_DEVPLAN);

		$criteria->addSelectColumn(KonektorMusrenbangPeer::KOMPONEN_ID);

		$criteria->addSelectColumn(KonektorMusrenbangPeer::KOMPONEN_REKENING);

		$criteria->addSelectColumn(KonektorMusrenbangPeer::HARGA_DEVPLAN);

	}

	const COUNT = 'COUNT(ebudget.konektor_musrenbang.ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.konektor_musrenbang.ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(KonektorMusrenbangPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(KonektorMusrenbangPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = KonektorMusrenbangPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = KonektorMusrenbangPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return KonektorMusrenbangPeer::populateObjects(KonektorMusrenbangPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			KonektorMusrenbangPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = KonektorMusrenbangPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return KonektorMusrenbangPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(KonektorMusrenbangPeer::ID);
			$selectCriteria->add(KonektorMusrenbangPeer::ID, $criteria->remove(KonektorMusrenbangPeer::ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(KonektorMusrenbangPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(KonektorMusrenbangPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof KonektorMusrenbang) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(KonektorMusrenbangPeer::ID, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(KonektorMusrenbang $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(KonektorMusrenbangPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(KonektorMusrenbangPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(KonektorMusrenbangPeer::DATABASE_NAME, KonektorMusrenbangPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = KonektorMusrenbangPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(KonektorMusrenbangPeer::DATABASE_NAME);

		$criteria->add(KonektorMusrenbangPeer::ID, $pk);


		$v = KonektorMusrenbangPeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(KonektorMusrenbangPeer::ID, $pks, Criteria::IN);
			$objs = KonektorMusrenbangPeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BaseKonektorMusrenbangPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/KonektorMusrenbangMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.KonektorMusrenbangMapBuilder');
}
