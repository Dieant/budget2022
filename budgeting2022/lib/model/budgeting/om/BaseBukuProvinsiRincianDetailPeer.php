<?php


abstract class BaseBukuProvinsiRincianDetailPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.buku_provinsi_rincian_detail';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.BukuProvinsiRincianDetail';

	
	const NUM_COLUMNS = 53;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const KEGIATAN_CODE = 'ebudget.buku_provinsi_rincian_detail.KEGIATAN_CODE';

	
	const TIPE = 'ebudget.buku_provinsi_rincian_detail.TIPE';

	
	const DETAIL_NO = 'ebudget.buku_provinsi_rincian_detail.DETAIL_NO';

	
	const REKENING_CODE = 'ebudget.buku_provinsi_rincian_detail.REKENING_CODE';

	
	const KOMPONEN_ID = 'ebudget.buku_provinsi_rincian_detail.KOMPONEN_ID';

	
	const DETAIL_NAME = 'ebudget.buku_provinsi_rincian_detail.DETAIL_NAME';

	
	const VOLUME = 'ebudget.buku_provinsi_rincian_detail.VOLUME';

	
	const KETERANGAN_KOEFISIEN = 'ebudget.buku_provinsi_rincian_detail.KETERANGAN_KOEFISIEN';

	
	const SUBTITLE = 'ebudget.buku_provinsi_rincian_detail.SUBTITLE';

	
	const KOMPONEN_HARGA = 'ebudget.buku_provinsi_rincian_detail.KOMPONEN_HARGA';

	
	const KOMPONEN_HARGA_AWAL = 'ebudget.buku_provinsi_rincian_detail.KOMPONEN_HARGA_AWAL';

	
	const KOMPONEN_NAME = 'ebudget.buku_provinsi_rincian_detail.KOMPONEN_NAME';

	
	const SATUAN = 'ebudget.buku_provinsi_rincian_detail.SATUAN';

	
	const PAJAK = 'ebudget.buku_provinsi_rincian_detail.PAJAK';

	
	const UNIT_ID = 'ebudget.buku_provinsi_rincian_detail.UNIT_ID';

	
	const FROM_SUB_KEGIATAN = 'ebudget.buku_provinsi_rincian_detail.FROM_SUB_KEGIATAN';

	
	const SUB = 'ebudget.buku_provinsi_rincian_detail.SUB';

	
	const KODE_SUB = 'ebudget.buku_provinsi_rincian_detail.KODE_SUB';

	
	const LAST_UPDATE_USER = 'ebudget.buku_provinsi_rincian_detail.LAST_UPDATE_USER';

	
	const LAST_UPDATE_TIME = 'ebudget.buku_provinsi_rincian_detail.LAST_UPDATE_TIME';

	
	const LAST_UPDATE_IP = 'ebudget.buku_provinsi_rincian_detail.LAST_UPDATE_IP';

	
	const TAHAP = 'ebudget.buku_provinsi_rincian_detail.TAHAP';

	
	const TAHAP_EDIT = 'ebudget.buku_provinsi_rincian_detail.TAHAP_EDIT';

	
	const TAHAP_NEW = 'ebudget.buku_provinsi_rincian_detail.TAHAP_NEW';

	
	const STATUS_LELANG = 'ebudget.buku_provinsi_rincian_detail.STATUS_LELANG';

	
	const NOMOR_LELANG = 'ebudget.buku_provinsi_rincian_detail.NOMOR_LELANG';

	
	const KOEFISIEN_SEMULA = 'ebudget.buku_provinsi_rincian_detail.KOEFISIEN_SEMULA';

	
	const VOLUME_SEMULA = 'ebudget.buku_provinsi_rincian_detail.VOLUME_SEMULA';

	
	const HARGA_SEMULA = 'ebudget.buku_provinsi_rincian_detail.HARGA_SEMULA';

	
	const TOTAL_SEMULA = 'ebudget.buku_provinsi_rincian_detail.TOTAL_SEMULA';

	
	const LOCK_SUBTITLE = 'ebudget.buku_provinsi_rincian_detail.LOCK_SUBTITLE';

	
	const STATUS_HAPUS = 'ebudget.buku_provinsi_rincian_detail.STATUS_HAPUS';

	
	const TAHUN = 'ebudget.buku_provinsi_rincian_detail.TAHUN';

	
	const KODE_LOKASI = 'ebudget.buku_provinsi_rincian_detail.KODE_LOKASI';

	
	const KECAMATAN = 'ebudget.buku_provinsi_rincian_detail.KECAMATAN';

	
	const REKENING_CODE_ASLI = 'ebudget.buku_provinsi_rincian_detail.REKENING_CODE_ASLI';

	
	const NOTE_SKPD = 'ebudget.buku_provinsi_rincian_detail.NOTE_SKPD';

	
	const NOTE_PENELITI = 'ebudget.buku_provinsi_rincian_detail.NOTE_PENELITI';

	
	const NILAI_ANGGARAN = 'ebudget.buku_provinsi_rincian_detail.NILAI_ANGGARAN';

	
	const IS_BLUD = 'ebudget.buku_provinsi_rincian_detail.IS_BLUD';

	
	const LOKASI_KECAMATAN = 'ebudget.buku_provinsi_rincian_detail.LOKASI_KECAMATAN';

	
	const LOKASI_KELURAHAN = 'ebudget.buku_provinsi_rincian_detail.LOKASI_KELURAHAN';

	
	const OB = 'ebudget.buku_provinsi_rincian_detail.OB';

	
	const OB_FROM_ID = 'ebudget.buku_provinsi_rincian_detail.OB_FROM_ID';

	
	const IS_PER_KOMPONEN = 'ebudget.buku_provinsi_rincian_detail.IS_PER_KOMPONEN';

	
	const KEGIATAN_CODE_ASAL = 'ebudget.buku_provinsi_rincian_detail.KEGIATAN_CODE_ASAL';

	
	const TH_KE_MULTIYEARS = 'ebudget.buku_provinsi_rincian_detail.TH_KE_MULTIYEARS';

	
	const HARGA_SEBELUM_SISA_LELANG = 'ebudget.buku_provinsi_rincian_detail.HARGA_SEBELUM_SISA_LELANG';

	
	const IS_MUSRENBANG = 'ebudget.buku_provinsi_rincian_detail.IS_MUSRENBANG';

	
	const SUB_ID_ASAL = 'ebudget.buku_provinsi_rincian_detail.SUB_ID_ASAL';

	
	const SUBTITLE_ASAL = 'ebudget.buku_provinsi_rincian_detail.SUBTITLE_ASAL';

	
	const KODE_SUB_ASAL = 'ebudget.buku_provinsi_rincian_detail.KODE_SUB_ASAL';

	
	const SUB_ASAL = 'ebudget.buku_provinsi_rincian_detail.SUB_ASAL';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('KegiatanCode', 'Tipe', 'DetailNo', 'RekeningCode', 'KomponenId', 'DetailName', 'Volume', 'KeteranganKoefisien', 'Subtitle', 'KomponenHarga', 'KomponenHargaAwal', 'KomponenName', 'Satuan', 'Pajak', 'UnitId', 'FromSubKegiatan', 'Sub', 'KodeSub', 'LastUpdateUser', 'LastUpdateTime', 'LastUpdateIp', 'Tahap', 'TahapEdit', 'TahapNew', 'StatusLelang', 'NomorLelang', 'KoefisienSemula', 'VolumeSemula', 'HargaSemula', 'TotalSemula', 'LockSubtitle', 'StatusHapus', 'Tahun', 'KodeLokasi', 'Kecamatan', 'RekeningCodeAsli', 'NoteSkpd', 'NotePeneliti', 'NilaiAnggaran', 'IsBlud', 'LokasiKecamatan', 'LokasiKelurahan', 'Ob', 'ObFromId', 'IsPerKomponen', 'KegiatanCodeAsal', 'ThKeMultiyears', 'HargaSebelumSisaLelang', 'IsMusrenbang', 'SubIdAsal', 'SubtitleAsal', 'KodeSubAsal', 'SubAsal', ),
		BasePeer::TYPE_COLNAME => array (BukuProvinsiRincianDetailPeer::KEGIATAN_CODE, BukuProvinsiRincianDetailPeer::TIPE, BukuProvinsiRincianDetailPeer::DETAIL_NO, BukuProvinsiRincianDetailPeer::REKENING_CODE, BukuProvinsiRincianDetailPeer::KOMPONEN_ID, BukuProvinsiRincianDetailPeer::DETAIL_NAME, BukuProvinsiRincianDetailPeer::VOLUME, BukuProvinsiRincianDetailPeer::KETERANGAN_KOEFISIEN, BukuProvinsiRincianDetailPeer::SUBTITLE, BukuProvinsiRincianDetailPeer::KOMPONEN_HARGA, BukuProvinsiRincianDetailPeer::KOMPONEN_HARGA_AWAL, BukuProvinsiRincianDetailPeer::KOMPONEN_NAME, BukuProvinsiRincianDetailPeer::SATUAN, BukuProvinsiRincianDetailPeer::PAJAK, BukuProvinsiRincianDetailPeer::UNIT_ID, BukuProvinsiRincianDetailPeer::FROM_SUB_KEGIATAN, BukuProvinsiRincianDetailPeer::SUB, BukuProvinsiRincianDetailPeer::KODE_SUB, BukuProvinsiRincianDetailPeer::LAST_UPDATE_USER, BukuProvinsiRincianDetailPeer::LAST_UPDATE_TIME, BukuProvinsiRincianDetailPeer::LAST_UPDATE_IP, BukuProvinsiRincianDetailPeer::TAHAP, BukuProvinsiRincianDetailPeer::TAHAP_EDIT, BukuProvinsiRincianDetailPeer::TAHAP_NEW, BukuProvinsiRincianDetailPeer::STATUS_LELANG, BukuProvinsiRincianDetailPeer::NOMOR_LELANG, BukuProvinsiRincianDetailPeer::KOEFISIEN_SEMULA, BukuProvinsiRincianDetailPeer::VOLUME_SEMULA, BukuProvinsiRincianDetailPeer::HARGA_SEMULA, BukuProvinsiRincianDetailPeer::TOTAL_SEMULA, BukuProvinsiRincianDetailPeer::LOCK_SUBTITLE, BukuProvinsiRincianDetailPeer::STATUS_HAPUS, BukuProvinsiRincianDetailPeer::TAHUN, BukuProvinsiRincianDetailPeer::KODE_LOKASI, BukuProvinsiRincianDetailPeer::KECAMATAN, BukuProvinsiRincianDetailPeer::REKENING_CODE_ASLI, BukuProvinsiRincianDetailPeer::NOTE_SKPD, BukuProvinsiRincianDetailPeer::NOTE_PENELITI, BukuProvinsiRincianDetailPeer::NILAI_ANGGARAN, BukuProvinsiRincianDetailPeer::IS_BLUD, BukuProvinsiRincianDetailPeer::LOKASI_KECAMATAN, BukuProvinsiRincianDetailPeer::LOKASI_KELURAHAN, BukuProvinsiRincianDetailPeer::OB, BukuProvinsiRincianDetailPeer::OB_FROM_ID, BukuProvinsiRincianDetailPeer::IS_PER_KOMPONEN, BukuProvinsiRincianDetailPeer::KEGIATAN_CODE_ASAL, BukuProvinsiRincianDetailPeer::TH_KE_MULTIYEARS, BukuProvinsiRincianDetailPeer::HARGA_SEBELUM_SISA_LELANG, BukuProvinsiRincianDetailPeer::IS_MUSRENBANG, BukuProvinsiRincianDetailPeer::SUB_ID_ASAL, BukuProvinsiRincianDetailPeer::SUBTITLE_ASAL, BukuProvinsiRincianDetailPeer::KODE_SUB_ASAL, BukuProvinsiRincianDetailPeer::SUB_ASAL, ),
		BasePeer::TYPE_FIELDNAME => array ('kegiatan_code', 'tipe', 'detail_no', 'rekening_code', 'komponen_id', 'detail_name', 'volume', 'keterangan_koefisien', 'subtitle', 'komponen_harga', 'komponen_harga_awal', 'komponen_name', 'satuan', 'pajak', 'unit_id', 'from_sub_kegiatan', 'sub', 'kode_sub', 'last_update_user', 'last_update_time', 'last_update_ip', 'tahap', 'tahap_edit', 'tahap_new', 'status_lelang', 'nomor_lelang', 'koefisien_semula', 'volume_semula', 'harga_semula', 'total_semula', 'lock_subtitle', 'status_hapus', 'tahun', 'kode_lokasi', 'kecamatan', 'rekening_code_asli', 'note_skpd', 'note_peneliti', 'nilai_anggaran', 'is_blud', 'lokasi_kecamatan', 'lokasi_kelurahan', 'ob', 'ob_from_id', 'is_per_komponen', 'kegiatan_code_asal', 'th_ke_multiyears', 'harga_sebelum_sisa_lelang', 'is_musrenbang', 'sub_id_asal', 'subtitle_asal', 'kode_sub_asal', 'sub_asal', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('KegiatanCode' => 0, 'Tipe' => 1, 'DetailNo' => 2, 'RekeningCode' => 3, 'KomponenId' => 4, 'DetailName' => 5, 'Volume' => 6, 'KeteranganKoefisien' => 7, 'Subtitle' => 8, 'KomponenHarga' => 9, 'KomponenHargaAwal' => 10, 'KomponenName' => 11, 'Satuan' => 12, 'Pajak' => 13, 'UnitId' => 14, 'FromSubKegiatan' => 15, 'Sub' => 16, 'KodeSub' => 17, 'LastUpdateUser' => 18, 'LastUpdateTime' => 19, 'LastUpdateIp' => 20, 'Tahap' => 21, 'TahapEdit' => 22, 'TahapNew' => 23, 'StatusLelang' => 24, 'NomorLelang' => 25, 'KoefisienSemula' => 26, 'VolumeSemula' => 27, 'HargaSemula' => 28, 'TotalSemula' => 29, 'LockSubtitle' => 30, 'StatusHapus' => 31, 'Tahun' => 32, 'KodeLokasi' => 33, 'Kecamatan' => 34, 'RekeningCodeAsli' => 35, 'NoteSkpd' => 36, 'NotePeneliti' => 37, 'NilaiAnggaran' => 38, 'IsBlud' => 39, 'LokasiKecamatan' => 40, 'LokasiKelurahan' => 41, 'Ob' => 42, 'ObFromId' => 43, 'IsPerKomponen' => 44, 'KegiatanCodeAsal' => 45, 'ThKeMultiyears' => 46, 'HargaSebelumSisaLelang' => 47, 'IsMusrenbang' => 48, 'SubIdAsal' => 49, 'SubtitleAsal' => 50, 'KodeSubAsal' => 51, 'SubAsal' => 52, ),
		BasePeer::TYPE_COLNAME => array (BukuProvinsiRincianDetailPeer::KEGIATAN_CODE => 0, BukuProvinsiRincianDetailPeer::TIPE => 1, BukuProvinsiRincianDetailPeer::DETAIL_NO => 2, BukuProvinsiRincianDetailPeer::REKENING_CODE => 3, BukuProvinsiRincianDetailPeer::KOMPONEN_ID => 4, BukuProvinsiRincianDetailPeer::DETAIL_NAME => 5, BukuProvinsiRincianDetailPeer::VOLUME => 6, BukuProvinsiRincianDetailPeer::KETERANGAN_KOEFISIEN => 7, BukuProvinsiRincianDetailPeer::SUBTITLE => 8, BukuProvinsiRincianDetailPeer::KOMPONEN_HARGA => 9, BukuProvinsiRincianDetailPeer::KOMPONEN_HARGA_AWAL => 10, BukuProvinsiRincianDetailPeer::KOMPONEN_NAME => 11, BukuProvinsiRincianDetailPeer::SATUAN => 12, BukuProvinsiRincianDetailPeer::PAJAK => 13, BukuProvinsiRincianDetailPeer::UNIT_ID => 14, BukuProvinsiRincianDetailPeer::FROM_SUB_KEGIATAN => 15, BukuProvinsiRincianDetailPeer::SUB => 16, BukuProvinsiRincianDetailPeer::KODE_SUB => 17, BukuProvinsiRincianDetailPeer::LAST_UPDATE_USER => 18, BukuProvinsiRincianDetailPeer::LAST_UPDATE_TIME => 19, BukuProvinsiRincianDetailPeer::LAST_UPDATE_IP => 20, BukuProvinsiRincianDetailPeer::TAHAP => 21, BukuProvinsiRincianDetailPeer::TAHAP_EDIT => 22, BukuProvinsiRincianDetailPeer::TAHAP_NEW => 23, BukuProvinsiRincianDetailPeer::STATUS_LELANG => 24, BukuProvinsiRincianDetailPeer::NOMOR_LELANG => 25, BukuProvinsiRincianDetailPeer::KOEFISIEN_SEMULA => 26, BukuProvinsiRincianDetailPeer::VOLUME_SEMULA => 27, BukuProvinsiRincianDetailPeer::HARGA_SEMULA => 28, BukuProvinsiRincianDetailPeer::TOTAL_SEMULA => 29, BukuProvinsiRincianDetailPeer::LOCK_SUBTITLE => 30, BukuProvinsiRincianDetailPeer::STATUS_HAPUS => 31, BukuProvinsiRincianDetailPeer::TAHUN => 32, BukuProvinsiRincianDetailPeer::KODE_LOKASI => 33, BukuProvinsiRincianDetailPeer::KECAMATAN => 34, BukuProvinsiRincianDetailPeer::REKENING_CODE_ASLI => 35, BukuProvinsiRincianDetailPeer::NOTE_SKPD => 36, BukuProvinsiRincianDetailPeer::NOTE_PENELITI => 37, BukuProvinsiRincianDetailPeer::NILAI_ANGGARAN => 38, BukuProvinsiRincianDetailPeer::IS_BLUD => 39, BukuProvinsiRincianDetailPeer::LOKASI_KECAMATAN => 40, BukuProvinsiRincianDetailPeer::LOKASI_KELURAHAN => 41, BukuProvinsiRincianDetailPeer::OB => 42, BukuProvinsiRincianDetailPeer::OB_FROM_ID => 43, BukuProvinsiRincianDetailPeer::IS_PER_KOMPONEN => 44, BukuProvinsiRincianDetailPeer::KEGIATAN_CODE_ASAL => 45, BukuProvinsiRincianDetailPeer::TH_KE_MULTIYEARS => 46, BukuProvinsiRincianDetailPeer::HARGA_SEBELUM_SISA_LELANG => 47, BukuProvinsiRincianDetailPeer::IS_MUSRENBANG => 48, BukuProvinsiRincianDetailPeer::SUB_ID_ASAL => 49, BukuProvinsiRincianDetailPeer::SUBTITLE_ASAL => 50, BukuProvinsiRincianDetailPeer::KODE_SUB_ASAL => 51, BukuProvinsiRincianDetailPeer::SUB_ASAL => 52, ),
		BasePeer::TYPE_FIELDNAME => array ('kegiatan_code' => 0, 'tipe' => 1, 'detail_no' => 2, 'rekening_code' => 3, 'komponen_id' => 4, 'detail_name' => 5, 'volume' => 6, 'keterangan_koefisien' => 7, 'subtitle' => 8, 'komponen_harga' => 9, 'komponen_harga_awal' => 10, 'komponen_name' => 11, 'satuan' => 12, 'pajak' => 13, 'unit_id' => 14, 'from_sub_kegiatan' => 15, 'sub' => 16, 'kode_sub' => 17, 'last_update_user' => 18, 'last_update_time' => 19, 'last_update_ip' => 20, 'tahap' => 21, 'tahap_edit' => 22, 'tahap_new' => 23, 'status_lelang' => 24, 'nomor_lelang' => 25, 'koefisien_semula' => 26, 'volume_semula' => 27, 'harga_semula' => 28, 'total_semula' => 29, 'lock_subtitle' => 30, 'status_hapus' => 31, 'tahun' => 32, 'kode_lokasi' => 33, 'kecamatan' => 34, 'rekening_code_asli' => 35, 'note_skpd' => 36, 'note_peneliti' => 37, 'nilai_anggaran' => 38, 'is_blud' => 39, 'lokasi_kecamatan' => 40, 'lokasi_kelurahan' => 41, 'ob' => 42, 'ob_from_id' => 43, 'is_per_komponen' => 44, 'kegiatan_code_asal' => 45, 'th_ke_multiyears' => 46, 'harga_sebelum_sisa_lelang' => 47, 'is_musrenbang' => 48, 'sub_id_asal' => 49, 'subtitle_asal' => 50, 'kode_sub_asal' => 51, 'sub_asal' => 52, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/BukuProvinsiRincianDetailMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.BukuProvinsiRincianDetailMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = BukuProvinsiRincianDetailPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(BukuProvinsiRincianDetailPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(BukuProvinsiRincianDetailPeer::KEGIATAN_CODE);

		$criteria->addSelectColumn(BukuProvinsiRincianDetailPeer::TIPE);

		$criteria->addSelectColumn(BukuProvinsiRincianDetailPeer::DETAIL_NO);

		$criteria->addSelectColumn(BukuProvinsiRincianDetailPeer::REKENING_CODE);

		$criteria->addSelectColumn(BukuProvinsiRincianDetailPeer::KOMPONEN_ID);

		$criteria->addSelectColumn(BukuProvinsiRincianDetailPeer::DETAIL_NAME);

		$criteria->addSelectColumn(BukuProvinsiRincianDetailPeer::VOLUME);

		$criteria->addSelectColumn(BukuProvinsiRincianDetailPeer::KETERANGAN_KOEFISIEN);

		$criteria->addSelectColumn(BukuProvinsiRincianDetailPeer::SUBTITLE);

		$criteria->addSelectColumn(BukuProvinsiRincianDetailPeer::KOMPONEN_HARGA);

		$criteria->addSelectColumn(BukuProvinsiRincianDetailPeer::KOMPONEN_HARGA_AWAL);

		$criteria->addSelectColumn(BukuProvinsiRincianDetailPeer::KOMPONEN_NAME);

		$criteria->addSelectColumn(BukuProvinsiRincianDetailPeer::SATUAN);

		$criteria->addSelectColumn(BukuProvinsiRincianDetailPeer::PAJAK);

		$criteria->addSelectColumn(BukuProvinsiRincianDetailPeer::UNIT_ID);

		$criteria->addSelectColumn(BukuProvinsiRincianDetailPeer::FROM_SUB_KEGIATAN);

		$criteria->addSelectColumn(BukuProvinsiRincianDetailPeer::SUB);

		$criteria->addSelectColumn(BukuProvinsiRincianDetailPeer::KODE_SUB);

		$criteria->addSelectColumn(BukuProvinsiRincianDetailPeer::LAST_UPDATE_USER);

		$criteria->addSelectColumn(BukuProvinsiRincianDetailPeer::LAST_UPDATE_TIME);

		$criteria->addSelectColumn(BukuProvinsiRincianDetailPeer::LAST_UPDATE_IP);

		$criteria->addSelectColumn(BukuProvinsiRincianDetailPeer::TAHAP);

		$criteria->addSelectColumn(BukuProvinsiRincianDetailPeer::TAHAP_EDIT);

		$criteria->addSelectColumn(BukuProvinsiRincianDetailPeer::TAHAP_NEW);

		$criteria->addSelectColumn(BukuProvinsiRincianDetailPeer::STATUS_LELANG);

		$criteria->addSelectColumn(BukuProvinsiRincianDetailPeer::NOMOR_LELANG);

		$criteria->addSelectColumn(BukuProvinsiRincianDetailPeer::KOEFISIEN_SEMULA);

		$criteria->addSelectColumn(BukuProvinsiRincianDetailPeer::VOLUME_SEMULA);

		$criteria->addSelectColumn(BukuProvinsiRincianDetailPeer::HARGA_SEMULA);

		$criteria->addSelectColumn(BukuProvinsiRincianDetailPeer::TOTAL_SEMULA);

		$criteria->addSelectColumn(BukuProvinsiRincianDetailPeer::LOCK_SUBTITLE);

		$criteria->addSelectColumn(BukuProvinsiRincianDetailPeer::STATUS_HAPUS);

		$criteria->addSelectColumn(BukuProvinsiRincianDetailPeer::TAHUN);

		$criteria->addSelectColumn(BukuProvinsiRincianDetailPeer::KODE_LOKASI);

		$criteria->addSelectColumn(BukuProvinsiRincianDetailPeer::KECAMATAN);

		$criteria->addSelectColumn(BukuProvinsiRincianDetailPeer::REKENING_CODE_ASLI);

		$criteria->addSelectColumn(BukuProvinsiRincianDetailPeer::NOTE_SKPD);

		$criteria->addSelectColumn(BukuProvinsiRincianDetailPeer::NOTE_PENELITI);

		$criteria->addSelectColumn(BukuProvinsiRincianDetailPeer::NILAI_ANGGARAN);

		$criteria->addSelectColumn(BukuProvinsiRincianDetailPeer::IS_BLUD);

		$criteria->addSelectColumn(BukuProvinsiRincianDetailPeer::LOKASI_KECAMATAN);

		$criteria->addSelectColumn(BukuProvinsiRincianDetailPeer::LOKASI_KELURAHAN);

		$criteria->addSelectColumn(BukuProvinsiRincianDetailPeer::OB);

		$criteria->addSelectColumn(BukuProvinsiRincianDetailPeer::OB_FROM_ID);

		$criteria->addSelectColumn(BukuProvinsiRincianDetailPeer::IS_PER_KOMPONEN);

		$criteria->addSelectColumn(BukuProvinsiRincianDetailPeer::KEGIATAN_CODE_ASAL);

		$criteria->addSelectColumn(BukuProvinsiRincianDetailPeer::TH_KE_MULTIYEARS);

		$criteria->addSelectColumn(BukuProvinsiRincianDetailPeer::HARGA_SEBELUM_SISA_LELANG);

		$criteria->addSelectColumn(BukuProvinsiRincianDetailPeer::IS_MUSRENBANG);

		$criteria->addSelectColumn(BukuProvinsiRincianDetailPeer::SUB_ID_ASAL);

		$criteria->addSelectColumn(BukuProvinsiRincianDetailPeer::SUBTITLE_ASAL);

		$criteria->addSelectColumn(BukuProvinsiRincianDetailPeer::KODE_SUB_ASAL);

		$criteria->addSelectColumn(BukuProvinsiRincianDetailPeer::SUB_ASAL);

	}

	const COUNT = 'COUNT(ebudget.buku_provinsi_rincian_detail.KEGIATAN_CODE)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.buku_provinsi_rincian_detail.KEGIATAN_CODE)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(BukuProvinsiRincianDetailPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(BukuProvinsiRincianDetailPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = BukuProvinsiRincianDetailPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = BukuProvinsiRincianDetailPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return BukuProvinsiRincianDetailPeer::populateObjects(BukuProvinsiRincianDetailPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			BukuProvinsiRincianDetailPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = BukuProvinsiRincianDetailPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return BukuProvinsiRincianDetailPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(BukuProvinsiRincianDetailPeer::KEGIATAN_CODE);
			$selectCriteria->add(BukuProvinsiRincianDetailPeer::KEGIATAN_CODE, $criteria->remove(BukuProvinsiRincianDetailPeer::KEGIATAN_CODE), $comparison);

			$comparison = $criteria->getComparison(BukuProvinsiRincianDetailPeer::DETAIL_NO);
			$selectCriteria->add(BukuProvinsiRincianDetailPeer::DETAIL_NO, $criteria->remove(BukuProvinsiRincianDetailPeer::DETAIL_NO), $comparison);

			$comparison = $criteria->getComparison(BukuProvinsiRincianDetailPeer::UNIT_ID);
			$selectCriteria->add(BukuProvinsiRincianDetailPeer::UNIT_ID, $criteria->remove(BukuProvinsiRincianDetailPeer::UNIT_ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(BukuProvinsiRincianDetailPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(BukuProvinsiRincianDetailPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof BukuProvinsiRincianDetail) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
												if(count($values) == count($values, COUNT_RECURSIVE))
			{
								$values = array($values);
			}
			$vals = array();
			foreach($values as $value)
			{

				$vals[0][] = $value[0];
				$vals[1][] = $value[1];
				$vals[2][] = $value[2];
			}

			$criteria->add(BukuProvinsiRincianDetailPeer::KEGIATAN_CODE, $vals[0], Criteria::IN);
			$criteria->add(BukuProvinsiRincianDetailPeer::DETAIL_NO, $vals[1], Criteria::IN);
			$criteria->add(BukuProvinsiRincianDetailPeer::UNIT_ID, $vals[2], Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(BukuProvinsiRincianDetail $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(BukuProvinsiRincianDetailPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(BukuProvinsiRincianDetailPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(BukuProvinsiRincianDetailPeer::DATABASE_NAME, BukuProvinsiRincianDetailPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = BukuProvinsiRincianDetailPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK( $kegiatan_code, $detail_no, $unit_id, $con = null) {
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$criteria = new Criteria();
		$criteria->add(BukuProvinsiRincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
		$criteria->add(BukuProvinsiRincianDetailPeer::DETAIL_NO, $detail_no);
		$criteria->add(BukuProvinsiRincianDetailPeer::UNIT_ID, $unit_id);
		$v = BukuProvinsiRincianDetailPeer::doSelect($criteria, $con);

		return !empty($v) ? $v[0] : null;
	}
} 
if (Propel::isInit()) {
			try {
		BaseBukuProvinsiRincianDetailPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/BukuProvinsiRincianDetailMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.BukuProvinsiRincianDetailMapBuilder');
}
