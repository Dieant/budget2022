<?php


abstract class BaseMasterSasaran extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $kode_sasaran;


	
	protected $nama_sasaran;


	
	protected $kode_misi;


	
	protected $kode_tujuan;


	
	protected $indikator_sasaran;


	
	protected $target;


	
	protected $id;


	
	protected $kode_sasaran2;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getKodeSasaran()
	{

		return $this->kode_sasaran;
	}

	
	public function getNamaSasaran()
	{

		return $this->nama_sasaran;
	}

	
	public function getKodeMisi()
	{

		return $this->kode_misi;
	}

	
	public function getKodeTujuan()
	{

		return $this->kode_tujuan;
	}

	
	public function getIndikatorSasaran()
	{

		return $this->indikator_sasaran;
	}

	
	public function getTarget()
	{

		return $this->target;
	}

	
	public function getId()
	{

		return $this->id;
	}

	
	public function getKodeSasaran2()
	{

		return $this->kode_sasaran2;
	}

	
	public function setKodeSasaran($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_sasaran !== $v) {
			$this->kode_sasaran = $v;
			$this->modifiedColumns[] = MasterSasaranPeer::KODE_SASARAN;
		}

	} 
	
	public function setNamaSasaran($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nama_sasaran !== $v) {
			$this->nama_sasaran = $v;
			$this->modifiedColumns[] = MasterSasaranPeer::NAMA_SASARAN;
		}

	} 
	
	public function setKodeMisi($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_misi !== $v) {
			$this->kode_misi = $v;
			$this->modifiedColumns[] = MasterSasaranPeer::KODE_MISI;
		}

	} 
	
	public function setKodeTujuan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_tujuan !== $v) {
			$this->kode_tujuan = $v;
			$this->modifiedColumns[] = MasterSasaranPeer::KODE_TUJUAN;
		}

	} 
	
	public function setIndikatorSasaran($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->indikator_sasaran !== $v) {
			$this->indikator_sasaran = $v;
			$this->modifiedColumns[] = MasterSasaranPeer::INDIKATOR_SASARAN;
		}

	} 
	
	public function setTarget($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->target !== $v) {
			$this->target = $v;
			$this->modifiedColumns[] = MasterSasaranPeer::TARGET;
		}

	} 
	
	public function setId($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = MasterSasaranPeer::ID;
		}

	} 
	
	public function setKodeSasaran2($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_sasaran2 !== $v) {
			$this->kode_sasaran2 = $v;
			$this->modifiedColumns[] = MasterSasaranPeer::KODE_SASARAN2;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->kode_sasaran = $rs->getString($startcol + 0);

			$this->nama_sasaran = $rs->getString($startcol + 1);

			$this->kode_misi = $rs->getString($startcol + 2);

			$this->kode_tujuan = $rs->getString($startcol + 3);

			$this->indikator_sasaran = $rs->getString($startcol + 4);

			$this->target = $rs->getString($startcol + 5);

			$this->id = $rs->getInt($startcol + 6);

			$this->kode_sasaran2 = $rs->getString($startcol + 7);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 8; 
		} catch (Exception $e) {
			throw new PropelException("Error populating MasterSasaran object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(MasterSasaranPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			MasterSasaranPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(MasterSasaranPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = MasterSasaranPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setId($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += MasterSasaranPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = MasterSasaranPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = MasterSasaranPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getKodeSasaran();
				break;
			case 1:
				return $this->getNamaSasaran();
				break;
			case 2:
				return $this->getKodeMisi();
				break;
			case 3:
				return $this->getKodeTujuan();
				break;
			case 4:
				return $this->getIndikatorSasaran();
				break;
			case 5:
				return $this->getTarget();
				break;
			case 6:
				return $this->getId();
				break;
			case 7:
				return $this->getKodeSasaran2();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = MasterSasaranPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getKodeSasaran(),
			$keys[1] => $this->getNamaSasaran(),
			$keys[2] => $this->getKodeMisi(),
			$keys[3] => $this->getKodeTujuan(),
			$keys[4] => $this->getIndikatorSasaran(),
			$keys[5] => $this->getTarget(),
			$keys[6] => $this->getId(),
			$keys[7] => $this->getKodeSasaran2(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = MasterSasaranPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setKodeSasaran($value);
				break;
			case 1:
				$this->setNamaSasaran($value);
				break;
			case 2:
				$this->setKodeMisi($value);
				break;
			case 3:
				$this->setKodeTujuan($value);
				break;
			case 4:
				$this->setIndikatorSasaran($value);
				break;
			case 5:
				$this->setTarget($value);
				break;
			case 6:
				$this->setId($value);
				break;
			case 7:
				$this->setKodeSasaran2($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = MasterSasaranPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setKodeSasaran($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setNamaSasaran($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setKodeMisi($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setKodeTujuan($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setIndikatorSasaran($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setTarget($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setId($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setKodeSasaran2($arr[$keys[7]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(MasterSasaranPeer::DATABASE_NAME);

		if ($this->isColumnModified(MasterSasaranPeer::KODE_SASARAN)) $criteria->add(MasterSasaranPeer::KODE_SASARAN, $this->kode_sasaran);
		if ($this->isColumnModified(MasterSasaranPeer::NAMA_SASARAN)) $criteria->add(MasterSasaranPeer::NAMA_SASARAN, $this->nama_sasaran);
		if ($this->isColumnModified(MasterSasaranPeer::KODE_MISI)) $criteria->add(MasterSasaranPeer::KODE_MISI, $this->kode_misi);
		if ($this->isColumnModified(MasterSasaranPeer::KODE_TUJUAN)) $criteria->add(MasterSasaranPeer::KODE_TUJUAN, $this->kode_tujuan);
		if ($this->isColumnModified(MasterSasaranPeer::INDIKATOR_SASARAN)) $criteria->add(MasterSasaranPeer::INDIKATOR_SASARAN, $this->indikator_sasaran);
		if ($this->isColumnModified(MasterSasaranPeer::TARGET)) $criteria->add(MasterSasaranPeer::TARGET, $this->target);
		if ($this->isColumnModified(MasterSasaranPeer::ID)) $criteria->add(MasterSasaranPeer::ID, $this->id);
		if ($this->isColumnModified(MasterSasaranPeer::KODE_SASARAN2)) $criteria->add(MasterSasaranPeer::KODE_SASARAN2, $this->kode_sasaran2);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(MasterSasaranPeer::DATABASE_NAME);

		$criteria->add(MasterSasaranPeer::ID, $this->id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setKodeSasaran($this->kode_sasaran);

		$copyObj->setNamaSasaran($this->nama_sasaran);

		$copyObj->setKodeMisi($this->kode_misi);

		$copyObj->setKodeTujuan($this->kode_tujuan);

		$copyObj->setIndikatorSasaran($this->indikator_sasaran);

		$copyObj->setTarget($this->target);

		$copyObj->setKodeSasaran2($this->kode_sasaran2);


		$copyObj->setNew(true);

		$copyObj->setId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new MasterSasaranPeer();
		}
		return self::$peer;
	}

} 