<?php


abstract class BasePakRincianDetailPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.pak_rincian_detail';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.PakRincianDetail';

	
	const NUM_COLUMNS = 36;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const KEGIATAN_CODE = 'ebudget.pak_rincian_detail.KEGIATAN_CODE';

	
	const TIPE = 'ebudget.pak_rincian_detail.TIPE';

	
	const DETAIL_NO = 'ebudget.pak_rincian_detail.DETAIL_NO';

	
	const REKENING_CODE = 'ebudget.pak_rincian_detail.REKENING_CODE';

	
	const KOMPONEN_ID = 'ebudget.pak_rincian_detail.KOMPONEN_ID';

	
	const DETAIL_NAME = 'ebudget.pak_rincian_detail.DETAIL_NAME';

	
	const VOLUME = 'ebudget.pak_rincian_detail.VOLUME';

	
	const KETERANGAN_KOEFISIEN = 'ebudget.pak_rincian_detail.KETERANGAN_KOEFISIEN';

	
	const SUBTITLE = 'ebudget.pak_rincian_detail.SUBTITLE';

	
	const KOMPONEN_HARGA = 'ebudget.pak_rincian_detail.KOMPONEN_HARGA';

	
	const KOMPONEN_HARGA_AWAL = 'ebudget.pak_rincian_detail.KOMPONEN_HARGA_AWAL';

	
	const KOMPONEN_NAME = 'ebudget.pak_rincian_detail.KOMPONEN_NAME';

	
	const SATUAN = 'ebudget.pak_rincian_detail.SATUAN';

	
	const PAJAK = 'ebudget.pak_rincian_detail.PAJAK';

	
	const UNIT_ID = 'ebudget.pak_rincian_detail.UNIT_ID';

	
	const FROM_SUB_KEGIATAN = 'ebudget.pak_rincian_detail.FROM_SUB_KEGIATAN';

	
	const SUB = 'ebudget.pak_rincian_detail.SUB';

	
	const KODE_SUB = 'ebudget.pak_rincian_detail.KODE_SUB';

	
	const LAST_UPDATE_USER = 'ebudget.pak_rincian_detail.LAST_UPDATE_USER';

	
	const LAST_UPDATE_TIME = 'ebudget.pak_rincian_detail.LAST_UPDATE_TIME';

	
	const LAST_UPDATE_IP = 'ebudget.pak_rincian_detail.LAST_UPDATE_IP';

	
	const TAHAP = 'ebudget.pak_rincian_detail.TAHAP';

	
	const TAHAP_EDIT = 'ebudget.pak_rincian_detail.TAHAP_EDIT';

	
	const TAHAP_NEW = 'ebudget.pak_rincian_detail.TAHAP_NEW';

	
	const STATUS_LELANG = 'ebudget.pak_rincian_detail.STATUS_LELANG';

	
	const NOMOR_LELANG = 'ebudget.pak_rincian_detail.NOMOR_LELANG';

	
	const KOEFISIEN_SEMULA = 'ebudget.pak_rincian_detail.KOEFISIEN_SEMULA';

	
	const VOLUME_SEMULA = 'ebudget.pak_rincian_detail.VOLUME_SEMULA';

	
	const HARGA_SEMULA = 'ebudget.pak_rincian_detail.HARGA_SEMULA';

	
	const TOTAL_SEMULA = 'ebudget.pak_rincian_detail.TOTAL_SEMULA';

	
	const LOCK_SUBTITLE = 'ebudget.pak_rincian_detail.LOCK_SUBTITLE';

	
	const STATUS_HAPUS = 'ebudget.pak_rincian_detail.STATUS_HAPUS';

	
	const TAHUN = 'ebudget.pak_rincian_detail.TAHUN';

	
	const KODE_LOKASI = 'ebudget.pak_rincian_detail.KODE_LOKASI';

	
	const KECAMATAN = 'ebudget.pak_rincian_detail.KECAMATAN';

	
	const REKENING_CODE_ASLI = 'ebudget.pak_rincian_detail.REKENING_CODE_ASLI';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('KegiatanCode', 'Tipe', 'DetailNo', 'RekeningCode', 'KomponenId', 'DetailName', 'Volume', 'KeteranganKoefisien', 'Subtitle', 'KomponenHarga', 'KomponenHargaAwal', 'KomponenName', 'Satuan', 'Pajak', 'UnitId', 'FromSubKegiatan', 'Sub', 'KodeSub', 'LastUpdateUser', 'LastUpdateTime', 'LastUpdateIp', 'Tahap', 'TahapEdit', 'TahapNew', 'StatusLelang', 'NomorLelang', 'KoefisienSemula', 'VolumeSemula', 'HargaSemula', 'TotalSemula', 'LockSubtitle', 'StatusHapus', 'Tahun', 'KodeLokasi', 'Kecamatan', 'RekeningCodeAsli', ),
		BasePeer::TYPE_COLNAME => array (PakRincianDetailPeer::KEGIATAN_CODE, PakRincianDetailPeer::TIPE, PakRincianDetailPeer::DETAIL_NO, PakRincianDetailPeer::REKENING_CODE, PakRincianDetailPeer::KOMPONEN_ID, PakRincianDetailPeer::DETAIL_NAME, PakRincianDetailPeer::VOLUME, PakRincianDetailPeer::KETERANGAN_KOEFISIEN, PakRincianDetailPeer::SUBTITLE, PakRincianDetailPeer::KOMPONEN_HARGA, PakRincianDetailPeer::KOMPONEN_HARGA_AWAL, PakRincianDetailPeer::KOMPONEN_NAME, PakRincianDetailPeer::SATUAN, PakRincianDetailPeer::PAJAK, PakRincianDetailPeer::UNIT_ID, PakRincianDetailPeer::FROM_SUB_KEGIATAN, PakRincianDetailPeer::SUB, PakRincianDetailPeer::KODE_SUB, PakRincianDetailPeer::LAST_UPDATE_USER, PakRincianDetailPeer::LAST_UPDATE_TIME, PakRincianDetailPeer::LAST_UPDATE_IP, PakRincianDetailPeer::TAHAP, PakRincianDetailPeer::TAHAP_EDIT, PakRincianDetailPeer::TAHAP_NEW, PakRincianDetailPeer::STATUS_LELANG, PakRincianDetailPeer::NOMOR_LELANG, PakRincianDetailPeer::KOEFISIEN_SEMULA, PakRincianDetailPeer::VOLUME_SEMULA, PakRincianDetailPeer::HARGA_SEMULA, PakRincianDetailPeer::TOTAL_SEMULA, PakRincianDetailPeer::LOCK_SUBTITLE, PakRincianDetailPeer::STATUS_HAPUS, PakRincianDetailPeer::TAHUN, PakRincianDetailPeer::KODE_LOKASI, PakRincianDetailPeer::KECAMATAN, PakRincianDetailPeer::REKENING_CODE_ASLI, ),
		BasePeer::TYPE_FIELDNAME => array ('kegiatan_code', 'tipe', 'detail_no', 'rekening_code', 'komponen_id', 'detail_name', 'volume', 'keterangan_koefisien', 'subtitle', 'komponen_harga', 'komponen_harga_awal', 'komponen_name', 'satuan', 'pajak', 'unit_id', 'from_sub_kegiatan', 'sub', 'kode_sub', 'last_update_user', 'last_update_time', 'last_update_ip', 'tahap', 'tahap_edit', 'tahap_new', 'status_lelang', 'nomor_lelang', 'koefisien_semula', 'volume_semula', 'harga_semula', 'total_semula', 'lock_subtitle', 'status_hapus', 'tahun', 'kode_lokasi', 'kecamatan', 'rekening_code_asli', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('KegiatanCode' => 0, 'Tipe' => 1, 'DetailNo' => 2, 'RekeningCode' => 3, 'KomponenId' => 4, 'DetailName' => 5, 'Volume' => 6, 'KeteranganKoefisien' => 7, 'Subtitle' => 8, 'KomponenHarga' => 9, 'KomponenHargaAwal' => 10, 'KomponenName' => 11, 'Satuan' => 12, 'Pajak' => 13, 'UnitId' => 14, 'FromSubKegiatan' => 15, 'Sub' => 16, 'KodeSub' => 17, 'LastUpdateUser' => 18, 'LastUpdateTime' => 19, 'LastUpdateIp' => 20, 'Tahap' => 21, 'TahapEdit' => 22, 'TahapNew' => 23, 'StatusLelang' => 24, 'NomorLelang' => 25, 'KoefisienSemula' => 26, 'VolumeSemula' => 27, 'HargaSemula' => 28, 'TotalSemula' => 29, 'LockSubtitle' => 30, 'StatusHapus' => 31, 'Tahun' => 32, 'KodeLokasi' => 33, 'Kecamatan' => 34, 'RekeningCodeAsli' => 35, ),
		BasePeer::TYPE_COLNAME => array (PakRincianDetailPeer::KEGIATAN_CODE => 0, PakRincianDetailPeer::TIPE => 1, PakRincianDetailPeer::DETAIL_NO => 2, PakRincianDetailPeer::REKENING_CODE => 3, PakRincianDetailPeer::KOMPONEN_ID => 4, PakRincianDetailPeer::DETAIL_NAME => 5, PakRincianDetailPeer::VOLUME => 6, PakRincianDetailPeer::KETERANGAN_KOEFISIEN => 7, PakRincianDetailPeer::SUBTITLE => 8, PakRincianDetailPeer::KOMPONEN_HARGA => 9, PakRincianDetailPeer::KOMPONEN_HARGA_AWAL => 10, PakRincianDetailPeer::KOMPONEN_NAME => 11, PakRincianDetailPeer::SATUAN => 12, PakRincianDetailPeer::PAJAK => 13, PakRincianDetailPeer::UNIT_ID => 14, PakRincianDetailPeer::FROM_SUB_KEGIATAN => 15, PakRincianDetailPeer::SUB => 16, PakRincianDetailPeer::KODE_SUB => 17, PakRincianDetailPeer::LAST_UPDATE_USER => 18, PakRincianDetailPeer::LAST_UPDATE_TIME => 19, PakRincianDetailPeer::LAST_UPDATE_IP => 20, PakRincianDetailPeer::TAHAP => 21, PakRincianDetailPeer::TAHAP_EDIT => 22, PakRincianDetailPeer::TAHAP_NEW => 23, PakRincianDetailPeer::STATUS_LELANG => 24, PakRincianDetailPeer::NOMOR_LELANG => 25, PakRincianDetailPeer::KOEFISIEN_SEMULA => 26, PakRincianDetailPeer::VOLUME_SEMULA => 27, PakRincianDetailPeer::HARGA_SEMULA => 28, PakRincianDetailPeer::TOTAL_SEMULA => 29, PakRincianDetailPeer::LOCK_SUBTITLE => 30, PakRincianDetailPeer::STATUS_HAPUS => 31, PakRincianDetailPeer::TAHUN => 32, PakRincianDetailPeer::KODE_LOKASI => 33, PakRincianDetailPeer::KECAMATAN => 34, PakRincianDetailPeer::REKENING_CODE_ASLI => 35, ),
		BasePeer::TYPE_FIELDNAME => array ('kegiatan_code' => 0, 'tipe' => 1, 'detail_no' => 2, 'rekening_code' => 3, 'komponen_id' => 4, 'detail_name' => 5, 'volume' => 6, 'keterangan_koefisien' => 7, 'subtitle' => 8, 'komponen_harga' => 9, 'komponen_harga_awal' => 10, 'komponen_name' => 11, 'satuan' => 12, 'pajak' => 13, 'unit_id' => 14, 'from_sub_kegiatan' => 15, 'sub' => 16, 'kode_sub' => 17, 'last_update_user' => 18, 'last_update_time' => 19, 'last_update_ip' => 20, 'tahap' => 21, 'tahap_edit' => 22, 'tahap_new' => 23, 'status_lelang' => 24, 'nomor_lelang' => 25, 'koefisien_semula' => 26, 'volume_semula' => 27, 'harga_semula' => 28, 'total_semula' => 29, 'lock_subtitle' => 30, 'status_hapus' => 31, 'tahun' => 32, 'kode_lokasi' => 33, 'kecamatan' => 34, 'rekening_code_asli' => 35, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/PakRincianDetailMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.PakRincianDetailMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = PakRincianDetailPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(PakRincianDetailPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(PakRincianDetailPeer::KEGIATAN_CODE);

		$criteria->addSelectColumn(PakRincianDetailPeer::TIPE);

		$criteria->addSelectColumn(PakRincianDetailPeer::DETAIL_NO);

		$criteria->addSelectColumn(PakRincianDetailPeer::REKENING_CODE);

		$criteria->addSelectColumn(PakRincianDetailPeer::KOMPONEN_ID);

		$criteria->addSelectColumn(PakRincianDetailPeer::DETAIL_NAME);

		$criteria->addSelectColumn(PakRincianDetailPeer::VOLUME);

		$criteria->addSelectColumn(PakRincianDetailPeer::KETERANGAN_KOEFISIEN);

		$criteria->addSelectColumn(PakRincianDetailPeer::SUBTITLE);

		$criteria->addSelectColumn(PakRincianDetailPeer::KOMPONEN_HARGA);

		$criteria->addSelectColumn(PakRincianDetailPeer::KOMPONEN_HARGA_AWAL);

		$criteria->addSelectColumn(PakRincianDetailPeer::KOMPONEN_NAME);

		$criteria->addSelectColumn(PakRincianDetailPeer::SATUAN);

		$criteria->addSelectColumn(PakRincianDetailPeer::PAJAK);

		$criteria->addSelectColumn(PakRincianDetailPeer::UNIT_ID);

		$criteria->addSelectColumn(PakRincianDetailPeer::FROM_SUB_KEGIATAN);

		$criteria->addSelectColumn(PakRincianDetailPeer::SUB);

		$criteria->addSelectColumn(PakRincianDetailPeer::KODE_SUB);

		$criteria->addSelectColumn(PakRincianDetailPeer::LAST_UPDATE_USER);

		$criteria->addSelectColumn(PakRincianDetailPeer::LAST_UPDATE_TIME);

		$criteria->addSelectColumn(PakRincianDetailPeer::LAST_UPDATE_IP);

		$criteria->addSelectColumn(PakRincianDetailPeer::TAHAP);

		$criteria->addSelectColumn(PakRincianDetailPeer::TAHAP_EDIT);

		$criteria->addSelectColumn(PakRincianDetailPeer::TAHAP_NEW);

		$criteria->addSelectColumn(PakRincianDetailPeer::STATUS_LELANG);

		$criteria->addSelectColumn(PakRincianDetailPeer::NOMOR_LELANG);

		$criteria->addSelectColumn(PakRincianDetailPeer::KOEFISIEN_SEMULA);

		$criteria->addSelectColumn(PakRincianDetailPeer::VOLUME_SEMULA);

		$criteria->addSelectColumn(PakRincianDetailPeer::HARGA_SEMULA);

		$criteria->addSelectColumn(PakRincianDetailPeer::TOTAL_SEMULA);

		$criteria->addSelectColumn(PakRincianDetailPeer::LOCK_SUBTITLE);

		$criteria->addSelectColumn(PakRincianDetailPeer::STATUS_HAPUS);

		$criteria->addSelectColumn(PakRincianDetailPeer::TAHUN);

		$criteria->addSelectColumn(PakRincianDetailPeer::KODE_LOKASI);

		$criteria->addSelectColumn(PakRincianDetailPeer::KECAMATAN);

		$criteria->addSelectColumn(PakRincianDetailPeer::REKENING_CODE_ASLI);

	}

	const COUNT = 'COUNT(ebudget.pak_rincian_detail.KEGIATAN_CODE)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.pak_rincian_detail.KEGIATAN_CODE)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(PakRincianDetailPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(PakRincianDetailPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = PakRincianDetailPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = PakRincianDetailPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return PakRincianDetailPeer::populateObjects(PakRincianDetailPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			PakRincianDetailPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = PakRincianDetailPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return PakRincianDetailPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(PakRincianDetailPeer::KEGIATAN_CODE);
			$selectCriteria->add(PakRincianDetailPeer::KEGIATAN_CODE, $criteria->remove(PakRincianDetailPeer::KEGIATAN_CODE), $comparison);

			$comparison = $criteria->getComparison(PakRincianDetailPeer::TIPE);
			$selectCriteria->add(PakRincianDetailPeer::TIPE, $criteria->remove(PakRincianDetailPeer::TIPE), $comparison);

			$comparison = $criteria->getComparison(PakRincianDetailPeer::DETAIL_NO);
			$selectCriteria->add(PakRincianDetailPeer::DETAIL_NO, $criteria->remove(PakRincianDetailPeer::DETAIL_NO), $comparison);

			$comparison = $criteria->getComparison(PakRincianDetailPeer::UNIT_ID);
			$selectCriteria->add(PakRincianDetailPeer::UNIT_ID, $criteria->remove(PakRincianDetailPeer::UNIT_ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(PakRincianDetailPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(PakRincianDetailPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof PakRincianDetail) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
												if(count($values) == count($values, COUNT_RECURSIVE))
			{
								$values = array($values);
			}
			$vals = array();
			foreach($values as $value)
			{

				$vals[0][] = $value[0];
				$vals[1][] = $value[1];
				$vals[2][] = $value[2];
				$vals[3][] = $value[3];
			}

			$criteria->add(PakRincianDetailPeer::KEGIATAN_CODE, $vals[0], Criteria::IN);
			$criteria->add(PakRincianDetailPeer::TIPE, $vals[1], Criteria::IN);
			$criteria->add(PakRincianDetailPeer::DETAIL_NO, $vals[2], Criteria::IN);
			$criteria->add(PakRincianDetailPeer::UNIT_ID, $vals[3], Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(PakRincianDetail $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(PakRincianDetailPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(PakRincianDetailPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(PakRincianDetailPeer::DATABASE_NAME, PakRincianDetailPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = PakRincianDetailPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK( $kegiatan_code, $tipe, $detail_no, $unit_id, $con = null) {
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$criteria = new Criteria();
		$criteria->add(PakRincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
		$criteria->add(PakRincianDetailPeer::TIPE, $tipe);
		$criteria->add(PakRincianDetailPeer::DETAIL_NO, $detail_no);
		$criteria->add(PakRincianDetailPeer::UNIT_ID, $unit_id);
		$v = PakRincianDetailPeer::doSelect($criteria, $con);

		return !empty($v) ? $v[0] : null;
	}
} 
if (Propel::isInit()) {
			try {
		BasePakRincianDetailPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/PakRincianDetailMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.PakRincianDetailMapBuilder');
}
