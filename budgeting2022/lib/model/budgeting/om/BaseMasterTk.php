<?php


abstract class BaseMasterTk extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $kode;


	
	protected $nama_tk;


	
	protected $alamat;


	
	protected $status;


	
	protected $kecamatan;


	
	protected $kelurahan;


	
	protected $kode_jalan;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getKode()
	{

		return $this->kode;
	}

	
	public function getNamaTk()
	{

		return $this->nama_tk;
	}

	
	public function getAlamat()
	{

		return $this->alamat;
	}

	
	public function getStatus()
	{

		return $this->status;
	}

	
	public function getKecamatan()
	{

		return $this->kecamatan;
	}

	
	public function getKelurahan()
	{

		return $this->kelurahan;
	}

	
	public function getKodeJalan()
	{

		return $this->kode_jalan;
	}

	
	public function setKode($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode !== $v) {
			$this->kode = $v;
			$this->modifiedColumns[] = MasterTkPeer::KODE;
		}

	} 
	
	public function setNamaTk($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nama_tk !== $v) {
			$this->nama_tk = $v;
			$this->modifiedColumns[] = MasterTkPeer::NAMA_TK;
		}

	} 
	
	public function setAlamat($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->alamat !== $v) {
			$this->alamat = $v;
			$this->modifiedColumns[] = MasterTkPeer::ALAMAT;
		}

	} 
	
	public function setStatus($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->status !== $v) {
			$this->status = $v;
			$this->modifiedColumns[] = MasterTkPeer::STATUS;
		}

	} 
	
	public function setKecamatan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kecamatan !== $v) {
			$this->kecamatan = $v;
			$this->modifiedColumns[] = MasterTkPeer::KECAMATAN;
		}

	} 
	
	public function setKelurahan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kelurahan !== $v) {
			$this->kelurahan = $v;
			$this->modifiedColumns[] = MasterTkPeer::KELURAHAN;
		}

	} 
	
	public function setKodeJalan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_jalan !== $v) {
			$this->kode_jalan = $v;
			$this->modifiedColumns[] = MasterTkPeer::KODE_JALAN;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->kode = $rs->getString($startcol + 0);

			$this->nama_tk = $rs->getString($startcol + 1);

			$this->alamat = $rs->getString($startcol + 2);

			$this->status = $rs->getString($startcol + 3);

			$this->kecamatan = $rs->getString($startcol + 4);

			$this->kelurahan = $rs->getString($startcol + 5);

			$this->kode_jalan = $rs->getString($startcol + 6);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 7; 
		} catch (Exception $e) {
			throw new PropelException("Error populating MasterTk object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(MasterTkPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			MasterTkPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(MasterTkPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = MasterTkPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += MasterTkPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = MasterTkPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = MasterTkPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getKode();
				break;
			case 1:
				return $this->getNamaTk();
				break;
			case 2:
				return $this->getAlamat();
				break;
			case 3:
				return $this->getStatus();
				break;
			case 4:
				return $this->getKecamatan();
				break;
			case 5:
				return $this->getKelurahan();
				break;
			case 6:
				return $this->getKodeJalan();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = MasterTkPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getKode(),
			$keys[1] => $this->getNamaTk(),
			$keys[2] => $this->getAlamat(),
			$keys[3] => $this->getStatus(),
			$keys[4] => $this->getKecamatan(),
			$keys[5] => $this->getKelurahan(),
			$keys[6] => $this->getKodeJalan(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = MasterTkPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setKode($value);
				break;
			case 1:
				$this->setNamaTk($value);
				break;
			case 2:
				$this->setAlamat($value);
				break;
			case 3:
				$this->setStatus($value);
				break;
			case 4:
				$this->setKecamatan($value);
				break;
			case 5:
				$this->setKelurahan($value);
				break;
			case 6:
				$this->setKodeJalan($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = MasterTkPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setKode($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setNamaTk($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setAlamat($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setStatus($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setKecamatan($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setKelurahan($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setKodeJalan($arr[$keys[6]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(MasterTkPeer::DATABASE_NAME);

		if ($this->isColumnModified(MasterTkPeer::KODE)) $criteria->add(MasterTkPeer::KODE, $this->kode);
		if ($this->isColumnModified(MasterTkPeer::NAMA_TK)) $criteria->add(MasterTkPeer::NAMA_TK, $this->nama_tk);
		if ($this->isColumnModified(MasterTkPeer::ALAMAT)) $criteria->add(MasterTkPeer::ALAMAT, $this->alamat);
		if ($this->isColumnModified(MasterTkPeer::STATUS)) $criteria->add(MasterTkPeer::STATUS, $this->status);
		if ($this->isColumnModified(MasterTkPeer::KECAMATAN)) $criteria->add(MasterTkPeer::KECAMATAN, $this->kecamatan);
		if ($this->isColumnModified(MasterTkPeer::KELURAHAN)) $criteria->add(MasterTkPeer::KELURAHAN, $this->kelurahan);
		if ($this->isColumnModified(MasterTkPeer::KODE_JALAN)) $criteria->add(MasterTkPeer::KODE_JALAN, $this->kode_jalan);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(MasterTkPeer::DATABASE_NAME);

		$criteria->add(MasterTkPeer::KODE, $this->kode);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getKode();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setKode($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setNamaTk($this->nama_tk);

		$copyObj->setAlamat($this->alamat);

		$copyObj->setStatus($this->status);

		$copyObj->setKecamatan($this->kecamatan);

		$copyObj->setKelurahan($this->kelurahan);

		$copyObj->setKodeJalan($this->kode_jalan);


		$copyObj->setNew(true);

		$copyObj->setKode(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new MasterTkPeer();
		}
		return self::$peer;
	}

} 