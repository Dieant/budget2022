<?php


abstract class BaseGeojsonlokasiRev1Peer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'gis_ebudget.geojsonlokasi_rev1';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.GeojsonlokasiRev1';

	
	const NUM_COLUMNS = 23;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const IDGEOLOKASI = 'gis_ebudget.geojsonlokasi_rev1.IDGEOLOKASI';

	
	const UNIT_ID = 'gis_ebudget.geojsonlokasi_rev1.UNIT_ID';

	
	const KEGIATAN_CODE = 'gis_ebudget.geojsonlokasi_rev1.KEGIATAN_CODE';

	
	const DETAIL_NO = 'gis_ebudget.geojsonlokasi_rev1.DETAIL_NO';

	
	const SATUAN = 'gis_ebudget.geojsonlokasi_rev1.SATUAN';

	
	const VOLUME = 'gis_ebudget.geojsonlokasi_rev1.VOLUME';

	
	const NILAI_ANGGARAN = 'gis_ebudget.geojsonlokasi_rev1.NILAI_ANGGARAN';

	
	const TAHUN = 'gis_ebudget.geojsonlokasi_rev1.TAHUN';

	
	const MLOKASI = 'gis_ebudget.geojsonlokasi_rev1.MLOKASI';

	
	const ID_KELOMPOK = 'gis_ebudget.geojsonlokasi_rev1.ID_KELOMPOK';

	
	const GEOJSON = 'gis_ebudget.geojsonlokasi_rev1.GEOJSON';

	
	const KETERANGAN = 'gis_ebudget.geojsonlokasi_rev1.KETERANGAN';

	
	const NMUSER = 'gis_ebudget.geojsonlokasi_rev1.NMUSER';

	
	const LEVEL = 'gis_ebudget.geojsonlokasi_rev1.LEVEL';

	
	const KOMPONEN_NAME = 'gis_ebudget.geojsonlokasi_rev1.KOMPONEN_NAME';

	
	const STATUS_HAPUS = 'gis_ebudget.geojsonlokasi_rev1.STATUS_HAPUS';

	
	const KETERANGAN_ALAMAT = 'gis_ebudget.geojsonlokasi_rev1.KETERANGAN_ALAMAT';

	
	const UNIT_NAME = 'gis_ebudget.geojsonlokasi_rev1.UNIT_NAME';

	
	const LAST_EDIT_TIME = 'gis_ebudget.geojsonlokasi_rev1.LAST_EDIT_TIME';

	
	const LAST_CREATE_TIME = 'gis_ebudget.geojsonlokasi_rev1.LAST_CREATE_TIME';

	
	const KOORDINAT = 'gis_ebudget.geojsonlokasi_rev1.KOORDINAT';

	
	const LOKASI_KE = 'gis_ebudget.geojsonlokasi_rev1.LOKASI_KE';

	
	const KODE_DETAIL_KEGIATAN = 'gis_ebudget.geojsonlokasi_rev1.KODE_DETAIL_KEGIATAN';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('Idgeolokasi', 'UnitId', 'KegiatanCode', 'DetailNo', 'Satuan', 'Volume', 'NilaiAnggaran', 'Tahun', 'Mlokasi', 'IdKelompok', 'Geojson', 'Keterangan', 'Nmuser', 'Level', 'KomponenName', 'StatusHapus', 'KeteranganAlamat', 'UnitName', 'LastEditTime', 'LastCreateTime', 'Koordinat', 'LokasiKe', 'KodeDetailKegiatan', ),
		BasePeer::TYPE_COLNAME => array (GeojsonlokasiRev1Peer::IDGEOLOKASI, GeojsonlokasiRev1Peer::UNIT_ID, GeojsonlokasiRev1Peer::KEGIATAN_CODE, GeojsonlokasiRev1Peer::DETAIL_NO, GeojsonlokasiRev1Peer::SATUAN, GeojsonlokasiRev1Peer::VOLUME, GeojsonlokasiRev1Peer::NILAI_ANGGARAN, GeojsonlokasiRev1Peer::TAHUN, GeojsonlokasiRev1Peer::MLOKASI, GeojsonlokasiRev1Peer::ID_KELOMPOK, GeojsonlokasiRev1Peer::GEOJSON, GeojsonlokasiRev1Peer::KETERANGAN, GeojsonlokasiRev1Peer::NMUSER, GeojsonlokasiRev1Peer::LEVEL, GeojsonlokasiRev1Peer::KOMPONEN_NAME, GeojsonlokasiRev1Peer::STATUS_HAPUS, GeojsonlokasiRev1Peer::KETERANGAN_ALAMAT, GeojsonlokasiRev1Peer::UNIT_NAME, GeojsonlokasiRev1Peer::LAST_EDIT_TIME, GeojsonlokasiRev1Peer::LAST_CREATE_TIME, GeojsonlokasiRev1Peer::KOORDINAT, GeojsonlokasiRev1Peer::LOKASI_KE, GeojsonlokasiRev1Peer::KODE_DETAIL_KEGIATAN, ),
		BasePeer::TYPE_FIELDNAME => array ('idgeolokasi', 'unit_id', 'kegiatan_code', 'detail_no', 'satuan', 'volume', 'nilai_anggaran', 'tahun', 'mlokasi', 'id_kelompok', 'geojson', 'keterangan', 'nmuser', 'level', 'komponen_name', 'status_hapus', 'keterangan_alamat', 'unit_name', 'last_edit_time', 'last_create_time', 'koordinat', 'lokasi_ke', 'kode_detail_kegiatan', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('Idgeolokasi' => 0, 'UnitId' => 1, 'KegiatanCode' => 2, 'DetailNo' => 3, 'Satuan' => 4, 'Volume' => 5, 'NilaiAnggaran' => 6, 'Tahun' => 7, 'Mlokasi' => 8, 'IdKelompok' => 9, 'Geojson' => 10, 'Keterangan' => 11, 'Nmuser' => 12, 'Level' => 13, 'KomponenName' => 14, 'StatusHapus' => 15, 'KeteranganAlamat' => 16, 'UnitName' => 17, 'LastEditTime' => 18, 'LastCreateTime' => 19, 'Koordinat' => 20, 'LokasiKe' => 21, 'KodeDetailKegiatan' => 22, ),
		BasePeer::TYPE_COLNAME => array (GeojsonlokasiRev1Peer::IDGEOLOKASI => 0, GeojsonlokasiRev1Peer::UNIT_ID => 1, GeojsonlokasiRev1Peer::KEGIATAN_CODE => 2, GeojsonlokasiRev1Peer::DETAIL_NO => 3, GeojsonlokasiRev1Peer::SATUAN => 4, GeojsonlokasiRev1Peer::VOLUME => 5, GeojsonlokasiRev1Peer::NILAI_ANGGARAN => 6, GeojsonlokasiRev1Peer::TAHUN => 7, GeojsonlokasiRev1Peer::MLOKASI => 8, GeojsonlokasiRev1Peer::ID_KELOMPOK => 9, GeojsonlokasiRev1Peer::GEOJSON => 10, GeojsonlokasiRev1Peer::KETERANGAN => 11, GeojsonlokasiRev1Peer::NMUSER => 12, GeojsonlokasiRev1Peer::LEVEL => 13, GeojsonlokasiRev1Peer::KOMPONEN_NAME => 14, GeojsonlokasiRev1Peer::STATUS_HAPUS => 15, GeojsonlokasiRev1Peer::KETERANGAN_ALAMAT => 16, GeojsonlokasiRev1Peer::UNIT_NAME => 17, GeojsonlokasiRev1Peer::LAST_EDIT_TIME => 18, GeojsonlokasiRev1Peer::LAST_CREATE_TIME => 19, GeojsonlokasiRev1Peer::KOORDINAT => 20, GeojsonlokasiRev1Peer::LOKASI_KE => 21, GeojsonlokasiRev1Peer::KODE_DETAIL_KEGIATAN => 22, ),
		BasePeer::TYPE_FIELDNAME => array ('idgeolokasi' => 0, 'unit_id' => 1, 'kegiatan_code' => 2, 'detail_no' => 3, 'satuan' => 4, 'volume' => 5, 'nilai_anggaran' => 6, 'tahun' => 7, 'mlokasi' => 8, 'id_kelompok' => 9, 'geojson' => 10, 'keterangan' => 11, 'nmuser' => 12, 'level' => 13, 'komponen_name' => 14, 'status_hapus' => 15, 'keterangan_alamat' => 16, 'unit_name' => 17, 'last_edit_time' => 18, 'last_create_time' => 19, 'koordinat' => 20, 'lokasi_ke' => 21, 'kode_detail_kegiatan' => 22, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/GeojsonlokasiRev1MapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.GeojsonlokasiRev1MapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = GeojsonlokasiRev1Peer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(GeojsonlokasiRev1Peer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(GeojsonlokasiRev1Peer::IDGEOLOKASI);

		$criteria->addSelectColumn(GeojsonlokasiRev1Peer::UNIT_ID);

		$criteria->addSelectColumn(GeojsonlokasiRev1Peer::KEGIATAN_CODE);

		$criteria->addSelectColumn(GeojsonlokasiRev1Peer::DETAIL_NO);

		$criteria->addSelectColumn(GeojsonlokasiRev1Peer::SATUAN);

		$criteria->addSelectColumn(GeojsonlokasiRev1Peer::VOLUME);

		$criteria->addSelectColumn(GeojsonlokasiRev1Peer::NILAI_ANGGARAN);

		$criteria->addSelectColumn(GeojsonlokasiRev1Peer::TAHUN);

		$criteria->addSelectColumn(GeojsonlokasiRev1Peer::MLOKASI);

		$criteria->addSelectColumn(GeojsonlokasiRev1Peer::ID_KELOMPOK);

		$criteria->addSelectColumn(GeojsonlokasiRev1Peer::GEOJSON);

		$criteria->addSelectColumn(GeojsonlokasiRev1Peer::KETERANGAN);

		$criteria->addSelectColumn(GeojsonlokasiRev1Peer::NMUSER);

		$criteria->addSelectColumn(GeojsonlokasiRev1Peer::LEVEL);

		$criteria->addSelectColumn(GeojsonlokasiRev1Peer::KOMPONEN_NAME);

		$criteria->addSelectColumn(GeojsonlokasiRev1Peer::STATUS_HAPUS);

		$criteria->addSelectColumn(GeojsonlokasiRev1Peer::KETERANGAN_ALAMAT);

		$criteria->addSelectColumn(GeojsonlokasiRev1Peer::UNIT_NAME);

		$criteria->addSelectColumn(GeojsonlokasiRev1Peer::LAST_EDIT_TIME);

		$criteria->addSelectColumn(GeojsonlokasiRev1Peer::LAST_CREATE_TIME);

		$criteria->addSelectColumn(GeojsonlokasiRev1Peer::KOORDINAT);

		$criteria->addSelectColumn(GeojsonlokasiRev1Peer::LOKASI_KE);

		$criteria->addSelectColumn(GeojsonlokasiRev1Peer::KODE_DETAIL_KEGIATAN);

	}

	const COUNT = 'COUNT(gis_ebudget.geojsonlokasi_rev1.IDGEOLOKASI)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT gis_ebudget.geojsonlokasi_rev1.IDGEOLOKASI)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(GeojsonlokasiRev1Peer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(GeojsonlokasiRev1Peer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = GeojsonlokasiRev1Peer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = GeojsonlokasiRev1Peer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return GeojsonlokasiRev1Peer::populateObjects(GeojsonlokasiRev1Peer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			GeojsonlokasiRev1Peer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = GeojsonlokasiRev1Peer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return GeojsonlokasiRev1Peer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(GeojsonlokasiRev1Peer::IDGEOLOKASI);
			$selectCriteria->add(GeojsonlokasiRev1Peer::IDGEOLOKASI, $criteria->remove(GeojsonlokasiRev1Peer::IDGEOLOKASI), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(GeojsonlokasiRev1Peer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(GeojsonlokasiRev1Peer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof GeojsonlokasiRev1) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(GeojsonlokasiRev1Peer::IDGEOLOKASI, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(GeojsonlokasiRev1 $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(GeojsonlokasiRev1Peer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(GeojsonlokasiRev1Peer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(GeojsonlokasiRev1Peer::DATABASE_NAME, GeojsonlokasiRev1Peer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = GeojsonlokasiRev1Peer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(GeojsonlokasiRev1Peer::DATABASE_NAME);

		$criteria->add(GeojsonlokasiRev1Peer::IDGEOLOKASI, $pk);


		$v = GeojsonlokasiRev1Peer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(GeojsonlokasiRev1Peer::IDGEOLOKASI, $pks, Criteria::IN);
			$objs = GeojsonlokasiRev1Peer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BaseGeojsonlokasiRev1Peer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/GeojsonlokasiRev1MapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.GeojsonlokasiRev1MapBuilder');
}
