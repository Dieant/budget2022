<?php


abstract class BasePakBukuPutihMasterKegiatanPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.pak_bukuputih_master_kegiatan';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.PakBukuPutihMasterKegiatan';

	
	const NUM_COLUMNS = 63;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const UNIT_ID = 'ebudget.pak_bukuputih_master_kegiatan.UNIT_ID';

	
	const KODE_KEGIATAN = 'ebudget.pak_bukuputih_master_kegiatan.KODE_KEGIATAN';

	
	const KODE_KEGIATAN_BARU = 'ebudget.pak_bukuputih_master_kegiatan.KODE_KEGIATAN_BARU';

	
	const KODE_BIDANG = 'ebudget.pak_bukuputih_master_kegiatan.KODE_BIDANG';

	
	const KODE_URUSAN_WAJIB = 'ebudget.pak_bukuputih_master_kegiatan.KODE_URUSAN_WAJIB';

	
	const KODE_PROGRAM = 'ebudget.pak_bukuputih_master_kegiatan.KODE_PROGRAM';

	
	const KODE_SASARAN = 'ebudget.pak_bukuputih_master_kegiatan.KODE_SASARAN';

	
	const KODE_INDIKATOR = 'ebudget.pak_bukuputih_master_kegiatan.KODE_INDIKATOR';

	
	const ALOKASI_DANA = 'ebudget.pak_bukuputih_master_kegiatan.ALOKASI_DANA';

	
	const NAMA_KEGIATAN = 'ebudget.pak_bukuputih_master_kegiatan.NAMA_KEGIATAN';

	
	const MASUKAN = 'ebudget.pak_bukuputih_master_kegiatan.MASUKAN';

	
	const OUTPUT = 'ebudget.pak_bukuputih_master_kegiatan.OUTPUT';

	
	const OUTCOME = 'ebudget.pak_bukuputih_master_kegiatan.OUTCOME';

	
	const BENEFIT = 'ebudget.pak_bukuputih_master_kegiatan.BENEFIT';

	
	const IMPACT = 'ebudget.pak_bukuputih_master_kegiatan.IMPACT';

	
	const TIPE = 'ebudget.pak_bukuputih_master_kegiatan.TIPE';

	
	const KEGIATAN_ACTIVE = 'ebudget.pak_bukuputih_master_kegiatan.KEGIATAN_ACTIVE';

	
	const TO_KEGIATAN_CODE = 'ebudget.pak_bukuputih_master_kegiatan.TO_KEGIATAN_CODE';

	
	const CATATAN = 'ebudget.pak_bukuputih_master_kegiatan.CATATAN';

	
	const TARGET_OUTCOME = 'ebudget.pak_bukuputih_master_kegiatan.TARGET_OUTCOME';

	
	const LOKASI = 'ebudget.pak_bukuputih_master_kegiatan.LOKASI';

	
	const JUMLAH_PREV = 'ebudget.pak_bukuputih_master_kegiatan.JUMLAH_PREV';

	
	const JUMLAH_NOW = 'ebudget.pak_bukuputih_master_kegiatan.JUMLAH_NOW';

	
	const JUMLAH_NEXT = 'ebudget.pak_bukuputih_master_kegiatan.JUMLAH_NEXT';

	
	const KODE_PROGRAM2 = 'ebudget.pak_bukuputih_master_kegiatan.KODE_PROGRAM2';

	
	const KODE_URUSAN = 'ebudget.pak_bukuputih_master_kegiatan.KODE_URUSAN';

	
	const LAST_UPDATE_USER = 'ebudget.pak_bukuputih_master_kegiatan.LAST_UPDATE_USER';

	
	const LAST_UPDATE_TIME = 'ebudget.pak_bukuputih_master_kegiatan.LAST_UPDATE_TIME';

	
	const LAST_UPDATE_IP = 'ebudget.pak_bukuputih_master_kegiatan.LAST_UPDATE_IP';

	
	const TAHAP = 'ebudget.pak_bukuputih_master_kegiatan.TAHAP';

	
	const KODE_MISI = 'ebudget.pak_bukuputih_master_kegiatan.KODE_MISI';

	
	const KODE_TUJUAN = 'ebudget.pak_bukuputih_master_kegiatan.KODE_TUJUAN';

	
	const RANKING = 'ebudget.pak_bukuputih_master_kegiatan.RANKING';

	
	const NOMOR13 = 'ebudget.pak_bukuputih_master_kegiatan.NOMOR13';

	
	const PPA_NAMA = 'ebudget.pak_bukuputih_master_kegiatan.PPA_NAMA';

	
	const PPA_PANGKAT = 'ebudget.pak_bukuputih_master_kegiatan.PPA_PANGKAT';

	
	const PPA_NIP = 'ebudget.pak_bukuputih_master_kegiatan.PPA_NIP';

	
	const LANJUTAN = 'ebudget.pak_bukuputih_master_kegiatan.LANJUTAN';

	
	const USER_ID = 'ebudget.pak_bukuputih_master_kegiatan.USER_ID';

	
	const ID = 'ebudget.pak_bukuputih_master_kegiatan.ID';

	
	const TAHUN = 'ebudget.pak_bukuputih_master_kegiatan.TAHUN';

	
	const TAMBAHAN_PAGU = 'ebudget.pak_bukuputih_master_kegiatan.TAMBAHAN_PAGU';

	
	const GENDER = 'ebudget.pak_bukuputih_master_kegiatan.GENDER';

	
	const KODE_KEG_KEUANGAN = 'ebudget.pak_bukuputih_master_kegiatan.KODE_KEG_KEUANGAN';

	
	const USER_ID_LAMA = 'ebudget.pak_bukuputih_master_kegiatan.USER_ID_LAMA';

	
	const INDIKATOR = 'ebudget.pak_bukuputih_master_kegiatan.INDIKATOR';

	
	const IS_DAK = 'ebudget.pak_bukuputih_master_kegiatan.IS_DAK';

	
	const KODE_KEGIATAN_ASAL = 'ebudget.pak_bukuputih_master_kegiatan.KODE_KEGIATAN_ASAL';

	
	const KODE_KEG_KEUANGAN_ASAL = 'ebudget.pak_bukuputih_master_kegiatan.KODE_KEG_KEUANGAN_ASAL';

	
	const TH_KE_MULTIYEARS = 'ebudget.pak_bukuputih_master_kegiatan.TH_KE_MULTIYEARS';

	
	const KELOMPOK_SASARAN = 'ebudget.pak_bukuputih_master_kegiatan.KELOMPOK_SASARAN';

	
	const PAGU_BAPPEKO = 'ebudget.pak_bukuputih_master_kegiatan.PAGU_BAPPEKO';

	
	const KODE_DPA = 'ebudget.pak_bukuputih_master_kegiatan.KODE_DPA';

	
	const USER_ID_PPTK = 'ebudget.pak_bukuputih_master_kegiatan.USER_ID_PPTK';

	
	const USER_ID_KPA = 'ebudget.pak_bukuputih_master_kegiatan.USER_ID_KPA';

	
	const CATATAN_PEMBAHASAN = 'ebudget.pak_bukuputih_master_kegiatan.CATATAN_PEMBAHASAN';

	
	const CATATAN_PENYELIA = 'ebudget.pak_bukuputih_master_kegiatan.CATATAN_PENYELIA';

	
	const CATATAN_BAPPEKO = 'ebudget.pak_bukuputih_master_kegiatan.CATATAN_BAPPEKO';

	
	const STATUS_LEVEL = 'ebudget.pak_bukuputih_master_kegiatan.STATUS_LEVEL';

	
	const IS_TAPD_SETUJU = 'ebudget.pak_bukuputih_master_kegiatan.IS_TAPD_SETUJU';

	
	const IS_BAPPEKO_SETUJU = 'ebudget.pak_bukuputih_master_kegiatan.IS_BAPPEKO_SETUJU';

	
	const IS_PENYELIA_SETUJU = 'ebudget.pak_bukuputih_master_kegiatan.IS_PENYELIA_SETUJU';

	
	const IS_PERNAH_RKA = 'ebudget.pak_bukuputih_master_kegiatan.IS_PERNAH_RKA';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('UnitId', 'KodeKegiatan', 'KodeKegiatanBaru', 'KodeBidang', 'KodeUrusanWajib', 'KodeProgram', 'KodeSasaran', 'KodeIndikator', 'AlokasiDana', 'NamaKegiatan', 'Masukan', 'Output', 'Outcome', 'Benefit', 'Impact', 'Tipe', 'KegiatanActive', 'ToKegiatanCode', 'Catatan', 'TargetOutcome', 'Lokasi', 'JumlahPrev', 'JumlahNow', 'JumlahNext', 'KodeProgram2', 'KodeUrusan', 'LastUpdateUser', 'LastUpdateTime', 'LastUpdateIp', 'Tahap', 'KodeMisi', 'KodeTujuan', 'Ranking', 'Nomor13', 'PpaNama', 'PpaPangkat', 'PpaNip', 'Lanjutan', 'UserId', 'Id', 'Tahun', 'TambahanPagu', 'Gender', 'KodeKegKeuangan', 'UserIdLama', 'Indikator', 'IsDak', 'KodeKegiatanAsal', 'KodeKegKeuanganAsal', 'ThKeMultiyears', 'KelompokSasaran', 'PaguBappeko', 'KodeDpa', 'UserIdPptk', 'UserIdKpa', 'CatatanPembahasan', 'CatatanPenyelia', 'CatatanBappeko', 'StatusLevel', 'IsTapdSetuju', 'IsBappekoSetuju', 'IsPenyeliaSetuju', 'IsPernahRka', ),
		BasePeer::TYPE_COLNAME => array (PakBukuPutihMasterKegiatanPeer::UNIT_ID, PakBukuPutihMasterKegiatanPeer::KODE_KEGIATAN, PakBukuPutihMasterKegiatanPeer::KODE_KEGIATAN_BARU, PakBukuPutihMasterKegiatanPeer::KODE_BIDANG, PakBukuPutihMasterKegiatanPeer::KODE_URUSAN_WAJIB, PakBukuPutihMasterKegiatanPeer::KODE_PROGRAM, PakBukuPutihMasterKegiatanPeer::KODE_SASARAN, PakBukuPutihMasterKegiatanPeer::KODE_INDIKATOR, PakBukuPutihMasterKegiatanPeer::ALOKASI_DANA, PakBukuPutihMasterKegiatanPeer::NAMA_KEGIATAN, PakBukuPutihMasterKegiatanPeer::MASUKAN, PakBukuPutihMasterKegiatanPeer::OUTPUT, PakBukuPutihMasterKegiatanPeer::OUTCOME, PakBukuPutihMasterKegiatanPeer::BENEFIT, PakBukuPutihMasterKegiatanPeer::IMPACT, PakBukuPutihMasterKegiatanPeer::TIPE, PakBukuPutihMasterKegiatanPeer::KEGIATAN_ACTIVE, PakBukuPutihMasterKegiatanPeer::TO_KEGIATAN_CODE, PakBukuPutihMasterKegiatanPeer::CATATAN, PakBukuPutihMasterKegiatanPeer::TARGET_OUTCOME, PakBukuPutihMasterKegiatanPeer::LOKASI, PakBukuPutihMasterKegiatanPeer::JUMLAH_PREV, PakBukuPutihMasterKegiatanPeer::JUMLAH_NOW, PakBukuPutihMasterKegiatanPeer::JUMLAH_NEXT, PakBukuPutihMasterKegiatanPeer::KODE_PROGRAM2, PakBukuPutihMasterKegiatanPeer::KODE_URUSAN, PakBukuPutihMasterKegiatanPeer::LAST_UPDATE_USER, PakBukuPutihMasterKegiatanPeer::LAST_UPDATE_TIME, PakBukuPutihMasterKegiatanPeer::LAST_UPDATE_IP, PakBukuPutihMasterKegiatanPeer::TAHAP, PakBukuPutihMasterKegiatanPeer::KODE_MISI, PakBukuPutihMasterKegiatanPeer::KODE_TUJUAN, PakBukuPutihMasterKegiatanPeer::RANKING, PakBukuPutihMasterKegiatanPeer::NOMOR13, PakBukuPutihMasterKegiatanPeer::PPA_NAMA, PakBukuPutihMasterKegiatanPeer::PPA_PANGKAT, PakBukuPutihMasterKegiatanPeer::PPA_NIP, PakBukuPutihMasterKegiatanPeer::LANJUTAN, PakBukuPutihMasterKegiatanPeer::USER_ID, PakBukuPutihMasterKegiatanPeer::ID, PakBukuPutihMasterKegiatanPeer::TAHUN, PakBukuPutihMasterKegiatanPeer::TAMBAHAN_PAGU, PakBukuPutihMasterKegiatanPeer::GENDER, PakBukuPutihMasterKegiatanPeer::KODE_KEG_KEUANGAN, PakBukuPutihMasterKegiatanPeer::USER_ID_LAMA, PakBukuPutihMasterKegiatanPeer::INDIKATOR, PakBukuPutihMasterKegiatanPeer::IS_DAK, PakBukuPutihMasterKegiatanPeer::KODE_KEGIATAN_ASAL, PakBukuPutihMasterKegiatanPeer::KODE_KEG_KEUANGAN_ASAL, PakBukuPutihMasterKegiatanPeer::TH_KE_MULTIYEARS, PakBukuPutihMasterKegiatanPeer::KELOMPOK_SASARAN, PakBukuPutihMasterKegiatanPeer::PAGU_BAPPEKO, PakBukuPutihMasterKegiatanPeer::KODE_DPA, PakBukuPutihMasterKegiatanPeer::USER_ID_PPTK, PakBukuPutihMasterKegiatanPeer::USER_ID_KPA, PakBukuPutihMasterKegiatanPeer::CATATAN_PEMBAHASAN, PakBukuPutihMasterKegiatanPeer::CATATAN_PENYELIA, PakBukuPutihMasterKegiatanPeer::CATATAN_BAPPEKO, PakBukuPutihMasterKegiatanPeer::STATUS_LEVEL, PakBukuPutihMasterKegiatanPeer::IS_TAPD_SETUJU, PakBukuPutihMasterKegiatanPeer::IS_BAPPEKO_SETUJU, PakBukuPutihMasterKegiatanPeer::IS_PENYELIA_SETUJU, PakBukuPutihMasterKegiatanPeer::IS_PERNAH_RKA, ),
		BasePeer::TYPE_FIELDNAME => array ('unit_id', 'kode_kegiatan', 'kode_kegiatan_baru', 'kode_bidang', 'kode_urusan_wajib', 'kode_program', 'kode_sasaran', 'kode_indikator', 'alokasi_dana', 'nama_kegiatan', 'masukan', 'output', 'outcome', 'benefit', 'impact', 'tipe', 'kegiatan_active', 'to_kegiatan_code', 'catatan', 'target_outcome', 'lokasi', 'jumlah_prev', 'jumlah_now', 'jumlah_next', 'kode_program2', 'kode_urusan', 'last_update_user', 'last_update_time', 'last_update_ip', 'tahap', 'kode_misi', 'kode_tujuan', 'ranking', 'nomor13', 'ppa_nama', 'ppa_pangkat', 'ppa_nip', 'lanjutan', 'user_id', 'id', 'tahun', 'tambahan_pagu', 'gender', 'kode_keg_keuangan', 'user_id_lama', 'indikator', 'is_dak', 'kode_kegiatan_asal', 'kode_keg_keuangan_asal', 'th_ke_multiyears', 'kelompok_sasaran', 'pagu_bappeko', 'kode_dpa', 'user_id_pptk', 'user_id_kpa', 'catatan_pembahasan', 'catatan_penyelia', 'catatan_bappeko', 'status_level', 'is_tapd_setuju', 'is_bappeko_setuju', 'is_penyelia_setuju', 'is_pernah_rka', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('UnitId' => 0, 'KodeKegiatan' => 1, 'KodeKegiatanBaru' => 2, 'KodeBidang' => 3, 'KodeUrusanWajib' => 4, 'KodeProgram' => 5, 'KodeSasaran' => 6, 'KodeIndikator' => 7, 'AlokasiDana' => 8, 'NamaKegiatan' => 9, 'Masukan' => 10, 'Output' => 11, 'Outcome' => 12, 'Benefit' => 13, 'Impact' => 14, 'Tipe' => 15, 'KegiatanActive' => 16, 'ToKegiatanCode' => 17, 'Catatan' => 18, 'TargetOutcome' => 19, 'Lokasi' => 20, 'JumlahPrev' => 21, 'JumlahNow' => 22, 'JumlahNext' => 23, 'KodeProgram2' => 24, 'KodeUrusan' => 25, 'LastUpdateUser' => 26, 'LastUpdateTime' => 27, 'LastUpdateIp' => 28, 'Tahap' => 29, 'KodeMisi' => 30, 'KodeTujuan' => 31, 'Ranking' => 32, 'Nomor13' => 33, 'PpaNama' => 34, 'PpaPangkat' => 35, 'PpaNip' => 36, 'Lanjutan' => 37, 'UserId' => 38, 'Id' => 39, 'Tahun' => 40, 'TambahanPagu' => 41, 'Gender' => 42, 'KodeKegKeuangan' => 43, 'UserIdLama' => 44, 'Indikator' => 45, 'IsDak' => 46, 'KodeKegiatanAsal' => 47, 'KodeKegKeuanganAsal' => 48, 'ThKeMultiyears' => 49, 'KelompokSasaran' => 50, 'PaguBappeko' => 51, 'KodeDpa' => 52, 'UserIdPptk' => 53, 'UserIdKpa' => 54, 'CatatanPembahasan' => 55, 'CatatanPenyelia' => 56, 'CatatanBappeko' => 57, 'StatusLevel' => 58, 'IsTapdSetuju' => 59, 'IsBappekoSetuju' => 60, 'IsPenyeliaSetuju' => 61, 'IsPernahRka' => 62, ),
		BasePeer::TYPE_COLNAME => array (PakBukuPutihMasterKegiatanPeer::UNIT_ID => 0, PakBukuPutihMasterKegiatanPeer::KODE_KEGIATAN => 1, PakBukuPutihMasterKegiatanPeer::KODE_KEGIATAN_BARU => 2, PakBukuPutihMasterKegiatanPeer::KODE_BIDANG => 3, PakBukuPutihMasterKegiatanPeer::KODE_URUSAN_WAJIB => 4, PakBukuPutihMasterKegiatanPeer::KODE_PROGRAM => 5, PakBukuPutihMasterKegiatanPeer::KODE_SASARAN => 6, PakBukuPutihMasterKegiatanPeer::KODE_INDIKATOR => 7, PakBukuPutihMasterKegiatanPeer::ALOKASI_DANA => 8, PakBukuPutihMasterKegiatanPeer::NAMA_KEGIATAN => 9, PakBukuPutihMasterKegiatanPeer::MASUKAN => 10, PakBukuPutihMasterKegiatanPeer::OUTPUT => 11, PakBukuPutihMasterKegiatanPeer::OUTCOME => 12, PakBukuPutihMasterKegiatanPeer::BENEFIT => 13, PakBukuPutihMasterKegiatanPeer::IMPACT => 14, PakBukuPutihMasterKegiatanPeer::TIPE => 15, PakBukuPutihMasterKegiatanPeer::KEGIATAN_ACTIVE => 16, PakBukuPutihMasterKegiatanPeer::TO_KEGIATAN_CODE => 17, PakBukuPutihMasterKegiatanPeer::CATATAN => 18, PakBukuPutihMasterKegiatanPeer::TARGET_OUTCOME => 19, PakBukuPutihMasterKegiatanPeer::LOKASI => 20, PakBukuPutihMasterKegiatanPeer::JUMLAH_PREV => 21, PakBukuPutihMasterKegiatanPeer::JUMLAH_NOW => 22, PakBukuPutihMasterKegiatanPeer::JUMLAH_NEXT => 23, PakBukuPutihMasterKegiatanPeer::KODE_PROGRAM2 => 24, PakBukuPutihMasterKegiatanPeer::KODE_URUSAN => 25, PakBukuPutihMasterKegiatanPeer::LAST_UPDATE_USER => 26, PakBukuPutihMasterKegiatanPeer::LAST_UPDATE_TIME => 27, PakBukuPutihMasterKegiatanPeer::LAST_UPDATE_IP => 28, PakBukuPutihMasterKegiatanPeer::TAHAP => 29, PakBukuPutihMasterKegiatanPeer::KODE_MISI => 30, PakBukuPutihMasterKegiatanPeer::KODE_TUJUAN => 31, PakBukuPutihMasterKegiatanPeer::RANKING => 32, PakBukuPutihMasterKegiatanPeer::NOMOR13 => 33, PakBukuPutihMasterKegiatanPeer::PPA_NAMA => 34, PakBukuPutihMasterKegiatanPeer::PPA_PANGKAT => 35, PakBukuPutihMasterKegiatanPeer::PPA_NIP => 36, PakBukuPutihMasterKegiatanPeer::LANJUTAN => 37, PakBukuPutihMasterKegiatanPeer::USER_ID => 38, PakBukuPutihMasterKegiatanPeer::ID => 39, PakBukuPutihMasterKegiatanPeer::TAHUN => 40, PakBukuPutihMasterKegiatanPeer::TAMBAHAN_PAGU => 41, PakBukuPutihMasterKegiatanPeer::GENDER => 42, PakBukuPutihMasterKegiatanPeer::KODE_KEG_KEUANGAN => 43, PakBukuPutihMasterKegiatanPeer::USER_ID_LAMA => 44, PakBukuPutihMasterKegiatanPeer::INDIKATOR => 45, PakBukuPutihMasterKegiatanPeer::IS_DAK => 46, PakBukuPutihMasterKegiatanPeer::KODE_KEGIATAN_ASAL => 47, PakBukuPutihMasterKegiatanPeer::KODE_KEG_KEUANGAN_ASAL => 48, PakBukuPutihMasterKegiatanPeer::TH_KE_MULTIYEARS => 49, PakBukuPutihMasterKegiatanPeer::KELOMPOK_SASARAN => 50, PakBukuPutihMasterKegiatanPeer::PAGU_BAPPEKO => 51, PakBukuPutihMasterKegiatanPeer::KODE_DPA => 52, PakBukuPutihMasterKegiatanPeer::USER_ID_PPTK => 53, PakBukuPutihMasterKegiatanPeer::USER_ID_KPA => 54, PakBukuPutihMasterKegiatanPeer::CATATAN_PEMBAHASAN => 55, PakBukuPutihMasterKegiatanPeer::CATATAN_PENYELIA => 56, PakBukuPutihMasterKegiatanPeer::CATATAN_BAPPEKO => 57, PakBukuPutihMasterKegiatanPeer::STATUS_LEVEL => 58, PakBukuPutihMasterKegiatanPeer::IS_TAPD_SETUJU => 59, PakBukuPutihMasterKegiatanPeer::IS_BAPPEKO_SETUJU => 60, PakBukuPutihMasterKegiatanPeer::IS_PENYELIA_SETUJU => 61, PakBukuPutihMasterKegiatanPeer::IS_PERNAH_RKA => 62, ),
		BasePeer::TYPE_FIELDNAME => array ('unit_id' => 0, 'kode_kegiatan' => 1, 'kode_kegiatan_baru' => 2, 'kode_bidang' => 3, 'kode_urusan_wajib' => 4, 'kode_program' => 5, 'kode_sasaran' => 6, 'kode_indikator' => 7, 'alokasi_dana' => 8, 'nama_kegiatan' => 9, 'masukan' => 10, 'output' => 11, 'outcome' => 12, 'benefit' => 13, 'impact' => 14, 'tipe' => 15, 'kegiatan_active' => 16, 'to_kegiatan_code' => 17, 'catatan' => 18, 'target_outcome' => 19, 'lokasi' => 20, 'jumlah_prev' => 21, 'jumlah_now' => 22, 'jumlah_next' => 23, 'kode_program2' => 24, 'kode_urusan' => 25, 'last_update_user' => 26, 'last_update_time' => 27, 'last_update_ip' => 28, 'tahap' => 29, 'kode_misi' => 30, 'kode_tujuan' => 31, 'ranking' => 32, 'nomor13' => 33, 'ppa_nama' => 34, 'ppa_pangkat' => 35, 'ppa_nip' => 36, 'lanjutan' => 37, 'user_id' => 38, 'id' => 39, 'tahun' => 40, 'tambahan_pagu' => 41, 'gender' => 42, 'kode_keg_keuangan' => 43, 'user_id_lama' => 44, 'indikator' => 45, 'is_dak' => 46, 'kode_kegiatan_asal' => 47, 'kode_keg_keuangan_asal' => 48, 'th_ke_multiyears' => 49, 'kelompok_sasaran' => 50, 'pagu_bappeko' => 51, 'kode_dpa' => 52, 'user_id_pptk' => 53, 'user_id_kpa' => 54, 'catatan_pembahasan' => 55, 'catatan_penyelia' => 56, 'catatan_bappeko' => 57, 'status_level' => 58, 'is_tapd_setuju' => 59, 'is_bappeko_setuju' => 60, 'is_penyelia_setuju' => 61, 'is_pernah_rka' => 62, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/PakBukuPutihMasterKegiatanMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.PakBukuPutihMasterKegiatanMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = PakBukuPutihMasterKegiatanPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(PakBukuPutihMasterKegiatanPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(PakBukuPutihMasterKegiatanPeer::UNIT_ID);

		$criteria->addSelectColumn(PakBukuPutihMasterKegiatanPeer::KODE_KEGIATAN);

		$criteria->addSelectColumn(PakBukuPutihMasterKegiatanPeer::KODE_KEGIATAN_BARU);

		$criteria->addSelectColumn(PakBukuPutihMasterKegiatanPeer::KODE_BIDANG);

		$criteria->addSelectColumn(PakBukuPutihMasterKegiatanPeer::KODE_URUSAN_WAJIB);

		$criteria->addSelectColumn(PakBukuPutihMasterKegiatanPeer::KODE_PROGRAM);

		$criteria->addSelectColumn(PakBukuPutihMasterKegiatanPeer::KODE_SASARAN);

		$criteria->addSelectColumn(PakBukuPutihMasterKegiatanPeer::KODE_INDIKATOR);

		$criteria->addSelectColumn(PakBukuPutihMasterKegiatanPeer::ALOKASI_DANA);

		$criteria->addSelectColumn(PakBukuPutihMasterKegiatanPeer::NAMA_KEGIATAN);

		$criteria->addSelectColumn(PakBukuPutihMasterKegiatanPeer::MASUKAN);

		$criteria->addSelectColumn(PakBukuPutihMasterKegiatanPeer::OUTPUT);

		$criteria->addSelectColumn(PakBukuPutihMasterKegiatanPeer::OUTCOME);

		$criteria->addSelectColumn(PakBukuPutihMasterKegiatanPeer::BENEFIT);

		$criteria->addSelectColumn(PakBukuPutihMasterKegiatanPeer::IMPACT);

		$criteria->addSelectColumn(PakBukuPutihMasterKegiatanPeer::TIPE);

		$criteria->addSelectColumn(PakBukuPutihMasterKegiatanPeer::KEGIATAN_ACTIVE);

		$criteria->addSelectColumn(PakBukuPutihMasterKegiatanPeer::TO_KEGIATAN_CODE);

		$criteria->addSelectColumn(PakBukuPutihMasterKegiatanPeer::CATATAN);

		$criteria->addSelectColumn(PakBukuPutihMasterKegiatanPeer::TARGET_OUTCOME);

		$criteria->addSelectColumn(PakBukuPutihMasterKegiatanPeer::LOKASI);

		$criteria->addSelectColumn(PakBukuPutihMasterKegiatanPeer::JUMLAH_PREV);

		$criteria->addSelectColumn(PakBukuPutihMasterKegiatanPeer::JUMLAH_NOW);

		$criteria->addSelectColumn(PakBukuPutihMasterKegiatanPeer::JUMLAH_NEXT);

		$criteria->addSelectColumn(PakBukuPutihMasterKegiatanPeer::KODE_PROGRAM2);

		$criteria->addSelectColumn(PakBukuPutihMasterKegiatanPeer::KODE_URUSAN);

		$criteria->addSelectColumn(PakBukuPutihMasterKegiatanPeer::LAST_UPDATE_USER);

		$criteria->addSelectColumn(PakBukuPutihMasterKegiatanPeer::LAST_UPDATE_TIME);

		$criteria->addSelectColumn(PakBukuPutihMasterKegiatanPeer::LAST_UPDATE_IP);

		$criteria->addSelectColumn(PakBukuPutihMasterKegiatanPeer::TAHAP);

		$criteria->addSelectColumn(PakBukuPutihMasterKegiatanPeer::KODE_MISI);

		$criteria->addSelectColumn(PakBukuPutihMasterKegiatanPeer::KODE_TUJUAN);

		$criteria->addSelectColumn(PakBukuPutihMasterKegiatanPeer::RANKING);

		$criteria->addSelectColumn(PakBukuPutihMasterKegiatanPeer::NOMOR13);

		$criteria->addSelectColumn(PakBukuPutihMasterKegiatanPeer::PPA_NAMA);

		$criteria->addSelectColumn(PakBukuPutihMasterKegiatanPeer::PPA_PANGKAT);

		$criteria->addSelectColumn(PakBukuPutihMasterKegiatanPeer::PPA_NIP);

		$criteria->addSelectColumn(PakBukuPutihMasterKegiatanPeer::LANJUTAN);

		$criteria->addSelectColumn(PakBukuPutihMasterKegiatanPeer::USER_ID);

		$criteria->addSelectColumn(PakBukuPutihMasterKegiatanPeer::ID);

		$criteria->addSelectColumn(PakBukuPutihMasterKegiatanPeer::TAHUN);

		$criteria->addSelectColumn(PakBukuPutihMasterKegiatanPeer::TAMBAHAN_PAGU);

		$criteria->addSelectColumn(PakBukuPutihMasterKegiatanPeer::GENDER);

		$criteria->addSelectColumn(PakBukuPutihMasterKegiatanPeer::KODE_KEG_KEUANGAN);

		$criteria->addSelectColumn(PakBukuPutihMasterKegiatanPeer::USER_ID_LAMA);

		$criteria->addSelectColumn(PakBukuPutihMasterKegiatanPeer::INDIKATOR);

		$criteria->addSelectColumn(PakBukuPutihMasterKegiatanPeer::IS_DAK);

		$criteria->addSelectColumn(PakBukuPutihMasterKegiatanPeer::KODE_KEGIATAN_ASAL);

		$criteria->addSelectColumn(PakBukuPutihMasterKegiatanPeer::KODE_KEG_KEUANGAN_ASAL);

		$criteria->addSelectColumn(PakBukuPutihMasterKegiatanPeer::TH_KE_MULTIYEARS);

		$criteria->addSelectColumn(PakBukuPutihMasterKegiatanPeer::KELOMPOK_SASARAN);

		$criteria->addSelectColumn(PakBukuPutihMasterKegiatanPeer::PAGU_BAPPEKO);

		$criteria->addSelectColumn(PakBukuPutihMasterKegiatanPeer::KODE_DPA);

		$criteria->addSelectColumn(PakBukuPutihMasterKegiatanPeer::USER_ID_PPTK);

		$criteria->addSelectColumn(PakBukuPutihMasterKegiatanPeer::USER_ID_KPA);

		$criteria->addSelectColumn(PakBukuPutihMasterKegiatanPeer::CATATAN_PEMBAHASAN);

		$criteria->addSelectColumn(PakBukuPutihMasterKegiatanPeer::CATATAN_PENYELIA);

		$criteria->addSelectColumn(PakBukuPutihMasterKegiatanPeer::CATATAN_BAPPEKO);

		$criteria->addSelectColumn(PakBukuPutihMasterKegiatanPeer::STATUS_LEVEL);

		$criteria->addSelectColumn(PakBukuPutihMasterKegiatanPeer::IS_TAPD_SETUJU);

		$criteria->addSelectColumn(PakBukuPutihMasterKegiatanPeer::IS_BAPPEKO_SETUJU);

		$criteria->addSelectColumn(PakBukuPutihMasterKegiatanPeer::IS_PENYELIA_SETUJU);

		$criteria->addSelectColumn(PakBukuPutihMasterKegiatanPeer::IS_PERNAH_RKA);

	}

	const COUNT = 'COUNT(ebudget.pak_bukuputih_master_kegiatan.UNIT_ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.pak_bukuputih_master_kegiatan.UNIT_ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(PakBukuPutihMasterKegiatanPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(PakBukuPutihMasterKegiatanPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = PakBukuPutihMasterKegiatanPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = PakBukuPutihMasterKegiatanPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return PakBukuPutihMasterKegiatanPeer::populateObjects(PakBukuPutihMasterKegiatanPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			PakBukuPutihMasterKegiatanPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = PakBukuPutihMasterKegiatanPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return PakBukuPutihMasterKegiatanPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}

		$criteria->remove(PakBukuPutihMasterKegiatanPeer::ID); 

				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(PakBukuPutihMasterKegiatanPeer::UNIT_ID);
			$selectCriteria->add(PakBukuPutihMasterKegiatanPeer::UNIT_ID, $criteria->remove(PakBukuPutihMasterKegiatanPeer::UNIT_ID), $comparison);

			$comparison = $criteria->getComparison(PakBukuPutihMasterKegiatanPeer::KODE_KEGIATAN);
			$selectCriteria->add(PakBukuPutihMasterKegiatanPeer::KODE_KEGIATAN, $criteria->remove(PakBukuPutihMasterKegiatanPeer::KODE_KEGIATAN), $comparison);

			$comparison = $criteria->getComparison(PakBukuPutihMasterKegiatanPeer::ID);
			$selectCriteria->add(PakBukuPutihMasterKegiatanPeer::ID, $criteria->remove(PakBukuPutihMasterKegiatanPeer::ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(PakBukuPutihMasterKegiatanPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(PakBukuPutihMasterKegiatanPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof PakBukuPutihMasterKegiatan) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
												if(count($values) == count($values, COUNT_RECURSIVE))
			{
								$values = array($values);
			}
			$vals = array();
			foreach($values as $value)
			{

				$vals[0][] = $value[0];
				$vals[1][] = $value[1];
				$vals[2][] = $value[2];
			}

			$criteria->add(PakBukuPutihMasterKegiatanPeer::UNIT_ID, $vals[0], Criteria::IN);
			$criteria->add(PakBukuPutihMasterKegiatanPeer::KODE_KEGIATAN, $vals[1], Criteria::IN);
			$criteria->add(PakBukuPutihMasterKegiatanPeer::ID, $vals[2], Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(PakBukuPutihMasterKegiatan $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(PakBukuPutihMasterKegiatanPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(PakBukuPutihMasterKegiatanPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(PakBukuPutihMasterKegiatanPeer::DATABASE_NAME, PakBukuPutihMasterKegiatanPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = PakBukuPutihMasterKegiatanPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK( $unit_id, $kode_kegiatan, $id, $con = null) {
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$criteria = new Criteria();
		$criteria->add(PakBukuPutihMasterKegiatanPeer::UNIT_ID, $unit_id);
		$criteria->add(PakBukuPutihMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
		$criteria->add(PakBukuPutihMasterKegiatanPeer::ID, $id);
		$v = PakBukuPutihMasterKegiatanPeer::doSelect($criteria, $con);

		return !empty($v) ? $v[0] : null;
	}
} 
if (Propel::isInit()) {
			try {
		BasePakBukuPutihMasterKegiatanPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/PakBukuPutihMasterKegiatanMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.PakBukuPutihMasterKegiatanMapBuilder');
}
