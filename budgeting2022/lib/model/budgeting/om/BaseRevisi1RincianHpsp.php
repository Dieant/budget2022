<?php


abstract class BaseRevisi1RincianHpsp extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $unit_id;


	
	protected $kegiatan_code;


	
	protected $komponen_id;


	
	protected $detail_no;


	
	protected $id_komponen_lelang;


	
	protected $sisa_anggaran;


	
	protected $catatan;


	
	protected $tahap;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getUnitId()
	{

		return $this->unit_id;
	}

	
	public function getKegiatanCode()
	{

		return $this->kegiatan_code;
	}

	
	public function getKomponenId()
	{

		return $this->komponen_id;
	}

	
	public function getDetailNo()
	{

		return $this->detail_no;
	}

	
	public function getIdKomponenLelang()
	{

		return $this->id_komponen_lelang;
	}

	
	public function getSisaAnggaran()
	{

		return $this->sisa_anggaran;
	}

	
	public function getCatatan()
	{

		return $this->catatan;
	}

	
	public function getTahap()
	{

		return $this->tahap;
	}

	
	public function setUnitId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->unit_id !== $v) {
			$this->unit_id = $v;
			$this->modifiedColumns[] = Revisi1RincianHpspPeer::UNIT_ID;
		}

	} 
	
	public function setKegiatanCode($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kegiatan_code !== $v) {
			$this->kegiatan_code = $v;
			$this->modifiedColumns[] = Revisi1RincianHpspPeer::KEGIATAN_CODE;
		}

	} 
	
	public function setKomponenId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->komponen_id !== $v) {
			$this->komponen_id = $v;
			$this->modifiedColumns[] = Revisi1RincianHpspPeer::KOMPONEN_ID;
		}

	} 
	
	public function setDetailNo($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->detail_no !== $v) {
			$this->detail_no = $v;
			$this->modifiedColumns[] = Revisi1RincianHpspPeer::DETAIL_NO;
		}

	} 
	
	public function setIdKomponenLelang($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->id_komponen_lelang !== $v) {
			$this->id_komponen_lelang = $v;
			$this->modifiedColumns[] = Revisi1RincianHpspPeer::ID_KOMPONEN_LELANG;
		}

	} 
	
	public function setSisaAnggaran($v)
	{

		if ($this->sisa_anggaran !== $v) {
			$this->sisa_anggaran = $v;
			$this->modifiedColumns[] = Revisi1RincianHpspPeer::SISA_ANGGARAN;
		}

	} 
	
	public function setCatatan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->catatan !== $v) {
			$this->catatan = $v;
			$this->modifiedColumns[] = Revisi1RincianHpspPeer::CATATAN;
		}

	} 
	
	public function setTahap($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->tahap !== $v) {
			$this->tahap = $v;
			$this->modifiedColumns[] = Revisi1RincianHpspPeer::TAHAP;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->unit_id = $rs->getString($startcol + 0);

			$this->kegiatan_code = $rs->getString($startcol + 1);

			$this->komponen_id = $rs->getString($startcol + 2);

			$this->detail_no = $rs->getInt($startcol + 3);

			$this->id_komponen_lelang = $rs->getString($startcol + 4);

			$this->sisa_anggaran = $rs->getFloat($startcol + 5);

			$this->catatan = $rs->getString($startcol + 6);

			$this->tahap = $rs->getString($startcol + 7);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 8; 
		} catch (Exception $e) {
			throw new PropelException("Error populating Revisi1RincianHpsp object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(Revisi1RincianHpspPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			Revisi1RincianHpspPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(Revisi1RincianHpspPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = Revisi1RincianHpspPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += Revisi1RincianHpspPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = Revisi1RincianHpspPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = Revisi1RincianHpspPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getUnitId();
				break;
			case 1:
				return $this->getKegiatanCode();
				break;
			case 2:
				return $this->getKomponenId();
				break;
			case 3:
				return $this->getDetailNo();
				break;
			case 4:
				return $this->getIdKomponenLelang();
				break;
			case 5:
				return $this->getSisaAnggaran();
				break;
			case 6:
				return $this->getCatatan();
				break;
			case 7:
				return $this->getTahap();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = Revisi1RincianHpspPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getUnitId(),
			$keys[1] => $this->getKegiatanCode(),
			$keys[2] => $this->getKomponenId(),
			$keys[3] => $this->getDetailNo(),
			$keys[4] => $this->getIdKomponenLelang(),
			$keys[5] => $this->getSisaAnggaran(),
			$keys[6] => $this->getCatatan(),
			$keys[7] => $this->getTahap(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = Revisi1RincianHpspPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setUnitId($value);
				break;
			case 1:
				$this->setKegiatanCode($value);
				break;
			case 2:
				$this->setKomponenId($value);
				break;
			case 3:
				$this->setDetailNo($value);
				break;
			case 4:
				$this->setIdKomponenLelang($value);
				break;
			case 5:
				$this->setSisaAnggaran($value);
				break;
			case 6:
				$this->setCatatan($value);
				break;
			case 7:
				$this->setTahap($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = Revisi1RincianHpspPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setUnitId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setKegiatanCode($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setKomponenId($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setDetailNo($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setIdKomponenLelang($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setSisaAnggaran($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setCatatan($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setTahap($arr[$keys[7]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(Revisi1RincianHpspPeer::DATABASE_NAME);

		if ($this->isColumnModified(Revisi1RincianHpspPeer::UNIT_ID)) $criteria->add(Revisi1RincianHpspPeer::UNIT_ID, $this->unit_id);
		if ($this->isColumnModified(Revisi1RincianHpspPeer::KEGIATAN_CODE)) $criteria->add(Revisi1RincianHpspPeer::KEGIATAN_CODE, $this->kegiatan_code);
		if ($this->isColumnModified(Revisi1RincianHpspPeer::KOMPONEN_ID)) $criteria->add(Revisi1RincianHpspPeer::KOMPONEN_ID, $this->komponen_id);
		if ($this->isColumnModified(Revisi1RincianHpspPeer::DETAIL_NO)) $criteria->add(Revisi1RincianHpspPeer::DETAIL_NO, $this->detail_no);
		if ($this->isColumnModified(Revisi1RincianHpspPeer::ID_KOMPONEN_LELANG)) $criteria->add(Revisi1RincianHpspPeer::ID_KOMPONEN_LELANG, $this->id_komponen_lelang);
		if ($this->isColumnModified(Revisi1RincianHpspPeer::SISA_ANGGARAN)) $criteria->add(Revisi1RincianHpspPeer::SISA_ANGGARAN, $this->sisa_anggaran);
		if ($this->isColumnModified(Revisi1RincianHpspPeer::CATATAN)) $criteria->add(Revisi1RincianHpspPeer::CATATAN, $this->catatan);
		if ($this->isColumnModified(Revisi1RincianHpspPeer::TAHAP)) $criteria->add(Revisi1RincianHpspPeer::TAHAP, $this->tahap);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(Revisi1RincianHpspPeer::DATABASE_NAME);

		$criteria->add(Revisi1RincianHpspPeer::UNIT_ID, $this->unit_id);
		$criteria->add(Revisi1RincianHpspPeer::KEGIATAN_CODE, $this->kegiatan_code);
		$criteria->add(Revisi1RincianHpspPeer::DETAIL_NO, $this->detail_no);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		$pks = array();

		$pks[0] = $this->getUnitId();

		$pks[1] = $this->getKegiatanCode();

		$pks[2] = $this->getDetailNo();

		return $pks;
	}

	
	public function setPrimaryKey($keys)
	{

		$this->setUnitId($keys[0]);

		$this->setKegiatanCode($keys[1]);

		$this->setDetailNo($keys[2]);

	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setKomponenId($this->komponen_id);

		$copyObj->setIdKomponenLelang($this->id_komponen_lelang);

		$copyObj->setSisaAnggaran($this->sisa_anggaran);

		$copyObj->setCatatan($this->catatan);

		$copyObj->setTahap($this->tahap);


		$copyObj->setNew(true);

		$copyObj->setUnitId(NULL); 
		$copyObj->setKegiatanCode(NULL); 
		$copyObj->setDetailNo(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new Revisi1RincianHpspPeer();
		}
		return self::$peer;
	}

} 