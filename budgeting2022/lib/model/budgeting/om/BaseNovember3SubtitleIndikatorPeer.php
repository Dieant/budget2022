<?php


abstract class BaseNovember3SubtitleIndikatorPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.november3_subtitle_indikator';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.November3SubtitleIndikator';

	
	const NUM_COLUMNS = 22;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const UNIT_ID = 'ebudget.november3_subtitle_indikator.UNIT_ID';

	
	const KEGIATAN_CODE = 'ebudget.november3_subtitle_indikator.KEGIATAN_CODE';

	
	const SUBTITLE = 'ebudget.november3_subtitle_indikator.SUBTITLE';

	
	const INDIKATOR = 'ebudget.november3_subtitle_indikator.INDIKATOR';

	
	const NILAI = 'ebudget.november3_subtitle_indikator.NILAI';

	
	const SATUAN = 'ebudget.november3_subtitle_indikator.SATUAN';

	
	const LAST_UPDATE_USER = 'ebudget.november3_subtitle_indikator.LAST_UPDATE_USER';

	
	const LAST_UPDATE_TIME = 'ebudget.november3_subtitle_indikator.LAST_UPDATE_TIME';

	
	const LAST_UPDATE_IP = 'ebudget.november3_subtitle_indikator.LAST_UPDATE_IP';

	
	const TAHAP = 'ebudget.november3_subtitle_indikator.TAHAP';

	
	const SUB_ID = 'ebudget.november3_subtitle_indikator.SUB_ID';

	
	const TAHUN = 'ebudget.november3_subtitle_indikator.TAHUN';

	
	const LOCK_SUBTITLE = 'ebudget.november3_subtitle_indikator.LOCK_SUBTITLE';

	
	const PRIORITAS = 'ebudget.november3_subtitle_indikator.PRIORITAS';

	
	const CATATAN = 'ebudget.november3_subtitle_indikator.CATATAN';

	
	const LAKILAKI = 'ebudget.november3_subtitle_indikator.LAKILAKI';

	
	const PEREMPUAN = 'ebudget.november3_subtitle_indikator.PEREMPUAN';

	
	const DEWASA = 'ebudget.november3_subtitle_indikator.DEWASA';

	
	const ANAK = 'ebudget.november3_subtitle_indikator.ANAK';

	
	const LANSIA = 'ebudget.november3_subtitle_indikator.LANSIA';

	
	const INKLUSI = 'ebudget.november3_subtitle_indikator.INKLUSI';

	
	const GENDER = 'ebudget.november3_subtitle_indikator.GENDER';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('UnitId', 'KegiatanCode', 'Subtitle', 'Indikator', 'Nilai', 'Satuan', 'LastUpdateUser', 'LastUpdateTime', 'LastUpdateIp', 'Tahap', 'SubId', 'Tahun', 'LockSubtitle', 'Prioritas', 'Catatan', 'Lakilaki', 'Perempuan', 'Dewasa', 'Anak', 'Lansia', 'Inklusi', 'Gender', ),
		BasePeer::TYPE_COLNAME => array (November3SubtitleIndikatorPeer::UNIT_ID, November3SubtitleIndikatorPeer::KEGIATAN_CODE, November3SubtitleIndikatorPeer::SUBTITLE, November3SubtitleIndikatorPeer::INDIKATOR, November3SubtitleIndikatorPeer::NILAI, November3SubtitleIndikatorPeer::SATUAN, November3SubtitleIndikatorPeer::LAST_UPDATE_USER, November3SubtitleIndikatorPeer::LAST_UPDATE_TIME, November3SubtitleIndikatorPeer::LAST_UPDATE_IP, November3SubtitleIndikatorPeer::TAHAP, November3SubtitleIndikatorPeer::SUB_ID, November3SubtitleIndikatorPeer::TAHUN, November3SubtitleIndikatorPeer::LOCK_SUBTITLE, November3SubtitleIndikatorPeer::PRIORITAS, November3SubtitleIndikatorPeer::CATATAN, November3SubtitleIndikatorPeer::LAKILAKI, November3SubtitleIndikatorPeer::PEREMPUAN, November3SubtitleIndikatorPeer::DEWASA, November3SubtitleIndikatorPeer::ANAK, November3SubtitleIndikatorPeer::LANSIA, November3SubtitleIndikatorPeer::INKLUSI, November3SubtitleIndikatorPeer::GENDER, ),
		BasePeer::TYPE_FIELDNAME => array ('unit_id', 'kegiatan_code', 'subtitle', 'indikator', 'nilai', 'satuan', 'last_update_user', 'last_update_time', 'last_update_ip', 'tahap', 'sub_id', 'tahun', 'lock_subtitle', 'prioritas', 'catatan', 'lakilaki', 'perempuan', 'dewasa', 'anak', 'lansia', 'inklusi', 'gender', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('UnitId' => 0, 'KegiatanCode' => 1, 'Subtitle' => 2, 'Indikator' => 3, 'Nilai' => 4, 'Satuan' => 5, 'LastUpdateUser' => 6, 'LastUpdateTime' => 7, 'LastUpdateIp' => 8, 'Tahap' => 9, 'SubId' => 10, 'Tahun' => 11, 'LockSubtitle' => 12, 'Prioritas' => 13, 'Catatan' => 14, 'Lakilaki' => 15, 'Perempuan' => 16, 'Dewasa' => 17, 'Anak' => 18, 'Lansia' => 19, 'Inklusi' => 20, 'Gender' => 21, ),
		BasePeer::TYPE_COLNAME => array (November3SubtitleIndikatorPeer::UNIT_ID => 0, November3SubtitleIndikatorPeer::KEGIATAN_CODE => 1, November3SubtitleIndikatorPeer::SUBTITLE => 2, November3SubtitleIndikatorPeer::INDIKATOR => 3, November3SubtitleIndikatorPeer::NILAI => 4, November3SubtitleIndikatorPeer::SATUAN => 5, November3SubtitleIndikatorPeer::LAST_UPDATE_USER => 6, November3SubtitleIndikatorPeer::LAST_UPDATE_TIME => 7, November3SubtitleIndikatorPeer::LAST_UPDATE_IP => 8, November3SubtitleIndikatorPeer::TAHAP => 9, November3SubtitleIndikatorPeer::SUB_ID => 10, November3SubtitleIndikatorPeer::TAHUN => 11, November3SubtitleIndikatorPeer::LOCK_SUBTITLE => 12, November3SubtitleIndikatorPeer::PRIORITAS => 13, November3SubtitleIndikatorPeer::CATATAN => 14, November3SubtitleIndikatorPeer::LAKILAKI => 15, November3SubtitleIndikatorPeer::PEREMPUAN => 16, November3SubtitleIndikatorPeer::DEWASA => 17, November3SubtitleIndikatorPeer::ANAK => 18, November3SubtitleIndikatorPeer::LANSIA => 19, November3SubtitleIndikatorPeer::INKLUSI => 20, November3SubtitleIndikatorPeer::GENDER => 21, ),
		BasePeer::TYPE_FIELDNAME => array ('unit_id' => 0, 'kegiatan_code' => 1, 'subtitle' => 2, 'indikator' => 3, 'nilai' => 4, 'satuan' => 5, 'last_update_user' => 6, 'last_update_time' => 7, 'last_update_ip' => 8, 'tahap' => 9, 'sub_id' => 10, 'tahun' => 11, 'lock_subtitle' => 12, 'prioritas' => 13, 'catatan' => 14, 'lakilaki' => 15, 'perempuan' => 16, 'dewasa' => 17, 'anak' => 18, 'lansia' => 19, 'inklusi' => 20, 'gender' => 21, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/November3SubtitleIndikatorMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.November3SubtitleIndikatorMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = November3SubtitleIndikatorPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(November3SubtitleIndikatorPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(November3SubtitleIndikatorPeer::UNIT_ID);

		$criteria->addSelectColumn(November3SubtitleIndikatorPeer::KEGIATAN_CODE);

		$criteria->addSelectColumn(November3SubtitleIndikatorPeer::SUBTITLE);

		$criteria->addSelectColumn(November3SubtitleIndikatorPeer::INDIKATOR);

		$criteria->addSelectColumn(November3SubtitleIndikatorPeer::NILAI);

		$criteria->addSelectColumn(November3SubtitleIndikatorPeer::SATUAN);

		$criteria->addSelectColumn(November3SubtitleIndikatorPeer::LAST_UPDATE_USER);

		$criteria->addSelectColumn(November3SubtitleIndikatorPeer::LAST_UPDATE_TIME);

		$criteria->addSelectColumn(November3SubtitleIndikatorPeer::LAST_UPDATE_IP);

		$criteria->addSelectColumn(November3SubtitleIndikatorPeer::TAHAP);

		$criteria->addSelectColumn(November3SubtitleIndikatorPeer::SUB_ID);

		$criteria->addSelectColumn(November3SubtitleIndikatorPeer::TAHUN);

		$criteria->addSelectColumn(November3SubtitleIndikatorPeer::LOCK_SUBTITLE);

		$criteria->addSelectColumn(November3SubtitleIndikatorPeer::PRIORITAS);

		$criteria->addSelectColumn(November3SubtitleIndikatorPeer::CATATAN);

		$criteria->addSelectColumn(November3SubtitleIndikatorPeer::LAKILAKI);

		$criteria->addSelectColumn(November3SubtitleIndikatorPeer::PEREMPUAN);

		$criteria->addSelectColumn(November3SubtitleIndikatorPeer::DEWASA);

		$criteria->addSelectColumn(November3SubtitleIndikatorPeer::ANAK);

		$criteria->addSelectColumn(November3SubtitleIndikatorPeer::LANSIA);

		$criteria->addSelectColumn(November3SubtitleIndikatorPeer::INKLUSI);

		$criteria->addSelectColumn(November3SubtitleIndikatorPeer::GENDER);

	}

	const COUNT = 'COUNT(ebudget.november3_subtitle_indikator.SUB_ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.november3_subtitle_indikator.SUB_ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(November3SubtitleIndikatorPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(November3SubtitleIndikatorPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = November3SubtitleIndikatorPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = November3SubtitleIndikatorPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return November3SubtitleIndikatorPeer::populateObjects(November3SubtitleIndikatorPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			November3SubtitleIndikatorPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = November3SubtitleIndikatorPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return November3SubtitleIndikatorPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}

		$criteria->remove(November3SubtitleIndikatorPeer::SUB_ID); 

				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(November3SubtitleIndikatorPeer::SUB_ID);
			$selectCriteria->add(November3SubtitleIndikatorPeer::SUB_ID, $criteria->remove(November3SubtitleIndikatorPeer::SUB_ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(November3SubtitleIndikatorPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(November3SubtitleIndikatorPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof November3SubtitleIndikator) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(November3SubtitleIndikatorPeer::SUB_ID, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(November3SubtitleIndikator $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(November3SubtitleIndikatorPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(November3SubtitleIndikatorPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(November3SubtitleIndikatorPeer::DATABASE_NAME, November3SubtitleIndikatorPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = November3SubtitleIndikatorPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(November3SubtitleIndikatorPeer::DATABASE_NAME);

		$criteria->add(November3SubtitleIndikatorPeer::SUB_ID, $pk);


		$v = November3SubtitleIndikatorPeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(November3SubtitleIndikatorPeer::SUB_ID, $pks, Criteria::IN);
			$objs = November3SubtitleIndikatorPeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BaseNovember3SubtitleIndikatorPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/November3SubtitleIndikatorMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.November3SubtitleIndikatorMapBuilder');
}
