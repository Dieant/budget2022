<?php


abstract class BaseBukuSSH extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id;


	
	protected $kode;


	
	protected $nama;


	
	protected $merk;


	
	protected $spec;


	
	protected $hidden_spec;


	
	protected $satuan;


	
	protected $perubahan;


	
	protected $tanggal;


	
	protected $perubahan_revisi;


	
	protected $status_hapus;


	
	protected $harga;


	
	protected $tahun;


	
	protected $replaced_at;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getId()
	{

		return $this->id;
	}

	
	public function getKode()
	{

		return $this->kode;
	}

	
	public function getNama()
	{

		return $this->nama;
	}

	
	public function getMerk()
	{

		return $this->merk;
	}

	
	public function getSpec()
	{

		return $this->spec;
	}

	
	public function getHiddenSpec()
	{

		return $this->hidden_spec;
	}

	
	public function getSatuan()
	{

		return $this->satuan;
	}

	
	public function getPerubahan()
	{

		return $this->perubahan;
	}

	
	public function getTanggal($format = 'Y-m-d H:i:s')
	{

		if ($this->tanggal === null || $this->tanggal === '') {
			return null;
		} elseif (!is_int($this->tanggal)) {
						$ts = strtotime($this->tanggal);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [tanggal] as date/time value: " . var_export($this->tanggal, true));
			}
		} else {
			$ts = $this->tanggal;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getPerubahanRevisi()
	{

		return $this->perubahan_revisi;
	}

	
	public function getStatusHapus()
	{

		return $this->status_hapus;
	}

	
	public function getHarga()
	{

		return $this->harga;
	}

	
	public function getTahun()
	{

		return $this->tahun;
	}

	
	public function getReplacedAt($format = 'Y-m-d H:i:s')
	{

		if ($this->replaced_at === null || $this->replaced_at === '') {
			return null;
		} elseif (!is_int($this->replaced_at)) {
						$ts = strtotime($this->replaced_at);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [replaced_at] as date/time value: " . var_export($this->replaced_at, true));
			}
		} else {
			$ts = $this->replaced_at;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function setId($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = BukuSSHPeer::ID;
		}

	} 
	
	public function setKode($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode !== $v) {
			$this->kode = $v;
			$this->modifiedColumns[] = BukuSSHPeer::KODE;
		}

	} 
	
	public function setNama($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nama !== $v) {
			$this->nama = $v;
			$this->modifiedColumns[] = BukuSSHPeer::NAMA;
		}

	} 
	
	public function setMerk($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->merk !== $v) {
			$this->merk = $v;
			$this->modifiedColumns[] = BukuSSHPeer::MERK;
		}

	} 
	
	public function setSpec($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->spec !== $v) {
			$this->spec = $v;
			$this->modifiedColumns[] = BukuSSHPeer::SPEC;
		}

	} 
	
	public function setHiddenSpec($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->hidden_spec !== $v) {
			$this->hidden_spec = $v;
			$this->modifiedColumns[] = BukuSSHPeer::HIDDEN_SPEC;
		}

	} 
	
	public function setSatuan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->satuan !== $v) {
			$this->satuan = $v;
			$this->modifiedColumns[] = BukuSSHPeer::SATUAN;
		}

	} 
	
	public function setPerubahan($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->perubahan !== $v) {
			$this->perubahan = $v;
			$this->modifiedColumns[] = BukuSSHPeer::PERUBAHAN;
		}

	} 
	
	public function setTanggal($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [tanggal] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->tanggal !== $ts) {
			$this->tanggal = $ts;
			$this->modifiedColumns[] = BukuSSHPeer::TANGGAL;
		}

	} 
	
	public function setPerubahanRevisi($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->perubahan_revisi !== $v) {
			$this->perubahan_revisi = $v;
			$this->modifiedColumns[] = BukuSSHPeer::PERUBAHAN_REVISI;
		}

	} 
	
	public function setStatusHapus($v)
	{

		if ($this->status_hapus !== $v) {
			$this->status_hapus = $v;
			$this->modifiedColumns[] = BukuSSHPeer::STATUS_HAPUS;
		}

	} 
	
	public function setHarga($v)
	{

		if ($this->harga !== $v) {
			$this->harga = $v;
			$this->modifiedColumns[] = BukuSSHPeer::HARGA;
		}

	} 
	
	public function setTahun($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->tahun !== $v) {
			$this->tahun = $v;
			$this->modifiedColumns[] = BukuSSHPeer::TAHUN;
		}

	} 
	
	public function setReplacedAt($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [replaced_at] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->replaced_at !== $ts) {
			$this->replaced_at = $ts;
			$this->modifiedColumns[] = BukuSSHPeer::REPLACED_AT;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getInt($startcol + 0);

			$this->kode = $rs->getString($startcol + 1);

			$this->nama = $rs->getString($startcol + 2);

			$this->merk = $rs->getString($startcol + 3);

			$this->spec = $rs->getString($startcol + 4);

			$this->hidden_spec = $rs->getString($startcol + 5);

			$this->satuan = $rs->getString($startcol + 6);

			$this->perubahan = $rs->getInt($startcol + 7);

			$this->tanggal = $rs->getTimestamp($startcol + 8, null);

			$this->perubahan_revisi = $rs->getString($startcol + 9);

			$this->status_hapus = $rs->getBoolean($startcol + 10);

			$this->harga = $rs->getFloat($startcol + 11);

			$this->tahun = $rs->getString($startcol + 12);

			$this->replaced_at = $rs->getTimestamp($startcol + 13, null);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 14; 
		} catch (Exception $e) {
			throw new PropelException("Error populating BukuSSH object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(BukuSSHPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			BukuSSHPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(BukuSSHPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = BukuSSHPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += BukuSSHPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = BukuSSHPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = BukuSSHPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getKode();
				break;
			case 2:
				return $this->getNama();
				break;
			case 3:
				return $this->getMerk();
				break;
			case 4:
				return $this->getSpec();
				break;
			case 5:
				return $this->getHiddenSpec();
				break;
			case 6:
				return $this->getSatuan();
				break;
			case 7:
				return $this->getPerubahan();
				break;
			case 8:
				return $this->getTanggal();
				break;
			case 9:
				return $this->getPerubahanRevisi();
				break;
			case 10:
				return $this->getStatusHapus();
				break;
			case 11:
				return $this->getHarga();
				break;
			case 12:
				return $this->getTahun();
				break;
			case 13:
				return $this->getReplacedAt();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = BukuSSHPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getKode(),
			$keys[2] => $this->getNama(),
			$keys[3] => $this->getMerk(),
			$keys[4] => $this->getSpec(),
			$keys[5] => $this->getHiddenSpec(),
			$keys[6] => $this->getSatuan(),
			$keys[7] => $this->getPerubahan(),
			$keys[8] => $this->getTanggal(),
			$keys[9] => $this->getPerubahanRevisi(),
			$keys[10] => $this->getStatusHapus(),
			$keys[11] => $this->getHarga(),
			$keys[12] => $this->getTahun(),
			$keys[13] => $this->getReplacedAt(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = BukuSSHPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setKode($value);
				break;
			case 2:
				$this->setNama($value);
				break;
			case 3:
				$this->setMerk($value);
				break;
			case 4:
				$this->setSpec($value);
				break;
			case 5:
				$this->setHiddenSpec($value);
				break;
			case 6:
				$this->setSatuan($value);
				break;
			case 7:
				$this->setPerubahan($value);
				break;
			case 8:
				$this->setTanggal($value);
				break;
			case 9:
				$this->setPerubahanRevisi($value);
				break;
			case 10:
				$this->setStatusHapus($value);
				break;
			case 11:
				$this->setHarga($value);
				break;
			case 12:
				$this->setTahun($value);
				break;
			case 13:
				$this->setReplacedAt($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = BukuSSHPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setKode($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setNama($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setMerk($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setSpec($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setHiddenSpec($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setSatuan($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setPerubahan($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setTanggal($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setPerubahanRevisi($arr[$keys[9]]);
		if (array_key_exists($keys[10], $arr)) $this->setStatusHapus($arr[$keys[10]]);
		if (array_key_exists($keys[11], $arr)) $this->setHarga($arr[$keys[11]]);
		if (array_key_exists($keys[12], $arr)) $this->setTahun($arr[$keys[12]]);
		if (array_key_exists($keys[13], $arr)) $this->setReplacedAt($arr[$keys[13]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(BukuSSHPeer::DATABASE_NAME);

		if ($this->isColumnModified(BukuSSHPeer::ID)) $criteria->add(BukuSSHPeer::ID, $this->id);
		if ($this->isColumnModified(BukuSSHPeer::KODE)) $criteria->add(BukuSSHPeer::KODE, $this->kode);
		if ($this->isColumnModified(BukuSSHPeer::NAMA)) $criteria->add(BukuSSHPeer::NAMA, $this->nama);
		if ($this->isColumnModified(BukuSSHPeer::MERK)) $criteria->add(BukuSSHPeer::MERK, $this->merk);
		if ($this->isColumnModified(BukuSSHPeer::SPEC)) $criteria->add(BukuSSHPeer::SPEC, $this->spec);
		if ($this->isColumnModified(BukuSSHPeer::HIDDEN_SPEC)) $criteria->add(BukuSSHPeer::HIDDEN_SPEC, $this->hidden_spec);
		if ($this->isColumnModified(BukuSSHPeer::SATUAN)) $criteria->add(BukuSSHPeer::SATUAN, $this->satuan);
		if ($this->isColumnModified(BukuSSHPeer::PERUBAHAN)) $criteria->add(BukuSSHPeer::PERUBAHAN, $this->perubahan);
		if ($this->isColumnModified(BukuSSHPeer::TANGGAL)) $criteria->add(BukuSSHPeer::TANGGAL, $this->tanggal);
		if ($this->isColumnModified(BukuSSHPeer::PERUBAHAN_REVISI)) $criteria->add(BukuSSHPeer::PERUBAHAN_REVISI, $this->perubahan_revisi);
		if ($this->isColumnModified(BukuSSHPeer::STATUS_HAPUS)) $criteria->add(BukuSSHPeer::STATUS_HAPUS, $this->status_hapus);
		if ($this->isColumnModified(BukuSSHPeer::HARGA)) $criteria->add(BukuSSHPeer::HARGA, $this->harga);
		if ($this->isColumnModified(BukuSSHPeer::TAHUN)) $criteria->add(BukuSSHPeer::TAHUN, $this->tahun);
		if ($this->isColumnModified(BukuSSHPeer::REPLACED_AT)) $criteria->add(BukuSSHPeer::REPLACED_AT, $this->replaced_at);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(BukuSSHPeer::DATABASE_NAME);

		$criteria->add(BukuSSHPeer::ID, $this->id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setKode($this->kode);

		$copyObj->setNama($this->nama);

		$copyObj->setMerk($this->merk);

		$copyObj->setSpec($this->spec);

		$copyObj->setHiddenSpec($this->hidden_spec);

		$copyObj->setSatuan($this->satuan);

		$copyObj->setPerubahan($this->perubahan);

		$copyObj->setTanggal($this->tanggal);

		$copyObj->setPerubahanRevisi($this->perubahan_revisi);

		$copyObj->setStatusHapus($this->status_hapus);

		$copyObj->setHarga($this->harga);

		$copyObj->setTahun($this->tahun);

		$copyObj->setReplacedAt($this->replaced_at);


		$copyObj->setNew(true);

		$copyObj->setId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new BukuSSHPeer();
		}
		return self::$peer;
	}

} 