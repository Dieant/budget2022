<?php


abstract class BasePakBukuPutihSubtitleIndikator extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $unit_id;


	
	protected $kegiatan_code;


	
	protected $subtitle;


	
	protected $indikator;


	
	protected $nilai;


	
	protected $satuan;


	
	protected $last_update_user;


	
	protected $last_update_time;


	
	protected $last_update_ip;


	
	protected $tahap;


	
	protected $sub_id;


	
	protected $tahun;


	
	protected $lock_subtitle = false;


	
	protected $prioritas = 0;


	
	protected $catatan;


	
	protected $lakilaki;


	
	protected $perempuan;


	
	protected $dewasa;


	
	protected $anak;


	
	protected $lansia;


	
	protected $inklusi;


	
	protected $gender = false;


	
	protected $is_nol_anggaran = false;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getUnitId()
	{

		return $this->unit_id;
	}

	
	public function getKegiatanCode()
	{

		return $this->kegiatan_code;
	}

	
	public function getSubtitle()
	{

		return $this->subtitle;
	}

	
	public function getIndikator()
	{

		return $this->indikator;
	}

	
	public function getNilai()
	{

		return $this->nilai;
	}

	
	public function getSatuan()
	{

		return $this->satuan;
	}

	
	public function getLastUpdateUser()
	{

		return $this->last_update_user;
	}

	
	public function getLastUpdateTime($format = 'Y-m-d H:i:s')
	{

		if ($this->last_update_time === null || $this->last_update_time === '') {
			return null;
		} elseif (!is_int($this->last_update_time)) {
						$ts = strtotime($this->last_update_time);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [last_update_time] as date/time value: " . var_export($this->last_update_time, true));
			}
		} else {
			$ts = $this->last_update_time;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getLastUpdateIp()
	{

		return $this->last_update_ip;
	}

	
	public function getTahap()
	{

		return $this->tahap;
	}

	
	public function getSubId()
	{

		return $this->sub_id;
	}

	
	public function getTahun()
	{

		return $this->tahun;
	}

	
	public function getLockSubtitle()
	{

		return $this->lock_subtitle;
	}

	
	public function getPrioritas()
	{

		return $this->prioritas;
	}

	
	public function getCatatan()
	{

		return $this->catatan;
	}

	
	public function getLakilaki()
	{

		return $this->lakilaki;
	}

	
	public function getPerempuan()
	{

		return $this->perempuan;
	}

	
	public function getDewasa()
	{

		return $this->dewasa;
	}

	
	public function getAnak()
	{

		return $this->anak;
	}

	
	public function getLansia()
	{

		return $this->lansia;
	}

	
	public function getInklusi()
	{

		return $this->inklusi;
	}

	
	public function getGender()
	{

		return $this->gender;
	}

	
	public function getIsNolAnggaran()
	{

		return $this->is_nol_anggaran;
	}

	
	public function setUnitId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->unit_id !== $v) {
			$this->unit_id = $v;
			$this->modifiedColumns[] = PakBukuPutihSubtitleIndikatorPeer::UNIT_ID;
		}

	} 
	
	public function setKegiatanCode($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kegiatan_code !== $v) {
			$this->kegiatan_code = $v;
			$this->modifiedColumns[] = PakBukuPutihSubtitleIndikatorPeer::KEGIATAN_CODE;
		}

	} 
	
	public function setSubtitle($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->subtitle !== $v) {
			$this->subtitle = $v;
			$this->modifiedColumns[] = PakBukuPutihSubtitleIndikatorPeer::SUBTITLE;
		}

	} 
	
	public function setIndikator($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->indikator !== $v) {
			$this->indikator = $v;
			$this->modifiedColumns[] = PakBukuPutihSubtitleIndikatorPeer::INDIKATOR;
		}

	} 
	
	public function setNilai($v)
	{

		if ($this->nilai !== $v) {
			$this->nilai = $v;
			$this->modifiedColumns[] = PakBukuPutihSubtitleIndikatorPeer::NILAI;
		}

	} 
	
	public function setSatuan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->satuan !== $v) {
			$this->satuan = $v;
			$this->modifiedColumns[] = PakBukuPutihSubtitleIndikatorPeer::SATUAN;
		}

	} 
	
	public function setLastUpdateUser($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->last_update_user !== $v) {
			$this->last_update_user = $v;
			$this->modifiedColumns[] = PakBukuPutihSubtitleIndikatorPeer::LAST_UPDATE_USER;
		}

	} 
	
	public function setLastUpdateTime($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [last_update_time] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->last_update_time !== $ts) {
			$this->last_update_time = $ts;
			$this->modifiedColumns[] = PakBukuPutihSubtitleIndikatorPeer::LAST_UPDATE_TIME;
		}

	} 
	
	public function setLastUpdateIp($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->last_update_ip !== $v) {
			$this->last_update_ip = $v;
			$this->modifiedColumns[] = PakBukuPutihSubtitleIndikatorPeer::LAST_UPDATE_IP;
		}

	} 
	
	public function setTahap($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->tahap !== $v) {
			$this->tahap = $v;
			$this->modifiedColumns[] = PakBukuPutihSubtitleIndikatorPeer::TAHAP;
		}

	} 
	
	public function setSubId($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->sub_id !== $v) {
			$this->sub_id = $v;
			$this->modifiedColumns[] = PakBukuPutihSubtitleIndikatorPeer::SUB_ID;
		}

	} 
	
	public function setTahun($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->tahun !== $v) {
			$this->tahun = $v;
			$this->modifiedColumns[] = PakBukuPutihSubtitleIndikatorPeer::TAHUN;
		}

	} 
	
	public function setLockSubtitle($v)
	{

		if ($this->lock_subtitle !== $v || $v === false) {
			$this->lock_subtitle = $v;
			$this->modifiedColumns[] = PakBukuPutihSubtitleIndikatorPeer::LOCK_SUBTITLE;
		}

	} 
	
	public function setPrioritas($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->prioritas !== $v || $v === 0) {
			$this->prioritas = $v;
			$this->modifiedColumns[] = PakBukuPutihSubtitleIndikatorPeer::PRIORITAS;
		}

	} 
	
	public function setCatatan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->catatan !== $v) {
			$this->catatan = $v;
			$this->modifiedColumns[] = PakBukuPutihSubtitleIndikatorPeer::CATATAN;
		}

	} 
	
	public function setLakilaki($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->lakilaki !== $v) {
			$this->lakilaki = $v;
			$this->modifiedColumns[] = PakBukuPutihSubtitleIndikatorPeer::LAKILAKI;
		}

	} 
	
	public function setPerempuan($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->perempuan !== $v) {
			$this->perempuan = $v;
			$this->modifiedColumns[] = PakBukuPutihSubtitleIndikatorPeer::PEREMPUAN;
		}

	} 
	
	public function setDewasa($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->dewasa !== $v) {
			$this->dewasa = $v;
			$this->modifiedColumns[] = PakBukuPutihSubtitleIndikatorPeer::DEWASA;
		}

	} 
	
	public function setAnak($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->anak !== $v) {
			$this->anak = $v;
			$this->modifiedColumns[] = PakBukuPutihSubtitleIndikatorPeer::ANAK;
		}

	} 
	
	public function setLansia($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->lansia !== $v) {
			$this->lansia = $v;
			$this->modifiedColumns[] = PakBukuPutihSubtitleIndikatorPeer::LANSIA;
		}

	} 
	
	public function setInklusi($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->inklusi !== $v) {
			$this->inklusi = $v;
			$this->modifiedColumns[] = PakBukuPutihSubtitleIndikatorPeer::INKLUSI;
		}

	} 
	
	public function setGender($v)
	{

		if ($this->gender !== $v || $v === false) {
			$this->gender = $v;
			$this->modifiedColumns[] = PakBukuPutihSubtitleIndikatorPeer::GENDER;
		}

	} 
	
	public function setIsNolAnggaran($v)
	{

		if ($this->is_nol_anggaran !== $v || $v === false) {
			$this->is_nol_anggaran = $v;
			$this->modifiedColumns[] = PakBukuPutihSubtitleIndikatorPeer::IS_NOL_ANGGARAN;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->unit_id = $rs->getString($startcol + 0);

			$this->kegiatan_code = $rs->getString($startcol + 1);

			$this->subtitle = $rs->getString($startcol + 2);

			$this->indikator = $rs->getString($startcol + 3);

			$this->nilai = $rs->getFloat($startcol + 4);

			$this->satuan = $rs->getString($startcol + 5);

			$this->last_update_user = $rs->getString($startcol + 6);

			$this->last_update_time = $rs->getTimestamp($startcol + 7, null);

			$this->last_update_ip = $rs->getString($startcol + 8);

			$this->tahap = $rs->getString($startcol + 9);

			$this->sub_id = $rs->getInt($startcol + 10);

			$this->tahun = $rs->getString($startcol + 11);

			$this->lock_subtitle = $rs->getBoolean($startcol + 12);

			$this->prioritas = $rs->getInt($startcol + 13);

			$this->catatan = $rs->getString($startcol + 14);

			$this->lakilaki = $rs->getInt($startcol + 15);

			$this->perempuan = $rs->getInt($startcol + 16);

			$this->dewasa = $rs->getInt($startcol + 17);

			$this->anak = $rs->getInt($startcol + 18);

			$this->lansia = $rs->getInt($startcol + 19);

			$this->inklusi = $rs->getInt($startcol + 20);

			$this->gender = $rs->getBoolean($startcol + 21);

			$this->is_nol_anggaran = $rs->getBoolean($startcol + 22);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 23; 
		} catch (Exception $e) {
			throw new PropelException("Error populating PakBukuPutihSubtitleIndikator object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(PakBukuPutihSubtitleIndikatorPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			PakBukuPutihSubtitleIndikatorPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(PakBukuPutihSubtitleIndikatorPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = PakBukuPutihSubtitleIndikatorPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setSubId($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += PakBukuPutihSubtitleIndikatorPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = PakBukuPutihSubtitleIndikatorPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = PakBukuPutihSubtitleIndikatorPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getUnitId();
				break;
			case 1:
				return $this->getKegiatanCode();
				break;
			case 2:
				return $this->getSubtitle();
				break;
			case 3:
				return $this->getIndikator();
				break;
			case 4:
				return $this->getNilai();
				break;
			case 5:
				return $this->getSatuan();
				break;
			case 6:
				return $this->getLastUpdateUser();
				break;
			case 7:
				return $this->getLastUpdateTime();
				break;
			case 8:
				return $this->getLastUpdateIp();
				break;
			case 9:
				return $this->getTahap();
				break;
			case 10:
				return $this->getSubId();
				break;
			case 11:
				return $this->getTahun();
				break;
			case 12:
				return $this->getLockSubtitle();
				break;
			case 13:
				return $this->getPrioritas();
				break;
			case 14:
				return $this->getCatatan();
				break;
			case 15:
				return $this->getLakilaki();
				break;
			case 16:
				return $this->getPerempuan();
				break;
			case 17:
				return $this->getDewasa();
				break;
			case 18:
				return $this->getAnak();
				break;
			case 19:
				return $this->getLansia();
				break;
			case 20:
				return $this->getInklusi();
				break;
			case 21:
				return $this->getGender();
				break;
			case 22:
				return $this->getIsNolAnggaran();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = PakBukuPutihSubtitleIndikatorPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getUnitId(),
			$keys[1] => $this->getKegiatanCode(),
			$keys[2] => $this->getSubtitle(),
			$keys[3] => $this->getIndikator(),
			$keys[4] => $this->getNilai(),
			$keys[5] => $this->getSatuan(),
			$keys[6] => $this->getLastUpdateUser(),
			$keys[7] => $this->getLastUpdateTime(),
			$keys[8] => $this->getLastUpdateIp(),
			$keys[9] => $this->getTahap(),
			$keys[10] => $this->getSubId(),
			$keys[11] => $this->getTahun(),
			$keys[12] => $this->getLockSubtitle(),
			$keys[13] => $this->getPrioritas(),
			$keys[14] => $this->getCatatan(),
			$keys[15] => $this->getLakilaki(),
			$keys[16] => $this->getPerempuan(),
			$keys[17] => $this->getDewasa(),
			$keys[18] => $this->getAnak(),
			$keys[19] => $this->getLansia(),
			$keys[20] => $this->getInklusi(),
			$keys[21] => $this->getGender(),
			$keys[22] => $this->getIsNolAnggaran(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = PakBukuPutihSubtitleIndikatorPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setUnitId($value);
				break;
			case 1:
				$this->setKegiatanCode($value);
				break;
			case 2:
				$this->setSubtitle($value);
				break;
			case 3:
				$this->setIndikator($value);
				break;
			case 4:
				$this->setNilai($value);
				break;
			case 5:
				$this->setSatuan($value);
				break;
			case 6:
				$this->setLastUpdateUser($value);
				break;
			case 7:
				$this->setLastUpdateTime($value);
				break;
			case 8:
				$this->setLastUpdateIp($value);
				break;
			case 9:
				$this->setTahap($value);
				break;
			case 10:
				$this->setSubId($value);
				break;
			case 11:
				$this->setTahun($value);
				break;
			case 12:
				$this->setLockSubtitle($value);
				break;
			case 13:
				$this->setPrioritas($value);
				break;
			case 14:
				$this->setCatatan($value);
				break;
			case 15:
				$this->setLakilaki($value);
				break;
			case 16:
				$this->setPerempuan($value);
				break;
			case 17:
				$this->setDewasa($value);
				break;
			case 18:
				$this->setAnak($value);
				break;
			case 19:
				$this->setLansia($value);
				break;
			case 20:
				$this->setInklusi($value);
				break;
			case 21:
				$this->setGender($value);
				break;
			case 22:
				$this->setIsNolAnggaran($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = PakBukuPutihSubtitleIndikatorPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setUnitId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setKegiatanCode($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setSubtitle($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setIndikator($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setNilai($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setSatuan($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setLastUpdateUser($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setLastUpdateTime($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setLastUpdateIp($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setTahap($arr[$keys[9]]);
		if (array_key_exists($keys[10], $arr)) $this->setSubId($arr[$keys[10]]);
		if (array_key_exists($keys[11], $arr)) $this->setTahun($arr[$keys[11]]);
		if (array_key_exists($keys[12], $arr)) $this->setLockSubtitle($arr[$keys[12]]);
		if (array_key_exists($keys[13], $arr)) $this->setPrioritas($arr[$keys[13]]);
		if (array_key_exists($keys[14], $arr)) $this->setCatatan($arr[$keys[14]]);
		if (array_key_exists($keys[15], $arr)) $this->setLakilaki($arr[$keys[15]]);
		if (array_key_exists($keys[16], $arr)) $this->setPerempuan($arr[$keys[16]]);
		if (array_key_exists($keys[17], $arr)) $this->setDewasa($arr[$keys[17]]);
		if (array_key_exists($keys[18], $arr)) $this->setAnak($arr[$keys[18]]);
		if (array_key_exists($keys[19], $arr)) $this->setLansia($arr[$keys[19]]);
		if (array_key_exists($keys[20], $arr)) $this->setInklusi($arr[$keys[20]]);
		if (array_key_exists($keys[21], $arr)) $this->setGender($arr[$keys[21]]);
		if (array_key_exists($keys[22], $arr)) $this->setIsNolAnggaran($arr[$keys[22]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(PakBukuPutihSubtitleIndikatorPeer::DATABASE_NAME);

		if ($this->isColumnModified(PakBukuPutihSubtitleIndikatorPeer::UNIT_ID)) $criteria->add(PakBukuPutihSubtitleIndikatorPeer::UNIT_ID, $this->unit_id);
		if ($this->isColumnModified(PakBukuPutihSubtitleIndikatorPeer::KEGIATAN_CODE)) $criteria->add(PakBukuPutihSubtitleIndikatorPeer::KEGIATAN_CODE, $this->kegiatan_code);
		if ($this->isColumnModified(PakBukuPutihSubtitleIndikatorPeer::SUBTITLE)) $criteria->add(PakBukuPutihSubtitleIndikatorPeer::SUBTITLE, $this->subtitle);
		if ($this->isColumnModified(PakBukuPutihSubtitleIndikatorPeer::INDIKATOR)) $criteria->add(PakBukuPutihSubtitleIndikatorPeer::INDIKATOR, $this->indikator);
		if ($this->isColumnModified(PakBukuPutihSubtitleIndikatorPeer::NILAI)) $criteria->add(PakBukuPutihSubtitleIndikatorPeer::NILAI, $this->nilai);
		if ($this->isColumnModified(PakBukuPutihSubtitleIndikatorPeer::SATUAN)) $criteria->add(PakBukuPutihSubtitleIndikatorPeer::SATUAN, $this->satuan);
		if ($this->isColumnModified(PakBukuPutihSubtitleIndikatorPeer::LAST_UPDATE_USER)) $criteria->add(PakBukuPutihSubtitleIndikatorPeer::LAST_UPDATE_USER, $this->last_update_user);
		if ($this->isColumnModified(PakBukuPutihSubtitleIndikatorPeer::LAST_UPDATE_TIME)) $criteria->add(PakBukuPutihSubtitleIndikatorPeer::LAST_UPDATE_TIME, $this->last_update_time);
		if ($this->isColumnModified(PakBukuPutihSubtitleIndikatorPeer::LAST_UPDATE_IP)) $criteria->add(PakBukuPutihSubtitleIndikatorPeer::LAST_UPDATE_IP, $this->last_update_ip);
		if ($this->isColumnModified(PakBukuPutihSubtitleIndikatorPeer::TAHAP)) $criteria->add(PakBukuPutihSubtitleIndikatorPeer::TAHAP, $this->tahap);
		if ($this->isColumnModified(PakBukuPutihSubtitleIndikatorPeer::SUB_ID)) $criteria->add(PakBukuPutihSubtitleIndikatorPeer::SUB_ID, $this->sub_id);
		if ($this->isColumnModified(PakBukuPutihSubtitleIndikatorPeer::TAHUN)) $criteria->add(PakBukuPutihSubtitleIndikatorPeer::TAHUN, $this->tahun);
		if ($this->isColumnModified(PakBukuPutihSubtitleIndikatorPeer::LOCK_SUBTITLE)) $criteria->add(PakBukuPutihSubtitleIndikatorPeer::LOCK_SUBTITLE, $this->lock_subtitle);
		if ($this->isColumnModified(PakBukuPutihSubtitleIndikatorPeer::PRIORITAS)) $criteria->add(PakBukuPutihSubtitleIndikatorPeer::PRIORITAS, $this->prioritas);
		if ($this->isColumnModified(PakBukuPutihSubtitleIndikatorPeer::CATATAN)) $criteria->add(PakBukuPutihSubtitleIndikatorPeer::CATATAN, $this->catatan);
		if ($this->isColumnModified(PakBukuPutihSubtitleIndikatorPeer::LAKILAKI)) $criteria->add(PakBukuPutihSubtitleIndikatorPeer::LAKILAKI, $this->lakilaki);
		if ($this->isColumnModified(PakBukuPutihSubtitleIndikatorPeer::PEREMPUAN)) $criteria->add(PakBukuPutihSubtitleIndikatorPeer::PEREMPUAN, $this->perempuan);
		if ($this->isColumnModified(PakBukuPutihSubtitleIndikatorPeer::DEWASA)) $criteria->add(PakBukuPutihSubtitleIndikatorPeer::DEWASA, $this->dewasa);
		if ($this->isColumnModified(PakBukuPutihSubtitleIndikatorPeer::ANAK)) $criteria->add(PakBukuPutihSubtitleIndikatorPeer::ANAK, $this->anak);
		if ($this->isColumnModified(PakBukuPutihSubtitleIndikatorPeer::LANSIA)) $criteria->add(PakBukuPutihSubtitleIndikatorPeer::LANSIA, $this->lansia);
		if ($this->isColumnModified(PakBukuPutihSubtitleIndikatorPeer::INKLUSI)) $criteria->add(PakBukuPutihSubtitleIndikatorPeer::INKLUSI, $this->inklusi);
		if ($this->isColumnModified(PakBukuPutihSubtitleIndikatorPeer::GENDER)) $criteria->add(PakBukuPutihSubtitleIndikatorPeer::GENDER, $this->gender);
		if ($this->isColumnModified(PakBukuPutihSubtitleIndikatorPeer::IS_NOL_ANGGARAN)) $criteria->add(PakBukuPutihSubtitleIndikatorPeer::IS_NOL_ANGGARAN, $this->is_nol_anggaran);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(PakBukuPutihSubtitleIndikatorPeer::DATABASE_NAME);

		$criteria->add(PakBukuPutihSubtitleIndikatorPeer::SUB_ID, $this->sub_id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getSubId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setSubId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setUnitId($this->unit_id);

		$copyObj->setKegiatanCode($this->kegiatan_code);

		$copyObj->setSubtitle($this->subtitle);

		$copyObj->setIndikator($this->indikator);

		$copyObj->setNilai($this->nilai);

		$copyObj->setSatuan($this->satuan);

		$copyObj->setLastUpdateUser($this->last_update_user);

		$copyObj->setLastUpdateTime($this->last_update_time);

		$copyObj->setLastUpdateIp($this->last_update_ip);

		$copyObj->setTahap($this->tahap);

		$copyObj->setTahun($this->tahun);

		$copyObj->setLockSubtitle($this->lock_subtitle);

		$copyObj->setPrioritas($this->prioritas);

		$copyObj->setCatatan($this->catatan);

		$copyObj->setLakilaki($this->lakilaki);

		$copyObj->setPerempuan($this->perempuan);

		$copyObj->setDewasa($this->dewasa);

		$copyObj->setAnak($this->anak);

		$copyObj->setLansia($this->lansia);

		$copyObj->setInklusi($this->inklusi);

		$copyObj->setGender($this->gender);

		$copyObj->setIsNolAnggaran($this->is_nol_anggaran);


		$copyObj->setNew(true);

		$copyObj->setSubId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new PakBukuPutihSubtitleIndikatorPeer();
		}
		return self::$peer;
	}

} 