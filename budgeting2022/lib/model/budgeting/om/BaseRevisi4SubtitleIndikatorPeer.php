<?php


abstract class BaseRevisi4SubtitleIndikatorPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.revisi4_subtitle_indikator';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.Revisi4SubtitleIndikator';

	
	const NUM_COLUMNS = 22;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const UNIT_ID = 'ebudget.revisi4_subtitle_indikator.UNIT_ID';

	
	const KEGIATAN_CODE = 'ebudget.revisi4_subtitle_indikator.KEGIATAN_CODE';

	
	const SUBTITLE = 'ebudget.revisi4_subtitle_indikator.SUBTITLE';

	
	const INDIKATOR = 'ebudget.revisi4_subtitle_indikator.INDIKATOR';

	
	const NILAI = 'ebudget.revisi4_subtitle_indikator.NILAI';

	
	const SATUAN = 'ebudget.revisi4_subtitle_indikator.SATUAN';

	
	const LAST_UPDATE_USER = 'ebudget.revisi4_subtitle_indikator.LAST_UPDATE_USER';

	
	const LAST_UPDATE_TIME = 'ebudget.revisi4_subtitle_indikator.LAST_UPDATE_TIME';

	
	const LAST_UPDATE_IP = 'ebudget.revisi4_subtitle_indikator.LAST_UPDATE_IP';

	
	const TAHAP = 'ebudget.revisi4_subtitle_indikator.TAHAP';

	
	const SUB_ID = 'ebudget.revisi4_subtitle_indikator.SUB_ID';

	
	const TAHUN = 'ebudget.revisi4_subtitle_indikator.TAHUN';

	
	const LOCK_SUBTITLE = 'ebudget.revisi4_subtitle_indikator.LOCK_SUBTITLE';

	
	const PRIORITAS = 'ebudget.revisi4_subtitle_indikator.PRIORITAS';

	
	const CATATAN = 'ebudget.revisi4_subtitle_indikator.CATATAN';

	
	const LAKILAKI = 'ebudget.revisi4_subtitle_indikator.LAKILAKI';

	
	const PEREMPUAN = 'ebudget.revisi4_subtitle_indikator.PEREMPUAN';

	
	const DEWASA = 'ebudget.revisi4_subtitle_indikator.DEWASA';

	
	const ANAK = 'ebudget.revisi4_subtitle_indikator.ANAK';

	
	const LANSIA = 'ebudget.revisi4_subtitle_indikator.LANSIA';

	
	const INKLUSI = 'ebudget.revisi4_subtitle_indikator.INKLUSI';

	
	const GENDER = 'ebudget.revisi4_subtitle_indikator.GENDER';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('UnitId', 'KegiatanCode', 'Subtitle', 'Indikator', 'Nilai', 'Satuan', 'LastUpdateUser', 'LastUpdateTime', 'LastUpdateIp', 'Tahap', 'SubId', 'Tahun', 'LockSubtitle', 'Prioritas', 'Catatan', 'Lakilaki', 'Perempuan', 'Dewasa', 'Anak', 'Lansia', 'Inklusi', 'Gender', ),
		BasePeer::TYPE_COLNAME => array (Revisi4SubtitleIndikatorPeer::UNIT_ID, Revisi4SubtitleIndikatorPeer::KEGIATAN_CODE, Revisi4SubtitleIndikatorPeer::SUBTITLE, Revisi4SubtitleIndikatorPeer::INDIKATOR, Revisi4SubtitleIndikatorPeer::NILAI, Revisi4SubtitleIndikatorPeer::SATUAN, Revisi4SubtitleIndikatorPeer::LAST_UPDATE_USER, Revisi4SubtitleIndikatorPeer::LAST_UPDATE_TIME, Revisi4SubtitleIndikatorPeer::LAST_UPDATE_IP, Revisi4SubtitleIndikatorPeer::TAHAP, Revisi4SubtitleIndikatorPeer::SUB_ID, Revisi4SubtitleIndikatorPeer::TAHUN, Revisi4SubtitleIndikatorPeer::LOCK_SUBTITLE, Revisi4SubtitleIndikatorPeer::PRIORITAS, Revisi4SubtitleIndikatorPeer::CATATAN, Revisi4SubtitleIndikatorPeer::LAKILAKI, Revisi4SubtitleIndikatorPeer::PEREMPUAN, Revisi4SubtitleIndikatorPeer::DEWASA, Revisi4SubtitleIndikatorPeer::ANAK, Revisi4SubtitleIndikatorPeer::LANSIA, Revisi4SubtitleIndikatorPeer::INKLUSI, Revisi4SubtitleIndikatorPeer::GENDER, ),
		BasePeer::TYPE_FIELDNAME => array ('unit_id', 'kegiatan_code', 'subtitle', 'indikator', 'nilai', 'satuan', 'last_update_user', 'last_update_time', 'last_update_ip', 'tahap', 'sub_id', 'tahun', 'lock_subtitle', 'prioritas', 'catatan', 'lakilaki', 'perempuan', 'dewasa', 'anak', 'lansia', 'inklusi', 'gender', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('UnitId' => 0, 'KegiatanCode' => 1, 'Subtitle' => 2, 'Indikator' => 3, 'Nilai' => 4, 'Satuan' => 5, 'LastUpdateUser' => 6, 'LastUpdateTime' => 7, 'LastUpdateIp' => 8, 'Tahap' => 9, 'SubId' => 10, 'Tahun' => 11, 'LockSubtitle' => 12, 'Prioritas' => 13, 'Catatan' => 14, 'Lakilaki' => 15, 'Perempuan' => 16, 'Dewasa' => 17, 'Anak' => 18, 'Lansia' => 19, 'Inklusi' => 20, 'Gender' => 21, ),
		BasePeer::TYPE_COLNAME => array (Revisi4SubtitleIndikatorPeer::UNIT_ID => 0, Revisi4SubtitleIndikatorPeer::KEGIATAN_CODE => 1, Revisi4SubtitleIndikatorPeer::SUBTITLE => 2, Revisi4SubtitleIndikatorPeer::INDIKATOR => 3, Revisi4SubtitleIndikatorPeer::NILAI => 4, Revisi4SubtitleIndikatorPeer::SATUAN => 5, Revisi4SubtitleIndikatorPeer::LAST_UPDATE_USER => 6, Revisi4SubtitleIndikatorPeer::LAST_UPDATE_TIME => 7, Revisi4SubtitleIndikatorPeer::LAST_UPDATE_IP => 8, Revisi4SubtitleIndikatorPeer::TAHAP => 9, Revisi4SubtitleIndikatorPeer::SUB_ID => 10, Revisi4SubtitleIndikatorPeer::TAHUN => 11, Revisi4SubtitleIndikatorPeer::LOCK_SUBTITLE => 12, Revisi4SubtitleIndikatorPeer::PRIORITAS => 13, Revisi4SubtitleIndikatorPeer::CATATAN => 14, Revisi4SubtitleIndikatorPeer::LAKILAKI => 15, Revisi4SubtitleIndikatorPeer::PEREMPUAN => 16, Revisi4SubtitleIndikatorPeer::DEWASA => 17, Revisi4SubtitleIndikatorPeer::ANAK => 18, Revisi4SubtitleIndikatorPeer::LANSIA => 19, Revisi4SubtitleIndikatorPeer::INKLUSI => 20, Revisi4SubtitleIndikatorPeer::GENDER => 21, ),
		BasePeer::TYPE_FIELDNAME => array ('unit_id' => 0, 'kegiatan_code' => 1, 'subtitle' => 2, 'indikator' => 3, 'nilai' => 4, 'satuan' => 5, 'last_update_user' => 6, 'last_update_time' => 7, 'last_update_ip' => 8, 'tahap' => 9, 'sub_id' => 10, 'tahun' => 11, 'lock_subtitle' => 12, 'prioritas' => 13, 'catatan' => 14, 'lakilaki' => 15, 'perempuan' => 16, 'dewasa' => 17, 'anak' => 18, 'lansia' => 19, 'inklusi' => 20, 'gender' => 21, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/Revisi4SubtitleIndikatorMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.Revisi4SubtitleIndikatorMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = Revisi4SubtitleIndikatorPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(Revisi4SubtitleIndikatorPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(Revisi4SubtitleIndikatorPeer::UNIT_ID);

		$criteria->addSelectColumn(Revisi4SubtitleIndikatorPeer::KEGIATAN_CODE);

		$criteria->addSelectColumn(Revisi4SubtitleIndikatorPeer::SUBTITLE);

		$criteria->addSelectColumn(Revisi4SubtitleIndikatorPeer::INDIKATOR);

		$criteria->addSelectColumn(Revisi4SubtitleIndikatorPeer::NILAI);

		$criteria->addSelectColumn(Revisi4SubtitleIndikatorPeer::SATUAN);

		$criteria->addSelectColumn(Revisi4SubtitleIndikatorPeer::LAST_UPDATE_USER);

		$criteria->addSelectColumn(Revisi4SubtitleIndikatorPeer::LAST_UPDATE_TIME);

		$criteria->addSelectColumn(Revisi4SubtitleIndikatorPeer::LAST_UPDATE_IP);

		$criteria->addSelectColumn(Revisi4SubtitleIndikatorPeer::TAHAP);

		$criteria->addSelectColumn(Revisi4SubtitleIndikatorPeer::SUB_ID);

		$criteria->addSelectColumn(Revisi4SubtitleIndikatorPeer::TAHUN);

		$criteria->addSelectColumn(Revisi4SubtitleIndikatorPeer::LOCK_SUBTITLE);

		$criteria->addSelectColumn(Revisi4SubtitleIndikatorPeer::PRIORITAS);

		$criteria->addSelectColumn(Revisi4SubtitleIndikatorPeer::CATATAN);

		$criteria->addSelectColumn(Revisi4SubtitleIndikatorPeer::LAKILAKI);

		$criteria->addSelectColumn(Revisi4SubtitleIndikatorPeer::PEREMPUAN);

		$criteria->addSelectColumn(Revisi4SubtitleIndikatorPeer::DEWASA);

		$criteria->addSelectColumn(Revisi4SubtitleIndikatorPeer::ANAK);

		$criteria->addSelectColumn(Revisi4SubtitleIndikatorPeer::LANSIA);

		$criteria->addSelectColumn(Revisi4SubtitleIndikatorPeer::INKLUSI);

		$criteria->addSelectColumn(Revisi4SubtitleIndikatorPeer::GENDER);

	}

	const COUNT = 'COUNT(ebudget.revisi4_subtitle_indikator.SUB_ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.revisi4_subtitle_indikator.SUB_ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(Revisi4SubtitleIndikatorPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(Revisi4SubtitleIndikatorPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = Revisi4SubtitleIndikatorPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = Revisi4SubtitleIndikatorPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return Revisi4SubtitleIndikatorPeer::populateObjects(Revisi4SubtitleIndikatorPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			Revisi4SubtitleIndikatorPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = Revisi4SubtitleIndikatorPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return Revisi4SubtitleIndikatorPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}

		$criteria->remove(Revisi4SubtitleIndikatorPeer::SUB_ID); 

				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(Revisi4SubtitleIndikatorPeer::SUB_ID);
			$selectCriteria->add(Revisi4SubtitleIndikatorPeer::SUB_ID, $criteria->remove(Revisi4SubtitleIndikatorPeer::SUB_ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(Revisi4SubtitleIndikatorPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(Revisi4SubtitleIndikatorPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof Revisi4SubtitleIndikator) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(Revisi4SubtitleIndikatorPeer::SUB_ID, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(Revisi4SubtitleIndikator $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(Revisi4SubtitleIndikatorPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(Revisi4SubtitleIndikatorPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(Revisi4SubtitleIndikatorPeer::DATABASE_NAME, Revisi4SubtitleIndikatorPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = Revisi4SubtitleIndikatorPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(Revisi4SubtitleIndikatorPeer::DATABASE_NAME);

		$criteria->add(Revisi4SubtitleIndikatorPeer::SUB_ID, $pk);


		$v = Revisi4SubtitleIndikatorPeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(Revisi4SubtitleIndikatorPeer::SUB_ID, $pks, Criteria::IN);
			$objs = Revisi4SubtitleIndikatorPeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BaseRevisi4SubtitleIndikatorPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/Revisi4SubtitleIndikatorMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.Revisi4SubtitleIndikatorMapBuilder');
}
