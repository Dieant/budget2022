<?php


abstract class BaseKegiatanNilai extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $unit_id;


	
	protected $nilai_1;


	
	protected $nilai_2;


	
	protected $nilai_3;


	
	protected $total;


	
	protected $keterangan;


	
	protected $kegiatan_code;


	
	protected $prioritas;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getUnitId()
	{

		return $this->unit_id;
	}

	
	public function getNilai1()
	{

		return $this->nilai_1;
	}

	
	public function getNilai2()
	{

		return $this->nilai_2;
	}

	
	public function getNilai3()
	{

		return $this->nilai_3;
	}

	
	public function getTotal()
	{

		return $this->total;
	}

	
	public function getKeterangan()
	{

		return $this->keterangan;
	}

	
	public function getKegiatanCode()
	{

		return $this->kegiatan_code;
	}

	
	public function getPrioritas()
	{

		return $this->prioritas;
	}

	
	public function setUnitId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->unit_id !== $v) {
			$this->unit_id = $v;
			$this->modifiedColumns[] = KegiatanNilaiPeer::UNIT_ID;
		}

	} 
	
	public function setNilai1($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->nilai_1 !== $v) {
			$this->nilai_1 = $v;
			$this->modifiedColumns[] = KegiatanNilaiPeer::NILAI_1;
		}

	} 
	
	public function setNilai2($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->nilai_2 !== $v) {
			$this->nilai_2 = $v;
			$this->modifiedColumns[] = KegiatanNilaiPeer::NILAI_2;
		}

	} 
	
	public function setNilai3($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->nilai_3 !== $v) {
			$this->nilai_3 = $v;
			$this->modifiedColumns[] = KegiatanNilaiPeer::NILAI_3;
		}

	} 
	
	public function setTotal($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->total !== $v) {
			$this->total = $v;
			$this->modifiedColumns[] = KegiatanNilaiPeer::TOTAL;
		}

	} 
	
	public function setKeterangan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->keterangan !== $v) {
			$this->keterangan = $v;
			$this->modifiedColumns[] = KegiatanNilaiPeer::KETERANGAN;
		}

	} 
	
	public function setKegiatanCode($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kegiatan_code !== $v) {
			$this->kegiatan_code = $v;
			$this->modifiedColumns[] = KegiatanNilaiPeer::KEGIATAN_CODE;
		}

	} 
	
	public function setPrioritas($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->prioritas !== $v) {
			$this->prioritas = $v;
			$this->modifiedColumns[] = KegiatanNilaiPeer::PRIORITAS;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->unit_id = $rs->getString($startcol + 0);

			$this->nilai_1 = $rs->getInt($startcol + 1);

			$this->nilai_2 = $rs->getInt($startcol + 2);

			$this->nilai_3 = $rs->getInt($startcol + 3);

			$this->total = $rs->getInt($startcol + 4);

			$this->keterangan = $rs->getString($startcol + 5);

			$this->kegiatan_code = $rs->getString($startcol + 6);

			$this->prioritas = $rs->getString($startcol + 7);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 8; 
		} catch (Exception $e) {
			throw new PropelException("Error populating KegiatanNilai object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(KegiatanNilaiPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			KegiatanNilaiPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(KegiatanNilaiPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = KegiatanNilaiPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += KegiatanNilaiPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = KegiatanNilaiPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = KegiatanNilaiPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getUnitId();
				break;
			case 1:
				return $this->getNilai1();
				break;
			case 2:
				return $this->getNilai2();
				break;
			case 3:
				return $this->getNilai3();
				break;
			case 4:
				return $this->getTotal();
				break;
			case 5:
				return $this->getKeterangan();
				break;
			case 6:
				return $this->getKegiatanCode();
				break;
			case 7:
				return $this->getPrioritas();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = KegiatanNilaiPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getUnitId(),
			$keys[1] => $this->getNilai1(),
			$keys[2] => $this->getNilai2(),
			$keys[3] => $this->getNilai3(),
			$keys[4] => $this->getTotal(),
			$keys[5] => $this->getKeterangan(),
			$keys[6] => $this->getKegiatanCode(),
			$keys[7] => $this->getPrioritas(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = KegiatanNilaiPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setUnitId($value);
				break;
			case 1:
				$this->setNilai1($value);
				break;
			case 2:
				$this->setNilai2($value);
				break;
			case 3:
				$this->setNilai3($value);
				break;
			case 4:
				$this->setTotal($value);
				break;
			case 5:
				$this->setKeterangan($value);
				break;
			case 6:
				$this->setKegiatanCode($value);
				break;
			case 7:
				$this->setPrioritas($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = KegiatanNilaiPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setUnitId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setNilai1($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setNilai2($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setNilai3($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setTotal($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setKeterangan($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setKegiatanCode($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setPrioritas($arr[$keys[7]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(KegiatanNilaiPeer::DATABASE_NAME);

		if ($this->isColumnModified(KegiatanNilaiPeer::UNIT_ID)) $criteria->add(KegiatanNilaiPeer::UNIT_ID, $this->unit_id);
		if ($this->isColumnModified(KegiatanNilaiPeer::NILAI_1)) $criteria->add(KegiatanNilaiPeer::NILAI_1, $this->nilai_1);
		if ($this->isColumnModified(KegiatanNilaiPeer::NILAI_2)) $criteria->add(KegiatanNilaiPeer::NILAI_2, $this->nilai_2);
		if ($this->isColumnModified(KegiatanNilaiPeer::NILAI_3)) $criteria->add(KegiatanNilaiPeer::NILAI_3, $this->nilai_3);
		if ($this->isColumnModified(KegiatanNilaiPeer::TOTAL)) $criteria->add(KegiatanNilaiPeer::TOTAL, $this->total);
		if ($this->isColumnModified(KegiatanNilaiPeer::KETERANGAN)) $criteria->add(KegiatanNilaiPeer::KETERANGAN, $this->keterangan);
		if ($this->isColumnModified(KegiatanNilaiPeer::KEGIATAN_CODE)) $criteria->add(KegiatanNilaiPeer::KEGIATAN_CODE, $this->kegiatan_code);
		if ($this->isColumnModified(KegiatanNilaiPeer::PRIORITAS)) $criteria->add(KegiatanNilaiPeer::PRIORITAS, $this->prioritas);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(KegiatanNilaiPeer::DATABASE_NAME);

		$criteria->add(KegiatanNilaiPeer::UNIT_ID, $this->unit_id);
		$criteria->add(KegiatanNilaiPeer::KEGIATAN_CODE, $this->kegiatan_code);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		$pks = array();

		$pks[0] = $this->getUnitId();

		$pks[1] = $this->getKegiatanCode();

		return $pks;
	}

	
	public function setPrimaryKey($keys)
	{

		$this->setUnitId($keys[0]);

		$this->setKegiatanCode($keys[1]);

	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setNilai1($this->nilai_1);

		$copyObj->setNilai2($this->nilai_2);

		$copyObj->setNilai3($this->nilai_3);

		$copyObj->setTotal($this->total);

		$copyObj->setKeterangan($this->keterangan);

		$copyObj->setPrioritas($this->prioritas);


		$copyObj->setNew(true);

		$copyObj->setUnitId(NULL); 
		$copyObj->setKegiatanCode(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new KegiatanNilaiPeer();
		}
		return self::$peer;
	}

} 