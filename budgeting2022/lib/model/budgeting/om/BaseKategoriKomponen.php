<?php


abstract class BaseKategoriKomponen extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $kategori_komponen_id;


	
	protected $kategori_komponen_name;


	
	protected $komponen_tipe;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getKategoriKomponenId()
	{

		return $this->kategori_komponen_id;
	}

	
	public function getKategoriKomponenName()
	{

		return $this->kategori_komponen_name;
	}

	
	public function getKomponenTipe()
	{

		return $this->komponen_tipe;
	}

	
	public function setKategoriKomponenId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kategori_komponen_id !== $v) {
			$this->kategori_komponen_id = $v;
			$this->modifiedColumns[] = KategoriKomponenPeer::KATEGORI_KOMPONEN_ID;
		}

	} 
	
	public function setKategoriKomponenName($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kategori_komponen_name !== $v) {
			$this->kategori_komponen_name = $v;
			$this->modifiedColumns[] = KategoriKomponenPeer::KATEGORI_KOMPONEN_NAME;
		}

	} 
	
	public function setKomponenTipe($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->komponen_tipe !== $v) {
			$this->komponen_tipe = $v;
			$this->modifiedColumns[] = KategoriKomponenPeer::KOMPONEN_TIPE;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->kategori_komponen_id = $rs->getString($startcol + 0);

			$this->kategori_komponen_name = $rs->getString($startcol + 1);

			$this->komponen_tipe = $rs->getString($startcol + 2);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 3; 
		} catch (Exception $e) {
			throw new PropelException("Error populating KategoriKomponen object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(KategoriKomponenPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			KategoriKomponenPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(KategoriKomponenPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = KategoriKomponenPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += KategoriKomponenPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = KategoriKomponenPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = KategoriKomponenPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getKategoriKomponenId();
				break;
			case 1:
				return $this->getKategoriKomponenName();
				break;
			case 2:
				return $this->getKomponenTipe();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = KategoriKomponenPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getKategoriKomponenId(),
			$keys[1] => $this->getKategoriKomponenName(),
			$keys[2] => $this->getKomponenTipe(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = KategoriKomponenPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setKategoriKomponenId($value);
				break;
			case 1:
				$this->setKategoriKomponenName($value);
				break;
			case 2:
				$this->setKomponenTipe($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = KategoriKomponenPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setKategoriKomponenId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setKategoriKomponenName($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setKomponenTipe($arr[$keys[2]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(KategoriKomponenPeer::DATABASE_NAME);

		if ($this->isColumnModified(KategoriKomponenPeer::KATEGORI_KOMPONEN_ID)) $criteria->add(KategoriKomponenPeer::KATEGORI_KOMPONEN_ID, $this->kategori_komponen_id);
		if ($this->isColumnModified(KategoriKomponenPeer::KATEGORI_KOMPONEN_NAME)) $criteria->add(KategoriKomponenPeer::KATEGORI_KOMPONEN_NAME, $this->kategori_komponen_name);
		if ($this->isColumnModified(KategoriKomponenPeer::KOMPONEN_TIPE)) $criteria->add(KategoriKomponenPeer::KOMPONEN_TIPE, $this->komponen_tipe);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(KategoriKomponenPeer::DATABASE_NAME);

		$criteria->add(KategoriKomponenPeer::KATEGORI_KOMPONEN_ID, $this->kategori_komponen_id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getKategoriKomponenId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setKategoriKomponenId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setKategoriKomponenName($this->kategori_komponen_name);

		$copyObj->setKomponenTipe($this->komponen_tipe);


		$copyObj->setNew(true);

		$copyObj->setKategoriKomponenId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new KategoriKomponenPeer();
		}
		return self::$peer;
	}

} 