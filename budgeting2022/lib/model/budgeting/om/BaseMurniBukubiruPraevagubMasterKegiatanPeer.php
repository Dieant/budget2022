<?php


abstract class BaseMurniBukubiruPraevagubMasterKegiatanPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.murni_bukubiru_praevagub_master_kegiatan';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.MurniBukubiruPraevagubMasterKegiatan';

	
	const NUM_COLUMNS = 85;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const UNIT_ID = 'ebudget.murni_bukubiru_praevagub_master_kegiatan.UNIT_ID';

	
	const KODE_KEGIATAN = 'ebudget.murni_bukubiru_praevagub_master_kegiatan.KODE_KEGIATAN';

	
	const KODE_BIDANG = 'ebudget.murni_bukubiru_praevagub_master_kegiatan.KODE_BIDANG';

	
	const KODE_URUSAN_WAJIB = 'ebudget.murni_bukubiru_praevagub_master_kegiatan.KODE_URUSAN_WAJIB';

	
	const KODE_PROGRAM = 'ebudget.murni_bukubiru_praevagub_master_kegiatan.KODE_PROGRAM';

	
	const KODE_SASARAN = 'ebudget.murni_bukubiru_praevagub_master_kegiatan.KODE_SASARAN';

	
	const KODE_INDIKATOR = 'ebudget.murni_bukubiru_praevagub_master_kegiatan.KODE_INDIKATOR';

	
	const ALOKASI_DANA = 'ebudget.murni_bukubiru_praevagub_master_kegiatan.ALOKASI_DANA';

	
	const NAMA_KEGIATAN = 'ebudget.murni_bukubiru_praevagub_master_kegiatan.NAMA_KEGIATAN';

	
	const MASUKAN = 'ebudget.murni_bukubiru_praevagub_master_kegiatan.MASUKAN';

	
	const OUTPUT = 'ebudget.murni_bukubiru_praevagub_master_kegiatan.OUTPUT';

	
	const OUTCOME = 'ebudget.murni_bukubiru_praevagub_master_kegiatan.OUTCOME';

	
	const BENEFIT = 'ebudget.murni_bukubiru_praevagub_master_kegiatan.BENEFIT';

	
	const IMPACT = 'ebudget.murni_bukubiru_praevagub_master_kegiatan.IMPACT';

	
	const TIPE = 'ebudget.murni_bukubiru_praevagub_master_kegiatan.TIPE';

	
	const KEGIATAN_ACTIVE = 'ebudget.murni_bukubiru_praevagub_master_kegiatan.KEGIATAN_ACTIVE';

	
	const TO_KEGIATAN_CODE = 'ebudget.murni_bukubiru_praevagub_master_kegiatan.TO_KEGIATAN_CODE';

	
	const CATATAN = 'ebudget.murni_bukubiru_praevagub_master_kegiatan.CATATAN';

	
	const TARGET_OUTCOME = 'ebudget.murni_bukubiru_praevagub_master_kegiatan.TARGET_OUTCOME';

	
	const LOKASI = 'ebudget.murni_bukubiru_praevagub_master_kegiatan.LOKASI';

	
	const JUMLAH_PREV = 'ebudget.murni_bukubiru_praevagub_master_kegiatan.JUMLAH_PREV';

	
	const JUMLAH_NOW = 'ebudget.murni_bukubiru_praevagub_master_kegiatan.JUMLAH_NOW';

	
	const JUMLAH_NEXT = 'ebudget.murni_bukubiru_praevagub_master_kegiatan.JUMLAH_NEXT';

	
	const KODE_PROGRAM2 = 'ebudget.murni_bukubiru_praevagub_master_kegiatan.KODE_PROGRAM2';

	
	const KODE_URUSAN = 'ebudget.murni_bukubiru_praevagub_master_kegiatan.KODE_URUSAN';

	
	const LAST_UPDATE_USER = 'ebudget.murni_bukubiru_praevagub_master_kegiatan.LAST_UPDATE_USER';

	
	const LAST_UPDATE_TIME = 'ebudget.murni_bukubiru_praevagub_master_kegiatan.LAST_UPDATE_TIME';

	
	const LAST_UPDATE_IP = 'ebudget.murni_bukubiru_praevagub_master_kegiatan.LAST_UPDATE_IP';

	
	const TAHAP = 'ebudget.murni_bukubiru_praevagub_master_kegiatan.TAHAP';

	
	const KODE_MISI = 'ebudget.murni_bukubiru_praevagub_master_kegiatan.KODE_MISI';

	
	const KODE_TUJUAN = 'ebudget.murni_bukubiru_praevagub_master_kegiatan.KODE_TUJUAN';

	
	const RANKING = 'ebudget.murni_bukubiru_praevagub_master_kegiatan.RANKING';

	
	const NOMOR13 = 'ebudget.murni_bukubiru_praevagub_master_kegiatan.NOMOR13';

	
	const PPA_NAMA = 'ebudget.murni_bukubiru_praevagub_master_kegiatan.PPA_NAMA';

	
	const PPA_PANGKAT = 'ebudget.murni_bukubiru_praevagub_master_kegiatan.PPA_PANGKAT';

	
	const PPA_NIP = 'ebudget.murni_bukubiru_praevagub_master_kegiatan.PPA_NIP';

	
	const LANJUTAN = 'ebudget.murni_bukubiru_praevagub_master_kegiatan.LANJUTAN';

	
	const USER_ID = 'ebudget.murni_bukubiru_praevagub_master_kegiatan.USER_ID';

	
	const ID = 'ebudget.murni_bukubiru_praevagub_master_kegiatan.ID';

	
	const TAHUN = 'ebudget.murni_bukubiru_praevagub_master_kegiatan.TAHUN';

	
	const TAMBAHAN_PAGU = 'ebudget.murni_bukubiru_praevagub_master_kegiatan.TAMBAHAN_PAGU';

	
	const GENDER = 'ebudget.murni_bukubiru_praevagub_master_kegiatan.GENDER';

	
	const KODE_KEG_KEUANGAN = 'ebudget.murni_bukubiru_praevagub_master_kegiatan.KODE_KEG_KEUANGAN';

	
	const USER_ID_LAMA = 'ebudget.murni_bukubiru_praevagub_master_kegiatan.USER_ID_LAMA';

	
	const INDIKATOR = 'ebudget.murni_bukubiru_praevagub_master_kegiatan.INDIKATOR';

	
	const IS_DAK = 'ebudget.murni_bukubiru_praevagub_master_kegiatan.IS_DAK';

	
	const KODE_KEGIATAN_ASAL = 'ebudget.murni_bukubiru_praevagub_master_kegiatan.KODE_KEGIATAN_ASAL';

	
	const KODE_KEG_KEUANGAN_ASAL = 'ebudget.murni_bukubiru_praevagub_master_kegiatan.KODE_KEG_KEUANGAN_ASAL';

	
	const TH_KE_MULTIYEARS = 'ebudget.murni_bukubiru_praevagub_master_kegiatan.TH_KE_MULTIYEARS';

	
	const KELOMPOK_SASARAN = 'ebudget.murni_bukubiru_praevagub_master_kegiatan.KELOMPOK_SASARAN';

	
	const PAGU_BAPPEKO = 'ebudget.murni_bukubiru_praevagub_master_kegiatan.PAGU_BAPPEKO';

	
	const KODE_DPA = 'ebudget.murni_bukubiru_praevagub_master_kegiatan.KODE_DPA';

	
	const USER_ID_PPTK = 'ebudget.murni_bukubiru_praevagub_master_kegiatan.USER_ID_PPTK';

	
	const USER_ID_KPA = 'ebudget.murni_bukubiru_praevagub_master_kegiatan.USER_ID_KPA';

	
	const CATATAN_PEMBAHASAN = 'ebudget.murni_bukubiru_praevagub_master_kegiatan.CATATAN_PEMBAHASAN';

	
	const STATUS_LEVEL = 'ebudget.murni_bukubiru_praevagub_master_kegiatan.STATUS_LEVEL';

	
	const IS_TAPD_SETUJU = 'ebudget.murni_bukubiru_praevagub_master_kegiatan.IS_TAPD_SETUJU';

	
	const IS_BAPPEKO_SETUJU = 'ebudget.murni_bukubiru_praevagub_master_kegiatan.IS_BAPPEKO_SETUJU';

	
	const IS_PENYELIA_SETUJU = 'ebudget.murni_bukubiru_praevagub_master_kegiatan.IS_PENYELIA_SETUJU';

	
	const IS_PERNAH_RKA = 'ebudget.murni_bukubiru_praevagub_master_kegiatan.IS_PERNAH_RKA';

	
	const KODE_KEGIATAN_BARU = 'ebudget.murni_bukubiru_praevagub_master_kegiatan.KODE_KEGIATAN_BARU';

	
	const CATATAN_BPKPD = 'ebudget.murni_bukubiru_praevagub_master_kegiatan.CATATAN_BPKPD';

	
	const UBAH_F1_DINAS = 'ebudget.murni_bukubiru_praevagub_master_kegiatan.UBAH_F1_DINAS';

	
	const UBAH_F1_PENELITI = 'ebudget.murni_bukubiru_praevagub_master_kegiatan.UBAH_F1_PENELITI';

	
	const SISA_LELANG_DINAS = 'ebudget.murni_bukubiru_praevagub_master_kegiatan.SISA_LELANG_DINAS';

	
	const SISA_LELANG_PENELITI = 'ebudget.murni_bukubiru_praevagub_master_kegiatan.SISA_LELANG_PENELITI';

	
	const CATATAN_UBAH_F1_DINAS = 'ebudget.murni_bukubiru_praevagub_master_kegiatan.CATATAN_UBAH_F1_DINAS';

	
	const CATATAN_SISA_LELANG_PENELITI = 'ebudget.murni_bukubiru_praevagub_master_kegiatan.CATATAN_SISA_LELANG_PENELITI';

	
	const PPTK_APPROVAL = 'ebudget.murni_bukubiru_praevagub_master_kegiatan.PPTK_APPROVAL';

	
	const KPA_APPROVAL = 'ebudget.murni_bukubiru_praevagub_master_kegiatan.KPA_APPROVAL';

	
	const CATATAN_BAGIAN_HUKUM = 'ebudget.murni_bukubiru_praevagub_master_kegiatan.CATATAN_BAGIAN_HUKUM';

	
	const CATATAN_INSPEKTORAT = 'ebudget.murni_bukubiru_praevagub_master_kegiatan.CATATAN_INSPEKTORAT';

	
	const CATATAN_BADAN_KEPEGAWAIAN = 'ebudget.murni_bukubiru_praevagub_master_kegiatan.CATATAN_BADAN_KEPEGAWAIAN';

	
	const CATATAN_LPPA = 'ebudget.murni_bukubiru_praevagub_master_kegiatan.CATATAN_LPPA';

	
	const IS_BAGIAN_HUKUM_SETUJU = 'ebudget.murni_bukubiru_praevagub_master_kegiatan.IS_BAGIAN_HUKUM_SETUJU';

	
	const IS_INSPEKTORAT_SETUJU = 'ebudget.murni_bukubiru_praevagub_master_kegiatan.IS_INSPEKTORAT_SETUJU';

	
	const IS_BADAN_KEPEGAWAIAN_SETUJU = 'ebudget.murni_bukubiru_praevagub_master_kegiatan.IS_BADAN_KEPEGAWAIAN_SETUJU';

	
	const IS_LPPA_SETUJU = 'ebudget.murni_bukubiru_praevagub_master_kegiatan.IS_LPPA_SETUJU';

	
	const VERIFIKASI_BPKPD = 'ebudget.murni_bukubiru_praevagub_master_kegiatan.VERIFIKASI_BPKPD';

	
	const VERIFIKASI_BAPPEKO = 'ebudget.murni_bukubiru_praevagub_master_kegiatan.VERIFIKASI_BAPPEKO';

	
	const VERIFIKASI_PENYELIA = 'ebudget.murni_bukubiru_praevagub_master_kegiatan.VERIFIKASI_PENYELIA';

	
	const VERIFIKASI_BAGIAN_HUKUM = 'ebudget.murni_bukubiru_praevagub_master_kegiatan.VERIFIKASI_BAGIAN_HUKUM';

	
	const VERIFIKASI_INSPEKTORAT = 'ebudget.murni_bukubiru_praevagub_master_kegiatan.VERIFIKASI_INSPEKTORAT';

	
	const VERIFIKASI_BADAN_KEPEGAWAIAN = 'ebudget.murni_bukubiru_praevagub_master_kegiatan.VERIFIKASI_BADAN_KEPEGAWAIAN';

	
	const VERIFIKASI_LPPA = 'ebudget.murni_bukubiru_praevagub_master_kegiatan.VERIFIKASI_LPPA';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('UnitId', 'KodeKegiatan', 'KodeBidang', 'KodeUrusanWajib', 'KodeProgram', 'KodeSasaran', 'KodeIndikator', 'AlokasiDana', 'NamaKegiatan', 'Masukan', 'Output', 'Outcome', 'Benefit', 'Impact', 'Tipe', 'KegiatanActive', 'ToKegiatanCode', 'Catatan', 'TargetOutcome', 'Lokasi', 'JumlahPrev', 'JumlahNow', 'JumlahNext', 'KodeProgram2', 'KodeUrusan', 'LastUpdateUser', 'LastUpdateTime', 'LastUpdateIp', 'Tahap', 'KodeMisi', 'KodeTujuan', 'Ranking', 'Nomor13', 'PpaNama', 'PpaPangkat', 'PpaNip', 'Lanjutan', 'UserId', 'Id', 'Tahun', 'TambahanPagu', 'Gender', 'KodeKegKeuangan', 'UserIdLama', 'Indikator', 'IsDak', 'KodeKegiatanAsal', 'KodeKegKeuanganAsal', 'ThKeMultiyears', 'KelompokSasaran', 'PaguBappeko', 'KodeDpa', 'UserIdPptk', 'UserIdKpa', 'CatatanPembahasan', 'StatusLevel', 'IsTapdSetuju', 'IsBappekoSetuju', 'IsPenyeliaSetuju', 'IsPernahRka', 'KodeKegiatanBaru', 'CatatanBpkpd', 'UbahF1Dinas', 'UbahF1Peneliti', 'SisaLelangDinas', 'SisaLelangPeneliti', 'CatatanUbahF1Dinas', 'CatatanSisaLelangPeneliti', 'PptkApproval', 'KpaApproval', 'CatatanBagianHukum', 'CatatanInspektorat', 'CatatanBadanKepegawaian', 'CatatanLppa', 'IsBagianHukumSetuju', 'IsInspektoratSetuju', 'IsBadanKepegawaianSetuju', 'IsLppaSetuju', 'VerifikasiBpkpd', 'VerifikasiBappeko', 'VerifikasiPenyelia', 'VerifikasiBagianHukum', 'VerifikasiInspektorat', 'VerifikasiBadanKepegawaian', 'VerifikasiLppa', ),
		BasePeer::TYPE_COLNAME => array (MurniBukubiruPraevagubMasterKegiatanPeer::UNIT_ID, MurniBukubiruPraevagubMasterKegiatanPeer::KODE_KEGIATAN, MurniBukubiruPraevagubMasterKegiatanPeer::KODE_BIDANG, MurniBukubiruPraevagubMasterKegiatanPeer::KODE_URUSAN_WAJIB, MurniBukubiruPraevagubMasterKegiatanPeer::KODE_PROGRAM, MurniBukubiruPraevagubMasterKegiatanPeer::KODE_SASARAN, MurniBukubiruPraevagubMasterKegiatanPeer::KODE_INDIKATOR, MurniBukubiruPraevagubMasterKegiatanPeer::ALOKASI_DANA, MurniBukubiruPraevagubMasterKegiatanPeer::NAMA_KEGIATAN, MurniBukubiruPraevagubMasterKegiatanPeer::MASUKAN, MurniBukubiruPraevagubMasterKegiatanPeer::OUTPUT, MurniBukubiruPraevagubMasterKegiatanPeer::OUTCOME, MurniBukubiruPraevagubMasterKegiatanPeer::BENEFIT, MurniBukubiruPraevagubMasterKegiatanPeer::IMPACT, MurniBukubiruPraevagubMasterKegiatanPeer::TIPE, MurniBukubiruPraevagubMasterKegiatanPeer::KEGIATAN_ACTIVE, MurniBukubiruPraevagubMasterKegiatanPeer::TO_KEGIATAN_CODE, MurniBukubiruPraevagubMasterKegiatanPeer::CATATAN, MurniBukubiruPraevagubMasterKegiatanPeer::TARGET_OUTCOME, MurniBukubiruPraevagubMasterKegiatanPeer::LOKASI, MurniBukubiruPraevagubMasterKegiatanPeer::JUMLAH_PREV, MurniBukubiruPraevagubMasterKegiatanPeer::JUMLAH_NOW, MurniBukubiruPraevagubMasterKegiatanPeer::JUMLAH_NEXT, MurniBukubiruPraevagubMasterKegiatanPeer::KODE_PROGRAM2, MurniBukubiruPraevagubMasterKegiatanPeer::KODE_URUSAN, MurniBukubiruPraevagubMasterKegiatanPeer::LAST_UPDATE_USER, MurniBukubiruPraevagubMasterKegiatanPeer::LAST_UPDATE_TIME, MurniBukubiruPraevagubMasterKegiatanPeer::LAST_UPDATE_IP, MurniBukubiruPraevagubMasterKegiatanPeer::TAHAP, MurniBukubiruPraevagubMasterKegiatanPeer::KODE_MISI, MurniBukubiruPraevagubMasterKegiatanPeer::KODE_TUJUAN, MurniBukubiruPraevagubMasterKegiatanPeer::RANKING, MurniBukubiruPraevagubMasterKegiatanPeer::NOMOR13, MurniBukubiruPraevagubMasterKegiatanPeer::PPA_NAMA, MurniBukubiruPraevagubMasterKegiatanPeer::PPA_PANGKAT, MurniBukubiruPraevagubMasterKegiatanPeer::PPA_NIP, MurniBukubiruPraevagubMasterKegiatanPeer::LANJUTAN, MurniBukubiruPraevagubMasterKegiatanPeer::USER_ID, MurniBukubiruPraevagubMasterKegiatanPeer::ID, MurniBukubiruPraevagubMasterKegiatanPeer::TAHUN, MurniBukubiruPraevagubMasterKegiatanPeer::TAMBAHAN_PAGU, MurniBukubiruPraevagubMasterKegiatanPeer::GENDER, MurniBukubiruPraevagubMasterKegiatanPeer::KODE_KEG_KEUANGAN, MurniBukubiruPraevagubMasterKegiatanPeer::USER_ID_LAMA, MurniBukubiruPraevagubMasterKegiatanPeer::INDIKATOR, MurniBukubiruPraevagubMasterKegiatanPeer::IS_DAK, MurniBukubiruPraevagubMasterKegiatanPeer::KODE_KEGIATAN_ASAL, MurniBukubiruPraevagubMasterKegiatanPeer::KODE_KEG_KEUANGAN_ASAL, MurniBukubiruPraevagubMasterKegiatanPeer::TH_KE_MULTIYEARS, MurniBukubiruPraevagubMasterKegiatanPeer::KELOMPOK_SASARAN, MurniBukubiruPraevagubMasterKegiatanPeer::PAGU_BAPPEKO, MurniBukubiruPraevagubMasterKegiatanPeer::KODE_DPA, MurniBukubiruPraevagubMasterKegiatanPeer::USER_ID_PPTK, MurniBukubiruPraevagubMasterKegiatanPeer::USER_ID_KPA, MurniBukubiruPraevagubMasterKegiatanPeer::CATATAN_PEMBAHASAN, MurniBukubiruPraevagubMasterKegiatanPeer::STATUS_LEVEL, MurniBukubiruPraevagubMasterKegiatanPeer::IS_TAPD_SETUJU, MurniBukubiruPraevagubMasterKegiatanPeer::IS_BAPPEKO_SETUJU, MurniBukubiruPraevagubMasterKegiatanPeer::IS_PENYELIA_SETUJU, MurniBukubiruPraevagubMasterKegiatanPeer::IS_PERNAH_RKA, MurniBukubiruPraevagubMasterKegiatanPeer::KODE_KEGIATAN_BARU, MurniBukubiruPraevagubMasterKegiatanPeer::CATATAN_BPKPD, MurniBukubiruPraevagubMasterKegiatanPeer::UBAH_F1_DINAS, MurniBukubiruPraevagubMasterKegiatanPeer::UBAH_F1_PENELITI, MurniBukubiruPraevagubMasterKegiatanPeer::SISA_LELANG_DINAS, MurniBukubiruPraevagubMasterKegiatanPeer::SISA_LELANG_PENELITI, MurniBukubiruPraevagubMasterKegiatanPeer::CATATAN_UBAH_F1_DINAS, MurniBukubiruPraevagubMasterKegiatanPeer::CATATAN_SISA_LELANG_PENELITI, MurniBukubiruPraevagubMasterKegiatanPeer::PPTK_APPROVAL, MurniBukubiruPraevagubMasterKegiatanPeer::KPA_APPROVAL, MurniBukubiruPraevagubMasterKegiatanPeer::CATATAN_BAGIAN_HUKUM, MurniBukubiruPraevagubMasterKegiatanPeer::CATATAN_INSPEKTORAT, MurniBukubiruPraevagubMasterKegiatanPeer::CATATAN_BADAN_KEPEGAWAIAN, MurniBukubiruPraevagubMasterKegiatanPeer::CATATAN_LPPA, MurniBukubiruPraevagubMasterKegiatanPeer::IS_BAGIAN_HUKUM_SETUJU, MurniBukubiruPraevagubMasterKegiatanPeer::IS_INSPEKTORAT_SETUJU, MurniBukubiruPraevagubMasterKegiatanPeer::IS_BADAN_KEPEGAWAIAN_SETUJU, MurniBukubiruPraevagubMasterKegiatanPeer::IS_LPPA_SETUJU, MurniBukubiruPraevagubMasterKegiatanPeer::VERIFIKASI_BPKPD, MurniBukubiruPraevagubMasterKegiatanPeer::VERIFIKASI_BAPPEKO, MurniBukubiruPraevagubMasterKegiatanPeer::VERIFIKASI_PENYELIA, MurniBukubiruPraevagubMasterKegiatanPeer::VERIFIKASI_BAGIAN_HUKUM, MurniBukubiruPraevagubMasterKegiatanPeer::VERIFIKASI_INSPEKTORAT, MurniBukubiruPraevagubMasterKegiatanPeer::VERIFIKASI_BADAN_KEPEGAWAIAN, MurniBukubiruPraevagubMasterKegiatanPeer::VERIFIKASI_LPPA, ),
		BasePeer::TYPE_FIELDNAME => array ('unit_id', 'kode_kegiatan', 'kode_bidang', 'kode_urusan_wajib', 'kode_program', 'kode_sasaran', 'kode_indikator', 'alokasi_dana', 'nama_kegiatan', 'masukan', 'output', 'outcome', 'benefit', 'impact', 'tipe', 'kegiatan_active', 'to_kegiatan_code', 'catatan', 'target_outcome', 'lokasi', 'jumlah_prev', 'jumlah_now', 'jumlah_next', 'kode_program2', 'kode_urusan', 'last_update_user', 'last_update_time', 'last_update_ip', 'tahap', 'kode_misi', 'kode_tujuan', 'ranking', 'nomor13', 'ppa_nama', 'ppa_pangkat', 'ppa_nip', 'lanjutan', 'user_id', 'id', 'tahun', 'tambahan_pagu', 'gender', 'kode_keg_keuangan', 'user_id_lama', 'indikator', 'is_dak', 'kode_kegiatan_asal', 'kode_keg_keuangan_asal', 'th_ke_multiyears', 'kelompok_sasaran', 'pagu_bappeko', 'kode_dpa', 'user_id_pptk', 'user_id_kpa', 'catatan_pembahasan', 'status_level', 'is_tapd_setuju', 'is_bappeko_setuju', 'is_penyelia_setuju', 'is_pernah_rka', 'kode_kegiatan_baru', 'catatan_bpkpd', 'ubah_f1_dinas', 'ubah_f1_peneliti', 'sisa_lelang_dinas', 'sisa_lelang_peneliti', 'catatan_ubah_f1_dinas', 'catatan_sisa_lelang_peneliti', 'pptk_approval', 'kpa_approval', 'catatan_bagian_hukum', 'catatan_inspektorat', 'catatan_badan_kepegawaian', 'catatan_lppa', 'is_bagian_hukum_setuju', 'is_inspektorat_setuju', 'is_badan_kepegawaian_setuju', 'is_lppa_setuju', 'verifikasi_bpkpd', 'verifikasi_bappeko', 'verifikasi_penyelia', 'verifikasi_bagian_hukum', 'verifikasi_inspektorat', 'verifikasi_badan_kepegawaian', 'verifikasi_lppa', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('UnitId' => 0, 'KodeKegiatan' => 1, 'KodeBidang' => 2, 'KodeUrusanWajib' => 3, 'KodeProgram' => 4, 'KodeSasaran' => 5, 'KodeIndikator' => 6, 'AlokasiDana' => 7, 'NamaKegiatan' => 8, 'Masukan' => 9, 'Output' => 10, 'Outcome' => 11, 'Benefit' => 12, 'Impact' => 13, 'Tipe' => 14, 'KegiatanActive' => 15, 'ToKegiatanCode' => 16, 'Catatan' => 17, 'TargetOutcome' => 18, 'Lokasi' => 19, 'JumlahPrev' => 20, 'JumlahNow' => 21, 'JumlahNext' => 22, 'KodeProgram2' => 23, 'KodeUrusan' => 24, 'LastUpdateUser' => 25, 'LastUpdateTime' => 26, 'LastUpdateIp' => 27, 'Tahap' => 28, 'KodeMisi' => 29, 'KodeTujuan' => 30, 'Ranking' => 31, 'Nomor13' => 32, 'PpaNama' => 33, 'PpaPangkat' => 34, 'PpaNip' => 35, 'Lanjutan' => 36, 'UserId' => 37, 'Id' => 38, 'Tahun' => 39, 'TambahanPagu' => 40, 'Gender' => 41, 'KodeKegKeuangan' => 42, 'UserIdLama' => 43, 'Indikator' => 44, 'IsDak' => 45, 'KodeKegiatanAsal' => 46, 'KodeKegKeuanganAsal' => 47, 'ThKeMultiyears' => 48, 'KelompokSasaran' => 49, 'PaguBappeko' => 50, 'KodeDpa' => 51, 'UserIdPptk' => 52, 'UserIdKpa' => 53, 'CatatanPembahasan' => 54, 'StatusLevel' => 55, 'IsTapdSetuju' => 56, 'IsBappekoSetuju' => 57, 'IsPenyeliaSetuju' => 58, 'IsPernahRka' => 59, 'KodeKegiatanBaru' => 60, 'CatatanBpkpd' => 61, 'UbahF1Dinas' => 62, 'UbahF1Peneliti' => 63, 'SisaLelangDinas' => 64, 'SisaLelangPeneliti' => 65, 'CatatanUbahF1Dinas' => 66, 'CatatanSisaLelangPeneliti' => 67, 'PptkApproval' => 68, 'KpaApproval' => 69, 'CatatanBagianHukum' => 70, 'CatatanInspektorat' => 71, 'CatatanBadanKepegawaian' => 72, 'CatatanLppa' => 73, 'IsBagianHukumSetuju' => 74, 'IsInspektoratSetuju' => 75, 'IsBadanKepegawaianSetuju' => 76, 'IsLppaSetuju' => 77, 'VerifikasiBpkpd' => 78, 'VerifikasiBappeko' => 79, 'VerifikasiPenyelia' => 80, 'VerifikasiBagianHukum' => 81, 'VerifikasiInspektorat' => 82, 'VerifikasiBadanKepegawaian' => 83, 'VerifikasiLppa' => 84, ),
		BasePeer::TYPE_COLNAME => array (MurniBukubiruPraevagubMasterKegiatanPeer::UNIT_ID => 0, MurniBukubiruPraevagubMasterKegiatanPeer::KODE_KEGIATAN => 1, MurniBukubiruPraevagubMasterKegiatanPeer::KODE_BIDANG => 2, MurniBukubiruPraevagubMasterKegiatanPeer::KODE_URUSAN_WAJIB => 3, MurniBukubiruPraevagubMasterKegiatanPeer::KODE_PROGRAM => 4, MurniBukubiruPraevagubMasterKegiatanPeer::KODE_SASARAN => 5, MurniBukubiruPraevagubMasterKegiatanPeer::KODE_INDIKATOR => 6, MurniBukubiruPraevagubMasterKegiatanPeer::ALOKASI_DANA => 7, MurniBukubiruPraevagubMasterKegiatanPeer::NAMA_KEGIATAN => 8, MurniBukubiruPraevagubMasterKegiatanPeer::MASUKAN => 9, MurniBukubiruPraevagubMasterKegiatanPeer::OUTPUT => 10, MurniBukubiruPraevagubMasterKegiatanPeer::OUTCOME => 11, MurniBukubiruPraevagubMasterKegiatanPeer::BENEFIT => 12, MurniBukubiruPraevagubMasterKegiatanPeer::IMPACT => 13, MurniBukubiruPraevagubMasterKegiatanPeer::TIPE => 14, MurniBukubiruPraevagubMasterKegiatanPeer::KEGIATAN_ACTIVE => 15, MurniBukubiruPraevagubMasterKegiatanPeer::TO_KEGIATAN_CODE => 16, MurniBukubiruPraevagubMasterKegiatanPeer::CATATAN => 17, MurniBukubiruPraevagubMasterKegiatanPeer::TARGET_OUTCOME => 18, MurniBukubiruPraevagubMasterKegiatanPeer::LOKASI => 19, MurniBukubiruPraevagubMasterKegiatanPeer::JUMLAH_PREV => 20, MurniBukubiruPraevagubMasterKegiatanPeer::JUMLAH_NOW => 21, MurniBukubiruPraevagubMasterKegiatanPeer::JUMLAH_NEXT => 22, MurniBukubiruPraevagubMasterKegiatanPeer::KODE_PROGRAM2 => 23, MurniBukubiruPraevagubMasterKegiatanPeer::KODE_URUSAN => 24, MurniBukubiruPraevagubMasterKegiatanPeer::LAST_UPDATE_USER => 25, MurniBukubiruPraevagubMasterKegiatanPeer::LAST_UPDATE_TIME => 26, MurniBukubiruPraevagubMasterKegiatanPeer::LAST_UPDATE_IP => 27, MurniBukubiruPraevagubMasterKegiatanPeer::TAHAP => 28, MurniBukubiruPraevagubMasterKegiatanPeer::KODE_MISI => 29, MurniBukubiruPraevagubMasterKegiatanPeer::KODE_TUJUAN => 30, MurniBukubiruPraevagubMasterKegiatanPeer::RANKING => 31, MurniBukubiruPraevagubMasterKegiatanPeer::NOMOR13 => 32, MurniBukubiruPraevagubMasterKegiatanPeer::PPA_NAMA => 33, MurniBukubiruPraevagubMasterKegiatanPeer::PPA_PANGKAT => 34, MurniBukubiruPraevagubMasterKegiatanPeer::PPA_NIP => 35, MurniBukubiruPraevagubMasterKegiatanPeer::LANJUTAN => 36, MurniBukubiruPraevagubMasterKegiatanPeer::USER_ID => 37, MurniBukubiruPraevagubMasterKegiatanPeer::ID => 38, MurniBukubiruPraevagubMasterKegiatanPeer::TAHUN => 39, MurniBukubiruPraevagubMasterKegiatanPeer::TAMBAHAN_PAGU => 40, MurniBukubiruPraevagubMasterKegiatanPeer::GENDER => 41, MurniBukubiruPraevagubMasterKegiatanPeer::KODE_KEG_KEUANGAN => 42, MurniBukubiruPraevagubMasterKegiatanPeer::USER_ID_LAMA => 43, MurniBukubiruPraevagubMasterKegiatanPeer::INDIKATOR => 44, MurniBukubiruPraevagubMasterKegiatanPeer::IS_DAK => 45, MurniBukubiruPraevagubMasterKegiatanPeer::KODE_KEGIATAN_ASAL => 46, MurniBukubiruPraevagubMasterKegiatanPeer::KODE_KEG_KEUANGAN_ASAL => 47, MurniBukubiruPraevagubMasterKegiatanPeer::TH_KE_MULTIYEARS => 48, MurniBukubiruPraevagubMasterKegiatanPeer::KELOMPOK_SASARAN => 49, MurniBukubiruPraevagubMasterKegiatanPeer::PAGU_BAPPEKO => 50, MurniBukubiruPraevagubMasterKegiatanPeer::KODE_DPA => 51, MurniBukubiruPraevagubMasterKegiatanPeer::USER_ID_PPTK => 52, MurniBukubiruPraevagubMasterKegiatanPeer::USER_ID_KPA => 53, MurniBukubiruPraevagubMasterKegiatanPeer::CATATAN_PEMBAHASAN => 54, MurniBukubiruPraevagubMasterKegiatanPeer::STATUS_LEVEL => 55, MurniBukubiruPraevagubMasterKegiatanPeer::IS_TAPD_SETUJU => 56, MurniBukubiruPraevagubMasterKegiatanPeer::IS_BAPPEKO_SETUJU => 57, MurniBukubiruPraevagubMasterKegiatanPeer::IS_PENYELIA_SETUJU => 58, MurniBukubiruPraevagubMasterKegiatanPeer::IS_PERNAH_RKA => 59, MurniBukubiruPraevagubMasterKegiatanPeer::KODE_KEGIATAN_BARU => 60, MurniBukubiruPraevagubMasterKegiatanPeer::CATATAN_BPKPD => 61, MurniBukubiruPraevagubMasterKegiatanPeer::UBAH_F1_DINAS => 62, MurniBukubiruPraevagubMasterKegiatanPeer::UBAH_F1_PENELITI => 63, MurniBukubiruPraevagubMasterKegiatanPeer::SISA_LELANG_DINAS => 64, MurniBukubiruPraevagubMasterKegiatanPeer::SISA_LELANG_PENELITI => 65, MurniBukubiruPraevagubMasterKegiatanPeer::CATATAN_UBAH_F1_DINAS => 66, MurniBukubiruPraevagubMasterKegiatanPeer::CATATAN_SISA_LELANG_PENELITI => 67, MurniBukubiruPraevagubMasterKegiatanPeer::PPTK_APPROVAL => 68, MurniBukubiruPraevagubMasterKegiatanPeer::KPA_APPROVAL => 69, MurniBukubiruPraevagubMasterKegiatanPeer::CATATAN_BAGIAN_HUKUM => 70, MurniBukubiruPraevagubMasterKegiatanPeer::CATATAN_INSPEKTORAT => 71, MurniBukubiruPraevagubMasterKegiatanPeer::CATATAN_BADAN_KEPEGAWAIAN => 72, MurniBukubiruPraevagubMasterKegiatanPeer::CATATAN_LPPA => 73, MurniBukubiruPraevagubMasterKegiatanPeer::IS_BAGIAN_HUKUM_SETUJU => 74, MurniBukubiruPraevagubMasterKegiatanPeer::IS_INSPEKTORAT_SETUJU => 75, MurniBukubiruPraevagubMasterKegiatanPeer::IS_BADAN_KEPEGAWAIAN_SETUJU => 76, MurniBukubiruPraevagubMasterKegiatanPeer::IS_LPPA_SETUJU => 77, MurniBukubiruPraevagubMasterKegiatanPeer::VERIFIKASI_BPKPD => 78, MurniBukubiruPraevagubMasterKegiatanPeer::VERIFIKASI_BAPPEKO => 79, MurniBukubiruPraevagubMasterKegiatanPeer::VERIFIKASI_PENYELIA => 80, MurniBukubiruPraevagubMasterKegiatanPeer::VERIFIKASI_BAGIAN_HUKUM => 81, MurniBukubiruPraevagubMasterKegiatanPeer::VERIFIKASI_INSPEKTORAT => 82, MurniBukubiruPraevagubMasterKegiatanPeer::VERIFIKASI_BADAN_KEPEGAWAIAN => 83, MurniBukubiruPraevagubMasterKegiatanPeer::VERIFIKASI_LPPA => 84, ),
		BasePeer::TYPE_FIELDNAME => array ('unit_id' => 0, 'kode_kegiatan' => 1, 'kode_bidang' => 2, 'kode_urusan_wajib' => 3, 'kode_program' => 4, 'kode_sasaran' => 5, 'kode_indikator' => 6, 'alokasi_dana' => 7, 'nama_kegiatan' => 8, 'masukan' => 9, 'output' => 10, 'outcome' => 11, 'benefit' => 12, 'impact' => 13, 'tipe' => 14, 'kegiatan_active' => 15, 'to_kegiatan_code' => 16, 'catatan' => 17, 'target_outcome' => 18, 'lokasi' => 19, 'jumlah_prev' => 20, 'jumlah_now' => 21, 'jumlah_next' => 22, 'kode_program2' => 23, 'kode_urusan' => 24, 'last_update_user' => 25, 'last_update_time' => 26, 'last_update_ip' => 27, 'tahap' => 28, 'kode_misi' => 29, 'kode_tujuan' => 30, 'ranking' => 31, 'nomor13' => 32, 'ppa_nama' => 33, 'ppa_pangkat' => 34, 'ppa_nip' => 35, 'lanjutan' => 36, 'user_id' => 37, 'id' => 38, 'tahun' => 39, 'tambahan_pagu' => 40, 'gender' => 41, 'kode_keg_keuangan' => 42, 'user_id_lama' => 43, 'indikator' => 44, 'is_dak' => 45, 'kode_kegiatan_asal' => 46, 'kode_keg_keuangan_asal' => 47, 'th_ke_multiyears' => 48, 'kelompok_sasaran' => 49, 'pagu_bappeko' => 50, 'kode_dpa' => 51, 'user_id_pptk' => 52, 'user_id_kpa' => 53, 'catatan_pembahasan' => 54, 'status_level' => 55, 'is_tapd_setuju' => 56, 'is_bappeko_setuju' => 57, 'is_penyelia_setuju' => 58, 'is_pernah_rka' => 59, 'kode_kegiatan_baru' => 60, 'catatan_bpkpd' => 61, 'ubah_f1_dinas' => 62, 'ubah_f1_peneliti' => 63, 'sisa_lelang_dinas' => 64, 'sisa_lelang_peneliti' => 65, 'catatan_ubah_f1_dinas' => 66, 'catatan_sisa_lelang_peneliti' => 67, 'pptk_approval' => 68, 'kpa_approval' => 69, 'catatan_bagian_hukum' => 70, 'catatan_inspektorat' => 71, 'catatan_badan_kepegawaian' => 72, 'catatan_lppa' => 73, 'is_bagian_hukum_setuju' => 74, 'is_inspektorat_setuju' => 75, 'is_badan_kepegawaian_setuju' => 76, 'is_lppa_setuju' => 77, 'verifikasi_bpkpd' => 78, 'verifikasi_bappeko' => 79, 'verifikasi_penyelia' => 80, 'verifikasi_bagian_hukum' => 81, 'verifikasi_inspektorat' => 82, 'verifikasi_badan_kepegawaian' => 83, 'verifikasi_lppa' => 84, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/MurniBukubiruPraevagubMasterKegiatanMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.MurniBukubiruPraevagubMasterKegiatanMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = MurniBukubiruPraevagubMasterKegiatanPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(MurniBukubiruPraevagubMasterKegiatanPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(MurniBukubiruPraevagubMasterKegiatanPeer::UNIT_ID);

		$criteria->addSelectColumn(MurniBukubiruPraevagubMasterKegiatanPeer::KODE_KEGIATAN);

		$criteria->addSelectColumn(MurniBukubiruPraevagubMasterKegiatanPeer::KODE_BIDANG);

		$criteria->addSelectColumn(MurniBukubiruPraevagubMasterKegiatanPeer::KODE_URUSAN_WAJIB);

		$criteria->addSelectColumn(MurniBukubiruPraevagubMasterKegiatanPeer::KODE_PROGRAM);

		$criteria->addSelectColumn(MurniBukubiruPraevagubMasterKegiatanPeer::KODE_SASARAN);

		$criteria->addSelectColumn(MurniBukubiruPraevagubMasterKegiatanPeer::KODE_INDIKATOR);

		$criteria->addSelectColumn(MurniBukubiruPraevagubMasterKegiatanPeer::ALOKASI_DANA);

		$criteria->addSelectColumn(MurniBukubiruPraevagubMasterKegiatanPeer::NAMA_KEGIATAN);

		$criteria->addSelectColumn(MurniBukubiruPraevagubMasterKegiatanPeer::MASUKAN);

		$criteria->addSelectColumn(MurniBukubiruPraevagubMasterKegiatanPeer::OUTPUT);

		$criteria->addSelectColumn(MurniBukubiruPraevagubMasterKegiatanPeer::OUTCOME);

		$criteria->addSelectColumn(MurniBukubiruPraevagubMasterKegiatanPeer::BENEFIT);

		$criteria->addSelectColumn(MurniBukubiruPraevagubMasterKegiatanPeer::IMPACT);

		$criteria->addSelectColumn(MurniBukubiruPraevagubMasterKegiatanPeer::TIPE);

		$criteria->addSelectColumn(MurniBukubiruPraevagubMasterKegiatanPeer::KEGIATAN_ACTIVE);

		$criteria->addSelectColumn(MurniBukubiruPraevagubMasterKegiatanPeer::TO_KEGIATAN_CODE);

		$criteria->addSelectColumn(MurniBukubiruPraevagubMasterKegiatanPeer::CATATAN);

		$criteria->addSelectColumn(MurniBukubiruPraevagubMasterKegiatanPeer::TARGET_OUTCOME);

		$criteria->addSelectColumn(MurniBukubiruPraevagubMasterKegiatanPeer::LOKASI);

		$criteria->addSelectColumn(MurniBukubiruPraevagubMasterKegiatanPeer::JUMLAH_PREV);

		$criteria->addSelectColumn(MurniBukubiruPraevagubMasterKegiatanPeer::JUMLAH_NOW);

		$criteria->addSelectColumn(MurniBukubiruPraevagubMasterKegiatanPeer::JUMLAH_NEXT);

		$criteria->addSelectColumn(MurniBukubiruPraevagubMasterKegiatanPeer::KODE_PROGRAM2);

		$criteria->addSelectColumn(MurniBukubiruPraevagubMasterKegiatanPeer::KODE_URUSAN);

		$criteria->addSelectColumn(MurniBukubiruPraevagubMasterKegiatanPeer::LAST_UPDATE_USER);

		$criteria->addSelectColumn(MurniBukubiruPraevagubMasterKegiatanPeer::LAST_UPDATE_TIME);

		$criteria->addSelectColumn(MurniBukubiruPraevagubMasterKegiatanPeer::LAST_UPDATE_IP);

		$criteria->addSelectColumn(MurniBukubiruPraevagubMasterKegiatanPeer::TAHAP);

		$criteria->addSelectColumn(MurniBukubiruPraevagubMasterKegiatanPeer::KODE_MISI);

		$criteria->addSelectColumn(MurniBukubiruPraevagubMasterKegiatanPeer::KODE_TUJUAN);

		$criteria->addSelectColumn(MurniBukubiruPraevagubMasterKegiatanPeer::RANKING);

		$criteria->addSelectColumn(MurniBukubiruPraevagubMasterKegiatanPeer::NOMOR13);

		$criteria->addSelectColumn(MurniBukubiruPraevagubMasterKegiatanPeer::PPA_NAMA);

		$criteria->addSelectColumn(MurniBukubiruPraevagubMasterKegiatanPeer::PPA_PANGKAT);

		$criteria->addSelectColumn(MurniBukubiruPraevagubMasterKegiatanPeer::PPA_NIP);

		$criteria->addSelectColumn(MurniBukubiruPraevagubMasterKegiatanPeer::LANJUTAN);

		$criteria->addSelectColumn(MurniBukubiruPraevagubMasterKegiatanPeer::USER_ID);

		$criteria->addSelectColumn(MurniBukubiruPraevagubMasterKegiatanPeer::ID);

		$criteria->addSelectColumn(MurniBukubiruPraevagubMasterKegiatanPeer::TAHUN);

		$criteria->addSelectColumn(MurniBukubiruPraevagubMasterKegiatanPeer::TAMBAHAN_PAGU);

		$criteria->addSelectColumn(MurniBukubiruPraevagubMasterKegiatanPeer::GENDER);

		$criteria->addSelectColumn(MurniBukubiruPraevagubMasterKegiatanPeer::KODE_KEG_KEUANGAN);

		$criteria->addSelectColumn(MurniBukubiruPraevagubMasterKegiatanPeer::USER_ID_LAMA);

		$criteria->addSelectColumn(MurniBukubiruPraevagubMasterKegiatanPeer::INDIKATOR);

		$criteria->addSelectColumn(MurniBukubiruPraevagubMasterKegiatanPeer::IS_DAK);

		$criteria->addSelectColumn(MurniBukubiruPraevagubMasterKegiatanPeer::KODE_KEGIATAN_ASAL);

		$criteria->addSelectColumn(MurniBukubiruPraevagubMasterKegiatanPeer::KODE_KEG_KEUANGAN_ASAL);

		$criteria->addSelectColumn(MurniBukubiruPraevagubMasterKegiatanPeer::TH_KE_MULTIYEARS);

		$criteria->addSelectColumn(MurniBukubiruPraevagubMasterKegiatanPeer::KELOMPOK_SASARAN);

		$criteria->addSelectColumn(MurniBukubiruPraevagubMasterKegiatanPeer::PAGU_BAPPEKO);

		$criteria->addSelectColumn(MurniBukubiruPraevagubMasterKegiatanPeer::KODE_DPA);

		$criteria->addSelectColumn(MurniBukubiruPraevagubMasterKegiatanPeer::USER_ID_PPTK);

		$criteria->addSelectColumn(MurniBukubiruPraevagubMasterKegiatanPeer::USER_ID_KPA);

		$criteria->addSelectColumn(MurniBukubiruPraevagubMasterKegiatanPeer::CATATAN_PEMBAHASAN);

		$criteria->addSelectColumn(MurniBukubiruPraevagubMasterKegiatanPeer::STATUS_LEVEL);

		$criteria->addSelectColumn(MurniBukubiruPraevagubMasterKegiatanPeer::IS_TAPD_SETUJU);

		$criteria->addSelectColumn(MurniBukubiruPraevagubMasterKegiatanPeer::IS_BAPPEKO_SETUJU);

		$criteria->addSelectColumn(MurniBukubiruPraevagubMasterKegiatanPeer::IS_PENYELIA_SETUJU);

		$criteria->addSelectColumn(MurniBukubiruPraevagubMasterKegiatanPeer::IS_PERNAH_RKA);

		$criteria->addSelectColumn(MurniBukubiruPraevagubMasterKegiatanPeer::KODE_KEGIATAN_BARU);

		$criteria->addSelectColumn(MurniBukubiruPraevagubMasterKegiatanPeer::CATATAN_BPKPD);

		$criteria->addSelectColumn(MurniBukubiruPraevagubMasterKegiatanPeer::UBAH_F1_DINAS);

		$criteria->addSelectColumn(MurniBukubiruPraevagubMasterKegiatanPeer::UBAH_F1_PENELITI);

		$criteria->addSelectColumn(MurniBukubiruPraevagubMasterKegiatanPeer::SISA_LELANG_DINAS);

		$criteria->addSelectColumn(MurniBukubiruPraevagubMasterKegiatanPeer::SISA_LELANG_PENELITI);

		$criteria->addSelectColumn(MurniBukubiruPraevagubMasterKegiatanPeer::CATATAN_UBAH_F1_DINAS);

		$criteria->addSelectColumn(MurniBukubiruPraevagubMasterKegiatanPeer::CATATAN_SISA_LELANG_PENELITI);

		$criteria->addSelectColumn(MurniBukubiruPraevagubMasterKegiatanPeer::PPTK_APPROVAL);

		$criteria->addSelectColumn(MurniBukubiruPraevagubMasterKegiatanPeer::KPA_APPROVAL);

		$criteria->addSelectColumn(MurniBukubiruPraevagubMasterKegiatanPeer::CATATAN_BAGIAN_HUKUM);

		$criteria->addSelectColumn(MurniBukubiruPraevagubMasterKegiatanPeer::CATATAN_INSPEKTORAT);

		$criteria->addSelectColumn(MurniBukubiruPraevagubMasterKegiatanPeer::CATATAN_BADAN_KEPEGAWAIAN);

		$criteria->addSelectColumn(MurniBukubiruPraevagubMasterKegiatanPeer::CATATAN_LPPA);

		$criteria->addSelectColumn(MurniBukubiruPraevagubMasterKegiatanPeer::IS_BAGIAN_HUKUM_SETUJU);

		$criteria->addSelectColumn(MurniBukubiruPraevagubMasterKegiatanPeer::IS_INSPEKTORAT_SETUJU);

		$criteria->addSelectColumn(MurniBukubiruPraevagubMasterKegiatanPeer::IS_BADAN_KEPEGAWAIAN_SETUJU);

		$criteria->addSelectColumn(MurniBukubiruPraevagubMasterKegiatanPeer::IS_LPPA_SETUJU);

		$criteria->addSelectColumn(MurniBukubiruPraevagubMasterKegiatanPeer::VERIFIKASI_BPKPD);

		$criteria->addSelectColumn(MurniBukubiruPraevagubMasterKegiatanPeer::VERIFIKASI_BAPPEKO);

		$criteria->addSelectColumn(MurniBukubiruPraevagubMasterKegiatanPeer::VERIFIKASI_PENYELIA);

		$criteria->addSelectColumn(MurniBukubiruPraevagubMasterKegiatanPeer::VERIFIKASI_BAGIAN_HUKUM);

		$criteria->addSelectColumn(MurniBukubiruPraevagubMasterKegiatanPeer::VERIFIKASI_INSPEKTORAT);

		$criteria->addSelectColumn(MurniBukubiruPraevagubMasterKegiatanPeer::VERIFIKASI_BADAN_KEPEGAWAIAN);

		$criteria->addSelectColumn(MurniBukubiruPraevagubMasterKegiatanPeer::VERIFIKASI_LPPA);

	}

	const COUNT = 'COUNT(ebudget.murni_bukubiru_praevagub_master_kegiatan.UNIT_ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.murni_bukubiru_praevagub_master_kegiatan.UNIT_ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(MurniBukubiruPraevagubMasterKegiatanPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(MurniBukubiruPraevagubMasterKegiatanPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = MurniBukubiruPraevagubMasterKegiatanPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = MurniBukubiruPraevagubMasterKegiatanPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return MurniBukubiruPraevagubMasterKegiatanPeer::populateObjects(MurniBukubiruPraevagubMasterKegiatanPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			MurniBukubiruPraevagubMasterKegiatanPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = MurniBukubiruPraevagubMasterKegiatanPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return MurniBukubiruPraevagubMasterKegiatanPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}

		$criteria->remove(MurniBukubiruPraevagubMasterKegiatanPeer::ID); 

				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(MurniBukubiruPraevagubMasterKegiatanPeer::UNIT_ID);
			$selectCriteria->add(MurniBukubiruPraevagubMasterKegiatanPeer::UNIT_ID, $criteria->remove(MurniBukubiruPraevagubMasterKegiatanPeer::UNIT_ID), $comparison);

			$comparison = $criteria->getComparison(MurniBukubiruPraevagubMasterKegiatanPeer::KODE_KEGIATAN);
			$selectCriteria->add(MurniBukubiruPraevagubMasterKegiatanPeer::KODE_KEGIATAN, $criteria->remove(MurniBukubiruPraevagubMasterKegiatanPeer::KODE_KEGIATAN), $comparison);

			$comparison = $criteria->getComparison(MurniBukubiruPraevagubMasterKegiatanPeer::ID);
			$selectCriteria->add(MurniBukubiruPraevagubMasterKegiatanPeer::ID, $criteria->remove(MurniBukubiruPraevagubMasterKegiatanPeer::ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(MurniBukubiruPraevagubMasterKegiatanPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(MurniBukubiruPraevagubMasterKegiatanPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof MurniBukubiruPraevagubMasterKegiatan) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
												if(count($values) == count($values, COUNT_RECURSIVE))
			{
								$values = array($values);
			}
			$vals = array();
			foreach($values as $value)
			{

				$vals[0][] = $value[0];
				$vals[1][] = $value[1];
				$vals[2][] = $value[2];
			}

			$criteria->add(MurniBukubiruPraevagubMasterKegiatanPeer::UNIT_ID, $vals[0], Criteria::IN);
			$criteria->add(MurniBukubiruPraevagubMasterKegiatanPeer::KODE_KEGIATAN, $vals[1], Criteria::IN);
			$criteria->add(MurniBukubiruPraevagubMasterKegiatanPeer::ID, $vals[2], Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(MurniBukubiruPraevagubMasterKegiatan $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(MurniBukubiruPraevagubMasterKegiatanPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(MurniBukubiruPraevagubMasterKegiatanPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(MurniBukubiruPraevagubMasterKegiatanPeer::DATABASE_NAME, MurniBukubiruPraevagubMasterKegiatanPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = MurniBukubiruPraevagubMasterKegiatanPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK( $unit_id, $kode_kegiatan, $id, $con = null) {
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$criteria = new Criteria();
		$criteria->add(MurniBukubiruPraevagubMasterKegiatanPeer::UNIT_ID, $unit_id);
		$criteria->add(MurniBukubiruPraevagubMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
		$criteria->add(MurniBukubiruPraevagubMasterKegiatanPeer::ID, $id);
		$v = MurniBukubiruPraevagubMasterKegiatanPeer::doSelect($criteria, $con);

		return !empty($v) ? $v[0] : null;
	}
} 
if (Propel::isInit()) {
			try {
		BaseMurniBukubiruPraevagubMasterKegiatanPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/MurniBukubiruPraevagubMasterKegiatanMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.MurniBukubiruPraevagubMasterKegiatanMapBuilder');
}
