<?php


abstract class BaseMasterUserV2 extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $user_id;


	
	protected $user_name;


	
	protected $user_default_password;


	
	protected $user_password;


	
	protected $ip_address;


	
	protected $waktu_access;


	
	protected $user_enable;


	
	protected $nip;


	
	protected $eula_budgeting;


	
	protected $jabatan;


	
	protected $email;


	
	protected $telepon;


	
	protected $jenis_kelamin;

	
	protected $collSchemaAksesV2s;

	
	protected $lastSchemaAksesV2Criteria = null;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getUserId()
	{

		return $this->user_id;
	}

	
	public function getUserName()
	{

		return $this->user_name;
	}

	
	public function getUserDefaultPassword()
	{

		return $this->user_default_password;
	}

	
	public function getUserPassword()
	{

		return $this->user_password;
	}

	
	public function getIpAddress()
	{

		return $this->ip_address;
	}

	
	public function getWaktuAccess($format = 'Y-m-d H:i:s')
	{

		if ($this->waktu_access === null || $this->waktu_access === '') {
			return null;
		} elseif (!is_int($this->waktu_access)) {
						$ts = strtotime($this->waktu_access);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [waktu_access] as date/time value: " . var_export($this->waktu_access, true));
			}
		} else {
			$ts = $this->waktu_access;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getUserEnable()
	{

		return $this->user_enable;
	}

	
	public function getNip()
	{

		return $this->nip;
	}

	
	public function getEulaBudgeting()
	{

		return $this->eula_budgeting;
	}

	
	public function getJabatan()
	{

		return $this->jabatan;
	}

	
	public function getEmail()
	{

		return $this->email;
	}

	
	public function getTelepon()
	{

		return $this->telepon;
	}

	
	public function getJenisKelamin()
	{

		return $this->jenis_kelamin;
	}

	
	public function setUserId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->user_id !== $v) {
			$this->user_id = $v;
			$this->modifiedColumns[] = MasterUserV2Peer::USER_ID;
		}

	} 
	
	public function setUserName($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->user_name !== $v) {
			$this->user_name = $v;
			$this->modifiedColumns[] = MasterUserV2Peer::USER_NAME;
		}

	} 
	
	public function setUserDefaultPassword($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->user_default_password !== $v) {
			$this->user_default_password = $v;
			$this->modifiedColumns[] = MasterUserV2Peer::USER_DEFAULT_PASSWORD;
		}

	} 
	
	public function setUserPassword($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->user_password !== $v) {
			$this->user_password = $v;
			$this->modifiedColumns[] = MasterUserV2Peer::USER_PASSWORD;
		}

	} 
	
	public function setIpAddress($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->ip_address !== $v) {
			$this->ip_address = $v;
			$this->modifiedColumns[] = MasterUserV2Peer::IP_ADDRESS;
		}

	} 
	
	public function setWaktuAccess($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [waktu_access] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->waktu_access !== $ts) {
			$this->waktu_access = $ts;
			$this->modifiedColumns[] = MasterUserV2Peer::WAKTU_ACCESS;
		}

	} 
	
	public function setUserEnable($v)
	{

		if ($this->user_enable !== $v) {
			$this->user_enable = $v;
			$this->modifiedColumns[] = MasterUserV2Peer::USER_ENABLE;
		}

	} 
	
	public function setNip($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nip !== $v) {
			$this->nip = $v;
			$this->modifiedColumns[] = MasterUserV2Peer::NIP;
		}

	} 
	
	public function setEulaBudgeting($v)
	{

		if ($this->eula_budgeting !== $v) {
			$this->eula_budgeting = $v;
			$this->modifiedColumns[] = MasterUserV2Peer::EULA_BUDGETING;
		}

	} 
	
	public function setJabatan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->jabatan !== $v) {
			$this->jabatan = $v;
			$this->modifiedColumns[] = MasterUserV2Peer::JABATAN;
		}

	} 
	
	public function setEmail($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->email !== $v) {
			$this->email = $v;
			$this->modifiedColumns[] = MasterUserV2Peer::EMAIL;
		}

	} 
	
	public function setTelepon($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->telepon !== $v) {
			$this->telepon = $v;
			$this->modifiedColumns[] = MasterUserV2Peer::TELEPON;
		}

	} 
	
	public function setJenisKelamin($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->jenis_kelamin !== $v) {
			$this->jenis_kelamin = $v;
			$this->modifiedColumns[] = MasterUserV2Peer::JENIS_KELAMIN;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->user_id = $rs->getString($startcol + 0);

			$this->user_name = $rs->getString($startcol + 1);

			$this->user_default_password = $rs->getString($startcol + 2);

			$this->user_password = $rs->getString($startcol + 3);

			$this->ip_address = $rs->getString($startcol + 4);

			$this->waktu_access = $rs->getTimestamp($startcol + 5, null);

			$this->user_enable = $rs->getBoolean($startcol + 6);

			$this->nip = $rs->getString($startcol + 7);

			$this->eula_budgeting = $rs->getBoolean($startcol + 8);

			$this->jabatan = $rs->getString($startcol + 9);

			$this->email = $rs->getString($startcol + 10);

			$this->telepon = $rs->getString($startcol + 11);

			$this->jenis_kelamin = $rs->getString($startcol + 12);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 13; 
		} catch (Exception $e) {
			throw new PropelException("Error populating MasterUserV2 object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(MasterUserV2Peer::DATABASE_NAME);
		}

		try {
			$con->begin();
			MasterUserV2Peer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(MasterUserV2Peer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = MasterUserV2Peer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += MasterUserV2Peer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			if ($this->collSchemaAksesV2s !== null) {
				foreach($this->collSchemaAksesV2s as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = MasterUserV2Peer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}


				if ($this->collSchemaAksesV2s !== null) {
					foreach($this->collSchemaAksesV2s as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}


			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = MasterUserV2Peer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getUserId();
				break;
			case 1:
				return $this->getUserName();
				break;
			case 2:
				return $this->getUserDefaultPassword();
				break;
			case 3:
				return $this->getUserPassword();
				break;
			case 4:
				return $this->getIpAddress();
				break;
			case 5:
				return $this->getWaktuAccess();
				break;
			case 6:
				return $this->getUserEnable();
				break;
			case 7:
				return $this->getNip();
				break;
			case 8:
				return $this->getEulaBudgeting();
				break;
			case 9:
				return $this->getJabatan();
				break;
			case 10:
				return $this->getEmail();
				break;
			case 11:
				return $this->getTelepon();
				break;
			case 12:
				return $this->getJenisKelamin();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = MasterUserV2Peer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getUserId(),
			$keys[1] => $this->getUserName(),
			$keys[2] => $this->getUserDefaultPassword(),
			$keys[3] => $this->getUserPassword(),
			$keys[4] => $this->getIpAddress(),
			$keys[5] => $this->getWaktuAccess(),
			$keys[6] => $this->getUserEnable(),
			$keys[7] => $this->getNip(),
			$keys[8] => $this->getEulaBudgeting(),
			$keys[9] => $this->getJabatan(),
			$keys[10] => $this->getEmail(),
			$keys[11] => $this->getTelepon(),
			$keys[12] => $this->getJenisKelamin(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = MasterUserV2Peer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setUserId($value);
				break;
			case 1:
				$this->setUserName($value);
				break;
			case 2:
				$this->setUserDefaultPassword($value);
				break;
			case 3:
				$this->setUserPassword($value);
				break;
			case 4:
				$this->setIpAddress($value);
				break;
			case 5:
				$this->setWaktuAccess($value);
				break;
			case 6:
				$this->setUserEnable($value);
				break;
			case 7:
				$this->setNip($value);
				break;
			case 8:
				$this->setEulaBudgeting($value);
				break;
			case 9:
				$this->setJabatan($value);
				break;
			case 10:
				$this->setEmail($value);
				break;
			case 11:
				$this->setTelepon($value);
				break;
			case 12:
				$this->setJenisKelamin($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = MasterUserV2Peer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setUserId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setUserName($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setUserDefaultPassword($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setUserPassword($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setIpAddress($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setWaktuAccess($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setUserEnable($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setNip($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setEulaBudgeting($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setJabatan($arr[$keys[9]]);
		if (array_key_exists($keys[10], $arr)) $this->setEmail($arr[$keys[10]]);
		if (array_key_exists($keys[11], $arr)) $this->setTelepon($arr[$keys[11]]);
		if (array_key_exists($keys[12], $arr)) $this->setJenisKelamin($arr[$keys[12]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(MasterUserV2Peer::DATABASE_NAME);

		if ($this->isColumnModified(MasterUserV2Peer::USER_ID)) $criteria->add(MasterUserV2Peer::USER_ID, $this->user_id);
		if ($this->isColumnModified(MasterUserV2Peer::USER_NAME)) $criteria->add(MasterUserV2Peer::USER_NAME, $this->user_name);
		if ($this->isColumnModified(MasterUserV2Peer::USER_DEFAULT_PASSWORD)) $criteria->add(MasterUserV2Peer::USER_DEFAULT_PASSWORD, $this->user_default_password);
		if ($this->isColumnModified(MasterUserV2Peer::USER_PASSWORD)) $criteria->add(MasterUserV2Peer::USER_PASSWORD, $this->user_password);
		if ($this->isColumnModified(MasterUserV2Peer::IP_ADDRESS)) $criteria->add(MasterUserV2Peer::IP_ADDRESS, $this->ip_address);
		if ($this->isColumnModified(MasterUserV2Peer::WAKTU_ACCESS)) $criteria->add(MasterUserV2Peer::WAKTU_ACCESS, $this->waktu_access);
		if ($this->isColumnModified(MasterUserV2Peer::USER_ENABLE)) $criteria->add(MasterUserV2Peer::USER_ENABLE, $this->user_enable);
		if ($this->isColumnModified(MasterUserV2Peer::NIP)) $criteria->add(MasterUserV2Peer::NIP, $this->nip);
		if ($this->isColumnModified(MasterUserV2Peer::EULA_BUDGETING)) $criteria->add(MasterUserV2Peer::EULA_BUDGETING, $this->eula_budgeting);
		if ($this->isColumnModified(MasterUserV2Peer::JABATAN)) $criteria->add(MasterUserV2Peer::JABATAN, $this->jabatan);
		if ($this->isColumnModified(MasterUserV2Peer::EMAIL)) $criteria->add(MasterUserV2Peer::EMAIL, $this->email);
		if ($this->isColumnModified(MasterUserV2Peer::TELEPON)) $criteria->add(MasterUserV2Peer::TELEPON, $this->telepon);
		if ($this->isColumnModified(MasterUserV2Peer::JENIS_KELAMIN)) $criteria->add(MasterUserV2Peer::JENIS_KELAMIN, $this->jenis_kelamin);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(MasterUserV2Peer::DATABASE_NAME);

		$criteria->add(MasterUserV2Peer::USER_ID, $this->user_id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getUserId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setUserId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setUserName($this->user_name);

		$copyObj->setUserDefaultPassword($this->user_default_password);

		$copyObj->setUserPassword($this->user_password);

		$copyObj->setIpAddress($this->ip_address);

		$copyObj->setWaktuAccess($this->waktu_access);

		$copyObj->setUserEnable($this->user_enable);

		$copyObj->setNip($this->nip);

		$copyObj->setEulaBudgeting($this->eula_budgeting);

		$copyObj->setJabatan($this->jabatan);

		$copyObj->setEmail($this->email);

		$copyObj->setTelepon($this->telepon);

		$copyObj->setJenisKelamin($this->jenis_kelamin);


		if ($deepCopy) {
									$copyObj->setNew(false);

			foreach($this->getSchemaAksesV2s() as $relObj) {
				$copyObj->addSchemaAksesV2($relObj->copy($deepCopy));
			}

		} 

		$copyObj->setNew(true);

		$copyObj->setUserId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new MasterUserV2Peer();
		}
		return self::$peer;
	}

	
	public function initSchemaAksesV2s()
	{
		if ($this->collSchemaAksesV2s === null) {
			$this->collSchemaAksesV2s = array();
		}
	}

	
	public function getSchemaAksesV2s($criteria = null, $con = null)
	{
				include_once 'lib/model/budgeting/om/BaseSchemaAksesV2Peer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collSchemaAksesV2s === null) {
			if ($this->isNew()) {
			   $this->collSchemaAksesV2s = array();
			} else {

				$criteria->add(SchemaAksesV2Peer::USER_ID, $this->getUserId());

				SchemaAksesV2Peer::addSelectColumns($criteria);
				$this->collSchemaAksesV2s = SchemaAksesV2Peer::doSelect($criteria, $con);
			}
		} else {
						if (!$this->isNew()) {
												

				$criteria->add(SchemaAksesV2Peer::USER_ID, $this->getUserId());

				SchemaAksesV2Peer::addSelectColumns($criteria);
				if (!isset($this->lastSchemaAksesV2Criteria) || !$this->lastSchemaAksesV2Criteria->equals($criteria)) {
					$this->collSchemaAksesV2s = SchemaAksesV2Peer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastSchemaAksesV2Criteria = $criteria;
		return $this->collSchemaAksesV2s;
	}

	
	public function countSchemaAksesV2s($criteria = null, $distinct = false, $con = null)
	{
				include_once 'lib/model/budgeting/om/BaseSchemaAksesV2Peer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(SchemaAksesV2Peer::USER_ID, $this->getUserId());

		return SchemaAksesV2Peer::doCount($criteria, $distinct, $con);
	}

	
	public function addSchemaAksesV2(SchemaAksesV2 $l)
	{
		$this->collSchemaAksesV2s[] = $l;
		$l->setMasterUserV2($this);
	}


	
	public function getSchemaAksesV2sJoinMasterSchema($criteria = null, $con = null)
	{
				include_once 'lib/model/budgeting/om/BaseSchemaAksesV2Peer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collSchemaAksesV2s === null) {
			if ($this->isNew()) {
				$this->collSchemaAksesV2s = array();
			} else {

				$criteria->add(SchemaAksesV2Peer::USER_ID, $this->getUserId());

				$this->collSchemaAksesV2s = SchemaAksesV2Peer::doSelectJoinMasterSchema($criteria, $con);
			}
		} else {
									
			$criteria->add(SchemaAksesV2Peer::USER_ID, $this->getUserId());

			if (!isset($this->lastSchemaAksesV2Criteria) || !$this->lastSchemaAksesV2Criteria->equals($criteria)) {
				$this->collSchemaAksesV2s = SchemaAksesV2Peer::doSelectJoinMasterSchema($criteria, $con);
			}
		}
		$this->lastSchemaAksesV2Criteria = $criteria;

		return $this->collSchemaAksesV2s;
	}


	
	public function getSchemaAksesV2sJoinUserLevel($criteria = null, $con = null)
	{
				include_once 'lib/model/budgeting/om/BaseSchemaAksesV2Peer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collSchemaAksesV2s === null) {
			if ($this->isNew()) {
				$this->collSchemaAksesV2s = array();
			} else {

				$criteria->add(SchemaAksesV2Peer::USER_ID, $this->getUserId());

				$this->collSchemaAksesV2s = SchemaAksesV2Peer::doSelectJoinUserLevel($criteria, $con);
			}
		} else {
									
			$criteria->add(SchemaAksesV2Peer::USER_ID, $this->getUserId());

			if (!isset($this->lastSchemaAksesV2Criteria) || !$this->lastSchemaAksesV2Criteria->equals($criteria)) {
				$this->collSchemaAksesV2s = SchemaAksesV2Peer::doSelectJoinUserLevel($criteria, $con);
			}
		}
		$this->lastSchemaAksesV2Criteria = $criteria;

		return $this->collSchemaAksesV2s;
	}

} 