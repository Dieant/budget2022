<?php


abstract class BaseAkrualKomponenPenyusunan extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $kode_akrual_komponen_penyusun;


	
	protected $nama;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getKodeAkrualKomponenPenyusun()
	{

		return $this->kode_akrual_komponen_penyusun;
	}

	
	public function getNama()
	{

		return $this->nama;
	}

	
	public function setKodeAkrualKomponenPenyusun($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_akrual_komponen_penyusun !== $v) {
			$this->kode_akrual_komponen_penyusun = $v;
			$this->modifiedColumns[] = AkrualKomponenPenyusunanPeer::KODE_AKRUAL_KOMPONEN_PENYUSUN;
		}

	} 
	
	public function setNama($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nama !== $v) {
			$this->nama = $v;
			$this->modifiedColumns[] = AkrualKomponenPenyusunanPeer::NAMA;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->kode_akrual_komponen_penyusun = $rs->getString($startcol + 0);

			$this->nama = $rs->getString($startcol + 1);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 2; 
		} catch (Exception $e) {
			throw new PropelException("Error populating AkrualKomponenPenyusunan object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(AkrualKomponenPenyusunanPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			AkrualKomponenPenyusunanPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(AkrualKomponenPenyusunanPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = AkrualKomponenPenyusunanPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += AkrualKomponenPenyusunanPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = AkrualKomponenPenyusunanPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = AkrualKomponenPenyusunanPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getKodeAkrualKomponenPenyusun();
				break;
			case 1:
				return $this->getNama();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = AkrualKomponenPenyusunanPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getKodeAkrualKomponenPenyusun(),
			$keys[1] => $this->getNama(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = AkrualKomponenPenyusunanPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setKodeAkrualKomponenPenyusun($value);
				break;
			case 1:
				$this->setNama($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = AkrualKomponenPenyusunanPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setKodeAkrualKomponenPenyusun($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setNama($arr[$keys[1]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(AkrualKomponenPenyusunanPeer::DATABASE_NAME);

		if ($this->isColumnModified(AkrualKomponenPenyusunanPeer::KODE_AKRUAL_KOMPONEN_PENYUSUN)) $criteria->add(AkrualKomponenPenyusunanPeer::KODE_AKRUAL_KOMPONEN_PENYUSUN, $this->kode_akrual_komponen_penyusun);
		if ($this->isColumnModified(AkrualKomponenPenyusunanPeer::NAMA)) $criteria->add(AkrualKomponenPenyusunanPeer::NAMA, $this->nama);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(AkrualKomponenPenyusunanPeer::DATABASE_NAME);

		$criteria->add(AkrualKomponenPenyusunanPeer::KODE_AKRUAL_KOMPONEN_PENYUSUN, $this->kode_akrual_komponen_penyusun);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getKodeAkrualKomponenPenyusun();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setKodeAkrualKomponenPenyusun($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setNama($this->nama);


		$copyObj->setNew(true);

		$copyObj->setKodeAkrualKomponenPenyusun(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new AkrualKomponenPenyusunanPeer();
		}
		return self::$peer;
	}

} 