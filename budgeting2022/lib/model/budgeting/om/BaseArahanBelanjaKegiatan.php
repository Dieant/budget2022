<?php


abstract class BaseArahanBelanjaKegiatan extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id;


	
	protected $unit_id;


	
	protected $kegiatan_code;


	
	protected $kode_belanja;


	
	protected $anggaran;


	
	protected $status = 0;


	
	protected $kode_detail_kegiatan;


	
	protected $created_at;


	
	protected $updated_at;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getId()
	{

		return $this->id;
	}

	
	public function getUnitId()
	{

		return $this->unit_id;
	}

	
	public function getKegiatanCode()
	{

		return $this->kegiatan_code;
	}

	
	public function getKodeBelanja()
	{

		return $this->kode_belanja;
	}

	
	public function getAnggaran()
	{

		return $this->anggaran;
	}

	
	public function getStatus()
	{

		return $this->status;
	}

	
	public function getKodeDetailKegiatan()
	{

		return $this->kode_detail_kegiatan;
	}

	
	public function getCreatedAt($format = 'Y-m-d H:i:s')
	{

		if ($this->created_at === null || $this->created_at === '') {
			return null;
		} elseif (!is_int($this->created_at)) {
						$ts = strtotime($this->created_at);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [created_at] as date/time value: " . var_export($this->created_at, true));
			}
		} else {
			$ts = $this->created_at;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getUpdatedAt($format = 'Y-m-d H:i:s')
	{

		if ($this->updated_at === null || $this->updated_at === '') {
			return null;
		} elseif (!is_int($this->updated_at)) {
						$ts = strtotime($this->updated_at);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [updated_at] as date/time value: " . var_export($this->updated_at, true));
			}
		} else {
			$ts = $this->updated_at;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function setId($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = ArahanBelanjaKegiatanPeer::ID;
		}

	} 
	
	public function setUnitId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->unit_id !== $v) {
			$this->unit_id = $v;
			$this->modifiedColumns[] = ArahanBelanjaKegiatanPeer::UNIT_ID;
		}

	} 
	
	public function setKegiatanCode($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kegiatan_code !== $v) {
			$this->kegiatan_code = $v;
			$this->modifiedColumns[] = ArahanBelanjaKegiatanPeer::KEGIATAN_CODE;
		}

	} 
	
	public function setKodeBelanja($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_belanja !== $v) {
			$this->kode_belanja = $v;
			$this->modifiedColumns[] = ArahanBelanjaKegiatanPeer::KODE_BELANJA;
		}

	} 
	
	public function setAnggaran($v)
	{

		if ($this->anggaran !== $v) {
			$this->anggaran = $v;
			$this->modifiedColumns[] = ArahanBelanjaKegiatanPeer::ANGGARAN;
		}

	} 
	
	public function setStatus($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->status !== $v || $v === 0) {
			$this->status = $v;
			$this->modifiedColumns[] = ArahanBelanjaKegiatanPeer::STATUS;
		}

	} 
	
	public function setKodeDetailKegiatan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_detail_kegiatan !== $v) {
			$this->kode_detail_kegiatan = $v;
			$this->modifiedColumns[] = ArahanBelanjaKegiatanPeer::KODE_DETAIL_KEGIATAN;
		}

	} 
	
	public function setCreatedAt($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [created_at] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->created_at !== $ts) {
			$this->created_at = $ts;
			$this->modifiedColumns[] = ArahanBelanjaKegiatanPeer::CREATED_AT;
		}

	} 
	
	public function setUpdatedAt($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [updated_at] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->updated_at !== $ts) {
			$this->updated_at = $ts;
			$this->modifiedColumns[] = ArahanBelanjaKegiatanPeer::UPDATED_AT;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getInt($startcol + 0);

			$this->unit_id = $rs->getString($startcol + 1);

			$this->kegiatan_code = $rs->getString($startcol + 2);

			$this->kode_belanja = $rs->getString($startcol + 3);

			$this->anggaran = $rs->getFloat($startcol + 4);

			$this->status = $rs->getInt($startcol + 5);

			$this->kode_detail_kegiatan = $rs->getString($startcol + 6);

			$this->created_at = $rs->getTimestamp($startcol + 7, null);

			$this->updated_at = $rs->getTimestamp($startcol + 8, null);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 9; 
		} catch (Exception $e) {
			throw new PropelException("Error populating ArahanBelanjaKegiatan object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(ArahanBelanjaKegiatanPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			ArahanBelanjaKegiatanPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
    if ($this->isNew() && !$this->isColumnModified(ArahanBelanjaKegiatanPeer::CREATED_AT))
    {
      $this->setCreatedAt(time());
    }

    if ($this->isModified() && !$this->isColumnModified(ArahanBelanjaKegiatanPeer::UPDATED_AT))
    {
      $this->setUpdatedAt(time());
    }

		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(ArahanBelanjaKegiatanPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = ArahanBelanjaKegiatanPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += ArahanBelanjaKegiatanPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = ArahanBelanjaKegiatanPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = ArahanBelanjaKegiatanPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getUnitId();
				break;
			case 2:
				return $this->getKegiatanCode();
				break;
			case 3:
				return $this->getKodeBelanja();
				break;
			case 4:
				return $this->getAnggaran();
				break;
			case 5:
				return $this->getStatus();
				break;
			case 6:
				return $this->getKodeDetailKegiatan();
				break;
			case 7:
				return $this->getCreatedAt();
				break;
			case 8:
				return $this->getUpdatedAt();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = ArahanBelanjaKegiatanPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getUnitId(),
			$keys[2] => $this->getKegiatanCode(),
			$keys[3] => $this->getKodeBelanja(),
			$keys[4] => $this->getAnggaran(),
			$keys[5] => $this->getStatus(),
			$keys[6] => $this->getKodeDetailKegiatan(),
			$keys[7] => $this->getCreatedAt(),
			$keys[8] => $this->getUpdatedAt(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = ArahanBelanjaKegiatanPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setUnitId($value);
				break;
			case 2:
				$this->setKegiatanCode($value);
				break;
			case 3:
				$this->setKodeBelanja($value);
				break;
			case 4:
				$this->setAnggaran($value);
				break;
			case 5:
				$this->setStatus($value);
				break;
			case 6:
				$this->setKodeDetailKegiatan($value);
				break;
			case 7:
				$this->setCreatedAt($value);
				break;
			case 8:
				$this->setUpdatedAt($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = ArahanBelanjaKegiatanPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setUnitId($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setKegiatanCode($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setKodeBelanja($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setAnggaran($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setStatus($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setKodeDetailKegiatan($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setCreatedAt($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setUpdatedAt($arr[$keys[8]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(ArahanBelanjaKegiatanPeer::DATABASE_NAME);

		if ($this->isColumnModified(ArahanBelanjaKegiatanPeer::ID)) $criteria->add(ArahanBelanjaKegiatanPeer::ID, $this->id);
		if ($this->isColumnModified(ArahanBelanjaKegiatanPeer::UNIT_ID)) $criteria->add(ArahanBelanjaKegiatanPeer::UNIT_ID, $this->unit_id);
		if ($this->isColumnModified(ArahanBelanjaKegiatanPeer::KEGIATAN_CODE)) $criteria->add(ArahanBelanjaKegiatanPeer::KEGIATAN_CODE, $this->kegiatan_code);
		if ($this->isColumnModified(ArahanBelanjaKegiatanPeer::KODE_BELANJA)) $criteria->add(ArahanBelanjaKegiatanPeer::KODE_BELANJA, $this->kode_belanja);
		if ($this->isColumnModified(ArahanBelanjaKegiatanPeer::ANGGARAN)) $criteria->add(ArahanBelanjaKegiatanPeer::ANGGARAN, $this->anggaran);
		if ($this->isColumnModified(ArahanBelanjaKegiatanPeer::STATUS)) $criteria->add(ArahanBelanjaKegiatanPeer::STATUS, $this->status);
		if ($this->isColumnModified(ArahanBelanjaKegiatanPeer::KODE_DETAIL_KEGIATAN)) $criteria->add(ArahanBelanjaKegiatanPeer::KODE_DETAIL_KEGIATAN, $this->kode_detail_kegiatan);
		if ($this->isColumnModified(ArahanBelanjaKegiatanPeer::CREATED_AT)) $criteria->add(ArahanBelanjaKegiatanPeer::CREATED_AT, $this->created_at);
		if ($this->isColumnModified(ArahanBelanjaKegiatanPeer::UPDATED_AT)) $criteria->add(ArahanBelanjaKegiatanPeer::UPDATED_AT, $this->updated_at);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(ArahanBelanjaKegiatanPeer::DATABASE_NAME);

		$criteria->add(ArahanBelanjaKegiatanPeer::ID, $this->id);
		$criteria->add(ArahanBelanjaKegiatanPeer::UNIT_ID, $this->unit_id);
		$criteria->add(ArahanBelanjaKegiatanPeer::KEGIATAN_CODE, $this->kegiatan_code);
		$criteria->add(ArahanBelanjaKegiatanPeer::KODE_BELANJA, $this->kode_belanja);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		$pks = array();

		$pks[0] = $this->getId();

		$pks[1] = $this->getUnitId();

		$pks[2] = $this->getKegiatanCode();

		$pks[3] = $this->getKodeBelanja();

		return $pks;
	}

	
	public function setPrimaryKey($keys)
	{

		$this->setId($keys[0]);

		$this->setUnitId($keys[1]);

		$this->setKegiatanCode($keys[2]);

		$this->setKodeBelanja($keys[3]);

	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setAnggaran($this->anggaran);

		$copyObj->setStatus($this->status);

		$copyObj->setKodeDetailKegiatan($this->kode_detail_kegiatan);

		$copyObj->setCreatedAt($this->created_at);

		$copyObj->setUpdatedAt($this->updated_at);


		$copyObj->setNew(true);

		$copyObj->setId(NULL); 
		$copyObj->setUnitId(NULL); 
		$copyObj->setKegiatanCode(NULL); 
		$copyObj->setKodeBelanja(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new ArahanBelanjaKegiatanPeer();
		}
		return self::$peer;
	}

} 