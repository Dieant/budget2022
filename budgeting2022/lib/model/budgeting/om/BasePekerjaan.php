<?php


abstract class BasePekerjaan extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $unit_id;


	
	protected $kegiatan_code;


	
	protected $komponen_id;


	
	protected $unit_name;


	
	protected $kegiatan_name;


	
	protected $subtitle;


	
	protected $komponen_name;


	
	protected $detail_name;


	
	protected $keterangan_koefisien;


	
	protected $kecamatan;


	
	protected $kelurahan;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getUnitId()
	{

		return $this->unit_id;
	}

	
	public function getKegiatanCode()
	{

		return $this->kegiatan_code;
	}

	
	public function getKomponenId()
	{

		return $this->komponen_id;
	}

	
	public function getUnitName()
	{

		return $this->unit_name;
	}

	
	public function getKegiatanName()
	{

		return $this->kegiatan_name;
	}

	
	public function getSubtitle()
	{

		return $this->subtitle;
	}

	
	public function getKomponenName()
	{

		return $this->komponen_name;
	}

	
	public function getDetailName()
	{

		return $this->detail_name;
	}

	
	public function getKeteranganKoefisien()
	{

		return $this->keterangan_koefisien;
	}

	
	public function getKecamatan()
	{

		return $this->kecamatan;
	}

	
	public function getKelurahan()
	{

		return $this->kelurahan;
	}

	
	public function setUnitId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->unit_id !== $v) {
			$this->unit_id = $v;
			$this->modifiedColumns[] = PekerjaanPeer::UNIT_ID;
		}

	} 
	
	public function setKegiatanCode($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kegiatan_code !== $v) {
			$this->kegiatan_code = $v;
			$this->modifiedColumns[] = PekerjaanPeer::KEGIATAN_CODE;
		}

	} 
	
	public function setKomponenId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->komponen_id !== $v) {
			$this->komponen_id = $v;
			$this->modifiedColumns[] = PekerjaanPeer::KOMPONEN_ID;
		}

	} 
	
	public function setUnitName($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->unit_name !== $v) {
			$this->unit_name = $v;
			$this->modifiedColumns[] = PekerjaanPeer::UNIT_NAME;
		}

	} 
	
	public function setKegiatanName($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kegiatan_name !== $v) {
			$this->kegiatan_name = $v;
			$this->modifiedColumns[] = PekerjaanPeer::KEGIATAN_NAME;
		}

	} 
	
	public function setSubtitle($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->subtitle !== $v) {
			$this->subtitle = $v;
			$this->modifiedColumns[] = PekerjaanPeer::SUBTITLE;
		}

	} 
	
	public function setKomponenName($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->komponen_name !== $v) {
			$this->komponen_name = $v;
			$this->modifiedColumns[] = PekerjaanPeer::KOMPONEN_NAME;
		}

	} 
	
	public function setDetailName($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->detail_name !== $v) {
			$this->detail_name = $v;
			$this->modifiedColumns[] = PekerjaanPeer::DETAIL_NAME;
		}

	} 
	
	public function setKeteranganKoefisien($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->keterangan_koefisien !== $v) {
			$this->keterangan_koefisien = $v;
			$this->modifiedColumns[] = PekerjaanPeer::KETERANGAN_KOEFISIEN;
		}

	} 
	
	public function setKecamatan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kecamatan !== $v) {
			$this->kecamatan = $v;
			$this->modifiedColumns[] = PekerjaanPeer::KECAMATAN;
		}

	} 
	
	public function setKelurahan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kelurahan !== $v) {
			$this->kelurahan = $v;
			$this->modifiedColumns[] = PekerjaanPeer::KELURAHAN;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->unit_id = $rs->getString($startcol + 0);

			$this->kegiatan_code = $rs->getString($startcol + 1);

			$this->komponen_id = $rs->getString($startcol + 2);

			$this->unit_name = $rs->getString($startcol + 3);

			$this->kegiatan_name = $rs->getString($startcol + 4);

			$this->subtitle = $rs->getString($startcol + 5);

			$this->komponen_name = $rs->getString($startcol + 6);

			$this->detail_name = $rs->getString($startcol + 7);

			$this->keterangan_koefisien = $rs->getString($startcol + 8);

			$this->kecamatan = $rs->getString($startcol + 9);

			$this->kelurahan = $rs->getString($startcol + 10);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 11; 
		} catch (Exception $e) {
			throw new PropelException("Error populating Pekerjaan object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(PekerjaanPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			PekerjaanPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(PekerjaanPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = PekerjaanPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += PekerjaanPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = PekerjaanPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = PekerjaanPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getUnitId();
				break;
			case 1:
				return $this->getKegiatanCode();
				break;
			case 2:
				return $this->getKomponenId();
				break;
			case 3:
				return $this->getUnitName();
				break;
			case 4:
				return $this->getKegiatanName();
				break;
			case 5:
				return $this->getSubtitle();
				break;
			case 6:
				return $this->getKomponenName();
				break;
			case 7:
				return $this->getDetailName();
				break;
			case 8:
				return $this->getKeteranganKoefisien();
				break;
			case 9:
				return $this->getKecamatan();
				break;
			case 10:
				return $this->getKelurahan();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = PekerjaanPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getUnitId(),
			$keys[1] => $this->getKegiatanCode(),
			$keys[2] => $this->getKomponenId(),
			$keys[3] => $this->getUnitName(),
			$keys[4] => $this->getKegiatanName(),
			$keys[5] => $this->getSubtitle(),
			$keys[6] => $this->getKomponenName(),
			$keys[7] => $this->getDetailName(),
			$keys[8] => $this->getKeteranganKoefisien(),
			$keys[9] => $this->getKecamatan(),
			$keys[10] => $this->getKelurahan(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = PekerjaanPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setUnitId($value);
				break;
			case 1:
				$this->setKegiatanCode($value);
				break;
			case 2:
				$this->setKomponenId($value);
				break;
			case 3:
				$this->setUnitName($value);
				break;
			case 4:
				$this->setKegiatanName($value);
				break;
			case 5:
				$this->setSubtitle($value);
				break;
			case 6:
				$this->setKomponenName($value);
				break;
			case 7:
				$this->setDetailName($value);
				break;
			case 8:
				$this->setKeteranganKoefisien($value);
				break;
			case 9:
				$this->setKecamatan($value);
				break;
			case 10:
				$this->setKelurahan($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = PekerjaanPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setUnitId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setKegiatanCode($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setKomponenId($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setUnitName($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setKegiatanName($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setSubtitle($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setKomponenName($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setDetailName($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setKeteranganKoefisien($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setKecamatan($arr[$keys[9]]);
		if (array_key_exists($keys[10], $arr)) $this->setKelurahan($arr[$keys[10]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(PekerjaanPeer::DATABASE_NAME);

		if ($this->isColumnModified(PekerjaanPeer::UNIT_ID)) $criteria->add(PekerjaanPeer::UNIT_ID, $this->unit_id);
		if ($this->isColumnModified(PekerjaanPeer::KEGIATAN_CODE)) $criteria->add(PekerjaanPeer::KEGIATAN_CODE, $this->kegiatan_code);
		if ($this->isColumnModified(PekerjaanPeer::KOMPONEN_ID)) $criteria->add(PekerjaanPeer::KOMPONEN_ID, $this->komponen_id);
		if ($this->isColumnModified(PekerjaanPeer::UNIT_NAME)) $criteria->add(PekerjaanPeer::UNIT_NAME, $this->unit_name);
		if ($this->isColumnModified(PekerjaanPeer::KEGIATAN_NAME)) $criteria->add(PekerjaanPeer::KEGIATAN_NAME, $this->kegiatan_name);
		if ($this->isColumnModified(PekerjaanPeer::SUBTITLE)) $criteria->add(PekerjaanPeer::SUBTITLE, $this->subtitle);
		if ($this->isColumnModified(PekerjaanPeer::KOMPONEN_NAME)) $criteria->add(PekerjaanPeer::KOMPONEN_NAME, $this->komponen_name);
		if ($this->isColumnModified(PekerjaanPeer::DETAIL_NAME)) $criteria->add(PekerjaanPeer::DETAIL_NAME, $this->detail_name);
		if ($this->isColumnModified(PekerjaanPeer::KETERANGAN_KOEFISIEN)) $criteria->add(PekerjaanPeer::KETERANGAN_KOEFISIEN, $this->keterangan_koefisien);
		if ($this->isColumnModified(PekerjaanPeer::KECAMATAN)) $criteria->add(PekerjaanPeer::KECAMATAN, $this->kecamatan);
		if ($this->isColumnModified(PekerjaanPeer::KELURAHAN)) $criteria->add(PekerjaanPeer::KELURAHAN, $this->kelurahan);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(PekerjaanPeer::DATABASE_NAME);


		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return null;
	}

	
	 public function setPrimaryKey($pk)
	 {
		 	 }

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setUnitId($this->unit_id);

		$copyObj->setKegiatanCode($this->kegiatan_code);

		$copyObj->setKomponenId($this->komponen_id);

		$copyObj->setUnitName($this->unit_name);

		$copyObj->setKegiatanName($this->kegiatan_name);

		$copyObj->setSubtitle($this->subtitle);

		$copyObj->setKomponenName($this->komponen_name);

		$copyObj->setDetailName($this->detail_name);

		$copyObj->setKeteranganKoefisien($this->keterangan_koefisien);

		$copyObj->setKecamatan($this->kecamatan);

		$copyObj->setKelurahan($this->kelurahan);


		$copyObj->setNew(true);

	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new PekerjaanPeer();
		}
		return self::$peer;
	}

} 