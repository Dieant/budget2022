<?php


abstract class BaseUserLevel extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $level_id;


	
	protected $level_name;

	
	protected $collSchemaAksess;

	
	protected $lastSchemaAksesCriteria = null;

	
	protected $collSchemaAksesV2s;

	
	protected $lastSchemaAksesV2Criteria = null;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getLevelId()
	{

		return $this->level_id;
	}

	
	public function getLevelName()
	{

		return $this->level_name;
	}

	
	public function setLevelId($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->level_id !== $v) {
			$this->level_id = $v;
			$this->modifiedColumns[] = UserLevelPeer::LEVEL_ID;
		}

	} 
	
	public function setLevelName($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->level_name !== $v) {
			$this->level_name = $v;
			$this->modifiedColumns[] = UserLevelPeer::LEVEL_NAME;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->level_id = $rs->getInt($startcol + 0);

			$this->level_name = $rs->getString($startcol + 1);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 2; 
		} catch (Exception $e) {
			throw new PropelException("Error populating UserLevel object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(UserLevelPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			UserLevelPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(UserLevelPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = UserLevelPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += UserLevelPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			if ($this->collSchemaAksess !== null) {
				foreach($this->collSchemaAksess as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			if ($this->collSchemaAksesV2s !== null) {
				foreach($this->collSchemaAksesV2s as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = UserLevelPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}


				if ($this->collSchemaAksess !== null) {
					foreach($this->collSchemaAksess as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}

				if ($this->collSchemaAksesV2s !== null) {
					foreach($this->collSchemaAksesV2s as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}


			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = UserLevelPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getLevelId();
				break;
			case 1:
				return $this->getLevelName();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = UserLevelPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getLevelId(),
			$keys[1] => $this->getLevelName(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = UserLevelPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setLevelId($value);
				break;
			case 1:
				$this->setLevelName($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = UserLevelPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setLevelId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setLevelName($arr[$keys[1]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(UserLevelPeer::DATABASE_NAME);

		if ($this->isColumnModified(UserLevelPeer::LEVEL_ID)) $criteria->add(UserLevelPeer::LEVEL_ID, $this->level_id);
		if ($this->isColumnModified(UserLevelPeer::LEVEL_NAME)) $criteria->add(UserLevelPeer::LEVEL_NAME, $this->level_name);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(UserLevelPeer::DATABASE_NAME);

		$criteria->add(UserLevelPeer::LEVEL_ID, $this->level_id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getLevelId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setLevelId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setLevelName($this->level_name);


		if ($deepCopy) {
									$copyObj->setNew(false);

			foreach($this->getSchemaAksess() as $relObj) {
				$copyObj->addSchemaAkses($relObj->copy($deepCopy));
			}

			foreach($this->getSchemaAksesV2s() as $relObj) {
				$copyObj->addSchemaAksesV2($relObj->copy($deepCopy));
			}

		} 

		$copyObj->setNew(true);

		$copyObj->setLevelId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new UserLevelPeer();
		}
		return self::$peer;
	}

	
	public function initSchemaAksess()
	{
		if ($this->collSchemaAksess === null) {
			$this->collSchemaAksess = array();
		}
	}

	
	public function getSchemaAksess($criteria = null, $con = null)
	{
				include_once 'lib/model/budgeting/om/BaseSchemaAksesPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collSchemaAksess === null) {
			if ($this->isNew()) {
			   $this->collSchemaAksess = array();
			} else {

				$criteria->add(SchemaAksesPeer::LEVEL_ID, $this->getLevelId());

				SchemaAksesPeer::addSelectColumns($criteria);
				$this->collSchemaAksess = SchemaAksesPeer::doSelect($criteria, $con);
			}
		} else {
						if (!$this->isNew()) {
												

				$criteria->add(SchemaAksesPeer::LEVEL_ID, $this->getLevelId());

				SchemaAksesPeer::addSelectColumns($criteria);
				if (!isset($this->lastSchemaAksesCriteria) || !$this->lastSchemaAksesCriteria->equals($criteria)) {
					$this->collSchemaAksess = SchemaAksesPeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastSchemaAksesCriteria = $criteria;
		return $this->collSchemaAksess;
	}

	
	public function countSchemaAksess($criteria = null, $distinct = false, $con = null)
	{
				include_once 'lib/model/budgeting/om/BaseSchemaAksesPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(SchemaAksesPeer::LEVEL_ID, $this->getLevelId());

		return SchemaAksesPeer::doCount($criteria, $distinct, $con);
	}

	
	public function addSchemaAkses(SchemaAkses $l)
	{
		$this->collSchemaAksess[] = $l;
		$l->setUserLevel($this);
	}


	
	public function getSchemaAksessJoinMasterUser($criteria = null, $con = null)
	{
				include_once 'lib/model/budgeting/om/BaseSchemaAksesPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collSchemaAksess === null) {
			if ($this->isNew()) {
				$this->collSchemaAksess = array();
			} else {

				$criteria->add(SchemaAksesPeer::LEVEL_ID, $this->getLevelId());

				$this->collSchemaAksess = SchemaAksesPeer::doSelectJoinMasterUser($criteria, $con);
			}
		} else {
									
			$criteria->add(SchemaAksesPeer::LEVEL_ID, $this->getLevelId());

			if (!isset($this->lastSchemaAksesCriteria) || !$this->lastSchemaAksesCriteria->equals($criteria)) {
				$this->collSchemaAksess = SchemaAksesPeer::doSelectJoinMasterUser($criteria, $con);
			}
		}
		$this->lastSchemaAksesCriteria = $criteria;

		return $this->collSchemaAksess;
	}


	
	public function getSchemaAksessJoinMasterSchema($criteria = null, $con = null)
	{
				include_once 'lib/model/budgeting/om/BaseSchemaAksesPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collSchemaAksess === null) {
			if ($this->isNew()) {
				$this->collSchemaAksess = array();
			} else {

				$criteria->add(SchemaAksesPeer::LEVEL_ID, $this->getLevelId());

				$this->collSchemaAksess = SchemaAksesPeer::doSelectJoinMasterSchema($criteria, $con);
			}
		} else {
									
			$criteria->add(SchemaAksesPeer::LEVEL_ID, $this->getLevelId());

			if (!isset($this->lastSchemaAksesCriteria) || !$this->lastSchemaAksesCriteria->equals($criteria)) {
				$this->collSchemaAksess = SchemaAksesPeer::doSelectJoinMasterSchema($criteria, $con);
			}
		}
		$this->lastSchemaAksesCriteria = $criteria;

		return $this->collSchemaAksess;
	}

	
	public function initSchemaAksesV2s()
	{
		if ($this->collSchemaAksesV2s === null) {
			$this->collSchemaAksesV2s = array();
		}
	}

	
	public function getSchemaAksesV2s($criteria = null, $con = null)
	{
				include_once 'lib/model/budgeting/om/BaseSchemaAksesV2Peer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collSchemaAksesV2s === null) {
			if ($this->isNew()) {
			   $this->collSchemaAksesV2s = array();
			} else {

				$criteria->add(SchemaAksesV2Peer::LEVEL_ID, $this->getLevelId());

				SchemaAksesV2Peer::addSelectColumns($criteria);
				$this->collSchemaAksesV2s = SchemaAksesV2Peer::doSelect($criteria, $con);
			}
		} else {
						if (!$this->isNew()) {
												

				$criteria->add(SchemaAksesV2Peer::LEVEL_ID, $this->getLevelId());

				SchemaAksesV2Peer::addSelectColumns($criteria);
				if (!isset($this->lastSchemaAksesV2Criteria) || !$this->lastSchemaAksesV2Criteria->equals($criteria)) {
					$this->collSchemaAksesV2s = SchemaAksesV2Peer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastSchemaAksesV2Criteria = $criteria;
		return $this->collSchemaAksesV2s;
	}

	
	public function countSchemaAksesV2s($criteria = null, $distinct = false, $con = null)
	{
				include_once 'lib/model/budgeting/om/BaseSchemaAksesV2Peer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(SchemaAksesV2Peer::LEVEL_ID, $this->getLevelId());

		return SchemaAksesV2Peer::doCount($criteria, $distinct, $con);
	}

	
	public function addSchemaAksesV2(SchemaAksesV2 $l)
	{
		$this->collSchemaAksesV2s[] = $l;
		$l->setUserLevel($this);
	}


	
	public function getSchemaAksesV2sJoinMasterUserV2($criteria = null, $con = null)
	{
				include_once 'lib/model/budgeting/om/BaseSchemaAksesV2Peer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collSchemaAksesV2s === null) {
			if ($this->isNew()) {
				$this->collSchemaAksesV2s = array();
			} else {

				$criteria->add(SchemaAksesV2Peer::LEVEL_ID, $this->getLevelId());

				$this->collSchemaAksesV2s = SchemaAksesV2Peer::doSelectJoinMasterUserV2($criteria, $con);
			}
		} else {
									
			$criteria->add(SchemaAksesV2Peer::LEVEL_ID, $this->getLevelId());

			if (!isset($this->lastSchemaAksesV2Criteria) || !$this->lastSchemaAksesV2Criteria->equals($criteria)) {
				$this->collSchemaAksesV2s = SchemaAksesV2Peer::doSelectJoinMasterUserV2($criteria, $con);
			}
		}
		$this->lastSchemaAksesV2Criteria = $criteria;

		return $this->collSchemaAksesV2s;
	}


	
	public function getSchemaAksesV2sJoinMasterSchema($criteria = null, $con = null)
	{
				include_once 'lib/model/budgeting/om/BaseSchemaAksesV2Peer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collSchemaAksesV2s === null) {
			if ($this->isNew()) {
				$this->collSchemaAksesV2s = array();
			} else {

				$criteria->add(SchemaAksesV2Peer::LEVEL_ID, $this->getLevelId());

				$this->collSchemaAksesV2s = SchemaAksesV2Peer::doSelectJoinMasterSchema($criteria, $con);
			}
		} else {
									
			$criteria->add(SchemaAksesV2Peer::LEVEL_ID, $this->getLevelId());

			if (!isset($this->lastSchemaAksesV2Criteria) || !$this->lastSchemaAksesV2Criteria->equals($criteria)) {
				$this->collSchemaAksesV2s = SchemaAksesV2Peer::doSelectJoinMasterSchema($criteria, $con);
			}
		}
		$this->lastSchemaAksesV2Criteria = $criteria;

		return $this->collSchemaAksesV2s;
	}

} 