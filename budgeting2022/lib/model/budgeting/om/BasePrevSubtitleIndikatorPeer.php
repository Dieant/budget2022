<?php


abstract class BasePrevSubtitleIndikatorPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.prev_subtitle_indikator';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.PrevSubtitleIndikator';

	
	const NUM_COLUMNS = 15;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const UNIT_ID = 'ebudget.prev_subtitle_indikator.UNIT_ID';

	
	const KEGIATAN_CODE = 'ebudget.prev_subtitle_indikator.KEGIATAN_CODE';

	
	const SUBTITLE = 'ebudget.prev_subtitle_indikator.SUBTITLE';

	
	const INDIKATOR = 'ebudget.prev_subtitle_indikator.INDIKATOR';

	
	const NILAI = 'ebudget.prev_subtitle_indikator.NILAI';

	
	const SATUAN = 'ebudget.prev_subtitle_indikator.SATUAN';

	
	const LAST_UPDATE_USER = 'ebudget.prev_subtitle_indikator.LAST_UPDATE_USER';

	
	const LAST_UPDATE_TIME = 'ebudget.prev_subtitle_indikator.LAST_UPDATE_TIME';

	
	const LAST_UPDATE_IP = 'ebudget.prev_subtitle_indikator.LAST_UPDATE_IP';

	
	const TAHAP = 'ebudget.prev_subtitle_indikator.TAHAP';

	
	const SUB_ID = 'ebudget.prev_subtitle_indikator.SUB_ID';

	
	const TAHUN = 'ebudget.prev_subtitle_indikator.TAHUN';

	
	const LOCK_SUBTITLE = 'ebudget.prev_subtitle_indikator.LOCK_SUBTITLE';

	
	const PRIORITAS = 'ebudget.prev_subtitle_indikator.PRIORITAS';

	
	const GENDER = 'ebudget.prev_subtitle_indikator.GENDER';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('UnitId', 'KegiatanCode', 'Subtitle', 'Indikator', 'Nilai', 'Satuan', 'LastUpdateUser', 'LastUpdateTime', 'LastUpdateIp', 'Tahap', 'SubId', 'Tahun', 'LockSubtitle', 'Prioritas', 'Gender', ),
		BasePeer::TYPE_COLNAME => array (PrevSubtitleIndikatorPeer::UNIT_ID, PrevSubtitleIndikatorPeer::KEGIATAN_CODE, PrevSubtitleIndikatorPeer::SUBTITLE, PrevSubtitleIndikatorPeer::INDIKATOR, PrevSubtitleIndikatorPeer::NILAI, PrevSubtitleIndikatorPeer::SATUAN, PrevSubtitleIndikatorPeer::LAST_UPDATE_USER, PrevSubtitleIndikatorPeer::LAST_UPDATE_TIME, PrevSubtitleIndikatorPeer::LAST_UPDATE_IP, PrevSubtitleIndikatorPeer::TAHAP, PrevSubtitleIndikatorPeer::SUB_ID, PrevSubtitleIndikatorPeer::TAHUN, PrevSubtitleIndikatorPeer::LOCK_SUBTITLE, PrevSubtitleIndikatorPeer::PRIORITAS, PrevSubtitleIndikatorPeer::GENDER, ),
		BasePeer::TYPE_FIELDNAME => array ('unit_id', 'kegiatan_code', 'subtitle', 'indikator', 'nilai', 'satuan', 'last_update_user', 'last_update_time', 'last_update_ip', 'tahap', 'sub_id', 'tahun', 'lock_subtitle', 'prioritas', 'gender', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('UnitId' => 0, 'KegiatanCode' => 1, 'Subtitle' => 2, 'Indikator' => 3, 'Nilai' => 4, 'Satuan' => 5, 'LastUpdateUser' => 6, 'LastUpdateTime' => 7, 'LastUpdateIp' => 8, 'Tahap' => 9, 'SubId' => 10, 'Tahun' => 11, 'LockSubtitle' => 12, 'Prioritas' => 13, 'Gender' => 14, ),
		BasePeer::TYPE_COLNAME => array (PrevSubtitleIndikatorPeer::UNIT_ID => 0, PrevSubtitleIndikatorPeer::KEGIATAN_CODE => 1, PrevSubtitleIndikatorPeer::SUBTITLE => 2, PrevSubtitleIndikatorPeer::INDIKATOR => 3, PrevSubtitleIndikatorPeer::NILAI => 4, PrevSubtitleIndikatorPeer::SATUAN => 5, PrevSubtitleIndikatorPeer::LAST_UPDATE_USER => 6, PrevSubtitleIndikatorPeer::LAST_UPDATE_TIME => 7, PrevSubtitleIndikatorPeer::LAST_UPDATE_IP => 8, PrevSubtitleIndikatorPeer::TAHAP => 9, PrevSubtitleIndikatorPeer::SUB_ID => 10, PrevSubtitleIndikatorPeer::TAHUN => 11, PrevSubtitleIndikatorPeer::LOCK_SUBTITLE => 12, PrevSubtitleIndikatorPeer::PRIORITAS => 13, PrevSubtitleIndikatorPeer::GENDER => 14, ),
		BasePeer::TYPE_FIELDNAME => array ('unit_id' => 0, 'kegiatan_code' => 1, 'subtitle' => 2, 'indikator' => 3, 'nilai' => 4, 'satuan' => 5, 'last_update_user' => 6, 'last_update_time' => 7, 'last_update_ip' => 8, 'tahap' => 9, 'sub_id' => 10, 'tahun' => 11, 'lock_subtitle' => 12, 'prioritas' => 13, 'gender' => 14, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/PrevSubtitleIndikatorMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.PrevSubtitleIndikatorMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = PrevSubtitleIndikatorPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(PrevSubtitleIndikatorPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(PrevSubtitleIndikatorPeer::UNIT_ID);

		$criteria->addSelectColumn(PrevSubtitleIndikatorPeer::KEGIATAN_CODE);

		$criteria->addSelectColumn(PrevSubtitleIndikatorPeer::SUBTITLE);

		$criteria->addSelectColumn(PrevSubtitleIndikatorPeer::INDIKATOR);

		$criteria->addSelectColumn(PrevSubtitleIndikatorPeer::NILAI);

		$criteria->addSelectColumn(PrevSubtitleIndikatorPeer::SATUAN);

		$criteria->addSelectColumn(PrevSubtitleIndikatorPeer::LAST_UPDATE_USER);

		$criteria->addSelectColumn(PrevSubtitleIndikatorPeer::LAST_UPDATE_TIME);

		$criteria->addSelectColumn(PrevSubtitleIndikatorPeer::LAST_UPDATE_IP);

		$criteria->addSelectColumn(PrevSubtitleIndikatorPeer::TAHAP);

		$criteria->addSelectColumn(PrevSubtitleIndikatorPeer::SUB_ID);

		$criteria->addSelectColumn(PrevSubtitleIndikatorPeer::TAHUN);

		$criteria->addSelectColumn(PrevSubtitleIndikatorPeer::LOCK_SUBTITLE);

		$criteria->addSelectColumn(PrevSubtitleIndikatorPeer::PRIORITAS);

		$criteria->addSelectColumn(PrevSubtitleIndikatorPeer::GENDER);

	}

	const COUNT = 'COUNT(ebudget.prev_subtitle_indikator.SUB_ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.prev_subtitle_indikator.SUB_ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(PrevSubtitleIndikatorPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(PrevSubtitleIndikatorPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = PrevSubtitleIndikatorPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = PrevSubtitleIndikatorPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return PrevSubtitleIndikatorPeer::populateObjects(PrevSubtitleIndikatorPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			PrevSubtitleIndikatorPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = PrevSubtitleIndikatorPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return PrevSubtitleIndikatorPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}

		$criteria->remove(PrevSubtitleIndikatorPeer::SUB_ID); 

				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(PrevSubtitleIndikatorPeer::SUB_ID);
			$selectCriteria->add(PrevSubtitleIndikatorPeer::SUB_ID, $criteria->remove(PrevSubtitleIndikatorPeer::SUB_ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(PrevSubtitleIndikatorPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(PrevSubtitleIndikatorPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof PrevSubtitleIndikator) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(PrevSubtitleIndikatorPeer::SUB_ID, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(PrevSubtitleIndikator $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(PrevSubtitleIndikatorPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(PrevSubtitleIndikatorPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(PrevSubtitleIndikatorPeer::DATABASE_NAME, PrevSubtitleIndikatorPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = PrevSubtitleIndikatorPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(PrevSubtitleIndikatorPeer::DATABASE_NAME);

		$criteria->add(PrevSubtitleIndikatorPeer::SUB_ID, $pk);


		$v = PrevSubtitleIndikatorPeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(PrevSubtitleIndikatorPeer::SUB_ID, $pks, Criteria::IN);
			$objs = PrevSubtitleIndikatorPeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BasePrevSubtitleIndikatorPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/PrevSubtitleIndikatorMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.PrevSubtitleIndikatorMapBuilder');
}
