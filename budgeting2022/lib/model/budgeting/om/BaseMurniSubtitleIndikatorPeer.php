<?php


abstract class BaseMurniSubtitleIndikatorPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.murni_subtitle_indikator';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.MurniSubtitleIndikator';

	
	const NUM_COLUMNS = 26;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const UNIT_ID = 'ebudget.murni_subtitle_indikator.UNIT_ID';

	
	const KEGIATAN_CODE = 'ebudget.murni_subtitle_indikator.KEGIATAN_CODE';

	
	const SUBTITLE = 'ebudget.murni_subtitle_indikator.SUBTITLE';

	
	const INDIKATOR = 'ebudget.murni_subtitle_indikator.INDIKATOR';

	
	const NILAI = 'ebudget.murni_subtitle_indikator.NILAI';

	
	const SATUAN = 'ebudget.murni_subtitle_indikator.SATUAN';

	
	const LAST_UPDATE_USER = 'ebudget.murni_subtitle_indikator.LAST_UPDATE_USER';

	
	const LAST_UPDATE_TIME = 'ebudget.murni_subtitle_indikator.LAST_UPDATE_TIME';

	
	const LAST_UPDATE_IP = 'ebudget.murni_subtitle_indikator.LAST_UPDATE_IP';

	
	const TAHAP = 'ebudget.murni_subtitle_indikator.TAHAP';

	
	const SUB_ID = 'ebudget.murni_subtitle_indikator.SUB_ID';

	
	const TAHUN = 'ebudget.murni_subtitle_indikator.TAHUN';

	
	const LOCK_SUBTITLE = 'ebudget.murni_subtitle_indikator.LOCK_SUBTITLE';

	
	const PRIORITAS = 'ebudget.murni_subtitle_indikator.PRIORITAS';

	
	const CATATAN = 'ebudget.murni_subtitle_indikator.CATATAN';

	
	const LAKILAKI = 'ebudget.murni_subtitle_indikator.LAKILAKI';

	
	const PEREMPUAN = 'ebudget.murni_subtitle_indikator.PEREMPUAN';

	
	const DEWASA = 'ebudget.murni_subtitle_indikator.DEWASA';

	
	const ANAK = 'ebudget.murni_subtitle_indikator.ANAK';

	
	const LANSIA = 'ebudget.murni_subtitle_indikator.LANSIA';

	
	const INKLUSI = 'ebudget.murni_subtitle_indikator.INKLUSI';

	
	const GENDER = 'ebudget.murni_subtitle_indikator.GENDER';

	
	const IS_NOL_ANGGARAN = 'ebudget.murni_subtitle_indikator.IS_NOL_ANGGARAN';

	
	const INDIKATOR_CM = 'ebudget.murni_subtitle_indikator.INDIKATOR_CM';

	
	const NILAI_CM = 'ebudget.murni_subtitle_indikator.NILAI_CM';

	
	const SATUAN_CM = 'ebudget.murni_subtitle_indikator.SATUAN_CM';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('UnitId', 'KegiatanCode', 'Subtitle', 'Indikator', 'Nilai', 'Satuan', 'LastUpdateUser', 'LastUpdateTime', 'LastUpdateIp', 'Tahap', 'SubId', 'Tahun', 'LockSubtitle', 'Prioritas', 'Catatan', 'Lakilaki', 'Perempuan', 'Dewasa', 'Anak', 'Lansia', 'Inklusi', 'Gender', 'IsNolAnggaran', 'IndikatorCm', 'NilaiCm', 'SatuanCm', ),
		BasePeer::TYPE_COLNAME => array (MurniSubtitleIndikatorPeer::UNIT_ID, MurniSubtitleIndikatorPeer::KEGIATAN_CODE, MurniSubtitleIndikatorPeer::SUBTITLE, MurniSubtitleIndikatorPeer::INDIKATOR, MurniSubtitleIndikatorPeer::NILAI, MurniSubtitleIndikatorPeer::SATUAN, MurniSubtitleIndikatorPeer::LAST_UPDATE_USER, MurniSubtitleIndikatorPeer::LAST_UPDATE_TIME, MurniSubtitleIndikatorPeer::LAST_UPDATE_IP, MurniSubtitleIndikatorPeer::TAHAP, MurniSubtitleIndikatorPeer::SUB_ID, MurniSubtitleIndikatorPeer::TAHUN, MurniSubtitleIndikatorPeer::LOCK_SUBTITLE, MurniSubtitleIndikatorPeer::PRIORITAS, MurniSubtitleIndikatorPeer::CATATAN, MurniSubtitleIndikatorPeer::LAKILAKI, MurniSubtitleIndikatorPeer::PEREMPUAN, MurniSubtitleIndikatorPeer::DEWASA, MurniSubtitleIndikatorPeer::ANAK, MurniSubtitleIndikatorPeer::LANSIA, MurniSubtitleIndikatorPeer::INKLUSI, MurniSubtitleIndikatorPeer::GENDER, MurniSubtitleIndikatorPeer::IS_NOL_ANGGARAN, MurniSubtitleIndikatorPeer::INDIKATOR_CM, MurniSubtitleIndikatorPeer::NILAI_CM, MurniSubtitleIndikatorPeer::SATUAN_CM, ),
		BasePeer::TYPE_FIELDNAME => array ('unit_id', 'kegiatan_code', 'subtitle', 'indikator', 'nilai', 'satuan', 'last_update_user', 'last_update_time', 'last_update_ip', 'tahap', 'sub_id', 'tahun', 'lock_subtitle', 'prioritas', 'catatan', 'lakilaki', 'perempuan', 'dewasa', 'anak', 'lansia', 'inklusi', 'gender', 'is_nol_anggaran', 'indikator_cm', 'nilai_cm', 'satuan_cm', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('UnitId' => 0, 'KegiatanCode' => 1, 'Subtitle' => 2, 'Indikator' => 3, 'Nilai' => 4, 'Satuan' => 5, 'LastUpdateUser' => 6, 'LastUpdateTime' => 7, 'LastUpdateIp' => 8, 'Tahap' => 9, 'SubId' => 10, 'Tahun' => 11, 'LockSubtitle' => 12, 'Prioritas' => 13, 'Catatan' => 14, 'Lakilaki' => 15, 'Perempuan' => 16, 'Dewasa' => 17, 'Anak' => 18, 'Lansia' => 19, 'Inklusi' => 20, 'Gender' => 21, 'IsNolAnggaran' => 22, 'IndikatorCm' => 23, 'NilaiCm' => 24, 'SatuanCm' => 25, ),
		BasePeer::TYPE_COLNAME => array (MurniSubtitleIndikatorPeer::UNIT_ID => 0, MurniSubtitleIndikatorPeer::KEGIATAN_CODE => 1, MurniSubtitleIndikatorPeer::SUBTITLE => 2, MurniSubtitleIndikatorPeer::INDIKATOR => 3, MurniSubtitleIndikatorPeer::NILAI => 4, MurniSubtitleIndikatorPeer::SATUAN => 5, MurniSubtitleIndikatorPeer::LAST_UPDATE_USER => 6, MurniSubtitleIndikatorPeer::LAST_UPDATE_TIME => 7, MurniSubtitleIndikatorPeer::LAST_UPDATE_IP => 8, MurniSubtitleIndikatorPeer::TAHAP => 9, MurniSubtitleIndikatorPeer::SUB_ID => 10, MurniSubtitleIndikatorPeer::TAHUN => 11, MurniSubtitleIndikatorPeer::LOCK_SUBTITLE => 12, MurniSubtitleIndikatorPeer::PRIORITAS => 13, MurniSubtitleIndikatorPeer::CATATAN => 14, MurniSubtitleIndikatorPeer::LAKILAKI => 15, MurniSubtitleIndikatorPeer::PEREMPUAN => 16, MurniSubtitleIndikatorPeer::DEWASA => 17, MurniSubtitleIndikatorPeer::ANAK => 18, MurniSubtitleIndikatorPeer::LANSIA => 19, MurniSubtitleIndikatorPeer::INKLUSI => 20, MurniSubtitleIndikatorPeer::GENDER => 21, MurniSubtitleIndikatorPeer::IS_NOL_ANGGARAN => 22, MurniSubtitleIndikatorPeer::INDIKATOR_CM => 23, MurniSubtitleIndikatorPeer::NILAI_CM => 24, MurniSubtitleIndikatorPeer::SATUAN_CM => 25, ),
		BasePeer::TYPE_FIELDNAME => array ('unit_id' => 0, 'kegiatan_code' => 1, 'subtitle' => 2, 'indikator' => 3, 'nilai' => 4, 'satuan' => 5, 'last_update_user' => 6, 'last_update_time' => 7, 'last_update_ip' => 8, 'tahap' => 9, 'sub_id' => 10, 'tahun' => 11, 'lock_subtitle' => 12, 'prioritas' => 13, 'catatan' => 14, 'lakilaki' => 15, 'perempuan' => 16, 'dewasa' => 17, 'anak' => 18, 'lansia' => 19, 'inklusi' => 20, 'gender' => 21, 'is_nol_anggaran' => 22, 'indikator_cm' => 23, 'nilai_cm' => 24, 'satuan_cm' => 25, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/MurniSubtitleIndikatorMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.MurniSubtitleIndikatorMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = MurniSubtitleIndikatorPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(MurniSubtitleIndikatorPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(MurniSubtitleIndikatorPeer::UNIT_ID);

		$criteria->addSelectColumn(MurniSubtitleIndikatorPeer::KEGIATAN_CODE);

		$criteria->addSelectColumn(MurniSubtitleIndikatorPeer::SUBTITLE);

		$criteria->addSelectColumn(MurniSubtitleIndikatorPeer::INDIKATOR);

		$criteria->addSelectColumn(MurniSubtitleIndikatorPeer::NILAI);

		$criteria->addSelectColumn(MurniSubtitleIndikatorPeer::SATUAN);

		$criteria->addSelectColumn(MurniSubtitleIndikatorPeer::LAST_UPDATE_USER);

		$criteria->addSelectColumn(MurniSubtitleIndikatorPeer::LAST_UPDATE_TIME);

		$criteria->addSelectColumn(MurniSubtitleIndikatorPeer::LAST_UPDATE_IP);

		$criteria->addSelectColumn(MurniSubtitleIndikatorPeer::TAHAP);

		$criteria->addSelectColumn(MurniSubtitleIndikatorPeer::SUB_ID);

		$criteria->addSelectColumn(MurniSubtitleIndikatorPeer::TAHUN);

		$criteria->addSelectColumn(MurniSubtitleIndikatorPeer::LOCK_SUBTITLE);

		$criteria->addSelectColumn(MurniSubtitleIndikatorPeer::PRIORITAS);

		$criteria->addSelectColumn(MurniSubtitleIndikatorPeer::CATATAN);

		$criteria->addSelectColumn(MurniSubtitleIndikatorPeer::LAKILAKI);

		$criteria->addSelectColumn(MurniSubtitleIndikatorPeer::PEREMPUAN);

		$criteria->addSelectColumn(MurniSubtitleIndikatorPeer::DEWASA);

		$criteria->addSelectColumn(MurniSubtitleIndikatorPeer::ANAK);

		$criteria->addSelectColumn(MurniSubtitleIndikatorPeer::LANSIA);

		$criteria->addSelectColumn(MurniSubtitleIndikatorPeer::INKLUSI);

		$criteria->addSelectColumn(MurniSubtitleIndikatorPeer::GENDER);

		$criteria->addSelectColumn(MurniSubtitleIndikatorPeer::IS_NOL_ANGGARAN);

		$criteria->addSelectColumn(MurniSubtitleIndikatorPeer::INDIKATOR_CM);

		$criteria->addSelectColumn(MurniSubtitleIndikatorPeer::NILAI_CM);

		$criteria->addSelectColumn(MurniSubtitleIndikatorPeer::SATUAN_CM);

	}

	const COUNT = 'COUNT(ebudget.murni_subtitle_indikator.SUB_ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.murni_subtitle_indikator.SUB_ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(MurniSubtitleIndikatorPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(MurniSubtitleIndikatorPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = MurniSubtitleIndikatorPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = MurniSubtitleIndikatorPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return MurniSubtitleIndikatorPeer::populateObjects(MurniSubtitleIndikatorPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			MurniSubtitleIndikatorPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = MurniSubtitleIndikatorPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return MurniSubtitleIndikatorPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}

		$criteria->remove(MurniSubtitleIndikatorPeer::SUB_ID); 

				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(MurniSubtitleIndikatorPeer::SUB_ID);
			$selectCriteria->add(MurniSubtitleIndikatorPeer::SUB_ID, $criteria->remove(MurniSubtitleIndikatorPeer::SUB_ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(MurniSubtitleIndikatorPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(MurniSubtitleIndikatorPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof MurniSubtitleIndikator) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(MurniSubtitleIndikatorPeer::SUB_ID, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(MurniSubtitleIndikator $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(MurniSubtitleIndikatorPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(MurniSubtitleIndikatorPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		return BasePeer::doValidate(MurniSubtitleIndikatorPeer::DATABASE_NAME, MurniSubtitleIndikatorPeer::TABLE_NAME, $columns);
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(MurniSubtitleIndikatorPeer::DATABASE_NAME);

		$criteria->add(MurniSubtitleIndikatorPeer::SUB_ID, $pk);


		$v = MurniSubtitleIndikatorPeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(MurniSubtitleIndikatorPeer::SUB_ID, $pks, Criteria::IN);
			$objs = MurniSubtitleIndikatorPeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BaseMurniSubtitleIndikatorPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/MurniSubtitleIndikatorMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.MurniSubtitleIndikatorMapBuilder');
}
