<?php


abstract class BaseDinasMasterKegiatanPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.dinas_master_kegiatan';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.DinasMasterKegiatan';

	
	const NUM_COLUMNS = 104;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const UNIT_ID = 'ebudget.dinas_master_kegiatan.UNIT_ID';

	
	const KODE_KEGIATAN = 'ebudget.dinas_master_kegiatan.KODE_KEGIATAN';

	
	const KODE_BIDANG = 'ebudget.dinas_master_kegiatan.KODE_BIDANG';

	
	const KODE_URUSAN_WAJIB = 'ebudget.dinas_master_kegiatan.KODE_URUSAN_WAJIB';

	
	const KODE_PROGRAM = 'ebudget.dinas_master_kegiatan.KODE_PROGRAM';

	
	const KODE_SASARAN = 'ebudget.dinas_master_kegiatan.KODE_SASARAN';

	
	const KODE_INDIKATOR = 'ebudget.dinas_master_kegiatan.KODE_INDIKATOR';

	
	const ALOKASI_DANA = 'ebudget.dinas_master_kegiatan.ALOKASI_DANA';

	
	const NAMA_KEGIATAN = 'ebudget.dinas_master_kegiatan.NAMA_KEGIATAN';

	
	const MASUKAN = 'ebudget.dinas_master_kegiatan.MASUKAN';

	
	const OUTPUT = 'ebudget.dinas_master_kegiatan.OUTPUT';

	
	const OUTCOME = 'ebudget.dinas_master_kegiatan.OUTCOME';

	
	const BENEFIT = 'ebudget.dinas_master_kegiatan.BENEFIT';

	
	const IMPACT = 'ebudget.dinas_master_kegiatan.IMPACT';

	
	const TIPE = 'ebudget.dinas_master_kegiatan.TIPE';

	
	const KEGIATAN_ACTIVE = 'ebudget.dinas_master_kegiatan.KEGIATAN_ACTIVE';

	
	const TO_KEGIATAN_CODE = 'ebudget.dinas_master_kegiatan.TO_KEGIATAN_CODE';

	
	const CATATAN = 'ebudget.dinas_master_kegiatan.CATATAN';

	
	const TARGET_OUTCOME = 'ebudget.dinas_master_kegiatan.TARGET_OUTCOME';

	
	const LOKASI = 'ebudget.dinas_master_kegiatan.LOKASI';

	
	const JUMLAH_PREV = 'ebudget.dinas_master_kegiatan.JUMLAH_PREV';

	
	const JUMLAH_NOW = 'ebudget.dinas_master_kegiatan.JUMLAH_NOW';

	
	const JUMLAH_NEXT = 'ebudget.dinas_master_kegiatan.JUMLAH_NEXT';

	
	const KODE_PROGRAM2 = 'ebudget.dinas_master_kegiatan.KODE_PROGRAM2';

	
	const KODE_URUSAN = 'ebudget.dinas_master_kegiatan.KODE_URUSAN';

	
	const LAST_UPDATE_USER = 'ebudget.dinas_master_kegiatan.LAST_UPDATE_USER';

	
	const LAST_UPDATE_TIME = 'ebudget.dinas_master_kegiatan.LAST_UPDATE_TIME';

	
	const LAST_UPDATE_IP = 'ebudget.dinas_master_kegiatan.LAST_UPDATE_IP';

	
	const TAHAP = 'ebudget.dinas_master_kegiatan.TAHAP';

	
	const KODE_MISI = 'ebudget.dinas_master_kegiatan.KODE_MISI';

	
	const KODE_TUJUAN = 'ebudget.dinas_master_kegiatan.KODE_TUJUAN';

	
	const RANKING = 'ebudget.dinas_master_kegiatan.RANKING';

	
	const NOMOR13 = 'ebudget.dinas_master_kegiatan.NOMOR13';

	
	const PPA_NAMA = 'ebudget.dinas_master_kegiatan.PPA_NAMA';

	
	const PPA_PANGKAT = 'ebudget.dinas_master_kegiatan.PPA_PANGKAT';

	
	const PPA_NIP = 'ebudget.dinas_master_kegiatan.PPA_NIP';

	
	const LANJUTAN = 'ebudget.dinas_master_kegiatan.LANJUTAN';

	
	const USER_ID = 'ebudget.dinas_master_kegiatan.USER_ID';

	
	const ID = 'ebudget.dinas_master_kegiatan.ID';

	
	const TAHUN = 'ebudget.dinas_master_kegiatan.TAHUN';

	
	const TAMBAHAN_PAGU = 'ebudget.dinas_master_kegiatan.TAMBAHAN_PAGU';

	
	const GENDER = 'ebudget.dinas_master_kegiatan.GENDER';

	
	const KODE_KEG_KEUANGAN = 'ebudget.dinas_master_kegiatan.KODE_KEG_KEUANGAN';

	
	const USER_ID_LAMA = 'ebudget.dinas_master_kegiatan.USER_ID_LAMA';

	
	const INDIKATOR = 'ebudget.dinas_master_kegiatan.INDIKATOR';

	
	const IS_DAK = 'ebudget.dinas_master_kegiatan.IS_DAK';

	
	const KODE_KEGIATAN_ASAL = 'ebudget.dinas_master_kegiatan.KODE_KEGIATAN_ASAL';

	
	const KODE_KEG_KEUANGAN_ASAL = 'ebudget.dinas_master_kegiatan.KODE_KEG_KEUANGAN_ASAL';

	
	const TH_KE_MULTIYEARS = 'ebudget.dinas_master_kegiatan.TH_KE_MULTIYEARS';

	
	const KELOMPOK_SASARAN = 'ebudget.dinas_master_kegiatan.KELOMPOK_SASARAN';

	
	const PAGU_BAPPEKO = 'ebudget.dinas_master_kegiatan.PAGU_BAPPEKO';

	
	const KODE_DPA = 'ebudget.dinas_master_kegiatan.KODE_DPA';

	
	const USER_ID_PPTK = 'ebudget.dinas_master_kegiatan.USER_ID_PPTK';

	
	const USER_ID_KPA = 'ebudget.dinas_master_kegiatan.USER_ID_KPA';

	
	const CATATAN_PEMBAHASAN = 'ebudget.dinas_master_kegiatan.CATATAN_PEMBAHASAN';

	
	const CATATAN_PENYELIA = 'ebudget.dinas_master_kegiatan.CATATAN_PENYELIA';

	
	const CATATAN_BAPPEKO = 'ebudget.dinas_master_kegiatan.CATATAN_BAPPEKO';

	
	const STATUS_LEVEL = 'ebudget.dinas_master_kegiatan.STATUS_LEVEL';

	
	const IS_TAPD_SETUJU = 'ebudget.dinas_master_kegiatan.IS_TAPD_SETUJU';

	
	const IS_BAPPEKO_SETUJU = 'ebudget.dinas_master_kegiatan.IS_BAPPEKO_SETUJU';

	
	const IS_PENYELIA_SETUJU = 'ebudget.dinas_master_kegiatan.IS_PENYELIA_SETUJU';

	
	const IS_PERNAH_RKA = 'ebudget.dinas_master_kegiatan.IS_PERNAH_RKA';

	
	const KODE_KEGIATAN_BARU = 'ebudget.dinas_master_kegiatan.KODE_KEGIATAN_BARU';

	
	const CATATAN_BPKPD = 'ebudget.dinas_master_kegiatan.CATATAN_BPKPD';

	
	const UBAH_F1_DINAS = 'ebudget.dinas_master_kegiatan.UBAH_F1_DINAS';

	
	const UBAH_F1_PENELITI = 'ebudget.dinas_master_kegiatan.UBAH_F1_PENELITI';

	
	const SISA_LELANG_DINAS = 'ebudget.dinas_master_kegiatan.SISA_LELANG_DINAS';

	
	const SISA_LELANG_PENELITI = 'ebudget.dinas_master_kegiatan.SISA_LELANG_PENELITI';

	
	const CATATAN_UBAH_F1_DINAS = 'ebudget.dinas_master_kegiatan.CATATAN_UBAH_F1_DINAS';

	
	const CATATAN_SISA_LELANG_PENELITI = 'ebudget.dinas_master_kegiatan.CATATAN_SISA_LELANG_PENELITI';

	
	const PPTK_APPROVAL = 'ebudget.dinas_master_kegiatan.PPTK_APPROVAL';

	
	const KPA_APPROVAL = 'ebudget.dinas_master_kegiatan.KPA_APPROVAL';

	
	const CATATAN_BAGIAN_HUKUM = 'ebudget.dinas_master_kegiatan.CATATAN_BAGIAN_HUKUM';

	
	const CATATAN_INSPEKTORAT = 'ebudget.dinas_master_kegiatan.CATATAN_INSPEKTORAT';

	
	const CATATAN_BADAN_KEPEGAWAIAN = 'ebudget.dinas_master_kegiatan.CATATAN_BADAN_KEPEGAWAIAN';

	
	const CATATAN_LPPA = 'ebudget.dinas_master_kegiatan.CATATAN_LPPA';

	
	const IS_BAGIAN_HUKUM_SETUJU = 'ebudget.dinas_master_kegiatan.IS_BAGIAN_HUKUM_SETUJU';

	
	const IS_INSPEKTORAT_SETUJU = 'ebudget.dinas_master_kegiatan.IS_INSPEKTORAT_SETUJU';

	
	const IS_BADAN_KEPEGAWAIAN_SETUJU = 'ebudget.dinas_master_kegiatan.IS_BADAN_KEPEGAWAIAN_SETUJU';

	
	const IS_LPPA_SETUJU = 'ebudget.dinas_master_kegiatan.IS_LPPA_SETUJU';

	
	const VERIFIKASI_BPKPD = 'ebudget.dinas_master_kegiatan.VERIFIKASI_BPKPD';

	
	const VERIFIKASI_BAPPEKO = 'ebudget.dinas_master_kegiatan.VERIFIKASI_BAPPEKO';

	
	const VERIFIKASI_PENYELIA = 'ebudget.dinas_master_kegiatan.VERIFIKASI_PENYELIA';

	
	const VERIFIKASI_BAGIAN_HUKUM = 'ebudget.dinas_master_kegiatan.VERIFIKASI_BAGIAN_HUKUM';

	
	const VERIFIKASI_INSPEKTORAT = 'ebudget.dinas_master_kegiatan.VERIFIKASI_INSPEKTORAT';

	
	const VERIFIKASI_BADAN_KEPEGAWAIAN = 'ebudget.dinas_master_kegiatan.VERIFIKASI_BADAN_KEPEGAWAIAN';

	
	const VERIFIKASI_LPPA = 'ebudget.dinas_master_kegiatan.VERIFIKASI_LPPA';

	
	const METODE_COUNT = 'ebudget.dinas_master_kegiatan.METODE_COUNT';

	
	const CATATAN_BAGIAN_ORGANISASI = 'ebudget.dinas_master_kegiatan.CATATAN_BAGIAN_ORGANISASI';

	
	const IS_BAGIAN_ORGANISASI_SETUJU = 'ebudget.dinas_master_kegiatan.IS_BAGIAN_ORGANISASI_SETUJU';

	
	const VERIFIKASI_BAGIAN_ORGANISASI = 'ebudget.dinas_master_kegiatan.VERIFIKASI_BAGIAN_ORGANISASI';

	
	const CATATAN_ASISTEN1 = 'ebudget.dinas_master_kegiatan.CATATAN_ASISTEN1';

	
	const IS_ASISTEN1_SETUJU = 'ebudget.dinas_master_kegiatan.IS_ASISTEN1_SETUJU';

	
	const VERIFIKASI_ASISTEN1 = 'ebudget.dinas_master_kegiatan.VERIFIKASI_ASISTEN1';

	
	const CATATAN_ASISTEN2 = 'ebudget.dinas_master_kegiatan.CATATAN_ASISTEN2';

	
	const IS_ASISTEN2_SETUJU = 'ebudget.dinas_master_kegiatan.IS_ASISTEN2_SETUJU';

	
	const VERIFIKASI_ASISTEN2 = 'ebudget.dinas_master_kegiatan.VERIFIKASI_ASISTEN2';

	
	const CATATAN_ASISTEN3 = 'ebudget.dinas_master_kegiatan.CATATAN_ASISTEN3';

	
	const IS_ASISTEN3_SETUJU = 'ebudget.dinas_master_kegiatan.IS_ASISTEN3_SETUJU';

	
	const VERIFIKASI_ASISTEN3 = 'ebudget.dinas_master_kegiatan.VERIFIKASI_ASISTEN3';

	
	const CATATAN_SEKDA = 'ebudget.dinas_master_kegiatan.CATATAN_SEKDA';

	
	const IS_SEKDA_SETUJU = 'ebudget.dinas_master_kegiatan.IS_SEKDA_SETUJU';

	
	const VERIFIKASI_SEKDA = 'ebudget.dinas_master_kegiatan.VERIFIKASI_SEKDA';

	
	const VERIFIKASI_ASISTEN = 'ebudget.dinas_master_kegiatan.VERIFIKASI_ASISTEN';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('UnitId', 'KodeKegiatan', 'KodeBidang', 'KodeUrusanWajib', 'KodeProgram', 'KodeSasaran', 'KodeIndikator', 'AlokasiDana', 'NamaKegiatan', 'Masukan', 'Output', 'Outcome', 'Benefit', 'Impact', 'Tipe', 'KegiatanActive', 'ToKegiatanCode', 'Catatan', 'TargetOutcome', 'Lokasi', 'JumlahPrev', 'JumlahNow', 'JumlahNext', 'KodeProgram2', 'KodeUrusan', 'LastUpdateUser', 'LastUpdateTime', 'LastUpdateIp', 'Tahap', 'KodeMisi', 'KodeTujuan', 'Ranking', 'Nomor13', 'PpaNama', 'PpaPangkat', 'PpaNip', 'Lanjutan', 'UserId', 'Id', 'Tahun', 'TambahanPagu', 'Gender', 'KodeKegKeuangan', 'UserIdLama', 'Indikator', 'IsDak', 'KodeKegiatanAsal', 'KodeKegKeuanganAsal', 'ThKeMultiyears', 'KelompokSasaran', 'PaguBappeko', 'KodeDpa', 'UserIdPptk', 'UserIdKpa', 'CatatanPembahasan', 'CatatanPenyelia', 'CatatanBappeko', 'StatusLevel', 'IsTapdSetuju', 'IsBappekoSetuju', 'IsPenyeliaSetuju', 'IsPernahRka', 'KodeKegiatanBaru', 'CatatanBpkpd', 'UbahF1Dinas', 'UbahF1Peneliti', 'SisaLelangDinas', 'SisaLelangPeneliti', 'CatatanUbahF1Dinas', 'CatatanSisaLelangPeneliti', 'PptkApproval', 'KpaApproval', 'CatatanBagianHukum', 'CatatanInspektorat', 'CatatanBadanKepegawaian', 'CatatanLppa', 'IsBagianHukumSetuju', 'IsInspektoratSetuju', 'IsBadanKepegawaianSetuju', 'IsLppaSetuju', 'VerifikasiBpkpd', 'VerifikasiBappeko', 'VerifikasiPenyelia', 'VerifikasiBagianHukum', 'VerifikasiInspektorat', 'VerifikasiBadanKepegawaian', 'VerifikasiLppa', 'MetodeCount', 'CatatanBagianOrganisasi', 'IsBagianOrganisasiSetuju', 'VerifikasiBagianOrganisasi', 'CatatanAsisten1', 'IsAsisten1Setuju', 'VerifikasiAsisten1', 'CatatanAsisten2', 'IsAsisten2Setuju', 'VerifikasiAsisten2', 'CatatanAsisten3', 'IsAsisten3Setuju', 'VerifikasiAsisten3', 'CatatanSekda', 'IsSekdaSetuju', 'VerifikasiSekda', 'VerifikasiAsisten', ),
		BasePeer::TYPE_COLNAME => array (DinasMasterKegiatanPeer::UNIT_ID, DinasMasterKegiatanPeer::KODE_KEGIATAN, DinasMasterKegiatanPeer::KODE_BIDANG, DinasMasterKegiatanPeer::KODE_URUSAN_WAJIB, DinasMasterKegiatanPeer::KODE_PROGRAM, DinasMasterKegiatanPeer::KODE_SASARAN, DinasMasterKegiatanPeer::KODE_INDIKATOR, DinasMasterKegiatanPeer::ALOKASI_DANA, DinasMasterKegiatanPeer::NAMA_KEGIATAN, DinasMasterKegiatanPeer::MASUKAN, DinasMasterKegiatanPeer::OUTPUT, DinasMasterKegiatanPeer::OUTCOME, DinasMasterKegiatanPeer::BENEFIT, DinasMasterKegiatanPeer::IMPACT, DinasMasterKegiatanPeer::TIPE, DinasMasterKegiatanPeer::KEGIATAN_ACTIVE, DinasMasterKegiatanPeer::TO_KEGIATAN_CODE, DinasMasterKegiatanPeer::CATATAN, DinasMasterKegiatanPeer::TARGET_OUTCOME, DinasMasterKegiatanPeer::LOKASI, DinasMasterKegiatanPeer::JUMLAH_PREV, DinasMasterKegiatanPeer::JUMLAH_NOW, DinasMasterKegiatanPeer::JUMLAH_NEXT, DinasMasterKegiatanPeer::KODE_PROGRAM2, DinasMasterKegiatanPeer::KODE_URUSAN, DinasMasterKegiatanPeer::LAST_UPDATE_USER, DinasMasterKegiatanPeer::LAST_UPDATE_TIME, DinasMasterKegiatanPeer::LAST_UPDATE_IP, DinasMasterKegiatanPeer::TAHAP, DinasMasterKegiatanPeer::KODE_MISI, DinasMasterKegiatanPeer::KODE_TUJUAN, DinasMasterKegiatanPeer::RANKING, DinasMasterKegiatanPeer::NOMOR13, DinasMasterKegiatanPeer::PPA_NAMA, DinasMasterKegiatanPeer::PPA_PANGKAT, DinasMasterKegiatanPeer::PPA_NIP, DinasMasterKegiatanPeer::LANJUTAN, DinasMasterKegiatanPeer::USER_ID, DinasMasterKegiatanPeer::ID, DinasMasterKegiatanPeer::TAHUN, DinasMasterKegiatanPeer::TAMBAHAN_PAGU, DinasMasterKegiatanPeer::GENDER, DinasMasterKegiatanPeer::KODE_KEG_KEUANGAN, DinasMasterKegiatanPeer::USER_ID_LAMA, DinasMasterKegiatanPeer::INDIKATOR, DinasMasterKegiatanPeer::IS_DAK, DinasMasterKegiatanPeer::KODE_KEGIATAN_ASAL, DinasMasterKegiatanPeer::KODE_KEG_KEUANGAN_ASAL, DinasMasterKegiatanPeer::TH_KE_MULTIYEARS, DinasMasterKegiatanPeer::KELOMPOK_SASARAN, DinasMasterKegiatanPeer::PAGU_BAPPEKO, DinasMasterKegiatanPeer::KODE_DPA, DinasMasterKegiatanPeer::USER_ID_PPTK, DinasMasterKegiatanPeer::USER_ID_KPA, DinasMasterKegiatanPeer::CATATAN_PEMBAHASAN, DinasMasterKegiatanPeer::CATATAN_PENYELIA, DinasMasterKegiatanPeer::CATATAN_BAPPEKO, DinasMasterKegiatanPeer::STATUS_LEVEL, DinasMasterKegiatanPeer::IS_TAPD_SETUJU, DinasMasterKegiatanPeer::IS_BAPPEKO_SETUJU, DinasMasterKegiatanPeer::IS_PENYELIA_SETUJU, DinasMasterKegiatanPeer::IS_PERNAH_RKA, DinasMasterKegiatanPeer::KODE_KEGIATAN_BARU, DinasMasterKegiatanPeer::CATATAN_BPKPD, DinasMasterKegiatanPeer::UBAH_F1_DINAS, DinasMasterKegiatanPeer::UBAH_F1_PENELITI, DinasMasterKegiatanPeer::SISA_LELANG_DINAS, DinasMasterKegiatanPeer::SISA_LELANG_PENELITI, DinasMasterKegiatanPeer::CATATAN_UBAH_F1_DINAS, DinasMasterKegiatanPeer::CATATAN_SISA_LELANG_PENELITI, DinasMasterKegiatanPeer::PPTK_APPROVAL, DinasMasterKegiatanPeer::KPA_APPROVAL, DinasMasterKegiatanPeer::CATATAN_BAGIAN_HUKUM, DinasMasterKegiatanPeer::CATATAN_INSPEKTORAT, DinasMasterKegiatanPeer::CATATAN_BADAN_KEPEGAWAIAN, DinasMasterKegiatanPeer::CATATAN_LPPA, DinasMasterKegiatanPeer::IS_BAGIAN_HUKUM_SETUJU, DinasMasterKegiatanPeer::IS_INSPEKTORAT_SETUJU, DinasMasterKegiatanPeer::IS_BADAN_KEPEGAWAIAN_SETUJU, DinasMasterKegiatanPeer::IS_LPPA_SETUJU, DinasMasterKegiatanPeer::VERIFIKASI_BPKPD, DinasMasterKegiatanPeer::VERIFIKASI_BAPPEKO, DinasMasterKegiatanPeer::VERIFIKASI_PENYELIA, DinasMasterKegiatanPeer::VERIFIKASI_BAGIAN_HUKUM, DinasMasterKegiatanPeer::VERIFIKASI_INSPEKTORAT, DinasMasterKegiatanPeer::VERIFIKASI_BADAN_KEPEGAWAIAN, DinasMasterKegiatanPeer::VERIFIKASI_LPPA, DinasMasterKegiatanPeer::METODE_COUNT, DinasMasterKegiatanPeer::CATATAN_BAGIAN_ORGANISASI, DinasMasterKegiatanPeer::IS_BAGIAN_ORGANISASI_SETUJU, DinasMasterKegiatanPeer::VERIFIKASI_BAGIAN_ORGANISASI, DinasMasterKegiatanPeer::CATATAN_ASISTEN1, DinasMasterKegiatanPeer::IS_ASISTEN1_SETUJU, DinasMasterKegiatanPeer::VERIFIKASI_ASISTEN1, DinasMasterKegiatanPeer::CATATAN_ASISTEN2, DinasMasterKegiatanPeer::IS_ASISTEN2_SETUJU, DinasMasterKegiatanPeer::VERIFIKASI_ASISTEN2, DinasMasterKegiatanPeer::CATATAN_ASISTEN3, DinasMasterKegiatanPeer::IS_ASISTEN3_SETUJU, DinasMasterKegiatanPeer::VERIFIKASI_ASISTEN3, DinasMasterKegiatanPeer::CATATAN_SEKDA, DinasMasterKegiatanPeer::IS_SEKDA_SETUJU, DinasMasterKegiatanPeer::VERIFIKASI_SEKDA, DinasMasterKegiatanPeer::VERIFIKASI_ASISTEN, ),
		BasePeer::TYPE_FIELDNAME => array ('unit_id', 'kode_kegiatan', 'kode_bidang', 'kode_urusan_wajib', 'kode_program', 'kode_sasaran', 'kode_indikator', 'alokasi_dana', 'nama_kegiatan', 'masukan', 'output', 'outcome', 'benefit', 'impact', 'tipe', 'kegiatan_active', 'to_kegiatan_code', 'catatan', 'target_outcome', 'lokasi', 'jumlah_prev', 'jumlah_now', 'jumlah_next', 'kode_program2', 'kode_urusan', 'last_update_user', 'last_update_time', 'last_update_ip', 'tahap', 'kode_misi', 'kode_tujuan', 'ranking', 'nomor13', 'ppa_nama', 'ppa_pangkat', 'ppa_nip', 'lanjutan', 'user_id', 'id', 'tahun', 'tambahan_pagu', 'gender', 'kode_keg_keuangan', 'user_id_lama', 'indikator', 'is_dak', 'kode_kegiatan_asal', 'kode_keg_keuangan_asal', 'th_ke_multiyears', 'kelompok_sasaran', 'pagu_bappeko', 'kode_dpa', 'user_id_pptk', 'user_id_kpa', 'catatan_pembahasan', 'catatan_penyelia', 'catatan_bappeko', 'status_level', 'is_tapd_setuju', 'is_bappeko_setuju', 'is_penyelia_setuju', 'is_pernah_rka', 'kode_kegiatan_baru', 'catatan_bpkpd', 'ubah_f1_dinas', 'ubah_f1_peneliti', 'sisa_lelang_dinas', 'sisa_lelang_peneliti', 'catatan_ubah_f1_dinas', 'catatan_sisa_lelang_peneliti', 'pptk_approval', 'kpa_approval', 'catatan_bagian_hukum', 'catatan_inspektorat', 'catatan_badan_kepegawaian', 'catatan_lppa', 'is_bagian_hukum_setuju', 'is_inspektorat_setuju', 'is_badan_kepegawaian_setuju', 'is_lppa_setuju', 'verifikasi_bpkpd', 'verifikasi_bappeko', 'verifikasi_penyelia', 'verifikasi_bagian_hukum', 'verifikasi_inspektorat', 'verifikasi_badan_kepegawaian', 'verifikasi_lppa', 'metode_count', 'catatan_bagian_organisasi', 'is_bagian_organisasi_setuju', 'verifikasi_bagian_organisasi', 'catatan_asisten1', 'is_asisten1_setuju', 'verifikasi_asisten1', 'catatan_asisten2', 'is_asisten2_setuju', 'verifikasi_asisten2', 'catatan_asisten3', 'is_asisten3_setuju', 'verifikasi_asisten3', 'catatan_sekda', 'is_sekda_setuju', 'verifikasi_sekda', 'verifikasi_asisten', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('UnitId' => 0, 'KodeKegiatan' => 1, 'KodeBidang' => 2, 'KodeUrusanWajib' => 3, 'KodeProgram' => 4, 'KodeSasaran' => 5, 'KodeIndikator' => 6, 'AlokasiDana' => 7, 'NamaKegiatan' => 8, 'Masukan' => 9, 'Output' => 10, 'Outcome' => 11, 'Benefit' => 12, 'Impact' => 13, 'Tipe' => 14, 'KegiatanActive' => 15, 'ToKegiatanCode' => 16, 'Catatan' => 17, 'TargetOutcome' => 18, 'Lokasi' => 19, 'JumlahPrev' => 20, 'JumlahNow' => 21, 'JumlahNext' => 22, 'KodeProgram2' => 23, 'KodeUrusan' => 24, 'LastUpdateUser' => 25, 'LastUpdateTime' => 26, 'LastUpdateIp' => 27, 'Tahap' => 28, 'KodeMisi' => 29, 'KodeTujuan' => 30, 'Ranking' => 31, 'Nomor13' => 32, 'PpaNama' => 33, 'PpaPangkat' => 34, 'PpaNip' => 35, 'Lanjutan' => 36, 'UserId' => 37, 'Id' => 38, 'Tahun' => 39, 'TambahanPagu' => 40, 'Gender' => 41, 'KodeKegKeuangan' => 42, 'UserIdLama' => 43, 'Indikator' => 44, 'IsDak' => 45, 'KodeKegiatanAsal' => 46, 'KodeKegKeuanganAsal' => 47, 'ThKeMultiyears' => 48, 'KelompokSasaran' => 49, 'PaguBappeko' => 50, 'KodeDpa' => 51, 'UserIdPptk' => 52, 'UserIdKpa' => 53, 'CatatanPembahasan' => 54, 'CatatanPenyelia' => 55, 'CatatanBappeko' => 56, 'StatusLevel' => 57, 'IsTapdSetuju' => 58, 'IsBappekoSetuju' => 59, 'IsPenyeliaSetuju' => 60, 'IsPernahRka' => 61, 'KodeKegiatanBaru' => 62, 'CatatanBpkpd' => 63, 'UbahF1Dinas' => 64, 'UbahF1Peneliti' => 65, 'SisaLelangDinas' => 66, 'SisaLelangPeneliti' => 67, 'CatatanUbahF1Dinas' => 68, 'CatatanSisaLelangPeneliti' => 69, 'PptkApproval' => 70, 'KpaApproval' => 71, 'CatatanBagianHukum' => 72, 'CatatanInspektorat' => 73, 'CatatanBadanKepegawaian' => 74, 'CatatanLppa' => 75, 'IsBagianHukumSetuju' => 76, 'IsInspektoratSetuju' => 77, 'IsBadanKepegawaianSetuju' => 78, 'IsLppaSetuju' => 79, 'VerifikasiBpkpd' => 80, 'VerifikasiBappeko' => 81, 'VerifikasiPenyelia' => 82, 'VerifikasiBagianHukum' => 83, 'VerifikasiInspektorat' => 84, 'VerifikasiBadanKepegawaian' => 85, 'VerifikasiLppa' => 86, 'MetodeCount' => 87, 'CatatanBagianOrganisasi' => 88, 'IsBagianOrganisasiSetuju' => 89, 'VerifikasiBagianOrganisasi' => 90, 'CatatanAsisten1' => 91, 'IsAsisten1Setuju' => 92, 'VerifikasiAsisten1' => 93, 'CatatanAsisten2' => 94, 'IsAsisten2Setuju' => 95, 'VerifikasiAsisten2' => 96, 'CatatanAsisten3' => 97, 'IsAsisten3Setuju' => 98, 'VerifikasiAsisten3' => 99, 'CatatanSekda' => 100, 'IsSekdaSetuju' => 101, 'VerifikasiSekda' => 102, 'VerifikasiAsisten' => 103, ),
		BasePeer::TYPE_COLNAME => array (DinasMasterKegiatanPeer::UNIT_ID => 0, DinasMasterKegiatanPeer::KODE_KEGIATAN => 1, DinasMasterKegiatanPeer::KODE_BIDANG => 2, DinasMasterKegiatanPeer::KODE_URUSAN_WAJIB => 3, DinasMasterKegiatanPeer::KODE_PROGRAM => 4, DinasMasterKegiatanPeer::KODE_SASARAN => 5, DinasMasterKegiatanPeer::KODE_INDIKATOR => 6, DinasMasterKegiatanPeer::ALOKASI_DANA => 7, DinasMasterKegiatanPeer::NAMA_KEGIATAN => 8, DinasMasterKegiatanPeer::MASUKAN => 9, DinasMasterKegiatanPeer::OUTPUT => 10, DinasMasterKegiatanPeer::OUTCOME => 11, DinasMasterKegiatanPeer::BENEFIT => 12, DinasMasterKegiatanPeer::IMPACT => 13, DinasMasterKegiatanPeer::TIPE => 14, DinasMasterKegiatanPeer::KEGIATAN_ACTIVE => 15, DinasMasterKegiatanPeer::TO_KEGIATAN_CODE => 16, DinasMasterKegiatanPeer::CATATAN => 17, DinasMasterKegiatanPeer::TARGET_OUTCOME => 18, DinasMasterKegiatanPeer::LOKASI => 19, DinasMasterKegiatanPeer::JUMLAH_PREV => 20, DinasMasterKegiatanPeer::JUMLAH_NOW => 21, DinasMasterKegiatanPeer::JUMLAH_NEXT => 22, DinasMasterKegiatanPeer::KODE_PROGRAM2 => 23, DinasMasterKegiatanPeer::KODE_URUSAN => 24, DinasMasterKegiatanPeer::LAST_UPDATE_USER => 25, DinasMasterKegiatanPeer::LAST_UPDATE_TIME => 26, DinasMasterKegiatanPeer::LAST_UPDATE_IP => 27, DinasMasterKegiatanPeer::TAHAP => 28, DinasMasterKegiatanPeer::KODE_MISI => 29, DinasMasterKegiatanPeer::KODE_TUJUAN => 30, DinasMasterKegiatanPeer::RANKING => 31, DinasMasterKegiatanPeer::NOMOR13 => 32, DinasMasterKegiatanPeer::PPA_NAMA => 33, DinasMasterKegiatanPeer::PPA_PANGKAT => 34, DinasMasterKegiatanPeer::PPA_NIP => 35, DinasMasterKegiatanPeer::LANJUTAN => 36, DinasMasterKegiatanPeer::USER_ID => 37, DinasMasterKegiatanPeer::ID => 38, DinasMasterKegiatanPeer::TAHUN => 39, DinasMasterKegiatanPeer::TAMBAHAN_PAGU => 40, DinasMasterKegiatanPeer::GENDER => 41, DinasMasterKegiatanPeer::KODE_KEG_KEUANGAN => 42, DinasMasterKegiatanPeer::USER_ID_LAMA => 43, DinasMasterKegiatanPeer::INDIKATOR => 44, DinasMasterKegiatanPeer::IS_DAK => 45, DinasMasterKegiatanPeer::KODE_KEGIATAN_ASAL => 46, DinasMasterKegiatanPeer::KODE_KEG_KEUANGAN_ASAL => 47, DinasMasterKegiatanPeer::TH_KE_MULTIYEARS => 48, DinasMasterKegiatanPeer::KELOMPOK_SASARAN => 49, DinasMasterKegiatanPeer::PAGU_BAPPEKO => 50, DinasMasterKegiatanPeer::KODE_DPA => 51, DinasMasterKegiatanPeer::USER_ID_PPTK => 52, DinasMasterKegiatanPeer::USER_ID_KPA => 53, DinasMasterKegiatanPeer::CATATAN_PEMBAHASAN => 54, DinasMasterKegiatanPeer::CATATAN_PENYELIA => 55, DinasMasterKegiatanPeer::CATATAN_BAPPEKO => 56, DinasMasterKegiatanPeer::STATUS_LEVEL => 57, DinasMasterKegiatanPeer::IS_TAPD_SETUJU => 58, DinasMasterKegiatanPeer::IS_BAPPEKO_SETUJU => 59, DinasMasterKegiatanPeer::IS_PENYELIA_SETUJU => 60, DinasMasterKegiatanPeer::IS_PERNAH_RKA => 61, DinasMasterKegiatanPeer::KODE_KEGIATAN_BARU => 62, DinasMasterKegiatanPeer::CATATAN_BPKPD => 63, DinasMasterKegiatanPeer::UBAH_F1_DINAS => 64, DinasMasterKegiatanPeer::UBAH_F1_PENELITI => 65, DinasMasterKegiatanPeer::SISA_LELANG_DINAS => 66, DinasMasterKegiatanPeer::SISA_LELANG_PENELITI => 67, DinasMasterKegiatanPeer::CATATAN_UBAH_F1_DINAS => 68, DinasMasterKegiatanPeer::CATATAN_SISA_LELANG_PENELITI => 69, DinasMasterKegiatanPeer::PPTK_APPROVAL => 70, DinasMasterKegiatanPeer::KPA_APPROVAL => 71, DinasMasterKegiatanPeer::CATATAN_BAGIAN_HUKUM => 72, DinasMasterKegiatanPeer::CATATAN_INSPEKTORAT => 73, DinasMasterKegiatanPeer::CATATAN_BADAN_KEPEGAWAIAN => 74, DinasMasterKegiatanPeer::CATATAN_LPPA => 75, DinasMasterKegiatanPeer::IS_BAGIAN_HUKUM_SETUJU => 76, DinasMasterKegiatanPeer::IS_INSPEKTORAT_SETUJU => 77, DinasMasterKegiatanPeer::IS_BADAN_KEPEGAWAIAN_SETUJU => 78, DinasMasterKegiatanPeer::IS_LPPA_SETUJU => 79, DinasMasterKegiatanPeer::VERIFIKASI_BPKPD => 80, DinasMasterKegiatanPeer::VERIFIKASI_BAPPEKO => 81, DinasMasterKegiatanPeer::VERIFIKASI_PENYELIA => 82, DinasMasterKegiatanPeer::VERIFIKASI_BAGIAN_HUKUM => 83, DinasMasterKegiatanPeer::VERIFIKASI_INSPEKTORAT => 84, DinasMasterKegiatanPeer::VERIFIKASI_BADAN_KEPEGAWAIAN => 85, DinasMasterKegiatanPeer::VERIFIKASI_LPPA => 86, DinasMasterKegiatanPeer::METODE_COUNT => 87, DinasMasterKegiatanPeer::CATATAN_BAGIAN_ORGANISASI => 88, DinasMasterKegiatanPeer::IS_BAGIAN_ORGANISASI_SETUJU => 89, DinasMasterKegiatanPeer::VERIFIKASI_BAGIAN_ORGANISASI => 90, DinasMasterKegiatanPeer::CATATAN_ASISTEN1 => 91, DinasMasterKegiatanPeer::IS_ASISTEN1_SETUJU => 92, DinasMasterKegiatanPeer::VERIFIKASI_ASISTEN1 => 93, DinasMasterKegiatanPeer::CATATAN_ASISTEN2 => 94, DinasMasterKegiatanPeer::IS_ASISTEN2_SETUJU => 95, DinasMasterKegiatanPeer::VERIFIKASI_ASISTEN2 => 96, DinasMasterKegiatanPeer::CATATAN_ASISTEN3 => 97, DinasMasterKegiatanPeer::IS_ASISTEN3_SETUJU => 98, DinasMasterKegiatanPeer::VERIFIKASI_ASISTEN3 => 99, DinasMasterKegiatanPeer::CATATAN_SEKDA => 100, DinasMasterKegiatanPeer::IS_SEKDA_SETUJU => 101, DinasMasterKegiatanPeer::VERIFIKASI_SEKDA => 102, DinasMasterKegiatanPeer::VERIFIKASI_ASISTEN => 103, ),
		BasePeer::TYPE_FIELDNAME => array ('unit_id' => 0, 'kode_kegiatan' => 1, 'kode_bidang' => 2, 'kode_urusan_wajib' => 3, 'kode_program' => 4, 'kode_sasaran' => 5, 'kode_indikator' => 6, 'alokasi_dana' => 7, 'nama_kegiatan' => 8, 'masukan' => 9, 'output' => 10, 'outcome' => 11, 'benefit' => 12, 'impact' => 13, 'tipe' => 14, 'kegiatan_active' => 15, 'to_kegiatan_code' => 16, 'catatan' => 17, 'target_outcome' => 18, 'lokasi' => 19, 'jumlah_prev' => 20, 'jumlah_now' => 21, 'jumlah_next' => 22, 'kode_program2' => 23, 'kode_urusan' => 24, 'last_update_user' => 25, 'last_update_time' => 26, 'last_update_ip' => 27, 'tahap' => 28, 'kode_misi' => 29, 'kode_tujuan' => 30, 'ranking' => 31, 'nomor13' => 32, 'ppa_nama' => 33, 'ppa_pangkat' => 34, 'ppa_nip' => 35, 'lanjutan' => 36, 'user_id' => 37, 'id' => 38, 'tahun' => 39, 'tambahan_pagu' => 40, 'gender' => 41, 'kode_keg_keuangan' => 42, 'user_id_lama' => 43, 'indikator' => 44, 'is_dak' => 45, 'kode_kegiatan_asal' => 46, 'kode_keg_keuangan_asal' => 47, 'th_ke_multiyears' => 48, 'kelompok_sasaran' => 49, 'pagu_bappeko' => 50, 'kode_dpa' => 51, 'user_id_pptk' => 52, 'user_id_kpa' => 53, 'catatan_pembahasan' => 54, 'catatan_penyelia' => 55, 'catatan_bappeko' => 56, 'status_level' => 57, 'is_tapd_setuju' => 58, 'is_bappeko_setuju' => 59, 'is_penyelia_setuju' => 60, 'is_pernah_rka' => 61, 'kode_kegiatan_baru' => 62, 'catatan_bpkpd' => 63, 'ubah_f1_dinas' => 64, 'ubah_f1_peneliti' => 65, 'sisa_lelang_dinas' => 66, 'sisa_lelang_peneliti' => 67, 'catatan_ubah_f1_dinas' => 68, 'catatan_sisa_lelang_peneliti' => 69, 'pptk_approval' => 70, 'kpa_approval' => 71, 'catatan_bagian_hukum' => 72, 'catatan_inspektorat' => 73, 'catatan_badan_kepegawaian' => 74, 'catatan_lppa' => 75, 'is_bagian_hukum_setuju' => 76, 'is_inspektorat_setuju' => 77, 'is_badan_kepegawaian_setuju' => 78, 'is_lppa_setuju' => 79, 'verifikasi_bpkpd' => 80, 'verifikasi_bappeko' => 81, 'verifikasi_penyelia' => 82, 'verifikasi_bagian_hukum' => 83, 'verifikasi_inspektorat' => 84, 'verifikasi_badan_kepegawaian' => 85, 'verifikasi_lppa' => 86, 'metode_count' => 87, 'catatan_bagian_organisasi' => 88, 'is_bagian_organisasi_setuju' => 89, 'verifikasi_bagian_organisasi' => 90, 'catatan_asisten1' => 91, 'is_asisten1_setuju' => 92, 'verifikasi_asisten1' => 93, 'catatan_asisten2' => 94, 'is_asisten2_setuju' => 95, 'verifikasi_asisten2' => 96, 'catatan_asisten3' => 97, 'is_asisten3_setuju' => 98, 'verifikasi_asisten3' => 99, 'catatan_sekda' => 100, 'is_sekda_setuju' => 101, 'verifikasi_sekda' => 102, 'verifikasi_asisten' => 103, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/DinasMasterKegiatanMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.DinasMasterKegiatanMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = DinasMasterKegiatanPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(DinasMasterKegiatanPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::UNIT_ID);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::KODE_KEGIATAN);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::KODE_BIDANG);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::KODE_URUSAN_WAJIB);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::KODE_PROGRAM);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::KODE_SASARAN);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::KODE_INDIKATOR);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::ALOKASI_DANA);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::NAMA_KEGIATAN);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::MASUKAN);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::OUTPUT);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::OUTCOME);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::BENEFIT);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::IMPACT);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::TIPE);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::KEGIATAN_ACTIVE);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::TO_KEGIATAN_CODE);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::CATATAN);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::TARGET_OUTCOME);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::LOKASI);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::JUMLAH_PREV);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::JUMLAH_NOW);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::JUMLAH_NEXT);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::KODE_PROGRAM2);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::KODE_URUSAN);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::LAST_UPDATE_USER);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::LAST_UPDATE_TIME);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::LAST_UPDATE_IP);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::TAHAP);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::KODE_MISI);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::KODE_TUJUAN);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::RANKING);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::NOMOR13);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::PPA_NAMA);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::PPA_PANGKAT);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::PPA_NIP);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::LANJUTAN);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::USER_ID);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::ID);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::TAHUN);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::TAMBAHAN_PAGU);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::GENDER);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::KODE_KEG_KEUANGAN);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::USER_ID_LAMA);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::INDIKATOR);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::IS_DAK);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::KODE_KEGIATAN_ASAL);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::KODE_KEG_KEUANGAN_ASAL);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::TH_KE_MULTIYEARS);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::KELOMPOK_SASARAN);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::PAGU_BAPPEKO);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::KODE_DPA);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::USER_ID_PPTK);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::USER_ID_KPA);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::CATATAN_PEMBAHASAN);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::CATATAN_PENYELIA);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::CATATAN_BAPPEKO);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::STATUS_LEVEL);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::IS_TAPD_SETUJU);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::IS_BAPPEKO_SETUJU);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::IS_PENYELIA_SETUJU);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::IS_PERNAH_RKA);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::KODE_KEGIATAN_BARU);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::CATATAN_BPKPD);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::UBAH_F1_DINAS);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::UBAH_F1_PENELITI);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::SISA_LELANG_DINAS);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::SISA_LELANG_PENELITI);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::CATATAN_UBAH_F1_DINAS);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::CATATAN_SISA_LELANG_PENELITI);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::PPTK_APPROVAL);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::KPA_APPROVAL);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::CATATAN_BAGIAN_HUKUM);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::CATATAN_INSPEKTORAT);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::CATATAN_BADAN_KEPEGAWAIAN);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::CATATAN_LPPA);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::IS_BAGIAN_HUKUM_SETUJU);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::IS_INSPEKTORAT_SETUJU);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::IS_BADAN_KEPEGAWAIAN_SETUJU);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::IS_LPPA_SETUJU);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::VERIFIKASI_BPKPD);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::VERIFIKASI_BAPPEKO);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::VERIFIKASI_PENYELIA);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::VERIFIKASI_BAGIAN_HUKUM);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::VERIFIKASI_INSPEKTORAT);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::VERIFIKASI_BADAN_KEPEGAWAIAN);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::VERIFIKASI_LPPA);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::METODE_COUNT);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::CATATAN_BAGIAN_ORGANISASI);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::IS_BAGIAN_ORGANISASI_SETUJU);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::VERIFIKASI_BAGIAN_ORGANISASI);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::CATATAN_ASISTEN1);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::IS_ASISTEN1_SETUJU);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::VERIFIKASI_ASISTEN1);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::CATATAN_ASISTEN2);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::IS_ASISTEN2_SETUJU);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::VERIFIKASI_ASISTEN2);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::CATATAN_ASISTEN3);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::IS_ASISTEN3_SETUJU);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::VERIFIKASI_ASISTEN3);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::CATATAN_SEKDA);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::IS_SEKDA_SETUJU);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::VERIFIKASI_SEKDA);

		$criteria->addSelectColumn(DinasMasterKegiatanPeer::VERIFIKASI_ASISTEN);

	}

	const COUNT = 'COUNT(ebudget.dinas_master_kegiatan.UNIT_ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.dinas_master_kegiatan.UNIT_ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(DinasMasterKegiatanPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(DinasMasterKegiatanPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = DinasMasterKegiatanPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = DinasMasterKegiatanPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return DinasMasterKegiatanPeer::populateObjects(DinasMasterKegiatanPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			DinasMasterKegiatanPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = DinasMasterKegiatanPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return DinasMasterKegiatanPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}

		$criteria->remove(DinasMasterKegiatanPeer::ID); 

				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(DinasMasterKegiatanPeer::UNIT_ID);
			$selectCriteria->add(DinasMasterKegiatanPeer::UNIT_ID, $criteria->remove(DinasMasterKegiatanPeer::UNIT_ID), $comparison);

			$comparison = $criteria->getComparison(DinasMasterKegiatanPeer::KODE_KEGIATAN);
			$selectCriteria->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $criteria->remove(DinasMasterKegiatanPeer::KODE_KEGIATAN), $comparison);

			$comparison = $criteria->getComparison(DinasMasterKegiatanPeer::ID);
			$selectCriteria->add(DinasMasterKegiatanPeer::ID, $criteria->remove(DinasMasterKegiatanPeer::ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(DinasMasterKegiatanPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(DinasMasterKegiatanPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof DinasMasterKegiatan) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
												if(count($values) == count($values, COUNT_RECURSIVE))
			{
								$values = array($values);
			}
			$vals = array();
			foreach($values as $value)
			{

				$vals[0][] = $value[0];
				$vals[1][] = $value[1];
				$vals[2][] = $value[2];
			}

			$criteria->add(DinasMasterKegiatanPeer::UNIT_ID, $vals[0], Criteria::IN);
			$criteria->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $vals[1], Criteria::IN);
			$criteria->add(DinasMasterKegiatanPeer::ID, $vals[2], Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(DinasMasterKegiatan $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(DinasMasterKegiatanPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(DinasMasterKegiatanPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(DinasMasterKegiatanPeer::DATABASE_NAME, DinasMasterKegiatanPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = DinasMasterKegiatanPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK( $unit_id, $kode_kegiatan, $id, $con = null) {
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$criteria = new Criteria();
		$criteria->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
		$criteria->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
		$criteria->add(DinasMasterKegiatanPeer::ID, $id);
		$v = DinasMasterKegiatanPeer::doSelect($criteria, $con);

		return !empty($v) ? $v[0] : null;
	}
} 
if (Propel::isInit()) {
			try {
		BaseDinasMasterKegiatanPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/DinasMasterKegiatanMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.DinasMasterKegiatanMapBuilder');
}
