<?php


abstract class BasePakBukuBiruRincianDetailPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.pak_bukubiru_rincian_detail';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.PakBukuBiruRincianDetail';

	
	const NUM_COLUMNS = 70;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const KEGIATAN_CODE = 'ebudget.pak_bukubiru_rincian_detail.KEGIATAN_CODE';

	
	const TIPE = 'ebudget.pak_bukubiru_rincian_detail.TIPE';

	
	const DETAIL_NO = 'ebudget.pak_bukubiru_rincian_detail.DETAIL_NO';

	
	const REKENING_CODE = 'ebudget.pak_bukubiru_rincian_detail.REKENING_CODE';

	
	const KOMPONEN_ID = 'ebudget.pak_bukubiru_rincian_detail.KOMPONEN_ID';

	
	const DETAIL_NAME = 'ebudget.pak_bukubiru_rincian_detail.DETAIL_NAME';

	
	const VOLUME = 'ebudget.pak_bukubiru_rincian_detail.VOLUME';

	
	const KETERANGAN_KOEFISIEN = 'ebudget.pak_bukubiru_rincian_detail.KETERANGAN_KOEFISIEN';

	
	const SUBTITLE = 'ebudget.pak_bukubiru_rincian_detail.SUBTITLE';

	
	const KOMPONEN_HARGA = 'ebudget.pak_bukubiru_rincian_detail.KOMPONEN_HARGA';

	
	const KOMPONEN_HARGA_AWAL = 'ebudget.pak_bukubiru_rincian_detail.KOMPONEN_HARGA_AWAL';

	
	const KOMPONEN_NAME = 'ebudget.pak_bukubiru_rincian_detail.KOMPONEN_NAME';

	
	const SATUAN = 'ebudget.pak_bukubiru_rincian_detail.SATUAN';

	
	const PAJAK = 'ebudget.pak_bukubiru_rincian_detail.PAJAK';

	
	const UNIT_ID = 'ebudget.pak_bukubiru_rincian_detail.UNIT_ID';

	
	const FROM_SUB_KEGIATAN = 'ebudget.pak_bukubiru_rincian_detail.FROM_SUB_KEGIATAN';

	
	const SUB = 'ebudget.pak_bukubiru_rincian_detail.SUB';

	
	const KODE_SUB = 'ebudget.pak_bukubiru_rincian_detail.KODE_SUB';

	
	const LAST_UPDATE_USER = 'ebudget.pak_bukubiru_rincian_detail.LAST_UPDATE_USER';

	
	const LAST_UPDATE_TIME = 'ebudget.pak_bukubiru_rincian_detail.LAST_UPDATE_TIME';

	
	const LAST_UPDATE_IP = 'ebudget.pak_bukubiru_rincian_detail.LAST_UPDATE_IP';

	
	const TAHAP = 'ebudget.pak_bukubiru_rincian_detail.TAHAP';

	
	const TAHAP_EDIT = 'ebudget.pak_bukubiru_rincian_detail.TAHAP_EDIT';

	
	const TAHAP_NEW = 'ebudget.pak_bukubiru_rincian_detail.TAHAP_NEW';

	
	const STATUS_LELANG = 'ebudget.pak_bukubiru_rincian_detail.STATUS_LELANG';

	
	const NOMOR_LELANG = 'ebudget.pak_bukubiru_rincian_detail.NOMOR_LELANG';

	
	const KOEFISIEN_SEMULA = 'ebudget.pak_bukubiru_rincian_detail.KOEFISIEN_SEMULA';

	
	const VOLUME_SEMULA = 'ebudget.pak_bukubiru_rincian_detail.VOLUME_SEMULA';

	
	const HARGA_SEMULA = 'ebudget.pak_bukubiru_rincian_detail.HARGA_SEMULA';

	
	const TOTAL_SEMULA = 'ebudget.pak_bukubiru_rincian_detail.TOTAL_SEMULA';

	
	const LOCK_SUBTITLE = 'ebudget.pak_bukubiru_rincian_detail.LOCK_SUBTITLE';

	
	const STATUS_HAPUS = 'ebudget.pak_bukubiru_rincian_detail.STATUS_HAPUS';

	
	const TAHUN = 'ebudget.pak_bukubiru_rincian_detail.TAHUN';

	
	const KODE_LOKASI = 'ebudget.pak_bukubiru_rincian_detail.KODE_LOKASI';

	
	const KECAMATAN = 'ebudget.pak_bukubiru_rincian_detail.KECAMATAN';

	
	const REKENING_CODE_ASLI = 'ebudget.pak_bukubiru_rincian_detail.REKENING_CODE_ASLI';

	
	const NOTE_SKPD = 'ebudget.pak_bukubiru_rincian_detail.NOTE_SKPD';

	
	const NOTE_PENELITI = 'ebudget.pak_bukubiru_rincian_detail.NOTE_PENELITI';

	
	const NILAI_ANGGARAN = 'ebudget.pak_bukubiru_rincian_detail.NILAI_ANGGARAN';

	
	const IS_BLUD = 'ebudget.pak_bukubiru_rincian_detail.IS_BLUD';

	
	const LOKASI_KECAMATAN = 'ebudget.pak_bukubiru_rincian_detail.LOKASI_KECAMATAN';

	
	const LOKASI_KELURAHAN = 'ebudget.pak_bukubiru_rincian_detail.LOKASI_KELURAHAN';

	
	const OB = 'ebudget.pak_bukubiru_rincian_detail.OB';

	
	const OB_FROM_ID = 'ebudget.pak_bukubiru_rincian_detail.OB_FROM_ID';

	
	const IS_PER_KOMPONEN = 'ebudget.pak_bukubiru_rincian_detail.IS_PER_KOMPONEN';

	
	const KEGIATAN_CODE_ASAL = 'ebudget.pak_bukubiru_rincian_detail.KEGIATAN_CODE_ASAL';

	
	const TH_KE_MULTIYEARS = 'ebudget.pak_bukubiru_rincian_detail.TH_KE_MULTIYEARS';

	
	const HARGA_SEBELUM_SISA_LELANG = 'ebudget.pak_bukubiru_rincian_detail.HARGA_SEBELUM_SISA_LELANG';

	
	const IS_MUSRENBANG = 'ebudget.pak_bukubiru_rincian_detail.IS_MUSRENBANG';

	
	const SUB_ID_ASAL = 'ebudget.pak_bukubiru_rincian_detail.SUB_ID_ASAL';

	
	const SUBTITLE_ASAL = 'ebudget.pak_bukubiru_rincian_detail.SUBTITLE_ASAL';

	
	const KODE_SUB_ASAL = 'ebudget.pak_bukubiru_rincian_detail.KODE_SUB_ASAL';

	
	const SUB_ASAL = 'ebudget.pak_bukubiru_rincian_detail.SUB_ASAL';

	
	const LAST_EDIT_TIME = 'ebudget.pak_bukubiru_rincian_detail.LAST_EDIT_TIME';

	
	const IS_POTONG_BPJS = 'ebudget.pak_bukubiru_rincian_detail.IS_POTONG_BPJS';

	
	const IS_IURAN_BPJS = 'ebudget.pak_bukubiru_rincian_detail.IS_IURAN_BPJS';

	
	const STATUS_OB = 'ebudget.pak_bukubiru_rincian_detail.STATUS_OB';

	
	const OB_PARENT = 'ebudget.pak_bukubiru_rincian_detail.OB_PARENT';

	
	const OB_ALOKASI_BARU = 'ebudget.pak_bukubiru_rincian_detail.OB_ALOKASI_BARU';

	
	const IS_HIBAH = 'ebudget.pak_bukubiru_rincian_detail.IS_HIBAH';

	
	const STATUS_LEVEL = 'ebudget.pak_bukubiru_rincian_detail.STATUS_LEVEL';

	
	const STATUS_LEVEL_TOLAK = 'ebudget.pak_bukubiru_rincian_detail.STATUS_LEVEL_TOLAK';

	
	const STATUS_SISIPAN = 'ebudget.pak_bukubiru_rincian_detail.STATUS_SISIPAN';

	
	const IS_TAPD_SETUJU = 'ebudget.pak_bukubiru_rincian_detail.IS_TAPD_SETUJU';

	
	const IS_BAPPEKO_SETUJU = 'ebudget.pak_bukubiru_rincian_detail.IS_BAPPEKO_SETUJU';

	
	const AKRUAL_CODE = 'ebudget.pak_bukubiru_rincian_detail.AKRUAL_CODE';

	
	const TIPE2 = 'ebudget.pak_bukubiru_rincian_detail.TIPE2';

	
	const IS_PENYELIA_SETUJU = 'ebudget.pak_bukubiru_rincian_detail.IS_PENYELIA_SETUJU';

	
	const NOTE_TAPD = 'ebudget.pak_bukubiru_rincian_detail.NOTE_TAPD';

	
	const NOTE_BAPPEKO = 'ebudget.pak_bukubiru_rincian_detail.NOTE_BAPPEKO';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('KegiatanCode', 'Tipe', 'DetailNo', 'RekeningCode', 'KomponenId', 'DetailName', 'Volume', 'KeteranganKoefisien', 'Subtitle', 'KomponenHarga', 'KomponenHargaAwal', 'KomponenName', 'Satuan', 'Pajak', 'UnitId', 'FromSubKegiatan', 'Sub', 'KodeSub', 'LastUpdateUser', 'LastUpdateTime', 'LastUpdateIp', 'Tahap', 'TahapEdit', 'TahapNew', 'StatusLelang', 'NomorLelang', 'KoefisienSemula', 'VolumeSemula', 'HargaSemula', 'TotalSemula', 'LockSubtitle', 'StatusHapus', 'Tahun', 'KodeLokasi', 'Kecamatan', 'RekeningCodeAsli', 'NoteSkpd', 'NotePeneliti', 'NilaiAnggaran', 'IsBlud', 'LokasiKecamatan', 'LokasiKelurahan', 'Ob', 'ObFromId', 'IsPerKomponen', 'KegiatanCodeAsal', 'ThKeMultiyears', 'HargaSebelumSisaLelang', 'IsMusrenbang', 'SubIdAsal', 'SubtitleAsal', 'KodeSubAsal', 'SubAsal', 'LastEditTime', 'IsPotongBpjs', 'IsIuranBpjs', 'StatusOb', 'ObParent', 'ObAlokasiBaru', 'IsHibah', 'StatusLevel', 'StatusLevelTolak', 'StatusSisipan', 'IsTapdSetuju', 'IsBappekoSetuju', 'AkrualCode', 'Tipe2', 'IsPenyeliaSetuju', 'NoteTapd', 'NoteBappeko', ),
		BasePeer::TYPE_COLNAME => array (PakBukuBiruRincianDetailPeer::KEGIATAN_CODE, PakBukuBiruRincianDetailPeer::TIPE, PakBukuBiruRincianDetailPeer::DETAIL_NO, PakBukuBiruRincianDetailPeer::REKENING_CODE, PakBukuBiruRincianDetailPeer::KOMPONEN_ID, PakBukuBiruRincianDetailPeer::DETAIL_NAME, PakBukuBiruRincianDetailPeer::VOLUME, PakBukuBiruRincianDetailPeer::KETERANGAN_KOEFISIEN, PakBukuBiruRincianDetailPeer::SUBTITLE, PakBukuBiruRincianDetailPeer::KOMPONEN_HARGA, PakBukuBiruRincianDetailPeer::KOMPONEN_HARGA_AWAL, PakBukuBiruRincianDetailPeer::KOMPONEN_NAME, PakBukuBiruRincianDetailPeer::SATUAN, PakBukuBiruRincianDetailPeer::PAJAK, PakBukuBiruRincianDetailPeer::UNIT_ID, PakBukuBiruRincianDetailPeer::FROM_SUB_KEGIATAN, PakBukuBiruRincianDetailPeer::SUB, PakBukuBiruRincianDetailPeer::KODE_SUB, PakBukuBiruRincianDetailPeer::LAST_UPDATE_USER, PakBukuBiruRincianDetailPeer::LAST_UPDATE_TIME, PakBukuBiruRincianDetailPeer::LAST_UPDATE_IP, PakBukuBiruRincianDetailPeer::TAHAP, PakBukuBiruRincianDetailPeer::TAHAP_EDIT, PakBukuBiruRincianDetailPeer::TAHAP_NEW, PakBukuBiruRincianDetailPeer::STATUS_LELANG, PakBukuBiruRincianDetailPeer::NOMOR_LELANG, PakBukuBiruRincianDetailPeer::KOEFISIEN_SEMULA, PakBukuBiruRincianDetailPeer::VOLUME_SEMULA, PakBukuBiruRincianDetailPeer::HARGA_SEMULA, PakBukuBiruRincianDetailPeer::TOTAL_SEMULA, PakBukuBiruRincianDetailPeer::LOCK_SUBTITLE, PakBukuBiruRincianDetailPeer::STATUS_HAPUS, PakBukuBiruRincianDetailPeer::TAHUN, PakBukuBiruRincianDetailPeer::KODE_LOKASI, PakBukuBiruRincianDetailPeer::KECAMATAN, PakBukuBiruRincianDetailPeer::REKENING_CODE_ASLI, PakBukuBiruRincianDetailPeer::NOTE_SKPD, PakBukuBiruRincianDetailPeer::NOTE_PENELITI, PakBukuBiruRincianDetailPeer::NILAI_ANGGARAN, PakBukuBiruRincianDetailPeer::IS_BLUD, PakBukuBiruRincianDetailPeer::LOKASI_KECAMATAN, PakBukuBiruRincianDetailPeer::LOKASI_KELURAHAN, PakBukuBiruRincianDetailPeer::OB, PakBukuBiruRincianDetailPeer::OB_FROM_ID, PakBukuBiruRincianDetailPeer::IS_PER_KOMPONEN, PakBukuBiruRincianDetailPeer::KEGIATAN_CODE_ASAL, PakBukuBiruRincianDetailPeer::TH_KE_MULTIYEARS, PakBukuBiruRincianDetailPeer::HARGA_SEBELUM_SISA_LELANG, PakBukuBiruRincianDetailPeer::IS_MUSRENBANG, PakBukuBiruRincianDetailPeer::SUB_ID_ASAL, PakBukuBiruRincianDetailPeer::SUBTITLE_ASAL, PakBukuBiruRincianDetailPeer::KODE_SUB_ASAL, PakBukuBiruRincianDetailPeer::SUB_ASAL, PakBukuBiruRincianDetailPeer::LAST_EDIT_TIME, PakBukuBiruRincianDetailPeer::IS_POTONG_BPJS, PakBukuBiruRincianDetailPeer::IS_IURAN_BPJS, PakBukuBiruRincianDetailPeer::STATUS_OB, PakBukuBiruRincianDetailPeer::OB_PARENT, PakBukuBiruRincianDetailPeer::OB_ALOKASI_BARU, PakBukuBiruRincianDetailPeer::IS_HIBAH, PakBukuBiruRincianDetailPeer::STATUS_LEVEL, PakBukuBiruRincianDetailPeer::STATUS_LEVEL_TOLAK, PakBukuBiruRincianDetailPeer::STATUS_SISIPAN, PakBukuBiruRincianDetailPeer::IS_TAPD_SETUJU, PakBukuBiruRincianDetailPeer::IS_BAPPEKO_SETUJU, PakBukuBiruRincianDetailPeer::AKRUAL_CODE, PakBukuBiruRincianDetailPeer::TIPE2, PakBukuBiruRincianDetailPeer::IS_PENYELIA_SETUJU, PakBukuBiruRincianDetailPeer::NOTE_TAPD, PakBukuBiruRincianDetailPeer::NOTE_BAPPEKO, ),
		BasePeer::TYPE_FIELDNAME => array ('kegiatan_code', 'tipe', 'detail_no', 'rekening_code', 'komponen_id', 'detail_name', 'volume', 'keterangan_koefisien', 'subtitle', 'komponen_harga', 'komponen_harga_awal', 'komponen_name', 'satuan', 'pajak', 'unit_id', 'from_sub_kegiatan', 'sub', 'kode_sub', 'last_update_user', 'last_update_time', 'last_update_ip', 'tahap', 'tahap_edit', 'tahap_new', 'status_lelang', 'nomor_lelang', 'koefisien_semula', 'volume_semula', 'harga_semula', 'total_semula', 'lock_subtitle', 'status_hapus', 'tahun', 'kode_lokasi', 'kecamatan', 'rekening_code_asli', 'note_skpd', 'note_peneliti', 'nilai_anggaran', 'is_blud', 'lokasi_kecamatan', 'lokasi_kelurahan', 'ob', 'ob_from_id', 'is_per_komponen', 'kegiatan_code_asal', 'th_ke_multiyears', 'harga_sebelum_sisa_lelang', 'is_musrenbang', 'sub_id_asal', 'subtitle_asal', 'kode_sub_asal', 'sub_asal', 'last_edit_time', 'is_potong_bpjs', 'is_iuran_bpjs', 'status_ob', 'ob_parent', 'ob_alokasi_baru', 'is_hibah', 'status_level', 'status_level_tolak', 'status_sisipan', 'is_tapd_setuju', 'is_bappeko_setuju', 'akrual_code', 'tipe2', 'is_penyelia_setuju', 'note_tapd', 'note_bappeko', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('KegiatanCode' => 0, 'Tipe' => 1, 'DetailNo' => 2, 'RekeningCode' => 3, 'KomponenId' => 4, 'DetailName' => 5, 'Volume' => 6, 'KeteranganKoefisien' => 7, 'Subtitle' => 8, 'KomponenHarga' => 9, 'KomponenHargaAwal' => 10, 'KomponenName' => 11, 'Satuan' => 12, 'Pajak' => 13, 'UnitId' => 14, 'FromSubKegiatan' => 15, 'Sub' => 16, 'KodeSub' => 17, 'LastUpdateUser' => 18, 'LastUpdateTime' => 19, 'LastUpdateIp' => 20, 'Tahap' => 21, 'TahapEdit' => 22, 'TahapNew' => 23, 'StatusLelang' => 24, 'NomorLelang' => 25, 'KoefisienSemula' => 26, 'VolumeSemula' => 27, 'HargaSemula' => 28, 'TotalSemula' => 29, 'LockSubtitle' => 30, 'StatusHapus' => 31, 'Tahun' => 32, 'KodeLokasi' => 33, 'Kecamatan' => 34, 'RekeningCodeAsli' => 35, 'NoteSkpd' => 36, 'NotePeneliti' => 37, 'NilaiAnggaran' => 38, 'IsBlud' => 39, 'LokasiKecamatan' => 40, 'LokasiKelurahan' => 41, 'Ob' => 42, 'ObFromId' => 43, 'IsPerKomponen' => 44, 'KegiatanCodeAsal' => 45, 'ThKeMultiyears' => 46, 'HargaSebelumSisaLelang' => 47, 'IsMusrenbang' => 48, 'SubIdAsal' => 49, 'SubtitleAsal' => 50, 'KodeSubAsal' => 51, 'SubAsal' => 52, 'LastEditTime' => 53, 'IsPotongBpjs' => 54, 'IsIuranBpjs' => 55, 'StatusOb' => 56, 'ObParent' => 57, 'ObAlokasiBaru' => 58, 'IsHibah' => 59, 'StatusLevel' => 60, 'StatusLevelTolak' => 61, 'StatusSisipan' => 62, 'IsTapdSetuju' => 63, 'IsBappekoSetuju' => 64, 'AkrualCode' => 65, 'Tipe2' => 66, 'IsPenyeliaSetuju' => 67, 'NoteTapd' => 68, 'NoteBappeko' => 69, ),
		BasePeer::TYPE_COLNAME => array (PakBukuBiruRincianDetailPeer::KEGIATAN_CODE => 0, PakBukuBiruRincianDetailPeer::TIPE => 1, PakBukuBiruRincianDetailPeer::DETAIL_NO => 2, PakBukuBiruRincianDetailPeer::REKENING_CODE => 3, PakBukuBiruRincianDetailPeer::KOMPONEN_ID => 4, PakBukuBiruRincianDetailPeer::DETAIL_NAME => 5, PakBukuBiruRincianDetailPeer::VOLUME => 6, PakBukuBiruRincianDetailPeer::KETERANGAN_KOEFISIEN => 7, PakBukuBiruRincianDetailPeer::SUBTITLE => 8, PakBukuBiruRincianDetailPeer::KOMPONEN_HARGA => 9, PakBukuBiruRincianDetailPeer::KOMPONEN_HARGA_AWAL => 10, PakBukuBiruRincianDetailPeer::KOMPONEN_NAME => 11, PakBukuBiruRincianDetailPeer::SATUAN => 12, PakBukuBiruRincianDetailPeer::PAJAK => 13, PakBukuBiruRincianDetailPeer::UNIT_ID => 14, PakBukuBiruRincianDetailPeer::FROM_SUB_KEGIATAN => 15, PakBukuBiruRincianDetailPeer::SUB => 16, PakBukuBiruRincianDetailPeer::KODE_SUB => 17, PakBukuBiruRincianDetailPeer::LAST_UPDATE_USER => 18, PakBukuBiruRincianDetailPeer::LAST_UPDATE_TIME => 19, PakBukuBiruRincianDetailPeer::LAST_UPDATE_IP => 20, PakBukuBiruRincianDetailPeer::TAHAP => 21, PakBukuBiruRincianDetailPeer::TAHAP_EDIT => 22, PakBukuBiruRincianDetailPeer::TAHAP_NEW => 23, PakBukuBiruRincianDetailPeer::STATUS_LELANG => 24, PakBukuBiruRincianDetailPeer::NOMOR_LELANG => 25, PakBukuBiruRincianDetailPeer::KOEFISIEN_SEMULA => 26, PakBukuBiruRincianDetailPeer::VOLUME_SEMULA => 27, PakBukuBiruRincianDetailPeer::HARGA_SEMULA => 28, PakBukuBiruRincianDetailPeer::TOTAL_SEMULA => 29, PakBukuBiruRincianDetailPeer::LOCK_SUBTITLE => 30, PakBukuBiruRincianDetailPeer::STATUS_HAPUS => 31, PakBukuBiruRincianDetailPeer::TAHUN => 32, PakBukuBiruRincianDetailPeer::KODE_LOKASI => 33, PakBukuBiruRincianDetailPeer::KECAMATAN => 34, PakBukuBiruRincianDetailPeer::REKENING_CODE_ASLI => 35, PakBukuBiruRincianDetailPeer::NOTE_SKPD => 36, PakBukuBiruRincianDetailPeer::NOTE_PENELITI => 37, PakBukuBiruRincianDetailPeer::NILAI_ANGGARAN => 38, PakBukuBiruRincianDetailPeer::IS_BLUD => 39, PakBukuBiruRincianDetailPeer::LOKASI_KECAMATAN => 40, PakBukuBiruRincianDetailPeer::LOKASI_KELURAHAN => 41, PakBukuBiruRincianDetailPeer::OB => 42, PakBukuBiruRincianDetailPeer::OB_FROM_ID => 43, PakBukuBiruRincianDetailPeer::IS_PER_KOMPONEN => 44, PakBukuBiruRincianDetailPeer::KEGIATAN_CODE_ASAL => 45, PakBukuBiruRincianDetailPeer::TH_KE_MULTIYEARS => 46, PakBukuBiruRincianDetailPeer::HARGA_SEBELUM_SISA_LELANG => 47, PakBukuBiruRincianDetailPeer::IS_MUSRENBANG => 48, PakBukuBiruRincianDetailPeer::SUB_ID_ASAL => 49, PakBukuBiruRincianDetailPeer::SUBTITLE_ASAL => 50, PakBukuBiruRincianDetailPeer::KODE_SUB_ASAL => 51, PakBukuBiruRincianDetailPeer::SUB_ASAL => 52, PakBukuBiruRincianDetailPeer::LAST_EDIT_TIME => 53, PakBukuBiruRincianDetailPeer::IS_POTONG_BPJS => 54, PakBukuBiruRincianDetailPeer::IS_IURAN_BPJS => 55, PakBukuBiruRincianDetailPeer::STATUS_OB => 56, PakBukuBiruRincianDetailPeer::OB_PARENT => 57, PakBukuBiruRincianDetailPeer::OB_ALOKASI_BARU => 58, PakBukuBiruRincianDetailPeer::IS_HIBAH => 59, PakBukuBiruRincianDetailPeer::STATUS_LEVEL => 60, PakBukuBiruRincianDetailPeer::STATUS_LEVEL_TOLAK => 61, PakBukuBiruRincianDetailPeer::STATUS_SISIPAN => 62, PakBukuBiruRincianDetailPeer::IS_TAPD_SETUJU => 63, PakBukuBiruRincianDetailPeer::IS_BAPPEKO_SETUJU => 64, PakBukuBiruRincianDetailPeer::AKRUAL_CODE => 65, PakBukuBiruRincianDetailPeer::TIPE2 => 66, PakBukuBiruRincianDetailPeer::IS_PENYELIA_SETUJU => 67, PakBukuBiruRincianDetailPeer::NOTE_TAPD => 68, PakBukuBiruRincianDetailPeer::NOTE_BAPPEKO => 69, ),
		BasePeer::TYPE_FIELDNAME => array ('kegiatan_code' => 0, 'tipe' => 1, 'detail_no' => 2, 'rekening_code' => 3, 'komponen_id' => 4, 'detail_name' => 5, 'volume' => 6, 'keterangan_koefisien' => 7, 'subtitle' => 8, 'komponen_harga' => 9, 'komponen_harga_awal' => 10, 'komponen_name' => 11, 'satuan' => 12, 'pajak' => 13, 'unit_id' => 14, 'from_sub_kegiatan' => 15, 'sub' => 16, 'kode_sub' => 17, 'last_update_user' => 18, 'last_update_time' => 19, 'last_update_ip' => 20, 'tahap' => 21, 'tahap_edit' => 22, 'tahap_new' => 23, 'status_lelang' => 24, 'nomor_lelang' => 25, 'koefisien_semula' => 26, 'volume_semula' => 27, 'harga_semula' => 28, 'total_semula' => 29, 'lock_subtitle' => 30, 'status_hapus' => 31, 'tahun' => 32, 'kode_lokasi' => 33, 'kecamatan' => 34, 'rekening_code_asli' => 35, 'note_skpd' => 36, 'note_peneliti' => 37, 'nilai_anggaran' => 38, 'is_blud' => 39, 'lokasi_kecamatan' => 40, 'lokasi_kelurahan' => 41, 'ob' => 42, 'ob_from_id' => 43, 'is_per_komponen' => 44, 'kegiatan_code_asal' => 45, 'th_ke_multiyears' => 46, 'harga_sebelum_sisa_lelang' => 47, 'is_musrenbang' => 48, 'sub_id_asal' => 49, 'subtitle_asal' => 50, 'kode_sub_asal' => 51, 'sub_asal' => 52, 'last_edit_time' => 53, 'is_potong_bpjs' => 54, 'is_iuran_bpjs' => 55, 'status_ob' => 56, 'ob_parent' => 57, 'ob_alokasi_baru' => 58, 'is_hibah' => 59, 'status_level' => 60, 'status_level_tolak' => 61, 'status_sisipan' => 62, 'is_tapd_setuju' => 63, 'is_bappeko_setuju' => 64, 'akrual_code' => 65, 'tipe2' => 66, 'is_penyelia_setuju' => 67, 'note_tapd' => 68, 'note_bappeko' => 69, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/PakBukuBiruRincianDetailMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.PakBukuBiruRincianDetailMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = PakBukuBiruRincianDetailPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(PakBukuBiruRincianDetailPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(PakBukuBiruRincianDetailPeer::KEGIATAN_CODE);

		$criteria->addSelectColumn(PakBukuBiruRincianDetailPeer::TIPE);

		$criteria->addSelectColumn(PakBukuBiruRincianDetailPeer::DETAIL_NO);

		$criteria->addSelectColumn(PakBukuBiruRincianDetailPeer::REKENING_CODE);

		$criteria->addSelectColumn(PakBukuBiruRincianDetailPeer::KOMPONEN_ID);

		$criteria->addSelectColumn(PakBukuBiruRincianDetailPeer::DETAIL_NAME);

		$criteria->addSelectColumn(PakBukuBiruRincianDetailPeer::VOLUME);

		$criteria->addSelectColumn(PakBukuBiruRincianDetailPeer::KETERANGAN_KOEFISIEN);

		$criteria->addSelectColumn(PakBukuBiruRincianDetailPeer::SUBTITLE);

		$criteria->addSelectColumn(PakBukuBiruRincianDetailPeer::KOMPONEN_HARGA);

		$criteria->addSelectColumn(PakBukuBiruRincianDetailPeer::KOMPONEN_HARGA_AWAL);

		$criteria->addSelectColumn(PakBukuBiruRincianDetailPeer::KOMPONEN_NAME);

		$criteria->addSelectColumn(PakBukuBiruRincianDetailPeer::SATUAN);

		$criteria->addSelectColumn(PakBukuBiruRincianDetailPeer::PAJAK);

		$criteria->addSelectColumn(PakBukuBiruRincianDetailPeer::UNIT_ID);

		$criteria->addSelectColumn(PakBukuBiruRincianDetailPeer::FROM_SUB_KEGIATAN);

		$criteria->addSelectColumn(PakBukuBiruRincianDetailPeer::SUB);

		$criteria->addSelectColumn(PakBukuBiruRincianDetailPeer::KODE_SUB);

		$criteria->addSelectColumn(PakBukuBiruRincianDetailPeer::LAST_UPDATE_USER);

		$criteria->addSelectColumn(PakBukuBiruRincianDetailPeer::LAST_UPDATE_TIME);

		$criteria->addSelectColumn(PakBukuBiruRincianDetailPeer::LAST_UPDATE_IP);

		$criteria->addSelectColumn(PakBukuBiruRincianDetailPeer::TAHAP);

		$criteria->addSelectColumn(PakBukuBiruRincianDetailPeer::TAHAP_EDIT);

		$criteria->addSelectColumn(PakBukuBiruRincianDetailPeer::TAHAP_NEW);

		$criteria->addSelectColumn(PakBukuBiruRincianDetailPeer::STATUS_LELANG);

		$criteria->addSelectColumn(PakBukuBiruRincianDetailPeer::NOMOR_LELANG);

		$criteria->addSelectColumn(PakBukuBiruRincianDetailPeer::KOEFISIEN_SEMULA);

		$criteria->addSelectColumn(PakBukuBiruRincianDetailPeer::VOLUME_SEMULA);

		$criteria->addSelectColumn(PakBukuBiruRincianDetailPeer::HARGA_SEMULA);

		$criteria->addSelectColumn(PakBukuBiruRincianDetailPeer::TOTAL_SEMULA);

		$criteria->addSelectColumn(PakBukuBiruRincianDetailPeer::LOCK_SUBTITLE);

		$criteria->addSelectColumn(PakBukuBiruRincianDetailPeer::STATUS_HAPUS);

		$criteria->addSelectColumn(PakBukuBiruRincianDetailPeer::TAHUN);

		$criteria->addSelectColumn(PakBukuBiruRincianDetailPeer::KODE_LOKASI);

		$criteria->addSelectColumn(PakBukuBiruRincianDetailPeer::KECAMATAN);

		$criteria->addSelectColumn(PakBukuBiruRincianDetailPeer::REKENING_CODE_ASLI);

		$criteria->addSelectColumn(PakBukuBiruRincianDetailPeer::NOTE_SKPD);

		$criteria->addSelectColumn(PakBukuBiruRincianDetailPeer::NOTE_PENELITI);

		$criteria->addSelectColumn(PakBukuBiruRincianDetailPeer::NILAI_ANGGARAN);

		$criteria->addSelectColumn(PakBukuBiruRincianDetailPeer::IS_BLUD);

		$criteria->addSelectColumn(PakBukuBiruRincianDetailPeer::LOKASI_KECAMATAN);

		$criteria->addSelectColumn(PakBukuBiruRincianDetailPeer::LOKASI_KELURAHAN);

		$criteria->addSelectColumn(PakBukuBiruRincianDetailPeer::OB);

		$criteria->addSelectColumn(PakBukuBiruRincianDetailPeer::OB_FROM_ID);

		$criteria->addSelectColumn(PakBukuBiruRincianDetailPeer::IS_PER_KOMPONEN);

		$criteria->addSelectColumn(PakBukuBiruRincianDetailPeer::KEGIATAN_CODE_ASAL);

		$criteria->addSelectColumn(PakBukuBiruRincianDetailPeer::TH_KE_MULTIYEARS);

		$criteria->addSelectColumn(PakBukuBiruRincianDetailPeer::HARGA_SEBELUM_SISA_LELANG);

		$criteria->addSelectColumn(PakBukuBiruRincianDetailPeer::IS_MUSRENBANG);

		$criteria->addSelectColumn(PakBukuBiruRincianDetailPeer::SUB_ID_ASAL);

		$criteria->addSelectColumn(PakBukuBiruRincianDetailPeer::SUBTITLE_ASAL);

		$criteria->addSelectColumn(PakBukuBiruRincianDetailPeer::KODE_SUB_ASAL);

		$criteria->addSelectColumn(PakBukuBiruRincianDetailPeer::SUB_ASAL);

		$criteria->addSelectColumn(PakBukuBiruRincianDetailPeer::LAST_EDIT_TIME);

		$criteria->addSelectColumn(PakBukuBiruRincianDetailPeer::IS_POTONG_BPJS);

		$criteria->addSelectColumn(PakBukuBiruRincianDetailPeer::IS_IURAN_BPJS);

		$criteria->addSelectColumn(PakBukuBiruRincianDetailPeer::STATUS_OB);

		$criteria->addSelectColumn(PakBukuBiruRincianDetailPeer::OB_PARENT);

		$criteria->addSelectColumn(PakBukuBiruRincianDetailPeer::OB_ALOKASI_BARU);

		$criteria->addSelectColumn(PakBukuBiruRincianDetailPeer::IS_HIBAH);

		$criteria->addSelectColumn(PakBukuBiruRincianDetailPeer::STATUS_LEVEL);

		$criteria->addSelectColumn(PakBukuBiruRincianDetailPeer::STATUS_LEVEL_TOLAK);

		$criteria->addSelectColumn(PakBukuBiruRincianDetailPeer::STATUS_SISIPAN);

		$criteria->addSelectColumn(PakBukuBiruRincianDetailPeer::IS_TAPD_SETUJU);

		$criteria->addSelectColumn(PakBukuBiruRincianDetailPeer::IS_BAPPEKO_SETUJU);

		$criteria->addSelectColumn(PakBukuBiruRincianDetailPeer::AKRUAL_CODE);

		$criteria->addSelectColumn(PakBukuBiruRincianDetailPeer::TIPE2);

		$criteria->addSelectColumn(PakBukuBiruRincianDetailPeer::IS_PENYELIA_SETUJU);

		$criteria->addSelectColumn(PakBukuBiruRincianDetailPeer::NOTE_TAPD);

		$criteria->addSelectColumn(PakBukuBiruRincianDetailPeer::NOTE_BAPPEKO);

	}

	const COUNT = 'COUNT(ebudget.pak_bukubiru_rincian_detail.KEGIATAN_CODE)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.pak_bukubiru_rincian_detail.KEGIATAN_CODE)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(PakBukuBiruRincianDetailPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(PakBukuBiruRincianDetailPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = PakBukuBiruRincianDetailPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = PakBukuBiruRincianDetailPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return PakBukuBiruRincianDetailPeer::populateObjects(PakBukuBiruRincianDetailPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			PakBukuBiruRincianDetailPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = PakBukuBiruRincianDetailPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return PakBukuBiruRincianDetailPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(PakBukuBiruRincianDetailPeer::KEGIATAN_CODE);
			$selectCriteria->add(PakBukuBiruRincianDetailPeer::KEGIATAN_CODE, $criteria->remove(PakBukuBiruRincianDetailPeer::KEGIATAN_CODE), $comparison);

			$comparison = $criteria->getComparison(PakBukuBiruRincianDetailPeer::DETAIL_NO);
			$selectCriteria->add(PakBukuBiruRincianDetailPeer::DETAIL_NO, $criteria->remove(PakBukuBiruRincianDetailPeer::DETAIL_NO), $comparison);

			$comparison = $criteria->getComparison(PakBukuBiruRincianDetailPeer::UNIT_ID);
			$selectCriteria->add(PakBukuBiruRincianDetailPeer::UNIT_ID, $criteria->remove(PakBukuBiruRincianDetailPeer::UNIT_ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(PakBukuBiruRincianDetailPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(PakBukuBiruRincianDetailPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof PakBukuBiruRincianDetail) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
												if(count($values) == count($values, COUNT_RECURSIVE))
			{
								$values = array($values);
			}
			$vals = array();
			foreach($values as $value)
			{

				$vals[0][] = $value[0];
				$vals[1][] = $value[1];
				$vals[2][] = $value[2];
			}

			$criteria->add(PakBukuBiruRincianDetailPeer::KEGIATAN_CODE, $vals[0], Criteria::IN);
			$criteria->add(PakBukuBiruRincianDetailPeer::DETAIL_NO, $vals[1], Criteria::IN);
			$criteria->add(PakBukuBiruRincianDetailPeer::UNIT_ID, $vals[2], Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(PakBukuBiruRincianDetail $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(PakBukuBiruRincianDetailPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(PakBukuBiruRincianDetailPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(PakBukuBiruRincianDetailPeer::DATABASE_NAME, PakBukuBiruRincianDetailPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = PakBukuBiruRincianDetailPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK( $kegiatan_code, $detail_no, $unit_id, $con = null) {
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$criteria = new Criteria();
		$criteria->add(PakBukuBiruRincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
		$criteria->add(PakBukuBiruRincianDetailPeer::DETAIL_NO, $detail_no);
		$criteria->add(PakBukuBiruRincianDetailPeer::UNIT_ID, $unit_id);
		$v = PakBukuBiruRincianDetailPeer::doSelect($criteria, $con);

		return !empty($v) ? $v[0] : null;
	}
} 
if (Propel::isInit()) {
			try {
		BasePakBukuBiruRincianDetailPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/PakBukuBiruRincianDetailMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.PakBukuBiruRincianDetailMapBuilder');
}
