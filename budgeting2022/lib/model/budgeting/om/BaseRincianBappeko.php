<?php


abstract class BaseRincianBappeko extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $unit_id;


	
	protected $kode_kegiatan;


	
	protected $tahap;


	
	protected $jawaban1_dinas;


	
	protected $jawaban2_dinas;


	
	protected $catatan_dinas;


	
	protected $jawaban1_bappeko;


	
	protected $jawaban2_bappeko;


	
	protected $catatan_bappeko;


	
	protected $status_buka;


	
	protected $updated_at;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getUnitId()
	{

		return $this->unit_id;
	}

	
	public function getKodeKegiatan()
	{

		return $this->kode_kegiatan;
	}

	
	public function getTahap()
	{

		return $this->tahap;
	}

	
	public function getJawaban1Dinas()
	{

		return $this->jawaban1_dinas;
	}

	
	public function getJawaban2Dinas()
	{

		return $this->jawaban2_dinas;
	}

	
	public function getCatatanDinas()
	{

		return $this->catatan_dinas;
	}

	
	public function getJawaban1Bappeko()
	{

		return $this->jawaban1_bappeko;
	}

	
	public function getJawaban2Bappeko()
	{

		return $this->jawaban2_bappeko;
	}

	
	public function getCatatanBappeko()
	{

		return $this->catatan_bappeko;
	}

	
	public function getStatusBuka()
	{

		return $this->status_buka;
	}

	
	public function getUpdatedAt($format = 'Y-m-d H:i:s')
	{

		if ($this->updated_at === null || $this->updated_at === '') {
			return null;
		} elseif (!is_int($this->updated_at)) {
						$ts = strtotime($this->updated_at);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [updated_at] as date/time value: " . var_export($this->updated_at, true));
			}
		} else {
			$ts = $this->updated_at;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function setUnitId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->unit_id !== $v) {
			$this->unit_id = $v;
			$this->modifiedColumns[] = RincianBappekoPeer::UNIT_ID;
		}

	} 
	
	public function setKodeKegiatan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_kegiatan !== $v) {
			$this->kode_kegiatan = $v;
			$this->modifiedColumns[] = RincianBappekoPeer::KODE_KEGIATAN;
		}

	} 
	
	public function setTahap($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->tahap !== $v) {
			$this->tahap = $v;
			$this->modifiedColumns[] = RincianBappekoPeer::TAHAP;
		}

	} 
	
	public function setJawaban1Dinas($v)
	{

		if ($this->jawaban1_dinas !== $v) {
			$this->jawaban1_dinas = $v;
			$this->modifiedColumns[] = RincianBappekoPeer::JAWABAN1_DINAS;
		}

	} 
	
	public function setJawaban2Dinas($v)
	{

		if ($this->jawaban2_dinas !== $v) {
			$this->jawaban2_dinas = $v;
			$this->modifiedColumns[] = RincianBappekoPeer::JAWABAN2_DINAS;
		}

	} 
	
	public function setCatatanDinas($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->catatan_dinas !== $v) {
			$this->catatan_dinas = $v;
			$this->modifiedColumns[] = RincianBappekoPeer::CATATAN_DINAS;
		}

	} 
	
	public function setJawaban1Bappeko($v)
	{

		if ($this->jawaban1_bappeko !== $v) {
			$this->jawaban1_bappeko = $v;
			$this->modifiedColumns[] = RincianBappekoPeer::JAWABAN1_BAPPEKO;
		}

	} 
	
	public function setJawaban2Bappeko($v)
	{

		if ($this->jawaban2_bappeko !== $v) {
			$this->jawaban2_bappeko = $v;
			$this->modifiedColumns[] = RincianBappekoPeer::JAWABAN2_BAPPEKO;
		}

	} 
	
	public function setCatatanBappeko($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->catatan_bappeko !== $v) {
			$this->catatan_bappeko = $v;
			$this->modifiedColumns[] = RincianBappekoPeer::CATATAN_BAPPEKO;
		}

	} 
	
	public function setStatusBuka($v)
	{

		if ($this->status_buka !== $v) {
			$this->status_buka = $v;
			$this->modifiedColumns[] = RincianBappekoPeer::STATUS_BUKA;
		}

	} 
	
	public function setUpdatedAt($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [updated_at] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->updated_at !== $ts) {
			$this->updated_at = $ts;
			$this->modifiedColumns[] = RincianBappekoPeer::UPDATED_AT;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->unit_id = $rs->getString($startcol + 0);

			$this->kode_kegiatan = $rs->getString($startcol + 1);

			$this->tahap = $rs->getInt($startcol + 2);

			$this->jawaban1_dinas = $rs->getBoolean($startcol + 3);

			$this->jawaban2_dinas = $rs->getBoolean($startcol + 4);

			$this->catatan_dinas = $rs->getString($startcol + 5);

			$this->jawaban1_bappeko = $rs->getBoolean($startcol + 6);

			$this->jawaban2_bappeko = $rs->getBoolean($startcol + 7);

			$this->catatan_bappeko = $rs->getString($startcol + 8);

			$this->status_buka = $rs->getBoolean($startcol + 9);

			$this->updated_at = $rs->getTimestamp($startcol + 10, null);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 11; 
		} catch (Exception $e) {
			throw new PropelException("Error populating RincianBappeko object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(RincianBappekoPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			RincianBappekoPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
    if ($this->isModified() && !$this->isColumnModified(RincianBappekoPeer::UPDATED_AT))
    {
      $this->setUpdatedAt(time());
    }

		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(RincianBappekoPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = RincianBappekoPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += RincianBappekoPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = RincianBappekoPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = RincianBappekoPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getUnitId();
				break;
			case 1:
				return $this->getKodeKegiatan();
				break;
			case 2:
				return $this->getTahap();
				break;
			case 3:
				return $this->getJawaban1Dinas();
				break;
			case 4:
				return $this->getJawaban2Dinas();
				break;
			case 5:
				return $this->getCatatanDinas();
				break;
			case 6:
				return $this->getJawaban1Bappeko();
				break;
			case 7:
				return $this->getJawaban2Bappeko();
				break;
			case 8:
				return $this->getCatatanBappeko();
				break;
			case 9:
				return $this->getStatusBuka();
				break;
			case 10:
				return $this->getUpdatedAt();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = RincianBappekoPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getUnitId(),
			$keys[1] => $this->getKodeKegiatan(),
			$keys[2] => $this->getTahap(),
			$keys[3] => $this->getJawaban1Dinas(),
			$keys[4] => $this->getJawaban2Dinas(),
			$keys[5] => $this->getCatatanDinas(),
			$keys[6] => $this->getJawaban1Bappeko(),
			$keys[7] => $this->getJawaban2Bappeko(),
			$keys[8] => $this->getCatatanBappeko(),
			$keys[9] => $this->getStatusBuka(),
			$keys[10] => $this->getUpdatedAt(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = RincianBappekoPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setUnitId($value);
				break;
			case 1:
				$this->setKodeKegiatan($value);
				break;
			case 2:
				$this->setTahap($value);
				break;
			case 3:
				$this->setJawaban1Dinas($value);
				break;
			case 4:
				$this->setJawaban2Dinas($value);
				break;
			case 5:
				$this->setCatatanDinas($value);
				break;
			case 6:
				$this->setJawaban1Bappeko($value);
				break;
			case 7:
				$this->setJawaban2Bappeko($value);
				break;
			case 8:
				$this->setCatatanBappeko($value);
				break;
			case 9:
				$this->setStatusBuka($value);
				break;
			case 10:
				$this->setUpdatedAt($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = RincianBappekoPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setUnitId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setKodeKegiatan($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setTahap($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setJawaban1Dinas($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setJawaban2Dinas($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setCatatanDinas($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setJawaban1Bappeko($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setJawaban2Bappeko($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setCatatanBappeko($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setStatusBuka($arr[$keys[9]]);
		if (array_key_exists($keys[10], $arr)) $this->setUpdatedAt($arr[$keys[10]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(RincianBappekoPeer::DATABASE_NAME);

		if ($this->isColumnModified(RincianBappekoPeer::UNIT_ID)) $criteria->add(RincianBappekoPeer::UNIT_ID, $this->unit_id);
		if ($this->isColumnModified(RincianBappekoPeer::KODE_KEGIATAN)) $criteria->add(RincianBappekoPeer::KODE_KEGIATAN, $this->kode_kegiatan);
		if ($this->isColumnModified(RincianBappekoPeer::TAHAP)) $criteria->add(RincianBappekoPeer::TAHAP, $this->tahap);
		if ($this->isColumnModified(RincianBappekoPeer::JAWABAN1_DINAS)) $criteria->add(RincianBappekoPeer::JAWABAN1_DINAS, $this->jawaban1_dinas);
		if ($this->isColumnModified(RincianBappekoPeer::JAWABAN2_DINAS)) $criteria->add(RincianBappekoPeer::JAWABAN2_DINAS, $this->jawaban2_dinas);
		if ($this->isColumnModified(RincianBappekoPeer::CATATAN_DINAS)) $criteria->add(RincianBappekoPeer::CATATAN_DINAS, $this->catatan_dinas);
		if ($this->isColumnModified(RincianBappekoPeer::JAWABAN1_BAPPEKO)) $criteria->add(RincianBappekoPeer::JAWABAN1_BAPPEKO, $this->jawaban1_bappeko);
		if ($this->isColumnModified(RincianBappekoPeer::JAWABAN2_BAPPEKO)) $criteria->add(RincianBappekoPeer::JAWABAN2_BAPPEKO, $this->jawaban2_bappeko);
		if ($this->isColumnModified(RincianBappekoPeer::CATATAN_BAPPEKO)) $criteria->add(RincianBappekoPeer::CATATAN_BAPPEKO, $this->catatan_bappeko);
		if ($this->isColumnModified(RincianBappekoPeer::STATUS_BUKA)) $criteria->add(RincianBappekoPeer::STATUS_BUKA, $this->status_buka);
		if ($this->isColumnModified(RincianBappekoPeer::UPDATED_AT)) $criteria->add(RincianBappekoPeer::UPDATED_AT, $this->updated_at);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(RincianBappekoPeer::DATABASE_NAME);

		$criteria->add(RincianBappekoPeer::UNIT_ID, $this->unit_id);
		$criteria->add(RincianBappekoPeer::KODE_KEGIATAN, $this->kode_kegiatan);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		$pks = array();

		$pks[0] = $this->getUnitId();

		$pks[1] = $this->getKodeKegiatan();

		return $pks;
	}

	
	public function setPrimaryKey($keys)
	{

		$this->setUnitId($keys[0]);

		$this->setKodeKegiatan($keys[1]);

	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setTahap($this->tahap);

		$copyObj->setJawaban1Dinas($this->jawaban1_dinas);

		$copyObj->setJawaban2Dinas($this->jawaban2_dinas);

		$copyObj->setCatatanDinas($this->catatan_dinas);

		$copyObj->setJawaban1Bappeko($this->jawaban1_bappeko);

		$copyObj->setJawaban2Bappeko($this->jawaban2_bappeko);

		$copyObj->setCatatanBappeko($this->catatan_bappeko);

		$copyObj->setStatusBuka($this->status_buka);

		$copyObj->setUpdatedAt($this->updated_at);


		$copyObj->setNew(true);

		$copyObj->setUnitId(NULL); 
		$copyObj->setKodeKegiatan(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new RincianBappekoPeer();
		}
		return self::$peer;
	}

} 