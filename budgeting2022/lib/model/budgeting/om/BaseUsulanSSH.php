<?php


abstract class BaseUsulanSSH extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id_usulan;


	
	protected $created_at;


	
	protected $updated_at;


	
	protected $nama;


	
	protected $spec;


	
	protected $hidden_spec;


	
	protected $merek;


	
	protected $satuan;


	
	protected $harga;


	
	protected $rekening;


	
	protected $pajak;


	
	protected $keterangan;


	
	protected $kode_barang;


	
	protected $id_spjm;


	
	protected $status_verifikasi;


	
	protected $status_hapus;


	
	protected $tipe_usulan;


	
	protected $unit_id;


	
	protected $shsd_id;


	
	protected $status_confirmasi_penyelia;


	
	protected $komentar_verifikator;


	
	protected $nama_sebelum;


	
	protected $harga_sebelum;


	
	protected $alasan_perubahan_harga;


	
	protected $is_perubahan_harga;


	
	protected $is_perbedaan_pendukung;


	
	protected $alasan_perbedaan_dinas;


	
	protected $alasan_perbedaan_penyelia;


	
	protected $harga_pendukung1;


	
	protected $harga_pendukung2;


	
	protected $harga_pendukung3;


	
	protected $tahap;


	
	protected $status_baru;


	
	protected $status_pending;


	
	protected $komponen_tipe2;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getIdUsulan()
	{

		return $this->id_usulan;
	}

	
	public function getCreatedAt($format = 'Y-m-d H:i:s')
	{

		if ($this->created_at === null || $this->created_at === '') {
			return null;
		} elseif (!is_int($this->created_at)) {
						$ts = strtotime($this->created_at);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [created_at] as date/time value: " . var_export($this->created_at, true));
			}
		} else {
			$ts = $this->created_at;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getUpdatedAt($format = 'Y-m-d H:i:s')
	{

		if ($this->updated_at === null || $this->updated_at === '') {
			return null;
		} elseif (!is_int($this->updated_at)) {
						$ts = strtotime($this->updated_at);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [updated_at] as date/time value: " . var_export($this->updated_at, true));
			}
		} else {
			$ts = $this->updated_at;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getNama()
	{

		return $this->nama;
	}

	
	public function getSpec()
	{

		return $this->spec;
	}

	
	public function getHiddenSpec()
	{

		return $this->hidden_spec;
	}

	
	public function getMerek()
	{

		return $this->merek;
	}

	
	public function getSatuan()
	{

		return $this->satuan;
	}

	
	public function getHarga()
	{

		return $this->harga;
	}

	
	public function getRekening()
	{

		return $this->rekening;
	}

	
	public function getPajak()
	{

		return $this->pajak;
	}

	
	public function getKeterangan()
	{

		return $this->keterangan;
	}

	
	public function getKodeBarang()
	{

		return $this->kode_barang;
	}

	
	public function getIdSpjm()
	{

		return $this->id_spjm;
	}

	
	public function getStatusVerifikasi()
	{

		return $this->status_verifikasi;
	}

	
	public function getStatusHapus()
	{

		return $this->status_hapus;
	}

	
	public function getTipeUsulan()
	{

		return $this->tipe_usulan;
	}

	
	public function getUnitId()
	{

		return $this->unit_id;
	}

	
	public function getShsdId()
	{

		return $this->shsd_id;
	}

	
	public function getStatusConfirmasiPenyelia()
	{

		return $this->status_confirmasi_penyelia;
	}

	
	public function getKomentarVerifikator()
	{

		return $this->komentar_verifikator;
	}

	
	public function getNamaSebelum()
	{

		return $this->nama_sebelum;
	}

	
	public function getHargaSebelum()
	{

		return $this->harga_sebelum;
	}

	
	public function getAlasanPerubahanHarga()
	{

		return $this->alasan_perubahan_harga;
	}

	
	public function getIsPerubahanHarga()
	{

		return $this->is_perubahan_harga;
	}

	
	public function getIsPerbedaanPendukung()
	{

		return $this->is_perbedaan_pendukung;
	}

	
	public function getAlasanPerbedaanDinas()
	{

		return $this->alasan_perbedaan_dinas;
	}

	
	public function getAlasanPerbedaanPenyelia()
	{

		return $this->alasan_perbedaan_penyelia;
	}

	
	public function getHargaPendukung1()
	{

		return $this->harga_pendukung1;
	}

	
	public function getHargaPendukung2()
	{

		return $this->harga_pendukung2;
	}

	
	public function getHargaPendukung3()
	{

		return $this->harga_pendukung3;
	}

	
	public function getTahap()
	{

		return $this->tahap;
	}

	
	public function getStatusBaru()
	{

		return $this->status_baru;
	}

	
	public function getStatusPending()
	{

		return $this->status_pending;
	}

	
	public function getKomponenTipe2()
	{

		return $this->komponen_tipe2;
	}

	
	public function setIdUsulan($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id_usulan !== $v) {
			$this->id_usulan = $v;
			$this->modifiedColumns[] = UsulanSSHPeer::ID_USULAN;
		}

	} 
	
	public function setCreatedAt($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [created_at] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->created_at !== $ts) {
			$this->created_at = $ts;
			$this->modifiedColumns[] = UsulanSSHPeer::CREATED_AT;
		}

	} 
	
	public function setUpdatedAt($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [updated_at] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->updated_at !== $ts) {
			$this->updated_at = $ts;
			$this->modifiedColumns[] = UsulanSSHPeer::UPDATED_AT;
		}

	} 
	
	public function setNama($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nama !== $v) {
			$this->nama = $v;
			$this->modifiedColumns[] = UsulanSSHPeer::NAMA;
		}

	} 
	
	public function setSpec($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->spec !== $v) {
			$this->spec = $v;
			$this->modifiedColumns[] = UsulanSSHPeer::SPEC;
		}

	} 
	
	public function setHiddenSpec($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->hidden_spec !== $v) {
			$this->hidden_spec = $v;
			$this->modifiedColumns[] = UsulanSSHPeer::HIDDEN_SPEC;
		}

	} 
	
	public function setMerek($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->merek !== $v) {
			$this->merek = $v;
			$this->modifiedColumns[] = UsulanSSHPeer::MEREK;
		}

	} 
	
	public function setSatuan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->satuan !== $v) {
			$this->satuan = $v;
			$this->modifiedColumns[] = UsulanSSHPeer::SATUAN;
		}

	} 
	
	public function setHarga($v)
	{

		if ($this->harga !== $v) {
			$this->harga = $v;
			$this->modifiedColumns[] = UsulanSSHPeer::HARGA;
		}

	} 
	
	public function setRekening($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->rekening !== $v) {
			$this->rekening = $v;
			$this->modifiedColumns[] = UsulanSSHPeer::REKENING;
		}

	} 
	
	public function setPajak($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->pajak !== $v) {
			$this->pajak = $v;
			$this->modifiedColumns[] = UsulanSSHPeer::PAJAK;
		}

	} 
	
	public function setKeterangan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->keterangan !== $v) {
			$this->keterangan = $v;
			$this->modifiedColumns[] = UsulanSSHPeer::KETERANGAN;
		}

	} 
	
	public function setKodeBarang($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_barang !== $v) {
			$this->kode_barang = $v;
			$this->modifiedColumns[] = UsulanSSHPeer::KODE_BARANG;
		}

	} 
	
	public function setIdSpjm($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id_spjm !== $v) {
			$this->id_spjm = $v;
			$this->modifiedColumns[] = UsulanSSHPeer::ID_SPJM;
		}

	} 
	
	public function setStatusVerifikasi($v)
	{

		if ($this->status_verifikasi !== $v) {
			$this->status_verifikasi = $v;
			$this->modifiedColumns[] = UsulanSSHPeer::STATUS_VERIFIKASI;
		}

	} 
	
	public function setStatusHapus($v)
	{

		if ($this->status_hapus !== $v) {
			$this->status_hapus = $v;
			$this->modifiedColumns[] = UsulanSSHPeer::STATUS_HAPUS;
		}

	} 
	
	public function setTipeUsulan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->tipe_usulan !== $v) {
			$this->tipe_usulan = $v;
			$this->modifiedColumns[] = UsulanSSHPeer::TIPE_USULAN;
		}

	} 
	
	public function setUnitId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->unit_id !== $v) {
			$this->unit_id = $v;
			$this->modifiedColumns[] = UsulanSSHPeer::UNIT_ID;
		}

	} 
	
	public function setShsdId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->shsd_id !== $v) {
			$this->shsd_id = $v;
			$this->modifiedColumns[] = UsulanSSHPeer::SHSD_ID;
		}

	} 
	
	public function setStatusConfirmasiPenyelia($v)
	{

		if ($this->status_confirmasi_penyelia !== $v) {
			$this->status_confirmasi_penyelia = $v;
			$this->modifiedColumns[] = UsulanSSHPeer::STATUS_CONFIRMASI_PENYELIA;
		}

	} 
	
	public function setKomentarVerifikator($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->komentar_verifikator !== $v) {
			$this->komentar_verifikator = $v;
			$this->modifiedColumns[] = UsulanSSHPeer::KOMENTAR_VERIFIKATOR;
		}

	} 
	
	public function setNamaSebelum($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nama_sebelum !== $v) {
			$this->nama_sebelum = $v;
			$this->modifiedColumns[] = UsulanSSHPeer::NAMA_SEBELUM;
		}

	} 
	
	public function setHargaSebelum($v)
	{

		if ($this->harga_sebelum !== $v) {
			$this->harga_sebelum = $v;
			$this->modifiedColumns[] = UsulanSSHPeer::HARGA_SEBELUM;
		}

	} 
	
	public function setAlasanPerubahanHarga($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->alasan_perubahan_harga !== $v) {
			$this->alasan_perubahan_harga = $v;
			$this->modifiedColumns[] = UsulanSSHPeer::ALASAN_PERUBAHAN_HARGA;
		}

	} 
	
	public function setIsPerubahanHarga($v)
	{

		if ($this->is_perubahan_harga !== $v) {
			$this->is_perubahan_harga = $v;
			$this->modifiedColumns[] = UsulanSSHPeer::IS_PERUBAHAN_HARGA;
		}

	} 
	
	public function setIsPerbedaanPendukung($v)
	{

		if ($this->is_perbedaan_pendukung !== $v) {
			$this->is_perbedaan_pendukung = $v;
			$this->modifiedColumns[] = UsulanSSHPeer::IS_PERBEDAAN_PENDUKUNG;
		}

	} 
	
	public function setAlasanPerbedaanDinas($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->alasan_perbedaan_dinas !== $v) {
			$this->alasan_perbedaan_dinas = $v;
			$this->modifiedColumns[] = UsulanSSHPeer::ALASAN_PERBEDAAN_DINAS;
		}

	} 
	
	public function setAlasanPerbedaanPenyelia($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->alasan_perbedaan_penyelia !== $v) {
			$this->alasan_perbedaan_penyelia = $v;
			$this->modifiedColumns[] = UsulanSSHPeer::ALASAN_PERBEDAAN_PENYELIA;
		}

	} 
	
	public function setHargaPendukung1($v)
	{

		if ($this->harga_pendukung1 !== $v) {
			$this->harga_pendukung1 = $v;
			$this->modifiedColumns[] = UsulanSSHPeer::HARGA_PENDUKUNG1;
		}

	} 
	
	public function setHargaPendukung2($v)
	{

		if ($this->harga_pendukung2 !== $v) {
			$this->harga_pendukung2 = $v;
			$this->modifiedColumns[] = UsulanSSHPeer::HARGA_PENDUKUNG2;
		}

	} 
	
	public function setHargaPendukung3($v)
	{

		if ($this->harga_pendukung3 !== $v) {
			$this->harga_pendukung3 = $v;
			$this->modifiedColumns[] = UsulanSSHPeer::HARGA_PENDUKUNG3;
		}

	} 
	
	public function setTahap($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->tahap !== $v) {
			$this->tahap = $v;
			$this->modifiedColumns[] = UsulanSSHPeer::TAHAP;
		}

	} 
	
	public function setStatusBaru($v)
	{

		if ($this->status_baru !== $v) {
			$this->status_baru = $v;
			$this->modifiedColumns[] = UsulanSSHPeer::STATUS_BARU;
		}

	} 
	
	public function setStatusPending($v)
	{

		if ($this->status_pending !== $v) {
			$this->status_pending = $v;
			$this->modifiedColumns[] = UsulanSSHPeer::STATUS_PENDING;
		}

	} 
	
	public function setKomponenTipe2($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->komponen_tipe2 !== $v) {
			$this->komponen_tipe2 = $v;
			$this->modifiedColumns[] = UsulanSSHPeer::KOMPONEN_TIPE2;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id_usulan = $rs->getInt($startcol + 0);

			$this->created_at = $rs->getTimestamp($startcol + 1, null);

			$this->updated_at = $rs->getTimestamp($startcol + 2, null);

			$this->nama = $rs->getString($startcol + 3);

			$this->spec = $rs->getString($startcol + 4);

			$this->hidden_spec = $rs->getString($startcol + 5);

			$this->merek = $rs->getString($startcol + 6);

			$this->satuan = $rs->getString($startcol + 7);

			$this->harga = $rs->getFloat($startcol + 8);

			$this->rekening = $rs->getString($startcol + 9);

			$this->pajak = $rs->getInt($startcol + 10);

			$this->keterangan = $rs->getString($startcol + 11);

			$this->kode_barang = $rs->getString($startcol + 12);

			$this->id_spjm = $rs->getInt($startcol + 13);

			$this->status_verifikasi = $rs->getBoolean($startcol + 14);

			$this->status_hapus = $rs->getBoolean($startcol + 15);

			$this->tipe_usulan = $rs->getString($startcol + 16);

			$this->unit_id = $rs->getString($startcol + 17);

			$this->shsd_id = $rs->getString($startcol + 18);

			$this->status_confirmasi_penyelia = $rs->getBoolean($startcol + 19);

			$this->komentar_verifikator = $rs->getString($startcol + 20);

			$this->nama_sebelum = $rs->getString($startcol + 21);

			$this->harga_sebelum = $rs->getFloat($startcol + 22);

			$this->alasan_perubahan_harga = $rs->getString($startcol + 23);

			$this->is_perubahan_harga = $rs->getBoolean($startcol + 24);

			$this->is_perbedaan_pendukung = $rs->getBoolean($startcol + 25);

			$this->alasan_perbedaan_dinas = $rs->getString($startcol + 26);

			$this->alasan_perbedaan_penyelia = $rs->getString($startcol + 27);

			$this->harga_pendukung1 = $rs->getFloat($startcol + 28);

			$this->harga_pendukung2 = $rs->getFloat($startcol + 29);

			$this->harga_pendukung3 = $rs->getFloat($startcol + 30);

			$this->tahap = $rs->getString($startcol + 31);

			$this->status_baru = $rs->getBoolean($startcol + 32);

			$this->status_pending = $rs->getBoolean($startcol + 33);

			$this->komponen_tipe2 = $rs->getString($startcol + 34);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 35; 
		} catch (Exception $e) {
			throw new PropelException("Error populating UsulanSSH object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(UsulanSSHPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			UsulanSSHPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
    if ($this->isNew() && !$this->isColumnModified(UsulanSSHPeer::CREATED_AT))
    {
      $this->setCreatedAt(time());
    }

    if ($this->isModified() && !$this->isColumnModified(UsulanSSHPeer::UPDATED_AT))
    {
      $this->setUpdatedAt(time());
    }

		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(UsulanSSHPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = UsulanSSHPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += UsulanSSHPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = UsulanSSHPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = UsulanSSHPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getIdUsulan();
				break;
			case 1:
				return $this->getCreatedAt();
				break;
			case 2:
				return $this->getUpdatedAt();
				break;
			case 3:
				return $this->getNama();
				break;
			case 4:
				return $this->getSpec();
				break;
			case 5:
				return $this->getHiddenSpec();
				break;
			case 6:
				return $this->getMerek();
				break;
			case 7:
				return $this->getSatuan();
				break;
			case 8:
				return $this->getHarga();
				break;
			case 9:
				return $this->getRekening();
				break;
			case 10:
				return $this->getPajak();
				break;
			case 11:
				return $this->getKeterangan();
				break;
			case 12:
				return $this->getKodeBarang();
				break;
			case 13:
				return $this->getIdSpjm();
				break;
			case 14:
				return $this->getStatusVerifikasi();
				break;
			case 15:
				return $this->getStatusHapus();
				break;
			case 16:
				return $this->getTipeUsulan();
				break;
			case 17:
				return $this->getUnitId();
				break;
			case 18:
				return $this->getShsdId();
				break;
			case 19:
				return $this->getStatusConfirmasiPenyelia();
				break;
			case 20:
				return $this->getKomentarVerifikator();
				break;
			case 21:
				return $this->getNamaSebelum();
				break;
			case 22:
				return $this->getHargaSebelum();
				break;
			case 23:
				return $this->getAlasanPerubahanHarga();
				break;
			case 24:
				return $this->getIsPerubahanHarga();
				break;
			case 25:
				return $this->getIsPerbedaanPendukung();
				break;
			case 26:
				return $this->getAlasanPerbedaanDinas();
				break;
			case 27:
				return $this->getAlasanPerbedaanPenyelia();
				break;
			case 28:
				return $this->getHargaPendukung1();
				break;
			case 29:
				return $this->getHargaPendukung2();
				break;
			case 30:
				return $this->getHargaPendukung3();
				break;
			case 31:
				return $this->getTahap();
				break;
			case 32:
				return $this->getStatusBaru();
				break;
			case 33:
				return $this->getStatusPending();
				break;
			case 34:
				return $this->getKomponenTipe2();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = UsulanSSHPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getIdUsulan(),
			$keys[1] => $this->getCreatedAt(),
			$keys[2] => $this->getUpdatedAt(),
			$keys[3] => $this->getNama(),
			$keys[4] => $this->getSpec(),
			$keys[5] => $this->getHiddenSpec(),
			$keys[6] => $this->getMerek(),
			$keys[7] => $this->getSatuan(),
			$keys[8] => $this->getHarga(),
			$keys[9] => $this->getRekening(),
			$keys[10] => $this->getPajak(),
			$keys[11] => $this->getKeterangan(),
			$keys[12] => $this->getKodeBarang(),
			$keys[13] => $this->getIdSpjm(),
			$keys[14] => $this->getStatusVerifikasi(),
			$keys[15] => $this->getStatusHapus(),
			$keys[16] => $this->getTipeUsulan(),
			$keys[17] => $this->getUnitId(),
			$keys[18] => $this->getShsdId(),
			$keys[19] => $this->getStatusConfirmasiPenyelia(),
			$keys[20] => $this->getKomentarVerifikator(),
			$keys[21] => $this->getNamaSebelum(),
			$keys[22] => $this->getHargaSebelum(),
			$keys[23] => $this->getAlasanPerubahanHarga(),
			$keys[24] => $this->getIsPerubahanHarga(),
			$keys[25] => $this->getIsPerbedaanPendukung(),
			$keys[26] => $this->getAlasanPerbedaanDinas(),
			$keys[27] => $this->getAlasanPerbedaanPenyelia(),
			$keys[28] => $this->getHargaPendukung1(),
			$keys[29] => $this->getHargaPendukung2(),
			$keys[30] => $this->getHargaPendukung3(),
			$keys[31] => $this->getTahap(),
			$keys[32] => $this->getStatusBaru(),
			$keys[33] => $this->getStatusPending(),
			$keys[34] => $this->getKomponenTipe2(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = UsulanSSHPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setIdUsulan($value);
				break;
			case 1:
				$this->setCreatedAt($value);
				break;
			case 2:
				$this->setUpdatedAt($value);
				break;
			case 3:
				$this->setNama($value);
				break;
			case 4:
				$this->setSpec($value);
				break;
			case 5:
				$this->setHiddenSpec($value);
				break;
			case 6:
				$this->setMerek($value);
				break;
			case 7:
				$this->setSatuan($value);
				break;
			case 8:
				$this->setHarga($value);
				break;
			case 9:
				$this->setRekening($value);
				break;
			case 10:
				$this->setPajak($value);
				break;
			case 11:
				$this->setKeterangan($value);
				break;
			case 12:
				$this->setKodeBarang($value);
				break;
			case 13:
				$this->setIdSpjm($value);
				break;
			case 14:
				$this->setStatusVerifikasi($value);
				break;
			case 15:
				$this->setStatusHapus($value);
				break;
			case 16:
				$this->setTipeUsulan($value);
				break;
			case 17:
				$this->setUnitId($value);
				break;
			case 18:
				$this->setShsdId($value);
				break;
			case 19:
				$this->setStatusConfirmasiPenyelia($value);
				break;
			case 20:
				$this->setKomentarVerifikator($value);
				break;
			case 21:
				$this->setNamaSebelum($value);
				break;
			case 22:
				$this->setHargaSebelum($value);
				break;
			case 23:
				$this->setAlasanPerubahanHarga($value);
				break;
			case 24:
				$this->setIsPerubahanHarga($value);
				break;
			case 25:
				$this->setIsPerbedaanPendukung($value);
				break;
			case 26:
				$this->setAlasanPerbedaanDinas($value);
				break;
			case 27:
				$this->setAlasanPerbedaanPenyelia($value);
				break;
			case 28:
				$this->setHargaPendukung1($value);
				break;
			case 29:
				$this->setHargaPendukung2($value);
				break;
			case 30:
				$this->setHargaPendukung3($value);
				break;
			case 31:
				$this->setTahap($value);
				break;
			case 32:
				$this->setStatusBaru($value);
				break;
			case 33:
				$this->setStatusPending($value);
				break;
			case 34:
				$this->setKomponenTipe2($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = UsulanSSHPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setIdUsulan($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setCreatedAt($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setUpdatedAt($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setNama($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setSpec($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setHiddenSpec($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setMerek($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setSatuan($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setHarga($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setRekening($arr[$keys[9]]);
		if (array_key_exists($keys[10], $arr)) $this->setPajak($arr[$keys[10]]);
		if (array_key_exists($keys[11], $arr)) $this->setKeterangan($arr[$keys[11]]);
		if (array_key_exists($keys[12], $arr)) $this->setKodeBarang($arr[$keys[12]]);
		if (array_key_exists($keys[13], $arr)) $this->setIdSpjm($arr[$keys[13]]);
		if (array_key_exists($keys[14], $arr)) $this->setStatusVerifikasi($arr[$keys[14]]);
		if (array_key_exists($keys[15], $arr)) $this->setStatusHapus($arr[$keys[15]]);
		if (array_key_exists($keys[16], $arr)) $this->setTipeUsulan($arr[$keys[16]]);
		if (array_key_exists($keys[17], $arr)) $this->setUnitId($arr[$keys[17]]);
		if (array_key_exists($keys[18], $arr)) $this->setShsdId($arr[$keys[18]]);
		if (array_key_exists($keys[19], $arr)) $this->setStatusConfirmasiPenyelia($arr[$keys[19]]);
		if (array_key_exists($keys[20], $arr)) $this->setKomentarVerifikator($arr[$keys[20]]);
		if (array_key_exists($keys[21], $arr)) $this->setNamaSebelum($arr[$keys[21]]);
		if (array_key_exists($keys[22], $arr)) $this->setHargaSebelum($arr[$keys[22]]);
		if (array_key_exists($keys[23], $arr)) $this->setAlasanPerubahanHarga($arr[$keys[23]]);
		if (array_key_exists($keys[24], $arr)) $this->setIsPerubahanHarga($arr[$keys[24]]);
		if (array_key_exists($keys[25], $arr)) $this->setIsPerbedaanPendukung($arr[$keys[25]]);
		if (array_key_exists($keys[26], $arr)) $this->setAlasanPerbedaanDinas($arr[$keys[26]]);
		if (array_key_exists($keys[27], $arr)) $this->setAlasanPerbedaanPenyelia($arr[$keys[27]]);
		if (array_key_exists($keys[28], $arr)) $this->setHargaPendukung1($arr[$keys[28]]);
		if (array_key_exists($keys[29], $arr)) $this->setHargaPendukung2($arr[$keys[29]]);
		if (array_key_exists($keys[30], $arr)) $this->setHargaPendukung3($arr[$keys[30]]);
		if (array_key_exists($keys[31], $arr)) $this->setTahap($arr[$keys[31]]);
		if (array_key_exists($keys[32], $arr)) $this->setStatusBaru($arr[$keys[32]]);
		if (array_key_exists($keys[33], $arr)) $this->setStatusPending($arr[$keys[33]]);
		if (array_key_exists($keys[34], $arr)) $this->setKomponenTipe2($arr[$keys[34]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(UsulanSSHPeer::DATABASE_NAME);

		if ($this->isColumnModified(UsulanSSHPeer::ID_USULAN)) $criteria->add(UsulanSSHPeer::ID_USULAN, $this->id_usulan);
		if ($this->isColumnModified(UsulanSSHPeer::CREATED_AT)) $criteria->add(UsulanSSHPeer::CREATED_AT, $this->created_at);
		if ($this->isColumnModified(UsulanSSHPeer::UPDATED_AT)) $criteria->add(UsulanSSHPeer::UPDATED_AT, $this->updated_at);
		if ($this->isColumnModified(UsulanSSHPeer::NAMA)) $criteria->add(UsulanSSHPeer::NAMA, $this->nama);
		if ($this->isColumnModified(UsulanSSHPeer::SPEC)) $criteria->add(UsulanSSHPeer::SPEC, $this->spec);
		if ($this->isColumnModified(UsulanSSHPeer::HIDDEN_SPEC)) $criteria->add(UsulanSSHPeer::HIDDEN_SPEC, $this->hidden_spec);
		if ($this->isColumnModified(UsulanSSHPeer::MEREK)) $criteria->add(UsulanSSHPeer::MEREK, $this->merek);
		if ($this->isColumnModified(UsulanSSHPeer::SATUAN)) $criteria->add(UsulanSSHPeer::SATUAN, $this->satuan);
		if ($this->isColumnModified(UsulanSSHPeer::HARGA)) $criteria->add(UsulanSSHPeer::HARGA, $this->harga);
		if ($this->isColumnModified(UsulanSSHPeer::REKENING)) $criteria->add(UsulanSSHPeer::REKENING, $this->rekening);
		if ($this->isColumnModified(UsulanSSHPeer::PAJAK)) $criteria->add(UsulanSSHPeer::PAJAK, $this->pajak);
		if ($this->isColumnModified(UsulanSSHPeer::KETERANGAN)) $criteria->add(UsulanSSHPeer::KETERANGAN, $this->keterangan);
		if ($this->isColumnModified(UsulanSSHPeer::KODE_BARANG)) $criteria->add(UsulanSSHPeer::KODE_BARANG, $this->kode_barang);
		if ($this->isColumnModified(UsulanSSHPeer::ID_SPJM)) $criteria->add(UsulanSSHPeer::ID_SPJM, $this->id_spjm);
		if ($this->isColumnModified(UsulanSSHPeer::STATUS_VERIFIKASI)) $criteria->add(UsulanSSHPeer::STATUS_VERIFIKASI, $this->status_verifikasi);
		if ($this->isColumnModified(UsulanSSHPeer::STATUS_HAPUS)) $criteria->add(UsulanSSHPeer::STATUS_HAPUS, $this->status_hapus);
		if ($this->isColumnModified(UsulanSSHPeer::TIPE_USULAN)) $criteria->add(UsulanSSHPeer::TIPE_USULAN, $this->tipe_usulan);
		if ($this->isColumnModified(UsulanSSHPeer::UNIT_ID)) $criteria->add(UsulanSSHPeer::UNIT_ID, $this->unit_id);
		if ($this->isColumnModified(UsulanSSHPeer::SHSD_ID)) $criteria->add(UsulanSSHPeer::SHSD_ID, $this->shsd_id);
		if ($this->isColumnModified(UsulanSSHPeer::STATUS_CONFIRMASI_PENYELIA)) $criteria->add(UsulanSSHPeer::STATUS_CONFIRMASI_PENYELIA, $this->status_confirmasi_penyelia);
		if ($this->isColumnModified(UsulanSSHPeer::KOMENTAR_VERIFIKATOR)) $criteria->add(UsulanSSHPeer::KOMENTAR_VERIFIKATOR, $this->komentar_verifikator);
		if ($this->isColumnModified(UsulanSSHPeer::NAMA_SEBELUM)) $criteria->add(UsulanSSHPeer::NAMA_SEBELUM, $this->nama_sebelum);
		if ($this->isColumnModified(UsulanSSHPeer::HARGA_SEBELUM)) $criteria->add(UsulanSSHPeer::HARGA_SEBELUM, $this->harga_sebelum);
		if ($this->isColumnModified(UsulanSSHPeer::ALASAN_PERUBAHAN_HARGA)) $criteria->add(UsulanSSHPeer::ALASAN_PERUBAHAN_HARGA, $this->alasan_perubahan_harga);
		if ($this->isColumnModified(UsulanSSHPeer::IS_PERUBAHAN_HARGA)) $criteria->add(UsulanSSHPeer::IS_PERUBAHAN_HARGA, $this->is_perubahan_harga);
		if ($this->isColumnModified(UsulanSSHPeer::IS_PERBEDAAN_PENDUKUNG)) $criteria->add(UsulanSSHPeer::IS_PERBEDAAN_PENDUKUNG, $this->is_perbedaan_pendukung);
		if ($this->isColumnModified(UsulanSSHPeer::ALASAN_PERBEDAAN_DINAS)) $criteria->add(UsulanSSHPeer::ALASAN_PERBEDAAN_DINAS, $this->alasan_perbedaan_dinas);
		if ($this->isColumnModified(UsulanSSHPeer::ALASAN_PERBEDAAN_PENYELIA)) $criteria->add(UsulanSSHPeer::ALASAN_PERBEDAAN_PENYELIA, $this->alasan_perbedaan_penyelia);
		if ($this->isColumnModified(UsulanSSHPeer::HARGA_PENDUKUNG1)) $criteria->add(UsulanSSHPeer::HARGA_PENDUKUNG1, $this->harga_pendukung1);
		if ($this->isColumnModified(UsulanSSHPeer::HARGA_PENDUKUNG2)) $criteria->add(UsulanSSHPeer::HARGA_PENDUKUNG2, $this->harga_pendukung2);
		if ($this->isColumnModified(UsulanSSHPeer::HARGA_PENDUKUNG3)) $criteria->add(UsulanSSHPeer::HARGA_PENDUKUNG3, $this->harga_pendukung3);
		if ($this->isColumnModified(UsulanSSHPeer::TAHAP)) $criteria->add(UsulanSSHPeer::TAHAP, $this->tahap);
		if ($this->isColumnModified(UsulanSSHPeer::STATUS_BARU)) $criteria->add(UsulanSSHPeer::STATUS_BARU, $this->status_baru);
		if ($this->isColumnModified(UsulanSSHPeer::STATUS_PENDING)) $criteria->add(UsulanSSHPeer::STATUS_PENDING, $this->status_pending);
		if ($this->isColumnModified(UsulanSSHPeer::KOMPONEN_TIPE2)) $criteria->add(UsulanSSHPeer::KOMPONEN_TIPE2, $this->komponen_tipe2);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(UsulanSSHPeer::DATABASE_NAME);

		$criteria->add(UsulanSSHPeer::ID_USULAN, $this->id_usulan);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getIdUsulan();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setIdUsulan($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setCreatedAt($this->created_at);

		$copyObj->setUpdatedAt($this->updated_at);

		$copyObj->setNama($this->nama);

		$copyObj->setSpec($this->spec);

		$copyObj->setHiddenSpec($this->hidden_spec);

		$copyObj->setMerek($this->merek);

		$copyObj->setSatuan($this->satuan);

		$copyObj->setHarga($this->harga);

		$copyObj->setRekening($this->rekening);

		$copyObj->setPajak($this->pajak);

		$copyObj->setKeterangan($this->keterangan);

		$copyObj->setKodeBarang($this->kode_barang);

		$copyObj->setIdSpjm($this->id_spjm);

		$copyObj->setStatusVerifikasi($this->status_verifikasi);

		$copyObj->setStatusHapus($this->status_hapus);

		$copyObj->setTipeUsulan($this->tipe_usulan);

		$copyObj->setUnitId($this->unit_id);

		$copyObj->setShsdId($this->shsd_id);

		$copyObj->setStatusConfirmasiPenyelia($this->status_confirmasi_penyelia);

		$copyObj->setKomentarVerifikator($this->komentar_verifikator);

		$copyObj->setNamaSebelum($this->nama_sebelum);

		$copyObj->setHargaSebelum($this->harga_sebelum);

		$copyObj->setAlasanPerubahanHarga($this->alasan_perubahan_harga);

		$copyObj->setIsPerubahanHarga($this->is_perubahan_harga);

		$copyObj->setIsPerbedaanPendukung($this->is_perbedaan_pendukung);

		$copyObj->setAlasanPerbedaanDinas($this->alasan_perbedaan_dinas);

		$copyObj->setAlasanPerbedaanPenyelia($this->alasan_perbedaan_penyelia);

		$copyObj->setHargaPendukung1($this->harga_pendukung1);

		$copyObj->setHargaPendukung2($this->harga_pendukung2);

		$copyObj->setHargaPendukung3($this->harga_pendukung3);

		$copyObj->setTahap($this->tahap);

		$copyObj->setStatusBaru($this->status_baru);

		$copyObj->setStatusPending($this->status_pending);

		$copyObj->setKomponenTipe2($this->komponen_tipe2);


		$copyObj->setNew(true);

		$copyObj->setIdUsulan(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new UsulanSSHPeer();
		}
		return self::$peer;
	}

} 