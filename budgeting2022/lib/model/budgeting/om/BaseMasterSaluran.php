<?php


abstract class BaseMasterSaluran extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $kode;


	
	protected $nama_saluran;


	
	protected $batasan;


	
	protected $kecamatan;


	
	protected $panjang;


	
	protected $lebar;


	
	protected $keterangan;


	
	protected $kelurahan;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getKode()
	{

		return $this->kode;
	}

	
	public function getNamaSaluran()
	{

		return $this->nama_saluran;
	}

	
	public function getBatasan()
	{

		return $this->batasan;
	}

	
	public function getKecamatan()
	{

		return $this->kecamatan;
	}

	
	public function getPanjang()
	{

		return $this->panjang;
	}

	
	public function getLebar()
	{

		return $this->lebar;
	}

	
	public function getKeterangan()
	{

		return $this->keterangan;
	}

	
	public function getKelurahan()
	{

		return $this->kelurahan;
	}

	
	public function setKode($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode !== $v) {
			$this->kode = $v;
			$this->modifiedColumns[] = MasterSaluranPeer::KODE;
		}

	} 
	
	public function setNamaSaluran($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nama_saluran !== $v) {
			$this->nama_saluran = $v;
			$this->modifiedColumns[] = MasterSaluranPeer::NAMA_SALURAN;
		}

	} 
	
	public function setBatasan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->batasan !== $v) {
			$this->batasan = $v;
			$this->modifiedColumns[] = MasterSaluranPeer::BATASAN;
		}

	} 
	
	public function setKecamatan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kecamatan !== $v) {
			$this->kecamatan = $v;
			$this->modifiedColumns[] = MasterSaluranPeer::KECAMATAN;
		}

	} 
	
	public function setPanjang($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->panjang !== $v) {
			$this->panjang = $v;
			$this->modifiedColumns[] = MasterSaluranPeer::PANJANG;
		}

	} 
	
	public function setLebar($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->lebar !== $v) {
			$this->lebar = $v;
			$this->modifiedColumns[] = MasterSaluranPeer::LEBAR;
		}

	} 
	
	public function setKeterangan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->keterangan !== $v) {
			$this->keterangan = $v;
			$this->modifiedColumns[] = MasterSaluranPeer::KETERANGAN;
		}

	} 
	
	public function setKelurahan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kelurahan !== $v) {
			$this->kelurahan = $v;
			$this->modifiedColumns[] = MasterSaluranPeer::KELURAHAN;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->kode = $rs->getString($startcol + 0);

			$this->nama_saluran = $rs->getString($startcol + 1);

			$this->batasan = $rs->getString($startcol + 2);

			$this->kecamatan = $rs->getString($startcol + 3);

			$this->panjang = $rs->getString($startcol + 4);

			$this->lebar = $rs->getString($startcol + 5);

			$this->keterangan = $rs->getString($startcol + 6);

			$this->kelurahan = $rs->getString($startcol + 7);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 8; 
		} catch (Exception $e) {
			throw new PropelException("Error populating MasterSaluran object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(MasterSaluranPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			MasterSaluranPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(MasterSaluranPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = MasterSaluranPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += MasterSaluranPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = MasterSaluranPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = MasterSaluranPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getKode();
				break;
			case 1:
				return $this->getNamaSaluran();
				break;
			case 2:
				return $this->getBatasan();
				break;
			case 3:
				return $this->getKecamatan();
				break;
			case 4:
				return $this->getPanjang();
				break;
			case 5:
				return $this->getLebar();
				break;
			case 6:
				return $this->getKeterangan();
				break;
			case 7:
				return $this->getKelurahan();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = MasterSaluranPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getKode(),
			$keys[1] => $this->getNamaSaluran(),
			$keys[2] => $this->getBatasan(),
			$keys[3] => $this->getKecamatan(),
			$keys[4] => $this->getPanjang(),
			$keys[5] => $this->getLebar(),
			$keys[6] => $this->getKeterangan(),
			$keys[7] => $this->getKelurahan(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = MasterSaluranPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setKode($value);
				break;
			case 1:
				$this->setNamaSaluran($value);
				break;
			case 2:
				$this->setBatasan($value);
				break;
			case 3:
				$this->setKecamatan($value);
				break;
			case 4:
				$this->setPanjang($value);
				break;
			case 5:
				$this->setLebar($value);
				break;
			case 6:
				$this->setKeterangan($value);
				break;
			case 7:
				$this->setKelurahan($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = MasterSaluranPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setKode($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setNamaSaluran($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setBatasan($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setKecamatan($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setPanjang($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setLebar($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setKeterangan($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setKelurahan($arr[$keys[7]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(MasterSaluranPeer::DATABASE_NAME);

		if ($this->isColumnModified(MasterSaluranPeer::KODE)) $criteria->add(MasterSaluranPeer::KODE, $this->kode);
		if ($this->isColumnModified(MasterSaluranPeer::NAMA_SALURAN)) $criteria->add(MasterSaluranPeer::NAMA_SALURAN, $this->nama_saluran);
		if ($this->isColumnModified(MasterSaluranPeer::BATASAN)) $criteria->add(MasterSaluranPeer::BATASAN, $this->batasan);
		if ($this->isColumnModified(MasterSaluranPeer::KECAMATAN)) $criteria->add(MasterSaluranPeer::KECAMATAN, $this->kecamatan);
		if ($this->isColumnModified(MasterSaluranPeer::PANJANG)) $criteria->add(MasterSaluranPeer::PANJANG, $this->panjang);
		if ($this->isColumnModified(MasterSaluranPeer::LEBAR)) $criteria->add(MasterSaluranPeer::LEBAR, $this->lebar);
		if ($this->isColumnModified(MasterSaluranPeer::KETERANGAN)) $criteria->add(MasterSaluranPeer::KETERANGAN, $this->keterangan);
		if ($this->isColumnModified(MasterSaluranPeer::KELURAHAN)) $criteria->add(MasterSaluranPeer::KELURAHAN, $this->kelurahan);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(MasterSaluranPeer::DATABASE_NAME);

		$criteria->add(MasterSaluranPeer::KODE, $this->kode);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getKode();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setKode($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setNamaSaluran($this->nama_saluran);

		$copyObj->setBatasan($this->batasan);

		$copyObj->setKecamatan($this->kecamatan);

		$copyObj->setPanjang($this->panjang);

		$copyObj->setLebar($this->lebar);

		$copyObj->setKeterangan($this->keterangan);

		$copyObj->setKelurahan($this->kelurahan);


		$copyObj->setNew(true);

		$copyObj->setKode(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new MasterSaluranPeer();
		}
		return self::$peer;
	}

} 