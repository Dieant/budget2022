<?php


abstract class BaseUsulanSPJMPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.usulan_spjm';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.UsulanSPJM';

	
	const NUM_COLUMNS = 17;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const ID_SPJM = 'ebudget.usulan_spjm.ID_SPJM';

	
	const TGL_SPJM = 'ebudget.usulan_spjm.TGL_SPJM';

	
	const CREATED_AT = 'ebudget.usulan_spjm.CREATED_AT';

	
	const UPDATED_AT = 'ebudget.usulan_spjm.UPDATED_AT';

	
	const PENYELIA = 'ebudget.usulan_spjm.PENYELIA';

	
	const SKPD = 'ebudget.usulan_spjm.SKPD';

	
	const TIPE_USULAN = 'ebudget.usulan_spjm.TIPE_USULAN';

	
	const JENIS_USULAN = 'ebudget.usulan_spjm.JENIS_USULAN';

	
	const JUMLAH_DUKUNGAN = 'ebudget.usulan_spjm.JUMLAH_DUKUNGAN';

	
	const STATUS_VERIFIKASI = 'ebudget.usulan_spjm.STATUS_VERIFIKASI';

	
	const STATUS_HAPUS = 'ebudget.usulan_spjm.STATUS_HAPUS';

	
	const FILEPATH = 'ebudget.usulan_spjm.FILEPATH';

	
	const FILEPATH_PDF = 'ebudget.usulan_spjm.FILEPATH_PDF';

	
	const ID_TOKEN = 'ebudget.usulan_spjm.ID_TOKEN';

	
	const NOMOR_SURAT = 'ebudget.usulan_spjm.NOMOR_SURAT';

	
	const KOMENTAR_VERIFIKATOR = 'ebudget.usulan_spjm.KOMENTAR_VERIFIKATOR';

	
	const TAHAP = 'ebudget.usulan_spjm.TAHAP';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('IdSpjm', 'TglSpjm', 'CreatedAt', 'UpdatedAt', 'Penyelia', 'Skpd', 'TipeUsulan', 'JenisUsulan', 'JumlahDukungan', 'StatusVerifikasi', 'StatusHapus', 'Filepath', 'FilepathPdf', 'IdToken', 'NomorSurat', 'KomentarVerifikator', 'Tahap', ),
		BasePeer::TYPE_COLNAME => array (UsulanSPJMPeer::ID_SPJM, UsulanSPJMPeer::TGL_SPJM, UsulanSPJMPeer::CREATED_AT, UsulanSPJMPeer::UPDATED_AT, UsulanSPJMPeer::PENYELIA, UsulanSPJMPeer::SKPD, UsulanSPJMPeer::TIPE_USULAN, UsulanSPJMPeer::JENIS_USULAN, UsulanSPJMPeer::JUMLAH_DUKUNGAN, UsulanSPJMPeer::STATUS_VERIFIKASI, UsulanSPJMPeer::STATUS_HAPUS, UsulanSPJMPeer::FILEPATH, UsulanSPJMPeer::FILEPATH_PDF, UsulanSPJMPeer::ID_TOKEN, UsulanSPJMPeer::NOMOR_SURAT, UsulanSPJMPeer::KOMENTAR_VERIFIKATOR, UsulanSPJMPeer::TAHAP, ),
		BasePeer::TYPE_FIELDNAME => array ('id_spjm', 'tgl_spjm', 'created_at', 'updated_at', 'penyelia', 'skpd', 'tipe_usulan', 'jenis_usulan', 'jumlah_dukungan', 'status_verifikasi', 'status_hapus', 'filepath', 'filepath_pdf', 'id_token', 'nomor_surat', 'komentar_verifikator', 'tahap', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('IdSpjm' => 0, 'TglSpjm' => 1, 'CreatedAt' => 2, 'UpdatedAt' => 3, 'Penyelia' => 4, 'Skpd' => 5, 'TipeUsulan' => 6, 'JenisUsulan' => 7, 'JumlahDukungan' => 8, 'StatusVerifikasi' => 9, 'StatusHapus' => 10, 'Filepath' => 11, 'FilepathPdf' => 12, 'IdToken' => 13, 'NomorSurat' => 14, 'KomentarVerifikator' => 15, 'Tahap' => 16, ),
		BasePeer::TYPE_COLNAME => array (UsulanSPJMPeer::ID_SPJM => 0, UsulanSPJMPeer::TGL_SPJM => 1, UsulanSPJMPeer::CREATED_AT => 2, UsulanSPJMPeer::UPDATED_AT => 3, UsulanSPJMPeer::PENYELIA => 4, UsulanSPJMPeer::SKPD => 5, UsulanSPJMPeer::TIPE_USULAN => 6, UsulanSPJMPeer::JENIS_USULAN => 7, UsulanSPJMPeer::JUMLAH_DUKUNGAN => 8, UsulanSPJMPeer::STATUS_VERIFIKASI => 9, UsulanSPJMPeer::STATUS_HAPUS => 10, UsulanSPJMPeer::FILEPATH => 11, UsulanSPJMPeer::FILEPATH_PDF => 12, UsulanSPJMPeer::ID_TOKEN => 13, UsulanSPJMPeer::NOMOR_SURAT => 14, UsulanSPJMPeer::KOMENTAR_VERIFIKATOR => 15, UsulanSPJMPeer::TAHAP => 16, ),
		BasePeer::TYPE_FIELDNAME => array ('id_spjm' => 0, 'tgl_spjm' => 1, 'created_at' => 2, 'updated_at' => 3, 'penyelia' => 4, 'skpd' => 5, 'tipe_usulan' => 6, 'jenis_usulan' => 7, 'jumlah_dukungan' => 8, 'status_verifikasi' => 9, 'status_hapus' => 10, 'filepath' => 11, 'filepath_pdf' => 12, 'id_token' => 13, 'nomor_surat' => 14, 'komentar_verifikator' => 15, 'tahap' => 16, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/UsulanSPJMMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.UsulanSPJMMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = UsulanSPJMPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(UsulanSPJMPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(UsulanSPJMPeer::ID_SPJM);

		$criteria->addSelectColumn(UsulanSPJMPeer::TGL_SPJM);

		$criteria->addSelectColumn(UsulanSPJMPeer::CREATED_AT);

		$criteria->addSelectColumn(UsulanSPJMPeer::UPDATED_AT);

		$criteria->addSelectColumn(UsulanSPJMPeer::PENYELIA);

		$criteria->addSelectColumn(UsulanSPJMPeer::SKPD);

		$criteria->addSelectColumn(UsulanSPJMPeer::TIPE_USULAN);

		$criteria->addSelectColumn(UsulanSPJMPeer::JENIS_USULAN);

		$criteria->addSelectColumn(UsulanSPJMPeer::JUMLAH_DUKUNGAN);

		$criteria->addSelectColumn(UsulanSPJMPeer::STATUS_VERIFIKASI);

		$criteria->addSelectColumn(UsulanSPJMPeer::STATUS_HAPUS);

		$criteria->addSelectColumn(UsulanSPJMPeer::FILEPATH);

		$criteria->addSelectColumn(UsulanSPJMPeer::FILEPATH_PDF);

		$criteria->addSelectColumn(UsulanSPJMPeer::ID_TOKEN);

		$criteria->addSelectColumn(UsulanSPJMPeer::NOMOR_SURAT);

		$criteria->addSelectColumn(UsulanSPJMPeer::KOMENTAR_VERIFIKATOR);

		$criteria->addSelectColumn(UsulanSPJMPeer::TAHAP);

	}

	const COUNT = 'COUNT(ebudget.usulan_spjm.ID_SPJM)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.usulan_spjm.ID_SPJM)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(UsulanSPJMPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(UsulanSPJMPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = UsulanSPJMPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = UsulanSPJMPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return UsulanSPJMPeer::populateObjects(UsulanSPJMPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			UsulanSPJMPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = UsulanSPJMPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return UsulanSPJMPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(UsulanSPJMPeer::ID_SPJM);
			$selectCriteria->add(UsulanSPJMPeer::ID_SPJM, $criteria->remove(UsulanSPJMPeer::ID_SPJM), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(UsulanSPJMPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(UsulanSPJMPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof UsulanSPJM) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(UsulanSPJMPeer::ID_SPJM, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(UsulanSPJM $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(UsulanSPJMPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(UsulanSPJMPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(UsulanSPJMPeer::DATABASE_NAME, UsulanSPJMPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = UsulanSPJMPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(UsulanSPJMPeer::DATABASE_NAME);

		$criteria->add(UsulanSPJMPeer::ID_SPJM, $pk);


		$v = UsulanSPJMPeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(UsulanSPJMPeer::ID_SPJM, $pks, Criteria::IN);
			$objs = UsulanSPJMPeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BaseUsulanSPJMPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/UsulanSPJMMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.UsulanSPJMMapBuilder');
}
