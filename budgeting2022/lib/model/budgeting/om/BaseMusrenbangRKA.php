<?php


abstract class BaseMusrenbangRKA extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id;


	
	protected $status_hapus;


	
	protected $unit_id;


	
	protected $kegiatan_code;


	
	protected $detail_no;


	
	protected $id_musrenbang;


	
	protected $tahun_musrenbang;


	
	protected $koordinat_musrenbang;


	
	protected $volume;


	
	protected $satuan;


	
	protected $anggaran;


	
	protected $komponen_name;


	
	protected $komponen_harga;


	
	protected $komponen_pajak;


	
	protected $detail_name;


	
	protected $komponen_rekening;


	
	protected $komponen_tipe;


	
	protected $subtitle;


	
	protected $komponen_id;


	
	protected $created_by;


	
	protected $updated_by;


	
	protected $created_at;


	
	protected $updated_at;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getId()
	{

		return $this->id;
	}

	
	public function getStatusHapus()
	{

		return $this->status_hapus;
	}

	
	public function getUnitId()
	{

		return $this->unit_id;
	}

	
	public function getKegiatanCode()
	{

		return $this->kegiatan_code;
	}

	
	public function getDetailNo()
	{

		return $this->detail_no;
	}

	
	public function getIdMusrenbang()
	{

		return $this->id_musrenbang;
	}

	
	public function getTahunMusrenbang()
	{

		return $this->tahun_musrenbang;
	}

	
	public function getKoordinatMusrenbang()
	{

		return $this->koordinat_musrenbang;
	}

	
	public function getVolume()
	{

		return $this->volume;
	}

	
	public function getSatuan()
	{

		return $this->satuan;
	}

	
	public function getAnggaran()
	{

		return $this->anggaran;
	}

	
	public function getKomponenName()
	{

		return $this->komponen_name;
	}

	
	public function getKomponenHarga()
	{

		return $this->komponen_harga;
	}

	
	public function getKomponenPajak()
	{

		return $this->komponen_pajak;
	}

	
	public function getDetailName()
	{

		return $this->detail_name;
	}

	
	public function getKomponenRekening()
	{

		return $this->komponen_rekening;
	}

	
	public function getKomponenTipe()
	{

		return $this->komponen_tipe;
	}

	
	public function getSubtitle()
	{

		return $this->subtitle;
	}

	
	public function getKomponenId()
	{

		return $this->komponen_id;
	}

	
	public function getCreatedBy()
	{

		return $this->created_by;
	}

	
	public function getUpdatedBy()
	{

		return $this->updated_by;
	}

	
	public function getCreatedAt($format = 'Y-m-d H:i:s')
	{

		if ($this->created_at === null || $this->created_at === '') {
			return null;
		} elseif (!is_int($this->created_at)) {
						$ts = strtotime($this->created_at);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [created_at] as date/time value: " . var_export($this->created_at, true));
			}
		} else {
			$ts = $this->created_at;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getUpdatedAt($format = 'Y-m-d H:i:s')
	{

		if ($this->updated_at === null || $this->updated_at === '') {
			return null;
		} elseif (!is_int($this->updated_at)) {
						$ts = strtotime($this->updated_at);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [updated_at] as date/time value: " . var_export($this->updated_at, true));
			}
		} else {
			$ts = $this->updated_at;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function setId($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = MusrenbangRKAPeer::ID;
		}

	} 
	
	public function setStatusHapus($v)
	{

		if ($this->status_hapus !== $v) {
			$this->status_hapus = $v;
			$this->modifiedColumns[] = MusrenbangRKAPeer::STATUS_HAPUS;
		}

	} 
	
	public function setUnitId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->unit_id !== $v) {
			$this->unit_id = $v;
			$this->modifiedColumns[] = MusrenbangRKAPeer::UNIT_ID;
		}

	} 
	
	public function setKegiatanCode($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kegiatan_code !== $v) {
			$this->kegiatan_code = $v;
			$this->modifiedColumns[] = MusrenbangRKAPeer::KEGIATAN_CODE;
		}

	} 
	
	public function setDetailNo($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->detail_no !== $v) {
			$this->detail_no = $v;
			$this->modifiedColumns[] = MusrenbangRKAPeer::DETAIL_NO;
		}

	} 
	
	public function setIdMusrenbang($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id_musrenbang !== $v) {
			$this->id_musrenbang = $v;
			$this->modifiedColumns[] = MusrenbangRKAPeer::ID_MUSRENBANG;
		}

	} 
	
	public function setTahunMusrenbang($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->tahun_musrenbang !== $v) {
			$this->tahun_musrenbang = $v;
			$this->modifiedColumns[] = MusrenbangRKAPeer::TAHUN_MUSRENBANG;
		}

	} 
	
	public function setKoordinatMusrenbang($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->koordinat_musrenbang !== $v) {
			$this->koordinat_musrenbang = $v;
			$this->modifiedColumns[] = MusrenbangRKAPeer::KOORDINAT_MUSRENBANG;
		}

	} 
	
	public function setVolume($v)
	{

		if ($this->volume !== $v) {
			$this->volume = $v;
			$this->modifiedColumns[] = MusrenbangRKAPeer::VOLUME;
		}

	} 
	
	public function setSatuan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->satuan !== $v) {
			$this->satuan = $v;
			$this->modifiedColumns[] = MusrenbangRKAPeer::SATUAN;
		}

	} 
	
	public function setAnggaran($v)
	{

		if ($this->anggaran !== $v) {
			$this->anggaran = $v;
			$this->modifiedColumns[] = MusrenbangRKAPeer::ANGGARAN;
		}

	} 
	
	public function setKomponenName($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->komponen_name !== $v) {
			$this->komponen_name = $v;
			$this->modifiedColumns[] = MusrenbangRKAPeer::KOMPONEN_NAME;
		}

	} 
	
	public function setKomponenHarga($v)
	{

		if ($this->komponen_harga !== $v) {
			$this->komponen_harga = $v;
			$this->modifiedColumns[] = MusrenbangRKAPeer::KOMPONEN_HARGA;
		}

	} 
	
	public function setKomponenPajak($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->komponen_pajak !== $v) {
			$this->komponen_pajak = $v;
			$this->modifiedColumns[] = MusrenbangRKAPeer::KOMPONEN_PAJAK;
		}

	} 
	
	public function setDetailName($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->detail_name !== $v) {
			$this->detail_name = $v;
			$this->modifiedColumns[] = MusrenbangRKAPeer::DETAIL_NAME;
		}

	} 
	
	public function setKomponenRekening($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->komponen_rekening !== $v) {
			$this->komponen_rekening = $v;
			$this->modifiedColumns[] = MusrenbangRKAPeer::KOMPONEN_REKENING;
		}

	} 
	
	public function setKomponenTipe($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->komponen_tipe !== $v) {
			$this->komponen_tipe = $v;
			$this->modifiedColumns[] = MusrenbangRKAPeer::KOMPONEN_TIPE;
		}

	} 
	
	public function setSubtitle($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->subtitle !== $v) {
			$this->subtitle = $v;
			$this->modifiedColumns[] = MusrenbangRKAPeer::SUBTITLE;
		}

	} 
	
	public function setKomponenId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->komponen_id !== $v) {
			$this->komponen_id = $v;
			$this->modifiedColumns[] = MusrenbangRKAPeer::KOMPONEN_ID;
		}

	} 
	
	public function setCreatedBy($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->created_by !== $v) {
			$this->created_by = $v;
			$this->modifiedColumns[] = MusrenbangRKAPeer::CREATED_BY;
		}

	} 
	
	public function setUpdatedBy($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->updated_by !== $v) {
			$this->updated_by = $v;
			$this->modifiedColumns[] = MusrenbangRKAPeer::UPDATED_BY;
		}

	} 
	
	public function setCreatedAt($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [created_at] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->created_at !== $ts) {
			$this->created_at = $ts;
			$this->modifiedColumns[] = MusrenbangRKAPeer::CREATED_AT;
		}

	} 
	
	public function setUpdatedAt($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [updated_at] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->updated_at !== $ts) {
			$this->updated_at = $ts;
			$this->modifiedColumns[] = MusrenbangRKAPeer::UPDATED_AT;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getInt($startcol + 0);

			$this->status_hapus = $rs->getBoolean($startcol + 1);

			$this->unit_id = $rs->getString($startcol + 2);

			$this->kegiatan_code = $rs->getString($startcol + 3);

			$this->detail_no = $rs->getInt($startcol + 4);

			$this->id_musrenbang = $rs->getInt($startcol + 5);

			$this->tahun_musrenbang = $rs->getString($startcol + 6);

			$this->koordinat_musrenbang = $rs->getString($startcol + 7);

			$this->volume = $rs->getFloat($startcol + 8);

			$this->satuan = $rs->getString($startcol + 9);

			$this->anggaran = $rs->getFloat($startcol + 10);

			$this->komponen_name = $rs->getString($startcol + 11);

			$this->komponen_harga = $rs->getFloat($startcol + 12);

			$this->komponen_pajak = $rs->getInt($startcol + 13);

			$this->detail_name = $rs->getString($startcol + 14);

			$this->komponen_rekening = $rs->getString($startcol + 15);

			$this->komponen_tipe = $rs->getString($startcol + 16);

			$this->subtitle = $rs->getString($startcol + 17);

			$this->komponen_id = $rs->getString($startcol + 18);

			$this->created_by = $rs->getString($startcol + 19);

			$this->updated_by = $rs->getString($startcol + 20);

			$this->created_at = $rs->getTimestamp($startcol + 21, null);

			$this->updated_at = $rs->getTimestamp($startcol + 22, null);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 23; 
		} catch (Exception $e) {
			throw new PropelException("Error populating MusrenbangRKA object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(MusrenbangRKAPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			MusrenbangRKAPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
    if ($this->isNew() && !$this->isColumnModified(MusrenbangRKAPeer::CREATED_AT))
    {
      $this->setCreatedAt(time());
    }

    if ($this->isModified() && !$this->isColumnModified(MusrenbangRKAPeer::UPDATED_AT))
    {
      $this->setUpdatedAt(time());
    }

		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(MusrenbangRKAPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = MusrenbangRKAPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += MusrenbangRKAPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = MusrenbangRKAPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = MusrenbangRKAPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getStatusHapus();
				break;
			case 2:
				return $this->getUnitId();
				break;
			case 3:
				return $this->getKegiatanCode();
				break;
			case 4:
				return $this->getDetailNo();
				break;
			case 5:
				return $this->getIdMusrenbang();
				break;
			case 6:
				return $this->getTahunMusrenbang();
				break;
			case 7:
				return $this->getKoordinatMusrenbang();
				break;
			case 8:
				return $this->getVolume();
				break;
			case 9:
				return $this->getSatuan();
				break;
			case 10:
				return $this->getAnggaran();
				break;
			case 11:
				return $this->getKomponenName();
				break;
			case 12:
				return $this->getKomponenHarga();
				break;
			case 13:
				return $this->getKomponenPajak();
				break;
			case 14:
				return $this->getDetailName();
				break;
			case 15:
				return $this->getKomponenRekening();
				break;
			case 16:
				return $this->getKomponenTipe();
				break;
			case 17:
				return $this->getSubtitle();
				break;
			case 18:
				return $this->getKomponenId();
				break;
			case 19:
				return $this->getCreatedBy();
				break;
			case 20:
				return $this->getUpdatedBy();
				break;
			case 21:
				return $this->getCreatedAt();
				break;
			case 22:
				return $this->getUpdatedAt();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = MusrenbangRKAPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getStatusHapus(),
			$keys[2] => $this->getUnitId(),
			$keys[3] => $this->getKegiatanCode(),
			$keys[4] => $this->getDetailNo(),
			$keys[5] => $this->getIdMusrenbang(),
			$keys[6] => $this->getTahunMusrenbang(),
			$keys[7] => $this->getKoordinatMusrenbang(),
			$keys[8] => $this->getVolume(),
			$keys[9] => $this->getSatuan(),
			$keys[10] => $this->getAnggaran(),
			$keys[11] => $this->getKomponenName(),
			$keys[12] => $this->getKomponenHarga(),
			$keys[13] => $this->getKomponenPajak(),
			$keys[14] => $this->getDetailName(),
			$keys[15] => $this->getKomponenRekening(),
			$keys[16] => $this->getKomponenTipe(),
			$keys[17] => $this->getSubtitle(),
			$keys[18] => $this->getKomponenId(),
			$keys[19] => $this->getCreatedBy(),
			$keys[20] => $this->getUpdatedBy(),
			$keys[21] => $this->getCreatedAt(),
			$keys[22] => $this->getUpdatedAt(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = MusrenbangRKAPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setStatusHapus($value);
				break;
			case 2:
				$this->setUnitId($value);
				break;
			case 3:
				$this->setKegiatanCode($value);
				break;
			case 4:
				$this->setDetailNo($value);
				break;
			case 5:
				$this->setIdMusrenbang($value);
				break;
			case 6:
				$this->setTahunMusrenbang($value);
				break;
			case 7:
				$this->setKoordinatMusrenbang($value);
				break;
			case 8:
				$this->setVolume($value);
				break;
			case 9:
				$this->setSatuan($value);
				break;
			case 10:
				$this->setAnggaran($value);
				break;
			case 11:
				$this->setKomponenName($value);
				break;
			case 12:
				$this->setKomponenHarga($value);
				break;
			case 13:
				$this->setKomponenPajak($value);
				break;
			case 14:
				$this->setDetailName($value);
				break;
			case 15:
				$this->setKomponenRekening($value);
				break;
			case 16:
				$this->setKomponenTipe($value);
				break;
			case 17:
				$this->setSubtitle($value);
				break;
			case 18:
				$this->setKomponenId($value);
				break;
			case 19:
				$this->setCreatedBy($value);
				break;
			case 20:
				$this->setUpdatedBy($value);
				break;
			case 21:
				$this->setCreatedAt($value);
				break;
			case 22:
				$this->setUpdatedAt($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = MusrenbangRKAPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setStatusHapus($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setUnitId($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setKegiatanCode($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setDetailNo($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setIdMusrenbang($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setTahunMusrenbang($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setKoordinatMusrenbang($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setVolume($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setSatuan($arr[$keys[9]]);
		if (array_key_exists($keys[10], $arr)) $this->setAnggaran($arr[$keys[10]]);
		if (array_key_exists($keys[11], $arr)) $this->setKomponenName($arr[$keys[11]]);
		if (array_key_exists($keys[12], $arr)) $this->setKomponenHarga($arr[$keys[12]]);
		if (array_key_exists($keys[13], $arr)) $this->setKomponenPajak($arr[$keys[13]]);
		if (array_key_exists($keys[14], $arr)) $this->setDetailName($arr[$keys[14]]);
		if (array_key_exists($keys[15], $arr)) $this->setKomponenRekening($arr[$keys[15]]);
		if (array_key_exists($keys[16], $arr)) $this->setKomponenTipe($arr[$keys[16]]);
		if (array_key_exists($keys[17], $arr)) $this->setSubtitle($arr[$keys[17]]);
		if (array_key_exists($keys[18], $arr)) $this->setKomponenId($arr[$keys[18]]);
		if (array_key_exists($keys[19], $arr)) $this->setCreatedBy($arr[$keys[19]]);
		if (array_key_exists($keys[20], $arr)) $this->setUpdatedBy($arr[$keys[20]]);
		if (array_key_exists($keys[21], $arr)) $this->setCreatedAt($arr[$keys[21]]);
		if (array_key_exists($keys[22], $arr)) $this->setUpdatedAt($arr[$keys[22]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(MusrenbangRKAPeer::DATABASE_NAME);

		if ($this->isColumnModified(MusrenbangRKAPeer::ID)) $criteria->add(MusrenbangRKAPeer::ID, $this->id);
		if ($this->isColumnModified(MusrenbangRKAPeer::STATUS_HAPUS)) $criteria->add(MusrenbangRKAPeer::STATUS_HAPUS, $this->status_hapus);
		if ($this->isColumnModified(MusrenbangRKAPeer::UNIT_ID)) $criteria->add(MusrenbangRKAPeer::UNIT_ID, $this->unit_id);
		if ($this->isColumnModified(MusrenbangRKAPeer::KEGIATAN_CODE)) $criteria->add(MusrenbangRKAPeer::KEGIATAN_CODE, $this->kegiatan_code);
		if ($this->isColumnModified(MusrenbangRKAPeer::DETAIL_NO)) $criteria->add(MusrenbangRKAPeer::DETAIL_NO, $this->detail_no);
		if ($this->isColumnModified(MusrenbangRKAPeer::ID_MUSRENBANG)) $criteria->add(MusrenbangRKAPeer::ID_MUSRENBANG, $this->id_musrenbang);
		if ($this->isColumnModified(MusrenbangRKAPeer::TAHUN_MUSRENBANG)) $criteria->add(MusrenbangRKAPeer::TAHUN_MUSRENBANG, $this->tahun_musrenbang);
		if ($this->isColumnModified(MusrenbangRKAPeer::KOORDINAT_MUSRENBANG)) $criteria->add(MusrenbangRKAPeer::KOORDINAT_MUSRENBANG, $this->koordinat_musrenbang);
		if ($this->isColumnModified(MusrenbangRKAPeer::VOLUME)) $criteria->add(MusrenbangRKAPeer::VOLUME, $this->volume);
		if ($this->isColumnModified(MusrenbangRKAPeer::SATUAN)) $criteria->add(MusrenbangRKAPeer::SATUAN, $this->satuan);
		if ($this->isColumnModified(MusrenbangRKAPeer::ANGGARAN)) $criteria->add(MusrenbangRKAPeer::ANGGARAN, $this->anggaran);
		if ($this->isColumnModified(MusrenbangRKAPeer::KOMPONEN_NAME)) $criteria->add(MusrenbangRKAPeer::KOMPONEN_NAME, $this->komponen_name);
		if ($this->isColumnModified(MusrenbangRKAPeer::KOMPONEN_HARGA)) $criteria->add(MusrenbangRKAPeer::KOMPONEN_HARGA, $this->komponen_harga);
		if ($this->isColumnModified(MusrenbangRKAPeer::KOMPONEN_PAJAK)) $criteria->add(MusrenbangRKAPeer::KOMPONEN_PAJAK, $this->komponen_pajak);
		if ($this->isColumnModified(MusrenbangRKAPeer::DETAIL_NAME)) $criteria->add(MusrenbangRKAPeer::DETAIL_NAME, $this->detail_name);
		if ($this->isColumnModified(MusrenbangRKAPeer::KOMPONEN_REKENING)) $criteria->add(MusrenbangRKAPeer::KOMPONEN_REKENING, $this->komponen_rekening);
		if ($this->isColumnModified(MusrenbangRKAPeer::KOMPONEN_TIPE)) $criteria->add(MusrenbangRKAPeer::KOMPONEN_TIPE, $this->komponen_tipe);
		if ($this->isColumnModified(MusrenbangRKAPeer::SUBTITLE)) $criteria->add(MusrenbangRKAPeer::SUBTITLE, $this->subtitle);
		if ($this->isColumnModified(MusrenbangRKAPeer::KOMPONEN_ID)) $criteria->add(MusrenbangRKAPeer::KOMPONEN_ID, $this->komponen_id);
		if ($this->isColumnModified(MusrenbangRKAPeer::CREATED_BY)) $criteria->add(MusrenbangRKAPeer::CREATED_BY, $this->created_by);
		if ($this->isColumnModified(MusrenbangRKAPeer::UPDATED_BY)) $criteria->add(MusrenbangRKAPeer::UPDATED_BY, $this->updated_by);
		if ($this->isColumnModified(MusrenbangRKAPeer::CREATED_AT)) $criteria->add(MusrenbangRKAPeer::CREATED_AT, $this->created_at);
		if ($this->isColumnModified(MusrenbangRKAPeer::UPDATED_AT)) $criteria->add(MusrenbangRKAPeer::UPDATED_AT, $this->updated_at);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(MusrenbangRKAPeer::DATABASE_NAME);

		$criteria->add(MusrenbangRKAPeer::ID, $this->id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setStatusHapus($this->status_hapus);

		$copyObj->setUnitId($this->unit_id);

		$copyObj->setKegiatanCode($this->kegiatan_code);

		$copyObj->setDetailNo($this->detail_no);

		$copyObj->setIdMusrenbang($this->id_musrenbang);

		$copyObj->setTahunMusrenbang($this->tahun_musrenbang);

		$copyObj->setKoordinatMusrenbang($this->koordinat_musrenbang);

		$copyObj->setVolume($this->volume);

		$copyObj->setSatuan($this->satuan);

		$copyObj->setAnggaran($this->anggaran);

		$copyObj->setKomponenName($this->komponen_name);

		$copyObj->setKomponenHarga($this->komponen_harga);

		$copyObj->setKomponenPajak($this->komponen_pajak);

		$copyObj->setDetailName($this->detail_name);

		$copyObj->setKomponenRekening($this->komponen_rekening);

		$copyObj->setKomponenTipe($this->komponen_tipe);

		$copyObj->setSubtitle($this->subtitle);

		$copyObj->setKomponenId($this->komponen_id);

		$copyObj->setCreatedBy($this->created_by);

		$copyObj->setUpdatedBy($this->updated_by);

		$copyObj->setCreatedAt($this->created_at);

		$copyObj->setUpdatedAt($this->updated_at);


		$copyObj->setNew(true);

		$copyObj->setId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new MusrenbangRKAPeer();
		}
		return self::$peer;
	}

} 