<?php


abstract class BaseSubKegiatanMemberPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.sub_kegiatan_member';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.SubKegiatanMember';

	
	const NUM_COLUMNS = 17;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const SUB_KEGIATAN_ID = 'ebudget.sub_kegiatan_member.SUB_KEGIATAN_ID';

	
	const DETAIL_NO = 'ebudget.sub_kegiatan_member.DETAIL_NO';

	
	const KOMPONEN_ID = 'ebudget.sub_kegiatan_member.KOMPONEN_ID';

	
	const REKENING_CODE = 'ebudget.sub_kegiatan_member.REKENING_CODE';

	
	const DETAIL_NAME = 'ebudget.sub_kegiatan_member.DETAIL_NAME';

	
	const VOLUME = 'ebudget.sub_kegiatan_member.VOLUME';

	
	const KETERANGAN_KOEFISIEN = 'ebudget.sub_kegiatan_member.KETERANGAN_KOEFISIEN';

	
	const KOEFISIEN = 'ebudget.sub_kegiatan_member.KOEFISIEN';

	
	const SUBTITLE = 'ebudget.sub_kegiatan_member.SUBTITLE';

	
	const KOMPONEN_HARGA = 'ebudget.sub_kegiatan_member.KOMPONEN_HARGA';

	
	const KOMPONEN_HARGA_AWAL = 'ebudget.sub_kegiatan_member.KOMPONEN_HARGA_AWAL';

	
	const KOMPONEN_NAME = 'ebudget.sub_kegiatan_member.KOMPONEN_NAME';

	
	const SATUAN = 'ebudget.sub_kegiatan_member.SATUAN';

	
	const PAJAK = 'ebudget.sub_kegiatan_member.PAJAK';

	
	const PARAM = 'ebudget.sub_kegiatan_member.PARAM';

	
	const FROM_KEGIATAN_MEMBER = 'ebudget.sub_kegiatan_member.FROM_KEGIATAN_MEMBER';

	
	const TEMP_KOMPONEN_ID = 'ebudget.sub_kegiatan_member.TEMP_KOMPONEN_ID';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('SubKegiatanId', 'DetailNo', 'KomponenId', 'RekeningCode', 'DetailName', 'Volume', 'KeteranganKoefisien', 'Koefisien', 'Subtitle', 'KomponenHarga', 'KomponenHargaAwal', 'KomponenName', 'Satuan', 'Pajak', 'Param', 'FromKegiatanMember', 'TempKomponenId', ),
		BasePeer::TYPE_COLNAME => array (SubKegiatanMemberPeer::SUB_KEGIATAN_ID, SubKegiatanMemberPeer::DETAIL_NO, SubKegiatanMemberPeer::KOMPONEN_ID, SubKegiatanMemberPeer::REKENING_CODE, SubKegiatanMemberPeer::DETAIL_NAME, SubKegiatanMemberPeer::VOLUME, SubKegiatanMemberPeer::KETERANGAN_KOEFISIEN, SubKegiatanMemberPeer::KOEFISIEN, SubKegiatanMemberPeer::SUBTITLE, SubKegiatanMemberPeer::KOMPONEN_HARGA, SubKegiatanMemberPeer::KOMPONEN_HARGA_AWAL, SubKegiatanMemberPeer::KOMPONEN_NAME, SubKegiatanMemberPeer::SATUAN, SubKegiatanMemberPeer::PAJAK, SubKegiatanMemberPeer::PARAM, SubKegiatanMemberPeer::FROM_KEGIATAN_MEMBER, SubKegiatanMemberPeer::TEMP_KOMPONEN_ID, ),
		BasePeer::TYPE_FIELDNAME => array ('sub_kegiatan_id', 'detail_no', 'komponen_id', 'rekening_code', 'detail_name', 'volume', 'keterangan_koefisien', 'koefisien', 'subtitle', 'komponen_harga', 'komponen_harga_awal', 'komponen_name', 'satuan', 'pajak', 'param', 'from_kegiatan_member', 'temp_komponen_id', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('SubKegiatanId' => 0, 'DetailNo' => 1, 'KomponenId' => 2, 'RekeningCode' => 3, 'DetailName' => 4, 'Volume' => 5, 'KeteranganKoefisien' => 6, 'Koefisien' => 7, 'Subtitle' => 8, 'KomponenHarga' => 9, 'KomponenHargaAwal' => 10, 'KomponenName' => 11, 'Satuan' => 12, 'Pajak' => 13, 'Param' => 14, 'FromKegiatanMember' => 15, 'TempKomponenId' => 16, ),
		BasePeer::TYPE_COLNAME => array (SubKegiatanMemberPeer::SUB_KEGIATAN_ID => 0, SubKegiatanMemberPeer::DETAIL_NO => 1, SubKegiatanMemberPeer::KOMPONEN_ID => 2, SubKegiatanMemberPeer::REKENING_CODE => 3, SubKegiatanMemberPeer::DETAIL_NAME => 4, SubKegiatanMemberPeer::VOLUME => 5, SubKegiatanMemberPeer::KETERANGAN_KOEFISIEN => 6, SubKegiatanMemberPeer::KOEFISIEN => 7, SubKegiatanMemberPeer::SUBTITLE => 8, SubKegiatanMemberPeer::KOMPONEN_HARGA => 9, SubKegiatanMemberPeer::KOMPONEN_HARGA_AWAL => 10, SubKegiatanMemberPeer::KOMPONEN_NAME => 11, SubKegiatanMemberPeer::SATUAN => 12, SubKegiatanMemberPeer::PAJAK => 13, SubKegiatanMemberPeer::PARAM => 14, SubKegiatanMemberPeer::FROM_KEGIATAN_MEMBER => 15, SubKegiatanMemberPeer::TEMP_KOMPONEN_ID => 16, ),
		BasePeer::TYPE_FIELDNAME => array ('sub_kegiatan_id' => 0, 'detail_no' => 1, 'komponen_id' => 2, 'rekening_code' => 3, 'detail_name' => 4, 'volume' => 5, 'keterangan_koefisien' => 6, 'koefisien' => 7, 'subtitle' => 8, 'komponen_harga' => 9, 'komponen_harga_awal' => 10, 'komponen_name' => 11, 'satuan' => 12, 'pajak' => 13, 'param' => 14, 'from_kegiatan_member' => 15, 'temp_komponen_id' => 16, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/SubKegiatanMemberMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.SubKegiatanMemberMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = SubKegiatanMemberPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(SubKegiatanMemberPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(SubKegiatanMemberPeer::SUB_KEGIATAN_ID);

		$criteria->addSelectColumn(SubKegiatanMemberPeer::DETAIL_NO);

		$criteria->addSelectColumn(SubKegiatanMemberPeer::KOMPONEN_ID);

		$criteria->addSelectColumn(SubKegiatanMemberPeer::REKENING_CODE);

		$criteria->addSelectColumn(SubKegiatanMemberPeer::DETAIL_NAME);

		$criteria->addSelectColumn(SubKegiatanMemberPeer::VOLUME);

		$criteria->addSelectColumn(SubKegiatanMemberPeer::KETERANGAN_KOEFISIEN);

		$criteria->addSelectColumn(SubKegiatanMemberPeer::KOEFISIEN);

		$criteria->addSelectColumn(SubKegiatanMemberPeer::SUBTITLE);

		$criteria->addSelectColumn(SubKegiatanMemberPeer::KOMPONEN_HARGA);

		$criteria->addSelectColumn(SubKegiatanMemberPeer::KOMPONEN_HARGA_AWAL);

		$criteria->addSelectColumn(SubKegiatanMemberPeer::KOMPONEN_NAME);

		$criteria->addSelectColumn(SubKegiatanMemberPeer::SATUAN);

		$criteria->addSelectColumn(SubKegiatanMemberPeer::PAJAK);

		$criteria->addSelectColumn(SubKegiatanMemberPeer::PARAM);

		$criteria->addSelectColumn(SubKegiatanMemberPeer::FROM_KEGIATAN_MEMBER);

		$criteria->addSelectColumn(SubKegiatanMemberPeer::TEMP_KOMPONEN_ID);

	}

	const COUNT = 'COUNT(*)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT *)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(SubKegiatanMemberPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(SubKegiatanMemberPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = SubKegiatanMemberPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = SubKegiatanMemberPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return SubKegiatanMemberPeer::populateObjects(SubKegiatanMemberPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			SubKegiatanMemberPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = SubKegiatanMemberPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return SubKegiatanMemberPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(SubKegiatanMemberPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(SubKegiatanMemberPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof SubKegiatanMember) {

			$criteria = $values->buildCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
												if(count($values) == count($values, COUNT_RECURSIVE))
			{
								$values = array($values);
			}
			$vals = array();
			foreach($values as $value)
			{

			}

		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(SubKegiatanMember $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(SubKegiatanMemberPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(SubKegiatanMemberPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(SubKegiatanMemberPeer::DATABASE_NAME, SubKegiatanMemberPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = SubKegiatanMemberPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

} 
if (Propel::isInit()) {
			try {
		BaseSubKegiatanMemberPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/SubKegiatanMemberMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.SubKegiatanMemberMapBuilder');
}
