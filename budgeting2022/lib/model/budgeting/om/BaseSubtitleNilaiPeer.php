<?php


abstract class BaseSubtitleNilaiPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.subtitle_nilai';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.SubtitleNilai';

	
	const NUM_COLUMNS = 9;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const UNIT_ID = 'ebudget.subtitle_nilai.UNIT_ID';

	
	const NILAI_1 = 'ebudget.subtitle_nilai.NILAI_1';

	
	const NILAI_2 = 'ebudget.subtitle_nilai.NILAI_2';

	
	const NILAI_3 = 'ebudget.subtitle_nilai.NILAI_3';

	
	const TOTAL = 'ebudget.subtitle_nilai.TOTAL';

	
	const KEGIATAN_CODE = 'ebudget.subtitle_nilai.KEGIATAN_CODE';

	
	const SUBTITLE = 'ebudget.subtitle_nilai.SUBTITLE';

	
	const KETERANGAN = 'ebudget.subtitle_nilai.KETERANGAN';

	
	const SUB_ID = 'ebudget.subtitle_nilai.SUB_ID';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('UnitId', 'Nilai1', 'Nilai2', 'Nilai3', 'Total', 'KegiatanCode', 'Subtitle', 'Keterangan', 'SubId', ),
		BasePeer::TYPE_COLNAME => array (SubtitleNilaiPeer::UNIT_ID, SubtitleNilaiPeer::NILAI_1, SubtitleNilaiPeer::NILAI_2, SubtitleNilaiPeer::NILAI_3, SubtitleNilaiPeer::TOTAL, SubtitleNilaiPeer::KEGIATAN_CODE, SubtitleNilaiPeer::SUBTITLE, SubtitleNilaiPeer::KETERANGAN, SubtitleNilaiPeer::SUB_ID, ),
		BasePeer::TYPE_FIELDNAME => array ('unit_id', 'nilai_1', 'nilai_2', 'nilai_3', 'total', 'kegiatan_code', 'subtitle', 'keterangan', 'sub_id', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('UnitId' => 0, 'Nilai1' => 1, 'Nilai2' => 2, 'Nilai3' => 3, 'Total' => 4, 'KegiatanCode' => 5, 'Subtitle' => 6, 'Keterangan' => 7, 'SubId' => 8, ),
		BasePeer::TYPE_COLNAME => array (SubtitleNilaiPeer::UNIT_ID => 0, SubtitleNilaiPeer::NILAI_1 => 1, SubtitleNilaiPeer::NILAI_2 => 2, SubtitleNilaiPeer::NILAI_3 => 3, SubtitleNilaiPeer::TOTAL => 4, SubtitleNilaiPeer::KEGIATAN_CODE => 5, SubtitleNilaiPeer::SUBTITLE => 6, SubtitleNilaiPeer::KETERANGAN => 7, SubtitleNilaiPeer::SUB_ID => 8, ),
		BasePeer::TYPE_FIELDNAME => array ('unit_id' => 0, 'nilai_1' => 1, 'nilai_2' => 2, 'nilai_3' => 3, 'total' => 4, 'kegiatan_code' => 5, 'subtitle' => 6, 'keterangan' => 7, 'sub_id' => 8, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/SubtitleNilaiMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.SubtitleNilaiMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = SubtitleNilaiPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(SubtitleNilaiPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(SubtitleNilaiPeer::UNIT_ID);

		$criteria->addSelectColumn(SubtitleNilaiPeer::NILAI_1);

		$criteria->addSelectColumn(SubtitleNilaiPeer::NILAI_2);

		$criteria->addSelectColumn(SubtitleNilaiPeer::NILAI_3);

		$criteria->addSelectColumn(SubtitleNilaiPeer::TOTAL);

		$criteria->addSelectColumn(SubtitleNilaiPeer::KEGIATAN_CODE);

		$criteria->addSelectColumn(SubtitleNilaiPeer::SUBTITLE);

		$criteria->addSelectColumn(SubtitleNilaiPeer::KETERANGAN);

		$criteria->addSelectColumn(SubtitleNilaiPeer::SUB_ID);

	}

	const COUNT = 'COUNT(*)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT *)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(SubtitleNilaiPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(SubtitleNilaiPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = SubtitleNilaiPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = SubtitleNilaiPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return SubtitleNilaiPeer::populateObjects(SubtitleNilaiPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			SubtitleNilaiPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = SubtitleNilaiPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return SubtitleNilaiPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(SubtitleNilaiPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(SubtitleNilaiPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof SubtitleNilai) {

			$criteria = $values->buildCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
												if(count($values) == count($values, COUNT_RECURSIVE))
			{
								$values = array($values);
			}
			$vals = array();
			foreach($values as $value)
			{

			}

		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(SubtitleNilai $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(SubtitleNilaiPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(SubtitleNilaiPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(SubtitleNilaiPeer::DATABASE_NAME, SubtitleNilaiPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = SubtitleNilaiPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

} 
if (Propel::isInit()) {
			try {
		BaseSubtitleNilaiPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/SubtitleNilaiMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.SubtitleNilaiMapBuilder');
}
