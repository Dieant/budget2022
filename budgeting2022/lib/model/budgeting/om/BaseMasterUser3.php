<?php


abstract class BaseMasterUser3 extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $user_id;


	
	protected $user_name;


	
	protected $user_default_password;


	
	protected $user_password;


	
	protected $ip_address;


	
	protected $waktu_access;


	
	protected $user_enable;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getUserId()
	{

		return $this->user_id;
	}

	
	public function getUserName()
	{

		return $this->user_name;
	}

	
	public function getUserDefaultPassword()
	{

		return $this->user_default_password;
	}

	
	public function getUserPassword()
	{

		return $this->user_password;
	}

	
	public function getIpAddress()
	{

		return $this->ip_address;
	}

	
	public function getWaktuAccess($format = 'Y-m-d H:i:s')
	{

		if ($this->waktu_access === null || $this->waktu_access === '') {
			return null;
		} elseif (!is_int($this->waktu_access)) {
						$ts = strtotime($this->waktu_access);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [waktu_access] as date/time value: " . var_export($this->waktu_access, true));
			}
		} else {
			$ts = $this->waktu_access;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getUserEnable()
	{

		return $this->user_enable;
	}

	
	public function setUserId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->user_id !== $v) {
			$this->user_id = $v;
			$this->modifiedColumns[] = MasterUser3Peer::USER_ID;
		}

	} 
	
	public function setUserName($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->user_name !== $v) {
			$this->user_name = $v;
			$this->modifiedColumns[] = MasterUser3Peer::USER_NAME;
		}

	} 
	
	public function setUserDefaultPassword($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->user_default_password !== $v) {
			$this->user_default_password = $v;
			$this->modifiedColumns[] = MasterUser3Peer::USER_DEFAULT_PASSWORD;
		}

	} 
	
	public function setUserPassword($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->user_password !== $v) {
			$this->user_password = $v;
			$this->modifiedColumns[] = MasterUser3Peer::USER_PASSWORD;
		}

	} 
	
	public function setIpAddress($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->ip_address !== $v) {
			$this->ip_address = $v;
			$this->modifiedColumns[] = MasterUser3Peer::IP_ADDRESS;
		}

	} 
	
	public function setWaktuAccess($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [waktu_access] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->waktu_access !== $ts) {
			$this->waktu_access = $ts;
			$this->modifiedColumns[] = MasterUser3Peer::WAKTU_ACCESS;
		}

	} 
	
	public function setUserEnable($v)
	{

		if ($this->user_enable !== $v) {
			$this->user_enable = $v;
			$this->modifiedColumns[] = MasterUser3Peer::USER_ENABLE;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->user_id = $rs->getString($startcol + 0);

			$this->user_name = $rs->getString($startcol + 1);

			$this->user_default_password = $rs->getString($startcol + 2);

			$this->user_password = $rs->getString($startcol + 3);

			$this->ip_address = $rs->getString($startcol + 4);

			$this->waktu_access = $rs->getTimestamp($startcol + 5, null);

			$this->user_enable = $rs->getBoolean($startcol + 6);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 7; 
		} catch (Exception $e) {
			throw new PropelException("Error populating MasterUser3 object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(MasterUser3Peer::DATABASE_NAME);
		}

		try {
			$con->begin();
			MasterUser3Peer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(MasterUser3Peer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = MasterUser3Peer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += MasterUser3Peer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = MasterUser3Peer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = MasterUser3Peer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getUserId();
				break;
			case 1:
				return $this->getUserName();
				break;
			case 2:
				return $this->getUserDefaultPassword();
				break;
			case 3:
				return $this->getUserPassword();
				break;
			case 4:
				return $this->getIpAddress();
				break;
			case 5:
				return $this->getWaktuAccess();
				break;
			case 6:
				return $this->getUserEnable();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = MasterUser3Peer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getUserId(),
			$keys[1] => $this->getUserName(),
			$keys[2] => $this->getUserDefaultPassword(),
			$keys[3] => $this->getUserPassword(),
			$keys[4] => $this->getIpAddress(),
			$keys[5] => $this->getWaktuAccess(),
			$keys[6] => $this->getUserEnable(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = MasterUser3Peer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setUserId($value);
				break;
			case 1:
				$this->setUserName($value);
				break;
			case 2:
				$this->setUserDefaultPassword($value);
				break;
			case 3:
				$this->setUserPassword($value);
				break;
			case 4:
				$this->setIpAddress($value);
				break;
			case 5:
				$this->setWaktuAccess($value);
				break;
			case 6:
				$this->setUserEnable($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = MasterUser3Peer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setUserId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setUserName($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setUserDefaultPassword($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setUserPassword($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setIpAddress($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setWaktuAccess($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setUserEnable($arr[$keys[6]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(MasterUser3Peer::DATABASE_NAME);

		if ($this->isColumnModified(MasterUser3Peer::USER_ID)) $criteria->add(MasterUser3Peer::USER_ID, $this->user_id);
		if ($this->isColumnModified(MasterUser3Peer::USER_NAME)) $criteria->add(MasterUser3Peer::USER_NAME, $this->user_name);
		if ($this->isColumnModified(MasterUser3Peer::USER_DEFAULT_PASSWORD)) $criteria->add(MasterUser3Peer::USER_DEFAULT_PASSWORD, $this->user_default_password);
		if ($this->isColumnModified(MasterUser3Peer::USER_PASSWORD)) $criteria->add(MasterUser3Peer::USER_PASSWORD, $this->user_password);
		if ($this->isColumnModified(MasterUser3Peer::IP_ADDRESS)) $criteria->add(MasterUser3Peer::IP_ADDRESS, $this->ip_address);
		if ($this->isColumnModified(MasterUser3Peer::WAKTU_ACCESS)) $criteria->add(MasterUser3Peer::WAKTU_ACCESS, $this->waktu_access);
		if ($this->isColumnModified(MasterUser3Peer::USER_ENABLE)) $criteria->add(MasterUser3Peer::USER_ENABLE, $this->user_enable);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(MasterUser3Peer::DATABASE_NAME);

		$criteria->add(MasterUser3Peer::USER_ID, $this->user_id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getUserId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setUserId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setUserName($this->user_name);

		$copyObj->setUserDefaultPassword($this->user_default_password);

		$copyObj->setUserPassword($this->user_password);

		$copyObj->setIpAddress($this->ip_address);

		$copyObj->setWaktuAccess($this->waktu_access);

		$copyObj->setUserEnable($this->user_enable);


		$copyObj->setNew(true);

		$copyObj->setUserId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new MasterUser3Peer();
		}
		return self::$peer;
	}

} 