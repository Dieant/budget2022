<?php


abstract class BaseKomponenPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.komponen';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.Komponen';

	
	const NUM_COLUMNS = 38;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const KOMPONEN_ID = 'ebudget.komponen.KOMPONEN_ID';

	
	const SATUAN = 'ebudget.komponen.SATUAN';

	
	const KOMPONEN_NAME = 'ebudget.komponen.KOMPONEN_NAME';

	
	const SHSD_ID = 'ebudget.komponen.SHSD_ID';

	
	const KOMPONEN_HARGA = 'ebudget.komponen.KOMPONEN_HARGA';

	
	const KOMPONEN_SHOW = 'ebudget.komponen.KOMPONEN_SHOW';

	
	const IP_ADDRESS = 'ebudget.komponen.IP_ADDRESS';

	
	const WAKTU_ACCESS = 'ebudget.komponen.WAKTU_ACCESS';

	
	const KOMPONEN_TIPE = 'ebudget.komponen.KOMPONEN_TIPE';

	
	const KOMPONEN_CONFIRMED = 'ebudget.komponen.KOMPONEN_CONFIRMED';

	
	const KOMPONEN_NON_PAJAK = 'ebudget.komponen.KOMPONEN_NON_PAJAK';

	
	const USER_ID = 'ebudget.komponen.USER_ID';

	
	const REKENING = 'ebudget.komponen.REKENING';

	
	const KELOMPOK = 'ebudget.komponen.KELOMPOK';

	
	const PEMELIHARAAN = 'ebudget.komponen.PEMELIHARAAN';

	
	const REK_UPAH = 'ebudget.komponen.REK_UPAH';

	
	const REK_BAHAN = 'ebudget.komponen.REK_BAHAN';

	
	const REK_SEWA = 'ebudget.komponen.REK_SEWA';

	
	const DESKRIPSI = 'ebudget.komponen.DESKRIPSI';

	
	const STATUS_MASUK = 'ebudget.komponen.STATUS_MASUK';

	
	const RKA_MEMBER = 'ebudget.komponen.RKA_MEMBER';

	
	const MAINTENANCE = 'ebudget.komponen.MAINTENANCE';

	
	const IS_POTONG_BPJS = 'ebudget.komponen.IS_POTONG_BPJS';

	
	const IS_IURAN_BPJS = 'ebudget.komponen.IS_IURAN_BPJS';

	
	const USULAN_SKPD = 'ebudget.komponen.USULAN_SKPD';

	
	const TAHAP = 'ebudget.komponen.TAHAP';

	
	const IS_EST_FISIK = 'ebudget.komponen.IS_EST_FISIK';

	
	const AKRUAL_CODE = 'ebudget.komponen.AKRUAL_CODE';

	
	const KODE_AKRUAL_KOMPONEN_PENYUSUN = 'ebudget.komponen.KODE_AKRUAL_KOMPONEN_PENYUSUN';

	
	const KOMPONEN_TIPE2 = 'ebudget.komponen.KOMPONEN_TIPE2';

	
	const IS_SURVEY_BP = 'ebudget.komponen.IS_SURVEY_BP';

	
	const KOMPONEN_HARGA_BULAT = 'ebudget.komponen.KOMPONEN_HARGA_BULAT';

	
	const IS_IURAN_JKN = 'ebudget.komponen.IS_IURAN_JKN';

	
	const IS_IURAN_JKK = 'ebudget.komponen.IS_IURAN_JKK';

	
	const IS_IURAN_JK = 'ebudget.komponen.IS_IURAN_JK';

	
	const IS_NARSUM = 'ebudget.komponen.IS_NARSUM';

	
	const IS_RAB = 'ebudget.komponen.IS_RAB';

	
	const IS_BBM = 'ebudget.komponen.IS_BBM';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('KomponenId', 'Satuan', 'KomponenName', 'ShsdId', 'KomponenHarga', 'KomponenShow', 'IpAddress', 'WaktuAccess', 'KomponenTipe', 'KomponenConfirmed', 'KomponenNonPajak', 'UserId', 'Rekening', 'Kelompok', 'Pemeliharaan', 'RekUpah', 'RekBahan', 'RekSewa', 'Deskripsi', 'StatusMasuk', 'RkaMember', 'Maintenance', 'IsPotongBpjs', 'IsIuranBpjs', 'UsulanSkpd', 'Tahap', 'IsEstFisik', 'AkrualCode', 'KodeAkrualKomponenPenyusun', 'KomponenTipe2', 'IsSurveyBp', 'KomponenHargaBulat', 'IsIuranJkn', 'IsIuranJkk', 'IsIuranJk', 'IsNarsum', 'IsRab', 'IsBbm', ),
		BasePeer::TYPE_COLNAME => array (KomponenPeer::KOMPONEN_ID, KomponenPeer::SATUAN, KomponenPeer::KOMPONEN_NAME, KomponenPeer::SHSD_ID, KomponenPeer::KOMPONEN_HARGA, KomponenPeer::KOMPONEN_SHOW, KomponenPeer::IP_ADDRESS, KomponenPeer::WAKTU_ACCESS, KomponenPeer::KOMPONEN_TIPE, KomponenPeer::KOMPONEN_CONFIRMED, KomponenPeer::KOMPONEN_NON_PAJAK, KomponenPeer::USER_ID, KomponenPeer::REKENING, KomponenPeer::KELOMPOK, KomponenPeer::PEMELIHARAAN, KomponenPeer::REK_UPAH, KomponenPeer::REK_BAHAN, KomponenPeer::REK_SEWA, KomponenPeer::DESKRIPSI, KomponenPeer::STATUS_MASUK, KomponenPeer::RKA_MEMBER, KomponenPeer::MAINTENANCE, KomponenPeer::IS_POTONG_BPJS, KomponenPeer::IS_IURAN_BPJS, KomponenPeer::USULAN_SKPD, KomponenPeer::TAHAP, KomponenPeer::IS_EST_FISIK, KomponenPeer::AKRUAL_CODE, KomponenPeer::KODE_AKRUAL_KOMPONEN_PENYUSUN, KomponenPeer::KOMPONEN_TIPE2, KomponenPeer::IS_SURVEY_BP, KomponenPeer::KOMPONEN_HARGA_BULAT, KomponenPeer::IS_IURAN_JKN, KomponenPeer::IS_IURAN_JKK, KomponenPeer::IS_IURAN_JK, KomponenPeer::IS_NARSUM, KomponenPeer::IS_RAB, KomponenPeer::IS_BBM, ),
		BasePeer::TYPE_FIELDNAME => array ('komponen_id', 'satuan', 'komponen_name', 'shsd_id', 'komponen_harga', 'komponen_show', 'ip_address', 'waktu_access', 'komponen_tipe', 'komponen_confirmed', 'komponen_non_pajak', 'user_id', 'rekening', 'kelompok', 'pemeliharaan', 'rek_upah', 'rek_bahan', 'rek_sewa', 'deskripsi', 'status_masuk', 'rka_member', 'maintenance', 'is_potong_bpjs', 'is_iuran_bpjs', 'usulan_skpd', 'tahap', 'is_est_fisik', 'akrual_code', 'kode_akrual_komponen_penyusun', 'komponen_tipe2', 'is_survey_bp', 'komponen_harga_bulat', 'is_iuran_jkn', 'is_iuran_jkk', 'is_iuran_jk', 'is_narsum', 'is_rab', 'is_bbm', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('KomponenId' => 0, 'Satuan' => 1, 'KomponenName' => 2, 'ShsdId' => 3, 'KomponenHarga' => 4, 'KomponenShow' => 5, 'IpAddress' => 6, 'WaktuAccess' => 7, 'KomponenTipe' => 8, 'KomponenConfirmed' => 9, 'KomponenNonPajak' => 10, 'UserId' => 11, 'Rekening' => 12, 'Kelompok' => 13, 'Pemeliharaan' => 14, 'RekUpah' => 15, 'RekBahan' => 16, 'RekSewa' => 17, 'Deskripsi' => 18, 'StatusMasuk' => 19, 'RkaMember' => 20, 'Maintenance' => 21, 'IsPotongBpjs' => 22, 'IsIuranBpjs' => 23, 'UsulanSkpd' => 24, 'Tahap' => 25, 'IsEstFisik' => 26, 'AkrualCode' => 27, 'KodeAkrualKomponenPenyusun' => 28, 'KomponenTipe2' => 29, 'IsSurveyBp' => 30, 'KomponenHargaBulat' => 31, 'IsIuranJkn' => 32, 'IsIuranJkk' => 33, 'IsIuranJk' => 34, 'IsNarsum' => 35, 'IsRab' => 36, 'IsBbm' => 37, ),
		BasePeer::TYPE_COLNAME => array (KomponenPeer::KOMPONEN_ID => 0, KomponenPeer::SATUAN => 1, KomponenPeer::KOMPONEN_NAME => 2, KomponenPeer::SHSD_ID => 3, KomponenPeer::KOMPONEN_HARGA => 4, KomponenPeer::KOMPONEN_SHOW => 5, KomponenPeer::IP_ADDRESS => 6, KomponenPeer::WAKTU_ACCESS => 7, KomponenPeer::KOMPONEN_TIPE => 8, KomponenPeer::KOMPONEN_CONFIRMED => 9, KomponenPeer::KOMPONEN_NON_PAJAK => 10, KomponenPeer::USER_ID => 11, KomponenPeer::REKENING => 12, KomponenPeer::KELOMPOK => 13, KomponenPeer::PEMELIHARAAN => 14, KomponenPeer::REK_UPAH => 15, KomponenPeer::REK_BAHAN => 16, KomponenPeer::REK_SEWA => 17, KomponenPeer::DESKRIPSI => 18, KomponenPeer::STATUS_MASUK => 19, KomponenPeer::RKA_MEMBER => 20, KomponenPeer::MAINTENANCE => 21, KomponenPeer::IS_POTONG_BPJS => 22, KomponenPeer::IS_IURAN_BPJS => 23, KomponenPeer::USULAN_SKPD => 24, KomponenPeer::TAHAP => 25, KomponenPeer::IS_EST_FISIK => 26, KomponenPeer::AKRUAL_CODE => 27, KomponenPeer::KODE_AKRUAL_KOMPONEN_PENYUSUN => 28, KomponenPeer::KOMPONEN_TIPE2 => 29, KomponenPeer::IS_SURVEY_BP => 30, KomponenPeer::KOMPONEN_HARGA_BULAT => 31, KomponenPeer::IS_IURAN_JKN => 32, KomponenPeer::IS_IURAN_JKK => 33, KomponenPeer::IS_IURAN_JK => 34, KomponenPeer::IS_NARSUM => 35, KomponenPeer::IS_RAB => 36, KomponenPeer::IS_BBM => 37, ),
		BasePeer::TYPE_FIELDNAME => array ('komponen_id' => 0, 'satuan' => 1, 'komponen_name' => 2, 'shsd_id' => 3, 'komponen_harga' => 4, 'komponen_show' => 5, 'ip_address' => 6, 'waktu_access' => 7, 'komponen_tipe' => 8, 'komponen_confirmed' => 9, 'komponen_non_pajak' => 10, 'user_id' => 11, 'rekening' => 12, 'kelompok' => 13, 'pemeliharaan' => 14, 'rek_upah' => 15, 'rek_bahan' => 16, 'rek_sewa' => 17, 'deskripsi' => 18, 'status_masuk' => 19, 'rka_member' => 20, 'maintenance' => 21, 'is_potong_bpjs' => 22, 'is_iuran_bpjs' => 23, 'usulan_skpd' => 24, 'tahap' => 25, 'is_est_fisik' => 26, 'akrual_code' => 27, 'kode_akrual_komponen_penyusun' => 28, 'komponen_tipe2' => 29, 'is_survey_bp' => 30, 'komponen_harga_bulat' => 31, 'is_iuran_jkn' => 32, 'is_iuran_jkk' => 33, 'is_iuran_jk' => 34, 'is_narsum' => 35, 'is_rab' => 36, 'is_bbm' => 37, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/KomponenMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.KomponenMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = KomponenPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(KomponenPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(KomponenPeer::KOMPONEN_ID);

		$criteria->addSelectColumn(KomponenPeer::SATUAN);

		$criteria->addSelectColumn(KomponenPeer::KOMPONEN_NAME);

		$criteria->addSelectColumn(KomponenPeer::SHSD_ID);

		$criteria->addSelectColumn(KomponenPeer::KOMPONEN_HARGA);

		$criteria->addSelectColumn(KomponenPeer::KOMPONEN_SHOW);

		$criteria->addSelectColumn(KomponenPeer::IP_ADDRESS);

		$criteria->addSelectColumn(KomponenPeer::WAKTU_ACCESS);

		$criteria->addSelectColumn(KomponenPeer::KOMPONEN_TIPE);

		$criteria->addSelectColumn(KomponenPeer::KOMPONEN_CONFIRMED);

		$criteria->addSelectColumn(KomponenPeer::KOMPONEN_NON_PAJAK);

		$criteria->addSelectColumn(KomponenPeer::USER_ID);

		$criteria->addSelectColumn(KomponenPeer::REKENING);

		$criteria->addSelectColumn(KomponenPeer::KELOMPOK);

		$criteria->addSelectColumn(KomponenPeer::PEMELIHARAAN);

		$criteria->addSelectColumn(KomponenPeer::REK_UPAH);

		$criteria->addSelectColumn(KomponenPeer::REK_BAHAN);

		$criteria->addSelectColumn(KomponenPeer::REK_SEWA);

		$criteria->addSelectColumn(KomponenPeer::DESKRIPSI);

		$criteria->addSelectColumn(KomponenPeer::STATUS_MASUK);

		$criteria->addSelectColumn(KomponenPeer::RKA_MEMBER);

		$criteria->addSelectColumn(KomponenPeer::MAINTENANCE);

		$criteria->addSelectColumn(KomponenPeer::IS_POTONG_BPJS);

		$criteria->addSelectColumn(KomponenPeer::IS_IURAN_BPJS);

		$criteria->addSelectColumn(KomponenPeer::USULAN_SKPD);

		$criteria->addSelectColumn(KomponenPeer::TAHAP);

		$criteria->addSelectColumn(KomponenPeer::IS_EST_FISIK);

		$criteria->addSelectColumn(KomponenPeer::AKRUAL_CODE);

		$criteria->addSelectColumn(KomponenPeer::KODE_AKRUAL_KOMPONEN_PENYUSUN);

		$criteria->addSelectColumn(KomponenPeer::KOMPONEN_TIPE2);

		$criteria->addSelectColumn(KomponenPeer::IS_SURVEY_BP);

		$criteria->addSelectColumn(KomponenPeer::KOMPONEN_HARGA_BULAT);

		$criteria->addSelectColumn(KomponenPeer::IS_IURAN_JKN);

		$criteria->addSelectColumn(KomponenPeer::IS_IURAN_JKK);

		$criteria->addSelectColumn(KomponenPeer::IS_IURAN_JK);

		$criteria->addSelectColumn(KomponenPeer::IS_NARSUM);

		$criteria->addSelectColumn(KomponenPeer::IS_RAB);

		$criteria->addSelectColumn(KomponenPeer::IS_BBM);

	}

	const COUNT = 'COUNT(ebudget.komponen.KOMPONEN_ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.komponen.KOMPONEN_ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(KomponenPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(KomponenPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = KomponenPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = KomponenPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return KomponenPeer::populateObjects(KomponenPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			KomponenPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = KomponenPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return KomponenPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(KomponenPeer::KOMPONEN_ID);
			$selectCriteria->add(KomponenPeer::KOMPONEN_ID, $criteria->remove(KomponenPeer::KOMPONEN_ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(KomponenPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(KomponenPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof Komponen) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(KomponenPeer::KOMPONEN_ID, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(Komponen $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(KomponenPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(KomponenPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(KomponenPeer::DATABASE_NAME, KomponenPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = KomponenPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(KomponenPeer::DATABASE_NAME);

		$criteria->add(KomponenPeer::KOMPONEN_ID, $pk);


		$v = KomponenPeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(KomponenPeer::KOMPONEN_ID, $pks, Criteria::IN);
			$objs = KomponenPeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BaseKomponenPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/KomponenMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.KomponenMapBuilder');
}
