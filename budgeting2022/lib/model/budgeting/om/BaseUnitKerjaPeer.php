<?php


abstract class BaseUnitKerjaPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'unit_kerja';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.UnitKerja';

	
	const NUM_COLUMNS = 12;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const UNIT_ID = 'unit_kerja.UNIT_ID';

	
	const KELOMPOK_ID = 'unit_kerja.KELOMPOK_ID';

	
	const UNIT_NAME = 'unit_kerja.UNIT_NAME';

	
	const UNIT_ADDRESS = 'unit_kerja.UNIT_ADDRESS';

	
	const KEPALA_NAMA = 'unit_kerja.KEPALA_NAMA';

	
	const KEPALA_PANGKAT = 'unit_kerja.KEPALA_PANGKAT';

	
	const KEPALA_NIP = 'unit_kerja.KEPALA_NIP';

	
	const KODE_PERMEN = 'unit_kerja.KODE_PERMEN';

	
	const PAGU = 'unit_kerja.PAGU';

	
	const JUMLAH_PNS = 'unit_kerja.JUMLAH_PNS';

	
	const JUMLAH_HONDA = 'unit_kerja.JUMLAH_HONDA';

	
	const JUMLAH_NONPNS = 'unit_kerja.JUMLAH_NONPNS';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('UnitId', 'KelompokId', 'UnitName', 'UnitAddress', 'KepalaNama', 'KepalaPangkat', 'KepalaNip', 'KodePermen', 'Pagu', 'JumlahPns', 'JumlahHonda', 'JumlahNonpns', ),
		BasePeer::TYPE_COLNAME => array (UnitKerjaPeer::UNIT_ID, UnitKerjaPeer::KELOMPOK_ID, UnitKerjaPeer::UNIT_NAME, UnitKerjaPeer::UNIT_ADDRESS, UnitKerjaPeer::KEPALA_NAMA, UnitKerjaPeer::KEPALA_PANGKAT, UnitKerjaPeer::KEPALA_NIP, UnitKerjaPeer::KODE_PERMEN, UnitKerjaPeer::PAGU, UnitKerjaPeer::JUMLAH_PNS, UnitKerjaPeer::JUMLAH_HONDA, UnitKerjaPeer::JUMLAH_NONPNS, ),
		BasePeer::TYPE_FIELDNAME => array ('unit_id', 'kelompok_id', 'unit_name', 'unit_address', 'kepala_nama', 'kepala_pangkat', 'kepala_nip', 'kode_permen', 'pagu', 'jumlah_pns', 'jumlah_honda', 'jumlah_nonpns', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('UnitId' => 0, 'KelompokId' => 1, 'UnitName' => 2, 'UnitAddress' => 3, 'KepalaNama' => 4, 'KepalaPangkat' => 5, 'KepalaNip' => 6, 'KodePermen' => 7, 'Pagu' => 8, 'JumlahPns' => 9, 'JumlahHonda' => 10, 'JumlahNonpns' => 11, ),
		BasePeer::TYPE_COLNAME => array (UnitKerjaPeer::UNIT_ID => 0, UnitKerjaPeer::KELOMPOK_ID => 1, UnitKerjaPeer::UNIT_NAME => 2, UnitKerjaPeer::UNIT_ADDRESS => 3, UnitKerjaPeer::KEPALA_NAMA => 4, UnitKerjaPeer::KEPALA_PANGKAT => 5, UnitKerjaPeer::KEPALA_NIP => 6, UnitKerjaPeer::KODE_PERMEN => 7, UnitKerjaPeer::PAGU => 8, UnitKerjaPeer::JUMLAH_PNS => 9, UnitKerjaPeer::JUMLAH_HONDA => 10, UnitKerjaPeer::JUMLAH_NONPNS => 11, ),
		BasePeer::TYPE_FIELDNAME => array ('unit_id' => 0, 'kelompok_id' => 1, 'unit_name' => 2, 'unit_address' => 3, 'kepala_nama' => 4, 'kepala_pangkat' => 5, 'kepala_nip' => 6, 'kode_permen' => 7, 'pagu' => 8, 'jumlah_pns' => 9, 'jumlah_honda' => 10, 'jumlah_nonpns' => 11, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/UnitKerjaMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.UnitKerjaMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = UnitKerjaPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(UnitKerjaPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(UnitKerjaPeer::UNIT_ID);

		$criteria->addSelectColumn(UnitKerjaPeer::KELOMPOK_ID);

		$criteria->addSelectColumn(UnitKerjaPeer::UNIT_NAME);

		$criteria->addSelectColumn(UnitKerjaPeer::UNIT_ADDRESS);

		$criteria->addSelectColumn(UnitKerjaPeer::KEPALA_NAMA);

		$criteria->addSelectColumn(UnitKerjaPeer::KEPALA_PANGKAT);

		$criteria->addSelectColumn(UnitKerjaPeer::KEPALA_NIP);

		$criteria->addSelectColumn(UnitKerjaPeer::KODE_PERMEN);

		$criteria->addSelectColumn(UnitKerjaPeer::PAGU);

		$criteria->addSelectColumn(UnitKerjaPeer::JUMLAH_PNS);

		$criteria->addSelectColumn(UnitKerjaPeer::JUMLAH_HONDA);

		$criteria->addSelectColumn(UnitKerjaPeer::JUMLAH_NONPNS);

	}

	const COUNT = 'COUNT(unit_kerja.UNIT_ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT unit_kerja.UNIT_ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(UnitKerjaPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(UnitKerjaPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = UnitKerjaPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = UnitKerjaPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return UnitKerjaPeer::populateObjects(UnitKerjaPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			UnitKerjaPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = UnitKerjaPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}

	
	public static function doCountJoinKelompokDinas(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(UnitKerjaPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(UnitKerjaPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(UnitKerjaPeer::KELOMPOK_ID, KelompokDinasPeer::KELOMPOK_ID);

		$rs = UnitKerjaPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doSelectJoinKelompokDinas(Criteria $c, $con = null)
	{
		$c = clone $c;

				if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		UnitKerjaPeer::addSelectColumns($c);
		$startcol = (UnitKerjaPeer::NUM_COLUMNS - UnitKerjaPeer::NUM_LAZY_LOAD_COLUMNS) + 1;
		KelompokDinasPeer::addSelectColumns($c);

		$c->addJoin(UnitKerjaPeer::KELOMPOK_ID, KelompokDinasPeer::KELOMPOK_ID);
		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = UnitKerjaPeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = KelompokDinasPeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj2 = new $cls();
			$obj2->hydrate($rs, $startcol);

			$newObject = true;
			foreach($results as $temp_obj1) {
				$temp_obj2 = $temp_obj1->getKelompokDinas(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
										$temp_obj2->addUnitKerja($obj1); 					break;
				}
			}
			if ($newObject) {
				$obj2->initUnitKerjas();
				$obj2->addUnitKerja($obj1); 			}
			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doCountJoinAll(Criteria $criteria, $distinct = false, $con = null)
	{
		$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(UnitKerjaPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(UnitKerjaPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(UnitKerjaPeer::KELOMPOK_ID, KelompokDinasPeer::KELOMPOK_ID);

		$rs = UnitKerjaPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doSelectJoinAll(Criteria $c, $con = null)
	{
		$c = clone $c;

				if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		UnitKerjaPeer::addSelectColumns($c);
		$startcol2 = (UnitKerjaPeer::NUM_COLUMNS - UnitKerjaPeer::NUM_LAZY_LOAD_COLUMNS) + 1;

		KelompokDinasPeer::addSelectColumns($c);
		$startcol3 = $startcol2 + KelompokDinasPeer::NUM_COLUMNS;

		$c->addJoin(UnitKerjaPeer::KELOMPOK_ID, KelompokDinasPeer::KELOMPOK_ID);

		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = UnitKerjaPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);


					
			$omClass = KelompokDinasPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj2 = new $cls();
			$obj2->hydrate($rs, $startcol2);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj2 = $temp_obj1->getKelompokDinas(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
					$temp_obj2->addUnitKerja($obj1); 					break;
				}
			}

			if ($newObject) {
				$obj2->initUnitKerjas();
				$obj2->addUnitKerja($obj1);
			}

			$results[] = $obj1;
		}
		return $results;
	}

	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return UnitKerjaPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(UnitKerjaPeer::UNIT_ID);
			$selectCriteria->add(UnitKerjaPeer::UNIT_ID, $criteria->remove(UnitKerjaPeer::UNIT_ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(UnitKerjaPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(UnitKerjaPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof UnitKerja) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(UnitKerjaPeer::UNIT_ID, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(UnitKerja $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(UnitKerjaPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(UnitKerjaPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(UnitKerjaPeer::DATABASE_NAME, UnitKerjaPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = UnitKerjaPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(UnitKerjaPeer::DATABASE_NAME);

		$criteria->add(UnitKerjaPeer::UNIT_ID, $pk);


		$v = UnitKerjaPeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(UnitKerjaPeer::UNIT_ID, $pks, Criteria::IN);
			$objs = UnitKerjaPeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BaseUnitKerjaPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/UnitKerjaMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.UnitKerjaMapBuilder');
}
