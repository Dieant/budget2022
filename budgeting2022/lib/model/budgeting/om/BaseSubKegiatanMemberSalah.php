<?php


abstract class BaseSubKegiatanMemberSalah extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $sub_kegiatan_id;


	
	protected $detail_no;


	
	protected $komponen_id;


	
	protected $rekening_code;


	
	protected $detail_name;


	
	protected $volume;


	
	protected $keterangan_koefisien;


	
	protected $koefisien;


	
	protected $subtitle;


	
	protected $komponen_harga;


	
	protected $komponen_harga_awal;


	
	protected $komponen_name;


	
	protected $satuan;


	
	protected $pajak = 0;


	
	protected $param;


	
	protected $from_kegiatan_member;


	
	protected $temp_komponen_id;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getSubKegiatanId()
	{

		return $this->sub_kegiatan_id;
	}

	
	public function getDetailNo()
	{

		return $this->detail_no;
	}

	
	public function getKomponenId()
	{

		return $this->komponen_id;
	}

	
	public function getRekeningCode()
	{

		return $this->rekening_code;
	}

	
	public function getDetailName()
	{

		return $this->detail_name;
	}

	
	public function getVolume()
	{

		return $this->volume;
	}

	
	public function getKeteranganKoefisien()
	{

		return $this->keterangan_koefisien;
	}

	
	public function getKoefisien()
	{

		return $this->koefisien;
	}

	
	public function getSubtitle()
	{

		return $this->subtitle;
	}

	
	public function getKomponenHarga()
	{

		return $this->komponen_harga;
	}

	
	public function getKomponenHargaAwal()
	{

		return $this->komponen_harga_awal;
	}

	
	public function getKomponenName()
	{

		return $this->komponen_name;
	}

	
	public function getSatuan()
	{

		return $this->satuan;
	}

	
	public function getPajak()
	{

		return $this->pajak;
	}

	
	public function getParam()
	{

		return $this->param;
	}

	
	public function getFromKegiatanMember()
	{

		return $this->from_kegiatan_member;
	}

	
	public function getTempKomponenId()
	{

		return $this->temp_komponen_id;
	}

	
	public function setSubKegiatanId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->sub_kegiatan_id !== $v) {
			$this->sub_kegiatan_id = $v;
			$this->modifiedColumns[] = SubKegiatanMemberSalahPeer::SUB_KEGIATAN_ID;
		}

	} 
	
	public function setDetailNo($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->detail_no !== $v) {
			$this->detail_no = $v;
			$this->modifiedColumns[] = SubKegiatanMemberSalahPeer::DETAIL_NO;
		}

	} 
	
	public function setKomponenId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->komponen_id !== $v) {
			$this->komponen_id = $v;
			$this->modifiedColumns[] = SubKegiatanMemberSalahPeer::KOMPONEN_ID;
		}

	} 
	
	public function setRekeningCode($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->rekening_code !== $v) {
			$this->rekening_code = $v;
			$this->modifiedColumns[] = SubKegiatanMemberSalahPeer::REKENING_CODE;
		}

	} 
	
	public function setDetailName($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->detail_name !== $v) {
			$this->detail_name = $v;
			$this->modifiedColumns[] = SubKegiatanMemberSalahPeer::DETAIL_NAME;
		}

	} 
	
	public function setVolume($v)
	{

		if ($this->volume !== $v) {
			$this->volume = $v;
			$this->modifiedColumns[] = SubKegiatanMemberSalahPeer::VOLUME;
		}

	} 
	
	public function setKeteranganKoefisien($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->keterangan_koefisien !== $v) {
			$this->keterangan_koefisien = $v;
			$this->modifiedColumns[] = SubKegiatanMemberSalahPeer::KETERANGAN_KOEFISIEN;
		}

	} 
	
	public function setKoefisien($v)
	{

		if ($this->koefisien !== $v) {
			$this->koefisien = $v;
			$this->modifiedColumns[] = SubKegiatanMemberSalahPeer::KOEFISIEN;
		}

	} 
	
	public function setSubtitle($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->subtitle !== $v) {
			$this->subtitle = $v;
			$this->modifiedColumns[] = SubKegiatanMemberSalahPeer::SUBTITLE;
		}

	} 
	
	public function setKomponenHarga($v)
	{

		if ($this->komponen_harga !== $v) {
			$this->komponen_harga = $v;
			$this->modifiedColumns[] = SubKegiatanMemberSalahPeer::KOMPONEN_HARGA;
		}

	} 
	
	public function setKomponenHargaAwal($v)
	{

		if ($this->komponen_harga_awal !== $v) {
			$this->komponen_harga_awal = $v;
			$this->modifiedColumns[] = SubKegiatanMemberSalahPeer::KOMPONEN_HARGA_AWAL;
		}

	} 
	
	public function setKomponenName($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->komponen_name !== $v) {
			$this->komponen_name = $v;
			$this->modifiedColumns[] = SubKegiatanMemberSalahPeer::KOMPONEN_NAME;
		}

	} 
	
	public function setSatuan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->satuan !== $v) {
			$this->satuan = $v;
			$this->modifiedColumns[] = SubKegiatanMemberSalahPeer::SATUAN;
		}

	} 
	
	public function setPajak($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->pajak !== $v || $v === 0) {
			$this->pajak = $v;
			$this->modifiedColumns[] = SubKegiatanMemberSalahPeer::PAJAK;
		}

	} 
	
	public function setParam($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->param !== $v) {
			$this->param = $v;
			$this->modifiedColumns[] = SubKegiatanMemberSalahPeer::PARAM;
		}

	} 
	
	public function setFromKegiatanMember($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->from_kegiatan_member !== $v) {
			$this->from_kegiatan_member = $v;
			$this->modifiedColumns[] = SubKegiatanMemberSalahPeer::FROM_KEGIATAN_MEMBER;
		}

	} 
	
	public function setTempKomponenId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->temp_komponen_id !== $v) {
			$this->temp_komponen_id = $v;
			$this->modifiedColumns[] = SubKegiatanMemberSalahPeer::TEMP_KOMPONEN_ID;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->sub_kegiatan_id = $rs->getString($startcol + 0);

			$this->detail_no = $rs->getInt($startcol + 1);

			$this->komponen_id = $rs->getString($startcol + 2);

			$this->rekening_code = $rs->getString($startcol + 3);

			$this->detail_name = $rs->getString($startcol + 4);

			$this->volume = $rs->getFloat($startcol + 5);

			$this->keterangan_koefisien = $rs->getString($startcol + 6);

			$this->koefisien = $rs->getFloat($startcol + 7);

			$this->subtitle = $rs->getString($startcol + 8);

			$this->komponen_harga = $rs->getFloat($startcol + 9);

			$this->komponen_harga_awal = $rs->getFloat($startcol + 10);

			$this->komponen_name = $rs->getString($startcol + 11);

			$this->satuan = $rs->getString($startcol + 12);

			$this->pajak = $rs->getInt($startcol + 13);

			$this->param = $rs->getString($startcol + 14);

			$this->from_kegiatan_member = $rs->getString($startcol + 15);

			$this->temp_komponen_id = $rs->getString($startcol + 16);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 17; 
		} catch (Exception $e) {
			throw new PropelException("Error populating SubKegiatanMemberSalah object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(SubKegiatanMemberSalahPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			SubKegiatanMemberSalahPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(SubKegiatanMemberSalahPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = SubKegiatanMemberSalahPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += SubKegiatanMemberSalahPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = SubKegiatanMemberSalahPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = SubKegiatanMemberSalahPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getSubKegiatanId();
				break;
			case 1:
				return $this->getDetailNo();
				break;
			case 2:
				return $this->getKomponenId();
				break;
			case 3:
				return $this->getRekeningCode();
				break;
			case 4:
				return $this->getDetailName();
				break;
			case 5:
				return $this->getVolume();
				break;
			case 6:
				return $this->getKeteranganKoefisien();
				break;
			case 7:
				return $this->getKoefisien();
				break;
			case 8:
				return $this->getSubtitle();
				break;
			case 9:
				return $this->getKomponenHarga();
				break;
			case 10:
				return $this->getKomponenHargaAwal();
				break;
			case 11:
				return $this->getKomponenName();
				break;
			case 12:
				return $this->getSatuan();
				break;
			case 13:
				return $this->getPajak();
				break;
			case 14:
				return $this->getParam();
				break;
			case 15:
				return $this->getFromKegiatanMember();
				break;
			case 16:
				return $this->getTempKomponenId();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = SubKegiatanMemberSalahPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getSubKegiatanId(),
			$keys[1] => $this->getDetailNo(),
			$keys[2] => $this->getKomponenId(),
			$keys[3] => $this->getRekeningCode(),
			$keys[4] => $this->getDetailName(),
			$keys[5] => $this->getVolume(),
			$keys[6] => $this->getKeteranganKoefisien(),
			$keys[7] => $this->getKoefisien(),
			$keys[8] => $this->getSubtitle(),
			$keys[9] => $this->getKomponenHarga(),
			$keys[10] => $this->getKomponenHargaAwal(),
			$keys[11] => $this->getKomponenName(),
			$keys[12] => $this->getSatuan(),
			$keys[13] => $this->getPajak(),
			$keys[14] => $this->getParam(),
			$keys[15] => $this->getFromKegiatanMember(),
			$keys[16] => $this->getTempKomponenId(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = SubKegiatanMemberSalahPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setSubKegiatanId($value);
				break;
			case 1:
				$this->setDetailNo($value);
				break;
			case 2:
				$this->setKomponenId($value);
				break;
			case 3:
				$this->setRekeningCode($value);
				break;
			case 4:
				$this->setDetailName($value);
				break;
			case 5:
				$this->setVolume($value);
				break;
			case 6:
				$this->setKeteranganKoefisien($value);
				break;
			case 7:
				$this->setKoefisien($value);
				break;
			case 8:
				$this->setSubtitle($value);
				break;
			case 9:
				$this->setKomponenHarga($value);
				break;
			case 10:
				$this->setKomponenHargaAwal($value);
				break;
			case 11:
				$this->setKomponenName($value);
				break;
			case 12:
				$this->setSatuan($value);
				break;
			case 13:
				$this->setPajak($value);
				break;
			case 14:
				$this->setParam($value);
				break;
			case 15:
				$this->setFromKegiatanMember($value);
				break;
			case 16:
				$this->setTempKomponenId($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = SubKegiatanMemberSalahPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setSubKegiatanId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setDetailNo($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setKomponenId($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setRekeningCode($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setDetailName($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setVolume($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setKeteranganKoefisien($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setKoefisien($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setSubtitle($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setKomponenHarga($arr[$keys[9]]);
		if (array_key_exists($keys[10], $arr)) $this->setKomponenHargaAwal($arr[$keys[10]]);
		if (array_key_exists($keys[11], $arr)) $this->setKomponenName($arr[$keys[11]]);
		if (array_key_exists($keys[12], $arr)) $this->setSatuan($arr[$keys[12]]);
		if (array_key_exists($keys[13], $arr)) $this->setPajak($arr[$keys[13]]);
		if (array_key_exists($keys[14], $arr)) $this->setParam($arr[$keys[14]]);
		if (array_key_exists($keys[15], $arr)) $this->setFromKegiatanMember($arr[$keys[15]]);
		if (array_key_exists($keys[16], $arr)) $this->setTempKomponenId($arr[$keys[16]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(SubKegiatanMemberSalahPeer::DATABASE_NAME);

		if ($this->isColumnModified(SubKegiatanMemberSalahPeer::SUB_KEGIATAN_ID)) $criteria->add(SubKegiatanMemberSalahPeer::SUB_KEGIATAN_ID, $this->sub_kegiatan_id);
		if ($this->isColumnModified(SubKegiatanMemberSalahPeer::DETAIL_NO)) $criteria->add(SubKegiatanMemberSalahPeer::DETAIL_NO, $this->detail_no);
		if ($this->isColumnModified(SubKegiatanMemberSalahPeer::KOMPONEN_ID)) $criteria->add(SubKegiatanMemberSalahPeer::KOMPONEN_ID, $this->komponen_id);
		if ($this->isColumnModified(SubKegiatanMemberSalahPeer::REKENING_CODE)) $criteria->add(SubKegiatanMemberSalahPeer::REKENING_CODE, $this->rekening_code);
		if ($this->isColumnModified(SubKegiatanMemberSalahPeer::DETAIL_NAME)) $criteria->add(SubKegiatanMemberSalahPeer::DETAIL_NAME, $this->detail_name);
		if ($this->isColumnModified(SubKegiatanMemberSalahPeer::VOLUME)) $criteria->add(SubKegiatanMemberSalahPeer::VOLUME, $this->volume);
		if ($this->isColumnModified(SubKegiatanMemberSalahPeer::KETERANGAN_KOEFISIEN)) $criteria->add(SubKegiatanMemberSalahPeer::KETERANGAN_KOEFISIEN, $this->keterangan_koefisien);
		if ($this->isColumnModified(SubKegiatanMemberSalahPeer::KOEFISIEN)) $criteria->add(SubKegiatanMemberSalahPeer::KOEFISIEN, $this->koefisien);
		if ($this->isColumnModified(SubKegiatanMemberSalahPeer::SUBTITLE)) $criteria->add(SubKegiatanMemberSalahPeer::SUBTITLE, $this->subtitle);
		if ($this->isColumnModified(SubKegiatanMemberSalahPeer::KOMPONEN_HARGA)) $criteria->add(SubKegiatanMemberSalahPeer::KOMPONEN_HARGA, $this->komponen_harga);
		if ($this->isColumnModified(SubKegiatanMemberSalahPeer::KOMPONEN_HARGA_AWAL)) $criteria->add(SubKegiatanMemberSalahPeer::KOMPONEN_HARGA_AWAL, $this->komponen_harga_awal);
		if ($this->isColumnModified(SubKegiatanMemberSalahPeer::KOMPONEN_NAME)) $criteria->add(SubKegiatanMemberSalahPeer::KOMPONEN_NAME, $this->komponen_name);
		if ($this->isColumnModified(SubKegiatanMemberSalahPeer::SATUAN)) $criteria->add(SubKegiatanMemberSalahPeer::SATUAN, $this->satuan);
		if ($this->isColumnModified(SubKegiatanMemberSalahPeer::PAJAK)) $criteria->add(SubKegiatanMemberSalahPeer::PAJAK, $this->pajak);
		if ($this->isColumnModified(SubKegiatanMemberSalahPeer::PARAM)) $criteria->add(SubKegiatanMemberSalahPeer::PARAM, $this->param);
		if ($this->isColumnModified(SubKegiatanMemberSalahPeer::FROM_KEGIATAN_MEMBER)) $criteria->add(SubKegiatanMemberSalahPeer::FROM_KEGIATAN_MEMBER, $this->from_kegiatan_member);
		if ($this->isColumnModified(SubKegiatanMemberSalahPeer::TEMP_KOMPONEN_ID)) $criteria->add(SubKegiatanMemberSalahPeer::TEMP_KOMPONEN_ID, $this->temp_komponen_id);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(SubKegiatanMemberSalahPeer::DATABASE_NAME);


		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return null;
	}

	
	 public function setPrimaryKey($pk)
	 {
		 	 }

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setSubKegiatanId($this->sub_kegiatan_id);

		$copyObj->setDetailNo($this->detail_no);

		$copyObj->setKomponenId($this->komponen_id);

		$copyObj->setRekeningCode($this->rekening_code);

		$copyObj->setDetailName($this->detail_name);

		$copyObj->setVolume($this->volume);

		$copyObj->setKeteranganKoefisien($this->keterangan_koefisien);

		$copyObj->setKoefisien($this->koefisien);

		$copyObj->setSubtitle($this->subtitle);

		$copyObj->setKomponenHarga($this->komponen_harga);

		$copyObj->setKomponenHargaAwal($this->komponen_harga_awal);

		$copyObj->setKomponenName($this->komponen_name);

		$copyObj->setSatuan($this->satuan);

		$copyObj->setPajak($this->pajak);

		$copyObj->setParam($this->param);

		$copyObj->setFromKegiatanMember($this->from_kegiatan_member);

		$copyObj->setTempKomponenId($this->temp_komponen_id);


		$copyObj->setNew(true);

	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new SubKegiatanMemberSalahPeer();
		}
		return self::$peer;
	}

} 