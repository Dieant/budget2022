<?php


abstract class BaseMasterProgram2 extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $kode_program;


	
	protected $kode_program2;


	
	protected $nama_program2;


	
	protected $ranking;


	
	protected $kode_program3;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getKodeProgram()
	{

		return $this->kode_program;
	}

	
	public function getKodeProgram2()
	{

		return $this->kode_program2;
	}

	
	public function getNamaProgram2()
	{

		return $this->nama_program2;
	}

	
	public function getRanking()
	{

		return $this->ranking;
	}

	
	public function getKodeProgram3()
	{

		return $this->kode_program3;
	}

	
	public function setKodeProgram($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_program !== $v) {
			$this->kode_program = $v;
			$this->modifiedColumns[] = MasterProgram2Peer::KODE_PROGRAM;
		}

	} 
	
	public function setKodeProgram2($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_program2 !== $v) {
			$this->kode_program2 = $v;
			$this->modifiedColumns[] = MasterProgram2Peer::KODE_PROGRAM2;
		}

	} 
	
	public function setNamaProgram2($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nama_program2 !== $v) {
			$this->nama_program2 = $v;
			$this->modifiedColumns[] = MasterProgram2Peer::NAMA_PROGRAM2;
		}

	} 
	
	public function setRanking($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->ranking !== $v) {
			$this->ranking = $v;
			$this->modifiedColumns[] = MasterProgram2Peer::RANKING;
		}

	} 
	
	public function setKodeProgram3($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_program3 !== $v) {
			$this->kode_program3 = $v;
			$this->modifiedColumns[] = MasterProgram2Peer::KODE_PROGRAM3;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->kode_program = $rs->getString($startcol + 0);

			$this->kode_program2 = $rs->getString($startcol + 1);

			$this->nama_program2 = $rs->getString($startcol + 2);

			$this->ranking = $rs->getInt($startcol + 3);

			$this->kode_program3 = $rs->getString($startcol + 4);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 5; 
		} catch (Exception $e) {
			throw new PropelException("Error populating MasterProgram2 object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(MasterProgram2Peer::DATABASE_NAME);
		}

		try {
			$con->begin();
			MasterProgram2Peer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(MasterProgram2Peer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = MasterProgram2Peer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += MasterProgram2Peer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = MasterProgram2Peer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = MasterProgram2Peer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getKodeProgram();
				break;
			case 1:
				return $this->getKodeProgram2();
				break;
			case 2:
				return $this->getNamaProgram2();
				break;
			case 3:
				return $this->getRanking();
				break;
			case 4:
				return $this->getKodeProgram3();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = MasterProgram2Peer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getKodeProgram(),
			$keys[1] => $this->getKodeProgram2(),
			$keys[2] => $this->getNamaProgram2(),
			$keys[3] => $this->getRanking(),
			$keys[4] => $this->getKodeProgram3(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = MasterProgram2Peer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setKodeProgram($value);
				break;
			case 1:
				$this->setKodeProgram2($value);
				break;
			case 2:
				$this->setNamaProgram2($value);
				break;
			case 3:
				$this->setRanking($value);
				break;
			case 4:
				$this->setKodeProgram3($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = MasterProgram2Peer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setKodeProgram($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setKodeProgram2($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setNamaProgram2($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setRanking($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setKodeProgram3($arr[$keys[4]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(MasterProgram2Peer::DATABASE_NAME);

		if ($this->isColumnModified(MasterProgram2Peer::KODE_PROGRAM)) $criteria->add(MasterProgram2Peer::KODE_PROGRAM, $this->kode_program);
		if ($this->isColumnModified(MasterProgram2Peer::KODE_PROGRAM2)) $criteria->add(MasterProgram2Peer::KODE_PROGRAM2, $this->kode_program2);
		if ($this->isColumnModified(MasterProgram2Peer::NAMA_PROGRAM2)) $criteria->add(MasterProgram2Peer::NAMA_PROGRAM2, $this->nama_program2);
		if ($this->isColumnModified(MasterProgram2Peer::RANKING)) $criteria->add(MasterProgram2Peer::RANKING, $this->ranking);
		if ($this->isColumnModified(MasterProgram2Peer::KODE_PROGRAM3)) $criteria->add(MasterProgram2Peer::KODE_PROGRAM3, $this->kode_program3);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(MasterProgram2Peer::DATABASE_NAME);

		$criteria->add(MasterProgram2Peer::KODE_PROGRAM, $this->kode_program);
		$criteria->add(MasterProgram2Peer::KODE_PROGRAM2, $this->kode_program2);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		$pks = array();

		$pks[0] = $this->getKodeProgram();

		$pks[1] = $this->getKodeProgram2();

		return $pks;
	}

	
	public function setPrimaryKey($keys)
	{

		$this->setKodeProgram($keys[0]);

		$this->setKodeProgram2($keys[1]);

	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setNamaProgram2($this->nama_program2);

		$copyObj->setRanking($this->ranking);

		$copyObj->setKodeProgram3($this->kode_program3);


		$copyObj->setNew(true);

		$copyObj->setKodeProgram(NULL); 
		$copyObj->setKodeProgram2(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new MasterProgram2Peer();
		}
		return self::$peer;
	}

} 