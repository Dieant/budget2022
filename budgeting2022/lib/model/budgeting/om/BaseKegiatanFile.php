<?php


abstract class BaseKegiatanFile extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $unit_id;


	
	protected $kegiatan_code;


	
	protected $urut;


	
	protected $nama_file;


	
	protected $tipe_file;


	
	protected $isi_file;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getUnitId()
	{

		return $this->unit_id;
	}

	
	public function getKegiatanCode()
	{

		return $this->kegiatan_code;
	}

	
	public function getUrut()
	{

		return $this->urut;
	}

	
	public function getNamaFile()
	{

		return $this->nama_file;
	}

	
	public function getTipeFile()
	{

		return $this->tipe_file;
	}

	
	public function getIsiFile()
	{

		return $this->isi_file;
	}

	
	public function setUnitId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->unit_id !== $v) {
			$this->unit_id = $v;
			$this->modifiedColumns[] = KegiatanFilePeer::UNIT_ID;
		}

	} 
	
	public function setKegiatanCode($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kegiatan_code !== $v) {
			$this->kegiatan_code = $v;
			$this->modifiedColumns[] = KegiatanFilePeer::KEGIATAN_CODE;
		}

	} 
	
	public function setUrut($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->urut !== $v) {
			$this->urut = $v;
			$this->modifiedColumns[] = KegiatanFilePeer::URUT;
		}

	} 
	
	public function setNamaFile($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nama_file !== $v) {
			$this->nama_file = $v;
			$this->modifiedColumns[] = KegiatanFilePeer::NAMA_FILE;
		}

	} 
	
	public function setTipeFile($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->tipe_file !== $v) {
			$this->tipe_file = $v;
			$this->modifiedColumns[] = KegiatanFilePeer::TIPE_FILE;
		}

	} 
	
	public function setIsiFile($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->isi_file !== $v) {
			$this->isi_file = $v;
			$this->modifiedColumns[] = KegiatanFilePeer::ISI_FILE;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->unit_id = $rs->getString($startcol + 0);

			$this->kegiatan_code = $rs->getString($startcol + 1);

			$this->urut = $rs->getInt($startcol + 2);

			$this->nama_file = $rs->getString($startcol + 3);

			$this->tipe_file = $rs->getString($startcol + 4);

			$this->isi_file = $rs->getBlob($startcol + 5);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 6; 
		} catch (Exception $e) {
			throw new PropelException("Error populating KegiatanFile object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(KegiatanFilePeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			KegiatanFilePeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(KegiatanFilePeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = KegiatanFilePeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += KegiatanFilePeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = KegiatanFilePeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = KegiatanFilePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getUnitId();
				break;
			case 1:
				return $this->getKegiatanCode();
				break;
			case 2:
				return $this->getUrut();
				break;
			case 3:
				return $this->getNamaFile();
				break;
			case 4:
				return $this->getTipeFile();
				break;
			case 5:
				return $this->getIsiFile();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = KegiatanFilePeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getUnitId(),
			$keys[1] => $this->getKegiatanCode(),
			$keys[2] => $this->getUrut(),
			$keys[3] => $this->getNamaFile(),
			$keys[4] => $this->getTipeFile(),
			$keys[5] => $this->getIsiFile(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = KegiatanFilePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setUnitId($value);
				break;
			case 1:
				$this->setKegiatanCode($value);
				break;
			case 2:
				$this->setUrut($value);
				break;
			case 3:
				$this->setNamaFile($value);
				break;
			case 4:
				$this->setTipeFile($value);
				break;
			case 5:
				$this->setIsiFile($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = KegiatanFilePeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setUnitId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setKegiatanCode($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setUrut($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setNamaFile($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setTipeFile($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setIsiFile($arr[$keys[5]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(KegiatanFilePeer::DATABASE_NAME);

		if ($this->isColumnModified(KegiatanFilePeer::UNIT_ID)) $criteria->add(KegiatanFilePeer::UNIT_ID, $this->unit_id);
		if ($this->isColumnModified(KegiatanFilePeer::KEGIATAN_CODE)) $criteria->add(KegiatanFilePeer::KEGIATAN_CODE, $this->kegiatan_code);
		if ($this->isColumnModified(KegiatanFilePeer::URUT)) $criteria->add(KegiatanFilePeer::URUT, $this->urut);
		if ($this->isColumnModified(KegiatanFilePeer::NAMA_FILE)) $criteria->add(KegiatanFilePeer::NAMA_FILE, $this->nama_file);
		if ($this->isColumnModified(KegiatanFilePeer::TIPE_FILE)) $criteria->add(KegiatanFilePeer::TIPE_FILE, $this->tipe_file);
		if ($this->isColumnModified(KegiatanFilePeer::ISI_FILE)) $criteria->add(KegiatanFilePeer::ISI_FILE, $this->isi_file);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(KegiatanFilePeer::DATABASE_NAME);

		$criteria->add(KegiatanFilePeer::UNIT_ID, $this->unit_id);
		$criteria->add(KegiatanFilePeer::KEGIATAN_CODE, $this->kegiatan_code);
		$criteria->add(KegiatanFilePeer::URUT, $this->urut);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		$pks = array();

		$pks[0] = $this->getUnitId();

		$pks[1] = $this->getKegiatanCode();

		$pks[2] = $this->getUrut();

		return $pks;
	}

	
	public function setPrimaryKey($keys)
	{

		$this->setUnitId($keys[0]);

		$this->setKegiatanCode($keys[1]);

		$this->setUrut($keys[2]);

	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setNamaFile($this->nama_file);

		$copyObj->setTipeFile($this->tipe_file);

		$copyObj->setIsiFile($this->isi_file);


		$copyObj->setNew(true);

		$copyObj->setUnitId(NULL); 
		$copyObj->setKegiatanCode(NULL); 
		$copyObj->setUrut(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new KegiatanFilePeer();
		}
		return self::$peer;
	}

} 