<?php


abstract class BaseUsulanDinasPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.usulan_dinas';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.UsulanDinas';

	
	const NUM_COLUMNS = 17;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const ID_USULAN = 'ebudget.usulan_dinas.ID_USULAN';

	
	const TGL_USULAN = 'ebudget.usulan_dinas.TGL_USULAN';

	
	const CREATED_AT = 'ebudget.usulan_dinas.CREATED_AT';

	
	const UPDATED_AT = 'ebudget.usulan_dinas.UPDATED_AT';

	
	const PENYELIA = 'ebudget.usulan_dinas.PENYELIA';

	
	const SKPD = 'ebudget.usulan_dinas.SKPD';

	
	const JENIS_USULAN = 'ebudget.usulan_dinas.JENIS_USULAN';

	
	const JUMLAH_DUKUNGAN = 'ebudget.usulan_dinas.JUMLAH_DUKUNGAN';

	
	const JUMLAH_USULAN = 'ebudget.usulan_dinas.JUMLAH_USULAN';

	
	const STATUS_VERIFIKASI = 'ebudget.usulan_dinas.STATUS_VERIFIKASI';

	
	const STATUS_HAPUS = 'ebudget.usulan_dinas.STATUS_HAPUS';

	
	const FILEPATH = 'ebudget.usulan_dinas.FILEPATH';

	
	const FILEPATH_RAR = 'ebudget.usulan_dinas.FILEPATH_RAR';

	
	const KOMENTAR_VERIFIKATOR = 'ebudget.usulan_dinas.KOMENTAR_VERIFIKATOR';

	
	const NOMOR_SURAT = 'ebudget.usulan_dinas.NOMOR_SURAT';

	
	const STATUS_TOLAK = 'ebudget.usulan_dinas.STATUS_TOLAK';

	
	const KETERANGAN_DINAS = 'ebudget.usulan_dinas.KETERANGAN_DINAS';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('IdUsulan', 'TglUsulan', 'CreatedAt', 'UpdatedAt', 'Penyelia', 'Skpd', 'JenisUsulan', 'JumlahDukungan', 'JumlahUsulan', 'StatusVerifikasi', 'StatusHapus', 'Filepath', 'FilepathRar', 'KomentarVerifikator', 'NomorSurat', 'StatusTolak', 'KeteranganDinas', ),
		BasePeer::TYPE_COLNAME => array (UsulanDinasPeer::ID_USULAN, UsulanDinasPeer::TGL_USULAN, UsulanDinasPeer::CREATED_AT, UsulanDinasPeer::UPDATED_AT, UsulanDinasPeer::PENYELIA, UsulanDinasPeer::SKPD, UsulanDinasPeer::JENIS_USULAN, UsulanDinasPeer::JUMLAH_DUKUNGAN, UsulanDinasPeer::JUMLAH_USULAN, UsulanDinasPeer::STATUS_VERIFIKASI, UsulanDinasPeer::STATUS_HAPUS, UsulanDinasPeer::FILEPATH, UsulanDinasPeer::FILEPATH_RAR, UsulanDinasPeer::KOMENTAR_VERIFIKATOR, UsulanDinasPeer::NOMOR_SURAT, UsulanDinasPeer::STATUS_TOLAK, UsulanDinasPeer::KETERANGAN_DINAS, ),
		BasePeer::TYPE_FIELDNAME => array ('id_usulan', 'tgl_usulan', 'created_at', 'updated_at', 'penyelia', 'skpd', 'jenis_usulan', 'jumlah_dukungan', 'jumlah_usulan', 'status_verifikasi', 'status_hapus', 'filepath', 'filepath_rar', 'komentar_verifikator', 'nomor_surat', 'status_tolak', 'keterangan_dinas', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('IdUsulan' => 0, 'TglUsulan' => 1, 'CreatedAt' => 2, 'UpdatedAt' => 3, 'Penyelia' => 4, 'Skpd' => 5, 'JenisUsulan' => 6, 'JumlahDukungan' => 7, 'JumlahUsulan' => 8, 'StatusVerifikasi' => 9, 'StatusHapus' => 10, 'Filepath' => 11, 'FilepathRar' => 12, 'KomentarVerifikator' => 13, 'NomorSurat' => 14, 'StatusTolak' => 15, 'KeteranganDinas' => 16, ),
		BasePeer::TYPE_COLNAME => array (UsulanDinasPeer::ID_USULAN => 0, UsulanDinasPeer::TGL_USULAN => 1, UsulanDinasPeer::CREATED_AT => 2, UsulanDinasPeer::UPDATED_AT => 3, UsulanDinasPeer::PENYELIA => 4, UsulanDinasPeer::SKPD => 5, UsulanDinasPeer::JENIS_USULAN => 6, UsulanDinasPeer::JUMLAH_DUKUNGAN => 7, UsulanDinasPeer::JUMLAH_USULAN => 8, UsulanDinasPeer::STATUS_VERIFIKASI => 9, UsulanDinasPeer::STATUS_HAPUS => 10, UsulanDinasPeer::FILEPATH => 11, UsulanDinasPeer::FILEPATH_RAR => 12, UsulanDinasPeer::KOMENTAR_VERIFIKATOR => 13, UsulanDinasPeer::NOMOR_SURAT => 14, UsulanDinasPeer::STATUS_TOLAK => 15, UsulanDinasPeer::KETERANGAN_DINAS => 16, ),
		BasePeer::TYPE_FIELDNAME => array ('id_usulan' => 0, 'tgl_usulan' => 1, 'created_at' => 2, 'updated_at' => 3, 'penyelia' => 4, 'skpd' => 5, 'jenis_usulan' => 6, 'jumlah_dukungan' => 7, 'jumlah_usulan' => 8, 'status_verifikasi' => 9, 'status_hapus' => 10, 'filepath' => 11, 'filepath_rar' => 12, 'komentar_verifikator' => 13, 'nomor_surat' => 14, 'status_tolak' => 15, 'keterangan_dinas' => 16, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/UsulanDinasMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.UsulanDinasMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = UsulanDinasPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(UsulanDinasPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(UsulanDinasPeer::ID_USULAN);

		$criteria->addSelectColumn(UsulanDinasPeer::TGL_USULAN);

		$criteria->addSelectColumn(UsulanDinasPeer::CREATED_AT);

		$criteria->addSelectColumn(UsulanDinasPeer::UPDATED_AT);

		$criteria->addSelectColumn(UsulanDinasPeer::PENYELIA);

		$criteria->addSelectColumn(UsulanDinasPeer::SKPD);

		$criteria->addSelectColumn(UsulanDinasPeer::JENIS_USULAN);

		$criteria->addSelectColumn(UsulanDinasPeer::JUMLAH_DUKUNGAN);

		$criteria->addSelectColumn(UsulanDinasPeer::JUMLAH_USULAN);

		$criteria->addSelectColumn(UsulanDinasPeer::STATUS_VERIFIKASI);

		$criteria->addSelectColumn(UsulanDinasPeer::STATUS_HAPUS);

		$criteria->addSelectColumn(UsulanDinasPeer::FILEPATH);

		$criteria->addSelectColumn(UsulanDinasPeer::FILEPATH_RAR);

		$criteria->addSelectColumn(UsulanDinasPeer::KOMENTAR_VERIFIKATOR);

		$criteria->addSelectColumn(UsulanDinasPeer::NOMOR_SURAT);

		$criteria->addSelectColumn(UsulanDinasPeer::STATUS_TOLAK);

		$criteria->addSelectColumn(UsulanDinasPeer::KETERANGAN_DINAS);

	}

	const COUNT = 'COUNT(ebudget.usulan_dinas.ID_USULAN)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.usulan_dinas.ID_USULAN)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(UsulanDinasPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(UsulanDinasPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = UsulanDinasPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = UsulanDinasPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return UsulanDinasPeer::populateObjects(UsulanDinasPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			UsulanDinasPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = UsulanDinasPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return UsulanDinasPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(UsulanDinasPeer::ID_USULAN);
			$selectCriteria->add(UsulanDinasPeer::ID_USULAN, $criteria->remove(UsulanDinasPeer::ID_USULAN), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(UsulanDinasPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(UsulanDinasPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof UsulanDinas) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(UsulanDinasPeer::ID_USULAN, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(UsulanDinas $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(UsulanDinasPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(UsulanDinasPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(UsulanDinasPeer::DATABASE_NAME, UsulanDinasPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = UsulanDinasPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(UsulanDinasPeer::DATABASE_NAME);

		$criteria->add(UsulanDinasPeer::ID_USULAN, $pk);


		$v = UsulanDinasPeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(UsulanDinasPeer::ID_USULAN, $pks, Criteria::IN);
			$objs = UsulanDinasPeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BaseUsulanDinasPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/UsulanDinasMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.UsulanDinasMapBuilder');
}
