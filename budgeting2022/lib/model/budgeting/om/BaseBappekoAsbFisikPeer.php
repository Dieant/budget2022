<?php


abstract class BaseBappekoAsbFisikPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.bappeko_asb_fisik';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.BappekoAsbFisik';

	
	const NUM_COLUMNS = 20;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const KOMPONEN_ID = 'ebudget.bappeko_asb_fisik.KOMPONEN_ID';

	
	const SATUAN = 'ebudget.bappeko_asb_fisik.SATUAN';

	
	const KOMPONEN_NAME = 'ebudget.bappeko_asb_fisik.KOMPONEN_NAME';

	
	const SHSD_ID = 'ebudget.bappeko_asb_fisik.SHSD_ID';

	
	const KOMPONEN_HARGA = 'ebudget.bappeko_asb_fisik.KOMPONEN_HARGA';

	
	const KOMPONEN_SHOW = 'ebudget.bappeko_asb_fisik.KOMPONEN_SHOW';

	
	const IP_ADDRESS = 'ebudget.bappeko_asb_fisik.IP_ADDRESS';

	
	const WAKTU_ACCESS = 'ebudget.bappeko_asb_fisik.WAKTU_ACCESS';

	
	const KOMPONEN_TIPE = 'ebudget.bappeko_asb_fisik.KOMPONEN_TIPE';

	
	const KOMPONEN_CONFIRMED = 'ebudget.bappeko_asb_fisik.KOMPONEN_CONFIRMED';

	
	const KOMPONEN_NON_PAJAK = 'ebudget.bappeko_asb_fisik.KOMPONEN_NON_PAJAK';

	
	const USER_ID = 'ebudget.bappeko_asb_fisik.USER_ID';

	
	const REKENING = 'ebudget.bappeko_asb_fisik.REKENING';

	
	const KELOMPOK = 'ebudget.bappeko_asb_fisik.KELOMPOK';

	
	const PEMELIHARAAN = 'ebudget.bappeko_asb_fisik.PEMELIHARAAN';

	
	const REK_UPAH = 'ebudget.bappeko_asb_fisik.REK_UPAH';

	
	const REK_BAHAN = 'ebudget.bappeko_asb_fisik.REK_BAHAN';

	
	const REK_SEWA = 'ebudget.bappeko_asb_fisik.REK_SEWA';

	
	const DESKRIPSI = 'ebudget.bappeko_asb_fisik.DESKRIPSI';

	
	const ASB_LOCKED = 'ebudget.bappeko_asb_fisik.ASB_LOCKED';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('KomponenId', 'Satuan', 'KomponenName', 'ShsdId', 'KomponenHarga', 'KomponenShow', 'IpAddress', 'WaktuAccess', 'KomponenTipe', 'KomponenConfirmed', 'KomponenNonPajak', 'UserId', 'Rekening', 'Kelompok', 'Pemeliharaan', 'RekUpah', 'RekBahan', 'RekSewa', 'Deskripsi', 'AsbLocked', ),
		BasePeer::TYPE_COLNAME => array (BappekoAsbFisikPeer::KOMPONEN_ID, BappekoAsbFisikPeer::SATUAN, BappekoAsbFisikPeer::KOMPONEN_NAME, BappekoAsbFisikPeer::SHSD_ID, BappekoAsbFisikPeer::KOMPONEN_HARGA, BappekoAsbFisikPeer::KOMPONEN_SHOW, BappekoAsbFisikPeer::IP_ADDRESS, BappekoAsbFisikPeer::WAKTU_ACCESS, BappekoAsbFisikPeer::KOMPONEN_TIPE, BappekoAsbFisikPeer::KOMPONEN_CONFIRMED, BappekoAsbFisikPeer::KOMPONEN_NON_PAJAK, BappekoAsbFisikPeer::USER_ID, BappekoAsbFisikPeer::REKENING, BappekoAsbFisikPeer::KELOMPOK, BappekoAsbFisikPeer::PEMELIHARAAN, BappekoAsbFisikPeer::REK_UPAH, BappekoAsbFisikPeer::REK_BAHAN, BappekoAsbFisikPeer::REK_SEWA, BappekoAsbFisikPeer::DESKRIPSI, BappekoAsbFisikPeer::ASB_LOCKED, ),
		BasePeer::TYPE_FIELDNAME => array ('komponen_id', 'satuan', 'komponen_name', 'shsd_id', 'komponen_harga', 'komponen_show', 'ip_address', 'waktu_access', 'komponen_tipe', 'komponen_confirmed', 'komponen_non_pajak', 'user_id', 'rekening', 'kelompok', 'pemeliharaan', 'rek_upah', 'rek_bahan', 'rek_sewa', 'deskripsi', 'asb_locked', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('KomponenId' => 0, 'Satuan' => 1, 'KomponenName' => 2, 'ShsdId' => 3, 'KomponenHarga' => 4, 'KomponenShow' => 5, 'IpAddress' => 6, 'WaktuAccess' => 7, 'KomponenTipe' => 8, 'KomponenConfirmed' => 9, 'KomponenNonPajak' => 10, 'UserId' => 11, 'Rekening' => 12, 'Kelompok' => 13, 'Pemeliharaan' => 14, 'RekUpah' => 15, 'RekBahan' => 16, 'RekSewa' => 17, 'Deskripsi' => 18, 'AsbLocked' => 19, ),
		BasePeer::TYPE_COLNAME => array (BappekoAsbFisikPeer::KOMPONEN_ID => 0, BappekoAsbFisikPeer::SATUAN => 1, BappekoAsbFisikPeer::KOMPONEN_NAME => 2, BappekoAsbFisikPeer::SHSD_ID => 3, BappekoAsbFisikPeer::KOMPONEN_HARGA => 4, BappekoAsbFisikPeer::KOMPONEN_SHOW => 5, BappekoAsbFisikPeer::IP_ADDRESS => 6, BappekoAsbFisikPeer::WAKTU_ACCESS => 7, BappekoAsbFisikPeer::KOMPONEN_TIPE => 8, BappekoAsbFisikPeer::KOMPONEN_CONFIRMED => 9, BappekoAsbFisikPeer::KOMPONEN_NON_PAJAK => 10, BappekoAsbFisikPeer::USER_ID => 11, BappekoAsbFisikPeer::REKENING => 12, BappekoAsbFisikPeer::KELOMPOK => 13, BappekoAsbFisikPeer::PEMELIHARAAN => 14, BappekoAsbFisikPeer::REK_UPAH => 15, BappekoAsbFisikPeer::REK_BAHAN => 16, BappekoAsbFisikPeer::REK_SEWA => 17, BappekoAsbFisikPeer::DESKRIPSI => 18, BappekoAsbFisikPeer::ASB_LOCKED => 19, ),
		BasePeer::TYPE_FIELDNAME => array ('komponen_id' => 0, 'satuan' => 1, 'komponen_name' => 2, 'shsd_id' => 3, 'komponen_harga' => 4, 'komponen_show' => 5, 'ip_address' => 6, 'waktu_access' => 7, 'komponen_tipe' => 8, 'komponen_confirmed' => 9, 'komponen_non_pajak' => 10, 'user_id' => 11, 'rekening' => 12, 'kelompok' => 13, 'pemeliharaan' => 14, 'rek_upah' => 15, 'rek_bahan' => 16, 'rek_sewa' => 17, 'deskripsi' => 18, 'asb_locked' => 19, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/BappekoAsbFisikMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.BappekoAsbFisikMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = BappekoAsbFisikPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(BappekoAsbFisikPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(BappekoAsbFisikPeer::KOMPONEN_ID);

		$criteria->addSelectColumn(BappekoAsbFisikPeer::SATUAN);

		$criteria->addSelectColumn(BappekoAsbFisikPeer::KOMPONEN_NAME);

		$criteria->addSelectColumn(BappekoAsbFisikPeer::SHSD_ID);

		$criteria->addSelectColumn(BappekoAsbFisikPeer::KOMPONEN_HARGA);

		$criteria->addSelectColumn(BappekoAsbFisikPeer::KOMPONEN_SHOW);

		$criteria->addSelectColumn(BappekoAsbFisikPeer::IP_ADDRESS);

		$criteria->addSelectColumn(BappekoAsbFisikPeer::WAKTU_ACCESS);

		$criteria->addSelectColumn(BappekoAsbFisikPeer::KOMPONEN_TIPE);

		$criteria->addSelectColumn(BappekoAsbFisikPeer::KOMPONEN_CONFIRMED);

		$criteria->addSelectColumn(BappekoAsbFisikPeer::KOMPONEN_NON_PAJAK);

		$criteria->addSelectColumn(BappekoAsbFisikPeer::USER_ID);

		$criteria->addSelectColumn(BappekoAsbFisikPeer::REKENING);

		$criteria->addSelectColumn(BappekoAsbFisikPeer::KELOMPOK);

		$criteria->addSelectColumn(BappekoAsbFisikPeer::PEMELIHARAAN);

		$criteria->addSelectColumn(BappekoAsbFisikPeer::REK_UPAH);

		$criteria->addSelectColumn(BappekoAsbFisikPeer::REK_BAHAN);

		$criteria->addSelectColumn(BappekoAsbFisikPeer::REK_SEWA);

		$criteria->addSelectColumn(BappekoAsbFisikPeer::DESKRIPSI);

		$criteria->addSelectColumn(BappekoAsbFisikPeer::ASB_LOCKED);

	}

	const COUNT = 'COUNT(ebudget.bappeko_asb_fisik.KOMPONEN_ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.bappeko_asb_fisik.KOMPONEN_ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(BappekoAsbFisikPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(BappekoAsbFisikPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = BappekoAsbFisikPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = BappekoAsbFisikPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return BappekoAsbFisikPeer::populateObjects(BappekoAsbFisikPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			BappekoAsbFisikPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = BappekoAsbFisikPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return BappekoAsbFisikPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(BappekoAsbFisikPeer::KOMPONEN_ID);
			$selectCriteria->add(BappekoAsbFisikPeer::KOMPONEN_ID, $criteria->remove(BappekoAsbFisikPeer::KOMPONEN_ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(BappekoAsbFisikPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(BappekoAsbFisikPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof BappekoAsbFisik) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(BappekoAsbFisikPeer::KOMPONEN_ID, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(BappekoAsbFisik $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(BappekoAsbFisikPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(BappekoAsbFisikPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(BappekoAsbFisikPeer::DATABASE_NAME, BappekoAsbFisikPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = BappekoAsbFisikPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(BappekoAsbFisikPeer::DATABASE_NAME);

		$criteria->add(BappekoAsbFisikPeer::KOMPONEN_ID, $pk);


		$v = BappekoAsbFisikPeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(BappekoAsbFisikPeer::KOMPONEN_ID, $pks, Criteria::IN);
			$objs = BappekoAsbFisikPeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BaseBappekoAsbFisikPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/BappekoAsbFisikMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.BappekoAsbFisikMapBuilder');
}
