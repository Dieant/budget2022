<?php


abstract class BaseSchemaAksesV2Peer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'schema_akses_v2';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.SchemaAksesV2';

	
	const NUM_COLUMNS = 4;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const USER_ID = 'schema_akses_v2.USER_ID';

	
	const SCHEMA_ID = 'schema_akses_v2.SCHEMA_ID';

	
	const LEVEL_ID = 'schema_akses_v2.LEVEL_ID';

	
	const IS_UBAH_PASS = 'schema_akses_v2.IS_UBAH_PASS';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('UserId', 'SchemaId', 'LevelId', 'IsUbahPass', ),
		BasePeer::TYPE_COLNAME => array (SchemaAksesV2Peer::USER_ID, SchemaAksesV2Peer::SCHEMA_ID, SchemaAksesV2Peer::LEVEL_ID, SchemaAksesV2Peer::IS_UBAH_PASS, ),
		BasePeer::TYPE_FIELDNAME => array ('user_id', 'schema_id', 'level_id', 'is_ubah_pass', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('UserId' => 0, 'SchemaId' => 1, 'LevelId' => 2, 'IsUbahPass' => 3, ),
		BasePeer::TYPE_COLNAME => array (SchemaAksesV2Peer::USER_ID => 0, SchemaAksesV2Peer::SCHEMA_ID => 1, SchemaAksesV2Peer::LEVEL_ID => 2, SchemaAksesV2Peer::IS_UBAH_PASS => 3, ),
		BasePeer::TYPE_FIELDNAME => array ('user_id' => 0, 'schema_id' => 1, 'level_id' => 2, 'is_ubah_pass' => 3, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/SchemaAksesV2MapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.SchemaAksesV2MapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = SchemaAksesV2Peer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(SchemaAksesV2Peer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(SchemaAksesV2Peer::USER_ID);

		$criteria->addSelectColumn(SchemaAksesV2Peer::SCHEMA_ID);

		$criteria->addSelectColumn(SchemaAksesV2Peer::LEVEL_ID);

		$criteria->addSelectColumn(SchemaAksesV2Peer::IS_UBAH_PASS);

	}

	const COUNT = 'COUNT(schema_akses_v2.USER_ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT schema_akses_v2.USER_ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(SchemaAksesV2Peer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(SchemaAksesV2Peer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = SchemaAksesV2Peer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = SchemaAksesV2Peer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return SchemaAksesV2Peer::populateObjects(SchemaAksesV2Peer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			SchemaAksesV2Peer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = SchemaAksesV2Peer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}

	
	public static function doCountJoinMasterUserV2(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(SchemaAksesV2Peer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(SchemaAksesV2Peer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(SchemaAksesV2Peer::USER_ID, MasterUserV2Peer::USER_ID);

		$rs = SchemaAksesV2Peer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doCountJoinMasterSchema(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(SchemaAksesV2Peer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(SchemaAksesV2Peer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(SchemaAksesV2Peer::SCHEMA_ID, MasterSchemaPeer::SCHEMA_ID);

		$rs = SchemaAksesV2Peer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doCountJoinUserLevel(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(SchemaAksesV2Peer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(SchemaAksesV2Peer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(SchemaAksesV2Peer::LEVEL_ID, UserLevelPeer::LEVEL_ID);

		$rs = SchemaAksesV2Peer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doSelectJoinMasterUserV2(Criteria $c, $con = null)
	{
		$c = clone $c;

				if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		SchemaAksesV2Peer::addSelectColumns($c);
		$startcol = (SchemaAksesV2Peer::NUM_COLUMNS - SchemaAksesV2Peer::NUM_LAZY_LOAD_COLUMNS) + 1;
		MasterUserV2Peer::addSelectColumns($c);

		$c->addJoin(SchemaAksesV2Peer::USER_ID, MasterUserV2Peer::USER_ID);
		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = SchemaAksesV2Peer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = MasterUserV2Peer::getOMClass();

			$cls = Propel::import($omClass);
			$obj2 = new $cls();
			$obj2->hydrate($rs, $startcol);

			$newObject = true;
			foreach($results as $temp_obj1) {
				$temp_obj2 = $temp_obj1->getMasterUserV2(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
										$temp_obj2->addSchemaAksesV2($obj1); 					break;
				}
			}
			if ($newObject) {
				$obj2->initSchemaAksesV2s();
				$obj2->addSchemaAksesV2($obj1); 			}
			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doSelectJoinMasterSchema(Criteria $c, $con = null)
	{
		$c = clone $c;

				if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		SchemaAksesV2Peer::addSelectColumns($c);
		$startcol = (SchemaAksesV2Peer::NUM_COLUMNS - SchemaAksesV2Peer::NUM_LAZY_LOAD_COLUMNS) + 1;
		MasterSchemaPeer::addSelectColumns($c);

		$c->addJoin(SchemaAksesV2Peer::SCHEMA_ID, MasterSchemaPeer::SCHEMA_ID);
		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = SchemaAksesV2Peer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = MasterSchemaPeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj2 = new $cls();
			$obj2->hydrate($rs, $startcol);

			$newObject = true;
			foreach($results as $temp_obj1) {
				$temp_obj2 = $temp_obj1->getMasterSchema(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
										$temp_obj2->addSchemaAksesV2($obj1); 					break;
				}
			}
			if ($newObject) {
				$obj2->initSchemaAksesV2s();
				$obj2->addSchemaAksesV2($obj1); 			}
			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doSelectJoinUserLevel(Criteria $c, $con = null)
	{
		$c = clone $c;

				if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		SchemaAksesV2Peer::addSelectColumns($c);
		$startcol = (SchemaAksesV2Peer::NUM_COLUMNS - SchemaAksesV2Peer::NUM_LAZY_LOAD_COLUMNS) + 1;
		UserLevelPeer::addSelectColumns($c);

		$c->addJoin(SchemaAksesV2Peer::LEVEL_ID, UserLevelPeer::LEVEL_ID);
		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = SchemaAksesV2Peer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = UserLevelPeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj2 = new $cls();
			$obj2->hydrate($rs, $startcol);

			$newObject = true;
			foreach($results as $temp_obj1) {
				$temp_obj2 = $temp_obj1->getUserLevel(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
										$temp_obj2->addSchemaAksesV2($obj1); 					break;
				}
			}
			if ($newObject) {
				$obj2->initSchemaAksesV2s();
				$obj2->addSchemaAksesV2($obj1); 			}
			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doCountJoinAll(Criteria $criteria, $distinct = false, $con = null)
	{
		$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(SchemaAksesV2Peer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(SchemaAksesV2Peer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(SchemaAksesV2Peer::USER_ID, MasterUserV2Peer::USER_ID);

		$criteria->addJoin(SchemaAksesV2Peer::SCHEMA_ID, MasterSchemaPeer::SCHEMA_ID);

		$criteria->addJoin(SchemaAksesV2Peer::LEVEL_ID, UserLevelPeer::LEVEL_ID);

		$rs = SchemaAksesV2Peer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doSelectJoinAll(Criteria $c, $con = null)
	{
		$c = clone $c;

				if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		SchemaAksesV2Peer::addSelectColumns($c);
		$startcol2 = (SchemaAksesV2Peer::NUM_COLUMNS - SchemaAksesV2Peer::NUM_LAZY_LOAD_COLUMNS) + 1;

		MasterUserV2Peer::addSelectColumns($c);
		$startcol3 = $startcol2 + MasterUserV2Peer::NUM_COLUMNS;

		MasterSchemaPeer::addSelectColumns($c);
		$startcol4 = $startcol3 + MasterSchemaPeer::NUM_COLUMNS;

		UserLevelPeer::addSelectColumns($c);
		$startcol5 = $startcol4 + UserLevelPeer::NUM_COLUMNS;

		$c->addJoin(SchemaAksesV2Peer::USER_ID, MasterUserV2Peer::USER_ID);

		$c->addJoin(SchemaAksesV2Peer::SCHEMA_ID, MasterSchemaPeer::SCHEMA_ID);

		$c->addJoin(SchemaAksesV2Peer::LEVEL_ID, UserLevelPeer::LEVEL_ID);

		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = SchemaAksesV2Peer::getOMClass();


			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);


					
			$omClass = MasterUserV2Peer::getOMClass();


			$cls = Propel::import($omClass);
			$obj2 = new $cls();
			$obj2->hydrate($rs, $startcol2);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj2 = $temp_obj1->getMasterUserV2(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
					$temp_obj2->addSchemaAksesV2($obj1); 					break;
				}
			}

			if ($newObject) {
				$obj2->initSchemaAksesV2s();
				$obj2->addSchemaAksesV2($obj1);
			}


					
			$omClass = MasterSchemaPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj3 = new $cls();
			$obj3->hydrate($rs, $startcol3);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj3 = $temp_obj1->getMasterSchema(); 				if ($temp_obj3->getPrimaryKey() === $obj3->getPrimaryKey()) {
					$newObject = false;
					$temp_obj3->addSchemaAksesV2($obj1); 					break;
				}
			}

			if ($newObject) {
				$obj3->initSchemaAksesV2s();
				$obj3->addSchemaAksesV2($obj1);
			}


					
			$omClass = UserLevelPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj4 = new $cls();
			$obj4->hydrate($rs, $startcol4);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj4 = $temp_obj1->getUserLevel(); 				if ($temp_obj4->getPrimaryKey() === $obj4->getPrimaryKey()) {
					$newObject = false;
					$temp_obj4->addSchemaAksesV2($obj1); 					break;
				}
			}

			if ($newObject) {
				$obj4->initSchemaAksesV2s();
				$obj4->addSchemaAksesV2($obj1);
			}

			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doCountJoinAllExceptMasterUserV2(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(SchemaAksesV2Peer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(SchemaAksesV2Peer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(SchemaAksesV2Peer::SCHEMA_ID, MasterSchemaPeer::SCHEMA_ID);

		$criteria->addJoin(SchemaAksesV2Peer::LEVEL_ID, UserLevelPeer::LEVEL_ID);

		$rs = SchemaAksesV2Peer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doCountJoinAllExceptMasterSchema(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(SchemaAksesV2Peer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(SchemaAksesV2Peer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(SchemaAksesV2Peer::USER_ID, MasterUserV2Peer::USER_ID);

		$criteria->addJoin(SchemaAksesV2Peer::LEVEL_ID, UserLevelPeer::LEVEL_ID);

		$rs = SchemaAksesV2Peer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doCountJoinAllExceptUserLevel(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(SchemaAksesV2Peer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(SchemaAksesV2Peer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(SchemaAksesV2Peer::USER_ID, MasterUserV2Peer::USER_ID);

		$criteria->addJoin(SchemaAksesV2Peer::SCHEMA_ID, MasterSchemaPeer::SCHEMA_ID);

		$rs = SchemaAksesV2Peer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doSelectJoinAllExceptMasterUserV2(Criteria $c, $con = null)
	{
		$c = clone $c;

								if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		SchemaAksesV2Peer::addSelectColumns($c);
		$startcol2 = (SchemaAksesV2Peer::NUM_COLUMNS - SchemaAksesV2Peer::NUM_LAZY_LOAD_COLUMNS) + 1;

		MasterSchemaPeer::addSelectColumns($c);
		$startcol3 = $startcol2 + MasterSchemaPeer::NUM_COLUMNS;

		UserLevelPeer::addSelectColumns($c);
		$startcol4 = $startcol3 + UserLevelPeer::NUM_COLUMNS;

		$c->addJoin(SchemaAksesV2Peer::SCHEMA_ID, MasterSchemaPeer::SCHEMA_ID);

		$c->addJoin(SchemaAksesV2Peer::LEVEL_ID, UserLevelPeer::LEVEL_ID);


		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = SchemaAksesV2Peer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = MasterSchemaPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj2  = new $cls();
			$obj2->hydrate($rs, $startcol2);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj2 = $temp_obj1->getMasterSchema(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
					$temp_obj2->addSchemaAksesV2($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj2->initSchemaAksesV2s();
				$obj2->addSchemaAksesV2($obj1);
			}

			$omClass = UserLevelPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj3  = new $cls();
			$obj3->hydrate($rs, $startcol3);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj3 = $temp_obj1->getUserLevel(); 				if ($temp_obj3->getPrimaryKey() === $obj3->getPrimaryKey()) {
					$newObject = false;
					$temp_obj3->addSchemaAksesV2($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj3->initSchemaAksesV2s();
				$obj3->addSchemaAksesV2($obj1);
			}

			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doSelectJoinAllExceptMasterSchema(Criteria $c, $con = null)
	{
		$c = clone $c;

								if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		SchemaAksesV2Peer::addSelectColumns($c);
		$startcol2 = (SchemaAksesV2Peer::NUM_COLUMNS - SchemaAksesV2Peer::NUM_LAZY_LOAD_COLUMNS) + 1;

		MasterUserV2Peer::addSelectColumns($c);
		$startcol3 = $startcol2 + MasterUserV2Peer::NUM_COLUMNS;

		UserLevelPeer::addSelectColumns($c);
		$startcol4 = $startcol3 + UserLevelPeer::NUM_COLUMNS;

		$c->addJoin(SchemaAksesV2Peer::USER_ID, MasterUserV2Peer::USER_ID);

		$c->addJoin(SchemaAksesV2Peer::LEVEL_ID, UserLevelPeer::LEVEL_ID);


		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = SchemaAksesV2Peer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = MasterUserV2Peer::getOMClass();


			$cls = Propel::import($omClass);
			$obj2  = new $cls();
			$obj2->hydrate($rs, $startcol2);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj2 = $temp_obj1->getMasterUserV2(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
					$temp_obj2->addSchemaAksesV2($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj2->initSchemaAksesV2s();
				$obj2->addSchemaAksesV2($obj1);
			}

			$omClass = UserLevelPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj3  = new $cls();
			$obj3->hydrate($rs, $startcol3);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj3 = $temp_obj1->getUserLevel(); 				if ($temp_obj3->getPrimaryKey() === $obj3->getPrimaryKey()) {
					$newObject = false;
					$temp_obj3->addSchemaAksesV2($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj3->initSchemaAksesV2s();
				$obj3->addSchemaAksesV2($obj1);
			}

			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doSelectJoinAllExceptUserLevel(Criteria $c, $con = null)
	{
		$c = clone $c;

								if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		SchemaAksesV2Peer::addSelectColumns($c);
		$startcol2 = (SchemaAksesV2Peer::NUM_COLUMNS - SchemaAksesV2Peer::NUM_LAZY_LOAD_COLUMNS) + 1;

		MasterUserV2Peer::addSelectColumns($c);
		$startcol3 = $startcol2 + MasterUserV2Peer::NUM_COLUMNS;

		MasterSchemaPeer::addSelectColumns($c);
		$startcol4 = $startcol3 + MasterSchemaPeer::NUM_COLUMNS;

		$c->addJoin(SchemaAksesV2Peer::USER_ID, MasterUserV2Peer::USER_ID);

		$c->addJoin(SchemaAksesV2Peer::SCHEMA_ID, MasterSchemaPeer::SCHEMA_ID);


		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = SchemaAksesV2Peer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = MasterUserV2Peer::getOMClass();


			$cls = Propel::import($omClass);
			$obj2  = new $cls();
			$obj2->hydrate($rs, $startcol2);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj2 = $temp_obj1->getMasterUserV2(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
					$temp_obj2->addSchemaAksesV2($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj2->initSchemaAksesV2s();
				$obj2->addSchemaAksesV2($obj1);
			}

			$omClass = MasterSchemaPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj3  = new $cls();
			$obj3->hydrate($rs, $startcol3);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj3 = $temp_obj1->getMasterSchema(); 				if ($temp_obj3->getPrimaryKey() === $obj3->getPrimaryKey()) {
					$newObject = false;
					$temp_obj3->addSchemaAksesV2($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj3->initSchemaAksesV2s();
				$obj3->addSchemaAksesV2($obj1);
			}

			$results[] = $obj1;
		}
		return $results;
	}

	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return SchemaAksesV2Peer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(SchemaAksesV2Peer::USER_ID);
			$selectCriteria->add(SchemaAksesV2Peer::USER_ID, $criteria->remove(SchemaAksesV2Peer::USER_ID), $comparison);

			$comparison = $criteria->getComparison(SchemaAksesV2Peer::SCHEMA_ID);
			$selectCriteria->add(SchemaAksesV2Peer::SCHEMA_ID, $criteria->remove(SchemaAksesV2Peer::SCHEMA_ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(SchemaAksesV2Peer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(SchemaAksesV2Peer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof SchemaAksesV2) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
												if(count($values) == count($values, COUNT_RECURSIVE))
			{
								$values = array($values);
			}
			$vals = array();
			foreach($values as $value)
			{

				$vals[0][] = $value[0];
				$vals[1][] = $value[1];
			}

			$criteria->add(SchemaAksesV2Peer::USER_ID, $vals[0], Criteria::IN);
			$criteria->add(SchemaAksesV2Peer::SCHEMA_ID, $vals[1], Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(SchemaAksesV2 $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(SchemaAksesV2Peer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(SchemaAksesV2Peer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(SchemaAksesV2Peer::DATABASE_NAME, SchemaAksesV2Peer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = SchemaAksesV2Peer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK( $user_id, $schema_id, $con = null) {
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$criteria = new Criteria();
		$criteria->add(SchemaAksesV2Peer::USER_ID, $user_id);
		$criteria->add(SchemaAksesV2Peer::SCHEMA_ID, $schema_id);
		$v = SchemaAksesV2Peer::doSelect($criteria, $con);

		return !empty($v) ? $v[0] : null;
	}
} 
if (Propel::isInit()) {
			try {
		BaseSchemaAksesV2Peer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/SchemaAksesV2MapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.SchemaAksesV2MapBuilder');
}
