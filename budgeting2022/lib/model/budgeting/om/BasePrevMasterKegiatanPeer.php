<?php


abstract class BasePrevMasterKegiatanPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.prev_master_kegiatan';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.PrevMasterKegiatan';

	
	const NUM_COLUMNS = 43;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const UNIT_ID = 'ebudget.prev_master_kegiatan.UNIT_ID';

	
	const KODE_KEGIATAN = 'ebudget.prev_master_kegiatan.KODE_KEGIATAN';

	
	const KODE_BIDANG = 'ebudget.prev_master_kegiatan.KODE_BIDANG';

	
	const KODE_URUSAN_WAJIB = 'ebudget.prev_master_kegiatan.KODE_URUSAN_WAJIB';

	
	const KODE_PROGRAM = 'ebudget.prev_master_kegiatan.KODE_PROGRAM';

	
	const KODE_SASARAN = 'ebudget.prev_master_kegiatan.KODE_SASARAN';

	
	const KODE_INDIKATOR = 'ebudget.prev_master_kegiatan.KODE_INDIKATOR';

	
	const ALOKASI_DANA = 'ebudget.prev_master_kegiatan.ALOKASI_DANA';

	
	const NAMA_KEGIATAN = 'ebudget.prev_master_kegiatan.NAMA_KEGIATAN';

	
	const MASUKAN = 'ebudget.prev_master_kegiatan.MASUKAN';

	
	const OUTPUT = 'ebudget.prev_master_kegiatan.OUTPUT';

	
	const OUTCOME = 'ebudget.prev_master_kegiatan.OUTCOME';

	
	const BENEFIT = 'ebudget.prev_master_kegiatan.BENEFIT';

	
	const IMPACT = 'ebudget.prev_master_kegiatan.IMPACT';

	
	const TIPE = 'ebudget.prev_master_kegiatan.TIPE';

	
	const KEGIATAN_ACTIVE = 'ebudget.prev_master_kegiatan.KEGIATAN_ACTIVE';

	
	const TO_KEGIATAN_CODE = 'ebudget.prev_master_kegiatan.TO_KEGIATAN_CODE';

	
	const CATATAN = 'ebudget.prev_master_kegiatan.CATATAN';

	
	const TARGET_OUTCOME = 'ebudget.prev_master_kegiatan.TARGET_OUTCOME';

	
	const LOKASI = 'ebudget.prev_master_kegiatan.LOKASI';

	
	const JUMLAH_PREV = 'ebudget.prev_master_kegiatan.JUMLAH_PREV';

	
	const JUMLAH_NOW = 'ebudget.prev_master_kegiatan.JUMLAH_NOW';

	
	const JUMLAH_NEXT = 'ebudget.prev_master_kegiatan.JUMLAH_NEXT';

	
	const KODE_PROGRAM2 = 'ebudget.prev_master_kegiatan.KODE_PROGRAM2';

	
	const KODE_URUSAN = 'ebudget.prev_master_kegiatan.KODE_URUSAN';

	
	const LAST_UPDATE_USER = 'ebudget.prev_master_kegiatan.LAST_UPDATE_USER';

	
	const LAST_UPDATE_TIME = 'ebudget.prev_master_kegiatan.LAST_UPDATE_TIME';

	
	const LAST_UPDATE_IP = 'ebudget.prev_master_kegiatan.LAST_UPDATE_IP';

	
	const TAHAP = 'ebudget.prev_master_kegiatan.TAHAP';

	
	const KODE_MISI = 'ebudget.prev_master_kegiatan.KODE_MISI';

	
	const KODE_TUJUAN = 'ebudget.prev_master_kegiatan.KODE_TUJUAN';

	
	const RANKING = 'ebudget.prev_master_kegiatan.RANKING';

	
	const NOMOR13 = 'ebudget.prev_master_kegiatan.NOMOR13';

	
	const PPA_NAMA = 'ebudget.prev_master_kegiatan.PPA_NAMA';

	
	const PPA_PANGKAT = 'ebudget.prev_master_kegiatan.PPA_PANGKAT';

	
	const PPA_NIP = 'ebudget.prev_master_kegiatan.PPA_NIP';

	
	const LANJUTAN = 'ebudget.prev_master_kegiatan.LANJUTAN';

	
	const USER_ID = 'ebudget.prev_master_kegiatan.USER_ID';

	
	const ID = 'ebudget.prev_master_kegiatan.ID';

	
	const TAHUN = 'ebudget.prev_master_kegiatan.TAHUN';

	
	const TAMBAHAN_PAGU = 'ebudget.prev_master_kegiatan.TAMBAHAN_PAGU';

	
	const GENDER = 'ebudget.prev_master_kegiatan.GENDER';

	
	const KODE_KEG_KEUANGAN = 'ebudget.prev_master_kegiatan.KODE_KEG_KEUANGAN';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('UnitId', 'KodeKegiatan', 'KodeBidang', 'KodeUrusanWajib', 'KodeProgram', 'KodeSasaran', 'KodeIndikator', 'AlokasiDana', 'NamaKegiatan', 'Masukan', 'Output', 'Outcome', 'Benefit', 'Impact', 'Tipe', 'KegiatanActive', 'ToKegiatanCode', 'Catatan', 'TargetOutcome', 'Lokasi', 'JumlahPrev', 'JumlahNow', 'JumlahNext', 'KodeProgram2', 'KodeUrusan', 'LastUpdateUser', 'LastUpdateTime', 'LastUpdateIp', 'Tahap', 'KodeMisi', 'KodeTujuan', 'Ranking', 'Nomor13', 'PpaNama', 'PpaPangkat', 'PpaNip', 'Lanjutan', 'UserId', 'Id', 'Tahun', 'TambahanPagu', 'Gender', 'KodeKegKeuangan', ),
		BasePeer::TYPE_COLNAME => array (PrevMasterKegiatanPeer::UNIT_ID, PrevMasterKegiatanPeer::KODE_KEGIATAN, PrevMasterKegiatanPeer::KODE_BIDANG, PrevMasterKegiatanPeer::KODE_URUSAN_WAJIB, PrevMasterKegiatanPeer::KODE_PROGRAM, PrevMasterKegiatanPeer::KODE_SASARAN, PrevMasterKegiatanPeer::KODE_INDIKATOR, PrevMasterKegiatanPeer::ALOKASI_DANA, PrevMasterKegiatanPeer::NAMA_KEGIATAN, PrevMasterKegiatanPeer::MASUKAN, PrevMasterKegiatanPeer::OUTPUT, PrevMasterKegiatanPeer::OUTCOME, PrevMasterKegiatanPeer::BENEFIT, PrevMasterKegiatanPeer::IMPACT, PrevMasterKegiatanPeer::TIPE, PrevMasterKegiatanPeer::KEGIATAN_ACTIVE, PrevMasterKegiatanPeer::TO_KEGIATAN_CODE, PrevMasterKegiatanPeer::CATATAN, PrevMasterKegiatanPeer::TARGET_OUTCOME, PrevMasterKegiatanPeer::LOKASI, PrevMasterKegiatanPeer::JUMLAH_PREV, PrevMasterKegiatanPeer::JUMLAH_NOW, PrevMasterKegiatanPeer::JUMLAH_NEXT, PrevMasterKegiatanPeer::KODE_PROGRAM2, PrevMasterKegiatanPeer::KODE_URUSAN, PrevMasterKegiatanPeer::LAST_UPDATE_USER, PrevMasterKegiatanPeer::LAST_UPDATE_TIME, PrevMasterKegiatanPeer::LAST_UPDATE_IP, PrevMasterKegiatanPeer::TAHAP, PrevMasterKegiatanPeer::KODE_MISI, PrevMasterKegiatanPeer::KODE_TUJUAN, PrevMasterKegiatanPeer::RANKING, PrevMasterKegiatanPeer::NOMOR13, PrevMasterKegiatanPeer::PPA_NAMA, PrevMasterKegiatanPeer::PPA_PANGKAT, PrevMasterKegiatanPeer::PPA_NIP, PrevMasterKegiatanPeer::LANJUTAN, PrevMasterKegiatanPeer::USER_ID, PrevMasterKegiatanPeer::ID, PrevMasterKegiatanPeer::TAHUN, PrevMasterKegiatanPeer::TAMBAHAN_PAGU, PrevMasterKegiatanPeer::GENDER, PrevMasterKegiatanPeer::KODE_KEG_KEUANGAN, ),
		BasePeer::TYPE_FIELDNAME => array ('unit_id', 'kode_kegiatan', 'kode_bidang', 'kode_urusan_wajib', 'kode_program', 'kode_sasaran', 'kode_indikator', 'alokasi_dana', 'nama_kegiatan', 'masukan', 'output', 'outcome', 'benefit', 'impact', 'tipe', 'kegiatan_active', 'to_kegiatan_code', 'catatan', 'target_outcome', 'lokasi', 'jumlah_prev', 'jumlah_now', 'jumlah_next', 'kode_program2', 'kode_urusan', 'last_update_user', 'last_update_time', 'last_update_ip', 'tahap', 'kode_misi', 'kode_tujuan', 'ranking', 'nomor13', 'ppa_nama', 'ppa_pangkat', 'ppa_nip', 'lanjutan', 'user_id', 'id', 'tahun', 'tambahan_pagu', 'gender', 'kode_keg_keuangan', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('UnitId' => 0, 'KodeKegiatan' => 1, 'KodeBidang' => 2, 'KodeUrusanWajib' => 3, 'KodeProgram' => 4, 'KodeSasaran' => 5, 'KodeIndikator' => 6, 'AlokasiDana' => 7, 'NamaKegiatan' => 8, 'Masukan' => 9, 'Output' => 10, 'Outcome' => 11, 'Benefit' => 12, 'Impact' => 13, 'Tipe' => 14, 'KegiatanActive' => 15, 'ToKegiatanCode' => 16, 'Catatan' => 17, 'TargetOutcome' => 18, 'Lokasi' => 19, 'JumlahPrev' => 20, 'JumlahNow' => 21, 'JumlahNext' => 22, 'KodeProgram2' => 23, 'KodeUrusan' => 24, 'LastUpdateUser' => 25, 'LastUpdateTime' => 26, 'LastUpdateIp' => 27, 'Tahap' => 28, 'KodeMisi' => 29, 'KodeTujuan' => 30, 'Ranking' => 31, 'Nomor13' => 32, 'PpaNama' => 33, 'PpaPangkat' => 34, 'PpaNip' => 35, 'Lanjutan' => 36, 'UserId' => 37, 'Id' => 38, 'Tahun' => 39, 'TambahanPagu' => 40, 'Gender' => 41, 'KodeKegKeuangan' => 42, ),
		BasePeer::TYPE_COLNAME => array (PrevMasterKegiatanPeer::UNIT_ID => 0, PrevMasterKegiatanPeer::KODE_KEGIATAN => 1, PrevMasterKegiatanPeer::KODE_BIDANG => 2, PrevMasterKegiatanPeer::KODE_URUSAN_WAJIB => 3, PrevMasterKegiatanPeer::KODE_PROGRAM => 4, PrevMasterKegiatanPeer::KODE_SASARAN => 5, PrevMasterKegiatanPeer::KODE_INDIKATOR => 6, PrevMasterKegiatanPeer::ALOKASI_DANA => 7, PrevMasterKegiatanPeer::NAMA_KEGIATAN => 8, PrevMasterKegiatanPeer::MASUKAN => 9, PrevMasterKegiatanPeer::OUTPUT => 10, PrevMasterKegiatanPeer::OUTCOME => 11, PrevMasterKegiatanPeer::BENEFIT => 12, PrevMasterKegiatanPeer::IMPACT => 13, PrevMasterKegiatanPeer::TIPE => 14, PrevMasterKegiatanPeer::KEGIATAN_ACTIVE => 15, PrevMasterKegiatanPeer::TO_KEGIATAN_CODE => 16, PrevMasterKegiatanPeer::CATATAN => 17, PrevMasterKegiatanPeer::TARGET_OUTCOME => 18, PrevMasterKegiatanPeer::LOKASI => 19, PrevMasterKegiatanPeer::JUMLAH_PREV => 20, PrevMasterKegiatanPeer::JUMLAH_NOW => 21, PrevMasterKegiatanPeer::JUMLAH_NEXT => 22, PrevMasterKegiatanPeer::KODE_PROGRAM2 => 23, PrevMasterKegiatanPeer::KODE_URUSAN => 24, PrevMasterKegiatanPeer::LAST_UPDATE_USER => 25, PrevMasterKegiatanPeer::LAST_UPDATE_TIME => 26, PrevMasterKegiatanPeer::LAST_UPDATE_IP => 27, PrevMasterKegiatanPeer::TAHAP => 28, PrevMasterKegiatanPeer::KODE_MISI => 29, PrevMasterKegiatanPeer::KODE_TUJUAN => 30, PrevMasterKegiatanPeer::RANKING => 31, PrevMasterKegiatanPeer::NOMOR13 => 32, PrevMasterKegiatanPeer::PPA_NAMA => 33, PrevMasterKegiatanPeer::PPA_PANGKAT => 34, PrevMasterKegiatanPeer::PPA_NIP => 35, PrevMasterKegiatanPeer::LANJUTAN => 36, PrevMasterKegiatanPeer::USER_ID => 37, PrevMasterKegiatanPeer::ID => 38, PrevMasterKegiatanPeer::TAHUN => 39, PrevMasterKegiatanPeer::TAMBAHAN_PAGU => 40, PrevMasterKegiatanPeer::GENDER => 41, PrevMasterKegiatanPeer::KODE_KEG_KEUANGAN => 42, ),
		BasePeer::TYPE_FIELDNAME => array ('unit_id' => 0, 'kode_kegiatan' => 1, 'kode_bidang' => 2, 'kode_urusan_wajib' => 3, 'kode_program' => 4, 'kode_sasaran' => 5, 'kode_indikator' => 6, 'alokasi_dana' => 7, 'nama_kegiatan' => 8, 'masukan' => 9, 'output' => 10, 'outcome' => 11, 'benefit' => 12, 'impact' => 13, 'tipe' => 14, 'kegiatan_active' => 15, 'to_kegiatan_code' => 16, 'catatan' => 17, 'target_outcome' => 18, 'lokasi' => 19, 'jumlah_prev' => 20, 'jumlah_now' => 21, 'jumlah_next' => 22, 'kode_program2' => 23, 'kode_urusan' => 24, 'last_update_user' => 25, 'last_update_time' => 26, 'last_update_ip' => 27, 'tahap' => 28, 'kode_misi' => 29, 'kode_tujuan' => 30, 'ranking' => 31, 'nomor13' => 32, 'ppa_nama' => 33, 'ppa_pangkat' => 34, 'ppa_nip' => 35, 'lanjutan' => 36, 'user_id' => 37, 'id' => 38, 'tahun' => 39, 'tambahan_pagu' => 40, 'gender' => 41, 'kode_keg_keuangan' => 42, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/PrevMasterKegiatanMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.PrevMasterKegiatanMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = PrevMasterKegiatanPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(PrevMasterKegiatanPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(PrevMasterKegiatanPeer::UNIT_ID);

		$criteria->addSelectColumn(PrevMasterKegiatanPeer::KODE_KEGIATAN);

		$criteria->addSelectColumn(PrevMasterKegiatanPeer::KODE_BIDANG);

		$criteria->addSelectColumn(PrevMasterKegiatanPeer::KODE_URUSAN_WAJIB);

		$criteria->addSelectColumn(PrevMasterKegiatanPeer::KODE_PROGRAM);

		$criteria->addSelectColumn(PrevMasterKegiatanPeer::KODE_SASARAN);

		$criteria->addSelectColumn(PrevMasterKegiatanPeer::KODE_INDIKATOR);

		$criteria->addSelectColumn(PrevMasterKegiatanPeer::ALOKASI_DANA);

		$criteria->addSelectColumn(PrevMasterKegiatanPeer::NAMA_KEGIATAN);

		$criteria->addSelectColumn(PrevMasterKegiatanPeer::MASUKAN);

		$criteria->addSelectColumn(PrevMasterKegiatanPeer::OUTPUT);

		$criteria->addSelectColumn(PrevMasterKegiatanPeer::OUTCOME);

		$criteria->addSelectColumn(PrevMasterKegiatanPeer::BENEFIT);

		$criteria->addSelectColumn(PrevMasterKegiatanPeer::IMPACT);

		$criteria->addSelectColumn(PrevMasterKegiatanPeer::TIPE);

		$criteria->addSelectColumn(PrevMasterKegiatanPeer::KEGIATAN_ACTIVE);

		$criteria->addSelectColumn(PrevMasterKegiatanPeer::TO_KEGIATAN_CODE);

		$criteria->addSelectColumn(PrevMasterKegiatanPeer::CATATAN);

		$criteria->addSelectColumn(PrevMasterKegiatanPeer::TARGET_OUTCOME);

		$criteria->addSelectColumn(PrevMasterKegiatanPeer::LOKASI);

		$criteria->addSelectColumn(PrevMasterKegiatanPeer::JUMLAH_PREV);

		$criteria->addSelectColumn(PrevMasterKegiatanPeer::JUMLAH_NOW);

		$criteria->addSelectColumn(PrevMasterKegiatanPeer::JUMLAH_NEXT);

		$criteria->addSelectColumn(PrevMasterKegiatanPeer::KODE_PROGRAM2);

		$criteria->addSelectColumn(PrevMasterKegiatanPeer::KODE_URUSAN);

		$criteria->addSelectColumn(PrevMasterKegiatanPeer::LAST_UPDATE_USER);

		$criteria->addSelectColumn(PrevMasterKegiatanPeer::LAST_UPDATE_TIME);

		$criteria->addSelectColumn(PrevMasterKegiatanPeer::LAST_UPDATE_IP);

		$criteria->addSelectColumn(PrevMasterKegiatanPeer::TAHAP);

		$criteria->addSelectColumn(PrevMasterKegiatanPeer::KODE_MISI);

		$criteria->addSelectColumn(PrevMasterKegiatanPeer::KODE_TUJUAN);

		$criteria->addSelectColumn(PrevMasterKegiatanPeer::RANKING);

		$criteria->addSelectColumn(PrevMasterKegiatanPeer::NOMOR13);

		$criteria->addSelectColumn(PrevMasterKegiatanPeer::PPA_NAMA);

		$criteria->addSelectColumn(PrevMasterKegiatanPeer::PPA_PANGKAT);

		$criteria->addSelectColumn(PrevMasterKegiatanPeer::PPA_NIP);

		$criteria->addSelectColumn(PrevMasterKegiatanPeer::LANJUTAN);

		$criteria->addSelectColumn(PrevMasterKegiatanPeer::USER_ID);

		$criteria->addSelectColumn(PrevMasterKegiatanPeer::ID);

		$criteria->addSelectColumn(PrevMasterKegiatanPeer::TAHUN);

		$criteria->addSelectColumn(PrevMasterKegiatanPeer::TAMBAHAN_PAGU);

		$criteria->addSelectColumn(PrevMasterKegiatanPeer::GENDER);

		$criteria->addSelectColumn(PrevMasterKegiatanPeer::KODE_KEG_KEUANGAN);

	}

	const COUNT = 'COUNT(ebudget.prev_master_kegiatan.UNIT_ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.prev_master_kegiatan.UNIT_ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(PrevMasterKegiatanPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(PrevMasterKegiatanPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = PrevMasterKegiatanPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = PrevMasterKegiatanPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return PrevMasterKegiatanPeer::populateObjects(PrevMasterKegiatanPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			PrevMasterKegiatanPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = PrevMasterKegiatanPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return PrevMasterKegiatanPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}

		$criteria->remove(PrevMasterKegiatanPeer::ID); 

				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(PrevMasterKegiatanPeer::UNIT_ID);
			$selectCriteria->add(PrevMasterKegiatanPeer::UNIT_ID, $criteria->remove(PrevMasterKegiatanPeer::UNIT_ID), $comparison);

			$comparison = $criteria->getComparison(PrevMasterKegiatanPeer::KODE_KEGIATAN);
			$selectCriteria->add(PrevMasterKegiatanPeer::KODE_KEGIATAN, $criteria->remove(PrevMasterKegiatanPeer::KODE_KEGIATAN), $comparison);

			$comparison = $criteria->getComparison(PrevMasterKegiatanPeer::ID);
			$selectCriteria->add(PrevMasterKegiatanPeer::ID, $criteria->remove(PrevMasterKegiatanPeer::ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(PrevMasterKegiatanPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(PrevMasterKegiatanPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof PrevMasterKegiatan) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
												if(count($values) == count($values, COUNT_RECURSIVE))
			{
								$values = array($values);
			}
			$vals = array();
			foreach($values as $value)
			{

				$vals[0][] = $value[0];
				$vals[1][] = $value[1];
				$vals[2][] = $value[2];
			}

			$criteria->add(PrevMasterKegiatanPeer::UNIT_ID, $vals[0], Criteria::IN);
			$criteria->add(PrevMasterKegiatanPeer::KODE_KEGIATAN, $vals[1], Criteria::IN);
			$criteria->add(PrevMasterKegiatanPeer::ID, $vals[2], Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(PrevMasterKegiatan $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(PrevMasterKegiatanPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(PrevMasterKegiatanPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(PrevMasterKegiatanPeer::DATABASE_NAME, PrevMasterKegiatanPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = PrevMasterKegiatanPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK( $unit_id, $kode_kegiatan, $id, $con = null) {
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$criteria = new Criteria();
		$criteria->add(PrevMasterKegiatanPeer::UNIT_ID, $unit_id);
		$criteria->add(PrevMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
		$criteria->add(PrevMasterKegiatanPeer::ID, $id);
		$v = PrevMasterKegiatanPeer::doSelect($criteria, $con);

		return !empty($v) ? $v[0] : null;
	}
} 
if (Propel::isInit()) {
			try {
		BasePrevMasterKegiatanPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/PrevMasterKegiatanMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.PrevMasterKegiatanMapBuilder');
}
