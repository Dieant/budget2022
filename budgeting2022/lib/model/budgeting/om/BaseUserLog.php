<?php


abstract class BaseUserLog extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id;


	
	protected $waktu;


	
	protected $nama_user;


	
	protected $deskripsi;


	
	protected $ip_address;


	
	protected $nama_lengkap;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getId()
	{

		return $this->id;
	}

	
	public function getWaktu($format = 'Y-m-d H:i:s')
	{

		if ($this->waktu === null || $this->waktu === '') {
			return null;
		} elseif (!is_int($this->waktu)) {
						$ts = strtotime($this->waktu);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [waktu] as date/time value: " . var_export($this->waktu, true));
			}
		} else {
			$ts = $this->waktu;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getNamaUser()
	{

		return $this->nama_user;
	}

	
	public function getDeskripsi()
	{

		return $this->deskripsi;
	}

	
	public function getIpAddress()
	{

		return $this->ip_address;
	}

	
	public function getNamaLengkap()
	{

		return $this->nama_lengkap;
	}

	
	public function setId($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = UserLogPeer::ID;
		}

	} 
	
	public function setWaktu($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [waktu] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->waktu !== $ts) {
			$this->waktu = $ts;
			$this->modifiedColumns[] = UserLogPeer::WAKTU;
		}

	} 
	
	public function setNamaUser($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nama_user !== $v) {
			$this->nama_user = $v;
			$this->modifiedColumns[] = UserLogPeer::NAMA_USER;
		}

	} 
	
	public function setDeskripsi($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->deskripsi !== $v) {
			$this->deskripsi = $v;
			$this->modifiedColumns[] = UserLogPeer::DESKRIPSI;
		}

	} 
	
	public function setIpAddress($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->ip_address !== $v) {
			$this->ip_address = $v;
			$this->modifiedColumns[] = UserLogPeer::IP_ADDRESS;
		}

	} 
	
	public function setNamaLengkap($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nama_lengkap !== $v) {
			$this->nama_lengkap = $v;
			$this->modifiedColumns[] = UserLogPeer::NAMA_LENGKAP;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getInt($startcol + 0);

			$this->waktu = $rs->getTimestamp($startcol + 1, null);

			$this->nama_user = $rs->getString($startcol + 2);

			$this->deskripsi = $rs->getString($startcol + 3);

			$this->ip_address = $rs->getString($startcol + 4);

			$this->nama_lengkap = $rs->getString($startcol + 5);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 6; 
		} catch (Exception $e) {
			throw new PropelException("Error populating UserLog object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(UserLogPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			UserLogPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(UserLogPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = UserLogPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setId($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += UserLogPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = UserLogPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = UserLogPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getWaktu();
				break;
			case 2:
				return $this->getNamaUser();
				break;
			case 3:
				return $this->getDeskripsi();
				break;
			case 4:
				return $this->getIpAddress();
				break;
			case 5:
				return $this->getNamaLengkap();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = UserLogPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getWaktu(),
			$keys[2] => $this->getNamaUser(),
			$keys[3] => $this->getDeskripsi(),
			$keys[4] => $this->getIpAddress(),
			$keys[5] => $this->getNamaLengkap(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = UserLogPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setWaktu($value);
				break;
			case 2:
				$this->setNamaUser($value);
				break;
			case 3:
				$this->setDeskripsi($value);
				break;
			case 4:
				$this->setIpAddress($value);
				break;
			case 5:
				$this->setNamaLengkap($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = UserLogPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setWaktu($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setNamaUser($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setDeskripsi($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setIpAddress($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setNamaLengkap($arr[$keys[5]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(UserLogPeer::DATABASE_NAME);

		if ($this->isColumnModified(UserLogPeer::ID)) $criteria->add(UserLogPeer::ID, $this->id);
		if ($this->isColumnModified(UserLogPeer::WAKTU)) $criteria->add(UserLogPeer::WAKTU, $this->waktu);
		if ($this->isColumnModified(UserLogPeer::NAMA_USER)) $criteria->add(UserLogPeer::NAMA_USER, $this->nama_user);
		if ($this->isColumnModified(UserLogPeer::DESKRIPSI)) $criteria->add(UserLogPeer::DESKRIPSI, $this->deskripsi);
		if ($this->isColumnModified(UserLogPeer::IP_ADDRESS)) $criteria->add(UserLogPeer::IP_ADDRESS, $this->ip_address);
		if ($this->isColumnModified(UserLogPeer::NAMA_LENGKAP)) $criteria->add(UserLogPeer::NAMA_LENGKAP, $this->nama_lengkap);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(UserLogPeer::DATABASE_NAME);

		$criteria->add(UserLogPeer::ID, $this->id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setWaktu($this->waktu);

		$copyObj->setNamaUser($this->nama_user);

		$copyObj->setDeskripsi($this->deskripsi);

		$copyObj->setIpAddress($this->ip_address);

		$copyObj->setNamaLengkap($this->nama_lengkap);


		$copyObj->setNew(true);

		$copyObj->setId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new UserLogPeer();
		}
		return self::$peer;
	}

} 