<?php


abstract class BaseRevisi1RincianDetail extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $kegiatan_code;


	
	protected $tipe;


	
	protected $detail_no;


	
	protected $rekening_code;


	
	protected $komponen_id;


	
	protected $detail_name;


	
	protected $volume;


	
	protected $keterangan_koefisien;


	
	protected $subtitle;


	
	protected $komponen_harga;


	
	protected $komponen_harga_awal;


	
	protected $komponen_name;


	
	protected $satuan;


	
	protected $pajak = 0;


	
	protected $unit_id;


	
	protected $from_sub_kegiatan;


	
	protected $sub;


	
	protected $kode_sub;


	
	protected $last_update_user;


	
	protected $last_update_time;


	
	protected $last_update_ip;


	
	protected $tahap;


	
	protected $tahap_edit;


	
	protected $tahap_new;


	
	protected $status_lelang;


	
	protected $nomor_lelang;


	
	protected $koefisien_semula;


	
	protected $volume_semula;


	
	protected $harga_semula;


	
	protected $total_semula;


	
	protected $lock_subtitle;


	
	protected $status_hapus = false;


	
	protected $tahun;


	
	protected $kode_lokasi;


	
	protected $kecamatan;


	
	protected $rekening_code_asli;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getKegiatanCode()
	{

		return $this->kegiatan_code;
	}

	
	public function getTipe()
	{

		return $this->tipe;
	}

	
	public function getDetailNo()
	{

		return $this->detail_no;
	}

	
	public function getRekeningCode()
	{

		return $this->rekening_code;
	}

	
	public function getKomponenId()
	{

		return $this->komponen_id;
	}

	
	public function getDetailName()
	{

		return $this->detail_name;
	}

	
	public function getVolume()
	{

		return $this->volume;
	}

	
	public function getKeteranganKoefisien()
	{

		return $this->keterangan_koefisien;
	}

	
	public function getSubtitle()
	{

		return $this->subtitle;
	}

	
	public function getKomponenHarga()
	{

		return $this->komponen_harga;
	}

	
	public function getKomponenHargaAwal()
	{

		return $this->komponen_harga_awal;
	}

	
	public function getKomponenName()
	{

		return $this->komponen_name;
	}

	
	public function getSatuan()
	{

		return $this->satuan;
	}

	
	public function getPajak()
	{

		return $this->pajak;
	}

	
	public function getUnitId()
	{

		return $this->unit_id;
	}

	
	public function getFromSubKegiatan()
	{

		return $this->from_sub_kegiatan;
	}

	
	public function getSub()
	{

		return $this->sub;
	}

	
	public function getKodeSub()
	{

		return $this->kode_sub;
	}

	
	public function getLastUpdateUser()
	{

		return $this->last_update_user;
	}

	
	public function getLastUpdateTime($format = 'Y-m-d H:i:s')
	{

		if ($this->last_update_time === null || $this->last_update_time === '') {
			return null;
		} elseif (!is_int($this->last_update_time)) {
						$ts = strtotime($this->last_update_time);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [last_update_time] as date/time value: " . var_export($this->last_update_time, true));
			}
		} else {
			$ts = $this->last_update_time;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getLastUpdateIp()
	{

		return $this->last_update_ip;
	}

	
	public function getTahap()
	{

		return $this->tahap;
	}

	
	public function getTahapEdit()
	{

		return $this->tahap_edit;
	}

	
	public function getTahapNew()
	{

		return $this->tahap_new;
	}

	
	public function getStatusLelang()
	{

		return $this->status_lelang;
	}

	
	public function getNomorLelang()
	{

		return $this->nomor_lelang;
	}

	
	public function getKoefisienSemula()
	{

		return $this->koefisien_semula;
	}

	
	public function getVolumeSemula()
	{

		return $this->volume_semula;
	}

	
	public function getHargaSemula()
	{

		return $this->harga_semula;
	}

	
	public function getTotalSemula()
	{

		return $this->total_semula;
	}

	
	public function getLockSubtitle()
	{

		return $this->lock_subtitle;
	}

	
	public function getStatusHapus()
	{

		return $this->status_hapus;
	}

	
	public function getTahun()
	{

		return $this->tahun;
	}

	
	public function getKodeLokasi()
	{

		return $this->kode_lokasi;
	}

	
	public function getKecamatan()
	{

		return $this->kecamatan;
	}

	
	public function getRekeningCodeAsli()
	{

		return $this->rekening_code_asli;
	}

	
	public function setKegiatanCode($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kegiatan_code !== $v) {
			$this->kegiatan_code = $v;
			$this->modifiedColumns[] = Revisi1RincianDetailPeer::KEGIATAN_CODE;
		}

	} 
	
	public function setTipe($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->tipe !== $v) {
			$this->tipe = $v;
			$this->modifiedColumns[] = Revisi1RincianDetailPeer::TIPE;
		}

	} 
	
	public function setDetailNo($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->detail_no !== $v) {
			$this->detail_no = $v;
			$this->modifiedColumns[] = Revisi1RincianDetailPeer::DETAIL_NO;
		}

	} 
	
	public function setRekeningCode($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->rekening_code !== $v) {
			$this->rekening_code = $v;
			$this->modifiedColumns[] = Revisi1RincianDetailPeer::REKENING_CODE;
		}

	} 
	
	public function setKomponenId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->komponen_id !== $v) {
			$this->komponen_id = $v;
			$this->modifiedColumns[] = Revisi1RincianDetailPeer::KOMPONEN_ID;
		}

	} 
	
	public function setDetailName($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->detail_name !== $v) {
			$this->detail_name = $v;
			$this->modifiedColumns[] = Revisi1RincianDetailPeer::DETAIL_NAME;
		}

	} 
	
	public function setVolume($v)
	{

		if ($this->volume !== $v) {
			$this->volume = $v;
			$this->modifiedColumns[] = Revisi1RincianDetailPeer::VOLUME;
		}

	} 
	
	public function setKeteranganKoefisien($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->keterangan_koefisien !== $v) {
			$this->keterangan_koefisien = $v;
			$this->modifiedColumns[] = Revisi1RincianDetailPeer::KETERANGAN_KOEFISIEN;
		}

	} 
	
	public function setSubtitle($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->subtitle !== $v) {
			$this->subtitle = $v;
			$this->modifiedColumns[] = Revisi1RincianDetailPeer::SUBTITLE;
		}

	} 
	
	public function setKomponenHarga($v)
	{

		if ($this->komponen_harga !== $v) {
			$this->komponen_harga = $v;
			$this->modifiedColumns[] = Revisi1RincianDetailPeer::KOMPONEN_HARGA;
		}

	} 
	
	public function setKomponenHargaAwal($v)
	{

		if ($this->komponen_harga_awal !== $v) {
			$this->komponen_harga_awal = $v;
			$this->modifiedColumns[] = Revisi1RincianDetailPeer::KOMPONEN_HARGA_AWAL;
		}

	} 
	
	public function setKomponenName($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->komponen_name !== $v) {
			$this->komponen_name = $v;
			$this->modifiedColumns[] = Revisi1RincianDetailPeer::KOMPONEN_NAME;
		}

	} 
	
	public function setSatuan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->satuan !== $v) {
			$this->satuan = $v;
			$this->modifiedColumns[] = Revisi1RincianDetailPeer::SATUAN;
		}

	} 
	
	public function setPajak($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->pajak !== $v || $v === 0) {
			$this->pajak = $v;
			$this->modifiedColumns[] = Revisi1RincianDetailPeer::PAJAK;
		}

	} 
	
	public function setUnitId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->unit_id !== $v) {
			$this->unit_id = $v;
			$this->modifiedColumns[] = Revisi1RincianDetailPeer::UNIT_ID;
		}

	} 
	
	public function setFromSubKegiatan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->from_sub_kegiatan !== $v) {
			$this->from_sub_kegiatan = $v;
			$this->modifiedColumns[] = Revisi1RincianDetailPeer::FROM_SUB_KEGIATAN;
		}

	} 
	
	public function setSub($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->sub !== $v) {
			$this->sub = $v;
			$this->modifiedColumns[] = Revisi1RincianDetailPeer::SUB;
		}

	} 
	
	public function setKodeSub($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_sub !== $v) {
			$this->kode_sub = $v;
			$this->modifiedColumns[] = Revisi1RincianDetailPeer::KODE_SUB;
		}

	} 
	
	public function setLastUpdateUser($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->last_update_user !== $v) {
			$this->last_update_user = $v;
			$this->modifiedColumns[] = Revisi1RincianDetailPeer::LAST_UPDATE_USER;
		}

	} 
	
	public function setLastUpdateTime($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [last_update_time] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->last_update_time !== $ts) {
			$this->last_update_time = $ts;
			$this->modifiedColumns[] = Revisi1RincianDetailPeer::LAST_UPDATE_TIME;
		}

	} 
	
	public function setLastUpdateIp($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->last_update_ip !== $v) {
			$this->last_update_ip = $v;
			$this->modifiedColumns[] = Revisi1RincianDetailPeer::LAST_UPDATE_IP;
		}

	} 
	
	public function setTahap($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->tahap !== $v) {
			$this->tahap = $v;
			$this->modifiedColumns[] = Revisi1RincianDetailPeer::TAHAP;
		}

	} 
	
	public function setTahapEdit($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->tahap_edit !== $v) {
			$this->tahap_edit = $v;
			$this->modifiedColumns[] = Revisi1RincianDetailPeer::TAHAP_EDIT;
		}

	} 
	
	public function setTahapNew($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->tahap_new !== $v) {
			$this->tahap_new = $v;
			$this->modifiedColumns[] = Revisi1RincianDetailPeer::TAHAP_NEW;
		}

	} 
	
	public function setStatusLelang($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->status_lelang !== $v) {
			$this->status_lelang = $v;
			$this->modifiedColumns[] = Revisi1RincianDetailPeer::STATUS_LELANG;
		}

	} 
	
	public function setNomorLelang($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nomor_lelang !== $v) {
			$this->nomor_lelang = $v;
			$this->modifiedColumns[] = Revisi1RincianDetailPeer::NOMOR_LELANG;
		}

	} 
	
	public function setKoefisienSemula($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->koefisien_semula !== $v) {
			$this->koefisien_semula = $v;
			$this->modifiedColumns[] = Revisi1RincianDetailPeer::KOEFISIEN_SEMULA;
		}

	} 
	
	public function setVolumeSemula($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->volume_semula !== $v) {
			$this->volume_semula = $v;
			$this->modifiedColumns[] = Revisi1RincianDetailPeer::VOLUME_SEMULA;
		}

	} 
	
	public function setHargaSemula($v)
	{

		if ($this->harga_semula !== $v) {
			$this->harga_semula = $v;
			$this->modifiedColumns[] = Revisi1RincianDetailPeer::HARGA_SEMULA;
		}

	} 
	
	public function setTotalSemula($v)
	{

		if ($this->total_semula !== $v) {
			$this->total_semula = $v;
			$this->modifiedColumns[] = Revisi1RincianDetailPeer::TOTAL_SEMULA;
		}

	} 
	
	public function setLockSubtitle($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->lock_subtitle !== $v) {
			$this->lock_subtitle = $v;
			$this->modifiedColumns[] = Revisi1RincianDetailPeer::LOCK_SUBTITLE;
		}

	} 
	
	public function setStatusHapus($v)
	{

		if ($this->status_hapus !== $v || $v === false) {
			$this->status_hapus = $v;
			$this->modifiedColumns[] = Revisi1RincianDetailPeer::STATUS_HAPUS;
		}

	} 
	
	public function setTahun($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->tahun !== $v) {
			$this->tahun = $v;
			$this->modifiedColumns[] = Revisi1RincianDetailPeer::TAHUN;
		}

	} 
	
	public function setKodeLokasi($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_lokasi !== $v) {
			$this->kode_lokasi = $v;
			$this->modifiedColumns[] = Revisi1RincianDetailPeer::KODE_LOKASI;
		}

	} 
	
	public function setKecamatan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kecamatan !== $v) {
			$this->kecamatan = $v;
			$this->modifiedColumns[] = Revisi1RincianDetailPeer::KECAMATAN;
		}

	} 
	
	public function setRekeningCodeAsli($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->rekening_code_asli !== $v) {
			$this->rekening_code_asli = $v;
			$this->modifiedColumns[] = Revisi1RincianDetailPeer::REKENING_CODE_ASLI;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->kegiatan_code = $rs->getString($startcol + 0);

			$this->tipe = $rs->getString($startcol + 1);

			$this->detail_no = $rs->getInt($startcol + 2);

			$this->rekening_code = $rs->getString($startcol + 3);

			$this->komponen_id = $rs->getString($startcol + 4);

			$this->detail_name = $rs->getString($startcol + 5);

			$this->volume = $rs->getFloat($startcol + 6);

			$this->keterangan_koefisien = $rs->getString($startcol + 7);

			$this->subtitle = $rs->getString($startcol + 8);

			$this->komponen_harga = $rs->getFloat($startcol + 9);

			$this->komponen_harga_awal = $rs->getFloat($startcol + 10);

			$this->komponen_name = $rs->getString($startcol + 11);

			$this->satuan = $rs->getString($startcol + 12);

			$this->pajak = $rs->getInt($startcol + 13);

			$this->unit_id = $rs->getString($startcol + 14);

			$this->from_sub_kegiatan = $rs->getString($startcol + 15);

			$this->sub = $rs->getString($startcol + 16);

			$this->kode_sub = $rs->getString($startcol + 17);

			$this->last_update_user = $rs->getString($startcol + 18);

			$this->last_update_time = $rs->getTimestamp($startcol + 19, null);

			$this->last_update_ip = $rs->getString($startcol + 20);

			$this->tahap = $rs->getString($startcol + 21);

			$this->tahap_edit = $rs->getString($startcol + 22);

			$this->tahap_new = $rs->getString($startcol + 23);

			$this->status_lelang = $rs->getString($startcol + 24);

			$this->nomor_lelang = $rs->getString($startcol + 25);

			$this->koefisien_semula = $rs->getString($startcol + 26);

			$this->volume_semula = $rs->getInt($startcol + 27);

			$this->harga_semula = $rs->getFloat($startcol + 28);

			$this->total_semula = $rs->getFloat($startcol + 29);

			$this->lock_subtitle = $rs->getString($startcol + 30);

			$this->status_hapus = $rs->getBoolean($startcol + 31);

			$this->tahun = $rs->getString($startcol + 32);

			$this->kode_lokasi = $rs->getString($startcol + 33);

			$this->kecamatan = $rs->getString($startcol + 34);

			$this->rekening_code_asli = $rs->getString($startcol + 35);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 36; 
		} catch (Exception $e) {
			throw new PropelException("Error populating Revisi1RincianDetail object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(Revisi1RincianDetailPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			Revisi1RincianDetailPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(Revisi1RincianDetailPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = Revisi1RincianDetailPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += Revisi1RincianDetailPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = Revisi1RincianDetailPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = Revisi1RincianDetailPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getKegiatanCode();
				break;
			case 1:
				return $this->getTipe();
				break;
			case 2:
				return $this->getDetailNo();
				break;
			case 3:
				return $this->getRekeningCode();
				break;
			case 4:
				return $this->getKomponenId();
				break;
			case 5:
				return $this->getDetailName();
				break;
			case 6:
				return $this->getVolume();
				break;
			case 7:
				return $this->getKeteranganKoefisien();
				break;
			case 8:
				return $this->getSubtitle();
				break;
			case 9:
				return $this->getKomponenHarga();
				break;
			case 10:
				return $this->getKomponenHargaAwal();
				break;
			case 11:
				return $this->getKomponenName();
				break;
			case 12:
				return $this->getSatuan();
				break;
			case 13:
				return $this->getPajak();
				break;
			case 14:
				return $this->getUnitId();
				break;
			case 15:
				return $this->getFromSubKegiatan();
				break;
			case 16:
				return $this->getSub();
				break;
			case 17:
				return $this->getKodeSub();
				break;
			case 18:
				return $this->getLastUpdateUser();
				break;
			case 19:
				return $this->getLastUpdateTime();
				break;
			case 20:
				return $this->getLastUpdateIp();
				break;
			case 21:
				return $this->getTahap();
				break;
			case 22:
				return $this->getTahapEdit();
				break;
			case 23:
				return $this->getTahapNew();
				break;
			case 24:
				return $this->getStatusLelang();
				break;
			case 25:
				return $this->getNomorLelang();
				break;
			case 26:
				return $this->getKoefisienSemula();
				break;
			case 27:
				return $this->getVolumeSemula();
				break;
			case 28:
				return $this->getHargaSemula();
				break;
			case 29:
				return $this->getTotalSemula();
				break;
			case 30:
				return $this->getLockSubtitle();
				break;
			case 31:
				return $this->getStatusHapus();
				break;
			case 32:
				return $this->getTahun();
				break;
			case 33:
				return $this->getKodeLokasi();
				break;
			case 34:
				return $this->getKecamatan();
				break;
			case 35:
				return $this->getRekeningCodeAsli();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = Revisi1RincianDetailPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getKegiatanCode(),
			$keys[1] => $this->getTipe(),
			$keys[2] => $this->getDetailNo(),
			$keys[3] => $this->getRekeningCode(),
			$keys[4] => $this->getKomponenId(),
			$keys[5] => $this->getDetailName(),
			$keys[6] => $this->getVolume(),
			$keys[7] => $this->getKeteranganKoefisien(),
			$keys[8] => $this->getSubtitle(),
			$keys[9] => $this->getKomponenHarga(),
			$keys[10] => $this->getKomponenHargaAwal(),
			$keys[11] => $this->getKomponenName(),
			$keys[12] => $this->getSatuan(),
			$keys[13] => $this->getPajak(),
			$keys[14] => $this->getUnitId(),
			$keys[15] => $this->getFromSubKegiatan(),
			$keys[16] => $this->getSub(),
			$keys[17] => $this->getKodeSub(),
			$keys[18] => $this->getLastUpdateUser(),
			$keys[19] => $this->getLastUpdateTime(),
			$keys[20] => $this->getLastUpdateIp(),
			$keys[21] => $this->getTahap(),
			$keys[22] => $this->getTahapEdit(),
			$keys[23] => $this->getTahapNew(),
			$keys[24] => $this->getStatusLelang(),
			$keys[25] => $this->getNomorLelang(),
			$keys[26] => $this->getKoefisienSemula(),
			$keys[27] => $this->getVolumeSemula(),
			$keys[28] => $this->getHargaSemula(),
			$keys[29] => $this->getTotalSemula(),
			$keys[30] => $this->getLockSubtitle(),
			$keys[31] => $this->getStatusHapus(),
			$keys[32] => $this->getTahun(),
			$keys[33] => $this->getKodeLokasi(),
			$keys[34] => $this->getKecamatan(),
			$keys[35] => $this->getRekeningCodeAsli(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = Revisi1RincianDetailPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setKegiatanCode($value);
				break;
			case 1:
				$this->setTipe($value);
				break;
			case 2:
				$this->setDetailNo($value);
				break;
			case 3:
				$this->setRekeningCode($value);
				break;
			case 4:
				$this->setKomponenId($value);
				break;
			case 5:
				$this->setDetailName($value);
				break;
			case 6:
				$this->setVolume($value);
				break;
			case 7:
				$this->setKeteranganKoefisien($value);
				break;
			case 8:
				$this->setSubtitle($value);
				break;
			case 9:
				$this->setKomponenHarga($value);
				break;
			case 10:
				$this->setKomponenHargaAwal($value);
				break;
			case 11:
				$this->setKomponenName($value);
				break;
			case 12:
				$this->setSatuan($value);
				break;
			case 13:
				$this->setPajak($value);
				break;
			case 14:
				$this->setUnitId($value);
				break;
			case 15:
				$this->setFromSubKegiatan($value);
				break;
			case 16:
				$this->setSub($value);
				break;
			case 17:
				$this->setKodeSub($value);
				break;
			case 18:
				$this->setLastUpdateUser($value);
				break;
			case 19:
				$this->setLastUpdateTime($value);
				break;
			case 20:
				$this->setLastUpdateIp($value);
				break;
			case 21:
				$this->setTahap($value);
				break;
			case 22:
				$this->setTahapEdit($value);
				break;
			case 23:
				$this->setTahapNew($value);
				break;
			case 24:
				$this->setStatusLelang($value);
				break;
			case 25:
				$this->setNomorLelang($value);
				break;
			case 26:
				$this->setKoefisienSemula($value);
				break;
			case 27:
				$this->setVolumeSemula($value);
				break;
			case 28:
				$this->setHargaSemula($value);
				break;
			case 29:
				$this->setTotalSemula($value);
				break;
			case 30:
				$this->setLockSubtitle($value);
				break;
			case 31:
				$this->setStatusHapus($value);
				break;
			case 32:
				$this->setTahun($value);
				break;
			case 33:
				$this->setKodeLokasi($value);
				break;
			case 34:
				$this->setKecamatan($value);
				break;
			case 35:
				$this->setRekeningCodeAsli($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = Revisi1RincianDetailPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setKegiatanCode($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setTipe($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setDetailNo($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setRekeningCode($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setKomponenId($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setDetailName($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setVolume($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setKeteranganKoefisien($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setSubtitle($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setKomponenHarga($arr[$keys[9]]);
		if (array_key_exists($keys[10], $arr)) $this->setKomponenHargaAwal($arr[$keys[10]]);
		if (array_key_exists($keys[11], $arr)) $this->setKomponenName($arr[$keys[11]]);
		if (array_key_exists($keys[12], $arr)) $this->setSatuan($arr[$keys[12]]);
		if (array_key_exists($keys[13], $arr)) $this->setPajak($arr[$keys[13]]);
		if (array_key_exists($keys[14], $arr)) $this->setUnitId($arr[$keys[14]]);
		if (array_key_exists($keys[15], $arr)) $this->setFromSubKegiatan($arr[$keys[15]]);
		if (array_key_exists($keys[16], $arr)) $this->setSub($arr[$keys[16]]);
		if (array_key_exists($keys[17], $arr)) $this->setKodeSub($arr[$keys[17]]);
		if (array_key_exists($keys[18], $arr)) $this->setLastUpdateUser($arr[$keys[18]]);
		if (array_key_exists($keys[19], $arr)) $this->setLastUpdateTime($arr[$keys[19]]);
		if (array_key_exists($keys[20], $arr)) $this->setLastUpdateIp($arr[$keys[20]]);
		if (array_key_exists($keys[21], $arr)) $this->setTahap($arr[$keys[21]]);
		if (array_key_exists($keys[22], $arr)) $this->setTahapEdit($arr[$keys[22]]);
		if (array_key_exists($keys[23], $arr)) $this->setTahapNew($arr[$keys[23]]);
		if (array_key_exists($keys[24], $arr)) $this->setStatusLelang($arr[$keys[24]]);
		if (array_key_exists($keys[25], $arr)) $this->setNomorLelang($arr[$keys[25]]);
		if (array_key_exists($keys[26], $arr)) $this->setKoefisienSemula($arr[$keys[26]]);
		if (array_key_exists($keys[27], $arr)) $this->setVolumeSemula($arr[$keys[27]]);
		if (array_key_exists($keys[28], $arr)) $this->setHargaSemula($arr[$keys[28]]);
		if (array_key_exists($keys[29], $arr)) $this->setTotalSemula($arr[$keys[29]]);
		if (array_key_exists($keys[30], $arr)) $this->setLockSubtitle($arr[$keys[30]]);
		if (array_key_exists($keys[31], $arr)) $this->setStatusHapus($arr[$keys[31]]);
		if (array_key_exists($keys[32], $arr)) $this->setTahun($arr[$keys[32]]);
		if (array_key_exists($keys[33], $arr)) $this->setKodeLokasi($arr[$keys[33]]);
		if (array_key_exists($keys[34], $arr)) $this->setKecamatan($arr[$keys[34]]);
		if (array_key_exists($keys[35], $arr)) $this->setRekeningCodeAsli($arr[$keys[35]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(Revisi1RincianDetailPeer::DATABASE_NAME);

		if ($this->isColumnModified(Revisi1RincianDetailPeer::KEGIATAN_CODE)) $criteria->add(Revisi1RincianDetailPeer::KEGIATAN_CODE, $this->kegiatan_code);
		if ($this->isColumnModified(Revisi1RincianDetailPeer::TIPE)) $criteria->add(Revisi1RincianDetailPeer::TIPE, $this->tipe);
		if ($this->isColumnModified(Revisi1RincianDetailPeer::DETAIL_NO)) $criteria->add(Revisi1RincianDetailPeer::DETAIL_NO, $this->detail_no);
		if ($this->isColumnModified(Revisi1RincianDetailPeer::REKENING_CODE)) $criteria->add(Revisi1RincianDetailPeer::REKENING_CODE, $this->rekening_code);
		if ($this->isColumnModified(Revisi1RincianDetailPeer::KOMPONEN_ID)) $criteria->add(Revisi1RincianDetailPeer::KOMPONEN_ID, $this->komponen_id);
		if ($this->isColumnModified(Revisi1RincianDetailPeer::DETAIL_NAME)) $criteria->add(Revisi1RincianDetailPeer::DETAIL_NAME, $this->detail_name);
		if ($this->isColumnModified(Revisi1RincianDetailPeer::VOLUME)) $criteria->add(Revisi1RincianDetailPeer::VOLUME, $this->volume);
		if ($this->isColumnModified(Revisi1RincianDetailPeer::KETERANGAN_KOEFISIEN)) $criteria->add(Revisi1RincianDetailPeer::KETERANGAN_KOEFISIEN, $this->keterangan_koefisien);
		if ($this->isColumnModified(Revisi1RincianDetailPeer::SUBTITLE)) $criteria->add(Revisi1RincianDetailPeer::SUBTITLE, $this->subtitle);
		if ($this->isColumnModified(Revisi1RincianDetailPeer::KOMPONEN_HARGA)) $criteria->add(Revisi1RincianDetailPeer::KOMPONEN_HARGA, $this->komponen_harga);
		if ($this->isColumnModified(Revisi1RincianDetailPeer::KOMPONEN_HARGA_AWAL)) $criteria->add(Revisi1RincianDetailPeer::KOMPONEN_HARGA_AWAL, $this->komponen_harga_awal);
		if ($this->isColumnModified(Revisi1RincianDetailPeer::KOMPONEN_NAME)) $criteria->add(Revisi1RincianDetailPeer::KOMPONEN_NAME, $this->komponen_name);
		if ($this->isColumnModified(Revisi1RincianDetailPeer::SATUAN)) $criteria->add(Revisi1RincianDetailPeer::SATUAN, $this->satuan);
		if ($this->isColumnModified(Revisi1RincianDetailPeer::PAJAK)) $criteria->add(Revisi1RincianDetailPeer::PAJAK, $this->pajak);
		if ($this->isColumnModified(Revisi1RincianDetailPeer::UNIT_ID)) $criteria->add(Revisi1RincianDetailPeer::UNIT_ID, $this->unit_id);
		if ($this->isColumnModified(Revisi1RincianDetailPeer::FROM_SUB_KEGIATAN)) $criteria->add(Revisi1RincianDetailPeer::FROM_SUB_KEGIATAN, $this->from_sub_kegiatan);
		if ($this->isColumnModified(Revisi1RincianDetailPeer::SUB)) $criteria->add(Revisi1RincianDetailPeer::SUB, $this->sub);
		if ($this->isColumnModified(Revisi1RincianDetailPeer::KODE_SUB)) $criteria->add(Revisi1RincianDetailPeer::KODE_SUB, $this->kode_sub);
		if ($this->isColumnModified(Revisi1RincianDetailPeer::LAST_UPDATE_USER)) $criteria->add(Revisi1RincianDetailPeer::LAST_UPDATE_USER, $this->last_update_user);
		if ($this->isColumnModified(Revisi1RincianDetailPeer::LAST_UPDATE_TIME)) $criteria->add(Revisi1RincianDetailPeer::LAST_UPDATE_TIME, $this->last_update_time);
		if ($this->isColumnModified(Revisi1RincianDetailPeer::LAST_UPDATE_IP)) $criteria->add(Revisi1RincianDetailPeer::LAST_UPDATE_IP, $this->last_update_ip);
		if ($this->isColumnModified(Revisi1RincianDetailPeer::TAHAP)) $criteria->add(Revisi1RincianDetailPeer::TAHAP, $this->tahap);
		if ($this->isColumnModified(Revisi1RincianDetailPeer::TAHAP_EDIT)) $criteria->add(Revisi1RincianDetailPeer::TAHAP_EDIT, $this->tahap_edit);
		if ($this->isColumnModified(Revisi1RincianDetailPeer::TAHAP_NEW)) $criteria->add(Revisi1RincianDetailPeer::TAHAP_NEW, $this->tahap_new);
		if ($this->isColumnModified(Revisi1RincianDetailPeer::STATUS_LELANG)) $criteria->add(Revisi1RincianDetailPeer::STATUS_LELANG, $this->status_lelang);
		if ($this->isColumnModified(Revisi1RincianDetailPeer::NOMOR_LELANG)) $criteria->add(Revisi1RincianDetailPeer::NOMOR_LELANG, $this->nomor_lelang);
		if ($this->isColumnModified(Revisi1RincianDetailPeer::KOEFISIEN_SEMULA)) $criteria->add(Revisi1RincianDetailPeer::KOEFISIEN_SEMULA, $this->koefisien_semula);
		if ($this->isColumnModified(Revisi1RincianDetailPeer::VOLUME_SEMULA)) $criteria->add(Revisi1RincianDetailPeer::VOLUME_SEMULA, $this->volume_semula);
		if ($this->isColumnModified(Revisi1RincianDetailPeer::HARGA_SEMULA)) $criteria->add(Revisi1RincianDetailPeer::HARGA_SEMULA, $this->harga_semula);
		if ($this->isColumnModified(Revisi1RincianDetailPeer::TOTAL_SEMULA)) $criteria->add(Revisi1RincianDetailPeer::TOTAL_SEMULA, $this->total_semula);
		if ($this->isColumnModified(Revisi1RincianDetailPeer::LOCK_SUBTITLE)) $criteria->add(Revisi1RincianDetailPeer::LOCK_SUBTITLE, $this->lock_subtitle);
		if ($this->isColumnModified(Revisi1RincianDetailPeer::STATUS_HAPUS)) $criteria->add(Revisi1RincianDetailPeer::STATUS_HAPUS, $this->status_hapus);
		if ($this->isColumnModified(Revisi1RincianDetailPeer::TAHUN)) $criteria->add(Revisi1RincianDetailPeer::TAHUN, $this->tahun);
		if ($this->isColumnModified(Revisi1RincianDetailPeer::KODE_LOKASI)) $criteria->add(Revisi1RincianDetailPeer::KODE_LOKASI, $this->kode_lokasi);
		if ($this->isColumnModified(Revisi1RincianDetailPeer::KECAMATAN)) $criteria->add(Revisi1RincianDetailPeer::KECAMATAN, $this->kecamatan);
		if ($this->isColumnModified(Revisi1RincianDetailPeer::REKENING_CODE_ASLI)) $criteria->add(Revisi1RincianDetailPeer::REKENING_CODE_ASLI, $this->rekening_code_asli);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(Revisi1RincianDetailPeer::DATABASE_NAME);

		$criteria->add(Revisi1RincianDetailPeer::KEGIATAN_CODE, $this->kegiatan_code);
		$criteria->add(Revisi1RincianDetailPeer::TIPE, $this->tipe);
		$criteria->add(Revisi1RincianDetailPeer::DETAIL_NO, $this->detail_no);
		$criteria->add(Revisi1RincianDetailPeer::UNIT_ID, $this->unit_id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		$pks = array();

		$pks[0] = $this->getKegiatanCode();

		$pks[1] = $this->getTipe();

		$pks[2] = $this->getDetailNo();

		$pks[3] = $this->getUnitId();

		return $pks;
	}

	
	public function setPrimaryKey($keys)
	{

		$this->setKegiatanCode($keys[0]);

		$this->setTipe($keys[1]);

		$this->setDetailNo($keys[2]);

		$this->setUnitId($keys[3]);

	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setRekeningCode($this->rekening_code);

		$copyObj->setKomponenId($this->komponen_id);

		$copyObj->setDetailName($this->detail_name);

		$copyObj->setVolume($this->volume);

		$copyObj->setKeteranganKoefisien($this->keterangan_koefisien);

		$copyObj->setSubtitle($this->subtitle);

		$copyObj->setKomponenHarga($this->komponen_harga);

		$copyObj->setKomponenHargaAwal($this->komponen_harga_awal);

		$copyObj->setKomponenName($this->komponen_name);

		$copyObj->setSatuan($this->satuan);

		$copyObj->setPajak($this->pajak);

		$copyObj->setFromSubKegiatan($this->from_sub_kegiatan);

		$copyObj->setSub($this->sub);

		$copyObj->setKodeSub($this->kode_sub);

		$copyObj->setLastUpdateUser($this->last_update_user);

		$copyObj->setLastUpdateTime($this->last_update_time);

		$copyObj->setLastUpdateIp($this->last_update_ip);

		$copyObj->setTahap($this->tahap);

		$copyObj->setTahapEdit($this->tahap_edit);

		$copyObj->setTahapNew($this->tahap_new);

		$copyObj->setStatusLelang($this->status_lelang);

		$copyObj->setNomorLelang($this->nomor_lelang);

		$copyObj->setKoefisienSemula($this->koefisien_semula);

		$copyObj->setVolumeSemula($this->volume_semula);

		$copyObj->setHargaSemula($this->harga_semula);

		$copyObj->setTotalSemula($this->total_semula);

		$copyObj->setLockSubtitle($this->lock_subtitle);

		$copyObj->setStatusHapus($this->status_hapus);

		$copyObj->setTahun($this->tahun);

		$copyObj->setKodeLokasi($this->kode_lokasi);

		$copyObj->setKecamatan($this->kecamatan);

		$copyObj->setRekeningCodeAsli($this->rekening_code_asli);


		$copyObj->setNew(true);

		$copyObj->setKegiatanCode(NULL); 
		$copyObj->setTipe(NULL); 
		$copyObj->setDetailNo(NULL); 
		$copyObj->setUnitId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new Revisi1RincianDetailPeer();
		}
		return self::$peer;
	}

} 