<?php


abstract class BaseIndikatorTujuanPdPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.indikator_tujuan_pd';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.IndikatorTujuanPd';

	
	const NUM_COLUMNS = 5;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const ID = 'ebudget.indikator_tujuan_pd.ID';

	
	const UNIT_ID = 'ebudget.indikator_tujuan_pd.UNIT_ID';

	
	const KODE_KEGIATAN = 'ebudget.indikator_tujuan_pd.KODE_KEGIATAN';

	
	const INDIKATOR_TUJUAN = 'ebudget.indikator_tujuan_pd.INDIKATOR_TUJUAN';

	
	const NILAI = 'ebudget.indikator_tujuan_pd.NILAI';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('Id', 'UnitId', 'KodeKegiatan', 'IndikatorTujuan', 'Nilai', ),
		BasePeer::TYPE_COLNAME => array (IndikatorTujuanPdPeer::ID, IndikatorTujuanPdPeer::UNIT_ID, IndikatorTujuanPdPeer::KODE_KEGIATAN, IndikatorTujuanPdPeer::INDIKATOR_TUJUAN, IndikatorTujuanPdPeer::NILAI, ),
		BasePeer::TYPE_FIELDNAME => array ('id', 'unit_id', 'kode_kegiatan', 'indikator_tujuan', 'nilai', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('Id' => 0, 'UnitId' => 1, 'KodeKegiatan' => 2, 'IndikatorTujuan' => 3, 'Nilai' => 4, ),
		BasePeer::TYPE_COLNAME => array (IndikatorTujuanPdPeer::ID => 0, IndikatorTujuanPdPeer::UNIT_ID => 1, IndikatorTujuanPdPeer::KODE_KEGIATAN => 2, IndikatorTujuanPdPeer::INDIKATOR_TUJUAN => 3, IndikatorTujuanPdPeer::NILAI => 4, ),
		BasePeer::TYPE_FIELDNAME => array ('id' => 0, 'unit_id' => 1, 'kode_kegiatan' => 2, 'indikator_tujuan' => 3, 'nilai' => 4, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/IndikatorTujuanPdMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.IndikatorTujuanPdMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = IndikatorTujuanPdPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(IndikatorTujuanPdPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(IndikatorTujuanPdPeer::ID);

		$criteria->addSelectColumn(IndikatorTujuanPdPeer::UNIT_ID);

		$criteria->addSelectColumn(IndikatorTujuanPdPeer::KODE_KEGIATAN);

		$criteria->addSelectColumn(IndikatorTujuanPdPeer::INDIKATOR_TUJUAN);

		$criteria->addSelectColumn(IndikatorTujuanPdPeer::NILAI);

	}

	const COUNT = 'COUNT(ebudget.indikator_tujuan_pd.ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.indikator_tujuan_pd.ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(IndikatorTujuanPdPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(IndikatorTujuanPdPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = IndikatorTujuanPdPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = IndikatorTujuanPdPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return IndikatorTujuanPdPeer::populateObjects(IndikatorTujuanPdPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			IndikatorTujuanPdPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = IndikatorTujuanPdPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return IndikatorTujuanPdPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(IndikatorTujuanPdPeer::ID);
			$selectCriteria->add(IndikatorTujuanPdPeer::ID, $criteria->remove(IndikatorTujuanPdPeer::ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(IndikatorTujuanPdPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(IndikatorTujuanPdPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof IndikatorTujuanPd) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(IndikatorTujuanPdPeer::ID, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(IndikatorTujuanPd $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(IndikatorTujuanPdPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(IndikatorTujuanPdPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(IndikatorTujuanPdPeer::DATABASE_NAME, IndikatorTujuanPdPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = IndikatorTujuanPdPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(IndikatorTujuanPdPeer::DATABASE_NAME);

		$criteria->add(IndikatorTujuanPdPeer::ID, $pk);


		$v = IndikatorTujuanPdPeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(IndikatorTujuanPdPeer::ID, $pks, Criteria::IN);
			$objs = IndikatorTujuanPdPeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BaseIndikatorTujuanPdPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/IndikatorTujuanPdMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.IndikatorTujuanPdMapBuilder');
}
