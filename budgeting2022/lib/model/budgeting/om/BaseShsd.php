<?php


abstract class BaseShsd extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $shsd_id;


	
	protected $satuan;


	
	protected $shsd_name;


	
	protected $shsd_harga;


	
	protected $shsd_harga_dasar;


	
	protected $shsd_show;


	
	protected $shsd_faktor_inflasi;


	
	protected $shsd_locked = false;


	
	protected $rekening_code;


	
	protected $shsd_merk;


	
	protected $non_pajak = false;


	
	protected $shsd_id_old;


	
	protected $nama_dasar;


	
	protected $spec;


	
	protected $hidden_spec;


	
	protected $usulan_skpd;


	
	protected $is_potong_bpjs = false;


	
	protected $is_iuran_bpjs = false;


	
	protected $tahap;


	
	protected $is_survey_bp = false;


	
	protected $is_inflasi = true;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getShsdId()
	{

		return $this->shsd_id;
	}

	
	public function getSatuan()
	{

		return $this->satuan;
	}

	
	public function getShsdName()
	{

		return $this->shsd_name;
	}

	
	public function getShsdHarga()
	{

		return $this->shsd_harga;
	}

	
	public function getShsdHargaDasar()
	{

		return $this->shsd_harga_dasar;
	}

	
	public function getShsdShow()
	{

		return $this->shsd_show;
	}

	
	public function getShsdFaktorInflasi()
	{

		return $this->shsd_faktor_inflasi;
	}

	
	public function getShsdLocked()
	{

		return $this->shsd_locked;
	}

	
	public function getRekeningCode()
	{

		return $this->rekening_code;
	}

	
	public function getShsdMerk()
	{

		return $this->shsd_merk;
	}

	
	public function getNonPajak()
	{

		return $this->non_pajak;
	}

	
	public function getShsdIdOld()
	{

		return $this->shsd_id_old;
	}

	
	public function getNamaDasar()
	{

		return $this->nama_dasar;
	}

	
	public function getSpec()
	{

		return $this->spec;
	}

	
	public function getHiddenSpec()
	{

		return $this->hidden_spec;
	}

	
	public function getUsulanSkpd()
	{

		return $this->usulan_skpd;
	}

	
	public function getIsPotongBpjs()
	{

		return $this->is_potong_bpjs;
	}

	
	public function getIsIuranBpjs()
	{

		return $this->is_iuran_bpjs;
	}

	
	public function getTahap()
	{

		return $this->tahap;
	}

	
	public function getIsSurveyBp()
	{

		return $this->is_survey_bp;
	}

	
	public function getIsInflasi()
	{

		return $this->is_inflasi;
	}

	
	public function setShsdId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->shsd_id !== $v) {
			$this->shsd_id = $v;
			$this->modifiedColumns[] = ShsdPeer::SHSD_ID;
		}

	} 
	
	public function setSatuan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->satuan !== $v) {
			$this->satuan = $v;
			$this->modifiedColumns[] = ShsdPeer::SATUAN;
		}

	} 
	
	public function setShsdName($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->shsd_name !== $v) {
			$this->shsd_name = $v;
			$this->modifiedColumns[] = ShsdPeer::SHSD_NAME;
		}

	} 
	
	public function setShsdHarga($v)
	{

		if ($this->shsd_harga !== $v) {
			$this->shsd_harga = $v;
			$this->modifiedColumns[] = ShsdPeer::SHSD_HARGA;
		}

	} 
	
	public function setShsdHargaDasar($v)
	{

		if ($this->shsd_harga_dasar !== $v) {
			$this->shsd_harga_dasar = $v;
			$this->modifiedColumns[] = ShsdPeer::SHSD_HARGA_DASAR;
		}

	} 
	
	public function setShsdShow($v)
	{

		if ($this->shsd_show !== $v) {
			$this->shsd_show = $v;
			$this->modifiedColumns[] = ShsdPeer::SHSD_SHOW;
		}

	} 
	
	public function setShsdFaktorInflasi($v)
	{

		if ($this->shsd_faktor_inflasi !== $v) {
			$this->shsd_faktor_inflasi = $v;
			$this->modifiedColumns[] = ShsdPeer::SHSD_FAKTOR_INFLASI;
		}

	} 
	
	public function setShsdLocked($v)
	{

		if ($this->shsd_locked !== $v || $v === false) {
			$this->shsd_locked = $v;
			$this->modifiedColumns[] = ShsdPeer::SHSD_LOCKED;
		}

	} 
	
	public function setRekeningCode($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->rekening_code !== $v) {
			$this->rekening_code = $v;
			$this->modifiedColumns[] = ShsdPeer::REKENING_CODE;
		}

	} 
	
	public function setShsdMerk($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->shsd_merk !== $v) {
			$this->shsd_merk = $v;
			$this->modifiedColumns[] = ShsdPeer::SHSD_MERK;
		}

	} 
	
	public function setNonPajak($v)
	{

		if ($this->non_pajak !== $v || $v === false) {
			$this->non_pajak = $v;
			$this->modifiedColumns[] = ShsdPeer::NON_PAJAK;
		}

	} 
	
	public function setShsdIdOld($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->shsd_id_old !== $v) {
			$this->shsd_id_old = $v;
			$this->modifiedColumns[] = ShsdPeer::SHSD_ID_OLD;
		}

	} 
	
	public function setNamaDasar($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nama_dasar !== $v) {
			$this->nama_dasar = $v;
			$this->modifiedColumns[] = ShsdPeer::NAMA_DASAR;
		}

	} 
	
	public function setSpec($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->spec !== $v) {
			$this->spec = $v;
			$this->modifiedColumns[] = ShsdPeer::SPEC;
		}

	} 
	
	public function setHiddenSpec($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->hidden_spec !== $v) {
			$this->hidden_spec = $v;
			$this->modifiedColumns[] = ShsdPeer::HIDDEN_SPEC;
		}

	} 
	
	public function setUsulanSkpd($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->usulan_skpd !== $v) {
			$this->usulan_skpd = $v;
			$this->modifiedColumns[] = ShsdPeer::USULAN_SKPD;
		}

	} 
	
	public function setIsPotongBpjs($v)
	{

		if ($this->is_potong_bpjs !== $v || $v === false) {
			$this->is_potong_bpjs = $v;
			$this->modifiedColumns[] = ShsdPeer::IS_POTONG_BPJS;
		}

	} 
	
	public function setIsIuranBpjs($v)
	{

		if ($this->is_iuran_bpjs !== $v || $v === false) {
			$this->is_iuran_bpjs = $v;
			$this->modifiedColumns[] = ShsdPeer::IS_IURAN_BPJS;
		}

	} 
	
	public function setTahap($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->tahap !== $v) {
			$this->tahap = $v;
			$this->modifiedColumns[] = ShsdPeer::TAHAP;
		}

	} 
	
	public function setIsSurveyBp($v)
	{

		if ($this->is_survey_bp !== $v || $v === false) {
			$this->is_survey_bp = $v;
			$this->modifiedColumns[] = ShsdPeer::IS_SURVEY_BP;
		}

	} 
	
	public function setIsInflasi($v)
	{

		if ($this->is_inflasi !== $v || $v === true) {
			$this->is_inflasi = $v;
			$this->modifiedColumns[] = ShsdPeer::IS_INFLASI;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->shsd_id = $rs->getString($startcol + 0);

			$this->satuan = $rs->getString($startcol + 1);

			$this->shsd_name = $rs->getString($startcol + 2);

			$this->shsd_harga = $rs->getFloat($startcol + 3);

			$this->shsd_harga_dasar = $rs->getFloat($startcol + 4);

			$this->shsd_show = $rs->getBoolean($startcol + 5);

			$this->shsd_faktor_inflasi = $rs->getFloat($startcol + 6);

			$this->shsd_locked = $rs->getBoolean($startcol + 7);

			$this->rekening_code = $rs->getString($startcol + 8);

			$this->shsd_merk = $rs->getString($startcol + 9);

			$this->non_pajak = $rs->getBoolean($startcol + 10);

			$this->shsd_id_old = $rs->getString($startcol + 11);

			$this->nama_dasar = $rs->getString($startcol + 12);

			$this->spec = $rs->getString($startcol + 13);

			$this->hidden_spec = $rs->getString($startcol + 14);

			$this->usulan_skpd = $rs->getString($startcol + 15);

			$this->is_potong_bpjs = $rs->getBoolean($startcol + 16);

			$this->is_iuran_bpjs = $rs->getBoolean($startcol + 17);

			$this->tahap = $rs->getString($startcol + 18);

			$this->is_survey_bp = $rs->getBoolean($startcol + 19);

			$this->is_inflasi = $rs->getBoolean($startcol + 20);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 21; 
		} catch (Exception $e) {
			throw new PropelException("Error populating Shsd object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(ShsdPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			ShsdPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(ShsdPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = ShsdPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += ShsdPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = ShsdPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = ShsdPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getShsdId();
				break;
			case 1:
				return $this->getSatuan();
				break;
			case 2:
				return $this->getShsdName();
				break;
			case 3:
				return $this->getShsdHarga();
				break;
			case 4:
				return $this->getShsdHargaDasar();
				break;
			case 5:
				return $this->getShsdShow();
				break;
			case 6:
				return $this->getShsdFaktorInflasi();
				break;
			case 7:
				return $this->getShsdLocked();
				break;
			case 8:
				return $this->getRekeningCode();
				break;
			case 9:
				return $this->getShsdMerk();
				break;
			case 10:
				return $this->getNonPajak();
				break;
			case 11:
				return $this->getShsdIdOld();
				break;
			case 12:
				return $this->getNamaDasar();
				break;
			case 13:
				return $this->getSpec();
				break;
			case 14:
				return $this->getHiddenSpec();
				break;
			case 15:
				return $this->getUsulanSkpd();
				break;
			case 16:
				return $this->getIsPotongBpjs();
				break;
			case 17:
				return $this->getIsIuranBpjs();
				break;
			case 18:
				return $this->getTahap();
				break;
			case 19:
				return $this->getIsSurveyBp();
				break;
			case 20:
				return $this->getIsInflasi();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = ShsdPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getShsdId(),
			$keys[1] => $this->getSatuan(),
			$keys[2] => $this->getShsdName(),
			$keys[3] => $this->getShsdHarga(),
			$keys[4] => $this->getShsdHargaDasar(),
			$keys[5] => $this->getShsdShow(),
			$keys[6] => $this->getShsdFaktorInflasi(),
			$keys[7] => $this->getShsdLocked(),
			$keys[8] => $this->getRekeningCode(),
			$keys[9] => $this->getShsdMerk(),
			$keys[10] => $this->getNonPajak(),
			$keys[11] => $this->getShsdIdOld(),
			$keys[12] => $this->getNamaDasar(),
			$keys[13] => $this->getSpec(),
			$keys[14] => $this->getHiddenSpec(),
			$keys[15] => $this->getUsulanSkpd(),
			$keys[16] => $this->getIsPotongBpjs(),
			$keys[17] => $this->getIsIuranBpjs(),
			$keys[18] => $this->getTahap(),
			$keys[19] => $this->getIsSurveyBp(),
			$keys[20] => $this->getIsInflasi(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = ShsdPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setShsdId($value);
				break;
			case 1:
				$this->setSatuan($value);
				break;
			case 2:
				$this->setShsdName($value);
				break;
			case 3:
				$this->setShsdHarga($value);
				break;
			case 4:
				$this->setShsdHargaDasar($value);
				break;
			case 5:
				$this->setShsdShow($value);
				break;
			case 6:
				$this->setShsdFaktorInflasi($value);
				break;
			case 7:
				$this->setShsdLocked($value);
				break;
			case 8:
				$this->setRekeningCode($value);
				break;
			case 9:
				$this->setShsdMerk($value);
				break;
			case 10:
				$this->setNonPajak($value);
				break;
			case 11:
				$this->setShsdIdOld($value);
				break;
			case 12:
				$this->setNamaDasar($value);
				break;
			case 13:
				$this->setSpec($value);
				break;
			case 14:
				$this->setHiddenSpec($value);
				break;
			case 15:
				$this->setUsulanSkpd($value);
				break;
			case 16:
				$this->setIsPotongBpjs($value);
				break;
			case 17:
				$this->setIsIuranBpjs($value);
				break;
			case 18:
				$this->setTahap($value);
				break;
			case 19:
				$this->setIsSurveyBp($value);
				break;
			case 20:
				$this->setIsInflasi($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = ShsdPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setShsdId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setSatuan($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setShsdName($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setShsdHarga($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setShsdHargaDasar($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setShsdShow($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setShsdFaktorInflasi($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setShsdLocked($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setRekeningCode($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setShsdMerk($arr[$keys[9]]);
		if (array_key_exists($keys[10], $arr)) $this->setNonPajak($arr[$keys[10]]);
		if (array_key_exists($keys[11], $arr)) $this->setShsdIdOld($arr[$keys[11]]);
		if (array_key_exists($keys[12], $arr)) $this->setNamaDasar($arr[$keys[12]]);
		if (array_key_exists($keys[13], $arr)) $this->setSpec($arr[$keys[13]]);
		if (array_key_exists($keys[14], $arr)) $this->setHiddenSpec($arr[$keys[14]]);
		if (array_key_exists($keys[15], $arr)) $this->setUsulanSkpd($arr[$keys[15]]);
		if (array_key_exists($keys[16], $arr)) $this->setIsPotongBpjs($arr[$keys[16]]);
		if (array_key_exists($keys[17], $arr)) $this->setIsIuranBpjs($arr[$keys[17]]);
		if (array_key_exists($keys[18], $arr)) $this->setTahap($arr[$keys[18]]);
		if (array_key_exists($keys[19], $arr)) $this->setIsSurveyBp($arr[$keys[19]]);
		if (array_key_exists($keys[20], $arr)) $this->setIsInflasi($arr[$keys[20]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(ShsdPeer::DATABASE_NAME);

		if ($this->isColumnModified(ShsdPeer::SHSD_ID)) $criteria->add(ShsdPeer::SHSD_ID, $this->shsd_id);
		if ($this->isColumnModified(ShsdPeer::SATUAN)) $criteria->add(ShsdPeer::SATUAN, $this->satuan);
		if ($this->isColumnModified(ShsdPeer::SHSD_NAME)) $criteria->add(ShsdPeer::SHSD_NAME, $this->shsd_name);
		if ($this->isColumnModified(ShsdPeer::SHSD_HARGA)) $criteria->add(ShsdPeer::SHSD_HARGA, $this->shsd_harga);
		if ($this->isColumnModified(ShsdPeer::SHSD_HARGA_DASAR)) $criteria->add(ShsdPeer::SHSD_HARGA_DASAR, $this->shsd_harga_dasar);
		if ($this->isColumnModified(ShsdPeer::SHSD_SHOW)) $criteria->add(ShsdPeer::SHSD_SHOW, $this->shsd_show);
		if ($this->isColumnModified(ShsdPeer::SHSD_FAKTOR_INFLASI)) $criteria->add(ShsdPeer::SHSD_FAKTOR_INFLASI, $this->shsd_faktor_inflasi);
		if ($this->isColumnModified(ShsdPeer::SHSD_LOCKED)) $criteria->add(ShsdPeer::SHSD_LOCKED, $this->shsd_locked);
		if ($this->isColumnModified(ShsdPeer::REKENING_CODE)) $criteria->add(ShsdPeer::REKENING_CODE, $this->rekening_code);
		if ($this->isColumnModified(ShsdPeer::SHSD_MERK)) $criteria->add(ShsdPeer::SHSD_MERK, $this->shsd_merk);
		if ($this->isColumnModified(ShsdPeer::NON_PAJAK)) $criteria->add(ShsdPeer::NON_PAJAK, $this->non_pajak);
		if ($this->isColumnModified(ShsdPeer::SHSD_ID_OLD)) $criteria->add(ShsdPeer::SHSD_ID_OLD, $this->shsd_id_old);
		if ($this->isColumnModified(ShsdPeer::NAMA_DASAR)) $criteria->add(ShsdPeer::NAMA_DASAR, $this->nama_dasar);
		if ($this->isColumnModified(ShsdPeer::SPEC)) $criteria->add(ShsdPeer::SPEC, $this->spec);
		if ($this->isColumnModified(ShsdPeer::HIDDEN_SPEC)) $criteria->add(ShsdPeer::HIDDEN_SPEC, $this->hidden_spec);
		if ($this->isColumnModified(ShsdPeer::USULAN_SKPD)) $criteria->add(ShsdPeer::USULAN_SKPD, $this->usulan_skpd);
		if ($this->isColumnModified(ShsdPeer::IS_POTONG_BPJS)) $criteria->add(ShsdPeer::IS_POTONG_BPJS, $this->is_potong_bpjs);
		if ($this->isColumnModified(ShsdPeer::IS_IURAN_BPJS)) $criteria->add(ShsdPeer::IS_IURAN_BPJS, $this->is_iuran_bpjs);
		if ($this->isColumnModified(ShsdPeer::TAHAP)) $criteria->add(ShsdPeer::TAHAP, $this->tahap);
		if ($this->isColumnModified(ShsdPeer::IS_SURVEY_BP)) $criteria->add(ShsdPeer::IS_SURVEY_BP, $this->is_survey_bp);
		if ($this->isColumnModified(ShsdPeer::IS_INFLASI)) $criteria->add(ShsdPeer::IS_INFLASI, $this->is_inflasi);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(ShsdPeer::DATABASE_NAME);

		$criteria->add(ShsdPeer::SHSD_ID, $this->shsd_id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getShsdId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setShsdId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setSatuan($this->satuan);

		$copyObj->setShsdName($this->shsd_name);

		$copyObj->setShsdHarga($this->shsd_harga);

		$copyObj->setShsdHargaDasar($this->shsd_harga_dasar);

		$copyObj->setShsdShow($this->shsd_show);

		$copyObj->setShsdFaktorInflasi($this->shsd_faktor_inflasi);

		$copyObj->setShsdLocked($this->shsd_locked);

		$copyObj->setRekeningCode($this->rekening_code);

		$copyObj->setShsdMerk($this->shsd_merk);

		$copyObj->setNonPajak($this->non_pajak);

		$copyObj->setShsdIdOld($this->shsd_id_old);

		$copyObj->setNamaDasar($this->nama_dasar);

		$copyObj->setSpec($this->spec);

		$copyObj->setHiddenSpec($this->hidden_spec);

		$copyObj->setUsulanSkpd($this->usulan_skpd);

		$copyObj->setIsPotongBpjs($this->is_potong_bpjs);

		$copyObj->setIsIuranBpjs($this->is_iuran_bpjs);

		$copyObj->setTahap($this->tahap);

		$copyObj->setIsSurveyBp($this->is_survey_bp);

		$copyObj->setIsInflasi($this->is_inflasi);


		$copyObj->setNew(true);

		$copyObj->setShsdId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new ShsdPeer();
		}
		return self::$peer;
	}

} 