<?php


abstract class BasePakBukuBiruSubtitleIndikatorPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.pak_bukubiru_subtitle_indikator';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.PakBukuBiruSubtitleIndikator';

	
	const NUM_COLUMNS = 23;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const UNIT_ID = 'ebudget.pak_bukubiru_subtitle_indikator.UNIT_ID';

	
	const KEGIATAN_CODE = 'ebudget.pak_bukubiru_subtitle_indikator.KEGIATAN_CODE';

	
	const SUBTITLE = 'ebudget.pak_bukubiru_subtitle_indikator.SUBTITLE';

	
	const INDIKATOR = 'ebudget.pak_bukubiru_subtitle_indikator.INDIKATOR';

	
	const NILAI = 'ebudget.pak_bukubiru_subtitle_indikator.NILAI';

	
	const SATUAN = 'ebudget.pak_bukubiru_subtitle_indikator.SATUAN';

	
	const LAST_UPDATE_USER = 'ebudget.pak_bukubiru_subtitle_indikator.LAST_UPDATE_USER';

	
	const LAST_UPDATE_TIME = 'ebudget.pak_bukubiru_subtitle_indikator.LAST_UPDATE_TIME';

	
	const LAST_UPDATE_IP = 'ebudget.pak_bukubiru_subtitle_indikator.LAST_UPDATE_IP';

	
	const TAHAP = 'ebudget.pak_bukubiru_subtitle_indikator.TAHAP';

	
	const SUB_ID = 'ebudget.pak_bukubiru_subtitle_indikator.SUB_ID';

	
	const TAHUN = 'ebudget.pak_bukubiru_subtitle_indikator.TAHUN';

	
	const LOCK_SUBTITLE = 'ebudget.pak_bukubiru_subtitle_indikator.LOCK_SUBTITLE';

	
	const PRIORITAS = 'ebudget.pak_bukubiru_subtitle_indikator.PRIORITAS';

	
	const CATATAN = 'ebudget.pak_bukubiru_subtitle_indikator.CATATAN';

	
	const LAKILAKI = 'ebudget.pak_bukubiru_subtitle_indikator.LAKILAKI';

	
	const PEREMPUAN = 'ebudget.pak_bukubiru_subtitle_indikator.PEREMPUAN';

	
	const DEWASA = 'ebudget.pak_bukubiru_subtitle_indikator.DEWASA';

	
	const ANAK = 'ebudget.pak_bukubiru_subtitle_indikator.ANAK';

	
	const LANSIA = 'ebudget.pak_bukubiru_subtitle_indikator.LANSIA';

	
	const INKLUSI = 'ebudget.pak_bukubiru_subtitle_indikator.INKLUSI';

	
	const GENDER = 'ebudget.pak_bukubiru_subtitle_indikator.GENDER';

	
	const IS_NOL_ANGGARAN = 'ebudget.pak_bukubiru_subtitle_indikator.IS_NOL_ANGGARAN';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('UnitId', 'KegiatanCode', 'Subtitle', 'Indikator', 'Nilai', 'Satuan', 'LastUpdateUser', 'LastUpdateTime', 'LastUpdateIp', 'Tahap', 'SubId', 'Tahun', 'LockSubtitle', 'Prioritas', 'Catatan', 'Lakilaki', 'Perempuan', 'Dewasa', 'Anak', 'Lansia', 'Inklusi', 'Gender', 'IsNolAnggaran', ),
		BasePeer::TYPE_COLNAME => array (PakBukuBiruSubtitleIndikatorPeer::UNIT_ID, PakBukuBiruSubtitleIndikatorPeer::KEGIATAN_CODE, PakBukuBiruSubtitleIndikatorPeer::SUBTITLE, PakBukuBiruSubtitleIndikatorPeer::INDIKATOR, PakBukuBiruSubtitleIndikatorPeer::NILAI, PakBukuBiruSubtitleIndikatorPeer::SATUAN, PakBukuBiruSubtitleIndikatorPeer::LAST_UPDATE_USER, PakBukuBiruSubtitleIndikatorPeer::LAST_UPDATE_TIME, PakBukuBiruSubtitleIndikatorPeer::LAST_UPDATE_IP, PakBukuBiruSubtitleIndikatorPeer::TAHAP, PakBukuBiruSubtitleIndikatorPeer::SUB_ID, PakBukuBiruSubtitleIndikatorPeer::TAHUN, PakBukuBiruSubtitleIndikatorPeer::LOCK_SUBTITLE, PakBukuBiruSubtitleIndikatorPeer::PRIORITAS, PakBukuBiruSubtitleIndikatorPeer::CATATAN, PakBukuBiruSubtitleIndikatorPeer::LAKILAKI, PakBukuBiruSubtitleIndikatorPeer::PEREMPUAN, PakBukuBiruSubtitleIndikatorPeer::DEWASA, PakBukuBiruSubtitleIndikatorPeer::ANAK, PakBukuBiruSubtitleIndikatorPeer::LANSIA, PakBukuBiruSubtitleIndikatorPeer::INKLUSI, PakBukuBiruSubtitleIndikatorPeer::GENDER, PakBukuBiruSubtitleIndikatorPeer::IS_NOL_ANGGARAN, ),
		BasePeer::TYPE_FIELDNAME => array ('unit_id', 'kegiatan_code', 'subtitle', 'indikator', 'nilai', 'satuan', 'last_update_user', 'last_update_time', 'last_update_ip', 'tahap', 'sub_id', 'tahun', 'lock_subtitle', 'prioritas', 'catatan', 'lakilaki', 'perempuan', 'dewasa', 'anak', 'lansia', 'inklusi', 'gender', 'is_nol_anggaran', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('UnitId' => 0, 'KegiatanCode' => 1, 'Subtitle' => 2, 'Indikator' => 3, 'Nilai' => 4, 'Satuan' => 5, 'LastUpdateUser' => 6, 'LastUpdateTime' => 7, 'LastUpdateIp' => 8, 'Tahap' => 9, 'SubId' => 10, 'Tahun' => 11, 'LockSubtitle' => 12, 'Prioritas' => 13, 'Catatan' => 14, 'Lakilaki' => 15, 'Perempuan' => 16, 'Dewasa' => 17, 'Anak' => 18, 'Lansia' => 19, 'Inklusi' => 20, 'Gender' => 21, 'IsNolAnggaran' => 22, ),
		BasePeer::TYPE_COLNAME => array (PakBukuBiruSubtitleIndikatorPeer::UNIT_ID => 0, PakBukuBiruSubtitleIndikatorPeer::KEGIATAN_CODE => 1, PakBukuBiruSubtitleIndikatorPeer::SUBTITLE => 2, PakBukuBiruSubtitleIndikatorPeer::INDIKATOR => 3, PakBukuBiruSubtitleIndikatorPeer::NILAI => 4, PakBukuBiruSubtitleIndikatorPeer::SATUAN => 5, PakBukuBiruSubtitleIndikatorPeer::LAST_UPDATE_USER => 6, PakBukuBiruSubtitleIndikatorPeer::LAST_UPDATE_TIME => 7, PakBukuBiruSubtitleIndikatorPeer::LAST_UPDATE_IP => 8, PakBukuBiruSubtitleIndikatorPeer::TAHAP => 9, PakBukuBiruSubtitleIndikatorPeer::SUB_ID => 10, PakBukuBiruSubtitleIndikatorPeer::TAHUN => 11, PakBukuBiruSubtitleIndikatorPeer::LOCK_SUBTITLE => 12, PakBukuBiruSubtitleIndikatorPeer::PRIORITAS => 13, PakBukuBiruSubtitleIndikatorPeer::CATATAN => 14, PakBukuBiruSubtitleIndikatorPeer::LAKILAKI => 15, PakBukuBiruSubtitleIndikatorPeer::PEREMPUAN => 16, PakBukuBiruSubtitleIndikatorPeer::DEWASA => 17, PakBukuBiruSubtitleIndikatorPeer::ANAK => 18, PakBukuBiruSubtitleIndikatorPeer::LANSIA => 19, PakBukuBiruSubtitleIndikatorPeer::INKLUSI => 20, PakBukuBiruSubtitleIndikatorPeer::GENDER => 21, PakBukuBiruSubtitleIndikatorPeer::IS_NOL_ANGGARAN => 22, ),
		BasePeer::TYPE_FIELDNAME => array ('unit_id' => 0, 'kegiatan_code' => 1, 'subtitle' => 2, 'indikator' => 3, 'nilai' => 4, 'satuan' => 5, 'last_update_user' => 6, 'last_update_time' => 7, 'last_update_ip' => 8, 'tahap' => 9, 'sub_id' => 10, 'tahun' => 11, 'lock_subtitle' => 12, 'prioritas' => 13, 'catatan' => 14, 'lakilaki' => 15, 'perempuan' => 16, 'dewasa' => 17, 'anak' => 18, 'lansia' => 19, 'inklusi' => 20, 'gender' => 21, 'is_nol_anggaran' => 22, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/PakBukuBiruSubtitleIndikatorMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.PakBukuBiruSubtitleIndikatorMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = PakBukuBiruSubtitleIndikatorPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(PakBukuBiruSubtitleIndikatorPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(PakBukuBiruSubtitleIndikatorPeer::UNIT_ID);

		$criteria->addSelectColumn(PakBukuBiruSubtitleIndikatorPeer::KEGIATAN_CODE);

		$criteria->addSelectColumn(PakBukuBiruSubtitleIndikatorPeer::SUBTITLE);

		$criteria->addSelectColumn(PakBukuBiruSubtitleIndikatorPeer::INDIKATOR);

		$criteria->addSelectColumn(PakBukuBiruSubtitleIndikatorPeer::NILAI);

		$criteria->addSelectColumn(PakBukuBiruSubtitleIndikatorPeer::SATUAN);

		$criteria->addSelectColumn(PakBukuBiruSubtitleIndikatorPeer::LAST_UPDATE_USER);

		$criteria->addSelectColumn(PakBukuBiruSubtitleIndikatorPeer::LAST_UPDATE_TIME);

		$criteria->addSelectColumn(PakBukuBiruSubtitleIndikatorPeer::LAST_UPDATE_IP);

		$criteria->addSelectColumn(PakBukuBiruSubtitleIndikatorPeer::TAHAP);

		$criteria->addSelectColumn(PakBukuBiruSubtitleIndikatorPeer::SUB_ID);

		$criteria->addSelectColumn(PakBukuBiruSubtitleIndikatorPeer::TAHUN);

		$criteria->addSelectColumn(PakBukuBiruSubtitleIndikatorPeer::LOCK_SUBTITLE);

		$criteria->addSelectColumn(PakBukuBiruSubtitleIndikatorPeer::PRIORITAS);

		$criteria->addSelectColumn(PakBukuBiruSubtitleIndikatorPeer::CATATAN);

		$criteria->addSelectColumn(PakBukuBiruSubtitleIndikatorPeer::LAKILAKI);

		$criteria->addSelectColumn(PakBukuBiruSubtitleIndikatorPeer::PEREMPUAN);

		$criteria->addSelectColumn(PakBukuBiruSubtitleIndikatorPeer::DEWASA);

		$criteria->addSelectColumn(PakBukuBiruSubtitleIndikatorPeer::ANAK);

		$criteria->addSelectColumn(PakBukuBiruSubtitleIndikatorPeer::LANSIA);

		$criteria->addSelectColumn(PakBukuBiruSubtitleIndikatorPeer::INKLUSI);

		$criteria->addSelectColumn(PakBukuBiruSubtitleIndikatorPeer::GENDER);

		$criteria->addSelectColumn(PakBukuBiruSubtitleIndikatorPeer::IS_NOL_ANGGARAN);

	}

	const COUNT = 'COUNT(ebudget.pak_bukubiru_subtitle_indikator.SUB_ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.pak_bukubiru_subtitle_indikator.SUB_ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(PakBukuBiruSubtitleIndikatorPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(PakBukuBiruSubtitleIndikatorPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = PakBukuBiruSubtitleIndikatorPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = PakBukuBiruSubtitleIndikatorPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return PakBukuBiruSubtitleIndikatorPeer::populateObjects(PakBukuBiruSubtitleIndikatorPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			PakBukuBiruSubtitleIndikatorPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = PakBukuBiruSubtitleIndikatorPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return PakBukuBiruSubtitleIndikatorPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}

		$criteria->remove(PakBukuBiruSubtitleIndikatorPeer::SUB_ID); 

				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(PakBukuBiruSubtitleIndikatorPeer::SUB_ID);
			$selectCriteria->add(PakBukuBiruSubtitleIndikatorPeer::SUB_ID, $criteria->remove(PakBukuBiruSubtitleIndikatorPeer::SUB_ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(PakBukuBiruSubtitleIndikatorPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(PakBukuBiruSubtitleIndikatorPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof PakBukuBiruSubtitleIndikator) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(PakBukuBiruSubtitleIndikatorPeer::SUB_ID, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(PakBukuBiruSubtitleIndikator $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(PakBukuBiruSubtitleIndikatorPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(PakBukuBiruSubtitleIndikatorPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(PakBukuBiruSubtitleIndikatorPeer::DATABASE_NAME, PakBukuBiruSubtitleIndikatorPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = PakBukuBiruSubtitleIndikatorPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(PakBukuBiruSubtitleIndikatorPeer::DATABASE_NAME);

		$criteria->add(PakBukuBiruSubtitleIndikatorPeer::SUB_ID, $pk);


		$v = PakBukuBiruSubtitleIndikatorPeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(PakBukuBiruSubtitleIndikatorPeer::SUB_ID, $pks, Criteria::IN);
			$objs = PakBukuBiruSubtitleIndikatorPeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BasePakBukuBiruSubtitleIndikatorPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/PakBukuBiruSubtitleIndikatorMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.PakBukuBiruSubtitleIndikatorMapBuilder');
}
