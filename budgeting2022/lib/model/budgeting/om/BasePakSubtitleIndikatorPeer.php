<?php


abstract class BasePakSubtitleIndikatorPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.pak_subtitle_indikator';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.PakSubtitleIndikator';

	
	const NUM_COLUMNS = 22;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const UNIT_ID = 'ebudget.pak_subtitle_indikator.UNIT_ID';

	
	const KEGIATAN_CODE = 'ebudget.pak_subtitle_indikator.KEGIATAN_CODE';

	
	const SUBTITLE = 'ebudget.pak_subtitle_indikator.SUBTITLE';

	
	const INDIKATOR = 'ebudget.pak_subtitle_indikator.INDIKATOR';

	
	const NILAI = 'ebudget.pak_subtitle_indikator.NILAI';

	
	const SATUAN = 'ebudget.pak_subtitle_indikator.SATUAN';

	
	const LAST_UPDATE_USER = 'ebudget.pak_subtitle_indikator.LAST_UPDATE_USER';

	
	const LAST_UPDATE_TIME = 'ebudget.pak_subtitle_indikator.LAST_UPDATE_TIME';

	
	const LAST_UPDATE_IP = 'ebudget.pak_subtitle_indikator.LAST_UPDATE_IP';

	
	const TAHAP = 'ebudget.pak_subtitle_indikator.TAHAP';

	
	const SUB_ID = 'ebudget.pak_subtitle_indikator.SUB_ID';

	
	const TAHUN = 'ebudget.pak_subtitle_indikator.TAHUN';

	
	const LOCK_SUBTITLE = 'ebudget.pak_subtitle_indikator.LOCK_SUBTITLE';

	
	const PRIORITAS = 'ebudget.pak_subtitle_indikator.PRIORITAS';

	
	const CATATAN = 'ebudget.pak_subtitle_indikator.CATATAN';

	
	const LAKILAKI = 'ebudget.pak_subtitle_indikator.LAKILAKI';

	
	const PEREMPUAN = 'ebudget.pak_subtitle_indikator.PEREMPUAN';

	
	const DEWASA = 'ebudget.pak_subtitle_indikator.DEWASA';

	
	const ANAK = 'ebudget.pak_subtitle_indikator.ANAK';

	
	const LANSIA = 'ebudget.pak_subtitle_indikator.LANSIA';

	
	const INKLUSI = 'ebudget.pak_subtitle_indikator.INKLUSI';

	
	const GENDER = 'ebudget.pak_subtitle_indikator.GENDER';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('UnitId', 'KegiatanCode', 'Subtitle', 'Indikator', 'Nilai', 'Satuan', 'LastUpdateUser', 'LastUpdateTime', 'LastUpdateIp', 'Tahap', 'SubId', 'Tahun', 'LockSubtitle', 'Prioritas', 'Catatan', 'Lakilaki', 'Perempuan', 'Dewasa', 'Anak', 'Lansia', 'Inklusi', 'Gender', ),
		BasePeer::TYPE_COLNAME => array (PakSubtitleIndikatorPeer::UNIT_ID, PakSubtitleIndikatorPeer::KEGIATAN_CODE, PakSubtitleIndikatorPeer::SUBTITLE, PakSubtitleIndikatorPeer::INDIKATOR, PakSubtitleIndikatorPeer::NILAI, PakSubtitleIndikatorPeer::SATUAN, PakSubtitleIndikatorPeer::LAST_UPDATE_USER, PakSubtitleIndikatorPeer::LAST_UPDATE_TIME, PakSubtitleIndikatorPeer::LAST_UPDATE_IP, PakSubtitleIndikatorPeer::TAHAP, PakSubtitleIndikatorPeer::SUB_ID, PakSubtitleIndikatorPeer::TAHUN, PakSubtitleIndikatorPeer::LOCK_SUBTITLE, PakSubtitleIndikatorPeer::PRIORITAS, PakSubtitleIndikatorPeer::CATATAN, PakSubtitleIndikatorPeer::LAKILAKI, PakSubtitleIndikatorPeer::PEREMPUAN, PakSubtitleIndikatorPeer::DEWASA, PakSubtitleIndikatorPeer::ANAK, PakSubtitleIndikatorPeer::LANSIA, PakSubtitleIndikatorPeer::INKLUSI, PakSubtitleIndikatorPeer::GENDER, ),
		BasePeer::TYPE_FIELDNAME => array ('unit_id', 'kegiatan_code', 'subtitle', 'indikator', 'nilai', 'satuan', 'last_update_user', 'last_update_time', 'last_update_ip', 'tahap', 'sub_id', 'tahun', 'lock_subtitle', 'prioritas', 'catatan', 'lakilaki', 'perempuan', 'dewasa', 'anak', 'lansia', 'inklusi', 'gender', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('UnitId' => 0, 'KegiatanCode' => 1, 'Subtitle' => 2, 'Indikator' => 3, 'Nilai' => 4, 'Satuan' => 5, 'LastUpdateUser' => 6, 'LastUpdateTime' => 7, 'LastUpdateIp' => 8, 'Tahap' => 9, 'SubId' => 10, 'Tahun' => 11, 'LockSubtitle' => 12, 'Prioritas' => 13, 'Catatan' => 14, 'Lakilaki' => 15, 'Perempuan' => 16, 'Dewasa' => 17, 'Anak' => 18, 'Lansia' => 19, 'Inklusi' => 20, 'Gender' => 21, ),
		BasePeer::TYPE_COLNAME => array (PakSubtitleIndikatorPeer::UNIT_ID => 0, PakSubtitleIndikatorPeer::KEGIATAN_CODE => 1, PakSubtitleIndikatorPeer::SUBTITLE => 2, PakSubtitleIndikatorPeer::INDIKATOR => 3, PakSubtitleIndikatorPeer::NILAI => 4, PakSubtitleIndikatorPeer::SATUAN => 5, PakSubtitleIndikatorPeer::LAST_UPDATE_USER => 6, PakSubtitleIndikatorPeer::LAST_UPDATE_TIME => 7, PakSubtitleIndikatorPeer::LAST_UPDATE_IP => 8, PakSubtitleIndikatorPeer::TAHAP => 9, PakSubtitleIndikatorPeer::SUB_ID => 10, PakSubtitleIndikatorPeer::TAHUN => 11, PakSubtitleIndikatorPeer::LOCK_SUBTITLE => 12, PakSubtitleIndikatorPeer::PRIORITAS => 13, PakSubtitleIndikatorPeer::CATATAN => 14, PakSubtitleIndikatorPeer::LAKILAKI => 15, PakSubtitleIndikatorPeer::PEREMPUAN => 16, PakSubtitleIndikatorPeer::DEWASA => 17, PakSubtitleIndikatorPeer::ANAK => 18, PakSubtitleIndikatorPeer::LANSIA => 19, PakSubtitleIndikatorPeer::INKLUSI => 20, PakSubtitleIndikatorPeer::GENDER => 21, ),
		BasePeer::TYPE_FIELDNAME => array ('unit_id' => 0, 'kegiatan_code' => 1, 'subtitle' => 2, 'indikator' => 3, 'nilai' => 4, 'satuan' => 5, 'last_update_user' => 6, 'last_update_time' => 7, 'last_update_ip' => 8, 'tahap' => 9, 'sub_id' => 10, 'tahun' => 11, 'lock_subtitle' => 12, 'prioritas' => 13, 'catatan' => 14, 'lakilaki' => 15, 'perempuan' => 16, 'dewasa' => 17, 'anak' => 18, 'lansia' => 19, 'inklusi' => 20, 'gender' => 21, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/PakSubtitleIndikatorMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.PakSubtitleIndikatorMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = PakSubtitleIndikatorPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(PakSubtitleIndikatorPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(PakSubtitleIndikatorPeer::UNIT_ID);

		$criteria->addSelectColumn(PakSubtitleIndikatorPeer::KEGIATAN_CODE);

		$criteria->addSelectColumn(PakSubtitleIndikatorPeer::SUBTITLE);

		$criteria->addSelectColumn(PakSubtitleIndikatorPeer::INDIKATOR);

		$criteria->addSelectColumn(PakSubtitleIndikatorPeer::NILAI);

		$criteria->addSelectColumn(PakSubtitleIndikatorPeer::SATUAN);

		$criteria->addSelectColumn(PakSubtitleIndikatorPeer::LAST_UPDATE_USER);

		$criteria->addSelectColumn(PakSubtitleIndikatorPeer::LAST_UPDATE_TIME);

		$criteria->addSelectColumn(PakSubtitleIndikatorPeer::LAST_UPDATE_IP);

		$criteria->addSelectColumn(PakSubtitleIndikatorPeer::TAHAP);

		$criteria->addSelectColumn(PakSubtitleIndikatorPeer::SUB_ID);

		$criteria->addSelectColumn(PakSubtitleIndikatorPeer::TAHUN);

		$criteria->addSelectColumn(PakSubtitleIndikatorPeer::LOCK_SUBTITLE);

		$criteria->addSelectColumn(PakSubtitleIndikatorPeer::PRIORITAS);

		$criteria->addSelectColumn(PakSubtitleIndikatorPeer::CATATAN);

		$criteria->addSelectColumn(PakSubtitleIndikatorPeer::LAKILAKI);

		$criteria->addSelectColumn(PakSubtitleIndikatorPeer::PEREMPUAN);

		$criteria->addSelectColumn(PakSubtitleIndikatorPeer::DEWASA);

		$criteria->addSelectColumn(PakSubtitleIndikatorPeer::ANAK);

		$criteria->addSelectColumn(PakSubtitleIndikatorPeer::LANSIA);

		$criteria->addSelectColumn(PakSubtitleIndikatorPeer::INKLUSI);

		$criteria->addSelectColumn(PakSubtitleIndikatorPeer::GENDER);

	}

	const COUNT = 'COUNT(ebudget.pak_subtitle_indikator.SUB_ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.pak_subtitle_indikator.SUB_ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(PakSubtitleIndikatorPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(PakSubtitleIndikatorPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = PakSubtitleIndikatorPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = PakSubtitleIndikatorPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return PakSubtitleIndikatorPeer::populateObjects(PakSubtitleIndikatorPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			PakSubtitleIndikatorPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = PakSubtitleIndikatorPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return PakSubtitleIndikatorPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}

		$criteria->remove(PakSubtitleIndikatorPeer::SUB_ID); 

				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(PakSubtitleIndikatorPeer::SUB_ID);
			$selectCriteria->add(PakSubtitleIndikatorPeer::SUB_ID, $criteria->remove(PakSubtitleIndikatorPeer::SUB_ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(PakSubtitleIndikatorPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(PakSubtitleIndikatorPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof PakSubtitleIndikator) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(PakSubtitleIndikatorPeer::SUB_ID, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(PakSubtitleIndikator $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(PakSubtitleIndikatorPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(PakSubtitleIndikatorPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(PakSubtitleIndikatorPeer::DATABASE_NAME, PakSubtitleIndikatorPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = PakSubtitleIndikatorPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(PakSubtitleIndikatorPeer::DATABASE_NAME);

		$criteria->add(PakSubtitleIndikatorPeer::SUB_ID, $pk);


		$v = PakSubtitleIndikatorPeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(PakSubtitleIndikatorPeer::SUB_ID, $pks, Criteria::IN);
			$objs = PakSubtitleIndikatorPeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BasePakSubtitleIndikatorPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/PakSubtitleIndikatorMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.PakSubtitleIndikatorMapBuilder');
}
