<?php


abstract class BaseBeritaAcaraRevisiPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.berita_acara_revisi';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.BeritaAcaraRevisi';

	
	const NUM_COLUMNS = 24;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const UNIT_ID = 'ebudget.berita_acara_revisi.UNIT_ID';

	
	const KEGIATAN_CODE = 'ebudget.berita_acara_revisi.KEGIATAN_CODE';

	
	const NOMOR = 'ebudget.berita_acara_revisi.NOMOR';

	
	const KESIMPULAN = 'ebudget.berita_acara_revisi.KESIMPULAN';

	
	const DASAR1 = 'ebudget.berita_acara_revisi.DASAR1';

	
	const DASAR2 = 'ebudget.berita_acara_revisi.DASAR2';

	
	const DASAR3 = 'ebudget.berita_acara_revisi.DASAR3';

	
	const DASAR4 = 'ebudget.berita_acara_revisi.DASAR4';

	
	const DASAR5 = 'ebudget.berita_acara_revisi.DASAR5';

	
	const DASAR6 = 'ebudget.berita_acara_revisi.DASAR6';

	
	const DASAR7 = 'ebudget.berita_acara_revisi.DASAR7';

	
	const DASAR8 = 'ebudget.berita_acara_revisi.DASAR8';

	
	const DASAR9 = 'ebudget.berita_acara_revisi.DASAR9';

	
	const DASAR10 = 'ebudget.berita_acara_revisi.DASAR10';

	
	const PERTIMBANGAN1 = 'ebudget.berita_acara_revisi.PERTIMBANGAN1';

	
	const PERTIMBANGAN2 = 'ebudget.berita_acara_revisi.PERTIMBANGAN2';

	
	const PERTIMBANGAN3 = 'ebudget.berita_acara_revisi.PERTIMBANGAN3';

	
	const PERTIMBANGAN4 = 'ebudget.berita_acara_revisi.PERTIMBANGAN4';

	
	const PERTIMBANGAN5 = 'ebudget.berita_acara_revisi.PERTIMBANGAN5';

	
	const PERTIMBANGAN6 = 'ebudget.berita_acara_revisi.PERTIMBANGAN6';

	
	const PERTIMBANGAN7 = 'ebudget.berita_acara_revisi.PERTIMBANGAN7';

	
	const PERTIMBANGAN8 = 'ebudget.berita_acara_revisi.PERTIMBANGAN8';

	
	const PERTIMBANGAN9 = 'ebudget.berita_acara_revisi.PERTIMBANGAN9';

	
	const PERTIMBANGAN10 = 'ebudget.berita_acara_revisi.PERTIMBANGAN10';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('UnitId', 'KegiatanCode', 'Nomor', 'Kesimpulan', 'Dasar1', 'Dasar2', 'Dasar3', 'Dasar4', 'Dasar5', 'Dasar6', 'Dasar7', 'Dasar8', 'Dasar9', 'Dasar10', 'Pertimbangan1', 'Pertimbangan2', 'Pertimbangan3', 'Pertimbangan4', 'Pertimbangan5', 'Pertimbangan6', 'Pertimbangan7', 'Pertimbangan8', 'Pertimbangan9', 'Pertimbangan10', ),
		BasePeer::TYPE_COLNAME => array (BeritaAcaraRevisiPeer::UNIT_ID, BeritaAcaraRevisiPeer::KEGIATAN_CODE, BeritaAcaraRevisiPeer::NOMOR, BeritaAcaraRevisiPeer::KESIMPULAN, BeritaAcaraRevisiPeer::DASAR1, BeritaAcaraRevisiPeer::DASAR2, BeritaAcaraRevisiPeer::DASAR3, BeritaAcaraRevisiPeer::DASAR4, BeritaAcaraRevisiPeer::DASAR5, BeritaAcaraRevisiPeer::DASAR6, BeritaAcaraRevisiPeer::DASAR7, BeritaAcaraRevisiPeer::DASAR8, BeritaAcaraRevisiPeer::DASAR9, BeritaAcaraRevisiPeer::DASAR10, BeritaAcaraRevisiPeer::PERTIMBANGAN1, BeritaAcaraRevisiPeer::PERTIMBANGAN2, BeritaAcaraRevisiPeer::PERTIMBANGAN3, BeritaAcaraRevisiPeer::PERTIMBANGAN4, BeritaAcaraRevisiPeer::PERTIMBANGAN5, BeritaAcaraRevisiPeer::PERTIMBANGAN6, BeritaAcaraRevisiPeer::PERTIMBANGAN7, BeritaAcaraRevisiPeer::PERTIMBANGAN8, BeritaAcaraRevisiPeer::PERTIMBANGAN9, BeritaAcaraRevisiPeer::PERTIMBANGAN10, ),
		BasePeer::TYPE_FIELDNAME => array ('unit_id', 'kegiatan_code', 'nomor', 'kesimpulan', 'dasar1', 'dasar2', 'dasar3', 'dasar4', 'dasar5', 'dasar6', 'dasar7', 'dasar8', 'dasar9', 'dasar10', 'pertimbangan1', 'pertimbangan2', 'pertimbangan3', 'pertimbangan4', 'pertimbangan5', 'pertimbangan6', 'pertimbangan7', 'pertimbangan8', 'pertimbangan9', 'pertimbangan10', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('UnitId' => 0, 'KegiatanCode' => 1, 'Nomor' => 2, 'Kesimpulan' => 3, 'Dasar1' => 4, 'Dasar2' => 5, 'Dasar3' => 6, 'Dasar4' => 7, 'Dasar5' => 8, 'Dasar6' => 9, 'Dasar7' => 10, 'Dasar8' => 11, 'Dasar9' => 12, 'Dasar10' => 13, 'Pertimbangan1' => 14, 'Pertimbangan2' => 15, 'Pertimbangan3' => 16, 'Pertimbangan4' => 17, 'Pertimbangan5' => 18, 'Pertimbangan6' => 19, 'Pertimbangan7' => 20, 'Pertimbangan8' => 21, 'Pertimbangan9' => 22, 'Pertimbangan10' => 23, ),
		BasePeer::TYPE_COLNAME => array (BeritaAcaraRevisiPeer::UNIT_ID => 0, BeritaAcaraRevisiPeer::KEGIATAN_CODE => 1, BeritaAcaraRevisiPeer::NOMOR => 2, BeritaAcaraRevisiPeer::KESIMPULAN => 3, BeritaAcaraRevisiPeer::DASAR1 => 4, BeritaAcaraRevisiPeer::DASAR2 => 5, BeritaAcaraRevisiPeer::DASAR3 => 6, BeritaAcaraRevisiPeer::DASAR4 => 7, BeritaAcaraRevisiPeer::DASAR5 => 8, BeritaAcaraRevisiPeer::DASAR6 => 9, BeritaAcaraRevisiPeer::DASAR7 => 10, BeritaAcaraRevisiPeer::DASAR8 => 11, BeritaAcaraRevisiPeer::DASAR9 => 12, BeritaAcaraRevisiPeer::DASAR10 => 13, BeritaAcaraRevisiPeer::PERTIMBANGAN1 => 14, BeritaAcaraRevisiPeer::PERTIMBANGAN2 => 15, BeritaAcaraRevisiPeer::PERTIMBANGAN3 => 16, BeritaAcaraRevisiPeer::PERTIMBANGAN4 => 17, BeritaAcaraRevisiPeer::PERTIMBANGAN5 => 18, BeritaAcaraRevisiPeer::PERTIMBANGAN6 => 19, BeritaAcaraRevisiPeer::PERTIMBANGAN7 => 20, BeritaAcaraRevisiPeer::PERTIMBANGAN8 => 21, BeritaAcaraRevisiPeer::PERTIMBANGAN9 => 22, BeritaAcaraRevisiPeer::PERTIMBANGAN10 => 23, ),
		BasePeer::TYPE_FIELDNAME => array ('unit_id' => 0, 'kegiatan_code' => 1, 'nomor' => 2, 'kesimpulan' => 3, 'dasar1' => 4, 'dasar2' => 5, 'dasar3' => 6, 'dasar4' => 7, 'dasar5' => 8, 'dasar6' => 9, 'dasar7' => 10, 'dasar8' => 11, 'dasar9' => 12, 'dasar10' => 13, 'pertimbangan1' => 14, 'pertimbangan2' => 15, 'pertimbangan3' => 16, 'pertimbangan4' => 17, 'pertimbangan5' => 18, 'pertimbangan6' => 19, 'pertimbangan7' => 20, 'pertimbangan8' => 21, 'pertimbangan9' => 22, 'pertimbangan10' => 23, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/BeritaAcaraRevisiMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.BeritaAcaraRevisiMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = BeritaAcaraRevisiPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(BeritaAcaraRevisiPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(BeritaAcaraRevisiPeer::UNIT_ID);

		$criteria->addSelectColumn(BeritaAcaraRevisiPeer::KEGIATAN_CODE);

		$criteria->addSelectColumn(BeritaAcaraRevisiPeer::NOMOR);

		$criteria->addSelectColumn(BeritaAcaraRevisiPeer::KESIMPULAN);

		$criteria->addSelectColumn(BeritaAcaraRevisiPeer::DASAR1);

		$criteria->addSelectColumn(BeritaAcaraRevisiPeer::DASAR2);

		$criteria->addSelectColumn(BeritaAcaraRevisiPeer::DASAR3);

		$criteria->addSelectColumn(BeritaAcaraRevisiPeer::DASAR4);

		$criteria->addSelectColumn(BeritaAcaraRevisiPeer::DASAR5);

		$criteria->addSelectColumn(BeritaAcaraRevisiPeer::DASAR6);

		$criteria->addSelectColumn(BeritaAcaraRevisiPeer::DASAR7);

		$criteria->addSelectColumn(BeritaAcaraRevisiPeer::DASAR8);

		$criteria->addSelectColumn(BeritaAcaraRevisiPeer::DASAR9);

		$criteria->addSelectColumn(BeritaAcaraRevisiPeer::DASAR10);

		$criteria->addSelectColumn(BeritaAcaraRevisiPeer::PERTIMBANGAN1);

		$criteria->addSelectColumn(BeritaAcaraRevisiPeer::PERTIMBANGAN2);

		$criteria->addSelectColumn(BeritaAcaraRevisiPeer::PERTIMBANGAN3);

		$criteria->addSelectColumn(BeritaAcaraRevisiPeer::PERTIMBANGAN4);

		$criteria->addSelectColumn(BeritaAcaraRevisiPeer::PERTIMBANGAN5);

		$criteria->addSelectColumn(BeritaAcaraRevisiPeer::PERTIMBANGAN6);

		$criteria->addSelectColumn(BeritaAcaraRevisiPeer::PERTIMBANGAN7);

		$criteria->addSelectColumn(BeritaAcaraRevisiPeer::PERTIMBANGAN8);

		$criteria->addSelectColumn(BeritaAcaraRevisiPeer::PERTIMBANGAN9);

		$criteria->addSelectColumn(BeritaAcaraRevisiPeer::PERTIMBANGAN10);

	}

	const COUNT = 'COUNT(*)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT *)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(BeritaAcaraRevisiPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(BeritaAcaraRevisiPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = BeritaAcaraRevisiPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = BeritaAcaraRevisiPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return BeritaAcaraRevisiPeer::populateObjects(BeritaAcaraRevisiPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			BeritaAcaraRevisiPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = BeritaAcaraRevisiPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return BeritaAcaraRevisiPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(BeritaAcaraRevisiPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(BeritaAcaraRevisiPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof BeritaAcaraRevisi) {

			$criteria = $values->buildCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
												if(count($values) == count($values, COUNT_RECURSIVE))
			{
								$values = array($values);
			}
			$vals = array();
			foreach($values as $value)
			{

			}

		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(BeritaAcaraRevisi $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(BeritaAcaraRevisiPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(BeritaAcaraRevisiPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(BeritaAcaraRevisiPeer::DATABASE_NAME, BeritaAcaraRevisiPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = BeritaAcaraRevisiPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

} 
if (Propel::isInit()) {
			try {
		BaseBeritaAcaraRevisiPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/BeritaAcaraRevisiMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.BeritaAcaraRevisiMapBuilder');
}
