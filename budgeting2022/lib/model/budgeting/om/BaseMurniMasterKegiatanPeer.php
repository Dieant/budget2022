<?php


abstract class BaseMurniMasterKegiatanPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.murni_master_kegiatan';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.MurniMasterKegiatan';

	
	const NUM_COLUMNS = 63;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const UNIT_ID = 'ebudget.murni_master_kegiatan.UNIT_ID';

	
	const KODE_KEGIATAN = 'ebudget.murni_master_kegiatan.KODE_KEGIATAN';

	
	const KODE_BIDANG = 'ebudget.murni_master_kegiatan.KODE_BIDANG';

	
	const KODE_URUSAN_WAJIB = 'ebudget.murni_master_kegiatan.KODE_URUSAN_WAJIB';

	
	const KODE_PROGRAM = 'ebudget.murni_master_kegiatan.KODE_PROGRAM';

	
	const KODE_SASARAN = 'ebudget.murni_master_kegiatan.KODE_SASARAN';

	
	const KODE_INDIKATOR = 'ebudget.murni_master_kegiatan.KODE_INDIKATOR';

	
	const ALOKASI_DANA = 'ebudget.murni_master_kegiatan.ALOKASI_DANA';

	
	const NAMA_KEGIATAN = 'ebudget.murni_master_kegiatan.NAMA_KEGIATAN';

	
	const MASUKAN = 'ebudget.murni_master_kegiatan.MASUKAN';

	
	const OUTPUT = 'ebudget.murni_master_kegiatan.OUTPUT';

	
	const OUTCOME = 'ebudget.murni_master_kegiatan.OUTCOME';

	
	const BENEFIT = 'ebudget.murni_master_kegiatan.BENEFIT';

	
	const IMPACT = 'ebudget.murni_master_kegiatan.IMPACT';

	
	const TIPE = 'ebudget.murni_master_kegiatan.TIPE';

	
	const KEGIATAN_ACTIVE = 'ebudget.murni_master_kegiatan.KEGIATAN_ACTIVE';

	
	const TO_KEGIATAN_CODE = 'ebudget.murni_master_kegiatan.TO_KEGIATAN_CODE';

	
	const CATATAN = 'ebudget.murni_master_kegiatan.CATATAN';

	
	const TARGET_OUTCOME = 'ebudget.murni_master_kegiatan.TARGET_OUTCOME';

	
	const LOKASI = 'ebudget.murni_master_kegiatan.LOKASI';

	
	const JUMLAH_PREV = 'ebudget.murni_master_kegiatan.JUMLAH_PREV';

	
	const JUMLAH_NOW = 'ebudget.murni_master_kegiatan.JUMLAH_NOW';

	
	const JUMLAH_NEXT = 'ebudget.murni_master_kegiatan.JUMLAH_NEXT';

	
	const KODE_PROGRAM2 = 'ebudget.murni_master_kegiatan.KODE_PROGRAM2';

	
	const KODE_URUSAN = 'ebudget.murni_master_kegiatan.KODE_URUSAN';

	
	const LAST_UPDATE_USER = 'ebudget.murni_master_kegiatan.LAST_UPDATE_USER';

	
	const LAST_UPDATE_TIME = 'ebudget.murni_master_kegiatan.LAST_UPDATE_TIME';

	
	const LAST_UPDATE_IP = 'ebudget.murni_master_kegiatan.LAST_UPDATE_IP';

	
	const TAHAP = 'ebudget.murni_master_kegiatan.TAHAP';

	
	const KODE_MISI = 'ebudget.murni_master_kegiatan.KODE_MISI';

	
	const KODE_TUJUAN = 'ebudget.murni_master_kegiatan.KODE_TUJUAN';

	
	const RANKING = 'ebudget.murni_master_kegiatan.RANKING';

	
	const NOMOR13 = 'ebudget.murni_master_kegiatan.NOMOR13';

	
	const PPA_NAMA = 'ebudget.murni_master_kegiatan.PPA_NAMA';

	
	const PPA_PANGKAT = 'ebudget.murni_master_kegiatan.PPA_PANGKAT';

	
	const PPA_NIP = 'ebudget.murni_master_kegiatan.PPA_NIP';

	
	const LANJUTAN = 'ebudget.murni_master_kegiatan.LANJUTAN';

	
	const USER_ID = 'ebudget.murni_master_kegiatan.USER_ID';

	
	const ID = 'ebudget.murni_master_kegiatan.ID';

	
	const TAHUN = 'ebudget.murni_master_kegiatan.TAHUN';

	
	const TAMBAHAN_PAGU = 'ebudget.murni_master_kegiatan.TAMBAHAN_PAGU';

	
	const GENDER = 'ebudget.murni_master_kegiatan.GENDER';

	
	const KODE_KEG_KEUANGAN = 'ebudget.murni_master_kegiatan.KODE_KEG_KEUANGAN';

	
	const USER_ID_LAMA = 'ebudget.murni_master_kegiatan.USER_ID_LAMA';

	
	const INDIKATOR = 'ebudget.murni_master_kegiatan.INDIKATOR';

	
	const IS_DAK = 'ebudget.murni_master_kegiatan.IS_DAK';

	
	const KODE_KEGIATAN_ASAL = 'ebudget.murni_master_kegiatan.KODE_KEGIATAN_ASAL';

	
	const KODE_KEG_KEUANGAN_ASAL = 'ebudget.murni_master_kegiatan.KODE_KEG_KEUANGAN_ASAL';

	
	const TH_KE_MULTIYEARS = 'ebudget.murni_master_kegiatan.TH_KE_MULTIYEARS';

	
	const KELOMPOK_SASARAN = 'ebudget.murni_master_kegiatan.KELOMPOK_SASARAN';

	
	const PAGU_BAPPEKO = 'ebudget.murni_master_kegiatan.PAGU_BAPPEKO';

	
	const KODE_DPA = 'ebudget.murni_master_kegiatan.KODE_DPA';

	
	const USER_ID_PPTK = 'ebudget.murni_master_kegiatan.USER_ID_PPTK';

	
	const USER_ID_KPA = 'ebudget.murni_master_kegiatan.USER_ID_KPA';

	
	const CATATAN_PEMBAHASAN = 'ebudget.murni_master_kegiatan.CATATAN_PEMBAHASAN';

	
	const CATATAN_PENYELIA = 'ebudget.murni_master_kegiatan.CATATAN_PENYELIA';

	
	const CATATAN_BAPPEKO = 'ebudget.murni_master_kegiatan.CATATAN_BAPPEKO';

	
	const STATUS_LEVEL = 'ebudget.murni_master_kegiatan.STATUS_LEVEL';

	
	const IS_TAPD_SETUJU = 'ebudget.murni_master_kegiatan.IS_TAPD_SETUJU';

	
	const IS_BAPPEKO_SETUJU = 'ebudget.murni_master_kegiatan.IS_BAPPEKO_SETUJU';

	
	const IS_PENYELIA_SETUJU = 'ebudget.murni_master_kegiatan.IS_PENYELIA_SETUJU';

	
	const IS_PERNAH_RKA = 'ebudget.murni_master_kegiatan.IS_PERNAH_RKA';

	
	const KODE_KEGIATAN_BARU = 'ebudget.murni_master_kegiatan.KODE_KEGIATAN_BARU';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('UnitId', 'KodeKegiatan', 'KodeBidang', 'KodeUrusanWajib', 'KodeProgram', 'KodeSasaran', 'KodeIndikator', 'AlokasiDana', 'NamaKegiatan', 'Masukan', 'Output', 'Outcome', 'Benefit', 'Impact', 'Tipe', 'KegiatanActive', 'ToKegiatanCode', 'Catatan', 'TargetOutcome', 'Lokasi', 'JumlahPrev', 'JumlahNow', 'JumlahNext', 'KodeProgram2', 'KodeUrusan', 'LastUpdateUser', 'LastUpdateTime', 'LastUpdateIp', 'Tahap', 'KodeMisi', 'KodeTujuan', 'Ranking', 'Nomor13', 'PpaNama', 'PpaPangkat', 'PpaNip', 'Lanjutan', 'UserId', 'Id', 'Tahun', 'TambahanPagu', 'Gender', 'KodeKegKeuangan', 'UserIdLama', 'Indikator', 'IsDak', 'KodeKegiatanAsal', 'KodeKegKeuanganAsal', 'ThKeMultiyears', 'KelompokSasaran', 'PaguBappeko', 'KodeDpa', 'UserIdPptk', 'UserIdKpa', 'CatatanPembahasan', 'CatatanPenyelia', 'CatatanBappeko', 'StatusLevel', 'IsTapdSetuju', 'IsBappekoSetuju', 'IsPenyeliaSetuju', 'IsPernahRka', 'KodeKegiatanBaru', ),
		BasePeer::TYPE_COLNAME => array (MurniMasterKegiatanPeer::UNIT_ID, MurniMasterKegiatanPeer::KODE_KEGIATAN, MurniMasterKegiatanPeer::KODE_BIDANG, MurniMasterKegiatanPeer::KODE_URUSAN_WAJIB, MurniMasterKegiatanPeer::KODE_PROGRAM, MurniMasterKegiatanPeer::KODE_SASARAN, MurniMasterKegiatanPeer::KODE_INDIKATOR, MurniMasterKegiatanPeer::ALOKASI_DANA, MurniMasterKegiatanPeer::NAMA_KEGIATAN, MurniMasterKegiatanPeer::MASUKAN, MurniMasterKegiatanPeer::OUTPUT, MurniMasterKegiatanPeer::OUTCOME, MurniMasterKegiatanPeer::BENEFIT, MurniMasterKegiatanPeer::IMPACT, MurniMasterKegiatanPeer::TIPE, MurniMasterKegiatanPeer::KEGIATAN_ACTIVE, MurniMasterKegiatanPeer::TO_KEGIATAN_CODE, MurniMasterKegiatanPeer::CATATAN, MurniMasterKegiatanPeer::TARGET_OUTCOME, MurniMasterKegiatanPeer::LOKASI, MurniMasterKegiatanPeer::JUMLAH_PREV, MurniMasterKegiatanPeer::JUMLAH_NOW, MurniMasterKegiatanPeer::JUMLAH_NEXT, MurniMasterKegiatanPeer::KODE_PROGRAM2, MurniMasterKegiatanPeer::KODE_URUSAN, MurniMasterKegiatanPeer::LAST_UPDATE_USER, MurniMasterKegiatanPeer::LAST_UPDATE_TIME, MurniMasterKegiatanPeer::LAST_UPDATE_IP, MurniMasterKegiatanPeer::TAHAP, MurniMasterKegiatanPeer::KODE_MISI, MurniMasterKegiatanPeer::KODE_TUJUAN, MurniMasterKegiatanPeer::RANKING, MurniMasterKegiatanPeer::NOMOR13, MurniMasterKegiatanPeer::PPA_NAMA, MurniMasterKegiatanPeer::PPA_PANGKAT, MurniMasterKegiatanPeer::PPA_NIP, MurniMasterKegiatanPeer::LANJUTAN, MurniMasterKegiatanPeer::USER_ID, MurniMasterKegiatanPeer::ID, MurniMasterKegiatanPeer::TAHUN, MurniMasterKegiatanPeer::TAMBAHAN_PAGU, MurniMasterKegiatanPeer::GENDER, MurniMasterKegiatanPeer::KODE_KEG_KEUANGAN, MurniMasterKegiatanPeer::USER_ID_LAMA, MurniMasterKegiatanPeer::INDIKATOR, MurniMasterKegiatanPeer::IS_DAK, MurniMasterKegiatanPeer::KODE_KEGIATAN_ASAL, MurniMasterKegiatanPeer::KODE_KEG_KEUANGAN_ASAL, MurniMasterKegiatanPeer::TH_KE_MULTIYEARS, MurniMasterKegiatanPeer::KELOMPOK_SASARAN, MurniMasterKegiatanPeer::PAGU_BAPPEKO, MurniMasterKegiatanPeer::KODE_DPA, MurniMasterKegiatanPeer::USER_ID_PPTK, MurniMasterKegiatanPeer::USER_ID_KPA, MurniMasterKegiatanPeer::CATATAN_PEMBAHASAN, MurniMasterKegiatanPeer::CATATAN_PENYELIA, MurniMasterKegiatanPeer::CATATAN_BAPPEKO, MurniMasterKegiatanPeer::STATUS_LEVEL, MurniMasterKegiatanPeer::IS_TAPD_SETUJU, MurniMasterKegiatanPeer::IS_BAPPEKO_SETUJU, MurniMasterKegiatanPeer::IS_PENYELIA_SETUJU, MurniMasterKegiatanPeer::IS_PERNAH_RKA, MurniMasterKegiatanPeer::KODE_KEGIATAN_BARU, ),
		BasePeer::TYPE_FIELDNAME => array ('unit_id', 'kode_kegiatan', 'kode_bidang', 'kode_urusan_wajib', 'kode_program', 'kode_sasaran', 'kode_indikator', 'alokasi_dana', 'nama_kegiatan', 'masukan', 'output', 'outcome', 'benefit', 'impact', 'tipe', 'kegiatan_active', 'to_kegiatan_code', 'catatan', 'target_outcome', 'lokasi', 'jumlah_prev', 'jumlah_now', 'jumlah_next', 'kode_program2', 'kode_urusan', 'last_update_user', 'last_update_time', 'last_update_ip', 'tahap', 'kode_misi', 'kode_tujuan', 'ranking', 'nomor13', 'ppa_nama', 'ppa_pangkat', 'ppa_nip', 'lanjutan', 'user_id', 'id', 'tahun', 'tambahan_pagu', 'gender', 'kode_keg_keuangan', 'user_id_lama', 'indikator', 'is_dak', 'kode_kegiatan_asal', 'kode_keg_keuangan_asal', 'th_ke_multiyears', 'kelompok_sasaran', 'pagu_bappeko', 'kode_dpa', 'user_id_pptk', 'user_id_kpa', 'catatan_pembahasan', 'catatan_penyelia', 'catatan_bappeko', 'status_level', 'is_tapd_setuju', 'is_bappeko_setuju', 'is_penyelia_setuju', 'is_pernah_rka', 'kode_kegiatan_baru', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('UnitId' => 0, 'KodeKegiatan' => 1, 'KodeBidang' => 2, 'KodeUrusanWajib' => 3, 'KodeProgram' => 4, 'KodeSasaran' => 5, 'KodeIndikator' => 6, 'AlokasiDana' => 7, 'NamaKegiatan' => 8, 'Masukan' => 9, 'Output' => 10, 'Outcome' => 11, 'Benefit' => 12, 'Impact' => 13, 'Tipe' => 14, 'KegiatanActive' => 15, 'ToKegiatanCode' => 16, 'Catatan' => 17, 'TargetOutcome' => 18, 'Lokasi' => 19, 'JumlahPrev' => 20, 'JumlahNow' => 21, 'JumlahNext' => 22, 'KodeProgram2' => 23, 'KodeUrusan' => 24, 'LastUpdateUser' => 25, 'LastUpdateTime' => 26, 'LastUpdateIp' => 27, 'Tahap' => 28, 'KodeMisi' => 29, 'KodeTujuan' => 30, 'Ranking' => 31, 'Nomor13' => 32, 'PpaNama' => 33, 'PpaPangkat' => 34, 'PpaNip' => 35, 'Lanjutan' => 36, 'UserId' => 37, 'Id' => 38, 'Tahun' => 39, 'TambahanPagu' => 40, 'Gender' => 41, 'KodeKegKeuangan' => 42, 'UserIdLama' => 43, 'Indikator' => 44, 'IsDak' => 45, 'KodeKegiatanAsal' => 46, 'KodeKegKeuanganAsal' => 47, 'ThKeMultiyears' => 48, 'KelompokSasaran' => 49, 'PaguBappeko' => 50, 'KodeDpa' => 51, 'UserIdPptk' => 52, 'UserIdKpa' => 53, 'CatatanPembahasan' => 54, 'CatatanPenyelia' => 55, 'CatatanBappeko' => 56, 'StatusLevel' => 57, 'IsTapdSetuju' => 58, 'IsBappekoSetuju' => 59, 'IsPenyeliaSetuju' => 60, 'IsPernahRka' => 61, 'KodeKegiatanBaru' => 62, ),
		BasePeer::TYPE_COLNAME => array (MurniMasterKegiatanPeer::UNIT_ID => 0, MurniMasterKegiatanPeer::KODE_KEGIATAN => 1, MurniMasterKegiatanPeer::KODE_BIDANG => 2, MurniMasterKegiatanPeer::KODE_URUSAN_WAJIB => 3, MurniMasterKegiatanPeer::KODE_PROGRAM => 4, MurniMasterKegiatanPeer::KODE_SASARAN => 5, MurniMasterKegiatanPeer::KODE_INDIKATOR => 6, MurniMasterKegiatanPeer::ALOKASI_DANA => 7, MurniMasterKegiatanPeer::NAMA_KEGIATAN => 8, MurniMasterKegiatanPeer::MASUKAN => 9, MurniMasterKegiatanPeer::OUTPUT => 10, MurniMasterKegiatanPeer::OUTCOME => 11, MurniMasterKegiatanPeer::BENEFIT => 12, MurniMasterKegiatanPeer::IMPACT => 13, MurniMasterKegiatanPeer::TIPE => 14, MurniMasterKegiatanPeer::KEGIATAN_ACTIVE => 15, MurniMasterKegiatanPeer::TO_KEGIATAN_CODE => 16, MurniMasterKegiatanPeer::CATATAN => 17, MurniMasterKegiatanPeer::TARGET_OUTCOME => 18, MurniMasterKegiatanPeer::LOKASI => 19, MurniMasterKegiatanPeer::JUMLAH_PREV => 20, MurniMasterKegiatanPeer::JUMLAH_NOW => 21, MurniMasterKegiatanPeer::JUMLAH_NEXT => 22, MurniMasterKegiatanPeer::KODE_PROGRAM2 => 23, MurniMasterKegiatanPeer::KODE_URUSAN => 24, MurniMasterKegiatanPeer::LAST_UPDATE_USER => 25, MurniMasterKegiatanPeer::LAST_UPDATE_TIME => 26, MurniMasterKegiatanPeer::LAST_UPDATE_IP => 27, MurniMasterKegiatanPeer::TAHAP => 28, MurniMasterKegiatanPeer::KODE_MISI => 29, MurniMasterKegiatanPeer::KODE_TUJUAN => 30, MurniMasterKegiatanPeer::RANKING => 31, MurniMasterKegiatanPeer::NOMOR13 => 32, MurniMasterKegiatanPeer::PPA_NAMA => 33, MurniMasterKegiatanPeer::PPA_PANGKAT => 34, MurniMasterKegiatanPeer::PPA_NIP => 35, MurniMasterKegiatanPeer::LANJUTAN => 36, MurniMasterKegiatanPeer::USER_ID => 37, MurniMasterKegiatanPeer::ID => 38, MurniMasterKegiatanPeer::TAHUN => 39, MurniMasterKegiatanPeer::TAMBAHAN_PAGU => 40, MurniMasterKegiatanPeer::GENDER => 41, MurniMasterKegiatanPeer::KODE_KEG_KEUANGAN => 42, MurniMasterKegiatanPeer::USER_ID_LAMA => 43, MurniMasterKegiatanPeer::INDIKATOR => 44, MurniMasterKegiatanPeer::IS_DAK => 45, MurniMasterKegiatanPeer::KODE_KEGIATAN_ASAL => 46, MurniMasterKegiatanPeer::KODE_KEG_KEUANGAN_ASAL => 47, MurniMasterKegiatanPeer::TH_KE_MULTIYEARS => 48, MurniMasterKegiatanPeer::KELOMPOK_SASARAN => 49, MurniMasterKegiatanPeer::PAGU_BAPPEKO => 50, MurniMasterKegiatanPeer::KODE_DPA => 51, MurniMasterKegiatanPeer::USER_ID_PPTK => 52, MurniMasterKegiatanPeer::USER_ID_KPA => 53, MurniMasterKegiatanPeer::CATATAN_PEMBAHASAN => 54, MurniMasterKegiatanPeer::CATATAN_PENYELIA => 55, MurniMasterKegiatanPeer::CATATAN_BAPPEKO => 56, MurniMasterKegiatanPeer::STATUS_LEVEL => 57, MurniMasterKegiatanPeer::IS_TAPD_SETUJU => 58, MurniMasterKegiatanPeer::IS_BAPPEKO_SETUJU => 59, MurniMasterKegiatanPeer::IS_PENYELIA_SETUJU => 60, MurniMasterKegiatanPeer::IS_PERNAH_RKA => 61, MurniMasterKegiatanPeer::KODE_KEGIATAN_BARU => 62, ),
		BasePeer::TYPE_FIELDNAME => array ('unit_id' => 0, 'kode_kegiatan' => 1, 'kode_bidang' => 2, 'kode_urusan_wajib' => 3, 'kode_program' => 4, 'kode_sasaran' => 5, 'kode_indikator' => 6, 'alokasi_dana' => 7, 'nama_kegiatan' => 8, 'masukan' => 9, 'output' => 10, 'outcome' => 11, 'benefit' => 12, 'impact' => 13, 'tipe' => 14, 'kegiatan_active' => 15, 'to_kegiatan_code' => 16, 'catatan' => 17, 'target_outcome' => 18, 'lokasi' => 19, 'jumlah_prev' => 20, 'jumlah_now' => 21, 'jumlah_next' => 22, 'kode_program2' => 23, 'kode_urusan' => 24, 'last_update_user' => 25, 'last_update_time' => 26, 'last_update_ip' => 27, 'tahap' => 28, 'kode_misi' => 29, 'kode_tujuan' => 30, 'ranking' => 31, 'nomor13' => 32, 'ppa_nama' => 33, 'ppa_pangkat' => 34, 'ppa_nip' => 35, 'lanjutan' => 36, 'user_id' => 37, 'id' => 38, 'tahun' => 39, 'tambahan_pagu' => 40, 'gender' => 41, 'kode_keg_keuangan' => 42, 'user_id_lama' => 43, 'indikator' => 44, 'is_dak' => 45, 'kode_kegiatan_asal' => 46, 'kode_keg_keuangan_asal' => 47, 'th_ke_multiyears' => 48, 'kelompok_sasaran' => 49, 'pagu_bappeko' => 50, 'kode_dpa' => 51, 'user_id_pptk' => 52, 'user_id_kpa' => 53, 'catatan_pembahasan' => 54, 'catatan_penyelia' => 55, 'catatan_bappeko' => 56, 'status_level' => 57, 'is_tapd_setuju' => 58, 'is_bappeko_setuju' => 59, 'is_penyelia_setuju' => 60, 'is_pernah_rka' => 61, 'kode_kegiatan_baru' => 62, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/MurniMasterKegiatanMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.MurniMasterKegiatanMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = MurniMasterKegiatanPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(MurniMasterKegiatanPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(MurniMasterKegiatanPeer::UNIT_ID);

		$criteria->addSelectColumn(MurniMasterKegiatanPeer::KODE_KEGIATAN);

		$criteria->addSelectColumn(MurniMasterKegiatanPeer::KODE_BIDANG);

		$criteria->addSelectColumn(MurniMasterKegiatanPeer::KODE_URUSAN_WAJIB);

		$criteria->addSelectColumn(MurniMasterKegiatanPeer::KODE_PROGRAM);

		$criteria->addSelectColumn(MurniMasterKegiatanPeer::KODE_SASARAN);

		$criteria->addSelectColumn(MurniMasterKegiatanPeer::KODE_INDIKATOR);

		$criteria->addSelectColumn(MurniMasterKegiatanPeer::ALOKASI_DANA);

		$criteria->addSelectColumn(MurniMasterKegiatanPeer::NAMA_KEGIATAN);

		$criteria->addSelectColumn(MurniMasterKegiatanPeer::MASUKAN);

		$criteria->addSelectColumn(MurniMasterKegiatanPeer::OUTPUT);

		$criteria->addSelectColumn(MurniMasterKegiatanPeer::OUTCOME);

		$criteria->addSelectColumn(MurniMasterKegiatanPeer::BENEFIT);

		$criteria->addSelectColumn(MurniMasterKegiatanPeer::IMPACT);

		$criteria->addSelectColumn(MurniMasterKegiatanPeer::TIPE);

		$criteria->addSelectColumn(MurniMasterKegiatanPeer::KEGIATAN_ACTIVE);

		$criteria->addSelectColumn(MurniMasterKegiatanPeer::TO_KEGIATAN_CODE);

		$criteria->addSelectColumn(MurniMasterKegiatanPeer::CATATAN);

		$criteria->addSelectColumn(MurniMasterKegiatanPeer::TARGET_OUTCOME);

		$criteria->addSelectColumn(MurniMasterKegiatanPeer::LOKASI);

		$criteria->addSelectColumn(MurniMasterKegiatanPeer::JUMLAH_PREV);

		$criteria->addSelectColumn(MurniMasterKegiatanPeer::JUMLAH_NOW);

		$criteria->addSelectColumn(MurniMasterKegiatanPeer::JUMLAH_NEXT);

		$criteria->addSelectColumn(MurniMasterKegiatanPeer::KODE_PROGRAM2);

		$criteria->addSelectColumn(MurniMasterKegiatanPeer::KODE_URUSAN);

		$criteria->addSelectColumn(MurniMasterKegiatanPeer::LAST_UPDATE_USER);

		$criteria->addSelectColumn(MurniMasterKegiatanPeer::LAST_UPDATE_TIME);

		$criteria->addSelectColumn(MurniMasterKegiatanPeer::LAST_UPDATE_IP);

		$criteria->addSelectColumn(MurniMasterKegiatanPeer::TAHAP);

		$criteria->addSelectColumn(MurniMasterKegiatanPeer::KODE_MISI);

		$criteria->addSelectColumn(MurniMasterKegiatanPeer::KODE_TUJUAN);

		$criteria->addSelectColumn(MurniMasterKegiatanPeer::RANKING);

		$criteria->addSelectColumn(MurniMasterKegiatanPeer::NOMOR13);

		$criteria->addSelectColumn(MurniMasterKegiatanPeer::PPA_NAMA);

		$criteria->addSelectColumn(MurniMasterKegiatanPeer::PPA_PANGKAT);

		$criteria->addSelectColumn(MurniMasterKegiatanPeer::PPA_NIP);

		$criteria->addSelectColumn(MurniMasterKegiatanPeer::LANJUTAN);

		$criteria->addSelectColumn(MurniMasterKegiatanPeer::USER_ID);

		$criteria->addSelectColumn(MurniMasterKegiatanPeer::ID);

		$criteria->addSelectColumn(MurniMasterKegiatanPeer::TAHUN);

		$criteria->addSelectColumn(MurniMasterKegiatanPeer::TAMBAHAN_PAGU);

		$criteria->addSelectColumn(MurniMasterKegiatanPeer::GENDER);

		$criteria->addSelectColumn(MurniMasterKegiatanPeer::KODE_KEG_KEUANGAN);

		$criteria->addSelectColumn(MurniMasterKegiatanPeer::USER_ID_LAMA);

		$criteria->addSelectColumn(MurniMasterKegiatanPeer::INDIKATOR);

		$criteria->addSelectColumn(MurniMasterKegiatanPeer::IS_DAK);

		$criteria->addSelectColumn(MurniMasterKegiatanPeer::KODE_KEGIATAN_ASAL);

		$criteria->addSelectColumn(MurniMasterKegiatanPeer::KODE_KEG_KEUANGAN_ASAL);

		$criteria->addSelectColumn(MurniMasterKegiatanPeer::TH_KE_MULTIYEARS);

		$criteria->addSelectColumn(MurniMasterKegiatanPeer::KELOMPOK_SASARAN);

		$criteria->addSelectColumn(MurniMasterKegiatanPeer::PAGU_BAPPEKO);

		$criteria->addSelectColumn(MurniMasterKegiatanPeer::KODE_DPA);

		$criteria->addSelectColumn(MurniMasterKegiatanPeer::USER_ID_PPTK);

		$criteria->addSelectColumn(MurniMasterKegiatanPeer::USER_ID_KPA);

		$criteria->addSelectColumn(MurniMasterKegiatanPeer::CATATAN_PEMBAHASAN);

		$criteria->addSelectColumn(MurniMasterKegiatanPeer::CATATAN_PENYELIA);

		$criteria->addSelectColumn(MurniMasterKegiatanPeer::CATATAN_BAPPEKO);

		$criteria->addSelectColumn(MurniMasterKegiatanPeer::STATUS_LEVEL);

		$criteria->addSelectColumn(MurniMasterKegiatanPeer::IS_TAPD_SETUJU);

		$criteria->addSelectColumn(MurniMasterKegiatanPeer::IS_BAPPEKO_SETUJU);

		$criteria->addSelectColumn(MurniMasterKegiatanPeer::IS_PENYELIA_SETUJU);

		$criteria->addSelectColumn(MurniMasterKegiatanPeer::IS_PERNAH_RKA);

		$criteria->addSelectColumn(MurniMasterKegiatanPeer::KODE_KEGIATAN_BARU);

	}

	const COUNT = 'COUNT(ebudget.murni_master_kegiatan.UNIT_ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.murni_master_kegiatan.UNIT_ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(MurniMasterKegiatanPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(MurniMasterKegiatanPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = MurniMasterKegiatanPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = MurniMasterKegiatanPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return MurniMasterKegiatanPeer::populateObjects(MurniMasterKegiatanPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			MurniMasterKegiatanPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = MurniMasterKegiatanPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return MurniMasterKegiatanPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}

		$criteria->remove(MurniMasterKegiatanPeer::ID); 

				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(MurniMasterKegiatanPeer::UNIT_ID);
			$selectCriteria->add(MurniMasterKegiatanPeer::UNIT_ID, $criteria->remove(MurniMasterKegiatanPeer::UNIT_ID), $comparison);

			$comparison = $criteria->getComparison(MurniMasterKegiatanPeer::KODE_KEGIATAN);
			$selectCriteria->add(MurniMasterKegiatanPeer::KODE_KEGIATAN, $criteria->remove(MurniMasterKegiatanPeer::KODE_KEGIATAN), $comparison);

			$comparison = $criteria->getComparison(MurniMasterKegiatanPeer::ID);
			$selectCriteria->add(MurniMasterKegiatanPeer::ID, $criteria->remove(MurniMasterKegiatanPeer::ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(MurniMasterKegiatanPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(MurniMasterKegiatanPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof MurniMasterKegiatan) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
												if(count($values) == count($values, COUNT_RECURSIVE))
			{
								$values = array($values);
			}
			$vals = array();
			foreach($values as $value)
			{

				$vals[0][] = $value[0];
				$vals[1][] = $value[1];
				$vals[2][] = $value[2];
			}

			$criteria->add(MurniMasterKegiatanPeer::UNIT_ID, $vals[0], Criteria::IN);
			$criteria->add(MurniMasterKegiatanPeer::KODE_KEGIATAN, $vals[1], Criteria::IN);
			$criteria->add(MurniMasterKegiatanPeer::ID, $vals[2], Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(MurniMasterKegiatan $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(MurniMasterKegiatanPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(MurniMasterKegiatanPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		return BasePeer::doValidate(MurniMasterKegiatanPeer::DATABASE_NAME, MurniMasterKegiatanPeer::TABLE_NAME, $columns);
	}

	
	public static function retrieveByPK( $unit_id, $kode_kegiatan, $id, $con = null) {
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$criteria = new Criteria();
		$criteria->add(MurniMasterKegiatanPeer::UNIT_ID, $unit_id);
		$criteria->add(MurniMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
		$criteria->add(MurniMasterKegiatanPeer::ID, $id);
		$v = MurniMasterKegiatanPeer::doSelect($criteria, $con);

		return !empty($v) ? $v[0] : null;
	}
} 
if (Propel::isInit()) {
			try {
		BaseMurniMasterKegiatanPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/MurniMasterKegiatanMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.MurniMasterKegiatanMapBuilder');
}
