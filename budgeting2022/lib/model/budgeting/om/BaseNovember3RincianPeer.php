<?php


abstract class BaseNovember3RincianPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.november3_rincian';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.November3Rincian';

	
	const NUM_COLUMNS = 16;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const KEGIATAN_CODE = 'ebudget.november3_rincian.KEGIATAN_CODE';

	
	const TIPE = 'ebudget.november3_rincian.TIPE';

	
	const RINCIAN_CONFIRMED = 'ebudget.november3_rincian.RINCIAN_CONFIRMED';

	
	const RINCIAN_CHANGED = 'ebudget.november3_rincian.RINCIAN_CHANGED';

	
	const RINCIAN_SELESAI = 'ebudget.november3_rincian.RINCIAN_SELESAI';

	
	const IP_ADDRESS = 'ebudget.november3_rincian.IP_ADDRESS';

	
	const WAKTU_ACCESS = 'ebudget.november3_rincian.WAKTU_ACCESS';

	
	const TARGET = 'ebudget.november3_rincian.TARGET';

	
	const UNIT_ID = 'ebudget.november3_rincian.UNIT_ID';

	
	const RINCIAN_LEVEL = 'ebudget.november3_rincian.RINCIAN_LEVEL';

	
	const LOCK = 'ebudget.november3_rincian.LOCK';

	
	const LAST_UPDATE_USER = 'ebudget.november3_rincian.LAST_UPDATE_USER';

	
	const LAST_UPDATE_TIME = 'ebudget.november3_rincian.LAST_UPDATE_TIME';

	
	const LAST_UPDATE_IP = 'ebudget.november3_rincian.LAST_UPDATE_IP';

	
	const TAHAP = 'ebudget.november3_rincian.TAHAP';

	
	const TAHUN = 'ebudget.november3_rincian.TAHUN';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('KegiatanCode', 'Tipe', 'RincianConfirmed', 'RincianChanged', 'RincianSelesai', 'IpAddress', 'WaktuAccess', 'Target', 'UnitId', 'RincianLevel', 'Lock', 'LastUpdateUser', 'LastUpdateTime', 'LastUpdateIp', 'Tahap', 'Tahun', ),
		BasePeer::TYPE_COLNAME => array (November3RincianPeer::KEGIATAN_CODE, November3RincianPeer::TIPE, November3RincianPeer::RINCIAN_CONFIRMED, November3RincianPeer::RINCIAN_CHANGED, November3RincianPeer::RINCIAN_SELESAI, November3RincianPeer::IP_ADDRESS, November3RincianPeer::WAKTU_ACCESS, November3RincianPeer::TARGET, November3RincianPeer::UNIT_ID, November3RincianPeer::RINCIAN_LEVEL, November3RincianPeer::LOCK, November3RincianPeer::LAST_UPDATE_USER, November3RincianPeer::LAST_UPDATE_TIME, November3RincianPeer::LAST_UPDATE_IP, November3RincianPeer::TAHAP, November3RincianPeer::TAHUN, ),
		BasePeer::TYPE_FIELDNAME => array ('kegiatan_code', 'tipe', 'rincian_confirmed', 'rincian_changed', 'rincian_selesai', 'ip_address', 'waktu_access', 'target', 'unit_id', 'rincian_level', 'lock', 'last_update_user', 'last_update_time', 'last_update_ip', 'tahap', 'tahun', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('KegiatanCode' => 0, 'Tipe' => 1, 'RincianConfirmed' => 2, 'RincianChanged' => 3, 'RincianSelesai' => 4, 'IpAddress' => 5, 'WaktuAccess' => 6, 'Target' => 7, 'UnitId' => 8, 'RincianLevel' => 9, 'Lock' => 10, 'LastUpdateUser' => 11, 'LastUpdateTime' => 12, 'LastUpdateIp' => 13, 'Tahap' => 14, 'Tahun' => 15, ),
		BasePeer::TYPE_COLNAME => array (November3RincianPeer::KEGIATAN_CODE => 0, November3RincianPeer::TIPE => 1, November3RincianPeer::RINCIAN_CONFIRMED => 2, November3RincianPeer::RINCIAN_CHANGED => 3, November3RincianPeer::RINCIAN_SELESAI => 4, November3RincianPeer::IP_ADDRESS => 5, November3RincianPeer::WAKTU_ACCESS => 6, November3RincianPeer::TARGET => 7, November3RincianPeer::UNIT_ID => 8, November3RincianPeer::RINCIAN_LEVEL => 9, November3RincianPeer::LOCK => 10, November3RincianPeer::LAST_UPDATE_USER => 11, November3RincianPeer::LAST_UPDATE_TIME => 12, November3RincianPeer::LAST_UPDATE_IP => 13, November3RincianPeer::TAHAP => 14, November3RincianPeer::TAHUN => 15, ),
		BasePeer::TYPE_FIELDNAME => array ('kegiatan_code' => 0, 'tipe' => 1, 'rincian_confirmed' => 2, 'rincian_changed' => 3, 'rincian_selesai' => 4, 'ip_address' => 5, 'waktu_access' => 6, 'target' => 7, 'unit_id' => 8, 'rincian_level' => 9, 'lock' => 10, 'last_update_user' => 11, 'last_update_time' => 12, 'last_update_ip' => 13, 'tahap' => 14, 'tahun' => 15, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/November3RincianMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.November3RincianMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = November3RincianPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(November3RincianPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(November3RincianPeer::KEGIATAN_CODE);

		$criteria->addSelectColumn(November3RincianPeer::TIPE);

		$criteria->addSelectColumn(November3RincianPeer::RINCIAN_CONFIRMED);

		$criteria->addSelectColumn(November3RincianPeer::RINCIAN_CHANGED);

		$criteria->addSelectColumn(November3RincianPeer::RINCIAN_SELESAI);

		$criteria->addSelectColumn(November3RincianPeer::IP_ADDRESS);

		$criteria->addSelectColumn(November3RincianPeer::WAKTU_ACCESS);

		$criteria->addSelectColumn(November3RincianPeer::TARGET);

		$criteria->addSelectColumn(November3RincianPeer::UNIT_ID);

		$criteria->addSelectColumn(November3RincianPeer::RINCIAN_LEVEL);

		$criteria->addSelectColumn(November3RincianPeer::LOCK);

		$criteria->addSelectColumn(November3RincianPeer::LAST_UPDATE_USER);

		$criteria->addSelectColumn(November3RincianPeer::LAST_UPDATE_TIME);

		$criteria->addSelectColumn(November3RincianPeer::LAST_UPDATE_IP);

		$criteria->addSelectColumn(November3RincianPeer::TAHAP);

		$criteria->addSelectColumn(November3RincianPeer::TAHUN);

	}

	const COUNT = 'COUNT(ebudget.november3_rincian.KEGIATAN_CODE)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.november3_rincian.KEGIATAN_CODE)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(November3RincianPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(November3RincianPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = November3RincianPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = November3RincianPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return November3RincianPeer::populateObjects(November3RincianPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			November3RincianPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = November3RincianPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return November3RincianPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(November3RincianPeer::KEGIATAN_CODE);
			$selectCriteria->add(November3RincianPeer::KEGIATAN_CODE, $criteria->remove(November3RincianPeer::KEGIATAN_CODE), $comparison);

			$comparison = $criteria->getComparison(November3RincianPeer::TIPE);
			$selectCriteria->add(November3RincianPeer::TIPE, $criteria->remove(November3RincianPeer::TIPE), $comparison);

			$comparison = $criteria->getComparison(November3RincianPeer::UNIT_ID);
			$selectCriteria->add(November3RincianPeer::UNIT_ID, $criteria->remove(November3RincianPeer::UNIT_ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(November3RincianPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(November3RincianPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof November3Rincian) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
												if(count($values) == count($values, COUNT_RECURSIVE))
			{
								$values = array($values);
			}
			$vals = array();
			foreach($values as $value)
			{

				$vals[0][] = $value[0];
				$vals[1][] = $value[1];
				$vals[2][] = $value[2];
			}

			$criteria->add(November3RincianPeer::KEGIATAN_CODE, $vals[0], Criteria::IN);
			$criteria->add(November3RincianPeer::TIPE, $vals[1], Criteria::IN);
			$criteria->add(November3RincianPeer::UNIT_ID, $vals[2], Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(November3Rincian $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(November3RincianPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(November3RincianPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(November3RincianPeer::DATABASE_NAME, November3RincianPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = November3RincianPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK( $kegiatan_code, $tipe, $unit_id, $con = null) {
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$criteria = new Criteria();
		$criteria->add(November3RincianPeer::KEGIATAN_CODE, $kegiatan_code);
		$criteria->add(November3RincianPeer::TIPE, $tipe);
		$criteria->add(November3RincianPeer::UNIT_ID, $unit_id);
		$v = November3RincianPeer::doSelect($criteria, $con);

		return !empty($v) ? $v[0] : null;
	}
} 
if (Propel::isInit()) {
			try {
		BaseNovember3RincianPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/November3RincianMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.November3RincianMapBuilder');
}
