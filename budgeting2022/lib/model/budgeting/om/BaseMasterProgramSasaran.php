<?php


abstract class BaseMasterProgramSasaran extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $kode_sasaran_indikator;


	
	protected $kode_program_indikator;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getKodeSasaranIndikator()
	{

		return $this->kode_sasaran_indikator;
	}

	
	public function getKodeProgramIndikator()
	{

		return $this->kode_program_indikator;
	}

	
	public function setKodeSasaranIndikator($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_sasaran_indikator !== $v) {
			$this->kode_sasaran_indikator = $v;
			$this->modifiedColumns[] = MasterProgramSasaranPeer::KODE_SASARAN_INDIKATOR;
		}

	} 
	
	public function setKodeProgramIndikator($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_program_indikator !== $v) {
			$this->kode_program_indikator = $v;
			$this->modifiedColumns[] = MasterProgramSasaranPeer::KODE_PROGRAM_INDIKATOR;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->kode_sasaran_indikator = $rs->getString($startcol + 0);

			$this->kode_program_indikator = $rs->getString($startcol + 1);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 2; 
		} catch (Exception $e) {
			throw new PropelException("Error populating MasterProgramSasaran object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(MasterProgramSasaranPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			MasterProgramSasaranPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(MasterProgramSasaranPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = MasterProgramSasaranPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += MasterProgramSasaranPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = MasterProgramSasaranPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = MasterProgramSasaranPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getKodeSasaranIndikator();
				break;
			case 1:
				return $this->getKodeProgramIndikator();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = MasterProgramSasaranPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getKodeSasaranIndikator(),
			$keys[1] => $this->getKodeProgramIndikator(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = MasterProgramSasaranPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setKodeSasaranIndikator($value);
				break;
			case 1:
				$this->setKodeProgramIndikator($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = MasterProgramSasaranPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setKodeSasaranIndikator($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setKodeProgramIndikator($arr[$keys[1]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(MasterProgramSasaranPeer::DATABASE_NAME);

		if ($this->isColumnModified(MasterProgramSasaranPeer::KODE_SASARAN_INDIKATOR)) $criteria->add(MasterProgramSasaranPeer::KODE_SASARAN_INDIKATOR, $this->kode_sasaran_indikator);
		if ($this->isColumnModified(MasterProgramSasaranPeer::KODE_PROGRAM_INDIKATOR)) $criteria->add(MasterProgramSasaranPeer::KODE_PROGRAM_INDIKATOR, $this->kode_program_indikator);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(MasterProgramSasaranPeer::DATABASE_NAME);

		$criteria->add(MasterProgramSasaranPeer::KODE_SASARAN_INDIKATOR, $this->kode_sasaran_indikator);
		$criteria->add(MasterProgramSasaranPeer::KODE_PROGRAM_INDIKATOR, $this->kode_program_indikator);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		$pks = array();

		$pks[0] = $this->getKodeSasaranIndikator();

		$pks[1] = $this->getKodeProgramIndikator();

		return $pks;
	}

	
	public function setPrimaryKey($keys)
	{

		$this->setKodeSasaranIndikator($keys[0]);

		$this->setKodeProgramIndikator($keys[1]);

	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{


		$copyObj->setNew(true);

		$copyObj->setKodeSasaranIndikator(NULL); 
		$copyObj->setKodeProgramIndikator(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new MasterProgramSasaranPeer();
		}
		return self::$peer;
	}

} 