<?php


abstract class BaseDeskripsiResumeRapatKegiatanPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.deskripsi_resume_rapat_kegiatan';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.DeskripsiResumeRapatKegiatan';

	
	const NUM_COLUMNS = 4;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const ID_DESKRIPSI_RESUME_RAPAT = 'ebudget.deskripsi_resume_rapat_kegiatan.ID_DESKRIPSI_RESUME_RAPAT';

	
	const KEGIATAN_CODE = 'ebudget.deskripsi_resume_rapat_kegiatan.KEGIATAN_CODE';

	
	const KEGIATAN_NAME = 'ebudget.deskripsi_resume_rapat_kegiatan.KEGIATAN_NAME';

	
	const CATATAN_PEMBAHASAN = 'ebudget.deskripsi_resume_rapat_kegiatan.CATATAN_PEMBAHASAN';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('IdDeskripsiResumeRapat', 'KegiatanCode', 'KegiatanName', 'CatatanPembahasan', ),
		BasePeer::TYPE_COLNAME => array (DeskripsiResumeRapatKegiatanPeer::ID_DESKRIPSI_RESUME_RAPAT, DeskripsiResumeRapatKegiatanPeer::KEGIATAN_CODE, DeskripsiResumeRapatKegiatanPeer::KEGIATAN_NAME, DeskripsiResumeRapatKegiatanPeer::CATATAN_PEMBAHASAN, ),
		BasePeer::TYPE_FIELDNAME => array ('id_deskripsi_resume_rapat', 'kegiatan_code', 'kegiatan_name', 'catatan_pembahasan', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('IdDeskripsiResumeRapat' => 0, 'KegiatanCode' => 1, 'KegiatanName' => 2, 'CatatanPembahasan' => 3, ),
		BasePeer::TYPE_COLNAME => array (DeskripsiResumeRapatKegiatanPeer::ID_DESKRIPSI_RESUME_RAPAT => 0, DeskripsiResumeRapatKegiatanPeer::KEGIATAN_CODE => 1, DeskripsiResumeRapatKegiatanPeer::KEGIATAN_NAME => 2, DeskripsiResumeRapatKegiatanPeer::CATATAN_PEMBAHASAN => 3, ),
		BasePeer::TYPE_FIELDNAME => array ('id_deskripsi_resume_rapat' => 0, 'kegiatan_code' => 1, 'kegiatan_name' => 2, 'catatan_pembahasan' => 3, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/DeskripsiResumeRapatKegiatanMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.DeskripsiResumeRapatKegiatanMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = DeskripsiResumeRapatKegiatanPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(DeskripsiResumeRapatKegiatanPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(DeskripsiResumeRapatKegiatanPeer::ID_DESKRIPSI_RESUME_RAPAT);

		$criteria->addSelectColumn(DeskripsiResumeRapatKegiatanPeer::KEGIATAN_CODE);

		$criteria->addSelectColumn(DeskripsiResumeRapatKegiatanPeer::KEGIATAN_NAME);

		$criteria->addSelectColumn(DeskripsiResumeRapatKegiatanPeer::CATATAN_PEMBAHASAN);

	}

	const COUNT = 'COUNT(ebudget.deskripsi_resume_rapat_kegiatan.ID_DESKRIPSI_RESUME_RAPAT)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.deskripsi_resume_rapat_kegiatan.ID_DESKRIPSI_RESUME_RAPAT)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(DeskripsiResumeRapatKegiatanPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(DeskripsiResumeRapatKegiatanPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = DeskripsiResumeRapatKegiatanPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = DeskripsiResumeRapatKegiatanPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return DeskripsiResumeRapatKegiatanPeer::populateObjects(DeskripsiResumeRapatKegiatanPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			DeskripsiResumeRapatKegiatanPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = DeskripsiResumeRapatKegiatanPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return DeskripsiResumeRapatKegiatanPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(DeskripsiResumeRapatKegiatanPeer::ID_DESKRIPSI_RESUME_RAPAT);
			$selectCriteria->add(DeskripsiResumeRapatKegiatanPeer::ID_DESKRIPSI_RESUME_RAPAT, $criteria->remove(DeskripsiResumeRapatKegiatanPeer::ID_DESKRIPSI_RESUME_RAPAT), $comparison);

			$comparison = $criteria->getComparison(DeskripsiResumeRapatKegiatanPeer::KEGIATAN_CODE);
			$selectCriteria->add(DeskripsiResumeRapatKegiatanPeer::KEGIATAN_CODE, $criteria->remove(DeskripsiResumeRapatKegiatanPeer::KEGIATAN_CODE), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(DeskripsiResumeRapatKegiatanPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(DeskripsiResumeRapatKegiatanPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof DeskripsiResumeRapatKegiatan) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
												if(count($values) == count($values, COUNT_RECURSIVE))
			{
								$values = array($values);
			}
			$vals = array();
			foreach($values as $value)
			{

				$vals[0][] = $value[0];
				$vals[1][] = $value[1];
			}

			$criteria->add(DeskripsiResumeRapatKegiatanPeer::ID_DESKRIPSI_RESUME_RAPAT, $vals[0], Criteria::IN);
			$criteria->add(DeskripsiResumeRapatKegiatanPeer::KEGIATAN_CODE, $vals[1], Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(DeskripsiResumeRapatKegiatan $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(DeskripsiResumeRapatKegiatanPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(DeskripsiResumeRapatKegiatanPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(DeskripsiResumeRapatKegiatanPeer::DATABASE_NAME, DeskripsiResumeRapatKegiatanPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = DeskripsiResumeRapatKegiatanPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK( $id_deskripsi_resume_rapat, $kegiatan_code, $con = null) {
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$criteria = new Criteria();
		$criteria->add(DeskripsiResumeRapatKegiatanPeer::ID_DESKRIPSI_RESUME_RAPAT, $id_deskripsi_resume_rapat);
		$criteria->add(DeskripsiResumeRapatKegiatanPeer::KEGIATAN_CODE, $kegiatan_code);
		$v = DeskripsiResumeRapatKegiatanPeer::doSelect($criteria, $con);

		return !empty($v) ? $v[0] : null;
	}
} 
if (Propel::isInit()) {
			try {
		BaseDeskripsiResumeRapatKegiatanPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/DeskripsiResumeRapatKegiatanMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.DeskripsiResumeRapatKegiatanMapBuilder');
}
