<?php


abstract class BaseRevisi3RkaMemberPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.revisi3_rka_member';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.Revisi3RkaMember';

	
	const NUM_COLUMNS = 9;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const KODE_SUB = 'ebudget.revisi3_rka_member.KODE_SUB';

	
	const UNIT_ID = 'ebudget.revisi3_rka_member.UNIT_ID';

	
	const KEGIATAN_CODE = 'ebudget.revisi3_rka_member.KEGIATAN_CODE';

	
	const KOMPONEN_ID = 'ebudget.revisi3_rka_member.KOMPONEN_ID';

	
	const KOMPONEN_NAME = 'ebudget.revisi3_rka_member.KOMPONEN_NAME';

	
	const DETAIL_NAME = 'ebudget.revisi3_rka_member.DETAIL_NAME';

	
	const REKENING_ASLI = 'ebudget.revisi3_rka_member.REKENING_ASLI';

	
	const TAHUN = 'ebudget.revisi3_rka_member.TAHUN';

	
	const DETAIL_NO = 'ebudget.revisi3_rka_member.DETAIL_NO';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('KodeSub', 'UnitId', 'KegiatanCode', 'KomponenId', 'KomponenName', 'DetailName', 'RekeningAsli', 'Tahun', 'DetailNo', ),
		BasePeer::TYPE_COLNAME => array (Revisi3RkaMemberPeer::KODE_SUB, Revisi3RkaMemberPeer::UNIT_ID, Revisi3RkaMemberPeer::KEGIATAN_CODE, Revisi3RkaMemberPeer::KOMPONEN_ID, Revisi3RkaMemberPeer::KOMPONEN_NAME, Revisi3RkaMemberPeer::DETAIL_NAME, Revisi3RkaMemberPeer::REKENING_ASLI, Revisi3RkaMemberPeer::TAHUN, Revisi3RkaMemberPeer::DETAIL_NO, ),
		BasePeer::TYPE_FIELDNAME => array ('kode_sub', 'unit_id', 'kegiatan_code', 'komponen_id', 'komponen_name', 'detail_name', 'rekening_asli', 'tahun', 'detail_no', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('KodeSub' => 0, 'UnitId' => 1, 'KegiatanCode' => 2, 'KomponenId' => 3, 'KomponenName' => 4, 'DetailName' => 5, 'RekeningAsli' => 6, 'Tahun' => 7, 'DetailNo' => 8, ),
		BasePeer::TYPE_COLNAME => array (Revisi3RkaMemberPeer::KODE_SUB => 0, Revisi3RkaMemberPeer::UNIT_ID => 1, Revisi3RkaMemberPeer::KEGIATAN_CODE => 2, Revisi3RkaMemberPeer::KOMPONEN_ID => 3, Revisi3RkaMemberPeer::KOMPONEN_NAME => 4, Revisi3RkaMemberPeer::DETAIL_NAME => 5, Revisi3RkaMemberPeer::REKENING_ASLI => 6, Revisi3RkaMemberPeer::TAHUN => 7, Revisi3RkaMemberPeer::DETAIL_NO => 8, ),
		BasePeer::TYPE_FIELDNAME => array ('kode_sub' => 0, 'unit_id' => 1, 'kegiatan_code' => 2, 'komponen_id' => 3, 'komponen_name' => 4, 'detail_name' => 5, 'rekening_asli' => 6, 'tahun' => 7, 'detail_no' => 8, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/Revisi3RkaMemberMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.Revisi3RkaMemberMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = Revisi3RkaMemberPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(Revisi3RkaMemberPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(Revisi3RkaMemberPeer::KODE_SUB);

		$criteria->addSelectColumn(Revisi3RkaMemberPeer::UNIT_ID);

		$criteria->addSelectColumn(Revisi3RkaMemberPeer::KEGIATAN_CODE);

		$criteria->addSelectColumn(Revisi3RkaMemberPeer::KOMPONEN_ID);

		$criteria->addSelectColumn(Revisi3RkaMemberPeer::KOMPONEN_NAME);

		$criteria->addSelectColumn(Revisi3RkaMemberPeer::DETAIL_NAME);

		$criteria->addSelectColumn(Revisi3RkaMemberPeer::REKENING_ASLI);

		$criteria->addSelectColumn(Revisi3RkaMemberPeer::TAHUN);

		$criteria->addSelectColumn(Revisi3RkaMemberPeer::DETAIL_NO);

	}

	const COUNT = 'COUNT(ebudget.revisi3_rka_member.KODE_SUB)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.revisi3_rka_member.KODE_SUB)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(Revisi3RkaMemberPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(Revisi3RkaMemberPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = Revisi3RkaMemberPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = Revisi3RkaMemberPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return Revisi3RkaMemberPeer::populateObjects(Revisi3RkaMemberPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			Revisi3RkaMemberPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = Revisi3RkaMemberPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return Revisi3RkaMemberPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(Revisi3RkaMemberPeer::KODE_SUB);
			$selectCriteria->add(Revisi3RkaMemberPeer::KODE_SUB, $criteria->remove(Revisi3RkaMemberPeer::KODE_SUB), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(Revisi3RkaMemberPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(Revisi3RkaMemberPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof Revisi3RkaMember) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(Revisi3RkaMemberPeer::KODE_SUB, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(Revisi3RkaMember $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(Revisi3RkaMemberPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(Revisi3RkaMemberPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(Revisi3RkaMemberPeer::DATABASE_NAME, Revisi3RkaMemberPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = Revisi3RkaMemberPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(Revisi3RkaMemberPeer::DATABASE_NAME);

		$criteria->add(Revisi3RkaMemberPeer::KODE_SUB, $pk);


		$v = Revisi3RkaMemberPeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(Revisi3RkaMemberPeer::KODE_SUB, $pks, Criteria::IN);
			$objs = Revisi3RkaMemberPeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BaseRevisi3RkaMemberPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/Revisi3RkaMemberMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.Revisi3RkaMemberMapBuilder');
}
