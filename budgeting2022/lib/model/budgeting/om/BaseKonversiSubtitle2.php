<?php


abstract class BaseKonversiSubtitle2 extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $unit_id_lama;


	
	protected $kegiatan_code_lama;


	
	protected $subtitle_lama;


	
	protected $unit_id_baru;


	
	protected $kegiatan_code_baru;


	
	protected $subtitle_baru;


	
	protected $masuk;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getUnitIdLama()
	{

		return $this->unit_id_lama;
	}

	
	public function getKegiatanCodeLama()
	{

		return $this->kegiatan_code_lama;
	}

	
	public function getSubtitleLama()
	{

		return $this->subtitle_lama;
	}

	
	public function getUnitIdBaru()
	{

		return $this->unit_id_baru;
	}

	
	public function getKegiatanCodeBaru()
	{

		return $this->kegiatan_code_baru;
	}

	
	public function getSubtitleBaru()
	{

		return $this->subtitle_baru;
	}

	
	public function getMasuk()
	{

		return $this->masuk;
	}

	
	public function setUnitIdLama($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->unit_id_lama !== $v) {
			$this->unit_id_lama = $v;
			$this->modifiedColumns[] = KonversiSubtitle2Peer::UNIT_ID_LAMA;
		}

	} 
	
	public function setKegiatanCodeLama($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kegiatan_code_lama !== $v) {
			$this->kegiatan_code_lama = $v;
			$this->modifiedColumns[] = KonversiSubtitle2Peer::KEGIATAN_CODE_LAMA;
		}

	} 
	
	public function setSubtitleLama($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->subtitle_lama !== $v) {
			$this->subtitle_lama = $v;
			$this->modifiedColumns[] = KonversiSubtitle2Peer::SUBTITLE_LAMA;
		}

	} 
	
	public function setUnitIdBaru($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->unit_id_baru !== $v) {
			$this->unit_id_baru = $v;
			$this->modifiedColumns[] = KonversiSubtitle2Peer::UNIT_ID_BARU;
		}

	} 
	
	public function setKegiatanCodeBaru($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kegiatan_code_baru !== $v) {
			$this->kegiatan_code_baru = $v;
			$this->modifiedColumns[] = KonversiSubtitle2Peer::KEGIATAN_CODE_BARU;
		}

	} 
	
	public function setSubtitleBaru($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->subtitle_baru !== $v) {
			$this->subtitle_baru = $v;
			$this->modifiedColumns[] = KonversiSubtitle2Peer::SUBTITLE_BARU;
		}

	} 
	
	public function setMasuk($v)
	{

		if ($this->masuk !== $v) {
			$this->masuk = $v;
			$this->modifiedColumns[] = KonversiSubtitle2Peer::MASUK;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->unit_id_lama = $rs->getString($startcol + 0);

			$this->kegiatan_code_lama = $rs->getString($startcol + 1);

			$this->subtitle_lama = $rs->getString($startcol + 2);

			$this->unit_id_baru = $rs->getString($startcol + 3);

			$this->kegiatan_code_baru = $rs->getString($startcol + 4);

			$this->subtitle_baru = $rs->getString($startcol + 5);

			$this->masuk = $rs->getBoolean($startcol + 6);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 7; 
		} catch (Exception $e) {
			throw new PropelException("Error populating KonversiSubtitle2 object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(KonversiSubtitle2Peer::DATABASE_NAME);
		}

		try {
			$con->begin();
			KonversiSubtitle2Peer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(KonversiSubtitle2Peer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = KonversiSubtitle2Peer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += KonversiSubtitle2Peer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = KonversiSubtitle2Peer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = KonversiSubtitle2Peer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getUnitIdLama();
				break;
			case 1:
				return $this->getKegiatanCodeLama();
				break;
			case 2:
				return $this->getSubtitleLama();
				break;
			case 3:
				return $this->getUnitIdBaru();
				break;
			case 4:
				return $this->getKegiatanCodeBaru();
				break;
			case 5:
				return $this->getSubtitleBaru();
				break;
			case 6:
				return $this->getMasuk();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = KonversiSubtitle2Peer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getUnitIdLama(),
			$keys[1] => $this->getKegiatanCodeLama(),
			$keys[2] => $this->getSubtitleLama(),
			$keys[3] => $this->getUnitIdBaru(),
			$keys[4] => $this->getKegiatanCodeBaru(),
			$keys[5] => $this->getSubtitleBaru(),
			$keys[6] => $this->getMasuk(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = KonversiSubtitle2Peer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setUnitIdLama($value);
				break;
			case 1:
				$this->setKegiatanCodeLama($value);
				break;
			case 2:
				$this->setSubtitleLama($value);
				break;
			case 3:
				$this->setUnitIdBaru($value);
				break;
			case 4:
				$this->setKegiatanCodeBaru($value);
				break;
			case 5:
				$this->setSubtitleBaru($value);
				break;
			case 6:
				$this->setMasuk($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = KonversiSubtitle2Peer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setUnitIdLama($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setKegiatanCodeLama($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setSubtitleLama($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setUnitIdBaru($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setKegiatanCodeBaru($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setSubtitleBaru($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setMasuk($arr[$keys[6]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(KonversiSubtitle2Peer::DATABASE_NAME);

		if ($this->isColumnModified(KonversiSubtitle2Peer::UNIT_ID_LAMA)) $criteria->add(KonversiSubtitle2Peer::UNIT_ID_LAMA, $this->unit_id_lama);
		if ($this->isColumnModified(KonversiSubtitle2Peer::KEGIATAN_CODE_LAMA)) $criteria->add(KonversiSubtitle2Peer::KEGIATAN_CODE_LAMA, $this->kegiatan_code_lama);
		if ($this->isColumnModified(KonversiSubtitle2Peer::SUBTITLE_LAMA)) $criteria->add(KonversiSubtitle2Peer::SUBTITLE_LAMA, $this->subtitle_lama);
		if ($this->isColumnModified(KonversiSubtitle2Peer::UNIT_ID_BARU)) $criteria->add(KonversiSubtitle2Peer::UNIT_ID_BARU, $this->unit_id_baru);
		if ($this->isColumnModified(KonversiSubtitle2Peer::KEGIATAN_CODE_BARU)) $criteria->add(KonversiSubtitle2Peer::KEGIATAN_CODE_BARU, $this->kegiatan_code_baru);
		if ($this->isColumnModified(KonversiSubtitle2Peer::SUBTITLE_BARU)) $criteria->add(KonversiSubtitle2Peer::SUBTITLE_BARU, $this->subtitle_baru);
		if ($this->isColumnModified(KonversiSubtitle2Peer::MASUK)) $criteria->add(KonversiSubtitle2Peer::MASUK, $this->masuk);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(KonversiSubtitle2Peer::DATABASE_NAME);


		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return null;
	}

	
	 public function setPrimaryKey($pk)
	 {
		 	 }

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setUnitIdLama($this->unit_id_lama);

		$copyObj->setKegiatanCodeLama($this->kegiatan_code_lama);

		$copyObj->setSubtitleLama($this->subtitle_lama);

		$copyObj->setUnitIdBaru($this->unit_id_baru);

		$copyObj->setKegiatanCodeBaru($this->kegiatan_code_baru);

		$copyObj->setSubtitleBaru($this->subtitle_baru);

		$copyObj->setMasuk($this->masuk);


		$copyObj->setNew(true);

	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new KonversiSubtitle2Peer();
		}
		return self::$peer;
	}

} 