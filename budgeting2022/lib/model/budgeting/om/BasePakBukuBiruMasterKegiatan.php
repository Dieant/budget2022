<?php


abstract class BasePakBukuBiruMasterKegiatan extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $unit_id;


	
	protected $kode_kegiatan;


	
	protected $kode_kegiatan_baru;


	
	protected $kode_bidang;


	
	protected $kode_urusan_wajib;


	
	protected $kode_program;


	
	protected $kode_sasaran;


	
	protected $kode_indikator;


	
	protected $alokasi_dana;


	
	protected $nama_kegiatan;


	
	protected $masukan;


	
	protected $output;


	
	protected $outcome;


	
	protected $benefit;


	
	protected $impact;


	
	protected $tipe;


	
	protected $kegiatan_active = false;


	
	protected $to_kegiatan_code;


	
	protected $catatan;


	
	protected $target_outcome;


	
	protected $lokasi;


	
	protected $jumlah_prev;


	
	protected $jumlah_now;


	
	protected $jumlah_next;


	
	protected $kode_program2;


	
	protected $kode_urusan;


	
	protected $last_update_user;


	
	protected $last_update_time;


	
	protected $last_update_ip;


	
	protected $tahap;


	
	protected $kode_misi;


	
	protected $kode_tujuan;


	
	protected $ranking;


	
	protected $nomor13;


	
	protected $ppa_nama;


	
	protected $ppa_pangkat;


	
	protected $ppa_nip;


	
	protected $lanjutan;


	
	protected $user_id;


	
	protected $id;


	
	protected $tahun;


	
	protected $tambahan_pagu;


	
	protected $gender = false;


	
	protected $kode_keg_keuangan;


	
	protected $user_id_lama;


	
	protected $indikator;


	
	protected $is_dak;


	
	protected $kode_kegiatan_asal;


	
	protected $kode_keg_keuangan_asal;


	
	protected $th_ke_multiyears;


	
	protected $kelompok_sasaran;


	
	protected $pagu_bappeko;


	
	protected $kode_dpa;


	
	protected $user_id_pptk;


	
	protected $user_id_kpa;


	
	protected $catatan_pembahasan;


	
	protected $catatan_penyelia;


	
	protected $catatan_bappeko;


	
	protected $status_level;


	
	protected $is_tapd_setuju = false;


	
	protected $is_bappeko_setuju = false;


	
	protected $is_penyelia_setuju = false;


	
	protected $is_pernah_rka = false;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getUnitId()
	{

		return $this->unit_id;
	}

	
	public function getKodeKegiatan()
	{

		return $this->kode_kegiatan;
	}

	
	public function getKodeKegiatanBaru()
	{

		return $this->kode_kegiatan_baru;
	}

	
	public function getKodeBidang()
	{

		return $this->kode_bidang;
	}

	
	public function getKodeUrusanWajib()
	{

		return $this->kode_urusan_wajib;
	}

	
	public function getKodeProgram()
	{

		return $this->kode_program;
	}

	
	public function getKodeSasaran()
	{

		return $this->kode_sasaran;
	}

	
	public function getKodeIndikator()
	{

		return $this->kode_indikator;
	}

	
	public function getAlokasiDana()
	{

		return $this->alokasi_dana;
	}

	
	public function getNamaKegiatan()
	{

		return $this->nama_kegiatan;
	}

	
	public function getMasukan()
	{

		return $this->masukan;
	}

	
	public function getOutput()
	{

		return $this->output;
	}

	
	public function getOutcome()
	{

		return $this->outcome;
	}

	
	public function getBenefit()
	{

		return $this->benefit;
	}

	
	public function getImpact()
	{

		return $this->impact;
	}

	
	public function getTipe()
	{

		return $this->tipe;
	}

	
	public function getKegiatanActive()
	{

		return $this->kegiatan_active;
	}

	
	public function getToKegiatanCode()
	{

		return $this->to_kegiatan_code;
	}

	
	public function getCatatan()
	{

		return $this->catatan;
	}

	
	public function getTargetOutcome()
	{

		return $this->target_outcome;
	}

	
	public function getLokasi()
	{

		return $this->lokasi;
	}

	
	public function getJumlahPrev()
	{

		return $this->jumlah_prev;
	}

	
	public function getJumlahNow()
	{

		return $this->jumlah_now;
	}

	
	public function getJumlahNext()
	{

		return $this->jumlah_next;
	}

	
	public function getKodeProgram2()
	{

		return $this->kode_program2;
	}

	
	public function getKodeUrusan()
	{

		return $this->kode_urusan;
	}

	
	public function getLastUpdateUser()
	{

		return $this->last_update_user;
	}

	
	public function getLastUpdateTime($format = 'Y-m-d H:i:s')
	{

		if ($this->last_update_time === null || $this->last_update_time === '') {
			return null;
		} elseif (!is_int($this->last_update_time)) {
						$ts = strtotime($this->last_update_time);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [last_update_time] as date/time value: " . var_export($this->last_update_time, true));
			}
		} else {
			$ts = $this->last_update_time;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getLastUpdateIp()
	{

		return $this->last_update_ip;
	}

	
	public function getTahap()
	{

		return $this->tahap;
	}

	
	public function getKodeMisi()
	{

		return $this->kode_misi;
	}

	
	public function getKodeTujuan()
	{

		return $this->kode_tujuan;
	}

	
	public function getRanking()
	{

		return $this->ranking;
	}

	
	public function getNomor13()
	{

		return $this->nomor13;
	}

	
	public function getPpaNama()
	{

		return $this->ppa_nama;
	}

	
	public function getPpaPangkat()
	{

		return $this->ppa_pangkat;
	}

	
	public function getPpaNip()
	{

		return $this->ppa_nip;
	}

	
	public function getLanjutan()
	{

		return $this->lanjutan;
	}

	
	public function getUserId()
	{

		return $this->user_id;
	}

	
	public function getId()
	{

		return $this->id;
	}

	
	public function getTahun()
	{

		return $this->tahun;
	}

	
	public function getTambahanPagu()
	{

		return $this->tambahan_pagu;
	}

	
	public function getGender()
	{

		return $this->gender;
	}

	
	public function getKodeKegKeuangan()
	{

		return $this->kode_keg_keuangan;
	}

	
	public function getUserIdLama()
	{

		return $this->user_id_lama;
	}

	
	public function getIndikator()
	{

		return $this->indikator;
	}

	
	public function getIsDak()
	{

		return $this->is_dak;
	}

	
	public function getKodeKegiatanAsal()
	{

		return $this->kode_kegiatan_asal;
	}

	
	public function getKodeKegKeuanganAsal()
	{

		return $this->kode_keg_keuangan_asal;
	}

	
	public function getThKeMultiyears()
	{

		return $this->th_ke_multiyears;
	}

	
	public function getKelompokSasaran()
	{

		return $this->kelompok_sasaran;
	}

	
	public function getPaguBappeko()
	{

		return $this->pagu_bappeko;
	}

	
	public function getKodeDpa()
	{

		return $this->kode_dpa;
	}

	
	public function getUserIdPptk()
	{

		return $this->user_id_pptk;
	}

	
	public function getUserIdKpa()
	{

		return $this->user_id_kpa;
	}

	
	public function getCatatanPembahasan()
	{

		return $this->catatan_pembahasan;
	}

	
	public function getCatatanPenyelia()
	{

		return $this->catatan_penyelia;
	}

	
	public function getCatatanBappeko()
	{

		return $this->catatan_bappeko;
	}

	
	public function getStatusLevel()
	{

		return $this->status_level;
	}

	
	public function getIsTapdSetuju()
	{

		return $this->is_tapd_setuju;
	}

	
	public function getIsBappekoSetuju()
	{

		return $this->is_bappeko_setuju;
	}

	
	public function getIsPenyeliaSetuju()
	{

		return $this->is_penyelia_setuju;
	}

	
	public function getIsPernahRka()
	{

		return $this->is_pernah_rka;
	}

	
	public function setUnitId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->unit_id !== $v) {
			$this->unit_id = $v;
			$this->modifiedColumns[] = PakBukuBiruMasterKegiatanPeer::UNIT_ID;
		}

	} 
	
	public function setKodeKegiatan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_kegiatan !== $v) {
			$this->kode_kegiatan = $v;
			$this->modifiedColumns[] = PakBukuBiruMasterKegiatanPeer::KODE_KEGIATAN;
		}

	} 
	
	public function setKodeKegiatanBaru($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_kegiatan_baru !== $v) {
			$this->kode_kegiatan_baru = $v;
			$this->modifiedColumns[] = PakBukuBiruMasterKegiatanPeer::KODE_KEGIATAN_BARU;
		}

	} 
	
	public function setKodeBidang($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_bidang !== $v) {
			$this->kode_bidang = $v;
			$this->modifiedColumns[] = PakBukuBiruMasterKegiatanPeer::KODE_BIDANG;
		}

	} 
	
	public function setKodeUrusanWajib($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_urusan_wajib !== $v) {
			$this->kode_urusan_wajib = $v;
			$this->modifiedColumns[] = PakBukuBiruMasterKegiatanPeer::KODE_URUSAN_WAJIB;
		}

	} 
	
	public function setKodeProgram($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_program !== $v) {
			$this->kode_program = $v;
			$this->modifiedColumns[] = PakBukuBiruMasterKegiatanPeer::KODE_PROGRAM;
		}

	} 
	
	public function setKodeSasaran($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_sasaran !== $v) {
			$this->kode_sasaran = $v;
			$this->modifiedColumns[] = PakBukuBiruMasterKegiatanPeer::KODE_SASARAN;
		}

	} 
	
	public function setKodeIndikator($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_indikator !== $v) {
			$this->kode_indikator = $v;
			$this->modifiedColumns[] = PakBukuBiruMasterKegiatanPeer::KODE_INDIKATOR;
		}

	} 
	
	public function setAlokasiDana($v)
	{

		if ($this->alokasi_dana !== $v) {
			$this->alokasi_dana = $v;
			$this->modifiedColumns[] = PakBukuBiruMasterKegiatanPeer::ALOKASI_DANA;
		}

	} 
	
	public function setNamaKegiatan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nama_kegiatan !== $v) {
			$this->nama_kegiatan = $v;
			$this->modifiedColumns[] = PakBukuBiruMasterKegiatanPeer::NAMA_KEGIATAN;
		}

	} 
	
	public function setMasukan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->masukan !== $v) {
			$this->masukan = $v;
			$this->modifiedColumns[] = PakBukuBiruMasterKegiatanPeer::MASUKAN;
		}

	} 
	
	public function setOutput($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->output !== $v) {
			$this->output = $v;
			$this->modifiedColumns[] = PakBukuBiruMasterKegiatanPeer::OUTPUT;
		}

	} 
	
	public function setOutcome($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->outcome !== $v) {
			$this->outcome = $v;
			$this->modifiedColumns[] = PakBukuBiruMasterKegiatanPeer::OUTCOME;
		}

	} 
	
	public function setBenefit($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->benefit !== $v) {
			$this->benefit = $v;
			$this->modifiedColumns[] = PakBukuBiruMasterKegiatanPeer::BENEFIT;
		}

	} 
	
	public function setImpact($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->impact !== $v) {
			$this->impact = $v;
			$this->modifiedColumns[] = PakBukuBiruMasterKegiatanPeer::IMPACT;
		}

	} 
	
	public function setTipe($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->tipe !== $v) {
			$this->tipe = $v;
			$this->modifiedColumns[] = PakBukuBiruMasterKegiatanPeer::TIPE;
		}

	} 
	
	public function setKegiatanActive($v)
	{

		if ($this->kegiatan_active !== $v || $v === false) {
			$this->kegiatan_active = $v;
			$this->modifiedColumns[] = PakBukuBiruMasterKegiatanPeer::KEGIATAN_ACTIVE;
		}

	} 
	
	public function setToKegiatanCode($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->to_kegiatan_code !== $v) {
			$this->to_kegiatan_code = $v;
			$this->modifiedColumns[] = PakBukuBiruMasterKegiatanPeer::TO_KEGIATAN_CODE;
		}

	} 
	
	public function setCatatan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->catatan !== $v) {
			$this->catatan = $v;
			$this->modifiedColumns[] = PakBukuBiruMasterKegiatanPeer::CATATAN;
		}

	} 
	
	public function setTargetOutcome($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->target_outcome !== $v) {
			$this->target_outcome = $v;
			$this->modifiedColumns[] = PakBukuBiruMasterKegiatanPeer::TARGET_OUTCOME;
		}

	} 
	
	public function setLokasi($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->lokasi !== $v) {
			$this->lokasi = $v;
			$this->modifiedColumns[] = PakBukuBiruMasterKegiatanPeer::LOKASI;
		}

	} 
	
	public function setJumlahPrev($v)
	{

		if ($this->jumlah_prev !== $v) {
			$this->jumlah_prev = $v;
			$this->modifiedColumns[] = PakBukuBiruMasterKegiatanPeer::JUMLAH_PREV;
		}

	} 
	
	public function setJumlahNow($v)
	{

		if ($this->jumlah_now !== $v) {
			$this->jumlah_now = $v;
			$this->modifiedColumns[] = PakBukuBiruMasterKegiatanPeer::JUMLAH_NOW;
		}

	} 
	
	public function setJumlahNext($v)
	{

		if ($this->jumlah_next !== $v) {
			$this->jumlah_next = $v;
			$this->modifiedColumns[] = PakBukuBiruMasterKegiatanPeer::JUMLAH_NEXT;
		}

	} 
	
	public function setKodeProgram2($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_program2 !== $v) {
			$this->kode_program2 = $v;
			$this->modifiedColumns[] = PakBukuBiruMasterKegiatanPeer::KODE_PROGRAM2;
		}

	} 
	
	public function setKodeUrusan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_urusan !== $v) {
			$this->kode_urusan = $v;
			$this->modifiedColumns[] = PakBukuBiruMasterKegiatanPeer::KODE_URUSAN;
		}

	} 
	
	public function setLastUpdateUser($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->last_update_user !== $v) {
			$this->last_update_user = $v;
			$this->modifiedColumns[] = PakBukuBiruMasterKegiatanPeer::LAST_UPDATE_USER;
		}

	} 
	
	public function setLastUpdateTime($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [last_update_time] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->last_update_time !== $ts) {
			$this->last_update_time = $ts;
			$this->modifiedColumns[] = PakBukuBiruMasterKegiatanPeer::LAST_UPDATE_TIME;
		}

	} 
	
	public function setLastUpdateIp($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->last_update_ip !== $v) {
			$this->last_update_ip = $v;
			$this->modifiedColumns[] = PakBukuBiruMasterKegiatanPeer::LAST_UPDATE_IP;
		}

	} 
	
	public function setTahap($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->tahap !== $v) {
			$this->tahap = $v;
			$this->modifiedColumns[] = PakBukuBiruMasterKegiatanPeer::TAHAP;
		}

	} 
	
	public function setKodeMisi($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_misi !== $v) {
			$this->kode_misi = $v;
			$this->modifiedColumns[] = PakBukuBiruMasterKegiatanPeer::KODE_MISI;
		}

	} 
	
	public function setKodeTujuan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_tujuan !== $v) {
			$this->kode_tujuan = $v;
			$this->modifiedColumns[] = PakBukuBiruMasterKegiatanPeer::KODE_TUJUAN;
		}

	} 
	
	public function setRanking($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->ranking !== $v) {
			$this->ranking = $v;
			$this->modifiedColumns[] = PakBukuBiruMasterKegiatanPeer::RANKING;
		}

	} 
	
	public function setNomor13($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nomor13 !== $v) {
			$this->nomor13 = $v;
			$this->modifiedColumns[] = PakBukuBiruMasterKegiatanPeer::NOMOR13;
		}

	} 
	
	public function setPpaNama($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->ppa_nama !== $v) {
			$this->ppa_nama = $v;
			$this->modifiedColumns[] = PakBukuBiruMasterKegiatanPeer::PPA_NAMA;
		}

	} 
	
	public function setPpaPangkat($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->ppa_pangkat !== $v) {
			$this->ppa_pangkat = $v;
			$this->modifiedColumns[] = PakBukuBiruMasterKegiatanPeer::PPA_PANGKAT;
		}

	} 
	
	public function setPpaNip($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->ppa_nip !== $v) {
			$this->ppa_nip = $v;
			$this->modifiedColumns[] = PakBukuBiruMasterKegiatanPeer::PPA_NIP;
		}

	} 
	
	public function setLanjutan($v)
	{

		if ($this->lanjutan !== $v) {
			$this->lanjutan = $v;
			$this->modifiedColumns[] = PakBukuBiruMasterKegiatanPeer::LANJUTAN;
		}

	} 
	
	public function setUserId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->user_id !== $v) {
			$this->user_id = $v;
			$this->modifiedColumns[] = PakBukuBiruMasterKegiatanPeer::USER_ID;
		}

	} 
	
	public function setId($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = PakBukuBiruMasterKegiatanPeer::ID;
		}

	} 
	
	public function setTahun($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->tahun !== $v) {
			$this->tahun = $v;
			$this->modifiedColumns[] = PakBukuBiruMasterKegiatanPeer::TAHUN;
		}

	} 
	
	public function setTambahanPagu($v)
	{

		if ($this->tambahan_pagu !== $v) {
			$this->tambahan_pagu = $v;
			$this->modifiedColumns[] = PakBukuBiruMasterKegiatanPeer::TAMBAHAN_PAGU;
		}

	} 
	
	public function setGender($v)
	{

		if ($this->gender !== $v || $v === false) {
			$this->gender = $v;
			$this->modifiedColumns[] = PakBukuBiruMasterKegiatanPeer::GENDER;
		}

	} 
	
	public function setKodeKegKeuangan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_keg_keuangan !== $v) {
			$this->kode_keg_keuangan = $v;
			$this->modifiedColumns[] = PakBukuBiruMasterKegiatanPeer::KODE_KEG_KEUANGAN;
		}

	} 
	
	public function setUserIdLama($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->user_id_lama !== $v) {
			$this->user_id_lama = $v;
			$this->modifiedColumns[] = PakBukuBiruMasterKegiatanPeer::USER_ID_LAMA;
		}

	} 
	
	public function setIndikator($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->indikator !== $v) {
			$this->indikator = $v;
			$this->modifiedColumns[] = PakBukuBiruMasterKegiatanPeer::INDIKATOR;
		}

	} 
	
	public function setIsDak($v)
	{

		if ($this->is_dak !== $v) {
			$this->is_dak = $v;
			$this->modifiedColumns[] = PakBukuBiruMasterKegiatanPeer::IS_DAK;
		}

	} 
	
	public function setKodeKegiatanAsal($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_kegiatan_asal !== $v) {
			$this->kode_kegiatan_asal = $v;
			$this->modifiedColumns[] = PakBukuBiruMasterKegiatanPeer::KODE_KEGIATAN_ASAL;
		}

	} 
	
	public function setKodeKegKeuanganAsal($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_keg_keuangan_asal !== $v) {
			$this->kode_keg_keuangan_asal = $v;
			$this->modifiedColumns[] = PakBukuBiruMasterKegiatanPeer::KODE_KEG_KEUANGAN_ASAL;
		}

	} 
	
	public function setThKeMultiyears($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->th_ke_multiyears !== $v) {
			$this->th_ke_multiyears = $v;
			$this->modifiedColumns[] = PakBukuBiruMasterKegiatanPeer::TH_KE_MULTIYEARS;
		}

	} 
	
	public function setKelompokSasaran($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kelompok_sasaran !== $v) {
			$this->kelompok_sasaran = $v;
			$this->modifiedColumns[] = PakBukuBiruMasterKegiatanPeer::KELOMPOK_SASARAN;
		}

	} 
	
	public function setPaguBappeko($v)
	{

		if ($this->pagu_bappeko !== $v) {
			$this->pagu_bappeko = $v;
			$this->modifiedColumns[] = PakBukuBiruMasterKegiatanPeer::PAGU_BAPPEKO;
		}

	} 
	
	public function setKodeDpa($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_dpa !== $v) {
			$this->kode_dpa = $v;
			$this->modifiedColumns[] = PakBukuBiruMasterKegiatanPeer::KODE_DPA;
		}

	} 
	
	public function setUserIdPptk($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->user_id_pptk !== $v) {
			$this->user_id_pptk = $v;
			$this->modifiedColumns[] = PakBukuBiruMasterKegiatanPeer::USER_ID_PPTK;
		}

	} 
	
	public function setUserIdKpa($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->user_id_kpa !== $v) {
			$this->user_id_kpa = $v;
			$this->modifiedColumns[] = PakBukuBiruMasterKegiatanPeer::USER_ID_KPA;
		}

	} 
	
	public function setCatatanPembahasan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->catatan_pembahasan !== $v) {
			$this->catatan_pembahasan = $v;
			$this->modifiedColumns[] = PakBukuBiruMasterKegiatanPeer::CATATAN_PEMBAHASAN;
		}

	} 
	
	public function setCatatanPenyelia($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->catatan_penyelia !== $v) {
			$this->catatan_penyelia = $v;
			$this->modifiedColumns[] = PakBukuBiruMasterKegiatanPeer::CATATAN_PENYELIA;
		}

	} 
	
	public function setCatatanBappeko($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->catatan_bappeko !== $v) {
			$this->catatan_bappeko = $v;
			$this->modifiedColumns[] = PakBukuBiruMasterKegiatanPeer::CATATAN_BAPPEKO;
		}

	} 
	
	public function setStatusLevel($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->status_level !== $v) {
			$this->status_level = $v;
			$this->modifiedColumns[] = PakBukuBiruMasterKegiatanPeer::STATUS_LEVEL;
		}

	} 
	
	public function setIsTapdSetuju($v)
	{

		if ($this->is_tapd_setuju !== $v || $v === false) {
			$this->is_tapd_setuju = $v;
			$this->modifiedColumns[] = PakBukuBiruMasterKegiatanPeer::IS_TAPD_SETUJU;
		}

	} 
	
	public function setIsBappekoSetuju($v)
	{

		if ($this->is_bappeko_setuju !== $v || $v === false) {
			$this->is_bappeko_setuju = $v;
			$this->modifiedColumns[] = PakBukuBiruMasterKegiatanPeer::IS_BAPPEKO_SETUJU;
		}

	} 
	
	public function setIsPenyeliaSetuju($v)
	{

		if ($this->is_penyelia_setuju !== $v || $v === false) {
			$this->is_penyelia_setuju = $v;
			$this->modifiedColumns[] = PakBukuBiruMasterKegiatanPeer::IS_PENYELIA_SETUJU;
		}

	} 
	
	public function setIsPernahRka($v)
	{

		if ($this->is_pernah_rka !== $v || $v === false) {
			$this->is_pernah_rka = $v;
			$this->modifiedColumns[] = PakBukuBiruMasterKegiatanPeer::IS_PERNAH_RKA;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->unit_id = $rs->getString($startcol + 0);

			$this->kode_kegiatan = $rs->getString($startcol + 1);

			$this->kode_kegiatan_baru = $rs->getString($startcol + 2);

			$this->kode_bidang = $rs->getString($startcol + 3);

			$this->kode_urusan_wajib = $rs->getString($startcol + 4);

			$this->kode_program = $rs->getString($startcol + 5);

			$this->kode_sasaran = $rs->getString($startcol + 6);

			$this->kode_indikator = $rs->getString($startcol + 7);

			$this->alokasi_dana = $rs->getFloat($startcol + 8);

			$this->nama_kegiatan = $rs->getString($startcol + 9);

			$this->masukan = $rs->getString($startcol + 10);

			$this->output = $rs->getString($startcol + 11);

			$this->outcome = $rs->getString($startcol + 12);

			$this->benefit = $rs->getString($startcol + 13);

			$this->impact = $rs->getString($startcol + 14);

			$this->tipe = $rs->getString($startcol + 15);

			$this->kegiatan_active = $rs->getBoolean($startcol + 16);

			$this->to_kegiatan_code = $rs->getString($startcol + 17);

			$this->catatan = $rs->getString($startcol + 18);

			$this->target_outcome = $rs->getString($startcol + 19);

			$this->lokasi = $rs->getString($startcol + 20);

			$this->jumlah_prev = $rs->getFloat($startcol + 21);

			$this->jumlah_now = $rs->getFloat($startcol + 22);

			$this->jumlah_next = $rs->getFloat($startcol + 23);

			$this->kode_program2 = $rs->getString($startcol + 24);

			$this->kode_urusan = $rs->getString($startcol + 25);

			$this->last_update_user = $rs->getString($startcol + 26);

			$this->last_update_time = $rs->getTimestamp($startcol + 27, null);

			$this->last_update_ip = $rs->getString($startcol + 28);

			$this->tahap = $rs->getString($startcol + 29);

			$this->kode_misi = $rs->getString($startcol + 30);

			$this->kode_tujuan = $rs->getString($startcol + 31);

			$this->ranking = $rs->getInt($startcol + 32);

			$this->nomor13 = $rs->getString($startcol + 33);

			$this->ppa_nama = $rs->getString($startcol + 34);

			$this->ppa_pangkat = $rs->getString($startcol + 35);

			$this->ppa_nip = $rs->getString($startcol + 36);

			$this->lanjutan = $rs->getBoolean($startcol + 37);

			$this->user_id = $rs->getString($startcol + 38);

			$this->id = $rs->getInt($startcol + 39);

			$this->tahun = $rs->getString($startcol + 40);

			$this->tambahan_pagu = $rs->getFloat($startcol + 41);

			$this->gender = $rs->getBoolean($startcol + 42);

			$this->kode_keg_keuangan = $rs->getString($startcol + 43);

			$this->user_id_lama = $rs->getString($startcol + 44);

			$this->indikator = $rs->getString($startcol + 45);

			$this->is_dak = $rs->getBoolean($startcol + 46);

			$this->kode_kegiatan_asal = $rs->getString($startcol + 47);

			$this->kode_keg_keuangan_asal = $rs->getString($startcol + 48);

			$this->th_ke_multiyears = $rs->getInt($startcol + 49);

			$this->kelompok_sasaran = $rs->getString($startcol + 50);

			$this->pagu_bappeko = $rs->getFloat($startcol + 51);

			$this->kode_dpa = $rs->getString($startcol + 52);

			$this->user_id_pptk = $rs->getString($startcol + 53);

			$this->user_id_kpa = $rs->getString($startcol + 54);

			$this->catatan_pembahasan = $rs->getString($startcol + 55);

			$this->catatan_penyelia = $rs->getString($startcol + 56);

			$this->catatan_bappeko = $rs->getString($startcol + 57);

			$this->status_level = $rs->getInt($startcol + 58);

			$this->is_tapd_setuju = $rs->getBoolean($startcol + 59);

			$this->is_bappeko_setuju = $rs->getBoolean($startcol + 60);

			$this->is_penyelia_setuju = $rs->getBoolean($startcol + 61);

			$this->is_pernah_rka = $rs->getBoolean($startcol + 62);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 63; 
		} catch (Exception $e) {
			throw new PropelException("Error populating PakBukuBiruMasterKegiatan object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(PakBukuBiruMasterKegiatanPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			PakBukuBiruMasterKegiatanPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(PakBukuBiruMasterKegiatanPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = PakBukuBiruMasterKegiatanPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setId($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += PakBukuBiruMasterKegiatanPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = PakBukuBiruMasterKegiatanPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = PakBukuBiruMasterKegiatanPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getUnitId();
				break;
			case 1:
				return $this->getKodeKegiatan();
				break;
			case 2:
				return $this->getKodeKegiatanBaru();
				break;
			case 3:
				return $this->getKodeBidang();
				break;
			case 4:
				return $this->getKodeUrusanWajib();
				break;
			case 5:
				return $this->getKodeProgram();
				break;
			case 6:
				return $this->getKodeSasaran();
				break;
			case 7:
				return $this->getKodeIndikator();
				break;
			case 8:
				return $this->getAlokasiDana();
				break;
			case 9:
				return $this->getNamaKegiatan();
				break;
			case 10:
				return $this->getMasukan();
				break;
			case 11:
				return $this->getOutput();
				break;
			case 12:
				return $this->getOutcome();
				break;
			case 13:
				return $this->getBenefit();
				break;
			case 14:
				return $this->getImpact();
				break;
			case 15:
				return $this->getTipe();
				break;
			case 16:
				return $this->getKegiatanActive();
				break;
			case 17:
				return $this->getToKegiatanCode();
				break;
			case 18:
				return $this->getCatatan();
				break;
			case 19:
				return $this->getTargetOutcome();
				break;
			case 20:
				return $this->getLokasi();
				break;
			case 21:
				return $this->getJumlahPrev();
				break;
			case 22:
				return $this->getJumlahNow();
				break;
			case 23:
				return $this->getJumlahNext();
				break;
			case 24:
				return $this->getKodeProgram2();
				break;
			case 25:
				return $this->getKodeUrusan();
				break;
			case 26:
				return $this->getLastUpdateUser();
				break;
			case 27:
				return $this->getLastUpdateTime();
				break;
			case 28:
				return $this->getLastUpdateIp();
				break;
			case 29:
				return $this->getTahap();
				break;
			case 30:
				return $this->getKodeMisi();
				break;
			case 31:
				return $this->getKodeTujuan();
				break;
			case 32:
				return $this->getRanking();
				break;
			case 33:
				return $this->getNomor13();
				break;
			case 34:
				return $this->getPpaNama();
				break;
			case 35:
				return $this->getPpaPangkat();
				break;
			case 36:
				return $this->getPpaNip();
				break;
			case 37:
				return $this->getLanjutan();
				break;
			case 38:
				return $this->getUserId();
				break;
			case 39:
				return $this->getId();
				break;
			case 40:
				return $this->getTahun();
				break;
			case 41:
				return $this->getTambahanPagu();
				break;
			case 42:
				return $this->getGender();
				break;
			case 43:
				return $this->getKodeKegKeuangan();
				break;
			case 44:
				return $this->getUserIdLama();
				break;
			case 45:
				return $this->getIndikator();
				break;
			case 46:
				return $this->getIsDak();
				break;
			case 47:
				return $this->getKodeKegiatanAsal();
				break;
			case 48:
				return $this->getKodeKegKeuanganAsal();
				break;
			case 49:
				return $this->getThKeMultiyears();
				break;
			case 50:
				return $this->getKelompokSasaran();
				break;
			case 51:
				return $this->getPaguBappeko();
				break;
			case 52:
				return $this->getKodeDpa();
				break;
			case 53:
				return $this->getUserIdPptk();
				break;
			case 54:
				return $this->getUserIdKpa();
				break;
			case 55:
				return $this->getCatatanPembahasan();
				break;
			case 56:
				return $this->getCatatanPenyelia();
				break;
			case 57:
				return $this->getCatatanBappeko();
				break;
			case 58:
				return $this->getStatusLevel();
				break;
			case 59:
				return $this->getIsTapdSetuju();
				break;
			case 60:
				return $this->getIsBappekoSetuju();
				break;
			case 61:
				return $this->getIsPenyeliaSetuju();
				break;
			case 62:
				return $this->getIsPernahRka();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = PakBukuBiruMasterKegiatanPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getUnitId(),
			$keys[1] => $this->getKodeKegiatan(),
			$keys[2] => $this->getKodeKegiatanBaru(),
			$keys[3] => $this->getKodeBidang(),
			$keys[4] => $this->getKodeUrusanWajib(),
			$keys[5] => $this->getKodeProgram(),
			$keys[6] => $this->getKodeSasaran(),
			$keys[7] => $this->getKodeIndikator(),
			$keys[8] => $this->getAlokasiDana(),
			$keys[9] => $this->getNamaKegiatan(),
			$keys[10] => $this->getMasukan(),
			$keys[11] => $this->getOutput(),
			$keys[12] => $this->getOutcome(),
			$keys[13] => $this->getBenefit(),
			$keys[14] => $this->getImpact(),
			$keys[15] => $this->getTipe(),
			$keys[16] => $this->getKegiatanActive(),
			$keys[17] => $this->getToKegiatanCode(),
			$keys[18] => $this->getCatatan(),
			$keys[19] => $this->getTargetOutcome(),
			$keys[20] => $this->getLokasi(),
			$keys[21] => $this->getJumlahPrev(),
			$keys[22] => $this->getJumlahNow(),
			$keys[23] => $this->getJumlahNext(),
			$keys[24] => $this->getKodeProgram2(),
			$keys[25] => $this->getKodeUrusan(),
			$keys[26] => $this->getLastUpdateUser(),
			$keys[27] => $this->getLastUpdateTime(),
			$keys[28] => $this->getLastUpdateIp(),
			$keys[29] => $this->getTahap(),
			$keys[30] => $this->getKodeMisi(),
			$keys[31] => $this->getKodeTujuan(),
			$keys[32] => $this->getRanking(),
			$keys[33] => $this->getNomor13(),
			$keys[34] => $this->getPpaNama(),
			$keys[35] => $this->getPpaPangkat(),
			$keys[36] => $this->getPpaNip(),
			$keys[37] => $this->getLanjutan(),
			$keys[38] => $this->getUserId(),
			$keys[39] => $this->getId(),
			$keys[40] => $this->getTahun(),
			$keys[41] => $this->getTambahanPagu(),
			$keys[42] => $this->getGender(),
			$keys[43] => $this->getKodeKegKeuangan(),
			$keys[44] => $this->getUserIdLama(),
			$keys[45] => $this->getIndikator(),
			$keys[46] => $this->getIsDak(),
			$keys[47] => $this->getKodeKegiatanAsal(),
			$keys[48] => $this->getKodeKegKeuanganAsal(),
			$keys[49] => $this->getThKeMultiyears(),
			$keys[50] => $this->getKelompokSasaran(),
			$keys[51] => $this->getPaguBappeko(),
			$keys[52] => $this->getKodeDpa(),
			$keys[53] => $this->getUserIdPptk(),
			$keys[54] => $this->getUserIdKpa(),
			$keys[55] => $this->getCatatanPembahasan(),
			$keys[56] => $this->getCatatanPenyelia(),
			$keys[57] => $this->getCatatanBappeko(),
			$keys[58] => $this->getStatusLevel(),
			$keys[59] => $this->getIsTapdSetuju(),
			$keys[60] => $this->getIsBappekoSetuju(),
			$keys[61] => $this->getIsPenyeliaSetuju(),
			$keys[62] => $this->getIsPernahRka(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = PakBukuBiruMasterKegiatanPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setUnitId($value);
				break;
			case 1:
				$this->setKodeKegiatan($value);
				break;
			case 2:
				$this->setKodeKegiatanBaru($value);
				break;
			case 3:
				$this->setKodeBidang($value);
				break;
			case 4:
				$this->setKodeUrusanWajib($value);
				break;
			case 5:
				$this->setKodeProgram($value);
				break;
			case 6:
				$this->setKodeSasaran($value);
				break;
			case 7:
				$this->setKodeIndikator($value);
				break;
			case 8:
				$this->setAlokasiDana($value);
				break;
			case 9:
				$this->setNamaKegiatan($value);
				break;
			case 10:
				$this->setMasukan($value);
				break;
			case 11:
				$this->setOutput($value);
				break;
			case 12:
				$this->setOutcome($value);
				break;
			case 13:
				$this->setBenefit($value);
				break;
			case 14:
				$this->setImpact($value);
				break;
			case 15:
				$this->setTipe($value);
				break;
			case 16:
				$this->setKegiatanActive($value);
				break;
			case 17:
				$this->setToKegiatanCode($value);
				break;
			case 18:
				$this->setCatatan($value);
				break;
			case 19:
				$this->setTargetOutcome($value);
				break;
			case 20:
				$this->setLokasi($value);
				break;
			case 21:
				$this->setJumlahPrev($value);
				break;
			case 22:
				$this->setJumlahNow($value);
				break;
			case 23:
				$this->setJumlahNext($value);
				break;
			case 24:
				$this->setKodeProgram2($value);
				break;
			case 25:
				$this->setKodeUrusan($value);
				break;
			case 26:
				$this->setLastUpdateUser($value);
				break;
			case 27:
				$this->setLastUpdateTime($value);
				break;
			case 28:
				$this->setLastUpdateIp($value);
				break;
			case 29:
				$this->setTahap($value);
				break;
			case 30:
				$this->setKodeMisi($value);
				break;
			case 31:
				$this->setKodeTujuan($value);
				break;
			case 32:
				$this->setRanking($value);
				break;
			case 33:
				$this->setNomor13($value);
				break;
			case 34:
				$this->setPpaNama($value);
				break;
			case 35:
				$this->setPpaPangkat($value);
				break;
			case 36:
				$this->setPpaNip($value);
				break;
			case 37:
				$this->setLanjutan($value);
				break;
			case 38:
				$this->setUserId($value);
				break;
			case 39:
				$this->setId($value);
				break;
			case 40:
				$this->setTahun($value);
				break;
			case 41:
				$this->setTambahanPagu($value);
				break;
			case 42:
				$this->setGender($value);
				break;
			case 43:
				$this->setKodeKegKeuangan($value);
				break;
			case 44:
				$this->setUserIdLama($value);
				break;
			case 45:
				$this->setIndikator($value);
				break;
			case 46:
				$this->setIsDak($value);
				break;
			case 47:
				$this->setKodeKegiatanAsal($value);
				break;
			case 48:
				$this->setKodeKegKeuanganAsal($value);
				break;
			case 49:
				$this->setThKeMultiyears($value);
				break;
			case 50:
				$this->setKelompokSasaran($value);
				break;
			case 51:
				$this->setPaguBappeko($value);
				break;
			case 52:
				$this->setKodeDpa($value);
				break;
			case 53:
				$this->setUserIdPptk($value);
				break;
			case 54:
				$this->setUserIdKpa($value);
				break;
			case 55:
				$this->setCatatanPembahasan($value);
				break;
			case 56:
				$this->setCatatanPenyelia($value);
				break;
			case 57:
				$this->setCatatanBappeko($value);
				break;
			case 58:
				$this->setStatusLevel($value);
				break;
			case 59:
				$this->setIsTapdSetuju($value);
				break;
			case 60:
				$this->setIsBappekoSetuju($value);
				break;
			case 61:
				$this->setIsPenyeliaSetuju($value);
				break;
			case 62:
				$this->setIsPernahRka($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = PakBukuBiruMasterKegiatanPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setUnitId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setKodeKegiatan($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setKodeKegiatanBaru($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setKodeBidang($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setKodeUrusanWajib($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setKodeProgram($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setKodeSasaran($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setKodeIndikator($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setAlokasiDana($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setNamaKegiatan($arr[$keys[9]]);
		if (array_key_exists($keys[10], $arr)) $this->setMasukan($arr[$keys[10]]);
		if (array_key_exists($keys[11], $arr)) $this->setOutput($arr[$keys[11]]);
		if (array_key_exists($keys[12], $arr)) $this->setOutcome($arr[$keys[12]]);
		if (array_key_exists($keys[13], $arr)) $this->setBenefit($arr[$keys[13]]);
		if (array_key_exists($keys[14], $arr)) $this->setImpact($arr[$keys[14]]);
		if (array_key_exists($keys[15], $arr)) $this->setTipe($arr[$keys[15]]);
		if (array_key_exists($keys[16], $arr)) $this->setKegiatanActive($arr[$keys[16]]);
		if (array_key_exists($keys[17], $arr)) $this->setToKegiatanCode($arr[$keys[17]]);
		if (array_key_exists($keys[18], $arr)) $this->setCatatan($arr[$keys[18]]);
		if (array_key_exists($keys[19], $arr)) $this->setTargetOutcome($arr[$keys[19]]);
		if (array_key_exists($keys[20], $arr)) $this->setLokasi($arr[$keys[20]]);
		if (array_key_exists($keys[21], $arr)) $this->setJumlahPrev($arr[$keys[21]]);
		if (array_key_exists($keys[22], $arr)) $this->setJumlahNow($arr[$keys[22]]);
		if (array_key_exists($keys[23], $arr)) $this->setJumlahNext($arr[$keys[23]]);
		if (array_key_exists($keys[24], $arr)) $this->setKodeProgram2($arr[$keys[24]]);
		if (array_key_exists($keys[25], $arr)) $this->setKodeUrusan($arr[$keys[25]]);
		if (array_key_exists($keys[26], $arr)) $this->setLastUpdateUser($arr[$keys[26]]);
		if (array_key_exists($keys[27], $arr)) $this->setLastUpdateTime($arr[$keys[27]]);
		if (array_key_exists($keys[28], $arr)) $this->setLastUpdateIp($arr[$keys[28]]);
		if (array_key_exists($keys[29], $arr)) $this->setTahap($arr[$keys[29]]);
		if (array_key_exists($keys[30], $arr)) $this->setKodeMisi($arr[$keys[30]]);
		if (array_key_exists($keys[31], $arr)) $this->setKodeTujuan($arr[$keys[31]]);
		if (array_key_exists($keys[32], $arr)) $this->setRanking($arr[$keys[32]]);
		if (array_key_exists($keys[33], $arr)) $this->setNomor13($arr[$keys[33]]);
		if (array_key_exists($keys[34], $arr)) $this->setPpaNama($arr[$keys[34]]);
		if (array_key_exists($keys[35], $arr)) $this->setPpaPangkat($arr[$keys[35]]);
		if (array_key_exists($keys[36], $arr)) $this->setPpaNip($arr[$keys[36]]);
		if (array_key_exists($keys[37], $arr)) $this->setLanjutan($arr[$keys[37]]);
		if (array_key_exists($keys[38], $arr)) $this->setUserId($arr[$keys[38]]);
		if (array_key_exists($keys[39], $arr)) $this->setId($arr[$keys[39]]);
		if (array_key_exists($keys[40], $arr)) $this->setTahun($arr[$keys[40]]);
		if (array_key_exists($keys[41], $arr)) $this->setTambahanPagu($arr[$keys[41]]);
		if (array_key_exists($keys[42], $arr)) $this->setGender($arr[$keys[42]]);
		if (array_key_exists($keys[43], $arr)) $this->setKodeKegKeuangan($arr[$keys[43]]);
		if (array_key_exists($keys[44], $arr)) $this->setUserIdLama($arr[$keys[44]]);
		if (array_key_exists($keys[45], $arr)) $this->setIndikator($arr[$keys[45]]);
		if (array_key_exists($keys[46], $arr)) $this->setIsDak($arr[$keys[46]]);
		if (array_key_exists($keys[47], $arr)) $this->setKodeKegiatanAsal($arr[$keys[47]]);
		if (array_key_exists($keys[48], $arr)) $this->setKodeKegKeuanganAsal($arr[$keys[48]]);
		if (array_key_exists($keys[49], $arr)) $this->setThKeMultiyears($arr[$keys[49]]);
		if (array_key_exists($keys[50], $arr)) $this->setKelompokSasaran($arr[$keys[50]]);
		if (array_key_exists($keys[51], $arr)) $this->setPaguBappeko($arr[$keys[51]]);
		if (array_key_exists($keys[52], $arr)) $this->setKodeDpa($arr[$keys[52]]);
		if (array_key_exists($keys[53], $arr)) $this->setUserIdPptk($arr[$keys[53]]);
		if (array_key_exists($keys[54], $arr)) $this->setUserIdKpa($arr[$keys[54]]);
		if (array_key_exists($keys[55], $arr)) $this->setCatatanPembahasan($arr[$keys[55]]);
		if (array_key_exists($keys[56], $arr)) $this->setCatatanPenyelia($arr[$keys[56]]);
		if (array_key_exists($keys[57], $arr)) $this->setCatatanBappeko($arr[$keys[57]]);
		if (array_key_exists($keys[58], $arr)) $this->setStatusLevel($arr[$keys[58]]);
		if (array_key_exists($keys[59], $arr)) $this->setIsTapdSetuju($arr[$keys[59]]);
		if (array_key_exists($keys[60], $arr)) $this->setIsBappekoSetuju($arr[$keys[60]]);
		if (array_key_exists($keys[61], $arr)) $this->setIsPenyeliaSetuju($arr[$keys[61]]);
		if (array_key_exists($keys[62], $arr)) $this->setIsPernahRka($arr[$keys[62]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(PakBukuBiruMasterKegiatanPeer::DATABASE_NAME);

		if ($this->isColumnModified(PakBukuBiruMasterKegiatanPeer::UNIT_ID)) $criteria->add(PakBukuBiruMasterKegiatanPeer::UNIT_ID, $this->unit_id);
		if ($this->isColumnModified(PakBukuBiruMasterKegiatanPeer::KODE_KEGIATAN)) $criteria->add(PakBukuBiruMasterKegiatanPeer::KODE_KEGIATAN, $this->kode_kegiatan);
		if ($this->isColumnModified(PakBukuBiruMasterKegiatanPeer::KODE_KEGIATAN_BARU)) $criteria->add(PakBukuBiruMasterKegiatanPeer::KODE_KEGIATAN_BARU, $this->kode_kegiatan_baru);
		if ($this->isColumnModified(PakBukuBiruMasterKegiatanPeer::KODE_BIDANG)) $criteria->add(PakBukuBiruMasterKegiatanPeer::KODE_BIDANG, $this->kode_bidang);
		if ($this->isColumnModified(PakBukuBiruMasterKegiatanPeer::KODE_URUSAN_WAJIB)) $criteria->add(PakBukuBiruMasterKegiatanPeer::KODE_URUSAN_WAJIB, $this->kode_urusan_wajib);
		if ($this->isColumnModified(PakBukuBiruMasterKegiatanPeer::KODE_PROGRAM)) $criteria->add(PakBukuBiruMasterKegiatanPeer::KODE_PROGRAM, $this->kode_program);
		if ($this->isColumnModified(PakBukuBiruMasterKegiatanPeer::KODE_SASARAN)) $criteria->add(PakBukuBiruMasterKegiatanPeer::KODE_SASARAN, $this->kode_sasaran);
		if ($this->isColumnModified(PakBukuBiruMasterKegiatanPeer::KODE_INDIKATOR)) $criteria->add(PakBukuBiruMasterKegiatanPeer::KODE_INDIKATOR, $this->kode_indikator);
		if ($this->isColumnModified(PakBukuBiruMasterKegiatanPeer::ALOKASI_DANA)) $criteria->add(PakBukuBiruMasterKegiatanPeer::ALOKASI_DANA, $this->alokasi_dana);
		if ($this->isColumnModified(PakBukuBiruMasterKegiatanPeer::NAMA_KEGIATAN)) $criteria->add(PakBukuBiruMasterKegiatanPeer::NAMA_KEGIATAN, $this->nama_kegiatan);
		if ($this->isColumnModified(PakBukuBiruMasterKegiatanPeer::MASUKAN)) $criteria->add(PakBukuBiruMasterKegiatanPeer::MASUKAN, $this->masukan);
		if ($this->isColumnModified(PakBukuBiruMasterKegiatanPeer::OUTPUT)) $criteria->add(PakBukuBiruMasterKegiatanPeer::OUTPUT, $this->output);
		if ($this->isColumnModified(PakBukuBiruMasterKegiatanPeer::OUTCOME)) $criteria->add(PakBukuBiruMasterKegiatanPeer::OUTCOME, $this->outcome);
		if ($this->isColumnModified(PakBukuBiruMasterKegiatanPeer::BENEFIT)) $criteria->add(PakBukuBiruMasterKegiatanPeer::BENEFIT, $this->benefit);
		if ($this->isColumnModified(PakBukuBiruMasterKegiatanPeer::IMPACT)) $criteria->add(PakBukuBiruMasterKegiatanPeer::IMPACT, $this->impact);
		if ($this->isColumnModified(PakBukuBiruMasterKegiatanPeer::TIPE)) $criteria->add(PakBukuBiruMasterKegiatanPeer::TIPE, $this->tipe);
		if ($this->isColumnModified(PakBukuBiruMasterKegiatanPeer::KEGIATAN_ACTIVE)) $criteria->add(PakBukuBiruMasterKegiatanPeer::KEGIATAN_ACTIVE, $this->kegiatan_active);
		if ($this->isColumnModified(PakBukuBiruMasterKegiatanPeer::TO_KEGIATAN_CODE)) $criteria->add(PakBukuBiruMasterKegiatanPeer::TO_KEGIATAN_CODE, $this->to_kegiatan_code);
		if ($this->isColumnModified(PakBukuBiruMasterKegiatanPeer::CATATAN)) $criteria->add(PakBukuBiruMasterKegiatanPeer::CATATAN, $this->catatan);
		if ($this->isColumnModified(PakBukuBiruMasterKegiatanPeer::TARGET_OUTCOME)) $criteria->add(PakBukuBiruMasterKegiatanPeer::TARGET_OUTCOME, $this->target_outcome);
		if ($this->isColumnModified(PakBukuBiruMasterKegiatanPeer::LOKASI)) $criteria->add(PakBukuBiruMasterKegiatanPeer::LOKASI, $this->lokasi);
		if ($this->isColumnModified(PakBukuBiruMasterKegiatanPeer::JUMLAH_PREV)) $criteria->add(PakBukuBiruMasterKegiatanPeer::JUMLAH_PREV, $this->jumlah_prev);
		if ($this->isColumnModified(PakBukuBiruMasterKegiatanPeer::JUMLAH_NOW)) $criteria->add(PakBukuBiruMasterKegiatanPeer::JUMLAH_NOW, $this->jumlah_now);
		if ($this->isColumnModified(PakBukuBiruMasterKegiatanPeer::JUMLAH_NEXT)) $criteria->add(PakBukuBiruMasterKegiatanPeer::JUMLAH_NEXT, $this->jumlah_next);
		if ($this->isColumnModified(PakBukuBiruMasterKegiatanPeer::KODE_PROGRAM2)) $criteria->add(PakBukuBiruMasterKegiatanPeer::KODE_PROGRAM2, $this->kode_program2);
		if ($this->isColumnModified(PakBukuBiruMasterKegiatanPeer::KODE_URUSAN)) $criteria->add(PakBukuBiruMasterKegiatanPeer::KODE_URUSAN, $this->kode_urusan);
		if ($this->isColumnModified(PakBukuBiruMasterKegiatanPeer::LAST_UPDATE_USER)) $criteria->add(PakBukuBiruMasterKegiatanPeer::LAST_UPDATE_USER, $this->last_update_user);
		if ($this->isColumnModified(PakBukuBiruMasterKegiatanPeer::LAST_UPDATE_TIME)) $criteria->add(PakBukuBiruMasterKegiatanPeer::LAST_UPDATE_TIME, $this->last_update_time);
		if ($this->isColumnModified(PakBukuBiruMasterKegiatanPeer::LAST_UPDATE_IP)) $criteria->add(PakBukuBiruMasterKegiatanPeer::LAST_UPDATE_IP, $this->last_update_ip);
		if ($this->isColumnModified(PakBukuBiruMasterKegiatanPeer::TAHAP)) $criteria->add(PakBukuBiruMasterKegiatanPeer::TAHAP, $this->tahap);
		if ($this->isColumnModified(PakBukuBiruMasterKegiatanPeer::KODE_MISI)) $criteria->add(PakBukuBiruMasterKegiatanPeer::KODE_MISI, $this->kode_misi);
		if ($this->isColumnModified(PakBukuBiruMasterKegiatanPeer::KODE_TUJUAN)) $criteria->add(PakBukuBiruMasterKegiatanPeer::KODE_TUJUAN, $this->kode_tujuan);
		if ($this->isColumnModified(PakBukuBiruMasterKegiatanPeer::RANKING)) $criteria->add(PakBukuBiruMasterKegiatanPeer::RANKING, $this->ranking);
		if ($this->isColumnModified(PakBukuBiruMasterKegiatanPeer::NOMOR13)) $criteria->add(PakBukuBiruMasterKegiatanPeer::NOMOR13, $this->nomor13);
		if ($this->isColumnModified(PakBukuBiruMasterKegiatanPeer::PPA_NAMA)) $criteria->add(PakBukuBiruMasterKegiatanPeer::PPA_NAMA, $this->ppa_nama);
		if ($this->isColumnModified(PakBukuBiruMasterKegiatanPeer::PPA_PANGKAT)) $criteria->add(PakBukuBiruMasterKegiatanPeer::PPA_PANGKAT, $this->ppa_pangkat);
		if ($this->isColumnModified(PakBukuBiruMasterKegiatanPeer::PPA_NIP)) $criteria->add(PakBukuBiruMasterKegiatanPeer::PPA_NIP, $this->ppa_nip);
		if ($this->isColumnModified(PakBukuBiruMasterKegiatanPeer::LANJUTAN)) $criteria->add(PakBukuBiruMasterKegiatanPeer::LANJUTAN, $this->lanjutan);
		if ($this->isColumnModified(PakBukuBiruMasterKegiatanPeer::USER_ID)) $criteria->add(PakBukuBiruMasterKegiatanPeer::USER_ID, $this->user_id);
		if ($this->isColumnModified(PakBukuBiruMasterKegiatanPeer::ID)) $criteria->add(PakBukuBiruMasterKegiatanPeer::ID, $this->id);
		if ($this->isColumnModified(PakBukuBiruMasterKegiatanPeer::TAHUN)) $criteria->add(PakBukuBiruMasterKegiatanPeer::TAHUN, $this->tahun);
		if ($this->isColumnModified(PakBukuBiruMasterKegiatanPeer::TAMBAHAN_PAGU)) $criteria->add(PakBukuBiruMasterKegiatanPeer::TAMBAHAN_PAGU, $this->tambahan_pagu);
		if ($this->isColumnModified(PakBukuBiruMasterKegiatanPeer::GENDER)) $criteria->add(PakBukuBiruMasterKegiatanPeer::GENDER, $this->gender);
		if ($this->isColumnModified(PakBukuBiruMasterKegiatanPeer::KODE_KEG_KEUANGAN)) $criteria->add(PakBukuBiruMasterKegiatanPeer::KODE_KEG_KEUANGAN, $this->kode_keg_keuangan);
		if ($this->isColumnModified(PakBukuBiruMasterKegiatanPeer::USER_ID_LAMA)) $criteria->add(PakBukuBiruMasterKegiatanPeer::USER_ID_LAMA, $this->user_id_lama);
		if ($this->isColumnModified(PakBukuBiruMasterKegiatanPeer::INDIKATOR)) $criteria->add(PakBukuBiruMasterKegiatanPeer::INDIKATOR, $this->indikator);
		if ($this->isColumnModified(PakBukuBiruMasterKegiatanPeer::IS_DAK)) $criteria->add(PakBukuBiruMasterKegiatanPeer::IS_DAK, $this->is_dak);
		if ($this->isColumnModified(PakBukuBiruMasterKegiatanPeer::KODE_KEGIATAN_ASAL)) $criteria->add(PakBukuBiruMasterKegiatanPeer::KODE_KEGIATAN_ASAL, $this->kode_kegiatan_asal);
		if ($this->isColumnModified(PakBukuBiruMasterKegiatanPeer::KODE_KEG_KEUANGAN_ASAL)) $criteria->add(PakBukuBiruMasterKegiatanPeer::KODE_KEG_KEUANGAN_ASAL, $this->kode_keg_keuangan_asal);
		if ($this->isColumnModified(PakBukuBiruMasterKegiatanPeer::TH_KE_MULTIYEARS)) $criteria->add(PakBukuBiruMasterKegiatanPeer::TH_KE_MULTIYEARS, $this->th_ke_multiyears);
		if ($this->isColumnModified(PakBukuBiruMasterKegiatanPeer::KELOMPOK_SASARAN)) $criteria->add(PakBukuBiruMasterKegiatanPeer::KELOMPOK_SASARAN, $this->kelompok_sasaran);
		if ($this->isColumnModified(PakBukuBiruMasterKegiatanPeer::PAGU_BAPPEKO)) $criteria->add(PakBukuBiruMasterKegiatanPeer::PAGU_BAPPEKO, $this->pagu_bappeko);
		if ($this->isColumnModified(PakBukuBiruMasterKegiatanPeer::KODE_DPA)) $criteria->add(PakBukuBiruMasterKegiatanPeer::KODE_DPA, $this->kode_dpa);
		if ($this->isColumnModified(PakBukuBiruMasterKegiatanPeer::USER_ID_PPTK)) $criteria->add(PakBukuBiruMasterKegiatanPeer::USER_ID_PPTK, $this->user_id_pptk);
		if ($this->isColumnModified(PakBukuBiruMasterKegiatanPeer::USER_ID_KPA)) $criteria->add(PakBukuBiruMasterKegiatanPeer::USER_ID_KPA, $this->user_id_kpa);
		if ($this->isColumnModified(PakBukuBiruMasterKegiatanPeer::CATATAN_PEMBAHASAN)) $criteria->add(PakBukuBiruMasterKegiatanPeer::CATATAN_PEMBAHASAN, $this->catatan_pembahasan);
		if ($this->isColumnModified(PakBukuBiruMasterKegiatanPeer::CATATAN_PENYELIA)) $criteria->add(PakBukuBiruMasterKegiatanPeer::CATATAN_PENYELIA, $this->catatan_penyelia);
		if ($this->isColumnModified(PakBukuBiruMasterKegiatanPeer::CATATAN_BAPPEKO)) $criteria->add(PakBukuBiruMasterKegiatanPeer::CATATAN_BAPPEKO, $this->catatan_bappeko);
		if ($this->isColumnModified(PakBukuBiruMasterKegiatanPeer::STATUS_LEVEL)) $criteria->add(PakBukuBiruMasterKegiatanPeer::STATUS_LEVEL, $this->status_level);
		if ($this->isColumnModified(PakBukuBiruMasterKegiatanPeer::IS_TAPD_SETUJU)) $criteria->add(PakBukuBiruMasterKegiatanPeer::IS_TAPD_SETUJU, $this->is_tapd_setuju);
		if ($this->isColumnModified(PakBukuBiruMasterKegiatanPeer::IS_BAPPEKO_SETUJU)) $criteria->add(PakBukuBiruMasterKegiatanPeer::IS_BAPPEKO_SETUJU, $this->is_bappeko_setuju);
		if ($this->isColumnModified(PakBukuBiruMasterKegiatanPeer::IS_PENYELIA_SETUJU)) $criteria->add(PakBukuBiruMasterKegiatanPeer::IS_PENYELIA_SETUJU, $this->is_penyelia_setuju);
		if ($this->isColumnModified(PakBukuBiruMasterKegiatanPeer::IS_PERNAH_RKA)) $criteria->add(PakBukuBiruMasterKegiatanPeer::IS_PERNAH_RKA, $this->is_pernah_rka);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(PakBukuBiruMasterKegiatanPeer::DATABASE_NAME);

		$criteria->add(PakBukuBiruMasterKegiatanPeer::UNIT_ID, $this->unit_id);
		$criteria->add(PakBukuBiruMasterKegiatanPeer::KODE_KEGIATAN, $this->kode_kegiatan);
		$criteria->add(PakBukuBiruMasterKegiatanPeer::ID, $this->id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		$pks = array();

		$pks[0] = $this->getUnitId();

		$pks[1] = $this->getKodeKegiatan();

		$pks[2] = $this->getId();

		return $pks;
	}

	
	public function setPrimaryKey($keys)
	{

		$this->setUnitId($keys[0]);

		$this->setKodeKegiatan($keys[1]);

		$this->setId($keys[2]);

	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setKodeKegiatanBaru($this->kode_kegiatan_baru);

		$copyObj->setKodeBidang($this->kode_bidang);

		$copyObj->setKodeUrusanWajib($this->kode_urusan_wajib);

		$copyObj->setKodeProgram($this->kode_program);

		$copyObj->setKodeSasaran($this->kode_sasaran);

		$copyObj->setKodeIndikator($this->kode_indikator);

		$copyObj->setAlokasiDana($this->alokasi_dana);

		$copyObj->setNamaKegiatan($this->nama_kegiatan);

		$copyObj->setMasukan($this->masukan);

		$copyObj->setOutput($this->output);

		$copyObj->setOutcome($this->outcome);

		$copyObj->setBenefit($this->benefit);

		$copyObj->setImpact($this->impact);

		$copyObj->setTipe($this->tipe);

		$copyObj->setKegiatanActive($this->kegiatan_active);

		$copyObj->setToKegiatanCode($this->to_kegiatan_code);

		$copyObj->setCatatan($this->catatan);

		$copyObj->setTargetOutcome($this->target_outcome);

		$copyObj->setLokasi($this->lokasi);

		$copyObj->setJumlahPrev($this->jumlah_prev);

		$copyObj->setJumlahNow($this->jumlah_now);

		$copyObj->setJumlahNext($this->jumlah_next);

		$copyObj->setKodeProgram2($this->kode_program2);

		$copyObj->setKodeUrusan($this->kode_urusan);

		$copyObj->setLastUpdateUser($this->last_update_user);

		$copyObj->setLastUpdateTime($this->last_update_time);

		$copyObj->setLastUpdateIp($this->last_update_ip);

		$copyObj->setTahap($this->tahap);

		$copyObj->setKodeMisi($this->kode_misi);

		$copyObj->setKodeTujuan($this->kode_tujuan);

		$copyObj->setRanking($this->ranking);

		$copyObj->setNomor13($this->nomor13);

		$copyObj->setPpaNama($this->ppa_nama);

		$copyObj->setPpaPangkat($this->ppa_pangkat);

		$copyObj->setPpaNip($this->ppa_nip);

		$copyObj->setLanjutan($this->lanjutan);

		$copyObj->setUserId($this->user_id);

		$copyObj->setTahun($this->tahun);

		$copyObj->setTambahanPagu($this->tambahan_pagu);

		$copyObj->setGender($this->gender);

		$copyObj->setKodeKegKeuangan($this->kode_keg_keuangan);

		$copyObj->setUserIdLama($this->user_id_lama);

		$copyObj->setIndikator($this->indikator);

		$copyObj->setIsDak($this->is_dak);

		$copyObj->setKodeKegiatanAsal($this->kode_kegiatan_asal);

		$copyObj->setKodeKegKeuanganAsal($this->kode_keg_keuangan_asal);

		$copyObj->setThKeMultiyears($this->th_ke_multiyears);

		$copyObj->setKelompokSasaran($this->kelompok_sasaran);

		$copyObj->setPaguBappeko($this->pagu_bappeko);

		$copyObj->setKodeDpa($this->kode_dpa);

		$copyObj->setUserIdPptk($this->user_id_pptk);

		$copyObj->setUserIdKpa($this->user_id_kpa);

		$copyObj->setCatatanPembahasan($this->catatan_pembahasan);

		$copyObj->setCatatanPenyelia($this->catatan_penyelia);

		$copyObj->setCatatanBappeko($this->catatan_bappeko);

		$copyObj->setStatusLevel($this->status_level);

		$copyObj->setIsTapdSetuju($this->is_tapd_setuju);

		$copyObj->setIsBappekoSetuju($this->is_bappeko_setuju);

		$copyObj->setIsPenyeliaSetuju($this->is_penyelia_setuju);

		$copyObj->setIsPernahRka($this->is_pernah_rka);


		$copyObj->setNew(true);

		$copyObj->setUnitId(NULL); 
		$copyObj->setKodeKegiatan(NULL); 
		$copyObj->setId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new PakBukuBiruMasterKegiatanPeer();
		}
		return self::$peer;
	}

} 