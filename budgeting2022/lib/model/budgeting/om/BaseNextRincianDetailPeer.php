<?php


abstract class BaseNextRincianDetailPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.next_rincian_detail';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.NextRincianDetail';

	
	const NUM_COLUMNS = 36;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const KEGIATAN_CODE = 'ebudget.next_rincian_detail.KEGIATAN_CODE';

	
	const TIPE = 'ebudget.next_rincian_detail.TIPE';

	
	const DETAIL_NO = 'ebudget.next_rincian_detail.DETAIL_NO';

	
	const REKENING_CODE = 'ebudget.next_rincian_detail.REKENING_CODE';

	
	const KOMPONEN_ID = 'ebudget.next_rincian_detail.KOMPONEN_ID';

	
	const DETAIL_NAME = 'ebudget.next_rincian_detail.DETAIL_NAME';

	
	const VOLUME = 'ebudget.next_rincian_detail.VOLUME';

	
	const KETERANGAN_KOEFISIEN = 'ebudget.next_rincian_detail.KETERANGAN_KOEFISIEN';

	
	const SUBTITLE = 'ebudget.next_rincian_detail.SUBTITLE';

	
	const KOMPONEN_HARGA = 'ebudget.next_rincian_detail.KOMPONEN_HARGA';

	
	const KOMPONEN_HARGA_AWAL = 'ebudget.next_rincian_detail.KOMPONEN_HARGA_AWAL';

	
	const KOMPONEN_NAME = 'ebudget.next_rincian_detail.KOMPONEN_NAME';

	
	const SATUAN = 'ebudget.next_rincian_detail.SATUAN';

	
	const PAJAK = 'ebudget.next_rincian_detail.PAJAK';

	
	const UNIT_ID = 'ebudget.next_rincian_detail.UNIT_ID';

	
	const FROM_SUB_KEGIATAN = 'ebudget.next_rincian_detail.FROM_SUB_KEGIATAN';

	
	const SUB = 'ebudget.next_rincian_detail.SUB';

	
	const KODE_SUB = 'ebudget.next_rincian_detail.KODE_SUB';

	
	const LAST_UPDATE_USER = 'ebudget.next_rincian_detail.LAST_UPDATE_USER';

	
	const LAST_UPDATE_TIME = 'ebudget.next_rincian_detail.LAST_UPDATE_TIME';

	
	const LAST_UPDATE_IP = 'ebudget.next_rincian_detail.LAST_UPDATE_IP';

	
	const TAHAP = 'ebudget.next_rincian_detail.TAHAP';

	
	const TAHAP_EDIT = 'ebudget.next_rincian_detail.TAHAP_EDIT';

	
	const TAHAP_NEW = 'ebudget.next_rincian_detail.TAHAP_NEW';

	
	const STATUS_LELANG = 'ebudget.next_rincian_detail.STATUS_LELANG';

	
	const NOMOR_LELANG = 'ebudget.next_rincian_detail.NOMOR_LELANG';

	
	const KOEFISIEN_SEMULA = 'ebudget.next_rincian_detail.KOEFISIEN_SEMULA';

	
	const VOLUME_SEMULA = 'ebudget.next_rincian_detail.VOLUME_SEMULA';

	
	const HARGA_SEMULA = 'ebudget.next_rincian_detail.HARGA_SEMULA';

	
	const TOTAL_SEMULA = 'ebudget.next_rincian_detail.TOTAL_SEMULA';

	
	const LOCK_SUBTITLE = 'ebudget.next_rincian_detail.LOCK_SUBTITLE';

	
	const STATUS_HAPUS = 'ebudget.next_rincian_detail.STATUS_HAPUS';

	
	const TAHUN = 'ebudget.next_rincian_detail.TAHUN';

	
	const KODE_LOKASI = 'ebudget.next_rincian_detail.KODE_LOKASI';

	
	const KECAMATAN = 'ebudget.next_rincian_detail.KECAMATAN';

	
	const REKENING_CODE_ASLI = 'ebudget.next_rincian_detail.REKENING_CODE_ASLI';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('KegiatanCode', 'Tipe', 'DetailNo', 'RekeningCode', 'KomponenId', 'DetailName', 'Volume', 'KeteranganKoefisien', 'Subtitle', 'KomponenHarga', 'KomponenHargaAwal', 'KomponenName', 'Satuan', 'Pajak', 'UnitId', 'FromSubKegiatan', 'Sub', 'KodeSub', 'LastUpdateUser', 'LastUpdateTime', 'LastUpdateIp', 'Tahap', 'TahapEdit', 'TahapNew', 'StatusLelang', 'NomorLelang', 'KoefisienSemula', 'VolumeSemula', 'HargaSemula', 'TotalSemula', 'LockSubtitle', 'StatusHapus', 'Tahun', 'KodeLokasi', 'Kecamatan', 'RekeningCodeAsli', ),
		BasePeer::TYPE_COLNAME => array (NextRincianDetailPeer::KEGIATAN_CODE, NextRincianDetailPeer::TIPE, NextRincianDetailPeer::DETAIL_NO, NextRincianDetailPeer::REKENING_CODE, NextRincianDetailPeer::KOMPONEN_ID, NextRincianDetailPeer::DETAIL_NAME, NextRincianDetailPeer::VOLUME, NextRincianDetailPeer::KETERANGAN_KOEFISIEN, NextRincianDetailPeer::SUBTITLE, NextRincianDetailPeer::KOMPONEN_HARGA, NextRincianDetailPeer::KOMPONEN_HARGA_AWAL, NextRincianDetailPeer::KOMPONEN_NAME, NextRincianDetailPeer::SATUAN, NextRincianDetailPeer::PAJAK, NextRincianDetailPeer::UNIT_ID, NextRincianDetailPeer::FROM_SUB_KEGIATAN, NextRincianDetailPeer::SUB, NextRincianDetailPeer::KODE_SUB, NextRincianDetailPeer::LAST_UPDATE_USER, NextRincianDetailPeer::LAST_UPDATE_TIME, NextRincianDetailPeer::LAST_UPDATE_IP, NextRincianDetailPeer::TAHAP, NextRincianDetailPeer::TAHAP_EDIT, NextRincianDetailPeer::TAHAP_NEW, NextRincianDetailPeer::STATUS_LELANG, NextRincianDetailPeer::NOMOR_LELANG, NextRincianDetailPeer::KOEFISIEN_SEMULA, NextRincianDetailPeer::VOLUME_SEMULA, NextRincianDetailPeer::HARGA_SEMULA, NextRincianDetailPeer::TOTAL_SEMULA, NextRincianDetailPeer::LOCK_SUBTITLE, NextRincianDetailPeer::STATUS_HAPUS, NextRincianDetailPeer::TAHUN, NextRincianDetailPeer::KODE_LOKASI, NextRincianDetailPeer::KECAMATAN, NextRincianDetailPeer::REKENING_CODE_ASLI, ),
		BasePeer::TYPE_FIELDNAME => array ('kegiatan_code', 'tipe', 'detail_no', 'rekening_code', 'komponen_id', 'detail_name', 'volume', 'keterangan_koefisien', 'subtitle', 'komponen_harga', 'komponen_harga_awal', 'komponen_name', 'satuan', 'pajak', 'unit_id', 'from_sub_kegiatan', 'sub', 'kode_sub', 'last_update_user', 'last_update_time', 'last_update_ip', 'tahap', 'tahap_edit', 'tahap_new', 'status_lelang', 'nomor_lelang', 'koefisien_semula', 'volume_semula', 'harga_semula', 'total_semula', 'lock_subtitle', 'status_hapus', 'tahun', 'kode_lokasi', 'kecamatan', 'rekening_code_asli', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('KegiatanCode' => 0, 'Tipe' => 1, 'DetailNo' => 2, 'RekeningCode' => 3, 'KomponenId' => 4, 'DetailName' => 5, 'Volume' => 6, 'KeteranganKoefisien' => 7, 'Subtitle' => 8, 'KomponenHarga' => 9, 'KomponenHargaAwal' => 10, 'KomponenName' => 11, 'Satuan' => 12, 'Pajak' => 13, 'UnitId' => 14, 'FromSubKegiatan' => 15, 'Sub' => 16, 'KodeSub' => 17, 'LastUpdateUser' => 18, 'LastUpdateTime' => 19, 'LastUpdateIp' => 20, 'Tahap' => 21, 'TahapEdit' => 22, 'TahapNew' => 23, 'StatusLelang' => 24, 'NomorLelang' => 25, 'KoefisienSemula' => 26, 'VolumeSemula' => 27, 'HargaSemula' => 28, 'TotalSemula' => 29, 'LockSubtitle' => 30, 'StatusHapus' => 31, 'Tahun' => 32, 'KodeLokasi' => 33, 'Kecamatan' => 34, 'RekeningCodeAsli' => 35, ),
		BasePeer::TYPE_COLNAME => array (NextRincianDetailPeer::KEGIATAN_CODE => 0, NextRincianDetailPeer::TIPE => 1, NextRincianDetailPeer::DETAIL_NO => 2, NextRincianDetailPeer::REKENING_CODE => 3, NextRincianDetailPeer::KOMPONEN_ID => 4, NextRincianDetailPeer::DETAIL_NAME => 5, NextRincianDetailPeer::VOLUME => 6, NextRincianDetailPeer::KETERANGAN_KOEFISIEN => 7, NextRincianDetailPeer::SUBTITLE => 8, NextRincianDetailPeer::KOMPONEN_HARGA => 9, NextRincianDetailPeer::KOMPONEN_HARGA_AWAL => 10, NextRincianDetailPeer::KOMPONEN_NAME => 11, NextRincianDetailPeer::SATUAN => 12, NextRincianDetailPeer::PAJAK => 13, NextRincianDetailPeer::UNIT_ID => 14, NextRincianDetailPeer::FROM_SUB_KEGIATAN => 15, NextRincianDetailPeer::SUB => 16, NextRincianDetailPeer::KODE_SUB => 17, NextRincianDetailPeer::LAST_UPDATE_USER => 18, NextRincianDetailPeer::LAST_UPDATE_TIME => 19, NextRincianDetailPeer::LAST_UPDATE_IP => 20, NextRincianDetailPeer::TAHAP => 21, NextRincianDetailPeer::TAHAP_EDIT => 22, NextRincianDetailPeer::TAHAP_NEW => 23, NextRincianDetailPeer::STATUS_LELANG => 24, NextRincianDetailPeer::NOMOR_LELANG => 25, NextRincianDetailPeer::KOEFISIEN_SEMULA => 26, NextRincianDetailPeer::VOLUME_SEMULA => 27, NextRincianDetailPeer::HARGA_SEMULA => 28, NextRincianDetailPeer::TOTAL_SEMULA => 29, NextRincianDetailPeer::LOCK_SUBTITLE => 30, NextRincianDetailPeer::STATUS_HAPUS => 31, NextRincianDetailPeer::TAHUN => 32, NextRincianDetailPeer::KODE_LOKASI => 33, NextRincianDetailPeer::KECAMATAN => 34, NextRincianDetailPeer::REKENING_CODE_ASLI => 35, ),
		BasePeer::TYPE_FIELDNAME => array ('kegiatan_code' => 0, 'tipe' => 1, 'detail_no' => 2, 'rekening_code' => 3, 'komponen_id' => 4, 'detail_name' => 5, 'volume' => 6, 'keterangan_koefisien' => 7, 'subtitle' => 8, 'komponen_harga' => 9, 'komponen_harga_awal' => 10, 'komponen_name' => 11, 'satuan' => 12, 'pajak' => 13, 'unit_id' => 14, 'from_sub_kegiatan' => 15, 'sub' => 16, 'kode_sub' => 17, 'last_update_user' => 18, 'last_update_time' => 19, 'last_update_ip' => 20, 'tahap' => 21, 'tahap_edit' => 22, 'tahap_new' => 23, 'status_lelang' => 24, 'nomor_lelang' => 25, 'koefisien_semula' => 26, 'volume_semula' => 27, 'harga_semula' => 28, 'total_semula' => 29, 'lock_subtitle' => 30, 'status_hapus' => 31, 'tahun' => 32, 'kode_lokasi' => 33, 'kecamatan' => 34, 'rekening_code_asli' => 35, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/NextRincianDetailMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.NextRincianDetailMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = NextRincianDetailPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(NextRincianDetailPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(NextRincianDetailPeer::KEGIATAN_CODE);

		$criteria->addSelectColumn(NextRincianDetailPeer::TIPE);

		$criteria->addSelectColumn(NextRincianDetailPeer::DETAIL_NO);

		$criteria->addSelectColumn(NextRincianDetailPeer::REKENING_CODE);

		$criteria->addSelectColumn(NextRincianDetailPeer::KOMPONEN_ID);

		$criteria->addSelectColumn(NextRincianDetailPeer::DETAIL_NAME);

		$criteria->addSelectColumn(NextRincianDetailPeer::VOLUME);

		$criteria->addSelectColumn(NextRincianDetailPeer::KETERANGAN_KOEFISIEN);

		$criteria->addSelectColumn(NextRincianDetailPeer::SUBTITLE);

		$criteria->addSelectColumn(NextRincianDetailPeer::KOMPONEN_HARGA);

		$criteria->addSelectColumn(NextRincianDetailPeer::KOMPONEN_HARGA_AWAL);

		$criteria->addSelectColumn(NextRincianDetailPeer::KOMPONEN_NAME);

		$criteria->addSelectColumn(NextRincianDetailPeer::SATUAN);

		$criteria->addSelectColumn(NextRincianDetailPeer::PAJAK);

		$criteria->addSelectColumn(NextRincianDetailPeer::UNIT_ID);

		$criteria->addSelectColumn(NextRincianDetailPeer::FROM_SUB_KEGIATAN);

		$criteria->addSelectColumn(NextRincianDetailPeer::SUB);

		$criteria->addSelectColumn(NextRincianDetailPeer::KODE_SUB);

		$criteria->addSelectColumn(NextRincianDetailPeer::LAST_UPDATE_USER);

		$criteria->addSelectColumn(NextRincianDetailPeer::LAST_UPDATE_TIME);

		$criteria->addSelectColumn(NextRincianDetailPeer::LAST_UPDATE_IP);

		$criteria->addSelectColumn(NextRincianDetailPeer::TAHAP);

		$criteria->addSelectColumn(NextRincianDetailPeer::TAHAP_EDIT);

		$criteria->addSelectColumn(NextRincianDetailPeer::TAHAP_NEW);

		$criteria->addSelectColumn(NextRincianDetailPeer::STATUS_LELANG);

		$criteria->addSelectColumn(NextRincianDetailPeer::NOMOR_LELANG);

		$criteria->addSelectColumn(NextRincianDetailPeer::KOEFISIEN_SEMULA);

		$criteria->addSelectColumn(NextRincianDetailPeer::VOLUME_SEMULA);

		$criteria->addSelectColumn(NextRincianDetailPeer::HARGA_SEMULA);

		$criteria->addSelectColumn(NextRincianDetailPeer::TOTAL_SEMULA);

		$criteria->addSelectColumn(NextRincianDetailPeer::LOCK_SUBTITLE);

		$criteria->addSelectColumn(NextRincianDetailPeer::STATUS_HAPUS);

		$criteria->addSelectColumn(NextRincianDetailPeer::TAHUN);

		$criteria->addSelectColumn(NextRincianDetailPeer::KODE_LOKASI);

		$criteria->addSelectColumn(NextRincianDetailPeer::KECAMATAN);

		$criteria->addSelectColumn(NextRincianDetailPeer::REKENING_CODE_ASLI);

	}

	const COUNT = 'COUNT(ebudget.next_rincian_detail.KEGIATAN_CODE)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.next_rincian_detail.KEGIATAN_CODE)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(NextRincianDetailPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(NextRincianDetailPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = NextRincianDetailPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = NextRincianDetailPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return NextRincianDetailPeer::populateObjects(NextRincianDetailPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			NextRincianDetailPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = NextRincianDetailPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return NextRincianDetailPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(NextRincianDetailPeer::KEGIATAN_CODE);
			$selectCriteria->add(NextRincianDetailPeer::KEGIATAN_CODE, $criteria->remove(NextRincianDetailPeer::KEGIATAN_CODE), $comparison);

			$comparison = $criteria->getComparison(NextRincianDetailPeer::TIPE);
			$selectCriteria->add(NextRincianDetailPeer::TIPE, $criteria->remove(NextRincianDetailPeer::TIPE), $comparison);

			$comparison = $criteria->getComparison(NextRincianDetailPeer::DETAIL_NO);
			$selectCriteria->add(NextRincianDetailPeer::DETAIL_NO, $criteria->remove(NextRincianDetailPeer::DETAIL_NO), $comparison);

			$comparison = $criteria->getComparison(NextRincianDetailPeer::UNIT_ID);
			$selectCriteria->add(NextRincianDetailPeer::UNIT_ID, $criteria->remove(NextRincianDetailPeer::UNIT_ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(NextRincianDetailPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(NextRincianDetailPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof NextRincianDetail) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
												if(count($values) == count($values, COUNT_RECURSIVE))
			{
								$values = array($values);
			}
			$vals = array();
			foreach($values as $value)
			{

				$vals[0][] = $value[0];
				$vals[1][] = $value[1];
				$vals[2][] = $value[2];
				$vals[3][] = $value[3];
			}

			$criteria->add(NextRincianDetailPeer::KEGIATAN_CODE, $vals[0], Criteria::IN);
			$criteria->add(NextRincianDetailPeer::TIPE, $vals[1], Criteria::IN);
			$criteria->add(NextRincianDetailPeer::DETAIL_NO, $vals[2], Criteria::IN);
			$criteria->add(NextRincianDetailPeer::UNIT_ID, $vals[3], Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(NextRincianDetail $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(NextRincianDetailPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(NextRincianDetailPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(NextRincianDetailPeer::DATABASE_NAME, NextRincianDetailPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = NextRincianDetailPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK( $kegiatan_code, $tipe, $detail_no, $unit_id, $con = null) {
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$criteria = new Criteria();
		$criteria->add(NextRincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
		$criteria->add(NextRincianDetailPeer::TIPE, $tipe);
		$criteria->add(NextRincianDetailPeer::DETAIL_NO, $detail_no);
		$criteria->add(NextRincianDetailPeer::UNIT_ID, $unit_id);
		$v = NextRincianDetailPeer::doSelect($criteria, $con);

		return !empty($v) ? $v[0] : null;
	}
} 
if (Propel::isInit()) {
			try {
		BaseNextRincianDetailPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/NextRincianDetailMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.NextRincianDetailMapBuilder');
}
