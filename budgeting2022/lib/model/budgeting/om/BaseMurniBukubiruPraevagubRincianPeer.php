<?php


abstract class BaseMurniBukubiruPraevagubRincianPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.murni_bukubiru_praevagub_rincian';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.MurniBukubiruPraevagubRincian';

	
	const NUM_COLUMNS = 17;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const KEGIATAN_CODE = 'ebudget.murni_bukubiru_praevagub_rincian.KEGIATAN_CODE';

	
	const TIPE = 'ebudget.murni_bukubiru_praevagub_rincian.TIPE';

	
	const RINCIAN_CONFIRMED = 'ebudget.murni_bukubiru_praevagub_rincian.RINCIAN_CONFIRMED';

	
	const RINCIAN_CHANGED = 'ebudget.murni_bukubiru_praevagub_rincian.RINCIAN_CHANGED';

	
	const RINCIAN_SELESAI = 'ebudget.murni_bukubiru_praevagub_rincian.RINCIAN_SELESAI';

	
	const IP_ADDRESS = 'ebudget.murni_bukubiru_praevagub_rincian.IP_ADDRESS';

	
	const WAKTU_ACCESS = 'ebudget.murni_bukubiru_praevagub_rincian.WAKTU_ACCESS';

	
	const TARGET = 'ebudget.murni_bukubiru_praevagub_rincian.TARGET';

	
	const UNIT_ID = 'ebudget.murni_bukubiru_praevagub_rincian.UNIT_ID';

	
	const RINCIAN_LEVEL = 'ebudget.murni_bukubiru_praevagub_rincian.RINCIAN_LEVEL';

	
	const LOCK = 'ebudget.murni_bukubiru_praevagub_rincian.LOCK';

	
	const LAST_UPDATE_USER = 'ebudget.murni_bukubiru_praevagub_rincian.LAST_UPDATE_USER';

	
	const LAST_UPDATE_TIME = 'ebudget.murni_bukubiru_praevagub_rincian.LAST_UPDATE_TIME';

	
	const LAST_UPDATE_IP = 'ebudget.murni_bukubiru_praevagub_rincian.LAST_UPDATE_IP';

	
	const TAHAP = 'ebudget.murni_bukubiru_praevagub_rincian.TAHAP';

	
	const TAHUN = 'ebudget.murni_bukubiru_praevagub_rincian.TAHUN';

	
	const RINCIAN_REVISI_LEVEL = 'ebudget.murni_bukubiru_praevagub_rincian.RINCIAN_REVISI_LEVEL';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('KegiatanCode', 'Tipe', 'RincianConfirmed', 'RincianChanged', 'RincianSelesai', 'IpAddress', 'WaktuAccess', 'Target', 'UnitId', 'RincianLevel', 'Lock', 'LastUpdateUser', 'LastUpdateTime', 'LastUpdateIp', 'Tahap', 'Tahun', 'RincianRevisiLevel', ),
		BasePeer::TYPE_COLNAME => array (MurniBukubiruPraevagubRincianPeer::KEGIATAN_CODE, MurniBukubiruPraevagubRincianPeer::TIPE, MurniBukubiruPraevagubRincianPeer::RINCIAN_CONFIRMED, MurniBukubiruPraevagubRincianPeer::RINCIAN_CHANGED, MurniBukubiruPraevagubRincianPeer::RINCIAN_SELESAI, MurniBukubiruPraevagubRincianPeer::IP_ADDRESS, MurniBukubiruPraevagubRincianPeer::WAKTU_ACCESS, MurniBukubiruPraevagubRincianPeer::TARGET, MurniBukubiruPraevagubRincianPeer::UNIT_ID, MurniBukubiruPraevagubRincianPeer::RINCIAN_LEVEL, MurniBukubiruPraevagubRincianPeer::LOCK, MurniBukubiruPraevagubRincianPeer::LAST_UPDATE_USER, MurniBukubiruPraevagubRincianPeer::LAST_UPDATE_TIME, MurniBukubiruPraevagubRincianPeer::LAST_UPDATE_IP, MurniBukubiruPraevagubRincianPeer::TAHAP, MurniBukubiruPraevagubRincianPeer::TAHUN, MurniBukubiruPraevagubRincianPeer::RINCIAN_REVISI_LEVEL, ),
		BasePeer::TYPE_FIELDNAME => array ('kegiatan_code', 'tipe', 'rincian_confirmed', 'rincian_changed', 'rincian_selesai', 'ip_address', 'waktu_access', 'target', 'unit_id', 'rincian_level', 'lock', 'last_update_user', 'last_update_time', 'last_update_ip', 'tahap', 'tahun', 'rincian_revisi_level', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('KegiatanCode' => 0, 'Tipe' => 1, 'RincianConfirmed' => 2, 'RincianChanged' => 3, 'RincianSelesai' => 4, 'IpAddress' => 5, 'WaktuAccess' => 6, 'Target' => 7, 'UnitId' => 8, 'RincianLevel' => 9, 'Lock' => 10, 'LastUpdateUser' => 11, 'LastUpdateTime' => 12, 'LastUpdateIp' => 13, 'Tahap' => 14, 'Tahun' => 15, 'RincianRevisiLevel' => 16, ),
		BasePeer::TYPE_COLNAME => array (MurniBukubiruPraevagubRincianPeer::KEGIATAN_CODE => 0, MurniBukubiruPraevagubRincianPeer::TIPE => 1, MurniBukubiruPraevagubRincianPeer::RINCIAN_CONFIRMED => 2, MurniBukubiruPraevagubRincianPeer::RINCIAN_CHANGED => 3, MurniBukubiruPraevagubRincianPeer::RINCIAN_SELESAI => 4, MurniBukubiruPraevagubRincianPeer::IP_ADDRESS => 5, MurniBukubiruPraevagubRincianPeer::WAKTU_ACCESS => 6, MurniBukubiruPraevagubRincianPeer::TARGET => 7, MurniBukubiruPraevagubRincianPeer::UNIT_ID => 8, MurniBukubiruPraevagubRincianPeer::RINCIAN_LEVEL => 9, MurniBukubiruPraevagubRincianPeer::LOCK => 10, MurniBukubiruPraevagubRincianPeer::LAST_UPDATE_USER => 11, MurniBukubiruPraevagubRincianPeer::LAST_UPDATE_TIME => 12, MurniBukubiruPraevagubRincianPeer::LAST_UPDATE_IP => 13, MurniBukubiruPraevagubRincianPeer::TAHAP => 14, MurniBukubiruPraevagubRincianPeer::TAHUN => 15, MurniBukubiruPraevagubRincianPeer::RINCIAN_REVISI_LEVEL => 16, ),
		BasePeer::TYPE_FIELDNAME => array ('kegiatan_code' => 0, 'tipe' => 1, 'rincian_confirmed' => 2, 'rincian_changed' => 3, 'rincian_selesai' => 4, 'ip_address' => 5, 'waktu_access' => 6, 'target' => 7, 'unit_id' => 8, 'rincian_level' => 9, 'lock' => 10, 'last_update_user' => 11, 'last_update_time' => 12, 'last_update_ip' => 13, 'tahap' => 14, 'tahun' => 15, 'rincian_revisi_level' => 16, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/MurniBukubiruPraevagubRincianMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.MurniBukubiruPraevagubRincianMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = MurniBukubiruPraevagubRincianPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(MurniBukubiruPraevagubRincianPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(MurniBukubiruPraevagubRincianPeer::KEGIATAN_CODE);

		$criteria->addSelectColumn(MurniBukubiruPraevagubRincianPeer::TIPE);

		$criteria->addSelectColumn(MurniBukubiruPraevagubRincianPeer::RINCIAN_CONFIRMED);

		$criteria->addSelectColumn(MurniBukubiruPraevagubRincianPeer::RINCIAN_CHANGED);

		$criteria->addSelectColumn(MurniBukubiruPraevagubRincianPeer::RINCIAN_SELESAI);

		$criteria->addSelectColumn(MurniBukubiruPraevagubRincianPeer::IP_ADDRESS);

		$criteria->addSelectColumn(MurniBukubiruPraevagubRincianPeer::WAKTU_ACCESS);

		$criteria->addSelectColumn(MurniBukubiruPraevagubRincianPeer::TARGET);

		$criteria->addSelectColumn(MurniBukubiruPraevagubRincianPeer::UNIT_ID);

		$criteria->addSelectColumn(MurniBukubiruPraevagubRincianPeer::RINCIAN_LEVEL);

		$criteria->addSelectColumn(MurniBukubiruPraevagubRincianPeer::LOCK);

		$criteria->addSelectColumn(MurniBukubiruPraevagubRincianPeer::LAST_UPDATE_USER);

		$criteria->addSelectColumn(MurniBukubiruPraevagubRincianPeer::LAST_UPDATE_TIME);

		$criteria->addSelectColumn(MurniBukubiruPraevagubRincianPeer::LAST_UPDATE_IP);

		$criteria->addSelectColumn(MurniBukubiruPraevagubRincianPeer::TAHAP);

		$criteria->addSelectColumn(MurniBukubiruPraevagubRincianPeer::TAHUN);

		$criteria->addSelectColumn(MurniBukubiruPraevagubRincianPeer::RINCIAN_REVISI_LEVEL);

	}

	const COUNT = 'COUNT(ebudget.murni_bukubiru_praevagub_rincian.KEGIATAN_CODE)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.murni_bukubiru_praevagub_rincian.KEGIATAN_CODE)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(MurniBukubiruPraevagubRincianPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(MurniBukubiruPraevagubRincianPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = MurniBukubiruPraevagubRincianPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = MurniBukubiruPraevagubRincianPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return MurniBukubiruPraevagubRincianPeer::populateObjects(MurniBukubiruPraevagubRincianPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			MurniBukubiruPraevagubRincianPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = MurniBukubiruPraevagubRincianPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return MurniBukubiruPraevagubRincianPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(MurniBukubiruPraevagubRincianPeer::KEGIATAN_CODE);
			$selectCriteria->add(MurniBukubiruPraevagubRincianPeer::KEGIATAN_CODE, $criteria->remove(MurniBukubiruPraevagubRincianPeer::KEGIATAN_CODE), $comparison);

			$comparison = $criteria->getComparison(MurniBukubiruPraevagubRincianPeer::TIPE);
			$selectCriteria->add(MurniBukubiruPraevagubRincianPeer::TIPE, $criteria->remove(MurniBukubiruPraevagubRincianPeer::TIPE), $comparison);

			$comparison = $criteria->getComparison(MurniBukubiruPraevagubRincianPeer::UNIT_ID);
			$selectCriteria->add(MurniBukubiruPraevagubRincianPeer::UNIT_ID, $criteria->remove(MurniBukubiruPraevagubRincianPeer::UNIT_ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(MurniBukubiruPraevagubRincianPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(MurniBukubiruPraevagubRincianPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof MurniBukubiruPraevagubRincian) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
												if(count($values) == count($values, COUNT_RECURSIVE))
			{
								$values = array($values);
			}
			$vals = array();
			foreach($values as $value)
			{

				$vals[0][] = $value[0];
				$vals[1][] = $value[1];
				$vals[2][] = $value[2];
			}

			$criteria->add(MurniBukubiruPraevagubRincianPeer::KEGIATAN_CODE, $vals[0], Criteria::IN);
			$criteria->add(MurniBukubiruPraevagubRincianPeer::TIPE, $vals[1], Criteria::IN);
			$criteria->add(MurniBukubiruPraevagubRincianPeer::UNIT_ID, $vals[2], Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(MurniBukubiruPraevagubRincian $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(MurniBukubiruPraevagubRincianPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(MurniBukubiruPraevagubRincianPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(MurniBukubiruPraevagubRincianPeer::DATABASE_NAME, MurniBukubiruPraevagubRincianPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = MurniBukubiruPraevagubRincianPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK( $kegiatan_code, $tipe, $unit_id, $con = null) {
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$criteria = new Criteria();
		$criteria->add(MurniBukubiruPraevagubRincianPeer::KEGIATAN_CODE, $kegiatan_code);
		$criteria->add(MurniBukubiruPraevagubRincianPeer::TIPE, $tipe);
		$criteria->add(MurniBukubiruPraevagubRincianPeer::UNIT_ID, $unit_id);
		$v = MurniBukubiruPraevagubRincianPeer::doSelect($criteria, $con);

		return !empty($v) ? $v[0] : null;
	}
} 
if (Propel::isInit()) {
			try {
		BaseMurniBukubiruPraevagubRincianPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/MurniBukubiruPraevagubRincianMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.MurniBukubiruPraevagubRincianMapBuilder');
}
