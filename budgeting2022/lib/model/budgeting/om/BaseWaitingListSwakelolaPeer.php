<?php


abstract class BaseWaitingListSwakelolaPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.waitinglist_swakelola';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.WaitingListSwakelola';

	
	const NUM_COLUMNS = 29;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const ID_SWAKELOLA = 'ebudget.waitinglist_swakelola.ID_SWAKELOLA';

	
	const UNIT_ID = 'ebudget.waitinglist_swakelola.UNIT_ID';

	
	const KEGIATAN_CODE = 'ebudget.waitinglist_swakelola.KEGIATAN_CODE';

	
	const SUBTITLE = 'ebudget.waitinglist_swakelola.SUBTITLE';

	
	const KOMPONEN_ID = 'ebudget.waitinglist_swakelola.KOMPONEN_ID';

	
	const KOMPONEN_NAME = 'ebudget.waitinglist_swakelola.KOMPONEN_NAME';

	
	const KOMPONEN_LOKASI = 'ebudget.waitinglist_swakelola.KOMPONEN_LOKASI';

	
	const KOMPONEN_HARGA_AWAL = 'ebudget.waitinglist_swakelola.KOMPONEN_HARGA_AWAL';

	
	const PAJAK = 'ebudget.waitinglist_swakelola.PAJAK';

	
	const KOMPONEN_SATUAN = 'ebudget.waitinglist_swakelola.KOMPONEN_SATUAN';

	
	const KOMPONEN_REKENING = 'ebudget.waitinglist_swakelola.KOMPONEN_REKENING';

	
	const KOEFISIEN = 'ebudget.waitinglist_swakelola.KOEFISIEN';

	
	const VOLUME = 'ebudget.waitinglist_swakelola.VOLUME';

	
	const NILAI_ANGGARAN = 'ebudget.waitinglist_swakelola.NILAI_ANGGARAN';

	
	const TAHUN_INPUT = 'ebudget.waitinglist_swakelola.TAHUN_INPUT';

	
	const CREATED_AT = 'ebudget.waitinglist_swakelola.CREATED_AT';

	
	const UPDATED_AT = 'ebudget.waitinglist_swakelola.UPDATED_AT';

	
	const STATUS_HAPUS = 'ebudget.waitinglist_swakelola.STATUS_HAPUS';

	
	const STATUS_WAITING = 'ebudget.waitinglist_swakelola.STATUS_WAITING';

	
	const PRIORITAS = 'ebudget.waitinglist_swakelola.PRIORITAS';

	
	const KODE_RKA = 'ebudget.waitinglist_swakelola.KODE_RKA';

	
	const USER_PENGAMBIL = 'ebudget.waitinglist_swakelola.USER_PENGAMBIL';

	
	const NILAI_EE = 'ebudget.waitinglist_swakelola.NILAI_EE';

	
	const KETERANGAN = 'ebudget.waitinglist_swakelola.KETERANGAN';

	
	const KODE_JASMAS = 'ebudget.waitinglist_swakelola.KODE_JASMAS';

	
	const KECAMATAN = 'ebudget.waitinglist_swakelola.KECAMATAN';

	
	const KELURAHAN = 'ebudget.waitinglist_swakelola.KELURAHAN';

	
	const IS_MUSRENBANG = 'ebudget.waitinglist_swakelola.IS_MUSRENBANG';

	
	const NILAI_ANGGARAN_SEMULA = 'ebudget.waitinglist_swakelola.NILAI_ANGGARAN_SEMULA';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('IdSwakelola', 'UnitId', 'KegiatanCode', 'Subtitle', 'KomponenId', 'KomponenName', 'KomponenLokasi', 'KomponenHargaAwal', 'Pajak', 'KomponenSatuan', 'KomponenRekening', 'Koefisien', 'Volume', 'NilaiAnggaran', 'TahunInput', 'CreatedAt', 'UpdatedAt', 'StatusHapus', 'StatusWaiting', 'Prioritas', 'KodeRka', 'UserPengambil', 'NilaiEe', 'Keterangan', 'KodeJasmas', 'Kecamatan', 'Kelurahan', 'IsMusrenbang', 'NilaiAnggaranSemula', ),
		BasePeer::TYPE_COLNAME => array (WaitingListSwakelolaPeer::ID_SWAKELOLA, WaitingListSwakelolaPeer::UNIT_ID, WaitingListSwakelolaPeer::KEGIATAN_CODE, WaitingListSwakelolaPeer::SUBTITLE, WaitingListSwakelolaPeer::KOMPONEN_ID, WaitingListSwakelolaPeer::KOMPONEN_NAME, WaitingListSwakelolaPeer::KOMPONEN_LOKASI, WaitingListSwakelolaPeer::KOMPONEN_HARGA_AWAL, WaitingListSwakelolaPeer::PAJAK, WaitingListSwakelolaPeer::KOMPONEN_SATUAN, WaitingListSwakelolaPeer::KOMPONEN_REKENING, WaitingListSwakelolaPeer::KOEFISIEN, WaitingListSwakelolaPeer::VOLUME, WaitingListSwakelolaPeer::NILAI_ANGGARAN, WaitingListSwakelolaPeer::TAHUN_INPUT, WaitingListSwakelolaPeer::CREATED_AT, WaitingListSwakelolaPeer::UPDATED_AT, WaitingListSwakelolaPeer::STATUS_HAPUS, WaitingListSwakelolaPeer::STATUS_WAITING, WaitingListSwakelolaPeer::PRIORITAS, WaitingListSwakelolaPeer::KODE_RKA, WaitingListSwakelolaPeer::USER_PENGAMBIL, WaitingListSwakelolaPeer::NILAI_EE, WaitingListSwakelolaPeer::KETERANGAN, WaitingListSwakelolaPeer::KODE_JASMAS, WaitingListSwakelolaPeer::KECAMATAN, WaitingListSwakelolaPeer::KELURAHAN, WaitingListSwakelolaPeer::IS_MUSRENBANG, WaitingListSwakelolaPeer::NILAI_ANGGARAN_SEMULA, ),
		BasePeer::TYPE_FIELDNAME => array ('id_swakelola', 'unit_id', 'kegiatan_code', 'subtitle', 'komponen_id', 'komponen_name', 'komponen_lokasi', 'komponen_harga_awal', 'pajak', 'komponen_satuan', 'komponen_rekening', 'koefisien', 'volume', 'nilai_anggaran', 'tahun_input', 'created_at', 'updated_at', 'status_hapus', 'status_waiting', 'prioritas', 'kode_rka', 'user_pengambil', 'nilai_ee', 'keterangan', 'kode_jasmas', 'kecamatan', 'kelurahan', 'is_musrenbang', 'nilai_anggaran_semula', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('IdSwakelola' => 0, 'UnitId' => 1, 'KegiatanCode' => 2, 'Subtitle' => 3, 'KomponenId' => 4, 'KomponenName' => 5, 'KomponenLokasi' => 6, 'KomponenHargaAwal' => 7, 'Pajak' => 8, 'KomponenSatuan' => 9, 'KomponenRekening' => 10, 'Koefisien' => 11, 'Volume' => 12, 'NilaiAnggaran' => 13, 'TahunInput' => 14, 'CreatedAt' => 15, 'UpdatedAt' => 16, 'StatusHapus' => 17, 'StatusWaiting' => 18, 'Prioritas' => 19, 'KodeRka' => 20, 'UserPengambil' => 21, 'NilaiEe' => 22, 'Keterangan' => 23, 'KodeJasmas' => 24, 'Kecamatan' => 25, 'Kelurahan' => 26, 'IsMusrenbang' => 27, 'NilaiAnggaranSemula' => 28, ),
		BasePeer::TYPE_COLNAME => array (WaitingListSwakelolaPeer::ID_SWAKELOLA => 0, WaitingListSwakelolaPeer::UNIT_ID => 1, WaitingListSwakelolaPeer::KEGIATAN_CODE => 2, WaitingListSwakelolaPeer::SUBTITLE => 3, WaitingListSwakelolaPeer::KOMPONEN_ID => 4, WaitingListSwakelolaPeer::KOMPONEN_NAME => 5, WaitingListSwakelolaPeer::KOMPONEN_LOKASI => 6, WaitingListSwakelolaPeer::KOMPONEN_HARGA_AWAL => 7, WaitingListSwakelolaPeer::PAJAK => 8, WaitingListSwakelolaPeer::KOMPONEN_SATUAN => 9, WaitingListSwakelolaPeer::KOMPONEN_REKENING => 10, WaitingListSwakelolaPeer::KOEFISIEN => 11, WaitingListSwakelolaPeer::VOLUME => 12, WaitingListSwakelolaPeer::NILAI_ANGGARAN => 13, WaitingListSwakelolaPeer::TAHUN_INPUT => 14, WaitingListSwakelolaPeer::CREATED_AT => 15, WaitingListSwakelolaPeer::UPDATED_AT => 16, WaitingListSwakelolaPeer::STATUS_HAPUS => 17, WaitingListSwakelolaPeer::STATUS_WAITING => 18, WaitingListSwakelolaPeer::PRIORITAS => 19, WaitingListSwakelolaPeer::KODE_RKA => 20, WaitingListSwakelolaPeer::USER_PENGAMBIL => 21, WaitingListSwakelolaPeer::NILAI_EE => 22, WaitingListSwakelolaPeer::KETERANGAN => 23, WaitingListSwakelolaPeer::KODE_JASMAS => 24, WaitingListSwakelolaPeer::KECAMATAN => 25, WaitingListSwakelolaPeer::KELURAHAN => 26, WaitingListSwakelolaPeer::IS_MUSRENBANG => 27, WaitingListSwakelolaPeer::NILAI_ANGGARAN_SEMULA => 28, ),
		BasePeer::TYPE_FIELDNAME => array ('id_swakelola' => 0, 'unit_id' => 1, 'kegiatan_code' => 2, 'subtitle' => 3, 'komponen_id' => 4, 'komponen_name' => 5, 'komponen_lokasi' => 6, 'komponen_harga_awal' => 7, 'pajak' => 8, 'komponen_satuan' => 9, 'komponen_rekening' => 10, 'koefisien' => 11, 'volume' => 12, 'nilai_anggaran' => 13, 'tahun_input' => 14, 'created_at' => 15, 'updated_at' => 16, 'status_hapus' => 17, 'status_waiting' => 18, 'prioritas' => 19, 'kode_rka' => 20, 'user_pengambil' => 21, 'nilai_ee' => 22, 'keterangan' => 23, 'kode_jasmas' => 24, 'kecamatan' => 25, 'kelurahan' => 26, 'is_musrenbang' => 27, 'nilai_anggaran_semula' => 28, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/WaitingListSwakelolaMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.WaitingListSwakelolaMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = WaitingListSwakelolaPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(WaitingListSwakelolaPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(WaitingListSwakelolaPeer::ID_SWAKELOLA);

		$criteria->addSelectColumn(WaitingListSwakelolaPeer::UNIT_ID);

		$criteria->addSelectColumn(WaitingListSwakelolaPeer::KEGIATAN_CODE);

		$criteria->addSelectColumn(WaitingListSwakelolaPeer::SUBTITLE);

		$criteria->addSelectColumn(WaitingListSwakelolaPeer::KOMPONEN_ID);

		$criteria->addSelectColumn(WaitingListSwakelolaPeer::KOMPONEN_NAME);

		$criteria->addSelectColumn(WaitingListSwakelolaPeer::KOMPONEN_LOKASI);

		$criteria->addSelectColumn(WaitingListSwakelolaPeer::KOMPONEN_HARGA_AWAL);

		$criteria->addSelectColumn(WaitingListSwakelolaPeer::PAJAK);

		$criteria->addSelectColumn(WaitingListSwakelolaPeer::KOMPONEN_SATUAN);

		$criteria->addSelectColumn(WaitingListSwakelolaPeer::KOMPONEN_REKENING);

		$criteria->addSelectColumn(WaitingListSwakelolaPeer::KOEFISIEN);

		$criteria->addSelectColumn(WaitingListSwakelolaPeer::VOLUME);

		$criteria->addSelectColumn(WaitingListSwakelolaPeer::NILAI_ANGGARAN);

		$criteria->addSelectColumn(WaitingListSwakelolaPeer::TAHUN_INPUT);

		$criteria->addSelectColumn(WaitingListSwakelolaPeer::CREATED_AT);

		$criteria->addSelectColumn(WaitingListSwakelolaPeer::UPDATED_AT);

		$criteria->addSelectColumn(WaitingListSwakelolaPeer::STATUS_HAPUS);

		$criteria->addSelectColumn(WaitingListSwakelolaPeer::STATUS_WAITING);

		$criteria->addSelectColumn(WaitingListSwakelolaPeer::PRIORITAS);

		$criteria->addSelectColumn(WaitingListSwakelolaPeer::KODE_RKA);

		$criteria->addSelectColumn(WaitingListSwakelolaPeer::USER_PENGAMBIL);

		$criteria->addSelectColumn(WaitingListSwakelolaPeer::NILAI_EE);

		$criteria->addSelectColumn(WaitingListSwakelolaPeer::KETERANGAN);

		$criteria->addSelectColumn(WaitingListSwakelolaPeer::KODE_JASMAS);

		$criteria->addSelectColumn(WaitingListSwakelolaPeer::KECAMATAN);

		$criteria->addSelectColumn(WaitingListSwakelolaPeer::KELURAHAN);

		$criteria->addSelectColumn(WaitingListSwakelolaPeer::IS_MUSRENBANG);

		$criteria->addSelectColumn(WaitingListSwakelolaPeer::NILAI_ANGGARAN_SEMULA);

	}

	const COUNT = 'COUNT(ebudget.waitinglist_swakelola.ID_SWAKELOLA)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.waitinglist_swakelola.ID_SWAKELOLA)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(WaitingListSwakelolaPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(WaitingListSwakelolaPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = WaitingListSwakelolaPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = WaitingListSwakelolaPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return WaitingListSwakelolaPeer::populateObjects(WaitingListSwakelolaPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			WaitingListSwakelolaPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = WaitingListSwakelolaPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return WaitingListSwakelolaPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}

		$criteria->remove(WaitingListSwakelolaPeer::ID_SWAKELOLA); 

				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(WaitingListSwakelolaPeer::ID_SWAKELOLA);
			$selectCriteria->add(WaitingListSwakelolaPeer::ID_SWAKELOLA, $criteria->remove(WaitingListSwakelolaPeer::ID_SWAKELOLA), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(WaitingListSwakelolaPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(WaitingListSwakelolaPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof WaitingListSwakelola) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(WaitingListSwakelolaPeer::ID_SWAKELOLA, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(WaitingListSwakelola $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(WaitingListSwakelolaPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(WaitingListSwakelolaPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(WaitingListSwakelolaPeer::DATABASE_NAME, WaitingListSwakelolaPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = WaitingListSwakelolaPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(WaitingListSwakelolaPeer::DATABASE_NAME);

		$criteria->add(WaitingListSwakelolaPeer::ID_SWAKELOLA, $pk);


		$v = WaitingListSwakelolaPeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(WaitingListSwakelolaPeer::ID_SWAKELOLA, $pks, Criteria::IN);
			$objs = WaitingListSwakelolaPeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BaseWaitingListSwakelolaPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/WaitingListSwakelolaMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.WaitingListSwakelolaMapBuilder');
}
