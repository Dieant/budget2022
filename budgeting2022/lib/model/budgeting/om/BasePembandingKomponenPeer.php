<?php


abstract class BasePembandingKomponenPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.pembanding_komponen';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.PembandingKomponen';

	
	const NUM_COLUMNS = 11;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const ID_PEMBANDING_KEGIATAN = 'ebudget.pembanding_komponen.ID_PEMBANDING_KEGIATAN';

	
	const DETAIL_NO = 'ebudget.pembanding_komponen.DETAIL_NO';

	
	const REKENING_CODE = 'ebudget.pembanding_komponen.REKENING_CODE';

	
	const KOMPONEN_NAME = 'ebudget.pembanding_komponen.KOMPONEN_NAME';

	
	const DETAIL_NAME = 'ebudget.pembanding_komponen.DETAIL_NAME';

	
	const VOLUME = 'ebudget.pembanding_komponen.VOLUME';

	
	const SATUAN = 'ebudget.pembanding_komponen.SATUAN';

	
	const KETERANGAN_KOEFISIEN = 'ebudget.pembanding_komponen.KETERANGAN_KOEFISIEN';

	
	const SUBTITLE = 'ebudget.pembanding_komponen.SUBTITLE';

	
	const NILAI_ANGGARAN = 'ebudget.pembanding_komponen.NILAI_ANGGARAN';

	
	const KOMPONEN_HARGA = 'ebudget.pembanding_komponen.KOMPONEN_HARGA';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('IdPembandingKegiatan', 'DetailNo', 'RekeningCode', 'KomponenName', 'DetailName', 'Volume', 'Satuan', 'KeteranganKoefisien', 'Subtitle', 'NilaiAnggaran', 'KomponenHarga', ),
		BasePeer::TYPE_COLNAME => array (PembandingKomponenPeer::ID_PEMBANDING_KEGIATAN, PembandingKomponenPeer::DETAIL_NO, PembandingKomponenPeer::REKENING_CODE, PembandingKomponenPeer::KOMPONEN_NAME, PembandingKomponenPeer::DETAIL_NAME, PembandingKomponenPeer::VOLUME, PembandingKomponenPeer::SATUAN, PembandingKomponenPeer::KETERANGAN_KOEFISIEN, PembandingKomponenPeer::SUBTITLE, PembandingKomponenPeer::NILAI_ANGGARAN, PembandingKomponenPeer::KOMPONEN_HARGA, ),
		BasePeer::TYPE_FIELDNAME => array ('id_pembanding_kegiatan', 'detail_no', 'rekening_code', 'komponen_name', 'detail_name', 'volume', 'satuan', 'keterangan_koefisien', 'subtitle', 'nilai_anggaran', 'komponen_harga', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('IdPembandingKegiatan' => 0, 'DetailNo' => 1, 'RekeningCode' => 2, 'KomponenName' => 3, 'DetailName' => 4, 'Volume' => 5, 'Satuan' => 6, 'KeteranganKoefisien' => 7, 'Subtitle' => 8, 'NilaiAnggaran' => 9, 'KomponenHarga' => 10, ),
		BasePeer::TYPE_COLNAME => array (PembandingKomponenPeer::ID_PEMBANDING_KEGIATAN => 0, PembandingKomponenPeer::DETAIL_NO => 1, PembandingKomponenPeer::REKENING_CODE => 2, PembandingKomponenPeer::KOMPONEN_NAME => 3, PembandingKomponenPeer::DETAIL_NAME => 4, PembandingKomponenPeer::VOLUME => 5, PembandingKomponenPeer::SATUAN => 6, PembandingKomponenPeer::KETERANGAN_KOEFISIEN => 7, PembandingKomponenPeer::SUBTITLE => 8, PembandingKomponenPeer::NILAI_ANGGARAN => 9, PembandingKomponenPeer::KOMPONEN_HARGA => 10, ),
		BasePeer::TYPE_FIELDNAME => array ('id_pembanding_kegiatan' => 0, 'detail_no' => 1, 'rekening_code' => 2, 'komponen_name' => 3, 'detail_name' => 4, 'volume' => 5, 'satuan' => 6, 'keterangan_koefisien' => 7, 'subtitle' => 8, 'nilai_anggaran' => 9, 'komponen_harga' => 10, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/PembandingKomponenMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.PembandingKomponenMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = PembandingKomponenPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(PembandingKomponenPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(PembandingKomponenPeer::ID_PEMBANDING_KEGIATAN);

		$criteria->addSelectColumn(PembandingKomponenPeer::DETAIL_NO);

		$criteria->addSelectColumn(PembandingKomponenPeer::REKENING_CODE);

		$criteria->addSelectColumn(PembandingKomponenPeer::KOMPONEN_NAME);

		$criteria->addSelectColumn(PembandingKomponenPeer::DETAIL_NAME);

		$criteria->addSelectColumn(PembandingKomponenPeer::VOLUME);

		$criteria->addSelectColumn(PembandingKomponenPeer::SATUAN);

		$criteria->addSelectColumn(PembandingKomponenPeer::KETERANGAN_KOEFISIEN);

		$criteria->addSelectColumn(PembandingKomponenPeer::SUBTITLE);

		$criteria->addSelectColumn(PembandingKomponenPeer::NILAI_ANGGARAN);

		$criteria->addSelectColumn(PembandingKomponenPeer::KOMPONEN_HARGA);

	}

	const COUNT = 'COUNT(ebudget.pembanding_komponen.ID_PEMBANDING_KEGIATAN)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.pembanding_komponen.ID_PEMBANDING_KEGIATAN)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(PembandingKomponenPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(PembandingKomponenPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = PembandingKomponenPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = PembandingKomponenPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return PembandingKomponenPeer::populateObjects(PembandingKomponenPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			PembandingKomponenPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = PembandingKomponenPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return PembandingKomponenPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(PembandingKomponenPeer::ID_PEMBANDING_KEGIATAN);
			$selectCriteria->add(PembandingKomponenPeer::ID_PEMBANDING_KEGIATAN, $criteria->remove(PembandingKomponenPeer::ID_PEMBANDING_KEGIATAN), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(PembandingKomponenPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(PembandingKomponenPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof PembandingKomponen) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(PembandingKomponenPeer::ID_PEMBANDING_KEGIATAN, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(PembandingKomponen $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(PembandingKomponenPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(PembandingKomponenPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(PembandingKomponenPeer::DATABASE_NAME, PembandingKomponenPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = PembandingKomponenPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(PembandingKomponenPeer::DATABASE_NAME);

		$criteria->add(PembandingKomponenPeer::ID_PEMBANDING_KEGIATAN, $pk);


		$v = PembandingKomponenPeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(PembandingKomponenPeer::ID_PEMBANDING_KEGIATAN, $pks, Criteria::IN);
			$objs = PembandingKomponenPeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BasePembandingKomponenPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/PembandingKomponenMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.PembandingKomponenMapBuilder');
}
