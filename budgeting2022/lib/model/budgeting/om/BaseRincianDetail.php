<?php


abstract class BaseRincianDetail extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $kegiatan_code;


	
	protected $tipe;


	
	protected $detail_no;


	
	protected $rekening_code;


	
	protected $komponen_id;


	
	protected $detail_name;


	
	protected $volume;


	
	protected $keterangan_koefisien;


	
	protected $subtitle;


	
	protected $komponen_harga;


	
	protected $komponen_harga_awal;


	
	protected $komponen_name;


	
	protected $satuan;


	
	protected $pajak = 0;


	
	protected $unit_id;


	
	protected $from_sub_kegiatan;


	
	protected $sub;


	
	protected $kode_sub;


	
	protected $last_update_user;


	
	protected $last_update_time;


	
	protected $last_update_ip;


	
	protected $tahap;


	
	protected $tahap_edit;


	
	protected $tahap_new;


	
	protected $status_lelang;


	
	protected $nomor_lelang;


	
	protected $koefisien_semula;


	
	protected $volume_semula;


	
	protected $harga_semula;


	
	protected $total_semula;


	
	protected $lock_subtitle;


	
	protected $status_hapus = false;


	
	protected $tahun;


	
	protected $kode_lokasi;


	
	protected $kecamatan;


	
	protected $rekening_code_asli;


	
	protected $note_skpd;


	
	protected $note_peneliti;


	
	protected $nilai_anggaran;


	
	protected $is_blud;


	
	protected $is_kapitasi_bpjs;


	
	protected $lokasi_kecamatan;


	
	protected $lokasi_kelurahan;


	
	protected $ob;


	
	protected $ob_from_id;


	
	protected $is_per_komponen;


	
	protected $kegiatan_code_asal;


	
	protected $th_ke_multiyears;


	
	protected $harga_sebelum_sisa_lelang;


	
	protected $is_musrenbang = false;


	
	protected $sub_id_asal;


	
	protected $subtitle_asal;


	
	protected $kode_sub_asal;


	
	protected $sub_asal;


	
	protected $last_edit_time;


	
	protected $is_potong_bpjs = false;


	
	protected $is_iuran_bpjs = false;


	
	protected $status_ob = 0;


	
	protected $ob_parent;


	
	protected $ob_alokasi_baru;


	
	protected $is_hibah = false;


	
	protected $akrual_code;


	
	protected $tipe2;


	
	protected $status_level = 0;


	
	protected $status_level_tolak;


	
	protected $status_sisipan = false;


	
	protected $is_tapd_setuju = false;


	
	protected $is_bappeko_setuju = false;


	
	protected $is_penyelia_setuju = false;


	
	protected $note_tapd;


	
	protected $note_bappeko;


	
	protected $satuan_semula;


	
	protected $id_lokasi;


	
	protected $detail_kegiatan;


	
	protected $detail_kegiatan_semula;


	
	protected $status_komponen_baru = false;


	
	protected $status_komponen_berubah = false;


	
	protected $approve_unlock_harga;


	
	protected $tipe_lelang;


	
	protected $is_hpsp = false;


	
	protected $is_dak = false;


	
	protected $is_bos = false;


	
	protected $is_bobda = false;


	
	protected $is_narsum = false;


	
	protected $is_bagian_hukum_setuju;


	
	protected $is_inspektorat_setuju;


	
	protected $is_badan_kepegawaian_setuju;


	
	protected $is_lppa_setuju;


	
	protected $prioritas_wali;


	
	protected $is_output;


	
	protected $is_bagian_organisasi_setuju = false;


	
	protected $is_asisten1_setuju;


	
	protected $is_asisten2_setuju;


	
	protected $is_asisten3_setuju;


	
	protected $is_sekda_setuju;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getKegiatanCode()
	{

		return $this->kegiatan_code;
	}

	
	public function getTipe()
	{

		return $this->tipe;
	}

	
	public function getDetailNo()
	{

		return $this->detail_no;
	}

	
	public function getRekeningCode()
	{

		return $this->rekening_code;
	}

	
	public function getKomponenId()
	{

		return $this->komponen_id;
	}

	
	public function getDetailName()
	{

		return $this->detail_name;
	}

	
	public function getVolume()
	{

		return $this->volume;
	}

	
	public function getKeteranganKoefisien()
	{

		return $this->keterangan_koefisien;
	}

	
	public function getSubtitle()
	{

		return $this->subtitle;
	}

	
	public function getKomponenHarga()
	{

		return $this->komponen_harga;
	}

	
	public function getKomponenHargaAwal()
	{

		return $this->komponen_harga_awal;
	}

	
	public function getKomponenName()
	{

		return $this->komponen_name;
	}

	
	public function getSatuan()
	{

		return $this->satuan;
	}

	
	public function getPajak()
	{

		return $this->pajak;
	}

	
	public function getUnitId()
	{

		return $this->unit_id;
	}

	
	public function getFromSubKegiatan()
	{

		return $this->from_sub_kegiatan;
	}

	
	public function getSub()
	{

		return $this->sub;
	}

	
	public function getKodeSub()
	{

		return $this->kode_sub;
	}

	
	public function getLastUpdateUser()
	{

		return $this->last_update_user;
	}

	
	public function getLastUpdateTime($format = 'Y-m-d H:i:s')
	{

		if ($this->last_update_time === null || $this->last_update_time === '') {
			return null;
		} elseif (!is_int($this->last_update_time)) {
						$ts = strtotime($this->last_update_time);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [last_update_time] as date/time value: " . var_export($this->last_update_time, true));
			}
		} else {
			$ts = $this->last_update_time;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getLastUpdateIp()
	{

		return $this->last_update_ip;
	}

	
	public function getTahap()
	{

		return $this->tahap;
	}

	
	public function getTahapEdit()
	{

		return $this->tahap_edit;
	}

	
	public function getTahapNew()
	{

		return $this->tahap_new;
	}

	
	public function getStatusLelang()
	{

		return $this->status_lelang;
	}

	
	public function getNomorLelang()
	{

		return $this->nomor_lelang;
	}

	
	public function getKoefisienSemula()
	{

		return $this->koefisien_semula;
	}

	
	public function getVolumeSemula()
	{

		return $this->volume_semula;
	}

	
	public function getHargaSemula()
	{

		return $this->harga_semula;
	}

	
	public function getTotalSemula()
	{

		return $this->total_semula;
	}

	
	public function getLockSubtitle()
	{

		return $this->lock_subtitle;
	}

	
	public function getStatusHapus()
	{

		return $this->status_hapus;
	}

	
	public function getTahun()
	{

		return $this->tahun;
	}

	
	public function getKodeLokasi()
	{

		return $this->kode_lokasi;
	}

	
	public function getKecamatan()
	{

		return $this->kecamatan;
	}

	
	public function getRekeningCodeAsli()
	{

		return $this->rekening_code_asli;
	}

	
	public function getNoteSkpd()
	{

		return $this->note_skpd;
	}

	
	public function getNotePeneliti()
	{

		return $this->note_peneliti;
	}

	
	public function getNilaiAnggaran()
	{

		return $this->nilai_anggaran;
	}

	
	public function getIsBlud()
	{

		return $this->is_blud;
	}

	
	public function getIsKapitasiBpjs()
	{

		return $this->is_kapitasi_bpjs;
	}

	
	public function getLokasiKecamatan()
	{

		return $this->lokasi_kecamatan;
	}

	
	public function getLokasiKelurahan()
	{

		return $this->lokasi_kelurahan;
	}

	
	public function getOb()
	{

		return $this->ob;
	}

	
	public function getObFromId()
	{

		return $this->ob_from_id;
	}

	
	public function getIsPerKomponen()
	{

		return $this->is_per_komponen;
	}

	
	public function getKegiatanCodeAsal()
	{

		return $this->kegiatan_code_asal;
	}

	
	public function getThKeMultiyears()
	{

		return $this->th_ke_multiyears;
	}

	
	public function getHargaSebelumSisaLelang()
	{

		return $this->harga_sebelum_sisa_lelang;
	}

	
	public function getIsMusrenbang()
	{

		return $this->is_musrenbang;
	}

	
	public function getSubIdAsal()
	{

		return $this->sub_id_asal;
	}

	
	public function getSubtitleAsal()
	{

		return $this->subtitle_asal;
	}

	
	public function getKodeSubAsal()
	{

		return $this->kode_sub_asal;
	}

	
	public function getSubAsal()
	{

		return $this->sub_asal;
	}

	
	public function getLastEditTime($format = 'Y-m-d H:i:s')
	{

		if ($this->last_edit_time === null || $this->last_edit_time === '') {
			return null;
		} elseif (!is_int($this->last_edit_time)) {
						$ts = strtotime($this->last_edit_time);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [last_edit_time] as date/time value: " . var_export($this->last_edit_time, true));
			}
		} else {
			$ts = $this->last_edit_time;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getIsPotongBpjs()
	{

		return $this->is_potong_bpjs;
	}

	
	public function getIsIuranBpjs()
	{

		return $this->is_iuran_bpjs;
	}

	
	public function getStatusOb()
	{

		return $this->status_ob;
	}

	
	public function getObParent()
	{

		return $this->ob_parent;
	}

	
	public function getObAlokasiBaru()
	{

		return $this->ob_alokasi_baru;
	}

	
	public function getIsHibah()
	{

		return $this->is_hibah;
	}

	
	public function getAkrualCode()
	{

		return $this->akrual_code;
	}

	
	public function getTipe2()
	{

		return $this->tipe2;
	}

	
	public function getStatusLevel()
	{

		return $this->status_level;
	}

	
	public function getStatusLevelTolak()
	{

		return $this->status_level_tolak;
	}

	
	public function getStatusSisipan()
	{

		return $this->status_sisipan;
	}

	
	public function getIsTapdSetuju()
	{

		return $this->is_tapd_setuju;
	}

	
	public function getIsBappekoSetuju()
	{

		return $this->is_bappeko_setuju;
	}

	
	public function getIsPenyeliaSetuju()
	{

		return $this->is_penyelia_setuju;
	}

	
	public function getNoteTapd()
	{

		return $this->note_tapd;
	}

	
	public function getNoteBappeko()
	{

		return $this->note_bappeko;
	}

	
	public function getSatuanSemula()
	{

		return $this->satuan_semula;
	}

	
	public function getIdLokasi()
	{

		return $this->id_lokasi;
	}

	
	public function getDetailKegiatan()
	{

		return $this->detail_kegiatan;
	}

	
	public function getDetailKegiatanSemula()
	{

		return $this->detail_kegiatan_semula;
	}

	
	public function getStatusKomponenBaru()
	{

		return $this->status_komponen_baru;
	}

	
	public function getStatusKomponenBerubah()
	{

		return $this->status_komponen_berubah;
	}

	
	public function getApproveUnlockHarga()
	{

		return $this->approve_unlock_harga;
	}

	
	public function getTipeLelang()
	{

		return $this->tipe_lelang;
	}

	
	public function getIsHpsp()
	{

		return $this->is_hpsp;
	}

	
	public function getIsDak()
	{

		return $this->is_dak;
	}

	
	public function getIsBos()
	{

		return $this->is_bos;
	}

	
	public function getIsBobda()
	{

		return $this->is_bobda;
	}

	
	public function getIsNarsum()
	{

		return $this->is_narsum;
	}

	
	public function getIsBagianHukumSetuju()
	{

		return $this->is_bagian_hukum_setuju;
	}

	
	public function getIsInspektoratSetuju()
	{

		return $this->is_inspektorat_setuju;
	}

	
	public function getIsBadanKepegawaianSetuju()
	{

		return $this->is_badan_kepegawaian_setuju;
	}

	
	public function getIsLppaSetuju()
	{

		return $this->is_lppa_setuju;
	}

	
	public function getPrioritasWali()
	{

		return $this->prioritas_wali;
	}

	
	public function getIsOutput()
	{

		return $this->is_output;
	}

	
	public function getIsBagianOrganisasiSetuju()
	{

		return $this->is_bagian_organisasi_setuju;
	}

	
	public function getIsAsisten1Setuju()
	{

		return $this->is_asisten1_setuju;
	}

	
	public function getIsAsisten2Setuju()
	{

		return $this->is_asisten2_setuju;
	}

	
	public function getIsAsisten3Setuju()
	{

		return $this->is_asisten3_setuju;
	}

	
	public function getIsSekdaSetuju()
	{

		return $this->is_sekda_setuju;
	}

	
	public function setKegiatanCode($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kegiatan_code !== $v) {
			$this->kegiatan_code = $v;
			$this->modifiedColumns[] = RincianDetailPeer::KEGIATAN_CODE;
		}

	} 
	
	public function setTipe($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->tipe !== $v) {
			$this->tipe = $v;
			$this->modifiedColumns[] = RincianDetailPeer::TIPE;
		}

	} 
	
	public function setDetailNo($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->detail_no !== $v) {
			$this->detail_no = $v;
			$this->modifiedColumns[] = RincianDetailPeer::DETAIL_NO;
		}

	} 
	
	public function setRekeningCode($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->rekening_code !== $v) {
			$this->rekening_code = $v;
			$this->modifiedColumns[] = RincianDetailPeer::REKENING_CODE;
		}

	} 
	
	public function setKomponenId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->komponen_id !== $v) {
			$this->komponen_id = $v;
			$this->modifiedColumns[] = RincianDetailPeer::KOMPONEN_ID;
		}

	} 
	
	public function setDetailName($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->detail_name !== $v) {
			$this->detail_name = $v;
			$this->modifiedColumns[] = RincianDetailPeer::DETAIL_NAME;
		}

	} 
	
	public function setVolume($v)
	{

		if ($this->volume !== $v) {
			$this->volume = $v;
			$this->modifiedColumns[] = RincianDetailPeer::VOLUME;
		}

	} 
	
	public function setKeteranganKoefisien($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->keterangan_koefisien !== $v) {
			$this->keterangan_koefisien = $v;
			$this->modifiedColumns[] = RincianDetailPeer::KETERANGAN_KOEFISIEN;
		}

	} 
	
	public function setSubtitle($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->subtitle !== $v) {
			$this->subtitle = $v;
			$this->modifiedColumns[] = RincianDetailPeer::SUBTITLE;
		}

	} 
	
	public function setKomponenHarga($v)
	{

		if ($this->komponen_harga !== $v) {
			$this->komponen_harga = $v;
			$this->modifiedColumns[] = RincianDetailPeer::KOMPONEN_HARGA;
		}

	} 
	
	public function setKomponenHargaAwal($v)
	{

		if ($this->komponen_harga_awal !== $v) {
			$this->komponen_harga_awal = $v;
			$this->modifiedColumns[] = RincianDetailPeer::KOMPONEN_HARGA_AWAL;
		}

	} 
	
	public function setKomponenName($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->komponen_name !== $v) {
			$this->komponen_name = $v;
			$this->modifiedColumns[] = RincianDetailPeer::KOMPONEN_NAME;
		}

	} 
	
	public function setSatuan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->satuan !== $v) {
			$this->satuan = $v;
			$this->modifiedColumns[] = RincianDetailPeer::SATUAN;
		}

	} 
	
	public function setPajak($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->pajak !== $v || $v === 0) {
			$this->pajak = $v;
			$this->modifiedColumns[] = RincianDetailPeer::PAJAK;
		}

	} 
	
	public function setUnitId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->unit_id !== $v) {
			$this->unit_id = $v;
			$this->modifiedColumns[] = RincianDetailPeer::UNIT_ID;
		}

	} 
	
	public function setFromSubKegiatan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->from_sub_kegiatan !== $v) {
			$this->from_sub_kegiatan = $v;
			$this->modifiedColumns[] = RincianDetailPeer::FROM_SUB_KEGIATAN;
		}

	} 
	
	public function setSub($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->sub !== $v) {
			$this->sub = $v;
			$this->modifiedColumns[] = RincianDetailPeer::SUB;
		}

	} 
	
	public function setKodeSub($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_sub !== $v) {
			$this->kode_sub = $v;
			$this->modifiedColumns[] = RincianDetailPeer::KODE_SUB;
		}

	} 
	
	public function setLastUpdateUser($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->last_update_user !== $v) {
			$this->last_update_user = $v;
			$this->modifiedColumns[] = RincianDetailPeer::LAST_UPDATE_USER;
		}

	} 
	
	public function setLastUpdateTime($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [last_update_time] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->last_update_time !== $ts) {
			$this->last_update_time = $ts;
			$this->modifiedColumns[] = RincianDetailPeer::LAST_UPDATE_TIME;
		}

	} 
	
	public function setLastUpdateIp($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->last_update_ip !== $v) {
			$this->last_update_ip = $v;
			$this->modifiedColumns[] = RincianDetailPeer::LAST_UPDATE_IP;
		}

	} 
	
	public function setTahap($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->tahap !== $v) {
			$this->tahap = $v;
			$this->modifiedColumns[] = RincianDetailPeer::TAHAP;
		}

	} 
	
	public function setTahapEdit($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->tahap_edit !== $v) {
			$this->tahap_edit = $v;
			$this->modifiedColumns[] = RincianDetailPeer::TAHAP_EDIT;
		}

	} 
	
	public function setTahapNew($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->tahap_new !== $v) {
			$this->tahap_new = $v;
			$this->modifiedColumns[] = RincianDetailPeer::TAHAP_NEW;
		}

	} 
	
	public function setStatusLelang($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->status_lelang !== $v) {
			$this->status_lelang = $v;
			$this->modifiedColumns[] = RincianDetailPeer::STATUS_LELANG;
		}

	} 
	
	public function setNomorLelang($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nomor_lelang !== $v) {
			$this->nomor_lelang = $v;
			$this->modifiedColumns[] = RincianDetailPeer::NOMOR_LELANG;
		}

	} 
	
	public function setKoefisienSemula($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->koefisien_semula !== $v) {
			$this->koefisien_semula = $v;
			$this->modifiedColumns[] = RincianDetailPeer::KOEFISIEN_SEMULA;
		}

	} 
	
	public function setVolumeSemula($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->volume_semula !== $v) {
			$this->volume_semula = $v;
			$this->modifiedColumns[] = RincianDetailPeer::VOLUME_SEMULA;
		}

	} 
	
	public function setHargaSemula($v)
	{

		if ($this->harga_semula !== $v) {
			$this->harga_semula = $v;
			$this->modifiedColumns[] = RincianDetailPeer::HARGA_SEMULA;
		}

	} 
	
	public function setTotalSemula($v)
	{

		if ($this->total_semula !== $v) {
			$this->total_semula = $v;
			$this->modifiedColumns[] = RincianDetailPeer::TOTAL_SEMULA;
		}

	} 
	
	public function setLockSubtitle($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->lock_subtitle !== $v) {
			$this->lock_subtitle = $v;
			$this->modifiedColumns[] = RincianDetailPeer::LOCK_SUBTITLE;
		}

	} 
	
	public function setStatusHapus($v)
	{

		if ($this->status_hapus !== $v || $v === false) {
			$this->status_hapus = $v;
			$this->modifiedColumns[] = RincianDetailPeer::STATUS_HAPUS;
		}

	} 
	
	public function setTahun($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->tahun !== $v) {
			$this->tahun = $v;
			$this->modifiedColumns[] = RincianDetailPeer::TAHUN;
		}

	} 
	
	public function setKodeLokasi($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_lokasi !== $v) {
			$this->kode_lokasi = $v;
			$this->modifiedColumns[] = RincianDetailPeer::KODE_LOKASI;
		}

	} 
	
	public function setKecamatan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kecamatan !== $v) {
			$this->kecamatan = $v;
			$this->modifiedColumns[] = RincianDetailPeer::KECAMATAN;
		}

	} 
	
	public function setRekeningCodeAsli($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->rekening_code_asli !== $v) {
			$this->rekening_code_asli = $v;
			$this->modifiedColumns[] = RincianDetailPeer::REKENING_CODE_ASLI;
		}

	} 
	
	public function setNoteSkpd($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->note_skpd !== $v) {
			$this->note_skpd = $v;
			$this->modifiedColumns[] = RincianDetailPeer::NOTE_SKPD;
		}

	} 
	
	public function setNotePeneliti($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->note_peneliti !== $v) {
			$this->note_peneliti = $v;
			$this->modifiedColumns[] = RincianDetailPeer::NOTE_PENELITI;
		}

	} 
	
	public function setNilaiAnggaran($v)
	{

		if ($this->nilai_anggaran !== $v) {
			$this->nilai_anggaran = $v;
			$this->modifiedColumns[] = RincianDetailPeer::NILAI_ANGGARAN;
		}

	} 
	
	public function setIsBlud($v)
	{

		if ($this->is_blud !== $v) {
			$this->is_blud = $v;
			$this->modifiedColumns[] = RincianDetailPeer::IS_BLUD;
		}

	} 
	
	public function setIsKapitasiBpjs($v)
	{

		if ($this->is_kapitasi_bpjs !== $v) {
			$this->is_kapitasi_bpjs = $v;
			$this->modifiedColumns[] = RincianDetailPeer::IS_KAPITASI_BPJS;
		}

	} 
	
	public function setLokasiKecamatan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->lokasi_kecamatan !== $v) {
			$this->lokasi_kecamatan = $v;
			$this->modifiedColumns[] = RincianDetailPeer::LOKASI_KECAMATAN;
		}

	} 
	
	public function setLokasiKelurahan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->lokasi_kelurahan !== $v) {
			$this->lokasi_kelurahan = $v;
			$this->modifiedColumns[] = RincianDetailPeer::LOKASI_KELURAHAN;
		}

	} 
	
	public function setOb($v)
	{

		if ($this->ob !== $v) {
			$this->ob = $v;
			$this->modifiedColumns[] = RincianDetailPeer::OB;
		}

	} 
	
	public function setObFromId($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->ob_from_id !== $v) {
			$this->ob_from_id = $v;
			$this->modifiedColumns[] = RincianDetailPeer::OB_FROM_ID;
		}

	} 
	
	public function setIsPerKomponen($v)
	{

		if ($this->is_per_komponen !== $v) {
			$this->is_per_komponen = $v;
			$this->modifiedColumns[] = RincianDetailPeer::IS_PER_KOMPONEN;
		}

	} 
	
	public function setKegiatanCodeAsal($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kegiatan_code_asal !== $v) {
			$this->kegiatan_code_asal = $v;
			$this->modifiedColumns[] = RincianDetailPeer::KEGIATAN_CODE_ASAL;
		}

	} 
	
	public function setThKeMultiyears($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->th_ke_multiyears !== $v) {
			$this->th_ke_multiyears = $v;
			$this->modifiedColumns[] = RincianDetailPeer::TH_KE_MULTIYEARS;
		}

	} 
	
	public function setHargaSebelumSisaLelang($v)
	{

		if ($this->harga_sebelum_sisa_lelang !== $v) {
			$this->harga_sebelum_sisa_lelang = $v;
			$this->modifiedColumns[] = RincianDetailPeer::HARGA_SEBELUM_SISA_LELANG;
		}

	} 
	
	public function setIsMusrenbang($v)
	{

		if ($this->is_musrenbang !== $v || $v === false) {
			$this->is_musrenbang = $v;
			$this->modifiedColumns[] = RincianDetailPeer::IS_MUSRENBANG;
		}

	} 
	
	public function setSubIdAsal($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->sub_id_asal !== $v) {
			$this->sub_id_asal = $v;
			$this->modifiedColumns[] = RincianDetailPeer::SUB_ID_ASAL;
		}

	} 
	
	public function setSubtitleAsal($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->subtitle_asal !== $v) {
			$this->subtitle_asal = $v;
			$this->modifiedColumns[] = RincianDetailPeer::SUBTITLE_ASAL;
		}

	} 
	
	public function setKodeSubAsal($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_sub_asal !== $v) {
			$this->kode_sub_asal = $v;
			$this->modifiedColumns[] = RincianDetailPeer::KODE_SUB_ASAL;
		}

	} 
	
	public function setSubAsal($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->sub_asal !== $v) {
			$this->sub_asal = $v;
			$this->modifiedColumns[] = RincianDetailPeer::SUB_ASAL;
		}

	} 
	
	public function setLastEditTime($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [last_edit_time] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->last_edit_time !== $ts) {
			$this->last_edit_time = $ts;
			$this->modifiedColumns[] = RincianDetailPeer::LAST_EDIT_TIME;
		}

	} 
	
	public function setIsPotongBpjs($v)
	{

		if ($this->is_potong_bpjs !== $v || $v === false) {
			$this->is_potong_bpjs = $v;
			$this->modifiedColumns[] = RincianDetailPeer::IS_POTONG_BPJS;
		}

	} 
	
	public function setIsIuranBpjs($v)
	{

		if ($this->is_iuran_bpjs !== $v || $v === false) {
			$this->is_iuran_bpjs = $v;
			$this->modifiedColumns[] = RincianDetailPeer::IS_IURAN_BPJS;
		}

	} 
	
	public function setStatusOb($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->status_ob !== $v || $v === 0) {
			$this->status_ob = $v;
			$this->modifiedColumns[] = RincianDetailPeer::STATUS_OB;
		}

	} 
	
	public function setObParent($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->ob_parent !== $v) {
			$this->ob_parent = $v;
			$this->modifiedColumns[] = RincianDetailPeer::OB_PARENT;
		}

	} 
	
	public function setObAlokasiBaru($v)
	{

		if ($this->ob_alokasi_baru !== $v) {
			$this->ob_alokasi_baru = $v;
			$this->modifiedColumns[] = RincianDetailPeer::OB_ALOKASI_BARU;
		}

	} 
	
	public function setIsHibah($v)
	{

		if ($this->is_hibah !== $v || $v === false) {
			$this->is_hibah = $v;
			$this->modifiedColumns[] = RincianDetailPeer::IS_HIBAH;
		}

	} 
	
	public function setAkrualCode($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->akrual_code !== $v) {
			$this->akrual_code = $v;
			$this->modifiedColumns[] = RincianDetailPeer::AKRUAL_CODE;
		}

	} 
	
	public function setTipe2($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->tipe2 !== $v) {
			$this->tipe2 = $v;
			$this->modifiedColumns[] = RincianDetailPeer::TIPE2;
		}

	} 
	
	public function setStatusLevel($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->status_level !== $v || $v === 0) {
			$this->status_level = $v;
			$this->modifiedColumns[] = RincianDetailPeer::STATUS_LEVEL;
		}

	} 
	
	public function setStatusLevelTolak($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->status_level_tolak !== $v) {
			$this->status_level_tolak = $v;
			$this->modifiedColumns[] = RincianDetailPeer::STATUS_LEVEL_TOLAK;
		}

	} 
	
	public function setStatusSisipan($v)
	{

		if ($this->status_sisipan !== $v || $v === false) {
			$this->status_sisipan = $v;
			$this->modifiedColumns[] = RincianDetailPeer::STATUS_SISIPAN;
		}

	} 
	
	public function setIsTapdSetuju($v)
	{

		if ($this->is_tapd_setuju !== $v || $v === false) {
			$this->is_tapd_setuju = $v;
			$this->modifiedColumns[] = RincianDetailPeer::IS_TAPD_SETUJU;
		}

	} 
	
	public function setIsBappekoSetuju($v)
	{

		if ($this->is_bappeko_setuju !== $v || $v === false) {
			$this->is_bappeko_setuju = $v;
			$this->modifiedColumns[] = RincianDetailPeer::IS_BAPPEKO_SETUJU;
		}

	} 
	
	public function setIsPenyeliaSetuju($v)
	{

		if ($this->is_penyelia_setuju !== $v || $v === false) {
			$this->is_penyelia_setuju = $v;
			$this->modifiedColumns[] = RincianDetailPeer::IS_PENYELIA_SETUJU;
		}

	} 
	
	public function setNoteTapd($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->note_tapd !== $v) {
			$this->note_tapd = $v;
			$this->modifiedColumns[] = RincianDetailPeer::NOTE_TAPD;
		}

	} 
	
	public function setNoteBappeko($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->note_bappeko !== $v) {
			$this->note_bappeko = $v;
			$this->modifiedColumns[] = RincianDetailPeer::NOTE_BAPPEKO;
		}

	} 
	
	public function setSatuanSemula($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->satuan_semula !== $v) {
			$this->satuan_semula = $v;
			$this->modifiedColumns[] = RincianDetailPeer::SATUAN_SEMULA;
		}

	} 
	
	public function setIdLokasi($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->id_lokasi !== $v) {
			$this->id_lokasi = $v;
			$this->modifiedColumns[] = RincianDetailPeer::ID_LOKASI;
		}

	} 
	
	public function setDetailKegiatan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->detail_kegiatan !== $v) {
			$this->detail_kegiatan = $v;
			$this->modifiedColumns[] = RincianDetailPeer::DETAIL_KEGIATAN;
		}

	} 
	
	public function setDetailKegiatanSemula($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->detail_kegiatan_semula !== $v) {
			$this->detail_kegiatan_semula = $v;
			$this->modifiedColumns[] = RincianDetailPeer::DETAIL_KEGIATAN_SEMULA;
		}

	} 
	
	public function setStatusKomponenBaru($v)
	{

		if ($this->status_komponen_baru !== $v || $v === false) {
			$this->status_komponen_baru = $v;
			$this->modifiedColumns[] = RincianDetailPeer::STATUS_KOMPONEN_BARU;
		}

	} 
	
	public function setStatusKomponenBerubah($v)
	{

		if ($this->status_komponen_berubah !== $v || $v === false) {
			$this->status_komponen_berubah = $v;
			$this->modifiedColumns[] = RincianDetailPeer::STATUS_KOMPONEN_BERUBAH;
		}

	} 
	
	public function setApproveUnlockHarga($v)
	{

		if ($this->approve_unlock_harga !== $v) {
			$this->approve_unlock_harga = $v;
			$this->modifiedColumns[] = RincianDetailPeer::APPROVE_UNLOCK_HARGA;
		}

	} 
	
	public function setTipeLelang($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->tipe_lelang !== $v) {
			$this->tipe_lelang = $v;
			$this->modifiedColumns[] = RincianDetailPeer::TIPE_LELANG;
		}

	} 
	
	public function setIsHpsp($v)
	{

		if ($this->is_hpsp !== $v || $v === false) {
			$this->is_hpsp = $v;
			$this->modifiedColumns[] = RincianDetailPeer::IS_HPSP;
		}

	} 
	
	public function setIsDak($v)
	{

		if ($this->is_dak !== $v || $v === false) {
			$this->is_dak = $v;
			$this->modifiedColumns[] = RincianDetailPeer::IS_DAK;
		}

	} 
	
	public function setIsBos($v)
	{

		if ($this->is_bos !== $v || $v === false) {
			$this->is_bos = $v;
			$this->modifiedColumns[] = RincianDetailPeer::IS_BOS;
		}

	} 
	
	public function setIsBobda($v)
	{

		if ($this->is_bobda !== $v || $v === false) {
			$this->is_bobda = $v;
			$this->modifiedColumns[] = RincianDetailPeer::IS_BOBDA;
		}

	} 
	
	public function setIsNarsum($v)
	{

		if ($this->is_narsum !== $v || $v === false) {
			$this->is_narsum = $v;
			$this->modifiedColumns[] = RincianDetailPeer::IS_NARSUM;
		}

	} 
	
	public function setIsBagianHukumSetuju($v)
	{

		if ($this->is_bagian_hukum_setuju !== $v) {
			$this->is_bagian_hukum_setuju = $v;
			$this->modifiedColumns[] = RincianDetailPeer::IS_BAGIAN_HUKUM_SETUJU;
		}

	} 
	
	public function setIsInspektoratSetuju($v)
	{

		if ($this->is_inspektorat_setuju !== $v) {
			$this->is_inspektorat_setuju = $v;
			$this->modifiedColumns[] = RincianDetailPeer::IS_INSPEKTORAT_SETUJU;
		}

	} 
	
	public function setIsBadanKepegawaianSetuju($v)
	{

		if ($this->is_badan_kepegawaian_setuju !== $v) {
			$this->is_badan_kepegawaian_setuju = $v;
			$this->modifiedColumns[] = RincianDetailPeer::IS_BADAN_KEPEGAWAIAN_SETUJU;
		}

	} 
	
	public function setIsLppaSetuju($v)
	{

		if ($this->is_lppa_setuju !== $v) {
			$this->is_lppa_setuju = $v;
			$this->modifiedColumns[] = RincianDetailPeer::IS_LPPA_SETUJU;
		}

	} 
	
	public function setPrioritasWali($v)
	{

		if ($this->prioritas_wali !== $v) {
			$this->prioritas_wali = $v;
			$this->modifiedColumns[] = RincianDetailPeer::PRIORITAS_WALI;
		}

	} 
	
	public function setIsOutput($v)
	{

		if ($this->is_output !== $v) {
			$this->is_output = $v;
			$this->modifiedColumns[] = RincianDetailPeer::IS_OUTPUT;
		}

	} 
	
	public function setIsBagianOrganisasiSetuju($v)
	{

		if ($this->is_bagian_organisasi_setuju !== $v || $v === false) {
			$this->is_bagian_organisasi_setuju = $v;
			$this->modifiedColumns[] = RincianDetailPeer::IS_BAGIAN_ORGANISASI_SETUJU;
		}

	} 
	
	public function setIsAsisten1Setuju($v)
	{

		if ($this->is_asisten1_setuju !== $v) {
			$this->is_asisten1_setuju = $v;
			$this->modifiedColumns[] = RincianDetailPeer::IS_ASISTEN1_SETUJU;
		}

	} 
	
	public function setIsAsisten2Setuju($v)
	{

		if ($this->is_asisten2_setuju !== $v) {
			$this->is_asisten2_setuju = $v;
			$this->modifiedColumns[] = RincianDetailPeer::IS_ASISTEN2_SETUJU;
		}

	} 
	
	public function setIsAsisten3Setuju($v)
	{

		if ($this->is_asisten3_setuju !== $v) {
			$this->is_asisten3_setuju = $v;
			$this->modifiedColumns[] = RincianDetailPeer::IS_ASISTEN3_SETUJU;
		}

	} 
	
	public function setIsSekdaSetuju($v)
	{

		if ($this->is_sekda_setuju !== $v) {
			$this->is_sekda_setuju = $v;
			$this->modifiedColumns[] = RincianDetailPeer::IS_SEKDA_SETUJU;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->kegiatan_code = $rs->getString($startcol + 0);

			$this->tipe = $rs->getString($startcol + 1);

			$this->detail_no = $rs->getInt($startcol + 2);

			$this->rekening_code = $rs->getString($startcol + 3);

			$this->komponen_id = $rs->getString($startcol + 4);

			$this->detail_name = $rs->getString($startcol + 5);

			$this->volume = $rs->getFloat($startcol + 6);

			$this->keterangan_koefisien = $rs->getString($startcol + 7);

			$this->subtitle = $rs->getString($startcol + 8);

			$this->komponen_harga = $rs->getFloat($startcol + 9);

			$this->komponen_harga_awal = $rs->getFloat($startcol + 10);

			$this->komponen_name = $rs->getString($startcol + 11);

			$this->satuan = $rs->getString($startcol + 12);

			$this->pajak = $rs->getInt($startcol + 13);

			$this->unit_id = $rs->getString($startcol + 14);

			$this->from_sub_kegiatan = $rs->getString($startcol + 15);

			$this->sub = $rs->getString($startcol + 16);

			$this->kode_sub = $rs->getString($startcol + 17);

			$this->last_update_user = $rs->getString($startcol + 18);

			$this->last_update_time = $rs->getTimestamp($startcol + 19, null);

			$this->last_update_ip = $rs->getString($startcol + 20);

			$this->tahap = $rs->getString($startcol + 21);

			$this->tahap_edit = $rs->getString($startcol + 22);

			$this->tahap_new = $rs->getString($startcol + 23);

			$this->status_lelang = $rs->getString($startcol + 24);

			$this->nomor_lelang = $rs->getString($startcol + 25);

			$this->koefisien_semula = $rs->getString($startcol + 26);

			$this->volume_semula = $rs->getInt($startcol + 27);

			$this->harga_semula = $rs->getFloat($startcol + 28);

			$this->total_semula = $rs->getFloat($startcol + 29);

			$this->lock_subtitle = $rs->getString($startcol + 30);

			$this->status_hapus = $rs->getBoolean($startcol + 31);

			$this->tahun = $rs->getString($startcol + 32);

			$this->kode_lokasi = $rs->getString($startcol + 33);

			$this->kecamatan = $rs->getString($startcol + 34);

			$this->rekening_code_asli = $rs->getString($startcol + 35);

			$this->note_skpd = $rs->getString($startcol + 36);

			$this->note_peneliti = $rs->getString($startcol + 37);

			$this->nilai_anggaran = $rs->getFloat($startcol + 38);

			$this->is_blud = $rs->getBoolean($startcol + 39);

			$this->is_kapitasi_bpjs = $rs->getBoolean($startcol + 40);

			$this->lokasi_kecamatan = $rs->getString($startcol + 41);

			$this->lokasi_kelurahan = $rs->getString($startcol + 42);

			$this->ob = $rs->getBoolean($startcol + 43);

			$this->ob_from_id = $rs->getInt($startcol + 44);

			$this->is_per_komponen = $rs->getBoolean($startcol + 45);

			$this->kegiatan_code_asal = $rs->getString($startcol + 46);

			$this->th_ke_multiyears = $rs->getInt($startcol + 47);

			$this->harga_sebelum_sisa_lelang = $rs->getFloat($startcol + 48);

			$this->is_musrenbang = $rs->getBoolean($startcol + 49);

			$this->sub_id_asal = $rs->getInt($startcol + 50);

			$this->subtitle_asal = $rs->getString($startcol + 51);

			$this->kode_sub_asal = $rs->getString($startcol + 52);

			$this->sub_asal = $rs->getString($startcol + 53);

			$this->last_edit_time = $rs->getTimestamp($startcol + 54, null);

			$this->is_potong_bpjs = $rs->getBoolean($startcol + 55);

			$this->is_iuran_bpjs = $rs->getBoolean($startcol + 56);

			$this->status_ob = $rs->getInt($startcol + 57);

			$this->ob_parent = $rs->getString($startcol + 58);

			$this->ob_alokasi_baru = $rs->getFloat($startcol + 59);

			$this->is_hibah = $rs->getBoolean($startcol + 60);

			$this->akrual_code = $rs->getString($startcol + 61);

			$this->tipe2 = $rs->getString($startcol + 62);

			$this->status_level = $rs->getInt($startcol + 63);

			$this->status_level_tolak = $rs->getInt($startcol + 64);

			$this->status_sisipan = $rs->getBoolean($startcol + 65);

			$this->is_tapd_setuju = $rs->getBoolean($startcol + 66);

			$this->is_bappeko_setuju = $rs->getBoolean($startcol + 67);

			$this->is_penyelia_setuju = $rs->getBoolean($startcol + 68);

			$this->note_tapd = $rs->getString($startcol + 69);

			$this->note_bappeko = $rs->getString($startcol + 70);

			$this->satuan_semula = $rs->getString($startcol + 71);

			$this->id_lokasi = $rs->getString($startcol + 72);

			$this->detail_kegiatan = $rs->getString($startcol + 73);

			$this->detail_kegiatan_semula = $rs->getString($startcol + 74);

			$this->status_komponen_baru = $rs->getBoolean($startcol + 75);

			$this->status_komponen_berubah = $rs->getBoolean($startcol + 76);

			$this->approve_unlock_harga = $rs->getBoolean($startcol + 77);

			$this->tipe_lelang = $rs->getInt($startcol + 78);

			$this->is_hpsp = $rs->getBoolean($startcol + 79);

			$this->is_dak = $rs->getBoolean($startcol + 80);

			$this->is_bos = $rs->getBoolean($startcol + 81);

			$this->is_bobda = $rs->getBoolean($startcol + 82);

			$this->is_narsum = $rs->getBoolean($startcol + 83);

			$this->is_bagian_hukum_setuju = $rs->getBoolean($startcol + 84);

			$this->is_inspektorat_setuju = $rs->getBoolean($startcol + 85);

			$this->is_badan_kepegawaian_setuju = $rs->getBoolean($startcol + 86);

			$this->is_lppa_setuju = $rs->getBoolean($startcol + 87);

			$this->prioritas_wali = $rs->getBoolean($startcol + 88);

			$this->is_output = $rs->getBoolean($startcol + 89);

			$this->is_bagian_organisasi_setuju = $rs->getBoolean($startcol + 90);

			$this->is_asisten1_setuju = $rs->getBoolean($startcol + 91);

			$this->is_asisten2_setuju = $rs->getBoolean($startcol + 92);

			$this->is_asisten3_setuju = $rs->getBoolean($startcol + 93);

			$this->is_sekda_setuju = $rs->getBoolean($startcol + 94);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 95; 
		} catch (Exception $e) {
			throw new PropelException("Error populating RincianDetail object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(RincianDetailPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			RincianDetailPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(RincianDetailPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = RincianDetailPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += RincianDetailPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = RincianDetailPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = RincianDetailPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getKegiatanCode();
				break;
			case 1:
				return $this->getTipe();
				break;
			case 2:
				return $this->getDetailNo();
				break;
			case 3:
				return $this->getRekeningCode();
				break;
			case 4:
				return $this->getKomponenId();
				break;
			case 5:
				return $this->getDetailName();
				break;
			case 6:
				return $this->getVolume();
				break;
			case 7:
				return $this->getKeteranganKoefisien();
				break;
			case 8:
				return $this->getSubtitle();
				break;
			case 9:
				return $this->getKomponenHarga();
				break;
			case 10:
				return $this->getKomponenHargaAwal();
				break;
			case 11:
				return $this->getKomponenName();
				break;
			case 12:
				return $this->getSatuan();
				break;
			case 13:
				return $this->getPajak();
				break;
			case 14:
				return $this->getUnitId();
				break;
			case 15:
				return $this->getFromSubKegiatan();
				break;
			case 16:
				return $this->getSub();
				break;
			case 17:
				return $this->getKodeSub();
				break;
			case 18:
				return $this->getLastUpdateUser();
				break;
			case 19:
				return $this->getLastUpdateTime();
				break;
			case 20:
				return $this->getLastUpdateIp();
				break;
			case 21:
				return $this->getTahap();
				break;
			case 22:
				return $this->getTahapEdit();
				break;
			case 23:
				return $this->getTahapNew();
				break;
			case 24:
				return $this->getStatusLelang();
				break;
			case 25:
				return $this->getNomorLelang();
				break;
			case 26:
				return $this->getKoefisienSemula();
				break;
			case 27:
				return $this->getVolumeSemula();
				break;
			case 28:
				return $this->getHargaSemula();
				break;
			case 29:
				return $this->getTotalSemula();
				break;
			case 30:
				return $this->getLockSubtitle();
				break;
			case 31:
				return $this->getStatusHapus();
				break;
			case 32:
				return $this->getTahun();
				break;
			case 33:
				return $this->getKodeLokasi();
				break;
			case 34:
				return $this->getKecamatan();
				break;
			case 35:
				return $this->getRekeningCodeAsli();
				break;
			case 36:
				return $this->getNoteSkpd();
				break;
			case 37:
				return $this->getNotePeneliti();
				break;
			case 38:
				return $this->getNilaiAnggaran();
				break;
			case 39:
				return $this->getIsBlud();
				break;
			case 40:
				return $this->getIsKapitasiBpjs();
				break;
			case 41:
				return $this->getLokasiKecamatan();
				break;
			case 42:
				return $this->getLokasiKelurahan();
				break;
			case 43:
				return $this->getOb();
				break;
			case 44:
				return $this->getObFromId();
				break;
			case 45:
				return $this->getIsPerKomponen();
				break;
			case 46:
				return $this->getKegiatanCodeAsal();
				break;
			case 47:
				return $this->getThKeMultiyears();
				break;
			case 48:
				return $this->getHargaSebelumSisaLelang();
				break;
			case 49:
				return $this->getIsMusrenbang();
				break;
			case 50:
				return $this->getSubIdAsal();
				break;
			case 51:
				return $this->getSubtitleAsal();
				break;
			case 52:
				return $this->getKodeSubAsal();
				break;
			case 53:
				return $this->getSubAsal();
				break;
			case 54:
				return $this->getLastEditTime();
				break;
			case 55:
				return $this->getIsPotongBpjs();
				break;
			case 56:
				return $this->getIsIuranBpjs();
				break;
			case 57:
				return $this->getStatusOb();
				break;
			case 58:
				return $this->getObParent();
				break;
			case 59:
				return $this->getObAlokasiBaru();
				break;
			case 60:
				return $this->getIsHibah();
				break;
			case 61:
				return $this->getAkrualCode();
				break;
			case 62:
				return $this->getTipe2();
				break;
			case 63:
				return $this->getStatusLevel();
				break;
			case 64:
				return $this->getStatusLevelTolak();
				break;
			case 65:
				return $this->getStatusSisipan();
				break;
			case 66:
				return $this->getIsTapdSetuju();
				break;
			case 67:
				return $this->getIsBappekoSetuju();
				break;
			case 68:
				return $this->getIsPenyeliaSetuju();
				break;
			case 69:
				return $this->getNoteTapd();
				break;
			case 70:
				return $this->getNoteBappeko();
				break;
			case 71:
				return $this->getSatuanSemula();
				break;
			case 72:
				return $this->getIdLokasi();
				break;
			case 73:
				return $this->getDetailKegiatan();
				break;
			case 74:
				return $this->getDetailKegiatanSemula();
				break;
			case 75:
				return $this->getStatusKomponenBaru();
				break;
			case 76:
				return $this->getStatusKomponenBerubah();
				break;
			case 77:
				return $this->getApproveUnlockHarga();
				break;
			case 78:
				return $this->getTipeLelang();
				break;
			case 79:
				return $this->getIsHpsp();
				break;
			case 80:
				return $this->getIsDak();
				break;
			case 81:
				return $this->getIsBos();
				break;
			case 82:
				return $this->getIsBobda();
				break;
			case 83:
				return $this->getIsNarsum();
				break;
			case 84:
				return $this->getIsBagianHukumSetuju();
				break;
			case 85:
				return $this->getIsInspektoratSetuju();
				break;
			case 86:
				return $this->getIsBadanKepegawaianSetuju();
				break;
			case 87:
				return $this->getIsLppaSetuju();
				break;
			case 88:
				return $this->getPrioritasWali();
				break;
			case 89:
				return $this->getIsOutput();
				break;
			case 90:
				return $this->getIsBagianOrganisasiSetuju();
				break;
			case 91:
				return $this->getIsAsisten1Setuju();
				break;
			case 92:
				return $this->getIsAsisten2Setuju();
				break;
			case 93:
				return $this->getIsAsisten3Setuju();
				break;
			case 94:
				return $this->getIsSekdaSetuju();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = RincianDetailPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getKegiatanCode(),
			$keys[1] => $this->getTipe(),
			$keys[2] => $this->getDetailNo(),
			$keys[3] => $this->getRekeningCode(),
			$keys[4] => $this->getKomponenId(),
			$keys[5] => $this->getDetailName(),
			$keys[6] => $this->getVolume(),
			$keys[7] => $this->getKeteranganKoefisien(),
			$keys[8] => $this->getSubtitle(),
			$keys[9] => $this->getKomponenHarga(),
			$keys[10] => $this->getKomponenHargaAwal(),
			$keys[11] => $this->getKomponenName(),
			$keys[12] => $this->getSatuan(),
			$keys[13] => $this->getPajak(),
			$keys[14] => $this->getUnitId(),
			$keys[15] => $this->getFromSubKegiatan(),
			$keys[16] => $this->getSub(),
			$keys[17] => $this->getKodeSub(),
			$keys[18] => $this->getLastUpdateUser(),
			$keys[19] => $this->getLastUpdateTime(),
			$keys[20] => $this->getLastUpdateIp(),
			$keys[21] => $this->getTahap(),
			$keys[22] => $this->getTahapEdit(),
			$keys[23] => $this->getTahapNew(),
			$keys[24] => $this->getStatusLelang(),
			$keys[25] => $this->getNomorLelang(),
			$keys[26] => $this->getKoefisienSemula(),
			$keys[27] => $this->getVolumeSemula(),
			$keys[28] => $this->getHargaSemula(),
			$keys[29] => $this->getTotalSemula(),
			$keys[30] => $this->getLockSubtitle(),
			$keys[31] => $this->getStatusHapus(),
			$keys[32] => $this->getTahun(),
			$keys[33] => $this->getKodeLokasi(),
			$keys[34] => $this->getKecamatan(),
			$keys[35] => $this->getRekeningCodeAsli(),
			$keys[36] => $this->getNoteSkpd(),
			$keys[37] => $this->getNotePeneliti(),
			$keys[38] => $this->getNilaiAnggaran(),
			$keys[39] => $this->getIsBlud(),
			$keys[40] => $this->getIsKapitasiBpjs(),
			$keys[41] => $this->getLokasiKecamatan(),
			$keys[42] => $this->getLokasiKelurahan(),
			$keys[43] => $this->getOb(),
			$keys[44] => $this->getObFromId(),
			$keys[45] => $this->getIsPerKomponen(),
			$keys[46] => $this->getKegiatanCodeAsal(),
			$keys[47] => $this->getThKeMultiyears(),
			$keys[48] => $this->getHargaSebelumSisaLelang(),
			$keys[49] => $this->getIsMusrenbang(),
			$keys[50] => $this->getSubIdAsal(),
			$keys[51] => $this->getSubtitleAsal(),
			$keys[52] => $this->getKodeSubAsal(),
			$keys[53] => $this->getSubAsal(),
			$keys[54] => $this->getLastEditTime(),
			$keys[55] => $this->getIsPotongBpjs(),
			$keys[56] => $this->getIsIuranBpjs(),
			$keys[57] => $this->getStatusOb(),
			$keys[58] => $this->getObParent(),
			$keys[59] => $this->getObAlokasiBaru(),
			$keys[60] => $this->getIsHibah(),
			$keys[61] => $this->getAkrualCode(),
			$keys[62] => $this->getTipe2(),
			$keys[63] => $this->getStatusLevel(),
			$keys[64] => $this->getStatusLevelTolak(),
			$keys[65] => $this->getStatusSisipan(),
			$keys[66] => $this->getIsTapdSetuju(),
			$keys[67] => $this->getIsBappekoSetuju(),
			$keys[68] => $this->getIsPenyeliaSetuju(),
			$keys[69] => $this->getNoteTapd(),
			$keys[70] => $this->getNoteBappeko(),
			$keys[71] => $this->getSatuanSemula(),
			$keys[72] => $this->getIdLokasi(),
			$keys[73] => $this->getDetailKegiatan(),
			$keys[74] => $this->getDetailKegiatanSemula(),
			$keys[75] => $this->getStatusKomponenBaru(),
			$keys[76] => $this->getStatusKomponenBerubah(),
			$keys[77] => $this->getApproveUnlockHarga(),
			$keys[78] => $this->getTipeLelang(),
			$keys[79] => $this->getIsHpsp(),
			$keys[80] => $this->getIsDak(),
			$keys[81] => $this->getIsBos(),
			$keys[82] => $this->getIsBobda(),
			$keys[83] => $this->getIsNarsum(),
			$keys[84] => $this->getIsBagianHukumSetuju(),
			$keys[85] => $this->getIsInspektoratSetuju(),
			$keys[86] => $this->getIsBadanKepegawaianSetuju(),
			$keys[87] => $this->getIsLppaSetuju(),
			$keys[88] => $this->getPrioritasWali(),
			$keys[89] => $this->getIsOutput(),
			$keys[90] => $this->getIsBagianOrganisasiSetuju(),
			$keys[91] => $this->getIsAsisten1Setuju(),
			$keys[92] => $this->getIsAsisten2Setuju(),
			$keys[93] => $this->getIsAsisten3Setuju(),
			$keys[94] => $this->getIsSekdaSetuju(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = RincianDetailPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setKegiatanCode($value);
				break;
			case 1:
				$this->setTipe($value);
				break;
			case 2:
				$this->setDetailNo($value);
				break;
			case 3:
				$this->setRekeningCode($value);
				break;
			case 4:
				$this->setKomponenId($value);
				break;
			case 5:
				$this->setDetailName($value);
				break;
			case 6:
				$this->setVolume($value);
				break;
			case 7:
				$this->setKeteranganKoefisien($value);
				break;
			case 8:
				$this->setSubtitle($value);
				break;
			case 9:
				$this->setKomponenHarga($value);
				break;
			case 10:
				$this->setKomponenHargaAwal($value);
				break;
			case 11:
				$this->setKomponenName($value);
				break;
			case 12:
				$this->setSatuan($value);
				break;
			case 13:
				$this->setPajak($value);
				break;
			case 14:
				$this->setUnitId($value);
				break;
			case 15:
				$this->setFromSubKegiatan($value);
				break;
			case 16:
				$this->setSub($value);
				break;
			case 17:
				$this->setKodeSub($value);
				break;
			case 18:
				$this->setLastUpdateUser($value);
				break;
			case 19:
				$this->setLastUpdateTime($value);
				break;
			case 20:
				$this->setLastUpdateIp($value);
				break;
			case 21:
				$this->setTahap($value);
				break;
			case 22:
				$this->setTahapEdit($value);
				break;
			case 23:
				$this->setTahapNew($value);
				break;
			case 24:
				$this->setStatusLelang($value);
				break;
			case 25:
				$this->setNomorLelang($value);
				break;
			case 26:
				$this->setKoefisienSemula($value);
				break;
			case 27:
				$this->setVolumeSemula($value);
				break;
			case 28:
				$this->setHargaSemula($value);
				break;
			case 29:
				$this->setTotalSemula($value);
				break;
			case 30:
				$this->setLockSubtitle($value);
				break;
			case 31:
				$this->setStatusHapus($value);
				break;
			case 32:
				$this->setTahun($value);
				break;
			case 33:
				$this->setKodeLokasi($value);
				break;
			case 34:
				$this->setKecamatan($value);
				break;
			case 35:
				$this->setRekeningCodeAsli($value);
				break;
			case 36:
				$this->setNoteSkpd($value);
				break;
			case 37:
				$this->setNotePeneliti($value);
				break;
			case 38:
				$this->setNilaiAnggaran($value);
				break;
			case 39:
				$this->setIsBlud($value);
				break;
			case 40:
				$this->setIsKapitasiBpjs($value);
				break;
			case 41:
				$this->setLokasiKecamatan($value);
				break;
			case 42:
				$this->setLokasiKelurahan($value);
				break;
			case 43:
				$this->setOb($value);
				break;
			case 44:
				$this->setObFromId($value);
				break;
			case 45:
				$this->setIsPerKomponen($value);
				break;
			case 46:
				$this->setKegiatanCodeAsal($value);
				break;
			case 47:
				$this->setThKeMultiyears($value);
				break;
			case 48:
				$this->setHargaSebelumSisaLelang($value);
				break;
			case 49:
				$this->setIsMusrenbang($value);
				break;
			case 50:
				$this->setSubIdAsal($value);
				break;
			case 51:
				$this->setSubtitleAsal($value);
				break;
			case 52:
				$this->setKodeSubAsal($value);
				break;
			case 53:
				$this->setSubAsal($value);
				break;
			case 54:
				$this->setLastEditTime($value);
				break;
			case 55:
				$this->setIsPotongBpjs($value);
				break;
			case 56:
				$this->setIsIuranBpjs($value);
				break;
			case 57:
				$this->setStatusOb($value);
				break;
			case 58:
				$this->setObParent($value);
				break;
			case 59:
				$this->setObAlokasiBaru($value);
				break;
			case 60:
				$this->setIsHibah($value);
				break;
			case 61:
				$this->setAkrualCode($value);
				break;
			case 62:
				$this->setTipe2($value);
				break;
			case 63:
				$this->setStatusLevel($value);
				break;
			case 64:
				$this->setStatusLevelTolak($value);
				break;
			case 65:
				$this->setStatusSisipan($value);
				break;
			case 66:
				$this->setIsTapdSetuju($value);
				break;
			case 67:
				$this->setIsBappekoSetuju($value);
				break;
			case 68:
				$this->setIsPenyeliaSetuju($value);
				break;
			case 69:
				$this->setNoteTapd($value);
				break;
			case 70:
				$this->setNoteBappeko($value);
				break;
			case 71:
				$this->setSatuanSemula($value);
				break;
			case 72:
				$this->setIdLokasi($value);
				break;
			case 73:
				$this->setDetailKegiatan($value);
				break;
			case 74:
				$this->setDetailKegiatanSemula($value);
				break;
			case 75:
				$this->setStatusKomponenBaru($value);
				break;
			case 76:
				$this->setStatusKomponenBerubah($value);
				break;
			case 77:
				$this->setApproveUnlockHarga($value);
				break;
			case 78:
				$this->setTipeLelang($value);
				break;
			case 79:
				$this->setIsHpsp($value);
				break;
			case 80:
				$this->setIsDak($value);
				break;
			case 81:
				$this->setIsBos($value);
				break;
			case 82:
				$this->setIsBobda($value);
				break;
			case 83:
				$this->setIsNarsum($value);
				break;
			case 84:
				$this->setIsBagianHukumSetuju($value);
				break;
			case 85:
				$this->setIsInspektoratSetuju($value);
				break;
			case 86:
				$this->setIsBadanKepegawaianSetuju($value);
				break;
			case 87:
				$this->setIsLppaSetuju($value);
				break;
			case 88:
				$this->setPrioritasWali($value);
				break;
			case 89:
				$this->setIsOutput($value);
				break;
			case 90:
				$this->setIsBagianOrganisasiSetuju($value);
				break;
			case 91:
				$this->setIsAsisten1Setuju($value);
				break;
			case 92:
				$this->setIsAsisten2Setuju($value);
				break;
			case 93:
				$this->setIsAsisten3Setuju($value);
				break;
			case 94:
				$this->setIsSekdaSetuju($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = RincianDetailPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setKegiatanCode($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setTipe($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setDetailNo($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setRekeningCode($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setKomponenId($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setDetailName($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setVolume($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setKeteranganKoefisien($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setSubtitle($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setKomponenHarga($arr[$keys[9]]);
		if (array_key_exists($keys[10], $arr)) $this->setKomponenHargaAwal($arr[$keys[10]]);
		if (array_key_exists($keys[11], $arr)) $this->setKomponenName($arr[$keys[11]]);
		if (array_key_exists($keys[12], $arr)) $this->setSatuan($arr[$keys[12]]);
		if (array_key_exists($keys[13], $arr)) $this->setPajak($arr[$keys[13]]);
		if (array_key_exists($keys[14], $arr)) $this->setUnitId($arr[$keys[14]]);
		if (array_key_exists($keys[15], $arr)) $this->setFromSubKegiatan($arr[$keys[15]]);
		if (array_key_exists($keys[16], $arr)) $this->setSub($arr[$keys[16]]);
		if (array_key_exists($keys[17], $arr)) $this->setKodeSub($arr[$keys[17]]);
		if (array_key_exists($keys[18], $arr)) $this->setLastUpdateUser($arr[$keys[18]]);
		if (array_key_exists($keys[19], $arr)) $this->setLastUpdateTime($arr[$keys[19]]);
		if (array_key_exists($keys[20], $arr)) $this->setLastUpdateIp($arr[$keys[20]]);
		if (array_key_exists($keys[21], $arr)) $this->setTahap($arr[$keys[21]]);
		if (array_key_exists($keys[22], $arr)) $this->setTahapEdit($arr[$keys[22]]);
		if (array_key_exists($keys[23], $arr)) $this->setTahapNew($arr[$keys[23]]);
		if (array_key_exists($keys[24], $arr)) $this->setStatusLelang($arr[$keys[24]]);
		if (array_key_exists($keys[25], $arr)) $this->setNomorLelang($arr[$keys[25]]);
		if (array_key_exists($keys[26], $arr)) $this->setKoefisienSemula($arr[$keys[26]]);
		if (array_key_exists($keys[27], $arr)) $this->setVolumeSemula($arr[$keys[27]]);
		if (array_key_exists($keys[28], $arr)) $this->setHargaSemula($arr[$keys[28]]);
		if (array_key_exists($keys[29], $arr)) $this->setTotalSemula($arr[$keys[29]]);
		if (array_key_exists($keys[30], $arr)) $this->setLockSubtitle($arr[$keys[30]]);
		if (array_key_exists($keys[31], $arr)) $this->setStatusHapus($arr[$keys[31]]);
		if (array_key_exists($keys[32], $arr)) $this->setTahun($arr[$keys[32]]);
		if (array_key_exists($keys[33], $arr)) $this->setKodeLokasi($arr[$keys[33]]);
		if (array_key_exists($keys[34], $arr)) $this->setKecamatan($arr[$keys[34]]);
		if (array_key_exists($keys[35], $arr)) $this->setRekeningCodeAsli($arr[$keys[35]]);
		if (array_key_exists($keys[36], $arr)) $this->setNoteSkpd($arr[$keys[36]]);
		if (array_key_exists($keys[37], $arr)) $this->setNotePeneliti($arr[$keys[37]]);
		if (array_key_exists($keys[38], $arr)) $this->setNilaiAnggaran($arr[$keys[38]]);
		if (array_key_exists($keys[39], $arr)) $this->setIsBlud($arr[$keys[39]]);
		if (array_key_exists($keys[40], $arr)) $this->setIsKapitasiBpjs($arr[$keys[40]]);
		if (array_key_exists($keys[41], $arr)) $this->setLokasiKecamatan($arr[$keys[41]]);
		if (array_key_exists($keys[42], $arr)) $this->setLokasiKelurahan($arr[$keys[42]]);
		if (array_key_exists($keys[43], $arr)) $this->setOb($arr[$keys[43]]);
		if (array_key_exists($keys[44], $arr)) $this->setObFromId($arr[$keys[44]]);
		if (array_key_exists($keys[45], $arr)) $this->setIsPerKomponen($arr[$keys[45]]);
		if (array_key_exists($keys[46], $arr)) $this->setKegiatanCodeAsal($arr[$keys[46]]);
		if (array_key_exists($keys[47], $arr)) $this->setThKeMultiyears($arr[$keys[47]]);
		if (array_key_exists($keys[48], $arr)) $this->setHargaSebelumSisaLelang($arr[$keys[48]]);
		if (array_key_exists($keys[49], $arr)) $this->setIsMusrenbang($arr[$keys[49]]);
		if (array_key_exists($keys[50], $arr)) $this->setSubIdAsal($arr[$keys[50]]);
		if (array_key_exists($keys[51], $arr)) $this->setSubtitleAsal($arr[$keys[51]]);
		if (array_key_exists($keys[52], $arr)) $this->setKodeSubAsal($arr[$keys[52]]);
		if (array_key_exists($keys[53], $arr)) $this->setSubAsal($arr[$keys[53]]);
		if (array_key_exists($keys[54], $arr)) $this->setLastEditTime($arr[$keys[54]]);
		if (array_key_exists($keys[55], $arr)) $this->setIsPotongBpjs($arr[$keys[55]]);
		if (array_key_exists($keys[56], $arr)) $this->setIsIuranBpjs($arr[$keys[56]]);
		if (array_key_exists($keys[57], $arr)) $this->setStatusOb($arr[$keys[57]]);
		if (array_key_exists($keys[58], $arr)) $this->setObParent($arr[$keys[58]]);
		if (array_key_exists($keys[59], $arr)) $this->setObAlokasiBaru($arr[$keys[59]]);
		if (array_key_exists($keys[60], $arr)) $this->setIsHibah($arr[$keys[60]]);
		if (array_key_exists($keys[61], $arr)) $this->setAkrualCode($arr[$keys[61]]);
		if (array_key_exists($keys[62], $arr)) $this->setTipe2($arr[$keys[62]]);
		if (array_key_exists($keys[63], $arr)) $this->setStatusLevel($arr[$keys[63]]);
		if (array_key_exists($keys[64], $arr)) $this->setStatusLevelTolak($arr[$keys[64]]);
		if (array_key_exists($keys[65], $arr)) $this->setStatusSisipan($arr[$keys[65]]);
		if (array_key_exists($keys[66], $arr)) $this->setIsTapdSetuju($arr[$keys[66]]);
		if (array_key_exists($keys[67], $arr)) $this->setIsBappekoSetuju($arr[$keys[67]]);
		if (array_key_exists($keys[68], $arr)) $this->setIsPenyeliaSetuju($arr[$keys[68]]);
		if (array_key_exists($keys[69], $arr)) $this->setNoteTapd($arr[$keys[69]]);
		if (array_key_exists($keys[70], $arr)) $this->setNoteBappeko($arr[$keys[70]]);
		if (array_key_exists($keys[71], $arr)) $this->setSatuanSemula($arr[$keys[71]]);
		if (array_key_exists($keys[72], $arr)) $this->setIdLokasi($arr[$keys[72]]);
		if (array_key_exists($keys[73], $arr)) $this->setDetailKegiatan($arr[$keys[73]]);
		if (array_key_exists($keys[74], $arr)) $this->setDetailKegiatanSemula($arr[$keys[74]]);
		if (array_key_exists($keys[75], $arr)) $this->setStatusKomponenBaru($arr[$keys[75]]);
		if (array_key_exists($keys[76], $arr)) $this->setStatusKomponenBerubah($arr[$keys[76]]);
		if (array_key_exists($keys[77], $arr)) $this->setApproveUnlockHarga($arr[$keys[77]]);
		if (array_key_exists($keys[78], $arr)) $this->setTipeLelang($arr[$keys[78]]);
		if (array_key_exists($keys[79], $arr)) $this->setIsHpsp($arr[$keys[79]]);
		if (array_key_exists($keys[80], $arr)) $this->setIsDak($arr[$keys[80]]);
		if (array_key_exists($keys[81], $arr)) $this->setIsBos($arr[$keys[81]]);
		if (array_key_exists($keys[82], $arr)) $this->setIsBobda($arr[$keys[82]]);
		if (array_key_exists($keys[83], $arr)) $this->setIsNarsum($arr[$keys[83]]);
		if (array_key_exists($keys[84], $arr)) $this->setIsBagianHukumSetuju($arr[$keys[84]]);
		if (array_key_exists($keys[85], $arr)) $this->setIsInspektoratSetuju($arr[$keys[85]]);
		if (array_key_exists($keys[86], $arr)) $this->setIsBadanKepegawaianSetuju($arr[$keys[86]]);
		if (array_key_exists($keys[87], $arr)) $this->setIsLppaSetuju($arr[$keys[87]]);
		if (array_key_exists($keys[88], $arr)) $this->setPrioritasWali($arr[$keys[88]]);
		if (array_key_exists($keys[89], $arr)) $this->setIsOutput($arr[$keys[89]]);
		if (array_key_exists($keys[90], $arr)) $this->setIsBagianOrganisasiSetuju($arr[$keys[90]]);
		if (array_key_exists($keys[91], $arr)) $this->setIsAsisten1Setuju($arr[$keys[91]]);
		if (array_key_exists($keys[92], $arr)) $this->setIsAsisten2Setuju($arr[$keys[92]]);
		if (array_key_exists($keys[93], $arr)) $this->setIsAsisten3Setuju($arr[$keys[93]]);
		if (array_key_exists($keys[94], $arr)) $this->setIsSekdaSetuju($arr[$keys[94]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(RincianDetailPeer::DATABASE_NAME);

		if ($this->isColumnModified(RincianDetailPeer::KEGIATAN_CODE)) $criteria->add(RincianDetailPeer::KEGIATAN_CODE, $this->kegiatan_code);
		if ($this->isColumnModified(RincianDetailPeer::TIPE)) $criteria->add(RincianDetailPeer::TIPE, $this->tipe);
		if ($this->isColumnModified(RincianDetailPeer::DETAIL_NO)) $criteria->add(RincianDetailPeer::DETAIL_NO, $this->detail_no);
		if ($this->isColumnModified(RincianDetailPeer::REKENING_CODE)) $criteria->add(RincianDetailPeer::REKENING_CODE, $this->rekening_code);
		if ($this->isColumnModified(RincianDetailPeer::KOMPONEN_ID)) $criteria->add(RincianDetailPeer::KOMPONEN_ID, $this->komponen_id);
		if ($this->isColumnModified(RincianDetailPeer::DETAIL_NAME)) $criteria->add(RincianDetailPeer::DETAIL_NAME, $this->detail_name);
		if ($this->isColumnModified(RincianDetailPeer::VOLUME)) $criteria->add(RincianDetailPeer::VOLUME, $this->volume);
		if ($this->isColumnModified(RincianDetailPeer::KETERANGAN_KOEFISIEN)) $criteria->add(RincianDetailPeer::KETERANGAN_KOEFISIEN, $this->keterangan_koefisien);
		if ($this->isColumnModified(RincianDetailPeer::SUBTITLE)) $criteria->add(RincianDetailPeer::SUBTITLE, $this->subtitle);
		if ($this->isColumnModified(RincianDetailPeer::KOMPONEN_HARGA)) $criteria->add(RincianDetailPeer::KOMPONEN_HARGA, $this->komponen_harga);
		if ($this->isColumnModified(RincianDetailPeer::KOMPONEN_HARGA_AWAL)) $criteria->add(RincianDetailPeer::KOMPONEN_HARGA_AWAL, $this->komponen_harga_awal);
		if ($this->isColumnModified(RincianDetailPeer::KOMPONEN_NAME)) $criteria->add(RincianDetailPeer::KOMPONEN_NAME, $this->komponen_name);
		if ($this->isColumnModified(RincianDetailPeer::SATUAN)) $criteria->add(RincianDetailPeer::SATUAN, $this->satuan);
		if ($this->isColumnModified(RincianDetailPeer::PAJAK)) $criteria->add(RincianDetailPeer::PAJAK, $this->pajak);
		if ($this->isColumnModified(RincianDetailPeer::UNIT_ID)) $criteria->add(RincianDetailPeer::UNIT_ID, $this->unit_id);
		if ($this->isColumnModified(RincianDetailPeer::FROM_SUB_KEGIATAN)) $criteria->add(RincianDetailPeer::FROM_SUB_KEGIATAN, $this->from_sub_kegiatan);
		if ($this->isColumnModified(RincianDetailPeer::SUB)) $criteria->add(RincianDetailPeer::SUB, $this->sub);
		if ($this->isColumnModified(RincianDetailPeer::KODE_SUB)) $criteria->add(RincianDetailPeer::KODE_SUB, $this->kode_sub);
		if ($this->isColumnModified(RincianDetailPeer::LAST_UPDATE_USER)) $criteria->add(RincianDetailPeer::LAST_UPDATE_USER, $this->last_update_user);
		if ($this->isColumnModified(RincianDetailPeer::LAST_UPDATE_TIME)) $criteria->add(RincianDetailPeer::LAST_UPDATE_TIME, $this->last_update_time);
		if ($this->isColumnModified(RincianDetailPeer::LAST_UPDATE_IP)) $criteria->add(RincianDetailPeer::LAST_UPDATE_IP, $this->last_update_ip);
		if ($this->isColumnModified(RincianDetailPeer::TAHAP)) $criteria->add(RincianDetailPeer::TAHAP, $this->tahap);
		if ($this->isColumnModified(RincianDetailPeer::TAHAP_EDIT)) $criteria->add(RincianDetailPeer::TAHAP_EDIT, $this->tahap_edit);
		if ($this->isColumnModified(RincianDetailPeer::TAHAP_NEW)) $criteria->add(RincianDetailPeer::TAHAP_NEW, $this->tahap_new);
		if ($this->isColumnModified(RincianDetailPeer::STATUS_LELANG)) $criteria->add(RincianDetailPeer::STATUS_LELANG, $this->status_lelang);
		if ($this->isColumnModified(RincianDetailPeer::NOMOR_LELANG)) $criteria->add(RincianDetailPeer::NOMOR_LELANG, $this->nomor_lelang);
		if ($this->isColumnModified(RincianDetailPeer::KOEFISIEN_SEMULA)) $criteria->add(RincianDetailPeer::KOEFISIEN_SEMULA, $this->koefisien_semula);
		if ($this->isColumnModified(RincianDetailPeer::VOLUME_SEMULA)) $criteria->add(RincianDetailPeer::VOLUME_SEMULA, $this->volume_semula);
		if ($this->isColumnModified(RincianDetailPeer::HARGA_SEMULA)) $criteria->add(RincianDetailPeer::HARGA_SEMULA, $this->harga_semula);
		if ($this->isColumnModified(RincianDetailPeer::TOTAL_SEMULA)) $criteria->add(RincianDetailPeer::TOTAL_SEMULA, $this->total_semula);
		if ($this->isColumnModified(RincianDetailPeer::LOCK_SUBTITLE)) $criteria->add(RincianDetailPeer::LOCK_SUBTITLE, $this->lock_subtitle);
		if ($this->isColumnModified(RincianDetailPeer::STATUS_HAPUS)) $criteria->add(RincianDetailPeer::STATUS_HAPUS, $this->status_hapus);
		if ($this->isColumnModified(RincianDetailPeer::TAHUN)) $criteria->add(RincianDetailPeer::TAHUN, $this->tahun);
		if ($this->isColumnModified(RincianDetailPeer::KODE_LOKASI)) $criteria->add(RincianDetailPeer::KODE_LOKASI, $this->kode_lokasi);
		if ($this->isColumnModified(RincianDetailPeer::KECAMATAN)) $criteria->add(RincianDetailPeer::KECAMATAN, $this->kecamatan);
		if ($this->isColumnModified(RincianDetailPeer::REKENING_CODE_ASLI)) $criteria->add(RincianDetailPeer::REKENING_CODE_ASLI, $this->rekening_code_asli);
		if ($this->isColumnModified(RincianDetailPeer::NOTE_SKPD)) $criteria->add(RincianDetailPeer::NOTE_SKPD, $this->note_skpd);
		if ($this->isColumnModified(RincianDetailPeer::NOTE_PENELITI)) $criteria->add(RincianDetailPeer::NOTE_PENELITI, $this->note_peneliti);
		if ($this->isColumnModified(RincianDetailPeer::NILAI_ANGGARAN)) $criteria->add(RincianDetailPeer::NILAI_ANGGARAN, $this->nilai_anggaran);
		if ($this->isColumnModified(RincianDetailPeer::IS_BLUD)) $criteria->add(RincianDetailPeer::IS_BLUD, $this->is_blud);
		if ($this->isColumnModified(RincianDetailPeer::IS_KAPITASI_BPJS)) $criteria->add(RincianDetailPeer::IS_KAPITASI_BPJS, $this->is_kapitasi_bpjs);
		if ($this->isColumnModified(RincianDetailPeer::LOKASI_KECAMATAN)) $criteria->add(RincianDetailPeer::LOKASI_KECAMATAN, $this->lokasi_kecamatan);
		if ($this->isColumnModified(RincianDetailPeer::LOKASI_KELURAHAN)) $criteria->add(RincianDetailPeer::LOKASI_KELURAHAN, $this->lokasi_kelurahan);
		if ($this->isColumnModified(RincianDetailPeer::OB)) $criteria->add(RincianDetailPeer::OB, $this->ob);
		if ($this->isColumnModified(RincianDetailPeer::OB_FROM_ID)) $criteria->add(RincianDetailPeer::OB_FROM_ID, $this->ob_from_id);
		if ($this->isColumnModified(RincianDetailPeer::IS_PER_KOMPONEN)) $criteria->add(RincianDetailPeer::IS_PER_KOMPONEN, $this->is_per_komponen);
		if ($this->isColumnModified(RincianDetailPeer::KEGIATAN_CODE_ASAL)) $criteria->add(RincianDetailPeer::KEGIATAN_CODE_ASAL, $this->kegiatan_code_asal);
		if ($this->isColumnModified(RincianDetailPeer::TH_KE_MULTIYEARS)) $criteria->add(RincianDetailPeer::TH_KE_MULTIYEARS, $this->th_ke_multiyears);
		if ($this->isColumnModified(RincianDetailPeer::HARGA_SEBELUM_SISA_LELANG)) $criteria->add(RincianDetailPeer::HARGA_SEBELUM_SISA_LELANG, $this->harga_sebelum_sisa_lelang);
		if ($this->isColumnModified(RincianDetailPeer::IS_MUSRENBANG)) $criteria->add(RincianDetailPeer::IS_MUSRENBANG, $this->is_musrenbang);
		if ($this->isColumnModified(RincianDetailPeer::SUB_ID_ASAL)) $criteria->add(RincianDetailPeer::SUB_ID_ASAL, $this->sub_id_asal);
		if ($this->isColumnModified(RincianDetailPeer::SUBTITLE_ASAL)) $criteria->add(RincianDetailPeer::SUBTITLE_ASAL, $this->subtitle_asal);
		if ($this->isColumnModified(RincianDetailPeer::KODE_SUB_ASAL)) $criteria->add(RincianDetailPeer::KODE_SUB_ASAL, $this->kode_sub_asal);
		if ($this->isColumnModified(RincianDetailPeer::SUB_ASAL)) $criteria->add(RincianDetailPeer::SUB_ASAL, $this->sub_asal);
		if ($this->isColumnModified(RincianDetailPeer::LAST_EDIT_TIME)) $criteria->add(RincianDetailPeer::LAST_EDIT_TIME, $this->last_edit_time);
		if ($this->isColumnModified(RincianDetailPeer::IS_POTONG_BPJS)) $criteria->add(RincianDetailPeer::IS_POTONG_BPJS, $this->is_potong_bpjs);
		if ($this->isColumnModified(RincianDetailPeer::IS_IURAN_BPJS)) $criteria->add(RincianDetailPeer::IS_IURAN_BPJS, $this->is_iuran_bpjs);
		if ($this->isColumnModified(RincianDetailPeer::STATUS_OB)) $criteria->add(RincianDetailPeer::STATUS_OB, $this->status_ob);
		if ($this->isColumnModified(RincianDetailPeer::OB_PARENT)) $criteria->add(RincianDetailPeer::OB_PARENT, $this->ob_parent);
		if ($this->isColumnModified(RincianDetailPeer::OB_ALOKASI_BARU)) $criteria->add(RincianDetailPeer::OB_ALOKASI_BARU, $this->ob_alokasi_baru);
		if ($this->isColumnModified(RincianDetailPeer::IS_HIBAH)) $criteria->add(RincianDetailPeer::IS_HIBAH, $this->is_hibah);
		if ($this->isColumnModified(RincianDetailPeer::AKRUAL_CODE)) $criteria->add(RincianDetailPeer::AKRUAL_CODE, $this->akrual_code);
		if ($this->isColumnModified(RincianDetailPeer::TIPE2)) $criteria->add(RincianDetailPeer::TIPE2, $this->tipe2);
		if ($this->isColumnModified(RincianDetailPeer::STATUS_LEVEL)) $criteria->add(RincianDetailPeer::STATUS_LEVEL, $this->status_level);
		if ($this->isColumnModified(RincianDetailPeer::STATUS_LEVEL_TOLAK)) $criteria->add(RincianDetailPeer::STATUS_LEVEL_TOLAK, $this->status_level_tolak);
		if ($this->isColumnModified(RincianDetailPeer::STATUS_SISIPAN)) $criteria->add(RincianDetailPeer::STATUS_SISIPAN, $this->status_sisipan);
		if ($this->isColumnModified(RincianDetailPeer::IS_TAPD_SETUJU)) $criteria->add(RincianDetailPeer::IS_TAPD_SETUJU, $this->is_tapd_setuju);
		if ($this->isColumnModified(RincianDetailPeer::IS_BAPPEKO_SETUJU)) $criteria->add(RincianDetailPeer::IS_BAPPEKO_SETUJU, $this->is_bappeko_setuju);
		if ($this->isColumnModified(RincianDetailPeer::IS_PENYELIA_SETUJU)) $criteria->add(RincianDetailPeer::IS_PENYELIA_SETUJU, $this->is_penyelia_setuju);
		if ($this->isColumnModified(RincianDetailPeer::NOTE_TAPD)) $criteria->add(RincianDetailPeer::NOTE_TAPD, $this->note_tapd);
		if ($this->isColumnModified(RincianDetailPeer::NOTE_BAPPEKO)) $criteria->add(RincianDetailPeer::NOTE_BAPPEKO, $this->note_bappeko);
		if ($this->isColumnModified(RincianDetailPeer::SATUAN_SEMULA)) $criteria->add(RincianDetailPeer::SATUAN_SEMULA, $this->satuan_semula);
		if ($this->isColumnModified(RincianDetailPeer::ID_LOKASI)) $criteria->add(RincianDetailPeer::ID_LOKASI, $this->id_lokasi);
		if ($this->isColumnModified(RincianDetailPeer::DETAIL_KEGIATAN)) $criteria->add(RincianDetailPeer::DETAIL_KEGIATAN, $this->detail_kegiatan);
		if ($this->isColumnModified(RincianDetailPeer::DETAIL_KEGIATAN_SEMULA)) $criteria->add(RincianDetailPeer::DETAIL_KEGIATAN_SEMULA, $this->detail_kegiatan_semula);
		if ($this->isColumnModified(RincianDetailPeer::STATUS_KOMPONEN_BARU)) $criteria->add(RincianDetailPeer::STATUS_KOMPONEN_BARU, $this->status_komponen_baru);
		if ($this->isColumnModified(RincianDetailPeer::STATUS_KOMPONEN_BERUBAH)) $criteria->add(RincianDetailPeer::STATUS_KOMPONEN_BERUBAH, $this->status_komponen_berubah);
		if ($this->isColumnModified(RincianDetailPeer::APPROVE_UNLOCK_HARGA)) $criteria->add(RincianDetailPeer::APPROVE_UNLOCK_HARGA, $this->approve_unlock_harga);
		if ($this->isColumnModified(RincianDetailPeer::TIPE_LELANG)) $criteria->add(RincianDetailPeer::TIPE_LELANG, $this->tipe_lelang);
		if ($this->isColumnModified(RincianDetailPeer::IS_HPSP)) $criteria->add(RincianDetailPeer::IS_HPSP, $this->is_hpsp);
		if ($this->isColumnModified(RincianDetailPeer::IS_DAK)) $criteria->add(RincianDetailPeer::IS_DAK, $this->is_dak);
		if ($this->isColumnModified(RincianDetailPeer::IS_BOS)) $criteria->add(RincianDetailPeer::IS_BOS, $this->is_bos);
		if ($this->isColumnModified(RincianDetailPeer::IS_BOBDA)) $criteria->add(RincianDetailPeer::IS_BOBDA, $this->is_bobda);
		if ($this->isColumnModified(RincianDetailPeer::IS_NARSUM)) $criteria->add(RincianDetailPeer::IS_NARSUM, $this->is_narsum);
		if ($this->isColumnModified(RincianDetailPeer::IS_BAGIAN_HUKUM_SETUJU)) $criteria->add(RincianDetailPeer::IS_BAGIAN_HUKUM_SETUJU, $this->is_bagian_hukum_setuju);
		if ($this->isColumnModified(RincianDetailPeer::IS_INSPEKTORAT_SETUJU)) $criteria->add(RincianDetailPeer::IS_INSPEKTORAT_SETUJU, $this->is_inspektorat_setuju);
		if ($this->isColumnModified(RincianDetailPeer::IS_BADAN_KEPEGAWAIAN_SETUJU)) $criteria->add(RincianDetailPeer::IS_BADAN_KEPEGAWAIAN_SETUJU, $this->is_badan_kepegawaian_setuju);
		if ($this->isColumnModified(RincianDetailPeer::IS_LPPA_SETUJU)) $criteria->add(RincianDetailPeer::IS_LPPA_SETUJU, $this->is_lppa_setuju);
		if ($this->isColumnModified(RincianDetailPeer::PRIORITAS_WALI)) $criteria->add(RincianDetailPeer::PRIORITAS_WALI, $this->prioritas_wali);
		if ($this->isColumnModified(RincianDetailPeer::IS_OUTPUT)) $criteria->add(RincianDetailPeer::IS_OUTPUT, $this->is_output);
		if ($this->isColumnModified(RincianDetailPeer::IS_BAGIAN_ORGANISASI_SETUJU)) $criteria->add(RincianDetailPeer::IS_BAGIAN_ORGANISASI_SETUJU, $this->is_bagian_organisasi_setuju);
		if ($this->isColumnModified(RincianDetailPeer::IS_ASISTEN1_SETUJU)) $criteria->add(RincianDetailPeer::IS_ASISTEN1_SETUJU, $this->is_asisten1_setuju);
		if ($this->isColumnModified(RincianDetailPeer::IS_ASISTEN2_SETUJU)) $criteria->add(RincianDetailPeer::IS_ASISTEN2_SETUJU, $this->is_asisten2_setuju);
		if ($this->isColumnModified(RincianDetailPeer::IS_ASISTEN3_SETUJU)) $criteria->add(RincianDetailPeer::IS_ASISTEN3_SETUJU, $this->is_asisten3_setuju);
		if ($this->isColumnModified(RincianDetailPeer::IS_SEKDA_SETUJU)) $criteria->add(RincianDetailPeer::IS_SEKDA_SETUJU, $this->is_sekda_setuju);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(RincianDetailPeer::DATABASE_NAME);

		$criteria->add(RincianDetailPeer::KEGIATAN_CODE, $this->kegiatan_code);
		$criteria->add(RincianDetailPeer::DETAIL_NO, $this->detail_no);
		$criteria->add(RincianDetailPeer::UNIT_ID, $this->unit_id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		$pks = array();

		$pks[0] = $this->getKegiatanCode();

		$pks[1] = $this->getDetailNo();

		$pks[2] = $this->getUnitId();

		return $pks;
	}

	
	public function setPrimaryKey($keys)
	{

		$this->setKegiatanCode($keys[0]);

		$this->setDetailNo($keys[1]);

		$this->setUnitId($keys[2]);

	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setTipe($this->tipe);

		$copyObj->setRekeningCode($this->rekening_code);

		$copyObj->setKomponenId($this->komponen_id);

		$copyObj->setDetailName($this->detail_name);

		$copyObj->setVolume($this->volume);

		$copyObj->setKeteranganKoefisien($this->keterangan_koefisien);

		$copyObj->setSubtitle($this->subtitle);

		$copyObj->setKomponenHarga($this->komponen_harga);

		$copyObj->setKomponenHargaAwal($this->komponen_harga_awal);

		$copyObj->setKomponenName($this->komponen_name);

		$copyObj->setSatuan($this->satuan);

		$copyObj->setPajak($this->pajak);

		$copyObj->setFromSubKegiatan($this->from_sub_kegiatan);

		$copyObj->setSub($this->sub);

		$copyObj->setKodeSub($this->kode_sub);

		$copyObj->setLastUpdateUser($this->last_update_user);

		$copyObj->setLastUpdateTime($this->last_update_time);

		$copyObj->setLastUpdateIp($this->last_update_ip);

		$copyObj->setTahap($this->tahap);

		$copyObj->setTahapEdit($this->tahap_edit);

		$copyObj->setTahapNew($this->tahap_new);

		$copyObj->setStatusLelang($this->status_lelang);

		$copyObj->setNomorLelang($this->nomor_lelang);

		$copyObj->setKoefisienSemula($this->koefisien_semula);

		$copyObj->setVolumeSemula($this->volume_semula);

		$copyObj->setHargaSemula($this->harga_semula);

		$copyObj->setTotalSemula($this->total_semula);

		$copyObj->setLockSubtitle($this->lock_subtitle);

		$copyObj->setStatusHapus($this->status_hapus);

		$copyObj->setTahun($this->tahun);

		$copyObj->setKodeLokasi($this->kode_lokasi);

		$copyObj->setKecamatan($this->kecamatan);

		$copyObj->setRekeningCodeAsli($this->rekening_code_asli);

		$copyObj->setNoteSkpd($this->note_skpd);

		$copyObj->setNotePeneliti($this->note_peneliti);

		$copyObj->setNilaiAnggaran($this->nilai_anggaran);

		$copyObj->setIsBlud($this->is_blud);

		$copyObj->setIsKapitasiBpjs($this->is_kapitasi_bpjs);

		$copyObj->setLokasiKecamatan($this->lokasi_kecamatan);

		$copyObj->setLokasiKelurahan($this->lokasi_kelurahan);

		$copyObj->setOb($this->ob);

		$copyObj->setObFromId($this->ob_from_id);

		$copyObj->setIsPerKomponen($this->is_per_komponen);

		$copyObj->setKegiatanCodeAsal($this->kegiatan_code_asal);

		$copyObj->setThKeMultiyears($this->th_ke_multiyears);

		$copyObj->setHargaSebelumSisaLelang($this->harga_sebelum_sisa_lelang);

		$copyObj->setIsMusrenbang($this->is_musrenbang);

		$copyObj->setSubIdAsal($this->sub_id_asal);

		$copyObj->setSubtitleAsal($this->subtitle_asal);

		$copyObj->setKodeSubAsal($this->kode_sub_asal);

		$copyObj->setSubAsal($this->sub_asal);

		$copyObj->setLastEditTime($this->last_edit_time);

		$copyObj->setIsPotongBpjs($this->is_potong_bpjs);

		$copyObj->setIsIuranBpjs($this->is_iuran_bpjs);

		$copyObj->setStatusOb($this->status_ob);

		$copyObj->setObParent($this->ob_parent);

		$copyObj->setObAlokasiBaru($this->ob_alokasi_baru);

		$copyObj->setIsHibah($this->is_hibah);

		$copyObj->setAkrualCode($this->akrual_code);

		$copyObj->setTipe2($this->tipe2);

		$copyObj->setStatusLevel($this->status_level);

		$copyObj->setStatusLevelTolak($this->status_level_tolak);

		$copyObj->setStatusSisipan($this->status_sisipan);

		$copyObj->setIsTapdSetuju($this->is_tapd_setuju);

		$copyObj->setIsBappekoSetuju($this->is_bappeko_setuju);

		$copyObj->setIsPenyeliaSetuju($this->is_penyelia_setuju);

		$copyObj->setNoteTapd($this->note_tapd);

		$copyObj->setNoteBappeko($this->note_bappeko);

		$copyObj->setSatuanSemula($this->satuan_semula);

		$copyObj->setIdLokasi($this->id_lokasi);

		$copyObj->setDetailKegiatan($this->detail_kegiatan);

		$copyObj->setDetailKegiatanSemula($this->detail_kegiatan_semula);

		$copyObj->setStatusKomponenBaru($this->status_komponen_baru);

		$copyObj->setStatusKomponenBerubah($this->status_komponen_berubah);

		$copyObj->setApproveUnlockHarga($this->approve_unlock_harga);

		$copyObj->setTipeLelang($this->tipe_lelang);

		$copyObj->setIsHpsp($this->is_hpsp);

		$copyObj->setIsDak($this->is_dak);

		$copyObj->setIsBos($this->is_bos);

		$copyObj->setIsBobda($this->is_bobda);

		$copyObj->setIsNarsum($this->is_narsum);

		$copyObj->setIsBagianHukumSetuju($this->is_bagian_hukum_setuju);

		$copyObj->setIsInspektoratSetuju($this->is_inspektorat_setuju);

		$copyObj->setIsBadanKepegawaianSetuju($this->is_badan_kepegawaian_setuju);

		$copyObj->setIsLppaSetuju($this->is_lppa_setuju);

		$copyObj->setPrioritasWali($this->prioritas_wali);

		$copyObj->setIsOutput($this->is_output);

		$copyObj->setIsBagianOrganisasiSetuju($this->is_bagian_organisasi_setuju);

		$copyObj->setIsAsisten1Setuju($this->is_asisten1_setuju);

		$copyObj->setIsAsisten2Setuju($this->is_asisten2_setuju);

		$copyObj->setIsAsisten3Setuju($this->is_asisten3_setuju);

		$copyObj->setIsSekdaSetuju($this->is_sekda_setuju);


		$copyObj->setNew(true);

		$copyObj->setKegiatanCode(NULL); 
		$copyObj->setDetailNo(NULL); 
		$copyObj->setUnitId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new RincianDetailPeer();
		}
		return self::$peer;
	}

} 