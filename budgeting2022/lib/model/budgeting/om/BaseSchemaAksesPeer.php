<?php


abstract class BaseSchemaAksesPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'schema_akses';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.SchemaAkses';

	
	const NUM_COLUMNS = 3;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const USER_ID = 'schema_akses.USER_ID';

	
	const SCHEMA_ID = 'schema_akses.SCHEMA_ID';

	
	const LEVEL_ID = 'schema_akses.LEVEL_ID';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('UserId', 'SchemaId', 'LevelId', ),
		BasePeer::TYPE_COLNAME => array (SchemaAksesPeer::USER_ID, SchemaAksesPeer::SCHEMA_ID, SchemaAksesPeer::LEVEL_ID, ),
		BasePeer::TYPE_FIELDNAME => array ('user_id', 'schema_id', 'level_id', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('UserId' => 0, 'SchemaId' => 1, 'LevelId' => 2, ),
		BasePeer::TYPE_COLNAME => array (SchemaAksesPeer::USER_ID => 0, SchemaAksesPeer::SCHEMA_ID => 1, SchemaAksesPeer::LEVEL_ID => 2, ),
		BasePeer::TYPE_FIELDNAME => array ('user_id' => 0, 'schema_id' => 1, 'level_id' => 2, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/SchemaAksesMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.SchemaAksesMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = SchemaAksesPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(SchemaAksesPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(SchemaAksesPeer::USER_ID);

		$criteria->addSelectColumn(SchemaAksesPeer::SCHEMA_ID);

		$criteria->addSelectColumn(SchemaAksesPeer::LEVEL_ID);

	}

	const COUNT = 'COUNT(schema_akses.USER_ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT schema_akses.USER_ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(SchemaAksesPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(SchemaAksesPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = SchemaAksesPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = SchemaAksesPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return SchemaAksesPeer::populateObjects(SchemaAksesPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			SchemaAksesPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = SchemaAksesPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}

	
	public static function doCountJoinMasterUser(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(SchemaAksesPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(SchemaAksesPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(SchemaAksesPeer::USER_ID, MasterUserPeer::USER_ID);

		$rs = SchemaAksesPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doCountJoinMasterSchema(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(SchemaAksesPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(SchemaAksesPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(SchemaAksesPeer::SCHEMA_ID, MasterSchemaPeer::SCHEMA_ID);

		$rs = SchemaAksesPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doCountJoinUserLevel(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(SchemaAksesPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(SchemaAksesPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(SchemaAksesPeer::LEVEL_ID, UserLevelPeer::LEVEL_ID);

		$rs = SchemaAksesPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doSelectJoinMasterUser(Criteria $c, $con = null)
	{
		$c = clone $c;

				if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		SchemaAksesPeer::addSelectColumns($c);
		$startcol = (SchemaAksesPeer::NUM_COLUMNS - SchemaAksesPeer::NUM_LAZY_LOAD_COLUMNS) + 1;
		MasterUserPeer::addSelectColumns($c);

		$c->addJoin(SchemaAksesPeer::USER_ID, MasterUserPeer::USER_ID);
		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = SchemaAksesPeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = MasterUserPeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj2 = new $cls();
			$obj2->hydrate($rs, $startcol);

			$newObject = true;
			foreach($results as $temp_obj1) {
				$temp_obj2 = $temp_obj1->getMasterUser(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
										$temp_obj2->addSchemaAkses($obj1); 					break;
				}
			}
			if ($newObject) {
				$obj2->initSchemaAksess();
				$obj2->addSchemaAkses($obj1); 			}
			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doSelectJoinMasterSchema(Criteria $c, $con = null)
	{
		$c = clone $c;

				if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		SchemaAksesPeer::addSelectColumns($c);
		$startcol = (SchemaAksesPeer::NUM_COLUMNS - SchemaAksesPeer::NUM_LAZY_LOAD_COLUMNS) + 1;
		MasterSchemaPeer::addSelectColumns($c);

		$c->addJoin(SchemaAksesPeer::SCHEMA_ID, MasterSchemaPeer::SCHEMA_ID);
		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = SchemaAksesPeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = MasterSchemaPeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj2 = new $cls();
			$obj2->hydrate($rs, $startcol);

			$newObject = true;
			foreach($results as $temp_obj1) {
				$temp_obj2 = $temp_obj1->getMasterSchema(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
										$temp_obj2->addSchemaAkses($obj1); 					break;
				}
			}
			if ($newObject) {
				$obj2->initSchemaAksess();
				$obj2->addSchemaAkses($obj1); 			}
			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doSelectJoinUserLevel(Criteria $c, $con = null)
	{
		$c = clone $c;

				if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		SchemaAksesPeer::addSelectColumns($c);
		$startcol = (SchemaAksesPeer::NUM_COLUMNS - SchemaAksesPeer::NUM_LAZY_LOAD_COLUMNS) + 1;
		UserLevelPeer::addSelectColumns($c);

		$c->addJoin(SchemaAksesPeer::LEVEL_ID, UserLevelPeer::LEVEL_ID);
		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = SchemaAksesPeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = UserLevelPeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj2 = new $cls();
			$obj2->hydrate($rs, $startcol);

			$newObject = true;
			foreach($results as $temp_obj1) {
				$temp_obj2 = $temp_obj1->getUserLevel(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
										$temp_obj2->addSchemaAkses($obj1); 					break;
				}
			}
			if ($newObject) {
				$obj2->initSchemaAksess();
				$obj2->addSchemaAkses($obj1); 			}
			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doCountJoinAll(Criteria $criteria, $distinct = false, $con = null)
	{
		$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(SchemaAksesPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(SchemaAksesPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(SchemaAksesPeer::USER_ID, MasterUserPeer::USER_ID);

		$criteria->addJoin(SchemaAksesPeer::SCHEMA_ID, MasterSchemaPeer::SCHEMA_ID);

		$criteria->addJoin(SchemaAksesPeer::LEVEL_ID, UserLevelPeer::LEVEL_ID);

		$rs = SchemaAksesPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doSelectJoinAll(Criteria $c, $con = null)
	{
		$c = clone $c;

				if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		SchemaAksesPeer::addSelectColumns($c);
		$startcol2 = (SchemaAksesPeer::NUM_COLUMNS - SchemaAksesPeer::NUM_LAZY_LOAD_COLUMNS) + 1;

		MasterUserPeer::addSelectColumns($c);
		$startcol3 = $startcol2 + MasterUserPeer::NUM_COLUMNS;

		MasterSchemaPeer::addSelectColumns($c);
		$startcol4 = $startcol3 + MasterSchemaPeer::NUM_COLUMNS;

		UserLevelPeer::addSelectColumns($c);
		$startcol5 = $startcol4 + UserLevelPeer::NUM_COLUMNS;

		$c->addJoin(SchemaAksesPeer::USER_ID, MasterUserPeer::USER_ID);

		$c->addJoin(SchemaAksesPeer::SCHEMA_ID, MasterSchemaPeer::SCHEMA_ID);

		$c->addJoin(SchemaAksesPeer::LEVEL_ID, UserLevelPeer::LEVEL_ID);

		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = SchemaAksesPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);


					
			$omClass = MasterUserPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj2 = new $cls();
			$obj2->hydrate($rs, $startcol2);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj2 = $temp_obj1->getMasterUser(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
					$temp_obj2->addSchemaAkses($obj1); 					break;
				}
			}

			if ($newObject) {
				$obj2->initSchemaAksess();
				$obj2->addSchemaAkses($obj1);
			}


					
			$omClass = MasterSchemaPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj3 = new $cls();
			$obj3->hydrate($rs, $startcol3);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj3 = $temp_obj1->getMasterSchema(); 				if ($temp_obj3->getPrimaryKey() === $obj3->getPrimaryKey()) {
					$newObject = false;
					$temp_obj3->addSchemaAkses($obj1); 					break;
				}
			}

			if ($newObject) {
				$obj3->initSchemaAksess();
				$obj3->addSchemaAkses($obj1);
			}


					
			$omClass = UserLevelPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj4 = new $cls();
			$obj4->hydrate($rs, $startcol4);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj4 = $temp_obj1->getUserLevel(); 				if ($temp_obj4->getPrimaryKey() === $obj4->getPrimaryKey()) {
					$newObject = false;
					$temp_obj4->addSchemaAkses($obj1); 					break;
				}
			}

			if ($newObject) {
				$obj4->initSchemaAksess();
				$obj4->addSchemaAkses($obj1);
			}

			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doCountJoinAllExceptMasterUser(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(SchemaAksesPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(SchemaAksesPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(SchemaAksesPeer::SCHEMA_ID, MasterSchemaPeer::SCHEMA_ID);

		$criteria->addJoin(SchemaAksesPeer::LEVEL_ID, UserLevelPeer::LEVEL_ID);

		$rs = SchemaAksesPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doCountJoinAllExceptMasterSchema(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(SchemaAksesPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(SchemaAksesPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(SchemaAksesPeer::USER_ID, MasterUserPeer::USER_ID);

		$criteria->addJoin(SchemaAksesPeer::LEVEL_ID, UserLevelPeer::LEVEL_ID);

		$rs = SchemaAksesPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doCountJoinAllExceptUserLevel(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(SchemaAksesPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(SchemaAksesPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$criteria->addJoin(SchemaAksesPeer::USER_ID, MasterUserPeer::USER_ID);

		$criteria->addJoin(SchemaAksesPeer::SCHEMA_ID, MasterSchemaPeer::SCHEMA_ID);

		$rs = SchemaAksesPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}


	
	public static function doSelectJoinAllExceptMasterUser(Criteria $c, $con = null)
	{
		$c = clone $c;

								if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		SchemaAksesPeer::addSelectColumns($c);
		$startcol2 = (SchemaAksesPeer::NUM_COLUMNS - SchemaAksesPeer::NUM_LAZY_LOAD_COLUMNS) + 1;

		MasterSchemaPeer::addSelectColumns($c);
		$startcol3 = $startcol2 + MasterSchemaPeer::NUM_COLUMNS;

		UserLevelPeer::addSelectColumns($c);
		$startcol4 = $startcol3 + UserLevelPeer::NUM_COLUMNS;

		$c->addJoin(SchemaAksesPeer::SCHEMA_ID, MasterSchemaPeer::SCHEMA_ID);

		$c->addJoin(SchemaAksesPeer::LEVEL_ID, UserLevelPeer::LEVEL_ID);


		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = SchemaAksesPeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = MasterSchemaPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj2  = new $cls();
			$obj2->hydrate($rs, $startcol2);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj2 = $temp_obj1->getMasterSchema(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
					$temp_obj2->addSchemaAkses($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj2->initSchemaAksess();
				$obj2->addSchemaAkses($obj1);
			}

			$omClass = UserLevelPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj3  = new $cls();
			$obj3->hydrate($rs, $startcol3);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj3 = $temp_obj1->getUserLevel(); 				if ($temp_obj3->getPrimaryKey() === $obj3->getPrimaryKey()) {
					$newObject = false;
					$temp_obj3->addSchemaAkses($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj3->initSchemaAksess();
				$obj3->addSchemaAkses($obj1);
			}

			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doSelectJoinAllExceptMasterSchema(Criteria $c, $con = null)
	{
		$c = clone $c;

								if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		SchemaAksesPeer::addSelectColumns($c);
		$startcol2 = (SchemaAksesPeer::NUM_COLUMNS - SchemaAksesPeer::NUM_LAZY_LOAD_COLUMNS) + 1;

		MasterUserPeer::addSelectColumns($c);
		$startcol3 = $startcol2 + MasterUserPeer::NUM_COLUMNS;

		UserLevelPeer::addSelectColumns($c);
		$startcol4 = $startcol3 + UserLevelPeer::NUM_COLUMNS;

		$c->addJoin(SchemaAksesPeer::USER_ID, MasterUserPeer::USER_ID);

		$c->addJoin(SchemaAksesPeer::LEVEL_ID, UserLevelPeer::LEVEL_ID);


		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = SchemaAksesPeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = MasterUserPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj2  = new $cls();
			$obj2->hydrate($rs, $startcol2);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj2 = $temp_obj1->getMasterUser(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
					$temp_obj2->addSchemaAkses($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj2->initSchemaAksess();
				$obj2->addSchemaAkses($obj1);
			}

			$omClass = UserLevelPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj3  = new $cls();
			$obj3->hydrate($rs, $startcol3);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj3 = $temp_obj1->getUserLevel(); 				if ($temp_obj3->getPrimaryKey() === $obj3->getPrimaryKey()) {
					$newObject = false;
					$temp_obj3->addSchemaAkses($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj3->initSchemaAksess();
				$obj3->addSchemaAkses($obj1);
			}

			$results[] = $obj1;
		}
		return $results;
	}


	
	public static function doSelectJoinAllExceptUserLevel(Criteria $c, $con = null)
	{
		$c = clone $c;

								if ($c->getDbName() == Propel::getDefaultDB()) {
			$c->setDbName(self::DATABASE_NAME);
		}

		SchemaAksesPeer::addSelectColumns($c);
		$startcol2 = (SchemaAksesPeer::NUM_COLUMNS - SchemaAksesPeer::NUM_LAZY_LOAD_COLUMNS) + 1;

		MasterUserPeer::addSelectColumns($c);
		$startcol3 = $startcol2 + MasterUserPeer::NUM_COLUMNS;

		MasterSchemaPeer::addSelectColumns($c);
		$startcol4 = $startcol3 + MasterSchemaPeer::NUM_COLUMNS;

		$c->addJoin(SchemaAksesPeer::USER_ID, MasterUserPeer::USER_ID);

		$c->addJoin(SchemaAksesPeer::SCHEMA_ID, MasterSchemaPeer::SCHEMA_ID);


		$rs = BasePeer::doSelect($c, $con);
		$results = array();

		while($rs->next()) {

			$omClass = SchemaAksesPeer::getOMClass();

			$cls = Propel::import($omClass);
			$obj1 = new $cls();
			$obj1->hydrate($rs);

			$omClass = MasterUserPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj2  = new $cls();
			$obj2->hydrate($rs, $startcol2);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj2 = $temp_obj1->getMasterUser(); 				if ($temp_obj2->getPrimaryKey() === $obj2->getPrimaryKey()) {
					$newObject = false;
					$temp_obj2->addSchemaAkses($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj2->initSchemaAksess();
				$obj2->addSchemaAkses($obj1);
			}

			$omClass = MasterSchemaPeer::getOMClass();


			$cls = Propel::import($omClass);
			$obj3  = new $cls();
			$obj3->hydrate($rs, $startcol3);

			$newObject = true;
			for ($j=0, $resCount=count($results); $j < $resCount; $j++) {
				$temp_obj1 = $results[$j];
				$temp_obj3 = $temp_obj1->getMasterSchema(); 				if ($temp_obj3->getPrimaryKey() === $obj3->getPrimaryKey()) {
					$newObject = false;
					$temp_obj3->addSchemaAkses($obj1);
					break;
				}
			}

			if ($newObject) {
				$obj3->initSchemaAksess();
				$obj3->addSchemaAkses($obj1);
			}

			$results[] = $obj1;
		}
		return $results;
	}

	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return SchemaAksesPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(SchemaAksesPeer::USER_ID);
			$selectCriteria->add(SchemaAksesPeer::USER_ID, $criteria->remove(SchemaAksesPeer::USER_ID), $comparison);

			$comparison = $criteria->getComparison(SchemaAksesPeer::SCHEMA_ID);
			$selectCriteria->add(SchemaAksesPeer::SCHEMA_ID, $criteria->remove(SchemaAksesPeer::SCHEMA_ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(SchemaAksesPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(SchemaAksesPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof SchemaAkses) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
												if(count($values) == count($values, COUNT_RECURSIVE))
			{
								$values = array($values);
			}
			$vals = array();
			foreach($values as $value)
			{

				$vals[0][] = $value[0];
				$vals[1][] = $value[1];
			}

			$criteria->add(SchemaAksesPeer::USER_ID, $vals[0], Criteria::IN);
			$criteria->add(SchemaAksesPeer::SCHEMA_ID, $vals[1], Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(SchemaAkses $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(SchemaAksesPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(SchemaAksesPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(SchemaAksesPeer::DATABASE_NAME, SchemaAksesPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = SchemaAksesPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK( $user_id, $schema_id, $con = null) {
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$criteria = new Criteria();
		$criteria->add(SchemaAksesPeer::USER_ID, $user_id);
		$criteria->add(SchemaAksesPeer::SCHEMA_ID, $schema_id);
		$v = SchemaAksesPeer::doSelect($criteria, $con);

		return !empty($v) ? $v[0] : null;
	}
} 
if (Propel::isInit()) {
			try {
		BaseSchemaAksesPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/SchemaAksesMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.SchemaAksesMapBuilder');
}
