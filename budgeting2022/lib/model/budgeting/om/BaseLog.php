<?php


abstract class BaseLog extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $log_id;


	
	protected $waktu_access;


	
	protected $ip_address;


	
	protected $login;


	
	protected $tabel_asal;


	
	protected $isi1;


	
	protected $isi2;


	
	protected $isi3;


	
	protected $isi4;


	
	protected $isi5;


	
	protected $isi6;


	
	protected $isi7;


	
	protected $isi8;


	
	protected $isi9;


	
	protected $isi10;


	
	protected $isi11;


	
	protected $isi12;


	
	protected $isi13;


	
	protected $isi14;


	
	protected $isi15;


	
	protected $isi16;


	
	protected $isi17;


	
	protected $isi18;


	
	protected $isi19;


	
	protected $isi20;


	
	protected $isi21;


	
	protected $isi22;


	
	protected $isi23;


	
	protected $isi24;


	
	protected $isi25;


	
	protected $isi26;


	
	protected $isi27;


	
	protected $isi28;


	
	protected $isi29;


	
	protected $isi30;


	
	protected $isi31;


	
	protected $isi32;


	
	protected $isi33;


	
	protected $isi34;


	
	protected $isi35;


	
	protected $isi36;


	
	protected $isi37;


	
	protected $isi38;


	
	protected $isi39;


	
	protected $isi40;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getLogId()
	{

		return $this->log_id;
	}

	
	public function getWaktuAccess($format = 'Y-m-d H:i:s')
	{

		if ($this->waktu_access === null || $this->waktu_access === '') {
			return null;
		} elseif (!is_int($this->waktu_access)) {
						$ts = strtotime($this->waktu_access);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [waktu_access] as date/time value: " . var_export($this->waktu_access, true));
			}
		} else {
			$ts = $this->waktu_access;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getIpAddress()
	{

		return $this->ip_address;
	}

	
	public function getLogin()
	{

		return $this->login;
	}

	
	public function getTabelAsal()
	{

		return $this->tabel_asal;
	}

	
	public function getIsi1()
	{

		return $this->isi1;
	}

	
	public function getIsi2()
	{

		return $this->isi2;
	}

	
	public function getIsi3()
	{

		return $this->isi3;
	}

	
	public function getIsi4()
	{

		return $this->isi4;
	}

	
	public function getIsi5()
	{

		return $this->isi5;
	}

	
	public function getIsi6()
	{

		return $this->isi6;
	}

	
	public function getIsi7()
	{

		return $this->isi7;
	}

	
	public function getIsi8()
	{

		return $this->isi8;
	}

	
	public function getIsi9()
	{

		return $this->isi9;
	}

	
	public function getIsi10()
	{

		return $this->isi10;
	}

	
	public function getIsi11()
	{

		return $this->isi11;
	}

	
	public function getIsi12()
	{

		return $this->isi12;
	}

	
	public function getIsi13()
	{

		return $this->isi13;
	}

	
	public function getIsi14()
	{

		return $this->isi14;
	}

	
	public function getIsi15()
	{

		return $this->isi15;
	}

	
	public function getIsi16()
	{

		return $this->isi16;
	}

	
	public function getIsi17()
	{

		return $this->isi17;
	}

	
	public function getIsi18()
	{

		return $this->isi18;
	}

	
	public function getIsi19()
	{

		return $this->isi19;
	}

	
	public function getIsi20()
	{

		return $this->isi20;
	}

	
	public function getIsi21()
	{

		return $this->isi21;
	}

	
	public function getIsi22()
	{

		return $this->isi22;
	}

	
	public function getIsi23()
	{

		return $this->isi23;
	}

	
	public function getIsi24()
	{

		return $this->isi24;
	}

	
	public function getIsi25()
	{

		return $this->isi25;
	}

	
	public function getIsi26()
	{

		return $this->isi26;
	}

	
	public function getIsi27()
	{

		return $this->isi27;
	}

	
	public function getIsi28()
	{

		return $this->isi28;
	}

	
	public function getIsi29()
	{

		return $this->isi29;
	}

	
	public function getIsi30()
	{

		return $this->isi30;
	}

	
	public function getIsi31()
	{

		return $this->isi31;
	}

	
	public function getIsi32()
	{

		return $this->isi32;
	}

	
	public function getIsi33()
	{

		return $this->isi33;
	}

	
	public function getIsi34()
	{

		return $this->isi34;
	}

	
	public function getIsi35()
	{

		return $this->isi35;
	}

	
	public function getIsi36()
	{

		return $this->isi36;
	}

	
	public function getIsi37()
	{

		return $this->isi37;
	}

	
	public function getIsi38()
	{

		return $this->isi38;
	}

	
	public function getIsi39()
	{

		return $this->isi39;
	}

	
	public function getIsi40()
	{

		return $this->isi40;
	}

	
	public function setLogId($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->log_id !== $v) {
			$this->log_id = $v;
			$this->modifiedColumns[] = LogPeer::LOG_ID;
		}

	} 
	
	public function setWaktuAccess($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [waktu_access] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->waktu_access !== $ts) {
			$this->waktu_access = $ts;
			$this->modifiedColumns[] = LogPeer::WAKTU_ACCESS;
		}

	} 
	
	public function setIpAddress($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->ip_address !== $v) {
			$this->ip_address = $v;
			$this->modifiedColumns[] = LogPeer::IP_ADDRESS;
		}

	} 
	
	public function setLogin($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->login !== $v) {
			$this->login = $v;
			$this->modifiedColumns[] = LogPeer::LOGIN;
		}

	} 
	
	public function setTabelAsal($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->tabel_asal !== $v) {
			$this->tabel_asal = $v;
			$this->modifiedColumns[] = LogPeer::TABEL_ASAL;
		}

	} 
	
	public function setIsi1($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->isi1 !== $v) {
			$this->isi1 = $v;
			$this->modifiedColumns[] = LogPeer::ISI1;
		}

	} 
	
	public function setIsi2($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->isi2 !== $v) {
			$this->isi2 = $v;
			$this->modifiedColumns[] = LogPeer::ISI2;
		}

	} 
	
	public function setIsi3($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->isi3 !== $v) {
			$this->isi3 = $v;
			$this->modifiedColumns[] = LogPeer::ISI3;
		}

	} 
	
	public function setIsi4($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->isi4 !== $v) {
			$this->isi4 = $v;
			$this->modifiedColumns[] = LogPeer::ISI4;
		}

	} 
	
	public function setIsi5($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->isi5 !== $v) {
			$this->isi5 = $v;
			$this->modifiedColumns[] = LogPeer::ISI5;
		}

	} 
	
	public function setIsi6($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->isi6 !== $v) {
			$this->isi6 = $v;
			$this->modifiedColumns[] = LogPeer::ISI6;
		}

	} 
	
	public function setIsi7($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->isi7 !== $v) {
			$this->isi7 = $v;
			$this->modifiedColumns[] = LogPeer::ISI7;
		}

	} 
	
	public function setIsi8($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->isi8 !== $v) {
			$this->isi8 = $v;
			$this->modifiedColumns[] = LogPeer::ISI8;
		}

	} 
	
	public function setIsi9($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->isi9 !== $v) {
			$this->isi9 = $v;
			$this->modifiedColumns[] = LogPeer::ISI9;
		}

	} 
	
	public function setIsi10($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->isi10 !== $v) {
			$this->isi10 = $v;
			$this->modifiedColumns[] = LogPeer::ISI10;
		}

	} 
	
	public function setIsi11($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->isi11 !== $v) {
			$this->isi11 = $v;
			$this->modifiedColumns[] = LogPeer::ISI11;
		}

	} 
	
	public function setIsi12($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->isi12 !== $v) {
			$this->isi12 = $v;
			$this->modifiedColumns[] = LogPeer::ISI12;
		}

	} 
	
	public function setIsi13($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->isi13 !== $v) {
			$this->isi13 = $v;
			$this->modifiedColumns[] = LogPeer::ISI13;
		}

	} 
	
	public function setIsi14($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->isi14 !== $v) {
			$this->isi14 = $v;
			$this->modifiedColumns[] = LogPeer::ISI14;
		}

	} 
	
	public function setIsi15($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->isi15 !== $v) {
			$this->isi15 = $v;
			$this->modifiedColumns[] = LogPeer::ISI15;
		}

	} 
	
	public function setIsi16($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->isi16 !== $v) {
			$this->isi16 = $v;
			$this->modifiedColumns[] = LogPeer::ISI16;
		}

	} 
	
	public function setIsi17($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->isi17 !== $v) {
			$this->isi17 = $v;
			$this->modifiedColumns[] = LogPeer::ISI17;
		}

	} 
	
	public function setIsi18($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->isi18 !== $v) {
			$this->isi18 = $v;
			$this->modifiedColumns[] = LogPeer::ISI18;
		}

	} 
	
	public function setIsi19($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->isi19 !== $v) {
			$this->isi19 = $v;
			$this->modifiedColumns[] = LogPeer::ISI19;
		}

	} 
	
	public function setIsi20($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->isi20 !== $v) {
			$this->isi20 = $v;
			$this->modifiedColumns[] = LogPeer::ISI20;
		}

	} 
	
	public function setIsi21($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->isi21 !== $v) {
			$this->isi21 = $v;
			$this->modifiedColumns[] = LogPeer::ISI21;
		}

	} 
	
	public function setIsi22($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->isi22 !== $v) {
			$this->isi22 = $v;
			$this->modifiedColumns[] = LogPeer::ISI22;
		}

	} 
	
	public function setIsi23($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->isi23 !== $v) {
			$this->isi23 = $v;
			$this->modifiedColumns[] = LogPeer::ISI23;
		}

	} 
	
	public function setIsi24($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->isi24 !== $v) {
			$this->isi24 = $v;
			$this->modifiedColumns[] = LogPeer::ISI24;
		}

	} 
	
	public function setIsi25($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->isi25 !== $v) {
			$this->isi25 = $v;
			$this->modifiedColumns[] = LogPeer::ISI25;
		}

	} 
	
	public function setIsi26($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->isi26 !== $v) {
			$this->isi26 = $v;
			$this->modifiedColumns[] = LogPeer::ISI26;
		}

	} 
	
	public function setIsi27($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->isi27 !== $v) {
			$this->isi27 = $v;
			$this->modifiedColumns[] = LogPeer::ISI27;
		}

	} 
	
	public function setIsi28($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->isi28 !== $v) {
			$this->isi28 = $v;
			$this->modifiedColumns[] = LogPeer::ISI28;
		}

	} 
	
	public function setIsi29($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->isi29 !== $v) {
			$this->isi29 = $v;
			$this->modifiedColumns[] = LogPeer::ISI29;
		}

	} 
	
	public function setIsi30($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->isi30 !== $v) {
			$this->isi30 = $v;
			$this->modifiedColumns[] = LogPeer::ISI30;
		}

	} 
	
	public function setIsi31($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->isi31 !== $v) {
			$this->isi31 = $v;
			$this->modifiedColumns[] = LogPeer::ISI31;
		}

	} 
	
	public function setIsi32($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->isi32 !== $v) {
			$this->isi32 = $v;
			$this->modifiedColumns[] = LogPeer::ISI32;
		}

	} 
	
	public function setIsi33($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->isi33 !== $v) {
			$this->isi33 = $v;
			$this->modifiedColumns[] = LogPeer::ISI33;
		}

	} 
	
	public function setIsi34($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->isi34 !== $v) {
			$this->isi34 = $v;
			$this->modifiedColumns[] = LogPeer::ISI34;
		}

	} 
	
	public function setIsi35($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->isi35 !== $v) {
			$this->isi35 = $v;
			$this->modifiedColumns[] = LogPeer::ISI35;
		}

	} 
	
	public function setIsi36($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->isi36 !== $v) {
			$this->isi36 = $v;
			$this->modifiedColumns[] = LogPeer::ISI36;
		}

	} 
	
	public function setIsi37($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->isi37 !== $v) {
			$this->isi37 = $v;
			$this->modifiedColumns[] = LogPeer::ISI37;
		}

	} 
	
	public function setIsi38($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->isi38 !== $v) {
			$this->isi38 = $v;
			$this->modifiedColumns[] = LogPeer::ISI38;
		}

	} 
	
	public function setIsi39($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->isi39 !== $v) {
			$this->isi39 = $v;
			$this->modifiedColumns[] = LogPeer::ISI39;
		}

	} 
	
	public function setIsi40($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->isi40 !== $v) {
			$this->isi40 = $v;
			$this->modifiedColumns[] = LogPeer::ISI40;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->log_id = $rs->getInt($startcol + 0);

			$this->waktu_access = $rs->getTimestamp($startcol + 1, null);

			$this->ip_address = $rs->getString($startcol + 2);

			$this->login = $rs->getString($startcol + 3);

			$this->tabel_asal = $rs->getString($startcol + 4);

			$this->isi1 = $rs->getString($startcol + 5);

			$this->isi2 = $rs->getString($startcol + 6);

			$this->isi3 = $rs->getString($startcol + 7);

			$this->isi4 = $rs->getString($startcol + 8);

			$this->isi5 = $rs->getString($startcol + 9);

			$this->isi6 = $rs->getString($startcol + 10);

			$this->isi7 = $rs->getString($startcol + 11);

			$this->isi8 = $rs->getString($startcol + 12);

			$this->isi9 = $rs->getString($startcol + 13);

			$this->isi10 = $rs->getString($startcol + 14);

			$this->isi11 = $rs->getString($startcol + 15);

			$this->isi12 = $rs->getString($startcol + 16);

			$this->isi13 = $rs->getString($startcol + 17);

			$this->isi14 = $rs->getString($startcol + 18);

			$this->isi15 = $rs->getString($startcol + 19);

			$this->isi16 = $rs->getString($startcol + 20);

			$this->isi17 = $rs->getString($startcol + 21);

			$this->isi18 = $rs->getString($startcol + 22);

			$this->isi19 = $rs->getString($startcol + 23);

			$this->isi20 = $rs->getString($startcol + 24);

			$this->isi21 = $rs->getString($startcol + 25);

			$this->isi22 = $rs->getString($startcol + 26);

			$this->isi23 = $rs->getString($startcol + 27);

			$this->isi24 = $rs->getString($startcol + 28);

			$this->isi25 = $rs->getString($startcol + 29);

			$this->isi26 = $rs->getString($startcol + 30);

			$this->isi27 = $rs->getString($startcol + 31);

			$this->isi28 = $rs->getString($startcol + 32);

			$this->isi29 = $rs->getString($startcol + 33);

			$this->isi30 = $rs->getString($startcol + 34);

			$this->isi31 = $rs->getString($startcol + 35);

			$this->isi32 = $rs->getString($startcol + 36);

			$this->isi33 = $rs->getString($startcol + 37);

			$this->isi34 = $rs->getString($startcol + 38);

			$this->isi35 = $rs->getString($startcol + 39);

			$this->isi36 = $rs->getString($startcol + 40);

			$this->isi37 = $rs->getString($startcol + 41);

			$this->isi38 = $rs->getString($startcol + 42);

			$this->isi39 = $rs->getString($startcol + 43);

			$this->isi40 = $rs->getString($startcol + 44);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 45; 
		} catch (Exception $e) {
			throw new PropelException("Error populating Log object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(LogPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			LogPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(LogPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = LogPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += LogPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = LogPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = LogPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getLogId();
				break;
			case 1:
				return $this->getWaktuAccess();
				break;
			case 2:
				return $this->getIpAddress();
				break;
			case 3:
				return $this->getLogin();
				break;
			case 4:
				return $this->getTabelAsal();
				break;
			case 5:
				return $this->getIsi1();
				break;
			case 6:
				return $this->getIsi2();
				break;
			case 7:
				return $this->getIsi3();
				break;
			case 8:
				return $this->getIsi4();
				break;
			case 9:
				return $this->getIsi5();
				break;
			case 10:
				return $this->getIsi6();
				break;
			case 11:
				return $this->getIsi7();
				break;
			case 12:
				return $this->getIsi8();
				break;
			case 13:
				return $this->getIsi9();
				break;
			case 14:
				return $this->getIsi10();
				break;
			case 15:
				return $this->getIsi11();
				break;
			case 16:
				return $this->getIsi12();
				break;
			case 17:
				return $this->getIsi13();
				break;
			case 18:
				return $this->getIsi14();
				break;
			case 19:
				return $this->getIsi15();
				break;
			case 20:
				return $this->getIsi16();
				break;
			case 21:
				return $this->getIsi17();
				break;
			case 22:
				return $this->getIsi18();
				break;
			case 23:
				return $this->getIsi19();
				break;
			case 24:
				return $this->getIsi20();
				break;
			case 25:
				return $this->getIsi21();
				break;
			case 26:
				return $this->getIsi22();
				break;
			case 27:
				return $this->getIsi23();
				break;
			case 28:
				return $this->getIsi24();
				break;
			case 29:
				return $this->getIsi25();
				break;
			case 30:
				return $this->getIsi26();
				break;
			case 31:
				return $this->getIsi27();
				break;
			case 32:
				return $this->getIsi28();
				break;
			case 33:
				return $this->getIsi29();
				break;
			case 34:
				return $this->getIsi30();
				break;
			case 35:
				return $this->getIsi31();
				break;
			case 36:
				return $this->getIsi32();
				break;
			case 37:
				return $this->getIsi33();
				break;
			case 38:
				return $this->getIsi34();
				break;
			case 39:
				return $this->getIsi35();
				break;
			case 40:
				return $this->getIsi36();
				break;
			case 41:
				return $this->getIsi37();
				break;
			case 42:
				return $this->getIsi38();
				break;
			case 43:
				return $this->getIsi39();
				break;
			case 44:
				return $this->getIsi40();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = LogPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getLogId(),
			$keys[1] => $this->getWaktuAccess(),
			$keys[2] => $this->getIpAddress(),
			$keys[3] => $this->getLogin(),
			$keys[4] => $this->getTabelAsal(),
			$keys[5] => $this->getIsi1(),
			$keys[6] => $this->getIsi2(),
			$keys[7] => $this->getIsi3(),
			$keys[8] => $this->getIsi4(),
			$keys[9] => $this->getIsi5(),
			$keys[10] => $this->getIsi6(),
			$keys[11] => $this->getIsi7(),
			$keys[12] => $this->getIsi8(),
			$keys[13] => $this->getIsi9(),
			$keys[14] => $this->getIsi10(),
			$keys[15] => $this->getIsi11(),
			$keys[16] => $this->getIsi12(),
			$keys[17] => $this->getIsi13(),
			$keys[18] => $this->getIsi14(),
			$keys[19] => $this->getIsi15(),
			$keys[20] => $this->getIsi16(),
			$keys[21] => $this->getIsi17(),
			$keys[22] => $this->getIsi18(),
			$keys[23] => $this->getIsi19(),
			$keys[24] => $this->getIsi20(),
			$keys[25] => $this->getIsi21(),
			$keys[26] => $this->getIsi22(),
			$keys[27] => $this->getIsi23(),
			$keys[28] => $this->getIsi24(),
			$keys[29] => $this->getIsi25(),
			$keys[30] => $this->getIsi26(),
			$keys[31] => $this->getIsi27(),
			$keys[32] => $this->getIsi28(),
			$keys[33] => $this->getIsi29(),
			$keys[34] => $this->getIsi30(),
			$keys[35] => $this->getIsi31(),
			$keys[36] => $this->getIsi32(),
			$keys[37] => $this->getIsi33(),
			$keys[38] => $this->getIsi34(),
			$keys[39] => $this->getIsi35(),
			$keys[40] => $this->getIsi36(),
			$keys[41] => $this->getIsi37(),
			$keys[42] => $this->getIsi38(),
			$keys[43] => $this->getIsi39(),
			$keys[44] => $this->getIsi40(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = LogPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setLogId($value);
				break;
			case 1:
				$this->setWaktuAccess($value);
				break;
			case 2:
				$this->setIpAddress($value);
				break;
			case 3:
				$this->setLogin($value);
				break;
			case 4:
				$this->setTabelAsal($value);
				break;
			case 5:
				$this->setIsi1($value);
				break;
			case 6:
				$this->setIsi2($value);
				break;
			case 7:
				$this->setIsi3($value);
				break;
			case 8:
				$this->setIsi4($value);
				break;
			case 9:
				$this->setIsi5($value);
				break;
			case 10:
				$this->setIsi6($value);
				break;
			case 11:
				$this->setIsi7($value);
				break;
			case 12:
				$this->setIsi8($value);
				break;
			case 13:
				$this->setIsi9($value);
				break;
			case 14:
				$this->setIsi10($value);
				break;
			case 15:
				$this->setIsi11($value);
				break;
			case 16:
				$this->setIsi12($value);
				break;
			case 17:
				$this->setIsi13($value);
				break;
			case 18:
				$this->setIsi14($value);
				break;
			case 19:
				$this->setIsi15($value);
				break;
			case 20:
				$this->setIsi16($value);
				break;
			case 21:
				$this->setIsi17($value);
				break;
			case 22:
				$this->setIsi18($value);
				break;
			case 23:
				$this->setIsi19($value);
				break;
			case 24:
				$this->setIsi20($value);
				break;
			case 25:
				$this->setIsi21($value);
				break;
			case 26:
				$this->setIsi22($value);
				break;
			case 27:
				$this->setIsi23($value);
				break;
			case 28:
				$this->setIsi24($value);
				break;
			case 29:
				$this->setIsi25($value);
				break;
			case 30:
				$this->setIsi26($value);
				break;
			case 31:
				$this->setIsi27($value);
				break;
			case 32:
				$this->setIsi28($value);
				break;
			case 33:
				$this->setIsi29($value);
				break;
			case 34:
				$this->setIsi30($value);
				break;
			case 35:
				$this->setIsi31($value);
				break;
			case 36:
				$this->setIsi32($value);
				break;
			case 37:
				$this->setIsi33($value);
				break;
			case 38:
				$this->setIsi34($value);
				break;
			case 39:
				$this->setIsi35($value);
				break;
			case 40:
				$this->setIsi36($value);
				break;
			case 41:
				$this->setIsi37($value);
				break;
			case 42:
				$this->setIsi38($value);
				break;
			case 43:
				$this->setIsi39($value);
				break;
			case 44:
				$this->setIsi40($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = LogPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setLogId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setWaktuAccess($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setIpAddress($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setLogin($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setTabelAsal($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setIsi1($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setIsi2($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setIsi3($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setIsi4($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setIsi5($arr[$keys[9]]);
		if (array_key_exists($keys[10], $arr)) $this->setIsi6($arr[$keys[10]]);
		if (array_key_exists($keys[11], $arr)) $this->setIsi7($arr[$keys[11]]);
		if (array_key_exists($keys[12], $arr)) $this->setIsi8($arr[$keys[12]]);
		if (array_key_exists($keys[13], $arr)) $this->setIsi9($arr[$keys[13]]);
		if (array_key_exists($keys[14], $arr)) $this->setIsi10($arr[$keys[14]]);
		if (array_key_exists($keys[15], $arr)) $this->setIsi11($arr[$keys[15]]);
		if (array_key_exists($keys[16], $arr)) $this->setIsi12($arr[$keys[16]]);
		if (array_key_exists($keys[17], $arr)) $this->setIsi13($arr[$keys[17]]);
		if (array_key_exists($keys[18], $arr)) $this->setIsi14($arr[$keys[18]]);
		if (array_key_exists($keys[19], $arr)) $this->setIsi15($arr[$keys[19]]);
		if (array_key_exists($keys[20], $arr)) $this->setIsi16($arr[$keys[20]]);
		if (array_key_exists($keys[21], $arr)) $this->setIsi17($arr[$keys[21]]);
		if (array_key_exists($keys[22], $arr)) $this->setIsi18($arr[$keys[22]]);
		if (array_key_exists($keys[23], $arr)) $this->setIsi19($arr[$keys[23]]);
		if (array_key_exists($keys[24], $arr)) $this->setIsi20($arr[$keys[24]]);
		if (array_key_exists($keys[25], $arr)) $this->setIsi21($arr[$keys[25]]);
		if (array_key_exists($keys[26], $arr)) $this->setIsi22($arr[$keys[26]]);
		if (array_key_exists($keys[27], $arr)) $this->setIsi23($arr[$keys[27]]);
		if (array_key_exists($keys[28], $arr)) $this->setIsi24($arr[$keys[28]]);
		if (array_key_exists($keys[29], $arr)) $this->setIsi25($arr[$keys[29]]);
		if (array_key_exists($keys[30], $arr)) $this->setIsi26($arr[$keys[30]]);
		if (array_key_exists($keys[31], $arr)) $this->setIsi27($arr[$keys[31]]);
		if (array_key_exists($keys[32], $arr)) $this->setIsi28($arr[$keys[32]]);
		if (array_key_exists($keys[33], $arr)) $this->setIsi29($arr[$keys[33]]);
		if (array_key_exists($keys[34], $arr)) $this->setIsi30($arr[$keys[34]]);
		if (array_key_exists($keys[35], $arr)) $this->setIsi31($arr[$keys[35]]);
		if (array_key_exists($keys[36], $arr)) $this->setIsi32($arr[$keys[36]]);
		if (array_key_exists($keys[37], $arr)) $this->setIsi33($arr[$keys[37]]);
		if (array_key_exists($keys[38], $arr)) $this->setIsi34($arr[$keys[38]]);
		if (array_key_exists($keys[39], $arr)) $this->setIsi35($arr[$keys[39]]);
		if (array_key_exists($keys[40], $arr)) $this->setIsi36($arr[$keys[40]]);
		if (array_key_exists($keys[41], $arr)) $this->setIsi37($arr[$keys[41]]);
		if (array_key_exists($keys[42], $arr)) $this->setIsi38($arr[$keys[42]]);
		if (array_key_exists($keys[43], $arr)) $this->setIsi39($arr[$keys[43]]);
		if (array_key_exists($keys[44], $arr)) $this->setIsi40($arr[$keys[44]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(LogPeer::DATABASE_NAME);

		if ($this->isColumnModified(LogPeer::LOG_ID)) $criteria->add(LogPeer::LOG_ID, $this->log_id);
		if ($this->isColumnModified(LogPeer::WAKTU_ACCESS)) $criteria->add(LogPeer::WAKTU_ACCESS, $this->waktu_access);
		if ($this->isColumnModified(LogPeer::IP_ADDRESS)) $criteria->add(LogPeer::IP_ADDRESS, $this->ip_address);
		if ($this->isColumnModified(LogPeer::LOGIN)) $criteria->add(LogPeer::LOGIN, $this->login);
		if ($this->isColumnModified(LogPeer::TABEL_ASAL)) $criteria->add(LogPeer::TABEL_ASAL, $this->tabel_asal);
		if ($this->isColumnModified(LogPeer::ISI1)) $criteria->add(LogPeer::ISI1, $this->isi1);
		if ($this->isColumnModified(LogPeer::ISI2)) $criteria->add(LogPeer::ISI2, $this->isi2);
		if ($this->isColumnModified(LogPeer::ISI3)) $criteria->add(LogPeer::ISI3, $this->isi3);
		if ($this->isColumnModified(LogPeer::ISI4)) $criteria->add(LogPeer::ISI4, $this->isi4);
		if ($this->isColumnModified(LogPeer::ISI5)) $criteria->add(LogPeer::ISI5, $this->isi5);
		if ($this->isColumnModified(LogPeer::ISI6)) $criteria->add(LogPeer::ISI6, $this->isi6);
		if ($this->isColumnModified(LogPeer::ISI7)) $criteria->add(LogPeer::ISI7, $this->isi7);
		if ($this->isColumnModified(LogPeer::ISI8)) $criteria->add(LogPeer::ISI8, $this->isi8);
		if ($this->isColumnModified(LogPeer::ISI9)) $criteria->add(LogPeer::ISI9, $this->isi9);
		if ($this->isColumnModified(LogPeer::ISI10)) $criteria->add(LogPeer::ISI10, $this->isi10);
		if ($this->isColumnModified(LogPeer::ISI11)) $criteria->add(LogPeer::ISI11, $this->isi11);
		if ($this->isColumnModified(LogPeer::ISI12)) $criteria->add(LogPeer::ISI12, $this->isi12);
		if ($this->isColumnModified(LogPeer::ISI13)) $criteria->add(LogPeer::ISI13, $this->isi13);
		if ($this->isColumnModified(LogPeer::ISI14)) $criteria->add(LogPeer::ISI14, $this->isi14);
		if ($this->isColumnModified(LogPeer::ISI15)) $criteria->add(LogPeer::ISI15, $this->isi15);
		if ($this->isColumnModified(LogPeer::ISI16)) $criteria->add(LogPeer::ISI16, $this->isi16);
		if ($this->isColumnModified(LogPeer::ISI17)) $criteria->add(LogPeer::ISI17, $this->isi17);
		if ($this->isColumnModified(LogPeer::ISI18)) $criteria->add(LogPeer::ISI18, $this->isi18);
		if ($this->isColumnModified(LogPeer::ISI19)) $criteria->add(LogPeer::ISI19, $this->isi19);
		if ($this->isColumnModified(LogPeer::ISI20)) $criteria->add(LogPeer::ISI20, $this->isi20);
		if ($this->isColumnModified(LogPeer::ISI21)) $criteria->add(LogPeer::ISI21, $this->isi21);
		if ($this->isColumnModified(LogPeer::ISI22)) $criteria->add(LogPeer::ISI22, $this->isi22);
		if ($this->isColumnModified(LogPeer::ISI23)) $criteria->add(LogPeer::ISI23, $this->isi23);
		if ($this->isColumnModified(LogPeer::ISI24)) $criteria->add(LogPeer::ISI24, $this->isi24);
		if ($this->isColumnModified(LogPeer::ISI25)) $criteria->add(LogPeer::ISI25, $this->isi25);
		if ($this->isColumnModified(LogPeer::ISI26)) $criteria->add(LogPeer::ISI26, $this->isi26);
		if ($this->isColumnModified(LogPeer::ISI27)) $criteria->add(LogPeer::ISI27, $this->isi27);
		if ($this->isColumnModified(LogPeer::ISI28)) $criteria->add(LogPeer::ISI28, $this->isi28);
		if ($this->isColumnModified(LogPeer::ISI29)) $criteria->add(LogPeer::ISI29, $this->isi29);
		if ($this->isColumnModified(LogPeer::ISI30)) $criteria->add(LogPeer::ISI30, $this->isi30);
		if ($this->isColumnModified(LogPeer::ISI31)) $criteria->add(LogPeer::ISI31, $this->isi31);
		if ($this->isColumnModified(LogPeer::ISI32)) $criteria->add(LogPeer::ISI32, $this->isi32);
		if ($this->isColumnModified(LogPeer::ISI33)) $criteria->add(LogPeer::ISI33, $this->isi33);
		if ($this->isColumnModified(LogPeer::ISI34)) $criteria->add(LogPeer::ISI34, $this->isi34);
		if ($this->isColumnModified(LogPeer::ISI35)) $criteria->add(LogPeer::ISI35, $this->isi35);
		if ($this->isColumnModified(LogPeer::ISI36)) $criteria->add(LogPeer::ISI36, $this->isi36);
		if ($this->isColumnModified(LogPeer::ISI37)) $criteria->add(LogPeer::ISI37, $this->isi37);
		if ($this->isColumnModified(LogPeer::ISI38)) $criteria->add(LogPeer::ISI38, $this->isi38);
		if ($this->isColumnModified(LogPeer::ISI39)) $criteria->add(LogPeer::ISI39, $this->isi39);
		if ($this->isColumnModified(LogPeer::ISI40)) $criteria->add(LogPeer::ISI40, $this->isi40);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(LogPeer::DATABASE_NAME);

		$criteria->add(LogPeer::LOG_ID, $this->log_id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getLogId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setLogId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setWaktuAccess($this->waktu_access);

		$copyObj->setIpAddress($this->ip_address);

		$copyObj->setLogin($this->login);

		$copyObj->setTabelAsal($this->tabel_asal);

		$copyObj->setIsi1($this->isi1);

		$copyObj->setIsi2($this->isi2);

		$copyObj->setIsi3($this->isi3);

		$copyObj->setIsi4($this->isi4);

		$copyObj->setIsi5($this->isi5);

		$copyObj->setIsi6($this->isi6);

		$copyObj->setIsi7($this->isi7);

		$copyObj->setIsi8($this->isi8);

		$copyObj->setIsi9($this->isi9);

		$copyObj->setIsi10($this->isi10);

		$copyObj->setIsi11($this->isi11);

		$copyObj->setIsi12($this->isi12);

		$copyObj->setIsi13($this->isi13);

		$copyObj->setIsi14($this->isi14);

		$copyObj->setIsi15($this->isi15);

		$copyObj->setIsi16($this->isi16);

		$copyObj->setIsi17($this->isi17);

		$copyObj->setIsi18($this->isi18);

		$copyObj->setIsi19($this->isi19);

		$copyObj->setIsi20($this->isi20);

		$copyObj->setIsi21($this->isi21);

		$copyObj->setIsi22($this->isi22);

		$copyObj->setIsi23($this->isi23);

		$copyObj->setIsi24($this->isi24);

		$copyObj->setIsi25($this->isi25);

		$copyObj->setIsi26($this->isi26);

		$copyObj->setIsi27($this->isi27);

		$copyObj->setIsi28($this->isi28);

		$copyObj->setIsi29($this->isi29);

		$copyObj->setIsi30($this->isi30);

		$copyObj->setIsi31($this->isi31);

		$copyObj->setIsi32($this->isi32);

		$copyObj->setIsi33($this->isi33);

		$copyObj->setIsi34($this->isi34);

		$copyObj->setIsi35($this->isi35);

		$copyObj->setIsi36($this->isi36);

		$copyObj->setIsi37($this->isi37);

		$copyObj->setIsi38($this->isi38);

		$copyObj->setIsi39($this->isi39);

		$copyObj->setIsi40($this->isi40);


		$copyObj->setNew(true);

		$copyObj->setLogId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new LogPeer();
		}
		return self::$peer;
	}

} 