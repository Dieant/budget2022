<?php


abstract class BaseBukuSSHPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.buku_ssh';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.BukuSSH';

	
	const NUM_COLUMNS = 14;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const ID = 'ebudget.buku_ssh.ID';

	
	const KODE = 'ebudget.buku_ssh.KODE';

	
	const NAMA = 'ebudget.buku_ssh.NAMA';

	
	const MERK = 'ebudget.buku_ssh.MERK';

	
	const SPEC = 'ebudget.buku_ssh.SPEC';

	
	const HIDDEN_SPEC = 'ebudget.buku_ssh.HIDDEN_SPEC';

	
	const SATUAN = 'ebudget.buku_ssh.SATUAN';

	
	const PERUBAHAN = 'ebudget.buku_ssh.PERUBAHAN';

	
	const TANGGAL = 'ebudget.buku_ssh.TANGGAL';

	
	const PERUBAHAN_REVISI = 'ebudget.buku_ssh.PERUBAHAN_REVISI';

	
	const STATUS_HAPUS = 'ebudget.buku_ssh.STATUS_HAPUS';

	
	const HARGA = 'ebudget.buku_ssh.HARGA';

	
	const TAHUN = 'ebudget.buku_ssh.TAHUN';

	
	const REPLACED_AT = 'ebudget.buku_ssh.REPLACED_AT';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('Id', 'Kode', 'Nama', 'Merk', 'Spec', 'HiddenSpec', 'Satuan', 'Perubahan', 'Tanggal', 'PerubahanRevisi', 'StatusHapus', 'Harga', 'Tahun', 'ReplacedAt', ),
		BasePeer::TYPE_COLNAME => array (BukuSSHPeer::ID, BukuSSHPeer::KODE, BukuSSHPeer::NAMA, BukuSSHPeer::MERK, BukuSSHPeer::SPEC, BukuSSHPeer::HIDDEN_SPEC, BukuSSHPeer::SATUAN, BukuSSHPeer::PERUBAHAN, BukuSSHPeer::TANGGAL, BukuSSHPeer::PERUBAHAN_REVISI, BukuSSHPeer::STATUS_HAPUS, BukuSSHPeer::HARGA, BukuSSHPeer::TAHUN, BukuSSHPeer::REPLACED_AT, ),
		BasePeer::TYPE_FIELDNAME => array ('id', 'kode', 'nama', 'merk', 'spec', 'hidden_spec', 'satuan', 'perubahan', 'tanggal', 'perubahan_revisi', 'status_hapus', 'harga', 'tahun', 'replaced_at', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('Id' => 0, 'Kode' => 1, 'Nama' => 2, 'Merk' => 3, 'Spec' => 4, 'HiddenSpec' => 5, 'Satuan' => 6, 'Perubahan' => 7, 'Tanggal' => 8, 'PerubahanRevisi' => 9, 'StatusHapus' => 10, 'Harga' => 11, 'Tahun' => 12, 'ReplacedAt' => 13, ),
		BasePeer::TYPE_COLNAME => array (BukuSSHPeer::ID => 0, BukuSSHPeer::KODE => 1, BukuSSHPeer::NAMA => 2, BukuSSHPeer::MERK => 3, BukuSSHPeer::SPEC => 4, BukuSSHPeer::HIDDEN_SPEC => 5, BukuSSHPeer::SATUAN => 6, BukuSSHPeer::PERUBAHAN => 7, BukuSSHPeer::TANGGAL => 8, BukuSSHPeer::PERUBAHAN_REVISI => 9, BukuSSHPeer::STATUS_HAPUS => 10, BukuSSHPeer::HARGA => 11, BukuSSHPeer::TAHUN => 12, BukuSSHPeer::REPLACED_AT => 13, ),
		BasePeer::TYPE_FIELDNAME => array ('id' => 0, 'kode' => 1, 'nama' => 2, 'merk' => 3, 'spec' => 4, 'hidden_spec' => 5, 'satuan' => 6, 'perubahan' => 7, 'tanggal' => 8, 'perubahan_revisi' => 9, 'status_hapus' => 10, 'harga' => 11, 'tahun' => 12, 'replaced_at' => 13, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/BukuSSHMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.BukuSSHMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = BukuSSHPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(BukuSSHPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(BukuSSHPeer::ID);

		$criteria->addSelectColumn(BukuSSHPeer::KODE);

		$criteria->addSelectColumn(BukuSSHPeer::NAMA);

		$criteria->addSelectColumn(BukuSSHPeer::MERK);

		$criteria->addSelectColumn(BukuSSHPeer::SPEC);

		$criteria->addSelectColumn(BukuSSHPeer::HIDDEN_SPEC);

		$criteria->addSelectColumn(BukuSSHPeer::SATUAN);

		$criteria->addSelectColumn(BukuSSHPeer::PERUBAHAN);

		$criteria->addSelectColumn(BukuSSHPeer::TANGGAL);

		$criteria->addSelectColumn(BukuSSHPeer::PERUBAHAN_REVISI);

		$criteria->addSelectColumn(BukuSSHPeer::STATUS_HAPUS);

		$criteria->addSelectColumn(BukuSSHPeer::HARGA);

		$criteria->addSelectColumn(BukuSSHPeer::TAHUN);

		$criteria->addSelectColumn(BukuSSHPeer::REPLACED_AT);

	}

	const COUNT = 'COUNT(ebudget.buku_ssh.ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.buku_ssh.ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(BukuSSHPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(BukuSSHPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = BukuSSHPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = BukuSSHPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return BukuSSHPeer::populateObjects(BukuSSHPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			BukuSSHPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = BukuSSHPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return BukuSSHPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(BukuSSHPeer::ID);
			$selectCriteria->add(BukuSSHPeer::ID, $criteria->remove(BukuSSHPeer::ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(BukuSSHPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(BukuSSHPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof BukuSSH) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(BukuSSHPeer::ID, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(BukuSSH $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(BukuSSHPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(BukuSSHPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(BukuSSHPeer::DATABASE_NAME, BukuSSHPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = BukuSSHPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(BukuSSHPeer::DATABASE_NAME);

		$criteria->add(BukuSSHPeer::ID, $pk);


		$v = BukuSSHPeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(BukuSSHPeer::ID, $pks, Criteria::IN);
			$objs = BukuSSHPeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BaseBukuSSHPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/BukuSSHMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.BukuSSHMapBuilder');
}
