<?php


abstract class BaseKomponenMember2Peer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.komponen_member2';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.KomponenMember2';

	
	const NUM_COLUMNS = 17;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const KOMPONEN_ID = 'ebudget.komponen_member2.KOMPONEN_ID';

	
	const MEMBER_ID = 'ebudget.komponen_member2.MEMBER_ID';

	
	const SATUAN = 'ebudget.komponen_member2.SATUAN';

	
	const MEMBER_NAME = 'ebudget.komponen_member2.MEMBER_NAME';

	
	const MEMBER_HARGA = 'ebudget.komponen_member2.MEMBER_HARGA';

	
	const KOEFISIEN = 'ebudget.komponen_member2.KOEFISIEN';

	
	const MEMBER_TOTAL = 'ebudget.komponen_member2.MEMBER_TOTAL';

	
	const IP_ADDRESS = 'ebudget.komponen_member2.IP_ADDRESS';

	
	const WAKTU_ACCESS = 'ebudget.komponen_member2.WAKTU_ACCESS';

	
	const MEMBER_TIPE = 'ebudget.komponen_member2.MEMBER_TIPE';

	
	const SUBTITLE = 'ebudget.komponen_member2.SUBTITLE';

	
	const MEMBER_NO = 'ebudget.komponen_member2.MEMBER_NO';

	
	const TIPE = 'ebudget.komponen_member2.TIPE';

	
	const KOMPONEN_TIPE = 'ebudget.komponen_member2.KOMPONEN_TIPE';

	
	const FROM_ID = 'ebudget.komponen_member2.FROM_ID';

	
	const FROM_KOEF = 'ebudget.komponen_member2.FROM_KOEF';

	
	const HSPK_NAME = 'ebudget.komponen_member2.HSPK_NAME';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('KomponenId', 'MemberId', 'Satuan', 'MemberName', 'MemberHarga', 'Koefisien', 'MemberTotal', 'IpAddress', 'WaktuAccess', 'MemberTipe', 'Subtitle', 'MemberNo', 'Tipe', 'KomponenTipe', 'FromId', 'FromKoef', 'HspkName', ),
		BasePeer::TYPE_COLNAME => array (KomponenMember2Peer::KOMPONEN_ID, KomponenMember2Peer::MEMBER_ID, KomponenMember2Peer::SATUAN, KomponenMember2Peer::MEMBER_NAME, KomponenMember2Peer::MEMBER_HARGA, KomponenMember2Peer::KOEFISIEN, KomponenMember2Peer::MEMBER_TOTAL, KomponenMember2Peer::IP_ADDRESS, KomponenMember2Peer::WAKTU_ACCESS, KomponenMember2Peer::MEMBER_TIPE, KomponenMember2Peer::SUBTITLE, KomponenMember2Peer::MEMBER_NO, KomponenMember2Peer::TIPE, KomponenMember2Peer::KOMPONEN_TIPE, KomponenMember2Peer::FROM_ID, KomponenMember2Peer::FROM_KOEF, KomponenMember2Peer::HSPK_NAME, ),
		BasePeer::TYPE_FIELDNAME => array ('komponen_id', 'member_id', 'satuan', 'member_name', 'member_harga', 'koefisien', 'member_total', 'ip_address', 'waktu_access', 'member_tipe', 'subtitle', 'member_no', 'tipe', 'komponen_tipe', 'from_id', 'from_koef', 'hspk_name', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('KomponenId' => 0, 'MemberId' => 1, 'Satuan' => 2, 'MemberName' => 3, 'MemberHarga' => 4, 'Koefisien' => 5, 'MemberTotal' => 6, 'IpAddress' => 7, 'WaktuAccess' => 8, 'MemberTipe' => 9, 'Subtitle' => 10, 'MemberNo' => 11, 'Tipe' => 12, 'KomponenTipe' => 13, 'FromId' => 14, 'FromKoef' => 15, 'HspkName' => 16, ),
		BasePeer::TYPE_COLNAME => array (KomponenMember2Peer::KOMPONEN_ID => 0, KomponenMember2Peer::MEMBER_ID => 1, KomponenMember2Peer::SATUAN => 2, KomponenMember2Peer::MEMBER_NAME => 3, KomponenMember2Peer::MEMBER_HARGA => 4, KomponenMember2Peer::KOEFISIEN => 5, KomponenMember2Peer::MEMBER_TOTAL => 6, KomponenMember2Peer::IP_ADDRESS => 7, KomponenMember2Peer::WAKTU_ACCESS => 8, KomponenMember2Peer::MEMBER_TIPE => 9, KomponenMember2Peer::SUBTITLE => 10, KomponenMember2Peer::MEMBER_NO => 11, KomponenMember2Peer::TIPE => 12, KomponenMember2Peer::KOMPONEN_TIPE => 13, KomponenMember2Peer::FROM_ID => 14, KomponenMember2Peer::FROM_KOEF => 15, KomponenMember2Peer::HSPK_NAME => 16, ),
		BasePeer::TYPE_FIELDNAME => array ('komponen_id' => 0, 'member_id' => 1, 'satuan' => 2, 'member_name' => 3, 'member_harga' => 4, 'koefisien' => 5, 'member_total' => 6, 'ip_address' => 7, 'waktu_access' => 8, 'member_tipe' => 9, 'subtitle' => 10, 'member_no' => 11, 'tipe' => 12, 'komponen_tipe' => 13, 'from_id' => 14, 'from_koef' => 15, 'hspk_name' => 16, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/KomponenMember2MapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.KomponenMember2MapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = KomponenMember2Peer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(KomponenMember2Peer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(KomponenMember2Peer::KOMPONEN_ID);

		$criteria->addSelectColumn(KomponenMember2Peer::MEMBER_ID);

		$criteria->addSelectColumn(KomponenMember2Peer::SATUAN);

		$criteria->addSelectColumn(KomponenMember2Peer::MEMBER_NAME);

		$criteria->addSelectColumn(KomponenMember2Peer::MEMBER_HARGA);

		$criteria->addSelectColumn(KomponenMember2Peer::KOEFISIEN);

		$criteria->addSelectColumn(KomponenMember2Peer::MEMBER_TOTAL);

		$criteria->addSelectColumn(KomponenMember2Peer::IP_ADDRESS);

		$criteria->addSelectColumn(KomponenMember2Peer::WAKTU_ACCESS);

		$criteria->addSelectColumn(KomponenMember2Peer::MEMBER_TIPE);

		$criteria->addSelectColumn(KomponenMember2Peer::SUBTITLE);

		$criteria->addSelectColumn(KomponenMember2Peer::MEMBER_NO);

		$criteria->addSelectColumn(KomponenMember2Peer::TIPE);

		$criteria->addSelectColumn(KomponenMember2Peer::KOMPONEN_TIPE);

		$criteria->addSelectColumn(KomponenMember2Peer::FROM_ID);

		$criteria->addSelectColumn(KomponenMember2Peer::FROM_KOEF);

		$criteria->addSelectColumn(KomponenMember2Peer::HSPK_NAME);

	}

	const COUNT = 'COUNT(*)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT *)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(KomponenMember2Peer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(KomponenMember2Peer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = KomponenMember2Peer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = KomponenMember2Peer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return KomponenMember2Peer::populateObjects(KomponenMember2Peer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			KomponenMember2Peer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = KomponenMember2Peer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return KomponenMember2Peer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(KomponenMember2Peer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(KomponenMember2Peer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof KomponenMember2) {

			$criteria = $values->buildCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
												if(count($values) == count($values, COUNT_RECURSIVE))
			{
								$values = array($values);
			}
			$vals = array();
			foreach($values as $value)
			{

			}

		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(KomponenMember2 $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(KomponenMember2Peer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(KomponenMember2Peer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(KomponenMember2Peer::DATABASE_NAME, KomponenMember2Peer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = KomponenMember2Peer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

} 
if (Propel::isInit()) {
			try {
		BaseKomponenMember2Peer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/KomponenMember2MapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.KomponenMember2MapBuilder');
}
