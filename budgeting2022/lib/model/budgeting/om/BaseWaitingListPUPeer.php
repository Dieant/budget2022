<?php


abstract class BaseWaitingListPUPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.waitinglist_pu';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.WaitingListPU';

	
	const NUM_COLUMNS = 30;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const ID_WAITING = 'ebudget.waitinglist_pu.ID_WAITING';

	
	const UNIT_ID = 'ebudget.waitinglist_pu.UNIT_ID';

	
	const KEGIATAN_CODE = 'ebudget.waitinglist_pu.KEGIATAN_CODE';

	
	const SUBTITLE = 'ebudget.waitinglist_pu.SUBTITLE';

	
	const KOMPONEN_ID = 'ebudget.waitinglist_pu.KOMPONEN_ID';

	
	const KOMPONEN_NAME = 'ebudget.waitinglist_pu.KOMPONEN_NAME';

	
	const KOMPONEN_LOKASI = 'ebudget.waitinglist_pu.KOMPONEN_LOKASI';

	
	const KOMPONEN_HARGA_AWAL = 'ebudget.waitinglist_pu.KOMPONEN_HARGA_AWAL';

	
	const PAJAK = 'ebudget.waitinglist_pu.PAJAK';

	
	const KOMPONEN_SATUAN = 'ebudget.waitinglist_pu.KOMPONEN_SATUAN';

	
	const KOMPONEN_REKENING = 'ebudget.waitinglist_pu.KOMPONEN_REKENING';

	
	const KOEFISIEN = 'ebudget.waitinglist_pu.KOEFISIEN';

	
	const VOLUME = 'ebudget.waitinglist_pu.VOLUME';

	
	const NILAI_ANGGARAN = 'ebudget.waitinglist_pu.NILAI_ANGGARAN';

	
	const TAHUN_INPUT = 'ebudget.waitinglist_pu.TAHUN_INPUT';

	
	const CREATED_AT = 'ebudget.waitinglist_pu.CREATED_AT';

	
	const UPDATED_AT = 'ebudget.waitinglist_pu.UPDATED_AT';

	
	const STATUS_HAPUS = 'ebudget.waitinglist_pu.STATUS_HAPUS';

	
	const STATUS_WAITING = 'ebudget.waitinglist_pu.STATUS_WAITING';

	
	const PRIORITAS = 'ebudget.waitinglist_pu.PRIORITAS';

	
	const KODE_RKA = 'ebudget.waitinglist_pu.KODE_RKA';

	
	const USER_PENGAMBIL = 'ebudget.waitinglist_pu.USER_PENGAMBIL';

	
	const NILAI_EE = 'ebudget.waitinglist_pu.NILAI_EE';

	
	const KETERANGAN = 'ebudget.waitinglist_pu.KETERANGAN';

	
	const KODE_JASMAS = 'ebudget.waitinglist_pu.KODE_JASMAS';

	
	const KECAMATAN = 'ebudget.waitinglist_pu.KECAMATAN';

	
	const KELURAHAN = 'ebudget.waitinglist_pu.KELURAHAN';

	
	const IS_MUSRENBANG = 'ebudget.waitinglist_pu.IS_MUSRENBANG';

	
	const NILAI_ANGGARAN_SEMULA = 'ebudget.waitinglist_pu.NILAI_ANGGARAN_SEMULA';

	
	const METODE_WAITINGLIST = 'ebudget.waitinglist_pu.METODE_WAITINGLIST';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('IdWaiting', 'UnitId', 'KegiatanCode', 'Subtitle', 'KomponenId', 'KomponenName', 'KomponenLokasi', 'KomponenHargaAwal', 'Pajak', 'KomponenSatuan', 'KomponenRekening', 'Koefisien', 'Volume', 'NilaiAnggaran', 'TahunInput', 'CreatedAt', 'UpdatedAt', 'StatusHapus', 'StatusWaiting', 'Prioritas', 'KodeRka', 'UserPengambil', 'NilaiEe', 'Keterangan', 'KodeJasmas', 'Kecamatan', 'Kelurahan', 'IsMusrenbang', 'NilaiAnggaranSemula', 'MetodeWaitinglist', ),
		BasePeer::TYPE_COLNAME => array (WaitingListPUPeer::ID_WAITING, WaitingListPUPeer::UNIT_ID, WaitingListPUPeer::KEGIATAN_CODE, WaitingListPUPeer::SUBTITLE, WaitingListPUPeer::KOMPONEN_ID, WaitingListPUPeer::KOMPONEN_NAME, WaitingListPUPeer::KOMPONEN_LOKASI, WaitingListPUPeer::KOMPONEN_HARGA_AWAL, WaitingListPUPeer::PAJAK, WaitingListPUPeer::KOMPONEN_SATUAN, WaitingListPUPeer::KOMPONEN_REKENING, WaitingListPUPeer::KOEFISIEN, WaitingListPUPeer::VOLUME, WaitingListPUPeer::NILAI_ANGGARAN, WaitingListPUPeer::TAHUN_INPUT, WaitingListPUPeer::CREATED_AT, WaitingListPUPeer::UPDATED_AT, WaitingListPUPeer::STATUS_HAPUS, WaitingListPUPeer::STATUS_WAITING, WaitingListPUPeer::PRIORITAS, WaitingListPUPeer::KODE_RKA, WaitingListPUPeer::USER_PENGAMBIL, WaitingListPUPeer::NILAI_EE, WaitingListPUPeer::KETERANGAN, WaitingListPUPeer::KODE_JASMAS, WaitingListPUPeer::KECAMATAN, WaitingListPUPeer::KELURAHAN, WaitingListPUPeer::IS_MUSRENBANG, WaitingListPUPeer::NILAI_ANGGARAN_SEMULA, WaitingListPUPeer::METODE_WAITINGLIST, ),
		BasePeer::TYPE_FIELDNAME => array ('id_waiting', 'unit_id', 'kegiatan_code', 'subtitle', 'komponen_id', 'komponen_name', 'komponen_lokasi', 'komponen_harga_awal', 'pajak', 'komponen_satuan', 'komponen_rekening', 'koefisien', 'volume', 'nilai_anggaran', 'tahun_input', 'created_at', 'updated_at', 'status_hapus', 'status_waiting', 'prioritas', 'kode_rka', 'user_pengambil', 'nilai_ee', 'keterangan', 'kode_jasmas', 'kecamatan', 'kelurahan', 'is_musrenbang', 'nilai_anggaran_semula', 'metode_waitinglist', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('IdWaiting' => 0, 'UnitId' => 1, 'KegiatanCode' => 2, 'Subtitle' => 3, 'KomponenId' => 4, 'KomponenName' => 5, 'KomponenLokasi' => 6, 'KomponenHargaAwal' => 7, 'Pajak' => 8, 'KomponenSatuan' => 9, 'KomponenRekening' => 10, 'Koefisien' => 11, 'Volume' => 12, 'NilaiAnggaran' => 13, 'TahunInput' => 14, 'CreatedAt' => 15, 'UpdatedAt' => 16, 'StatusHapus' => 17, 'StatusWaiting' => 18, 'Prioritas' => 19, 'KodeRka' => 20, 'UserPengambil' => 21, 'NilaiEe' => 22, 'Keterangan' => 23, 'KodeJasmas' => 24, 'Kecamatan' => 25, 'Kelurahan' => 26, 'IsMusrenbang' => 27, 'NilaiAnggaranSemula' => 28, 'MetodeWaitinglist' => 29, ),
		BasePeer::TYPE_COLNAME => array (WaitingListPUPeer::ID_WAITING => 0, WaitingListPUPeer::UNIT_ID => 1, WaitingListPUPeer::KEGIATAN_CODE => 2, WaitingListPUPeer::SUBTITLE => 3, WaitingListPUPeer::KOMPONEN_ID => 4, WaitingListPUPeer::KOMPONEN_NAME => 5, WaitingListPUPeer::KOMPONEN_LOKASI => 6, WaitingListPUPeer::KOMPONEN_HARGA_AWAL => 7, WaitingListPUPeer::PAJAK => 8, WaitingListPUPeer::KOMPONEN_SATUAN => 9, WaitingListPUPeer::KOMPONEN_REKENING => 10, WaitingListPUPeer::KOEFISIEN => 11, WaitingListPUPeer::VOLUME => 12, WaitingListPUPeer::NILAI_ANGGARAN => 13, WaitingListPUPeer::TAHUN_INPUT => 14, WaitingListPUPeer::CREATED_AT => 15, WaitingListPUPeer::UPDATED_AT => 16, WaitingListPUPeer::STATUS_HAPUS => 17, WaitingListPUPeer::STATUS_WAITING => 18, WaitingListPUPeer::PRIORITAS => 19, WaitingListPUPeer::KODE_RKA => 20, WaitingListPUPeer::USER_PENGAMBIL => 21, WaitingListPUPeer::NILAI_EE => 22, WaitingListPUPeer::KETERANGAN => 23, WaitingListPUPeer::KODE_JASMAS => 24, WaitingListPUPeer::KECAMATAN => 25, WaitingListPUPeer::KELURAHAN => 26, WaitingListPUPeer::IS_MUSRENBANG => 27, WaitingListPUPeer::NILAI_ANGGARAN_SEMULA => 28, WaitingListPUPeer::METODE_WAITINGLIST => 29, ),
		BasePeer::TYPE_FIELDNAME => array ('id_waiting' => 0, 'unit_id' => 1, 'kegiatan_code' => 2, 'subtitle' => 3, 'komponen_id' => 4, 'komponen_name' => 5, 'komponen_lokasi' => 6, 'komponen_harga_awal' => 7, 'pajak' => 8, 'komponen_satuan' => 9, 'komponen_rekening' => 10, 'koefisien' => 11, 'volume' => 12, 'nilai_anggaran' => 13, 'tahun_input' => 14, 'created_at' => 15, 'updated_at' => 16, 'status_hapus' => 17, 'status_waiting' => 18, 'prioritas' => 19, 'kode_rka' => 20, 'user_pengambil' => 21, 'nilai_ee' => 22, 'keterangan' => 23, 'kode_jasmas' => 24, 'kecamatan' => 25, 'kelurahan' => 26, 'is_musrenbang' => 27, 'nilai_anggaran_semula' => 28, 'metode_waitinglist' => 29, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/WaitingListPUMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.WaitingListPUMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = WaitingListPUPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(WaitingListPUPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(WaitingListPUPeer::ID_WAITING);

		$criteria->addSelectColumn(WaitingListPUPeer::UNIT_ID);

		$criteria->addSelectColumn(WaitingListPUPeer::KEGIATAN_CODE);

		$criteria->addSelectColumn(WaitingListPUPeer::SUBTITLE);

		$criteria->addSelectColumn(WaitingListPUPeer::KOMPONEN_ID);

		$criteria->addSelectColumn(WaitingListPUPeer::KOMPONEN_NAME);

		$criteria->addSelectColumn(WaitingListPUPeer::KOMPONEN_LOKASI);

		$criteria->addSelectColumn(WaitingListPUPeer::KOMPONEN_HARGA_AWAL);

		$criteria->addSelectColumn(WaitingListPUPeer::PAJAK);

		$criteria->addSelectColumn(WaitingListPUPeer::KOMPONEN_SATUAN);

		$criteria->addSelectColumn(WaitingListPUPeer::KOMPONEN_REKENING);

		$criteria->addSelectColumn(WaitingListPUPeer::KOEFISIEN);

		$criteria->addSelectColumn(WaitingListPUPeer::VOLUME);

		$criteria->addSelectColumn(WaitingListPUPeer::NILAI_ANGGARAN);

		$criteria->addSelectColumn(WaitingListPUPeer::TAHUN_INPUT);

		$criteria->addSelectColumn(WaitingListPUPeer::CREATED_AT);

		$criteria->addSelectColumn(WaitingListPUPeer::UPDATED_AT);

		$criteria->addSelectColumn(WaitingListPUPeer::STATUS_HAPUS);

		$criteria->addSelectColumn(WaitingListPUPeer::STATUS_WAITING);

		$criteria->addSelectColumn(WaitingListPUPeer::PRIORITAS);

		$criteria->addSelectColumn(WaitingListPUPeer::KODE_RKA);

		$criteria->addSelectColumn(WaitingListPUPeer::USER_PENGAMBIL);

		$criteria->addSelectColumn(WaitingListPUPeer::NILAI_EE);

		$criteria->addSelectColumn(WaitingListPUPeer::KETERANGAN);

		$criteria->addSelectColumn(WaitingListPUPeer::KODE_JASMAS);

		$criteria->addSelectColumn(WaitingListPUPeer::KECAMATAN);

		$criteria->addSelectColumn(WaitingListPUPeer::KELURAHAN);

		$criteria->addSelectColumn(WaitingListPUPeer::IS_MUSRENBANG);

		$criteria->addSelectColumn(WaitingListPUPeer::NILAI_ANGGARAN_SEMULA);

		$criteria->addSelectColumn(WaitingListPUPeer::METODE_WAITINGLIST);

	}

	const COUNT = 'COUNT(ebudget.waitinglist_pu.ID_WAITING)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.waitinglist_pu.ID_WAITING)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(WaitingListPUPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(WaitingListPUPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = WaitingListPUPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = WaitingListPUPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return WaitingListPUPeer::populateObjects(WaitingListPUPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			WaitingListPUPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = WaitingListPUPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return WaitingListPUPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}

		$criteria->remove(WaitingListPUPeer::ID_WAITING); 

				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(WaitingListPUPeer::ID_WAITING);
			$selectCriteria->add(WaitingListPUPeer::ID_WAITING, $criteria->remove(WaitingListPUPeer::ID_WAITING), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(WaitingListPUPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(WaitingListPUPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof WaitingListPU) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(WaitingListPUPeer::ID_WAITING, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(WaitingListPU $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(WaitingListPUPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(WaitingListPUPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(WaitingListPUPeer::DATABASE_NAME, WaitingListPUPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = WaitingListPUPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(WaitingListPUPeer::DATABASE_NAME);

		$criteria->add(WaitingListPUPeer::ID_WAITING, $pk);


		$v = WaitingListPUPeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(WaitingListPUPeer::ID_WAITING, $pks, Criteria::IN);
			$objs = WaitingListPUPeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BaseWaitingListPUPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/WaitingListPUMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.WaitingListPUMapBuilder');
}
