<?php


abstract class BaseBelurRincianPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.belur_rincian';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.BelurRincian';

	
	const NUM_COLUMNS = 16;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const KEGIATAN_CODE = 'ebudget.belur_rincian.KEGIATAN_CODE';

	
	const TIPE = 'ebudget.belur_rincian.TIPE';

	
	const RINCIAN_CONFIRMED = 'ebudget.belur_rincian.RINCIAN_CONFIRMED';

	
	const RINCIAN_CHANGED = 'ebudget.belur_rincian.RINCIAN_CHANGED';

	
	const RINCIAN_SELESAI = 'ebudget.belur_rincian.RINCIAN_SELESAI';

	
	const IP_ADDRESS = 'ebudget.belur_rincian.IP_ADDRESS';

	
	const WAKTU_ACCESS = 'ebudget.belur_rincian.WAKTU_ACCESS';

	
	const TARGET = 'ebudget.belur_rincian.TARGET';

	
	const UNIT_ID = 'ebudget.belur_rincian.UNIT_ID';

	
	const RINCIAN_LEVEL = 'ebudget.belur_rincian.RINCIAN_LEVEL';

	
	const LOCK = 'ebudget.belur_rincian.LOCK';

	
	const LAST_UPDATE_USER = 'ebudget.belur_rincian.LAST_UPDATE_USER';

	
	const LAST_UPDATE_TIME = 'ebudget.belur_rincian.LAST_UPDATE_TIME';

	
	const LAST_UPDATE_IP = 'ebudget.belur_rincian.LAST_UPDATE_IP';

	
	const TAHAP = 'ebudget.belur_rincian.TAHAP';

	
	const TAHUN = 'ebudget.belur_rincian.TAHUN';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('KegiatanCode', 'Tipe', 'RincianConfirmed', 'RincianChanged', 'RincianSelesai', 'IpAddress', 'WaktuAccess', 'Target', 'UnitId', 'RincianLevel', 'Lock', 'LastUpdateUser', 'LastUpdateTime', 'LastUpdateIp', 'Tahap', 'Tahun', ),
		BasePeer::TYPE_COLNAME => array (BelurRincianPeer::KEGIATAN_CODE, BelurRincianPeer::TIPE, BelurRincianPeer::RINCIAN_CONFIRMED, BelurRincianPeer::RINCIAN_CHANGED, BelurRincianPeer::RINCIAN_SELESAI, BelurRincianPeer::IP_ADDRESS, BelurRincianPeer::WAKTU_ACCESS, BelurRincianPeer::TARGET, BelurRincianPeer::UNIT_ID, BelurRincianPeer::RINCIAN_LEVEL, BelurRincianPeer::LOCK, BelurRincianPeer::LAST_UPDATE_USER, BelurRincianPeer::LAST_UPDATE_TIME, BelurRincianPeer::LAST_UPDATE_IP, BelurRincianPeer::TAHAP, BelurRincianPeer::TAHUN, ),
		BasePeer::TYPE_FIELDNAME => array ('kegiatan_code', 'tipe', 'rincian_confirmed', 'rincian_changed', 'rincian_selesai', 'ip_address', 'waktu_access', 'target', 'unit_id', 'rincian_level', 'lock', 'last_update_user', 'last_update_time', 'last_update_ip', 'tahap', 'tahun', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('KegiatanCode' => 0, 'Tipe' => 1, 'RincianConfirmed' => 2, 'RincianChanged' => 3, 'RincianSelesai' => 4, 'IpAddress' => 5, 'WaktuAccess' => 6, 'Target' => 7, 'UnitId' => 8, 'RincianLevel' => 9, 'Lock' => 10, 'LastUpdateUser' => 11, 'LastUpdateTime' => 12, 'LastUpdateIp' => 13, 'Tahap' => 14, 'Tahun' => 15, ),
		BasePeer::TYPE_COLNAME => array (BelurRincianPeer::KEGIATAN_CODE => 0, BelurRincianPeer::TIPE => 1, BelurRincianPeer::RINCIAN_CONFIRMED => 2, BelurRincianPeer::RINCIAN_CHANGED => 3, BelurRincianPeer::RINCIAN_SELESAI => 4, BelurRincianPeer::IP_ADDRESS => 5, BelurRincianPeer::WAKTU_ACCESS => 6, BelurRincianPeer::TARGET => 7, BelurRincianPeer::UNIT_ID => 8, BelurRincianPeer::RINCIAN_LEVEL => 9, BelurRincianPeer::LOCK => 10, BelurRincianPeer::LAST_UPDATE_USER => 11, BelurRincianPeer::LAST_UPDATE_TIME => 12, BelurRincianPeer::LAST_UPDATE_IP => 13, BelurRincianPeer::TAHAP => 14, BelurRincianPeer::TAHUN => 15, ),
		BasePeer::TYPE_FIELDNAME => array ('kegiatan_code' => 0, 'tipe' => 1, 'rincian_confirmed' => 2, 'rincian_changed' => 3, 'rincian_selesai' => 4, 'ip_address' => 5, 'waktu_access' => 6, 'target' => 7, 'unit_id' => 8, 'rincian_level' => 9, 'lock' => 10, 'last_update_user' => 11, 'last_update_time' => 12, 'last_update_ip' => 13, 'tahap' => 14, 'tahun' => 15, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/BelurRincianMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.BelurRincianMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = BelurRincianPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(BelurRincianPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(BelurRincianPeer::KEGIATAN_CODE);

		$criteria->addSelectColumn(BelurRincianPeer::TIPE);

		$criteria->addSelectColumn(BelurRincianPeer::RINCIAN_CONFIRMED);

		$criteria->addSelectColumn(BelurRincianPeer::RINCIAN_CHANGED);

		$criteria->addSelectColumn(BelurRincianPeer::RINCIAN_SELESAI);

		$criteria->addSelectColumn(BelurRincianPeer::IP_ADDRESS);

		$criteria->addSelectColumn(BelurRincianPeer::WAKTU_ACCESS);

		$criteria->addSelectColumn(BelurRincianPeer::TARGET);

		$criteria->addSelectColumn(BelurRincianPeer::UNIT_ID);

		$criteria->addSelectColumn(BelurRincianPeer::RINCIAN_LEVEL);

		$criteria->addSelectColumn(BelurRincianPeer::LOCK);

		$criteria->addSelectColumn(BelurRincianPeer::LAST_UPDATE_USER);

		$criteria->addSelectColumn(BelurRincianPeer::LAST_UPDATE_TIME);

		$criteria->addSelectColumn(BelurRincianPeer::LAST_UPDATE_IP);

		$criteria->addSelectColumn(BelurRincianPeer::TAHAP);

		$criteria->addSelectColumn(BelurRincianPeer::TAHUN);

	}

	const COUNT = 'COUNT(ebudget.belur_rincian.KEGIATAN_CODE)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.belur_rincian.KEGIATAN_CODE)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(BelurRincianPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(BelurRincianPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = BelurRincianPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = BelurRincianPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return BelurRincianPeer::populateObjects(BelurRincianPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			BelurRincianPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = BelurRincianPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return BelurRincianPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(BelurRincianPeer::KEGIATAN_CODE);
			$selectCriteria->add(BelurRincianPeer::KEGIATAN_CODE, $criteria->remove(BelurRincianPeer::KEGIATAN_CODE), $comparison);

			$comparison = $criteria->getComparison(BelurRincianPeer::TIPE);
			$selectCriteria->add(BelurRincianPeer::TIPE, $criteria->remove(BelurRincianPeer::TIPE), $comparison);

			$comparison = $criteria->getComparison(BelurRincianPeer::UNIT_ID);
			$selectCriteria->add(BelurRincianPeer::UNIT_ID, $criteria->remove(BelurRincianPeer::UNIT_ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(BelurRincianPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(BelurRincianPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof BelurRincian) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
												if(count($values) == count($values, COUNT_RECURSIVE))
			{
								$values = array($values);
			}
			$vals = array();
			foreach($values as $value)
			{

				$vals[0][] = $value[0];
				$vals[1][] = $value[1];
				$vals[2][] = $value[2];
			}

			$criteria->add(BelurRincianPeer::KEGIATAN_CODE, $vals[0], Criteria::IN);
			$criteria->add(BelurRincianPeer::TIPE, $vals[1], Criteria::IN);
			$criteria->add(BelurRincianPeer::UNIT_ID, $vals[2], Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(BelurRincian $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(BelurRincianPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(BelurRincianPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(BelurRincianPeer::DATABASE_NAME, BelurRincianPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = BelurRincianPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK( $kegiatan_code, $tipe, $unit_id, $con = null) {
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$criteria = new Criteria();
		$criteria->add(BelurRincianPeer::KEGIATAN_CODE, $kegiatan_code);
		$criteria->add(BelurRincianPeer::TIPE, $tipe);
		$criteria->add(BelurRincianPeer::UNIT_ID, $unit_id);
		$v = BelurRincianPeer::doSelect($criteria, $con);

		return !empty($v) ? $v[0] : null;
	}
} 
if (Propel::isInit()) {
			try {
		BaseBelurRincianPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/BelurRincianMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.BelurRincianMapBuilder');
}
