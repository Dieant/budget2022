<?php


abstract class BaseMasterSchema extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $schema_id;


	
	protected $schema_name;

	
	protected $collSchemaAksess;

	
	protected $lastSchemaAksesCriteria = null;

	
	protected $collSchemaAksesV2s;

	
	protected $lastSchemaAksesV2Criteria = null;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getSchemaId()
	{

		return $this->schema_id;
	}

	
	public function getSchemaName()
	{

		return $this->schema_name;
	}

	
	public function setSchemaId($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->schema_id !== $v) {
			$this->schema_id = $v;
			$this->modifiedColumns[] = MasterSchemaPeer::SCHEMA_ID;
		}

	} 
	
	public function setSchemaName($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->schema_name !== $v) {
			$this->schema_name = $v;
			$this->modifiedColumns[] = MasterSchemaPeer::SCHEMA_NAME;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->schema_id = $rs->getInt($startcol + 0);

			$this->schema_name = $rs->getString($startcol + 1);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 2; 
		} catch (Exception $e) {
			throw new PropelException("Error populating MasterSchema object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(MasterSchemaPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			MasterSchemaPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(MasterSchemaPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = MasterSchemaPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setSchemaId($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += MasterSchemaPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			if ($this->collSchemaAksess !== null) {
				foreach($this->collSchemaAksess as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			if ($this->collSchemaAksesV2s !== null) {
				foreach($this->collSchemaAksesV2s as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = MasterSchemaPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}


				if ($this->collSchemaAksess !== null) {
					foreach($this->collSchemaAksess as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}

				if ($this->collSchemaAksesV2s !== null) {
					foreach($this->collSchemaAksesV2s as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}


			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = MasterSchemaPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getSchemaId();
				break;
			case 1:
				return $this->getSchemaName();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = MasterSchemaPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getSchemaId(),
			$keys[1] => $this->getSchemaName(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = MasterSchemaPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setSchemaId($value);
				break;
			case 1:
				$this->setSchemaName($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = MasterSchemaPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setSchemaId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setSchemaName($arr[$keys[1]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(MasterSchemaPeer::DATABASE_NAME);

		if ($this->isColumnModified(MasterSchemaPeer::SCHEMA_ID)) $criteria->add(MasterSchemaPeer::SCHEMA_ID, $this->schema_id);
		if ($this->isColumnModified(MasterSchemaPeer::SCHEMA_NAME)) $criteria->add(MasterSchemaPeer::SCHEMA_NAME, $this->schema_name);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(MasterSchemaPeer::DATABASE_NAME);

		$criteria->add(MasterSchemaPeer::SCHEMA_ID, $this->schema_id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getSchemaId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setSchemaId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setSchemaName($this->schema_name);


		if ($deepCopy) {
									$copyObj->setNew(false);

			foreach($this->getSchemaAksess() as $relObj) {
				$copyObj->addSchemaAkses($relObj->copy($deepCopy));
			}

			foreach($this->getSchemaAksesV2s() as $relObj) {
				$copyObj->addSchemaAksesV2($relObj->copy($deepCopy));
			}

		} 

		$copyObj->setNew(true);

		$copyObj->setSchemaId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new MasterSchemaPeer();
		}
		return self::$peer;
	}

	
	public function initSchemaAksess()
	{
		if ($this->collSchemaAksess === null) {
			$this->collSchemaAksess = array();
		}
	}

	
	public function getSchemaAksess($criteria = null, $con = null)
	{
				include_once 'lib/model/budgeting/om/BaseSchemaAksesPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collSchemaAksess === null) {
			if ($this->isNew()) {
			   $this->collSchemaAksess = array();
			} else {

				$criteria->add(SchemaAksesPeer::SCHEMA_ID, $this->getSchemaId());

				SchemaAksesPeer::addSelectColumns($criteria);
				$this->collSchemaAksess = SchemaAksesPeer::doSelect($criteria, $con);
			}
		} else {
						if (!$this->isNew()) {
												

				$criteria->add(SchemaAksesPeer::SCHEMA_ID, $this->getSchemaId());

				SchemaAksesPeer::addSelectColumns($criteria);
				if (!isset($this->lastSchemaAksesCriteria) || !$this->lastSchemaAksesCriteria->equals($criteria)) {
					$this->collSchemaAksess = SchemaAksesPeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastSchemaAksesCriteria = $criteria;
		return $this->collSchemaAksess;
	}

	
	public function countSchemaAksess($criteria = null, $distinct = false, $con = null)
	{
				include_once 'lib/model/budgeting/om/BaseSchemaAksesPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(SchemaAksesPeer::SCHEMA_ID, $this->getSchemaId());

		return SchemaAksesPeer::doCount($criteria, $distinct, $con);
	}

	
	public function addSchemaAkses(SchemaAkses $l)
	{
		$this->collSchemaAksess[] = $l;
		$l->setMasterSchema($this);
	}


	
	public function getSchemaAksessJoinMasterUser($criteria = null, $con = null)
	{
				include_once 'lib/model/budgeting/om/BaseSchemaAksesPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collSchemaAksess === null) {
			if ($this->isNew()) {
				$this->collSchemaAksess = array();
			} else {

				$criteria->add(SchemaAksesPeer::SCHEMA_ID, $this->getSchemaId());

				$this->collSchemaAksess = SchemaAksesPeer::doSelectJoinMasterUser($criteria, $con);
			}
		} else {
									
			$criteria->add(SchemaAksesPeer::SCHEMA_ID, $this->getSchemaId());

			if (!isset($this->lastSchemaAksesCriteria) || !$this->lastSchemaAksesCriteria->equals($criteria)) {
				$this->collSchemaAksess = SchemaAksesPeer::doSelectJoinMasterUser($criteria, $con);
			}
		}
		$this->lastSchemaAksesCriteria = $criteria;

		return $this->collSchemaAksess;
	}


	
	public function getSchemaAksessJoinUserLevel($criteria = null, $con = null)
	{
				include_once 'lib/model/budgeting/om/BaseSchemaAksesPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collSchemaAksess === null) {
			if ($this->isNew()) {
				$this->collSchemaAksess = array();
			} else {

				$criteria->add(SchemaAksesPeer::SCHEMA_ID, $this->getSchemaId());

				$this->collSchemaAksess = SchemaAksesPeer::doSelectJoinUserLevel($criteria, $con);
			}
		} else {
									
			$criteria->add(SchemaAksesPeer::SCHEMA_ID, $this->getSchemaId());

			if (!isset($this->lastSchemaAksesCriteria) || !$this->lastSchemaAksesCriteria->equals($criteria)) {
				$this->collSchemaAksess = SchemaAksesPeer::doSelectJoinUserLevel($criteria, $con);
			}
		}
		$this->lastSchemaAksesCriteria = $criteria;

		return $this->collSchemaAksess;
	}

	
	public function initSchemaAksesV2s()
	{
		if ($this->collSchemaAksesV2s === null) {
			$this->collSchemaAksesV2s = array();
		}
	}

	
	public function getSchemaAksesV2s($criteria = null, $con = null)
	{
				include_once 'lib/model/budgeting/om/BaseSchemaAksesV2Peer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collSchemaAksesV2s === null) {
			if ($this->isNew()) {
			   $this->collSchemaAksesV2s = array();
			} else {

				$criteria->add(SchemaAksesV2Peer::SCHEMA_ID, $this->getSchemaId());

				SchemaAksesV2Peer::addSelectColumns($criteria);
				$this->collSchemaAksesV2s = SchemaAksesV2Peer::doSelect($criteria, $con);
			}
		} else {
						if (!$this->isNew()) {
												

				$criteria->add(SchemaAksesV2Peer::SCHEMA_ID, $this->getSchemaId());

				SchemaAksesV2Peer::addSelectColumns($criteria);
				if (!isset($this->lastSchemaAksesV2Criteria) || !$this->lastSchemaAksesV2Criteria->equals($criteria)) {
					$this->collSchemaAksesV2s = SchemaAksesV2Peer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastSchemaAksesV2Criteria = $criteria;
		return $this->collSchemaAksesV2s;
	}

	
	public function countSchemaAksesV2s($criteria = null, $distinct = false, $con = null)
	{
				include_once 'lib/model/budgeting/om/BaseSchemaAksesV2Peer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(SchemaAksesV2Peer::SCHEMA_ID, $this->getSchemaId());

		return SchemaAksesV2Peer::doCount($criteria, $distinct, $con);
	}

	
	public function addSchemaAksesV2(SchemaAksesV2 $l)
	{
		$this->collSchemaAksesV2s[] = $l;
		$l->setMasterSchema($this);
	}


	
	public function getSchemaAksesV2sJoinMasterUserV2($criteria = null, $con = null)
	{
				include_once 'lib/model/budgeting/om/BaseSchemaAksesV2Peer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collSchemaAksesV2s === null) {
			if ($this->isNew()) {
				$this->collSchemaAksesV2s = array();
			} else {

				$criteria->add(SchemaAksesV2Peer::SCHEMA_ID, $this->getSchemaId());

				$this->collSchemaAksesV2s = SchemaAksesV2Peer::doSelectJoinMasterUserV2($criteria, $con);
			}
		} else {
									
			$criteria->add(SchemaAksesV2Peer::SCHEMA_ID, $this->getSchemaId());

			if (!isset($this->lastSchemaAksesV2Criteria) || !$this->lastSchemaAksesV2Criteria->equals($criteria)) {
				$this->collSchemaAksesV2s = SchemaAksesV2Peer::doSelectJoinMasterUserV2($criteria, $con);
			}
		}
		$this->lastSchemaAksesV2Criteria = $criteria;

		return $this->collSchemaAksesV2s;
	}


	
	public function getSchemaAksesV2sJoinUserLevel($criteria = null, $con = null)
	{
				include_once 'lib/model/budgeting/om/BaseSchemaAksesV2Peer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collSchemaAksesV2s === null) {
			if ($this->isNew()) {
				$this->collSchemaAksesV2s = array();
			} else {

				$criteria->add(SchemaAksesV2Peer::SCHEMA_ID, $this->getSchemaId());

				$this->collSchemaAksesV2s = SchemaAksesV2Peer::doSelectJoinUserLevel($criteria, $con);
			}
		} else {
									
			$criteria->add(SchemaAksesV2Peer::SCHEMA_ID, $this->getSchemaId());

			if (!isset($this->lastSchemaAksesV2Criteria) || !$this->lastSchemaAksesV2Criteria->equals($criteria)) {
				$this->collSchemaAksesV2s = SchemaAksesV2Peer::doSelectJoinUserLevel($criteria, $con);
			}
		}
		$this->lastSchemaAksesV2Criteria = $criteria;

		return $this->collSchemaAksesV2s;
	}

} 