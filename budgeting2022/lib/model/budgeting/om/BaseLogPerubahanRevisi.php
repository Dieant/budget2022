<?php


abstract class BaseLogPerubahanRevisi extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $kegiatan_code;


	
	protected $detail_no;


	
	protected $unit_id;


	
	protected $tahap;


	
	protected $status = 0;


	
	protected $nilai_anggaran_semula;


	
	protected $nilai_anggaran_menjadi;


	
	protected $volume_semula;


	
	protected $volume_menjadi;


	
	protected $satuan_semula;


	
	protected $satuan_menjadi;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getKegiatanCode()
	{

		return $this->kegiatan_code;
	}

	
	public function getDetailNo()
	{

		return $this->detail_no;
	}

	
	public function getUnitId()
	{

		return $this->unit_id;
	}

	
	public function getTahap()
	{

		return $this->tahap;
	}

	
	public function getStatus()
	{

		return $this->status;
	}

	
	public function getNilaiAnggaranSemula()
	{

		return $this->nilai_anggaran_semula;
	}

	
	public function getNilaiAnggaranMenjadi()
	{

		return $this->nilai_anggaran_menjadi;
	}

	
	public function getVolumeSemula()
	{

		return $this->volume_semula;
	}

	
	public function getVolumeMenjadi()
	{

		return $this->volume_menjadi;
	}

	
	public function getSatuanSemula()
	{

		return $this->satuan_semula;
	}

	
	public function getSatuanMenjadi()
	{

		return $this->satuan_menjadi;
	}

	
	public function setKegiatanCode($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kegiatan_code !== $v) {
			$this->kegiatan_code = $v;
			$this->modifiedColumns[] = LogPerubahanRevisiPeer::KEGIATAN_CODE;
		}

	} 
	
	public function setDetailNo($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->detail_no !== $v) {
			$this->detail_no = $v;
			$this->modifiedColumns[] = LogPerubahanRevisiPeer::DETAIL_NO;
		}

	} 
	
	public function setUnitId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->unit_id !== $v) {
			$this->unit_id = $v;
			$this->modifiedColumns[] = LogPerubahanRevisiPeer::UNIT_ID;
		}

	} 
	
	public function setTahap($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->tahap !== $v) {
			$this->tahap = $v;
			$this->modifiedColumns[] = LogPerubahanRevisiPeer::TAHAP;
		}

	} 
	
	public function setStatus($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->status !== $v || $v === 0) {
			$this->status = $v;
			$this->modifiedColumns[] = LogPerubahanRevisiPeer::STATUS;
		}

	} 
	
	public function setNilaiAnggaranSemula($v)
	{

		if ($this->nilai_anggaran_semula !== $v) {
			$this->nilai_anggaran_semula = $v;
			$this->modifiedColumns[] = LogPerubahanRevisiPeer::NILAI_ANGGARAN_SEMULA;
		}

	} 
	
	public function setNilaiAnggaranMenjadi($v)
	{

		if ($this->nilai_anggaran_menjadi !== $v) {
			$this->nilai_anggaran_menjadi = $v;
			$this->modifiedColumns[] = LogPerubahanRevisiPeer::NILAI_ANGGARAN_MENJADI;
		}

	} 
	
	public function setVolumeSemula($v)
	{

		if ($this->volume_semula !== $v) {
			$this->volume_semula = $v;
			$this->modifiedColumns[] = LogPerubahanRevisiPeer::VOLUME_SEMULA;
		}

	} 
	
	public function setVolumeMenjadi($v)
	{

		if ($this->volume_menjadi !== $v) {
			$this->volume_menjadi = $v;
			$this->modifiedColumns[] = LogPerubahanRevisiPeer::VOLUME_MENJADI;
		}

	} 
	
	public function setSatuanSemula($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->satuan_semula !== $v) {
			$this->satuan_semula = $v;
			$this->modifiedColumns[] = LogPerubahanRevisiPeer::SATUAN_SEMULA;
		}

	} 
	
	public function setSatuanMenjadi($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->satuan_menjadi !== $v) {
			$this->satuan_menjadi = $v;
			$this->modifiedColumns[] = LogPerubahanRevisiPeer::SATUAN_MENJADI;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->kegiatan_code = $rs->getString($startcol + 0);

			$this->detail_no = $rs->getInt($startcol + 1);

			$this->unit_id = $rs->getString($startcol + 2);

			$this->tahap = $rs->getInt($startcol + 3);

			$this->status = $rs->getInt($startcol + 4);

			$this->nilai_anggaran_semula = $rs->getFloat($startcol + 5);

			$this->nilai_anggaran_menjadi = $rs->getFloat($startcol + 6);

			$this->volume_semula = $rs->getFloat($startcol + 7);

			$this->volume_menjadi = $rs->getFloat($startcol + 8);

			$this->satuan_semula = $rs->getString($startcol + 9);

			$this->satuan_menjadi = $rs->getString($startcol + 10);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 11; 
		} catch (Exception $e) {
			throw new PropelException("Error populating LogPerubahanRevisi object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(LogPerubahanRevisiPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			LogPerubahanRevisiPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(LogPerubahanRevisiPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = LogPerubahanRevisiPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += LogPerubahanRevisiPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = LogPerubahanRevisiPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = LogPerubahanRevisiPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getKegiatanCode();
				break;
			case 1:
				return $this->getDetailNo();
				break;
			case 2:
				return $this->getUnitId();
				break;
			case 3:
				return $this->getTahap();
				break;
			case 4:
				return $this->getStatus();
				break;
			case 5:
				return $this->getNilaiAnggaranSemula();
				break;
			case 6:
				return $this->getNilaiAnggaranMenjadi();
				break;
			case 7:
				return $this->getVolumeSemula();
				break;
			case 8:
				return $this->getVolumeMenjadi();
				break;
			case 9:
				return $this->getSatuanSemula();
				break;
			case 10:
				return $this->getSatuanMenjadi();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = LogPerubahanRevisiPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getKegiatanCode(),
			$keys[1] => $this->getDetailNo(),
			$keys[2] => $this->getUnitId(),
			$keys[3] => $this->getTahap(),
			$keys[4] => $this->getStatus(),
			$keys[5] => $this->getNilaiAnggaranSemula(),
			$keys[6] => $this->getNilaiAnggaranMenjadi(),
			$keys[7] => $this->getVolumeSemula(),
			$keys[8] => $this->getVolumeMenjadi(),
			$keys[9] => $this->getSatuanSemula(),
			$keys[10] => $this->getSatuanMenjadi(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = LogPerubahanRevisiPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setKegiatanCode($value);
				break;
			case 1:
				$this->setDetailNo($value);
				break;
			case 2:
				$this->setUnitId($value);
				break;
			case 3:
				$this->setTahap($value);
				break;
			case 4:
				$this->setStatus($value);
				break;
			case 5:
				$this->setNilaiAnggaranSemula($value);
				break;
			case 6:
				$this->setNilaiAnggaranMenjadi($value);
				break;
			case 7:
				$this->setVolumeSemula($value);
				break;
			case 8:
				$this->setVolumeMenjadi($value);
				break;
			case 9:
				$this->setSatuanSemula($value);
				break;
			case 10:
				$this->setSatuanMenjadi($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = LogPerubahanRevisiPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setKegiatanCode($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setDetailNo($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setUnitId($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setTahap($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setStatus($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setNilaiAnggaranSemula($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setNilaiAnggaranMenjadi($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setVolumeSemula($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setVolumeMenjadi($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setSatuanSemula($arr[$keys[9]]);
		if (array_key_exists($keys[10], $arr)) $this->setSatuanMenjadi($arr[$keys[10]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(LogPerubahanRevisiPeer::DATABASE_NAME);

		if ($this->isColumnModified(LogPerubahanRevisiPeer::KEGIATAN_CODE)) $criteria->add(LogPerubahanRevisiPeer::KEGIATAN_CODE, $this->kegiatan_code);
		if ($this->isColumnModified(LogPerubahanRevisiPeer::DETAIL_NO)) $criteria->add(LogPerubahanRevisiPeer::DETAIL_NO, $this->detail_no);
		if ($this->isColumnModified(LogPerubahanRevisiPeer::UNIT_ID)) $criteria->add(LogPerubahanRevisiPeer::UNIT_ID, $this->unit_id);
		if ($this->isColumnModified(LogPerubahanRevisiPeer::TAHAP)) $criteria->add(LogPerubahanRevisiPeer::TAHAP, $this->tahap);
		if ($this->isColumnModified(LogPerubahanRevisiPeer::STATUS)) $criteria->add(LogPerubahanRevisiPeer::STATUS, $this->status);
		if ($this->isColumnModified(LogPerubahanRevisiPeer::NILAI_ANGGARAN_SEMULA)) $criteria->add(LogPerubahanRevisiPeer::NILAI_ANGGARAN_SEMULA, $this->nilai_anggaran_semula);
		if ($this->isColumnModified(LogPerubahanRevisiPeer::NILAI_ANGGARAN_MENJADI)) $criteria->add(LogPerubahanRevisiPeer::NILAI_ANGGARAN_MENJADI, $this->nilai_anggaran_menjadi);
		if ($this->isColumnModified(LogPerubahanRevisiPeer::VOLUME_SEMULA)) $criteria->add(LogPerubahanRevisiPeer::VOLUME_SEMULA, $this->volume_semula);
		if ($this->isColumnModified(LogPerubahanRevisiPeer::VOLUME_MENJADI)) $criteria->add(LogPerubahanRevisiPeer::VOLUME_MENJADI, $this->volume_menjadi);
		if ($this->isColumnModified(LogPerubahanRevisiPeer::SATUAN_SEMULA)) $criteria->add(LogPerubahanRevisiPeer::SATUAN_SEMULA, $this->satuan_semula);
		if ($this->isColumnModified(LogPerubahanRevisiPeer::SATUAN_MENJADI)) $criteria->add(LogPerubahanRevisiPeer::SATUAN_MENJADI, $this->satuan_menjadi);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(LogPerubahanRevisiPeer::DATABASE_NAME);

		$criteria->add(LogPerubahanRevisiPeer::KEGIATAN_CODE, $this->kegiatan_code);
		$criteria->add(LogPerubahanRevisiPeer::DETAIL_NO, $this->detail_no);
		$criteria->add(LogPerubahanRevisiPeer::UNIT_ID, $this->unit_id);
		$criteria->add(LogPerubahanRevisiPeer::TAHAP, $this->tahap);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		$pks = array();

		$pks[0] = $this->getKegiatanCode();

		$pks[1] = $this->getDetailNo();

		$pks[2] = $this->getUnitId();

		$pks[3] = $this->getTahap();

		return $pks;
	}

	
	public function setPrimaryKey($keys)
	{

		$this->setKegiatanCode($keys[0]);

		$this->setDetailNo($keys[1]);

		$this->setUnitId($keys[2]);

		$this->setTahap($keys[3]);

	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setStatus($this->status);

		$copyObj->setNilaiAnggaranSemula($this->nilai_anggaran_semula);

		$copyObj->setNilaiAnggaranMenjadi($this->nilai_anggaran_menjadi);

		$copyObj->setVolumeSemula($this->volume_semula);

		$copyObj->setVolumeMenjadi($this->volume_menjadi);

		$copyObj->setSatuanSemula($this->satuan_semula);

		$copyObj->setSatuanMenjadi($this->satuan_menjadi);


		$copyObj->setNew(true);

		$copyObj->setKegiatanCode(NULL); 
		$copyObj->setDetailNo(NULL); 
		$copyObj->setUnitId(NULL); 
		$copyObj->setTahap(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new LogPerubahanRevisiPeer();
		}
		return self::$peer;
	}

} 