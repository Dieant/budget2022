<?php


abstract class BaseKomponenFisikPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.komponen_fisik';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.KomponenFisik';

	
	const NUM_COLUMNS = 19;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const KOMPONEN_ID = 'ebudget.komponen_fisik.KOMPONEN_ID';

	
	const SATUAN = 'ebudget.komponen_fisik.SATUAN';

	
	const KOMPONEN_NAME = 'ebudget.komponen_fisik.KOMPONEN_NAME';

	
	const SHSD_ID = 'ebudget.komponen_fisik.SHSD_ID';

	
	const KOMPONEN_HARGA = 'ebudget.komponen_fisik.KOMPONEN_HARGA';

	
	const KOMPONEN_SHOW = 'ebudget.komponen_fisik.KOMPONEN_SHOW';

	
	const IP_ADDRESS = 'ebudget.komponen_fisik.IP_ADDRESS';

	
	const WAKTU_ACCESS = 'ebudget.komponen_fisik.WAKTU_ACCESS';

	
	const KOMPONEN_TIPE = 'ebudget.komponen_fisik.KOMPONEN_TIPE';

	
	const KOMPONEN_CONFIRMED = 'ebudget.komponen_fisik.KOMPONEN_CONFIRMED';

	
	const KOMPONEN_NON_PAJAK = 'ebudget.komponen_fisik.KOMPONEN_NON_PAJAK';

	
	const USER_ID = 'ebudget.komponen_fisik.USER_ID';

	
	const REKENING = 'ebudget.komponen_fisik.REKENING';

	
	const KELOMPOK = 'ebudget.komponen_fisik.KELOMPOK';

	
	const PEMELIHARAAN = 'ebudget.komponen_fisik.PEMELIHARAAN';

	
	const REK_UPAH = 'ebudget.komponen_fisik.REK_UPAH';

	
	const REK_BAHAN = 'ebudget.komponen_fisik.REK_BAHAN';

	
	const REK_SEWA = 'ebudget.komponen_fisik.REK_SEWA';

	
	const DESKRIPSI = 'ebudget.komponen_fisik.DESKRIPSI';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('KomponenId', 'Satuan', 'KomponenName', 'ShsdId', 'KomponenHarga', 'KomponenShow', 'IpAddress', 'WaktuAccess', 'KomponenTipe', 'KomponenConfirmed', 'KomponenNonPajak', 'UserId', 'Rekening', 'Kelompok', 'Pemeliharaan', 'RekUpah', 'RekBahan', 'RekSewa', 'Deskripsi', ),
		BasePeer::TYPE_COLNAME => array (KomponenFisikPeer::KOMPONEN_ID, KomponenFisikPeer::SATUAN, KomponenFisikPeer::KOMPONEN_NAME, KomponenFisikPeer::SHSD_ID, KomponenFisikPeer::KOMPONEN_HARGA, KomponenFisikPeer::KOMPONEN_SHOW, KomponenFisikPeer::IP_ADDRESS, KomponenFisikPeer::WAKTU_ACCESS, KomponenFisikPeer::KOMPONEN_TIPE, KomponenFisikPeer::KOMPONEN_CONFIRMED, KomponenFisikPeer::KOMPONEN_NON_PAJAK, KomponenFisikPeer::USER_ID, KomponenFisikPeer::REKENING, KomponenFisikPeer::KELOMPOK, KomponenFisikPeer::PEMELIHARAAN, KomponenFisikPeer::REK_UPAH, KomponenFisikPeer::REK_BAHAN, KomponenFisikPeer::REK_SEWA, KomponenFisikPeer::DESKRIPSI, ),
		BasePeer::TYPE_FIELDNAME => array ('komponen_id', 'satuan', 'komponen_name', 'shsd_id', 'komponen_harga', 'komponen_show', 'ip_address', 'waktu_access', 'komponen_tipe', 'komponen_confirmed', 'komponen_non_pajak', 'user_id', 'rekening', 'kelompok', 'pemeliharaan', 'rek_upah', 'rek_bahan', 'rek_sewa', 'deskripsi', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('KomponenId' => 0, 'Satuan' => 1, 'KomponenName' => 2, 'ShsdId' => 3, 'KomponenHarga' => 4, 'KomponenShow' => 5, 'IpAddress' => 6, 'WaktuAccess' => 7, 'KomponenTipe' => 8, 'KomponenConfirmed' => 9, 'KomponenNonPajak' => 10, 'UserId' => 11, 'Rekening' => 12, 'Kelompok' => 13, 'Pemeliharaan' => 14, 'RekUpah' => 15, 'RekBahan' => 16, 'RekSewa' => 17, 'Deskripsi' => 18, ),
		BasePeer::TYPE_COLNAME => array (KomponenFisikPeer::KOMPONEN_ID => 0, KomponenFisikPeer::SATUAN => 1, KomponenFisikPeer::KOMPONEN_NAME => 2, KomponenFisikPeer::SHSD_ID => 3, KomponenFisikPeer::KOMPONEN_HARGA => 4, KomponenFisikPeer::KOMPONEN_SHOW => 5, KomponenFisikPeer::IP_ADDRESS => 6, KomponenFisikPeer::WAKTU_ACCESS => 7, KomponenFisikPeer::KOMPONEN_TIPE => 8, KomponenFisikPeer::KOMPONEN_CONFIRMED => 9, KomponenFisikPeer::KOMPONEN_NON_PAJAK => 10, KomponenFisikPeer::USER_ID => 11, KomponenFisikPeer::REKENING => 12, KomponenFisikPeer::KELOMPOK => 13, KomponenFisikPeer::PEMELIHARAAN => 14, KomponenFisikPeer::REK_UPAH => 15, KomponenFisikPeer::REK_BAHAN => 16, KomponenFisikPeer::REK_SEWA => 17, KomponenFisikPeer::DESKRIPSI => 18, ),
		BasePeer::TYPE_FIELDNAME => array ('komponen_id' => 0, 'satuan' => 1, 'komponen_name' => 2, 'shsd_id' => 3, 'komponen_harga' => 4, 'komponen_show' => 5, 'ip_address' => 6, 'waktu_access' => 7, 'komponen_tipe' => 8, 'komponen_confirmed' => 9, 'komponen_non_pajak' => 10, 'user_id' => 11, 'rekening' => 12, 'kelompok' => 13, 'pemeliharaan' => 14, 'rek_upah' => 15, 'rek_bahan' => 16, 'rek_sewa' => 17, 'deskripsi' => 18, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/KomponenFisikMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.KomponenFisikMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = KomponenFisikPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(KomponenFisikPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(KomponenFisikPeer::KOMPONEN_ID);

		$criteria->addSelectColumn(KomponenFisikPeer::SATUAN);

		$criteria->addSelectColumn(KomponenFisikPeer::KOMPONEN_NAME);

		$criteria->addSelectColumn(KomponenFisikPeer::SHSD_ID);

		$criteria->addSelectColumn(KomponenFisikPeer::KOMPONEN_HARGA);

		$criteria->addSelectColumn(KomponenFisikPeer::KOMPONEN_SHOW);

		$criteria->addSelectColumn(KomponenFisikPeer::IP_ADDRESS);

		$criteria->addSelectColumn(KomponenFisikPeer::WAKTU_ACCESS);

		$criteria->addSelectColumn(KomponenFisikPeer::KOMPONEN_TIPE);

		$criteria->addSelectColumn(KomponenFisikPeer::KOMPONEN_CONFIRMED);

		$criteria->addSelectColumn(KomponenFisikPeer::KOMPONEN_NON_PAJAK);

		$criteria->addSelectColumn(KomponenFisikPeer::USER_ID);

		$criteria->addSelectColumn(KomponenFisikPeer::REKENING);

		$criteria->addSelectColumn(KomponenFisikPeer::KELOMPOK);

		$criteria->addSelectColumn(KomponenFisikPeer::PEMELIHARAAN);

		$criteria->addSelectColumn(KomponenFisikPeer::REK_UPAH);

		$criteria->addSelectColumn(KomponenFisikPeer::REK_BAHAN);

		$criteria->addSelectColumn(KomponenFisikPeer::REK_SEWA);

		$criteria->addSelectColumn(KomponenFisikPeer::DESKRIPSI);

	}

	const COUNT = 'COUNT(*)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT *)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(KomponenFisikPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(KomponenFisikPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = KomponenFisikPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = KomponenFisikPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return KomponenFisikPeer::populateObjects(KomponenFisikPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			KomponenFisikPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = KomponenFisikPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return KomponenFisikPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(KomponenFisikPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(KomponenFisikPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof KomponenFisik) {

			$criteria = $values->buildCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
												if(count($values) == count($values, COUNT_RECURSIVE))
			{
								$values = array($values);
			}
			$vals = array();
			foreach($values as $value)
			{

			}

		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(KomponenFisik $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(KomponenFisikPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(KomponenFisikPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(KomponenFisikPeer::DATABASE_NAME, KomponenFisikPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = KomponenFisikPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

} 
if (Propel::isInit()) {
			try {
		BaseKomponenFisikPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/KomponenFisikMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.KomponenFisikMapBuilder');
}
