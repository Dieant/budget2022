<?php


abstract class BasePersonilRka extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $nip;


	
	protected $nama;


	
	protected $unit_id;


	
	protected $kegiatan_code;


	
	protected $subtitle;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getNip()
	{

		return $this->nip;
	}

	
	public function getNama()
	{

		return $this->nama;
	}

	
	public function getUnitId()
	{

		return $this->unit_id;
	}

	
	public function getKegiatanCode()
	{

		return $this->kegiatan_code;
	}

	
	public function getSubtitle()
	{

		return $this->subtitle;
	}

	
	public function setNip($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nip !== $v) {
			$this->nip = $v;
			$this->modifiedColumns[] = PersonilRkaPeer::NIP;
		}

	} 
	
	public function setNama($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nama !== $v) {
			$this->nama = $v;
			$this->modifiedColumns[] = PersonilRkaPeer::NAMA;
		}

	} 
	
	public function setUnitId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->unit_id !== $v) {
			$this->unit_id = $v;
			$this->modifiedColumns[] = PersonilRkaPeer::UNIT_ID;
		}

	} 
	
	public function setKegiatanCode($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kegiatan_code !== $v) {
			$this->kegiatan_code = $v;
			$this->modifiedColumns[] = PersonilRkaPeer::KEGIATAN_CODE;
		}

	} 
	
	public function setSubtitle($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->subtitle !== $v) {
			$this->subtitle = $v;
			$this->modifiedColumns[] = PersonilRkaPeer::SUBTITLE;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->nip = $rs->getString($startcol + 0);

			$this->nama = $rs->getString($startcol + 1);

			$this->unit_id = $rs->getString($startcol + 2);

			$this->kegiatan_code = $rs->getString($startcol + 3);

			$this->subtitle = $rs->getString($startcol + 4);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 5; 
		} catch (Exception $e) {
			throw new PropelException("Error populating PersonilRka object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(PersonilRkaPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			PersonilRkaPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(PersonilRkaPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = PersonilRkaPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += PersonilRkaPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = PersonilRkaPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = PersonilRkaPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getNip();
				break;
			case 1:
				return $this->getNama();
				break;
			case 2:
				return $this->getUnitId();
				break;
			case 3:
				return $this->getKegiatanCode();
				break;
			case 4:
				return $this->getSubtitle();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = PersonilRkaPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getNip(),
			$keys[1] => $this->getNama(),
			$keys[2] => $this->getUnitId(),
			$keys[3] => $this->getKegiatanCode(),
			$keys[4] => $this->getSubtitle(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = PersonilRkaPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setNip($value);
				break;
			case 1:
				$this->setNama($value);
				break;
			case 2:
				$this->setUnitId($value);
				break;
			case 3:
				$this->setKegiatanCode($value);
				break;
			case 4:
				$this->setSubtitle($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = PersonilRkaPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setNip($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setNama($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setUnitId($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setKegiatanCode($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setSubtitle($arr[$keys[4]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(PersonilRkaPeer::DATABASE_NAME);

		if ($this->isColumnModified(PersonilRkaPeer::NIP)) $criteria->add(PersonilRkaPeer::NIP, $this->nip);
		if ($this->isColumnModified(PersonilRkaPeer::NAMA)) $criteria->add(PersonilRkaPeer::NAMA, $this->nama);
		if ($this->isColumnModified(PersonilRkaPeer::UNIT_ID)) $criteria->add(PersonilRkaPeer::UNIT_ID, $this->unit_id);
		if ($this->isColumnModified(PersonilRkaPeer::KEGIATAN_CODE)) $criteria->add(PersonilRkaPeer::KEGIATAN_CODE, $this->kegiatan_code);
		if ($this->isColumnModified(PersonilRkaPeer::SUBTITLE)) $criteria->add(PersonilRkaPeer::SUBTITLE, $this->subtitle);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(PersonilRkaPeer::DATABASE_NAME);


		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return null;
	}

	
	 public function setPrimaryKey($pk)
	 {
		 	 }

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setNip($this->nip);

		$copyObj->setNama($this->nama);

		$copyObj->setUnitId($this->unit_id);

		$copyObj->setKegiatanCode($this->kegiatan_code);

		$copyObj->setSubtitle($this->subtitle);


		$copyObj->setNew(true);

	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new PersonilRkaPeer();
		}
		return self::$peer;
	}

} 