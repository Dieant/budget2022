<?php


abstract class BaseDeskripsiResumeRapatKegiatan extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id_deskripsi_resume_rapat;


	
	protected $kegiatan_code;


	
	protected $kegiatan_name;


	
	protected $catatan_pembahasan;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getIdDeskripsiResumeRapat()
	{

		return $this->id_deskripsi_resume_rapat;
	}

	
	public function getKegiatanCode()
	{

		return $this->kegiatan_code;
	}

	
	public function getKegiatanName()
	{

		return $this->kegiatan_name;
	}

	
	public function getCatatanPembahasan()
	{

		return $this->catatan_pembahasan;
	}

	
	public function setIdDeskripsiResumeRapat($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id_deskripsi_resume_rapat !== $v) {
			$this->id_deskripsi_resume_rapat = $v;
			$this->modifiedColumns[] = DeskripsiResumeRapatKegiatanPeer::ID_DESKRIPSI_RESUME_RAPAT;
		}

	} 
	
	public function setKegiatanCode($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kegiatan_code !== $v) {
			$this->kegiatan_code = $v;
			$this->modifiedColumns[] = DeskripsiResumeRapatKegiatanPeer::KEGIATAN_CODE;
		}

	} 
	
	public function setKegiatanName($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kegiatan_name !== $v) {
			$this->kegiatan_name = $v;
			$this->modifiedColumns[] = DeskripsiResumeRapatKegiatanPeer::KEGIATAN_NAME;
		}

	} 
	
	public function setCatatanPembahasan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->catatan_pembahasan !== $v) {
			$this->catatan_pembahasan = $v;
			$this->modifiedColumns[] = DeskripsiResumeRapatKegiatanPeer::CATATAN_PEMBAHASAN;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id_deskripsi_resume_rapat = $rs->getInt($startcol + 0);

			$this->kegiatan_code = $rs->getString($startcol + 1);

			$this->kegiatan_name = $rs->getString($startcol + 2);

			$this->catatan_pembahasan = $rs->getString($startcol + 3);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 4; 
		} catch (Exception $e) {
			throw new PropelException("Error populating DeskripsiResumeRapatKegiatan object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(DeskripsiResumeRapatKegiatanPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			DeskripsiResumeRapatKegiatanPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(DeskripsiResumeRapatKegiatanPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = DeskripsiResumeRapatKegiatanPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += DeskripsiResumeRapatKegiatanPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = DeskripsiResumeRapatKegiatanPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = DeskripsiResumeRapatKegiatanPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getIdDeskripsiResumeRapat();
				break;
			case 1:
				return $this->getKegiatanCode();
				break;
			case 2:
				return $this->getKegiatanName();
				break;
			case 3:
				return $this->getCatatanPembahasan();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = DeskripsiResumeRapatKegiatanPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getIdDeskripsiResumeRapat(),
			$keys[1] => $this->getKegiatanCode(),
			$keys[2] => $this->getKegiatanName(),
			$keys[3] => $this->getCatatanPembahasan(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = DeskripsiResumeRapatKegiatanPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setIdDeskripsiResumeRapat($value);
				break;
			case 1:
				$this->setKegiatanCode($value);
				break;
			case 2:
				$this->setKegiatanName($value);
				break;
			case 3:
				$this->setCatatanPembahasan($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = DeskripsiResumeRapatKegiatanPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setIdDeskripsiResumeRapat($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setKegiatanCode($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setKegiatanName($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setCatatanPembahasan($arr[$keys[3]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(DeskripsiResumeRapatKegiatanPeer::DATABASE_NAME);

		if ($this->isColumnModified(DeskripsiResumeRapatKegiatanPeer::ID_DESKRIPSI_RESUME_RAPAT)) $criteria->add(DeskripsiResumeRapatKegiatanPeer::ID_DESKRIPSI_RESUME_RAPAT, $this->id_deskripsi_resume_rapat);
		if ($this->isColumnModified(DeskripsiResumeRapatKegiatanPeer::KEGIATAN_CODE)) $criteria->add(DeskripsiResumeRapatKegiatanPeer::KEGIATAN_CODE, $this->kegiatan_code);
		if ($this->isColumnModified(DeskripsiResumeRapatKegiatanPeer::KEGIATAN_NAME)) $criteria->add(DeskripsiResumeRapatKegiatanPeer::KEGIATAN_NAME, $this->kegiatan_name);
		if ($this->isColumnModified(DeskripsiResumeRapatKegiatanPeer::CATATAN_PEMBAHASAN)) $criteria->add(DeskripsiResumeRapatKegiatanPeer::CATATAN_PEMBAHASAN, $this->catatan_pembahasan);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(DeskripsiResumeRapatKegiatanPeer::DATABASE_NAME);

		$criteria->add(DeskripsiResumeRapatKegiatanPeer::ID_DESKRIPSI_RESUME_RAPAT, $this->id_deskripsi_resume_rapat);
		$criteria->add(DeskripsiResumeRapatKegiatanPeer::KEGIATAN_CODE, $this->kegiatan_code);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		$pks = array();

		$pks[0] = $this->getIdDeskripsiResumeRapat();

		$pks[1] = $this->getKegiatanCode();

		return $pks;
	}

	
	public function setPrimaryKey($keys)
	{

		$this->setIdDeskripsiResumeRapat($keys[0]);

		$this->setKegiatanCode($keys[1]);

	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setKegiatanName($this->kegiatan_name);

		$copyObj->setCatatanPembahasan($this->catatan_pembahasan);


		$copyObj->setNew(true);

		$copyObj->setIdDeskripsiResumeRapat(NULL); 
		$copyObj->setKegiatanCode(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new DeskripsiResumeRapatKegiatanPeer();
		}
		return self::$peer;
	}

} 