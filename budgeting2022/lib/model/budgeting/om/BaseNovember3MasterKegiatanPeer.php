<?php


abstract class BaseNovember3MasterKegiatanPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.november3_master_kegiatan';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.November3MasterKegiatan';

	
	const NUM_COLUMNS = 47;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const UNIT_ID = 'ebudget.november3_master_kegiatan.UNIT_ID';

	
	const KODE_KEGIATAN = 'ebudget.november3_master_kegiatan.KODE_KEGIATAN';

	
	const KODE_BIDANG = 'ebudget.november3_master_kegiatan.KODE_BIDANG';

	
	const KODE_URUSAN_WAJIB = 'ebudget.november3_master_kegiatan.KODE_URUSAN_WAJIB';

	
	const KODE_PROGRAM = 'ebudget.november3_master_kegiatan.KODE_PROGRAM';

	
	const KODE_SASARAN = 'ebudget.november3_master_kegiatan.KODE_SASARAN';

	
	const KODE_INDIKATOR = 'ebudget.november3_master_kegiatan.KODE_INDIKATOR';

	
	const ALOKASI_DANA = 'ebudget.november3_master_kegiatan.ALOKASI_DANA';

	
	const NAMA_KEGIATAN = 'ebudget.november3_master_kegiatan.NAMA_KEGIATAN';

	
	const MASUKAN = 'ebudget.november3_master_kegiatan.MASUKAN';

	
	const OUTPUT = 'ebudget.november3_master_kegiatan.OUTPUT';

	
	const OUTCOME = 'ebudget.november3_master_kegiatan.OUTCOME';

	
	const BENEFIT = 'ebudget.november3_master_kegiatan.BENEFIT';

	
	const IMPACT = 'ebudget.november3_master_kegiatan.IMPACT';

	
	const TIPE = 'ebudget.november3_master_kegiatan.TIPE';

	
	const KEGIATAN_ACTIVE = 'ebudget.november3_master_kegiatan.KEGIATAN_ACTIVE';

	
	const TO_KEGIATAN_CODE = 'ebudget.november3_master_kegiatan.TO_KEGIATAN_CODE';

	
	const CATATAN = 'ebudget.november3_master_kegiatan.CATATAN';

	
	const TARGET_OUTCOME = 'ebudget.november3_master_kegiatan.TARGET_OUTCOME';

	
	const LOKASI = 'ebudget.november3_master_kegiatan.LOKASI';

	
	const JUMLAH_PREV = 'ebudget.november3_master_kegiatan.JUMLAH_PREV';

	
	const JUMLAH_NOW = 'ebudget.november3_master_kegiatan.JUMLAH_NOW';

	
	const JUMLAH_NEXT = 'ebudget.november3_master_kegiatan.JUMLAH_NEXT';

	
	const KODE_PROGRAM2 = 'ebudget.november3_master_kegiatan.KODE_PROGRAM2';

	
	const KODE_URUSAN = 'ebudget.november3_master_kegiatan.KODE_URUSAN';

	
	const LAST_UPDATE_USER = 'ebudget.november3_master_kegiatan.LAST_UPDATE_USER';

	
	const LAST_UPDATE_TIME = 'ebudget.november3_master_kegiatan.LAST_UPDATE_TIME';

	
	const LAST_UPDATE_IP = 'ebudget.november3_master_kegiatan.LAST_UPDATE_IP';

	
	const TAHAP = 'ebudget.november3_master_kegiatan.TAHAP';

	
	const KODE_MISI = 'ebudget.november3_master_kegiatan.KODE_MISI';

	
	const KODE_TUJUAN = 'ebudget.november3_master_kegiatan.KODE_TUJUAN';

	
	const RANKING = 'ebudget.november3_master_kegiatan.RANKING';

	
	const NOMOR13 = 'ebudget.november3_master_kegiatan.NOMOR13';

	
	const PPA_NAMA = 'ebudget.november3_master_kegiatan.PPA_NAMA';

	
	const PPA_PANGKAT = 'ebudget.november3_master_kegiatan.PPA_PANGKAT';

	
	const PPA_NIP = 'ebudget.november3_master_kegiatan.PPA_NIP';

	
	const LANJUTAN = 'ebudget.november3_master_kegiatan.LANJUTAN';

	
	const USER_ID = 'ebudget.november3_master_kegiatan.USER_ID';

	
	const ID = 'ebudget.november3_master_kegiatan.ID';

	
	const TAHUN = 'ebudget.november3_master_kegiatan.TAHUN';

	
	const TAMBAHAN_PAGU = 'ebudget.november3_master_kegiatan.TAMBAHAN_PAGU';

	
	const GENDER = 'ebudget.november3_master_kegiatan.GENDER';

	
	const KODE_KEG_KEUANGAN = 'ebudget.november3_master_kegiatan.KODE_KEG_KEUANGAN';

	
	const CATATAN_BAGIAN_HUKUM = 'ebudget.november3_master_kegiatan.CATATAN_BAGIAN_HUKUM';

	
	const CATATAN_INSPEKTORAT = 'ebudget.november3_master_kegiatan.CATATAN_INSPEKTORAT';

	
	const CATATAN_BADAN_KEPEGAWAIAN = 'ebudget.november3_master_kegiatan.CATATAN_BADAN_KEPEGAWAIAN';

	
	const CATATAN_LPPA = 'ebudget.november3_master_kegiatan.CATATAN_LPPA';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('UnitId', 'KodeKegiatan', 'KodeBidang', 'KodeUrusanWajib', 'KodeProgram', 'KodeSasaran', 'KodeIndikator', 'AlokasiDana', 'NamaKegiatan', 'Masukan', 'Output', 'Outcome', 'Benefit', 'Impact', 'Tipe', 'KegiatanActive', 'ToKegiatanCode', 'Catatan', 'TargetOutcome', 'Lokasi', 'JumlahPrev', 'JumlahNow', 'JumlahNext', 'KodeProgram2', 'KodeUrusan', 'LastUpdateUser', 'LastUpdateTime', 'LastUpdateIp', 'Tahap', 'KodeMisi', 'KodeTujuan', 'Ranking', 'Nomor13', 'PpaNama', 'PpaPangkat', 'PpaNip', 'Lanjutan', 'UserId', 'Id', 'Tahun', 'TambahanPagu', 'Gender', 'KodeKegKeuangan', 'CatatanBagianHukum', 'CatatanInspektorat', 'CatatanBadanKepegawaian', 'CatatanLppa', ),
		BasePeer::TYPE_COLNAME => array (November3MasterKegiatanPeer::UNIT_ID, November3MasterKegiatanPeer::KODE_KEGIATAN, November3MasterKegiatanPeer::KODE_BIDANG, November3MasterKegiatanPeer::KODE_URUSAN_WAJIB, November3MasterKegiatanPeer::KODE_PROGRAM, November3MasterKegiatanPeer::KODE_SASARAN, November3MasterKegiatanPeer::KODE_INDIKATOR, November3MasterKegiatanPeer::ALOKASI_DANA, November3MasterKegiatanPeer::NAMA_KEGIATAN, November3MasterKegiatanPeer::MASUKAN, November3MasterKegiatanPeer::OUTPUT, November3MasterKegiatanPeer::OUTCOME, November3MasterKegiatanPeer::BENEFIT, November3MasterKegiatanPeer::IMPACT, November3MasterKegiatanPeer::TIPE, November3MasterKegiatanPeer::KEGIATAN_ACTIVE, November3MasterKegiatanPeer::TO_KEGIATAN_CODE, November3MasterKegiatanPeer::CATATAN, November3MasterKegiatanPeer::TARGET_OUTCOME, November3MasterKegiatanPeer::LOKASI, November3MasterKegiatanPeer::JUMLAH_PREV, November3MasterKegiatanPeer::JUMLAH_NOW, November3MasterKegiatanPeer::JUMLAH_NEXT, November3MasterKegiatanPeer::KODE_PROGRAM2, November3MasterKegiatanPeer::KODE_URUSAN, November3MasterKegiatanPeer::LAST_UPDATE_USER, November3MasterKegiatanPeer::LAST_UPDATE_TIME, November3MasterKegiatanPeer::LAST_UPDATE_IP, November3MasterKegiatanPeer::TAHAP, November3MasterKegiatanPeer::KODE_MISI, November3MasterKegiatanPeer::KODE_TUJUAN, November3MasterKegiatanPeer::RANKING, November3MasterKegiatanPeer::NOMOR13, November3MasterKegiatanPeer::PPA_NAMA, November3MasterKegiatanPeer::PPA_PANGKAT, November3MasterKegiatanPeer::PPA_NIP, November3MasterKegiatanPeer::LANJUTAN, November3MasterKegiatanPeer::USER_ID, November3MasterKegiatanPeer::ID, November3MasterKegiatanPeer::TAHUN, November3MasterKegiatanPeer::TAMBAHAN_PAGU, November3MasterKegiatanPeer::GENDER, November3MasterKegiatanPeer::KODE_KEG_KEUANGAN, November3MasterKegiatanPeer::CATATAN_BAGIAN_HUKUM, November3MasterKegiatanPeer::CATATAN_INSPEKTORAT, November3MasterKegiatanPeer::CATATAN_BADAN_KEPEGAWAIAN, November3MasterKegiatanPeer::CATATAN_LPPA, ),
		BasePeer::TYPE_FIELDNAME => array ('unit_id', 'kode_kegiatan', 'kode_bidang', 'kode_urusan_wajib', 'kode_program', 'kode_sasaran', 'kode_indikator', 'alokasi_dana', 'nama_kegiatan', 'masukan', 'output', 'outcome', 'benefit', 'impact', 'tipe', 'kegiatan_active', 'to_kegiatan_code', 'catatan', 'target_outcome', 'lokasi', 'jumlah_prev', 'jumlah_now', 'jumlah_next', 'kode_program2', 'kode_urusan', 'last_update_user', 'last_update_time', 'last_update_ip', 'tahap', 'kode_misi', 'kode_tujuan', 'ranking', 'nomor13', 'ppa_nama', 'ppa_pangkat', 'ppa_nip', 'lanjutan', 'user_id', 'id', 'tahun', 'tambahan_pagu', 'gender', 'kode_keg_keuangan', 'catatan_bagian_hukum', 'catatan_inspektorat', 'catatan_badan_kepegawaian', 'catatan_lppa', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('UnitId' => 0, 'KodeKegiatan' => 1, 'KodeBidang' => 2, 'KodeUrusanWajib' => 3, 'KodeProgram' => 4, 'KodeSasaran' => 5, 'KodeIndikator' => 6, 'AlokasiDana' => 7, 'NamaKegiatan' => 8, 'Masukan' => 9, 'Output' => 10, 'Outcome' => 11, 'Benefit' => 12, 'Impact' => 13, 'Tipe' => 14, 'KegiatanActive' => 15, 'ToKegiatanCode' => 16, 'Catatan' => 17, 'TargetOutcome' => 18, 'Lokasi' => 19, 'JumlahPrev' => 20, 'JumlahNow' => 21, 'JumlahNext' => 22, 'KodeProgram2' => 23, 'KodeUrusan' => 24, 'LastUpdateUser' => 25, 'LastUpdateTime' => 26, 'LastUpdateIp' => 27, 'Tahap' => 28, 'KodeMisi' => 29, 'KodeTujuan' => 30, 'Ranking' => 31, 'Nomor13' => 32, 'PpaNama' => 33, 'PpaPangkat' => 34, 'PpaNip' => 35, 'Lanjutan' => 36, 'UserId' => 37, 'Id' => 38, 'Tahun' => 39, 'TambahanPagu' => 40, 'Gender' => 41, 'KodeKegKeuangan' => 42, 'CatatanBagianHukum' => 43, 'CatatanInspektorat' => 44, 'CatatanBadanKepegawaian' => 45, 'CatatanLppa' => 46, ),
		BasePeer::TYPE_COLNAME => array (November3MasterKegiatanPeer::UNIT_ID => 0, November3MasterKegiatanPeer::KODE_KEGIATAN => 1, November3MasterKegiatanPeer::KODE_BIDANG => 2, November3MasterKegiatanPeer::KODE_URUSAN_WAJIB => 3, November3MasterKegiatanPeer::KODE_PROGRAM => 4, November3MasterKegiatanPeer::KODE_SASARAN => 5, November3MasterKegiatanPeer::KODE_INDIKATOR => 6, November3MasterKegiatanPeer::ALOKASI_DANA => 7, November3MasterKegiatanPeer::NAMA_KEGIATAN => 8, November3MasterKegiatanPeer::MASUKAN => 9, November3MasterKegiatanPeer::OUTPUT => 10, November3MasterKegiatanPeer::OUTCOME => 11, November3MasterKegiatanPeer::BENEFIT => 12, November3MasterKegiatanPeer::IMPACT => 13, November3MasterKegiatanPeer::TIPE => 14, November3MasterKegiatanPeer::KEGIATAN_ACTIVE => 15, November3MasterKegiatanPeer::TO_KEGIATAN_CODE => 16, November3MasterKegiatanPeer::CATATAN => 17, November3MasterKegiatanPeer::TARGET_OUTCOME => 18, November3MasterKegiatanPeer::LOKASI => 19, November3MasterKegiatanPeer::JUMLAH_PREV => 20, November3MasterKegiatanPeer::JUMLAH_NOW => 21, November3MasterKegiatanPeer::JUMLAH_NEXT => 22, November3MasterKegiatanPeer::KODE_PROGRAM2 => 23, November3MasterKegiatanPeer::KODE_URUSAN => 24, November3MasterKegiatanPeer::LAST_UPDATE_USER => 25, November3MasterKegiatanPeer::LAST_UPDATE_TIME => 26, November3MasterKegiatanPeer::LAST_UPDATE_IP => 27, November3MasterKegiatanPeer::TAHAP => 28, November3MasterKegiatanPeer::KODE_MISI => 29, November3MasterKegiatanPeer::KODE_TUJUAN => 30, November3MasterKegiatanPeer::RANKING => 31, November3MasterKegiatanPeer::NOMOR13 => 32, November3MasterKegiatanPeer::PPA_NAMA => 33, November3MasterKegiatanPeer::PPA_PANGKAT => 34, November3MasterKegiatanPeer::PPA_NIP => 35, November3MasterKegiatanPeer::LANJUTAN => 36, November3MasterKegiatanPeer::USER_ID => 37, November3MasterKegiatanPeer::ID => 38, November3MasterKegiatanPeer::TAHUN => 39, November3MasterKegiatanPeer::TAMBAHAN_PAGU => 40, November3MasterKegiatanPeer::GENDER => 41, November3MasterKegiatanPeer::KODE_KEG_KEUANGAN => 42, November3MasterKegiatanPeer::CATATAN_BAGIAN_HUKUM => 43, November3MasterKegiatanPeer::CATATAN_INSPEKTORAT => 44, November3MasterKegiatanPeer::CATATAN_BADAN_KEPEGAWAIAN => 45, November3MasterKegiatanPeer::CATATAN_LPPA => 46, ),
		BasePeer::TYPE_FIELDNAME => array ('unit_id' => 0, 'kode_kegiatan' => 1, 'kode_bidang' => 2, 'kode_urusan_wajib' => 3, 'kode_program' => 4, 'kode_sasaran' => 5, 'kode_indikator' => 6, 'alokasi_dana' => 7, 'nama_kegiatan' => 8, 'masukan' => 9, 'output' => 10, 'outcome' => 11, 'benefit' => 12, 'impact' => 13, 'tipe' => 14, 'kegiatan_active' => 15, 'to_kegiatan_code' => 16, 'catatan' => 17, 'target_outcome' => 18, 'lokasi' => 19, 'jumlah_prev' => 20, 'jumlah_now' => 21, 'jumlah_next' => 22, 'kode_program2' => 23, 'kode_urusan' => 24, 'last_update_user' => 25, 'last_update_time' => 26, 'last_update_ip' => 27, 'tahap' => 28, 'kode_misi' => 29, 'kode_tujuan' => 30, 'ranking' => 31, 'nomor13' => 32, 'ppa_nama' => 33, 'ppa_pangkat' => 34, 'ppa_nip' => 35, 'lanjutan' => 36, 'user_id' => 37, 'id' => 38, 'tahun' => 39, 'tambahan_pagu' => 40, 'gender' => 41, 'kode_keg_keuangan' => 42, 'catatan_bagian_hukum' => 43, 'catatan_inspektorat' => 44, 'catatan_badan_kepegawaian' => 45, 'catatan_lppa' => 46, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/November3MasterKegiatanMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.November3MasterKegiatanMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = November3MasterKegiatanPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(November3MasterKegiatanPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(November3MasterKegiatanPeer::UNIT_ID);

		$criteria->addSelectColumn(November3MasterKegiatanPeer::KODE_KEGIATAN);

		$criteria->addSelectColumn(November3MasterKegiatanPeer::KODE_BIDANG);

		$criteria->addSelectColumn(November3MasterKegiatanPeer::KODE_URUSAN_WAJIB);

		$criteria->addSelectColumn(November3MasterKegiatanPeer::KODE_PROGRAM);

		$criteria->addSelectColumn(November3MasterKegiatanPeer::KODE_SASARAN);

		$criteria->addSelectColumn(November3MasterKegiatanPeer::KODE_INDIKATOR);

		$criteria->addSelectColumn(November3MasterKegiatanPeer::ALOKASI_DANA);

		$criteria->addSelectColumn(November3MasterKegiatanPeer::NAMA_KEGIATAN);

		$criteria->addSelectColumn(November3MasterKegiatanPeer::MASUKAN);

		$criteria->addSelectColumn(November3MasterKegiatanPeer::OUTPUT);

		$criteria->addSelectColumn(November3MasterKegiatanPeer::OUTCOME);

		$criteria->addSelectColumn(November3MasterKegiatanPeer::BENEFIT);

		$criteria->addSelectColumn(November3MasterKegiatanPeer::IMPACT);

		$criteria->addSelectColumn(November3MasterKegiatanPeer::TIPE);

		$criteria->addSelectColumn(November3MasterKegiatanPeer::KEGIATAN_ACTIVE);

		$criteria->addSelectColumn(November3MasterKegiatanPeer::TO_KEGIATAN_CODE);

		$criteria->addSelectColumn(November3MasterKegiatanPeer::CATATAN);

		$criteria->addSelectColumn(November3MasterKegiatanPeer::TARGET_OUTCOME);

		$criteria->addSelectColumn(November3MasterKegiatanPeer::LOKASI);

		$criteria->addSelectColumn(November3MasterKegiatanPeer::JUMLAH_PREV);

		$criteria->addSelectColumn(November3MasterKegiatanPeer::JUMLAH_NOW);

		$criteria->addSelectColumn(November3MasterKegiatanPeer::JUMLAH_NEXT);

		$criteria->addSelectColumn(November3MasterKegiatanPeer::KODE_PROGRAM2);

		$criteria->addSelectColumn(November3MasterKegiatanPeer::KODE_URUSAN);

		$criteria->addSelectColumn(November3MasterKegiatanPeer::LAST_UPDATE_USER);

		$criteria->addSelectColumn(November3MasterKegiatanPeer::LAST_UPDATE_TIME);

		$criteria->addSelectColumn(November3MasterKegiatanPeer::LAST_UPDATE_IP);

		$criteria->addSelectColumn(November3MasterKegiatanPeer::TAHAP);

		$criteria->addSelectColumn(November3MasterKegiatanPeer::KODE_MISI);

		$criteria->addSelectColumn(November3MasterKegiatanPeer::KODE_TUJUAN);

		$criteria->addSelectColumn(November3MasterKegiatanPeer::RANKING);

		$criteria->addSelectColumn(November3MasterKegiatanPeer::NOMOR13);

		$criteria->addSelectColumn(November3MasterKegiatanPeer::PPA_NAMA);

		$criteria->addSelectColumn(November3MasterKegiatanPeer::PPA_PANGKAT);

		$criteria->addSelectColumn(November3MasterKegiatanPeer::PPA_NIP);

		$criteria->addSelectColumn(November3MasterKegiatanPeer::LANJUTAN);

		$criteria->addSelectColumn(November3MasterKegiatanPeer::USER_ID);

		$criteria->addSelectColumn(November3MasterKegiatanPeer::ID);

		$criteria->addSelectColumn(November3MasterKegiatanPeer::TAHUN);

		$criteria->addSelectColumn(November3MasterKegiatanPeer::TAMBAHAN_PAGU);

		$criteria->addSelectColumn(November3MasterKegiatanPeer::GENDER);

		$criteria->addSelectColumn(November3MasterKegiatanPeer::KODE_KEG_KEUANGAN);

		$criteria->addSelectColumn(November3MasterKegiatanPeer::CATATAN_BAGIAN_HUKUM);

		$criteria->addSelectColumn(November3MasterKegiatanPeer::CATATAN_INSPEKTORAT);

		$criteria->addSelectColumn(November3MasterKegiatanPeer::CATATAN_BADAN_KEPEGAWAIAN);

		$criteria->addSelectColumn(November3MasterKegiatanPeer::CATATAN_LPPA);

	}

	const COUNT = 'COUNT(ebudget.november3_master_kegiatan.UNIT_ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.november3_master_kegiatan.UNIT_ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(November3MasterKegiatanPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(November3MasterKegiatanPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = November3MasterKegiatanPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = November3MasterKegiatanPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return November3MasterKegiatanPeer::populateObjects(November3MasterKegiatanPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			November3MasterKegiatanPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = November3MasterKegiatanPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return November3MasterKegiatanPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}

		$criteria->remove(November3MasterKegiatanPeer::ID); 

				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(November3MasterKegiatanPeer::UNIT_ID);
			$selectCriteria->add(November3MasterKegiatanPeer::UNIT_ID, $criteria->remove(November3MasterKegiatanPeer::UNIT_ID), $comparison);

			$comparison = $criteria->getComparison(November3MasterKegiatanPeer::KODE_KEGIATAN);
			$selectCriteria->add(November3MasterKegiatanPeer::KODE_KEGIATAN, $criteria->remove(November3MasterKegiatanPeer::KODE_KEGIATAN), $comparison);

			$comparison = $criteria->getComparison(November3MasterKegiatanPeer::ID);
			$selectCriteria->add(November3MasterKegiatanPeer::ID, $criteria->remove(November3MasterKegiatanPeer::ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(November3MasterKegiatanPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(November3MasterKegiatanPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof November3MasterKegiatan) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
												if(count($values) == count($values, COUNT_RECURSIVE))
			{
								$values = array($values);
			}
			$vals = array();
			foreach($values as $value)
			{

				$vals[0][] = $value[0];
				$vals[1][] = $value[1];
				$vals[2][] = $value[2];
			}

			$criteria->add(November3MasterKegiatanPeer::UNIT_ID, $vals[0], Criteria::IN);
			$criteria->add(November3MasterKegiatanPeer::KODE_KEGIATAN, $vals[1], Criteria::IN);
			$criteria->add(November3MasterKegiatanPeer::ID, $vals[2], Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(November3MasterKegiatan $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(November3MasterKegiatanPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(November3MasterKegiatanPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(November3MasterKegiatanPeer::DATABASE_NAME, November3MasterKegiatanPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = November3MasterKegiatanPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK( $unit_id, $kode_kegiatan, $id, $con = null) {
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$criteria = new Criteria();
		$criteria->add(November3MasterKegiatanPeer::UNIT_ID, $unit_id);
		$criteria->add(November3MasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
		$criteria->add(November3MasterKegiatanPeer::ID, $id);
		$v = November3MasterKegiatanPeer::doSelect($criteria, $con);

		return !empty($v) ? $v[0] : null;
	}
} 
if (Propel::isInit()) {
			try {
		BaseNovember3MasterKegiatanPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/November3MasterKegiatanMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.November3MasterKegiatanMapBuilder');
}
