<?php


abstract class BaseMasterProgramPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.master_program';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.MasterProgram';

	
	const NUM_COLUMNS = 6;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const KODE_PROGRAM = 'ebudget.master_program.KODE_PROGRAM';

	
	const NAMA_PROGRAM = 'ebudget.master_program.NAMA_PROGRAM';

	
	const BAPPEKO = 'ebudget.master_program.BAPPEKO';

	
	const KODE_TUJUAN = 'ebudget.master_program.KODE_TUJUAN';

	
	const KODE_LUTFI = 'ebudget.master_program.KODE_LUTFI';

	
	const INDIKATOR = 'ebudget.master_program.INDIKATOR';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('KodeProgram', 'NamaProgram', 'Bappeko', 'KodeTujuan', 'KodeLutfi', 'Indikator', ),
		BasePeer::TYPE_COLNAME => array (MasterProgramPeer::KODE_PROGRAM, MasterProgramPeer::NAMA_PROGRAM, MasterProgramPeer::BAPPEKO, MasterProgramPeer::KODE_TUJUAN, MasterProgramPeer::KODE_LUTFI, MasterProgramPeer::INDIKATOR, ),
		BasePeer::TYPE_FIELDNAME => array ('kode_program', 'nama_program', 'bappeko', 'kode_tujuan', 'kode_lutfi', 'indikator', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('KodeProgram' => 0, 'NamaProgram' => 1, 'Bappeko' => 2, 'KodeTujuan' => 3, 'KodeLutfi' => 4, 'Indikator' => 5, ),
		BasePeer::TYPE_COLNAME => array (MasterProgramPeer::KODE_PROGRAM => 0, MasterProgramPeer::NAMA_PROGRAM => 1, MasterProgramPeer::BAPPEKO => 2, MasterProgramPeer::KODE_TUJUAN => 3, MasterProgramPeer::KODE_LUTFI => 4, MasterProgramPeer::INDIKATOR => 5, ),
		BasePeer::TYPE_FIELDNAME => array ('kode_program' => 0, 'nama_program' => 1, 'bappeko' => 2, 'kode_tujuan' => 3, 'kode_lutfi' => 4, 'indikator' => 5, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/MasterProgramMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.MasterProgramMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = MasterProgramPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(MasterProgramPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(MasterProgramPeer::KODE_PROGRAM);

		$criteria->addSelectColumn(MasterProgramPeer::NAMA_PROGRAM);

		$criteria->addSelectColumn(MasterProgramPeer::BAPPEKO);

		$criteria->addSelectColumn(MasterProgramPeer::KODE_TUJUAN);

		$criteria->addSelectColumn(MasterProgramPeer::KODE_LUTFI);

		$criteria->addSelectColumn(MasterProgramPeer::INDIKATOR);

	}

	const COUNT = 'COUNT(ebudget.master_program.KODE_PROGRAM)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.master_program.KODE_PROGRAM)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(MasterProgramPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(MasterProgramPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = MasterProgramPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = MasterProgramPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return MasterProgramPeer::populateObjects(MasterProgramPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			MasterProgramPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = MasterProgramPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return MasterProgramPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(MasterProgramPeer::KODE_PROGRAM);
			$selectCriteria->add(MasterProgramPeer::KODE_PROGRAM, $criteria->remove(MasterProgramPeer::KODE_PROGRAM), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(MasterProgramPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(MasterProgramPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof MasterProgram) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(MasterProgramPeer::KODE_PROGRAM, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(MasterProgram $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(MasterProgramPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(MasterProgramPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(MasterProgramPeer::DATABASE_NAME, MasterProgramPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = MasterProgramPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(MasterProgramPeer::DATABASE_NAME);

		$criteria->add(MasterProgramPeer::KODE_PROGRAM, $pk);


		$v = MasterProgramPeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(MasterProgramPeer::KODE_PROGRAM, $pks, Criteria::IN);
			$objs = MasterProgramPeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BaseMasterProgramPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/MasterProgramMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.MasterProgramMapBuilder');
}
