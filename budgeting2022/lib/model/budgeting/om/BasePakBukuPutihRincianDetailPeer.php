<?php


abstract class BasePakBukuPutihRincianDetailPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.pak_bukuputih_rincian_detail';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.PakBukuPutihRincianDetail';

	
	const NUM_COLUMNS = 70;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const KEGIATAN_CODE = 'ebudget.pak_bukuputih_rincian_detail.KEGIATAN_CODE';

	
	const TIPE = 'ebudget.pak_bukuputih_rincian_detail.TIPE';

	
	const DETAIL_NO = 'ebudget.pak_bukuputih_rincian_detail.DETAIL_NO';

	
	const REKENING_CODE = 'ebudget.pak_bukuputih_rincian_detail.REKENING_CODE';

	
	const KOMPONEN_ID = 'ebudget.pak_bukuputih_rincian_detail.KOMPONEN_ID';

	
	const DETAIL_NAME = 'ebudget.pak_bukuputih_rincian_detail.DETAIL_NAME';

	
	const VOLUME = 'ebudget.pak_bukuputih_rincian_detail.VOLUME';

	
	const KETERANGAN_KOEFISIEN = 'ebudget.pak_bukuputih_rincian_detail.KETERANGAN_KOEFISIEN';

	
	const SUBTITLE = 'ebudget.pak_bukuputih_rincian_detail.SUBTITLE';

	
	const KOMPONEN_HARGA = 'ebudget.pak_bukuputih_rincian_detail.KOMPONEN_HARGA';

	
	const KOMPONEN_HARGA_AWAL = 'ebudget.pak_bukuputih_rincian_detail.KOMPONEN_HARGA_AWAL';

	
	const KOMPONEN_NAME = 'ebudget.pak_bukuputih_rincian_detail.KOMPONEN_NAME';

	
	const SATUAN = 'ebudget.pak_bukuputih_rincian_detail.SATUAN';

	
	const PAJAK = 'ebudget.pak_bukuputih_rincian_detail.PAJAK';

	
	const UNIT_ID = 'ebudget.pak_bukuputih_rincian_detail.UNIT_ID';

	
	const FROM_SUB_KEGIATAN = 'ebudget.pak_bukuputih_rincian_detail.FROM_SUB_KEGIATAN';

	
	const SUB = 'ebudget.pak_bukuputih_rincian_detail.SUB';

	
	const KODE_SUB = 'ebudget.pak_bukuputih_rincian_detail.KODE_SUB';

	
	const LAST_UPDATE_USER = 'ebudget.pak_bukuputih_rincian_detail.LAST_UPDATE_USER';

	
	const LAST_UPDATE_TIME = 'ebudget.pak_bukuputih_rincian_detail.LAST_UPDATE_TIME';

	
	const LAST_UPDATE_IP = 'ebudget.pak_bukuputih_rincian_detail.LAST_UPDATE_IP';

	
	const TAHAP = 'ebudget.pak_bukuputih_rincian_detail.TAHAP';

	
	const TAHAP_EDIT = 'ebudget.pak_bukuputih_rincian_detail.TAHAP_EDIT';

	
	const TAHAP_NEW = 'ebudget.pak_bukuputih_rincian_detail.TAHAP_NEW';

	
	const STATUS_LELANG = 'ebudget.pak_bukuputih_rincian_detail.STATUS_LELANG';

	
	const NOMOR_LELANG = 'ebudget.pak_bukuputih_rincian_detail.NOMOR_LELANG';

	
	const KOEFISIEN_SEMULA = 'ebudget.pak_bukuputih_rincian_detail.KOEFISIEN_SEMULA';

	
	const VOLUME_SEMULA = 'ebudget.pak_bukuputih_rincian_detail.VOLUME_SEMULA';

	
	const HARGA_SEMULA = 'ebudget.pak_bukuputih_rincian_detail.HARGA_SEMULA';

	
	const TOTAL_SEMULA = 'ebudget.pak_bukuputih_rincian_detail.TOTAL_SEMULA';

	
	const LOCK_SUBTITLE = 'ebudget.pak_bukuputih_rincian_detail.LOCK_SUBTITLE';

	
	const STATUS_HAPUS = 'ebudget.pak_bukuputih_rincian_detail.STATUS_HAPUS';

	
	const TAHUN = 'ebudget.pak_bukuputih_rincian_detail.TAHUN';

	
	const KODE_LOKASI = 'ebudget.pak_bukuputih_rincian_detail.KODE_LOKASI';

	
	const KECAMATAN = 'ebudget.pak_bukuputih_rincian_detail.KECAMATAN';

	
	const REKENING_CODE_ASLI = 'ebudget.pak_bukuputih_rincian_detail.REKENING_CODE_ASLI';

	
	const NOTE_SKPD = 'ebudget.pak_bukuputih_rincian_detail.NOTE_SKPD';

	
	const NOTE_PENELITI = 'ebudget.pak_bukuputih_rincian_detail.NOTE_PENELITI';

	
	const NILAI_ANGGARAN = 'ebudget.pak_bukuputih_rincian_detail.NILAI_ANGGARAN';

	
	const IS_BLUD = 'ebudget.pak_bukuputih_rincian_detail.IS_BLUD';

	
	const LOKASI_KECAMATAN = 'ebudget.pak_bukuputih_rincian_detail.LOKASI_KECAMATAN';

	
	const LOKASI_KELURAHAN = 'ebudget.pak_bukuputih_rincian_detail.LOKASI_KELURAHAN';

	
	const OB = 'ebudget.pak_bukuputih_rincian_detail.OB';

	
	const OB_FROM_ID = 'ebudget.pak_bukuputih_rincian_detail.OB_FROM_ID';

	
	const IS_PER_KOMPONEN = 'ebudget.pak_bukuputih_rincian_detail.IS_PER_KOMPONEN';

	
	const KEGIATAN_CODE_ASAL = 'ebudget.pak_bukuputih_rincian_detail.KEGIATAN_CODE_ASAL';

	
	const TH_KE_MULTIYEARS = 'ebudget.pak_bukuputih_rincian_detail.TH_KE_MULTIYEARS';

	
	const HARGA_SEBELUM_SISA_LELANG = 'ebudget.pak_bukuputih_rincian_detail.HARGA_SEBELUM_SISA_LELANG';

	
	const IS_MUSRENBANG = 'ebudget.pak_bukuputih_rincian_detail.IS_MUSRENBANG';

	
	const SUB_ID_ASAL = 'ebudget.pak_bukuputih_rincian_detail.SUB_ID_ASAL';

	
	const SUBTITLE_ASAL = 'ebudget.pak_bukuputih_rincian_detail.SUBTITLE_ASAL';

	
	const KODE_SUB_ASAL = 'ebudget.pak_bukuputih_rincian_detail.KODE_SUB_ASAL';

	
	const SUB_ASAL = 'ebudget.pak_bukuputih_rincian_detail.SUB_ASAL';

	
	const LAST_EDIT_TIME = 'ebudget.pak_bukuputih_rincian_detail.LAST_EDIT_TIME';

	
	const IS_POTONG_BPJS = 'ebudget.pak_bukuputih_rincian_detail.IS_POTONG_BPJS';

	
	const IS_IURAN_BPJS = 'ebudget.pak_bukuputih_rincian_detail.IS_IURAN_BPJS';

	
	const STATUS_OB = 'ebudget.pak_bukuputih_rincian_detail.STATUS_OB';

	
	const OB_PARENT = 'ebudget.pak_bukuputih_rincian_detail.OB_PARENT';

	
	const OB_ALOKASI_BARU = 'ebudget.pak_bukuputih_rincian_detail.OB_ALOKASI_BARU';

	
	const IS_HIBAH = 'ebudget.pak_bukuputih_rincian_detail.IS_HIBAH';

	
	const STATUS_LEVEL = 'ebudget.pak_bukuputih_rincian_detail.STATUS_LEVEL';

	
	const STATUS_LEVEL_TOLAK = 'ebudget.pak_bukuputih_rincian_detail.STATUS_LEVEL_TOLAK';

	
	const STATUS_SISIPAN = 'ebudget.pak_bukuputih_rincian_detail.STATUS_SISIPAN';

	
	const IS_TAPD_SETUJU = 'ebudget.pak_bukuputih_rincian_detail.IS_TAPD_SETUJU';

	
	const IS_BAPPEKO_SETUJU = 'ebudget.pak_bukuputih_rincian_detail.IS_BAPPEKO_SETUJU';

	
	const AKRUAL_CODE = 'ebudget.pak_bukuputih_rincian_detail.AKRUAL_CODE';

	
	const TIPE2 = 'ebudget.pak_bukuputih_rincian_detail.TIPE2';

	
	const IS_PENYELIA_SETUJU = 'ebudget.pak_bukuputih_rincian_detail.IS_PENYELIA_SETUJU';

	
	const NOTE_TAPD = 'ebudget.pak_bukuputih_rincian_detail.NOTE_TAPD';

	
	const NOTE_BAPPEKO = 'ebudget.pak_bukuputih_rincian_detail.NOTE_BAPPEKO';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('KegiatanCode', 'Tipe', 'DetailNo', 'RekeningCode', 'KomponenId', 'DetailName', 'Volume', 'KeteranganKoefisien', 'Subtitle', 'KomponenHarga', 'KomponenHargaAwal', 'KomponenName', 'Satuan', 'Pajak', 'UnitId', 'FromSubKegiatan', 'Sub', 'KodeSub', 'LastUpdateUser', 'LastUpdateTime', 'LastUpdateIp', 'Tahap', 'TahapEdit', 'TahapNew', 'StatusLelang', 'NomorLelang', 'KoefisienSemula', 'VolumeSemula', 'HargaSemula', 'TotalSemula', 'LockSubtitle', 'StatusHapus', 'Tahun', 'KodeLokasi', 'Kecamatan', 'RekeningCodeAsli', 'NoteSkpd', 'NotePeneliti', 'NilaiAnggaran', 'IsBlud', 'LokasiKecamatan', 'LokasiKelurahan', 'Ob', 'ObFromId', 'IsPerKomponen', 'KegiatanCodeAsal', 'ThKeMultiyears', 'HargaSebelumSisaLelang', 'IsMusrenbang', 'SubIdAsal', 'SubtitleAsal', 'KodeSubAsal', 'SubAsal', 'LastEditTime', 'IsPotongBpjs', 'IsIuranBpjs', 'StatusOb', 'ObParent', 'ObAlokasiBaru', 'IsHibah', 'StatusLevel', 'StatusLevelTolak', 'StatusSisipan', 'IsTapdSetuju', 'IsBappekoSetuju', 'AkrualCode', 'Tipe2', 'IsPenyeliaSetuju', 'NoteTapd', 'NoteBappeko', ),
		BasePeer::TYPE_COLNAME => array (PakBukuPutihRincianDetailPeer::KEGIATAN_CODE, PakBukuPutihRincianDetailPeer::TIPE, PakBukuPutihRincianDetailPeer::DETAIL_NO, PakBukuPutihRincianDetailPeer::REKENING_CODE, PakBukuPutihRincianDetailPeer::KOMPONEN_ID, PakBukuPutihRincianDetailPeer::DETAIL_NAME, PakBukuPutihRincianDetailPeer::VOLUME, PakBukuPutihRincianDetailPeer::KETERANGAN_KOEFISIEN, PakBukuPutihRincianDetailPeer::SUBTITLE, PakBukuPutihRincianDetailPeer::KOMPONEN_HARGA, PakBukuPutihRincianDetailPeer::KOMPONEN_HARGA_AWAL, PakBukuPutihRincianDetailPeer::KOMPONEN_NAME, PakBukuPutihRincianDetailPeer::SATUAN, PakBukuPutihRincianDetailPeer::PAJAK, PakBukuPutihRincianDetailPeer::UNIT_ID, PakBukuPutihRincianDetailPeer::FROM_SUB_KEGIATAN, PakBukuPutihRincianDetailPeer::SUB, PakBukuPutihRincianDetailPeer::KODE_SUB, PakBukuPutihRincianDetailPeer::LAST_UPDATE_USER, PakBukuPutihRincianDetailPeer::LAST_UPDATE_TIME, PakBukuPutihRincianDetailPeer::LAST_UPDATE_IP, PakBukuPutihRincianDetailPeer::TAHAP, PakBukuPutihRincianDetailPeer::TAHAP_EDIT, PakBukuPutihRincianDetailPeer::TAHAP_NEW, PakBukuPutihRincianDetailPeer::STATUS_LELANG, PakBukuPutihRincianDetailPeer::NOMOR_LELANG, PakBukuPutihRincianDetailPeer::KOEFISIEN_SEMULA, PakBukuPutihRincianDetailPeer::VOLUME_SEMULA, PakBukuPutihRincianDetailPeer::HARGA_SEMULA, PakBukuPutihRincianDetailPeer::TOTAL_SEMULA, PakBukuPutihRincianDetailPeer::LOCK_SUBTITLE, PakBukuPutihRincianDetailPeer::STATUS_HAPUS, PakBukuPutihRincianDetailPeer::TAHUN, PakBukuPutihRincianDetailPeer::KODE_LOKASI, PakBukuPutihRincianDetailPeer::KECAMATAN, PakBukuPutihRincianDetailPeer::REKENING_CODE_ASLI, PakBukuPutihRincianDetailPeer::NOTE_SKPD, PakBukuPutihRincianDetailPeer::NOTE_PENELITI, PakBukuPutihRincianDetailPeer::NILAI_ANGGARAN, PakBukuPutihRincianDetailPeer::IS_BLUD, PakBukuPutihRincianDetailPeer::LOKASI_KECAMATAN, PakBukuPutihRincianDetailPeer::LOKASI_KELURAHAN, PakBukuPutihRincianDetailPeer::OB, PakBukuPutihRincianDetailPeer::OB_FROM_ID, PakBukuPutihRincianDetailPeer::IS_PER_KOMPONEN, PakBukuPutihRincianDetailPeer::KEGIATAN_CODE_ASAL, PakBukuPutihRincianDetailPeer::TH_KE_MULTIYEARS, PakBukuPutihRincianDetailPeer::HARGA_SEBELUM_SISA_LELANG, PakBukuPutihRincianDetailPeer::IS_MUSRENBANG, PakBukuPutihRincianDetailPeer::SUB_ID_ASAL, PakBukuPutihRincianDetailPeer::SUBTITLE_ASAL, PakBukuPutihRincianDetailPeer::KODE_SUB_ASAL, PakBukuPutihRincianDetailPeer::SUB_ASAL, PakBukuPutihRincianDetailPeer::LAST_EDIT_TIME, PakBukuPutihRincianDetailPeer::IS_POTONG_BPJS, PakBukuPutihRincianDetailPeer::IS_IURAN_BPJS, PakBukuPutihRincianDetailPeer::STATUS_OB, PakBukuPutihRincianDetailPeer::OB_PARENT, PakBukuPutihRincianDetailPeer::OB_ALOKASI_BARU, PakBukuPutihRincianDetailPeer::IS_HIBAH, PakBukuPutihRincianDetailPeer::STATUS_LEVEL, PakBukuPutihRincianDetailPeer::STATUS_LEVEL_TOLAK, PakBukuPutihRincianDetailPeer::STATUS_SISIPAN, PakBukuPutihRincianDetailPeer::IS_TAPD_SETUJU, PakBukuPutihRincianDetailPeer::IS_BAPPEKO_SETUJU, PakBukuPutihRincianDetailPeer::AKRUAL_CODE, PakBukuPutihRincianDetailPeer::TIPE2, PakBukuPutihRincianDetailPeer::IS_PENYELIA_SETUJU, PakBukuPutihRincianDetailPeer::NOTE_TAPD, PakBukuPutihRincianDetailPeer::NOTE_BAPPEKO, ),
		BasePeer::TYPE_FIELDNAME => array ('kegiatan_code', 'tipe', 'detail_no', 'rekening_code', 'komponen_id', 'detail_name', 'volume', 'keterangan_koefisien', 'subtitle', 'komponen_harga', 'komponen_harga_awal', 'komponen_name', 'satuan', 'pajak', 'unit_id', 'from_sub_kegiatan', 'sub', 'kode_sub', 'last_update_user', 'last_update_time', 'last_update_ip', 'tahap', 'tahap_edit', 'tahap_new', 'status_lelang', 'nomor_lelang', 'koefisien_semula', 'volume_semula', 'harga_semula', 'total_semula', 'lock_subtitle', 'status_hapus', 'tahun', 'kode_lokasi', 'kecamatan', 'rekening_code_asli', 'note_skpd', 'note_peneliti', 'nilai_anggaran', 'is_blud', 'lokasi_kecamatan', 'lokasi_kelurahan', 'ob', 'ob_from_id', 'is_per_komponen', 'kegiatan_code_asal', 'th_ke_multiyears', 'harga_sebelum_sisa_lelang', 'is_musrenbang', 'sub_id_asal', 'subtitle_asal', 'kode_sub_asal', 'sub_asal', 'last_edit_time', 'is_potong_bpjs', 'is_iuran_bpjs', 'status_ob', 'ob_parent', 'ob_alokasi_baru', 'is_hibah', 'status_level', 'status_level_tolak', 'status_sisipan', 'is_tapd_setuju', 'is_bappeko_setuju', 'akrual_code', 'tipe2', 'is_penyelia_setuju', 'note_tapd', 'note_bappeko', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('KegiatanCode' => 0, 'Tipe' => 1, 'DetailNo' => 2, 'RekeningCode' => 3, 'KomponenId' => 4, 'DetailName' => 5, 'Volume' => 6, 'KeteranganKoefisien' => 7, 'Subtitle' => 8, 'KomponenHarga' => 9, 'KomponenHargaAwal' => 10, 'KomponenName' => 11, 'Satuan' => 12, 'Pajak' => 13, 'UnitId' => 14, 'FromSubKegiatan' => 15, 'Sub' => 16, 'KodeSub' => 17, 'LastUpdateUser' => 18, 'LastUpdateTime' => 19, 'LastUpdateIp' => 20, 'Tahap' => 21, 'TahapEdit' => 22, 'TahapNew' => 23, 'StatusLelang' => 24, 'NomorLelang' => 25, 'KoefisienSemula' => 26, 'VolumeSemula' => 27, 'HargaSemula' => 28, 'TotalSemula' => 29, 'LockSubtitle' => 30, 'StatusHapus' => 31, 'Tahun' => 32, 'KodeLokasi' => 33, 'Kecamatan' => 34, 'RekeningCodeAsli' => 35, 'NoteSkpd' => 36, 'NotePeneliti' => 37, 'NilaiAnggaran' => 38, 'IsBlud' => 39, 'LokasiKecamatan' => 40, 'LokasiKelurahan' => 41, 'Ob' => 42, 'ObFromId' => 43, 'IsPerKomponen' => 44, 'KegiatanCodeAsal' => 45, 'ThKeMultiyears' => 46, 'HargaSebelumSisaLelang' => 47, 'IsMusrenbang' => 48, 'SubIdAsal' => 49, 'SubtitleAsal' => 50, 'KodeSubAsal' => 51, 'SubAsal' => 52, 'LastEditTime' => 53, 'IsPotongBpjs' => 54, 'IsIuranBpjs' => 55, 'StatusOb' => 56, 'ObParent' => 57, 'ObAlokasiBaru' => 58, 'IsHibah' => 59, 'StatusLevel' => 60, 'StatusLevelTolak' => 61, 'StatusSisipan' => 62, 'IsTapdSetuju' => 63, 'IsBappekoSetuju' => 64, 'AkrualCode' => 65, 'Tipe2' => 66, 'IsPenyeliaSetuju' => 67, 'NoteTapd' => 68, 'NoteBappeko' => 69, ),
		BasePeer::TYPE_COLNAME => array (PakBukuPutihRincianDetailPeer::KEGIATAN_CODE => 0, PakBukuPutihRincianDetailPeer::TIPE => 1, PakBukuPutihRincianDetailPeer::DETAIL_NO => 2, PakBukuPutihRincianDetailPeer::REKENING_CODE => 3, PakBukuPutihRincianDetailPeer::KOMPONEN_ID => 4, PakBukuPutihRincianDetailPeer::DETAIL_NAME => 5, PakBukuPutihRincianDetailPeer::VOLUME => 6, PakBukuPutihRincianDetailPeer::KETERANGAN_KOEFISIEN => 7, PakBukuPutihRincianDetailPeer::SUBTITLE => 8, PakBukuPutihRincianDetailPeer::KOMPONEN_HARGA => 9, PakBukuPutihRincianDetailPeer::KOMPONEN_HARGA_AWAL => 10, PakBukuPutihRincianDetailPeer::KOMPONEN_NAME => 11, PakBukuPutihRincianDetailPeer::SATUAN => 12, PakBukuPutihRincianDetailPeer::PAJAK => 13, PakBukuPutihRincianDetailPeer::UNIT_ID => 14, PakBukuPutihRincianDetailPeer::FROM_SUB_KEGIATAN => 15, PakBukuPutihRincianDetailPeer::SUB => 16, PakBukuPutihRincianDetailPeer::KODE_SUB => 17, PakBukuPutihRincianDetailPeer::LAST_UPDATE_USER => 18, PakBukuPutihRincianDetailPeer::LAST_UPDATE_TIME => 19, PakBukuPutihRincianDetailPeer::LAST_UPDATE_IP => 20, PakBukuPutihRincianDetailPeer::TAHAP => 21, PakBukuPutihRincianDetailPeer::TAHAP_EDIT => 22, PakBukuPutihRincianDetailPeer::TAHAP_NEW => 23, PakBukuPutihRincianDetailPeer::STATUS_LELANG => 24, PakBukuPutihRincianDetailPeer::NOMOR_LELANG => 25, PakBukuPutihRincianDetailPeer::KOEFISIEN_SEMULA => 26, PakBukuPutihRincianDetailPeer::VOLUME_SEMULA => 27, PakBukuPutihRincianDetailPeer::HARGA_SEMULA => 28, PakBukuPutihRincianDetailPeer::TOTAL_SEMULA => 29, PakBukuPutihRincianDetailPeer::LOCK_SUBTITLE => 30, PakBukuPutihRincianDetailPeer::STATUS_HAPUS => 31, PakBukuPutihRincianDetailPeer::TAHUN => 32, PakBukuPutihRincianDetailPeer::KODE_LOKASI => 33, PakBukuPutihRincianDetailPeer::KECAMATAN => 34, PakBukuPutihRincianDetailPeer::REKENING_CODE_ASLI => 35, PakBukuPutihRincianDetailPeer::NOTE_SKPD => 36, PakBukuPutihRincianDetailPeer::NOTE_PENELITI => 37, PakBukuPutihRincianDetailPeer::NILAI_ANGGARAN => 38, PakBukuPutihRincianDetailPeer::IS_BLUD => 39, PakBukuPutihRincianDetailPeer::LOKASI_KECAMATAN => 40, PakBukuPutihRincianDetailPeer::LOKASI_KELURAHAN => 41, PakBukuPutihRincianDetailPeer::OB => 42, PakBukuPutihRincianDetailPeer::OB_FROM_ID => 43, PakBukuPutihRincianDetailPeer::IS_PER_KOMPONEN => 44, PakBukuPutihRincianDetailPeer::KEGIATAN_CODE_ASAL => 45, PakBukuPutihRincianDetailPeer::TH_KE_MULTIYEARS => 46, PakBukuPutihRincianDetailPeer::HARGA_SEBELUM_SISA_LELANG => 47, PakBukuPutihRincianDetailPeer::IS_MUSRENBANG => 48, PakBukuPutihRincianDetailPeer::SUB_ID_ASAL => 49, PakBukuPutihRincianDetailPeer::SUBTITLE_ASAL => 50, PakBukuPutihRincianDetailPeer::KODE_SUB_ASAL => 51, PakBukuPutihRincianDetailPeer::SUB_ASAL => 52, PakBukuPutihRincianDetailPeer::LAST_EDIT_TIME => 53, PakBukuPutihRincianDetailPeer::IS_POTONG_BPJS => 54, PakBukuPutihRincianDetailPeer::IS_IURAN_BPJS => 55, PakBukuPutihRincianDetailPeer::STATUS_OB => 56, PakBukuPutihRincianDetailPeer::OB_PARENT => 57, PakBukuPutihRincianDetailPeer::OB_ALOKASI_BARU => 58, PakBukuPutihRincianDetailPeer::IS_HIBAH => 59, PakBukuPutihRincianDetailPeer::STATUS_LEVEL => 60, PakBukuPutihRincianDetailPeer::STATUS_LEVEL_TOLAK => 61, PakBukuPutihRincianDetailPeer::STATUS_SISIPAN => 62, PakBukuPutihRincianDetailPeer::IS_TAPD_SETUJU => 63, PakBukuPutihRincianDetailPeer::IS_BAPPEKO_SETUJU => 64, PakBukuPutihRincianDetailPeer::AKRUAL_CODE => 65, PakBukuPutihRincianDetailPeer::TIPE2 => 66, PakBukuPutihRincianDetailPeer::IS_PENYELIA_SETUJU => 67, PakBukuPutihRincianDetailPeer::NOTE_TAPD => 68, PakBukuPutihRincianDetailPeer::NOTE_BAPPEKO => 69, ),
		BasePeer::TYPE_FIELDNAME => array ('kegiatan_code' => 0, 'tipe' => 1, 'detail_no' => 2, 'rekening_code' => 3, 'komponen_id' => 4, 'detail_name' => 5, 'volume' => 6, 'keterangan_koefisien' => 7, 'subtitle' => 8, 'komponen_harga' => 9, 'komponen_harga_awal' => 10, 'komponen_name' => 11, 'satuan' => 12, 'pajak' => 13, 'unit_id' => 14, 'from_sub_kegiatan' => 15, 'sub' => 16, 'kode_sub' => 17, 'last_update_user' => 18, 'last_update_time' => 19, 'last_update_ip' => 20, 'tahap' => 21, 'tahap_edit' => 22, 'tahap_new' => 23, 'status_lelang' => 24, 'nomor_lelang' => 25, 'koefisien_semula' => 26, 'volume_semula' => 27, 'harga_semula' => 28, 'total_semula' => 29, 'lock_subtitle' => 30, 'status_hapus' => 31, 'tahun' => 32, 'kode_lokasi' => 33, 'kecamatan' => 34, 'rekening_code_asli' => 35, 'note_skpd' => 36, 'note_peneliti' => 37, 'nilai_anggaran' => 38, 'is_blud' => 39, 'lokasi_kecamatan' => 40, 'lokasi_kelurahan' => 41, 'ob' => 42, 'ob_from_id' => 43, 'is_per_komponen' => 44, 'kegiatan_code_asal' => 45, 'th_ke_multiyears' => 46, 'harga_sebelum_sisa_lelang' => 47, 'is_musrenbang' => 48, 'sub_id_asal' => 49, 'subtitle_asal' => 50, 'kode_sub_asal' => 51, 'sub_asal' => 52, 'last_edit_time' => 53, 'is_potong_bpjs' => 54, 'is_iuran_bpjs' => 55, 'status_ob' => 56, 'ob_parent' => 57, 'ob_alokasi_baru' => 58, 'is_hibah' => 59, 'status_level' => 60, 'status_level_tolak' => 61, 'status_sisipan' => 62, 'is_tapd_setuju' => 63, 'is_bappeko_setuju' => 64, 'akrual_code' => 65, 'tipe2' => 66, 'is_penyelia_setuju' => 67, 'note_tapd' => 68, 'note_bappeko' => 69, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/PakBukuPutihRincianDetailMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.PakBukuPutihRincianDetailMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = PakBukuPutihRincianDetailPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(PakBukuPutihRincianDetailPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(PakBukuPutihRincianDetailPeer::KEGIATAN_CODE);

		$criteria->addSelectColumn(PakBukuPutihRincianDetailPeer::TIPE);

		$criteria->addSelectColumn(PakBukuPutihRincianDetailPeer::DETAIL_NO);

		$criteria->addSelectColumn(PakBukuPutihRincianDetailPeer::REKENING_CODE);

		$criteria->addSelectColumn(PakBukuPutihRincianDetailPeer::KOMPONEN_ID);

		$criteria->addSelectColumn(PakBukuPutihRincianDetailPeer::DETAIL_NAME);

		$criteria->addSelectColumn(PakBukuPutihRincianDetailPeer::VOLUME);

		$criteria->addSelectColumn(PakBukuPutihRincianDetailPeer::KETERANGAN_KOEFISIEN);

		$criteria->addSelectColumn(PakBukuPutihRincianDetailPeer::SUBTITLE);

		$criteria->addSelectColumn(PakBukuPutihRincianDetailPeer::KOMPONEN_HARGA);

		$criteria->addSelectColumn(PakBukuPutihRincianDetailPeer::KOMPONEN_HARGA_AWAL);

		$criteria->addSelectColumn(PakBukuPutihRincianDetailPeer::KOMPONEN_NAME);

		$criteria->addSelectColumn(PakBukuPutihRincianDetailPeer::SATUAN);

		$criteria->addSelectColumn(PakBukuPutihRincianDetailPeer::PAJAK);

		$criteria->addSelectColumn(PakBukuPutihRincianDetailPeer::UNIT_ID);

		$criteria->addSelectColumn(PakBukuPutihRincianDetailPeer::FROM_SUB_KEGIATAN);

		$criteria->addSelectColumn(PakBukuPutihRincianDetailPeer::SUB);

		$criteria->addSelectColumn(PakBukuPutihRincianDetailPeer::KODE_SUB);

		$criteria->addSelectColumn(PakBukuPutihRincianDetailPeer::LAST_UPDATE_USER);

		$criteria->addSelectColumn(PakBukuPutihRincianDetailPeer::LAST_UPDATE_TIME);

		$criteria->addSelectColumn(PakBukuPutihRincianDetailPeer::LAST_UPDATE_IP);

		$criteria->addSelectColumn(PakBukuPutihRincianDetailPeer::TAHAP);

		$criteria->addSelectColumn(PakBukuPutihRincianDetailPeer::TAHAP_EDIT);

		$criteria->addSelectColumn(PakBukuPutihRincianDetailPeer::TAHAP_NEW);

		$criteria->addSelectColumn(PakBukuPutihRincianDetailPeer::STATUS_LELANG);

		$criteria->addSelectColumn(PakBukuPutihRincianDetailPeer::NOMOR_LELANG);

		$criteria->addSelectColumn(PakBukuPutihRincianDetailPeer::KOEFISIEN_SEMULA);

		$criteria->addSelectColumn(PakBukuPutihRincianDetailPeer::VOLUME_SEMULA);

		$criteria->addSelectColumn(PakBukuPutihRincianDetailPeer::HARGA_SEMULA);

		$criteria->addSelectColumn(PakBukuPutihRincianDetailPeer::TOTAL_SEMULA);

		$criteria->addSelectColumn(PakBukuPutihRincianDetailPeer::LOCK_SUBTITLE);

		$criteria->addSelectColumn(PakBukuPutihRincianDetailPeer::STATUS_HAPUS);

		$criteria->addSelectColumn(PakBukuPutihRincianDetailPeer::TAHUN);

		$criteria->addSelectColumn(PakBukuPutihRincianDetailPeer::KODE_LOKASI);

		$criteria->addSelectColumn(PakBukuPutihRincianDetailPeer::KECAMATAN);

		$criteria->addSelectColumn(PakBukuPutihRincianDetailPeer::REKENING_CODE_ASLI);

		$criteria->addSelectColumn(PakBukuPutihRincianDetailPeer::NOTE_SKPD);

		$criteria->addSelectColumn(PakBukuPutihRincianDetailPeer::NOTE_PENELITI);

		$criteria->addSelectColumn(PakBukuPutihRincianDetailPeer::NILAI_ANGGARAN);

		$criteria->addSelectColumn(PakBukuPutihRincianDetailPeer::IS_BLUD);

		$criteria->addSelectColumn(PakBukuPutihRincianDetailPeer::LOKASI_KECAMATAN);

		$criteria->addSelectColumn(PakBukuPutihRincianDetailPeer::LOKASI_KELURAHAN);

		$criteria->addSelectColumn(PakBukuPutihRincianDetailPeer::OB);

		$criteria->addSelectColumn(PakBukuPutihRincianDetailPeer::OB_FROM_ID);

		$criteria->addSelectColumn(PakBukuPutihRincianDetailPeer::IS_PER_KOMPONEN);

		$criteria->addSelectColumn(PakBukuPutihRincianDetailPeer::KEGIATAN_CODE_ASAL);

		$criteria->addSelectColumn(PakBukuPutihRincianDetailPeer::TH_KE_MULTIYEARS);

		$criteria->addSelectColumn(PakBukuPutihRincianDetailPeer::HARGA_SEBELUM_SISA_LELANG);

		$criteria->addSelectColumn(PakBukuPutihRincianDetailPeer::IS_MUSRENBANG);

		$criteria->addSelectColumn(PakBukuPutihRincianDetailPeer::SUB_ID_ASAL);

		$criteria->addSelectColumn(PakBukuPutihRincianDetailPeer::SUBTITLE_ASAL);

		$criteria->addSelectColumn(PakBukuPutihRincianDetailPeer::KODE_SUB_ASAL);

		$criteria->addSelectColumn(PakBukuPutihRincianDetailPeer::SUB_ASAL);

		$criteria->addSelectColumn(PakBukuPutihRincianDetailPeer::LAST_EDIT_TIME);

		$criteria->addSelectColumn(PakBukuPutihRincianDetailPeer::IS_POTONG_BPJS);

		$criteria->addSelectColumn(PakBukuPutihRincianDetailPeer::IS_IURAN_BPJS);

		$criteria->addSelectColumn(PakBukuPutihRincianDetailPeer::STATUS_OB);

		$criteria->addSelectColumn(PakBukuPutihRincianDetailPeer::OB_PARENT);

		$criteria->addSelectColumn(PakBukuPutihRincianDetailPeer::OB_ALOKASI_BARU);

		$criteria->addSelectColumn(PakBukuPutihRincianDetailPeer::IS_HIBAH);

		$criteria->addSelectColumn(PakBukuPutihRincianDetailPeer::STATUS_LEVEL);

		$criteria->addSelectColumn(PakBukuPutihRincianDetailPeer::STATUS_LEVEL_TOLAK);

		$criteria->addSelectColumn(PakBukuPutihRincianDetailPeer::STATUS_SISIPAN);

		$criteria->addSelectColumn(PakBukuPutihRincianDetailPeer::IS_TAPD_SETUJU);

		$criteria->addSelectColumn(PakBukuPutihRincianDetailPeer::IS_BAPPEKO_SETUJU);

		$criteria->addSelectColumn(PakBukuPutihRincianDetailPeer::AKRUAL_CODE);

		$criteria->addSelectColumn(PakBukuPutihRincianDetailPeer::TIPE2);

		$criteria->addSelectColumn(PakBukuPutihRincianDetailPeer::IS_PENYELIA_SETUJU);

		$criteria->addSelectColumn(PakBukuPutihRincianDetailPeer::NOTE_TAPD);

		$criteria->addSelectColumn(PakBukuPutihRincianDetailPeer::NOTE_BAPPEKO);

	}

	const COUNT = 'COUNT(ebudget.pak_bukuputih_rincian_detail.KEGIATAN_CODE)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.pak_bukuputih_rincian_detail.KEGIATAN_CODE)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(PakBukuPutihRincianDetailPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(PakBukuPutihRincianDetailPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = PakBukuPutihRincianDetailPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = PakBukuPutihRincianDetailPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return PakBukuPutihRincianDetailPeer::populateObjects(PakBukuPutihRincianDetailPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			PakBukuPutihRincianDetailPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = PakBukuPutihRincianDetailPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return PakBukuPutihRincianDetailPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(PakBukuPutihRincianDetailPeer::KEGIATAN_CODE);
			$selectCriteria->add(PakBukuPutihRincianDetailPeer::KEGIATAN_CODE, $criteria->remove(PakBukuPutihRincianDetailPeer::KEGIATAN_CODE), $comparison);

			$comparison = $criteria->getComparison(PakBukuPutihRincianDetailPeer::DETAIL_NO);
			$selectCriteria->add(PakBukuPutihRincianDetailPeer::DETAIL_NO, $criteria->remove(PakBukuPutihRincianDetailPeer::DETAIL_NO), $comparison);

			$comparison = $criteria->getComparison(PakBukuPutihRincianDetailPeer::UNIT_ID);
			$selectCriteria->add(PakBukuPutihRincianDetailPeer::UNIT_ID, $criteria->remove(PakBukuPutihRincianDetailPeer::UNIT_ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(PakBukuPutihRincianDetailPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(PakBukuPutihRincianDetailPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof PakBukuPutihRincianDetail) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
												if(count($values) == count($values, COUNT_RECURSIVE))
			{
								$values = array($values);
			}
			$vals = array();
			foreach($values as $value)
			{

				$vals[0][] = $value[0];
				$vals[1][] = $value[1];
				$vals[2][] = $value[2];
			}

			$criteria->add(PakBukuPutihRincianDetailPeer::KEGIATAN_CODE, $vals[0], Criteria::IN);
			$criteria->add(PakBukuPutihRincianDetailPeer::DETAIL_NO, $vals[1], Criteria::IN);
			$criteria->add(PakBukuPutihRincianDetailPeer::UNIT_ID, $vals[2], Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(PakBukuPutihRincianDetail $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(PakBukuPutihRincianDetailPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(PakBukuPutihRincianDetailPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(PakBukuPutihRincianDetailPeer::DATABASE_NAME, PakBukuPutihRincianDetailPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = PakBukuPutihRincianDetailPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK( $kegiatan_code, $detail_no, $unit_id, $con = null) {
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$criteria = new Criteria();
		$criteria->add(PakBukuPutihRincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
		$criteria->add(PakBukuPutihRincianDetailPeer::DETAIL_NO, $detail_no);
		$criteria->add(PakBukuPutihRincianDetailPeer::UNIT_ID, $unit_id);
		$v = PakBukuPutihRincianDetailPeer::doSelect($criteria, $con);

		return !empty($v) ? $v[0] : null;
	}
} 
if (Propel::isInit()) {
			try {
		BasePakBukuPutihRincianDetailPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/PakBukuPutihRincianDetailMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.PakBukuPutihRincianDetailMapBuilder');
}
