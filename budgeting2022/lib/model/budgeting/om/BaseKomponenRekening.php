<?php


abstract class BaseKomponenRekening extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $rekening_code;


	
	protected $komponen_id;


	
	protected $rekening_code29;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getRekeningCode()
	{

		return $this->rekening_code;
	}

	
	public function getKomponenId()
	{

		return $this->komponen_id;
	}

	
	public function getRekeningCode29()
	{

		return $this->rekening_code29;
	}

	
	public function setRekeningCode($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->rekening_code !== $v) {
			$this->rekening_code = $v;
			$this->modifiedColumns[] = KomponenRekeningPeer::REKENING_CODE;
		}

	} 
	
	public function setKomponenId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->komponen_id !== $v) {
			$this->komponen_id = $v;
			$this->modifiedColumns[] = KomponenRekeningPeer::KOMPONEN_ID;
		}

	} 
	
	public function setRekeningCode29($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->rekening_code29 !== $v) {
			$this->rekening_code29 = $v;
			$this->modifiedColumns[] = KomponenRekeningPeer::REKENING_CODE29;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->rekening_code = $rs->getString($startcol + 0);

			$this->komponen_id = $rs->getString($startcol + 1);

			$this->rekening_code29 = $rs->getString($startcol + 2);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 3; 
		} catch (Exception $e) {
			throw new PropelException("Error populating KomponenRekening object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(KomponenRekeningPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			KomponenRekeningPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(KomponenRekeningPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = KomponenRekeningPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += KomponenRekeningPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = KomponenRekeningPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = KomponenRekeningPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getRekeningCode();
				break;
			case 1:
				return $this->getKomponenId();
				break;
			case 2:
				return $this->getRekeningCode29();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = KomponenRekeningPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getRekeningCode(),
			$keys[1] => $this->getKomponenId(),
			$keys[2] => $this->getRekeningCode29(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = KomponenRekeningPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setRekeningCode($value);
				break;
			case 1:
				$this->setKomponenId($value);
				break;
			case 2:
				$this->setRekeningCode29($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = KomponenRekeningPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setRekeningCode($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setKomponenId($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setRekeningCode29($arr[$keys[2]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(KomponenRekeningPeer::DATABASE_NAME);

		if ($this->isColumnModified(KomponenRekeningPeer::REKENING_CODE)) $criteria->add(KomponenRekeningPeer::REKENING_CODE, $this->rekening_code);
		if ($this->isColumnModified(KomponenRekeningPeer::KOMPONEN_ID)) $criteria->add(KomponenRekeningPeer::KOMPONEN_ID, $this->komponen_id);
		if ($this->isColumnModified(KomponenRekeningPeer::REKENING_CODE29)) $criteria->add(KomponenRekeningPeer::REKENING_CODE29, $this->rekening_code29);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(KomponenRekeningPeer::DATABASE_NAME);


		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return null;
	}

	
	 public function setPrimaryKey($pk)
	 {
		 	 }

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setRekeningCode($this->rekening_code);

		$copyObj->setKomponenId($this->komponen_id);

		$copyObj->setRekeningCode29($this->rekening_code29);


		$copyObj->setNew(true);

	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new KomponenRekeningPeer();
		}
		return self::$peer;
	}

} 