<?php


abstract class BaseTarikSwakelolaBpPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.tarik_swakelola_bp';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.TarikSwakelolaBp';

	
	const NUM_COLUMNS = 56;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const KEGIATAN_CODE = 'ebudget.tarik_swakelola_bp.KEGIATAN_CODE';

	
	const TIPE = 'ebudget.tarik_swakelola_bp.TIPE';

	
	const DETAIL_NO = 'ebudget.tarik_swakelola_bp.DETAIL_NO';

	
	const REKENING_CODE = 'ebudget.tarik_swakelola_bp.REKENING_CODE';

	
	const KOMPONEN_ID = 'ebudget.tarik_swakelola_bp.KOMPONEN_ID';

	
	const DETAIL_NAME = 'ebudget.tarik_swakelola_bp.DETAIL_NAME';

	
	const VOLUME = 'ebudget.tarik_swakelola_bp.VOLUME';

	
	const KETERANGAN_KOEFISIEN = 'ebudget.tarik_swakelola_bp.KETERANGAN_KOEFISIEN';

	
	const SUBTITLE = 'ebudget.tarik_swakelola_bp.SUBTITLE';

	
	const KOMPONEN_HARGA = 'ebudget.tarik_swakelola_bp.KOMPONEN_HARGA';

	
	const KOMPONEN_HARGA_AWAL = 'ebudget.tarik_swakelola_bp.KOMPONEN_HARGA_AWAL';

	
	const KOMPONEN_NAME = 'ebudget.tarik_swakelola_bp.KOMPONEN_NAME';

	
	const SATUAN = 'ebudget.tarik_swakelola_bp.SATUAN';

	
	const PAJAK = 'ebudget.tarik_swakelola_bp.PAJAK';

	
	const UNIT_ID = 'ebudget.tarik_swakelola_bp.UNIT_ID';

	
	const FROM_SUB_KEGIATAN = 'ebudget.tarik_swakelola_bp.FROM_SUB_KEGIATAN';

	
	const SUB = 'ebudget.tarik_swakelola_bp.SUB';

	
	const KODE_SUB = 'ebudget.tarik_swakelola_bp.KODE_SUB';

	
	const LAST_UPDATE_USER = 'ebudget.tarik_swakelola_bp.LAST_UPDATE_USER';

	
	const LAST_UPDATE_TIME = 'ebudget.tarik_swakelola_bp.LAST_UPDATE_TIME';

	
	const LAST_UPDATE_IP = 'ebudget.tarik_swakelola_bp.LAST_UPDATE_IP';

	
	const TAHAP = 'ebudget.tarik_swakelola_bp.TAHAP';

	
	const TAHAP_EDIT = 'ebudget.tarik_swakelola_bp.TAHAP_EDIT';

	
	const TAHAP_NEW = 'ebudget.tarik_swakelola_bp.TAHAP_NEW';

	
	const STATUS_LELANG = 'ebudget.tarik_swakelola_bp.STATUS_LELANG';

	
	const NOMOR_LELANG = 'ebudget.tarik_swakelola_bp.NOMOR_LELANG';

	
	const KOEFISIEN_SEMULA = 'ebudget.tarik_swakelola_bp.KOEFISIEN_SEMULA';

	
	const VOLUME_SEMULA = 'ebudget.tarik_swakelola_bp.VOLUME_SEMULA';

	
	const HARGA_SEMULA = 'ebudget.tarik_swakelola_bp.HARGA_SEMULA';

	
	const TOTAL_SEMULA = 'ebudget.tarik_swakelola_bp.TOTAL_SEMULA';

	
	const LOCK_SUBTITLE = 'ebudget.tarik_swakelola_bp.LOCK_SUBTITLE';

	
	const STATUS_HAPUS = 'ebudget.tarik_swakelola_bp.STATUS_HAPUS';

	
	const TAHUN = 'ebudget.tarik_swakelola_bp.TAHUN';

	
	const KODE_LOKASI = 'ebudget.tarik_swakelola_bp.KODE_LOKASI';

	
	const KECAMATAN = 'ebudget.tarik_swakelola_bp.KECAMATAN';

	
	const REKENING_CODE_ASLI = 'ebudget.tarik_swakelola_bp.REKENING_CODE_ASLI';

	
	const NOTE_SKPD = 'ebudget.tarik_swakelola_bp.NOTE_SKPD';

	
	const NOTE_PENELITI = 'ebudget.tarik_swakelola_bp.NOTE_PENELITI';

	
	const NILAI_ANGGARAN = 'ebudget.tarik_swakelola_bp.NILAI_ANGGARAN';

	
	const IS_BLUD = 'ebudget.tarik_swakelola_bp.IS_BLUD';

	
	const LOKASI_KECAMATAN = 'ebudget.tarik_swakelola_bp.LOKASI_KECAMATAN';

	
	const LOKASI_KELURAHAN = 'ebudget.tarik_swakelola_bp.LOKASI_KELURAHAN';

	
	const OB = 'ebudget.tarik_swakelola_bp.OB';

	
	const OB_FROM_ID = 'ebudget.tarik_swakelola_bp.OB_FROM_ID';

	
	const IS_PER_KOMPONEN = 'ebudget.tarik_swakelola_bp.IS_PER_KOMPONEN';

	
	const KEGIATAN_CODE_ASAL = 'ebudget.tarik_swakelola_bp.KEGIATAN_CODE_ASAL';

	
	const TH_KE_MULTIYEARS = 'ebudget.tarik_swakelola_bp.TH_KE_MULTIYEARS';

	
	const HARGA_SEBELUM_SISA_LELANG = 'ebudget.tarik_swakelola_bp.HARGA_SEBELUM_SISA_LELANG';

	
	const IS_MUSRENBANG = 'ebudget.tarik_swakelola_bp.IS_MUSRENBANG';

	
	const SUB_ID_ASAL = 'ebudget.tarik_swakelola_bp.SUB_ID_ASAL';

	
	const SUBTITLE_ASAL = 'ebudget.tarik_swakelola_bp.SUBTITLE_ASAL';

	
	const KODE_SUB_ASAL = 'ebudget.tarik_swakelola_bp.KODE_SUB_ASAL';

	
	const SUB_ASAL = 'ebudget.tarik_swakelola_bp.SUB_ASAL';

	
	const LAST_EDIT_TIME = 'ebudget.tarik_swakelola_bp.LAST_EDIT_TIME';

	
	const IS_POTONG_BPJS = 'ebudget.tarik_swakelola_bp.IS_POTONG_BPJS';

	
	const IS_IURAN_BPJS = 'ebudget.tarik_swakelola_bp.IS_IURAN_BPJS';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('KegiatanCode', 'Tipe', 'DetailNo', 'RekeningCode', 'KomponenId', 'DetailName', 'Volume', 'KeteranganKoefisien', 'Subtitle', 'KomponenHarga', 'KomponenHargaAwal', 'KomponenName', 'Satuan', 'Pajak', 'UnitId', 'FromSubKegiatan', 'Sub', 'KodeSub', 'LastUpdateUser', 'LastUpdateTime', 'LastUpdateIp', 'Tahap', 'TahapEdit', 'TahapNew', 'StatusLelang', 'NomorLelang', 'KoefisienSemula', 'VolumeSemula', 'HargaSemula', 'TotalSemula', 'LockSubtitle', 'StatusHapus', 'Tahun', 'KodeLokasi', 'Kecamatan', 'RekeningCodeAsli', 'NoteSkpd', 'NotePeneliti', 'NilaiAnggaran', 'IsBlud', 'LokasiKecamatan', 'LokasiKelurahan', 'Ob', 'ObFromId', 'IsPerKomponen', 'KegiatanCodeAsal', 'ThKeMultiyears', 'HargaSebelumSisaLelang', 'IsMusrenbang', 'SubIdAsal', 'SubtitleAsal', 'KodeSubAsal', 'SubAsal', 'LastEditTime', 'IsPotongBpjs', 'IsIuranBpjs', ),
		BasePeer::TYPE_COLNAME => array (TarikSwakelolaBpPeer::KEGIATAN_CODE, TarikSwakelolaBpPeer::TIPE, TarikSwakelolaBpPeer::DETAIL_NO, TarikSwakelolaBpPeer::REKENING_CODE, TarikSwakelolaBpPeer::KOMPONEN_ID, TarikSwakelolaBpPeer::DETAIL_NAME, TarikSwakelolaBpPeer::VOLUME, TarikSwakelolaBpPeer::KETERANGAN_KOEFISIEN, TarikSwakelolaBpPeer::SUBTITLE, TarikSwakelolaBpPeer::KOMPONEN_HARGA, TarikSwakelolaBpPeer::KOMPONEN_HARGA_AWAL, TarikSwakelolaBpPeer::KOMPONEN_NAME, TarikSwakelolaBpPeer::SATUAN, TarikSwakelolaBpPeer::PAJAK, TarikSwakelolaBpPeer::UNIT_ID, TarikSwakelolaBpPeer::FROM_SUB_KEGIATAN, TarikSwakelolaBpPeer::SUB, TarikSwakelolaBpPeer::KODE_SUB, TarikSwakelolaBpPeer::LAST_UPDATE_USER, TarikSwakelolaBpPeer::LAST_UPDATE_TIME, TarikSwakelolaBpPeer::LAST_UPDATE_IP, TarikSwakelolaBpPeer::TAHAP, TarikSwakelolaBpPeer::TAHAP_EDIT, TarikSwakelolaBpPeer::TAHAP_NEW, TarikSwakelolaBpPeer::STATUS_LELANG, TarikSwakelolaBpPeer::NOMOR_LELANG, TarikSwakelolaBpPeer::KOEFISIEN_SEMULA, TarikSwakelolaBpPeer::VOLUME_SEMULA, TarikSwakelolaBpPeer::HARGA_SEMULA, TarikSwakelolaBpPeer::TOTAL_SEMULA, TarikSwakelolaBpPeer::LOCK_SUBTITLE, TarikSwakelolaBpPeer::STATUS_HAPUS, TarikSwakelolaBpPeer::TAHUN, TarikSwakelolaBpPeer::KODE_LOKASI, TarikSwakelolaBpPeer::KECAMATAN, TarikSwakelolaBpPeer::REKENING_CODE_ASLI, TarikSwakelolaBpPeer::NOTE_SKPD, TarikSwakelolaBpPeer::NOTE_PENELITI, TarikSwakelolaBpPeer::NILAI_ANGGARAN, TarikSwakelolaBpPeer::IS_BLUD, TarikSwakelolaBpPeer::LOKASI_KECAMATAN, TarikSwakelolaBpPeer::LOKASI_KELURAHAN, TarikSwakelolaBpPeer::OB, TarikSwakelolaBpPeer::OB_FROM_ID, TarikSwakelolaBpPeer::IS_PER_KOMPONEN, TarikSwakelolaBpPeer::KEGIATAN_CODE_ASAL, TarikSwakelolaBpPeer::TH_KE_MULTIYEARS, TarikSwakelolaBpPeer::HARGA_SEBELUM_SISA_LELANG, TarikSwakelolaBpPeer::IS_MUSRENBANG, TarikSwakelolaBpPeer::SUB_ID_ASAL, TarikSwakelolaBpPeer::SUBTITLE_ASAL, TarikSwakelolaBpPeer::KODE_SUB_ASAL, TarikSwakelolaBpPeer::SUB_ASAL, TarikSwakelolaBpPeer::LAST_EDIT_TIME, TarikSwakelolaBpPeer::IS_POTONG_BPJS, TarikSwakelolaBpPeer::IS_IURAN_BPJS, ),
		BasePeer::TYPE_FIELDNAME => array ('kegiatan_code', 'tipe', 'detail_no', 'rekening_code', 'komponen_id', 'detail_name', 'volume', 'keterangan_koefisien', 'subtitle', 'komponen_harga', 'komponen_harga_awal', 'komponen_name', 'satuan', 'pajak', 'unit_id', 'from_sub_kegiatan', 'sub', 'kode_sub', 'last_update_user', 'last_update_time', 'last_update_ip', 'tahap', 'tahap_edit', 'tahap_new', 'status_lelang', 'nomor_lelang', 'koefisien_semula', 'volume_semula', 'harga_semula', 'total_semula', 'lock_subtitle', 'status_hapus', 'tahun', 'kode_lokasi', 'kecamatan', 'rekening_code_asli', 'note_skpd', 'note_peneliti', 'nilai_anggaran', 'is_blud', 'lokasi_kecamatan', 'lokasi_kelurahan', 'ob', 'ob_from_id', 'is_per_komponen', 'kegiatan_code_asal', 'th_ke_multiyears', 'harga_sebelum_sisa_lelang', 'is_musrenbang', 'sub_id_asal', 'subtitle_asal', 'kode_sub_asal', 'sub_asal', 'last_edit_time', 'is_potong_bpjs', 'is_iuran_bpjs', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('KegiatanCode' => 0, 'Tipe' => 1, 'DetailNo' => 2, 'RekeningCode' => 3, 'KomponenId' => 4, 'DetailName' => 5, 'Volume' => 6, 'KeteranganKoefisien' => 7, 'Subtitle' => 8, 'KomponenHarga' => 9, 'KomponenHargaAwal' => 10, 'KomponenName' => 11, 'Satuan' => 12, 'Pajak' => 13, 'UnitId' => 14, 'FromSubKegiatan' => 15, 'Sub' => 16, 'KodeSub' => 17, 'LastUpdateUser' => 18, 'LastUpdateTime' => 19, 'LastUpdateIp' => 20, 'Tahap' => 21, 'TahapEdit' => 22, 'TahapNew' => 23, 'StatusLelang' => 24, 'NomorLelang' => 25, 'KoefisienSemula' => 26, 'VolumeSemula' => 27, 'HargaSemula' => 28, 'TotalSemula' => 29, 'LockSubtitle' => 30, 'StatusHapus' => 31, 'Tahun' => 32, 'KodeLokasi' => 33, 'Kecamatan' => 34, 'RekeningCodeAsli' => 35, 'NoteSkpd' => 36, 'NotePeneliti' => 37, 'NilaiAnggaran' => 38, 'IsBlud' => 39, 'LokasiKecamatan' => 40, 'LokasiKelurahan' => 41, 'Ob' => 42, 'ObFromId' => 43, 'IsPerKomponen' => 44, 'KegiatanCodeAsal' => 45, 'ThKeMultiyears' => 46, 'HargaSebelumSisaLelang' => 47, 'IsMusrenbang' => 48, 'SubIdAsal' => 49, 'SubtitleAsal' => 50, 'KodeSubAsal' => 51, 'SubAsal' => 52, 'LastEditTime' => 53, 'IsPotongBpjs' => 54, 'IsIuranBpjs' => 55, ),
		BasePeer::TYPE_COLNAME => array (TarikSwakelolaBpPeer::KEGIATAN_CODE => 0, TarikSwakelolaBpPeer::TIPE => 1, TarikSwakelolaBpPeer::DETAIL_NO => 2, TarikSwakelolaBpPeer::REKENING_CODE => 3, TarikSwakelolaBpPeer::KOMPONEN_ID => 4, TarikSwakelolaBpPeer::DETAIL_NAME => 5, TarikSwakelolaBpPeer::VOLUME => 6, TarikSwakelolaBpPeer::KETERANGAN_KOEFISIEN => 7, TarikSwakelolaBpPeer::SUBTITLE => 8, TarikSwakelolaBpPeer::KOMPONEN_HARGA => 9, TarikSwakelolaBpPeer::KOMPONEN_HARGA_AWAL => 10, TarikSwakelolaBpPeer::KOMPONEN_NAME => 11, TarikSwakelolaBpPeer::SATUAN => 12, TarikSwakelolaBpPeer::PAJAK => 13, TarikSwakelolaBpPeer::UNIT_ID => 14, TarikSwakelolaBpPeer::FROM_SUB_KEGIATAN => 15, TarikSwakelolaBpPeer::SUB => 16, TarikSwakelolaBpPeer::KODE_SUB => 17, TarikSwakelolaBpPeer::LAST_UPDATE_USER => 18, TarikSwakelolaBpPeer::LAST_UPDATE_TIME => 19, TarikSwakelolaBpPeer::LAST_UPDATE_IP => 20, TarikSwakelolaBpPeer::TAHAP => 21, TarikSwakelolaBpPeer::TAHAP_EDIT => 22, TarikSwakelolaBpPeer::TAHAP_NEW => 23, TarikSwakelolaBpPeer::STATUS_LELANG => 24, TarikSwakelolaBpPeer::NOMOR_LELANG => 25, TarikSwakelolaBpPeer::KOEFISIEN_SEMULA => 26, TarikSwakelolaBpPeer::VOLUME_SEMULA => 27, TarikSwakelolaBpPeer::HARGA_SEMULA => 28, TarikSwakelolaBpPeer::TOTAL_SEMULA => 29, TarikSwakelolaBpPeer::LOCK_SUBTITLE => 30, TarikSwakelolaBpPeer::STATUS_HAPUS => 31, TarikSwakelolaBpPeer::TAHUN => 32, TarikSwakelolaBpPeer::KODE_LOKASI => 33, TarikSwakelolaBpPeer::KECAMATAN => 34, TarikSwakelolaBpPeer::REKENING_CODE_ASLI => 35, TarikSwakelolaBpPeer::NOTE_SKPD => 36, TarikSwakelolaBpPeer::NOTE_PENELITI => 37, TarikSwakelolaBpPeer::NILAI_ANGGARAN => 38, TarikSwakelolaBpPeer::IS_BLUD => 39, TarikSwakelolaBpPeer::LOKASI_KECAMATAN => 40, TarikSwakelolaBpPeer::LOKASI_KELURAHAN => 41, TarikSwakelolaBpPeer::OB => 42, TarikSwakelolaBpPeer::OB_FROM_ID => 43, TarikSwakelolaBpPeer::IS_PER_KOMPONEN => 44, TarikSwakelolaBpPeer::KEGIATAN_CODE_ASAL => 45, TarikSwakelolaBpPeer::TH_KE_MULTIYEARS => 46, TarikSwakelolaBpPeer::HARGA_SEBELUM_SISA_LELANG => 47, TarikSwakelolaBpPeer::IS_MUSRENBANG => 48, TarikSwakelolaBpPeer::SUB_ID_ASAL => 49, TarikSwakelolaBpPeer::SUBTITLE_ASAL => 50, TarikSwakelolaBpPeer::KODE_SUB_ASAL => 51, TarikSwakelolaBpPeer::SUB_ASAL => 52, TarikSwakelolaBpPeer::LAST_EDIT_TIME => 53, TarikSwakelolaBpPeer::IS_POTONG_BPJS => 54, TarikSwakelolaBpPeer::IS_IURAN_BPJS => 55, ),
		BasePeer::TYPE_FIELDNAME => array ('kegiatan_code' => 0, 'tipe' => 1, 'detail_no' => 2, 'rekening_code' => 3, 'komponen_id' => 4, 'detail_name' => 5, 'volume' => 6, 'keterangan_koefisien' => 7, 'subtitle' => 8, 'komponen_harga' => 9, 'komponen_harga_awal' => 10, 'komponen_name' => 11, 'satuan' => 12, 'pajak' => 13, 'unit_id' => 14, 'from_sub_kegiatan' => 15, 'sub' => 16, 'kode_sub' => 17, 'last_update_user' => 18, 'last_update_time' => 19, 'last_update_ip' => 20, 'tahap' => 21, 'tahap_edit' => 22, 'tahap_new' => 23, 'status_lelang' => 24, 'nomor_lelang' => 25, 'koefisien_semula' => 26, 'volume_semula' => 27, 'harga_semula' => 28, 'total_semula' => 29, 'lock_subtitle' => 30, 'status_hapus' => 31, 'tahun' => 32, 'kode_lokasi' => 33, 'kecamatan' => 34, 'rekening_code_asli' => 35, 'note_skpd' => 36, 'note_peneliti' => 37, 'nilai_anggaran' => 38, 'is_blud' => 39, 'lokasi_kecamatan' => 40, 'lokasi_kelurahan' => 41, 'ob' => 42, 'ob_from_id' => 43, 'is_per_komponen' => 44, 'kegiatan_code_asal' => 45, 'th_ke_multiyears' => 46, 'harga_sebelum_sisa_lelang' => 47, 'is_musrenbang' => 48, 'sub_id_asal' => 49, 'subtitle_asal' => 50, 'kode_sub_asal' => 51, 'sub_asal' => 52, 'last_edit_time' => 53, 'is_potong_bpjs' => 54, 'is_iuran_bpjs' => 55, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/TarikSwakelolaBpMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.TarikSwakelolaBpMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = TarikSwakelolaBpPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(TarikSwakelolaBpPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(TarikSwakelolaBpPeer::KEGIATAN_CODE);

		$criteria->addSelectColumn(TarikSwakelolaBpPeer::TIPE);

		$criteria->addSelectColumn(TarikSwakelolaBpPeer::DETAIL_NO);

		$criteria->addSelectColumn(TarikSwakelolaBpPeer::REKENING_CODE);

		$criteria->addSelectColumn(TarikSwakelolaBpPeer::KOMPONEN_ID);

		$criteria->addSelectColumn(TarikSwakelolaBpPeer::DETAIL_NAME);

		$criteria->addSelectColumn(TarikSwakelolaBpPeer::VOLUME);

		$criteria->addSelectColumn(TarikSwakelolaBpPeer::KETERANGAN_KOEFISIEN);

		$criteria->addSelectColumn(TarikSwakelolaBpPeer::SUBTITLE);

		$criteria->addSelectColumn(TarikSwakelolaBpPeer::KOMPONEN_HARGA);

		$criteria->addSelectColumn(TarikSwakelolaBpPeer::KOMPONEN_HARGA_AWAL);

		$criteria->addSelectColumn(TarikSwakelolaBpPeer::KOMPONEN_NAME);

		$criteria->addSelectColumn(TarikSwakelolaBpPeer::SATUAN);

		$criteria->addSelectColumn(TarikSwakelolaBpPeer::PAJAK);

		$criteria->addSelectColumn(TarikSwakelolaBpPeer::UNIT_ID);

		$criteria->addSelectColumn(TarikSwakelolaBpPeer::FROM_SUB_KEGIATAN);

		$criteria->addSelectColumn(TarikSwakelolaBpPeer::SUB);

		$criteria->addSelectColumn(TarikSwakelolaBpPeer::KODE_SUB);

		$criteria->addSelectColumn(TarikSwakelolaBpPeer::LAST_UPDATE_USER);

		$criteria->addSelectColumn(TarikSwakelolaBpPeer::LAST_UPDATE_TIME);

		$criteria->addSelectColumn(TarikSwakelolaBpPeer::LAST_UPDATE_IP);

		$criteria->addSelectColumn(TarikSwakelolaBpPeer::TAHAP);

		$criteria->addSelectColumn(TarikSwakelolaBpPeer::TAHAP_EDIT);

		$criteria->addSelectColumn(TarikSwakelolaBpPeer::TAHAP_NEW);

		$criteria->addSelectColumn(TarikSwakelolaBpPeer::STATUS_LELANG);

		$criteria->addSelectColumn(TarikSwakelolaBpPeer::NOMOR_LELANG);

		$criteria->addSelectColumn(TarikSwakelolaBpPeer::KOEFISIEN_SEMULA);

		$criteria->addSelectColumn(TarikSwakelolaBpPeer::VOLUME_SEMULA);

		$criteria->addSelectColumn(TarikSwakelolaBpPeer::HARGA_SEMULA);

		$criteria->addSelectColumn(TarikSwakelolaBpPeer::TOTAL_SEMULA);

		$criteria->addSelectColumn(TarikSwakelolaBpPeer::LOCK_SUBTITLE);

		$criteria->addSelectColumn(TarikSwakelolaBpPeer::STATUS_HAPUS);

		$criteria->addSelectColumn(TarikSwakelolaBpPeer::TAHUN);

		$criteria->addSelectColumn(TarikSwakelolaBpPeer::KODE_LOKASI);

		$criteria->addSelectColumn(TarikSwakelolaBpPeer::KECAMATAN);

		$criteria->addSelectColumn(TarikSwakelolaBpPeer::REKENING_CODE_ASLI);

		$criteria->addSelectColumn(TarikSwakelolaBpPeer::NOTE_SKPD);

		$criteria->addSelectColumn(TarikSwakelolaBpPeer::NOTE_PENELITI);

		$criteria->addSelectColumn(TarikSwakelolaBpPeer::NILAI_ANGGARAN);

		$criteria->addSelectColumn(TarikSwakelolaBpPeer::IS_BLUD);

		$criteria->addSelectColumn(TarikSwakelolaBpPeer::LOKASI_KECAMATAN);

		$criteria->addSelectColumn(TarikSwakelolaBpPeer::LOKASI_KELURAHAN);

		$criteria->addSelectColumn(TarikSwakelolaBpPeer::OB);

		$criteria->addSelectColumn(TarikSwakelolaBpPeer::OB_FROM_ID);

		$criteria->addSelectColumn(TarikSwakelolaBpPeer::IS_PER_KOMPONEN);

		$criteria->addSelectColumn(TarikSwakelolaBpPeer::KEGIATAN_CODE_ASAL);

		$criteria->addSelectColumn(TarikSwakelolaBpPeer::TH_KE_MULTIYEARS);

		$criteria->addSelectColumn(TarikSwakelolaBpPeer::HARGA_SEBELUM_SISA_LELANG);

		$criteria->addSelectColumn(TarikSwakelolaBpPeer::IS_MUSRENBANG);

		$criteria->addSelectColumn(TarikSwakelolaBpPeer::SUB_ID_ASAL);

		$criteria->addSelectColumn(TarikSwakelolaBpPeer::SUBTITLE_ASAL);

		$criteria->addSelectColumn(TarikSwakelolaBpPeer::KODE_SUB_ASAL);

		$criteria->addSelectColumn(TarikSwakelolaBpPeer::SUB_ASAL);

		$criteria->addSelectColumn(TarikSwakelolaBpPeer::LAST_EDIT_TIME);

		$criteria->addSelectColumn(TarikSwakelolaBpPeer::IS_POTONG_BPJS);

		$criteria->addSelectColumn(TarikSwakelolaBpPeer::IS_IURAN_BPJS);

	}

	const COUNT = 'COUNT(ebudget.tarik_swakelola_bp.KEGIATAN_CODE)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.tarik_swakelola_bp.KEGIATAN_CODE)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(TarikSwakelolaBpPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(TarikSwakelolaBpPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = TarikSwakelolaBpPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = TarikSwakelolaBpPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return TarikSwakelolaBpPeer::populateObjects(TarikSwakelolaBpPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			TarikSwakelolaBpPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = TarikSwakelolaBpPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return TarikSwakelolaBpPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(TarikSwakelolaBpPeer::KEGIATAN_CODE);
			$selectCriteria->add(TarikSwakelolaBpPeer::KEGIATAN_CODE, $criteria->remove(TarikSwakelolaBpPeer::KEGIATAN_CODE), $comparison);

			$comparison = $criteria->getComparison(TarikSwakelolaBpPeer::DETAIL_NO);
			$selectCriteria->add(TarikSwakelolaBpPeer::DETAIL_NO, $criteria->remove(TarikSwakelolaBpPeer::DETAIL_NO), $comparison);

			$comparison = $criteria->getComparison(TarikSwakelolaBpPeer::UNIT_ID);
			$selectCriteria->add(TarikSwakelolaBpPeer::UNIT_ID, $criteria->remove(TarikSwakelolaBpPeer::UNIT_ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(TarikSwakelolaBpPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(TarikSwakelolaBpPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof TarikSwakelolaBp) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
												if(count($values) == count($values, COUNT_RECURSIVE))
			{
								$values = array($values);
			}
			$vals = array();
			foreach($values as $value)
			{

				$vals[0][] = $value[0];
				$vals[1][] = $value[1];
				$vals[2][] = $value[2];
			}

			$criteria->add(TarikSwakelolaBpPeer::KEGIATAN_CODE, $vals[0], Criteria::IN);
			$criteria->add(TarikSwakelolaBpPeer::DETAIL_NO, $vals[1], Criteria::IN);
			$criteria->add(TarikSwakelolaBpPeer::UNIT_ID, $vals[2], Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(TarikSwakelolaBp $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(TarikSwakelolaBpPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(TarikSwakelolaBpPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(TarikSwakelolaBpPeer::DATABASE_NAME, TarikSwakelolaBpPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = TarikSwakelolaBpPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK( $kegiatan_code, $detail_no, $unit_id, $con = null) {
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$criteria = new Criteria();
		$criteria->add(TarikSwakelolaBpPeer::KEGIATAN_CODE, $kegiatan_code);
		$criteria->add(TarikSwakelolaBpPeer::DETAIL_NO, $detail_no);
		$criteria->add(TarikSwakelolaBpPeer::UNIT_ID, $unit_id);
		$v = TarikSwakelolaBpPeer::doSelect($criteria, $con);

		return !empty($v) ? $v[0] : null;
	}
} 
if (Propel::isInit()) {
			try {
		BaseTarikSwakelolaBpPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/TarikSwakelolaBpMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.TarikSwakelolaBpMapBuilder');
}
