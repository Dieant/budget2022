<?php


abstract class BaseRkuaRincianDetailPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.rkua_rincian_detail';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.RkuaRincianDetail';

	
	const NUM_COLUMNS = 70;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const KEGIATAN_CODE = 'ebudget.rkua_rincian_detail.KEGIATAN_CODE';

	
	const TIPE = 'ebudget.rkua_rincian_detail.TIPE';

	
	const DETAIL_NO = 'ebudget.rkua_rincian_detail.DETAIL_NO';

	
	const REKENING_CODE = 'ebudget.rkua_rincian_detail.REKENING_CODE';

	
	const KOMPONEN_ID = 'ebudget.rkua_rincian_detail.KOMPONEN_ID';

	
	const DETAIL_NAME = 'ebudget.rkua_rincian_detail.DETAIL_NAME';

	
	const VOLUME = 'ebudget.rkua_rincian_detail.VOLUME';

	
	const KETERANGAN_KOEFISIEN = 'ebudget.rkua_rincian_detail.KETERANGAN_KOEFISIEN';

	
	const SUBTITLE = 'ebudget.rkua_rincian_detail.SUBTITLE';

	
	const KOMPONEN_HARGA = 'ebudget.rkua_rincian_detail.KOMPONEN_HARGA';

	
	const KOMPONEN_HARGA_AWAL = 'ebudget.rkua_rincian_detail.KOMPONEN_HARGA_AWAL';

	
	const KOMPONEN_NAME = 'ebudget.rkua_rincian_detail.KOMPONEN_NAME';

	
	const SATUAN = 'ebudget.rkua_rincian_detail.SATUAN';

	
	const PAJAK = 'ebudget.rkua_rincian_detail.PAJAK';

	
	const UNIT_ID = 'ebudget.rkua_rincian_detail.UNIT_ID';

	
	const FROM_SUB_KEGIATAN = 'ebudget.rkua_rincian_detail.FROM_SUB_KEGIATAN';

	
	const SUB = 'ebudget.rkua_rincian_detail.SUB';

	
	const KODE_SUB = 'ebudget.rkua_rincian_detail.KODE_SUB';

	
	const LAST_UPDATE_USER = 'ebudget.rkua_rincian_detail.LAST_UPDATE_USER';

	
	const LAST_UPDATE_TIME = 'ebudget.rkua_rincian_detail.LAST_UPDATE_TIME';

	
	const LAST_UPDATE_IP = 'ebudget.rkua_rincian_detail.LAST_UPDATE_IP';

	
	const TAHAP = 'ebudget.rkua_rincian_detail.TAHAP';

	
	const TAHAP_EDIT = 'ebudget.rkua_rincian_detail.TAHAP_EDIT';

	
	const TAHAP_NEW = 'ebudget.rkua_rincian_detail.TAHAP_NEW';

	
	const STATUS_LELANG = 'ebudget.rkua_rincian_detail.STATUS_LELANG';

	
	const NOMOR_LELANG = 'ebudget.rkua_rincian_detail.NOMOR_LELANG';

	
	const KOEFISIEN_SEMULA = 'ebudget.rkua_rincian_detail.KOEFISIEN_SEMULA';

	
	const VOLUME_SEMULA = 'ebudget.rkua_rincian_detail.VOLUME_SEMULA';

	
	const HARGA_SEMULA = 'ebudget.rkua_rincian_detail.HARGA_SEMULA';

	
	const TOTAL_SEMULA = 'ebudget.rkua_rincian_detail.TOTAL_SEMULA';

	
	const LOCK_SUBTITLE = 'ebudget.rkua_rincian_detail.LOCK_SUBTITLE';

	
	const STATUS_HAPUS = 'ebudget.rkua_rincian_detail.STATUS_HAPUS';

	
	const TAHUN = 'ebudget.rkua_rincian_detail.TAHUN';

	
	const KODE_LOKASI = 'ebudget.rkua_rincian_detail.KODE_LOKASI';

	
	const KECAMATAN = 'ebudget.rkua_rincian_detail.KECAMATAN';

	
	const REKENING_CODE_ASLI = 'ebudget.rkua_rincian_detail.REKENING_CODE_ASLI';

	
	const NOTE_SKPD = 'ebudget.rkua_rincian_detail.NOTE_SKPD';

	
	const NOTE_PENELITI = 'ebudget.rkua_rincian_detail.NOTE_PENELITI';

	
	const NILAI_ANGGARAN = 'ebudget.rkua_rincian_detail.NILAI_ANGGARAN';

	
	const IS_BLUD = 'ebudget.rkua_rincian_detail.IS_BLUD';

	
	const LOKASI_KECAMATAN = 'ebudget.rkua_rincian_detail.LOKASI_KECAMATAN';

	
	const LOKASI_KELURAHAN = 'ebudget.rkua_rincian_detail.LOKASI_KELURAHAN';

	
	const OB = 'ebudget.rkua_rincian_detail.OB';

	
	const OB_FROM_ID = 'ebudget.rkua_rincian_detail.OB_FROM_ID';

	
	const IS_PER_KOMPONEN = 'ebudget.rkua_rincian_detail.IS_PER_KOMPONEN';

	
	const KEGIATAN_CODE_ASAL = 'ebudget.rkua_rincian_detail.KEGIATAN_CODE_ASAL';

	
	const TH_KE_MULTIYEARS = 'ebudget.rkua_rincian_detail.TH_KE_MULTIYEARS';

	
	const HARGA_SEBELUM_SISA_LELANG = 'ebudget.rkua_rincian_detail.HARGA_SEBELUM_SISA_LELANG';

	
	const IS_MUSRENBANG = 'ebudget.rkua_rincian_detail.IS_MUSRENBANG';

	
	const SUB_ID_ASAL = 'ebudget.rkua_rincian_detail.SUB_ID_ASAL';

	
	const SUBTITLE_ASAL = 'ebudget.rkua_rincian_detail.SUBTITLE_ASAL';

	
	const KODE_SUB_ASAL = 'ebudget.rkua_rincian_detail.KODE_SUB_ASAL';

	
	const SUB_ASAL = 'ebudget.rkua_rincian_detail.SUB_ASAL';

	
	const LAST_EDIT_TIME = 'ebudget.rkua_rincian_detail.LAST_EDIT_TIME';

	
	const IS_POTONG_BPJS = 'ebudget.rkua_rincian_detail.IS_POTONG_BPJS';

	
	const IS_IURAN_BPJS = 'ebudget.rkua_rincian_detail.IS_IURAN_BPJS';

	
	const STATUS_OB = 'ebudget.rkua_rincian_detail.STATUS_OB';

	
	const OB_PARENT = 'ebudget.rkua_rincian_detail.OB_PARENT';

	
	const OB_ALOKASI_BARU = 'ebudget.rkua_rincian_detail.OB_ALOKASI_BARU';

	
	const IS_HIBAH = 'ebudget.rkua_rincian_detail.IS_HIBAH';

	
	const STATUS_LEVEL = 'ebudget.rkua_rincian_detail.STATUS_LEVEL';

	
	const STATUS_LEVEL_TOLAK = 'ebudget.rkua_rincian_detail.STATUS_LEVEL_TOLAK';

	
	const STATUS_SISIPAN = 'ebudget.rkua_rincian_detail.STATUS_SISIPAN';

	
	const IS_TAPD_SETUJU = 'ebudget.rkua_rincian_detail.IS_TAPD_SETUJU';

	
	const IS_BAPPEKO_SETUJU = 'ebudget.rkua_rincian_detail.IS_BAPPEKO_SETUJU';

	
	const AKRUAL_CODE = 'ebudget.rkua_rincian_detail.AKRUAL_CODE';

	
	const TIPE2 = 'ebudget.rkua_rincian_detail.TIPE2';

	
	const IS_PENYELIA_SETUJU = 'ebudget.rkua_rincian_detail.IS_PENYELIA_SETUJU';

	
	const NOTE_TAPD = 'ebudget.rkua_rincian_detail.NOTE_TAPD';

	
	const NOTE_BAPPEKO = 'ebudget.rkua_rincian_detail.NOTE_BAPPEKO';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('KegiatanCode', 'Tipe', 'DetailNo', 'RekeningCode', 'KomponenId', 'DetailName', 'Volume', 'KeteranganKoefisien', 'Subtitle', 'KomponenHarga', 'KomponenHargaAwal', 'KomponenName', 'Satuan', 'Pajak', 'UnitId', 'FromSubKegiatan', 'Sub', 'KodeSub', 'LastUpdateUser', 'LastUpdateTime', 'LastUpdateIp', 'Tahap', 'TahapEdit', 'TahapNew', 'StatusLelang', 'NomorLelang', 'KoefisienSemula', 'VolumeSemula', 'HargaSemula', 'TotalSemula', 'LockSubtitle', 'StatusHapus', 'Tahun', 'KodeLokasi', 'Kecamatan', 'RekeningCodeAsli', 'NoteSkpd', 'NotePeneliti', 'NilaiAnggaran', 'IsBlud', 'LokasiKecamatan', 'LokasiKelurahan', 'Ob', 'ObFromId', 'IsPerKomponen', 'KegiatanCodeAsal', 'ThKeMultiyears', 'HargaSebelumSisaLelang', 'IsMusrenbang', 'SubIdAsal', 'SubtitleAsal', 'KodeSubAsal', 'SubAsal', 'LastEditTime', 'IsPotongBpjs', 'IsIuranBpjs', 'StatusOb', 'ObParent', 'ObAlokasiBaru', 'IsHibah', 'StatusLevel', 'StatusLevelTolak', 'StatusSisipan', 'IsTapdSetuju', 'IsBappekoSetuju', 'AkrualCode', 'Tipe2', 'IsPenyeliaSetuju', 'NoteTapd', 'NoteBappeko', ),
		BasePeer::TYPE_COLNAME => array (RkuaRincianDetailPeer::KEGIATAN_CODE, RkuaRincianDetailPeer::TIPE, RkuaRincianDetailPeer::DETAIL_NO, RkuaRincianDetailPeer::REKENING_CODE, RkuaRincianDetailPeer::KOMPONEN_ID, RkuaRincianDetailPeer::DETAIL_NAME, RkuaRincianDetailPeer::VOLUME, RkuaRincianDetailPeer::KETERANGAN_KOEFISIEN, RkuaRincianDetailPeer::SUBTITLE, RkuaRincianDetailPeer::KOMPONEN_HARGA, RkuaRincianDetailPeer::KOMPONEN_HARGA_AWAL, RkuaRincianDetailPeer::KOMPONEN_NAME, RkuaRincianDetailPeer::SATUAN, RkuaRincianDetailPeer::PAJAK, RkuaRincianDetailPeer::UNIT_ID, RkuaRincianDetailPeer::FROM_SUB_KEGIATAN, RkuaRincianDetailPeer::SUB, RkuaRincianDetailPeer::KODE_SUB, RkuaRincianDetailPeer::LAST_UPDATE_USER, RkuaRincianDetailPeer::LAST_UPDATE_TIME, RkuaRincianDetailPeer::LAST_UPDATE_IP, RkuaRincianDetailPeer::TAHAP, RkuaRincianDetailPeer::TAHAP_EDIT, RkuaRincianDetailPeer::TAHAP_NEW, RkuaRincianDetailPeer::STATUS_LELANG, RkuaRincianDetailPeer::NOMOR_LELANG, RkuaRincianDetailPeer::KOEFISIEN_SEMULA, RkuaRincianDetailPeer::VOLUME_SEMULA, RkuaRincianDetailPeer::HARGA_SEMULA, RkuaRincianDetailPeer::TOTAL_SEMULA, RkuaRincianDetailPeer::LOCK_SUBTITLE, RkuaRincianDetailPeer::STATUS_HAPUS, RkuaRincianDetailPeer::TAHUN, RkuaRincianDetailPeer::KODE_LOKASI, RkuaRincianDetailPeer::KECAMATAN, RkuaRincianDetailPeer::REKENING_CODE_ASLI, RkuaRincianDetailPeer::NOTE_SKPD, RkuaRincianDetailPeer::NOTE_PENELITI, RkuaRincianDetailPeer::NILAI_ANGGARAN, RkuaRincianDetailPeer::IS_BLUD, RkuaRincianDetailPeer::LOKASI_KECAMATAN, RkuaRincianDetailPeer::LOKASI_KELURAHAN, RkuaRincianDetailPeer::OB, RkuaRincianDetailPeer::OB_FROM_ID, RkuaRincianDetailPeer::IS_PER_KOMPONEN, RkuaRincianDetailPeer::KEGIATAN_CODE_ASAL, RkuaRincianDetailPeer::TH_KE_MULTIYEARS, RkuaRincianDetailPeer::HARGA_SEBELUM_SISA_LELANG, RkuaRincianDetailPeer::IS_MUSRENBANG, RkuaRincianDetailPeer::SUB_ID_ASAL, RkuaRincianDetailPeer::SUBTITLE_ASAL, RkuaRincianDetailPeer::KODE_SUB_ASAL, RkuaRincianDetailPeer::SUB_ASAL, RkuaRincianDetailPeer::LAST_EDIT_TIME, RkuaRincianDetailPeer::IS_POTONG_BPJS, RkuaRincianDetailPeer::IS_IURAN_BPJS, RkuaRincianDetailPeer::STATUS_OB, RkuaRincianDetailPeer::OB_PARENT, RkuaRincianDetailPeer::OB_ALOKASI_BARU, RkuaRincianDetailPeer::IS_HIBAH, RkuaRincianDetailPeer::STATUS_LEVEL, RkuaRincianDetailPeer::STATUS_LEVEL_TOLAK, RkuaRincianDetailPeer::STATUS_SISIPAN, RkuaRincianDetailPeer::IS_TAPD_SETUJU, RkuaRincianDetailPeer::IS_BAPPEKO_SETUJU, RkuaRincianDetailPeer::AKRUAL_CODE, RkuaRincianDetailPeer::TIPE2, RkuaRincianDetailPeer::IS_PENYELIA_SETUJU, RkuaRincianDetailPeer::NOTE_TAPD, RkuaRincianDetailPeer::NOTE_BAPPEKO, ),
		BasePeer::TYPE_FIELDNAME => array ('kegiatan_code', 'tipe', 'detail_no', 'rekening_code', 'komponen_id', 'detail_name', 'volume', 'keterangan_koefisien', 'subtitle', 'komponen_harga', 'komponen_harga_awal', 'komponen_name', 'satuan', 'pajak', 'unit_id', 'from_sub_kegiatan', 'sub', 'kode_sub', 'last_update_user', 'last_update_time', 'last_update_ip', 'tahap', 'tahap_edit', 'tahap_new', 'status_lelang', 'nomor_lelang', 'koefisien_semula', 'volume_semula', 'harga_semula', 'total_semula', 'lock_subtitle', 'status_hapus', 'tahun', 'kode_lokasi', 'kecamatan', 'rekening_code_asli', 'note_skpd', 'note_peneliti', 'nilai_anggaran', 'is_blud', 'lokasi_kecamatan', 'lokasi_kelurahan', 'ob', 'ob_from_id', 'is_per_komponen', 'kegiatan_code_asal', 'th_ke_multiyears', 'harga_sebelum_sisa_lelang', 'is_musrenbang', 'sub_id_asal', 'subtitle_asal', 'kode_sub_asal', 'sub_asal', 'last_edit_time', 'is_potong_bpjs', 'is_iuran_bpjs', 'status_ob', 'ob_parent', 'ob_alokasi_baru', 'is_hibah', 'status_level', 'status_level_tolak', 'status_sisipan', 'is_tapd_setuju', 'is_bappeko_setuju', 'akrual_code', 'tipe2', 'is_penyelia_setuju', 'note_tapd', 'note_bappeko', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('KegiatanCode' => 0, 'Tipe' => 1, 'DetailNo' => 2, 'RekeningCode' => 3, 'KomponenId' => 4, 'DetailName' => 5, 'Volume' => 6, 'KeteranganKoefisien' => 7, 'Subtitle' => 8, 'KomponenHarga' => 9, 'KomponenHargaAwal' => 10, 'KomponenName' => 11, 'Satuan' => 12, 'Pajak' => 13, 'UnitId' => 14, 'FromSubKegiatan' => 15, 'Sub' => 16, 'KodeSub' => 17, 'LastUpdateUser' => 18, 'LastUpdateTime' => 19, 'LastUpdateIp' => 20, 'Tahap' => 21, 'TahapEdit' => 22, 'TahapNew' => 23, 'StatusLelang' => 24, 'NomorLelang' => 25, 'KoefisienSemula' => 26, 'VolumeSemula' => 27, 'HargaSemula' => 28, 'TotalSemula' => 29, 'LockSubtitle' => 30, 'StatusHapus' => 31, 'Tahun' => 32, 'KodeLokasi' => 33, 'Kecamatan' => 34, 'RekeningCodeAsli' => 35, 'NoteSkpd' => 36, 'NotePeneliti' => 37, 'NilaiAnggaran' => 38, 'IsBlud' => 39, 'LokasiKecamatan' => 40, 'LokasiKelurahan' => 41, 'Ob' => 42, 'ObFromId' => 43, 'IsPerKomponen' => 44, 'KegiatanCodeAsal' => 45, 'ThKeMultiyears' => 46, 'HargaSebelumSisaLelang' => 47, 'IsMusrenbang' => 48, 'SubIdAsal' => 49, 'SubtitleAsal' => 50, 'KodeSubAsal' => 51, 'SubAsal' => 52, 'LastEditTime' => 53, 'IsPotongBpjs' => 54, 'IsIuranBpjs' => 55, 'StatusOb' => 56, 'ObParent' => 57, 'ObAlokasiBaru' => 58, 'IsHibah' => 59, 'StatusLevel' => 60, 'StatusLevelTolak' => 61, 'StatusSisipan' => 62, 'IsTapdSetuju' => 63, 'IsBappekoSetuju' => 64, 'AkrualCode' => 65, 'Tipe2' => 66, 'IsPenyeliaSetuju' => 67, 'NoteTapd' => 68, 'NoteBappeko' => 69, ),
		BasePeer::TYPE_COLNAME => array (RkuaRincianDetailPeer::KEGIATAN_CODE => 0, RkuaRincianDetailPeer::TIPE => 1, RkuaRincianDetailPeer::DETAIL_NO => 2, RkuaRincianDetailPeer::REKENING_CODE => 3, RkuaRincianDetailPeer::KOMPONEN_ID => 4, RkuaRincianDetailPeer::DETAIL_NAME => 5, RkuaRincianDetailPeer::VOLUME => 6, RkuaRincianDetailPeer::KETERANGAN_KOEFISIEN => 7, RkuaRincianDetailPeer::SUBTITLE => 8, RkuaRincianDetailPeer::KOMPONEN_HARGA => 9, RkuaRincianDetailPeer::KOMPONEN_HARGA_AWAL => 10, RkuaRincianDetailPeer::KOMPONEN_NAME => 11, RkuaRincianDetailPeer::SATUAN => 12, RkuaRincianDetailPeer::PAJAK => 13, RkuaRincianDetailPeer::UNIT_ID => 14, RkuaRincianDetailPeer::FROM_SUB_KEGIATAN => 15, RkuaRincianDetailPeer::SUB => 16, RkuaRincianDetailPeer::KODE_SUB => 17, RkuaRincianDetailPeer::LAST_UPDATE_USER => 18, RkuaRincianDetailPeer::LAST_UPDATE_TIME => 19, RkuaRincianDetailPeer::LAST_UPDATE_IP => 20, RkuaRincianDetailPeer::TAHAP => 21, RkuaRincianDetailPeer::TAHAP_EDIT => 22, RkuaRincianDetailPeer::TAHAP_NEW => 23, RkuaRincianDetailPeer::STATUS_LELANG => 24, RkuaRincianDetailPeer::NOMOR_LELANG => 25, RkuaRincianDetailPeer::KOEFISIEN_SEMULA => 26, RkuaRincianDetailPeer::VOLUME_SEMULA => 27, RkuaRincianDetailPeer::HARGA_SEMULA => 28, RkuaRincianDetailPeer::TOTAL_SEMULA => 29, RkuaRincianDetailPeer::LOCK_SUBTITLE => 30, RkuaRincianDetailPeer::STATUS_HAPUS => 31, RkuaRincianDetailPeer::TAHUN => 32, RkuaRincianDetailPeer::KODE_LOKASI => 33, RkuaRincianDetailPeer::KECAMATAN => 34, RkuaRincianDetailPeer::REKENING_CODE_ASLI => 35, RkuaRincianDetailPeer::NOTE_SKPD => 36, RkuaRincianDetailPeer::NOTE_PENELITI => 37, RkuaRincianDetailPeer::NILAI_ANGGARAN => 38, RkuaRincianDetailPeer::IS_BLUD => 39, RkuaRincianDetailPeer::LOKASI_KECAMATAN => 40, RkuaRincianDetailPeer::LOKASI_KELURAHAN => 41, RkuaRincianDetailPeer::OB => 42, RkuaRincianDetailPeer::OB_FROM_ID => 43, RkuaRincianDetailPeer::IS_PER_KOMPONEN => 44, RkuaRincianDetailPeer::KEGIATAN_CODE_ASAL => 45, RkuaRincianDetailPeer::TH_KE_MULTIYEARS => 46, RkuaRincianDetailPeer::HARGA_SEBELUM_SISA_LELANG => 47, RkuaRincianDetailPeer::IS_MUSRENBANG => 48, RkuaRincianDetailPeer::SUB_ID_ASAL => 49, RkuaRincianDetailPeer::SUBTITLE_ASAL => 50, RkuaRincianDetailPeer::KODE_SUB_ASAL => 51, RkuaRincianDetailPeer::SUB_ASAL => 52, RkuaRincianDetailPeer::LAST_EDIT_TIME => 53, RkuaRincianDetailPeer::IS_POTONG_BPJS => 54, RkuaRincianDetailPeer::IS_IURAN_BPJS => 55, RkuaRincianDetailPeer::STATUS_OB => 56, RkuaRincianDetailPeer::OB_PARENT => 57, RkuaRincianDetailPeer::OB_ALOKASI_BARU => 58, RkuaRincianDetailPeer::IS_HIBAH => 59, RkuaRincianDetailPeer::STATUS_LEVEL => 60, RkuaRincianDetailPeer::STATUS_LEVEL_TOLAK => 61, RkuaRincianDetailPeer::STATUS_SISIPAN => 62, RkuaRincianDetailPeer::IS_TAPD_SETUJU => 63, RkuaRincianDetailPeer::IS_BAPPEKO_SETUJU => 64, RkuaRincianDetailPeer::AKRUAL_CODE => 65, RkuaRincianDetailPeer::TIPE2 => 66, RkuaRincianDetailPeer::IS_PENYELIA_SETUJU => 67, RkuaRincianDetailPeer::NOTE_TAPD => 68, RkuaRincianDetailPeer::NOTE_BAPPEKO => 69, ),
		BasePeer::TYPE_FIELDNAME => array ('kegiatan_code' => 0, 'tipe' => 1, 'detail_no' => 2, 'rekening_code' => 3, 'komponen_id' => 4, 'detail_name' => 5, 'volume' => 6, 'keterangan_koefisien' => 7, 'subtitle' => 8, 'komponen_harga' => 9, 'komponen_harga_awal' => 10, 'komponen_name' => 11, 'satuan' => 12, 'pajak' => 13, 'unit_id' => 14, 'from_sub_kegiatan' => 15, 'sub' => 16, 'kode_sub' => 17, 'last_update_user' => 18, 'last_update_time' => 19, 'last_update_ip' => 20, 'tahap' => 21, 'tahap_edit' => 22, 'tahap_new' => 23, 'status_lelang' => 24, 'nomor_lelang' => 25, 'koefisien_semula' => 26, 'volume_semula' => 27, 'harga_semula' => 28, 'total_semula' => 29, 'lock_subtitle' => 30, 'status_hapus' => 31, 'tahun' => 32, 'kode_lokasi' => 33, 'kecamatan' => 34, 'rekening_code_asli' => 35, 'note_skpd' => 36, 'note_peneliti' => 37, 'nilai_anggaran' => 38, 'is_blud' => 39, 'lokasi_kecamatan' => 40, 'lokasi_kelurahan' => 41, 'ob' => 42, 'ob_from_id' => 43, 'is_per_komponen' => 44, 'kegiatan_code_asal' => 45, 'th_ke_multiyears' => 46, 'harga_sebelum_sisa_lelang' => 47, 'is_musrenbang' => 48, 'sub_id_asal' => 49, 'subtitle_asal' => 50, 'kode_sub_asal' => 51, 'sub_asal' => 52, 'last_edit_time' => 53, 'is_potong_bpjs' => 54, 'is_iuran_bpjs' => 55, 'status_ob' => 56, 'ob_parent' => 57, 'ob_alokasi_baru' => 58, 'is_hibah' => 59, 'status_level' => 60, 'status_level_tolak' => 61, 'status_sisipan' => 62, 'is_tapd_setuju' => 63, 'is_bappeko_setuju' => 64, 'akrual_code' => 65, 'tipe2' => 66, 'is_penyelia_setuju' => 67, 'note_tapd' => 68, 'note_bappeko' => 69, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/RkuaRincianDetailMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.RkuaRincianDetailMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = RkuaRincianDetailPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(RkuaRincianDetailPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(RkuaRincianDetailPeer::KEGIATAN_CODE);

		$criteria->addSelectColumn(RkuaRincianDetailPeer::TIPE);

		$criteria->addSelectColumn(RkuaRincianDetailPeer::DETAIL_NO);

		$criteria->addSelectColumn(RkuaRincianDetailPeer::REKENING_CODE);

		$criteria->addSelectColumn(RkuaRincianDetailPeer::KOMPONEN_ID);

		$criteria->addSelectColumn(RkuaRincianDetailPeer::DETAIL_NAME);

		$criteria->addSelectColumn(RkuaRincianDetailPeer::VOLUME);

		$criteria->addSelectColumn(RkuaRincianDetailPeer::KETERANGAN_KOEFISIEN);

		$criteria->addSelectColumn(RkuaRincianDetailPeer::SUBTITLE);

		$criteria->addSelectColumn(RkuaRincianDetailPeer::KOMPONEN_HARGA);

		$criteria->addSelectColumn(RkuaRincianDetailPeer::KOMPONEN_HARGA_AWAL);

		$criteria->addSelectColumn(RkuaRincianDetailPeer::KOMPONEN_NAME);

		$criteria->addSelectColumn(RkuaRincianDetailPeer::SATUAN);

		$criteria->addSelectColumn(RkuaRincianDetailPeer::PAJAK);

		$criteria->addSelectColumn(RkuaRincianDetailPeer::UNIT_ID);

		$criteria->addSelectColumn(RkuaRincianDetailPeer::FROM_SUB_KEGIATAN);

		$criteria->addSelectColumn(RkuaRincianDetailPeer::SUB);

		$criteria->addSelectColumn(RkuaRincianDetailPeer::KODE_SUB);

		$criteria->addSelectColumn(RkuaRincianDetailPeer::LAST_UPDATE_USER);

		$criteria->addSelectColumn(RkuaRincianDetailPeer::LAST_UPDATE_TIME);

		$criteria->addSelectColumn(RkuaRincianDetailPeer::LAST_UPDATE_IP);

		$criteria->addSelectColumn(RkuaRincianDetailPeer::TAHAP);

		$criteria->addSelectColumn(RkuaRincianDetailPeer::TAHAP_EDIT);

		$criteria->addSelectColumn(RkuaRincianDetailPeer::TAHAP_NEW);

		$criteria->addSelectColumn(RkuaRincianDetailPeer::STATUS_LELANG);

		$criteria->addSelectColumn(RkuaRincianDetailPeer::NOMOR_LELANG);

		$criteria->addSelectColumn(RkuaRincianDetailPeer::KOEFISIEN_SEMULA);

		$criteria->addSelectColumn(RkuaRincianDetailPeer::VOLUME_SEMULA);

		$criteria->addSelectColumn(RkuaRincianDetailPeer::HARGA_SEMULA);

		$criteria->addSelectColumn(RkuaRincianDetailPeer::TOTAL_SEMULA);

		$criteria->addSelectColumn(RkuaRincianDetailPeer::LOCK_SUBTITLE);

		$criteria->addSelectColumn(RkuaRincianDetailPeer::STATUS_HAPUS);

		$criteria->addSelectColumn(RkuaRincianDetailPeer::TAHUN);

		$criteria->addSelectColumn(RkuaRincianDetailPeer::KODE_LOKASI);

		$criteria->addSelectColumn(RkuaRincianDetailPeer::KECAMATAN);

		$criteria->addSelectColumn(RkuaRincianDetailPeer::REKENING_CODE_ASLI);

		$criteria->addSelectColumn(RkuaRincianDetailPeer::NOTE_SKPD);

		$criteria->addSelectColumn(RkuaRincianDetailPeer::NOTE_PENELITI);

		$criteria->addSelectColumn(RkuaRincianDetailPeer::NILAI_ANGGARAN);

		$criteria->addSelectColumn(RkuaRincianDetailPeer::IS_BLUD);

		$criteria->addSelectColumn(RkuaRincianDetailPeer::LOKASI_KECAMATAN);

		$criteria->addSelectColumn(RkuaRincianDetailPeer::LOKASI_KELURAHAN);

		$criteria->addSelectColumn(RkuaRincianDetailPeer::OB);

		$criteria->addSelectColumn(RkuaRincianDetailPeer::OB_FROM_ID);

		$criteria->addSelectColumn(RkuaRincianDetailPeer::IS_PER_KOMPONEN);

		$criteria->addSelectColumn(RkuaRincianDetailPeer::KEGIATAN_CODE_ASAL);

		$criteria->addSelectColumn(RkuaRincianDetailPeer::TH_KE_MULTIYEARS);

		$criteria->addSelectColumn(RkuaRincianDetailPeer::HARGA_SEBELUM_SISA_LELANG);

		$criteria->addSelectColumn(RkuaRincianDetailPeer::IS_MUSRENBANG);

		$criteria->addSelectColumn(RkuaRincianDetailPeer::SUB_ID_ASAL);

		$criteria->addSelectColumn(RkuaRincianDetailPeer::SUBTITLE_ASAL);

		$criteria->addSelectColumn(RkuaRincianDetailPeer::KODE_SUB_ASAL);

		$criteria->addSelectColumn(RkuaRincianDetailPeer::SUB_ASAL);

		$criteria->addSelectColumn(RkuaRincianDetailPeer::LAST_EDIT_TIME);

		$criteria->addSelectColumn(RkuaRincianDetailPeer::IS_POTONG_BPJS);

		$criteria->addSelectColumn(RkuaRincianDetailPeer::IS_IURAN_BPJS);

		$criteria->addSelectColumn(RkuaRincianDetailPeer::STATUS_OB);

		$criteria->addSelectColumn(RkuaRincianDetailPeer::OB_PARENT);

		$criteria->addSelectColumn(RkuaRincianDetailPeer::OB_ALOKASI_BARU);

		$criteria->addSelectColumn(RkuaRincianDetailPeer::IS_HIBAH);

		$criteria->addSelectColumn(RkuaRincianDetailPeer::STATUS_LEVEL);

		$criteria->addSelectColumn(RkuaRincianDetailPeer::STATUS_LEVEL_TOLAK);

		$criteria->addSelectColumn(RkuaRincianDetailPeer::STATUS_SISIPAN);

		$criteria->addSelectColumn(RkuaRincianDetailPeer::IS_TAPD_SETUJU);

		$criteria->addSelectColumn(RkuaRincianDetailPeer::IS_BAPPEKO_SETUJU);

		$criteria->addSelectColumn(RkuaRincianDetailPeer::AKRUAL_CODE);

		$criteria->addSelectColumn(RkuaRincianDetailPeer::TIPE2);

		$criteria->addSelectColumn(RkuaRincianDetailPeer::IS_PENYELIA_SETUJU);

		$criteria->addSelectColumn(RkuaRincianDetailPeer::NOTE_TAPD);

		$criteria->addSelectColumn(RkuaRincianDetailPeer::NOTE_BAPPEKO);

	}

	const COUNT = 'COUNT(ebudget.rkua_rincian_detail.KEGIATAN_CODE)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.rkua_rincian_detail.KEGIATAN_CODE)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(RkuaRincianDetailPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(RkuaRincianDetailPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = RkuaRincianDetailPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = RkuaRincianDetailPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return RkuaRincianDetailPeer::populateObjects(RkuaRincianDetailPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			RkuaRincianDetailPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = RkuaRincianDetailPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return RkuaRincianDetailPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(RkuaRincianDetailPeer::KEGIATAN_CODE);
			$selectCriteria->add(RkuaRincianDetailPeer::KEGIATAN_CODE, $criteria->remove(RkuaRincianDetailPeer::KEGIATAN_CODE), $comparison);

			$comparison = $criteria->getComparison(RkuaRincianDetailPeer::DETAIL_NO);
			$selectCriteria->add(RkuaRincianDetailPeer::DETAIL_NO, $criteria->remove(RkuaRincianDetailPeer::DETAIL_NO), $comparison);

			$comparison = $criteria->getComparison(RkuaRincianDetailPeer::UNIT_ID);
			$selectCriteria->add(RkuaRincianDetailPeer::UNIT_ID, $criteria->remove(RkuaRincianDetailPeer::UNIT_ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(RkuaRincianDetailPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(RkuaRincianDetailPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof RkuaRincianDetail) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
												if(count($values) == count($values, COUNT_RECURSIVE))
			{
								$values = array($values);
			}
			$vals = array();
			foreach($values as $value)
			{

				$vals[0][] = $value[0];
				$vals[1][] = $value[1];
				$vals[2][] = $value[2];
			}

			$criteria->add(RkuaRincianDetailPeer::KEGIATAN_CODE, $vals[0], Criteria::IN);
			$criteria->add(RkuaRincianDetailPeer::DETAIL_NO, $vals[1], Criteria::IN);
			$criteria->add(RkuaRincianDetailPeer::UNIT_ID, $vals[2], Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(RkuaRincianDetail $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(RkuaRincianDetailPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(RkuaRincianDetailPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(RkuaRincianDetailPeer::DATABASE_NAME, RkuaRincianDetailPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = RkuaRincianDetailPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK( $kegiatan_code, $detail_no, $unit_id, $con = null) {
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$criteria = new Criteria();
		$criteria->add(RkuaRincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
		$criteria->add(RkuaRincianDetailPeer::DETAIL_NO, $detail_no);
		$criteria->add(RkuaRincianDetailPeer::UNIT_ID, $unit_id);
		$v = RkuaRincianDetailPeer::doSelect($criteria, $con);

		return !empty($v) ? $v[0] : null;
	}
} 
if (Propel::isInit()) {
			try {
		BaseRkuaRincianDetailPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/RkuaRincianDetailMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.RkuaRincianDetailMapBuilder');
}
