<?php


abstract class BaseSatuan extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $satuan_id;


	
	protected $user_id;


	
	protected $satuan_name;


	
	protected $ip_address;


	
	protected $waktu_access;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getSatuanId()
	{

		return $this->satuan_id;
	}

	
	public function getUserId()
	{

		return $this->user_id;
	}

	
	public function getSatuanName()
	{

		return $this->satuan_name;
	}

	
	public function getIpAddress()
	{

		return $this->ip_address;
	}

	
	public function getWaktuAccess($format = 'Y-m-d H:i:s')
	{

		if ($this->waktu_access === null || $this->waktu_access === '') {
			return null;
		} elseif (!is_int($this->waktu_access)) {
						$ts = strtotime($this->waktu_access);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [waktu_access] as date/time value: " . var_export($this->waktu_access, true));
			}
		} else {
			$ts = $this->waktu_access;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function setSatuanId($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->satuan_id !== $v) {
			$this->satuan_id = $v;
			$this->modifiedColumns[] = SatuanPeer::SATUAN_ID;
		}

	} 
	
	public function setUserId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->user_id !== $v) {
			$this->user_id = $v;
			$this->modifiedColumns[] = SatuanPeer::USER_ID;
		}

	} 
	
	public function setSatuanName($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->satuan_name !== $v) {
			$this->satuan_name = $v;
			$this->modifiedColumns[] = SatuanPeer::SATUAN_NAME;
		}

	} 
	
	public function setIpAddress($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->ip_address !== $v) {
			$this->ip_address = $v;
			$this->modifiedColumns[] = SatuanPeer::IP_ADDRESS;
		}

	} 
	
	public function setWaktuAccess($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [waktu_access] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->waktu_access !== $ts) {
			$this->waktu_access = $ts;
			$this->modifiedColumns[] = SatuanPeer::WAKTU_ACCESS;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->satuan_id = $rs->getInt($startcol + 0);

			$this->user_id = $rs->getString($startcol + 1);

			$this->satuan_name = $rs->getString($startcol + 2);

			$this->ip_address = $rs->getString($startcol + 3);

			$this->waktu_access = $rs->getTimestamp($startcol + 4, null);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 5; 
		} catch (Exception $e) {
			throw new PropelException("Error populating Satuan object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(SatuanPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			SatuanPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(SatuanPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = SatuanPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += SatuanPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = SatuanPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = SatuanPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getSatuanId();
				break;
			case 1:
				return $this->getUserId();
				break;
			case 2:
				return $this->getSatuanName();
				break;
			case 3:
				return $this->getIpAddress();
				break;
			case 4:
				return $this->getWaktuAccess();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = SatuanPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getSatuanId(),
			$keys[1] => $this->getUserId(),
			$keys[2] => $this->getSatuanName(),
			$keys[3] => $this->getIpAddress(),
			$keys[4] => $this->getWaktuAccess(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = SatuanPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setSatuanId($value);
				break;
			case 1:
				$this->setUserId($value);
				break;
			case 2:
				$this->setSatuanName($value);
				break;
			case 3:
				$this->setIpAddress($value);
				break;
			case 4:
				$this->setWaktuAccess($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = SatuanPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setSatuanId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setUserId($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setSatuanName($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setIpAddress($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setWaktuAccess($arr[$keys[4]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(SatuanPeer::DATABASE_NAME);

		if ($this->isColumnModified(SatuanPeer::SATUAN_ID)) $criteria->add(SatuanPeer::SATUAN_ID, $this->satuan_id);
		if ($this->isColumnModified(SatuanPeer::USER_ID)) $criteria->add(SatuanPeer::USER_ID, $this->user_id);
		if ($this->isColumnModified(SatuanPeer::SATUAN_NAME)) $criteria->add(SatuanPeer::SATUAN_NAME, $this->satuan_name);
		if ($this->isColumnModified(SatuanPeer::IP_ADDRESS)) $criteria->add(SatuanPeer::IP_ADDRESS, $this->ip_address);
		if ($this->isColumnModified(SatuanPeer::WAKTU_ACCESS)) $criteria->add(SatuanPeer::WAKTU_ACCESS, $this->waktu_access);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(SatuanPeer::DATABASE_NAME);

		$criteria->add(SatuanPeer::SATUAN_ID, $this->satuan_id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getSatuanId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setSatuanId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setUserId($this->user_id);

		$copyObj->setSatuanName($this->satuan_name);

		$copyObj->setIpAddress($this->ip_address);

		$copyObj->setWaktuAccess($this->waktu_access);


		$copyObj->setNew(true);

		$copyObj->setSatuanId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new SatuanPeer();
		}
		return self::$peer;
	}

} 