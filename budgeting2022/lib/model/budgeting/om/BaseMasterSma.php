<?php


abstract class BaseMasterSma extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $kode;


	
	protected $nama_sma;


	
	protected $alamat;


	
	protected $status;


	
	protected $kecamatan;


	
	protected $kelurahan;


	
	protected $kode_jalan;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getKode()
	{

		return $this->kode;
	}

	
	public function getNamaSma()
	{

		return $this->nama_sma;
	}

	
	public function getAlamat()
	{

		return $this->alamat;
	}

	
	public function getStatus()
	{

		return $this->status;
	}

	
	public function getKecamatan()
	{

		return $this->kecamatan;
	}

	
	public function getKelurahan()
	{

		return $this->kelurahan;
	}

	
	public function getKodeJalan()
	{

		return $this->kode_jalan;
	}

	
	public function setKode($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode !== $v) {
			$this->kode = $v;
			$this->modifiedColumns[] = MasterSmaPeer::KODE;
		}

	} 
	
	public function setNamaSma($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nama_sma !== $v) {
			$this->nama_sma = $v;
			$this->modifiedColumns[] = MasterSmaPeer::NAMA_SMA;
		}

	} 
	
	public function setAlamat($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->alamat !== $v) {
			$this->alamat = $v;
			$this->modifiedColumns[] = MasterSmaPeer::ALAMAT;
		}

	} 
	
	public function setStatus($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->status !== $v) {
			$this->status = $v;
			$this->modifiedColumns[] = MasterSmaPeer::STATUS;
		}

	} 
	
	public function setKecamatan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kecamatan !== $v) {
			$this->kecamatan = $v;
			$this->modifiedColumns[] = MasterSmaPeer::KECAMATAN;
		}

	} 
	
	public function setKelurahan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kelurahan !== $v) {
			$this->kelurahan = $v;
			$this->modifiedColumns[] = MasterSmaPeer::KELURAHAN;
		}

	} 
	
	public function setKodeJalan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_jalan !== $v) {
			$this->kode_jalan = $v;
			$this->modifiedColumns[] = MasterSmaPeer::KODE_JALAN;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->kode = $rs->getString($startcol + 0);

			$this->nama_sma = $rs->getString($startcol + 1);

			$this->alamat = $rs->getString($startcol + 2);

			$this->status = $rs->getString($startcol + 3);

			$this->kecamatan = $rs->getString($startcol + 4);

			$this->kelurahan = $rs->getString($startcol + 5);

			$this->kode_jalan = $rs->getString($startcol + 6);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 7; 
		} catch (Exception $e) {
			throw new PropelException("Error populating MasterSma object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(MasterSmaPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			MasterSmaPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(MasterSmaPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = MasterSmaPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += MasterSmaPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = MasterSmaPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = MasterSmaPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getKode();
				break;
			case 1:
				return $this->getNamaSma();
				break;
			case 2:
				return $this->getAlamat();
				break;
			case 3:
				return $this->getStatus();
				break;
			case 4:
				return $this->getKecamatan();
				break;
			case 5:
				return $this->getKelurahan();
				break;
			case 6:
				return $this->getKodeJalan();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = MasterSmaPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getKode(),
			$keys[1] => $this->getNamaSma(),
			$keys[2] => $this->getAlamat(),
			$keys[3] => $this->getStatus(),
			$keys[4] => $this->getKecamatan(),
			$keys[5] => $this->getKelurahan(),
			$keys[6] => $this->getKodeJalan(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = MasterSmaPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setKode($value);
				break;
			case 1:
				$this->setNamaSma($value);
				break;
			case 2:
				$this->setAlamat($value);
				break;
			case 3:
				$this->setStatus($value);
				break;
			case 4:
				$this->setKecamatan($value);
				break;
			case 5:
				$this->setKelurahan($value);
				break;
			case 6:
				$this->setKodeJalan($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = MasterSmaPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setKode($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setNamaSma($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setAlamat($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setStatus($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setKecamatan($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setKelurahan($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setKodeJalan($arr[$keys[6]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(MasterSmaPeer::DATABASE_NAME);

		if ($this->isColumnModified(MasterSmaPeer::KODE)) $criteria->add(MasterSmaPeer::KODE, $this->kode);
		if ($this->isColumnModified(MasterSmaPeer::NAMA_SMA)) $criteria->add(MasterSmaPeer::NAMA_SMA, $this->nama_sma);
		if ($this->isColumnModified(MasterSmaPeer::ALAMAT)) $criteria->add(MasterSmaPeer::ALAMAT, $this->alamat);
		if ($this->isColumnModified(MasterSmaPeer::STATUS)) $criteria->add(MasterSmaPeer::STATUS, $this->status);
		if ($this->isColumnModified(MasterSmaPeer::KECAMATAN)) $criteria->add(MasterSmaPeer::KECAMATAN, $this->kecamatan);
		if ($this->isColumnModified(MasterSmaPeer::KELURAHAN)) $criteria->add(MasterSmaPeer::KELURAHAN, $this->kelurahan);
		if ($this->isColumnModified(MasterSmaPeer::KODE_JALAN)) $criteria->add(MasterSmaPeer::KODE_JALAN, $this->kode_jalan);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(MasterSmaPeer::DATABASE_NAME);

		$criteria->add(MasterSmaPeer::KODE, $this->kode);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getKode();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setKode($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setNamaSma($this->nama_sma);

		$copyObj->setAlamat($this->alamat);

		$copyObj->setStatus($this->status);

		$copyObj->setKecamatan($this->kecamatan);

		$copyObj->setKelurahan($this->kelurahan);

		$copyObj->setKodeJalan($this->kode_jalan);


		$copyObj->setNew(true);

		$copyObj->setKode(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new MasterSmaPeer();
		}
		return self::$peer;
	}

} 