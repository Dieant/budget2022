<?php


abstract class BaseUsulanTokenPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.usulan_token';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.UsulanToken';

	
	const NUM_COLUMNS = 11;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const ID_TOKEN = 'ebudget.usulan_token.ID_TOKEN';

	
	const REQUIRED_AT = 'ebudget.usulan_token.REQUIRED_AT';

	
	const EXPIRED_AT = 'ebudget.usulan_token.EXPIRED_AT';

	
	const USED_AT = 'ebudget.usulan_token.USED_AT';

	
	const PENYELIA = 'ebudget.usulan_token.PENYELIA';

	
	const UNIT_ID = 'ebudget.usulan_token.UNIT_ID';

	
	const CREATED_BY = 'ebudget.usulan_token.CREATED_BY';

	
	const JUMLAH_SSH = 'ebudget.usulan_token.JUMLAH_SSH';

	
	const TOKEN = 'ebudget.usulan_token.TOKEN';

	
	const STATUS_USED = 'ebudget.usulan_token.STATUS_USED';

	
	const NOMOR_SURAT = 'ebudget.usulan_token.NOMOR_SURAT';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('IdToken', 'RequiredAt', 'ExpiredAt', 'UsedAt', 'Penyelia', 'UnitId', 'CreatedBy', 'JumlahSsh', 'Token', 'StatusUsed', 'NomorSurat', ),
		BasePeer::TYPE_COLNAME => array (UsulanTokenPeer::ID_TOKEN, UsulanTokenPeer::REQUIRED_AT, UsulanTokenPeer::EXPIRED_AT, UsulanTokenPeer::USED_AT, UsulanTokenPeer::PENYELIA, UsulanTokenPeer::UNIT_ID, UsulanTokenPeer::CREATED_BY, UsulanTokenPeer::JUMLAH_SSH, UsulanTokenPeer::TOKEN, UsulanTokenPeer::STATUS_USED, UsulanTokenPeer::NOMOR_SURAT, ),
		BasePeer::TYPE_FIELDNAME => array ('id_token', 'required_at', 'expired_at', 'used_at', 'penyelia', 'unit_id', 'created_by', 'jumlah_ssh', 'token', 'status_used', 'nomor_surat', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('IdToken' => 0, 'RequiredAt' => 1, 'ExpiredAt' => 2, 'UsedAt' => 3, 'Penyelia' => 4, 'UnitId' => 5, 'CreatedBy' => 6, 'JumlahSsh' => 7, 'Token' => 8, 'StatusUsed' => 9, 'NomorSurat' => 10, ),
		BasePeer::TYPE_COLNAME => array (UsulanTokenPeer::ID_TOKEN => 0, UsulanTokenPeer::REQUIRED_AT => 1, UsulanTokenPeer::EXPIRED_AT => 2, UsulanTokenPeer::USED_AT => 3, UsulanTokenPeer::PENYELIA => 4, UsulanTokenPeer::UNIT_ID => 5, UsulanTokenPeer::CREATED_BY => 6, UsulanTokenPeer::JUMLAH_SSH => 7, UsulanTokenPeer::TOKEN => 8, UsulanTokenPeer::STATUS_USED => 9, UsulanTokenPeer::NOMOR_SURAT => 10, ),
		BasePeer::TYPE_FIELDNAME => array ('id_token' => 0, 'required_at' => 1, 'expired_at' => 2, 'used_at' => 3, 'penyelia' => 4, 'unit_id' => 5, 'created_by' => 6, 'jumlah_ssh' => 7, 'token' => 8, 'status_used' => 9, 'nomor_surat' => 10, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/UsulanTokenMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.UsulanTokenMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = UsulanTokenPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(UsulanTokenPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(UsulanTokenPeer::ID_TOKEN);

		$criteria->addSelectColumn(UsulanTokenPeer::REQUIRED_AT);

		$criteria->addSelectColumn(UsulanTokenPeer::EXPIRED_AT);

		$criteria->addSelectColumn(UsulanTokenPeer::USED_AT);

		$criteria->addSelectColumn(UsulanTokenPeer::PENYELIA);

		$criteria->addSelectColumn(UsulanTokenPeer::UNIT_ID);

		$criteria->addSelectColumn(UsulanTokenPeer::CREATED_BY);

		$criteria->addSelectColumn(UsulanTokenPeer::JUMLAH_SSH);

		$criteria->addSelectColumn(UsulanTokenPeer::TOKEN);

		$criteria->addSelectColumn(UsulanTokenPeer::STATUS_USED);

		$criteria->addSelectColumn(UsulanTokenPeer::NOMOR_SURAT);

	}

	const COUNT = 'COUNT(ebudget.usulan_token.ID_TOKEN)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.usulan_token.ID_TOKEN)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(UsulanTokenPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(UsulanTokenPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = UsulanTokenPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = UsulanTokenPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return UsulanTokenPeer::populateObjects(UsulanTokenPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			UsulanTokenPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = UsulanTokenPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return UsulanTokenPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(UsulanTokenPeer::ID_TOKEN);
			$selectCriteria->add(UsulanTokenPeer::ID_TOKEN, $criteria->remove(UsulanTokenPeer::ID_TOKEN), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(UsulanTokenPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(UsulanTokenPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof UsulanToken) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(UsulanTokenPeer::ID_TOKEN, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(UsulanToken $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(UsulanTokenPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(UsulanTokenPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(UsulanTokenPeer::DATABASE_NAME, UsulanTokenPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = UsulanTokenPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(UsulanTokenPeer::DATABASE_NAME);

		$criteria->add(UsulanTokenPeer::ID_TOKEN, $pk);


		$v = UsulanTokenPeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(UsulanTokenPeer::ID_TOKEN, $pks, Criteria::IN);
			$objs = UsulanTokenPeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BaseUsulanTokenPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/UsulanTokenMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.UsulanTokenMapBuilder');
}
