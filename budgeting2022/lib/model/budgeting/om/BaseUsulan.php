<?php


abstract class BaseUsulan extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $dari;


	
	protected $pekerjaan;


	
	protected $kecamatan;


	
	protected $kelurahan;


	
	protected $nama;


	
	protected $tipe;


	
	protected $lokasi;


	
	protected $volume;


	
	protected $keterangan;


	
	protected $dana;


	
	protected $skpd;


	
	protected $tahap;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getDari()
	{

		return $this->dari;
	}

	
	public function getPekerjaan()
	{

		return $this->pekerjaan;
	}

	
	public function getKecamatan()
	{

		return $this->kecamatan;
	}

	
	public function getKelurahan()
	{

		return $this->kelurahan;
	}

	
	public function getNama()
	{

		return $this->nama;
	}

	
	public function getTipe()
	{

		return $this->tipe;
	}

	
	public function getLokasi()
	{

		return $this->lokasi;
	}

	
	public function getVolume()
	{

		return $this->volume;
	}

	
	public function getKeterangan()
	{

		return $this->keterangan;
	}

	
	public function getDana()
	{

		return $this->dana;
	}

	
	public function getSkpd()
	{

		return $this->skpd;
	}

	
	public function getTahap()
	{

		return $this->tahap;
	}

	
	public function setDari($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->dari !== $v) {
			$this->dari = $v;
			$this->modifiedColumns[] = UsulanPeer::DARI;
		}

	} 
	
	public function setPekerjaan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->pekerjaan !== $v) {
			$this->pekerjaan = $v;
			$this->modifiedColumns[] = UsulanPeer::PEKERJAAN;
		}

	} 
	
	public function setKecamatan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kecamatan !== $v) {
			$this->kecamatan = $v;
			$this->modifiedColumns[] = UsulanPeer::KECAMATAN;
		}

	} 
	
	public function setKelurahan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kelurahan !== $v) {
			$this->kelurahan = $v;
			$this->modifiedColumns[] = UsulanPeer::KELURAHAN;
		}

	} 
	
	public function setNama($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nama !== $v) {
			$this->nama = $v;
			$this->modifiedColumns[] = UsulanPeer::NAMA;
		}

	} 
	
	public function setTipe($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->tipe !== $v) {
			$this->tipe = $v;
			$this->modifiedColumns[] = UsulanPeer::TIPE;
		}

	} 
	
	public function setLokasi($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->lokasi !== $v) {
			$this->lokasi = $v;
			$this->modifiedColumns[] = UsulanPeer::LOKASI;
		}

	} 
	
	public function setVolume($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->volume !== $v) {
			$this->volume = $v;
			$this->modifiedColumns[] = UsulanPeer::VOLUME;
		}

	} 
	
	public function setKeterangan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->keterangan !== $v) {
			$this->keterangan = $v;
			$this->modifiedColumns[] = UsulanPeer::KETERANGAN;
		}

	} 
	
	public function setDana($v)
	{

		if ($this->dana !== $v) {
			$this->dana = $v;
			$this->modifiedColumns[] = UsulanPeer::DANA;
		}

	} 
	
	public function setSkpd($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->skpd !== $v) {
			$this->skpd = $v;
			$this->modifiedColumns[] = UsulanPeer::SKPD;
		}

	} 
	
	public function setTahap($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->tahap !== $v) {
			$this->tahap = $v;
			$this->modifiedColumns[] = UsulanPeer::TAHAP;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->dari = $rs->getString($startcol + 0);

			$this->pekerjaan = $rs->getString($startcol + 1);

			$this->kecamatan = $rs->getString($startcol + 2);

			$this->kelurahan = $rs->getString($startcol + 3);

			$this->nama = $rs->getString($startcol + 4);

			$this->tipe = $rs->getString($startcol + 5);

			$this->lokasi = $rs->getString($startcol + 6);

			$this->volume = $rs->getString($startcol + 7);

			$this->keterangan = $rs->getString($startcol + 8);

			$this->dana = $rs->getFloat($startcol + 9);

			$this->skpd = $rs->getString($startcol + 10);

			$this->tahap = $rs->getInt($startcol + 11);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 12; 
		} catch (Exception $e) {
			throw new PropelException("Error populating Usulan object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(UsulanPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			UsulanPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(UsulanPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = UsulanPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += UsulanPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = UsulanPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = UsulanPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getDari();
				break;
			case 1:
				return $this->getPekerjaan();
				break;
			case 2:
				return $this->getKecamatan();
				break;
			case 3:
				return $this->getKelurahan();
				break;
			case 4:
				return $this->getNama();
				break;
			case 5:
				return $this->getTipe();
				break;
			case 6:
				return $this->getLokasi();
				break;
			case 7:
				return $this->getVolume();
				break;
			case 8:
				return $this->getKeterangan();
				break;
			case 9:
				return $this->getDana();
				break;
			case 10:
				return $this->getSkpd();
				break;
			case 11:
				return $this->getTahap();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = UsulanPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getDari(),
			$keys[1] => $this->getPekerjaan(),
			$keys[2] => $this->getKecamatan(),
			$keys[3] => $this->getKelurahan(),
			$keys[4] => $this->getNama(),
			$keys[5] => $this->getTipe(),
			$keys[6] => $this->getLokasi(),
			$keys[7] => $this->getVolume(),
			$keys[8] => $this->getKeterangan(),
			$keys[9] => $this->getDana(),
			$keys[10] => $this->getSkpd(),
			$keys[11] => $this->getTahap(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = UsulanPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setDari($value);
				break;
			case 1:
				$this->setPekerjaan($value);
				break;
			case 2:
				$this->setKecamatan($value);
				break;
			case 3:
				$this->setKelurahan($value);
				break;
			case 4:
				$this->setNama($value);
				break;
			case 5:
				$this->setTipe($value);
				break;
			case 6:
				$this->setLokasi($value);
				break;
			case 7:
				$this->setVolume($value);
				break;
			case 8:
				$this->setKeterangan($value);
				break;
			case 9:
				$this->setDana($value);
				break;
			case 10:
				$this->setSkpd($value);
				break;
			case 11:
				$this->setTahap($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = UsulanPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setDari($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setPekerjaan($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setKecamatan($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setKelurahan($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setNama($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setTipe($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setLokasi($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setVolume($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setKeterangan($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setDana($arr[$keys[9]]);
		if (array_key_exists($keys[10], $arr)) $this->setSkpd($arr[$keys[10]]);
		if (array_key_exists($keys[11], $arr)) $this->setTahap($arr[$keys[11]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(UsulanPeer::DATABASE_NAME);

		if ($this->isColumnModified(UsulanPeer::DARI)) $criteria->add(UsulanPeer::DARI, $this->dari);
		if ($this->isColumnModified(UsulanPeer::PEKERJAAN)) $criteria->add(UsulanPeer::PEKERJAAN, $this->pekerjaan);
		if ($this->isColumnModified(UsulanPeer::KECAMATAN)) $criteria->add(UsulanPeer::KECAMATAN, $this->kecamatan);
		if ($this->isColumnModified(UsulanPeer::KELURAHAN)) $criteria->add(UsulanPeer::KELURAHAN, $this->kelurahan);
		if ($this->isColumnModified(UsulanPeer::NAMA)) $criteria->add(UsulanPeer::NAMA, $this->nama);
		if ($this->isColumnModified(UsulanPeer::TIPE)) $criteria->add(UsulanPeer::TIPE, $this->tipe);
		if ($this->isColumnModified(UsulanPeer::LOKASI)) $criteria->add(UsulanPeer::LOKASI, $this->lokasi);
		if ($this->isColumnModified(UsulanPeer::VOLUME)) $criteria->add(UsulanPeer::VOLUME, $this->volume);
		if ($this->isColumnModified(UsulanPeer::KETERANGAN)) $criteria->add(UsulanPeer::KETERANGAN, $this->keterangan);
		if ($this->isColumnModified(UsulanPeer::DANA)) $criteria->add(UsulanPeer::DANA, $this->dana);
		if ($this->isColumnModified(UsulanPeer::SKPD)) $criteria->add(UsulanPeer::SKPD, $this->skpd);
		if ($this->isColumnModified(UsulanPeer::TAHAP)) $criteria->add(UsulanPeer::TAHAP, $this->tahap);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(UsulanPeer::DATABASE_NAME);


		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return null;
	}

	
	 public function setPrimaryKey($pk)
	 {
		 	 }

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setDari($this->dari);

		$copyObj->setPekerjaan($this->pekerjaan);

		$copyObj->setKecamatan($this->kecamatan);

		$copyObj->setKelurahan($this->kelurahan);

		$copyObj->setNama($this->nama);

		$copyObj->setTipe($this->tipe);

		$copyObj->setLokasi($this->lokasi);

		$copyObj->setVolume($this->volume);

		$copyObj->setKeterangan($this->keterangan);

		$copyObj->setDana($this->dana);

		$copyObj->setSkpd($this->skpd);

		$copyObj->setTahap($this->tahap);


		$copyObj->setNew(true);

	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new UsulanPeer();
		}
		return self::$peer;
	}

} 