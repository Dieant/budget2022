<?php


abstract class BaseGeojsonlokasiWaitinglistPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'gis_ebudget.geojsonlokasi_waitinglist';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.GeojsonlokasiWaitinglist';

	
	const NUM_COLUMNS = 23;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const IDGEOLOKASI = 'gis_ebudget.geojsonlokasi_waitinglist.IDGEOLOKASI';

	
	const UNIT_ID = 'gis_ebudget.geojsonlokasi_waitinglist.UNIT_ID';

	
	const KEGIATAN_CODE = 'gis_ebudget.geojsonlokasi_waitinglist.KEGIATAN_CODE';

	
	const ID_WAITING = 'gis_ebudget.geojsonlokasi_waitinglist.ID_WAITING';

	
	const SATUAN = 'gis_ebudget.geojsonlokasi_waitinglist.SATUAN';

	
	const VOLUME = 'gis_ebudget.geojsonlokasi_waitinglist.VOLUME';

	
	const NILAI_ANGGARAN = 'gis_ebudget.geojsonlokasi_waitinglist.NILAI_ANGGARAN';

	
	const TAHUN = 'gis_ebudget.geojsonlokasi_waitinglist.TAHUN';

	
	const MLOKASI = 'gis_ebudget.geojsonlokasi_waitinglist.MLOKASI';

	
	const ID_KELOMPOK = 'gis_ebudget.geojsonlokasi_waitinglist.ID_KELOMPOK';

	
	const GEOJSON = 'gis_ebudget.geojsonlokasi_waitinglist.GEOJSON';

	
	const KETERANGAN = 'gis_ebudget.geojsonlokasi_waitinglist.KETERANGAN';

	
	const NMUSER = 'gis_ebudget.geojsonlokasi_waitinglist.NMUSER';

	
	const LEVEL = 'gis_ebudget.geojsonlokasi_waitinglist.LEVEL';

	
	const KOMPONEN_NAME = 'gis_ebudget.geojsonlokasi_waitinglist.KOMPONEN_NAME';

	
	const STATUS_HAPUS = 'gis_ebudget.geojsonlokasi_waitinglist.STATUS_HAPUS';

	
	const KETERANGAN_ALAMAT = 'gis_ebudget.geojsonlokasi_waitinglist.KETERANGAN_ALAMAT';

	
	const UNIT_NAME = 'gis_ebudget.geojsonlokasi_waitinglist.UNIT_NAME';

	
	const LAST_EDIT_TIME = 'gis_ebudget.geojsonlokasi_waitinglist.LAST_EDIT_TIME';

	
	const LAST_CREATE_TIME = 'gis_ebudget.geojsonlokasi_waitinglist.LAST_CREATE_TIME';

	
	const KOORDINAT = 'gis_ebudget.geojsonlokasi_waitinglist.KOORDINAT';

	
	const LOKASI_KE = 'gis_ebudget.geojsonlokasi_waitinglist.LOKASI_KE';

	
	const KODE_DETAIL_KEGIATAN = 'gis_ebudget.geojsonlokasi_waitinglist.KODE_DETAIL_KEGIATAN';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('Idgeolokasi', 'UnitId', 'KegiatanCode', 'IdWaiting', 'Satuan', 'Volume', 'NilaiAnggaran', 'Tahun', 'Mlokasi', 'IdKelompok', 'Geojson', 'Keterangan', 'Nmuser', 'Level', 'KomponenName', 'StatusHapus', 'KeteranganAlamat', 'UnitName', 'LastEditTime', 'LastCreateTime', 'Koordinat', 'LokasiKe', 'KodeDetailKegiatan', ),
		BasePeer::TYPE_COLNAME => array (GeojsonlokasiWaitinglistPeer::IDGEOLOKASI, GeojsonlokasiWaitinglistPeer::UNIT_ID, GeojsonlokasiWaitinglistPeer::KEGIATAN_CODE, GeojsonlokasiWaitinglistPeer::ID_WAITING, GeojsonlokasiWaitinglistPeer::SATUAN, GeojsonlokasiWaitinglistPeer::VOLUME, GeojsonlokasiWaitinglistPeer::NILAI_ANGGARAN, GeojsonlokasiWaitinglistPeer::TAHUN, GeojsonlokasiWaitinglistPeer::MLOKASI, GeojsonlokasiWaitinglistPeer::ID_KELOMPOK, GeojsonlokasiWaitinglistPeer::GEOJSON, GeojsonlokasiWaitinglistPeer::KETERANGAN, GeojsonlokasiWaitinglistPeer::NMUSER, GeojsonlokasiWaitinglistPeer::LEVEL, GeojsonlokasiWaitinglistPeer::KOMPONEN_NAME, GeojsonlokasiWaitinglistPeer::STATUS_HAPUS, GeojsonlokasiWaitinglistPeer::KETERANGAN_ALAMAT, GeojsonlokasiWaitinglistPeer::UNIT_NAME, GeojsonlokasiWaitinglistPeer::LAST_EDIT_TIME, GeojsonlokasiWaitinglistPeer::LAST_CREATE_TIME, GeojsonlokasiWaitinglistPeer::KOORDINAT, GeojsonlokasiWaitinglistPeer::LOKASI_KE, GeojsonlokasiWaitinglistPeer::KODE_DETAIL_KEGIATAN, ),
		BasePeer::TYPE_FIELDNAME => array ('idgeolokasi', 'unit_id', 'kegiatan_code', 'id_waiting', 'satuan', 'volume', 'nilai_anggaran', 'tahun', 'mlokasi', 'id_kelompok', 'geojson', 'keterangan', 'nmuser', 'level', 'komponen_name', 'status_hapus', 'keterangan_alamat', 'unit_name', 'last_edit_time', 'last_create_time', 'koordinat', 'lokasi_ke', 'kode_detail_kegiatan', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('Idgeolokasi' => 0, 'UnitId' => 1, 'KegiatanCode' => 2, 'IdWaiting' => 3, 'Satuan' => 4, 'Volume' => 5, 'NilaiAnggaran' => 6, 'Tahun' => 7, 'Mlokasi' => 8, 'IdKelompok' => 9, 'Geojson' => 10, 'Keterangan' => 11, 'Nmuser' => 12, 'Level' => 13, 'KomponenName' => 14, 'StatusHapus' => 15, 'KeteranganAlamat' => 16, 'UnitName' => 17, 'LastEditTime' => 18, 'LastCreateTime' => 19, 'Koordinat' => 20, 'LokasiKe' => 21, 'KodeDetailKegiatan' => 22, ),
		BasePeer::TYPE_COLNAME => array (GeojsonlokasiWaitinglistPeer::IDGEOLOKASI => 0, GeojsonlokasiWaitinglistPeer::UNIT_ID => 1, GeojsonlokasiWaitinglistPeer::KEGIATAN_CODE => 2, GeojsonlokasiWaitinglistPeer::ID_WAITING => 3, GeojsonlokasiWaitinglistPeer::SATUAN => 4, GeojsonlokasiWaitinglistPeer::VOLUME => 5, GeojsonlokasiWaitinglistPeer::NILAI_ANGGARAN => 6, GeojsonlokasiWaitinglistPeer::TAHUN => 7, GeojsonlokasiWaitinglistPeer::MLOKASI => 8, GeojsonlokasiWaitinglistPeer::ID_KELOMPOK => 9, GeojsonlokasiWaitinglistPeer::GEOJSON => 10, GeojsonlokasiWaitinglistPeer::KETERANGAN => 11, GeojsonlokasiWaitinglistPeer::NMUSER => 12, GeojsonlokasiWaitinglistPeer::LEVEL => 13, GeojsonlokasiWaitinglistPeer::KOMPONEN_NAME => 14, GeojsonlokasiWaitinglistPeer::STATUS_HAPUS => 15, GeojsonlokasiWaitinglistPeer::KETERANGAN_ALAMAT => 16, GeojsonlokasiWaitinglistPeer::UNIT_NAME => 17, GeojsonlokasiWaitinglistPeer::LAST_EDIT_TIME => 18, GeojsonlokasiWaitinglistPeer::LAST_CREATE_TIME => 19, GeojsonlokasiWaitinglistPeer::KOORDINAT => 20, GeojsonlokasiWaitinglistPeer::LOKASI_KE => 21, GeojsonlokasiWaitinglistPeer::KODE_DETAIL_KEGIATAN => 22, ),
		BasePeer::TYPE_FIELDNAME => array ('idgeolokasi' => 0, 'unit_id' => 1, 'kegiatan_code' => 2, 'id_waiting' => 3, 'satuan' => 4, 'volume' => 5, 'nilai_anggaran' => 6, 'tahun' => 7, 'mlokasi' => 8, 'id_kelompok' => 9, 'geojson' => 10, 'keterangan' => 11, 'nmuser' => 12, 'level' => 13, 'komponen_name' => 14, 'status_hapus' => 15, 'keterangan_alamat' => 16, 'unit_name' => 17, 'last_edit_time' => 18, 'last_create_time' => 19, 'koordinat' => 20, 'lokasi_ke' => 21, 'kode_detail_kegiatan' => 22, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/GeojsonlokasiWaitinglistMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.GeojsonlokasiWaitinglistMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = GeojsonlokasiWaitinglistPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(GeojsonlokasiWaitinglistPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(GeojsonlokasiWaitinglistPeer::IDGEOLOKASI);

		$criteria->addSelectColumn(GeojsonlokasiWaitinglistPeer::UNIT_ID);

		$criteria->addSelectColumn(GeojsonlokasiWaitinglistPeer::KEGIATAN_CODE);

		$criteria->addSelectColumn(GeojsonlokasiWaitinglistPeer::ID_WAITING);

		$criteria->addSelectColumn(GeojsonlokasiWaitinglistPeer::SATUAN);

		$criteria->addSelectColumn(GeojsonlokasiWaitinglistPeer::VOLUME);

		$criteria->addSelectColumn(GeojsonlokasiWaitinglistPeer::NILAI_ANGGARAN);

		$criteria->addSelectColumn(GeojsonlokasiWaitinglistPeer::TAHUN);

		$criteria->addSelectColumn(GeojsonlokasiWaitinglistPeer::MLOKASI);

		$criteria->addSelectColumn(GeojsonlokasiWaitinglistPeer::ID_KELOMPOK);

		$criteria->addSelectColumn(GeojsonlokasiWaitinglistPeer::GEOJSON);

		$criteria->addSelectColumn(GeojsonlokasiWaitinglistPeer::KETERANGAN);

		$criteria->addSelectColumn(GeojsonlokasiWaitinglistPeer::NMUSER);

		$criteria->addSelectColumn(GeojsonlokasiWaitinglistPeer::LEVEL);

		$criteria->addSelectColumn(GeojsonlokasiWaitinglistPeer::KOMPONEN_NAME);

		$criteria->addSelectColumn(GeojsonlokasiWaitinglistPeer::STATUS_HAPUS);

		$criteria->addSelectColumn(GeojsonlokasiWaitinglistPeer::KETERANGAN_ALAMAT);

		$criteria->addSelectColumn(GeojsonlokasiWaitinglistPeer::UNIT_NAME);

		$criteria->addSelectColumn(GeojsonlokasiWaitinglistPeer::LAST_EDIT_TIME);

		$criteria->addSelectColumn(GeojsonlokasiWaitinglistPeer::LAST_CREATE_TIME);

		$criteria->addSelectColumn(GeojsonlokasiWaitinglistPeer::KOORDINAT);

		$criteria->addSelectColumn(GeojsonlokasiWaitinglistPeer::LOKASI_KE);

		$criteria->addSelectColumn(GeojsonlokasiWaitinglistPeer::KODE_DETAIL_KEGIATAN);

	}

	const COUNT = 'COUNT(gis_ebudget.geojsonlokasi_waitinglist.IDGEOLOKASI)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT gis_ebudget.geojsonlokasi_waitinglist.IDGEOLOKASI)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(GeojsonlokasiWaitinglistPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(GeojsonlokasiWaitinglistPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = GeojsonlokasiWaitinglistPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = GeojsonlokasiWaitinglistPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return GeojsonlokasiWaitinglistPeer::populateObjects(GeojsonlokasiWaitinglistPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			GeojsonlokasiWaitinglistPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = GeojsonlokasiWaitinglistPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return GeojsonlokasiWaitinglistPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(GeojsonlokasiWaitinglistPeer::IDGEOLOKASI);
			$selectCriteria->add(GeojsonlokasiWaitinglistPeer::IDGEOLOKASI, $criteria->remove(GeojsonlokasiWaitinglistPeer::IDGEOLOKASI), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(GeojsonlokasiWaitinglistPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(GeojsonlokasiWaitinglistPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof GeojsonlokasiWaitinglist) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(GeojsonlokasiWaitinglistPeer::IDGEOLOKASI, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(GeojsonlokasiWaitinglist $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(GeojsonlokasiWaitinglistPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(GeojsonlokasiWaitinglistPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(GeojsonlokasiWaitinglistPeer::DATABASE_NAME, GeojsonlokasiWaitinglistPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = GeojsonlokasiWaitinglistPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(GeojsonlokasiWaitinglistPeer::DATABASE_NAME);

		$criteria->add(GeojsonlokasiWaitinglistPeer::IDGEOLOKASI, $pk);


		$v = GeojsonlokasiWaitinglistPeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(GeojsonlokasiWaitinglistPeer::IDGEOLOKASI, $pks, Criteria::IN);
			$objs = GeojsonlokasiWaitinglistPeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BaseGeojsonlokasiWaitinglistPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/GeojsonlokasiWaitinglistMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.GeojsonlokasiWaitinglistMapBuilder');
}
