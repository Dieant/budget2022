<?php


abstract class BasePembandingKomponen extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id_pembanding_kegiatan;


	
	protected $detail_no;


	
	protected $rekening_code;


	
	protected $komponen_name;


	
	protected $detail_name;


	
	protected $volume;


	
	protected $satuan;


	
	protected $keterangan_koefisien;


	
	protected $subtitle;


	
	protected $nilai_anggaran;


	
	protected $komponen_harga;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getIdPembandingKegiatan()
	{

		return $this->id_pembanding_kegiatan;
	}

	
	public function getDetailNo()
	{

		return $this->detail_no;
	}

	
	public function getRekeningCode()
	{

		return $this->rekening_code;
	}

	
	public function getKomponenName()
	{

		return $this->komponen_name;
	}

	
	public function getDetailName()
	{

		return $this->detail_name;
	}

	
	public function getVolume()
	{

		return $this->volume;
	}

	
	public function getSatuan()
	{

		return $this->satuan;
	}

	
	public function getKeteranganKoefisien()
	{

		return $this->keterangan_koefisien;
	}

	
	public function getSubtitle()
	{

		return $this->subtitle;
	}

	
	public function getNilaiAnggaran()
	{

		return $this->nilai_anggaran;
	}

	
	public function getKomponenHarga()
	{

		return $this->komponen_harga;
	}

	
	public function setIdPembandingKegiatan($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id_pembanding_kegiatan !== $v) {
			$this->id_pembanding_kegiatan = $v;
			$this->modifiedColumns[] = PembandingKomponenPeer::ID_PEMBANDING_KEGIATAN;
		}

	} 
	
	public function setDetailNo($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->detail_no !== $v) {
			$this->detail_no = $v;
			$this->modifiedColumns[] = PembandingKomponenPeer::DETAIL_NO;
		}

	} 
	
	public function setRekeningCode($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->rekening_code !== $v) {
			$this->rekening_code = $v;
			$this->modifiedColumns[] = PembandingKomponenPeer::REKENING_CODE;
		}

	} 
	
	public function setKomponenName($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->komponen_name !== $v) {
			$this->komponen_name = $v;
			$this->modifiedColumns[] = PembandingKomponenPeer::KOMPONEN_NAME;
		}

	} 
	
	public function setDetailName($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->detail_name !== $v) {
			$this->detail_name = $v;
			$this->modifiedColumns[] = PembandingKomponenPeer::DETAIL_NAME;
		}

	} 
	
	public function setVolume($v)
	{

		if ($this->volume !== $v) {
			$this->volume = $v;
			$this->modifiedColumns[] = PembandingKomponenPeer::VOLUME;
		}

	} 
	
	public function setSatuan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->satuan !== $v) {
			$this->satuan = $v;
			$this->modifiedColumns[] = PembandingKomponenPeer::SATUAN;
		}

	} 
	
	public function setKeteranganKoefisien($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->keterangan_koefisien !== $v) {
			$this->keterangan_koefisien = $v;
			$this->modifiedColumns[] = PembandingKomponenPeer::KETERANGAN_KOEFISIEN;
		}

	} 
	
	public function setSubtitle($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->subtitle !== $v) {
			$this->subtitle = $v;
			$this->modifiedColumns[] = PembandingKomponenPeer::SUBTITLE;
		}

	} 
	
	public function setNilaiAnggaran($v)
	{

		if ($this->nilai_anggaran !== $v) {
			$this->nilai_anggaran = $v;
			$this->modifiedColumns[] = PembandingKomponenPeer::NILAI_ANGGARAN;
		}

	} 
	
	public function setKomponenHarga($v)
	{

		if ($this->komponen_harga !== $v) {
			$this->komponen_harga = $v;
			$this->modifiedColumns[] = PembandingKomponenPeer::KOMPONEN_HARGA;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id_pembanding_kegiatan = $rs->getInt($startcol + 0);

			$this->detail_no = $rs->getInt($startcol + 1);

			$this->rekening_code = $rs->getString($startcol + 2);

			$this->komponen_name = $rs->getString($startcol + 3);

			$this->detail_name = $rs->getString($startcol + 4);

			$this->volume = $rs->getFloat($startcol + 5);

			$this->satuan = $rs->getString($startcol + 6);

			$this->keterangan_koefisien = $rs->getString($startcol + 7);

			$this->subtitle = $rs->getString($startcol + 8);

			$this->nilai_anggaran = $rs->getFloat($startcol + 9);

			$this->komponen_harga = $rs->getFloat($startcol + 10);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 11; 
		} catch (Exception $e) {
			throw new PropelException("Error populating PembandingKomponen object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(PembandingKomponenPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			PembandingKomponenPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(PembandingKomponenPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = PembandingKomponenPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += PembandingKomponenPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = PembandingKomponenPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = PembandingKomponenPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getIdPembandingKegiatan();
				break;
			case 1:
				return $this->getDetailNo();
				break;
			case 2:
				return $this->getRekeningCode();
				break;
			case 3:
				return $this->getKomponenName();
				break;
			case 4:
				return $this->getDetailName();
				break;
			case 5:
				return $this->getVolume();
				break;
			case 6:
				return $this->getSatuan();
				break;
			case 7:
				return $this->getKeteranganKoefisien();
				break;
			case 8:
				return $this->getSubtitle();
				break;
			case 9:
				return $this->getNilaiAnggaran();
				break;
			case 10:
				return $this->getKomponenHarga();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = PembandingKomponenPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getIdPembandingKegiatan(),
			$keys[1] => $this->getDetailNo(),
			$keys[2] => $this->getRekeningCode(),
			$keys[3] => $this->getKomponenName(),
			$keys[4] => $this->getDetailName(),
			$keys[5] => $this->getVolume(),
			$keys[6] => $this->getSatuan(),
			$keys[7] => $this->getKeteranganKoefisien(),
			$keys[8] => $this->getSubtitle(),
			$keys[9] => $this->getNilaiAnggaran(),
			$keys[10] => $this->getKomponenHarga(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = PembandingKomponenPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setIdPembandingKegiatan($value);
				break;
			case 1:
				$this->setDetailNo($value);
				break;
			case 2:
				$this->setRekeningCode($value);
				break;
			case 3:
				$this->setKomponenName($value);
				break;
			case 4:
				$this->setDetailName($value);
				break;
			case 5:
				$this->setVolume($value);
				break;
			case 6:
				$this->setSatuan($value);
				break;
			case 7:
				$this->setKeteranganKoefisien($value);
				break;
			case 8:
				$this->setSubtitle($value);
				break;
			case 9:
				$this->setNilaiAnggaran($value);
				break;
			case 10:
				$this->setKomponenHarga($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = PembandingKomponenPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setIdPembandingKegiatan($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setDetailNo($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setRekeningCode($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setKomponenName($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setDetailName($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setVolume($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setSatuan($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setKeteranganKoefisien($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setSubtitle($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setNilaiAnggaran($arr[$keys[9]]);
		if (array_key_exists($keys[10], $arr)) $this->setKomponenHarga($arr[$keys[10]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(PembandingKomponenPeer::DATABASE_NAME);

		if ($this->isColumnModified(PembandingKomponenPeer::ID_PEMBANDING_KEGIATAN)) $criteria->add(PembandingKomponenPeer::ID_PEMBANDING_KEGIATAN, $this->id_pembanding_kegiatan);
		if ($this->isColumnModified(PembandingKomponenPeer::DETAIL_NO)) $criteria->add(PembandingKomponenPeer::DETAIL_NO, $this->detail_no);
		if ($this->isColumnModified(PembandingKomponenPeer::REKENING_CODE)) $criteria->add(PembandingKomponenPeer::REKENING_CODE, $this->rekening_code);
		if ($this->isColumnModified(PembandingKomponenPeer::KOMPONEN_NAME)) $criteria->add(PembandingKomponenPeer::KOMPONEN_NAME, $this->komponen_name);
		if ($this->isColumnModified(PembandingKomponenPeer::DETAIL_NAME)) $criteria->add(PembandingKomponenPeer::DETAIL_NAME, $this->detail_name);
		if ($this->isColumnModified(PembandingKomponenPeer::VOLUME)) $criteria->add(PembandingKomponenPeer::VOLUME, $this->volume);
		if ($this->isColumnModified(PembandingKomponenPeer::SATUAN)) $criteria->add(PembandingKomponenPeer::SATUAN, $this->satuan);
		if ($this->isColumnModified(PembandingKomponenPeer::KETERANGAN_KOEFISIEN)) $criteria->add(PembandingKomponenPeer::KETERANGAN_KOEFISIEN, $this->keterangan_koefisien);
		if ($this->isColumnModified(PembandingKomponenPeer::SUBTITLE)) $criteria->add(PembandingKomponenPeer::SUBTITLE, $this->subtitle);
		if ($this->isColumnModified(PembandingKomponenPeer::NILAI_ANGGARAN)) $criteria->add(PembandingKomponenPeer::NILAI_ANGGARAN, $this->nilai_anggaran);
		if ($this->isColumnModified(PembandingKomponenPeer::KOMPONEN_HARGA)) $criteria->add(PembandingKomponenPeer::KOMPONEN_HARGA, $this->komponen_harga);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(PembandingKomponenPeer::DATABASE_NAME);

		$criteria->add(PembandingKomponenPeer::ID_PEMBANDING_KEGIATAN, $this->id_pembanding_kegiatan);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getIdPembandingKegiatan();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setIdPembandingKegiatan($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setDetailNo($this->detail_no);

		$copyObj->setRekeningCode($this->rekening_code);

		$copyObj->setKomponenName($this->komponen_name);

		$copyObj->setDetailName($this->detail_name);

		$copyObj->setVolume($this->volume);

		$copyObj->setSatuan($this->satuan);

		$copyObj->setKeteranganKoefisien($this->keterangan_koefisien);

		$copyObj->setSubtitle($this->subtitle);

		$copyObj->setNilaiAnggaran($this->nilai_anggaran);

		$copyObj->setKomponenHarga($this->komponen_harga);


		$copyObj->setNew(true);

		$copyObj->setIdPembandingKegiatan(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new PembandingKomponenPeer();
		}
		return self::$peer;
	}

} 