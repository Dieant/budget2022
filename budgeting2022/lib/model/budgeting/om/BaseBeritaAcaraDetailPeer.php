<?php


abstract class BaseBeritaAcaraDetailPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.berita_acara_detail';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.BeritaAcaraDetail';

	
	const NUM_COLUMNS = 21;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const ID_BERITA_ACARA = 'ebudget.berita_acara_detail.ID_BERITA_ACARA';

	
	const DETAIL_NO = 'ebudget.berita_acara_detail.DETAIL_NO';

	
	const BELANJA = 'ebudget.berita_acara_detail.BELANJA';

	
	const REKENING_CODE = 'ebudget.berita_acara_detail.REKENING_CODE';

	
	const REKENING_CODE_SEMULA = 'ebudget.berita_acara_detail.REKENING_CODE_SEMULA';

	
	const SUBTITLE = 'ebudget.berita_acara_detail.SUBTITLE';

	
	const SEMULA_KOMPONEN = 'ebudget.berita_acara_detail.SEMULA_KOMPONEN';

	
	const MENJADI_KOMPONEN = 'ebudget.berita_acara_detail.MENJADI_KOMPONEN';

	
	const SEMULA_SATUAN = 'ebudget.berita_acara_detail.SEMULA_SATUAN';

	
	const MENJADI_SATUAN = 'ebudget.berita_acara_detail.MENJADI_SATUAN';

	
	const SEMULA_KOEFISIEN = 'ebudget.berita_acara_detail.SEMULA_KOEFISIEN';

	
	const MENJADI_KOEFISIEN = 'ebudget.berita_acara_detail.MENJADI_KOEFISIEN';

	
	const SEMULA_HARGA = 'ebudget.berita_acara_detail.SEMULA_HARGA';

	
	const MENJADI_HARGA = 'ebudget.berita_acara_detail.MENJADI_HARGA';

	
	const SEMULA_HASIL = 'ebudget.berita_acara_detail.SEMULA_HASIL';

	
	const MENJADI_HASIL = 'ebudget.berita_acara_detail.MENJADI_HASIL';

	
	const SEMULA_PAJAK = 'ebudget.berita_acara_detail.SEMULA_PAJAK';

	
	const MENJADI_PAJAK = 'ebudget.berita_acara_detail.MENJADI_PAJAK';

	
	const SEMULA_TOTAL = 'ebudget.berita_acara_detail.SEMULA_TOTAL';

	
	const MENJADI_TOTAL = 'ebudget.berita_acara_detail.MENJADI_TOTAL';

	
	const CATATAN = 'ebudget.berita_acara_detail.CATATAN';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('IdBeritaAcara', 'DetailNo', 'Belanja', 'RekeningCode', 'RekeningCodeSemula', 'Subtitle', 'SemulaKomponen', 'MenjadiKomponen', 'SemulaSatuan', 'MenjadiSatuan', 'SemulaKoefisien', 'MenjadiKoefisien', 'SemulaHarga', 'MenjadiHarga', 'SemulaHasil', 'MenjadiHasil', 'SemulaPajak', 'MenjadiPajak', 'SemulaTotal', 'MenjadiTotal', 'Catatan', ),
		BasePeer::TYPE_COLNAME => array (BeritaAcaraDetailPeer::ID_BERITA_ACARA, BeritaAcaraDetailPeer::DETAIL_NO, BeritaAcaraDetailPeer::BELANJA, BeritaAcaraDetailPeer::REKENING_CODE, BeritaAcaraDetailPeer::REKENING_CODE_SEMULA, BeritaAcaraDetailPeer::SUBTITLE, BeritaAcaraDetailPeer::SEMULA_KOMPONEN, BeritaAcaraDetailPeer::MENJADI_KOMPONEN, BeritaAcaraDetailPeer::SEMULA_SATUAN, BeritaAcaraDetailPeer::MENJADI_SATUAN, BeritaAcaraDetailPeer::SEMULA_KOEFISIEN, BeritaAcaraDetailPeer::MENJADI_KOEFISIEN, BeritaAcaraDetailPeer::SEMULA_HARGA, BeritaAcaraDetailPeer::MENJADI_HARGA, BeritaAcaraDetailPeer::SEMULA_HASIL, BeritaAcaraDetailPeer::MENJADI_HASIL, BeritaAcaraDetailPeer::SEMULA_PAJAK, BeritaAcaraDetailPeer::MENJADI_PAJAK, BeritaAcaraDetailPeer::SEMULA_TOTAL, BeritaAcaraDetailPeer::MENJADI_TOTAL, BeritaAcaraDetailPeer::CATATAN, ),
		BasePeer::TYPE_FIELDNAME => array ('id_berita_acara', 'detail_no', 'belanja', 'rekening_code', 'rekening_code_semula', 'subtitle', 'semula_komponen', 'menjadi_komponen', 'semula_satuan', 'menjadi_satuan', 'semula_koefisien', 'menjadi_koefisien', 'semula_harga', 'menjadi_harga', 'semula_hasil', 'menjadi_hasil', 'semula_pajak', 'menjadi_pajak', 'semula_total', 'menjadi_total', 'catatan', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('IdBeritaAcara' => 0, 'DetailNo' => 1, 'Belanja' => 2, 'RekeningCode' => 3, 'RekeningCodeSemula' => 4, 'Subtitle' => 5, 'SemulaKomponen' => 6, 'MenjadiKomponen' => 7, 'SemulaSatuan' => 8, 'MenjadiSatuan' => 9, 'SemulaKoefisien' => 10, 'MenjadiKoefisien' => 11, 'SemulaHarga' => 12, 'MenjadiHarga' => 13, 'SemulaHasil' => 14, 'MenjadiHasil' => 15, 'SemulaPajak' => 16, 'MenjadiPajak' => 17, 'SemulaTotal' => 18, 'MenjadiTotal' => 19, 'Catatan' => 20, ),
		BasePeer::TYPE_COLNAME => array (BeritaAcaraDetailPeer::ID_BERITA_ACARA => 0, BeritaAcaraDetailPeer::DETAIL_NO => 1, BeritaAcaraDetailPeer::BELANJA => 2, BeritaAcaraDetailPeer::REKENING_CODE => 3, BeritaAcaraDetailPeer::REKENING_CODE_SEMULA => 4, BeritaAcaraDetailPeer::SUBTITLE => 5, BeritaAcaraDetailPeer::SEMULA_KOMPONEN => 6, BeritaAcaraDetailPeer::MENJADI_KOMPONEN => 7, BeritaAcaraDetailPeer::SEMULA_SATUAN => 8, BeritaAcaraDetailPeer::MENJADI_SATUAN => 9, BeritaAcaraDetailPeer::SEMULA_KOEFISIEN => 10, BeritaAcaraDetailPeer::MENJADI_KOEFISIEN => 11, BeritaAcaraDetailPeer::SEMULA_HARGA => 12, BeritaAcaraDetailPeer::MENJADI_HARGA => 13, BeritaAcaraDetailPeer::SEMULA_HASIL => 14, BeritaAcaraDetailPeer::MENJADI_HASIL => 15, BeritaAcaraDetailPeer::SEMULA_PAJAK => 16, BeritaAcaraDetailPeer::MENJADI_PAJAK => 17, BeritaAcaraDetailPeer::SEMULA_TOTAL => 18, BeritaAcaraDetailPeer::MENJADI_TOTAL => 19, BeritaAcaraDetailPeer::CATATAN => 20, ),
		BasePeer::TYPE_FIELDNAME => array ('id_berita_acara' => 0, 'detail_no' => 1, 'belanja' => 2, 'rekening_code' => 3, 'rekening_code_semula' => 4, 'subtitle' => 5, 'semula_komponen' => 6, 'menjadi_komponen' => 7, 'semula_satuan' => 8, 'menjadi_satuan' => 9, 'semula_koefisien' => 10, 'menjadi_koefisien' => 11, 'semula_harga' => 12, 'menjadi_harga' => 13, 'semula_hasil' => 14, 'menjadi_hasil' => 15, 'semula_pajak' => 16, 'menjadi_pajak' => 17, 'semula_total' => 18, 'menjadi_total' => 19, 'catatan' => 20, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/BeritaAcaraDetailMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.BeritaAcaraDetailMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = BeritaAcaraDetailPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(BeritaAcaraDetailPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(BeritaAcaraDetailPeer::ID_BERITA_ACARA);

		$criteria->addSelectColumn(BeritaAcaraDetailPeer::DETAIL_NO);

		$criteria->addSelectColumn(BeritaAcaraDetailPeer::BELANJA);

		$criteria->addSelectColumn(BeritaAcaraDetailPeer::REKENING_CODE);

		$criteria->addSelectColumn(BeritaAcaraDetailPeer::REKENING_CODE_SEMULA);

		$criteria->addSelectColumn(BeritaAcaraDetailPeer::SUBTITLE);

		$criteria->addSelectColumn(BeritaAcaraDetailPeer::SEMULA_KOMPONEN);

		$criteria->addSelectColumn(BeritaAcaraDetailPeer::MENJADI_KOMPONEN);

		$criteria->addSelectColumn(BeritaAcaraDetailPeer::SEMULA_SATUAN);

		$criteria->addSelectColumn(BeritaAcaraDetailPeer::MENJADI_SATUAN);

		$criteria->addSelectColumn(BeritaAcaraDetailPeer::SEMULA_KOEFISIEN);

		$criteria->addSelectColumn(BeritaAcaraDetailPeer::MENJADI_KOEFISIEN);

		$criteria->addSelectColumn(BeritaAcaraDetailPeer::SEMULA_HARGA);

		$criteria->addSelectColumn(BeritaAcaraDetailPeer::MENJADI_HARGA);

		$criteria->addSelectColumn(BeritaAcaraDetailPeer::SEMULA_HASIL);

		$criteria->addSelectColumn(BeritaAcaraDetailPeer::MENJADI_HASIL);

		$criteria->addSelectColumn(BeritaAcaraDetailPeer::SEMULA_PAJAK);

		$criteria->addSelectColumn(BeritaAcaraDetailPeer::MENJADI_PAJAK);

		$criteria->addSelectColumn(BeritaAcaraDetailPeer::SEMULA_TOTAL);

		$criteria->addSelectColumn(BeritaAcaraDetailPeer::MENJADI_TOTAL);

		$criteria->addSelectColumn(BeritaAcaraDetailPeer::CATATAN);

	}

	const COUNT = 'COUNT(ebudget.berita_acara_detail.ID_BERITA_ACARA)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.berita_acara_detail.ID_BERITA_ACARA)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(BeritaAcaraDetailPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(BeritaAcaraDetailPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = BeritaAcaraDetailPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = BeritaAcaraDetailPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return BeritaAcaraDetailPeer::populateObjects(BeritaAcaraDetailPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			BeritaAcaraDetailPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = BeritaAcaraDetailPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return BeritaAcaraDetailPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(BeritaAcaraDetailPeer::ID_BERITA_ACARA);
			$selectCriteria->add(BeritaAcaraDetailPeer::ID_BERITA_ACARA, $criteria->remove(BeritaAcaraDetailPeer::ID_BERITA_ACARA), $comparison);

			$comparison = $criteria->getComparison(BeritaAcaraDetailPeer::DETAIL_NO);
			$selectCriteria->add(BeritaAcaraDetailPeer::DETAIL_NO, $criteria->remove(BeritaAcaraDetailPeer::DETAIL_NO), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(BeritaAcaraDetailPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(BeritaAcaraDetailPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof BeritaAcaraDetail) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
												if(count($values) == count($values, COUNT_RECURSIVE))
			{
								$values = array($values);
			}
			$vals = array();
			foreach($values as $value)
			{

				$vals[0][] = $value[0];
				$vals[1][] = $value[1];
			}

			$criteria->add(BeritaAcaraDetailPeer::ID_BERITA_ACARA, $vals[0], Criteria::IN);
			$criteria->add(BeritaAcaraDetailPeer::DETAIL_NO, $vals[1], Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(BeritaAcaraDetail $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(BeritaAcaraDetailPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(BeritaAcaraDetailPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(BeritaAcaraDetailPeer::DATABASE_NAME, BeritaAcaraDetailPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = BeritaAcaraDetailPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK( $id_berita_acara, $detail_no, $con = null) {
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$criteria = new Criteria();
		$criteria->add(BeritaAcaraDetailPeer::ID_BERITA_ACARA, $id_berita_acara);
		$criteria->add(BeritaAcaraDetailPeer::DETAIL_NO, $detail_no);
		$v = BeritaAcaraDetailPeer::doSelect($criteria, $con);

		return !empty($v) ? $v[0] : null;
	}
} 
if (Propel::isInit()) {
			try {
		BaseBeritaAcaraDetailPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/BeritaAcaraDetailMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.BeritaAcaraDetailMapBuilder');
}
