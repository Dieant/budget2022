<?php


abstract class BaseMasterLokasi extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id_lokasi;


	
	protected $tahun;


	
	protected $status_hapus;


	
	protected $jalan;


	
	protected $gang;


	
	protected $nomor;


	
	protected $rw;


	
	protected $rt;


	
	protected $keterangan;


	
	protected $tempat;


	
	protected $kecamatan;


	
	protected $kelurahan;


	
	protected $lokasi;


	
	protected $status_verifikasi;


	
	protected $usulan_skpd;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getIdLokasi()
	{

		return $this->id_lokasi;
	}

	
	public function getTahun()
	{

		return $this->tahun;
	}

	
	public function getStatusHapus()
	{

		return $this->status_hapus;
	}

	
	public function getJalan()
	{

		return $this->jalan;
	}

	
	public function getGang()
	{

		return $this->gang;
	}

	
	public function getNomor()
	{

		return $this->nomor;
	}

	
	public function getRw()
	{

		return $this->rw;
	}

	
	public function getRt()
	{

		return $this->rt;
	}

	
	public function getKeterangan()
	{

		return $this->keterangan;
	}

	
	public function getTempat()
	{

		return $this->tempat;
	}

	
	public function getKecamatan()
	{

		return $this->kecamatan;
	}

	
	public function getKelurahan()
	{

		return $this->kelurahan;
	}

	
	public function getLokasi()
	{

		return $this->lokasi;
	}

	
	public function getStatusVerifikasi()
	{

		return $this->status_verifikasi;
	}

	
	public function getUsulanSkpd()
	{

		return $this->usulan_skpd;
	}

	
	public function setIdLokasi($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id_lokasi !== $v) {
			$this->id_lokasi = $v;
			$this->modifiedColumns[] = MasterLokasiPeer::ID_LOKASI;
		}

	} 
	
	public function setTahun($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->tahun !== $v) {
			$this->tahun = $v;
			$this->modifiedColumns[] = MasterLokasiPeer::TAHUN;
		}

	} 
	
	public function setStatusHapus($v)
	{

		if ($this->status_hapus !== $v) {
			$this->status_hapus = $v;
			$this->modifiedColumns[] = MasterLokasiPeer::STATUS_HAPUS;
		}

	} 
	
	public function setJalan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->jalan !== $v) {
			$this->jalan = $v;
			$this->modifiedColumns[] = MasterLokasiPeer::JALAN;
		}

	} 
	
	public function setGang($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->gang !== $v) {
			$this->gang = $v;
			$this->modifiedColumns[] = MasterLokasiPeer::GANG;
		}

	} 
	
	public function setNomor($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nomor !== $v) {
			$this->nomor = $v;
			$this->modifiedColumns[] = MasterLokasiPeer::NOMOR;
		}

	} 
	
	public function setRw($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->rw !== $v) {
			$this->rw = $v;
			$this->modifiedColumns[] = MasterLokasiPeer::RW;
		}

	} 
	
	public function setRt($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->rt !== $v) {
			$this->rt = $v;
			$this->modifiedColumns[] = MasterLokasiPeer::RT;
		}

	} 
	
	public function setKeterangan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->keterangan !== $v) {
			$this->keterangan = $v;
			$this->modifiedColumns[] = MasterLokasiPeer::KETERANGAN;
		}

	} 
	
	public function setTempat($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->tempat !== $v) {
			$this->tempat = $v;
			$this->modifiedColumns[] = MasterLokasiPeer::TEMPAT;
		}

	} 
	
	public function setKecamatan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kecamatan !== $v) {
			$this->kecamatan = $v;
			$this->modifiedColumns[] = MasterLokasiPeer::KECAMATAN;
		}

	} 
	
	public function setKelurahan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kelurahan !== $v) {
			$this->kelurahan = $v;
			$this->modifiedColumns[] = MasterLokasiPeer::KELURAHAN;
		}

	} 
	
	public function setLokasi($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->lokasi !== $v) {
			$this->lokasi = $v;
			$this->modifiedColumns[] = MasterLokasiPeer::LOKASI;
		}

	} 
	
	public function setStatusVerifikasi($v)
	{

		if ($this->status_verifikasi !== $v) {
			$this->status_verifikasi = $v;
			$this->modifiedColumns[] = MasterLokasiPeer::STATUS_VERIFIKASI;
		}

	} 
	
	public function setUsulanSkpd($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->usulan_skpd !== $v) {
			$this->usulan_skpd = $v;
			$this->modifiedColumns[] = MasterLokasiPeer::USULAN_SKPD;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id_lokasi = $rs->getInt($startcol + 0);

			$this->tahun = $rs->getString($startcol + 1);

			$this->status_hapus = $rs->getBoolean($startcol + 2);

			$this->jalan = $rs->getString($startcol + 3);

			$this->gang = $rs->getString($startcol + 4);

			$this->nomor = $rs->getString($startcol + 5);

			$this->rw = $rs->getString($startcol + 6);

			$this->rt = $rs->getString($startcol + 7);

			$this->keterangan = $rs->getString($startcol + 8);

			$this->tempat = $rs->getString($startcol + 9);

			$this->kecamatan = $rs->getString($startcol + 10);

			$this->kelurahan = $rs->getString($startcol + 11);

			$this->lokasi = $rs->getString($startcol + 12);

			$this->status_verifikasi = $rs->getBoolean($startcol + 13);

			$this->usulan_skpd = $rs->getString($startcol + 14);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 15; 
		} catch (Exception $e) {
			throw new PropelException("Error populating MasterLokasi object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(MasterLokasiPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			MasterLokasiPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(MasterLokasiPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = MasterLokasiPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += MasterLokasiPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = MasterLokasiPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = MasterLokasiPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getIdLokasi();
				break;
			case 1:
				return $this->getTahun();
				break;
			case 2:
				return $this->getStatusHapus();
				break;
			case 3:
				return $this->getJalan();
				break;
			case 4:
				return $this->getGang();
				break;
			case 5:
				return $this->getNomor();
				break;
			case 6:
				return $this->getRw();
				break;
			case 7:
				return $this->getRt();
				break;
			case 8:
				return $this->getKeterangan();
				break;
			case 9:
				return $this->getTempat();
				break;
			case 10:
				return $this->getKecamatan();
				break;
			case 11:
				return $this->getKelurahan();
				break;
			case 12:
				return $this->getLokasi();
				break;
			case 13:
				return $this->getStatusVerifikasi();
				break;
			case 14:
				return $this->getUsulanSkpd();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = MasterLokasiPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getIdLokasi(),
			$keys[1] => $this->getTahun(),
			$keys[2] => $this->getStatusHapus(),
			$keys[3] => $this->getJalan(),
			$keys[4] => $this->getGang(),
			$keys[5] => $this->getNomor(),
			$keys[6] => $this->getRw(),
			$keys[7] => $this->getRt(),
			$keys[8] => $this->getKeterangan(),
			$keys[9] => $this->getTempat(),
			$keys[10] => $this->getKecamatan(),
			$keys[11] => $this->getKelurahan(),
			$keys[12] => $this->getLokasi(),
			$keys[13] => $this->getStatusVerifikasi(),
			$keys[14] => $this->getUsulanSkpd(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = MasterLokasiPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setIdLokasi($value);
				break;
			case 1:
				$this->setTahun($value);
				break;
			case 2:
				$this->setStatusHapus($value);
				break;
			case 3:
				$this->setJalan($value);
				break;
			case 4:
				$this->setGang($value);
				break;
			case 5:
				$this->setNomor($value);
				break;
			case 6:
				$this->setRw($value);
				break;
			case 7:
				$this->setRt($value);
				break;
			case 8:
				$this->setKeterangan($value);
				break;
			case 9:
				$this->setTempat($value);
				break;
			case 10:
				$this->setKecamatan($value);
				break;
			case 11:
				$this->setKelurahan($value);
				break;
			case 12:
				$this->setLokasi($value);
				break;
			case 13:
				$this->setStatusVerifikasi($value);
				break;
			case 14:
				$this->setUsulanSkpd($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = MasterLokasiPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setIdLokasi($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setTahun($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setStatusHapus($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setJalan($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setGang($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setNomor($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setRw($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setRt($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setKeterangan($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setTempat($arr[$keys[9]]);
		if (array_key_exists($keys[10], $arr)) $this->setKecamatan($arr[$keys[10]]);
		if (array_key_exists($keys[11], $arr)) $this->setKelurahan($arr[$keys[11]]);
		if (array_key_exists($keys[12], $arr)) $this->setLokasi($arr[$keys[12]]);
		if (array_key_exists($keys[13], $arr)) $this->setStatusVerifikasi($arr[$keys[13]]);
		if (array_key_exists($keys[14], $arr)) $this->setUsulanSkpd($arr[$keys[14]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(MasterLokasiPeer::DATABASE_NAME);

		if ($this->isColumnModified(MasterLokasiPeer::ID_LOKASI)) $criteria->add(MasterLokasiPeer::ID_LOKASI, $this->id_lokasi);
		if ($this->isColumnModified(MasterLokasiPeer::TAHUN)) $criteria->add(MasterLokasiPeer::TAHUN, $this->tahun);
		if ($this->isColumnModified(MasterLokasiPeer::STATUS_HAPUS)) $criteria->add(MasterLokasiPeer::STATUS_HAPUS, $this->status_hapus);
		if ($this->isColumnModified(MasterLokasiPeer::JALAN)) $criteria->add(MasterLokasiPeer::JALAN, $this->jalan);
		if ($this->isColumnModified(MasterLokasiPeer::GANG)) $criteria->add(MasterLokasiPeer::GANG, $this->gang);
		if ($this->isColumnModified(MasterLokasiPeer::NOMOR)) $criteria->add(MasterLokasiPeer::NOMOR, $this->nomor);
		if ($this->isColumnModified(MasterLokasiPeer::RW)) $criteria->add(MasterLokasiPeer::RW, $this->rw);
		if ($this->isColumnModified(MasterLokasiPeer::RT)) $criteria->add(MasterLokasiPeer::RT, $this->rt);
		if ($this->isColumnModified(MasterLokasiPeer::KETERANGAN)) $criteria->add(MasterLokasiPeer::KETERANGAN, $this->keterangan);
		if ($this->isColumnModified(MasterLokasiPeer::TEMPAT)) $criteria->add(MasterLokasiPeer::TEMPAT, $this->tempat);
		if ($this->isColumnModified(MasterLokasiPeer::KECAMATAN)) $criteria->add(MasterLokasiPeer::KECAMATAN, $this->kecamatan);
		if ($this->isColumnModified(MasterLokasiPeer::KELURAHAN)) $criteria->add(MasterLokasiPeer::KELURAHAN, $this->kelurahan);
		if ($this->isColumnModified(MasterLokasiPeer::LOKASI)) $criteria->add(MasterLokasiPeer::LOKASI, $this->lokasi);
		if ($this->isColumnModified(MasterLokasiPeer::STATUS_VERIFIKASI)) $criteria->add(MasterLokasiPeer::STATUS_VERIFIKASI, $this->status_verifikasi);
		if ($this->isColumnModified(MasterLokasiPeer::USULAN_SKPD)) $criteria->add(MasterLokasiPeer::USULAN_SKPD, $this->usulan_skpd);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(MasterLokasiPeer::DATABASE_NAME);

		$criteria->add(MasterLokasiPeer::ID_LOKASI, $this->id_lokasi);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getIdLokasi();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setIdLokasi($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setTahun($this->tahun);

		$copyObj->setStatusHapus($this->status_hapus);

		$copyObj->setJalan($this->jalan);

		$copyObj->setGang($this->gang);

		$copyObj->setNomor($this->nomor);

		$copyObj->setRw($this->rw);

		$copyObj->setRt($this->rt);

		$copyObj->setKeterangan($this->keterangan);

		$copyObj->setTempat($this->tempat);

		$copyObj->setKecamatan($this->kecamatan);

		$copyObj->setKelurahan($this->kelurahan);

		$copyObj->setLokasi($this->lokasi);

		$copyObj->setStatusVerifikasi($this->status_verifikasi);

		$copyObj->setUsulanSkpd($this->usulan_skpd);


		$copyObj->setNew(true);

		$copyObj->setIdLokasi(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new MasterLokasiPeer();
		}
		return self::$peer;
	}

} 