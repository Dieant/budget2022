<?php


abstract class BaseKelurahanKecamatan extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $oid;


	
	protected $nama_kecamatan;


	
	protected $nama_kelurahan;


	
	protected $id_kecamatan;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getOid()
	{

		return $this->oid;
	}

	
	public function getNamaKecamatan()
	{

		return $this->nama_kecamatan;
	}

	
	public function getNamaKelurahan()
	{

		return $this->nama_kelurahan;
	}

	
	public function getIdKecamatan()
	{

		return $this->id_kecamatan;
	}

	
	public function setOid($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->oid !== $v) {
			$this->oid = $v;
			$this->modifiedColumns[] = KelurahanKecamatanPeer::OID;
		}

	} 
	
	public function setNamaKecamatan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nama_kecamatan !== $v) {
			$this->nama_kecamatan = $v;
			$this->modifiedColumns[] = KelurahanKecamatanPeer::NAMA_KECAMATAN;
		}

	} 
	
	public function setNamaKelurahan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nama_kelurahan !== $v) {
			$this->nama_kelurahan = $v;
			$this->modifiedColumns[] = KelurahanKecamatanPeer::NAMA_KELURAHAN;
		}

	} 
	
	public function setIdKecamatan($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id_kecamatan !== $v) {
			$this->id_kecamatan = $v;
			$this->modifiedColumns[] = KelurahanKecamatanPeer::ID_KECAMATAN;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->oid = $rs->getInt($startcol + 0);

			$this->nama_kecamatan = $rs->getString($startcol + 1);

			$this->nama_kelurahan = $rs->getString($startcol + 2);

			$this->id_kecamatan = $rs->getInt($startcol + 3);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 4; 
		} catch (Exception $e) {
			throw new PropelException("Error populating KelurahanKecamatan object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(KelurahanKecamatanPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			KelurahanKecamatanPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(KelurahanKecamatanPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = KelurahanKecamatanPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += KelurahanKecamatanPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = KelurahanKecamatanPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = KelurahanKecamatanPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getOid();
				break;
			case 1:
				return $this->getNamaKecamatan();
				break;
			case 2:
				return $this->getNamaKelurahan();
				break;
			case 3:
				return $this->getIdKecamatan();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = KelurahanKecamatanPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getOid(),
			$keys[1] => $this->getNamaKecamatan(),
			$keys[2] => $this->getNamaKelurahan(),
			$keys[3] => $this->getIdKecamatan(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = KelurahanKecamatanPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setOid($value);
				break;
			case 1:
				$this->setNamaKecamatan($value);
				break;
			case 2:
				$this->setNamaKelurahan($value);
				break;
			case 3:
				$this->setIdKecamatan($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = KelurahanKecamatanPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setOid($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setNamaKecamatan($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setNamaKelurahan($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setIdKecamatan($arr[$keys[3]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(KelurahanKecamatanPeer::DATABASE_NAME);

		if ($this->isColumnModified(KelurahanKecamatanPeer::OID)) $criteria->add(KelurahanKecamatanPeer::OID, $this->oid);
		if ($this->isColumnModified(KelurahanKecamatanPeer::NAMA_KECAMATAN)) $criteria->add(KelurahanKecamatanPeer::NAMA_KECAMATAN, $this->nama_kecamatan);
		if ($this->isColumnModified(KelurahanKecamatanPeer::NAMA_KELURAHAN)) $criteria->add(KelurahanKecamatanPeer::NAMA_KELURAHAN, $this->nama_kelurahan);
		if ($this->isColumnModified(KelurahanKecamatanPeer::ID_KECAMATAN)) $criteria->add(KelurahanKecamatanPeer::ID_KECAMATAN, $this->id_kecamatan);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(KelurahanKecamatanPeer::DATABASE_NAME);


		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return null;
	}

	
	 public function setPrimaryKey($pk)
	 {
		 	 }

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setOid($this->oid);

		$copyObj->setNamaKecamatan($this->nama_kecamatan);

		$copyObj->setNamaKelurahan($this->nama_kelurahan);

		$copyObj->setIdKecamatan($this->id_kecamatan);


		$copyObj->setNew(true);

	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new KelurahanKecamatanPeer();
		}
		return self::$peer;
	}

} 