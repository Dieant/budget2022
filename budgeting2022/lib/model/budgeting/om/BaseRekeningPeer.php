<?php


abstract class BaseRekeningPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.rekening';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.Rekening';

	
	const NUM_COLUMNS = 10;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const REKENING_CODE = 'ebudget.rekening.REKENING_CODE';

	
	const USER_ID = 'ebudget.rekening.USER_ID';

	
	const BELANJA_ID = 'ebudget.rekening.BELANJA_ID';

	
	const REKENING_NAME = 'ebudget.rekening.REKENING_NAME';

	
	const REKENING_PPN = 'ebudget.rekening.REKENING_PPN';

	
	const REKENING_PPH = 'ebudget.rekening.REKENING_PPH';

	
	const IP_ADDRESS = 'ebudget.rekening.IP_ADDRESS';

	
	const WAKTU_ACCESS = 'ebudget.rekening.WAKTU_ACCESS';

	
	const AKRUAL_CODE = 'ebudget.rekening.AKRUAL_CODE';

	
	const AKRUAL_KONSTRUKSI = 'ebudget.rekening.AKRUAL_KONSTRUKSI';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('RekeningCode', 'UserId', 'BelanjaId', 'RekeningName', 'RekeningPpn', 'RekeningPph', 'IpAddress', 'WaktuAccess', 'AkrualCode', 'AkrualKonstruksi', ),
		BasePeer::TYPE_COLNAME => array (RekeningPeer::REKENING_CODE, RekeningPeer::USER_ID, RekeningPeer::BELANJA_ID, RekeningPeer::REKENING_NAME, RekeningPeer::REKENING_PPN, RekeningPeer::REKENING_PPH, RekeningPeer::IP_ADDRESS, RekeningPeer::WAKTU_ACCESS, RekeningPeer::AKRUAL_CODE, RekeningPeer::AKRUAL_KONSTRUKSI, ),
		BasePeer::TYPE_FIELDNAME => array ('rekening_code', 'user_id', 'belanja_id', 'rekening_name', 'rekening_ppn', 'rekening_pph', 'ip_address', 'waktu_access', 'akrual_code', 'akrual_konstruksi', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('RekeningCode' => 0, 'UserId' => 1, 'BelanjaId' => 2, 'RekeningName' => 3, 'RekeningPpn' => 4, 'RekeningPph' => 5, 'IpAddress' => 6, 'WaktuAccess' => 7, 'AkrualCode' => 8, 'AkrualKonstruksi' => 9, ),
		BasePeer::TYPE_COLNAME => array (RekeningPeer::REKENING_CODE => 0, RekeningPeer::USER_ID => 1, RekeningPeer::BELANJA_ID => 2, RekeningPeer::REKENING_NAME => 3, RekeningPeer::REKENING_PPN => 4, RekeningPeer::REKENING_PPH => 5, RekeningPeer::IP_ADDRESS => 6, RekeningPeer::WAKTU_ACCESS => 7, RekeningPeer::AKRUAL_CODE => 8, RekeningPeer::AKRUAL_KONSTRUKSI => 9, ),
		BasePeer::TYPE_FIELDNAME => array ('rekening_code' => 0, 'user_id' => 1, 'belanja_id' => 2, 'rekening_name' => 3, 'rekening_ppn' => 4, 'rekening_pph' => 5, 'ip_address' => 6, 'waktu_access' => 7, 'akrual_code' => 8, 'akrual_konstruksi' => 9, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/RekeningMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.RekeningMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = RekeningPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(RekeningPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(RekeningPeer::REKENING_CODE);

		$criteria->addSelectColumn(RekeningPeer::USER_ID);

		$criteria->addSelectColumn(RekeningPeer::BELANJA_ID);

		$criteria->addSelectColumn(RekeningPeer::REKENING_NAME);

		$criteria->addSelectColumn(RekeningPeer::REKENING_PPN);

		$criteria->addSelectColumn(RekeningPeer::REKENING_PPH);

		$criteria->addSelectColumn(RekeningPeer::IP_ADDRESS);

		$criteria->addSelectColumn(RekeningPeer::WAKTU_ACCESS);

		$criteria->addSelectColumn(RekeningPeer::AKRUAL_CODE);

		$criteria->addSelectColumn(RekeningPeer::AKRUAL_KONSTRUKSI);

	}

	const COUNT = 'COUNT(ebudget.rekening.REKENING_CODE)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.rekening.REKENING_CODE)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(RekeningPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(RekeningPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = RekeningPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = RekeningPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return RekeningPeer::populateObjects(RekeningPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			RekeningPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = RekeningPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return RekeningPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(RekeningPeer::REKENING_CODE);
			$selectCriteria->add(RekeningPeer::REKENING_CODE, $criteria->remove(RekeningPeer::REKENING_CODE), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(RekeningPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(RekeningPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof Rekening) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(RekeningPeer::REKENING_CODE, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(Rekening $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(RekeningPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(RekeningPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(RekeningPeer::DATABASE_NAME, RekeningPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = RekeningPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(RekeningPeer::DATABASE_NAME);

		$criteria->add(RekeningPeer::REKENING_CODE, $pk);


		$v = RekeningPeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(RekeningPeer::REKENING_CODE, $pks, Criteria::IN);
			$objs = RekeningPeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BaseRekeningPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/RekeningMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.RekeningMapBuilder');
}
