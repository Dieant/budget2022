<?php


abstract class BaseKomponenGabung extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $kegiatan_code;


	
	protected $detail_no;


	
	protected $rekening_code;


	
	protected $komponen_id;


	
	protected $detail_name;


	
	protected $volume;


	
	protected $keterangan_koefisien;


	
	protected $subtitle;


	
	protected $komponen_harga;


	
	protected $komponen_harga_awal;


	
	protected $komponen_name;


	
	protected $satuan;


	
	protected $pajak = 0;


	
	protected $unit_id;


	
	protected $for_detail_no;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getKegiatanCode()
	{

		return $this->kegiatan_code;
	}

	
	public function getDetailNo()
	{

		return $this->detail_no;
	}

	
	public function getRekeningCode()
	{

		return $this->rekening_code;
	}

	
	public function getKomponenId()
	{

		return $this->komponen_id;
	}

	
	public function getDetailName()
	{

		return $this->detail_name;
	}

	
	public function getVolume()
	{

		return $this->volume;
	}

	
	public function getKeteranganKoefisien()
	{

		return $this->keterangan_koefisien;
	}

	
	public function getSubtitle()
	{

		return $this->subtitle;
	}

	
	public function getKomponenHarga()
	{

		return $this->komponen_harga;
	}

	
	public function getKomponenHargaAwal()
	{

		return $this->komponen_harga_awal;
	}

	
	public function getKomponenName()
	{

		return $this->komponen_name;
	}

	
	public function getSatuan()
	{

		return $this->satuan;
	}

	
	public function getPajak()
	{

		return $this->pajak;
	}

	
	public function getUnitId()
	{

		return $this->unit_id;
	}

	
	public function getForDetailNo()
	{

		return $this->for_detail_no;
	}

	
	public function setKegiatanCode($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kegiatan_code !== $v) {
			$this->kegiatan_code = $v;
			$this->modifiedColumns[] = KomponenGabungPeer::KEGIATAN_CODE;
		}

	} 
	
	public function setDetailNo($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->detail_no !== $v) {
			$this->detail_no = $v;
			$this->modifiedColumns[] = KomponenGabungPeer::DETAIL_NO;
		}

	} 
	
	public function setRekeningCode($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->rekening_code !== $v) {
			$this->rekening_code = $v;
			$this->modifiedColumns[] = KomponenGabungPeer::REKENING_CODE;
		}

	} 
	
	public function setKomponenId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->komponen_id !== $v) {
			$this->komponen_id = $v;
			$this->modifiedColumns[] = KomponenGabungPeer::KOMPONEN_ID;
		}

	} 
	
	public function setDetailName($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->detail_name !== $v) {
			$this->detail_name = $v;
			$this->modifiedColumns[] = KomponenGabungPeer::DETAIL_NAME;
		}

	} 
	
	public function setVolume($v)
	{

		if ($this->volume !== $v) {
			$this->volume = $v;
			$this->modifiedColumns[] = KomponenGabungPeer::VOLUME;
		}

	} 
	
	public function setKeteranganKoefisien($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->keterangan_koefisien !== $v) {
			$this->keterangan_koefisien = $v;
			$this->modifiedColumns[] = KomponenGabungPeer::KETERANGAN_KOEFISIEN;
		}

	} 
	
	public function setSubtitle($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->subtitle !== $v) {
			$this->subtitle = $v;
			$this->modifiedColumns[] = KomponenGabungPeer::SUBTITLE;
		}

	} 
	
	public function setKomponenHarga($v)
	{

		if ($this->komponen_harga !== $v) {
			$this->komponen_harga = $v;
			$this->modifiedColumns[] = KomponenGabungPeer::KOMPONEN_HARGA;
		}

	} 
	
	public function setKomponenHargaAwal($v)
	{

		if ($this->komponen_harga_awal !== $v) {
			$this->komponen_harga_awal = $v;
			$this->modifiedColumns[] = KomponenGabungPeer::KOMPONEN_HARGA_AWAL;
		}

	} 
	
	public function setKomponenName($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->komponen_name !== $v) {
			$this->komponen_name = $v;
			$this->modifiedColumns[] = KomponenGabungPeer::KOMPONEN_NAME;
		}

	} 
	
	public function setSatuan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->satuan !== $v) {
			$this->satuan = $v;
			$this->modifiedColumns[] = KomponenGabungPeer::SATUAN;
		}

	} 
	
	public function setPajak($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->pajak !== $v || $v === 0) {
			$this->pajak = $v;
			$this->modifiedColumns[] = KomponenGabungPeer::PAJAK;
		}

	} 
	
	public function setUnitId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->unit_id !== $v) {
			$this->unit_id = $v;
			$this->modifiedColumns[] = KomponenGabungPeer::UNIT_ID;
		}

	} 
	
	public function setForDetailNo($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->for_detail_no !== $v) {
			$this->for_detail_no = $v;
			$this->modifiedColumns[] = KomponenGabungPeer::FOR_DETAIL_NO;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->kegiatan_code = $rs->getString($startcol + 0);

			$this->detail_no = $rs->getInt($startcol + 1);

			$this->rekening_code = $rs->getString($startcol + 2);

			$this->komponen_id = $rs->getString($startcol + 3);

			$this->detail_name = $rs->getString($startcol + 4);

			$this->volume = $rs->getFloat($startcol + 5);

			$this->keterangan_koefisien = $rs->getString($startcol + 6);

			$this->subtitle = $rs->getString($startcol + 7);

			$this->komponen_harga = $rs->getFloat($startcol + 8);

			$this->komponen_harga_awal = $rs->getFloat($startcol + 9);

			$this->komponen_name = $rs->getString($startcol + 10);

			$this->satuan = $rs->getString($startcol + 11);

			$this->pajak = $rs->getInt($startcol + 12);

			$this->unit_id = $rs->getString($startcol + 13);

			$this->for_detail_no = $rs->getInt($startcol + 14);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 15; 
		} catch (Exception $e) {
			throw new PropelException("Error populating KomponenGabung object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(KomponenGabungPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			KomponenGabungPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(KomponenGabungPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = KomponenGabungPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += KomponenGabungPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = KomponenGabungPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = KomponenGabungPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getKegiatanCode();
				break;
			case 1:
				return $this->getDetailNo();
				break;
			case 2:
				return $this->getRekeningCode();
				break;
			case 3:
				return $this->getKomponenId();
				break;
			case 4:
				return $this->getDetailName();
				break;
			case 5:
				return $this->getVolume();
				break;
			case 6:
				return $this->getKeteranganKoefisien();
				break;
			case 7:
				return $this->getSubtitle();
				break;
			case 8:
				return $this->getKomponenHarga();
				break;
			case 9:
				return $this->getKomponenHargaAwal();
				break;
			case 10:
				return $this->getKomponenName();
				break;
			case 11:
				return $this->getSatuan();
				break;
			case 12:
				return $this->getPajak();
				break;
			case 13:
				return $this->getUnitId();
				break;
			case 14:
				return $this->getForDetailNo();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = KomponenGabungPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getKegiatanCode(),
			$keys[1] => $this->getDetailNo(),
			$keys[2] => $this->getRekeningCode(),
			$keys[3] => $this->getKomponenId(),
			$keys[4] => $this->getDetailName(),
			$keys[5] => $this->getVolume(),
			$keys[6] => $this->getKeteranganKoefisien(),
			$keys[7] => $this->getSubtitle(),
			$keys[8] => $this->getKomponenHarga(),
			$keys[9] => $this->getKomponenHargaAwal(),
			$keys[10] => $this->getKomponenName(),
			$keys[11] => $this->getSatuan(),
			$keys[12] => $this->getPajak(),
			$keys[13] => $this->getUnitId(),
			$keys[14] => $this->getForDetailNo(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = KomponenGabungPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setKegiatanCode($value);
				break;
			case 1:
				$this->setDetailNo($value);
				break;
			case 2:
				$this->setRekeningCode($value);
				break;
			case 3:
				$this->setKomponenId($value);
				break;
			case 4:
				$this->setDetailName($value);
				break;
			case 5:
				$this->setVolume($value);
				break;
			case 6:
				$this->setKeteranganKoefisien($value);
				break;
			case 7:
				$this->setSubtitle($value);
				break;
			case 8:
				$this->setKomponenHarga($value);
				break;
			case 9:
				$this->setKomponenHargaAwal($value);
				break;
			case 10:
				$this->setKomponenName($value);
				break;
			case 11:
				$this->setSatuan($value);
				break;
			case 12:
				$this->setPajak($value);
				break;
			case 13:
				$this->setUnitId($value);
				break;
			case 14:
				$this->setForDetailNo($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = KomponenGabungPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setKegiatanCode($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setDetailNo($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setRekeningCode($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setKomponenId($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setDetailName($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setVolume($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setKeteranganKoefisien($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setSubtitle($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setKomponenHarga($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setKomponenHargaAwal($arr[$keys[9]]);
		if (array_key_exists($keys[10], $arr)) $this->setKomponenName($arr[$keys[10]]);
		if (array_key_exists($keys[11], $arr)) $this->setSatuan($arr[$keys[11]]);
		if (array_key_exists($keys[12], $arr)) $this->setPajak($arr[$keys[12]]);
		if (array_key_exists($keys[13], $arr)) $this->setUnitId($arr[$keys[13]]);
		if (array_key_exists($keys[14], $arr)) $this->setForDetailNo($arr[$keys[14]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(KomponenGabungPeer::DATABASE_NAME);

		if ($this->isColumnModified(KomponenGabungPeer::KEGIATAN_CODE)) $criteria->add(KomponenGabungPeer::KEGIATAN_CODE, $this->kegiatan_code);
		if ($this->isColumnModified(KomponenGabungPeer::DETAIL_NO)) $criteria->add(KomponenGabungPeer::DETAIL_NO, $this->detail_no);
		if ($this->isColumnModified(KomponenGabungPeer::REKENING_CODE)) $criteria->add(KomponenGabungPeer::REKENING_CODE, $this->rekening_code);
		if ($this->isColumnModified(KomponenGabungPeer::KOMPONEN_ID)) $criteria->add(KomponenGabungPeer::KOMPONEN_ID, $this->komponen_id);
		if ($this->isColumnModified(KomponenGabungPeer::DETAIL_NAME)) $criteria->add(KomponenGabungPeer::DETAIL_NAME, $this->detail_name);
		if ($this->isColumnModified(KomponenGabungPeer::VOLUME)) $criteria->add(KomponenGabungPeer::VOLUME, $this->volume);
		if ($this->isColumnModified(KomponenGabungPeer::KETERANGAN_KOEFISIEN)) $criteria->add(KomponenGabungPeer::KETERANGAN_KOEFISIEN, $this->keterangan_koefisien);
		if ($this->isColumnModified(KomponenGabungPeer::SUBTITLE)) $criteria->add(KomponenGabungPeer::SUBTITLE, $this->subtitle);
		if ($this->isColumnModified(KomponenGabungPeer::KOMPONEN_HARGA)) $criteria->add(KomponenGabungPeer::KOMPONEN_HARGA, $this->komponen_harga);
		if ($this->isColumnModified(KomponenGabungPeer::KOMPONEN_HARGA_AWAL)) $criteria->add(KomponenGabungPeer::KOMPONEN_HARGA_AWAL, $this->komponen_harga_awal);
		if ($this->isColumnModified(KomponenGabungPeer::KOMPONEN_NAME)) $criteria->add(KomponenGabungPeer::KOMPONEN_NAME, $this->komponen_name);
		if ($this->isColumnModified(KomponenGabungPeer::SATUAN)) $criteria->add(KomponenGabungPeer::SATUAN, $this->satuan);
		if ($this->isColumnModified(KomponenGabungPeer::PAJAK)) $criteria->add(KomponenGabungPeer::PAJAK, $this->pajak);
		if ($this->isColumnModified(KomponenGabungPeer::UNIT_ID)) $criteria->add(KomponenGabungPeer::UNIT_ID, $this->unit_id);
		if ($this->isColumnModified(KomponenGabungPeer::FOR_DETAIL_NO)) $criteria->add(KomponenGabungPeer::FOR_DETAIL_NO, $this->for_detail_no);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(KomponenGabungPeer::DATABASE_NAME);

		$criteria->add(KomponenGabungPeer::KEGIATAN_CODE, $this->kegiatan_code);
		$criteria->add(KomponenGabungPeer::DETAIL_NO, $this->detail_no);
		$criteria->add(KomponenGabungPeer::UNIT_ID, $this->unit_id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		$pks = array();

		$pks[0] = $this->getKegiatanCode();

		$pks[1] = $this->getDetailNo();

		$pks[2] = $this->getUnitId();

		return $pks;
	}

	
	public function setPrimaryKey($keys)
	{

		$this->setKegiatanCode($keys[0]);

		$this->setDetailNo($keys[1]);

		$this->setUnitId($keys[2]);

	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setRekeningCode($this->rekening_code);

		$copyObj->setKomponenId($this->komponen_id);

		$copyObj->setDetailName($this->detail_name);

		$copyObj->setVolume($this->volume);

		$copyObj->setKeteranganKoefisien($this->keterangan_koefisien);

		$copyObj->setSubtitle($this->subtitle);

		$copyObj->setKomponenHarga($this->komponen_harga);

		$copyObj->setKomponenHargaAwal($this->komponen_harga_awal);

		$copyObj->setKomponenName($this->komponen_name);

		$copyObj->setSatuan($this->satuan);

		$copyObj->setPajak($this->pajak);

		$copyObj->setForDetailNo($this->for_detail_no);


		$copyObj->setNew(true);

		$copyObj->setKegiatanCode(NULL); 
		$copyObj->setDetailNo(NULL); 
		$copyObj->setUnitId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new KomponenGabungPeer();
		}
		return self::$peer;
	}

} 