<?php


abstract class BaseNextMasterKegiatanPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.next_master_kegiatan';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.NextMasterKegiatan';

	
	const NUM_COLUMNS = 43;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const UNIT_ID = 'ebudget.next_master_kegiatan.UNIT_ID';

	
	const KODE_KEGIATAN = 'ebudget.next_master_kegiatan.KODE_KEGIATAN';

	
	const KODE_BIDANG = 'ebudget.next_master_kegiatan.KODE_BIDANG';

	
	const KODE_URUSAN_WAJIB = 'ebudget.next_master_kegiatan.KODE_URUSAN_WAJIB';

	
	const KODE_PROGRAM = 'ebudget.next_master_kegiatan.KODE_PROGRAM';

	
	const KODE_SASARAN = 'ebudget.next_master_kegiatan.KODE_SASARAN';

	
	const KODE_INDIKATOR = 'ebudget.next_master_kegiatan.KODE_INDIKATOR';

	
	const ALOKASI_DANA = 'ebudget.next_master_kegiatan.ALOKASI_DANA';

	
	const NAMA_KEGIATAN = 'ebudget.next_master_kegiatan.NAMA_KEGIATAN';

	
	const MASUKAN = 'ebudget.next_master_kegiatan.MASUKAN';

	
	const OUTPUT = 'ebudget.next_master_kegiatan.OUTPUT';

	
	const OUTCOME = 'ebudget.next_master_kegiatan.OUTCOME';

	
	const BENEFIT = 'ebudget.next_master_kegiatan.BENEFIT';

	
	const IMPACT = 'ebudget.next_master_kegiatan.IMPACT';

	
	const TIPE = 'ebudget.next_master_kegiatan.TIPE';

	
	const KEGIATAN_ACTIVE = 'ebudget.next_master_kegiatan.KEGIATAN_ACTIVE';

	
	const TO_KEGIATAN_CODE = 'ebudget.next_master_kegiatan.TO_KEGIATAN_CODE';

	
	const CATATAN = 'ebudget.next_master_kegiatan.CATATAN';

	
	const TARGET_OUTCOME = 'ebudget.next_master_kegiatan.TARGET_OUTCOME';

	
	const LOKASI = 'ebudget.next_master_kegiatan.LOKASI';

	
	const JUMLAH_PREV = 'ebudget.next_master_kegiatan.JUMLAH_PREV';

	
	const JUMLAH_NOW = 'ebudget.next_master_kegiatan.JUMLAH_NOW';

	
	const JUMLAH_NEXT = 'ebudget.next_master_kegiatan.JUMLAH_NEXT';

	
	const KODE_PROGRAM2 = 'ebudget.next_master_kegiatan.KODE_PROGRAM2';

	
	const KODE_URUSAN = 'ebudget.next_master_kegiatan.KODE_URUSAN';

	
	const LAST_UPDATE_USER = 'ebudget.next_master_kegiatan.LAST_UPDATE_USER';

	
	const LAST_UPDATE_TIME = 'ebudget.next_master_kegiatan.LAST_UPDATE_TIME';

	
	const LAST_UPDATE_IP = 'ebudget.next_master_kegiatan.LAST_UPDATE_IP';

	
	const TAHAP = 'ebudget.next_master_kegiatan.TAHAP';

	
	const KODE_MISI = 'ebudget.next_master_kegiatan.KODE_MISI';

	
	const KODE_TUJUAN = 'ebudget.next_master_kegiatan.KODE_TUJUAN';

	
	const RANKING = 'ebudget.next_master_kegiatan.RANKING';

	
	const NOMOR13 = 'ebudget.next_master_kegiatan.NOMOR13';

	
	const PPA_NAMA = 'ebudget.next_master_kegiatan.PPA_NAMA';

	
	const PPA_PANGKAT = 'ebudget.next_master_kegiatan.PPA_PANGKAT';

	
	const PPA_NIP = 'ebudget.next_master_kegiatan.PPA_NIP';

	
	const LANJUTAN = 'ebudget.next_master_kegiatan.LANJUTAN';

	
	const USER_ID = 'ebudget.next_master_kegiatan.USER_ID';

	
	const ID = 'ebudget.next_master_kegiatan.ID';

	
	const TAHUN = 'ebudget.next_master_kegiatan.TAHUN';

	
	const TAMBAHAN_PAGU = 'ebudget.next_master_kegiatan.TAMBAHAN_PAGU';

	
	const GENDER = 'ebudget.next_master_kegiatan.GENDER';

	
	const KODE_KEG_KEUANGAN = 'ebudget.next_master_kegiatan.KODE_KEG_KEUANGAN';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('UnitId', 'KodeKegiatan', 'KodeBidang', 'KodeUrusanWajib', 'KodeProgram', 'KodeSasaran', 'KodeIndikator', 'AlokasiDana', 'NamaKegiatan', 'Masukan', 'Output', 'Outcome', 'Benefit', 'Impact', 'Tipe', 'KegiatanActive', 'ToKegiatanCode', 'Catatan', 'TargetOutcome', 'Lokasi', 'JumlahPrev', 'JumlahNow', 'JumlahNext', 'KodeProgram2', 'KodeUrusan', 'LastUpdateUser', 'LastUpdateTime', 'LastUpdateIp', 'Tahap', 'KodeMisi', 'KodeTujuan', 'Ranking', 'Nomor13', 'PpaNama', 'PpaPangkat', 'PpaNip', 'Lanjutan', 'UserId', 'Id', 'Tahun', 'TambahanPagu', 'Gender', 'KodeKegKeuangan', ),
		BasePeer::TYPE_COLNAME => array (NextMasterKegiatanPeer::UNIT_ID, NextMasterKegiatanPeer::KODE_KEGIATAN, NextMasterKegiatanPeer::KODE_BIDANG, NextMasterKegiatanPeer::KODE_URUSAN_WAJIB, NextMasterKegiatanPeer::KODE_PROGRAM, NextMasterKegiatanPeer::KODE_SASARAN, NextMasterKegiatanPeer::KODE_INDIKATOR, NextMasterKegiatanPeer::ALOKASI_DANA, NextMasterKegiatanPeer::NAMA_KEGIATAN, NextMasterKegiatanPeer::MASUKAN, NextMasterKegiatanPeer::OUTPUT, NextMasterKegiatanPeer::OUTCOME, NextMasterKegiatanPeer::BENEFIT, NextMasterKegiatanPeer::IMPACT, NextMasterKegiatanPeer::TIPE, NextMasterKegiatanPeer::KEGIATAN_ACTIVE, NextMasterKegiatanPeer::TO_KEGIATAN_CODE, NextMasterKegiatanPeer::CATATAN, NextMasterKegiatanPeer::TARGET_OUTCOME, NextMasterKegiatanPeer::LOKASI, NextMasterKegiatanPeer::JUMLAH_PREV, NextMasterKegiatanPeer::JUMLAH_NOW, NextMasterKegiatanPeer::JUMLAH_NEXT, NextMasterKegiatanPeer::KODE_PROGRAM2, NextMasterKegiatanPeer::KODE_URUSAN, NextMasterKegiatanPeer::LAST_UPDATE_USER, NextMasterKegiatanPeer::LAST_UPDATE_TIME, NextMasterKegiatanPeer::LAST_UPDATE_IP, NextMasterKegiatanPeer::TAHAP, NextMasterKegiatanPeer::KODE_MISI, NextMasterKegiatanPeer::KODE_TUJUAN, NextMasterKegiatanPeer::RANKING, NextMasterKegiatanPeer::NOMOR13, NextMasterKegiatanPeer::PPA_NAMA, NextMasterKegiatanPeer::PPA_PANGKAT, NextMasterKegiatanPeer::PPA_NIP, NextMasterKegiatanPeer::LANJUTAN, NextMasterKegiatanPeer::USER_ID, NextMasterKegiatanPeer::ID, NextMasterKegiatanPeer::TAHUN, NextMasterKegiatanPeer::TAMBAHAN_PAGU, NextMasterKegiatanPeer::GENDER, NextMasterKegiatanPeer::KODE_KEG_KEUANGAN, ),
		BasePeer::TYPE_FIELDNAME => array ('unit_id', 'kode_kegiatan', 'kode_bidang', 'kode_urusan_wajib', 'kode_program', 'kode_sasaran', 'kode_indikator', 'alokasi_dana', 'nama_kegiatan', 'masukan', 'output', 'outcome', 'benefit', 'impact', 'tipe', 'kegiatan_active', 'to_kegiatan_code', 'catatan', 'target_outcome', 'lokasi', 'jumlah_prev', 'jumlah_now', 'jumlah_next', 'kode_program2', 'kode_urusan', 'last_update_user', 'last_update_time', 'last_update_ip', 'tahap', 'kode_misi', 'kode_tujuan', 'ranking', 'nomor13', 'ppa_nama', 'ppa_pangkat', 'ppa_nip', 'lanjutan', 'user_id', 'id', 'tahun', 'tambahan_pagu', 'gender', 'kode_keg_keuangan', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('UnitId' => 0, 'KodeKegiatan' => 1, 'KodeBidang' => 2, 'KodeUrusanWajib' => 3, 'KodeProgram' => 4, 'KodeSasaran' => 5, 'KodeIndikator' => 6, 'AlokasiDana' => 7, 'NamaKegiatan' => 8, 'Masukan' => 9, 'Output' => 10, 'Outcome' => 11, 'Benefit' => 12, 'Impact' => 13, 'Tipe' => 14, 'KegiatanActive' => 15, 'ToKegiatanCode' => 16, 'Catatan' => 17, 'TargetOutcome' => 18, 'Lokasi' => 19, 'JumlahPrev' => 20, 'JumlahNow' => 21, 'JumlahNext' => 22, 'KodeProgram2' => 23, 'KodeUrusan' => 24, 'LastUpdateUser' => 25, 'LastUpdateTime' => 26, 'LastUpdateIp' => 27, 'Tahap' => 28, 'KodeMisi' => 29, 'KodeTujuan' => 30, 'Ranking' => 31, 'Nomor13' => 32, 'PpaNama' => 33, 'PpaPangkat' => 34, 'PpaNip' => 35, 'Lanjutan' => 36, 'UserId' => 37, 'Id' => 38, 'Tahun' => 39, 'TambahanPagu' => 40, 'Gender' => 41, 'KodeKegKeuangan' => 42, ),
		BasePeer::TYPE_COLNAME => array (NextMasterKegiatanPeer::UNIT_ID => 0, NextMasterKegiatanPeer::KODE_KEGIATAN => 1, NextMasterKegiatanPeer::KODE_BIDANG => 2, NextMasterKegiatanPeer::KODE_URUSAN_WAJIB => 3, NextMasterKegiatanPeer::KODE_PROGRAM => 4, NextMasterKegiatanPeer::KODE_SASARAN => 5, NextMasterKegiatanPeer::KODE_INDIKATOR => 6, NextMasterKegiatanPeer::ALOKASI_DANA => 7, NextMasterKegiatanPeer::NAMA_KEGIATAN => 8, NextMasterKegiatanPeer::MASUKAN => 9, NextMasterKegiatanPeer::OUTPUT => 10, NextMasterKegiatanPeer::OUTCOME => 11, NextMasterKegiatanPeer::BENEFIT => 12, NextMasterKegiatanPeer::IMPACT => 13, NextMasterKegiatanPeer::TIPE => 14, NextMasterKegiatanPeer::KEGIATAN_ACTIVE => 15, NextMasterKegiatanPeer::TO_KEGIATAN_CODE => 16, NextMasterKegiatanPeer::CATATAN => 17, NextMasterKegiatanPeer::TARGET_OUTCOME => 18, NextMasterKegiatanPeer::LOKASI => 19, NextMasterKegiatanPeer::JUMLAH_PREV => 20, NextMasterKegiatanPeer::JUMLAH_NOW => 21, NextMasterKegiatanPeer::JUMLAH_NEXT => 22, NextMasterKegiatanPeer::KODE_PROGRAM2 => 23, NextMasterKegiatanPeer::KODE_URUSAN => 24, NextMasterKegiatanPeer::LAST_UPDATE_USER => 25, NextMasterKegiatanPeer::LAST_UPDATE_TIME => 26, NextMasterKegiatanPeer::LAST_UPDATE_IP => 27, NextMasterKegiatanPeer::TAHAP => 28, NextMasterKegiatanPeer::KODE_MISI => 29, NextMasterKegiatanPeer::KODE_TUJUAN => 30, NextMasterKegiatanPeer::RANKING => 31, NextMasterKegiatanPeer::NOMOR13 => 32, NextMasterKegiatanPeer::PPA_NAMA => 33, NextMasterKegiatanPeer::PPA_PANGKAT => 34, NextMasterKegiatanPeer::PPA_NIP => 35, NextMasterKegiatanPeer::LANJUTAN => 36, NextMasterKegiatanPeer::USER_ID => 37, NextMasterKegiatanPeer::ID => 38, NextMasterKegiatanPeer::TAHUN => 39, NextMasterKegiatanPeer::TAMBAHAN_PAGU => 40, NextMasterKegiatanPeer::GENDER => 41, NextMasterKegiatanPeer::KODE_KEG_KEUANGAN => 42, ),
		BasePeer::TYPE_FIELDNAME => array ('unit_id' => 0, 'kode_kegiatan' => 1, 'kode_bidang' => 2, 'kode_urusan_wajib' => 3, 'kode_program' => 4, 'kode_sasaran' => 5, 'kode_indikator' => 6, 'alokasi_dana' => 7, 'nama_kegiatan' => 8, 'masukan' => 9, 'output' => 10, 'outcome' => 11, 'benefit' => 12, 'impact' => 13, 'tipe' => 14, 'kegiatan_active' => 15, 'to_kegiatan_code' => 16, 'catatan' => 17, 'target_outcome' => 18, 'lokasi' => 19, 'jumlah_prev' => 20, 'jumlah_now' => 21, 'jumlah_next' => 22, 'kode_program2' => 23, 'kode_urusan' => 24, 'last_update_user' => 25, 'last_update_time' => 26, 'last_update_ip' => 27, 'tahap' => 28, 'kode_misi' => 29, 'kode_tujuan' => 30, 'ranking' => 31, 'nomor13' => 32, 'ppa_nama' => 33, 'ppa_pangkat' => 34, 'ppa_nip' => 35, 'lanjutan' => 36, 'user_id' => 37, 'id' => 38, 'tahun' => 39, 'tambahan_pagu' => 40, 'gender' => 41, 'kode_keg_keuangan' => 42, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/NextMasterKegiatanMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.NextMasterKegiatanMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = NextMasterKegiatanPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(NextMasterKegiatanPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(NextMasterKegiatanPeer::UNIT_ID);

		$criteria->addSelectColumn(NextMasterKegiatanPeer::KODE_KEGIATAN);

		$criteria->addSelectColumn(NextMasterKegiatanPeer::KODE_BIDANG);

		$criteria->addSelectColumn(NextMasterKegiatanPeer::KODE_URUSAN_WAJIB);

		$criteria->addSelectColumn(NextMasterKegiatanPeer::KODE_PROGRAM);

		$criteria->addSelectColumn(NextMasterKegiatanPeer::KODE_SASARAN);

		$criteria->addSelectColumn(NextMasterKegiatanPeer::KODE_INDIKATOR);

		$criteria->addSelectColumn(NextMasterKegiatanPeer::ALOKASI_DANA);

		$criteria->addSelectColumn(NextMasterKegiatanPeer::NAMA_KEGIATAN);

		$criteria->addSelectColumn(NextMasterKegiatanPeer::MASUKAN);

		$criteria->addSelectColumn(NextMasterKegiatanPeer::OUTPUT);

		$criteria->addSelectColumn(NextMasterKegiatanPeer::OUTCOME);

		$criteria->addSelectColumn(NextMasterKegiatanPeer::BENEFIT);

		$criteria->addSelectColumn(NextMasterKegiatanPeer::IMPACT);

		$criteria->addSelectColumn(NextMasterKegiatanPeer::TIPE);

		$criteria->addSelectColumn(NextMasterKegiatanPeer::KEGIATAN_ACTIVE);

		$criteria->addSelectColumn(NextMasterKegiatanPeer::TO_KEGIATAN_CODE);

		$criteria->addSelectColumn(NextMasterKegiatanPeer::CATATAN);

		$criteria->addSelectColumn(NextMasterKegiatanPeer::TARGET_OUTCOME);

		$criteria->addSelectColumn(NextMasterKegiatanPeer::LOKASI);

		$criteria->addSelectColumn(NextMasterKegiatanPeer::JUMLAH_PREV);

		$criteria->addSelectColumn(NextMasterKegiatanPeer::JUMLAH_NOW);

		$criteria->addSelectColumn(NextMasterKegiatanPeer::JUMLAH_NEXT);

		$criteria->addSelectColumn(NextMasterKegiatanPeer::KODE_PROGRAM2);

		$criteria->addSelectColumn(NextMasterKegiatanPeer::KODE_URUSAN);

		$criteria->addSelectColumn(NextMasterKegiatanPeer::LAST_UPDATE_USER);

		$criteria->addSelectColumn(NextMasterKegiatanPeer::LAST_UPDATE_TIME);

		$criteria->addSelectColumn(NextMasterKegiatanPeer::LAST_UPDATE_IP);

		$criteria->addSelectColumn(NextMasterKegiatanPeer::TAHAP);

		$criteria->addSelectColumn(NextMasterKegiatanPeer::KODE_MISI);

		$criteria->addSelectColumn(NextMasterKegiatanPeer::KODE_TUJUAN);

		$criteria->addSelectColumn(NextMasterKegiatanPeer::RANKING);

		$criteria->addSelectColumn(NextMasterKegiatanPeer::NOMOR13);

		$criteria->addSelectColumn(NextMasterKegiatanPeer::PPA_NAMA);

		$criteria->addSelectColumn(NextMasterKegiatanPeer::PPA_PANGKAT);

		$criteria->addSelectColumn(NextMasterKegiatanPeer::PPA_NIP);

		$criteria->addSelectColumn(NextMasterKegiatanPeer::LANJUTAN);

		$criteria->addSelectColumn(NextMasterKegiatanPeer::USER_ID);

		$criteria->addSelectColumn(NextMasterKegiatanPeer::ID);

		$criteria->addSelectColumn(NextMasterKegiatanPeer::TAHUN);

		$criteria->addSelectColumn(NextMasterKegiatanPeer::TAMBAHAN_PAGU);

		$criteria->addSelectColumn(NextMasterKegiatanPeer::GENDER);

		$criteria->addSelectColumn(NextMasterKegiatanPeer::KODE_KEG_KEUANGAN);

	}

	const COUNT = 'COUNT(ebudget.next_master_kegiatan.UNIT_ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.next_master_kegiatan.UNIT_ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(NextMasterKegiatanPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(NextMasterKegiatanPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = NextMasterKegiatanPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = NextMasterKegiatanPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return NextMasterKegiatanPeer::populateObjects(NextMasterKegiatanPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			NextMasterKegiatanPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = NextMasterKegiatanPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return NextMasterKegiatanPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}

		$criteria->remove(NextMasterKegiatanPeer::ID); 

				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(NextMasterKegiatanPeer::UNIT_ID);
			$selectCriteria->add(NextMasterKegiatanPeer::UNIT_ID, $criteria->remove(NextMasterKegiatanPeer::UNIT_ID), $comparison);

			$comparison = $criteria->getComparison(NextMasterKegiatanPeer::KODE_KEGIATAN);
			$selectCriteria->add(NextMasterKegiatanPeer::KODE_KEGIATAN, $criteria->remove(NextMasterKegiatanPeer::KODE_KEGIATAN), $comparison);

			$comparison = $criteria->getComparison(NextMasterKegiatanPeer::ID);
			$selectCriteria->add(NextMasterKegiatanPeer::ID, $criteria->remove(NextMasterKegiatanPeer::ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(NextMasterKegiatanPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(NextMasterKegiatanPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof NextMasterKegiatan) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
												if(count($values) == count($values, COUNT_RECURSIVE))
			{
								$values = array($values);
			}
			$vals = array();
			foreach($values as $value)
			{

				$vals[0][] = $value[0];
				$vals[1][] = $value[1];
				$vals[2][] = $value[2];
			}

			$criteria->add(NextMasterKegiatanPeer::UNIT_ID, $vals[0], Criteria::IN);
			$criteria->add(NextMasterKegiatanPeer::KODE_KEGIATAN, $vals[1], Criteria::IN);
			$criteria->add(NextMasterKegiatanPeer::ID, $vals[2], Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(NextMasterKegiatan $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(NextMasterKegiatanPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(NextMasterKegiatanPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(NextMasterKegiatanPeer::DATABASE_NAME, NextMasterKegiatanPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = NextMasterKegiatanPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK( $unit_id, $kode_kegiatan, $id, $con = null) {
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$criteria = new Criteria();
		$criteria->add(NextMasterKegiatanPeer::UNIT_ID, $unit_id);
		$criteria->add(NextMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
		$criteria->add(NextMasterKegiatanPeer::ID, $id);
		$v = NextMasterKegiatanPeer::doSelect($criteria, $con);

		return !empty($v) ? $v[0] : null;
	}
} 
if (Propel::isInit()) {
			try {
		BaseNextMasterKegiatanPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/NextMasterKegiatanMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.NextMasterKegiatanMapBuilder');
}
