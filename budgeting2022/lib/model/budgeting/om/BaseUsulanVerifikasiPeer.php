<?php


abstract class BaseUsulanVerifikasiPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.usulan_verifikasi';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.UsulanVerifikasi';

	
	const NUM_COLUMNS = 19;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const ID_VERIFIKASI = 'ebudget.usulan_verifikasi.ID_VERIFIKASI';

	
	const SHSD_ID = 'ebudget.usulan_verifikasi.SHSD_ID';

	
	const SHSD_NAME = 'ebudget.usulan_verifikasi.SHSD_NAME';

	
	const HARGA = 'ebudget.usulan_verifikasi.HARGA';

	
	const NAMA = 'ebudget.usulan_verifikasi.NAMA';

	
	const SPEC = 'ebudget.usulan_verifikasi.SPEC';

	
	const HIDDEN_SPEC = 'ebudget.usulan_verifikasi.HIDDEN_SPEC';

	
	const MEREK = 'ebudget.usulan_verifikasi.MEREK';

	
	const SATUAN = 'ebudget.usulan_verifikasi.SATUAN';

	
	const REKENING = 'ebudget.usulan_verifikasi.REKENING';

	
	const PAJAK = 'ebudget.usulan_verifikasi.PAJAK';

	
	const KETERANGAN = 'ebudget.usulan_verifikasi.KETERANGAN';

	
	const ID_SPJM = 'ebudget.usulan_verifikasi.ID_SPJM';

	
	const ID_USULAN = 'ebudget.usulan_verifikasi.ID_USULAN';

	
	const CREATED_AT = 'ebudget.usulan_verifikasi.CREATED_AT';

	
	const UPDATED_AT = 'ebudget.usulan_verifikasi.UPDATED_AT';

	
	const UNIT_ID = 'ebudget.usulan_verifikasi.UNIT_ID';

	
	const TIPE = 'ebudget.usulan_verifikasi.TIPE';

	
	const TAHAP = 'ebudget.usulan_verifikasi.TAHAP';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('IdVerifikasi', 'ShsdId', 'ShsdName', 'Harga', 'Nama', 'Spec', 'HiddenSpec', 'Merek', 'Satuan', 'Rekening', 'Pajak', 'Keterangan', 'IdSpjm', 'IdUsulan', 'CreatedAt', 'UpdatedAt', 'UnitId', 'Tipe', 'Tahap', ),
		BasePeer::TYPE_COLNAME => array (UsulanVerifikasiPeer::ID_VERIFIKASI, UsulanVerifikasiPeer::SHSD_ID, UsulanVerifikasiPeer::SHSD_NAME, UsulanVerifikasiPeer::HARGA, UsulanVerifikasiPeer::NAMA, UsulanVerifikasiPeer::SPEC, UsulanVerifikasiPeer::HIDDEN_SPEC, UsulanVerifikasiPeer::MEREK, UsulanVerifikasiPeer::SATUAN, UsulanVerifikasiPeer::REKENING, UsulanVerifikasiPeer::PAJAK, UsulanVerifikasiPeer::KETERANGAN, UsulanVerifikasiPeer::ID_SPJM, UsulanVerifikasiPeer::ID_USULAN, UsulanVerifikasiPeer::CREATED_AT, UsulanVerifikasiPeer::UPDATED_AT, UsulanVerifikasiPeer::UNIT_ID, UsulanVerifikasiPeer::TIPE, UsulanVerifikasiPeer::TAHAP, ),
		BasePeer::TYPE_FIELDNAME => array ('id_verifikasi', 'shsd_id', 'shsd_name', 'harga', 'nama', 'spec', 'hidden_spec', 'merek', 'satuan', 'rekening', 'pajak', 'keterangan', 'id_spjm', 'id_usulan', 'created_at', 'updated_at', 'unit_id', 'tipe', 'tahap', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('IdVerifikasi' => 0, 'ShsdId' => 1, 'ShsdName' => 2, 'Harga' => 3, 'Nama' => 4, 'Spec' => 5, 'HiddenSpec' => 6, 'Merek' => 7, 'Satuan' => 8, 'Rekening' => 9, 'Pajak' => 10, 'Keterangan' => 11, 'IdSpjm' => 12, 'IdUsulan' => 13, 'CreatedAt' => 14, 'UpdatedAt' => 15, 'UnitId' => 16, 'Tipe' => 17, 'Tahap' => 18, ),
		BasePeer::TYPE_COLNAME => array (UsulanVerifikasiPeer::ID_VERIFIKASI => 0, UsulanVerifikasiPeer::SHSD_ID => 1, UsulanVerifikasiPeer::SHSD_NAME => 2, UsulanVerifikasiPeer::HARGA => 3, UsulanVerifikasiPeer::NAMA => 4, UsulanVerifikasiPeer::SPEC => 5, UsulanVerifikasiPeer::HIDDEN_SPEC => 6, UsulanVerifikasiPeer::MEREK => 7, UsulanVerifikasiPeer::SATUAN => 8, UsulanVerifikasiPeer::REKENING => 9, UsulanVerifikasiPeer::PAJAK => 10, UsulanVerifikasiPeer::KETERANGAN => 11, UsulanVerifikasiPeer::ID_SPJM => 12, UsulanVerifikasiPeer::ID_USULAN => 13, UsulanVerifikasiPeer::CREATED_AT => 14, UsulanVerifikasiPeer::UPDATED_AT => 15, UsulanVerifikasiPeer::UNIT_ID => 16, UsulanVerifikasiPeer::TIPE => 17, UsulanVerifikasiPeer::TAHAP => 18, ),
		BasePeer::TYPE_FIELDNAME => array ('id_verifikasi' => 0, 'shsd_id' => 1, 'shsd_name' => 2, 'harga' => 3, 'nama' => 4, 'spec' => 5, 'hidden_spec' => 6, 'merek' => 7, 'satuan' => 8, 'rekening' => 9, 'pajak' => 10, 'keterangan' => 11, 'id_spjm' => 12, 'id_usulan' => 13, 'created_at' => 14, 'updated_at' => 15, 'unit_id' => 16, 'tipe' => 17, 'tahap' => 18, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/UsulanVerifikasiMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.UsulanVerifikasiMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = UsulanVerifikasiPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(UsulanVerifikasiPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(UsulanVerifikasiPeer::ID_VERIFIKASI);

		$criteria->addSelectColumn(UsulanVerifikasiPeer::SHSD_ID);

		$criteria->addSelectColumn(UsulanVerifikasiPeer::SHSD_NAME);

		$criteria->addSelectColumn(UsulanVerifikasiPeer::HARGA);

		$criteria->addSelectColumn(UsulanVerifikasiPeer::NAMA);

		$criteria->addSelectColumn(UsulanVerifikasiPeer::SPEC);

		$criteria->addSelectColumn(UsulanVerifikasiPeer::HIDDEN_SPEC);

		$criteria->addSelectColumn(UsulanVerifikasiPeer::MEREK);

		$criteria->addSelectColumn(UsulanVerifikasiPeer::SATUAN);

		$criteria->addSelectColumn(UsulanVerifikasiPeer::REKENING);

		$criteria->addSelectColumn(UsulanVerifikasiPeer::PAJAK);

		$criteria->addSelectColumn(UsulanVerifikasiPeer::KETERANGAN);

		$criteria->addSelectColumn(UsulanVerifikasiPeer::ID_SPJM);

		$criteria->addSelectColumn(UsulanVerifikasiPeer::ID_USULAN);

		$criteria->addSelectColumn(UsulanVerifikasiPeer::CREATED_AT);

		$criteria->addSelectColumn(UsulanVerifikasiPeer::UPDATED_AT);

		$criteria->addSelectColumn(UsulanVerifikasiPeer::UNIT_ID);

		$criteria->addSelectColumn(UsulanVerifikasiPeer::TIPE);

		$criteria->addSelectColumn(UsulanVerifikasiPeer::TAHAP);

	}

	const COUNT = 'COUNT(ebudget.usulan_verifikasi.ID_VERIFIKASI)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.usulan_verifikasi.ID_VERIFIKASI)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(UsulanVerifikasiPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(UsulanVerifikasiPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = UsulanVerifikasiPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = UsulanVerifikasiPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return UsulanVerifikasiPeer::populateObjects(UsulanVerifikasiPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			UsulanVerifikasiPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = UsulanVerifikasiPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return UsulanVerifikasiPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(UsulanVerifikasiPeer::ID_VERIFIKASI);
			$selectCriteria->add(UsulanVerifikasiPeer::ID_VERIFIKASI, $criteria->remove(UsulanVerifikasiPeer::ID_VERIFIKASI), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(UsulanVerifikasiPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(UsulanVerifikasiPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof UsulanVerifikasi) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(UsulanVerifikasiPeer::ID_VERIFIKASI, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(UsulanVerifikasi $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(UsulanVerifikasiPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(UsulanVerifikasiPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(UsulanVerifikasiPeer::DATABASE_NAME, UsulanVerifikasiPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = UsulanVerifikasiPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(UsulanVerifikasiPeer::DATABASE_NAME);

		$criteria->add(UsulanVerifikasiPeer::ID_VERIFIKASI, $pk);


		$v = UsulanVerifikasiPeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(UsulanVerifikasiPeer::ID_VERIFIKASI, $pks, Criteria::IN);
			$objs = UsulanVerifikasiPeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BaseUsulanVerifikasiPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/UsulanVerifikasiMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.UsulanVerifikasiMapBuilder');
}
