<?php


abstract class BaseRevisi1SubtitleIndikatorPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.revisi1_subtitle_indikator';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.Revisi1SubtitleIndikator';

	
	const NUM_COLUMNS = 22;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const UNIT_ID = 'ebudget.revisi1_subtitle_indikator.UNIT_ID';

	
	const KEGIATAN_CODE = 'ebudget.revisi1_subtitle_indikator.KEGIATAN_CODE';

	
	const SUBTITLE = 'ebudget.revisi1_subtitle_indikator.SUBTITLE';

	
	const INDIKATOR = 'ebudget.revisi1_subtitle_indikator.INDIKATOR';

	
	const NILAI = 'ebudget.revisi1_subtitle_indikator.NILAI';

	
	const SATUAN = 'ebudget.revisi1_subtitle_indikator.SATUAN';

	
	const LAST_UPDATE_USER = 'ebudget.revisi1_subtitle_indikator.LAST_UPDATE_USER';

	
	const LAST_UPDATE_TIME = 'ebudget.revisi1_subtitle_indikator.LAST_UPDATE_TIME';

	
	const LAST_UPDATE_IP = 'ebudget.revisi1_subtitle_indikator.LAST_UPDATE_IP';

	
	const TAHAP = 'ebudget.revisi1_subtitle_indikator.TAHAP';

	
	const SUB_ID = 'ebudget.revisi1_subtitle_indikator.SUB_ID';

	
	const TAHUN = 'ebudget.revisi1_subtitle_indikator.TAHUN';

	
	const LOCK_SUBTITLE = 'ebudget.revisi1_subtitle_indikator.LOCK_SUBTITLE';

	
	const PRIORITAS = 'ebudget.revisi1_subtitle_indikator.PRIORITAS';

	
	const CATATAN = 'ebudget.revisi1_subtitle_indikator.CATATAN';

	
	const LAKILAKI = 'ebudget.revisi1_subtitle_indikator.LAKILAKI';

	
	const PEREMPUAN = 'ebudget.revisi1_subtitle_indikator.PEREMPUAN';

	
	const DEWASA = 'ebudget.revisi1_subtitle_indikator.DEWASA';

	
	const ANAK = 'ebudget.revisi1_subtitle_indikator.ANAK';

	
	const LANSIA = 'ebudget.revisi1_subtitle_indikator.LANSIA';

	
	const INKLUSI = 'ebudget.revisi1_subtitle_indikator.INKLUSI';

	
	const GENDER = 'ebudget.revisi1_subtitle_indikator.GENDER';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('UnitId', 'KegiatanCode', 'Subtitle', 'Indikator', 'Nilai', 'Satuan', 'LastUpdateUser', 'LastUpdateTime', 'LastUpdateIp', 'Tahap', 'SubId', 'Tahun', 'LockSubtitle', 'Prioritas', 'Catatan', 'Lakilaki', 'Perempuan', 'Dewasa', 'Anak', 'Lansia', 'Inklusi', 'Gender', ),
		BasePeer::TYPE_COLNAME => array (Revisi1SubtitleIndikatorPeer::UNIT_ID, Revisi1SubtitleIndikatorPeer::KEGIATAN_CODE, Revisi1SubtitleIndikatorPeer::SUBTITLE, Revisi1SubtitleIndikatorPeer::INDIKATOR, Revisi1SubtitleIndikatorPeer::NILAI, Revisi1SubtitleIndikatorPeer::SATUAN, Revisi1SubtitleIndikatorPeer::LAST_UPDATE_USER, Revisi1SubtitleIndikatorPeer::LAST_UPDATE_TIME, Revisi1SubtitleIndikatorPeer::LAST_UPDATE_IP, Revisi1SubtitleIndikatorPeer::TAHAP, Revisi1SubtitleIndikatorPeer::SUB_ID, Revisi1SubtitleIndikatorPeer::TAHUN, Revisi1SubtitleIndikatorPeer::LOCK_SUBTITLE, Revisi1SubtitleIndikatorPeer::PRIORITAS, Revisi1SubtitleIndikatorPeer::CATATAN, Revisi1SubtitleIndikatorPeer::LAKILAKI, Revisi1SubtitleIndikatorPeer::PEREMPUAN, Revisi1SubtitleIndikatorPeer::DEWASA, Revisi1SubtitleIndikatorPeer::ANAK, Revisi1SubtitleIndikatorPeer::LANSIA, Revisi1SubtitleIndikatorPeer::INKLUSI, Revisi1SubtitleIndikatorPeer::GENDER, ),
		BasePeer::TYPE_FIELDNAME => array ('unit_id', 'kegiatan_code', 'subtitle', 'indikator', 'nilai', 'satuan', 'last_update_user', 'last_update_time', 'last_update_ip', 'tahap', 'sub_id', 'tahun', 'lock_subtitle', 'prioritas', 'catatan', 'lakilaki', 'perempuan', 'dewasa', 'anak', 'lansia', 'inklusi', 'gender', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('UnitId' => 0, 'KegiatanCode' => 1, 'Subtitle' => 2, 'Indikator' => 3, 'Nilai' => 4, 'Satuan' => 5, 'LastUpdateUser' => 6, 'LastUpdateTime' => 7, 'LastUpdateIp' => 8, 'Tahap' => 9, 'SubId' => 10, 'Tahun' => 11, 'LockSubtitle' => 12, 'Prioritas' => 13, 'Catatan' => 14, 'Lakilaki' => 15, 'Perempuan' => 16, 'Dewasa' => 17, 'Anak' => 18, 'Lansia' => 19, 'Inklusi' => 20, 'Gender' => 21, ),
		BasePeer::TYPE_COLNAME => array (Revisi1SubtitleIndikatorPeer::UNIT_ID => 0, Revisi1SubtitleIndikatorPeer::KEGIATAN_CODE => 1, Revisi1SubtitleIndikatorPeer::SUBTITLE => 2, Revisi1SubtitleIndikatorPeer::INDIKATOR => 3, Revisi1SubtitleIndikatorPeer::NILAI => 4, Revisi1SubtitleIndikatorPeer::SATUAN => 5, Revisi1SubtitleIndikatorPeer::LAST_UPDATE_USER => 6, Revisi1SubtitleIndikatorPeer::LAST_UPDATE_TIME => 7, Revisi1SubtitleIndikatorPeer::LAST_UPDATE_IP => 8, Revisi1SubtitleIndikatorPeer::TAHAP => 9, Revisi1SubtitleIndikatorPeer::SUB_ID => 10, Revisi1SubtitleIndikatorPeer::TAHUN => 11, Revisi1SubtitleIndikatorPeer::LOCK_SUBTITLE => 12, Revisi1SubtitleIndikatorPeer::PRIORITAS => 13, Revisi1SubtitleIndikatorPeer::CATATAN => 14, Revisi1SubtitleIndikatorPeer::LAKILAKI => 15, Revisi1SubtitleIndikatorPeer::PEREMPUAN => 16, Revisi1SubtitleIndikatorPeer::DEWASA => 17, Revisi1SubtitleIndikatorPeer::ANAK => 18, Revisi1SubtitleIndikatorPeer::LANSIA => 19, Revisi1SubtitleIndikatorPeer::INKLUSI => 20, Revisi1SubtitleIndikatorPeer::GENDER => 21, ),
		BasePeer::TYPE_FIELDNAME => array ('unit_id' => 0, 'kegiatan_code' => 1, 'subtitle' => 2, 'indikator' => 3, 'nilai' => 4, 'satuan' => 5, 'last_update_user' => 6, 'last_update_time' => 7, 'last_update_ip' => 8, 'tahap' => 9, 'sub_id' => 10, 'tahun' => 11, 'lock_subtitle' => 12, 'prioritas' => 13, 'catatan' => 14, 'lakilaki' => 15, 'perempuan' => 16, 'dewasa' => 17, 'anak' => 18, 'lansia' => 19, 'inklusi' => 20, 'gender' => 21, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/Revisi1SubtitleIndikatorMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.Revisi1SubtitleIndikatorMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = Revisi1SubtitleIndikatorPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(Revisi1SubtitleIndikatorPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(Revisi1SubtitleIndikatorPeer::UNIT_ID);

		$criteria->addSelectColumn(Revisi1SubtitleIndikatorPeer::KEGIATAN_CODE);

		$criteria->addSelectColumn(Revisi1SubtitleIndikatorPeer::SUBTITLE);

		$criteria->addSelectColumn(Revisi1SubtitleIndikatorPeer::INDIKATOR);

		$criteria->addSelectColumn(Revisi1SubtitleIndikatorPeer::NILAI);

		$criteria->addSelectColumn(Revisi1SubtitleIndikatorPeer::SATUAN);

		$criteria->addSelectColumn(Revisi1SubtitleIndikatorPeer::LAST_UPDATE_USER);

		$criteria->addSelectColumn(Revisi1SubtitleIndikatorPeer::LAST_UPDATE_TIME);

		$criteria->addSelectColumn(Revisi1SubtitleIndikatorPeer::LAST_UPDATE_IP);

		$criteria->addSelectColumn(Revisi1SubtitleIndikatorPeer::TAHAP);

		$criteria->addSelectColumn(Revisi1SubtitleIndikatorPeer::SUB_ID);

		$criteria->addSelectColumn(Revisi1SubtitleIndikatorPeer::TAHUN);

		$criteria->addSelectColumn(Revisi1SubtitleIndikatorPeer::LOCK_SUBTITLE);

		$criteria->addSelectColumn(Revisi1SubtitleIndikatorPeer::PRIORITAS);

		$criteria->addSelectColumn(Revisi1SubtitleIndikatorPeer::CATATAN);

		$criteria->addSelectColumn(Revisi1SubtitleIndikatorPeer::LAKILAKI);

		$criteria->addSelectColumn(Revisi1SubtitleIndikatorPeer::PEREMPUAN);

		$criteria->addSelectColumn(Revisi1SubtitleIndikatorPeer::DEWASA);

		$criteria->addSelectColumn(Revisi1SubtitleIndikatorPeer::ANAK);

		$criteria->addSelectColumn(Revisi1SubtitleIndikatorPeer::LANSIA);

		$criteria->addSelectColumn(Revisi1SubtitleIndikatorPeer::INKLUSI);

		$criteria->addSelectColumn(Revisi1SubtitleIndikatorPeer::GENDER);

	}

	const COUNT = 'COUNT(ebudget.revisi1_subtitle_indikator.SUB_ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.revisi1_subtitle_indikator.SUB_ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(Revisi1SubtitleIndikatorPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(Revisi1SubtitleIndikatorPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = Revisi1SubtitleIndikatorPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = Revisi1SubtitleIndikatorPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return Revisi1SubtitleIndikatorPeer::populateObjects(Revisi1SubtitleIndikatorPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			Revisi1SubtitleIndikatorPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = Revisi1SubtitleIndikatorPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return Revisi1SubtitleIndikatorPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}

		$criteria->remove(Revisi1SubtitleIndikatorPeer::SUB_ID); 

				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(Revisi1SubtitleIndikatorPeer::SUB_ID);
			$selectCriteria->add(Revisi1SubtitleIndikatorPeer::SUB_ID, $criteria->remove(Revisi1SubtitleIndikatorPeer::SUB_ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(Revisi1SubtitleIndikatorPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(Revisi1SubtitleIndikatorPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof Revisi1SubtitleIndikator) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(Revisi1SubtitleIndikatorPeer::SUB_ID, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(Revisi1SubtitleIndikator $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(Revisi1SubtitleIndikatorPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(Revisi1SubtitleIndikatorPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(Revisi1SubtitleIndikatorPeer::DATABASE_NAME, Revisi1SubtitleIndikatorPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = Revisi1SubtitleIndikatorPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(Revisi1SubtitleIndikatorPeer::DATABASE_NAME);

		$criteria->add(Revisi1SubtitleIndikatorPeer::SUB_ID, $pk);


		$v = Revisi1SubtitleIndikatorPeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(Revisi1SubtitleIndikatorPeer::SUB_ID, $pks, Criteria::IN);
			$objs = Revisi1SubtitleIndikatorPeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BaseRevisi1SubtitleIndikatorPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/Revisi1SubtitleIndikatorMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.Revisi1SubtitleIndikatorMapBuilder');
}
