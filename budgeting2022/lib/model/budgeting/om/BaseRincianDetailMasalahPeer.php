<?php


abstract class BaseRincianDetailMasalahPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.rincian_detail_masalah';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.RincianDetailMasalah';

	
	const NUM_COLUMNS = 35;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const KEGIATAN_CODE = 'ebudget.rincian_detail_masalah.KEGIATAN_CODE';

	
	const TIPE = 'ebudget.rincian_detail_masalah.TIPE';

	
	const DETAIL_NO = 'ebudget.rincian_detail_masalah.DETAIL_NO';

	
	const REKENING_CODE = 'ebudget.rincian_detail_masalah.REKENING_CODE';

	
	const KOMPONEN_ID = 'ebudget.rincian_detail_masalah.KOMPONEN_ID';

	
	const DETAIL_NAME = 'ebudget.rincian_detail_masalah.DETAIL_NAME';

	
	const VOLUME = 'ebudget.rincian_detail_masalah.VOLUME';

	
	const KETERANGAN_KOEFISIEN = 'ebudget.rincian_detail_masalah.KETERANGAN_KOEFISIEN';

	
	const SUBTITLE = 'ebudget.rincian_detail_masalah.SUBTITLE';

	
	const KOMPONEN_HARGA = 'ebudget.rincian_detail_masalah.KOMPONEN_HARGA';

	
	const KOMPONEN_HARGA_AWAL = 'ebudget.rincian_detail_masalah.KOMPONEN_HARGA_AWAL';

	
	const KOMPONEN_NAME = 'ebudget.rincian_detail_masalah.KOMPONEN_NAME';

	
	const SATUAN = 'ebudget.rincian_detail_masalah.SATUAN';

	
	const PAJAK = 'ebudget.rincian_detail_masalah.PAJAK';

	
	const UNIT_ID = 'ebudget.rincian_detail_masalah.UNIT_ID';

	
	const FROM_SUB_KEGIATAN = 'ebudget.rincian_detail_masalah.FROM_SUB_KEGIATAN';

	
	const SUB = 'ebudget.rincian_detail_masalah.SUB';

	
	const KODE_SUB = 'ebudget.rincian_detail_masalah.KODE_SUB';

	
	const LAST_UPDATE_USER = 'ebudget.rincian_detail_masalah.LAST_UPDATE_USER';

	
	const LAST_UPDATE_TIME = 'ebudget.rincian_detail_masalah.LAST_UPDATE_TIME';

	
	const LAST_UPDATE_IP = 'ebudget.rincian_detail_masalah.LAST_UPDATE_IP';

	
	const TAHAP = 'ebudget.rincian_detail_masalah.TAHAP';

	
	const TAHAP_EDIT = 'ebudget.rincian_detail_masalah.TAHAP_EDIT';

	
	const TAHAP_NEW = 'ebudget.rincian_detail_masalah.TAHAP_NEW';

	
	const STATUS_LELANG = 'ebudget.rincian_detail_masalah.STATUS_LELANG';

	
	const NOMOR_LELANG = 'ebudget.rincian_detail_masalah.NOMOR_LELANG';

	
	const KOEFISIEN_SEMULA = 'ebudget.rincian_detail_masalah.KOEFISIEN_SEMULA';

	
	const VOLUME_SEMULA = 'ebudget.rincian_detail_masalah.VOLUME_SEMULA';

	
	const HARGA_SEMULA = 'ebudget.rincian_detail_masalah.HARGA_SEMULA';

	
	const TOTAL_SEMULA = 'ebudget.rincian_detail_masalah.TOTAL_SEMULA';

	
	const LOCK_SUBTITLE = 'ebudget.rincian_detail_masalah.LOCK_SUBTITLE';

	
	const STATUS_HAPUS = 'ebudget.rincian_detail_masalah.STATUS_HAPUS';

	
	const TAHUN = 'ebudget.rincian_detail_masalah.TAHUN';

	
	const KODE_LOKASI = 'ebudget.rincian_detail_masalah.KODE_LOKASI';

	
	const KECAMATAN = 'ebudget.rincian_detail_masalah.KECAMATAN';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('KegiatanCode', 'Tipe', 'DetailNo', 'RekeningCode', 'KomponenId', 'DetailName', 'Volume', 'KeteranganKoefisien', 'Subtitle', 'KomponenHarga', 'KomponenHargaAwal', 'KomponenName', 'Satuan', 'Pajak', 'UnitId', 'FromSubKegiatan', 'Sub', 'KodeSub', 'LastUpdateUser', 'LastUpdateTime', 'LastUpdateIp', 'Tahap', 'TahapEdit', 'TahapNew', 'StatusLelang', 'NomorLelang', 'KoefisienSemula', 'VolumeSemula', 'HargaSemula', 'TotalSemula', 'LockSubtitle', 'StatusHapus', 'Tahun', 'KodeLokasi', 'Kecamatan', ),
		BasePeer::TYPE_COLNAME => array (RincianDetailMasalahPeer::KEGIATAN_CODE, RincianDetailMasalahPeer::TIPE, RincianDetailMasalahPeer::DETAIL_NO, RincianDetailMasalahPeer::REKENING_CODE, RincianDetailMasalahPeer::KOMPONEN_ID, RincianDetailMasalahPeer::DETAIL_NAME, RincianDetailMasalahPeer::VOLUME, RincianDetailMasalahPeer::KETERANGAN_KOEFISIEN, RincianDetailMasalahPeer::SUBTITLE, RincianDetailMasalahPeer::KOMPONEN_HARGA, RincianDetailMasalahPeer::KOMPONEN_HARGA_AWAL, RincianDetailMasalahPeer::KOMPONEN_NAME, RincianDetailMasalahPeer::SATUAN, RincianDetailMasalahPeer::PAJAK, RincianDetailMasalahPeer::UNIT_ID, RincianDetailMasalahPeer::FROM_SUB_KEGIATAN, RincianDetailMasalahPeer::SUB, RincianDetailMasalahPeer::KODE_SUB, RincianDetailMasalahPeer::LAST_UPDATE_USER, RincianDetailMasalahPeer::LAST_UPDATE_TIME, RincianDetailMasalahPeer::LAST_UPDATE_IP, RincianDetailMasalahPeer::TAHAP, RincianDetailMasalahPeer::TAHAP_EDIT, RincianDetailMasalahPeer::TAHAP_NEW, RincianDetailMasalahPeer::STATUS_LELANG, RincianDetailMasalahPeer::NOMOR_LELANG, RincianDetailMasalahPeer::KOEFISIEN_SEMULA, RincianDetailMasalahPeer::VOLUME_SEMULA, RincianDetailMasalahPeer::HARGA_SEMULA, RincianDetailMasalahPeer::TOTAL_SEMULA, RincianDetailMasalahPeer::LOCK_SUBTITLE, RincianDetailMasalahPeer::STATUS_HAPUS, RincianDetailMasalahPeer::TAHUN, RincianDetailMasalahPeer::KODE_LOKASI, RincianDetailMasalahPeer::KECAMATAN, ),
		BasePeer::TYPE_FIELDNAME => array ('kegiatan_code', 'tipe', 'detail_no', 'rekening_code', 'komponen_id', 'detail_name', 'volume', 'keterangan_koefisien', 'subtitle', 'komponen_harga', 'komponen_harga_awal', 'komponen_name', 'satuan', 'pajak', 'unit_id', 'from_sub_kegiatan', 'sub', 'kode_sub', 'last_update_user', 'last_update_time', 'last_update_ip', 'tahap', 'tahap_edit', 'tahap_new', 'status_lelang', 'nomor_lelang', 'koefisien_semula', 'volume_semula', 'harga_semula', 'total_semula', 'lock_subtitle', 'status_hapus', 'tahun', 'kode_lokasi', 'kecamatan', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('KegiatanCode' => 0, 'Tipe' => 1, 'DetailNo' => 2, 'RekeningCode' => 3, 'KomponenId' => 4, 'DetailName' => 5, 'Volume' => 6, 'KeteranganKoefisien' => 7, 'Subtitle' => 8, 'KomponenHarga' => 9, 'KomponenHargaAwal' => 10, 'KomponenName' => 11, 'Satuan' => 12, 'Pajak' => 13, 'UnitId' => 14, 'FromSubKegiatan' => 15, 'Sub' => 16, 'KodeSub' => 17, 'LastUpdateUser' => 18, 'LastUpdateTime' => 19, 'LastUpdateIp' => 20, 'Tahap' => 21, 'TahapEdit' => 22, 'TahapNew' => 23, 'StatusLelang' => 24, 'NomorLelang' => 25, 'KoefisienSemula' => 26, 'VolumeSemula' => 27, 'HargaSemula' => 28, 'TotalSemula' => 29, 'LockSubtitle' => 30, 'StatusHapus' => 31, 'Tahun' => 32, 'KodeLokasi' => 33, 'Kecamatan' => 34, ),
		BasePeer::TYPE_COLNAME => array (RincianDetailMasalahPeer::KEGIATAN_CODE => 0, RincianDetailMasalahPeer::TIPE => 1, RincianDetailMasalahPeer::DETAIL_NO => 2, RincianDetailMasalahPeer::REKENING_CODE => 3, RincianDetailMasalahPeer::KOMPONEN_ID => 4, RincianDetailMasalahPeer::DETAIL_NAME => 5, RincianDetailMasalahPeer::VOLUME => 6, RincianDetailMasalahPeer::KETERANGAN_KOEFISIEN => 7, RincianDetailMasalahPeer::SUBTITLE => 8, RincianDetailMasalahPeer::KOMPONEN_HARGA => 9, RincianDetailMasalahPeer::KOMPONEN_HARGA_AWAL => 10, RincianDetailMasalahPeer::KOMPONEN_NAME => 11, RincianDetailMasalahPeer::SATUAN => 12, RincianDetailMasalahPeer::PAJAK => 13, RincianDetailMasalahPeer::UNIT_ID => 14, RincianDetailMasalahPeer::FROM_SUB_KEGIATAN => 15, RincianDetailMasalahPeer::SUB => 16, RincianDetailMasalahPeer::KODE_SUB => 17, RincianDetailMasalahPeer::LAST_UPDATE_USER => 18, RincianDetailMasalahPeer::LAST_UPDATE_TIME => 19, RincianDetailMasalahPeer::LAST_UPDATE_IP => 20, RincianDetailMasalahPeer::TAHAP => 21, RincianDetailMasalahPeer::TAHAP_EDIT => 22, RincianDetailMasalahPeer::TAHAP_NEW => 23, RincianDetailMasalahPeer::STATUS_LELANG => 24, RincianDetailMasalahPeer::NOMOR_LELANG => 25, RincianDetailMasalahPeer::KOEFISIEN_SEMULA => 26, RincianDetailMasalahPeer::VOLUME_SEMULA => 27, RincianDetailMasalahPeer::HARGA_SEMULA => 28, RincianDetailMasalahPeer::TOTAL_SEMULA => 29, RincianDetailMasalahPeer::LOCK_SUBTITLE => 30, RincianDetailMasalahPeer::STATUS_HAPUS => 31, RincianDetailMasalahPeer::TAHUN => 32, RincianDetailMasalahPeer::KODE_LOKASI => 33, RincianDetailMasalahPeer::KECAMATAN => 34, ),
		BasePeer::TYPE_FIELDNAME => array ('kegiatan_code' => 0, 'tipe' => 1, 'detail_no' => 2, 'rekening_code' => 3, 'komponen_id' => 4, 'detail_name' => 5, 'volume' => 6, 'keterangan_koefisien' => 7, 'subtitle' => 8, 'komponen_harga' => 9, 'komponen_harga_awal' => 10, 'komponen_name' => 11, 'satuan' => 12, 'pajak' => 13, 'unit_id' => 14, 'from_sub_kegiatan' => 15, 'sub' => 16, 'kode_sub' => 17, 'last_update_user' => 18, 'last_update_time' => 19, 'last_update_ip' => 20, 'tahap' => 21, 'tahap_edit' => 22, 'tahap_new' => 23, 'status_lelang' => 24, 'nomor_lelang' => 25, 'koefisien_semula' => 26, 'volume_semula' => 27, 'harga_semula' => 28, 'total_semula' => 29, 'lock_subtitle' => 30, 'status_hapus' => 31, 'tahun' => 32, 'kode_lokasi' => 33, 'kecamatan' => 34, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/RincianDetailMasalahMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.RincianDetailMasalahMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = RincianDetailMasalahPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(RincianDetailMasalahPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(RincianDetailMasalahPeer::KEGIATAN_CODE);

		$criteria->addSelectColumn(RincianDetailMasalahPeer::TIPE);

		$criteria->addSelectColumn(RincianDetailMasalahPeer::DETAIL_NO);

		$criteria->addSelectColumn(RincianDetailMasalahPeer::REKENING_CODE);

		$criteria->addSelectColumn(RincianDetailMasalahPeer::KOMPONEN_ID);

		$criteria->addSelectColumn(RincianDetailMasalahPeer::DETAIL_NAME);

		$criteria->addSelectColumn(RincianDetailMasalahPeer::VOLUME);

		$criteria->addSelectColumn(RincianDetailMasalahPeer::KETERANGAN_KOEFISIEN);

		$criteria->addSelectColumn(RincianDetailMasalahPeer::SUBTITLE);

		$criteria->addSelectColumn(RincianDetailMasalahPeer::KOMPONEN_HARGA);

		$criteria->addSelectColumn(RincianDetailMasalahPeer::KOMPONEN_HARGA_AWAL);

		$criteria->addSelectColumn(RincianDetailMasalahPeer::KOMPONEN_NAME);

		$criteria->addSelectColumn(RincianDetailMasalahPeer::SATUAN);

		$criteria->addSelectColumn(RincianDetailMasalahPeer::PAJAK);

		$criteria->addSelectColumn(RincianDetailMasalahPeer::UNIT_ID);

		$criteria->addSelectColumn(RincianDetailMasalahPeer::FROM_SUB_KEGIATAN);

		$criteria->addSelectColumn(RincianDetailMasalahPeer::SUB);

		$criteria->addSelectColumn(RincianDetailMasalahPeer::KODE_SUB);

		$criteria->addSelectColumn(RincianDetailMasalahPeer::LAST_UPDATE_USER);

		$criteria->addSelectColumn(RincianDetailMasalahPeer::LAST_UPDATE_TIME);

		$criteria->addSelectColumn(RincianDetailMasalahPeer::LAST_UPDATE_IP);

		$criteria->addSelectColumn(RincianDetailMasalahPeer::TAHAP);

		$criteria->addSelectColumn(RincianDetailMasalahPeer::TAHAP_EDIT);

		$criteria->addSelectColumn(RincianDetailMasalahPeer::TAHAP_NEW);

		$criteria->addSelectColumn(RincianDetailMasalahPeer::STATUS_LELANG);

		$criteria->addSelectColumn(RincianDetailMasalahPeer::NOMOR_LELANG);

		$criteria->addSelectColumn(RincianDetailMasalahPeer::KOEFISIEN_SEMULA);

		$criteria->addSelectColumn(RincianDetailMasalahPeer::VOLUME_SEMULA);

		$criteria->addSelectColumn(RincianDetailMasalahPeer::HARGA_SEMULA);

		$criteria->addSelectColumn(RincianDetailMasalahPeer::TOTAL_SEMULA);

		$criteria->addSelectColumn(RincianDetailMasalahPeer::LOCK_SUBTITLE);

		$criteria->addSelectColumn(RincianDetailMasalahPeer::STATUS_HAPUS);

		$criteria->addSelectColumn(RincianDetailMasalahPeer::TAHUN);

		$criteria->addSelectColumn(RincianDetailMasalahPeer::KODE_LOKASI);

		$criteria->addSelectColumn(RincianDetailMasalahPeer::KECAMATAN);

	}

	const COUNT = 'COUNT(ebudget.rincian_detail_masalah.KEGIATAN_CODE)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.rincian_detail_masalah.KEGIATAN_CODE)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(RincianDetailMasalahPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(RincianDetailMasalahPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = RincianDetailMasalahPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = RincianDetailMasalahPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return RincianDetailMasalahPeer::populateObjects(RincianDetailMasalahPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			RincianDetailMasalahPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = RincianDetailMasalahPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return RincianDetailMasalahPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(RincianDetailMasalahPeer::KEGIATAN_CODE);
			$selectCriteria->add(RincianDetailMasalahPeer::KEGIATAN_CODE, $criteria->remove(RincianDetailMasalahPeer::KEGIATAN_CODE), $comparison);

			$comparison = $criteria->getComparison(RincianDetailMasalahPeer::TIPE);
			$selectCriteria->add(RincianDetailMasalahPeer::TIPE, $criteria->remove(RincianDetailMasalahPeer::TIPE), $comparison);

			$comparison = $criteria->getComparison(RincianDetailMasalahPeer::DETAIL_NO);
			$selectCriteria->add(RincianDetailMasalahPeer::DETAIL_NO, $criteria->remove(RincianDetailMasalahPeer::DETAIL_NO), $comparison);

			$comparison = $criteria->getComparison(RincianDetailMasalahPeer::UNIT_ID);
			$selectCriteria->add(RincianDetailMasalahPeer::UNIT_ID, $criteria->remove(RincianDetailMasalahPeer::UNIT_ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(RincianDetailMasalahPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(RincianDetailMasalahPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof RincianDetailMasalah) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
												if(count($values) == count($values, COUNT_RECURSIVE))
			{
								$values = array($values);
			}
			$vals = array();
			foreach($values as $value)
			{

				$vals[0][] = $value[0];
				$vals[1][] = $value[1];
				$vals[2][] = $value[2];
				$vals[3][] = $value[3];
			}

			$criteria->add(RincianDetailMasalahPeer::KEGIATAN_CODE, $vals[0], Criteria::IN);
			$criteria->add(RincianDetailMasalahPeer::TIPE, $vals[1], Criteria::IN);
			$criteria->add(RincianDetailMasalahPeer::DETAIL_NO, $vals[2], Criteria::IN);
			$criteria->add(RincianDetailMasalahPeer::UNIT_ID, $vals[3], Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(RincianDetailMasalah $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(RincianDetailMasalahPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(RincianDetailMasalahPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(RincianDetailMasalahPeer::DATABASE_NAME, RincianDetailMasalahPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = RincianDetailMasalahPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK( $kegiatan_code, $tipe, $detail_no, $unit_id, $con = null) {
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$criteria = new Criteria();
		$criteria->add(RincianDetailMasalahPeer::KEGIATAN_CODE, $kegiatan_code);
		$criteria->add(RincianDetailMasalahPeer::TIPE, $tipe);
		$criteria->add(RincianDetailMasalahPeer::DETAIL_NO, $detail_no);
		$criteria->add(RincianDetailMasalahPeer::UNIT_ID, $unit_id);
		$v = RincianDetailMasalahPeer::doSelect($criteria, $con);

		return !empty($v) ? $v[0] : null;
	}
} 
if (Propel::isInit()) {
			try {
		BaseRincianDetailMasalahPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/RincianDetailMasalahMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.RincianDetailMasalahMapBuilder');
}
