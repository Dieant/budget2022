<?php


abstract class BaseKomponen extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $komponen_id;


	
	protected $satuan;


	
	protected $komponen_name;


	
	protected $shsd_id;


	
	protected $komponen_harga;


	
	protected $komponen_show = true;


	
	protected $ip_address;


	
	protected $waktu_access;


	
	protected $komponen_tipe;


	
	protected $komponen_confirmed = false;


	
	protected $komponen_non_pajak = false;


	
	protected $user_id;


	
	protected $rekening;


	
	protected $kelompok;


	
	protected $pemeliharaan = 0;


	
	protected $rek_upah;


	
	protected $rek_bahan;


	
	protected $rek_sewa;


	
	protected $deskripsi;


	
	protected $status_masuk;


	
	protected $rka_member;


	
	protected $maintenance;


	
	protected $is_potong_bpjs = false;


	
	protected $is_iuran_bpjs = false;


	
	protected $usulan_skpd;


	
	protected $tahap;


	
	protected $is_est_fisik;


	
	protected $akrual_code;


	
	protected $kode_akrual_komponen_penyusun;


	
	protected $komponen_tipe2;


	
	protected $is_survey_bp = false;


	
	protected $komponen_harga_bulat;


	
	protected $is_iuran_jkn = false;


	
	protected $is_iuran_jkk = false;


	
	protected $is_iuran_jk = false;


	
	protected $is_narsum = false;


	
	protected $is_rab = false;


	
	protected $is_bbm = false;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getKomponenId()
	{

		return $this->komponen_id;
	}

	
	public function getSatuan()
	{

		return $this->satuan;
	}

	
	public function getKomponenName()
	{

		return $this->komponen_name;
	}

	
	public function getShsdId()
	{

		return $this->shsd_id;
	}

	
	public function getKomponenHarga()
	{

		return $this->komponen_harga;
	}

	
	public function getKomponenShow()
	{

		return $this->komponen_show;
	}

	
	public function getIpAddress()
	{

		return $this->ip_address;
	}

	
	public function getWaktuAccess($format = 'Y-m-d H:i:s')
	{

		if ($this->waktu_access === null || $this->waktu_access === '') {
			return null;
		} elseif (!is_int($this->waktu_access)) {
						$ts = strtotime($this->waktu_access);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [waktu_access] as date/time value: " . var_export($this->waktu_access, true));
			}
		} else {
			$ts = $this->waktu_access;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getKomponenTipe()
	{

		return $this->komponen_tipe;
	}

	
	public function getKomponenConfirmed()
	{

		return $this->komponen_confirmed;
	}

	
	public function getKomponenNonPajak()
	{

		return $this->komponen_non_pajak;
	}

	
	public function getUserId()
	{

		return $this->user_id;
	}

	
	public function getRekening()
	{

		return $this->rekening;
	}

	
	public function getKelompok()
	{

		return $this->kelompok;
	}

	
	public function getPemeliharaan()
	{

		return $this->pemeliharaan;
	}

	
	public function getRekUpah()
	{

		return $this->rek_upah;
	}

	
	public function getRekBahan()
	{

		return $this->rek_bahan;
	}

	
	public function getRekSewa()
	{

		return $this->rek_sewa;
	}

	
	public function getDeskripsi()
	{

		return $this->deskripsi;
	}

	
	public function getStatusMasuk()
	{

		return $this->status_masuk;
	}

	
	public function getRkaMember()
	{

		return $this->rka_member;
	}

	
	public function getMaintenance()
	{

		return $this->maintenance;
	}

	
	public function getIsPotongBpjs()
	{

		return $this->is_potong_bpjs;
	}

	
	public function getIsIuranBpjs()
	{

		return $this->is_iuran_bpjs;
	}

	
	public function getUsulanSkpd()
	{

		return $this->usulan_skpd;
	}

	
	public function getTahap()
	{

		return $this->tahap;
	}

	
	public function getIsEstFisik()
	{

		return $this->is_est_fisik;
	}

	
	public function getAkrualCode()
	{

		return $this->akrual_code;
	}

	
	public function getKodeAkrualKomponenPenyusun()
	{

		return $this->kode_akrual_komponen_penyusun;
	}

	
	public function getKomponenTipe2()
	{

		return $this->komponen_tipe2;
	}

	
	public function getIsSurveyBp()
	{

		return $this->is_survey_bp;
	}

	
	public function getKomponenHargaBulat()
	{

		return $this->komponen_harga_bulat;
	}

	
	public function getIsIuranJkn()
	{

		return $this->is_iuran_jkn;
	}

	
	public function getIsIuranJkk()
	{

		return $this->is_iuran_jkk;
	}

	
	public function getIsIuranJk()
	{

		return $this->is_iuran_jk;
	}

	
	public function getIsNarsum()
	{

		return $this->is_narsum;
	}

	
	public function getIsRab()
	{

		return $this->is_rab;
	}

	
	public function getIsBbm()
	{

		return $this->is_bbm;
	}

	
	public function setKomponenId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->komponen_id !== $v) {
			$this->komponen_id = $v;
			$this->modifiedColumns[] = KomponenPeer::KOMPONEN_ID;
		}

	} 
	
	public function setSatuan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->satuan !== $v) {
			$this->satuan = $v;
			$this->modifiedColumns[] = KomponenPeer::SATUAN;
		}

	} 
	
	public function setKomponenName($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->komponen_name !== $v) {
			$this->komponen_name = $v;
			$this->modifiedColumns[] = KomponenPeer::KOMPONEN_NAME;
		}

	} 
	
	public function setShsdId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->shsd_id !== $v) {
			$this->shsd_id = $v;
			$this->modifiedColumns[] = KomponenPeer::SHSD_ID;
		}

	} 
	
	public function setKomponenHarga($v)
	{

		if ($this->komponen_harga !== $v) {
			$this->komponen_harga = $v;
			$this->modifiedColumns[] = KomponenPeer::KOMPONEN_HARGA;
		}

	} 
	
	public function setKomponenShow($v)
	{

		if ($this->komponen_show !== $v || $v === true) {
			$this->komponen_show = $v;
			$this->modifiedColumns[] = KomponenPeer::KOMPONEN_SHOW;
		}

	} 
	
	public function setIpAddress($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->ip_address !== $v) {
			$this->ip_address = $v;
			$this->modifiedColumns[] = KomponenPeer::IP_ADDRESS;
		}

	} 
	
	public function setWaktuAccess($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [waktu_access] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->waktu_access !== $ts) {
			$this->waktu_access = $ts;
			$this->modifiedColumns[] = KomponenPeer::WAKTU_ACCESS;
		}

	} 
	
	public function setKomponenTipe($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->komponen_tipe !== $v) {
			$this->komponen_tipe = $v;
			$this->modifiedColumns[] = KomponenPeer::KOMPONEN_TIPE;
		}

	} 
	
	public function setKomponenConfirmed($v)
	{

		if ($this->komponen_confirmed !== $v || $v === false) {
			$this->komponen_confirmed = $v;
			$this->modifiedColumns[] = KomponenPeer::KOMPONEN_CONFIRMED;
		}

	} 
	
	public function setKomponenNonPajak($v)
	{

		if ($this->komponen_non_pajak !== $v || $v === false) {
			$this->komponen_non_pajak = $v;
			$this->modifiedColumns[] = KomponenPeer::KOMPONEN_NON_PAJAK;
		}

	} 
	
	public function setUserId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->user_id !== $v) {
			$this->user_id = $v;
			$this->modifiedColumns[] = KomponenPeer::USER_ID;
		}

	} 
	
	public function setRekening($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->rekening !== $v) {
			$this->rekening = $v;
			$this->modifiedColumns[] = KomponenPeer::REKENING;
		}

	} 
	
	public function setKelompok($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kelompok !== $v) {
			$this->kelompok = $v;
			$this->modifiedColumns[] = KomponenPeer::KELOMPOK;
		}

	} 
	
	public function setPemeliharaan($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->pemeliharaan !== $v || $v === 0) {
			$this->pemeliharaan = $v;
			$this->modifiedColumns[] = KomponenPeer::PEMELIHARAAN;
		}

	} 
	
	public function setRekUpah($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->rek_upah !== $v) {
			$this->rek_upah = $v;
			$this->modifiedColumns[] = KomponenPeer::REK_UPAH;
		}

	} 
	
	public function setRekBahan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->rek_bahan !== $v) {
			$this->rek_bahan = $v;
			$this->modifiedColumns[] = KomponenPeer::REK_BAHAN;
		}

	} 
	
	public function setRekSewa($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->rek_sewa !== $v) {
			$this->rek_sewa = $v;
			$this->modifiedColumns[] = KomponenPeer::REK_SEWA;
		}

	} 
	
	public function setDeskripsi($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->deskripsi !== $v) {
			$this->deskripsi = $v;
			$this->modifiedColumns[] = KomponenPeer::DESKRIPSI;
		}

	} 
	
	public function setStatusMasuk($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->status_masuk !== $v) {
			$this->status_masuk = $v;
			$this->modifiedColumns[] = KomponenPeer::STATUS_MASUK;
		}

	} 
	
	public function setRkaMember($v)
	{

		if ($this->rka_member !== $v) {
			$this->rka_member = $v;
			$this->modifiedColumns[] = KomponenPeer::RKA_MEMBER;
		}

	} 
	
	public function setMaintenance($v)
	{

		if ($this->maintenance !== $v) {
			$this->maintenance = $v;
			$this->modifiedColumns[] = KomponenPeer::MAINTENANCE;
		}

	} 
	
	public function setIsPotongBpjs($v)
	{

		if ($this->is_potong_bpjs !== $v || $v === false) {
			$this->is_potong_bpjs = $v;
			$this->modifiedColumns[] = KomponenPeer::IS_POTONG_BPJS;
		}

	} 
	
	public function setIsIuranBpjs($v)
	{

		if ($this->is_iuran_bpjs !== $v || $v === false) {
			$this->is_iuran_bpjs = $v;
			$this->modifiedColumns[] = KomponenPeer::IS_IURAN_BPJS;
		}

	} 
	
	public function setUsulanSkpd($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->usulan_skpd !== $v) {
			$this->usulan_skpd = $v;
			$this->modifiedColumns[] = KomponenPeer::USULAN_SKPD;
		}

	} 
	
	public function setTahap($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->tahap !== $v) {
			$this->tahap = $v;
			$this->modifiedColumns[] = KomponenPeer::TAHAP;
		}

	} 
	
	public function setIsEstFisik($v)
	{

		if ($this->is_est_fisik !== $v) {
			$this->is_est_fisik = $v;
			$this->modifiedColumns[] = KomponenPeer::IS_EST_FISIK;
		}

	} 
	
	public function setAkrualCode($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->akrual_code !== $v) {
			$this->akrual_code = $v;
			$this->modifiedColumns[] = KomponenPeer::AKRUAL_CODE;
		}

	} 
	
	public function setKodeAkrualKomponenPenyusun($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_akrual_komponen_penyusun !== $v) {
			$this->kode_akrual_komponen_penyusun = $v;
			$this->modifiedColumns[] = KomponenPeer::KODE_AKRUAL_KOMPONEN_PENYUSUN;
		}

	} 
	
	public function setKomponenTipe2($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->komponen_tipe2 !== $v) {
			$this->komponen_tipe2 = $v;
			$this->modifiedColumns[] = KomponenPeer::KOMPONEN_TIPE2;
		}

	} 
	
	public function setIsSurveyBp($v)
	{

		if ($this->is_survey_bp !== $v || $v === false) {
			$this->is_survey_bp = $v;
			$this->modifiedColumns[] = KomponenPeer::IS_SURVEY_BP;
		}

	} 
	
	public function setKomponenHargaBulat($v)
	{

		if ($this->komponen_harga_bulat !== $v) {
			$this->komponen_harga_bulat = $v;
			$this->modifiedColumns[] = KomponenPeer::KOMPONEN_HARGA_BULAT;
		}

	} 
	
	public function setIsIuranJkn($v)
	{

		if ($this->is_iuran_jkn !== $v || $v === false) {
			$this->is_iuran_jkn = $v;
			$this->modifiedColumns[] = KomponenPeer::IS_IURAN_JKN;
		}

	} 
	
	public function setIsIuranJkk($v)
	{

		if ($this->is_iuran_jkk !== $v || $v === false) {
			$this->is_iuran_jkk = $v;
			$this->modifiedColumns[] = KomponenPeer::IS_IURAN_JKK;
		}

	} 
	
	public function setIsIuranJk($v)
	{

		if ($this->is_iuran_jk !== $v || $v === false) {
			$this->is_iuran_jk = $v;
			$this->modifiedColumns[] = KomponenPeer::IS_IURAN_JK;
		}

	} 
	
	public function setIsNarsum($v)
	{

		if ($this->is_narsum !== $v || $v === false) {
			$this->is_narsum = $v;
			$this->modifiedColumns[] = KomponenPeer::IS_NARSUM;
		}

	} 
	
	public function setIsRab($v)
	{

		if ($this->is_rab !== $v || $v === false) {
			$this->is_rab = $v;
			$this->modifiedColumns[] = KomponenPeer::IS_RAB;
		}

	} 
	
	public function setIsBbm($v)
	{

		if ($this->is_bbm !== $v || $v === false) {
			$this->is_bbm = $v;
			$this->modifiedColumns[] = KomponenPeer::IS_BBM;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->komponen_id = $rs->getString($startcol + 0);

			$this->satuan = $rs->getString($startcol + 1);

			$this->komponen_name = $rs->getString($startcol + 2);

			$this->shsd_id = $rs->getString($startcol + 3);

			$this->komponen_harga = $rs->getFloat($startcol + 4);

			$this->komponen_show = $rs->getBoolean($startcol + 5);

			$this->ip_address = $rs->getString($startcol + 6);

			$this->waktu_access = $rs->getTimestamp($startcol + 7, null);

			$this->komponen_tipe = $rs->getString($startcol + 8);

			$this->komponen_confirmed = $rs->getBoolean($startcol + 9);

			$this->komponen_non_pajak = $rs->getBoolean($startcol + 10);

			$this->user_id = $rs->getString($startcol + 11);

			$this->rekening = $rs->getString($startcol + 12);

			$this->kelompok = $rs->getString($startcol + 13);

			$this->pemeliharaan = $rs->getInt($startcol + 14);

			$this->rek_upah = $rs->getString($startcol + 15);

			$this->rek_bahan = $rs->getString($startcol + 16);

			$this->rek_sewa = $rs->getString($startcol + 17);

			$this->deskripsi = $rs->getString($startcol + 18);

			$this->status_masuk = $rs->getString($startcol + 19);

			$this->rka_member = $rs->getBoolean($startcol + 20);

			$this->maintenance = $rs->getBoolean($startcol + 21);

			$this->is_potong_bpjs = $rs->getBoolean($startcol + 22);

			$this->is_iuran_bpjs = $rs->getBoolean($startcol + 23);

			$this->usulan_skpd = $rs->getString($startcol + 24);

			$this->tahap = $rs->getString($startcol + 25);

			$this->is_est_fisik = $rs->getBoolean($startcol + 26);

			$this->akrual_code = $rs->getString($startcol + 27);

			$this->kode_akrual_komponen_penyusun = $rs->getString($startcol + 28);

			$this->komponen_tipe2 = $rs->getString($startcol + 29);

			$this->is_survey_bp = $rs->getBoolean($startcol + 30);

			$this->komponen_harga_bulat = $rs->getFloat($startcol + 31);

			$this->is_iuran_jkn = $rs->getBoolean($startcol + 32);

			$this->is_iuran_jkk = $rs->getBoolean($startcol + 33);

			$this->is_iuran_jk = $rs->getBoolean($startcol + 34);

			$this->is_narsum = $rs->getBoolean($startcol + 35);

			$this->is_rab = $rs->getBoolean($startcol + 36);

			$this->is_bbm = $rs->getBoolean($startcol + 37);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 38; 
		} catch (Exception $e) {
			throw new PropelException("Error populating Komponen object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(KomponenPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			KomponenPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(KomponenPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = KomponenPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += KomponenPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = KomponenPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = KomponenPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getKomponenId();
				break;
			case 1:
				return $this->getSatuan();
				break;
			case 2:
				return $this->getKomponenName();
				break;
			case 3:
				return $this->getShsdId();
				break;
			case 4:
				return $this->getKomponenHarga();
				break;
			case 5:
				return $this->getKomponenShow();
				break;
			case 6:
				return $this->getIpAddress();
				break;
			case 7:
				return $this->getWaktuAccess();
				break;
			case 8:
				return $this->getKomponenTipe();
				break;
			case 9:
				return $this->getKomponenConfirmed();
				break;
			case 10:
				return $this->getKomponenNonPajak();
				break;
			case 11:
				return $this->getUserId();
				break;
			case 12:
				return $this->getRekening();
				break;
			case 13:
				return $this->getKelompok();
				break;
			case 14:
				return $this->getPemeliharaan();
				break;
			case 15:
				return $this->getRekUpah();
				break;
			case 16:
				return $this->getRekBahan();
				break;
			case 17:
				return $this->getRekSewa();
				break;
			case 18:
				return $this->getDeskripsi();
				break;
			case 19:
				return $this->getStatusMasuk();
				break;
			case 20:
				return $this->getRkaMember();
				break;
			case 21:
				return $this->getMaintenance();
				break;
			case 22:
				return $this->getIsPotongBpjs();
				break;
			case 23:
				return $this->getIsIuranBpjs();
				break;
			case 24:
				return $this->getUsulanSkpd();
				break;
			case 25:
				return $this->getTahap();
				break;
			case 26:
				return $this->getIsEstFisik();
				break;
			case 27:
				return $this->getAkrualCode();
				break;
			case 28:
				return $this->getKodeAkrualKomponenPenyusun();
				break;
			case 29:
				return $this->getKomponenTipe2();
				break;
			case 30:
				return $this->getIsSurveyBp();
				break;
			case 31:
				return $this->getKomponenHargaBulat();
				break;
			case 32:
				return $this->getIsIuranJkn();
				break;
			case 33:
				return $this->getIsIuranJkk();
				break;
			case 34:
				return $this->getIsIuranJk();
				break;
			case 35:
				return $this->getIsNarsum();
				break;
			case 36:
				return $this->getIsRab();
				break;
			case 37:
				return $this->getIsBbm();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = KomponenPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getKomponenId(),
			$keys[1] => $this->getSatuan(),
			$keys[2] => $this->getKomponenName(),
			$keys[3] => $this->getShsdId(),
			$keys[4] => $this->getKomponenHarga(),
			$keys[5] => $this->getKomponenShow(),
			$keys[6] => $this->getIpAddress(),
			$keys[7] => $this->getWaktuAccess(),
			$keys[8] => $this->getKomponenTipe(),
			$keys[9] => $this->getKomponenConfirmed(),
			$keys[10] => $this->getKomponenNonPajak(),
			$keys[11] => $this->getUserId(),
			$keys[12] => $this->getRekening(),
			$keys[13] => $this->getKelompok(),
			$keys[14] => $this->getPemeliharaan(),
			$keys[15] => $this->getRekUpah(),
			$keys[16] => $this->getRekBahan(),
			$keys[17] => $this->getRekSewa(),
			$keys[18] => $this->getDeskripsi(),
			$keys[19] => $this->getStatusMasuk(),
			$keys[20] => $this->getRkaMember(),
			$keys[21] => $this->getMaintenance(),
			$keys[22] => $this->getIsPotongBpjs(),
			$keys[23] => $this->getIsIuranBpjs(),
			$keys[24] => $this->getUsulanSkpd(),
			$keys[25] => $this->getTahap(),
			$keys[26] => $this->getIsEstFisik(),
			$keys[27] => $this->getAkrualCode(),
			$keys[28] => $this->getKodeAkrualKomponenPenyusun(),
			$keys[29] => $this->getKomponenTipe2(),
			$keys[30] => $this->getIsSurveyBp(),
			$keys[31] => $this->getKomponenHargaBulat(),
			$keys[32] => $this->getIsIuranJkn(),
			$keys[33] => $this->getIsIuranJkk(),
			$keys[34] => $this->getIsIuranJk(),
			$keys[35] => $this->getIsNarsum(),
			$keys[36] => $this->getIsRab(),
			$keys[37] => $this->getIsBbm(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = KomponenPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setKomponenId($value);
				break;
			case 1:
				$this->setSatuan($value);
				break;
			case 2:
				$this->setKomponenName($value);
				break;
			case 3:
				$this->setShsdId($value);
				break;
			case 4:
				$this->setKomponenHarga($value);
				break;
			case 5:
				$this->setKomponenShow($value);
				break;
			case 6:
				$this->setIpAddress($value);
				break;
			case 7:
				$this->setWaktuAccess($value);
				break;
			case 8:
				$this->setKomponenTipe($value);
				break;
			case 9:
				$this->setKomponenConfirmed($value);
				break;
			case 10:
				$this->setKomponenNonPajak($value);
				break;
			case 11:
				$this->setUserId($value);
				break;
			case 12:
				$this->setRekening($value);
				break;
			case 13:
				$this->setKelompok($value);
				break;
			case 14:
				$this->setPemeliharaan($value);
				break;
			case 15:
				$this->setRekUpah($value);
				break;
			case 16:
				$this->setRekBahan($value);
				break;
			case 17:
				$this->setRekSewa($value);
				break;
			case 18:
				$this->setDeskripsi($value);
				break;
			case 19:
				$this->setStatusMasuk($value);
				break;
			case 20:
				$this->setRkaMember($value);
				break;
			case 21:
				$this->setMaintenance($value);
				break;
			case 22:
				$this->setIsPotongBpjs($value);
				break;
			case 23:
				$this->setIsIuranBpjs($value);
				break;
			case 24:
				$this->setUsulanSkpd($value);
				break;
			case 25:
				$this->setTahap($value);
				break;
			case 26:
				$this->setIsEstFisik($value);
				break;
			case 27:
				$this->setAkrualCode($value);
				break;
			case 28:
				$this->setKodeAkrualKomponenPenyusun($value);
				break;
			case 29:
				$this->setKomponenTipe2($value);
				break;
			case 30:
				$this->setIsSurveyBp($value);
				break;
			case 31:
				$this->setKomponenHargaBulat($value);
				break;
			case 32:
				$this->setIsIuranJkn($value);
				break;
			case 33:
				$this->setIsIuranJkk($value);
				break;
			case 34:
				$this->setIsIuranJk($value);
				break;
			case 35:
				$this->setIsNarsum($value);
				break;
			case 36:
				$this->setIsRab($value);
				break;
			case 37:
				$this->setIsBbm($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = KomponenPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setKomponenId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setSatuan($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setKomponenName($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setShsdId($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setKomponenHarga($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setKomponenShow($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setIpAddress($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setWaktuAccess($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setKomponenTipe($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setKomponenConfirmed($arr[$keys[9]]);
		if (array_key_exists($keys[10], $arr)) $this->setKomponenNonPajak($arr[$keys[10]]);
		if (array_key_exists($keys[11], $arr)) $this->setUserId($arr[$keys[11]]);
		if (array_key_exists($keys[12], $arr)) $this->setRekening($arr[$keys[12]]);
		if (array_key_exists($keys[13], $arr)) $this->setKelompok($arr[$keys[13]]);
		if (array_key_exists($keys[14], $arr)) $this->setPemeliharaan($arr[$keys[14]]);
		if (array_key_exists($keys[15], $arr)) $this->setRekUpah($arr[$keys[15]]);
		if (array_key_exists($keys[16], $arr)) $this->setRekBahan($arr[$keys[16]]);
		if (array_key_exists($keys[17], $arr)) $this->setRekSewa($arr[$keys[17]]);
		if (array_key_exists($keys[18], $arr)) $this->setDeskripsi($arr[$keys[18]]);
		if (array_key_exists($keys[19], $arr)) $this->setStatusMasuk($arr[$keys[19]]);
		if (array_key_exists($keys[20], $arr)) $this->setRkaMember($arr[$keys[20]]);
		if (array_key_exists($keys[21], $arr)) $this->setMaintenance($arr[$keys[21]]);
		if (array_key_exists($keys[22], $arr)) $this->setIsPotongBpjs($arr[$keys[22]]);
		if (array_key_exists($keys[23], $arr)) $this->setIsIuranBpjs($arr[$keys[23]]);
		if (array_key_exists($keys[24], $arr)) $this->setUsulanSkpd($arr[$keys[24]]);
		if (array_key_exists($keys[25], $arr)) $this->setTahap($arr[$keys[25]]);
		if (array_key_exists($keys[26], $arr)) $this->setIsEstFisik($arr[$keys[26]]);
		if (array_key_exists($keys[27], $arr)) $this->setAkrualCode($arr[$keys[27]]);
		if (array_key_exists($keys[28], $arr)) $this->setKodeAkrualKomponenPenyusun($arr[$keys[28]]);
		if (array_key_exists($keys[29], $arr)) $this->setKomponenTipe2($arr[$keys[29]]);
		if (array_key_exists($keys[30], $arr)) $this->setIsSurveyBp($arr[$keys[30]]);
		if (array_key_exists($keys[31], $arr)) $this->setKomponenHargaBulat($arr[$keys[31]]);
		if (array_key_exists($keys[32], $arr)) $this->setIsIuranJkn($arr[$keys[32]]);
		if (array_key_exists($keys[33], $arr)) $this->setIsIuranJkk($arr[$keys[33]]);
		if (array_key_exists($keys[34], $arr)) $this->setIsIuranJk($arr[$keys[34]]);
		if (array_key_exists($keys[35], $arr)) $this->setIsNarsum($arr[$keys[35]]);
		if (array_key_exists($keys[36], $arr)) $this->setIsRab($arr[$keys[36]]);
		if (array_key_exists($keys[37], $arr)) $this->setIsBbm($arr[$keys[37]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(KomponenPeer::DATABASE_NAME);

		if ($this->isColumnModified(KomponenPeer::KOMPONEN_ID)) $criteria->add(KomponenPeer::KOMPONEN_ID, $this->komponen_id);
		if ($this->isColumnModified(KomponenPeer::SATUAN)) $criteria->add(KomponenPeer::SATUAN, $this->satuan);
		if ($this->isColumnModified(KomponenPeer::KOMPONEN_NAME)) $criteria->add(KomponenPeer::KOMPONEN_NAME, $this->komponen_name);
		if ($this->isColumnModified(KomponenPeer::SHSD_ID)) $criteria->add(KomponenPeer::SHSD_ID, $this->shsd_id);
		if ($this->isColumnModified(KomponenPeer::KOMPONEN_HARGA)) $criteria->add(KomponenPeer::KOMPONEN_HARGA, $this->komponen_harga);
		if ($this->isColumnModified(KomponenPeer::KOMPONEN_SHOW)) $criteria->add(KomponenPeer::KOMPONEN_SHOW, $this->komponen_show);
		if ($this->isColumnModified(KomponenPeer::IP_ADDRESS)) $criteria->add(KomponenPeer::IP_ADDRESS, $this->ip_address);
		if ($this->isColumnModified(KomponenPeer::WAKTU_ACCESS)) $criteria->add(KomponenPeer::WAKTU_ACCESS, $this->waktu_access);
		if ($this->isColumnModified(KomponenPeer::KOMPONEN_TIPE)) $criteria->add(KomponenPeer::KOMPONEN_TIPE, $this->komponen_tipe);
		if ($this->isColumnModified(KomponenPeer::KOMPONEN_CONFIRMED)) $criteria->add(KomponenPeer::KOMPONEN_CONFIRMED, $this->komponen_confirmed);
		if ($this->isColumnModified(KomponenPeer::KOMPONEN_NON_PAJAK)) $criteria->add(KomponenPeer::KOMPONEN_NON_PAJAK, $this->komponen_non_pajak);
		if ($this->isColumnModified(KomponenPeer::USER_ID)) $criteria->add(KomponenPeer::USER_ID, $this->user_id);
		if ($this->isColumnModified(KomponenPeer::REKENING)) $criteria->add(KomponenPeer::REKENING, $this->rekening);
		if ($this->isColumnModified(KomponenPeer::KELOMPOK)) $criteria->add(KomponenPeer::KELOMPOK, $this->kelompok);
		if ($this->isColumnModified(KomponenPeer::PEMELIHARAAN)) $criteria->add(KomponenPeer::PEMELIHARAAN, $this->pemeliharaan);
		if ($this->isColumnModified(KomponenPeer::REK_UPAH)) $criteria->add(KomponenPeer::REK_UPAH, $this->rek_upah);
		if ($this->isColumnModified(KomponenPeer::REK_BAHAN)) $criteria->add(KomponenPeer::REK_BAHAN, $this->rek_bahan);
		if ($this->isColumnModified(KomponenPeer::REK_SEWA)) $criteria->add(KomponenPeer::REK_SEWA, $this->rek_sewa);
		if ($this->isColumnModified(KomponenPeer::DESKRIPSI)) $criteria->add(KomponenPeer::DESKRIPSI, $this->deskripsi);
		if ($this->isColumnModified(KomponenPeer::STATUS_MASUK)) $criteria->add(KomponenPeer::STATUS_MASUK, $this->status_masuk);
		if ($this->isColumnModified(KomponenPeer::RKA_MEMBER)) $criteria->add(KomponenPeer::RKA_MEMBER, $this->rka_member);
		if ($this->isColumnModified(KomponenPeer::MAINTENANCE)) $criteria->add(KomponenPeer::MAINTENANCE, $this->maintenance);
		if ($this->isColumnModified(KomponenPeer::IS_POTONG_BPJS)) $criteria->add(KomponenPeer::IS_POTONG_BPJS, $this->is_potong_bpjs);
		if ($this->isColumnModified(KomponenPeer::IS_IURAN_BPJS)) $criteria->add(KomponenPeer::IS_IURAN_BPJS, $this->is_iuran_bpjs);
		if ($this->isColumnModified(KomponenPeer::USULAN_SKPD)) $criteria->add(KomponenPeer::USULAN_SKPD, $this->usulan_skpd);
		if ($this->isColumnModified(KomponenPeer::TAHAP)) $criteria->add(KomponenPeer::TAHAP, $this->tahap);
		if ($this->isColumnModified(KomponenPeer::IS_EST_FISIK)) $criteria->add(KomponenPeer::IS_EST_FISIK, $this->is_est_fisik);
		if ($this->isColumnModified(KomponenPeer::AKRUAL_CODE)) $criteria->add(KomponenPeer::AKRUAL_CODE, $this->akrual_code);
		if ($this->isColumnModified(KomponenPeer::KODE_AKRUAL_KOMPONEN_PENYUSUN)) $criteria->add(KomponenPeer::KODE_AKRUAL_KOMPONEN_PENYUSUN, $this->kode_akrual_komponen_penyusun);
		if ($this->isColumnModified(KomponenPeer::KOMPONEN_TIPE2)) $criteria->add(KomponenPeer::KOMPONEN_TIPE2, $this->komponen_tipe2);
		if ($this->isColumnModified(KomponenPeer::IS_SURVEY_BP)) $criteria->add(KomponenPeer::IS_SURVEY_BP, $this->is_survey_bp);
		if ($this->isColumnModified(KomponenPeer::KOMPONEN_HARGA_BULAT)) $criteria->add(KomponenPeer::KOMPONEN_HARGA_BULAT, $this->komponen_harga_bulat);
		if ($this->isColumnModified(KomponenPeer::IS_IURAN_JKN)) $criteria->add(KomponenPeer::IS_IURAN_JKN, $this->is_iuran_jkn);
		if ($this->isColumnModified(KomponenPeer::IS_IURAN_JKK)) $criteria->add(KomponenPeer::IS_IURAN_JKK, $this->is_iuran_jkk);
		if ($this->isColumnModified(KomponenPeer::IS_IURAN_JK)) $criteria->add(KomponenPeer::IS_IURAN_JK, $this->is_iuran_jk);
		if ($this->isColumnModified(KomponenPeer::IS_NARSUM)) $criteria->add(KomponenPeer::IS_NARSUM, $this->is_narsum);
		if ($this->isColumnModified(KomponenPeer::IS_RAB)) $criteria->add(KomponenPeer::IS_RAB, $this->is_rab);
		if ($this->isColumnModified(KomponenPeer::IS_BBM)) $criteria->add(KomponenPeer::IS_BBM, $this->is_bbm);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(KomponenPeer::DATABASE_NAME);

		$criteria->add(KomponenPeer::KOMPONEN_ID, $this->komponen_id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getKomponenId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setKomponenId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setSatuan($this->satuan);

		$copyObj->setKomponenName($this->komponen_name);

		$copyObj->setShsdId($this->shsd_id);

		$copyObj->setKomponenHarga($this->komponen_harga);

		$copyObj->setKomponenShow($this->komponen_show);

		$copyObj->setIpAddress($this->ip_address);

		$copyObj->setWaktuAccess($this->waktu_access);

		$copyObj->setKomponenTipe($this->komponen_tipe);

		$copyObj->setKomponenConfirmed($this->komponen_confirmed);

		$copyObj->setKomponenNonPajak($this->komponen_non_pajak);

		$copyObj->setUserId($this->user_id);

		$copyObj->setRekening($this->rekening);

		$copyObj->setKelompok($this->kelompok);

		$copyObj->setPemeliharaan($this->pemeliharaan);

		$copyObj->setRekUpah($this->rek_upah);

		$copyObj->setRekBahan($this->rek_bahan);

		$copyObj->setRekSewa($this->rek_sewa);

		$copyObj->setDeskripsi($this->deskripsi);

		$copyObj->setStatusMasuk($this->status_masuk);

		$copyObj->setRkaMember($this->rka_member);

		$copyObj->setMaintenance($this->maintenance);

		$copyObj->setIsPotongBpjs($this->is_potong_bpjs);

		$copyObj->setIsIuranBpjs($this->is_iuran_bpjs);

		$copyObj->setUsulanSkpd($this->usulan_skpd);

		$copyObj->setTahap($this->tahap);

		$copyObj->setIsEstFisik($this->is_est_fisik);

		$copyObj->setAkrualCode($this->akrual_code);

		$copyObj->setKodeAkrualKomponenPenyusun($this->kode_akrual_komponen_penyusun);

		$copyObj->setKomponenTipe2($this->komponen_tipe2);

		$copyObj->setIsSurveyBp($this->is_survey_bp);

		$copyObj->setKomponenHargaBulat($this->komponen_harga_bulat);

		$copyObj->setIsIuranJkn($this->is_iuran_jkn);

		$copyObj->setIsIuranJkk($this->is_iuran_jkk);

		$copyObj->setIsIuranJk($this->is_iuran_jk);

		$copyObj->setIsNarsum($this->is_narsum);

		$copyObj->setIsRab($this->is_rab);

		$copyObj->setIsBbm($this->is_bbm);


		$copyObj->setNew(true);

		$copyObj->setKomponenId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new KomponenPeer();
		}
		return self::$peer;
	}

} 