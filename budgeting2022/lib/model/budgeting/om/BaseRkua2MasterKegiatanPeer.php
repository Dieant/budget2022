<?php


abstract class BaseRkua2MasterKegiatanPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.rkua2_master_kegiatan';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.Rkua2MasterKegiatan';

	
	const NUM_COLUMNS = 63;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const UNIT_ID = 'ebudget.rkua2_master_kegiatan.UNIT_ID';

	
	const KODE_KEGIATAN = 'ebudget.rkua2_master_kegiatan.KODE_KEGIATAN';

	
	const KODE_BIDANG = 'ebudget.rkua2_master_kegiatan.KODE_BIDANG';

	
	const KODE_URUSAN_WAJIB = 'ebudget.rkua2_master_kegiatan.KODE_URUSAN_WAJIB';

	
	const KODE_PROGRAM = 'ebudget.rkua2_master_kegiatan.KODE_PROGRAM';

	
	const KODE_SASARAN = 'ebudget.rkua2_master_kegiatan.KODE_SASARAN';

	
	const KODE_INDIKATOR = 'ebudget.rkua2_master_kegiatan.KODE_INDIKATOR';

	
	const ALOKASI_DANA = 'ebudget.rkua2_master_kegiatan.ALOKASI_DANA';

	
	const NAMA_KEGIATAN = 'ebudget.rkua2_master_kegiatan.NAMA_KEGIATAN';

	
	const MASUKAN = 'ebudget.rkua2_master_kegiatan.MASUKAN';

	
	const OUTPUT = 'ebudget.rkua2_master_kegiatan.OUTPUT';

	
	const OUTCOME = 'ebudget.rkua2_master_kegiatan.OUTCOME';

	
	const BENEFIT = 'ebudget.rkua2_master_kegiatan.BENEFIT';

	
	const IMPACT = 'ebudget.rkua2_master_kegiatan.IMPACT';

	
	const TIPE = 'ebudget.rkua2_master_kegiatan.TIPE';

	
	const KEGIATAN_ACTIVE = 'ebudget.rkua2_master_kegiatan.KEGIATAN_ACTIVE';

	
	const TO_KEGIATAN_CODE = 'ebudget.rkua2_master_kegiatan.TO_KEGIATAN_CODE';

	
	const CATATAN = 'ebudget.rkua2_master_kegiatan.CATATAN';

	
	const TARGET_OUTCOME = 'ebudget.rkua2_master_kegiatan.TARGET_OUTCOME';

	
	const LOKASI = 'ebudget.rkua2_master_kegiatan.LOKASI';

	
	const JUMLAH_PREV = 'ebudget.rkua2_master_kegiatan.JUMLAH_PREV';

	
	const JUMLAH_NOW = 'ebudget.rkua2_master_kegiatan.JUMLAH_NOW';

	
	const JUMLAH_NEXT = 'ebudget.rkua2_master_kegiatan.JUMLAH_NEXT';

	
	const KODE_PROGRAM2 = 'ebudget.rkua2_master_kegiatan.KODE_PROGRAM2';

	
	const KODE_URUSAN = 'ebudget.rkua2_master_kegiatan.KODE_URUSAN';

	
	const LAST_UPDATE_USER = 'ebudget.rkua2_master_kegiatan.LAST_UPDATE_USER';

	
	const LAST_UPDATE_TIME = 'ebudget.rkua2_master_kegiatan.LAST_UPDATE_TIME';

	
	const LAST_UPDATE_IP = 'ebudget.rkua2_master_kegiatan.LAST_UPDATE_IP';

	
	const TAHAP = 'ebudget.rkua2_master_kegiatan.TAHAP';

	
	const KODE_MISI = 'ebudget.rkua2_master_kegiatan.KODE_MISI';

	
	const KODE_TUJUAN = 'ebudget.rkua2_master_kegiatan.KODE_TUJUAN';

	
	const RANKING = 'ebudget.rkua2_master_kegiatan.RANKING';

	
	const NOMOR13 = 'ebudget.rkua2_master_kegiatan.NOMOR13';

	
	const PPA_NAMA = 'ebudget.rkua2_master_kegiatan.PPA_NAMA';

	
	const PPA_PANGKAT = 'ebudget.rkua2_master_kegiatan.PPA_PANGKAT';

	
	const PPA_NIP = 'ebudget.rkua2_master_kegiatan.PPA_NIP';

	
	const LANJUTAN = 'ebudget.rkua2_master_kegiatan.LANJUTAN';

	
	const USER_ID = 'ebudget.rkua2_master_kegiatan.USER_ID';

	
	const ID = 'ebudget.rkua2_master_kegiatan.ID';

	
	const TAHUN = 'ebudget.rkua2_master_kegiatan.TAHUN';

	
	const TAMBAHAN_PAGU = 'ebudget.rkua2_master_kegiatan.TAMBAHAN_PAGU';

	
	const GENDER = 'ebudget.rkua2_master_kegiatan.GENDER';

	
	const KODE_KEG_KEUANGAN = 'ebudget.rkua2_master_kegiatan.KODE_KEG_KEUANGAN';

	
	const USER_ID_LAMA = 'ebudget.rkua2_master_kegiatan.USER_ID_LAMA';

	
	const INDIKATOR = 'ebudget.rkua2_master_kegiatan.INDIKATOR';

	
	const IS_DAK = 'ebudget.rkua2_master_kegiatan.IS_DAK';

	
	const KODE_KEGIATAN_ASAL = 'ebudget.rkua2_master_kegiatan.KODE_KEGIATAN_ASAL';

	
	const KODE_KEG_KEUANGAN_ASAL = 'ebudget.rkua2_master_kegiatan.KODE_KEG_KEUANGAN_ASAL';

	
	const TH_KE_MULTIYEARS = 'ebudget.rkua2_master_kegiatan.TH_KE_MULTIYEARS';

	
	const KELOMPOK_SASARAN = 'ebudget.rkua2_master_kegiatan.KELOMPOK_SASARAN';

	
	const PAGU_BAPPEKO = 'ebudget.rkua2_master_kegiatan.PAGU_BAPPEKO';

	
	const KODE_DPA = 'ebudget.rkua2_master_kegiatan.KODE_DPA';

	
	const USER_ID_PPTK = 'ebudget.rkua2_master_kegiatan.USER_ID_PPTK';

	
	const USER_ID_KPA = 'ebudget.rkua2_master_kegiatan.USER_ID_KPA';

	
	const CATATAN_PEMBAHASAN = 'ebudget.rkua2_master_kegiatan.CATATAN_PEMBAHASAN';

	
	const CATATAN_PENYELIA = 'ebudget.rkua2_master_kegiatan.CATATAN_PENYELIA';

	
	const CATATAN_BAPPEKO = 'ebudget.rkua2_master_kegiatan.CATATAN_BAPPEKO';

	
	const STATUS_LEVEL = 'ebudget.rkua2_master_kegiatan.STATUS_LEVEL';

	
	const IS_TAPD_SETUJU = 'ebudget.rkua2_master_kegiatan.IS_TAPD_SETUJU';

	
	const IS_BAPPEKO_SETUJU = 'ebudget.rkua2_master_kegiatan.IS_BAPPEKO_SETUJU';

	
	const IS_PENYELIA_SETUJU = 'ebudget.rkua2_master_kegiatan.IS_PENYELIA_SETUJU';

	
	const IS_PERNAH_RKA = 'ebudget.rkua2_master_kegiatan.IS_PERNAH_RKA';

	
	const KODE_KEGIATAN_BARU = 'ebudget.rkua2_master_kegiatan.KODE_KEGIATAN_BARU';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('UnitId', 'KodeKegiatan', 'KodeBidang', 'KodeUrusanWajib', 'KodeProgram', 'KodeSasaran', 'KodeIndikator', 'AlokasiDana', 'NamaKegiatan', 'Masukan', 'Output', 'Outcome', 'Benefit', 'Impact', 'Tipe', 'KegiatanActive', 'ToKegiatanCode', 'Catatan', 'TargetOutcome', 'Lokasi', 'JumlahPrev', 'JumlahNow', 'JumlahNext', 'KodeProgram2', 'KodeUrusan', 'LastUpdateUser', 'LastUpdateTime', 'LastUpdateIp', 'Tahap', 'KodeMisi', 'KodeTujuan', 'Ranking', 'Nomor13', 'PpaNama', 'PpaPangkat', 'PpaNip', 'Lanjutan', 'UserId', 'Id', 'Tahun', 'TambahanPagu', 'Gender', 'KodeKegKeuangan', 'UserIdLama', 'Indikator', 'IsDak', 'KodeKegiatanAsal', 'KodeKegKeuanganAsal', 'ThKeMultiyears', 'KelompokSasaran', 'PaguBappeko', 'KodeDpa', 'UserIdPptk', 'UserIdKpa', 'CatatanPembahasan', 'CatatanPenyelia', 'CatatanBappeko', 'StatusLevel', 'IsTapdSetuju', 'IsBappekoSetuju', 'IsPenyeliaSetuju', 'IsPernahRka', 'KodeKegiatanBaru', ),
		BasePeer::TYPE_COLNAME => array (Rkua2MasterKegiatanPeer::UNIT_ID, Rkua2MasterKegiatanPeer::KODE_KEGIATAN, Rkua2MasterKegiatanPeer::KODE_BIDANG, Rkua2MasterKegiatanPeer::KODE_URUSAN_WAJIB, Rkua2MasterKegiatanPeer::KODE_PROGRAM, Rkua2MasterKegiatanPeer::KODE_SASARAN, Rkua2MasterKegiatanPeer::KODE_INDIKATOR, Rkua2MasterKegiatanPeer::ALOKASI_DANA, Rkua2MasterKegiatanPeer::NAMA_KEGIATAN, Rkua2MasterKegiatanPeer::MASUKAN, Rkua2MasterKegiatanPeer::OUTPUT, Rkua2MasterKegiatanPeer::OUTCOME, Rkua2MasterKegiatanPeer::BENEFIT, Rkua2MasterKegiatanPeer::IMPACT, Rkua2MasterKegiatanPeer::TIPE, Rkua2MasterKegiatanPeer::KEGIATAN_ACTIVE, Rkua2MasterKegiatanPeer::TO_KEGIATAN_CODE, Rkua2MasterKegiatanPeer::CATATAN, Rkua2MasterKegiatanPeer::TARGET_OUTCOME, Rkua2MasterKegiatanPeer::LOKASI, Rkua2MasterKegiatanPeer::JUMLAH_PREV, Rkua2MasterKegiatanPeer::JUMLAH_NOW, Rkua2MasterKegiatanPeer::JUMLAH_NEXT, Rkua2MasterKegiatanPeer::KODE_PROGRAM2, Rkua2MasterKegiatanPeer::KODE_URUSAN, Rkua2MasterKegiatanPeer::LAST_UPDATE_USER, Rkua2MasterKegiatanPeer::LAST_UPDATE_TIME, Rkua2MasterKegiatanPeer::LAST_UPDATE_IP, Rkua2MasterKegiatanPeer::TAHAP, Rkua2MasterKegiatanPeer::KODE_MISI, Rkua2MasterKegiatanPeer::KODE_TUJUAN, Rkua2MasterKegiatanPeer::RANKING, Rkua2MasterKegiatanPeer::NOMOR13, Rkua2MasterKegiatanPeer::PPA_NAMA, Rkua2MasterKegiatanPeer::PPA_PANGKAT, Rkua2MasterKegiatanPeer::PPA_NIP, Rkua2MasterKegiatanPeer::LANJUTAN, Rkua2MasterKegiatanPeer::USER_ID, Rkua2MasterKegiatanPeer::ID, Rkua2MasterKegiatanPeer::TAHUN, Rkua2MasterKegiatanPeer::TAMBAHAN_PAGU, Rkua2MasterKegiatanPeer::GENDER, Rkua2MasterKegiatanPeer::KODE_KEG_KEUANGAN, Rkua2MasterKegiatanPeer::USER_ID_LAMA, Rkua2MasterKegiatanPeer::INDIKATOR, Rkua2MasterKegiatanPeer::IS_DAK, Rkua2MasterKegiatanPeer::KODE_KEGIATAN_ASAL, Rkua2MasterKegiatanPeer::KODE_KEG_KEUANGAN_ASAL, Rkua2MasterKegiatanPeer::TH_KE_MULTIYEARS, Rkua2MasterKegiatanPeer::KELOMPOK_SASARAN, Rkua2MasterKegiatanPeer::PAGU_BAPPEKO, Rkua2MasterKegiatanPeer::KODE_DPA, Rkua2MasterKegiatanPeer::USER_ID_PPTK, Rkua2MasterKegiatanPeer::USER_ID_KPA, Rkua2MasterKegiatanPeer::CATATAN_PEMBAHASAN, Rkua2MasterKegiatanPeer::CATATAN_PENYELIA, Rkua2MasterKegiatanPeer::CATATAN_BAPPEKO, Rkua2MasterKegiatanPeer::STATUS_LEVEL, Rkua2MasterKegiatanPeer::IS_TAPD_SETUJU, Rkua2MasterKegiatanPeer::IS_BAPPEKO_SETUJU, Rkua2MasterKegiatanPeer::IS_PENYELIA_SETUJU, Rkua2MasterKegiatanPeer::IS_PERNAH_RKA, Rkua2MasterKegiatanPeer::KODE_KEGIATAN_BARU, ),
		BasePeer::TYPE_FIELDNAME => array ('unit_id', 'kode_kegiatan', 'kode_bidang', 'kode_urusan_wajib', 'kode_program', 'kode_sasaran', 'kode_indikator', 'alokasi_dana', 'nama_kegiatan', 'masukan', 'output', 'outcome', 'benefit', 'impact', 'tipe', 'kegiatan_active', 'to_kegiatan_code', 'catatan', 'target_outcome', 'lokasi', 'jumlah_prev', 'jumlah_now', 'jumlah_next', 'kode_program2', 'kode_urusan', 'last_update_user', 'last_update_time', 'last_update_ip', 'tahap', 'kode_misi', 'kode_tujuan', 'ranking', 'nomor13', 'ppa_nama', 'ppa_pangkat', 'ppa_nip', 'lanjutan', 'user_id', 'id', 'tahun', 'tambahan_pagu', 'gender', 'kode_keg_keuangan', 'user_id_lama', 'indikator', 'is_dak', 'kode_kegiatan_asal', 'kode_keg_keuangan_asal', 'th_ke_multiyears', 'kelompok_sasaran', 'pagu_bappeko', 'kode_dpa', 'user_id_pptk', 'user_id_kpa', 'catatan_pembahasan', 'catatan_penyelia', 'catatan_bappeko', 'status_level', 'is_tapd_setuju', 'is_bappeko_setuju', 'is_penyelia_setuju', 'is_pernah_rka', 'kode_kegiatan_baru', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('UnitId' => 0, 'KodeKegiatan' => 1, 'KodeBidang' => 2, 'KodeUrusanWajib' => 3, 'KodeProgram' => 4, 'KodeSasaran' => 5, 'KodeIndikator' => 6, 'AlokasiDana' => 7, 'NamaKegiatan' => 8, 'Masukan' => 9, 'Output' => 10, 'Outcome' => 11, 'Benefit' => 12, 'Impact' => 13, 'Tipe' => 14, 'KegiatanActive' => 15, 'ToKegiatanCode' => 16, 'Catatan' => 17, 'TargetOutcome' => 18, 'Lokasi' => 19, 'JumlahPrev' => 20, 'JumlahNow' => 21, 'JumlahNext' => 22, 'KodeProgram2' => 23, 'KodeUrusan' => 24, 'LastUpdateUser' => 25, 'LastUpdateTime' => 26, 'LastUpdateIp' => 27, 'Tahap' => 28, 'KodeMisi' => 29, 'KodeTujuan' => 30, 'Ranking' => 31, 'Nomor13' => 32, 'PpaNama' => 33, 'PpaPangkat' => 34, 'PpaNip' => 35, 'Lanjutan' => 36, 'UserId' => 37, 'Id' => 38, 'Tahun' => 39, 'TambahanPagu' => 40, 'Gender' => 41, 'KodeKegKeuangan' => 42, 'UserIdLama' => 43, 'Indikator' => 44, 'IsDak' => 45, 'KodeKegiatanAsal' => 46, 'KodeKegKeuanganAsal' => 47, 'ThKeMultiyears' => 48, 'KelompokSasaran' => 49, 'PaguBappeko' => 50, 'KodeDpa' => 51, 'UserIdPptk' => 52, 'UserIdKpa' => 53, 'CatatanPembahasan' => 54, 'CatatanPenyelia' => 55, 'CatatanBappeko' => 56, 'StatusLevel' => 57, 'IsTapdSetuju' => 58, 'IsBappekoSetuju' => 59, 'IsPenyeliaSetuju' => 60, 'IsPernahRka' => 61, 'KodeKegiatanBaru' => 62, ),
		BasePeer::TYPE_COLNAME => array (Rkua2MasterKegiatanPeer::UNIT_ID => 0, Rkua2MasterKegiatanPeer::KODE_KEGIATAN => 1, Rkua2MasterKegiatanPeer::KODE_BIDANG => 2, Rkua2MasterKegiatanPeer::KODE_URUSAN_WAJIB => 3, Rkua2MasterKegiatanPeer::KODE_PROGRAM => 4, Rkua2MasterKegiatanPeer::KODE_SASARAN => 5, Rkua2MasterKegiatanPeer::KODE_INDIKATOR => 6, Rkua2MasterKegiatanPeer::ALOKASI_DANA => 7, Rkua2MasterKegiatanPeer::NAMA_KEGIATAN => 8, Rkua2MasterKegiatanPeer::MASUKAN => 9, Rkua2MasterKegiatanPeer::OUTPUT => 10, Rkua2MasterKegiatanPeer::OUTCOME => 11, Rkua2MasterKegiatanPeer::BENEFIT => 12, Rkua2MasterKegiatanPeer::IMPACT => 13, Rkua2MasterKegiatanPeer::TIPE => 14, Rkua2MasterKegiatanPeer::KEGIATAN_ACTIVE => 15, Rkua2MasterKegiatanPeer::TO_KEGIATAN_CODE => 16, Rkua2MasterKegiatanPeer::CATATAN => 17, Rkua2MasterKegiatanPeer::TARGET_OUTCOME => 18, Rkua2MasterKegiatanPeer::LOKASI => 19, Rkua2MasterKegiatanPeer::JUMLAH_PREV => 20, Rkua2MasterKegiatanPeer::JUMLAH_NOW => 21, Rkua2MasterKegiatanPeer::JUMLAH_NEXT => 22, Rkua2MasterKegiatanPeer::KODE_PROGRAM2 => 23, Rkua2MasterKegiatanPeer::KODE_URUSAN => 24, Rkua2MasterKegiatanPeer::LAST_UPDATE_USER => 25, Rkua2MasterKegiatanPeer::LAST_UPDATE_TIME => 26, Rkua2MasterKegiatanPeer::LAST_UPDATE_IP => 27, Rkua2MasterKegiatanPeer::TAHAP => 28, Rkua2MasterKegiatanPeer::KODE_MISI => 29, Rkua2MasterKegiatanPeer::KODE_TUJUAN => 30, Rkua2MasterKegiatanPeer::RANKING => 31, Rkua2MasterKegiatanPeer::NOMOR13 => 32, Rkua2MasterKegiatanPeer::PPA_NAMA => 33, Rkua2MasterKegiatanPeer::PPA_PANGKAT => 34, Rkua2MasterKegiatanPeer::PPA_NIP => 35, Rkua2MasterKegiatanPeer::LANJUTAN => 36, Rkua2MasterKegiatanPeer::USER_ID => 37, Rkua2MasterKegiatanPeer::ID => 38, Rkua2MasterKegiatanPeer::TAHUN => 39, Rkua2MasterKegiatanPeer::TAMBAHAN_PAGU => 40, Rkua2MasterKegiatanPeer::GENDER => 41, Rkua2MasterKegiatanPeer::KODE_KEG_KEUANGAN => 42, Rkua2MasterKegiatanPeer::USER_ID_LAMA => 43, Rkua2MasterKegiatanPeer::INDIKATOR => 44, Rkua2MasterKegiatanPeer::IS_DAK => 45, Rkua2MasterKegiatanPeer::KODE_KEGIATAN_ASAL => 46, Rkua2MasterKegiatanPeer::KODE_KEG_KEUANGAN_ASAL => 47, Rkua2MasterKegiatanPeer::TH_KE_MULTIYEARS => 48, Rkua2MasterKegiatanPeer::KELOMPOK_SASARAN => 49, Rkua2MasterKegiatanPeer::PAGU_BAPPEKO => 50, Rkua2MasterKegiatanPeer::KODE_DPA => 51, Rkua2MasterKegiatanPeer::USER_ID_PPTK => 52, Rkua2MasterKegiatanPeer::USER_ID_KPA => 53, Rkua2MasterKegiatanPeer::CATATAN_PEMBAHASAN => 54, Rkua2MasterKegiatanPeer::CATATAN_PENYELIA => 55, Rkua2MasterKegiatanPeer::CATATAN_BAPPEKO => 56, Rkua2MasterKegiatanPeer::STATUS_LEVEL => 57, Rkua2MasterKegiatanPeer::IS_TAPD_SETUJU => 58, Rkua2MasterKegiatanPeer::IS_BAPPEKO_SETUJU => 59, Rkua2MasterKegiatanPeer::IS_PENYELIA_SETUJU => 60, Rkua2MasterKegiatanPeer::IS_PERNAH_RKA => 61, Rkua2MasterKegiatanPeer::KODE_KEGIATAN_BARU => 62, ),
		BasePeer::TYPE_FIELDNAME => array ('unit_id' => 0, 'kode_kegiatan' => 1, 'kode_bidang' => 2, 'kode_urusan_wajib' => 3, 'kode_program' => 4, 'kode_sasaran' => 5, 'kode_indikator' => 6, 'alokasi_dana' => 7, 'nama_kegiatan' => 8, 'masukan' => 9, 'output' => 10, 'outcome' => 11, 'benefit' => 12, 'impact' => 13, 'tipe' => 14, 'kegiatan_active' => 15, 'to_kegiatan_code' => 16, 'catatan' => 17, 'target_outcome' => 18, 'lokasi' => 19, 'jumlah_prev' => 20, 'jumlah_now' => 21, 'jumlah_next' => 22, 'kode_program2' => 23, 'kode_urusan' => 24, 'last_update_user' => 25, 'last_update_time' => 26, 'last_update_ip' => 27, 'tahap' => 28, 'kode_misi' => 29, 'kode_tujuan' => 30, 'ranking' => 31, 'nomor13' => 32, 'ppa_nama' => 33, 'ppa_pangkat' => 34, 'ppa_nip' => 35, 'lanjutan' => 36, 'user_id' => 37, 'id' => 38, 'tahun' => 39, 'tambahan_pagu' => 40, 'gender' => 41, 'kode_keg_keuangan' => 42, 'user_id_lama' => 43, 'indikator' => 44, 'is_dak' => 45, 'kode_kegiatan_asal' => 46, 'kode_keg_keuangan_asal' => 47, 'th_ke_multiyears' => 48, 'kelompok_sasaran' => 49, 'pagu_bappeko' => 50, 'kode_dpa' => 51, 'user_id_pptk' => 52, 'user_id_kpa' => 53, 'catatan_pembahasan' => 54, 'catatan_penyelia' => 55, 'catatan_bappeko' => 56, 'status_level' => 57, 'is_tapd_setuju' => 58, 'is_bappeko_setuju' => 59, 'is_penyelia_setuju' => 60, 'is_pernah_rka' => 61, 'kode_kegiatan_baru' => 62, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/Rkua2MasterKegiatanMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.Rkua2MasterKegiatanMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = Rkua2MasterKegiatanPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(Rkua2MasterKegiatanPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(Rkua2MasterKegiatanPeer::UNIT_ID);

		$criteria->addSelectColumn(Rkua2MasterKegiatanPeer::KODE_KEGIATAN);

		$criteria->addSelectColumn(Rkua2MasterKegiatanPeer::KODE_BIDANG);

		$criteria->addSelectColumn(Rkua2MasterKegiatanPeer::KODE_URUSAN_WAJIB);

		$criteria->addSelectColumn(Rkua2MasterKegiatanPeer::KODE_PROGRAM);

		$criteria->addSelectColumn(Rkua2MasterKegiatanPeer::KODE_SASARAN);

		$criteria->addSelectColumn(Rkua2MasterKegiatanPeer::KODE_INDIKATOR);

		$criteria->addSelectColumn(Rkua2MasterKegiatanPeer::ALOKASI_DANA);

		$criteria->addSelectColumn(Rkua2MasterKegiatanPeer::NAMA_KEGIATAN);

		$criteria->addSelectColumn(Rkua2MasterKegiatanPeer::MASUKAN);

		$criteria->addSelectColumn(Rkua2MasterKegiatanPeer::OUTPUT);

		$criteria->addSelectColumn(Rkua2MasterKegiatanPeer::OUTCOME);

		$criteria->addSelectColumn(Rkua2MasterKegiatanPeer::BENEFIT);

		$criteria->addSelectColumn(Rkua2MasterKegiatanPeer::IMPACT);

		$criteria->addSelectColumn(Rkua2MasterKegiatanPeer::TIPE);

		$criteria->addSelectColumn(Rkua2MasterKegiatanPeer::KEGIATAN_ACTIVE);

		$criteria->addSelectColumn(Rkua2MasterKegiatanPeer::TO_KEGIATAN_CODE);

		$criteria->addSelectColumn(Rkua2MasterKegiatanPeer::CATATAN);

		$criteria->addSelectColumn(Rkua2MasterKegiatanPeer::TARGET_OUTCOME);

		$criteria->addSelectColumn(Rkua2MasterKegiatanPeer::LOKASI);

		$criteria->addSelectColumn(Rkua2MasterKegiatanPeer::JUMLAH_PREV);

		$criteria->addSelectColumn(Rkua2MasterKegiatanPeer::JUMLAH_NOW);

		$criteria->addSelectColumn(Rkua2MasterKegiatanPeer::JUMLAH_NEXT);

		$criteria->addSelectColumn(Rkua2MasterKegiatanPeer::KODE_PROGRAM2);

		$criteria->addSelectColumn(Rkua2MasterKegiatanPeer::KODE_URUSAN);

		$criteria->addSelectColumn(Rkua2MasterKegiatanPeer::LAST_UPDATE_USER);

		$criteria->addSelectColumn(Rkua2MasterKegiatanPeer::LAST_UPDATE_TIME);

		$criteria->addSelectColumn(Rkua2MasterKegiatanPeer::LAST_UPDATE_IP);

		$criteria->addSelectColumn(Rkua2MasterKegiatanPeer::TAHAP);

		$criteria->addSelectColumn(Rkua2MasterKegiatanPeer::KODE_MISI);

		$criteria->addSelectColumn(Rkua2MasterKegiatanPeer::KODE_TUJUAN);

		$criteria->addSelectColumn(Rkua2MasterKegiatanPeer::RANKING);

		$criteria->addSelectColumn(Rkua2MasterKegiatanPeer::NOMOR13);

		$criteria->addSelectColumn(Rkua2MasterKegiatanPeer::PPA_NAMA);

		$criteria->addSelectColumn(Rkua2MasterKegiatanPeer::PPA_PANGKAT);

		$criteria->addSelectColumn(Rkua2MasterKegiatanPeer::PPA_NIP);

		$criteria->addSelectColumn(Rkua2MasterKegiatanPeer::LANJUTAN);

		$criteria->addSelectColumn(Rkua2MasterKegiatanPeer::USER_ID);

		$criteria->addSelectColumn(Rkua2MasterKegiatanPeer::ID);

		$criteria->addSelectColumn(Rkua2MasterKegiatanPeer::TAHUN);

		$criteria->addSelectColumn(Rkua2MasterKegiatanPeer::TAMBAHAN_PAGU);

		$criteria->addSelectColumn(Rkua2MasterKegiatanPeer::GENDER);

		$criteria->addSelectColumn(Rkua2MasterKegiatanPeer::KODE_KEG_KEUANGAN);

		$criteria->addSelectColumn(Rkua2MasterKegiatanPeer::USER_ID_LAMA);

		$criteria->addSelectColumn(Rkua2MasterKegiatanPeer::INDIKATOR);

		$criteria->addSelectColumn(Rkua2MasterKegiatanPeer::IS_DAK);

		$criteria->addSelectColumn(Rkua2MasterKegiatanPeer::KODE_KEGIATAN_ASAL);

		$criteria->addSelectColumn(Rkua2MasterKegiatanPeer::KODE_KEG_KEUANGAN_ASAL);

		$criteria->addSelectColumn(Rkua2MasterKegiatanPeer::TH_KE_MULTIYEARS);

		$criteria->addSelectColumn(Rkua2MasterKegiatanPeer::KELOMPOK_SASARAN);

		$criteria->addSelectColumn(Rkua2MasterKegiatanPeer::PAGU_BAPPEKO);

		$criteria->addSelectColumn(Rkua2MasterKegiatanPeer::KODE_DPA);

		$criteria->addSelectColumn(Rkua2MasterKegiatanPeer::USER_ID_PPTK);

		$criteria->addSelectColumn(Rkua2MasterKegiatanPeer::USER_ID_KPA);

		$criteria->addSelectColumn(Rkua2MasterKegiatanPeer::CATATAN_PEMBAHASAN);

		$criteria->addSelectColumn(Rkua2MasterKegiatanPeer::CATATAN_PENYELIA);

		$criteria->addSelectColumn(Rkua2MasterKegiatanPeer::CATATAN_BAPPEKO);

		$criteria->addSelectColumn(Rkua2MasterKegiatanPeer::STATUS_LEVEL);

		$criteria->addSelectColumn(Rkua2MasterKegiatanPeer::IS_TAPD_SETUJU);

		$criteria->addSelectColumn(Rkua2MasterKegiatanPeer::IS_BAPPEKO_SETUJU);

		$criteria->addSelectColumn(Rkua2MasterKegiatanPeer::IS_PENYELIA_SETUJU);

		$criteria->addSelectColumn(Rkua2MasterKegiatanPeer::IS_PERNAH_RKA);

		$criteria->addSelectColumn(Rkua2MasterKegiatanPeer::KODE_KEGIATAN_BARU);

	}

	const COUNT = 'COUNT(ebudget.rkua2_master_kegiatan.UNIT_ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.rkua2_master_kegiatan.UNIT_ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(Rkua2MasterKegiatanPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(Rkua2MasterKegiatanPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = Rkua2MasterKegiatanPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = Rkua2MasterKegiatanPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return Rkua2MasterKegiatanPeer::populateObjects(Rkua2MasterKegiatanPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			Rkua2MasterKegiatanPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = Rkua2MasterKegiatanPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return Rkua2MasterKegiatanPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}

		$criteria->remove(Rkua2MasterKegiatanPeer::ID); 

				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(Rkua2MasterKegiatanPeer::UNIT_ID);
			$selectCriteria->add(Rkua2MasterKegiatanPeer::UNIT_ID, $criteria->remove(Rkua2MasterKegiatanPeer::UNIT_ID), $comparison);

			$comparison = $criteria->getComparison(Rkua2MasterKegiatanPeer::KODE_KEGIATAN);
			$selectCriteria->add(Rkua2MasterKegiatanPeer::KODE_KEGIATAN, $criteria->remove(Rkua2MasterKegiatanPeer::KODE_KEGIATAN), $comparison);

			$comparison = $criteria->getComparison(Rkua2MasterKegiatanPeer::ID);
			$selectCriteria->add(Rkua2MasterKegiatanPeer::ID, $criteria->remove(Rkua2MasterKegiatanPeer::ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(Rkua2MasterKegiatanPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(Rkua2MasterKegiatanPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof Rkua2MasterKegiatan) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
												if(count($values) == count($values, COUNT_RECURSIVE))
			{
								$values = array($values);
			}
			$vals = array();
			foreach($values as $value)
			{

				$vals[0][] = $value[0];
				$vals[1][] = $value[1];
				$vals[2][] = $value[2];
			}

			$criteria->add(Rkua2MasterKegiatanPeer::UNIT_ID, $vals[0], Criteria::IN);
			$criteria->add(Rkua2MasterKegiatanPeer::KODE_KEGIATAN, $vals[1], Criteria::IN);
			$criteria->add(Rkua2MasterKegiatanPeer::ID, $vals[2], Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(Rkua2MasterKegiatan $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(Rkua2MasterKegiatanPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(Rkua2MasterKegiatanPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(Rkua2MasterKegiatanPeer::DATABASE_NAME, Rkua2MasterKegiatanPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = Rkua2MasterKegiatanPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK( $unit_id, $kode_kegiatan, $id, $con = null) {
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$criteria = new Criteria();
		$criteria->add(Rkua2MasterKegiatanPeer::UNIT_ID, $unit_id);
		$criteria->add(Rkua2MasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
		$criteria->add(Rkua2MasterKegiatanPeer::ID, $id);
		$v = Rkua2MasterKegiatanPeer::doSelect($criteria, $con);

		return !empty($v) ? $v[0] : null;
	}
} 
if (Propel::isInit()) {
			try {
		BaseRkua2MasterKegiatanPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/Rkua2MasterKegiatanMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.Rkua2MasterKegiatanMapBuilder');
}
