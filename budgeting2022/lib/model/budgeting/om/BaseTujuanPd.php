<?php


abstract class BaseTujuanPd extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id;


	
	protected $id_tujuan;


	
	protected $tujuan_pd;


	
	protected $unit_id;


	
	protected $kode_kegiatan;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getId()
	{

		return $this->id;
	}

	
	public function getIdTujuan()
	{

		return $this->id_tujuan;
	}

	
	public function getTujuanPd()
	{

		return $this->tujuan_pd;
	}

	
	public function getUnitId()
	{

		return $this->unit_id;
	}

	
	public function getKodeKegiatan()
	{

		return $this->kode_kegiatan;
	}

	
	public function setId($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = TujuanPdPeer::ID;
		}

	} 
	
	public function setIdTujuan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->id_tujuan !== $v) {
			$this->id_tujuan = $v;
			$this->modifiedColumns[] = TujuanPdPeer::ID_TUJUAN;
		}

	} 
	
	public function setTujuanPd($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->tujuan_pd !== $v) {
			$this->tujuan_pd = $v;
			$this->modifiedColumns[] = TujuanPdPeer::TUJUAN_PD;
		}

	} 
	
	public function setUnitId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->unit_id !== $v) {
			$this->unit_id = $v;
			$this->modifiedColumns[] = TujuanPdPeer::UNIT_ID;
		}

	} 
	
	public function setKodeKegiatan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_kegiatan !== $v) {
			$this->kode_kegiatan = $v;
			$this->modifiedColumns[] = TujuanPdPeer::KODE_KEGIATAN;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getInt($startcol + 0);

			$this->id_tujuan = $rs->getString($startcol + 1);

			$this->tujuan_pd = $rs->getString($startcol + 2);

			$this->unit_id = $rs->getString($startcol + 3);

			$this->kode_kegiatan = $rs->getString($startcol + 4);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 5; 
		} catch (Exception $e) {
			throw new PropelException("Error populating TujuanPd object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(TujuanPdPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			TujuanPdPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(TujuanPdPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = TujuanPdPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += TujuanPdPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = TujuanPdPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = TujuanPdPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getIdTujuan();
				break;
			case 2:
				return $this->getTujuanPd();
				break;
			case 3:
				return $this->getUnitId();
				break;
			case 4:
				return $this->getKodeKegiatan();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = TujuanPdPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getIdTujuan(),
			$keys[2] => $this->getTujuanPd(),
			$keys[3] => $this->getUnitId(),
			$keys[4] => $this->getKodeKegiatan(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = TujuanPdPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setIdTujuan($value);
				break;
			case 2:
				$this->setTujuanPd($value);
				break;
			case 3:
				$this->setUnitId($value);
				break;
			case 4:
				$this->setKodeKegiatan($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = TujuanPdPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setIdTujuan($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setTujuanPd($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setUnitId($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setKodeKegiatan($arr[$keys[4]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(TujuanPdPeer::DATABASE_NAME);

		if ($this->isColumnModified(TujuanPdPeer::ID)) $criteria->add(TujuanPdPeer::ID, $this->id);
		if ($this->isColumnModified(TujuanPdPeer::ID_TUJUAN)) $criteria->add(TujuanPdPeer::ID_TUJUAN, $this->id_tujuan);
		if ($this->isColumnModified(TujuanPdPeer::TUJUAN_PD)) $criteria->add(TujuanPdPeer::TUJUAN_PD, $this->tujuan_pd);
		if ($this->isColumnModified(TujuanPdPeer::UNIT_ID)) $criteria->add(TujuanPdPeer::UNIT_ID, $this->unit_id);
		if ($this->isColumnModified(TujuanPdPeer::KODE_KEGIATAN)) $criteria->add(TujuanPdPeer::KODE_KEGIATAN, $this->kode_kegiatan);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(TujuanPdPeer::DATABASE_NAME);

		$criteria->add(TujuanPdPeer::ID, $this->id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setIdTujuan($this->id_tujuan);

		$copyObj->setTujuanPd($this->tujuan_pd);

		$copyObj->setUnitId($this->unit_id);

		$copyObj->setKodeKegiatan($this->kode_kegiatan);


		$copyObj->setNew(true);

		$copyObj->setId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new TujuanPdPeer();
		}
		return self::$peer;
	}

} 