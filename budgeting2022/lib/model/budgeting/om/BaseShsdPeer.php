<?php


abstract class BaseShsdPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.shsd';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.Shsd';

	
	const NUM_COLUMNS = 21;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const SHSD_ID = 'ebudget.shsd.SHSD_ID';

	
	const SATUAN = 'ebudget.shsd.SATUAN';

	
	const SHSD_NAME = 'ebudget.shsd.SHSD_NAME';

	
	const SHSD_HARGA = 'ebudget.shsd.SHSD_HARGA';

	
	const SHSD_HARGA_DASAR = 'ebudget.shsd.SHSD_HARGA_DASAR';

	
	const SHSD_SHOW = 'ebudget.shsd.SHSD_SHOW';

	
	const SHSD_FAKTOR_INFLASI = 'ebudget.shsd.SHSD_FAKTOR_INFLASI';

	
	const SHSD_LOCKED = 'ebudget.shsd.SHSD_LOCKED';

	
	const REKENING_CODE = 'ebudget.shsd.REKENING_CODE';

	
	const SHSD_MERK = 'ebudget.shsd.SHSD_MERK';

	
	const NON_PAJAK = 'ebudget.shsd.NON_PAJAK';

	
	const SHSD_ID_OLD = 'ebudget.shsd.SHSD_ID_OLD';

	
	const NAMA_DASAR = 'ebudget.shsd.NAMA_DASAR';

	
	const SPEC = 'ebudget.shsd.SPEC';

	
	const HIDDEN_SPEC = 'ebudget.shsd.HIDDEN_SPEC';

	
	const USULAN_SKPD = 'ebudget.shsd.USULAN_SKPD';

	
	const IS_POTONG_BPJS = 'ebudget.shsd.IS_POTONG_BPJS';

	
	const IS_IURAN_BPJS = 'ebudget.shsd.IS_IURAN_BPJS';

	
	const TAHAP = 'ebudget.shsd.TAHAP';

	
	const IS_SURVEY_BP = 'ebudget.shsd.IS_SURVEY_BP';

	
	const IS_INFLASI = 'ebudget.shsd.IS_INFLASI';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('ShsdId', 'Satuan', 'ShsdName', 'ShsdHarga', 'ShsdHargaDasar', 'ShsdShow', 'ShsdFaktorInflasi', 'ShsdLocked', 'RekeningCode', 'ShsdMerk', 'NonPajak', 'ShsdIdOld', 'NamaDasar', 'Spec', 'HiddenSpec', 'UsulanSkpd', 'IsPotongBpjs', 'IsIuranBpjs', 'Tahap', 'IsSurveyBp', 'IsInflasi', ),
		BasePeer::TYPE_COLNAME => array (ShsdPeer::SHSD_ID, ShsdPeer::SATUAN, ShsdPeer::SHSD_NAME, ShsdPeer::SHSD_HARGA, ShsdPeer::SHSD_HARGA_DASAR, ShsdPeer::SHSD_SHOW, ShsdPeer::SHSD_FAKTOR_INFLASI, ShsdPeer::SHSD_LOCKED, ShsdPeer::REKENING_CODE, ShsdPeer::SHSD_MERK, ShsdPeer::NON_PAJAK, ShsdPeer::SHSD_ID_OLD, ShsdPeer::NAMA_DASAR, ShsdPeer::SPEC, ShsdPeer::HIDDEN_SPEC, ShsdPeer::USULAN_SKPD, ShsdPeer::IS_POTONG_BPJS, ShsdPeer::IS_IURAN_BPJS, ShsdPeer::TAHAP, ShsdPeer::IS_SURVEY_BP, ShsdPeer::IS_INFLASI, ),
		BasePeer::TYPE_FIELDNAME => array ('shsd_id', 'satuan', 'shsd_name', 'shsd_harga', 'shsd_harga_dasar', 'shsd_show', 'shsd_faktor_inflasi', 'shsd_locked', 'rekening_code', 'shsd_merk', 'non_pajak', 'shsd_id_old', 'nama_dasar', 'spec', 'hidden_spec', 'usulan_skpd', 'is_potong_bpjs', 'is_iuran_bpjs', 'tahap', 'is_survey_bp', 'is_inflasi', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('ShsdId' => 0, 'Satuan' => 1, 'ShsdName' => 2, 'ShsdHarga' => 3, 'ShsdHargaDasar' => 4, 'ShsdShow' => 5, 'ShsdFaktorInflasi' => 6, 'ShsdLocked' => 7, 'RekeningCode' => 8, 'ShsdMerk' => 9, 'NonPajak' => 10, 'ShsdIdOld' => 11, 'NamaDasar' => 12, 'Spec' => 13, 'HiddenSpec' => 14, 'UsulanSkpd' => 15, 'IsPotongBpjs' => 16, 'IsIuranBpjs' => 17, 'Tahap' => 18, 'IsSurveyBp' => 19, 'IsInflasi' => 20, ),
		BasePeer::TYPE_COLNAME => array (ShsdPeer::SHSD_ID => 0, ShsdPeer::SATUAN => 1, ShsdPeer::SHSD_NAME => 2, ShsdPeer::SHSD_HARGA => 3, ShsdPeer::SHSD_HARGA_DASAR => 4, ShsdPeer::SHSD_SHOW => 5, ShsdPeer::SHSD_FAKTOR_INFLASI => 6, ShsdPeer::SHSD_LOCKED => 7, ShsdPeer::REKENING_CODE => 8, ShsdPeer::SHSD_MERK => 9, ShsdPeer::NON_PAJAK => 10, ShsdPeer::SHSD_ID_OLD => 11, ShsdPeer::NAMA_DASAR => 12, ShsdPeer::SPEC => 13, ShsdPeer::HIDDEN_SPEC => 14, ShsdPeer::USULAN_SKPD => 15, ShsdPeer::IS_POTONG_BPJS => 16, ShsdPeer::IS_IURAN_BPJS => 17, ShsdPeer::TAHAP => 18, ShsdPeer::IS_SURVEY_BP => 19, ShsdPeer::IS_INFLASI => 20, ),
		BasePeer::TYPE_FIELDNAME => array ('shsd_id' => 0, 'satuan' => 1, 'shsd_name' => 2, 'shsd_harga' => 3, 'shsd_harga_dasar' => 4, 'shsd_show' => 5, 'shsd_faktor_inflasi' => 6, 'shsd_locked' => 7, 'rekening_code' => 8, 'shsd_merk' => 9, 'non_pajak' => 10, 'shsd_id_old' => 11, 'nama_dasar' => 12, 'spec' => 13, 'hidden_spec' => 14, 'usulan_skpd' => 15, 'is_potong_bpjs' => 16, 'is_iuran_bpjs' => 17, 'tahap' => 18, 'is_survey_bp' => 19, 'is_inflasi' => 20, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/ShsdMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.ShsdMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = ShsdPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(ShsdPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(ShsdPeer::SHSD_ID);

		$criteria->addSelectColumn(ShsdPeer::SATUAN);

		$criteria->addSelectColumn(ShsdPeer::SHSD_NAME);

		$criteria->addSelectColumn(ShsdPeer::SHSD_HARGA);

		$criteria->addSelectColumn(ShsdPeer::SHSD_HARGA_DASAR);

		$criteria->addSelectColumn(ShsdPeer::SHSD_SHOW);

		$criteria->addSelectColumn(ShsdPeer::SHSD_FAKTOR_INFLASI);

		$criteria->addSelectColumn(ShsdPeer::SHSD_LOCKED);

		$criteria->addSelectColumn(ShsdPeer::REKENING_CODE);

		$criteria->addSelectColumn(ShsdPeer::SHSD_MERK);

		$criteria->addSelectColumn(ShsdPeer::NON_PAJAK);

		$criteria->addSelectColumn(ShsdPeer::SHSD_ID_OLD);

		$criteria->addSelectColumn(ShsdPeer::NAMA_DASAR);

		$criteria->addSelectColumn(ShsdPeer::SPEC);

		$criteria->addSelectColumn(ShsdPeer::HIDDEN_SPEC);

		$criteria->addSelectColumn(ShsdPeer::USULAN_SKPD);

		$criteria->addSelectColumn(ShsdPeer::IS_POTONG_BPJS);

		$criteria->addSelectColumn(ShsdPeer::IS_IURAN_BPJS);

		$criteria->addSelectColumn(ShsdPeer::TAHAP);

		$criteria->addSelectColumn(ShsdPeer::IS_SURVEY_BP);

		$criteria->addSelectColumn(ShsdPeer::IS_INFLASI);

	}

	const COUNT = 'COUNT(ebudget.shsd.SHSD_ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.shsd.SHSD_ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(ShsdPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(ShsdPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = ShsdPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = ShsdPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return ShsdPeer::populateObjects(ShsdPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			ShsdPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = ShsdPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return ShsdPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(ShsdPeer::SHSD_ID);
			$selectCriteria->add(ShsdPeer::SHSD_ID, $criteria->remove(ShsdPeer::SHSD_ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(ShsdPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(ShsdPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof Shsd) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(ShsdPeer::SHSD_ID, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(Shsd $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(ShsdPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(ShsdPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(ShsdPeer::DATABASE_NAME, ShsdPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = ShsdPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(ShsdPeer::DATABASE_NAME);

		$criteria->add(ShsdPeer::SHSD_ID, $pk);


		$v = ShsdPeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(ShsdPeer::SHSD_ID, $pks, Criteria::IN);
			$objs = ShsdPeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BaseShsdPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/ShsdMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.ShsdMapBuilder');
}
