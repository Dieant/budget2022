<?php


abstract class BaseLogPerubahanRevisiPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.log_perubahan_revisi';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.LogPerubahanRevisi';

	
	const NUM_COLUMNS = 11;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const KEGIATAN_CODE = 'ebudget.log_perubahan_revisi.KEGIATAN_CODE';

	
	const DETAIL_NO = 'ebudget.log_perubahan_revisi.DETAIL_NO';

	
	const UNIT_ID = 'ebudget.log_perubahan_revisi.UNIT_ID';

	
	const TAHAP = 'ebudget.log_perubahan_revisi.TAHAP';

	
	const STATUS = 'ebudget.log_perubahan_revisi.STATUS';

	
	const NILAI_ANGGARAN_SEMULA = 'ebudget.log_perubahan_revisi.NILAI_ANGGARAN_SEMULA';

	
	const NILAI_ANGGARAN_MENJADI = 'ebudget.log_perubahan_revisi.NILAI_ANGGARAN_MENJADI';

	
	const VOLUME_SEMULA = 'ebudget.log_perubahan_revisi.VOLUME_SEMULA';

	
	const VOLUME_MENJADI = 'ebudget.log_perubahan_revisi.VOLUME_MENJADI';

	
	const SATUAN_SEMULA = 'ebudget.log_perubahan_revisi.SATUAN_SEMULA';

	
	const SATUAN_MENJADI = 'ebudget.log_perubahan_revisi.SATUAN_MENJADI';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('KegiatanCode', 'DetailNo', 'UnitId', 'Tahap', 'Status', 'NilaiAnggaranSemula', 'NilaiAnggaranMenjadi', 'VolumeSemula', 'VolumeMenjadi', 'SatuanSemula', 'SatuanMenjadi', ),
		BasePeer::TYPE_COLNAME => array (LogPerubahanRevisiPeer::KEGIATAN_CODE, LogPerubahanRevisiPeer::DETAIL_NO, LogPerubahanRevisiPeer::UNIT_ID, LogPerubahanRevisiPeer::TAHAP, LogPerubahanRevisiPeer::STATUS, LogPerubahanRevisiPeer::NILAI_ANGGARAN_SEMULA, LogPerubahanRevisiPeer::NILAI_ANGGARAN_MENJADI, LogPerubahanRevisiPeer::VOLUME_SEMULA, LogPerubahanRevisiPeer::VOLUME_MENJADI, LogPerubahanRevisiPeer::SATUAN_SEMULA, LogPerubahanRevisiPeer::SATUAN_MENJADI, ),
		BasePeer::TYPE_FIELDNAME => array ('kegiatan_code', 'detail_no', 'unit_id', 'tahap', 'status', 'nilai_anggaran_semula', 'nilai_anggaran_menjadi', 'volume_semula', 'volume_menjadi', 'satuan_semula', 'satuan_menjadi', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('KegiatanCode' => 0, 'DetailNo' => 1, 'UnitId' => 2, 'Tahap' => 3, 'Status' => 4, 'NilaiAnggaranSemula' => 5, 'NilaiAnggaranMenjadi' => 6, 'VolumeSemula' => 7, 'VolumeMenjadi' => 8, 'SatuanSemula' => 9, 'SatuanMenjadi' => 10, ),
		BasePeer::TYPE_COLNAME => array (LogPerubahanRevisiPeer::KEGIATAN_CODE => 0, LogPerubahanRevisiPeer::DETAIL_NO => 1, LogPerubahanRevisiPeer::UNIT_ID => 2, LogPerubahanRevisiPeer::TAHAP => 3, LogPerubahanRevisiPeer::STATUS => 4, LogPerubahanRevisiPeer::NILAI_ANGGARAN_SEMULA => 5, LogPerubahanRevisiPeer::NILAI_ANGGARAN_MENJADI => 6, LogPerubahanRevisiPeer::VOLUME_SEMULA => 7, LogPerubahanRevisiPeer::VOLUME_MENJADI => 8, LogPerubahanRevisiPeer::SATUAN_SEMULA => 9, LogPerubahanRevisiPeer::SATUAN_MENJADI => 10, ),
		BasePeer::TYPE_FIELDNAME => array ('kegiatan_code' => 0, 'detail_no' => 1, 'unit_id' => 2, 'tahap' => 3, 'status' => 4, 'nilai_anggaran_semula' => 5, 'nilai_anggaran_menjadi' => 6, 'volume_semula' => 7, 'volume_menjadi' => 8, 'satuan_semula' => 9, 'satuan_menjadi' => 10, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/LogPerubahanRevisiMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.LogPerubahanRevisiMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = LogPerubahanRevisiPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(LogPerubahanRevisiPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(LogPerubahanRevisiPeer::KEGIATAN_CODE);

		$criteria->addSelectColumn(LogPerubahanRevisiPeer::DETAIL_NO);

		$criteria->addSelectColumn(LogPerubahanRevisiPeer::UNIT_ID);

		$criteria->addSelectColumn(LogPerubahanRevisiPeer::TAHAP);

		$criteria->addSelectColumn(LogPerubahanRevisiPeer::STATUS);

		$criteria->addSelectColumn(LogPerubahanRevisiPeer::NILAI_ANGGARAN_SEMULA);

		$criteria->addSelectColumn(LogPerubahanRevisiPeer::NILAI_ANGGARAN_MENJADI);

		$criteria->addSelectColumn(LogPerubahanRevisiPeer::VOLUME_SEMULA);

		$criteria->addSelectColumn(LogPerubahanRevisiPeer::VOLUME_MENJADI);

		$criteria->addSelectColumn(LogPerubahanRevisiPeer::SATUAN_SEMULA);

		$criteria->addSelectColumn(LogPerubahanRevisiPeer::SATUAN_MENJADI);

	}

	const COUNT = 'COUNT(ebudget.log_perubahan_revisi.KEGIATAN_CODE)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.log_perubahan_revisi.KEGIATAN_CODE)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(LogPerubahanRevisiPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(LogPerubahanRevisiPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = LogPerubahanRevisiPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = LogPerubahanRevisiPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return LogPerubahanRevisiPeer::populateObjects(LogPerubahanRevisiPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			LogPerubahanRevisiPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = LogPerubahanRevisiPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return LogPerubahanRevisiPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(LogPerubahanRevisiPeer::KEGIATAN_CODE);
			$selectCriteria->add(LogPerubahanRevisiPeer::KEGIATAN_CODE, $criteria->remove(LogPerubahanRevisiPeer::KEGIATAN_CODE), $comparison);

			$comparison = $criteria->getComparison(LogPerubahanRevisiPeer::DETAIL_NO);
			$selectCriteria->add(LogPerubahanRevisiPeer::DETAIL_NO, $criteria->remove(LogPerubahanRevisiPeer::DETAIL_NO), $comparison);

			$comparison = $criteria->getComparison(LogPerubahanRevisiPeer::UNIT_ID);
			$selectCriteria->add(LogPerubahanRevisiPeer::UNIT_ID, $criteria->remove(LogPerubahanRevisiPeer::UNIT_ID), $comparison);

			$comparison = $criteria->getComparison(LogPerubahanRevisiPeer::TAHAP);
			$selectCriteria->add(LogPerubahanRevisiPeer::TAHAP, $criteria->remove(LogPerubahanRevisiPeer::TAHAP), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(LogPerubahanRevisiPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(LogPerubahanRevisiPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof LogPerubahanRevisi) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
												if(count($values) == count($values, COUNT_RECURSIVE))
			{
								$values = array($values);
			}
			$vals = array();
			foreach($values as $value)
			{

				$vals[0][] = $value[0];
				$vals[1][] = $value[1];
				$vals[2][] = $value[2];
				$vals[3][] = $value[3];
			}

			$criteria->add(LogPerubahanRevisiPeer::KEGIATAN_CODE, $vals[0], Criteria::IN);
			$criteria->add(LogPerubahanRevisiPeer::DETAIL_NO, $vals[1], Criteria::IN);
			$criteria->add(LogPerubahanRevisiPeer::UNIT_ID, $vals[2], Criteria::IN);
			$criteria->add(LogPerubahanRevisiPeer::TAHAP, $vals[3], Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(LogPerubahanRevisi $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(LogPerubahanRevisiPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(LogPerubahanRevisiPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(LogPerubahanRevisiPeer::DATABASE_NAME, LogPerubahanRevisiPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = LogPerubahanRevisiPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK( $kegiatan_code, $detail_no, $unit_id, $tahap, $con = null) {
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$criteria = new Criteria();
		$criteria->add(LogPerubahanRevisiPeer::KEGIATAN_CODE, $kegiatan_code);
		$criteria->add(LogPerubahanRevisiPeer::DETAIL_NO, $detail_no);
		$criteria->add(LogPerubahanRevisiPeer::UNIT_ID, $unit_id);
		$criteria->add(LogPerubahanRevisiPeer::TAHAP, $tahap);
		$v = LogPerubahanRevisiPeer::doSelect($criteria, $con);

		return !empty($v) ? $v[0] : null;
	}
} 
if (Propel::isInit()) {
			try {
		BaseLogPerubahanRevisiPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/LogPerubahanRevisiMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.LogPerubahanRevisiMapBuilder');
}
