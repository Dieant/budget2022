<?php


abstract class BaseMasterSaluranPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'master_saluran';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.MasterSaluran';

	
	const NUM_COLUMNS = 8;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const KODE = 'master_saluran.KODE';

	
	const NAMA_SALURAN = 'master_saluran.NAMA_SALURAN';

	
	const BATASAN = 'master_saluran.BATASAN';

	
	const KECAMATAN = 'master_saluran.KECAMATAN';

	
	const PANJANG = 'master_saluran.PANJANG';

	
	const LEBAR = 'master_saluran.LEBAR';

	
	const KETERANGAN = 'master_saluran.KETERANGAN';

	
	const KELURAHAN = 'master_saluran.KELURAHAN';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('Kode', 'NamaSaluran', 'Batasan', 'Kecamatan', 'Panjang', 'Lebar', 'Keterangan', 'Kelurahan', ),
		BasePeer::TYPE_COLNAME => array (MasterSaluranPeer::KODE, MasterSaluranPeer::NAMA_SALURAN, MasterSaluranPeer::BATASAN, MasterSaluranPeer::KECAMATAN, MasterSaluranPeer::PANJANG, MasterSaluranPeer::LEBAR, MasterSaluranPeer::KETERANGAN, MasterSaluranPeer::KELURAHAN, ),
		BasePeer::TYPE_FIELDNAME => array ('kode', 'nama_saluran', 'batasan', 'kecamatan', 'panjang', 'lebar', 'keterangan', 'kelurahan', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('Kode' => 0, 'NamaSaluran' => 1, 'Batasan' => 2, 'Kecamatan' => 3, 'Panjang' => 4, 'Lebar' => 5, 'Keterangan' => 6, 'Kelurahan' => 7, ),
		BasePeer::TYPE_COLNAME => array (MasterSaluranPeer::KODE => 0, MasterSaluranPeer::NAMA_SALURAN => 1, MasterSaluranPeer::BATASAN => 2, MasterSaluranPeer::KECAMATAN => 3, MasterSaluranPeer::PANJANG => 4, MasterSaluranPeer::LEBAR => 5, MasterSaluranPeer::KETERANGAN => 6, MasterSaluranPeer::KELURAHAN => 7, ),
		BasePeer::TYPE_FIELDNAME => array ('kode' => 0, 'nama_saluran' => 1, 'batasan' => 2, 'kecamatan' => 3, 'panjang' => 4, 'lebar' => 5, 'keterangan' => 6, 'kelurahan' => 7, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/MasterSaluranMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.MasterSaluranMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = MasterSaluranPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(MasterSaluranPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(MasterSaluranPeer::KODE);

		$criteria->addSelectColumn(MasterSaluranPeer::NAMA_SALURAN);

		$criteria->addSelectColumn(MasterSaluranPeer::BATASAN);

		$criteria->addSelectColumn(MasterSaluranPeer::KECAMATAN);

		$criteria->addSelectColumn(MasterSaluranPeer::PANJANG);

		$criteria->addSelectColumn(MasterSaluranPeer::LEBAR);

		$criteria->addSelectColumn(MasterSaluranPeer::KETERANGAN);

		$criteria->addSelectColumn(MasterSaluranPeer::KELURAHAN);

	}

	const COUNT = 'COUNT(master_saluran.KODE)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT master_saluran.KODE)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(MasterSaluranPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(MasterSaluranPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = MasterSaluranPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = MasterSaluranPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return MasterSaluranPeer::populateObjects(MasterSaluranPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			MasterSaluranPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = MasterSaluranPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return MasterSaluranPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(MasterSaluranPeer::KODE);
			$selectCriteria->add(MasterSaluranPeer::KODE, $criteria->remove(MasterSaluranPeer::KODE), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(MasterSaluranPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(MasterSaluranPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof MasterSaluran) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(MasterSaluranPeer::KODE, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(MasterSaluran $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(MasterSaluranPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(MasterSaluranPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(MasterSaluranPeer::DATABASE_NAME, MasterSaluranPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = MasterSaluranPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(MasterSaluranPeer::DATABASE_NAME);

		$criteria->add(MasterSaluranPeer::KODE, $pk);


		$v = MasterSaluranPeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(MasterSaluranPeer::KODE, $pks, Criteria::IN);
			$objs = MasterSaluranPeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BaseMasterSaluranPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/MasterSaluranMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.MasterSaluranMapBuilder');
}
