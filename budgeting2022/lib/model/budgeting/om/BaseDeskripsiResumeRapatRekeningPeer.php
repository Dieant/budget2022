<?php


abstract class BaseDeskripsiResumeRapatRekeningPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.deskripsi_resume_rapat_rekening';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.DeskripsiResumeRapatRekening';

	
	const NUM_COLUMNS = 6;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const ID_DESKRIPSI_RESUME_RAPAT = 'ebudget.deskripsi_resume_rapat_rekening.ID_DESKRIPSI_RESUME_RAPAT';

	
	const KEGIATAN_CODE = 'ebudget.deskripsi_resume_rapat_rekening.KEGIATAN_CODE';

	
	const REKENING_CODE = 'ebudget.deskripsi_resume_rapat_rekening.REKENING_CODE';

	
	const REKENING_NAME = 'ebudget.deskripsi_resume_rapat_rekening.REKENING_NAME';

	
	const SEMULA = 'ebudget.deskripsi_resume_rapat_rekening.SEMULA';

	
	const MENJADI = 'ebudget.deskripsi_resume_rapat_rekening.MENJADI';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('IdDeskripsiResumeRapat', 'KegiatanCode', 'RekeningCode', 'RekeningName', 'Semula', 'Menjadi', ),
		BasePeer::TYPE_COLNAME => array (DeskripsiResumeRapatRekeningPeer::ID_DESKRIPSI_RESUME_RAPAT, DeskripsiResumeRapatRekeningPeer::KEGIATAN_CODE, DeskripsiResumeRapatRekeningPeer::REKENING_CODE, DeskripsiResumeRapatRekeningPeer::REKENING_NAME, DeskripsiResumeRapatRekeningPeer::SEMULA, DeskripsiResumeRapatRekeningPeer::MENJADI, ),
		BasePeer::TYPE_FIELDNAME => array ('id_deskripsi_resume_rapat', 'kegiatan_code', 'rekening_code', 'rekening_name', 'semula', 'menjadi', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('IdDeskripsiResumeRapat' => 0, 'KegiatanCode' => 1, 'RekeningCode' => 2, 'RekeningName' => 3, 'Semula' => 4, 'Menjadi' => 5, ),
		BasePeer::TYPE_COLNAME => array (DeskripsiResumeRapatRekeningPeer::ID_DESKRIPSI_RESUME_RAPAT => 0, DeskripsiResumeRapatRekeningPeer::KEGIATAN_CODE => 1, DeskripsiResumeRapatRekeningPeer::REKENING_CODE => 2, DeskripsiResumeRapatRekeningPeer::REKENING_NAME => 3, DeskripsiResumeRapatRekeningPeer::SEMULA => 4, DeskripsiResumeRapatRekeningPeer::MENJADI => 5, ),
		BasePeer::TYPE_FIELDNAME => array ('id_deskripsi_resume_rapat' => 0, 'kegiatan_code' => 1, 'rekening_code' => 2, 'rekening_name' => 3, 'semula' => 4, 'menjadi' => 5, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/DeskripsiResumeRapatRekeningMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.DeskripsiResumeRapatRekeningMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = DeskripsiResumeRapatRekeningPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(DeskripsiResumeRapatRekeningPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(DeskripsiResumeRapatRekeningPeer::ID_DESKRIPSI_RESUME_RAPAT);

		$criteria->addSelectColumn(DeskripsiResumeRapatRekeningPeer::KEGIATAN_CODE);

		$criteria->addSelectColumn(DeskripsiResumeRapatRekeningPeer::REKENING_CODE);

		$criteria->addSelectColumn(DeskripsiResumeRapatRekeningPeer::REKENING_NAME);

		$criteria->addSelectColumn(DeskripsiResumeRapatRekeningPeer::SEMULA);

		$criteria->addSelectColumn(DeskripsiResumeRapatRekeningPeer::MENJADI);

	}

	const COUNT = 'COUNT(ebudget.deskripsi_resume_rapat_rekening.ID_DESKRIPSI_RESUME_RAPAT)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.deskripsi_resume_rapat_rekening.ID_DESKRIPSI_RESUME_RAPAT)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(DeskripsiResumeRapatRekeningPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(DeskripsiResumeRapatRekeningPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = DeskripsiResumeRapatRekeningPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = DeskripsiResumeRapatRekeningPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return DeskripsiResumeRapatRekeningPeer::populateObjects(DeskripsiResumeRapatRekeningPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			DeskripsiResumeRapatRekeningPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = DeskripsiResumeRapatRekeningPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return DeskripsiResumeRapatRekeningPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(DeskripsiResumeRapatRekeningPeer::ID_DESKRIPSI_RESUME_RAPAT);
			$selectCriteria->add(DeskripsiResumeRapatRekeningPeer::ID_DESKRIPSI_RESUME_RAPAT, $criteria->remove(DeskripsiResumeRapatRekeningPeer::ID_DESKRIPSI_RESUME_RAPAT), $comparison);

			$comparison = $criteria->getComparison(DeskripsiResumeRapatRekeningPeer::KEGIATAN_CODE);
			$selectCriteria->add(DeskripsiResumeRapatRekeningPeer::KEGIATAN_CODE, $criteria->remove(DeskripsiResumeRapatRekeningPeer::KEGIATAN_CODE), $comparison);

			$comparison = $criteria->getComparison(DeskripsiResumeRapatRekeningPeer::REKENING_CODE);
			$selectCriteria->add(DeskripsiResumeRapatRekeningPeer::REKENING_CODE, $criteria->remove(DeskripsiResumeRapatRekeningPeer::REKENING_CODE), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(DeskripsiResumeRapatRekeningPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(DeskripsiResumeRapatRekeningPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof DeskripsiResumeRapatRekening) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
												if(count($values) == count($values, COUNT_RECURSIVE))
			{
								$values = array($values);
			}
			$vals = array();
			foreach($values as $value)
			{

				$vals[0][] = $value[0];
				$vals[1][] = $value[1];
				$vals[2][] = $value[2];
			}

			$criteria->add(DeskripsiResumeRapatRekeningPeer::ID_DESKRIPSI_RESUME_RAPAT, $vals[0], Criteria::IN);
			$criteria->add(DeskripsiResumeRapatRekeningPeer::KEGIATAN_CODE, $vals[1], Criteria::IN);
			$criteria->add(DeskripsiResumeRapatRekeningPeer::REKENING_CODE, $vals[2], Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(DeskripsiResumeRapatRekening $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(DeskripsiResumeRapatRekeningPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(DeskripsiResumeRapatRekeningPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(DeskripsiResumeRapatRekeningPeer::DATABASE_NAME, DeskripsiResumeRapatRekeningPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = DeskripsiResumeRapatRekeningPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK( $id_deskripsi_resume_rapat, $kegiatan_code, $rekening_code, $con = null) {
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$criteria = new Criteria();
		$criteria->add(DeskripsiResumeRapatRekeningPeer::ID_DESKRIPSI_RESUME_RAPAT, $id_deskripsi_resume_rapat);
		$criteria->add(DeskripsiResumeRapatRekeningPeer::KEGIATAN_CODE, $kegiatan_code);
		$criteria->add(DeskripsiResumeRapatRekeningPeer::REKENING_CODE, $rekening_code);
		$v = DeskripsiResumeRapatRekeningPeer::doSelect($criteria, $con);

		return !empty($v) ? $v[0] : null;
	}
} 
if (Propel::isInit()) {
			try {
		BaseDeskripsiResumeRapatRekeningPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/DeskripsiResumeRapatRekeningMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.DeskripsiResumeRapatRekeningMapBuilder');
}
