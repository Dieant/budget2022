<?php


abstract class BaseKomponenMemberPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.komponen_member';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.KomponenMember';

	
	const NUM_COLUMNS = 17;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const KOMPONEN_ID = 'ebudget.komponen_member.KOMPONEN_ID';

	
	const MEMBER_ID = 'ebudget.komponen_member.MEMBER_ID';

	
	const SATUAN = 'ebudget.komponen_member.SATUAN';

	
	const MEMBER_NAME = 'ebudget.komponen_member.MEMBER_NAME';

	
	const MEMBER_HARGA = 'ebudget.komponen_member.MEMBER_HARGA';

	
	const KOEFISIEN = 'ebudget.komponen_member.KOEFISIEN';

	
	const MEMBER_TOTAL = 'ebudget.komponen_member.MEMBER_TOTAL';

	
	const IP_ADDRESS = 'ebudget.komponen_member.IP_ADDRESS';

	
	const WAKTU_ACCESS = 'ebudget.komponen_member.WAKTU_ACCESS';

	
	const MEMBER_TIPE = 'ebudget.komponen_member.MEMBER_TIPE';

	
	const SUBTITLE = 'ebudget.komponen_member.SUBTITLE';

	
	const MEMBER_NO = 'ebudget.komponen_member.MEMBER_NO';

	
	const TIPE = 'ebudget.komponen_member.TIPE';

	
	const KOMPONEN_TIPE = 'ebudget.komponen_member.KOMPONEN_TIPE';

	
	const FROM_ID = 'ebudget.komponen_member.FROM_ID';

	
	const FROM_KOEF = 'ebudget.komponen_member.FROM_KOEF';

	
	const HSPK_NAME = 'ebudget.komponen_member.HSPK_NAME';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('KomponenId', 'MemberId', 'Satuan', 'MemberName', 'MemberHarga', 'Koefisien', 'MemberTotal', 'IpAddress', 'WaktuAccess', 'MemberTipe', 'Subtitle', 'MemberNo', 'Tipe', 'KomponenTipe', 'FromId', 'FromKoef', 'HspkName', ),
		BasePeer::TYPE_COLNAME => array (KomponenMemberPeer::KOMPONEN_ID, KomponenMemberPeer::MEMBER_ID, KomponenMemberPeer::SATUAN, KomponenMemberPeer::MEMBER_NAME, KomponenMemberPeer::MEMBER_HARGA, KomponenMemberPeer::KOEFISIEN, KomponenMemberPeer::MEMBER_TOTAL, KomponenMemberPeer::IP_ADDRESS, KomponenMemberPeer::WAKTU_ACCESS, KomponenMemberPeer::MEMBER_TIPE, KomponenMemberPeer::SUBTITLE, KomponenMemberPeer::MEMBER_NO, KomponenMemberPeer::TIPE, KomponenMemberPeer::KOMPONEN_TIPE, KomponenMemberPeer::FROM_ID, KomponenMemberPeer::FROM_KOEF, KomponenMemberPeer::HSPK_NAME, ),
		BasePeer::TYPE_FIELDNAME => array ('komponen_id', 'member_id', 'satuan', 'member_name', 'member_harga', 'koefisien', 'member_total', 'ip_address', 'waktu_access', 'member_tipe', 'subtitle', 'member_no', 'tipe', 'komponen_tipe', 'from_id', 'from_koef', 'hspk_name', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('KomponenId' => 0, 'MemberId' => 1, 'Satuan' => 2, 'MemberName' => 3, 'MemberHarga' => 4, 'Koefisien' => 5, 'MemberTotal' => 6, 'IpAddress' => 7, 'WaktuAccess' => 8, 'MemberTipe' => 9, 'Subtitle' => 10, 'MemberNo' => 11, 'Tipe' => 12, 'KomponenTipe' => 13, 'FromId' => 14, 'FromKoef' => 15, 'HspkName' => 16, ),
		BasePeer::TYPE_COLNAME => array (KomponenMemberPeer::KOMPONEN_ID => 0, KomponenMemberPeer::MEMBER_ID => 1, KomponenMemberPeer::SATUAN => 2, KomponenMemberPeer::MEMBER_NAME => 3, KomponenMemberPeer::MEMBER_HARGA => 4, KomponenMemberPeer::KOEFISIEN => 5, KomponenMemberPeer::MEMBER_TOTAL => 6, KomponenMemberPeer::IP_ADDRESS => 7, KomponenMemberPeer::WAKTU_ACCESS => 8, KomponenMemberPeer::MEMBER_TIPE => 9, KomponenMemberPeer::SUBTITLE => 10, KomponenMemberPeer::MEMBER_NO => 11, KomponenMemberPeer::TIPE => 12, KomponenMemberPeer::KOMPONEN_TIPE => 13, KomponenMemberPeer::FROM_ID => 14, KomponenMemberPeer::FROM_KOEF => 15, KomponenMemberPeer::HSPK_NAME => 16, ),
		BasePeer::TYPE_FIELDNAME => array ('komponen_id' => 0, 'member_id' => 1, 'satuan' => 2, 'member_name' => 3, 'member_harga' => 4, 'koefisien' => 5, 'member_total' => 6, 'ip_address' => 7, 'waktu_access' => 8, 'member_tipe' => 9, 'subtitle' => 10, 'member_no' => 11, 'tipe' => 12, 'komponen_tipe' => 13, 'from_id' => 14, 'from_koef' => 15, 'hspk_name' => 16, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/KomponenMemberMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.KomponenMemberMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = KomponenMemberPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(KomponenMemberPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(KomponenMemberPeer::KOMPONEN_ID);

		$criteria->addSelectColumn(KomponenMemberPeer::MEMBER_ID);

		$criteria->addSelectColumn(KomponenMemberPeer::SATUAN);

		$criteria->addSelectColumn(KomponenMemberPeer::MEMBER_NAME);

		$criteria->addSelectColumn(KomponenMemberPeer::MEMBER_HARGA);

		$criteria->addSelectColumn(KomponenMemberPeer::KOEFISIEN);

		$criteria->addSelectColumn(KomponenMemberPeer::MEMBER_TOTAL);

		$criteria->addSelectColumn(KomponenMemberPeer::IP_ADDRESS);

		$criteria->addSelectColumn(KomponenMemberPeer::WAKTU_ACCESS);

		$criteria->addSelectColumn(KomponenMemberPeer::MEMBER_TIPE);

		$criteria->addSelectColumn(KomponenMemberPeer::SUBTITLE);

		$criteria->addSelectColumn(KomponenMemberPeer::MEMBER_NO);

		$criteria->addSelectColumn(KomponenMemberPeer::TIPE);

		$criteria->addSelectColumn(KomponenMemberPeer::KOMPONEN_TIPE);

		$criteria->addSelectColumn(KomponenMemberPeer::FROM_ID);

		$criteria->addSelectColumn(KomponenMemberPeer::FROM_KOEF);

		$criteria->addSelectColumn(KomponenMemberPeer::HSPK_NAME);

	}

	const COUNT = 'COUNT(*)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT *)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(KomponenMemberPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(KomponenMemberPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = KomponenMemberPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = KomponenMemberPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return KomponenMemberPeer::populateObjects(KomponenMemberPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			KomponenMemberPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = KomponenMemberPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return KomponenMemberPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(KomponenMemberPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(KomponenMemberPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof KomponenMember) {

			$criteria = $values->buildCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
												if(count($values) == count($values, COUNT_RECURSIVE))
			{
								$values = array($values);
			}
			$vals = array();
			foreach($values as $value)
			{

			}

		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(KomponenMember $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(KomponenMemberPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(KomponenMemberPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(KomponenMemberPeer::DATABASE_NAME, KomponenMemberPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = KomponenMemberPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

} 
if (Propel::isInit()) {
			try {
		BaseKomponenMemberPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/KomponenMemberMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.KomponenMemberMapBuilder');
}
