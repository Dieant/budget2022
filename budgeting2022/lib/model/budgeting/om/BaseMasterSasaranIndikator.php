<?php


abstract class BaseMasterSasaranIndikator extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $kode_sasaran2;


	
	protected $kode_sasaran_indikator;


	
	protected $indikator;


	
	protected $target;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getKodeSasaran2()
	{

		return $this->kode_sasaran2;
	}

	
	public function getKodeSasaranIndikator()
	{

		return $this->kode_sasaran_indikator;
	}

	
	public function getIndikator()
	{

		return $this->indikator;
	}

	
	public function getTarget()
	{

		return $this->target;
	}

	
	public function setKodeSasaran2($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_sasaran2 !== $v) {
			$this->kode_sasaran2 = $v;
			$this->modifiedColumns[] = MasterSasaranIndikatorPeer::KODE_SASARAN2;
		}

	} 
	
	public function setKodeSasaranIndikator($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_sasaran_indikator !== $v) {
			$this->kode_sasaran_indikator = $v;
			$this->modifiedColumns[] = MasterSasaranIndikatorPeer::KODE_SASARAN_INDIKATOR;
		}

	} 
	
	public function setIndikator($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->indikator !== $v) {
			$this->indikator = $v;
			$this->modifiedColumns[] = MasterSasaranIndikatorPeer::INDIKATOR;
		}

	} 
	
	public function setTarget($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->target !== $v) {
			$this->target = $v;
			$this->modifiedColumns[] = MasterSasaranIndikatorPeer::TARGET;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->kode_sasaran2 = $rs->getString($startcol + 0);

			$this->kode_sasaran_indikator = $rs->getString($startcol + 1);

			$this->indikator = $rs->getString($startcol + 2);

			$this->target = $rs->getString($startcol + 3);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 4; 
		} catch (Exception $e) {
			throw new PropelException("Error populating MasterSasaranIndikator object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(MasterSasaranIndikatorPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			MasterSasaranIndikatorPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(MasterSasaranIndikatorPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = MasterSasaranIndikatorPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += MasterSasaranIndikatorPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = MasterSasaranIndikatorPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = MasterSasaranIndikatorPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getKodeSasaran2();
				break;
			case 1:
				return $this->getKodeSasaranIndikator();
				break;
			case 2:
				return $this->getIndikator();
				break;
			case 3:
				return $this->getTarget();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = MasterSasaranIndikatorPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getKodeSasaran2(),
			$keys[1] => $this->getKodeSasaranIndikator(),
			$keys[2] => $this->getIndikator(),
			$keys[3] => $this->getTarget(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = MasterSasaranIndikatorPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setKodeSasaran2($value);
				break;
			case 1:
				$this->setKodeSasaranIndikator($value);
				break;
			case 2:
				$this->setIndikator($value);
				break;
			case 3:
				$this->setTarget($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = MasterSasaranIndikatorPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setKodeSasaran2($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setKodeSasaranIndikator($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setIndikator($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setTarget($arr[$keys[3]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(MasterSasaranIndikatorPeer::DATABASE_NAME);

		if ($this->isColumnModified(MasterSasaranIndikatorPeer::KODE_SASARAN2)) $criteria->add(MasterSasaranIndikatorPeer::KODE_SASARAN2, $this->kode_sasaran2);
		if ($this->isColumnModified(MasterSasaranIndikatorPeer::KODE_SASARAN_INDIKATOR)) $criteria->add(MasterSasaranIndikatorPeer::KODE_SASARAN_INDIKATOR, $this->kode_sasaran_indikator);
		if ($this->isColumnModified(MasterSasaranIndikatorPeer::INDIKATOR)) $criteria->add(MasterSasaranIndikatorPeer::INDIKATOR, $this->indikator);
		if ($this->isColumnModified(MasterSasaranIndikatorPeer::TARGET)) $criteria->add(MasterSasaranIndikatorPeer::TARGET, $this->target);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(MasterSasaranIndikatorPeer::DATABASE_NAME);

		$criteria->add(MasterSasaranIndikatorPeer::KODE_SASARAN2, $this->kode_sasaran2);
		$criteria->add(MasterSasaranIndikatorPeer::KODE_SASARAN_INDIKATOR, $this->kode_sasaran_indikator);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		$pks = array();

		$pks[0] = $this->getKodeSasaran2();

		$pks[1] = $this->getKodeSasaranIndikator();

		return $pks;
	}

	
	public function setPrimaryKey($keys)
	{

		$this->setKodeSasaran2($keys[0]);

		$this->setKodeSasaranIndikator($keys[1]);

	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setIndikator($this->indikator);

		$copyObj->setTarget($this->target);


		$copyObj->setNew(true);

		$copyObj->setKodeSasaran2(NULL); 
		$copyObj->setKodeSasaranIndikator(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new MasterSasaranIndikatorPeer();
		}
		return self::$peer;
	}

} 