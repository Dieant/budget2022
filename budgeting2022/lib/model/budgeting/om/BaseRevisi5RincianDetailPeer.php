<?php


abstract class BaseRevisi5RincianDetailPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.revisi5_rincian_detail';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.Revisi5RincianDetail';

	
	const NUM_COLUMNS = 36;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const KEGIATAN_CODE = 'ebudget.revisi5_rincian_detail.KEGIATAN_CODE';

	
	const TIPE = 'ebudget.revisi5_rincian_detail.TIPE';

	
	const DETAIL_NO = 'ebudget.revisi5_rincian_detail.DETAIL_NO';

	
	const REKENING_CODE = 'ebudget.revisi5_rincian_detail.REKENING_CODE';

	
	const KOMPONEN_ID = 'ebudget.revisi5_rincian_detail.KOMPONEN_ID';

	
	const DETAIL_NAME = 'ebudget.revisi5_rincian_detail.DETAIL_NAME';

	
	const VOLUME = 'ebudget.revisi5_rincian_detail.VOLUME';

	
	const KETERANGAN_KOEFISIEN = 'ebudget.revisi5_rincian_detail.KETERANGAN_KOEFISIEN';

	
	const SUBTITLE = 'ebudget.revisi5_rincian_detail.SUBTITLE';

	
	const KOMPONEN_HARGA = 'ebudget.revisi5_rincian_detail.KOMPONEN_HARGA';

	
	const KOMPONEN_HARGA_AWAL = 'ebudget.revisi5_rincian_detail.KOMPONEN_HARGA_AWAL';

	
	const KOMPONEN_NAME = 'ebudget.revisi5_rincian_detail.KOMPONEN_NAME';

	
	const SATUAN = 'ebudget.revisi5_rincian_detail.SATUAN';

	
	const PAJAK = 'ebudget.revisi5_rincian_detail.PAJAK';

	
	const UNIT_ID = 'ebudget.revisi5_rincian_detail.UNIT_ID';

	
	const FROM_SUB_KEGIATAN = 'ebudget.revisi5_rincian_detail.FROM_SUB_KEGIATAN';

	
	const SUB = 'ebudget.revisi5_rincian_detail.SUB';

	
	const KODE_SUB = 'ebudget.revisi5_rincian_detail.KODE_SUB';

	
	const LAST_UPDATE_USER = 'ebudget.revisi5_rincian_detail.LAST_UPDATE_USER';

	
	const LAST_UPDATE_TIME = 'ebudget.revisi5_rincian_detail.LAST_UPDATE_TIME';

	
	const LAST_UPDATE_IP = 'ebudget.revisi5_rincian_detail.LAST_UPDATE_IP';

	
	const TAHAP = 'ebudget.revisi5_rincian_detail.TAHAP';

	
	const TAHAP_EDIT = 'ebudget.revisi5_rincian_detail.TAHAP_EDIT';

	
	const TAHAP_NEW = 'ebudget.revisi5_rincian_detail.TAHAP_NEW';

	
	const STATUS_LELANG = 'ebudget.revisi5_rincian_detail.STATUS_LELANG';

	
	const NOMOR_LELANG = 'ebudget.revisi5_rincian_detail.NOMOR_LELANG';

	
	const KOEFISIEN_SEMULA = 'ebudget.revisi5_rincian_detail.KOEFISIEN_SEMULA';

	
	const VOLUME_SEMULA = 'ebudget.revisi5_rincian_detail.VOLUME_SEMULA';

	
	const HARGA_SEMULA = 'ebudget.revisi5_rincian_detail.HARGA_SEMULA';

	
	const TOTAL_SEMULA = 'ebudget.revisi5_rincian_detail.TOTAL_SEMULA';

	
	const LOCK_SUBTITLE = 'ebudget.revisi5_rincian_detail.LOCK_SUBTITLE';

	
	const STATUS_HAPUS = 'ebudget.revisi5_rincian_detail.STATUS_HAPUS';

	
	const TAHUN = 'ebudget.revisi5_rincian_detail.TAHUN';

	
	const KODE_LOKASI = 'ebudget.revisi5_rincian_detail.KODE_LOKASI';

	
	const KECAMATAN = 'ebudget.revisi5_rincian_detail.KECAMATAN';

	
	const REKENING_CODE_ASLI = 'ebudget.revisi5_rincian_detail.REKENING_CODE_ASLI';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('KegiatanCode', 'Tipe', 'DetailNo', 'RekeningCode', 'KomponenId', 'DetailName', 'Volume', 'KeteranganKoefisien', 'Subtitle', 'KomponenHarga', 'KomponenHargaAwal', 'KomponenName', 'Satuan', 'Pajak', 'UnitId', 'FromSubKegiatan', 'Sub', 'KodeSub', 'LastUpdateUser', 'LastUpdateTime', 'LastUpdateIp', 'Tahap', 'TahapEdit', 'TahapNew', 'StatusLelang', 'NomorLelang', 'KoefisienSemula', 'VolumeSemula', 'HargaSemula', 'TotalSemula', 'LockSubtitle', 'StatusHapus', 'Tahun', 'KodeLokasi', 'Kecamatan', 'RekeningCodeAsli', ),
		BasePeer::TYPE_COLNAME => array (Revisi5RincianDetailPeer::KEGIATAN_CODE, Revisi5RincianDetailPeer::TIPE, Revisi5RincianDetailPeer::DETAIL_NO, Revisi5RincianDetailPeer::REKENING_CODE, Revisi5RincianDetailPeer::KOMPONEN_ID, Revisi5RincianDetailPeer::DETAIL_NAME, Revisi5RincianDetailPeer::VOLUME, Revisi5RincianDetailPeer::KETERANGAN_KOEFISIEN, Revisi5RincianDetailPeer::SUBTITLE, Revisi5RincianDetailPeer::KOMPONEN_HARGA, Revisi5RincianDetailPeer::KOMPONEN_HARGA_AWAL, Revisi5RincianDetailPeer::KOMPONEN_NAME, Revisi5RincianDetailPeer::SATUAN, Revisi5RincianDetailPeer::PAJAK, Revisi5RincianDetailPeer::UNIT_ID, Revisi5RincianDetailPeer::FROM_SUB_KEGIATAN, Revisi5RincianDetailPeer::SUB, Revisi5RincianDetailPeer::KODE_SUB, Revisi5RincianDetailPeer::LAST_UPDATE_USER, Revisi5RincianDetailPeer::LAST_UPDATE_TIME, Revisi5RincianDetailPeer::LAST_UPDATE_IP, Revisi5RincianDetailPeer::TAHAP, Revisi5RincianDetailPeer::TAHAP_EDIT, Revisi5RincianDetailPeer::TAHAP_NEW, Revisi5RincianDetailPeer::STATUS_LELANG, Revisi5RincianDetailPeer::NOMOR_LELANG, Revisi5RincianDetailPeer::KOEFISIEN_SEMULA, Revisi5RincianDetailPeer::VOLUME_SEMULA, Revisi5RincianDetailPeer::HARGA_SEMULA, Revisi5RincianDetailPeer::TOTAL_SEMULA, Revisi5RincianDetailPeer::LOCK_SUBTITLE, Revisi5RincianDetailPeer::STATUS_HAPUS, Revisi5RincianDetailPeer::TAHUN, Revisi5RincianDetailPeer::KODE_LOKASI, Revisi5RincianDetailPeer::KECAMATAN, Revisi5RincianDetailPeer::REKENING_CODE_ASLI, ),
		BasePeer::TYPE_FIELDNAME => array ('kegiatan_code', 'tipe', 'detail_no', 'rekening_code', 'komponen_id', 'detail_name', 'volume', 'keterangan_koefisien', 'subtitle', 'komponen_harga', 'komponen_harga_awal', 'komponen_name', 'satuan', 'pajak', 'unit_id', 'from_sub_kegiatan', 'sub', 'kode_sub', 'last_update_user', 'last_update_time', 'last_update_ip', 'tahap', 'tahap_edit', 'tahap_new', 'status_lelang', 'nomor_lelang', 'koefisien_semula', 'volume_semula', 'harga_semula', 'total_semula', 'lock_subtitle', 'status_hapus', 'tahun', 'kode_lokasi', 'kecamatan', 'rekening_code_asli', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('KegiatanCode' => 0, 'Tipe' => 1, 'DetailNo' => 2, 'RekeningCode' => 3, 'KomponenId' => 4, 'DetailName' => 5, 'Volume' => 6, 'KeteranganKoefisien' => 7, 'Subtitle' => 8, 'KomponenHarga' => 9, 'KomponenHargaAwal' => 10, 'KomponenName' => 11, 'Satuan' => 12, 'Pajak' => 13, 'UnitId' => 14, 'FromSubKegiatan' => 15, 'Sub' => 16, 'KodeSub' => 17, 'LastUpdateUser' => 18, 'LastUpdateTime' => 19, 'LastUpdateIp' => 20, 'Tahap' => 21, 'TahapEdit' => 22, 'TahapNew' => 23, 'StatusLelang' => 24, 'NomorLelang' => 25, 'KoefisienSemula' => 26, 'VolumeSemula' => 27, 'HargaSemula' => 28, 'TotalSemula' => 29, 'LockSubtitle' => 30, 'StatusHapus' => 31, 'Tahun' => 32, 'KodeLokasi' => 33, 'Kecamatan' => 34, 'RekeningCodeAsli' => 35, ),
		BasePeer::TYPE_COLNAME => array (Revisi5RincianDetailPeer::KEGIATAN_CODE => 0, Revisi5RincianDetailPeer::TIPE => 1, Revisi5RincianDetailPeer::DETAIL_NO => 2, Revisi5RincianDetailPeer::REKENING_CODE => 3, Revisi5RincianDetailPeer::KOMPONEN_ID => 4, Revisi5RincianDetailPeer::DETAIL_NAME => 5, Revisi5RincianDetailPeer::VOLUME => 6, Revisi5RincianDetailPeer::KETERANGAN_KOEFISIEN => 7, Revisi5RincianDetailPeer::SUBTITLE => 8, Revisi5RincianDetailPeer::KOMPONEN_HARGA => 9, Revisi5RincianDetailPeer::KOMPONEN_HARGA_AWAL => 10, Revisi5RincianDetailPeer::KOMPONEN_NAME => 11, Revisi5RincianDetailPeer::SATUAN => 12, Revisi5RincianDetailPeer::PAJAK => 13, Revisi5RincianDetailPeer::UNIT_ID => 14, Revisi5RincianDetailPeer::FROM_SUB_KEGIATAN => 15, Revisi5RincianDetailPeer::SUB => 16, Revisi5RincianDetailPeer::KODE_SUB => 17, Revisi5RincianDetailPeer::LAST_UPDATE_USER => 18, Revisi5RincianDetailPeer::LAST_UPDATE_TIME => 19, Revisi5RincianDetailPeer::LAST_UPDATE_IP => 20, Revisi5RincianDetailPeer::TAHAP => 21, Revisi5RincianDetailPeer::TAHAP_EDIT => 22, Revisi5RincianDetailPeer::TAHAP_NEW => 23, Revisi5RincianDetailPeer::STATUS_LELANG => 24, Revisi5RincianDetailPeer::NOMOR_LELANG => 25, Revisi5RincianDetailPeer::KOEFISIEN_SEMULA => 26, Revisi5RincianDetailPeer::VOLUME_SEMULA => 27, Revisi5RincianDetailPeer::HARGA_SEMULA => 28, Revisi5RincianDetailPeer::TOTAL_SEMULA => 29, Revisi5RincianDetailPeer::LOCK_SUBTITLE => 30, Revisi5RincianDetailPeer::STATUS_HAPUS => 31, Revisi5RincianDetailPeer::TAHUN => 32, Revisi5RincianDetailPeer::KODE_LOKASI => 33, Revisi5RincianDetailPeer::KECAMATAN => 34, Revisi5RincianDetailPeer::REKENING_CODE_ASLI => 35, ),
		BasePeer::TYPE_FIELDNAME => array ('kegiatan_code' => 0, 'tipe' => 1, 'detail_no' => 2, 'rekening_code' => 3, 'komponen_id' => 4, 'detail_name' => 5, 'volume' => 6, 'keterangan_koefisien' => 7, 'subtitle' => 8, 'komponen_harga' => 9, 'komponen_harga_awal' => 10, 'komponen_name' => 11, 'satuan' => 12, 'pajak' => 13, 'unit_id' => 14, 'from_sub_kegiatan' => 15, 'sub' => 16, 'kode_sub' => 17, 'last_update_user' => 18, 'last_update_time' => 19, 'last_update_ip' => 20, 'tahap' => 21, 'tahap_edit' => 22, 'tahap_new' => 23, 'status_lelang' => 24, 'nomor_lelang' => 25, 'koefisien_semula' => 26, 'volume_semula' => 27, 'harga_semula' => 28, 'total_semula' => 29, 'lock_subtitle' => 30, 'status_hapus' => 31, 'tahun' => 32, 'kode_lokasi' => 33, 'kecamatan' => 34, 'rekening_code_asli' => 35, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/Revisi5RincianDetailMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.Revisi5RincianDetailMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = Revisi5RincianDetailPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(Revisi5RincianDetailPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(Revisi5RincianDetailPeer::KEGIATAN_CODE);

		$criteria->addSelectColumn(Revisi5RincianDetailPeer::TIPE);

		$criteria->addSelectColumn(Revisi5RincianDetailPeer::DETAIL_NO);

		$criteria->addSelectColumn(Revisi5RincianDetailPeer::REKENING_CODE);

		$criteria->addSelectColumn(Revisi5RincianDetailPeer::KOMPONEN_ID);

		$criteria->addSelectColumn(Revisi5RincianDetailPeer::DETAIL_NAME);

		$criteria->addSelectColumn(Revisi5RincianDetailPeer::VOLUME);

		$criteria->addSelectColumn(Revisi5RincianDetailPeer::KETERANGAN_KOEFISIEN);

		$criteria->addSelectColumn(Revisi5RincianDetailPeer::SUBTITLE);

		$criteria->addSelectColumn(Revisi5RincianDetailPeer::KOMPONEN_HARGA);

		$criteria->addSelectColumn(Revisi5RincianDetailPeer::KOMPONEN_HARGA_AWAL);

		$criteria->addSelectColumn(Revisi5RincianDetailPeer::KOMPONEN_NAME);

		$criteria->addSelectColumn(Revisi5RincianDetailPeer::SATUAN);

		$criteria->addSelectColumn(Revisi5RincianDetailPeer::PAJAK);

		$criteria->addSelectColumn(Revisi5RincianDetailPeer::UNIT_ID);

		$criteria->addSelectColumn(Revisi5RincianDetailPeer::FROM_SUB_KEGIATAN);

		$criteria->addSelectColumn(Revisi5RincianDetailPeer::SUB);

		$criteria->addSelectColumn(Revisi5RincianDetailPeer::KODE_SUB);

		$criteria->addSelectColumn(Revisi5RincianDetailPeer::LAST_UPDATE_USER);

		$criteria->addSelectColumn(Revisi5RincianDetailPeer::LAST_UPDATE_TIME);

		$criteria->addSelectColumn(Revisi5RincianDetailPeer::LAST_UPDATE_IP);

		$criteria->addSelectColumn(Revisi5RincianDetailPeer::TAHAP);

		$criteria->addSelectColumn(Revisi5RincianDetailPeer::TAHAP_EDIT);

		$criteria->addSelectColumn(Revisi5RincianDetailPeer::TAHAP_NEW);

		$criteria->addSelectColumn(Revisi5RincianDetailPeer::STATUS_LELANG);

		$criteria->addSelectColumn(Revisi5RincianDetailPeer::NOMOR_LELANG);

		$criteria->addSelectColumn(Revisi5RincianDetailPeer::KOEFISIEN_SEMULA);

		$criteria->addSelectColumn(Revisi5RincianDetailPeer::VOLUME_SEMULA);

		$criteria->addSelectColumn(Revisi5RincianDetailPeer::HARGA_SEMULA);

		$criteria->addSelectColumn(Revisi5RincianDetailPeer::TOTAL_SEMULA);

		$criteria->addSelectColumn(Revisi5RincianDetailPeer::LOCK_SUBTITLE);

		$criteria->addSelectColumn(Revisi5RincianDetailPeer::STATUS_HAPUS);

		$criteria->addSelectColumn(Revisi5RincianDetailPeer::TAHUN);

		$criteria->addSelectColumn(Revisi5RincianDetailPeer::KODE_LOKASI);

		$criteria->addSelectColumn(Revisi5RincianDetailPeer::KECAMATAN);

		$criteria->addSelectColumn(Revisi5RincianDetailPeer::REKENING_CODE_ASLI);

	}

	const COUNT = 'COUNT(ebudget.revisi5_rincian_detail.KEGIATAN_CODE)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.revisi5_rincian_detail.KEGIATAN_CODE)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(Revisi5RincianDetailPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(Revisi5RincianDetailPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = Revisi5RincianDetailPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = Revisi5RincianDetailPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return Revisi5RincianDetailPeer::populateObjects(Revisi5RincianDetailPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			Revisi5RincianDetailPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = Revisi5RincianDetailPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return Revisi5RincianDetailPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(Revisi5RincianDetailPeer::KEGIATAN_CODE);
			$selectCriteria->add(Revisi5RincianDetailPeer::KEGIATAN_CODE, $criteria->remove(Revisi5RincianDetailPeer::KEGIATAN_CODE), $comparison);

			$comparison = $criteria->getComparison(Revisi5RincianDetailPeer::TIPE);
			$selectCriteria->add(Revisi5RincianDetailPeer::TIPE, $criteria->remove(Revisi5RincianDetailPeer::TIPE), $comparison);

			$comparison = $criteria->getComparison(Revisi5RincianDetailPeer::DETAIL_NO);
			$selectCriteria->add(Revisi5RincianDetailPeer::DETAIL_NO, $criteria->remove(Revisi5RincianDetailPeer::DETAIL_NO), $comparison);

			$comparison = $criteria->getComparison(Revisi5RincianDetailPeer::UNIT_ID);
			$selectCriteria->add(Revisi5RincianDetailPeer::UNIT_ID, $criteria->remove(Revisi5RincianDetailPeer::UNIT_ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(Revisi5RincianDetailPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(Revisi5RincianDetailPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof Revisi5RincianDetail) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
												if(count($values) == count($values, COUNT_RECURSIVE))
			{
								$values = array($values);
			}
			$vals = array();
			foreach($values as $value)
			{

				$vals[0][] = $value[0];
				$vals[1][] = $value[1];
				$vals[2][] = $value[2];
				$vals[3][] = $value[3];
			}

			$criteria->add(Revisi5RincianDetailPeer::KEGIATAN_CODE, $vals[0], Criteria::IN);
			$criteria->add(Revisi5RincianDetailPeer::TIPE, $vals[1], Criteria::IN);
			$criteria->add(Revisi5RincianDetailPeer::DETAIL_NO, $vals[2], Criteria::IN);
			$criteria->add(Revisi5RincianDetailPeer::UNIT_ID, $vals[3], Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(Revisi5RincianDetail $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(Revisi5RincianDetailPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(Revisi5RincianDetailPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(Revisi5RincianDetailPeer::DATABASE_NAME, Revisi5RincianDetailPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = Revisi5RincianDetailPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK( $kegiatan_code, $tipe, $detail_no, $unit_id, $con = null) {
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$criteria = new Criteria();
		$criteria->add(Revisi5RincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
		$criteria->add(Revisi5RincianDetailPeer::TIPE, $tipe);
		$criteria->add(Revisi5RincianDetailPeer::DETAIL_NO, $detail_no);
		$criteria->add(Revisi5RincianDetailPeer::UNIT_ID, $unit_id);
		$v = Revisi5RincianDetailPeer::doSelect($criteria, $con);

		return !empty($v) ? $v[0] : null;
	}
} 
if (Propel::isInit()) {
			try {
		BaseRevisi5RincianDetailPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/Revisi5RincianDetailMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.Revisi5RincianDetailMapBuilder');
}
