<?php


abstract class BaseHistoryUser extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id;


	
	protected $username;


	
	protected $ip;


	
	protected $time_act;


	
	protected $unit_id;


	
	protected $kegiatan_code;


	
	protected $detail_no;


	
	protected $description;


	
	protected $aksi;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getId()
	{

		return $this->id;
	}

	
	public function getUsername()
	{

		return $this->username;
	}

	
	public function getIp()
	{

		return $this->ip;
	}

	
	public function getTimeAct($format = 'Y-m-d H:i:s')
	{

		if ($this->time_act === null || $this->time_act === '') {
			return null;
		} elseif (!is_int($this->time_act)) {
						$ts = strtotime($this->time_act);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [time_act] as date/time value: " . var_export($this->time_act, true));
			}
		} else {
			$ts = $this->time_act;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getUnitId()
	{

		return $this->unit_id;
	}

	
	public function getKegiatanCode()
	{

		return $this->kegiatan_code;
	}

	
	public function getDetailNo()
	{

		return $this->detail_no;
	}

	
	public function getDescription()
	{

		return $this->description;
	}

	
	public function getAksi()
	{

		return $this->aksi;
	}

	
	public function setId($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = HistoryUserPeer::ID;
		}

	} 
	
	public function setUsername($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->username !== $v) {
			$this->username = $v;
			$this->modifiedColumns[] = HistoryUserPeer::USERNAME;
		}

	} 
	
	public function setIp($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->ip !== $v) {
			$this->ip = $v;
			$this->modifiedColumns[] = HistoryUserPeer::IP;
		}

	} 
	
	public function setTimeAct($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [time_act] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->time_act !== $ts) {
			$this->time_act = $ts;
			$this->modifiedColumns[] = HistoryUserPeer::TIME_ACT;
		}

	} 
	
	public function setUnitId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->unit_id !== $v) {
			$this->unit_id = $v;
			$this->modifiedColumns[] = HistoryUserPeer::UNIT_ID;
		}

	} 
	
	public function setKegiatanCode($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kegiatan_code !== $v) {
			$this->kegiatan_code = $v;
			$this->modifiedColumns[] = HistoryUserPeer::KEGIATAN_CODE;
		}

	} 
	
	public function setDetailNo($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->detail_no !== $v) {
			$this->detail_no = $v;
			$this->modifiedColumns[] = HistoryUserPeer::DETAIL_NO;
		}

	} 
	
	public function setDescription($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->description !== $v) {
			$this->description = $v;
			$this->modifiedColumns[] = HistoryUserPeer::DESCRIPTION;
		}

	} 
	
	public function setAksi($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->aksi !== $v) {
			$this->aksi = $v;
			$this->modifiedColumns[] = HistoryUserPeer::AKSI;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getInt($startcol + 0);

			$this->username = $rs->getString($startcol + 1);

			$this->ip = $rs->getString($startcol + 2);

			$this->time_act = $rs->getTimestamp($startcol + 3, null);

			$this->unit_id = $rs->getString($startcol + 4);

			$this->kegiatan_code = $rs->getString($startcol + 5);

			$this->detail_no = $rs->getString($startcol + 6);

			$this->description = $rs->getString($startcol + 7);

			$this->aksi = $rs->getString($startcol + 8);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 9; 
		} catch (Exception $e) {
			throw new PropelException("Error populating HistoryUser object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(HistoryUserPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			HistoryUserPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(HistoryUserPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = HistoryUserPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += HistoryUserPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = HistoryUserPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = HistoryUserPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getUsername();
				break;
			case 2:
				return $this->getIp();
				break;
			case 3:
				return $this->getTimeAct();
				break;
			case 4:
				return $this->getUnitId();
				break;
			case 5:
				return $this->getKegiatanCode();
				break;
			case 6:
				return $this->getDetailNo();
				break;
			case 7:
				return $this->getDescription();
				break;
			case 8:
				return $this->getAksi();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = HistoryUserPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getUsername(),
			$keys[2] => $this->getIp(),
			$keys[3] => $this->getTimeAct(),
			$keys[4] => $this->getUnitId(),
			$keys[5] => $this->getKegiatanCode(),
			$keys[6] => $this->getDetailNo(),
			$keys[7] => $this->getDescription(),
			$keys[8] => $this->getAksi(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = HistoryUserPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setUsername($value);
				break;
			case 2:
				$this->setIp($value);
				break;
			case 3:
				$this->setTimeAct($value);
				break;
			case 4:
				$this->setUnitId($value);
				break;
			case 5:
				$this->setKegiatanCode($value);
				break;
			case 6:
				$this->setDetailNo($value);
				break;
			case 7:
				$this->setDescription($value);
				break;
			case 8:
				$this->setAksi($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = HistoryUserPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setUsername($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setIp($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setTimeAct($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setUnitId($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setKegiatanCode($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setDetailNo($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setDescription($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setAksi($arr[$keys[8]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(HistoryUserPeer::DATABASE_NAME);

		if ($this->isColumnModified(HistoryUserPeer::ID)) $criteria->add(HistoryUserPeer::ID, $this->id);
		if ($this->isColumnModified(HistoryUserPeer::USERNAME)) $criteria->add(HistoryUserPeer::USERNAME, $this->username);
		if ($this->isColumnModified(HistoryUserPeer::IP)) $criteria->add(HistoryUserPeer::IP, $this->ip);
		if ($this->isColumnModified(HistoryUserPeer::TIME_ACT)) $criteria->add(HistoryUserPeer::TIME_ACT, $this->time_act);
		if ($this->isColumnModified(HistoryUserPeer::UNIT_ID)) $criteria->add(HistoryUserPeer::UNIT_ID, $this->unit_id);
		if ($this->isColumnModified(HistoryUserPeer::KEGIATAN_CODE)) $criteria->add(HistoryUserPeer::KEGIATAN_CODE, $this->kegiatan_code);
		if ($this->isColumnModified(HistoryUserPeer::DETAIL_NO)) $criteria->add(HistoryUserPeer::DETAIL_NO, $this->detail_no);
		if ($this->isColumnModified(HistoryUserPeer::DESCRIPTION)) $criteria->add(HistoryUserPeer::DESCRIPTION, $this->description);
		if ($this->isColumnModified(HistoryUserPeer::AKSI)) $criteria->add(HistoryUserPeer::AKSI, $this->aksi);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(HistoryUserPeer::DATABASE_NAME);

		$criteria->add(HistoryUserPeer::ID, $this->id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setUsername($this->username);

		$copyObj->setIp($this->ip);

		$copyObj->setTimeAct($this->time_act);

		$copyObj->setUnitId($this->unit_id);

		$copyObj->setKegiatanCode($this->kegiatan_code);

		$copyObj->setDetailNo($this->detail_no);

		$copyObj->setDescription($this->description);

		$copyObj->setAksi($this->aksi);


		$copyObj->setNew(true);

		$copyObj->setId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new HistoryUserPeer();
		}
		return self::$peer;
	}

} 