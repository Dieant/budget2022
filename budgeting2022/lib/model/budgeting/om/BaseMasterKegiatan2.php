<?php


abstract class BaseMasterKegiatan2 extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $kode_program2;


	
	protected $kode_kegiatan2;


	
	protected $nama_kegiatan2;


	
	protected $urut;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getKodeProgram2()
	{

		return $this->kode_program2;
	}

	
	public function getKodeKegiatan2()
	{

		return $this->kode_kegiatan2;
	}

	
	public function getNamaKegiatan2()
	{

		return $this->nama_kegiatan2;
	}

	
	public function getUrut()
	{

		return $this->urut;
	}

	
	public function setKodeProgram2($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_program2 !== $v) {
			$this->kode_program2 = $v;
			$this->modifiedColumns[] = MasterKegiatan2Peer::KODE_PROGRAM2;
		}

	} 
	
	public function setKodeKegiatan2($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_kegiatan2 !== $v) {
			$this->kode_kegiatan2 = $v;
			$this->modifiedColumns[] = MasterKegiatan2Peer::KODE_KEGIATAN2;
		}

	} 
	
	public function setNamaKegiatan2($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nama_kegiatan2 !== $v) {
			$this->nama_kegiatan2 = $v;
			$this->modifiedColumns[] = MasterKegiatan2Peer::NAMA_KEGIATAN2;
		}

	} 
	
	public function setUrut($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->urut !== $v) {
			$this->urut = $v;
			$this->modifiedColumns[] = MasterKegiatan2Peer::URUT;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->kode_program2 = $rs->getString($startcol + 0);

			$this->kode_kegiatan2 = $rs->getString($startcol + 1);

			$this->nama_kegiatan2 = $rs->getString($startcol + 2);

			$this->urut = $rs->getInt($startcol + 3);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 4; 
		} catch (Exception $e) {
			throw new PropelException("Error populating MasterKegiatan2 object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(MasterKegiatan2Peer::DATABASE_NAME);
		}

		try {
			$con->begin();
			MasterKegiatan2Peer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(MasterKegiatan2Peer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = MasterKegiatan2Peer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += MasterKegiatan2Peer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = MasterKegiatan2Peer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = MasterKegiatan2Peer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getKodeProgram2();
				break;
			case 1:
				return $this->getKodeKegiatan2();
				break;
			case 2:
				return $this->getNamaKegiatan2();
				break;
			case 3:
				return $this->getUrut();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = MasterKegiatan2Peer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getKodeProgram2(),
			$keys[1] => $this->getKodeKegiatan2(),
			$keys[2] => $this->getNamaKegiatan2(),
			$keys[3] => $this->getUrut(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = MasterKegiatan2Peer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setKodeProgram2($value);
				break;
			case 1:
				$this->setKodeKegiatan2($value);
				break;
			case 2:
				$this->setNamaKegiatan2($value);
				break;
			case 3:
				$this->setUrut($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = MasterKegiatan2Peer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setKodeProgram2($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setKodeKegiatan2($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setNamaKegiatan2($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setUrut($arr[$keys[3]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(MasterKegiatan2Peer::DATABASE_NAME);

		if ($this->isColumnModified(MasterKegiatan2Peer::KODE_PROGRAM2)) $criteria->add(MasterKegiatan2Peer::KODE_PROGRAM2, $this->kode_program2);
		if ($this->isColumnModified(MasterKegiatan2Peer::KODE_KEGIATAN2)) $criteria->add(MasterKegiatan2Peer::KODE_KEGIATAN2, $this->kode_kegiatan2);
		if ($this->isColumnModified(MasterKegiatan2Peer::NAMA_KEGIATAN2)) $criteria->add(MasterKegiatan2Peer::NAMA_KEGIATAN2, $this->nama_kegiatan2);
		if ($this->isColumnModified(MasterKegiatan2Peer::URUT)) $criteria->add(MasterKegiatan2Peer::URUT, $this->urut);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(MasterKegiatan2Peer::DATABASE_NAME);

		$criteria->add(MasterKegiatan2Peer::KODE_PROGRAM2, $this->kode_program2);
		$criteria->add(MasterKegiatan2Peer::KODE_KEGIATAN2, $this->kode_kegiatan2);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		$pks = array();

		$pks[0] = $this->getKodeProgram2();

		$pks[1] = $this->getKodeKegiatan2();

		return $pks;
	}

	
	public function setPrimaryKey($keys)
	{

		$this->setKodeProgram2($keys[0]);

		$this->setKodeKegiatan2($keys[1]);

	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setNamaKegiatan2($this->nama_kegiatan2);

		$copyObj->setUrut($this->urut);


		$copyObj->setNew(true);

		$copyObj->setKodeProgram2(NULL); 
		$copyObj->setKodeKegiatan2(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new MasterKegiatan2Peer();
		}
		return self::$peer;
	}

} 