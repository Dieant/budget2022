<?php


abstract class BaseMasterIndikator2 extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $kode_program2;


	
	protected $pk_indikator;


	
	protected $indikator;


	
	protected $target;


	
	protected $kode_program_indikator;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getKodeProgram2()
	{

		return $this->kode_program2;
	}

	
	public function getPkIndikator()
	{

		return $this->pk_indikator;
	}

	
	public function getIndikator()
	{

		return $this->indikator;
	}

	
	public function getTarget()
	{

		return $this->target;
	}

	
	public function getKodeProgramIndikator()
	{

		return $this->kode_program_indikator;
	}

	
	public function setKodeProgram2($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_program2 !== $v) {
			$this->kode_program2 = $v;
			$this->modifiedColumns[] = MasterIndikator2Peer::KODE_PROGRAM2;
		}

	} 
	
	public function setPkIndikator($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->pk_indikator !== $v) {
			$this->pk_indikator = $v;
			$this->modifiedColumns[] = MasterIndikator2Peer::PK_INDIKATOR;
		}

	} 
	
	public function setIndikator($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->indikator !== $v) {
			$this->indikator = $v;
			$this->modifiedColumns[] = MasterIndikator2Peer::INDIKATOR;
		}

	} 
	
	public function setTarget($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->target !== $v) {
			$this->target = $v;
			$this->modifiedColumns[] = MasterIndikator2Peer::TARGET;
		}

	} 
	
	public function setKodeProgramIndikator($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_program_indikator !== $v) {
			$this->kode_program_indikator = $v;
			$this->modifiedColumns[] = MasterIndikator2Peer::KODE_PROGRAM_INDIKATOR;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->kode_program2 = $rs->getString($startcol + 0);

			$this->pk_indikator = $rs->getInt($startcol + 1);

			$this->indikator = $rs->getString($startcol + 2);

			$this->target = $rs->getString($startcol + 3);

			$this->kode_program_indikator = $rs->getString($startcol + 4);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 5; 
		} catch (Exception $e) {
			throw new PropelException("Error populating MasterIndikator2 object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(MasterIndikator2Peer::DATABASE_NAME);
		}

		try {
			$con->begin();
			MasterIndikator2Peer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(MasterIndikator2Peer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = MasterIndikator2Peer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setPkIndikator($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += MasterIndikator2Peer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = MasterIndikator2Peer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = MasterIndikator2Peer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getKodeProgram2();
				break;
			case 1:
				return $this->getPkIndikator();
				break;
			case 2:
				return $this->getIndikator();
				break;
			case 3:
				return $this->getTarget();
				break;
			case 4:
				return $this->getKodeProgramIndikator();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = MasterIndikator2Peer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getKodeProgram2(),
			$keys[1] => $this->getPkIndikator(),
			$keys[2] => $this->getIndikator(),
			$keys[3] => $this->getTarget(),
			$keys[4] => $this->getKodeProgramIndikator(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = MasterIndikator2Peer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setKodeProgram2($value);
				break;
			case 1:
				$this->setPkIndikator($value);
				break;
			case 2:
				$this->setIndikator($value);
				break;
			case 3:
				$this->setTarget($value);
				break;
			case 4:
				$this->setKodeProgramIndikator($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = MasterIndikator2Peer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setKodeProgram2($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setPkIndikator($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setIndikator($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setTarget($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setKodeProgramIndikator($arr[$keys[4]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(MasterIndikator2Peer::DATABASE_NAME);

		if ($this->isColumnModified(MasterIndikator2Peer::KODE_PROGRAM2)) $criteria->add(MasterIndikator2Peer::KODE_PROGRAM2, $this->kode_program2);
		if ($this->isColumnModified(MasterIndikator2Peer::PK_INDIKATOR)) $criteria->add(MasterIndikator2Peer::PK_INDIKATOR, $this->pk_indikator);
		if ($this->isColumnModified(MasterIndikator2Peer::INDIKATOR)) $criteria->add(MasterIndikator2Peer::INDIKATOR, $this->indikator);
		if ($this->isColumnModified(MasterIndikator2Peer::TARGET)) $criteria->add(MasterIndikator2Peer::TARGET, $this->target);
		if ($this->isColumnModified(MasterIndikator2Peer::KODE_PROGRAM_INDIKATOR)) $criteria->add(MasterIndikator2Peer::KODE_PROGRAM_INDIKATOR, $this->kode_program_indikator);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(MasterIndikator2Peer::DATABASE_NAME);

		$criteria->add(MasterIndikator2Peer::KODE_PROGRAM2, $this->kode_program2);
		$criteria->add(MasterIndikator2Peer::PK_INDIKATOR, $this->pk_indikator);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		$pks = array();

		$pks[0] = $this->getKodeProgram2();

		$pks[1] = $this->getPkIndikator();

		return $pks;
	}

	
	public function setPrimaryKey($keys)
	{

		$this->setKodeProgram2($keys[0]);

		$this->setPkIndikator($keys[1]);

	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setIndikator($this->indikator);

		$copyObj->setTarget($this->target);

		$copyObj->setKodeProgramIndikator($this->kode_program_indikator);


		$copyObj->setNew(true);

		$copyObj->setKodeProgram2(NULL); 
		$copyObj->setPkIndikator(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new MasterIndikator2Peer();
		}
		return self::$peer;
	}

} 