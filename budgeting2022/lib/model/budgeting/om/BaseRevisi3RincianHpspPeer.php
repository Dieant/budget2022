<?php


abstract class BaseRevisi3RincianHpspPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.revisi3_rincian_hpsp';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.Revisi3RincianHpsp';

	
	const NUM_COLUMNS = 8;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const UNIT_ID = 'ebudget.revisi3_rincian_hpsp.UNIT_ID';

	
	const KEGIATAN_CODE = 'ebudget.revisi3_rincian_hpsp.KEGIATAN_CODE';

	
	const KOMPONEN_ID = 'ebudget.revisi3_rincian_hpsp.KOMPONEN_ID';

	
	const DETAIL_NO = 'ebudget.revisi3_rincian_hpsp.DETAIL_NO';

	
	const ID_KOMPONEN_LELANG = 'ebudget.revisi3_rincian_hpsp.ID_KOMPONEN_LELANG';

	
	const SISA_ANGGARAN = 'ebudget.revisi3_rincian_hpsp.SISA_ANGGARAN';

	
	const CATATAN = 'ebudget.revisi3_rincian_hpsp.CATATAN';

	
	const TAHAP = 'ebudget.revisi3_rincian_hpsp.TAHAP';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('UnitId', 'KegiatanCode', 'KomponenId', 'DetailNo', 'IdKomponenLelang', 'SisaAnggaran', 'Catatan', 'Tahap', ),
		BasePeer::TYPE_COLNAME => array (Revisi3RincianHpspPeer::UNIT_ID, Revisi3RincianHpspPeer::KEGIATAN_CODE, Revisi3RincianHpspPeer::KOMPONEN_ID, Revisi3RincianHpspPeer::DETAIL_NO, Revisi3RincianHpspPeer::ID_KOMPONEN_LELANG, Revisi3RincianHpspPeer::SISA_ANGGARAN, Revisi3RincianHpspPeer::CATATAN, Revisi3RincianHpspPeer::TAHAP, ),
		BasePeer::TYPE_FIELDNAME => array ('unit_id', 'kegiatan_code', 'komponen_id', 'detail_no', 'id_komponen_lelang', 'sisa_anggaran', 'catatan', 'tahap', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('UnitId' => 0, 'KegiatanCode' => 1, 'KomponenId' => 2, 'DetailNo' => 3, 'IdKomponenLelang' => 4, 'SisaAnggaran' => 5, 'Catatan' => 6, 'Tahap' => 7, ),
		BasePeer::TYPE_COLNAME => array (Revisi3RincianHpspPeer::UNIT_ID => 0, Revisi3RincianHpspPeer::KEGIATAN_CODE => 1, Revisi3RincianHpspPeer::KOMPONEN_ID => 2, Revisi3RincianHpspPeer::DETAIL_NO => 3, Revisi3RincianHpspPeer::ID_KOMPONEN_LELANG => 4, Revisi3RincianHpspPeer::SISA_ANGGARAN => 5, Revisi3RincianHpspPeer::CATATAN => 6, Revisi3RincianHpspPeer::TAHAP => 7, ),
		BasePeer::TYPE_FIELDNAME => array ('unit_id' => 0, 'kegiatan_code' => 1, 'komponen_id' => 2, 'detail_no' => 3, 'id_komponen_lelang' => 4, 'sisa_anggaran' => 5, 'catatan' => 6, 'tahap' => 7, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/Revisi3RincianHpspMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.Revisi3RincianHpspMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = Revisi3RincianHpspPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(Revisi3RincianHpspPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(Revisi3RincianHpspPeer::UNIT_ID);

		$criteria->addSelectColumn(Revisi3RincianHpspPeer::KEGIATAN_CODE);

		$criteria->addSelectColumn(Revisi3RincianHpspPeer::KOMPONEN_ID);

		$criteria->addSelectColumn(Revisi3RincianHpspPeer::DETAIL_NO);

		$criteria->addSelectColumn(Revisi3RincianHpspPeer::ID_KOMPONEN_LELANG);

		$criteria->addSelectColumn(Revisi3RincianHpspPeer::SISA_ANGGARAN);

		$criteria->addSelectColumn(Revisi3RincianHpspPeer::CATATAN);

		$criteria->addSelectColumn(Revisi3RincianHpspPeer::TAHAP);

	}

	const COUNT = 'COUNT(ebudget.revisi3_rincian_hpsp.UNIT_ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.revisi3_rincian_hpsp.UNIT_ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(Revisi3RincianHpspPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(Revisi3RincianHpspPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = Revisi3RincianHpspPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = Revisi3RincianHpspPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return Revisi3RincianHpspPeer::populateObjects(Revisi3RincianHpspPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			Revisi3RincianHpspPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = Revisi3RincianHpspPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return Revisi3RincianHpspPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(Revisi3RincianHpspPeer::UNIT_ID);
			$selectCriteria->add(Revisi3RincianHpspPeer::UNIT_ID, $criteria->remove(Revisi3RincianHpspPeer::UNIT_ID), $comparison);

			$comparison = $criteria->getComparison(Revisi3RincianHpspPeer::KEGIATAN_CODE);
			$selectCriteria->add(Revisi3RincianHpspPeer::KEGIATAN_CODE, $criteria->remove(Revisi3RincianHpspPeer::KEGIATAN_CODE), $comparison);

			$comparison = $criteria->getComparison(Revisi3RincianHpspPeer::DETAIL_NO);
			$selectCriteria->add(Revisi3RincianHpspPeer::DETAIL_NO, $criteria->remove(Revisi3RincianHpspPeer::DETAIL_NO), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(Revisi3RincianHpspPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(Revisi3RincianHpspPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof Revisi3RincianHpsp) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
												if(count($values) == count($values, COUNT_RECURSIVE))
			{
								$values = array($values);
			}
			$vals = array();
			foreach($values as $value)
			{

				$vals[0][] = $value[0];
				$vals[1][] = $value[1];
				$vals[2][] = $value[2];
			}

			$criteria->add(Revisi3RincianHpspPeer::UNIT_ID, $vals[0], Criteria::IN);
			$criteria->add(Revisi3RincianHpspPeer::KEGIATAN_CODE, $vals[1], Criteria::IN);
			$criteria->add(Revisi3RincianHpspPeer::DETAIL_NO, $vals[2], Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(Revisi3RincianHpsp $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(Revisi3RincianHpspPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(Revisi3RincianHpspPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(Revisi3RincianHpspPeer::DATABASE_NAME, Revisi3RincianHpspPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = Revisi3RincianHpspPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK( $unit_id, $kegiatan_code, $detail_no, $con = null) {
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$criteria = new Criteria();
		$criteria->add(Revisi3RincianHpspPeer::UNIT_ID, $unit_id);
		$criteria->add(Revisi3RincianHpspPeer::KEGIATAN_CODE, $kegiatan_code);
		$criteria->add(Revisi3RincianHpspPeer::DETAIL_NO, $detail_no);
		$v = Revisi3RincianHpspPeer::doSelect($criteria, $con);

		return !empty($v) ? $v[0] : null;
	}
} 
if (Propel::isInit()) {
			try {
		BaseRevisi3RincianHpspPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/Revisi3RincianHpspMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.Revisi3RincianHpspMapBuilder');
}
