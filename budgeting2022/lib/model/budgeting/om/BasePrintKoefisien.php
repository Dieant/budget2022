<?php


abstract class BasePrintKoefisien extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $unit_id;


	
	protected $kegiatan_code;


	
	protected $print_no;


	
	protected $keterangan;


	
	protected $tgl_print;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getUnitId()
	{

		return $this->unit_id;
	}

	
	public function getKegiatanCode()
	{

		return $this->kegiatan_code;
	}

	
	public function getPrintNo()
	{

		return $this->print_no;
	}

	
	public function getKeterangan()
	{

		return $this->keterangan;
	}

	
	public function getTglPrint($format = 'Y-m-d H:i:s')
	{

		if ($this->tgl_print === null || $this->tgl_print === '') {
			return null;
		} elseif (!is_int($this->tgl_print)) {
						$ts = strtotime($this->tgl_print);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [tgl_print] as date/time value: " . var_export($this->tgl_print, true));
			}
		} else {
			$ts = $this->tgl_print;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function setUnitId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->unit_id !== $v) {
			$this->unit_id = $v;
			$this->modifiedColumns[] = PrintKoefisienPeer::UNIT_ID;
		}

	} 
	
	public function setKegiatanCode($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kegiatan_code !== $v) {
			$this->kegiatan_code = $v;
			$this->modifiedColumns[] = PrintKoefisienPeer::KEGIATAN_CODE;
		}

	} 
	
	public function setPrintNo($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->print_no !== $v) {
			$this->print_no = $v;
			$this->modifiedColumns[] = PrintKoefisienPeer::PRINT_NO;
		}

	} 
	
	public function setKeterangan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->keterangan !== $v) {
			$this->keterangan = $v;
			$this->modifiedColumns[] = PrintKoefisienPeer::KETERANGAN;
		}

	} 
	
	public function setTglPrint($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [tgl_print] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->tgl_print !== $ts) {
			$this->tgl_print = $ts;
			$this->modifiedColumns[] = PrintKoefisienPeer::TGL_PRINT;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->unit_id = $rs->getString($startcol + 0);

			$this->kegiatan_code = $rs->getString($startcol + 1);

			$this->print_no = $rs->getInt($startcol + 2);

			$this->keterangan = $rs->getString($startcol + 3);

			$this->tgl_print = $rs->getTimestamp($startcol + 4, null);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 5; 
		} catch (Exception $e) {
			throw new PropelException("Error populating PrintKoefisien object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(PrintKoefisienPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			PrintKoefisienPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(PrintKoefisienPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = PrintKoefisienPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += PrintKoefisienPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = PrintKoefisienPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = PrintKoefisienPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getUnitId();
				break;
			case 1:
				return $this->getKegiatanCode();
				break;
			case 2:
				return $this->getPrintNo();
				break;
			case 3:
				return $this->getKeterangan();
				break;
			case 4:
				return $this->getTglPrint();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = PrintKoefisienPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getUnitId(),
			$keys[1] => $this->getKegiatanCode(),
			$keys[2] => $this->getPrintNo(),
			$keys[3] => $this->getKeterangan(),
			$keys[4] => $this->getTglPrint(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = PrintKoefisienPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setUnitId($value);
				break;
			case 1:
				$this->setKegiatanCode($value);
				break;
			case 2:
				$this->setPrintNo($value);
				break;
			case 3:
				$this->setKeterangan($value);
				break;
			case 4:
				$this->setTglPrint($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = PrintKoefisienPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setUnitId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setKegiatanCode($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setPrintNo($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setKeterangan($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setTglPrint($arr[$keys[4]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(PrintKoefisienPeer::DATABASE_NAME);

		if ($this->isColumnModified(PrintKoefisienPeer::UNIT_ID)) $criteria->add(PrintKoefisienPeer::UNIT_ID, $this->unit_id);
		if ($this->isColumnModified(PrintKoefisienPeer::KEGIATAN_CODE)) $criteria->add(PrintKoefisienPeer::KEGIATAN_CODE, $this->kegiatan_code);
		if ($this->isColumnModified(PrintKoefisienPeer::PRINT_NO)) $criteria->add(PrintKoefisienPeer::PRINT_NO, $this->print_no);
		if ($this->isColumnModified(PrintKoefisienPeer::KETERANGAN)) $criteria->add(PrintKoefisienPeer::KETERANGAN, $this->keterangan);
		if ($this->isColumnModified(PrintKoefisienPeer::TGL_PRINT)) $criteria->add(PrintKoefisienPeer::TGL_PRINT, $this->tgl_print);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(PrintKoefisienPeer::DATABASE_NAME);

		$criteria->add(PrintKoefisienPeer::UNIT_ID, $this->unit_id);
		$criteria->add(PrintKoefisienPeer::KEGIATAN_CODE, $this->kegiatan_code);
		$criteria->add(PrintKoefisienPeer::PRINT_NO, $this->print_no);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		$pks = array();

		$pks[0] = $this->getUnitId();

		$pks[1] = $this->getKegiatanCode();

		$pks[2] = $this->getPrintNo();

		return $pks;
	}

	
	public function setPrimaryKey($keys)
	{

		$this->setUnitId($keys[0]);

		$this->setKegiatanCode($keys[1]);

		$this->setPrintNo($keys[2]);

	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setKeterangan($this->keterangan);

		$copyObj->setTglPrint($this->tgl_print);


		$copyObj->setNew(true);

		$copyObj->setUnitId(NULL); 
		$copyObj->setKegiatanCode(NULL); 
		$copyObj->setPrintNo(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new PrintKoefisienPeer();
		}
		return self::$peer;
	}

} 