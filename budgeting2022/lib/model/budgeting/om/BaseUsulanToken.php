<?php


abstract class BaseUsulanToken extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id_token;


	
	protected $required_at;


	
	protected $expired_at;


	
	protected $used_at;


	
	protected $penyelia;


	
	protected $unit_id;


	
	protected $created_by;


	
	protected $jumlah_ssh;


	
	protected $token;


	
	protected $status_used;


	
	protected $nomor_surat;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getIdToken()
	{

		return $this->id_token;
	}

	
	public function getRequiredAt($format = 'Y-m-d H:i:s')
	{

		if ($this->required_at === null || $this->required_at === '') {
			return null;
		} elseif (!is_int($this->required_at)) {
						$ts = strtotime($this->required_at);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [required_at] as date/time value: " . var_export($this->required_at, true));
			}
		} else {
			$ts = $this->required_at;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getExpiredAt($format = 'Y-m-d H:i:s')
	{

		if ($this->expired_at === null || $this->expired_at === '') {
			return null;
		} elseif (!is_int($this->expired_at)) {
						$ts = strtotime($this->expired_at);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [expired_at] as date/time value: " . var_export($this->expired_at, true));
			}
		} else {
			$ts = $this->expired_at;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getUsedAt($format = 'Y-m-d H:i:s')
	{

		if ($this->used_at === null || $this->used_at === '') {
			return null;
		} elseif (!is_int($this->used_at)) {
						$ts = strtotime($this->used_at);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [used_at] as date/time value: " . var_export($this->used_at, true));
			}
		} else {
			$ts = $this->used_at;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getPenyelia()
	{

		return $this->penyelia;
	}

	
	public function getUnitId()
	{

		return $this->unit_id;
	}

	
	public function getCreatedBy()
	{

		return $this->created_by;
	}

	
	public function getJumlahSsh()
	{

		return $this->jumlah_ssh;
	}

	
	public function getToken()
	{

		return $this->token;
	}

	
	public function getStatusUsed()
	{

		return $this->status_used;
	}

	
	public function getNomorSurat()
	{

		return $this->nomor_surat;
	}

	
	public function setIdToken($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id_token !== $v) {
			$this->id_token = $v;
			$this->modifiedColumns[] = UsulanTokenPeer::ID_TOKEN;
		}

	} 
	
	public function setRequiredAt($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [required_at] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->required_at !== $ts) {
			$this->required_at = $ts;
			$this->modifiedColumns[] = UsulanTokenPeer::REQUIRED_AT;
		}

	} 
	
	public function setExpiredAt($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [expired_at] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->expired_at !== $ts) {
			$this->expired_at = $ts;
			$this->modifiedColumns[] = UsulanTokenPeer::EXPIRED_AT;
		}

	} 
	
	public function setUsedAt($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [used_at] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->used_at !== $ts) {
			$this->used_at = $ts;
			$this->modifiedColumns[] = UsulanTokenPeer::USED_AT;
		}

	} 
	
	public function setPenyelia($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->penyelia !== $v) {
			$this->penyelia = $v;
			$this->modifiedColumns[] = UsulanTokenPeer::PENYELIA;
		}

	} 
	
	public function setUnitId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->unit_id !== $v) {
			$this->unit_id = $v;
			$this->modifiedColumns[] = UsulanTokenPeer::UNIT_ID;
		}

	} 
	
	public function setCreatedBy($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->created_by !== $v) {
			$this->created_by = $v;
			$this->modifiedColumns[] = UsulanTokenPeer::CREATED_BY;
		}

	} 
	
	public function setJumlahSsh($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->jumlah_ssh !== $v) {
			$this->jumlah_ssh = $v;
			$this->modifiedColumns[] = UsulanTokenPeer::JUMLAH_SSH;
		}

	} 
	
	public function setToken($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->token !== $v) {
			$this->token = $v;
			$this->modifiedColumns[] = UsulanTokenPeer::TOKEN;
		}

	} 
	
	public function setStatusUsed($v)
	{

		if ($this->status_used !== $v) {
			$this->status_used = $v;
			$this->modifiedColumns[] = UsulanTokenPeer::STATUS_USED;
		}

	} 
	
	public function setNomorSurat($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nomor_surat !== $v) {
			$this->nomor_surat = $v;
			$this->modifiedColumns[] = UsulanTokenPeer::NOMOR_SURAT;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id_token = $rs->getInt($startcol + 0);

			$this->required_at = $rs->getTimestamp($startcol + 1, null);

			$this->expired_at = $rs->getTimestamp($startcol + 2, null);

			$this->used_at = $rs->getTimestamp($startcol + 3, null);

			$this->penyelia = $rs->getString($startcol + 4);

			$this->unit_id = $rs->getString($startcol + 5);

			$this->created_by = $rs->getString($startcol + 6);

			$this->jumlah_ssh = $rs->getInt($startcol + 7);

			$this->token = $rs->getString($startcol + 8);

			$this->status_used = $rs->getBoolean($startcol + 9);

			$this->nomor_surat = $rs->getString($startcol + 10);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 11; 
		} catch (Exception $e) {
			throw new PropelException("Error populating UsulanToken object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(UsulanTokenPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			UsulanTokenPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(UsulanTokenPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = UsulanTokenPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += UsulanTokenPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = UsulanTokenPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = UsulanTokenPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getIdToken();
				break;
			case 1:
				return $this->getRequiredAt();
				break;
			case 2:
				return $this->getExpiredAt();
				break;
			case 3:
				return $this->getUsedAt();
				break;
			case 4:
				return $this->getPenyelia();
				break;
			case 5:
				return $this->getUnitId();
				break;
			case 6:
				return $this->getCreatedBy();
				break;
			case 7:
				return $this->getJumlahSsh();
				break;
			case 8:
				return $this->getToken();
				break;
			case 9:
				return $this->getStatusUsed();
				break;
			case 10:
				return $this->getNomorSurat();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = UsulanTokenPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getIdToken(),
			$keys[1] => $this->getRequiredAt(),
			$keys[2] => $this->getExpiredAt(),
			$keys[3] => $this->getUsedAt(),
			$keys[4] => $this->getPenyelia(),
			$keys[5] => $this->getUnitId(),
			$keys[6] => $this->getCreatedBy(),
			$keys[7] => $this->getJumlahSsh(),
			$keys[8] => $this->getToken(),
			$keys[9] => $this->getStatusUsed(),
			$keys[10] => $this->getNomorSurat(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = UsulanTokenPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setIdToken($value);
				break;
			case 1:
				$this->setRequiredAt($value);
				break;
			case 2:
				$this->setExpiredAt($value);
				break;
			case 3:
				$this->setUsedAt($value);
				break;
			case 4:
				$this->setPenyelia($value);
				break;
			case 5:
				$this->setUnitId($value);
				break;
			case 6:
				$this->setCreatedBy($value);
				break;
			case 7:
				$this->setJumlahSsh($value);
				break;
			case 8:
				$this->setToken($value);
				break;
			case 9:
				$this->setStatusUsed($value);
				break;
			case 10:
				$this->setNomorSurat($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = UsulanTokenPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setIdToken($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setRequiredAt($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setExpiredAt($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setUsedAt($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setPenyelia($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setUnitId($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setCreatedBy($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setJumlahSsh($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setToken($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setStatusUsed($arr[$keys[9]]);
		if (array_key_exists($keys[10], $arr)) $this->setNomorSurat($arr[$keys[10]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(UsulanTokenPeer::DATABASE_NAME);

		if ($this->isColumnModified(UsulanTokenPeer::ID_TOKEN)) $criteria->add(UsulanTokenPeer::ID_TOKEN, $this->id_token);
		if ($this->isColumnModified(UsulanTokenPeer::REQUIRED_AT)) $criteria->add(UsulanTokenPeer::REQUIRED_AT, $this->required_at);
		if ($this->isColumnModified(UsulanTokenPeer::EXPIRED_AT)) $criteria->add(UsulanTokenPeer::EXPIRED_AT, $this->expired_at);
		if ($this->isColumnModified(UsulanTokenPeer::USED_AT)) $criteria->add(UsulanTokenPeer::USED_AT, $this->used_at);
		if ($this->isColumnModified(UsulanTokenPeer::PENYELIA)) $criteria->add(UsulanTokenPeer::PENYELIA, $this->penyelia);
		if ($this->isColumnModified(UsulanTokenPeer::UNIT_ID)) $criteria->add(UsulanTokenPeer::UNIT_ID, $this->unit_id);
		if ($this->isColumnModified(UsulanTokenPeer::CREATED_BY)) $criteria->add(UsulanTokenPeer::CREATED_BY, $this->created_by);
		if ($this->isColumnModified(UsulanTokenPeer::JUMLAH_SSH)) $criteria->add(UsulanTokenPeer::JUMLAH_SSH, $this->jumlah_ssh);
		if ($this->isColumnModified(UsulanTokenPeer::TOKEN)) $criteria->add(UsulanTokenPeer::TOKEN, $this->token);
		if ($this->isColumnModified(UsulanTokenPeer::STATUS_USED)) $criteria->add(UsulanTokenPeer::STATUS_USED, $this->status_used);
		if ($this->isColumnModified(UsulanTokenPeer::NOMOR_SURAT)) $criteria->add(UsulanTokenPeer::NOMOR_SURAT, $this->nomor_surat);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(UsulanTokenPeer::DATABASE_NAME);

		$criteria->add(UsulanTokenPeer::ID_TOKEN, $this->id_token);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getIdToken();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setIdToken($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setRequiredAt($this->required_at);

		$copyObj->setExpiredAt($this->expired_at);

		$copyObj->setUsedAt($this->used_at);

		$copyObj->setPenyelia($this->penyelia);

		$copyObj->setUnitId($this->unit_id);

		$copyObj->setCreatedBy($this->created_by);

		$copyObj->setJumlahSsh($this->jumlah_ssh);

		$copyObj->setToken($this->token);

		$copyObj->setStatusUsed($this->status_used);

		$copyObj->setNomorSurat($this->nomor_surat);


		$copyObj->setNew(true);

		$copyObj->setIdToken(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new UsulanTokenPeer();
		}
		return self::$peer;
	}

} 