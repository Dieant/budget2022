<?php


abstract class BaseUnitKerja extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $unit_id;


	
	protected $kelompok_id;


	
	protected $unit_name;


	
	protected $unit_address;


	
	protected $kepala_nama;


	
	protected $kepala_pangkat;


	
	protected $kepala_nip;


	
	protected $kode_permen;


	
	protected $pagu;


	
	protected $jumlah_pns = 0;


	
	protected $jumlah_honda;


	
	protected $jumlah_nonpns;

	
	protected $aKelompokDinas;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getUnitId()
	{

		return $this->unit_id;
	}

	
	public function getKelompokId()
	{

		return $this->kelompok_id;
	}

	
	public function getUnitName()
	{

		return $this->unit_name;
	}

	
	public function getUnitAddress()
	{

		return $this->unit_address;
	}

	
	public function getKepalaNama()
	{

		return $this->kepala_nama;
	}

	
	public function getKepalaPangkat()
	{

		return $this->kepala_pangkat;
	}

	
	public function getKepalaNip()
	{

		return $this->kepala_nip;
	}

	
	public function getKodePermen()
	{

		return $this->kode_permen;
	}

	
	public function getPagu()
	{

		return $this->pagu;
	}

	
	public function getJumlahPns()
	{

		return $this->jumlah_pns;
	}

	
	public function getJumlahHonda()
	{

		return $this->jumlah_honda;
	}

	
	public function getJumlahNonpns()
	{

		return $this->jumlah_nonpns;
	}

	
	public function setUnitId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->unit_id !== $v) {
			$this->unit_id = $v;
			$this->modifiedColumns[] = UnitKerjaPeer::UNIT_ID;
		}

	} 
	
	public function setKelompokId($v)
	{

		if ($this->kelompok_id !== $v) {
			$this->kelompok_id = $v;
			$this->modifiedColumns[] = UnitKerjaPeer::KELOMPOK_ID;
		}

		if ($this->aKelompokDinas !== null && $this->aKelompokDinas->getKelompokId() !== $v) {
			$this->aKelompokDinas = null;
		}

	} 
	
	public function setUnitName($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->unit_name !== $v) {
			$this->unit_name = $v;
			$this->modifiedColumns[] = UnitKerjaPeer::UNIT_NAME;
		}

	} 
	
	public function setUnitAddress($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->unit_address !== $v) {
			$this->unit_address = $v;
			$this->modifiedColumns[] = UnitKerjaPeer::UNIT_ADDRESS;
		}

	} 
	
	public function setKepalaNama($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kepala_nama !== $v) {
			$this->kepala_nama = $v;
			$this->modifiedColumns[] = UnitKerjaPeer::KEPALA_NAMA;
		}

	} 
	
	public function setKepalaPangkat($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kepala_pangkat !== $v) {
			$this->kepala_pangkat = $v;
			$this->modifiedColumns[] = UnitKerjaPeer::KEPALA_PANGKAT;
		}

	} 
	
	public function setKepalaNip($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kepala_nip !== $v) {
			$this->kepala_nip = $v;
			$this->modifiedColumns[] = UnitKerjaPeer::KEPALA_NIP;
		}

	} 
	
	public function setKodePermen($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_permen !== $v) {
			$this->kode_permen = $v;
			$this->modifiedColumns[] = UnitKerjaPeer::KODE_PERMEN;
		}

	} 
	
	public function setPagu($v)
	{

		if ($this->pagu !== $v) {
			$this->pagu = $v;
			$this->modifiedColumns[] = UnitKerjaPeer::PAGU;
		}

	} 
	
	public function setJumlahPns($v)
	{

		if ($this->jumlah_pns !== $v || $v === 0) {
			$this->jumlah_pns = $v;
			$this->modifiedColumns[] = UnitKerjaPeer::JUMLAH_PNS;
		}

	} 
	
	public function setJumlahHonda($v)
	{

		if ($this->jumlah_honda !== $v) {
			$this->jumlah_honda = $v;
			$this->modifiedColumns[] = UnitKerjaPeer::JUMLAH_HONDA;
		}

	} 
	
	public function setJumlahNonpns($v)
	{

		if ($this->jumlah_nonpns !== $v) {
			$this->jumlah_nonpns = $v;
			$this->modifiedColumns[] = UnitKerjaPeer::JUMLAH_NONPNS;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->unit_id = $rs->getString($startcol + 0);

			$this->kelompok_id = $rs->getFloat($startcol + 1);

			$this->unit_name = $rs->getString($startcol + 2);

			$this->unit_address = $rs->getString($startcol + 3);

			$this->kepala_nama = $rs->getString($startcol + 4);

			$this->kepala_pangkat = $rs->getString($startcol + 5);

			$this->kepala_nip = $rs->getString($startcol + 6);

			$this->kode_permen = $rs->getString($startcol + 7);

			$this->pagu = $rs->getFloat($startcol + 8);

			$this->jumlah_pns = $rs->getFloat($startcol + 9);

			$this->jumlah_honda = $rs->getFloat($startcol + 10);

			$this->jumlah_nonpns = $rs->getFloat($startcol + 11);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 12; 
		} catch (Exception $e) {
			throw new PropelException("Error populating UnitKerja object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(UnitKerjaPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			UnitKerjaPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(UnitKerjaPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


												
			if ($this->aKelompokDinas !== null) {
				if ($this->aKelompokDinas->isModified()) {
					$affectedRows += $this->aKelompokDinas->save($con);
				}
				$this->setKelompokDinas($this->aKelompokDinas);
			}


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = UnitKerjaPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += UnitKerjaPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


												
			if ($this->aKelompokDinas !== null) {
				if (!$this->aKelompokDinas->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aKelompokDinas->getValidationFailures());
				}
			}


			if (($retval = UnitKerjaPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = UnitKerjaPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getUnitId();
				break;
			case 1:
				return $this->getKelompokId();
				break;
			case 2:
				return $this->getUnitName();
				break;
			case 3:
				return $this->getUnitAddress();
				break;
			case 4:
				return $this->getKepalaNama();
				break;
			case 5:
				return $this->getKepalaPangkat();
				break;
			case 6:
				return $this->getKepalaNip();
				break;
			case 7:
				return $this->getKodePermen();
				break;
			case 8:
				return $this->getPagu();
				break;
			case 9:
				return $this->getJumlahPns();
				break;
			case 10:
				return $this->getJumlahHonda();
				break;
			case 11:
				return $this->getJumlahNonpns();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = UnitKerjaPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getUnitId(),
			$keys[1] => $this->getKelompokId(),
			$keys[2] => $this->getUnitName(),
			$keys[3] => $this->getUnitAddress(),
			$keys[4] => $this->getKepalaNama(),
			$keys[5] => $this->getKepalaPangkat(),
			$keys[6] => $this->getKepalaNip(),
			$keys[7] => $this->getKodePermen(),
			$keys[8] => $this->getPagu(),
			$keys[9] => $this->getJumlahPns(),
			$keys[10] => $this->getJumlahHonda(),
			$keys[11] => $this->getJumlahNonpns(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = UnitKerjaPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setUnitId($value);
				break;
			case 1:
				$this->setKelompokId($value);
				break;
			case 2:
				$this->setUnitName($value);
				break;
			case 3:
				$this->setUnitAddress($value);
				break;
			case 4:
				$this->setKepalaNama($value);
				break;
			case 5:
				$this->setKepalaPangkat($value);
				break;
			case 6:
				$this->setKepalaNip($value);
				break;
			case 7:
				$this->setKodePermen($value);
				break;
			case 8:
				$this->setPagu($value);
				break;
			case 9:
				$this->setJumlahPns($value);
				break;
			case 10:
				$this->setJumlahHonda($value);
				break;
			case 11:
				$this->setJumlahNonpns($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = UnitKerjaPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setUnitId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setKelompokId($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setUnitName($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setUnitAddress($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setKepalaNama($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setKepalaPangkat($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setKepalaNip($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setKodePermen($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setPagu($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setJumlahPns($arr[$keys[9]]);
		if (array_key_exists($keys[10], $arr)) $this->setJumlahHonda($arr[$keys[10]]);
		if (array_key_exists($keys[11], $arr)) $this->setJumlahNonpns($arr[$keys[11]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(UnitKerjaPeer::DATABASE_NAME);

		if ($this->isColumnModified(UnitKerjaPeer::UNIT_ID)) $criteria->add(UnitKerjaPeer::UNIT_ID, $this->unit_id);
		if ($this->isColumnModified(UnitKerjaPeer::KELOMPOK_ID)) $criteria->add(UnitKerjaPeer::KELOMPOK_ID, $this->kelompok_id);
		if ($this->isColumnModified(UnitKerjaPeer::UNIT_NAME)) $criteria->add(UnitKerjaPeer::UNIT_NAME, $this->unit_name);
		if ($this->isColumnModified(UnitKerjaPeer::UNIT_ADDRESS)) $criteria->add(UnitKerjaPeer::UNIT_ADDRESS, $this->unit_address);
		if ($this->isColumnModified(UnitKerjaPeer::KEPALA_NAMA)) $criteria->add(UnitKerjaPeer::KEPALA_NAMA, $this->kepala_nama);
		if ($this->isColumnModified(UnitKerjaPeer::KEPALA_PANGKAT)) $criteria->add(UnitKerjaPeer::KEPALA_PANGKAT, $this->kepala_pangkat);
		if ($this->isColumnModified(UnitKerjaPeer::KEPALA_NIP)) $criteria->add(UnitKerjaPeer::KEPALA_NIP, $this->kepala_nip);
		if ($this->isColumnModified(UnitKerjaPeer::KODE_PERMEN)) $criteria->add(UnitKerjaPeer::KODE_PERMEN, $this->kode_permen);
		if ($this->isColumnModified(UnitKerjaPeer::PAGU)) $criteria->add(UnitKerjaPeer::PAGU, $this->pagu);
		if ($this->isColumnModified(UnitKerjaPeer::JUMLAH_PNS)) $criteria->add(UnitKerjaPeer::JUMLAH_PNS, $this->jumlah_pns);
		if ($this->isColumnModified(UnitKerjaPeer::JUMLAH_HONDA)) $criteria->add(UnitKerjaPeer::JUMLAH_HONDA, $this->jumlah_honda);
		if ($this->isColumnModified(UnitKerjaPeer::JUMLAH_NONPNS)) $criteria->add(UnitKerjaPeer::JUMLAH_NONPNS, $this->jumlah_nonpns);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(UnitKerjaPeer::DATABASE_NAME);

		$criteria->add(UnitKerjaPeer::UNIT_ID, $this->unit_id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getUnitId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setUnitId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setKelompokId($this->kelompok_id);

		$copyObj->setUnitName($this->unit_name);

		$copyObj->setUnitAddress($this->unit_address);

		$copyObj->setKepalaNama($this->kepala_nama);

		$copyObj->setKepalaPangkat($this->kepala_pangkat);

		$copyObj->setKepalaNip($this->kepala_nip);

		$copyObj->setKodePermen($this->kode_permen);

		$copyObj->setPagu($this->pagu);

		$copyObj->setJumlahPns($this->jumlah_pns);

		$copyObj->setJumlahHonda($this->jumlah_honda);

		$copyObj->setJumlahNonpns($this->jumlah_nonpns);


		$copyObj->setNew(true);

		$copyObj->setUnitId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new UnitKerjaPeer();
		}
		return self::$peer;
	}

	
	public function setKelompokDinas($v)
	{


		if ($v === null) {
			$this->setKelompokId(NULL);
		} else {
			$this->setKelompokId($v->getKelompokId());
		}


		$this->aKelompokDinas = $v;
	}


	
	public function getKelompokDinas($con = null)
	{
		if ($this->aKelompokDinas === null && ($this->kelompok_id > 0)) {
						include_once 'lib/model/budgeting/om/BaseKelompokDinasPeer.php';

			$this->aKelompokDinas = KelompokDinasPeer::retrieveByPK($this->kelompok_id, $con);

			
		}
		return $this->aKelompokDinas;
	}

} 