<?php


abstract class BaseDinasSubtitleIndikatorPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.dinas_subtitle_indikator';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.DinasSubtitleIndikator';

	
	const NUM_COLUMNS = 27;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const UNIT_ID = 'ebudget.dinas_subtitle_indikator.UNIT_ID';

	
	const KEGIATAN_CODE = 'ebudget.dinas_subtitle_indikator.KEGIATAN_CODE';

	
	const SUBTITLE = 'ebudget.dinas_subtitle_indikator.SUBTITLE';

	
	const INDIKATOR = 'ebudget.dinas_subtitle_indikator.INDIKATOR';

	
	const NILAI = 'ebudget.dinas_subtitle_indikator.NILAI';

	
	const SATUAN = 'ebudget.dinas_subtitle_indikator.SATUAN';

	
	const LAST_UPDATE_USER = 'ebudget.dinas_subtitle_indikator.LAST_UPDATE_USER';

	
	const LAST_UPDATE_TIME = 'ebudget.dinas_subtitle_indikator.LAST_UPDATE_TIME';

	
	const LAST_UPDATE_IP = 'ebudget.dinas_subtitle_indikator.LAST_UPDATE_IP';

	
	const TAHAP = 'ebudget.dinas_subtitle_indikator.TAHAP';

	
	const SUB_ID = 'ebudget.dinas_subtitle_indikator.SUB_ID';

	
	const TAHUN = 'ebudget.dinas_subtitle_indikator.TAHUN';

	
	const LOCK_SUBTITLE = 'ebudget.dinas_subtitle_indikator.LOCK_SUBTITLE';

	
	const PRIORITAS = 'ebudget.dinas_subtitle_indikator.PRIORITAS';

	
	const CATATAN = 'ebudget.dinas_subtitle_indikator.CATATAN';

	
	const LAKILAKI = 'ebudget.dinas_subtitle_indikator.LAKILAKI';

	
	const PEREMPUAN = 'ebudget.dinas_subtitle_indikator.PEREMPUAN';

	
	const DEWASA = 'ebudget.dinas_subtitle_indikator.DEWASA';

	
	const ANAK = 'ebudget.dinas_subtitle_indikator.ANAK';

	
	const LANSIA = 'ebudget.dinas_subtitle_indikator.LANSIA';

	
	const INKLUSI = 'ebudget.dinas_subtitle_indikator.INKLUSI';

	
	const ALOKASI_DANA = 'ebudget.dinas_subtitle_indikator.ALOKASI_DANA';

	
	const GENDER = 'ebudget.dinas_subtitle_indikator.GENDER';

	
	const IS_NOL_ANGGARAN = 'ebudget.dinas_subtitle_indikator.IS_NOL_ANGGARAN';

	
	const INDIKATOR_CM = 'ebudget.dinas_subtitle_indikator.INDIKATOR_CM';

	
	const NILAI_CM = 'ebudget.dinas_subtitle_indikator.NILAI_CM';

	
	const SATUAN_CM = 'ebudget.dinas_subtitle_indikator.SATUAN_CM';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('UnitId', 'KegiatanCode', 'Subtitle', 'Indikator', 'Nilai', 'Satuan', 'LastUpdateUser', 'LastUpdateTime', 'LastUpdateIp', 'Tahap', 'SubId', 'Tahun', 'LockSubtitle', 'Prioritas', 'Catatan', 'Lakilaki', 'Perempuan', 'Dewasa', 'Anak', 'Lansia', 'Inklusi', 'AlokasiDana', 'Gender', 'IsNolAnggaran', 'IndikatorCm', 'NilaiCm', 'SatuanCm', ),
		BasePeer::TYPE_COLNAME => array (DinasSubtitleIndikatorPeer::UNIT_ID, DinasSubtitleIndikatorPeer::KEGIATAN_CODE, DinasSubtitleIndikatorPeer::SUBTITLE, DinasSubtitleIndikatorPeer::INDIKATOR, DinasSubtitleIndikatorPeer::NILAI, DinasSubtitleIndikatorPeer::SATUAN, DinasSubtitleIndikatorPeer::LAST_UPDATE_USER, DinasSubtitleIndikatorPeer::LAST_UPDATE_TIME, DinasSubtitleIndikatorPeer::LAST_UPDATE_IP, DinasSubtitleIndikatorPeer::TAHAP, DinasSubtitleIndikatorPeer::SUB_ID, DinasSubtitleIndikatorPeer::TAHUN, DinasSubtitleIndikatorPeer::LOCK_SUBTITLE, DinasSubtitleIndikatorPeer::PRIORITAS, DinasSubtitleIndikatorPeer::CATATAN, DinasSubtitleIndikatorPeer::LAKILAKI, DinasSubtitleIndikatorPeer::PEREMPUAN, DinasSubtitleIndikatorPeer::DEWASA, DinasSubtitleIndikatorPeer::ANAK, DinasSubtitleIndikatorPeer::LANSIA, DinasSubtitleIndikatorPeer::INKLUSI, DinasSubtitleIndikatorPeer::ALOKASI_DANA, DinasSubtitleIndikatorPeer::GENDER, DinasSubtitleIndikatorPeer::IS_NOL_ANGGARAN, DinasSubtitleIndikatorPeer::INDIKATOR_CM, DinasSubtitleIndikatorPeer::NILAI_CM, DinasSubtitleIndikatorPeer::SATUAN_CM, ),
		BasePeer::TYPE_FIELDNAME => array ('unit_id', 'kegiatan_code', 'subtitle', 'indikator', 'nilai', 'satuan', 'last_update_user', 'last_update_time', 'last_update_ip', 'tahap', 'sub_id', 'tahun', 'lock_subtitle', 'prioritas', 'catatan', 'lakilaki', 'perempuan', 'dewasa', 'anak', 'lansia', 'inklusi', 'alokasi_dana', 'gender', 'is_nol_anggaran', 'indikator_cm', 'nilai_cm', 'satuan_cm', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('UnitId' => 0, 'KegiatanCode' => 1, 'Subtitle' => 2, 'Indikator' => 3, 'Nilai' => 4, 'Satuan' => 5, 'LastUpdateUser' => 6, 'LastUpdateTime' => 7, 'LastUpdateIp' => 8, 'Tahap' => 9, 'SubId' => 10, 'Tahun' => 11, 'LockSubtitle' => 12, 'Prioritas' => 13, 'Catatan' => 14, 'Lakilaki' => 15, 'Perempuan' => 16, 'Dewasa' => 17, 'Anak' => 18, 'Lansia' => 19, 'Inklusi' => 20, 'AlokasiDana' => 21, 'Gender' => 22, 'IsNolAnggaran' => 23, 'IndikatorCm' => 24, 'NilaiCm' => 25, 'SatuanCm' => 26, ),
		BasePeer::TYPE_COLNAME => array (DinasSubtitleIndikatorPeer::UNIT_ID => 0, DinasSubtitleIndikatorPeer::KEGIATAN_CODE => 1, DinasSubtitleIndikatorPeer::SUBTITLE => 2, DinasSubtitleIndikatorPeer::INDIKATOR => 3, DinasSubtitleIndikatorPeer::NILAI => 4, DinasSubtitleIndikatorPeer::SATUAN => 5, DinasSubtitleIndikatorPeer::LAST_UPDATE_USER => 6, DinasSubtitleIndikatorPeer::LAST_UPDATE_TIME => 7, DinasSubtitleIndikatorPeer::LAST_UPDATE_IP => 8, DinasSubtitleIndikatorPeer::TAHAP => 9, DinasSubtitleIndikatorPeer::SUB_ID => 10, DinasSubtitleIndikatorPeer::TAHUN => 11, DinasSubtitleIndikatorPeer::LOCK_SUBTITLE => 12, DinasSubtitleIndikatorPeer::PRIORITAS => 13, DinasSubtitleIndikatorPeer::CATATAN => 14, DinasSubtitleIndikatorPeer::LAKILAKI => 15, DinasSubtitleIndikatorPeer::PEREMPUAN => 16, DinasSubtitleIndikatorPeer::DEWASA => 17, DinasSubtitleIndikatorPeer::ANAK => 18, DinasSubtitleIndikatorPeer::LANSIA => 19, DinasSubtitleIndikatorPeer::INKLUSI => 20, DinasSubtitleIndikatorPeer::ALOKASI_DANA => 21, DinasSubtitleIndikatorPeer::GENDER => 22, DinasSubtitleIndikatorPeer::IS_NOL_ANGGARAN => 23, DinasSubtitleIndikatorPeer::INDIKATOR_CM => 24, DinasSubtitleIndikatorPeer::NILAI_CM => 25, DinasSubtitleIndikatorPeer::SATUAN_CM => 26, ),
		BasePeer::TYPE_FIELDNAME => array ('unit_id' => 0, 'kegiatan_code' => 1, 'subtitle' => 2, 'indikator' => 3, 'nilai' => 4, 'satuan' => 5, 'last_update_user' => 6, 'last_update_time' => 7, 'last_update_ip' => 8, 'tahap' => 9, 'sub_id' => 10, 'tahun' => 11, 'lock_subtitle' => 12, 'prioritas' => 13, 'catatan' => 14, 'lakilaki' => 15, 'perempuan' => 16, 'dewasa' => 17, 'anak' => 18, 'lansia' => 19, 'inklusi' => 20, 'alokasi_dana' => 21, 'gender' => 22, 'is_nol_anggaran' => 23, 'indikator_cm' => 24, 'nilai_cm' => 25, 'satuan_cm' => 26, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/DinasSubtitleIndikatorMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.DinasSubtitleIndikatorMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = DinasSubtitleIndikatorPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(DinasSubtitleIndikatorPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(DinasSubtitleIndikatorPeer::UNIT_ID);

		$criteria->addSelectColumn(DinasSubtitleIndikatorPeer::KEGIATAN_CODE);

		$criteria->addSelectColumn(DinasSubtitleIndikatorPeer::SUBTITLE);

		$criteria->addSelectColumn(DinasSubtitleIndikatorPeer::INDIKATOR);

		$criteria->addSelectColumn(DinasSubtitleIndikatorPeer::NILAI);

		$criteria->addSelectColumn(DinasSubtitleIndikatorPeer::SATUAN);

		$criteria->addSelectColumn(DinasSubtitleIndikatorPeer::LAST_UPDATE_USER);

		$criteria->addSelectColumn(DinasSubtitleIndikatorPeer::LAST_UPDATE_TIME);

		$criteria->addSelectColumn(DinasSubtitleIndikatorPeer::LAST_UPDATE_IP);

		$criteria->addSelectColumn(DinasSubtitleIndikatorPeer::TAHAP);

		$criteria->addSelectColumn(DinasSubtitleIndikatorPeer::SUB_ID);

		$criteria->addSelectColumn(DinasSubtitleIndikatorPeer::TAHUN);

		$criteria->addSelectColumn(DinasSubtitleIndikatorPeer::LOCK_SUBTITLE);

		$criteria->addSelectColumn(DinasSubtitleIndikatorPeer::PRIORITAS);

		$criteria->addSelectColumn(DinasSubtitleIndikatorPeer::CATATAN);

		$criteria->addSelectColumn(DinasSubtitleIndikatorPeer::LAKILAKI);

		$criteria->addSelectColumn(DinasSubtitleIndikatorPeer::PEREMPUAN);

		$criteria->addSelectColumn(DinasSubtitleIndikatorPeer::DEWASA);

		$criteria->addSelectColumn(DinasSubtitleIndikatorPeer::ANAK);

		$criteria->addSelectColumn(DinasSubtitleIndikatorPeer::LANSIA);

		$criteria->addSelectColumn(DinasSubtitleIndikatorPeer::INKLUSI);

		$criteria->addSelectColumn(DinasSubtitleIndikatorPeer::ALOKASI_DANA);

		$criteria->addSelectColumn(DinasSubtitleIndikatorPeer::GENDER);

		$criteria->addSelectColumn(DinasSubtitleIndikatorPeer::IS_NOL_ANGGARAN);

		$criteria->addSelectColumn(DinasSubtitleIndikatorPeer::INDIKATOR_CM);

		$criteria->addSelectColumn(DinasSubtitleIndikatorPeer::NILAI_CM);

		$criteria->addSelectColumn(DinasSubtitleIndikatorPeer::SATUAN_CM);

	}

	const COUNT = 'COUNT(ebudget.dinas_subtitle_indikator.SUB_ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.dinas_subtitle_indikator.SUB_ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(DinasSubtitleIndikatorPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(DinasSubtitleIndikatorPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = DinasSubtitleIndikatorPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = DinasSubtitleIndikatorPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return DinasSubtitleIndikatorPeer::populateObjects(DinasSubtitleIndikatorPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			DinasSubtitleIndikatorPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = DinasSubtitleIndikatorPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return DinasSubtitleIndikatorPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}

		$criteria->remove(DinasSubtitleIndikatorPeer::SUB_ID); 

				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(DinasSubtitleIndikatorPeer::SUB_ID);
			$selectCriteria->add(DinasSubtitleIndikatorPeer::SUB_ID, $criteria->remove(DinasSubtitleIndikatorPeer::SUB_ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(DinasSubtitleIndikatorPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(DinasSubtitleIndikatorPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof DinasSubtitleIndikator) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(DinasSubtitleIndikatorPeer::SUB_ID, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(DinasSubtitleIndikator $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(DinasSubtitleIndikatorPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(DinasSubtitleIndikatorPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(DinasSubtitleIndikatorPeer::DATABASE_NAME, DinasSubtitleIndikatorPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = DinasSubtitleIndikatorPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(DinasSubtitleIndikatorPeer::DATABASE_NAME);

		$criteria->add(DinasSubtitleIndikatorPeer::SUB_ID, $pk);


		$v = DinasSubtitleIndikatorPeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(DinasSubtitleIndikatorPeer::SUB_ID, $pks, Criteria::IN);
			$objs = DinasSubtitleIndikatorPeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BaseDinasSubtitleIndikatorPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/DinasSubtitleIndikatorMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.DinasSubtitleIndikatorMapBuilder');
}
