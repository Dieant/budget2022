<?php


abstract class BaseSubKegiatan extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $sub_kegiatan_id;


	
	protected $sub_kegiatan_name;


	
	protected $param;


	
	protected $satuan;


	
	protected $status = 'Open';


	
	protected $pembagi;


	
	protected $keterangan;


	
	protected $unit_id;


	
	protected $penelitian_skala;


	
	protected $penelitian_waktu;


	
	protected $batas_nilai;


	
	protected $tenaga_ahli;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getSubKegiatanId()
	{

		return $this->sub_kegiatan_id;
	}

	
	public function getSubKegiatanName()
	{

		return $this->sub_kegiatan_name;
	}

	
	public function getParam()
	{

		return $this->param;
	}

	
	public function getSatuan()
	{

		return $this->satuan;
	}

	
	public function getStatus()
	{

		return $this->status;
	}

	
	public function getPembagi()
	{

		return $this->pembagi;
	}

	
	public function getKeterangan()
	{

		return $this->keterangan;
	}

	
	public function getUnitId()
	{

		return $this->unit_id;
	}

	
	public function getPenelitianSkala()
	{

		return $this->penelitian_skala;
	}

	
	public function getPenelitianWaktu()
	{

		return $this->penelitian_waktu;
	}

	
	public function getBatasNilai()
	{

		return $this->batas_nilai;
	}

	
	public function getTenagaAhli()
	{

		return $this->tenaga_ahli;
	}

	
	public function setSubKegiatanId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->sub_kegiatan_id !== $v) {
			$this->sub_kegiatan_id = $v;
			$this->modifiedColumns[] = SubKegiatanPeer::SUB_KEGIATAN_ID;
		}

	} 
	
	public function setSubKegiatanName($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->sub_kegiatan_name !== $v) {
			$this->sub_kegiatan_name = $v;
			$this->modifiedColumns[] = SubKegiatanPeer::SUB_KEGIATAN_NAME;
		}

	} 
	
	public function setParam($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->param !== $v) {
			$this->param = $v;
			$this->modifiedColumns[] = SubKegiatanPeer::PARAM;
		}

	} 
	
	public function setSatuan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->satuan !== $v) {
			$this->satuan = $v;
			$this->modifiedColumns[] = SubKegiatanPeer::SATUAN;
		}

	} 
	
	public function setStatus($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->status !== $v || $v === 'Open') {
			$this->status = $v;
			$this->modifiedColumns[] = SubKegiatanPeer::STATUS;
		}

	} 
	
	public function setPembagi($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->pembagi !== $v) {
			$this->pembagi = $v;
			$this->modifiedColumns[] = SubKegiatanPeer::PEMBAGI;
		}

	} 
	
	public function setKeterangan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->keterangan !== $v) {
			$this->keterangan = $v;
			$this->modifiedColumns[] = SubKegiatanPeer::KETERANGAN;
		}

	} 
	
	public function setUnitId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->unit_id !== $v) {
			$this->unit_id = $v;
			$this->modifiedColumns[] = SubKegiatanPeer::UNIT_ID;
		}

	} 
	
	public function setPenelitianSkala($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->penelitian_skala !== $v) {
			$this->penelitian_skala = $v;
			$this->modifiedColumns[] = SubKegiatanPeer::PENELITIAN_SKALA;
		}

	} 
	
	public function setPenelitianWaktu($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->penelitian_waktu !== $v) {
			$this->penelitian_waktu = $v;
			$this->modifiedColumns[] = SubKegiatanPeer::PENELITIAN_WAKTU;
		}

	} 
	
	public function setBatasNilai($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->batas_nilai !== $v) {
			$this->batas_nilai = $v;
			$this->modifiedColumns[] = SubKegiatanPeer::BATAS_NILAI;
		}

	} 
	
	public function setTenagaAhli($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->tenaga_ahli !== $v) {
			$this->tenaga_ahli = $v;
			$this->modifiedColumns[] = SubKegiatanPeer::TENAGA_AHLI;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->sub_kegiatan_id = $rs->getString($startcol + 0);

			$this->sub_kegiatan_name = $rs->getString($startcol + 1);

			$this->param = $rs->getString($startcol + 2);

			$this->satuan = $rs->getString($startcol + 3);

			$this->status = $rs->getString($startcol + 4);

			$this->pembagi = $rs->getString($startcol + 5);

			$this->keterangan = $rs->getString($startcol + 6);

			$this->unit_id = $rs->getString($startcol + 7);

			$this->penelitian_skala = $rs->getString($startcol + 8);

			$this->penelitian_waktu = $rs->getInt($startcol + 9);

			$this->batas_nilai = $rs->getString($startcol + 10);

			$this->tenaga_ahli = $rs->getInt($startcol + 11);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 12; 
		} catch (Exception $e) {
			throw new PropelException("Error populating SubKegiatan object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(SubKegiatanPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			SubKegiatanPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(SubKegiatanPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = SubKegiatanPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += SubKegiatanPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = SubKegiatanPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = SubKegiatanPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getSubKegiatanId();
				break;
			case 1:
				return $this->getSubKegiatanName();
				break;
			case 2:
				return $this->getParam();
				break;
			case 3:
				return $this->getSatuan();
				break;
			case 4:
				return $this->getStatus();
				break;
			case 5:
				return $this->getPembagi();
				break;
			case 6:
				return $this->getKeterangan();
				break;
			case 7:
				return $this->getUnitId();
				break;
			case 8:
				return $this->getPenelitianSkala();
				break;
			case 9:
				return $this->getPenelitianWaktu();
				break;
			case 10:
				return $this->getBatasNilai();
				break;
			case 11:
				return $this->getTenagaAhli();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = SubKegiatanPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getSubKegiatanId(),
			$keys[1] => $this->getSubKegiatanName(),
			$keys[2] => $this->getParam(),
			$keys[3] => $this->getSatuan(),
			$keys[4] => $this->getStatus(),
			$keys[5] => $this->getPembagi(),
			$keys[6] => $this->getKeterangan(),
			$keys[7] => $this->getUnitId(),
			$keys[8] => $this->getPenelitianSkala(),
			$keys[9] => $this->getPenelitianWaktu(),
			$keys[10] => $this->getBatasNilai(),
			$keys[11] => $this->getTenagaAhli(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = SubKegiatanPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setSubKegiatanId($value);
				break;
			case 1:
				$this->setSubKegiatanName($value);
				break;
			case 2:
				$this->setParam($value);
				break;
			case 3:
				$this->setSatuan($value);
				break;
			case 4:
				$this->setStatus($value);
				break;
			case 5:
				$this->setPembagi($value);
				break;
			case 6:
				$this->setKeterangan($value);
				break;
			case 7:
				$this->setUnitId($value);
				break;
			case 8:
				$this->setPenelitianSkala($value);
				break;
			case 9:
				$this->setPenelitianWaktu($value);
				break;
			case 10:
				$this->setBatasNilai($value);
				break;
			case 11:
				$this->setTenagaAhli($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = SubKegiatanPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setSubKegiatanId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setSubKegiatanName($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setParam($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setSatuan($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setStatus($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setPembagi($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setKeterangan($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setUnitId($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setPenelitianSkala($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setPenelitianWaktu($arr[$keys[9]]);
		if (array_key_exists($keys[10], $arr)) $this->setBatasNilai($arr[$keys[10]]);
		if (array_key_exists($keys[11], $arr)) $this->setTenagaAhli($arr[$keys[11]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(SubKegiatanPeer::DATABASE_NAME);

		if ($this->isColumnModified(SubKegiatanPeer::SUB_KEGIATAN_ID)) $criteria->add(SubKegiatanPeer::SUB_KEGIATAN_ID, $this->sub_kegiatan_id);
		if ($this->isColumnModified(SubKegiatanPeer::SUB_KEGIATAN_NAME)) $criteria->add(SubKegiatanPeer::SUB_KEGIATAN_NAME, $this->sub_kegiatan_name);
		if ($this->isColumnModified(SubKegiatanPeer::PARAM)) $criteria->add(SubKegiatanPeer::PARAM, $this->param);
		if ($this->isColumnModified(SubKegiatanPeer::SATUAN)) $criteria->add(SubKegiatanPeer::SATUAN, $this->satuan);
		if ($this->isColumnModified(SubKegiatanPeer::STATUS)) $criteria->add(SubKegiatanPeer::STATUS, $this->status);
		if ($this->isColumnModified(SubKegiatanPeer::PEMBAGI)) $criteria->add(SubKegiatanPeer::PEMBAGI, $this->pembagi);
		if ($this->isColumnModified(SubKegiatanPeer::KETERANGAN)) $criteria->add(SubKegiatanPeer::KETERANGAN, $this->keterangan);
		if ($this->isColumnModified(SubKegiatanPeer::UNIT_ID)) $criteria->add(SubKegiatanPeer::UNIT_ID, $this->unit_id);
		if ($this->isColumnModified(SubKegiatanPeer::PENELITIAN_SKALA)) $criteria->add(SubKegiatanPeer::PENELITIAN_SKALA, $this->penelitian_skala);
		if ($this->isColumnModified(SubKegiatanPeer::PENELITIAN_WAKTU)) $criteria->add(SubKegiatanPeer::PENELITIAN_WAKTU, $this->penelitian_waktu);
		if ($this->isColumnModified(SubKegiatanPeer::BATAS_NILAI)) $criteria->add(SubKegiatanPeer::BATAS_NILAI, $this->batas_nilai);
		if ($this->isColumnModified(SubKegiatanPeer::TENAGA_AHLI)) $criteria->add(SubKegiatanPeer::TENAGA_AHLI, $this->tenaga_ahli);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(SubKegiatanPeer::DATABASE_NAME);

		$criteria->add(SubKegiatanPeer::SUB_KEGIATAN_ID, $this->sub_kegiatan_id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getSubKegiatanId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setSubKegiatanId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setSubKegiatanName($this->sub_kegiatan_name);

		$copyObj->setParam($this->param);

		$copyObj->setSatuan($this->satuan);

		$copyObj->setStatus($this->status);

		$copyObj->setPembagi($this->pembagi);

		$copyObj->setKeterangan($this->keterangan);

		$copyObj->setUnitId($this->unit_id);

		$copyObj->setPenelitianSkala($this->penelitian_skala);

		$copyObj->setPenelitianWaktu($this->penelitian_waktu);

		$copyObj->setBatasNilai($this->batas_nilai);

		$copyObj->setTenagaAhli($this->tenaga_ahli);


		$copyObj->setNew(true);

		$copyObj->setSubKegiatanId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new SubKegiatanPeer();
		}
		return self::$peer;
	}

} 