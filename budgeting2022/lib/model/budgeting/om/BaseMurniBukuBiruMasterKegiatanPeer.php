<?php


abstract class BaseMurniBukuBiruMasterKegiatanPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.murni_bukubiru_master_kegiatan';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.MurniBukuBiruMasterKegiatan';

	
	const NUM_COLUMNS = 70;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const UNIT_ID = 'ebudget.murni_bukubiru_master_kegiatan.UNIT_ID';

	
	const KODE_KEGIATAN = 'ebudget.murni_bukubiru_master_kegiatan.KODE_KEGIATAN';

	
	const KODE_BIDANG = 'ebudget.murni_bukubiru_master_kegiatan.KODE_BIDANG';

	
	const KODE_URUSAN_WAJIB = 'ebudget.murni_bukubiru_master_kegiatan.KODE_URUSAN_WAJIB';

	
	const KODE_PROGRAM = 'ebudget.murni_bukubiru_master_kegiatan.KODE_PROGRAM';

	
	const KODE_SASARAN = 'ebudget.murni_bukubiru_master_kegiatan.KODE_SASARAN';

	
	const KODE_INDIKATOR = 'ebudget.murni_bukubiru_master_kegiatan.KODE_INDIKATOR';

	
	const ALOKASI_DANA = 'ebudget.murni_bukubiru_master_kegiatan.ALOKASI_DANA';

	
	const NAMA_KEGIATAN = 'ebudget.murni_bukubiru_master_kegiatan.NAMA_KEGIATAN';

	
	const MASUKAN = 'ebudget.murni_bukubiru_master_kegiatan.MASUKAN';

	
	const OUTPUT = 'ebudget.murni_bukubiru_master_kegiatan.OUTPUT';

	
	const OUTCOME = 'ebudget.murni_bukubiru_master_kegiatan.OUTCOME';

	
	const BENEFIT = 'ebudget.murni_bukubiru_master_kegiatan.BENEFIT';

	
	const IMPACT = 'ebudget.murni_bukubiru_master_kegiatan.IMPACT';

	
	const TIPE = 'ebudget.murni_bukubiru_master_kegiatan.TIPE';

	
	const KEGIATAN_ACTIVE = 'ebudget.murni_bukubiru_master_kegiatan.KEGIATAN_ACTIVE';

	
	const TO_KEGIATAN_CODE = 'ebudget.murni_bukubiru_master_kegiatan.TO_KEGIATAN_CODE';

	
	const CATATAN = 'ebudget.murni_bukubiru_master_kegiatan.CATATAN';

	
	const TARGET_OUTCOME = 'ebudget.murni_bukubiru_master_kegiatan.TARGET_OUTCOME';

	
	const LOKASI = 'ebudget.murni_bukubiru_master_kegiatan.LOKASI';

	
	const JUMLAH_PREV = 'ebudget.murni_bukubiru_master_kegiatan.JUMLAH_PREV';

	
	const JUMLAH_NOW = 'ebudget.murni_bukubiru_master_kegiatan.JUMLAH_NOW';

	
	const JUMLAH_NEXT = 'ebudget.murni_bukubiru_master_kegiatan.JUMLAH_NEXT';

	
	const KODE_PROGRAM2 = 'ebudget.murni_bukubiru_master_kegiatan.KODE_PROGRAM2';

	
	const KODE_URUSAN = 'ebudget.murni_bukubiru_master_kegiatan.KODE_URUSAN';

	
	const LAST_UPDATE_USER = 'ebudget.murni_bukubiru_master_kegiatan.LAST_UPDATE_USER';

	
	const LAST_UPDATE_TIME = 'ebudget.murni_bukubiru_master_kegiatan.LAST_UPDATE_TIME';

	
	const LAST_UPDATE_IP = 'ebudget.murni_bukubiru_master_kegiatan.LAST_UPDATE_IP';

	
	const TAHAP = 'ebudget.murni_bukubiru_master_kegiatan.TAHAP';

	
	const KODE_MISI = 'ebudget.murni_bukubiru_master_kegiatan.KODE_MISI';

	
	const KODE_TUJUAN = 'ebudget.murni_bukubiru_master_kegiatan.KODE_TUJUAN';

	
	const RANKING = 'ebudget.murni_bukubiru_master_kegiatan.RANKING';

	
	const NOMOR13 = 'ebudget.murni_bukubiru_master_kegiatan.NOMOR13';

	
	const PPA_NAMA = 'ebudget.murni_bukubiru_master_kegiatan.PPA_NAMA';

	
	const PPA_PANGKAT = 'ebudget.murni_bukubiru_master_kegiatan.PPA_PANGKAT';

	
	const PPA_NIP = 'ebudget.murni_bukubiru_master_kegiatan.PPA_NIP';

	
	const LANJUTAN = 'ebudget.murni_bukubiru_master_kegiatan.LANJUTAN';

	
	const USER_ID = 'ebudget.murni_bukubiru_master_kegiatan.USER_ID';

	
	const ID = 'ebudget.murni_bukubiru_master_kegiatan.ID';

	
	const TAHUN = 'ebudget.murni_bukubiru_master_kegiatan.TAHUN';

	
	const TAMBAHAN_PAGU = 'ebudget.murni_bukubiru_master_kegiatan.TAMBAHAN_PAGU';

	
	const GENDER = 'ebudget.murni_bukubiru_master_kegiatan.GENDER';

	
	const KODE_KEG_KEUANGAN = 'ebudget.murni_bukubiru_master_kegiatan.KODE_KEG_KEUANGAN';

	
	const USER_ID_LAMA = 'ebudget.murni_bukubiru_master_kegiatan.USER_ID_LAMA';

	
	const INDIKATOR = 'ebudget.murni_bukubiru_master_kegiatan.INDIKATOR';

	
	const IS_DAK = 'ebudget.murni_bukubiru_master_kegiatan.IS_DAK';

	
	const KODE_KEGIATAN_ASAL = 'ebudget.murni_bukubiru_master_kegiatan.KODE_KEGIATAN_ASAL';

	
	const KODE_KEG_KEUANGAN_ASAL = 'ebudget.murni_bukubiru_master_kegiatan.KODE_KEG_KEUANGAN_ASAL';

	
	const TH_KE_MULTIYEARS = 'ebudget.murni_bukubiru_master_kegiatan.TH_KE_MULTIYEARS';

	
	const KELOMPOK_SASARAN = 'ebudget.murni_bukubiru_master_kegiatan.KELOMPOK_SASARAN';

	
	const PAGU_BAPPEKO = 'ebudget.murni_bukubiru_master_kegiatan.PAGU_BAPPEKO';

	
	const KODE_DPA = 'ebudget.murni_bukubiru_master_kegiatan.KODE_DPA';

	
	const USER_ID_PPTK = 'ebudget.murni_bukubiru_master_kegiatan.USER_ID_PPTK';

	
	const USER_ID_KPA = 'ebudget.murni_bukubiru_master_kegiatan.USER_ID_KPA';

	
	const CATATAN_PEMBAHASAN = 'ebudget.murni_bukubiru_master_kegiatan.CATATAN_PEMBAHASAN';

	
	const CATATAN_PENYELIA = 'ebudget.murni_bukubiru_master_kegiatan.CATATAN_PENYELIA';

	
	const CATATAN_BAPPEKO = 'ebudget.murni_bukubiru_master_kegiatan.CATATAN_BAPPEKO';

	
	const STATUS_LEVEL = 'ebudget.murni_bukubiru_master_kegiatan.STATUS_LEVEL';

	
	const IS_TAPD_SETUJU = 'ebudget.murni_bukubiru_master_kegiatan.IS_TAPD_SETUJU';

	
	const IS_BAPPEKO_SETUJU = 'ebudget.murni_bukubiru_master_kegiatan.IS_BAPPEKO_SETUJU';

	
	const IS_PENYELIA_SETUJU = 'ebudget.murni_bukubiru_master_kegiatan.IS_PENYELIA_SETUJU';

	
	const IS_PERNAH_RKA = 'ebudget.murni_bukubiru_master_kegiatan.IS_PERNAH_RKA';

	
	const KODE_KEGIATAN_BARU = 'ebudget.murni_bukubiru_master_kegiatan.KODE_KEGIATAN_BARU';

	
	const VERIFIKASI_BPKPD = 'ebudget.murni_bukubiru_master_kegiatan.VERIFIKASI_BPKPD';

	
	const VERIFIKASI_BAPPEKO = 'ebudget.murni_bukubiru_master_kegiatan.VERIFIKASI_BAPPEKO';

	
	const VERIFIKASI_PENYELIA = 'ebudget.murni_bukubiru_master_kegiatan.VERIFIKASI_PENYELIA';

	
	const VERIFIKASI_BAGIAN_HUKUM = 'ebudget.murni_bukubiru_master_kegiatan.VERIFIKASI_BAGIAN_HUKUM';

	
	const VERIFIKASI_INSPEKTORAT = 'ebudget.murni_bukubiru_master_kegiatan.VERIFIKASI_INSPEKTORAT';

	
	const VERIFIKASI_BADAN_KEPEGAWAIAN = 'ebudget.murni_bukubiru_master_kegiatan.VERIFIKASI_BADAN_KEPEGAWAIAN';

	
	const VERIFIKASI_LPPA = 'ebudget.murni_bukubiru_master_kegiatan.VERIFIKASI_LPPA';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('UnitId', 'KodeKegiatan', 'KodeBidang', 'KodeUrusanWajib', 'KodeProgram', 'KodeSasaran', 'KodeIndikator', 'AlokasiDana', 'NamaKegiatan', 'Masukan', 'Output', 'Outcome', 'Benefit', 'Impact', 'Tipe', 'KegiatanActive', 'ToKegiatanCode', 'Catatan', 'TargetOutcome', 'Lokasi', 'JumlahPrev', 'JumlahNow', 'JumlahNext', 'KodeProgram2', 'KodeUrusan', 'LastUpdateUser', 'LastUpdateTime', 'LastUpdateIp', 'Tahap', 'KodeMisi', 'KodeTujuan', 'Ranking', 'Nomor13', 'PpaNama', 'PpaPangkat', 'PpaNip', 'Lanjutan', 'UserId', 'Id', 'Tahun', 'TambahanPagu', 'Gender', 'KodeKegKeuangan', 'UserIdLama', 'Indikator', 'IsDak', 'KodeKegiatanAsal', 'KodeKegKeuanganAsal', 'ThKeMultiyears', 'KelompokSasaran', 'PaguBappeko', 'KodeDpa', 'UserIdPptk', 'UserIdKpa', 'CatatanPembahasan', 'CatatanPenyelia', 'CatatanBappeko', 'StatusLevel', 'IsTapdSetuju', 'IsBappekoSetuju', 'IsPenyeliaSetuju', 'IsPernahRka', 'KodeKegiatanBaru', 'VerifikasiBpkpd', 'VerifikasiBappeko', 'VerifikasiPenyelia', 'VerifikasiBagianHukum', 'VerifikasiInspektorat', 'VerifikasiBadanKepegawaian', 'VerifikasiLppa', ),
		BasePeer::TYPE_COLNAME => array (MurniBukuBiruMasterKegiatanPeer::UNIT_ID, MurniBukuBiruMasterKegiatanPeer::KODE_KEGIATAN, MurniBukuBiruMasterKegiatanPeer::KODE_BIDANG, MurniBukuBiruMasterKegiatanPeer::KODE_URUSAN_WAJIB, MurniBukuBiruMasterKegiatanPeer::KODE_PROGRAM, MurniBukuBiruMasterKegiatanPeer::KODE_SASARAN, MurniBukuBiruMasterKegiatanPeer::KODE_INDIKATOR, MurniBukuBiruMasterKegiatanPeer::ALOKASI_DANA, MurniBukuBiruMasterKegiatanPeer::NAMA_KEGIATAN, MurniBukuBiruMasterKegiatanPeer::MASUKAN, MurniBukuBiruMasterKegiatanPeer::OUTPUT, MurniBukuBiruMasterKegiatanPeer::OUTCOME, MurniBukuBiruMasterKegiatanPeer::BENEFIT, MurniBukuBiruMasterKegiatanPeer::IMPACT, MurniBukuBiruMasterKegiatanPeer::TIPE, MurniBukuBiruMasterKegiatanPeer::KEGIATAN_ACTIVE, MurniBukuBiruMasterKegiatanPeer::TO_KEGIATAN_CODE, MurniBukuBiruMasterKegiatanPeer::CATATAN, MurniBukuBiruMasterKegiatanPeer::TARGET_OUTCOME, MurniBukuBiruMasterKegiatanPeer::LOKASI, MurniBukuBiruMasterKegiatanPeer::JUMLAH_PREV, MurniBukuBiruMasterKegiatanPeer::JUMLAH_NOW, MurniBukuBiruMasterKegiatanPeer::JUMLAH_NEXT, MurniBukuBiruMasterKegiatanPeer::KODE_PROGRAM2, MurniBukuBiruMasterKegiatanPeer::KODE_URUSAN, MurniBukuBiruMasterKegiatanPeer::LAST_UPDATE_USER, MurniBukuBiruMasterKegiatanPeer::LAST_UPDATE_TIME, MurniBukuBiruMasterKegiatanPeer::LAST_UPDATE_IP, MurniBukuBiruMasterKegiatanPeer::TAHAP, MurniBukuBiruMasterKegiatanPeer::KODE_MISI, MurniBukuBiruMasterKegiatanPeer::KODE_TUJUAN, MurniBukuBiruMasterKegiatanPeer::RANKING, MurniBukuBiruMasterKegiatanPeer::NOMOR13, MurniBukuBiruMasterKegiatanPeer::PPA_NAMA, MurniBukuBiruMasterKegiatanPeer::PPA_PANGKAT, MurniBukuBiruMasterKegiatanPeer::PPA_NIP, MurniBukuBiruMasterKegiatanPeer::LANJUTAN, MurniBukuBiruMasterKegiatanPeer::USER_ID, MurniBukuBiruMasterKegiatanPeer::ID, MurniBukuBiruMasterKegiatanPeer::TAHUN, MurniBukuBiruMasterKegiatanPeer::TAMBAHAN_PAGU, MurniBukuBiruMasterKegiatanPeer::GENDER, MurniBukuBiruMasterKegiatanPeer::KODE_KEG_KEUANGAN, MurniBukuBiruMasterKegiatanPeer::USER_ID_LAMA, MurniBukuBiruMasterKegiatanPeer::INDIKATOR, MurniBukuBiruMasterKegiatanPeer::IS_DAK, MurniBukuBiruMasterKegiatanPeer::KODE_KEGIATAN_ASAL, MurniBukuBiruMasterKegiatanPeer::KODE_KEG_KEUANGAN_ASAL, MurniBukuBiruMasterKegiatanPeer::TH_KE_MULTIYEARS, MurniBukuBiruMasterKegiatanPeer::KELOMPOK_SASARAN, MurniBukuBiruMasterKegiatanPeer::PAGU_BAPPEKO, MurniBukuBiruMasterKegiatanPeer::KODE_DPA, MurniBukuBiruMasterKegiatanPeer::USER_ID_PPTK, MurniBukuBiruMasterKegiatanPeer::USER_ID_KPA, MurniBukuBiruMasterKegiatanPeer::CATATAN_PEMBAHASAN, MurniBukuBiruMasterKegiatanPeer::CATATAN_PENYELIA, MurniBukuBiruMasterKegiatanPeer::CATATAN_BAPPEKO, MurniBukuBiruMasterKegiatanPeer::STATUS_LEVEL, MurniBukuBiruMasterKegiatanPeer::IS_TAPD_SETUJU, MurniBukuBiruMasterKegiatanPeer::IS_BAPPEKO_SETUJU, MurniBukuBiruMasterKegiatanPeer::IS_PENYELIA_SETUJU, MurniBukuBiruMasterKegiatanPeer::IS_PERNAH_RKA, MurniBukuBiruMasterKegiatanPeer::KODE_KEGIATAN_BARU, MurniBukuBiruMasterKegiatanPeer::VERIFIKASI_BPKPD, MurniBukuBiruMasterKegiatanPeer::VERIFIKASI_BAPPEKO, MurniBukuBiruMasterKegiatanPeer::VERIFIKASI_PENYELIA, MurniBukuBiruMasterKegiatanPeer::VERIFIKASI_BAGIAN_HUKUM, MurniBukuBiruMasterKegiatanPeer::VERIFIKASI_INSPEKTORAT, MurniBukuBiruMasterKegiatanPeer::VERIFIKASI_BADAN_KEPEGAWAIAN, MurniBukuBiruMasterKegiatanPeer::VERIFIKASI_LPPA, ),
		BasePeer::TYPE_FIELDNAME => array ('unit_id', 'kode_kegiatan', 'kode_bidang', 'kode_urusan_wajib', 'kode_program', 'kode_sasaran', 'kode_indikator', 'alokasi_dana', 'nama_kegiatan', 'masukan', 'output', 'outcome', 'benefit', 'impact', 'tipe', 'kegiatan_active', 'to_kegiatan_code', 'catatan', 'target_outcome', 'lokasi', 'jumlah_prev', 'jumlah_now', 'jumlah_next', 'kode_program2', 'kode_urusan', 'last_update_user', 'last_update_time', 'last_update_ip', 'tahap', 'kode_misi', 'kode_tujuan', 'ranking', 'nomor13', 'ppa_nama', 'ppa_pangkat', 'ppa_nip', 'lanjutan', 'user_id', 'id', 'tahun', 'tambahan_pagu', 'gender', 'kode_keg_keuangan', 'user_id_lama', 'indikator', 'is_dak', 'kode_kegiatan_asal', 'kode_keg_keuangan_asal', 'th_ke_multiyears', 'kelompok_sasaran', 'pagu_bappeko', 'kode_dpa', 'user_id_pptk', 'user_id_kpa', 'catatan_pembahasan', 'catatan_penyelia', 'catatan_bappeko', 'status_level', 'is_tapd_setuju', 'is_bappeko_setuju', 'is_penyelia_setuju', 'is_pernah_rka', 'kode_kegiatan_baru', 'verifikasi_bpkpd', 'verifikasi_bappeko', 'verifikasi_penyelia', 'verifikasi_bagian_hukum', 'verifikasi_inspektorat', 'verifikasi_badan_kepegawaian', 'verifikasi_lppa', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('UnitId' => 0, 'KodeKegiatan' => 1, 'KodeBidang' => 2, 'KodeUrusanWajib' => 3, 'KodeProgram' => 4, 'KodeSasaran' => 5, 'KodeIndikator' => 6, 'AlokasiDana' => 7, 'NamaKegiatan' => 8, 'Masukan' => 9, 'Output' => 10, 'Outcome' => 11, 'Benefit' => 12, 'Impact' => 13, 'Tipe' => 14, 'KegiatanActive' => 15, 'ToKegiatanCode' => 16, 'Catatan' => 17, 'TargetOutcome' => 18, 'Lokasi' => 19, 'JumlahPrev' => 20, 'JumlahNow' => 21, 'JumlahNext' => 22, 'KodeProgram2' => 23, 'KodeUrusan' => 24, 'LastUpdateUser' => 25, 'LastUpdateTime' => 26, 'LastUpdateIp' => 27, 'Tahap' => 28, 'KodeMisi' => 29, 'KodeTujuan' => 30, 'Ranking' => 31, 'Nomor13' => 32, 'PpaNama' => 33, 'PpaPangkat' => 34, 'PpaNip' => 35, 'Lanjutan' => 36, 'UserId' => 37, 'Id' => 38, 'Tahun' => 39, 'TambahanPagu' => 40, 'Gender' => 41, 'KodeKegKeuangan' => 42, 'UserIdLama' => 43, 'Indikator' => 44, 'IsDak' => 45, 'KodeKegiatanAsal' => 46, 'KodeKegKeuanganAsal' => 47, 'ThKeMultiyears' => 48, 'KelompokSasaran' => 49, 'PaguBappeko' => 50, 'KodeDpa' => 51, 'UserIdPptk' => 52, 'UserIdKpa' => 53, 'CatatanPembahasan' => 54, 'CatatanPenyelia' => 55, 'CatatanBappeko' => 56, 'StatusLevel' => 57, 'IsTapdSetuju' => 58, 'IsBappekoSetuju' => 59, 'IsPenyeliaSetuju' => 60, 'IsPernahRka' => 61, 'KodeKegiatanBaru' => 62, 'VerifikasiBpkpd' => 63, 'VerifikasiBappeko' => 64, 'VerifikasiPenyelia' => 65, 'VerifikasiBagianHukum' => 66, 'VerifikasiInspektorat' => 67, 'VerifikasiBadanKepegawaian' => 68, 'VerifikasiLppa' => 69, ),
		BasePeer::TYPE_COLNAME => array (MurniBukuBiruMasterKegiatanPeer::UNIT_ID => 0, MurniBukuBiruMasterKegiatanPeer::KODE_KEGIATAN => 1, MurniBukuBiruMasterKegiatanPeer::KODE_BIDANG => 2, MurniBukuBiruMasterKegiatanPeer::KODE_URUSAN_WAJIB => 3, MurniBukuBiruMasterKegiatanPeer::KODE_PROGRAM => 4, MurniBukuBiruMasterKegiatanPeer::KODE_SASARAN => 5, MurniBukuBiruMasterKegiatanPeer::KODE_INDIKATOR => 6, MurniBukuBiruMasterKegiatanPeer::ALOKASI_DANA => 7, MurniBukuBiruMasterKegiatanPeer::NAMA_KEGIATAN => 8, MurniBukuBiruMasterKegiatanPeer::MASUKAN => 9, MurniBukuBiruMasterKegiatanPeer::OUTPUT => 10, MurniBukuBiruMasterKegiatanPeer::OUTCOME => 11, MurniBukuBiruMasterKegiatanPeer::BENEFIT => 12, MurniBukuBiruMasterKegiatanPeer::IMPACT => 13, MurniBukuBiruMasterKegiatanPeer::TIPE => 14, MurniBukuBiruMasterKegiatanPeer::KEGIATAN_ACTIVE => 15, MurniBukuBiruMasterKegiatanPeer::TO_KEGIATAN_CODE => 16, MurniBukuBiruMasterKegiatanPeer::CATATAN => 17, MurniBukuBiruMasterKegiatanPeer::TARGET_OUTCOME => 18, MurniBukuBiruMasterKegiatanPeer::LOKASI => 19, MurniBukuBiruMasterKegiatanPeer::JUMLAH_PREV => 20, MurniBukuBiruMasterKegiatanPeer::JUMLAH_NOW => 21, MurniBukuBiruMasterKegiatanPeer::JUMLAH_NEXT => 22, MurniBukuBiruMasterKegiatanPeer::KODE_PROGRAM2 => 23, MurniBukuBiruMasterKegiatanPeer::KODE_URUSAN => 24, MurniBukuBiruMasterKegiatanPeer::LAST_UPDATE_USER => 25, MurniBukuBiruMasterKegiatanPeer::LAST_UPDATE_TIME => 26, MurniBukuBiruMasterKegiatanPeer::LAST_UPDATE_IP => 27, MurniBukuBiruMasterKegiatanPeer::TAHAP => 28, MurniBukuBiruMasterKegiatanPeer::KODE_MISI => 29, MurniBukuBiruMasterKegiatanPeer::KODE_TUJUAN => 30, MurniBukuBiruMasterKegiatanPeer::RANKING => 31, MurniBukuBiruMasterKegiatanPeer::NOMOR13 => 32, MurniBukuBiruMasterKegiatanPeer::PPA_NAMA => 33, MurniBukuBiruMasterKegiatanPeer::PPA_PANGKAT => 34, MurniBukuBiruMasterKegiatanPeer::PPA_NIP => 35, MurniBukuBiruMasterKegiatanPeer::LANJUTAN => 36, MurniBukuBiruMasterKegiatanPeer::USER_ID => 37, MurniBukuBiruMasterKegiatanPeer::ID => 38, MurniBukuBiruMasterKegiatanPeer::TAHUN => 39, MurniBukuBiruMasterKegiatanPeer::TAMBAHAN_PAGU => 40, MurniBukuBiruMasterKegiatanPeer::GENDER => 41, MurniBukuBiruMasterKegiatanPeer::KODE_KEG_KEUANGAN => 42, MurniBukuBiruMasterKegiatanPeer::USER_ID_LAMA => 43, MurniBukuBiruMasterKegiatanPeer::INDIKATOR => 44, MurniBukuBiruMasterKegiatanPeer::IS_DAK => 45, MurniBukuBiruMasterKegiatanPeer::KODE_KEGIATAN_ASAL => 46, MurniBukuBiruMasterKegiatanPeer::KODE_KEG_KEUANGAN_ASAL => 47, MurniBukuBiruMasterKegiatanPeer::TH_KE_MULTIYEARS => 48, MurniBukuBiruMasterKegiatanPeer::KELOMPOK_SASARAN => 49, MurniBukuBiruMasterKegiatanPeer::PAGU_BAPPEKO => 50, MurniBukuBiruMasterKegiatanPeer::KODE_DPA => 51, MurniBukuBiruMasterKegiatanPeer::USER_ID_PPTK => 52, MurniBukuBiruMasterKegiatanPeer::USER_ID_KPA => 53, MurniBukuBiruMasterKegiatanPeer::CATATAN_PEMBAHASAN => 54, MurniBukuBiruMasterKegiatanPeer::CATATAN_PENYELIA => 55, MurniBukuBiruMasterKegiatanPeer::CATATAN_BAPPEKO => 56, MurniBukuBiruMasterKegiatanPeer::STATUS_LEVEL => 57, MurniBukuBiruMasterKegiatanPeer::IS_TAPD_SETUJU => 58, MurniBukuBiruMasterKegiatanPeer::IS_BAPPEKO_SETUJU => 59, MurniBukuBiruMasterKegiatanPeer::IS_PENYELIA_SETUJU => 60, MurniBukuBiruMasterKegiatanPeer::IS_PERNAH_RKA => 61, MurniBukuBiruMasterKegiatanPeer::KODE_KEGIATAN_BARU => 62, MurniBukuBiruMasterKegiatanPeer::VERIFIKASI_BPKPD => 63, MurniBukuBiruMasterKegiatanPeer::VERIFIKASI_BAPPEKO => 64, MurniBukuBiruMasterKegiatanPeer::VERIFIKASI_PENYELIA => 65, MurniBukuBiruMasterKegiatanPeer::VERIFIKASI_BAGIAN_HUKUM => 66, MurniBukuBiruMasterKegiatanPeer::VERIFIKASI_INSPEKTORAT => 67, MurniBukuBiruMasterKegiatanPeer::VERIFIKASI_BADAN_KEPEGAWAIAN => 68, MurniBukuBiruMasterKegiatanPeer::VERIFIKASI_LPPA => 69, ),
		BasePeer::TYPE_FIELDNAME => array ('unit_id' => 0, 'kode_kegiatan' => 1, 'kode_bidang' => 2, 'kode_urusan_wajib' => 3, 'kode_program' => 4, 'kode_sasaran' => 5, 'kode_indikator' => 6, 'alokasi_dana' => 7, 'nama_kegiatan' => 8, 'masukan' => 9, 'output' => 10, 'outcome' => 11, 'benefit' => 12, 'impact' => 13, 'tipe' => 14, 'kegiatan_active' => 15, 'to_kegiatan_code' => 16, 'catatan' => 17, 'target_outcome' => 18, 'lokasi' => 19, 'jumlah_prev' => 20, 'jumlah_now' => 21, 'jumlah_next' => 22, 'kode_program2' => 23, 'kode_urusan' => 24, 'last_update_user' => 25, 'last_update_time' => 26, 'last_update_ip' => 27, 'tahap' => 28, 'kode_misi' => 29, 'kode_tujuan' => 30, 'ranking' => 31, 'nomor13' => 32, 'ppa_nama' => 33, 'ppa_pangkat' => 34, 'ppa_nip' => 35, 'lanjutan' => 36, 'user_id' => 37, 'id' => 38, 'tahun' => 39, 'tambahan_pagu' => 40, 'gender' => 41, 'kode_keg_keuangan' => 42, 'user_id_lama' => 43, 'indikator' => 44, 'is_dak' => 45, 'kode_kegiatan_asal' => 46, 'kode_keg_keuangan_asal' => 47, 'th_ke_multiyears' => 48, 'kelompok_sasaran' => 49, 'pagu_bappeko' => 50, 'kode_dpa' => 51, 'user_id_pptk' => 52, 'user_id_kpa' => 53, 'catatan_pembahasan' => 54, 'catatan_penyelia' => 55, 'catatan_bappeko' => 56, 'status_level' => 57, 'is_tapd_setuju' => 58, 'is_bappeko_setuju' => 59, 'is_penyelia_setuju' => 60, 'is_pernah_rka' => 61, 'kode_kegiatan_baru' => 62, 'verifikasi_bpkpd' => 63, 'verifikasi_bappeko' => 64, 'verifikasi_penyelia' => 65, 'verifikasi_bagian_hukum' => 66, 'verifikasi_inspektorat' => 67, 'verifikasi_badan_kepegawaian' => 68, 'verifikasi_lppa' => 69, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/MurniBukuBiruMasterKegiatanMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.MurniBukuBiruMasterKegiatanMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = MurniBukuBiruMasterKegiatanPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(MurniBukuBiruMasterKegiatanPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(MurniBukuBiruMasterKegiatanPeer::UNIT_ID);

		$criteria->addSelectColumn(MurniBukuBiruMasterKegiatanPeer::KODE_KEGIATAN);

		$criteria->addSelectColumn(MurniBukuBiruMasterKegiatanPeer::KODE_BIDANG);

		$criteria->addSelectColumn(MurniBukuBiruMasterKegiatanPeer::KODE_URUSAN_WAJIB);

		$criteria->addSelectColumn(MurniBukuBiruMasterKegiatanPeer::KODE_PROGRAM);

		$criteria->addSelectColumn(MurniBukuBiruMasterKegiatanPeer::KODE_SASARAN);

		$criteria->addSelectColumn(MurniBukuBiruMasterKegiatanPeer::KODE_INDIKATOR);

		$criteria->addSelectColumn(MurniBukuBiruMasterKegiatanPeer::ALOKASI_DANA);

		$criteria->addSelectColumn(MurniBukuBiruMasterKegiatanPeer::NAMA_KEGIATAN);

		$criteria->addSelectColumn(MurniBukuBiruMasterKegiatanPeer::MASUKAN);

		$criteria->addSelectColumn(MurniBukuBiruMasterKegiatanPeer::OUTPUT);

		$criteria->addSelectColumn(MurniBukuBiruMasterKegiatanPeer::OUTCOME);

		$criteria->addSelectColumn(MurniBukuBiruMasterKegiatanPeer::BENEFIT);

		$criteria->addSelectColumn(MurniBukuBiruMasterKegiatanPeer::IMPACT);

		$criteria->addSelectColumn(MurniBukuBiruMasterKegiatanPeer::TIPE);

		$criteria->addSelectColumn(MurniBukuBiruMasterKegiatanPeer::KEGIATAN_ACTIVE);

		$criteria->addSelectColumn(MurniBukuBiruMasterKegiatanPeer::TO_KEGIATAN_CODE);

		$criteria->addSelectColumn(MurniBukuBiruMasterKegiatanPeer::CATATAN);

		$criteria->addSelectColumn(MurniBukuBiruMasterKegiatanPeer::TARGET_OUTCOME);

		$criteria->addSelectColumn(MurniBukuBiruMasterKegiatanPeer::LOKASI);

		$criteria->addSelectColumn(MurniBukuBiruMasterKegiatanPeer::JUMLAH_PREV);

		$criteria->addSelectColumn(MurniBukuBiruMasterKegiatanPeer::JUMLAH_NOW);

		$criteria->addSelectColumn(MurniBukuBiruMasterKegiatanPeer::JUMLAH_NEXT);

		$criteria->addSelectColumn(MurniBukuBiruMasterKegiatanPeer::KODE_PROGRAM2);

		$criteria->addSelectColumn(MurniBukuBiruMasterKegiatanPeer::KODE_URUSAN);

		$criteria->addSelectColumn(MurniBukuBiruMasterKegiatanPeer::LAST_UPDATE_USER);

		$criteria->addSelectColumn(MurniBukuBiruMasterKegiatanPeer::LAST_UPDATE_TIME);

		$criteria->addSelectColumn(MurniBukuBiruMasterKegiatanPeer::LAST_UPDATE_IP);

		$criteria->addSelectColumn(MurniBukuBiruMasterKegiatanPeer::TAHAP);

		$criteria->addSelectColumn(MurniBukuBiruMasterKegiatanPeer::KODE_MISI);

		$criteria->addSelectColumn(MurniBukuBiruMasterKegiatanPeer::KODE_TUJUAN);

		$criteria->addSelectColumn(MurniBukuBiruMasterKegiatanPeer::RANKING);

		$criteria->addSelectColumn(MurniBukuBiruMasterKegiatanPeer::NOMOR13);

		$criteria->addSelectColumn(MurniBukuBiruMasterKegiatanPeer::PPA_NAMA);

		$criteria->addSelectColumn(MurniBukuBiruMasterKegiatanPeer::PPA_PANGKAT);

		$criteria->addSelectColumn(MurniBukuBiruMasterKegiatanPeer::PPA_NIP);

		$criteria->addSelectColumn(MurniBukuBiruMasterKegiatanPeer::LANJUTAN);

		$criteria->addSelectColumn(MurniBukuBiruMasterKegiatanPeer::USER_ID);

		$criteria->addSelectColumn(MurniBukuBiruMasterKegiatanPeer::ID);

		$criteria->addSelectColumn(MurniBukuBiruMasterKegiatanPeer::TAHUN);

		$criteria->addSelectColumn(MurniBukuBiruMasterKegiatanPeer::TAMBAHAN_PAGU);

		$criteria->addSelectColumn(MurniBukuBiruMasterKegiatanPeer::GENDER);

		$criteria->addSelectColumn(MurniBukuBiruMasterKegiatanPeer::KODE_KEG_KEUANGAN);

		$criteria->addSelectColumn(MurniBukuBiruMasterKegiatanPeer::USER_ID_LAMA);

		$criteria->addSelectColumn(MurniBukuBiruMasterKegiatanPeer::INDIKATOR);

		$criteria->addSelectColumn(MurniBukuBiruMasterKegiatanPeer::IS_DAK);

		$criteria->addSelectColumn(MurniBukuBiruMasterKegiatanPeer::KODE_KEGIATAN_ASAL);

		$criteria->addSelectColumn(MurniBukuBiruMasterKegiatanPeer::KODE_KEG_KEUANGAN_ASAL);

		$criteria->addSelectColumn(MurniBukuBiruMasterKegiatanPeer::TH_KE_MULTIYEARS);

		$criteria->addSelectColumn(MurniBukuBiruMasterKegiatanPeer::KELOMPOK_SASARAN);

		$criteria->addSelectColumn(MurniBukuBiruMasterKegiatanPeer::PAGU_BAPPEKO);

		$criteria->addSelectColumn(MurniBukuBiruMasterKegiatanPeer::KODE_DPA);

		$criteria->addSelectColumn(MurniBukuBiruMasterKegiatanPeer::USER_ID_PPTK);

		$criteria->addSelectColumn(MurniBukuBiruMasterKegiatanPeer::USER_ID_KPA);

		$criteria->addSelectColumn(MurniBukuBiruMasterKegiatanPeer::CATATAN_PEMBAHASAN);

		$criteria->addSelectColumn(MurniBukuBiruMasterKegiatanPeer::CATATAN_PENYELIA);

		$criteria->addSelectColumn(MurniBukuBiruMasterKegiatanPeer::CATATAN_BAPPEKO);

		$criteria->addSelectColumn(MurniBukuBiruMasterKegiatanPeer::STATUS_LEVEL);

		$criteria->addSelectColumn(MurniBukuBiruMasterKegiatanPeer::IS_TAPD_SETUJU);

		$criteria->addSelectColumn(MurniBukuBiruMasterKegiatanPeer::IS_BAPPEKO_SETUJU);

		$criteria->addSelectColumn(MurniBukuBiruMasterKegiatanPeer::IS_PENYELIA_SETUJU);

		$criteria->addSelectColumn(MurniBukuBiruMasterKegiatanPeer::IS_PERNAH_RKA);

		$criteria->addSelectColumn(MurniBukuBiruMasterKegiatanPeer::KODE_KEGIATAN_BARU);

		$criteria->addSelectColumn(MurniBukuBiruMasterKegiatanPeer::VERIFIKASI_BPKPD);

		$criteria->addSelectColumn(MurniBukuBiruMasterKegiatanPeer::VERIFIKASI_BAPPEKO);

		$criteria->addSelectColumn(MurniBukuBiruMasterKegiatanPeer::VERIFIKASI_PENYELIA);

		$criteria->addSelectColumn(MurniBukuBiruMasterKegiatanPeer::VERIFIKASI_BAGIAN_HUKUM);

		$criteria->addSelectColumn(MurniBukuBiruMasterKegiatanPeer::VERIFIKASI_INSPEKTORAT);

		$criteria->addSelectColumn(MurniBukuBiruMasterKegiatanPeer::VERIFIKASI_BADAN_KEPEGAWAIAN);

		$criteria->addSelectColumn(MurniBukuBiruMasterKegiatanPeer::VERIFIKASI_LPPA);

	}

	const COUNT = 'COUNT(ebudget.murni_bukubiru_master_kegiatan.UNIT_ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.murni_bukubiru_master_kegiatan.UNIT_ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(MurniBukuBiruMasterKegiatanPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(MurniBukuBiruMasterKegiatanPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = MurniBukuBiruMasterKegiatanPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = MurniBukuBiruMasterKegiatanPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return MurniBukuBiruMasterKegiatanPeer::populateObjects(MurniBukuBiruMasterKegiatanPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			MurniBukuBiruMasterKegiatanPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = MurniBukuBiruMasterKegiatanPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return MurniBukuBiruMasterKegiatanPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}

		$criteria->remove(MurniBukuBiruMasterKegiatanPeer::ID); 

				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(MurniBukuBiruMasterKegiatanPeer::UNIT_ID);
			$selectCriteria->add(MurniBukuBiruMasterKegiatanPeer::UNIT_ID, $criteria->remove(MurniBukuBiruMasterKegiatanPeer::UNIT_ID), $comparison);

			$comparison = $criteria->getComparison(MurniBukuBiruMasterKegiatanPeer::KODE_KEGIATAN);
			$selectCriteria->add(MurniBukuBiruMasterKegiatanPeer::KODE_KEGIATAN, $criteria->remove(MurniBukuBiruMasterKegiatanPeer::KODE_KEGIATAN), $comparison);

			$comparison = $criteria->getComparison(MurniBukuBiruMasterKegiatanPeer::ID);
			$selectCriteria->add(MurniBukuBiruMasterKegiatanPeer::ID, $criteria->remove(MurniBukuBiruMasterKegiatanPeer::ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(MurniBukuBiruMasterKegiatanPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(MurniBukuBiruMasterKegiatanPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof MurniBukuBiruMasterKegiatan) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
												if(count($values) == count($values, COUNT_RECURSIVE))
			{
								$values = array($values);
			}
			$vals = array();
			foreach($values as $value)
			{

				$vals[0][] = $value[0];
				$vals[1][] = $value[1];
				$vals[2][] = $value[2];
			}

			$criteria->add(MurniBukuBiruMasterKegiatanPeer::UNIT_ID, $vals[0], Criteria::IN);
			$criteria->add(MurniBukuBiruMasterKegiatanPeer::KODE_KEGIATAN, $vals[1], Criteria::IN);
			$criteria->add(MurniBukuBiruMasterKegiatanPeer::ID, $vals[2], Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(MurniBukuBiruMasterKegiatan $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(MurniBukuBiruMasterKegiatanPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(MurniBukuBiruMasterKegiatanPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(MurniBukuBiruMasterKegiatanPeer::DATABASE_NAME, MurniBukuBiruMasterKegiatanPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = MurniBukuBiruMasterKegiatanPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK( $unit_id, $kode_kegiatan, $id, $con = null) {
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$criteria = new Criteria();
		$criteria->add(MurniBukuBiruMasterKegiatanPeer::UNIT_ID, $unit_id);
		$criteria->add(MurniBukuBiruMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
		$criteria->add(MurniBukuBiruMasterKegiatanPeer::ID, $id);
		$v = MurniBukuBiruMasterKegiatanPeer::doSelect($criteria, $con);

		return !empty($v) ? $v[0] : null;
	}
} 
if (Propel::isInit()) {
			try {
		BaseMurniBukuBiruMasterKegiatanPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/MurniBukuBiruMasterKegiatanMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.MurniBukuBiruMasterKegiatanMapBuilder');
}
