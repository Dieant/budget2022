<?php


abstract class BasePrintKoefisienPeer {

	
	const DATABASE_NAME = 'budgeting';

	
	const TABLE_NAME = 'ebudget.print_koefisien';

	
	const CLASS_DEFAULT = 'lib.model.budgeting.PrintKoefisien';

	
	const NUM_COLUMNS = 5;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const UNIT_ID = 'ebudget.print_koefisien.UNIT_ID';

	
	const KEGIATAN_CODE = 'ebudget.print_koefisien.KEGIATAN_CODE';

	
	const PRINT_NO = 'ebudget.print_koefisien.PRINT_NO';

	
	const KETERANGAN = 'ebudget.print_koefisien.KETERANGAN';

	
	const TGL_PRINT = 'ebudget.print_koefisien.TGL_PRINT';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('UnitId', 'KegiatanCode', 'PrintNo', 'Keterangan', 'TglPrint', ),
		BasePeer::TYPE_COLNAME => array (PrintKoefisienPeer::UNIT_ID, PrintKoefisienPeer::KEGIATAN_CODE, PrintKoefisienPeer::PRINT_NO, PrintKoefisienPeer::KETERANGAN, PrintKoefisienPeer::TGL_PRINT, ),
		BasePeer::TYPE_FIELDNAME => array ('unit_id', 'kegiatan_code', 'print_no', 'keterangan', 'tgl_print', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('UnitId' => 0, 'KegiatanCode' => 1, 'PrintNo' => 2, 'Keterangan' => 3, 'TglPrint' => 4, ),
		BasePeer::TYPE_COLNAME => array (PrintKoefisienPeer::UNIT_ID => 0, PrintKoefisienPeer::KEGIATAN_CODE => 1, PrintKoefisienPeer::PRINT_NO => 2, PrintKoefisienPeer::KETERANGAN => 3, PrintKoefisienPeer::TGL_PRINT => 4, ),
		BasePeer::TYPE_FIELDNAME => array ('unit_id' => 0, 'kegiatan_code' => 1, 'print_no' => 2, 'keterangan' => 3, 'tgl_print' => 4, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/budgeting/map/PrintKoefisienMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.budgeting.map.PrintKoefisienMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = PrintKoefisienPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(PrintKoefisienPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(PrintKoefisienPeer::UNIT_ID);

		$criteria->addSelectColumn(PrintKoefisienPeer::KEGIATAN_CODE);

		$criteria->addSelectColumn(PrintKoefisienPeer::PRINT_NO);

		$criteria->addSelectColumn(PrintKoefisienPeer::KETERANGAN);

		$criteria->addSelectColumn(PrintKoefisienPeer::TGL_PRINT);

	}

	const COUNT = 'COUNT(ebudget.print_koefisien.UNIT_ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT ebudget.print_koefisien.UNIT_ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(PrintKoefisienPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(PrintKoefisienPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = PrintKoefisienPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = PrintKoefisienPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return PrintKoefisienPeer::populateObjects(PrintKoefisienPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			PrintKoefisienPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = PrintKoefisienPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return PrintKoefisienPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(PrintKoefisienPeer::UNIT_ID);
			$selectCriteria->add(PrintKoefisienPeer::UNIT_ID, $criteria->remove(PrintKoefisienPeer::UNIT_ID), $comparison);

			$comparison = $criteria->getComparison(PrintKoefisienPeer::KEGIATAN_CODE);
			$selectCriteria->add(PrintKoefisienPeer::KEGIATAN_CODE, $criteria->remove(PrintKoefisienPeer::KEGIATAN_CODE), $comparison);

			$comparison = $criteria->getComparison(PrintKoefisienPeer::PRINT_NO);
			$selectCriteria->add(PrintKoefisienPeer::PRINT_NO, $criteria->remove(PrintKoefisienPeer::PRINT_NO), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(PrintKoefisienPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(PrintKoefisienPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof PrintKoefisien) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
												if(count($values) == count($values, COUNT_RECURSIVE))
			{
								$values = array($values);
			}
			$vals = array();
			foreach($values as $value)
			{

				$vals[0][] = $value[0];
				$vals[1][] = $value[1];
				$vals[2][] = $value[2];
			}

			$criteria->add(PrintKoefisienPeer::UNIT_ID, $vals[0], Criteria::IN);
			$criteria->add(PrintKoefisienPeer::KEGIATAN_CODE, $vals[1], Criteria::IN);
			$criteria->add(PrintKoefisienPeer::PRINT_NO, $vals[2], Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(PrintKoefisien $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(PrintKoefisienPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(PrintKoefisienPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(PrintKoefisienPeer::DATABASE_NAME, PrintKoefisienPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = PrintKoefisienPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK( $unit_id, $kegiatan_code, $print_no, $con = null) {
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$criteria = new Criteria();
		$criteria->add(PrintKoefisienPeer::UNIT_ID, $unit_id);
		$criteria->add(PrintKoefisienPeer::KEGIATAN_CODE, $kegiatan_code);
		$criteria->add(PrintKoefisienPeer::PRINT_NO, $print_no);
		$v = PrintKoefisienPeer::doSelect($criteria, $con);

		return !empty($v) ? $v[0] : null;
	}
} 
if (Propel::isInit()) {
			try {
		BasePrintKoefisienPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/budgeting/map/PrintKoefisienMapBuilder.php';
	Propel::registerMapBuilder('lib.model.budgeting.map.PrintKoefisienMapBuilder');
}
