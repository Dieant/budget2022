<?php


abstract class BaseMasterUrusan extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $kode_urusan;


	
	protected $nama_urusan;


	
	protected $ranking;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getKodeUrusan()
	{

		return $this->kode_urusan;
	}

	
	public function getNamaUrusan()
	{

		return $this->nama_urusan;
	}

	
	public function getRanking()
	{

		return $this->ranking;
	}

	
	public function setKodeUrusan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_urusan !== $v) {
			$this->kode_urusan = $v;
			$this->modifiedColumns[] = MasterUrusanPeer::KODE_URUSAN;
		}

	} 
	
	public function setNamaUrusan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nama_urusan !== $v) {
			$this->nama_urusan = $v;
			$this->modifiedColumns[] = MasterUrusanPeer::NAMA_URUSAN;
		}

	} 
	
	public function setRanking($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->ranking !== $v) {
			$this->ranking = $v;
			$this->modifiedColumns[] = MasterUrusanPeer::RANKING;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->kode_urusan = $rs->getString($startcol + 0);

			$this->nama_urusan = $rs->getString($startcol + 1);

			$this->ranking = $rs->getInt($startcol + 2);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 3; 
		} catch (Exception $e) {
			throw new PropelException("Error populating MasterUrusan object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(MasterUrusanPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			MasterUrusanPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(MasterUrusanPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = MasterUrusanPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += MasterUrusanPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = MasterUrusanPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = MasterUrusanPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getKodeUrusan();
				break;
			case 1:
				return $this->getNamaUrusan();
				break;
			case 2:
				return $this->getRanking();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = MasterUrusanPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getKodeUrusan(),
			$keys[1] => $this->getNamaUrusan(),
			$keys[2] => $this->getRanking(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = MasterUrusanPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setKodeUrusan($value);
				break;
			case 1:
				$this->setNamaUrusan($value);
				break;
			case 2:
				$this->setRanking($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = MasterUrusanPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setKodeUrusan($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setNamaUrusan($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setRanking($arr[$keys[2]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(MasterUrusanPeer::DATABASE_NAME);

		if ($this->isColumnModified(MasterUrusanPeer::KODE_URUSAN)) $criteria->add(MasterUrusanPeer::KODE_URUSAN, $this->kode_urusan);
		if ($this->isColumnModified(MasterUrusanPeer::NAMA_URUSAN)) $criteria->add(MasterUrusanPeer::NAMA_URUSAN, $this->nama_urusan);
		if ($this->isColumnModified(MasterUrusanPeer::RANKING)) $criteria->add(MasterUrusanPeer::RANKING, $this->ranking);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(MasterUrusanPeer::DATABASE_NAME);

		$criteria->add(MasterUrusanPeer::KODE_URUSAN, $this->kode_urusan);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getKodeUrusan();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setKodeUrusan($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setNamaUrusan($this->nama_urusan);

		$copyObj->setRanking($this->ranking);


		$copyObj->setNew(true);

		$copyObj->setKodeUrusan(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new MasterUrusanPeer();
		}
		return self::$peer;
	}

} 