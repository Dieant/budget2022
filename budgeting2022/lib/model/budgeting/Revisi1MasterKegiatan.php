<?php

/**
 * Subclass for representing a row from the 'budget2012.revisi1_master_kegiatan' table.
 *
 * 
 *
 * @package lib.model.budgeting
 */ 
class Revisi1MasterKegiatan extends BaseRevisi1MasterKegiatan
{
    public function getKodeNamaKegiatan()
    {
        return $this->getKodeKegiatan().'-'.$this->getNamaKegiatan();
    }
}
