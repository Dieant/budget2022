<?php

/**
 * Subclass for representing a row from the 'budget2012.revisi3_master_kegiatan' table.
 *
 * 
 *
 * @package lib.model.budgeting
 */ 
class Revisi3MasterKegiatan extends BaseRevisi3MasterKegiatan
{
    public function getKodeNamaKegiatan()
    {
        return $this->getKodeKegiatan().'-'.$this->getNamaKegiatan();
    }
}
