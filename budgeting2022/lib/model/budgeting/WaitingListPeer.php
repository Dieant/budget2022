<?php

/**
 * Subclass for performing query and update operations on the 'ebudget.waitinglist' table.
 *
 * 
 *
 * @package lib.model.budgeting
 */ 
class WaitingListPeer extends BaseWaitingListPeer
{
}
