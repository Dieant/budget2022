<?php

/**
 * Subclass for performing query and update operations on the 'ebudget.usulan_spjm' table.
 *
 * 
 *
 * @package lib.model.budgeting
 */
class UsulanSPJMPeer extends BaseUsulanSPJMPeer {
    public static function getStringPenyelia($IdSpjm, $con = null) {
        if ($con === null) {
            $con = Propel::getConnection(self::DATABASE_NAME);
        }
        $c = new Criteria();
        $c->addSelectColumn(self::PENYELIA);
        $c->add(UsulanSPJMPeer::ID_SPJM, $IdSpjm, Criteria::EQUAL);
        $rs = UsulanSPJMPeer::doSelectRS($c, $con);
        if ($rs->next()) {
            $rt = $rs->get(1);
        }

        return $rt;
    }    
    
    public static function getStringNomorSurat($IdSpjm, $con = null) {
        if ($con === null) {
            $con = Propel::getConnection(self::DATABASE_NAME);
        }
        $c = new Criteria();
        $c->addSelectColumn(self::NOMOR_SURAT);
        $c->add(UsulanSPJMPeer::ID_SPJM, $IdSpjm, Criteria::EQUAL);
        $rs = UsulanSPJMPeer::doSelectRS($c, $con);
        if ($rs->next()) {
            $rt = $rs->get(1);
        }

        return $rt;
    }    
    
    public static function getStringFilepath($IdSpjm, $con = null) {
        if ($con === null) {
            $con = Propel::getConnection(self::DATABASE_NAME);
        }
        $c = new Criteria();
        $c->addSelectColumn(self::FILEPATH);
        $c->add(UsulanSPJMPeer::ID_SPJM, $IdSpjm, Criteria::EQUAL);
        $rs = UsulanSPJMPeer::doSelectRS($c, $con);
        if ($rs->next()) {
            $rt = $rs->get(1);
        }

        return $rt;
    }    
}
