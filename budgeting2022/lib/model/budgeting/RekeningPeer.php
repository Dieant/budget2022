<?php

/**
 * Subclass for performing query and update operations on the 'budget2010.rekening' table.
 *
 * 
 *
 * @package lib.model.budget2010
 */
class RekeningPeer extends BaseRekeningPeer {
    public static function getStringRekeningName($rekening_code, $con = null) {
        if ($con === null) {
            $con = Propel::getConnection(self::DATABASE_NAME);
        }
        $c = new Criteria();
        $c->addSelectColumn(self::REKENING_NAME);
        $c->add(RekeningPeer::REKENING_CODE, str_replace(',', '', $rekening_code), Criteria::EQUAL);
        $rs = RekeningPeer::doSelectRS($c, $con);
        if ($rs->next()) {
            $rt = $rs->get(1);
        }

        return $rt;
    }
}
