<?php

/**
 * Subclass for representing a row from the 'ebudget.dinas_rincian_detail' table.
 *
 * 
 *
 * @package lib.model.budgeting
 */
class DinasRincianDetail extends BaseDinasRincianDetail {

//    public function cekEstimasiFisik($komponen_id) {
//        $query = "select kode_barang from " . sfConfig::get('app_default_schema') . ".usulan_ssh 
//                where id_usulan = (select id_usulan from " . sfConfig::get('app_default_schema') . ".usulan_verifikasi 
//                        where shsd_name = (select komponen_name from " . sfConfig::get('app_default_schema') . ".komponen 
//                                where komponen_id='$komponen_id' and usulan_skpd<>''))";
//        $con = Propel::getConnection();
//        $stmt = $con->prepareStatement($query);
//        $rs = $stmt->executeQuery();
//        if ($rs->next()) {
//            $kode_barang = $rs->getString('kode_barang');
//            if ($kode_barang == '23.06.01.01') {
//                return TRUE;
//            }
//        }
//        return FALSE;
//    }

    public function getRealisasiMetodeLelang($unit_id, $kode_kegiatan, $detail_no) {
        $con = Propel::getConnection();
        $query = "select sum(nilai) as nilai
                    from (
                            select k.id, coalesce((mc.harga_realisasi_bulat + mc.lk + mc.biaya_lain), (dkom.harga_realisasi_bulat + dkom.lk + dkom.biaya_lain)) as nilai
                            from " . sfConfig::get('app_default_edelivery') . ".detail_komponen dkom
                            left join " . sfConfig::get('app_default_edelivery') . ".det_kom_mc mc on mc.id = dkom.id
                            join " . sfConfig::get('app_default_edelivery') . ".kontraks k on k.id = dkom.kontrak_id
                            join " . sfConfig::get('app_default_eproject') . ".detail_kegiatan dk on dk.id = dkom.detail_kegiatan_id
                            join (select k.id, pk.termin_ke
                                    from " . sfConfig::get('app_default_edelivery') . ".kontraks k 
                                    join " . sfConfig::get('app_default_eproject') . ".pekerjaan p on p.id = k.pekerjaan_id
                                    join " . sfConfig::get('app_default_eproject') . ".detail_pekerjaan dp on dp.pekerjaan_id = p.id
                                    join " . sfConfig::get('app_default_eproject') . ".detail_kegiatan dk on dk.id = dp.detail_kegiatan_id
                                    left join " . sfConfig::get('app_default_edelivery') . ".pemutusan_kontrak pk on pk.kontrak_id = k.id
                                    where k.metode in (3,4,12,13,14,15,16) and k.status=110 and dk.kode_detail_kegiatan = '$unit_id.$kode_kegiatan.$detail_no'
                                    group by k.id, pk.termin_ke) a ON k.id = a.id and dkom.termin_ke <= a.termin_ke
                            where k.metode in (3,4,12,13,14,15,16) and k.status=110 and dk.kode_detail_kegiatan = '$unit_id.$kode_kegiatan.$detail_no'
                    ) komponen";
        $stmt = $con->prepareStatement($query);
        $rs = $stmt->executeQuery();
        if ($rs->next()) {
            $nilai = $rs->getString('nilai');
        } else {
            $nilai = null;
        }

        if (is_null($nilai)) {
            $query = "select sum(nilai) as nilai
                from (
                        select k.id, (dkom.harga_realisasi_bulat + dkom.lk + dkom.biaya_lain) as nilai
                        from " . sfConfig::get('app_default_edelivery') . ".detail_komponen dkom
                        join " . sfConfig::get('app_default_edelivery') . ".kontraks k on k.id = dkom.kontrak_id
                        join " . sfConfig::get('app_default_eproject') . ".detail_kegiatan dk on dk.id = dkom.detail_kegiatan_id
                        where k.metode in (3,4,12,13,14,15,16) and k.status<>110 and dk.kode_detail_kegiatan = '$unit_id.$kode_kegiatan.$detail_no'
                ) komponen";
            $stmt = $con->prepareStatement($query);
            $rs = $stmt->executeQuery();
            if ($rs->next()) {
                $nilai = $rs->getString('nilai');
            }

            $query2 = "select dk.kode_detail_kegiatan, coalesce(sum(lkk.jumlah),0) as nilai_lk, coalesce(sum(lkk.volume_lk),0) as volume_lk
                    from " . sfConfig::get('app_default_edelivery') . ".kontrak_mc_addendum as kma
                    join " . sfConfig::get('app_default_edelivery') . ".lebih_kurang_komponen lkk on lkk.kontrak_mc_addendum_id = kma.id
                    join " . sfConfig::get('app_default_eproject') . ".detail_kegiatan dk on dk.id = lkk.detail_kegiatan_id
                    where dk.kode_detail_kegiatan = '$unit_id.$kode_kegiatan.$detail_no' and kma.status = 100
                    group by dk.kode_detail_kegiatan";
            $stmt2 = $con->prepareStatement($query2);
            $rs2 = $stmt2->executeQuery();
            if ($rs2->next()) {
                $nilai_lk = $rs2->getFloat('nilai_lk');
                $nilai += $nilai_lk;
            } else {
                $nilai_lk = null;
            }
        }
        return $nilai;
    }

    public function cekCatatanKosong($unit_id, $kode_kegiatan, $arr_detail_no) {
        $kumpulan_detail_no = implode(',', $arr_detail_no);
        $query = "select count(*) as nilai 
                  from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail
                  where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and 
                   status_hapus=false and detail_no in ($kumpulan_detail_no) and
                   note_skpd='' and note_peneliti='' and komponen_id <> '23.01.01.04.16'";
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query);
        $rs = $stmt->executeQuery();
        if ($rs->next())
            $jml = $rs->getInt('nilai');

        if ($jml > 0)
            return TRUE;
        else
            return FALSE;
    }

    public function cekSisipan($unit_id, $kode_kegiatan, $level) {
        $query = " select max(status_level_tolak) as nilai from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail
            where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan'";
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query);
        $rs = $stmt->executeQuery();
        $max_level = 0;
        if ($rs->next())
            $max_level = $rs->getInt('nilai');
        if ($level >= $max_level)
            return TRUE;
        else
            return FALSE;
    }

    public function cekPerBelanja($unit_id, $kode_kegiatan, $status_level2, $arr_detail_no) {
        $tabel_semula = 'rincian_detail';
        $c = new Criteria();
        $c->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
        $c->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
        $kegiatan = DinasMasterKegiatanPeer::doSelectOne($c);
        
        if ($kegiatan->getTahap() == 'pak') {
            return FALSE;
        }

         $lepas_isrka = array('');
        //tambahan lepas validasi tambah kurang pagu dak 310521 dan balik revisi khusus 290621|| $kegiatan->getTambahanPagu() <> 0
        if (($kegiatan->getIsPernahRka() == TRUE && in_array($kegiatan->getKodeKegiatan(), $lepas_isrka)) || in_array($kegiatan->getKodeKegiatan(), $lepas_isrka) )
        {
            return FALSE;
        }
        if ($kegiatan->getIsPernahRka()) {
            $tabel_semula = 'prev_rincian_detail';
        }
        if ($kegiatan->getTahap() == 'murni') {
             ///$tabel_semula = 'murni_bukubiru_rincian_detail_backup';
              return FALSE;
        }
        //TANPA BARJAS dan PEGAWAI
        $array_id_belanja = array(4,5,9,10,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44);
        $kumpulan_detail_no = implode(',', $arr_detail_no);
        foreach ($array_id_belanja as $id_belanja) {
            $query = "select belanja_name, belanja_code from " . sfConfig::get('app_default_schema') . ".kelompok_belanja where id = $id_belanja";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs = $stmt->executeQuery();
            $rs->next();
            $kode_belanja = $rs->getString('belanja_code');
            $query = "select max(status_level) as level from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail "
                    . "where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and status_hapus = false";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs = $stmt->executeQuery();
            while ($rs->next()) {
                $status_level_terjauh = $rs->getInt('level');
            }
            if ($status_level2 != $status_level_terjauh) {
                $status_level = $status_level_terjauh;
            } else if ($status_level2 == 4) {
                $status_level = 4;
            } else if ($status_level2 == $status_level_terjauh) {
                $status_level = $status_level_terjauh;
            } 
            // else{
            //     $status_level = -1;
            // }

//            $query = "select sum(rd.nilai_anggaran) as nilai from " . sfConfig::get('app_default_schema') . ".revisi1_rincian_detail rd
//                        where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rekening_code like '$kode_belanja%' and rd.status_hapus=false
//                 and detail_no in ($kumpulan_detail_no)";

            $total_belanja_semula = 0;
            $total_belanja_menjadi = 0;
            $query = "select sum(rd.nilai_anggaran) as nilai from " . sfConfig::get('app_default_schema') . ".$tabel_semula rd
                        where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rekening_code like '$kode_belanja%' and rd.status_hapus=false
                        and (detail_no in (select detail_no from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail rd
                            where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rekening_code like '$kode_belanja%' and 
                            rd.status_level=$status_level)
                    or detail_no in ($kumpulan_detail_no)
                    or detail_no in (select rd.detail_no from " . sfConfig::get('app_default_schema') . ".$tabel_semula rd 
                        inner join " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail drd on drd.unit_id=rd.unit_id and drd.kegiatan_code=rd.kegiatan_code and drd.detail_no=rd.detail_no 
                        where rd.unit_id='$unit_id' and rd.kegiatan_code='$kode_kegiatan' and rd.rekening_code like '$kode_belanja%' and drd.status_hapus=true and rd.status_hapus=false))";
//            if ($kegiatan->getTahap() == 'revisi2') {
//                $query = "select sum(rd.nilai_anggaran) as nilai from " . sfConfig::get('app_default_schema') . ".revisi1_rincian_detail rd
//                        where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rekening_code like '$kode_belanja%' and rd.status_hapus=false
//                        and (detail_no in (select detail_no from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail rd
//                            where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rekening_code like '$kode_belanja%' and 
//                            rd.status_level=$status_level)
//                    or detail_no in ($kumpulan_detail_no))";
//            }
            //echo $query; exit;
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs_harga_belanja = $stmt->executeQuery();
            while ($rs_harga_belanja->next()) {
                $total_belanja_semula = $rs_harga_belanja->getInt('nilai');
            }
//            $query = "select sum(rd.nilai_anggaran) as nilai from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail rd
//                  where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rekening_code like '$kode_belanja%' and rd.status_hapus=false and rd.status_level>=$status_level and detail_no in ($kumpulan_detail_no)";
            $query2 = "select sum(rd.nilai_anggaran) as nilai from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail rd
                        where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rekening_code like '$kode_belanja%' and rd.status_hapus=false and (rd.status_level=$status_level or rd.detail_no in ($kumpulan_detail_no))";
            //echo $query; exit;
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query2);
            $rs_harga_belanja = $stmt->executeQuery();
            while ($rs_harga_belanja->next()) {
                $total_belanja_menjadi = $rs_harga_belanja->getInt('nilai');
            }
            if ($total_belanja_semula != $total_belanja_menjadi) {
//                if ($unit_id == 'B004' && $kode_kegiatan == '2.1.2.01.02.0004') {
//                    echo $query. '<br>';
//                    echo $query2. '<br>';
//                    echo $kode_belanja . ' = ' . $total_belanja_semula . ' | ' . $total_belanja_menjadi;
//                    exit;
//                }
                return TRUE;
            }
        }
        return FALSE;
    }

    public function cekPerBelanjaPak($unit_id, $kode_kegiatan, $status_level2, $arr_detail_no) {
        $tabel_semula = 'rincian_detail';

        $c = new Criteria();
        $c->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
        $c->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
        $rs_master_kegiatan = DinasMasterKegiatanPeer::doSelectOne($c);
        $tahap = $rs_master_kegiatan->getTahap();

        if ($tahap == 'pak' ) {
            return FALSE;
        }

        $array_id_belanja = array(4,5,9,10,12,13,14,15,16,17,18,19,20);
        $kumpulan_detail_no = implode(',', $arr_detail_no);
        foreach ($array_id_belanja as $id_belanja) {
            $query = "select belanja_name, belanja_code from " . sfConfig::get('app_default_schema') . ".kelompok_belanja where id = $id_belanja";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs = $stmt->executeQuery();
            $rs->next();
            $kode_belanja = $rs->getString('belanja_code');
            $query = "select max(status_level) as level from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail "
                    . "where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and status_hapus = false";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs = $stmt->executeQuery();
            while ($rs->next()) {
                $status_level_terjauh = $rs->getInt('level');
            }
            if ($status_level2 != $status_level_terjauh || $status_level2 == $status_level_terjauh) {
                $status_level = $status_level_terjauh;
            } elseif ($status_level2 == 4) {
                $status_level = 4;
            } 
            // else {
            //     $status_level = -1;
            // }
//            $query = "select sum(rd.nilai_anggaran) as nilai from " . sfConfig::get('app_default_schema') . ".revisi1_rincian_detail rd
//                        where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rekening_code like '$kode_belanja%' and rd.status_hapus=false
//                 and detail_no in ($kumpulan_detail_no)";

            $total_belanja_semula = 0;
            $total_belanja_menjadi = 0;
            // ubah query cek total belanja semula
            $query = "select sum(rd.nilai_anggaran) as nilai from " . sfConfig::get('app_default_schema') . ".$tabel_semula rd
                        where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rekening_code like '$kode_belanja%' and rd.status_hapus=false
                        and (detail_no in (select detail_no from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail rd
                            where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rekening_code like '$kode_belanja%' 
                            )
                    or detail_no in ($kumpulan_detail_no)
                    or detail_no in (select rd.detail_no from " . sfConfig::get('app_default_schema') . ".$tabel_semula rd 
                        inner join " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail drd on drd.unit_id=rd.unit_id and drd.kegiatan_code=rd.kegiatan_code and drd.detail_no=rd.detail_no 
                        where rd.unit_id='$unit_id' and rd.kegiatan_code='$kode_kegiatan' and rd.rekening_code like '$kode_belanja%' and drd.status_hapus=true and rd.status_hapus=false)
                    or tahap = '$tahap'
                        )";

            //echo $query; exit;
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs_harga_belanja = $stmt->executeQuery();
            while ($rs_harga_belanja->next()) {
                $total_belanja_semula = $rs_harga_belanja->getInt('nilai');
            }
//            $query = "select sum(rd.nilai_anggaran) as nilai from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail rd
//                  where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rekening_code like '$kode_belanja%' and rd.status_hapus=false and rd.status_level>=$status_level and detail_no in ($kumpulan_detail_no)";
            // tambah hitung semua detail_no di semua status_level
            $query2 = "select sum(rd.nilai_anggaran) as nilai from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail rd
                        where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rekening_code like '$kode_belanja%' and rd.status_hapus=false and (rd.status_level <= $status_level 
                        or rd.detail_no in ($kumpulan_detail_no)
                        or rd.status_level > $status_level
                        )";
            //echo $query; exit;
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query2);
            $rs_harga_belanja = $stmt->executeQuery();
            while ($rs_harga_belanja->next()) {
                $total_belanja_menjadi = $rs_harga_belanja->getInt('nilai');
            }
            if ($total_belanja_semula != $total_belanja_menjadi) {
//                if ($unit_id == '2400' && $kode_kegiatan == '1.09.20.0002') {
//                    echo $query. '<br>';
//                    echo $query2. '<br>';
//                    echo $kode_belanja . ' = ' . $total_belanja_semula . ' | ' . $total_belanja_menjadi;
//                    exit;
//                }
                return TRUE;
            }
        }
        return FALSE;
    }

    public function cekPerBelanjaPakDinas($unit_id) {
        $array_id_belanja = array(9, 10, 11);
        foreach ($array_id_belanja as $id_belanja) {
            $query = "select belanja_name, belanja_code from " . sfConfig::get('app_default_schema') . ".kelompok_belanja where id = $id_belanja";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs = $stmt->executeQuery();
            $rs->next();
            $kode_belanja = $rs->getString('belanja_code');
            $total_belanja_semula = 0;
            $total_belanja_menjadi = 0;
            $query = "select sum(rd.nilai_anggaran) as nilai from " . sfConfig::get('app_default_schema') . ".perangkaan_rincian_detail rd
                        where unit_id='$unit_id' and rekening_code like '$kode_belanja%' and rd.status_hapus=false";
            $stmt = $con->prepareStatement($query);
            $rs_harga_belanja = $stmt->executeQuery();
            while ($rs_harga_belanja->next()) {
                $total_belanja_semula = $rs_harga_belanja->getInt('nilai');
            }
            $query2 = "select sum(rd.nilai_anggaran) as nilai from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail rd
                        where unit_id='$unit_id' and rekening_code like '$kode_belanja%' and rd.status_hapus=false";
            $stmt = $con->prepareStatement($query2);
            $rs_harga_belanja = $stmt->executeQuery();
            while ($rs_harga_belanja->next()) {
                $total_belanja_menjadi = $rs_harga_belanja->getInt('nilai');
            }
            if ($total_belanja_semula != $total_belanja_menjadi) {
                return TRUE;
            }
        }
        return FALSE;
    }

    public function cekPerKegiatanPak($unit_id, $kode_kegiatan, $status_level2, $arr_detail_no) {
        $c = new Criteria();
        $c->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
        $c->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
        $kegiatan = DinasMasterKegiatanPeer::doSelectOne($c);
        if ($kegiatan->getTahap() != 'pak') {
            return FALSE;
        }

        //$tabel_semula = 'pak_bukubiru_rincian_detail';
        $tabel_semula = 'rincian_detail';

        $kumpulan_detail_no = implode(',', $arr_detail_no);
        $query = "select max(status_level) as level from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail "
                . "where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and status_hapus = false";
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query);
        $rs = $stmt->executeQuery();
        while ($rs->next()) {
            $status_level_terjauh = $rs->getInt('level');
        }
        if ($status_level2 != $status_level_terjauh) {
            $status_level = $status_level_terjauh;
        } elseif ($status_level2 == 4) {
            $status_level = 4;
        } else {
            $status_level = -1;
        }

        $total_belanja_semula = 0;
        $total_belanja_menjadi = 0;
        $query = "select sum(rd.nilai_anggaran) as nilai from " . sfConfig::get('app_default_schema') . ".$tabel_semula rd
                        where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rd.status_hapus=false
                        and (detail_no in (select detail_no from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail rd
                            where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and 
                            rd.status_level=$status_level)
                    or detail_no in ($kumpulan_detail_no)
                    or detail_no in (select rd.detail_no from " . sfConfig::get('app_default_schema') . ".$tabel_semula rd 
                        inner join " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail drd on drd.unit_id=rd.unit_id and drd.kegiatan_code=rd.kegiatan_code and drd.detail_no=rd.detail_no 
                        where rd.unit_id='$unit_id' and rd.kegiatan_code='$kode_kegiatan' and drd.status_hapus=true and rd.status_hapus=false))";
        $query = "select sum(rd.nilai_anggaran) as nilai from " . sfConfig::get('app_default_schema') . ".$tabel_semula rd
                        where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rd.status_hapus=false";
        //echo $query; exit;
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query);
        $rs_harga_belanja = $stmt->executeQuery();
        while ($rs_harga_belanja->next()) {
            $total_belanja_semula = $rs_harga_belanja->getInt('nilai');
        }
        $query2 = "select sum(rd.nilai_anggaran) as nilai from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail rd
                        where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rd.status_hapus=false and (rd.status_level=$status_level or rd.detail_no in ($kumpulan_detail_no))";
        $query2 = "select sum(rd.nilai_anggaran) as nilai from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail rd
                        where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rd.status_hapus=false";
        //echo $query; exit;
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query2);
        $rs_harga_belanja = $stmt->executeQuery();
        while ($rs_harga_belanja->next()) {
            $total_belanja_menjadi = $rs_harga_belanja->getInt('nilai');
        }
        if ($total_belanja_semula != $total_belanja_menjadi) {
            return TRUE;
        }
        return FALSE;
    }

    public function cekPerRekening($unit_id, $kode_kegiatan, $status_level2, $arr_detail_no) {
        $c = new Criteria();
        $c->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
        $c->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
        $kegiatan = DinasMasterKegiatanPeer::doSelectOne($c);
        //lepas is_pernah rka 523 sementara 230621
        $lepas_isrka = array('856','1623','970','1703','111','1651','3449','1011','3529','452','1627','1485', '820', '850','263','264','267','859','1218','1219','1131','3462','3463','3388','962','1670','2288','367','1392','1391','3387','3430','3423','3393','2180','1790','635','1281','1047');
        if ($kegiatan->getIsPernahRka() == FALSE) {
            return FALSE;
        } else if ($kegiatan->getIsPernahRka() == TRUE && in_array($kegiatan->getKodeKegiatan(), $lepas_isrka)) {           
            return FALSE;
        }

        $kumpulan_detail_no = implode(',', $arr_detail_no);
        $arr_error = array();
        $rekening = array();
        $query = "select distinct rekening_code from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan'";
        $con = Propel::getConnection();
        //echo $query; exit;
        $stmt = $con->prepareStatement($query);
        $rs = $stmt->executeQuery();
        while ($rs->next()) {
            $rekening[] = $rs->getString('rekening_code');
        }
        foreach ($rekening as $rekening_code) {
            //$rekening_code = $rs->getString('rekening_code');
            $query = "select max(status_level) as level from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail "
                    . "where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and status_hapus = false";
            $stmt = $con->prepareStatement($query);
            $rs = $stmt->executeQuery();
            while ($rs->next()) {
                $status_level_terjauh = $rs->getInt('level');
            }
            if ($status_level2 != $status_level_terjauh) {
                $status_level = $status_level_terjauh;
            } elseif ($status_level2 == 4) {
                $status_level = 4;
            } else {
                $status_level = -1;
            }
            $total_rekening_semula = 0;
            $total_rekening_menjadi = 0;
            $query = "select sum(rd.nilai_anggaran) as nilai from " . sfConfig::get('app_default_schema') . ".rincian_detail rd
                        where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rekening_code = '$rekening_code' and rd.status_hapus=false
                        and (detail_no in (select detail_no from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail rd
                            where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rekening_code = '$rekening_code' and 
                            rd.status_hapus=false and rd.status_level=$status_level) or detail_no in ($kumpulan_detail_no))";
            // if($rekening_code == '5.2.03.01.01.0006') {
            // echo $query; exit;
            // }
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs_harga_belanja = $stmt->executeQuery();
            while ($rs_harga_belanja->next()) {
                $total_rekening_semula = $rs_harga_belanja->getInt('nilai');
            }
            $query2 = "select sum(rd.nilai_anggaran) as nilai from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail rd
                        where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rekening_code like '$rekening_code' and rd.status_hapus=false and (rd.status_level=$status_level or rd.detail_no in ($kumpulan_detail_no))";
            //echo $query; exit;
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query2);
            $rs_harga_belanja = $stmt->executeQuery();
            while ($rs_harga_belanja->next()) {
                $total_rekening_menjadi = $rs_harga_belanja->getInt('nilai');
            }
            if (!$total_rekening_semula)
                $total_rekening_semula = 0;
            if (!$total_rekening_menjadi)
                $total_rekening_menjadi = 0;

            if ($total_rekening_semula != $total_rekening_menjadi) {
                $arr_error[$rekening_code] = "$total_rekening_semula|$total_rekening_menjadi";
            }
        }
        if ($arr_error)
            return $arr_error;
        else
            return FALSE;
    }

    public function cekPerRekeningTertentu($unit_id, $kode_kegiatan, $status_level2, $arr_detail_no, $arr_rekening) {
        $kumpulan_detail_no = implode(',', $arr_detail_no);

        foreach ($arr_rekening as $rekening_code) {
            $query = "select max(status_level) as level from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail "
                    . "where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and status_hapus = false";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs = $stmt->executeQuery();
            while ($rs->next()) {
                $status_level_terjauh = $rs->getInt('level');
            }
            if ($status_level2 != $status_level_terjauh) {
                $status_level = $status_level_terjauh;
            } elseif ($status_level2 == 4) {
                $status_level = 4;
            } else {
                $status_level = -1;
            }
            $total_rekening_semula = 0;
            $total_rekening_menjadi = 0;
            $query = "select sum(rd.nilai_anggaran) as nilai from " . sfConfig::get('app_default_schema') . ".rincian_detail rd
                        where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rekening_code = '$rekening_code' and rd.status_hapus=false
                        and (detail_no in (select detail_no from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail rd
                            where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rekening_code = '$rekening_code' and 
                            rd.status_hapus=false and rd.status_level=$status_level)
                    or detail_no in ($kumpulan_detail_no))";
            //echo $query; exit;
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs_harga_belanja = $stmt->executeQuery();
            while ($rs_harga_belanja->next()) {
                $total_rekening_semula = $rs_harga_belanja->getInt('nilai');
            }
            $query2 = "select sum(rd.nilai_anggaran) as nilai from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail rd
                        where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rekening_code like '$rekening_code' and rd.status_hapus=false and (rd.status_level=$status_level or rd.detail_no in ($kumpulan_detail_no))";
            //echo $query; exit;
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query2);
            $rs_harga_belanja = $stmt->executeQuery();
            while ($rs_harga_belanja->next()) {
                $total_rekening_menjadi = $rs_harga_belanja->getInt('nilai');
            }
            if (!$total_rekening_semula)
                $total_rekening_semula = 0;
            if (!$total_rekening_menjadi)
                $total_rekening_menjadi = 0;

            if ($total_rekening_semula != $total_rekening_menjadi) {
                $arr_error[$rekening_code] = "$total_rekening_semula|$total_rekening_menjadi";
            }
        }
        if ($arr_error)
            return $arr_error;
        else
            return FALSE;
    }

    public function cekPerRekeningMurni($unit_id, $kode_kegiatan, $status_level2, $arr_detail_no) {
        $c = new Criteria();
        $c->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
        $c->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
        $kegiatan = DinasMasterKegiatanPeer::doSelectOne($c);
        if ($kegiatan->getTahap() != 'murni') {
            return FALSE;
        }

        $kumpulan_detail_no = implode(',', $arr_detail_no);
        $arr_error = array();
        $rekening = array();
        $query = "select distinct rekening_code from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan'";
        $con = Propel::getConnection();
        //echo $query; exit;
        $stmt = $con->prepareStatement($query);
        $rs = $stmt->executeQuery();
        while ($rs->next()) {
            $rekening[] = $rs->getString('rekening_code');
        }
        foreach ($rekening as $rekening_code) {
            //$rekening_code = $rs->getString('rekening_code');
            $query = "select max(status_level) as level from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail "
                    . "where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and status_hapus = false";
            $stmt = $con->prepareStatement($query);
            $rs = $stmt->executeQuery();
            while ($rs->next()) {
                $status_level_terjauh = $rs->getInt('level');
            }
            if ($status_level2 != $status_level_terjauh) {
                $status_level = $status_level_terjauh;
            } elseif ($status_level2 == 4) {
                $status_level = 4;
            } else {
                $status_level = -1;
            }
            $total_rekening_semula = 0;
            $total_rekening_menjadi = 0;
            $query = "select sum(rd.nilai_anggaran) as nilai from " . sfConfig::get('app_default_schema') . ".murni_bukubiru_rincian_detail rd
                        where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rekening_code = '$rekening_code' and rd.status_hapus=false";
            //echo $query; exit;
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs_harga_belanja = $stmt->executeQuery();
            while ($rs_harga_belanja->next()) {
                $total_rekening_semula = $rs_harga_belanja->getInt('nilai');
            }
            $query2 = "select sum(rd.nilai_anggaran) as nilai from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail rd
                        where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rekening_code like '$rekening_code' and rd.status_hapus=false";
            //echo $query2; exit;
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query2);
            $rs_harga_belanja = $stmt->executeQuery();
            while ($rs_harga_belanja->next()) {
                $total_rekening_menjadi = $rs_harga_belanja->getInt('nilai');
            }
            if (!$total_rekening_semula)
                $total_rekening_semula = 0;
            if (!$total_rekening_menjadi)
                $total_rekening_menjadi = 0;

            if ($total_rekening_semula != $total_rekening_menjadi) {
                //echo $query . '<br><br>' . $query2; exit;
                $arr_error[$rekening_code] = "$total_rekening_semula|$total_rekening_menjadi";
            }
        }
        if ($arr_error)
            return $arr_error;
        else
            return FALSE;
    }

    public function cekPerRekeningTahap($unit_id, $kode_kegiatan, $tahap_revisi) {
        $c = new Criteria();
        $c->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
        $c->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
        $kegiatan = DinasMasterKegiatanPeer::doSelectOne($c);
        if ($kegiatan->getTahap() != $tahap_revisi) {
            return FALSE;
        }

        if ($kegiatan->getTahap() == 'murni') {
            return FALSE;
        }

        $tabel = '';
        if ($tahap_revisi == 'murni') {
            $tabel = 'murni_';
        } elseif ($tahap_revisi == 'revisi1') {
            $tabel = 'revisi1_';
        } elseif ($tahap_revisi == 'revisi2') {
            $tabel = 'revisi2_';
        } elseif ($tahap_revisi == 'revisi3') {
            $tabel = 'revisi3_';
        } elseif ($tahap_revisi == 'pak') {
            $tabel = 'pak_bukubiru_';
        }

        $arr_error = array();
        $rekening = array();
        $query = "select distinct rekening_code from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan'";
        $con = Propel::getConnection();
        //echo $query; exit;
        $stmt = $con->prepareStatement($query);
        $rs = $stmt->executeQuery();
        while ($rs->next()) {
            $rekening[] = $rs->getString('rekening_code');
        }
        foreach ($rekening as $rekening_code) {
            $total_rekening_semula = 0;
            $total_rekening_menjadi = 0;
            $query = "select sum(rd.nilai_anggaran) as nilai from " . sfConfig::get('app_default_schema') . "." . $tabel . "rincian_detail rd
                        where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rekening_code = '$rekening_code' and rd.status_hapus=false";
            //echo $query; exit;
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs_harga_belanja = $stmt->executeQuery();
            while ($rs_harga_belanja->next()) {
                $total_rekening_semula = $rs_harga_belanja->getInt('nilai');
            }
            $query2 = "select sum(rd.nilai_anggaran) as nilai from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail rd
                        where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rekening_code like '$rekening_code' and rd.status_hapus=false";
            //echo $query2; exit;
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query2);
            $rs_harga_belanja = $stmt->executeQuery();
            while ($rs_harga_belanja->next()) {
                $total_rekening_menjadi = $rs_harga_belanja->getInt('nilai');
            }
            if (!$total_rekening_semula)
                $total_rekening_semula = 0;
            if (!$total_rekening_menjadi)
                $total_rekening_menjadi = 0;

            if ($total_rekening_semula != $total_rekening_menjadi) {
                //echo $query . '<br><br>' . $query2; exit;
                $arr_error[$rekening_code] = "$total_rekening_semula|$total_rekening_menjadi";
            }
        }
        if ($arr_error)
            return $arr_error;
        else
            return FALSE;
    }

    public function cekPerCovidTahap($unit_id, $kode_kegiatan, $tahap_revisi) {
        $c = new Criteria();
        $c->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
        $c->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
        $kegiatan = DinasMasterKegiatanPeer::doSelectOne($c);
        if ($kegiatan->getTahap() != $tahap_revisi) {
            return FALSE;
        }

        if ($kegiatan->getTahap() == 'murni') {
            return FALSE;
        }



        $tabel = '';
        if ($tahap_revisi == 'murni') {
            $tabel = 'murni_';
        } elseif ($tahap_revisi == 'revisi1') {
            $tabel = 'murni_';
        } elseif ($tahap_revisi == 'revisi2') {
            $tabel = 'revisi1_';
        } elseif ($tahap_revisi == 'revisi3') {
            $tabel = 'revisi2_';
        } elseif ($tahap_revisi == 'pak') {
            #$tabel = 'pak_bukubiru_';
            $tabel = '';
        }

        $arr_error = array();        
       
            $total_rekening_semula = 0;
            $total_rekening_menjadi = 0;
            $query = "select sum(rd.nilai_anggaran) as nilai from " . sfConfig::get('app_default_schema') . "." . $tabel . "rincian_detail rd
                        where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and is_covid=true and rd.status_hapus=false";
            //echo $query; exit;
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs_harga_belanja = $stmt->executeQuery();
            while ($rs_harga_belanja->next()) {
                $total_rekening_semula = $rs_harga_belanja->getInt('nilai');
            }
            $query2 = "select sum(rd.nilai_anggaran) as nilai from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail rd
                        where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and is_covid=true and rd.status_hapus=false";
            //echo $query2; exit;
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query2);
            $rs_harga_belanja = $stmt->executeQuery();
            while ($rs_harga_belanja->next()) {
                $total_rekening_menjadi = $rs_harga_belanja->getInt('nilai');
            }
            if (!$total_rekening_semula)
                $total_rekening_semula = 0;
            if (!$total_rekening_menjadi)
                $total_rekening_menjadi = 0;

            if ($total_rekening_semula > $total_rekening_menjadi) {
                //echo $query . '<br><br>' . $query2; exit;
                $arr_error[$rekening_code] = "$total_rekening_semula|$total_rekening_menjadi";
            }
        
        if ($arr_error)
            return $arr_error;
        else
            return FALSE;
    }

        public function cekPerBLUDTahap($unit_id, $kode_kegiatan, $tahap_revisi) {
        $c = new Criteria();
        $c->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
        $c->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
        $kegiatan = DinasMasterKegiatanPeer::doSelectOne($c);
        if ($kegiatan->getTahap() != $tahap_revisi) {
            return FALSE;
        }

        if ($kegiatan->getTahap() == 'murni') {
            return FALSE;
        }

        $tabel = '';
        if ($tahap_revisi == 'murni') {
            $tabel = 'murni_';
        } elseif ($tahap_revisi == 'revisi1') {
            $tabel = 'murni_';
        } elseif ($tahap_revisi == 'revisi2') {
            $tabel = 'revisi1_';
        } elseif ($tahap_revisi == 'revisi3') {
            $tabel = 'revisi2_';
        } elseif ($tahap_revisi == 'pak') {
            //$tabel = 'pak_bukubiru_';
            $tabel = '';
        }

        $arr_error = array();        
       
            $total_rekening_semula = 0;
            $total_rekening_menjadi = 0;
            $query = "select sum(rd.nilai_anggaran) as nilai from " . sfConfig::get('app_default_schema') . "." . $tabel . "rincian_detail rd
                        where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and is_blud=true and rd.status_hapus=false";
            //echo $query; exit;
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs_harga_belanja = $stmt->executeQuery();
            while ($rs_harga_belanja->next()) {
                $total_rekening_semula = $rs_harga_belanja->getInt('nilai');
            }
            $query2 = "select sum(rd.nilai_anggaran) as nilai from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail rd
                        where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and is_blud=true and rd.status_hapus=false";
            //echo $query2; exit;
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query2);
            $rs_harga_belanja = $stmt->executeQuery();
            while ($rs_harga_belanja->next()) {
                $total_rekening_menjadi = $rs_harga_belanja->getInt('nilai');
            }
            if (!$total_rekening_semula)
                $total_rekening_semula = 0;
            if (!$total_rekening_menjadi)
                $total_rekening_menjadi = 0;

            if ($total_rekening_semula != $total_rekening_menjadi) {
                //echo $query . '<br><br>' . $query2; exit;
                $arr_error[$rekening_code] = "$total_rekening_semula|$total_rekening_menjadi";
            }
        
        if ($arr_error)
            return $arr_error;
        else
            return FALSE;
    }

    public function cekPerBelanjaMurni($unit_id, $kode_kegiatan, $status_level2, $arr_detail_no) {
        // $tabel_semula = 'dinas_rincian_detail_19122017';
        $tabel_semula = 'murni_bukubiru_rincian_detail';
        $c = new Criteria();
        $c->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
        $c->addAnd(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
        $kegiatan = DinasMasterKegiatanPeer::doSelectOne($c);
        if ($kegiatan->getTahap() != 'murni') {
            return FALSE;
        }

        $array_id_belanja = array(4,5,9,10,12,13,14,15,16,17,18,19,20);
        $kumpulan_detail_no = implode(',', $arr_detail_no);
        foreach ($array_id_belanja as $id_belanja) {
            $query = "select belanja_name, belanja_code from " . sfConfig::get('app_default_schema') . ".kelompok_belanja where id = $id_belanja";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs = $stmt->executeQuery();
            $rs->next();
            $kode_belanja = $rs->getString('belanja_code');
            $query = "select max(status_level) as level from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail "
                    . "where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and status_hapus = false";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs = $stmt->executeQuery();
            while ($rs->next()) {
                $status_level_terjauh = $rs->getInt('level');
            }
            if ($status_level2 != $status_level_terjauh || $status_level2 == $status_level_terjauh ) {
                $status_level = $status_level_terjauh;
            } else if ($status_level2 == 4) {
                $status_level = 4;
            } 
            // else {
            //     $status_level = -1;
            // }
            $total_belanja_semula = 0;
            $total_belanja_menjadi = 0;
            $query = "select sum(rd.nilai_anggaran) as nilai from " . sfConfig::get('app_default_schema') . ".$tabel_semula rd
                        where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rekening_code like '$kode_belanja%' and rd.status_hapus=false
                        and (detail_no in (select detail_no from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail rd
                            where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rekening_code like '$kode_belanja%' and 
                            rd.status_level=$status_level)
                    or detail_no in ($kumpulan_detail_no)
                    or detail_no in (select rd.detail_no from " . sfConfig::get('app_default_schema') . ".$tabel_semula rd 
                        inner join " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail drd on drd.unit_id=rd.unit_id and drd.kegiatan_code=rd.kegiatan_code and drd.detail_no=rd.detail_no 
                        where rd.unit_id='$unit_id' and rd.kegiatan_code='$kode_kegiatan' and rd.rekening_code like '$kode_belanja%' and drd.status_hapus=true and rd.status_hapus=false)
                    or detail_no in (select detail_no from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rekening_code like '$kode_belanja%' and status_hapus=false and nilai_anggaran=0))";

            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs_harga_belanja = $stmt->executeQuery();
            while ($rs_harga_belanja->next()) {
                $total_belanja_semula = $rs_harga_belanja->getInt('nilai');
            }
            $query2 = "select sum(rd.nilai_anggaran) as nilai from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail rd
                        where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rekening_code like '$kode_belanja%' and rd.status_hapus=false and (rd.status_level=$status_level or rd.detail_no in ($kumpulan_detail_no))";
            //echo $query; exit;
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query2);
            $rs_harga_belanja = $stmt->executeQuery();
            while ($rs_harga_belanja->next()) {
                $total_belanja_menjadi = $rs_harga_belanja->getInt('nilai');
            }
            if ($total_belanja_semula != $total_belanja_menjadi) {
                return TRUE;
            }
        }
        return FALSE;
    }

    public function getMaxDetailNo($unit_id, $kegiatan_code) {
        $queryDetailNo = "select max(detail_no) as nilai from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail where unit_id='$unit_id' and kegiatan_code='$kegiatan_code'";
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($queryDetailNo);
        $rs_max = $stmt->executeQuery();
        while ($rs_max->next()) {
            $detail_no = $rs_max->getString('nilai');
        }
        $queryDetailNo2 = "select max(detail_no) as nilai from " . sfConfig::get('app_default_schema') . ".rincian_detail where unit_id='$unit_id' and kegiatan_code='$kegiatan_code'";
        $stmt2 = $con->prepareStatement($queryDetailNo2);
        $rs_max2 = $stmt2->executeQuery();
        while ($rs_max2->next()) {
            $detail_no2 = $rs_max2->getString('nilai');
        }
        if ($detail_no2 > $detail_no) {
            $detail_no = $detail_no2;
        }
        $detail_no+=1;
        return $detail_no;
    }

    public function getBatasPaguPerKegiatan($unit_id, $kegiatan_code, $komponen_id, $pajak, $vol1, $vol2, $vol3, $vol4, $komponen_penyusun, $volumePenyusun) {
        try {
            $total_baru = $this->getTotalNilaiBaru($komponen_id, $pajak, $vol1, $vol2, $vol3, $vol4, $komponen_penyusun, $volumePenyusun);

            $total_pagu = $this->getNilaiPagu($unit_id, $kegiatan_code, "kegiatan");

            $total_rincianDetail = $this->getNilaiRincianDetail($unit_id, $kegiatan_code);

            $total_baruRincian = $total_baru + $total_rincianDetail;

            if ($total_pagu < $total_baruRincian) {
                $backValue = '1';
            } else {
                $backValue = '0';
            }
        } catch (Exception $exc) {
            $backValue = '1';
        }

        return $backValue;
    }

    public function getBatasPaguPerKegiatanforApprove($unit_id, $kegiatan_code) {
        try {
            // $total_baru = $this->getTotalNilaiBaru($komponen_id, $pajak, $vol1, $vol2, $vol3, $vol4, $komponen_penyusun, $volumePenyusun);

            $total_pagu = $this->getNilaiPagu($unit_id, $kegiatan_code, 'kegiatan');

            $total_rincianDetail = $this->getNilaiRincianDetail($unit_id, $kegiatan_code);

            // $total_baruRincian = $total_baru + $total_rincianDetail;
           //print_r('test dari programer '.$total_rincianDetail.' '.$total_pagu);exit;

            if ($total_pagu != $total_rincianDetail) {
                $backValue = '1';
            } else {
                $backValue = '0';
            }
        } catch (Exception $exc) {
            $backValue = '1';
        }

        return $backValue;
    }

    public function getBatasPaguPerDinasforApprove($unit_id, $kegiatan_code) {
        try {
            // $total_baru = $this->getTotalNilaiBaru($komponen_id, $pajak, $vol1, $vol2, $vol3, $vol4, $komponen_penyusun, $volumePenyusun);

            $total_pagu = $this->getNilaiPagu($unit_id, $kegiatan_code, 'dinas');

            $total_rincianDetail = $this->getNilaiRincianDetail($unit_id, $kegiatan_code = null);

            // $total_baruRincian = $total_baru + $total_rincianDetail;

            if ($total_pagu != $total_rincianDetail) {
                $backValue = '1';
            } else {
                $backValue = '0';
            }
        } catch (Exception $exc) {
            $backValue = '1';
        }

        return $backValue;
    }

    public function getBatasPaguPerKegiatanSubKegiatan($unit_id, $kegiatan_code, $perkalian_keoef, $total) {
        $unit_id = $unit_id;
        $total_baru = $perkalian_keoef * $total;
        $total_pagu = $this->getNilaiPagu($unit_id, $kegiatan_code, 'kegiatan');
        $total_rincianDetail = $this->getNilaiRincianDetail($unit_id, $kegiatan_code);

        $total_baruRincian = $total_baru + $total_rincianDetail;
        if ($total_pagu < $total_baruRincian) {
            $backValue = '1';
        } else {
            $backValue = '0';
        }
        return $backValue;
    }

    public function getBatasPaguPerKegiatanforEdit($unit_id, $kegiatan_code, $detail_no, $volume) {
        try {
            $unit_id = $unit_id;

            $komponen_id = $this->getIsiKomponenId($unit_id, $kegiatan_code, $detail_no);
            $pajak = $this->getIsiPajak($unit_id, $kegiatan_code, $detail_no);
            //nilai komponen setelah di edit
            //$total_baru = $this->getTotalNilaiBaruforEdit($komponen_id, $pajak, $volume);
            $total_baru = $this->getTotalNilaiBaruforEdit2($unit_id, $kegiatan_code, $detail_no, $volume);
            //nilai komponen sebelum di edit
            $total_saatIni = $this->getTotalNilaiKomponenBdsrDetailNo($unit_id, $kegiatan_code, $detail_no);

            $total_pagu = $this->getNilaiPagu($unit_id, $kegiatan_code,"kegiatan");

            $total_rincianDetail = $this->getNilaiRincianDetail($unit_id, $kegiatan_code);
            //print_r($komponen_id.' '.$pajak.' totBaru '.$total_baru.' ');exit;
            //rumus : total baru = total rincian_detail + nilai komponen (setelah di edit) - nilai komponen (sebelum di edit)
            $total_baruRincian = $total_rincianDetail + $total_baru - $total_saatIni;
            //print_r('test dari programer '.$total_rincianDetail.' '.$total_baru.' '.$total_saatIni.' '.$total_pagu);exit;
            if ($total_pagu < $total_baruRincian) {
                if ($total_baru < $total_saatIni) {
                    $backValue = '0';
                } else {
                    $backValue = '1';
                }
                // $backValue = '1';
            } else {
                $backValue = '0';
            }
        } catch (Exception $exc) {
            $backValue = '1';
        }

        return $backValue;
    }

    public function getBatasPaguPerKegiatanforLelang($unit_id, $kegiatan_code, $detail_no, $total_baru) {
        try {
            //nilai komponen setelah di edit
            //$total_baru = $this->getTotalNilaiBaruforEdit($komponen_id, $pajak, $volume);
            //$total_baru = $this->getTotalNilaiBaruforEdit2($unit_id, $kegiatan_code, $detail_no, $volume);
            //nilai komponen sebelum di edit
            $total_saatIni = $this->getTotalNilaiKomponenBdsrDetailNo($unit_id, $kegiatan_code, $detail_no);

            $total_pagu = $this->getNilaiPagu($unit_id, $kegiatan_code, 'kegiatan');

            $total_rincianDetail = $this->getNilaiRincianDetail($unit_id, $kegiatan_code);
            //print_r($komponen_id.' '.$pajak.' totBaru '.$total_baru.' ');exit;
            //rumus : total baru = total rincian_detail + nilai komponen (setelah di edit) - nilai komponen (sebelum di edit)
            $total_baruRincian = $total_rincianDetail + $total_baru - $total_saatIni;
            //print_r('test dari programer '.$total_rincianDetail.' '.$total_baru.' '.$total_saatIni.' '.$total_pagu);exit;
            if ($total_pagu < $total_baruRincian) {
                if ($total_baru < $total_saatIni) {
                    $backValue = '0';
                } else {
                    $backValue = '1';
                }
            } else {
                $backValue = '0';
            }
        } catch (Exception $exc) {
            $backValue = '1';
        }

        return $backValue;
    }

    public function getBatasPaguPerKegiatanforKembalikan($unit_id, $kegiatan_code, $detail_no) {
        try {
            //nilai komponen semula (di rincian detail)
            $rd_fuction = new RincianDetail();
            $total_baru = $rd_fuction->getTotalNilaiKomponenBdsrDetailNo($unit_id, $kegiatan_code, $detail_no);
            //nilai komponen menjadi (di dinas rincian detail)
            $total_saatIni = $this->getTotalNilaiKomponenBdsrDetailNo($unit_id, $kegiatan_code, $detail_no);

            $total_pagu = $this->getNilaiPagu($unit_id, $kegiatan_code, 'kegiatan');

            $total_rincianDetail = $this->getNilaiRincianDetail($unit_id, $kegiatan_code);
            //print_r($komponen_id.' '.$pajak.' totBaru '.$total_baru.' ');exit;
            //rumus : total baru = total rincian_detail + nilai komponen (setelah di edit) - nilai komponen (sebelum di edit)
            $total_baruRincian = $total_rincianDetail + $total_baru - $total_saatIni;
            //print_r('test dari programer '.$total_rincianDetail.' '.$total_baru.' '.$total_saatIni.' '.$total_pagu);exit;
            if ($total_pagu < $total_baruRincian) {
                if ($total_baru < $total_saatIni) {
                    $backValue = '0';
                } else {
                    $backValue = '1';
                }
            } else {
                $backValue = '0';
            }
        } catch (Exception $exc) {
            $backValue = '1';
        }

        return $backValue;
    }

    public function getBatasPaguPerDinas($unit_id, $kegiatan_code, $komponen_id, $pajak, $vol1, $vol2, $vol3, $vol4, $komponen_penyusun, $volumePenyusun) {
        // echo 'aa';die();
        try {
        // echo $unit_id;die();
             $con = Propel::getConnection();
             $c = new Criteria();
             $c->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
             $c->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kegiatan_code);
             $kegiatan = DinasMasterKegiatanPeer::doSelectOne($c);

            $unit_id = $unit_id;
            $total_baru = $this->getTotalNilaiBaru($komponen_id, $pajak, $vol1, $vol2, $vol3, $vol4, $komponen_penyusun, $volumePenyusun);
            

            // echo $total_baru;die();
            $total_pagu = $this->getNilaiPagu($unit_id, $kegiatan_code, "dinas");
            //echo $total_pagu;die();

            if(strpos($unit_id,'12')!== FALSE)
            $total_rincianDetail = $this->getNilaiRincianDetailKelurahan($unit_id, $kegiatan_code);
            else
            $total_rincianDetail = $this->getNilaiRincianDetail($unit_id, $kegiatan_code = null);

            $total_baruRincian = $total_baru + $total_rincianDetail;
            if ($total_pagu < $total_baruRincian) {
                $backValue = '1';
            } else {
                $backValue = '0';
            }
        } 
        catch (Exception $exc) 
        {
            // echo $unit_id;die();
            $backValue = '1';
        }
        // echo $unit_id;die();
        return $backValue;
    }

    public function getBatasPaguPerDinasUK($unit_id, $komponen_id, $pajak, $vol1, $vol2, $vol3, $vol4, $komponen_penyusun, $volumePenyusun) {
        try {
            $total_baru = $this->getTotalNilaiBaru($komponen_id, $pajak, $vol1, $vol2, $vol3, $vol4, $komponen_penyusun, $volumePenyusun);

            $total_pagu = $this->getNilaiPaguUKMaks($unit_id);

            $total_rincianDetail = $this->getNilaiPaguUKSekarang($unit_id);

            $total_baruRincian = $total_baru + $total_rincianDetail;
            if ($total_pagu < $total_baruRincian) {
                $backValue = '1';
            } else {
                $backValue = '0';
            }
        } catch (Exception $exc) {
            $backValue = '1';
        }

        return $backValue;
    }

    public function getBatasPaguPerDinasMamin($unit_id, $komponen_id, $pajak, $vol1, $vol2, $vol3, $vol4, $komponen_penyusun, $volumePenyusun) {
        try {
            $total_baru = $this->getTotalNilaiBaru($komponen_id, $pajak, $vol1, $vol2, $vol3, $vol4, $komponen_penyusun, $volumePenyusun);

            $total_pagu = $this->getNilaiPaguMaminMaks($unit_id);

            $total_rincianDetail = $this->getNilaiPaguMaminSekarang($unit_id);

            $total_baruRincian = $total_baru + $total_rincianDetail;
            if ($total_pagu < $total_baruRincian) {
                $backValue = '1';
            } else {
                $backValue = '0';
            }
        } catch (Exception $exc) {
            $backValue = '1';
        }

        return $backValue;
    }

    public function getBatasPaguPerDinasAtk($unit_id, $kode_komponen, $komponen_id, $pajak, $vol1, $vol2, $vol3, $vol4, $komponen_penyusun, $volumePenyusun) {
        try {
            $total_baru = $this->getTotalNilaiBaru($komponen_id, $pajak, $vol1, $vol2, $vol3, $vol4, $komponen_penyusun, $volumePenyusun);

            $total_pagu = $this->getNilaiPaguAtkMaks($unit_id);

            $total_rincianDetail = $this->getNilaiPaguAtkSekarang($unit_id);

            $total_baruRincian = $total_baru + $total_rincianDetail;
            if ($total_pagu < $total_baruRincian) {
                $backValue = '1';
            } else {
                $backValue = '0';
            }
        } catch (Exception $exc) {
            $backValue = '1';
        }

        return $backValue;
    }

    public function getBatasPaguPerKegiatanAtk($unit_id, $kode_kegiatan, $komponen_id, $pajak, $vol1, $vol2, $vol3, $vol4, $komponen_penyusun, $volumePenyusun) {
        try {
            $total_baru = $this->getTotalNilaiBaru($komponen_id, $pajak, $vol1, $vol2, $vol3, $vol4, $komponen_penyusun, $volumePenyusun);

            $total_pagu = $this->getNilaiPaguAtkMaksKegiatan($unit_id, $kode_kegiatan);

            $total_rincianDetail = $this->getNilaiPaguAtkKegiatanSekarang($unit_id, $kode_kegiatan);

            $total_baruRincian = $total_baru + $total_rincianDetail;
            if ($total_pagu < $total_baruRincian) {
                $backValue = '1';
            } else {
                $backValue = '0';
            }
        } catch (Exception $exc) {
            $backValue = '1';
        }

        return $backValue;
    }

    public function getBatasPaguPerKegiatanUk($unit_id, $kode_kegiatan, $komponen_id, $pajak, $vol1, $vol2, $vol3, $vol4, $komponen_penyusun, $volumePenyusun) {
        try {
            $total_baru = $this->getTotalNilaiBaru($komponen_id, $pajak, $vol1, $vol2, $vol3, $vol4, $komponen_penyusun, $volumePenyusun);

            $total_pagu = $this->getNilaiPaguUkMaksKegiatan($unit_id, $kode_kegiatan);

            $total_rincianDetail = $this->getNilaiPaguUkKegiatanSekarang($unit_id, $kode_kegiatan);

            $total_baruRincian = $total_baru + $total_rincianDetail;
            // nilai pagu uk harus sama 
            if ($total_pagu == $total_baruRincian) {
                $backValue = '0';
            } 
            else {
                $backValue = '1';
            }
        } catch (Exception $exc) {
            $backValue = '1';
        }

        return $backValue;
    }

    public function getBatasPaguPerDinasforEdit($unit_id, $kegiatan_code, $detail_no, $volume) {
        try {
            $con = Propel::getConnection();
             $c = new Criteria();
             $c->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
             $c->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kegiatan_code);
             $kegiatan = DinasMasterKegiatanPeer::doSelectOne($c);

            $unit_id = $unit_id;
            $komponen_id = $this->getIsiKomponenId($unit_id, $kegiatan_code, $detail_no);
            $pajak = $this->getIsiPajak($unit_id, $kegiatan_code, $detail_no);
            //nilai komponen setelah di edit
            //$total_baru = $this->getTotalNilaiBaruforEdit($komponen_id, $pajak, $volume);
            $total_baru = $this->getTotalNilaiBaruforEdit2($unit_id, $kegiatan_code, $detail_no, $volume);
            //nilai komponen sebelum di edit
            $total_saatIni = $this->getTotalNilaiKomponenBdsrDetailNo($unit_id, $kegiatan_code, $detail_no);

            // $total_pagu = $this->getNilaiPagu($unit_id, $kegiatan_code = null);
            $total_pagu = $this->getNilaiPagu($unit_id, $kegiatan_code, "dinas");

            if(strpos($unit_id,'12')!== FALSE)
            $total_rincianDetail = $this->getNilaiRincianDetailKelurahan($unit_id, $kegiatan_code);
            else
            $total_rincianDetail = $this->getNilaiRincianDetail($unit_id, $kegiatan_code = null);
            
            //$total_rincianDetail = $this->getNilaiRincianDetail($unit_id, $kegiatan_code);
            //print_r($komponen_id.' '.$pajak.' totBaru '.$total_baru.' ');exit;
            //rumus : total baru = total rincian_detail + nilai komponen (setelah di edit) - nilai komponen (sebelum di edit)
            $total_baruRincian = $total_rincianDetail + $total_baru - $total_saatIni;
            //print_r('test dari programer '.$total_rincianDetail.' '.$total_baru.' '.$total_saatIni.' '.$total_pagu);exit;
            if ($total_pagu < $total_baruRincian) {
                if ($total_baru < $total_saatIni) {
                    $backValue = '0';
                } else {
                    $backValue = '1';
                }
            } else {
                $backValue = '0';
            }
        } catch (Exception $exc) {
            $backValue = '1';
        }
        return $backValue;
    }

    public function getBatasPaguPerDinasforLelang($unit_id, $kegiatan_code, $detail_no, $total_baru) {
        try {
            //nilai komponen setelah di edit
            //$total_baru = $this->getTotalNilaiBaruforEdit($komponen_id, $pajak, $volume);
            //$total_baru = $this->getTotalNilaiBaruforEdit2($unit_id, $kegiatan_code, $detail_no, $volume);
            //nilai komponen sebelum di edit
            $total_saatIni = $this->getTotalNilaiKomponenBdsrDetailNo($unit_id, $kegiatan_code, $detail_no);

            $total_pagu = $this->getNilaiPagu($unit_id, $kegiatan_code, 'dinas');

            $total_rincianDetail = $this->getNilaiRincianDetail($unit_id, $kegiatan_code = null);
            //print_r($komponen_id.' '.$pajak.' totBaru '.$total_baru.' ');exit;
            //rumus : total baru = total rincian_detail + nilai komponen (setelah di edit) - nilai komponen (sebelum di edit)
            $total_baruRincian = $total_rincianDetail + $total_baru - $total_saatIni;
            //print_r('test dari programer '.$total_rincianDetail.' '.$total_baru.' '.$total_saatIni.' '.$total_pagu);exit;
            if ($total_pagu < $total_baruRincian) {
                if ($total_baru < $total_saatIni) {
                    $backValue = '0';
                } else {
                    $backValue = '1';
                }
            } else {
                $backValue = '0';
            }
        } catch (Exception $exc) {
            $backValue = '1';
        }
        // var_dump($backValue);die();
        return $backValue;
    }

    public function getBatasPaguPerDinasforKembalikan($unit_id, $kegiatan_code, $detail_no) {
        try {
            //nilai komponen semula (di rincian detail)
            $rd_fuction = new RincianDetail();
            $total_baru = $rd_fuction->getTotalNilaiKomponenBdsrDetailNo($unit_id, $kegiatan_code, $detail_no);
            //nilai komponen menjadi (di dinas rincian detail)
            $total_saatIni = $this->getTotalNilaiKomponenBdsrDetailNo($unit_id, $kegiatan_code, $detail_no);

            $total_pagu = $this->getNilaiPagu($unit_id, $kegiatan_code , "dinas");

            $total_rincianDetail = $this->getNilaiRincianDetail($unit_id, $kegiatan_code = null);
            //print_r($komponen_id.' '.$pajak.' totBaru '.$total_baru.' ');exit;
            //rumus : total baru = total rincian_detail + nilai komponen (setelah di edit) - nilai komponen (sebelum di edit)
            $total_baruRincian = $total_rincianDetail + $total_baru - $total_saatIni;
            //print_r('test dari programer '.$total_rincianDetail.' '.$total_baru.' '.$total_saatIni.' '.$total_pagu);exit;
            if ($total_pagu < $total_baruRincian) {
                if ($total_baru < $total_saatIni) {
                    $backValue = '0';
                } else {
                    $backValue = '1';
                }
            } else {
                $backValue = '0';
            }
        } catch (Exception $exc) {
            $backValue = '1';
        }
        return $backValue;
    }

    public function getBatasPaguPerDinasforEditUK($unit_id, $kegiatan_code, $detail_no, $volume) {
        try {
            $komponen_id = $this->getIsiKomponenId($unit_id, $kegiatan_code, $detail_no);
            $pajak = $this->getIsiPajak($unit_id, $kegiatan_code, $detail_no);
            //nilai komponen setelah di edit
            //$total_baru = $this->getTotalNilaiBaruforEdit($komponen_id, $pajak, $volume);
            $total_baru = $this->getTotalNilaiBaruforEdit2($unit_id, $kegiatan_code, $detail_no, $volume);
            //nilai komponen sebelum di edit
            $total_saatIni = $this->getTotalNilaiKomponenBdsrDetailNo($unit_id, $kegiatan_code, $detail_no);

            $total_pagu = $this->getNilaiPaguUKMaks($unit_id);

            $total_rincianDetail = $this->getNilaiPaguUKSekarang($unit_id);
            //print_r($komponen_id.' '.$pajak.' totBaru '.$total_baru.' ');exit;
            //rumus : total baru = total rincian_detail + nilai komponen (setelah di edit) - nilai komponen (sebelum di edit)
            $total_baruRincian = $total_rincianDetail + $total_baru - $total_saatIni;
            //print_r('test dari programer '.$total_rincianDetail.' '.$total_baru.' '.$total_saatIni.' '.$total_pagu);exit;
            if ($total_pagu < $total_baruRincian) {
                $backValue = '1';
            } else {
                $backValue = '0';
            }
        } catch (Exception $exc) {
            $backValue = '1';
        }
        return $backValue;
    }

    public function getBatasPaguPerKegiatanforEditUK($unit_id, $kegiatan_code, $detail_no, $volume) {
        try {
            $komponen_id = $this->getIsiKomponenId($unit_id, $kegiatan_code, $detail_no);
            $pajak = $this->getIsiPajak($unit_id, $kegiatan_code, $detail_no);
            //nilai komponen setelah di edit
            //$total_baru = $this->getTotalNilaiBaruforEdit($komponen_id, $pajak, $volume);
            $total_baru = $this->getTotalNilaiBaruforEdit2($unit_id, $kegiatan_code, $detail_no, $volume);
            //nilai komponen sebelum di edit
            $total_saatIni = $this->getTotalNilaiKomponenBdsrDetailNo($unit_id, $kegiatan_code, $detail_no);

            $total_pagu = $this->getNilaiPaguUkMaksKegiatan($unit_id, $kegiatan_code);

            $total_rincianDetail = $this->getNilaiPaguUkKegiatanSekarang($unit_id, $kegiatan_code);
            //print_r($komponen_id.' '.$pajak.' totBaru '.$total_baru.' ');exit;
            //rumus : total baru = total rincian_detail + nilai komponen (setelah di edit) - nilai komponen (sebelum di edit)
            $total_baruRincian = $total_rincianDetail + $total_baru - $total_saatIni;
            //print_r('test dari programer '.$total_rincianDetail.' '.$total_baru.' '.$total_saatIni.' '.$total_pagu);exit;
            // harus sama nilai nya
            if ($total_pagu == $total_baruRincian) {
                $backValue = '0';
            } else {
                $backValue = '1';
            }
        } catch (Exception $exc) {
            $backValue = '1';
        }
        return $backValue;
    }

    public function getBatasPaguPerDinasforEditMamin($unit_id, $kegiatan_code, $detail_no, $volume) {
        try {
            $komponen_id = $this->getIsiKomponenId($unit_id, $kegiatan_code, $detail_no);
            $pajak = $this->getIsiPajak($unit_id, $kegiatan_code, $detail_no);
            //nilai komponen setelah di edit
            //$total_baru = $this->getTotalNilaiBaruforEdit($komponen_id, $pajak, $volume);
            $total_baru = $this->getTotalNilaiBaruforEdit2($unit_id, $kegiatan_code, $detail_no, $volume);
            //nilai komponen sebelum di edit
            $total_saatIni = $this->getTotalNilaiKomponenBdsrDetailNo($unit_id, $kegiatan_code, $detail_no);

            if ($total_baru < $total_saatIni) {
                return '0';
            }
            $total_pagu = $this->getNilaiPaguMaminMaks($unit_id);

            $total_rincianDetail = $this->getNilaiPaguMaminSekarang($unit_id);
            //print_r($komponen_id.' '.$pajak.' totBaru '.$total_baru.' ');exit;
            //rumus : total baru = total rincian_detail + nilai komponen (setelah di edit) - nilai komponen (sebelum di edit)
            $total_baruRincian = $total_rincianDetail + $total_baru - $total_saatIni;
            //print_r('test dari programer '.$total_rincianDetail.' '.$total_baru.' '.$total_saatIni.' '.$total_pagu);exit;
            if ($total_pagu < $total_baruRincian) {
                $backValue = '1';
            } else {
                $backValue = '0';
            }
        } catch (Exception $exc) {
            $backValue = '1';
        }
        return $backValue;
    }

    public function getBatasPaguPerDinasforEditAtk($unit_id, $kegiatan_code, $detail_no, $volume) {
        try {
            $komponen_id = $this->getIsiKomponenId($unit_id, $kegiatan_code, $detail_no);
            $pajak = $this->getIsiPajak($unit_id, $kegiatan_code, $detail_no);
            //nilai komponen setelah di edit
            //$total_baru = $this->getTotalNilaiBaruforEdit($komponen_id, $pajak, $volume);
            $total_baru = $this->getTotalNilaiBaruforEdit2($unit_id, $kegiatan_code, $detail_no, $volume);
            //nilai komponen sebelum di edit
            $total_saatIni = $this->getTotalNilaiKomponenBdsrDetailNo($unit_id, $kegiatan_code, $detail_no);

            if ($total_baru < $total_saatIni) {
                return '0';
            }
            $total_pagu = $this->getNilaiPaguAtkMaks($unit_id);

            $total_rincianDetail = $this->getNilaiPaguAtkSekarang($unit_id);
            //print_r($komponen_id.' '.$pajak.' totBaru '.$total_baru.' ');exit;
            //rumus : total baru = total rincian_detail + nilai komponen (setelah di edit) - nilai komponen (sebelum di edit)
            $total_baruRincian = $total_rincianDetail + $total_baru - $total_saatIni;
            //print_r('test dari programer '.$total_rincianDetail.' '.$total_baru.' '.$total_saatIni.' '.$total_pagu);exit;
            if ($total_pagu < $total_baruRincian) {
                $backValue = '1';
            } else {
                $backValue = '0';
            }
        } catch (Exception $exc) {
            $backValue = '1';
        }
        return $backValue;
    }

    public function getBatasPaguBelanjaDevplan($unit_id, $kegiatan_code, $komponen_id, $pajak, $vol1, $vol2, $vol3, $vol4, $komponen_penyusun, $volumePenyusun, $kode_belanja) {
        try {
            $total_baru = $this->getTotalNilaiBaru($komponen_id, $pajak, $vol1, $vol2, $vol3, $vol4, $komponen_penyusun, $volumePenyusun);

            $c = new Criteria();
            $c->add(KuaKegiatanPeer::UNIT_ID, $unit_id);
            $c->add(KuaKegiatanPeer::KODE_KEGIATAN, $kegiatan_code);
            $rs_belanja_devplan = KuaKegiatanPeer::doSelectOne($c);
            if ($kode_belanja == '5.2.1') {
                $total_pagu = $rs_belanja_devplan->getPaguPegawai();
            } elseif ($kode_belanja == '5.2.2') {
                $total_pagu = $rs_belanja_devplan->getPaguBarjas();
            } elseif ($kode_belanja == '5.2.3') {
                $total_pagu = $rs_belanja_devplan->getPaguModal();
            }
            $total_rincianDetail = $this->getNilaiBelanjaSekarang($unit_id, $kegiatan_code, $kode_belanja);
            $total_baruRincian = $total_rincianDetail + $total_baru;
            if ($total_pagu < $total_baruRincian) {
                $backValue = '1';
            } else {
                $backValue = '0';
            }
        } catch (Exception $exc) {
            $backValue = '1';
        }
        return $backValue;
    }

    public function cekPerBelanjaDevplan($unit_id, $kode_kegiatan)
    {
        try {
               
            $belanja_pegawai = 0;
            $belanja_barjas = 0;
            $belanja_modal = 0;
            $menjadi = 0;
            $backValue = 0;
            $con = Propel::getConnection();
            $query = " select pagu_pegawai, pagu_barjas, pagu_modal "
                    . " from " . sfConfig::get('app_default_schema') . ".kua_kegiatan "
                    . " where unit_id='$unit_id' and kode_kegiatan='$kode_kegiatan' ";
            $stmt = $con->prepareStatement($query);
            $rs = $stmt->executeQuery();
            if ($rs->next()) {
                $belanja_pegawai = $rs->getString('pagu_pegawai');
                $belanja_barjas = $rs->getString('pagu_barjas');
                $belanja_modal = $rs->getString('pagu_modal');
            }
            $query2 = "select belanja_code, belanja_name, sum(rd.nilai_anggaran) as nilai
                            from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail rd, " . sfConfig::get('app_default_schema') . ".kelompok_belanja kb
                            where kb.belanja_code=substring(rekening_code, 1, 5) and unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rd.status_hapus=false
                            group by belanja_code, belanja_name
                            order by belanja_code";
            $stmt = $con->prepareStatement($query2);
            $rs = $stmt->executeQuery();
            while ($rs->next()) {
                if ($rs->getString('belanja_code') == '5.2.1') {
                    $menjadi = $rs->getInt('nilai');
                    if($menjadi > $belanja_pegawai)
                        $backValue = '1';
                } elseif ($rs->getString('belanja_code') == '5.2.2') {
                    $menjadi = $rs->getInt('nilai');
                    if($menjadi > $belanja_barjas)
                        $backValue = '1';
                } elseif ($rs->getString('belanja_code') == '5.2.3') {
                    $menjadi = $rs->getInt('nilai');
                    if($menjadi > $belanja_modal)
                        $backValue = '1';
                }
            }
        } catch (Exception $e) {
            $backValue = '1';
        }   
        return $backValue;
    }

    public function getBatasPaguBelanjaDevplanForEdit($unit_id, $kegiatan_code, $detail_no, $volume, $kode_belanja) {
        try {
            $komponen_id = $this->getIsiKomponenId($unit_id, $kegiatan_code, $detail_no);
            $pajak = $this->getIsiPajak($unit_id, $kegiatan_code, $detail_no);
            //nilai komponen setelah di edit
            //$total_baru = $this->getTotalNilaiBaruforEdit($komponen_id, $pajak, $volume);
            $total_baru = $this->getTotalNilaiBaruforEdit2($unit_id, $kegiatan_code, $detail_no, $volume);
            //nilai komponen sebelum di edit
            $total_saatIni = $this->getTotalNilaiKomponenBdsrDetailNo($unit_id, $kegiatan_code, $detail_no);

            $c = new Criteria();
            $c->add(KuaKegiatanPeer::UNIT_ID, $unit_id);
            $c->add(KuaKegiatanPeer::KODE_KEGIATAN, $kegiatan_code);
            $rs_belanja_devplan = KuaKegiatanPeer::doSelectOne($c);
            if ($kode_belanja == '5.2.1') {
                $total_pagu = $rs_belanja_devplan->getPaguPegawai();
            } elseif ($kode_belanja == '5.2.2') {
                $total_pagu = $rs_belanja_devplan->getPaguBarjas();
            } elseif ($kode_belanja == '5.2.3') {
                $total_pagu = $rs_belanja_devplan->getPaguModal();
            }
            //$total_pagu = $this->getNilaiPaguUKMaks($unit_id);
            $total_rincianDetail = $this->getNilaiBelanjaSekarang($unit_id, $kegiatan_code, $kode_belanja);
            //print_r($komponen_id.' '.$pajak.' totBaru '.$total_baru.' ');exit;
            //rumus : total baru = total rincian_detail + nilai komponen (setelah di edit) - nilai komponen (sebelum di edit)
            $total_baruRincian = $total_rincianDetail + $total_baru - $total_saatIni;
            //print_r('test dari programer '.$total_rincianDetail.' '.$total_baru.' '.$total_saatIni.' '.$total_pagu);exit;
            // var_dump("total rincian detail: ".$total_rincianDetail);
            // var_dump("+");
            // var_dump("total baru: ".$total_baru);
            // var_dump("-");
            // var_dump("total saat ini: ".$total_saatIni);
            // var_dump("=");
            // var_dump("total baru rincian: ".$total_baruRincian);
            // var_dump(">");
            // var_dump("total_pagu barjas: ".$total_pagu);
            // die();
            if ($total_pagu < $total_baruRincian) {
                $backValue = '1';
            } else {
                $backValue = '0';
            }
        } catch (Exception $exc) {
            $backValue = '1';
        }
        return $backValue;
    }

    public function getNilaiBelanjaSekarang($unit_id, $kegiatan_code, $kode_belanja) {
        $query = " select sum(nilai_anggaran) as nilai from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail "
                . " where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and "
                . " status_hapus=false and rekening_code ilike '$kode_belanja%' ";
        $con = Propel::getConnection();
        $statement2 = $con->prepareStatement($query);
        $rs_nilai2 = $statement2->executeQuery();
        while ($rs_nilai2->next()) {
            $komponen_id = $rs_nilai2->getString('nilai');
        }
        return $komponen_id;
    }

    public function getIsiKomponenId($unit_id, $kegiatan_code, $detail_no) {
        $queryRd = "select komponen_id as komponen_id from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and detail_no='$detail_no' and status_hapus=FALSE";
        $con = Propel::getConnection();
        $statement2 = $con->prepareStatement($queryRd);
        $rs_nilai2 = $statement2->executeQuery();
        while ($rs_nilai2->next()) {
            $komponen_id = $rs_nilai2->getString('komponen_id');
        }
        return $komponen_id;
    }

    public function getTotalNilaiKomponenBdsrDetailNo($unit_id, $kegiatan_code, $detail_no) {
        $queryRd = "select nilai_anggaran as nilai from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail where 
                unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and detail_no='$detail_no' and status_hapus=FALSE";

        $con = Propel::getConnection();
        $statement2 = $con->prepareStatement($queryRd);
        $rs_nilai2 = $statement2->executeQuery();
        while ($rs_nilai2->next()) {
            $nilai_rincian_sebelum_edit = $rs_nilai2->getString('nilai');
        }
        return $nilai_rincian_sebelum_edit;
    }

    public function getIsiPajak($unit_id, $kegiatan_code, $detail_no) {
        $queryRd = "select pajak as pajak from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and detail_no='$detail_no' and status_hapus=FALSE";
        $con = Propel::getConnection();
        $statement2 = $con->prepareStatement($queryRd);
        $rs_nilai2 = $statement2->executeQuery();
        while ($rs_nilai2->next()) {
            $pajak = $rs_nilai2->getString('pajak');
        }
        return $pajak;
    }

    public function getTotalNilaiBaru($komponen_id, $pajak, $vol1, $vol2, $vol3, $vol4, $komponen_penyusun, $volumePenyusun) {
        /* print_r($komponen_penyusun);
          var_dump($komponen_penyusun);

          print_r('chekbox');
          var_dump($volumePenyusun);exit; */
          // echo $unit_id;die();
        $volume = 0;
        $c = new Criteria();
        $c->add(KomponenPeer::KOMPONEN_ID, $komponen_id);
        $rs_komponen = KomponenPeer::doSelectOne($c);
        if ($rs_komponen) {
            $komponen_harga = $rs_komponen->getKomponenHarga();
            $komponen_name = $rs_komponen->getKomponenName();
            $satuan = $rs_komponen->getSatuan();
        }
        if ($vol1 || $vol2 || $vol3 || $vol4) {
            if ($vol2 == '') {
                $vol2 = 1;
                $volume = $vol1 * $vol2;
            } else if (!$vol2 == '') {
                $volume = $vol1 * $vol2;
            }
            if ($vol3 == '') {
                $vol3 = 1;
                $volume = $volume * $vol3;
            } else if (!$vol3 == '') {
                $volume = $vol1 * $vol2 * $vol3;
            }
            if ($vol4 == '') {
                $vol4 = 1;
                $volume = $volume * $vol4;
            } else if (!$vol4 == '') {
                $volume = $vol1 * $vol2 * $vol3 * $vol4;
            }
        }
        $nilai_komponen = floor($komponen_harga * $volume * ((100 + $pajak) / 100));
        //print_r($komponen_id.' '.$komponen_name.' '.$vol1);exit;
        $totalKomponenPenyusun = 0;
        foreach ($komponen_penyusun as $penyusun) {
            $cekPenyusun = new Criteria();
            $cekPenyusun->add(KomponenPeer::KOMPONEN_ID, $penyusun);
            $rs_cekPenyusun = KomponenPeer::doSelect($cekPenyusun);
            foreach ($rs_cekPenyusun as $komPenyusun) {
                if ($komPenyusun->getKomponenNonPajak() == TRUE) {
                    $pajakPenyusun = 0;
                } else {
                    $pajakPenyusun = 10;
                }
                $penyu = $volumePenyusun[$komPenyusun->getKomponenId()] * $komPenyusun->getKomponenHarga() * ((100 + $pajakPenyusun) / 100);
                $totalKomponenPenyusun+=$penyu;
            }
        }
        $totalKomponenBaru = $nilai_komponen + $totalKomponenPenyusun;
        return $totalKomponenBaru;
    }

    public function getTotalNilaiBaruforEdit($komponen_id, $pajak, $volume) {

        $c = new Criteria();
        $c->add(KomponenPeer::KOMPONEN_ID, $komponen_id);
        $rs_komponen = KomponenPeer::doSelectOne($c);
        if ($rs_komponen) {
            if ($rs_komponen->getKomponenTipe() == 'FISIK') {
                $komponen_harga = $rs_komponen->getKomponenHargaBulat();
            } else {
                $komponen_harga = $rs_komponen->getKomponenHarga();
            }
            $komponen_name = $rs_komponen->getKomponenName();
            $satuan = $rs_komponen->getSatuan();
        }

        $nilai_komponen = floor($komponen_harga * $volume * ((100 + $pajak) / 100));
        //print_r($komponen_id.' '.$komponen_name.' '.$vol1);exit;

        $totalKomponenBaru = $nilai_komponen;
        return $totalKomponenBaru;
    }

    public function getTotalNilaiBaruforEdit2($unit_id, $kegiatan_code, $detail_no, $volume) {
        $c = new Criteria();
        $c->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
        $c->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
        $c->add(DinasRincianDetailPeer::DETAIL_NO, $detail_no);
        $rs_komponen = DinasRincianDetailPeer::doSelectOne($c);
        if ($rs_komponen) {
            $komponen_harga = $rs_komponen->getKomponenHargaAwal();
            $pajak = $rs_komponen->getPajak();
        }
        $totalKomponenBaru = floor($komponen_harga * $volume * ((100 + $pajak) / 100));
        return $totalKomponenBaru;
    }

    public function getNilaiPagu($unit_id, $kegiatan_code, $status) {
       // var_dump($unit_id);die();
        //untuk merubah menjadi nilai pagu pakai query no 1.
        $con = Propel::getConnection();
        $c = new Criteria();
        $c->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
        $c->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kegiatan_code);
        $kegiatan = DinasMasterKegiatanPeer::doSelectOne($c);
        // print_r($kegiatan);die();
        // var_dump($kegiatan);die();
        $nilai_paguTambahan = 0;
        if (sfConfig::get('app_fasilitas_paguBayangan') == 'buka') 
        { // menggunakan nilai rincian + paguBayangan #biasanya setalah dari dewan, dan agar dinas tidak melebihi total yang sudah ditetapkan
            // var_dump($unit_id);die();
            if ($status == "dinas") 
            {
                // var_dump($unit_id);die();
                //awal
//                $queryPagu = "select sum(nilai_anggaran) as nilai from " . sfConfig::get('app_default_schema') . ".murni_rincian_detail where unit_id='$unit_id' and status_hapus=false";
                //awal
                // $a = $kegiatan->getTahap();
                // echo $a;die();
                if ($kegiatan->getTahap() == "pak"){
                    // var_dump($unit_id);die();

                    $queryPagu = "select sum(pagu) as nilai from " . sfConfig::get('app_default_schema') . ".pagu_pak where unit_id='$unit_id' ";
                }
                else
                {
                    // var_dump($unit_id);die();
                //DIPAKAI BAWAH INI
                    // if ($kegiatan->getTahap() == "murni")
                    // $queryPagu = "select sum(alokasi_dana) as nilai from " . sfConfig::get('app_default_schema') . ".dinas_master_kegiatan where unit_id='$unit_id'";
                     if ($kegiatan->getTahap() == "murni")
                     {
                         // $queryPagu = "select sum(alokasi_dana) as nilai from " . sfConfig::get('app_default_schema') . ".dinas_master_kegiatan where unit_id='$unit_id'";
                        //jika kondisi sub kegiatan kelurahan
                        if(strpos($kegiatan->getNamaKegiatan(),'Kelurahan') === FALSE )
                            $queryPagu = "select sum(alokasi_dana) as nilai from " . sfConfig::get('app_default_schema') . ".dinas_master_kegiatan where unit_id='$unit_id' and kode_kegiatan='$kegiatan_code'";
                        else
                        $queryPagu = "select sum(alokasi_dana) as nilai from " . sfConfig::get('app_default_schema') . ".dinas_master_kegiatan where unit_id='$unit_id' and nama_kegiatan ilike '%Kelurahan%'";

                     }                    
                    else if ($kegiatan->getTahap() <> "murni" && $kegiatan->getTahap() <> "pak" )
                     if ($kegiatan->getIsDak() == true)
                     {
                         $queryPagu = "select alokasi as nilai from " . sfConfig::get('app_default_schema') . ".pagu where unit_id='$unit_id'";        
                     }
                      else
                     $queryPagu = "select sum(nilai_anggaran) as nilai from " . sfConfig::get('app_default_schema') . ".rincian_detail where unit_id='$unit_id' and status_hapus=false";                
                }

                $queryPaguTambahan = "select sum(tambahan_pagu) as nilai from " . sfConfig::get('app_default_schema') . ".dinas_master_kegiatan where unit_id='$unit_id'";

                //$queryPagu = "select sum(nilai_anggaran) as nilai from " . sfConfig::get('app_default_schema') . ".pak_bukubiru_rincian_detail where unit_id='$unit_id' and status_hapus=false";
            } 
            else 
            {
                // var_dump($unit_id);die();
                //awal
//                $queryPagu = "select sum(nilai_anggaran) as nilai from " . sfConfig::get('app_default_schema') . ".murni_rincian_detail where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and status_hapus=false";
                //awal
                if ($kegiatan->getTahap() == "pak")
                    $queryPagu = "select pagu as nilai from " . sfConfig::get('app_default_schema') . ".pagu_pak where unit_id='$unit_id' and kegiatan_code='$kegiatan_code'";
                else if ($kegiatan->getTahap() == "murni")
                    $queryPagu = "select sum(alokasi_dana) as nilai from " . sfConfig::get('app_default_schema') . ".dinas_master_kegiatan where unit_id='$unit_id' and kode_kegiatan='$kegiatan_code'";
                else if ($kegiatan->getTahap() <> "murni" && $kegiatan->getTahap() <> "pak" )
                {
                     if ($kegiatan->getIsDak() == true)
                     {
                         $queryPagu = "select alokasi as nilai from " . sfConfig::get('app_default_schema') . ".pagu where unit_id='$unit_id' and kode_kegiatan='$kegiatan_code'";        
                     }
                      else
                            $queryPagu = "select sum(nilai_anggaran) as nilai from " . sfConfig::get('app_default_schema') . ".rincian_detail where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and status_hapus=false";
                }

                     

                $queryPaguTambahan = "select sum(tambahan_pagu) as nilai from " . sfConfig::get('app_default_schema') . ".dinas_master_kegiatan where unit_id='$unit_id' and kode_kegiatan='$kegiatan_code'";
                //$queryPagu = "select sum(nilai_anggaran) as nilai from " . sfConfig::get('app_default_schema') . ".pak_bukubiru_rincian_detail where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and status_hapus=false";
            }
            $statement = $con->prepareStatement($queryPaguTambahan);
            $rs_nilaiTambahan = $statement->executeQuery();
            while ($rs_nilaiTambahan->next()) {
                $nilai_paguTambahan = $rs_nilaiTambahan->getString('nilai');
            }
        } else { 
        //berarti paguBayangan ditutup jadi menggunakan pagu asli
        //$queryPagu = "select pagu as nilai from " . sfConfig::get('app_default_schema') . ".pagu_pak where unit_id='$unit_id'";
            if ($status == "dinas") {
                if ($kegiatan->getTahap() == "pak")
                    $queryPagu = "select pagu as nilai from unit_kerja where unit_id='$unit_id'";
                else if ($kegiatan->getTahap() == "murni")
                    $queryPagu = "select sum(alokasi_dana) as nilai from " . sfConfig::get('app_default_schema') . ".dinas_master_kegiatan where unit_id='$unit_id' and kode_kegiatan='$kegiatan_code'";
                else if ($kegiatan->getTahap() <> "murni" && $kegiatan->getTahap() <> "pak" )
                     $queryPagu = "select sum(nilai_anggaran) as nilai from " . sfConfig::get('app_default_schema') . ".rincian_detail where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and status_hapus=false";
                
                $queryPaguTambahan = 0;
                //$queryPagu = "select sum(nilai_anggaran) as nilai from " . sfConfig::get('app_default_schema') . ".pak_bukubiru_rincian_detail where unit_id='$unit_id' and status_hapus=false";
            } else {
                if ($kegiatan->getTahap() == "pak")
                    $queryPagu = "select pagu as nilai from " . sfConfig::get('app_default_schema') . ".pagu_pak where unit_id='$unit_id' and kegiatan_code='$kegiatan_code'";
                else if ($kegiatan->getTahap() == "murni")
                    $queryPagu = "select sum(alokasi_dana) as nilai from " . sfConfig::get('app_default_schema') . ".dinas_master_kegiatan where unit_id='$unit_id' and kode_kegiatan='$kegiatan_code'";
                else if ($kegiatan->getTahap() <> "murni" && $kegiatan->getTahap() <> "pak" )
                     $queryPagu = "select sum(nilai_anggaran) as nilai from " . sfConfig::get('app_default_schema') . ".rincian_detail where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and status_hapus=false";
                
                $queryPaguTambahan = 0;
                //$queryPagu = "select sum(nilai_anggaran) as nilai from " . sfConfig::get('app_default_schema') . ".pak_bukubiru_rincian_detail where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and status_hapus=false";
            }
        }
        $statement = $con->prepareStatement($queryPagu);
        $rs_nilai = $statement->executeQuery();
        while ($rs_nilai->next()) {
            $nilai_awalPagu = $rs_nilai->getString('nilai');
        }
        $nilai_awal = $nilai_awalPagu + $nilai_paguTambahan;

        return myTools::round_ganjil_genap($nilai_awal);

    }
//    public function getNilaiPagu($unit_id, $kegiatan_code) {
//        $con = Propel::getConnection();
//        if ($kegiatan_code == null) {
//            $queryPagu = "select sum(nilai_anggaran) as nilai from " . sfConfig::get('app_default_schema') . ".murni_bukubiru_rincian_detail where unit_id='$unit_id' and status_hapus=false";
//        } else {
//            $queryPagu = "select sum(nilai_anggaran) as nilai from " . sfConfig::get('app_default_schema') . ".murni_bukubiru_rincian_detail where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and status_hapus=false";
//        }
//        $statement = $con->prepareStatement($queryPagu);
//        $rs_nilai = $statement->executeQuery();
//        while ($rs_nilai->next()) {
//            $nilai_awalPagu = $rs_nilai->getString('nilai');
//        }
//        return $nilai_awalPagu;
//    }

    public function getNilaiPaguUKMaks($unit_id) {
        //untuk merubah menjadi nilai pagu pakai query no 1.
        $con = Propel::getConnection();
        $queryPagu = "select nilai_maks as nilai from " . sfConfig::get('app_default_schema') . ".maks_uk where unit_id='$unit_id'";

        $statement = $con->prepareStatement($queryPagu);
        $rs_nilai = $statement->executeQuery();
        while ($rs_nilai->next()) {
            $nilai_awalPagu = $rs_nilai->getString('nilai');
        }

        $nilai_awal = $nilai_awalPagu;

        return $nilai_awal;
    }

    public function getNilaiPaguMaminMaks($unit_id) {
        //untuk merubah menjadi nilai pagu pakai query no 1.
        $con = Propel::getConnection();
        $queryPagu = "select nilai from " . sfConfig::get('app_default_schema') . ".maks_mamin where unit_id='$unit_id'";

        $statement = $con->prepareStatement($queryPagu);
        $rs_nilai = $statement->executeQuery();
        while ($rs_nilai->next()) {
            $nilai_awalPagu = $rs_nilai->getString('nilai');
        }

        return $nilai_awalPagu;
    }

    public function getNilaiPaguAtkMaks($unit_id) {
        //untuk merubah menjadi nilai pagu pakai query no 1.
        $con = Propel::getConnection();
        $queryPagu = "select nilai from " . sfConfig::get('app_default_schema') . ".maks_atk where unit_id='$unit_id'";

        $statement = $con->prepareStatement($queryPagu);
        $rs_nilai = $statement->executeQuery();
        while ($rs_nilai->next()) {
            $nilai_awalPagu = $rs_nilai->getString('nilai');
        }

        return $nilai_awalPagu;
    }

    public function getNilaiPaguAtkMaksKegiatan($unit_id, $kode_kegiatan) {
        //untuk merubah menjadi nilai pagu pakai query no 1.
        $con = Propel::getConnection();
        $queryPagu = "select nilai from " . sfConfig::get('app_default_schema') . ".maks_atk_kegiatan where unit_id='$unit_id' and kode_kegiatan = '$kode_kegiatan'";

        $statement = $con->prepareStatement($queryPagu);
        $rs_nilai = $statement->executeQuery();
        while ($rs_nilai->next()) {
            $nilai_awalPagu = $rs_nilai->getString('nilai');
        }

        return $nilai_awalPagu;
    }

    public function getNilaiPaguUkMaksKegiatan($unit_id, $kode_kegiatan) {
        //untuk merubah menjadi nilai pagu pakai query no 1.
        $con = Propel::getConnection();
        $queryPagu = "select nilai from " . sfConfig::get('app_default_schema') . ".maks_uk_kegiatan where unit_id='$unit_id' and kode_kegiatan = '$kode_kegiatan'";

        $statement = $con->prepareStatement($queryPagu);
        $rs_nilai = $statement->executeQuery();
        while ($rs_nilai->next()) {
            $nilai_awalPagu = $rs_nilai->getString('nilai');
        }

        return $nilai_awalPagu;
    }

    public function getNilaiRincianDetail($unit_id, $kegiatan_code) {
        if ($kegiatan_code == null) {
            $queryRd = "select sum(nilai_anggaran) as nilai from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail where unit_id='$unit_id' and status_hapus=FALSE";
                       
        } else {
            $queryRd = "select sum(nilai_anggaran) as nilai from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and status_hapus=FALSE";
        }
        $con = Propel::getConnection();
        $statement2 = $con->prepareStatement($queryRd);
        $rs_nilai2 = $statement2->executeQuery();
        while ($rs_nilai2->next()) {
            $nilai_rincian = $rs_nilai2->getString('nilai');
        }
        return $nilai_rincian;
    }

     public function getNilaiRincianDetailKelurahan($unit_id, $kegiatan_code) {
         
        if ($kegiatan_code != null) {
            $con = Propel::getConnection();
            $c = new Criteria();
           $c->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
           $c->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kegiatan_code);
           $kegiatan = DinasMasterKegiatanPeer::doSelectOne($c);

             //untuk kelurahan
            if(strpos($kegiatan->getNamaKegiatan(),'Kelurahan') !==FALSE) 
             $queryRd = "select sum(nilai_anggaran) as nilai from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail where unit_id='$unit_id' and status_hapus=FALSE and kegiatan_code in (select kode_kegiatan from ebudget.dinas_master_kegiatan where unit_id='$unit_id'
                 and nama_kegiatan ilike '%Kelurahan%')";
            else
             $queryRd = "select sum(nilai_anggaran) as nilai from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and status_hapus=FALSE";

            
        } else {
            $queryRd = "select sum(nilai_anggaran) as nilai from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and status_hapus=FALSE";
        }
        $con = Propel::getConnection();
        $statement2 = $con->prepareStatement($queryRd);
        $rs_nilai2 = $statement2->executeQuery();
        while ($rs_nilai2->next()) {
            $nilai_rincian = $rs_nilai2->getString('nilai');
        }
        return $nilai_rincian;
    }

    public function getNilaiPaguUKSekarang($unit_id) {
        $queryRd = "select sum(nilai_anggaran) as nilai "
                . "from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail "
                . "where unit_id='$unit_id' and status_hapus=FALSE and rekening_code = '5.2.1.04.01' ";

        $con = Propel::getConnection();
        $statement2 = $con->prepareStatement($queryRd);
        $rs_nilai2 = $statement2->executeQuery();
        while ($rs_nilai2->next()) {
            $nilai_rincian = $rs_nilai2->getString('nilai');
        }
        return $nilai_rincian;
    }



    public function getNilaiPaguMaminSekarang($unit_id) {
        $queryRd = "select sum(nilai_anggaran) as nilai "
                . "from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail "
                . "where unit_id='$unit_id' and status_hapus=FALSE and rekening_code like '5.2.2.12.%' ";

        $con = Propel::getConnection();
        $statement2 = $con->prepareStatement($queryRd);
        $rs_nilai2 = $statement2->executeQuery();
        while ($rs_nilai2->next()) {
            $nilai_rincian = $rs_nilai2->getString('nilai');
        }
        return $nilai_rincian;
    }

    public function getNilaiPaguAtkSekarang($unit_id) {
        $queryRd = "select sum(nilai_anggaran) as nilai "
                . "from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail "
                . "where unit_id='$unit_id' and status_hapus=FALSE and rekening_code like '5.2.2.01.01' ";

        $con = Propel::getConnection();
        $statement2 = $con->prepareStatement($queryRd);
        $rs_nilai2 = $statement2->executeQuery();
        while ($rs_nilai2->next()) {
            $nilai_rincian = $rs_nilai2->getString('nilai');
        }
        return $nilai_rincian;
    }

    public function getNilaiPaguAtkKegiatanSekarang($unit_id, $kode_kegiatan) {
        $queryRd = "select sum(nilai_anggaran) as nilai "
                . "from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail "
                . "where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and status_hapus=FALSE and rekening_code like '5.2.2.01.01' ";

        $con = Propel::getConnection();
        $statement2 = $con->prepareStatement($queryRd);
        $rs_nilai2 = $statement2->executeQuery();
        while ($rs_nilai2->next()) {
            $nilai_rincian = $rs_nilai2->getString('nilai');
        }
        return $nilai_rincian;
    }

    public function getNilaiPaguUkKegiatanSekarang($unit_id, $kode_kegiatan) {
        $queryRd = "select sum(nilai_anggaran) as nilai "
                . "from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail "
                . "where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and status_hapus=FALSE and rekening_code like '5.2.1.04.01' ";

        $con = Propel::getConnection();
        $statement2 = $con->prepareStatement($queryRd);
        $rs_nilai2 = $statement2->executeQuery();
        while ($rs_nilai2->next()) {
            $nilai_rincian = $rs_nilai2->getString('nilai');
        }
        return $nilai_rincian;
    }

    public function getBatasPaguPerDinasSubKegiatan($unit_id, $perkalian_keoef, $total) {
        $unit_id = $unit_id;
        $total_baru = $total * $perkalian_keoef;
        $total_pagu = $this->getNilaiPagu($unit_id, $kegiatan_code, 'dinas');
        $total_rincianDetail = $this->getNilaiRincianDetail($unit_id, $kegiatan_code = null);

        $total_baruRincian = $total_baru + $total_rincianDetail;
        if ($total_pagu < $total_baruRincian) {
            $backValue = '1';
        } else {
            $backValue = '0';
        }
        return $backValue;
        return $total_pagu;
    }

    public function getNilaiPaguDanSelisih($unit_id) {
        $queryPagu = "select sum(alokasi_dana) as nilai from " . sfConfig::get('app_default_schema') . ".dinas_master_kegiatan where unit_id='$unit_id'";
        $queryRd = "select sum(nilai_anggaran) as nilai from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail where unit_id='$unit_id' and status_hapus=FALSE";
        $con = Propel::getConnection();
        $statement = $con->prepareStatement($queryPagu);
        $rs_nilai = $statement->executeQuery();
        while ($rs_nilai->next()) {
            $nilai_awal = $rs_nilai->getString('nilai');
        }

        $statement2 = $con->prepareStatement($queryRd);
        $rs_nilai2 = $statement2->executeQuery();
        while ($rs_nilai2->next()) {
            $nilai_rincian = $rs_nilai2->getString('nilai');
        }
        //tolak=1, lolos=0;
        //print_r($queryPagu.'  '.$queryRd);
        //$tam=$queryPagu.'  '.$queryRd;
        if ($nilai_awal < $nilai_rincian) {
            $backValue = '1';
        } else {
            $backValue = '0';
        }
        return $backValue;
    }

    public function getVolumeDelivery($unit_id, $kode_kegiatan, $detail_no) {
        $totVolumeDelivery = 0;

        $con = Propel::getConnection();
//        ticket #25 validasi multiyears tahun ke2
        $query_cek_multiyears = "select * from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail rd "
                . "where rd.unit_id='$unit_id' and rd.kegiatan_code='$kode_kegiatan' and rd.detail_no='$detail_no' ";
        $stmt_cek_multiyears = $con->prepareStatement($query_cek_multiyears);
        $t_cek_multiyears = $stmt_cek_multiyears->executeQuery();
        while ($t_cek_multiyears->next()) {
            $tahun_multiyears = $t_cek_multiyears->getString('th_ke_multiyears');
        }
//        ticket #25 validasi multiyears tahun ke2
        $query2 = "select rd.unit_id,rd.kegiatan_code,rd.detail_no,rd.komponen_name,sum(rd.volume),sum(ds.volume_total) as volume,
    
                 sum(rd.nilai_anggaran) as nilai_budgeting,
                 sum(dk.harga_realisasi*dk.volume_realisasi) as nilai_realisasi

                from " . sfConfig::get('app_default_edelivery') . ".detail_komponen dk,
                                    " . sfConfig::get('app_default_eproject') . ".detail_kegiatan de,
                                    " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail rd,
                                    " . sfConfig::get('app_default_edelivery') . ".detail_swakelola ds

                where rd.unit_id='$unit_id' and rd.kegiatan_code='$kode_kegiatan' and rd.detail_no='$detail_no'
                      and rd.volume>0 and rd.status_hapus=FALSE
                      and de.id=dk.detail_kegiatan_id 
                      and dk.detail_swakelola_id=ds.id
                                    and de.kode_detail_kegiatan=rd.unit_id||'.'||rd.kegiatan_code||'.'||rd.detail_no

                group by rd.unit_id,rd.kegiatan_code,rd.detail_no,rd.komponen_name
                     ";

        $stmt = $con->prepareStatement($query2);
        $t = $stmt->executeQuery();
        while ($t->next()) {
            $jumlahRows = $t->getRow();
            if ($t->getString('volume') > 0) {
                $totVolumeDelivery = $totVolumeDelivery + $t->getString('volume');
            }
        }

        return $totVolumeDelivery;
    }

    public function getCekRealisasi($unit_id, $kode_kegiatan, $detail_no) {
        $totRealisasi = 0;

        $con = Propel::getConnection();
//        ticket #25 validasi multiyears tahun ke2
        $query_cek_multiyears = "select * from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail rd "
                . "where rd.unit_id='$unit_id' and rd.kegiatan_code='$kode_kegiatan' and rd.detail_no='$detail_no' ";
        $stmt_cek_multiyears = $con->prepareStatement($query_cek_multiyears);
        $t_cek_multiyears = $stmt_cek_multiyears->executeQuery();
        while ($t_cek_multiyears->next()) {
            $tahun_multiyears = $t_cek_multiyears->getString('th_ke_multiyears');
        }

        //***query realisasi
        $query2 = "select tb0.skpd_kode, tb0.skpd_nama, tb0.keg_kode, tb0.keg_nama, tb0.kode_komponen, tb0.kode_detail_kegiatan, tb0.komponen, sum(tb0.volume) as volume_realisasi, sum(tb0.nilai) as nilai_realisasi
        from (
        (
            select skpd.kode as skpd_kode, skpd.nama as skpd_nama, keg.kode as keg_kode, keg.nama as keg_nama, dkeg.kode_detail_kegiatan, dkeg.kode_komponen,
            dkeg.nama as komponen, dk.volume_kontrak as volume, (dk.harga_realisasi_bulat + dk.lk + dk.biaya_lain) as nilai
            from  edelivery.detail_komponen dk
            join edelivery.kontraks k 
            join eproject.pekerjaan p on k.pekerjaan_id = p.id
            on k.id = dk.kontrak_id
            right join eproject.detail_kegiatan dkeg
            on dkeg.id = dk.detail_kegiatan_id
            join  eproject.kegiatan keg
            on keg.id = dkeg.kegiatan_id
            right join  eproject.skpd skpd
            on skpd.id = keg.skpd_id
            where dk.harga_realisasi > 0 and ( k.status not in(110,100) or 
            (p.metode not in(11) and p.jenis in(4,12,13,14,15,16,1,3,5) and (k.status = 100 ) and k.bentuk_kontrak = 1 ) or
            (p.metode not in(11) and p.jenis in(4,12,13,14,15,16,1) and (k.status = 100 ) and k.bentuk_kontrak in(2,3,13))
            or (k.status = 100 and p.metode = 11) ) and k.id not in (
                select k2.id
                from edelivery.kontraks k2
                join edelivery.pemutusan_kontrak pk on k2.id = pk.kontrak_id
                where k2.metode != 6
            )
            and dk.detail_swakelola_id is null and dkeg.kode_detail_kegiatan='$unit_id.$kode_kegiatan.$detail_no'
        )
        union all
        (
        select skpd.kode as skpd_kode, skpd.nama as skpd_nama, keg.kode as keg_kode, keg.nama as keg_nama, dkeg.kode_detail_kegiatan, dkeg.kode_komponen,
            dkeg.nama as komponen, dk.volume_kontrak as volume, 
            coalesce((dkmc.harga_realisasi_bulat + dkmc.biaya_lain + coalesce(dkmc.lk,0)), (dk.harga_realisasi_bulat + coalesce(dk.lk,0) + dk.biaya_lain)) as realisasi
            from  edelivery.detail_komponen dk
            left join edelivery.det_kom_mc dkmc on dkmc.id = dk.id
            join edelivery.kontraks k on k.id = dk.kontrak_id
            right join eproject.detail_kegiatan dkeg on dkeg.id = dk.detail_kegiatan_id
            join  eproject.kegiatan keg on keg.id = dkeg.kegiatan_id
            right join  eproject.skpd skpd on skpd.id = keg.skpd_id
            join eproject.pekerjaan p on k.pekerjaan_id = p.id
            where dk.harga_realisasi > 0 and k.id not in (
                select k2.id
                from edelivery.kontraks k2
                join edelivery.pemutusan_kontrak pk on k2.id = pk.kontrak_id
                where k2.metode != 6
            ) and (p.metode not in(11) and p.jenis not in(4,12,13,14,15,16,1) and k.status = 100 and k.bentuk_kontrak in(2,3,13))
            and dk.detail_swakelola_id is null and dkeg.kode_detail_kegiatan='$unit_id.$kode_kegiatan.$detail_no' --and k.pekerjaan_id = 21000184
        )
        union all
        (
            select s2.kode as skpd_kode, s2.nama as skpd_nama, keg2.kode as keg_kode, keg2.nama as keg_nama, dk2.kode_detail_kegiatan, dk2.kode_komponen,
            dk2.nama as komponen, coalesce(dkmc2.volume_kontrak, dkom2.volume_kontrak) as volume, 
            coalesce(dkmc2.harga_realisasi_bulat, dkom2.harga_realisasi_bulat + dkom2.biaya_lain) as nilai
            from edelivery.kontraks k3 
            join edelivery.essact_sync es on k3.pekerjaan_id = es.kode_paket
            join edelivery.essact_komponen ek on es.id = ek.essact_sync_id
            join edelivery.detail_komponen dkom2 on dkom2.id = ek.detail_komponen_id
            join eproject.detail_pekerjaan dp2 on dp2.pekerjaan_id = k3.pekerjaan_id and dp2.detail_kegiatan_id = dkom2.detail_kegiatan_id
            join eproject.detail_kegiatan dk2 on dk2.id = dkom2.detail_kegiatan_id
            join eproject.pekerjaan p2 on p2.id = k3.pekerjaan_id
            join eproject.kegiatan keg2 on keg2.id = p2.kegiatan_id
            join eproject.skpd s2 on s2.id = keg2.skpd_id
            left join edelivery.det_kom_mc dkmc2 on dkmc2.id = dkom2.id
            where (k3.status = 110 or k3.id in (
                select k2.id
                from edelivery.kontraks k2
                join edelivery.pemutusan_kontrak pk on k2.id = pk.kontrak_id
                where k2.metode != 6
            )) and k3.metode != 6 and
            es.nilai_belanja > 0 and dk2.kode_detail_kegiatan = '$unit_id.$kode_kegiatan.$detail_no'
        )
        UNION all
        (
        select skpd.kode as skpd_kode, skpd.nama as skpd_nama, keg.kode as keg_kode, keg.nama as keg_nama, dkeg.kode_detail_kegiatan, dkeg.kode_komponen,
        dkeg.nama as komponen, ds.volume_total as volume, ds.nilai
        from edelivery.detail_swakelola ds
        right join eproject.detail_kegiatan dkeg
        on dkeg.id = ds.detail_kegiatan_id
        join eproject.kegiatan keg
        on keg.id = dkeg.kegiatan_id
        right join eproject.skpd skpd
        on skpd.id = keg.skpd_id
        right join edelivery.paket_honor ph
        on ph.id = ds.paket_honor_id
        right join eproject.pekerjaan p
        on p.id = ph.pekerjaan_id
        where p.metode != 11 and dkeg.kode_detail_kegiatan='$unit_id.$kode_kegiatan.$detail_no'
        )
        UNION all
        (
        select skpd.kode as skpd_kode, skpd.nama as skpd_nama, keg.kode as keg_kode, keg.nama as keg_nama, 
        dk.kode_detail_kegiatan , dk.kode_komponen , dk.nama as komponen ,
        coalesce (0) as volume, 
        case when eo.ppn = 10 then round(eod.nominal * 1.1) 
        else eod.nominal end as nilai 
        from edelivery.epls e
        join edelivery.epl_oe eo on eo.epl_id = e.id 
        join edelivery.epl_oe_detail eod  on eo.epl_id = eod.epl_id 
        join eproject.detail_kegiatan dk on dk.id = eod.detail_kegiatan_id 
        join eproject.detail_pekerjaan dp on dp.detail_kegiatan_id = dk.id and dp.pekerjaan_id = e.pekerjaan_id 
        join eproject.pekerjaan p on p.id = dp.pekerjaan_id
        join eproject.kegiatan keg on keg.id = p.kegiatan_id
        join eproject.skpd skpd on skpd.id = keg.skpd_id
        where e.status = 1 and dk.kode_detail_kegiatan = '$unit_id.$kode_kegiatan.$detail_no' and dp.alokasi > 0
        )
        ) tb0
        group by tb0.skpd_kode, tb0.skpd_nama, tb0.keg_kode, tb0.keg_nama, tb0.kode_komponen, tb0.kode_detail_kegiatan, tb0.komponen";

        $stmt = $con->prepareStatement($query2);
        $t = $stmt->executeQuery();
        while ($t->next()) {
            $jumlahRows = $t->getRow();
            if ($t->getString('nilai_realisasi') > 0) {
                $totRealisasi = $totRealisasi + $t->getString('nilai_realisasi');
            }
        }

        // cari realisasi yang putus kontrak
        $query = "select sum(nilai) as nilai
                    from (
                            select k.id, coalesce((mc.harga_realisasi_bulat + mc.lk + mc.biaya_lain), (dkom.harga_realisasi_bulat + dkom.lk + dkom.biaya_lain)) as nilai
                            from " . sfConfig::get('app_default_edelivery') . ".detail_komponen dkom
                            left join " . sfConfig::get('app_default_edelivery') . ".det_kom_mc mc on mc.id = dkom.id
                            join " . sfConfig::get('app_default_edelivery') . ".kontraks k on k.id = dkom.kontrak_id
                            join " . sfConfig::get('app_default_eproject') . ".detail_kegiatan dk on dk.id = dkom.detail_kegiatan_id
                            join (select k.id, (case when pk.jumlah > 0 then pk.termin_ke else pk.termin_ke - 1 end) as termin_ke
                                    from " . sfConfig::get('app_default_edelivery') . ".kontraks k 
                                    join " . sfConfig::get('app_default_eproject') . ".pekerjaan p on p.id = k.pekerjaan_id
                                    join " . sfConfig::get('app_default_eproject') . ".detail_pekerjaan dp on dp.pekerjaan_id = p.id
                                    join " . sfConfig::get('app_default_eproject') . ".detail_kegiatan dk on dk.id = dp.detail_kegiatan_id
                                    left join " . sfConfig::get('app_default_edelivery') . ".pemutusan_kontrak pk on pk.kontrak_id = k.id
                                    where k.metode <> 6 and k.status=110 and dk.kode_detail_kegiatan = '$unit_id.$kode_kegiatan.$detail_no'
                                    group by k.id, pk.termin_ke, pk.jumlah) a ON k.id = a.id and dkom.termin_ke <= a.termin_ke
                            where k.metode <> 6 and k.status=110 and dk.kode_detail_kegiatan = '$unit_id.$kode_kegiatan.$detail_no'
                    ) komponen";
        $stmt = $con->prepareStatement($query);
        $rs = $stmt->executeQuery();
        if ($rs->next()) {
            $nilai = $rs->getString('nilai');
            $totRealisasi += $nilai;
        } else {
            $nilai = null;
        }

        //****query sts
        $query3 = "select dkeg.kode_detail_kegiatan, dkeg.nama, sum(dks.nilai) as nilai_sts, sum(dks.volume) as volume
                    from " . sfConfig::get('app_default_edelivery') . ".sts sts
                    join " . sfConfig::get('app_default_edelivery') . ".detail_sts ds on ds.sts_id = sts.id
                    join " . sfConfig::get('app_default_edelivery') . ".detail_komponen_sts dks on dks.sts_id = ds.sts_id and dks.transaksi_id = ds.transaksi_id
                    join " . sfConfig::get('app_default_edelivery') . ".detail_komponen dk on dk.id = dks.detail_komponen_id
                    join " . sfConfig::get('app_default_eproject') . ".detail_kegiatan dkeg on dkeg.id = dk.detail_kegiatan_id
                    where sts.status = 100 and sts.jenis = 1 and dkeg.kode_detail_kegiatan = '$unit_id.$kode_kegiatan.$detail_no'
                    group by dkeg.kode_detail_kegiatan, dkeg.nama";
        $stmt1 = $con->prepareStatement($query3);
        $rs1 = $stmt1->executeQuery();
        if ($rs1->next()) {
            $nilai_sts = $rs1->getFloat('nilai_sts');
            $totRealisasi -= $nilai_sts;
        } else {
            $nilai_sts = null;
        }

         //*****query lk
        $query4 = "select dk.kode_detail_kegiatan, coalesce(sum(lkk.jumlah),0) as nilai_lk, coalesce(sum(lkk.volume_lk),0) as volume_lk
        from edelivery.kontrak_mc_addendum as kma
        join edelivery.lebih_kurang_komponen lkk on lkk.kontrak_mc_addendum_id = kma.id
        join eproject.detail_kegiatan dk on dk.id = lkk.detail_kegiatan_id
        join edelivery.kontraks k on kma.kontrak_id = k.id
        join eproject.pekerjaan p on k.pekerjaan_id = p.id 
        where dk.kode_detail_kegiatan = '$unit_id.$kode_kegiatan.$detail_no' and 
        ( k.status not in(100) or
        (p.metode not in(11) and p.jenis in(4,12,13,14,15,16,1,3,5) and (k.status = 100 ) and k.bentuk_kontrak = 1 ) or
        (p.metode not in(11) and p.jenis in(4,12,13,14,15,16,1) and (k.status = 100 ) and k.bentuk_kontrak in(2,3,13))
        or (k.status = 100 and p.metode = 11) )
        group by dk.kode_detail_kegiatan";
        $stmt2 = $con->prepareStatement($query4);
        $rs2 = $stmt2->executeQuery();
        if ($rs2->next()) {
            $nilai_lk = $rs2->getFloat('nilai_lk');
            $totRealisasi += $nilai_lk;
        } else {
            $nilai_lk = null;
        }

        return $totRealisasi;
    }

    public function getCekRealisasi2($unit_id, $kode_kegiatan, $detail_no) {
        $totRealisasi = 0;

        $con = Propel::getConnection();
//        ticket #25 validasi multiyears tahun ke2
        $query_cek_multiyears = "select * from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail rd "
                . "where rd.unit_id='$unit_id' and rd.kegiatan_code='$kode_kegiatan' and rd.detail_no='$detail_no' ";
        $stmt_cek_multiyears = $con->prepareStatement($query_cek_multiyears);
        $t_cek_multiyears = $stmt_cek_multiyears->executeQuery();
        while ($t_cek_multiyears->next()) {
            $tahun_multiyears = $t_cek_multiyears->getString('th_ke_multiyears');
        }
        $query2 = "select  tb1.nilai as nilai_realisasi, tb1.volume as volume_realisasi
        from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail rd
        left join unit_kerja uk
        on uk.unit_id = rd.unit_id
        left join (
        select tb0.skpd_kode, tb0.skpd_nama, tb0.keg_kode, tb0.keg_nama, tb0.kode_komponen, tb0.kode_detail_kegiatan, tb0.komponen, sum(tb0.volume) as volume, sum(tb0.nilai) as nilai
        from ((
        select skpd.kode as skpd_kode, skpd.nama as skpd_nama, keg.kode as keg_kode, keg.nama as keg_nama, dkeg.kode_detail_kegiatan, dkeg.kode_komponen, dkeg.nama as komponen, dk.volume_kontrak as volume, dk.harga_realisasi_bulat as nilai
        from  " . sfConfig::get('app_default_edelivery') . ".detail_komponen dk
        right join " . sfConfig::get('app_default_eproject') . ".detail_kegiatan dkeg
        on dkeg.id = dk.detail_kegiatan_id
        join  " . sfConfig::get('app_default_eproject') . ".kegiatan keg
        on keg.id = dkeg.kegiatan_id
        right join  " . sfConfig::get('app_default_eproject') . ".skpd skpd
        on skpd.id = keg.skpd_id
        where dk.harga_realisasi > 0 and dk.detail_swakelola_id is null
        )
        UNION all
        (
        select skpd.kode as skpd_kode, skpd.nama as skpd_nama, keg.kode as keg_kode, keg.nama as keg_nama, dkeg.kode_detail_kegiatan, dkeg.kode_komponen, dkeg.nama as komponen, ds.volume_total as volume, ds.nilai
        from " . sfConfig::get('app_default_edelivery') . ".detail_swakelola ds
        right join " . sfConfig::get('app_default_eproject') . ".detail_kegiatan dkeg
        on dkeg.id = ds.detail_kegiatan_id
        join " . sfConfig::get('app_default_eproject') . ".kegiatan keg
        on keg.id = dkeg.kegiatan_id
        right join " . sfConfig::get('app_default_eproject') . ".skpd skpd
        on skpd.id = keg.skpd_id
        )) tb0
        group by tb0.skpd_kode, tb0.skpd_nama, tb0.keg_kode, tb0.keg_nama, tb0.kode_komponen, tb0.kode_detail_kegiatan, tb0.komponen
        ) tb1
        on tb1.kode_detail_kegiatan = rd.unit_id||'.'||rd.kegiatan_code||'.'||rd.detail_no
        where   rd.status_hapus='false' and rd.nilai_anggaran <> 0 and rd.unit_id <> '9999' and rd.unit_id='$unit_id' and rd.kegiatan_code='$kode_kegiatan' and rd.detail_no='$detail_no'
        order by uk.unit_id, uk.unit_name, tb1.keg_kode, tb1.keg_nama, tb1.kode_komponen";

        $stmt = $con->prepareStatement($query2);
        $t = $stmt->executeQuery();
        while ($t->next()) {
            $jumlahRows = $t->getRow();
            if ($t->getString('nilai_realisasi') > 0 || $t->getString('volume_realisasi') > 0) {
                $totRealisasi = $totRealisasi + $t->getString('nilai_realisasi');
                $totVolume = $totVolume + $t->getString('volume_realisasi');
            }
        }
        return $totRealisasi . '|' . $totVolume;
    }

    public function getCekNilaiSwakelolaDelivery2($unit_id, $kode_kegiatan, $detail_no) {
        $totNilaiSwakelola = 0;

        $con = Propel::getConnection();
//        ticket #25 validasi multiyears tahun ke2
        $query_cek_multiyears = "select * from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail rd "
                . "where rd.unit_id='$unit_id' and rd.kegiatan_code='$kode_kegiatan' and rd.detail_no='$detail_no' ";
        $stmt_cek_multiyears = $con->prepareStatement($query_cek_multiyears);
        $t_cek_multiyears = $stmt_cek_multiyears->executeQuery();
        while ($t_cek_multiyears->next()) {
            $tahun_multiyears = $t_cek_multiyears->getString('th_ke_multiyears');
        }
//        ticket #25 validasi multiyears tahun ke2
//        $query2 = "select rd.unit_id,rd.kegiatan_code,rd.detail_no,rd.subtitle,rd.komponen_name,rd.kode_sub,
//               de.kode_detail_kegiatan,dk.id,
//               sum(rd.nilai_anggaran) as nilai_budgeting,
//               sum(dk.harga_realisasi*dk.volume_realisasi) as nilai_realisasi,
//
//              from " . sfConfig::get('app_default_edelivery') . ".detail_swakelola dk,
//                                    " . sfConfig::get('app_default_eproject') . ".detail_kegiatan de,
//                                    " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail rd
//
//              where rd.unit_id='$unit_id' and rd.kegiatan_code='$kode_kegiatan' and rd.detail_no='$detail_no'
//                    and rd.volume>0 and rd.status_hapus=FALSE
//                    and de.id=dk.detail_kegiatan_id 
//                                    and de.kode_detail_kegiatan=rd.unit_id||'.'||rd.kegiatan_code||'.'||rd.detail_no
//
//              group by rd.unit_id,rd.kegiatan_code,rd.detail_no,rd.subtitle,rd.komponen_name,rd.kode_sub,
//                    de.kode_detail_kegiatan,dk.id,dk.harga_realisasi
//                    having sum(dk.harga_realisasi*dk.volume_realisasi) >0
//                    order by rd.unit_id,rd.kegiatan_code,rd.detail_no";
        $query2 = "select rd.unit_id,rd.kegiatan_code,rd.detail_no,rd.subtitle,rd.komponen_name,rd.kode_sub,
                 de.kode_detail_kegiatan,dk.id,
                 sum(rd.nilai_anggaran) as nilai_budgeting,
                 dk.nilai as nilai_realisasi

                from " . sfConfig::get('app_default_edelivery') . ".detail_swakelola dk,
                                    " . sfConfig::get('app_default_eproject') . ".detail_kegiatan de,
                                    " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail rd

                where rd.unit_id='$unit_id' and rd.kegiatan_code='$kode_kegiatan' and rd.detail_no='$detail_no'
                      and rd.volume>0 and rd.status_hapus=FALSE
                      and de.id=dk.detail_kegiatan_id 
                                      and de.kode_detail_kegiatan=rd.unit_id||'.'||rd.kegiatan_code||'.'||rd.detail_no

                group by rd.unit_id,rd.kegiatan_code,rd.detail_no,rd.subtitle,rd.komponen_name,rd.kode_sub,
                      de.kode_detail_kegiatan,dk.id,dk.nilai
                      having dk.nilai >0
                      order by rd.unit_id,rd.kegiatan_code,rd.detail_no";

        $stmt = $con->prepareStatement($query2);
        $t = $stmt->executeQuery();
        while ($t->next()) {
            $jumlahRows = $t->getRow();
            if ($t->getString('nilai_realisasi') > 0) {
                $totNilaiSwakelola = $totNilaiSwakelola + $t->getString('nilai_realisasi');
            }
        }

        //tambhaan pengurangan sts
        $query3 = "select dkeg.kode_detail_kegiatan, dkeg.nama, sum(dks.nilai) as nilai_sts, sum(dks.volume) as volume
                    from " . sfConfig::get('app_default_edelivery') . ".sts sts
                    join " . sfConfig::get('app_default_edelivery') . ".detail_sts ds on ds.sts_id = sts.id
                    join " . sfConfig::get('app_default_edelivery') . ".detail_komponen_sts dks on dks.sts_id = ds.sts_id and dks.transaksi_id = ds.transaksi_id
                    join " . sfConfig::get('app_default_edelivery') . ".detail_komponen dk on dk.id = dks.detail_komponen_id
                    join " . sfConfig::get('app_default_eproject') . ".detail_kegiatan dkeg on dkeg.id = dk.detail_kegiatan_id
                    where sts.status = 100 and sts.jenis = 1 and dkeg.kode_detail_kegiatan = '$unit_id.$kode_kegiatan.$detail_no'
                    group by dkeg.kode_detail_kegiatan, dkeg.nama";
        $stmt1 = $con->prepareStatement($query3);
        $rs1 = $stmt1->executeQuery();
        if ($rs1->next()) {
            $nilai_sts = $rs1->getFloat('nilai_sts');
            $totNilaiSwakelola -= $nilai_sts;
        } else {
            $nilai_sts = null;
        }

        return $totNilaiSwakelola;
    }

    public function getCekNilaiSwakelolaDelivery() {
        $totNilaiSwakelola = 0;
        $jumlahRows = 0;
        $unit_id = $this->getUnitId();
        $kegiatan = $this->getKegiatanCode();
        $no = $this->getDetailNo();
        $sub = $this->getSub();
        $query2 = "select rd.unit_id,rd.kegiatan_code,rd.detail_no,rd.subtitle,rd.komponen_name,rd.kode_sub,
                 de.kode_detail_kegiatan,dk.detail_swakelola_id,dk.kontrak_id,
                 sum(rd.volume * rd.komponen_harga_awal * (100 + rd.pajak) / 100) as nilai_budgeting,
                 sum(dk.harga_realisasi*dk.volume_realisasi) as nilai_realisasi,
                 sum(dk.harga_kontrak*dk.volume_kontrak) as nilai_kontrak

                from " . sfConfig::get('app_default_edelivery') . ".detail_komponen dk, " . sfConfig::get('app_default_eproject') . ".detail_kegiatan de, " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail rd

                where rd.unit_id='$unit_id' and rd.kegiatan_code='$kegiatan' and rd.detail_no='$no'
                      and rd.subtitle ilike '$sub' and rd.volume>0 and rd.status_hapus=false
                      and de.id=dk.detail_kegiatan_id and substring(de.kode_detail_kegiatan,1,4)=rd.unit_id
                      and substring(de.kode_detail_kegiatan,6,4)=rd.kegiatan_code
                      and (substring(de.kode_detail_kegiatan,11))::integer=rd.detail_no

                group by rd.unit_id,rd.kegiatan_code,rd.detail_no,rd.subtitle,rd.komponen_name,rd.kode_sub,
                      de.kode_detail_kegiatan,dk.detail_swakelola_id,dk.kontrak_id
                      having sum(dk.harga_realisasi*dk.volume_realisasi)>0 or sum(dk.harga_kontrak*dk.volume_kontrak)>0
                      order by rd.unit_id,rd.kegiatan_code,rd.detail_no";
        //print_r($query2);//exit;

        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query2);
        $t = $stmt->executeQuery();
        //print_r($t);

        while ($t->next()) {
            $jumlahRows = $t->getRow();
            if ($t->getString('nilai_realisasi') > 0) {
                $totNilaiSwakelola = $totNilaiSwakelola + $t->getString('nilai_realisasi');
            }
        }
        // $totNilaiSwakelola=100;
        return $totNilaiSwakelola;
    }

     public function cekKontrak($detail_kegiatan) {
        $url = "https://edelivery.surabaya.go.id/2020/api/v1/budgeting/get-gaji";
        $postData = array(
            'kode_detail_kegiatan' => $detail_kegiatan
        );

        set_time_limit(0);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_USERPWD, 'budget_4p1:budget4p1rahas1aSek4li');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($postData));
        $output = curl_exec($ch);
        $obj = json_decode($output);

        $result = 0;
        foreach($obj->data as $value) {
            foreach($value->daftar_gaji as $gaji) {
                $result += $gaji->realisasi;
            }
        }

        return $result;
    }

    public function getCekNilaiKontrakDelivery2($unit_id, $kode_kegiatan, $detail_no) {
        $totNilaiKontrak = 0;

        $con = Propel::getConnection();

//        ticket #25 validasi multiyears tahun ke2
        $query_cek_multiyears = "select * from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail rd "
                . "where rd.unit_id='$unit_id' and rd.kegiatan_code='$kode_kegiatan' and rd.detail_no='$detail_no' ";
        $stmt_cek_multiyears = $con->prepareStatement($query_cek_multiyears);
        $t_cek_multiyears = $stmt_cek_multiyears->executeQuery();
        while ($t_cek_multiyears->next()) {
            $tahun_multiyears = $t_cek_multiyears->getString('th_ke_multiyears');
        }
//        ticket #25 validasi multiyears tahun ke2        
//        $query2 = "select rd.unit_id,rd.kegiatan_code,rd.detail_no,rd.subtitle,rd.komponen_name,rd.kode_sub,
//               de.kode_detail_kegiatan,dk.detail_swakelola_id,dk.kontrak_id,
//               sum(rd.nilai_anggaran) as nilai_budgeting,
//               sum(dk.harga_realisasi_bulat) as nilai_kontrak,
//                                 sum(dk.biaya_lain) as nilai_tambahan, 
//                                 sum(dk.lk) as adendum_perubahan,
//                                 sum(dk.pengurangan_sts) as pengembalian_sts 
//
//              from " . sfConfig::get('app_default_edelivery') . ".detail_komponen dk,
//                   " . sfConfig::get('app_default_edelivery') . ".kontraks k,
//                                     " . sfConfig::get('app_default_eproject') . ".detail_kegiatan de,
//                                     " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail rd
//
//              where rd.unit_id='$unit_id' and rd.kegiatan_code='$kode_kegiatan' and rd.detail_no='$detail_no'
//                    and rd.volume>0 and rd.status_hapus=FALSE
//                    and de.id=dk.detail_kegiatan_id 
//                                      and de.kode_detail_kegiatan=rd.unit_id||'.'||rd.kegiatan_code||'.'||rd.detail_no
//                                      and dk.kontrak_id=k.id and k.bukti_perjanjian=3
//              group by rd.unit_id,rd.kegiatan_code,rd.detail_no,rd.subtitle,rd.komponen_name,rd.kode_sub,
//                    de.kode_detail_kegiatan,dk.detail_swakelola_id,dk.kontrak_id
//                    having  sum(dk.harga_realisasi_bulat)>0
//                    order by rd.unit_id,rd.kegiatan_code,rd.detail_no";
        $query2 = "select rd.unit_id,rd.kegiatan_code,rd.detail_no,rd.subtitle,rd.komponen_name,rd.kode_sub,
                 de.kode_detail_kegiatan,dk.detail_swakelola_id,dk.kontrak_id,
                 sum(rd.nilai_anggaran) as nilai_budgeting,
                 sum(dk.harga_realisasi_bulat) as nilai_kontrak,
                                 sum(dk.biaya_lain) as nilai_tambahan, 
                                 sum(dk.lk) as adendum_perubahan,
                                 sum(dk.pengurangan_sts) as pengembalian_sts 

                from " . sfConfig::get('app_default_edelivery') . ".detail_komponen dk,
                     " . sfConfig::get('app_default_edelivery') . ".kontraks k,
                                     " . sfConfig::get('app_default_eproject') . ".detail_kegiatan de,
                                     " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail rd

                where rd.unit_id='$unit_id' and rd.kegiatan_code='$kode_kegiatan' and rd.detail_no='$detail_no'
                      and rd.volume>0 and rd.status_hapus=FALSE
                      and de.id=dk.detail_kegiatan_id 
                                      and de.kode_detail_kegiatan=rd.unit_id||'.'||rd.kegiatan_code||'.'||rd.detail_no
                                      and dk.kontrak_id=k.id and dk.kontrak_id is not null
                group by rd.unit_id,rd.kegiatan_code,rd.detail_no,rd.subtitle,rd.komponen_name,rd.kode_sub,
                      de.kode_detail_kegiatan,dk.detail_swakelola_id,dk.kontrak_id
                      having  sum(dk.harga_realisasi_bulat)>0
                      order by rd.unit_id,rd.kegiatan_code,rd.detail_no";

// UNTUK YANG PUNYA 2 KONTRAK, TETAPI SALAH SATUNYA SEHARUSNYA DIBATALKAN -> SPECTAL CASE
//        if ($unit_id == '2600' && $kode_kegiatan == '1.03.31.0005' && $detail_no == '111') {
//            $query2 = "select rd.unit_id,rd.kegiatan_code,rd.detail_no,rd.subtitle,rd.komponen_name,rd.kode_sub,
//               de.kode_detail_kegiatan,dk.detail_swakelola_id,dk.kontrak_id,
//               sum(rd.nilai_anggaran) as nilai_budgeting,
//               sum(dk.harga_realisasi_bulat) as nilai_kontrak,
//                                 sum(dk.biaya_lain) as nilai_tambahan, 
//                                 sum(dk.lk) as adendum_perubahan,
//                                 sum(dk.pengurangan_sts) as pengembalian_sts 
//
//              from " . sfConfig::get('app_default_edelivery') . ".detail_komponen dk,
//                   " . sfConfig::get('app_default_edelivery') . ".kontraks k,
//                                     " . sfConfig::get('app_default_eproject') . ".detail_kegiatan de,
//                                     " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail rd
//
//              where rd.unit_id='$unit_id' and rd.kegiatan_code='$kode_kegiatan' and rd.detail_no='$detail_no'
//                    and rd.volume>0 and rd.status_hapus=FALSE
//                    and de.id=dk.detail_kegiatan_id 
//                                      and de.kode_detail_kegiatan=rd.unit_id||'.'||rd.kegiatan_code||'.'||rd.detail_no
//                                      and dk.kontrak_id=k.id and dk.kontrak_id is not null
//              group by rd.unit_id,rd.kegiatan_code,rd.detail_no,rd.subtitle,rd.komponen_name,rd.kode_sub,
//                    de.kode_detail_kegiatan,dk.detail_swakelola_id,dk.kontrak_id
//                    having  sum(dk.harga_realisasi_bulat)>0
//                    order by rd.unit_id,rd.kegiatan_code,rd.detail_no
//                    LIMIT 1";
//        }
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query2);
        $t = $stmt->executeQuery();
        while ($t->next()) {
            $jumlahRows = $t->getRow();
            if ($t->getString('nilai_kontrak') > 0) {
                $totNilaiKontrak = $totNilaiKontrak + $t->getString('nilai_kontrak') + $t->getString('nilai_tambahan') + $t->getString('adendum_perubahan');
            }
        }

//        ticket #25 validasi multiyears tahun ke2
        if ($tahun_multiyears == 2) {
            $tahun_sekarang = sfConfig::get('app_tahun_default');
            $tahun_kemaren = $tahun_sekarang - 1;

            $query_id_pekerjaan = "select dp.* 
                from 
                " . sfConfig::get('app_default_eproject') . ".detail_pekerjaan dp, 
                " . sfConfig::get('app_default_eproject') . ".detail_kegiatan dk, 
                " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail rd 
                where rd.unit_id = '$unit_id' and rd.kegiatan_code = '$kode_kegiatan' and rd.detail_no = $detail_no 
                and dk.kode_detail_kegiatan = rd.unit_id||'.'||rd.kegiatan_code||'.'||rd.detail_no 
                and dp.detail_kegiatan_id = dk.id";
            $stmt_id_pekerjaan = $con->prepareStatement($query_id_pekerjaan);
            $t_id_pekerjaan = $stmt_id_pekerjaan->executeQuery();
            while ($t_id_pekerjaan->next()) {
                $id_pekerjaan = $t_id_pekerjaan->getString('pekerjaan_id');
            }

            $query_sp2d_tahun_lalu = " select p.id, sum(es.nilai_belanja) as total 
                from " . sfConfig::get('app_default_eproject') . ".detail_kegiatan dk 
                left join " . sfConfig::get('app_default_eproject') . ".kegiatan k 
                on k.id = dk.kegiatan_id 
                left join " . sfConfig::get('app_default_eproject') . ".pekerjaan p 
                on p.kegiatan_id = k.id 
                left join " . sfConfig::get('app_default_edelivery') . ".essact_sync es 
                on es.kode_paket = p.id 
                where p.id = $id_pekerjaan and date_part('year',es.tanggal_sp2d) = '$tahun_kemaren' 
                group by p.id ";
            $stmt_sp2d_tahun_lalu = $con->prepareStatement($query_sp2d_tahun_lalu);
            $t_sp2d_tahun_lalu = $stmt_sp2d_tahun_lalu->executeQuery();
            while ($t_sp2d_tahun_lalu->next()) {
                $sp2d_tahun_lalu = $t_sp2d_tahun_lalu->getString('total');
            }

            $totNilaiKontrak = $totNilaiKontrak - $sp2d_tahun_lalu;
        }
//        ticket #25 validasi multiyears tahun ke2        

        return $totNilaiKontrak;
    }

     public function getCekVolumeSPK($unit_id, $kode_kegiatan, $detail_no) 
     {
        $totVolumeSPK=0;

        $query="select edk.id as detail_kegiatan_id, edk.kode_detail_kegiatan, edk.kode_komponen as kode_komponen,edk.nama as nama_komponen, count(ek.id) as jumlah_orang, sum(CASE
        WHEN EXTRACT(MONTH FROM dps.kontrak_mulai) = EXTRACT(MONTH FROM dps.kontrak_selesai) THEN 1
        WHEN EXTRACT(MONTH FROM dps.kontrak_mulai) != EXTRACT(MONTH FROM dps.kontrak_selesai) THEN EXTRACT(MONTH FROM dps.kontrak_selesai)-EXTRACT(MONTH FROM dps.kontrak_mulai)+1
        ELSE NULL
        END) as volume_spk
        from
        edelivery.kontraks ek
        join eproject.detail_kegiatan edk on edk.id = ek.kode_komponen
        join ebudget.dinas_rincian_detail rd on rd.detail_kegiatan =  edk.kode_detail_kegiatan
        join edelivery.data_personal_swa dps on dps.id = ek.data_personal_swa_id
        where rd.unit_id='$unit_id' and rd.kegiatan_code=' $kode_kegiatan' and detail_no=$detail_no
        group by edk.id
        order by edk.nama";
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query);
        $t = $stmt->executeQuery();
        while ($t->next()) {          
           
               $totVolumeSPK = $totVolumeSPK + $t->getString('volume_spk');           
        }        
        return $totVolumeSPK;
     }
     
     public function getCekVolumeSPKArray($unit_id, $kode_kegiatan, $detail_no) 
     {
        $con = Propel::getConnection();
        //$totVolumeSPK=0;    
        //kumpulan kode detail kegiatan yang divalidasi
        $kumpulan_kode_detail_kegiatan = array();
        foreach ($detail_no as $no) {
            $kumpulan_kode_detail_kegiatan[] = "'$unit_id.$kode_kegiatan.$no'";
        }
        $kode_detail_kegiatan = implode(',', $kumpulan_kode_detail_kegiatan);
        $kode = $kumpulan_kode_detail_kegiatan[0];

        $arr_totVolumeSPK = array();

        $query="select edk.id as detail_kegiatan_id, edk.kode_detail_kegiatan, edk.kode_komponen as kode_komponen,edk.nama as nama_komponen, count(ek.id) as jumlah_orang, sum(CASE
        WHEN EXTRACT(MONTH FROM dps.kontrak_mulai) = EXTRACT(MONTH FROM dps.kontrak_selesai) THEN 1
        WHEN EXTRACT(MONTH FROM dps.kontrak_mulai) != EXTRACT(MONTH FROM dps.kontrak_selesai) THEN EXTRACT(MONTH FROM dps.kontrak_selesai)-EXTRACT(MONTH FROM dps.kontrak_mulai)+1
        ELSE NULL
        END) as volume_spk
        from
        edelivery.kontraks ek
        join eproject.detail_kegiatan edk on edk.id = ek.kode_komponen
        join ebudget.dinas_rincian_detail rd on rd.detail_kegiatan =  edk.kode_detail_kegiatan
        join edelivery.data_personal_swa dps on dps.id = ek.data_personal_swa_id
        where rd.unit_id||'.'||rd.kegiatan_code||'.'||rd.detail_no in ($kode_detail_kegiatan)
        group by edk.kode_detail_kegiatan,edk.id,edk.kode_komponen,edk.nama 
        order by edk.nama";
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query);
        $t = $stmt->executeQuery();
        while ($t->next()) {
            if ($kode != $t->getString('kode_detail_kegiatan')) {
                $arr_totVolumeSPK[$kode] = $totVolume;
                $totVolume = $t->getString('volume_spk');
                $kode = $t->getString('kode_detail_kegiatan');
            } else {
                $totVolume += $t->getString('volume_spk');
            }
        }
        $arr_totVolumeSPK[$kode] = $totVolume;
        return $arr_totVolumeSPK;
       
     }


      public function cekKomponenDobel($unit_id, $kode_kegiatan,$detail_no) {

        $kumpulan_kode_detail_kegiatan = array();
        foreach ($detail_no as $no) {
            $kumpulan_kode_detail_kegiatan[] = "'$unit_id.$kode_kegiatan.$no'";
        }
        $total_rekening_semula = 0;
        $komponen_name = '';
        $kode_detail_kegiatan = implode(',', $kumpulan_kode_detail_kegiatan);

        $arr_error = array();        
       
        
         // $query = "select komponen_id,komponen_name,count (*) as jumlah from ebudget.dinas_rincian_detail
         //    where unit_id ='$unit_id' and kegiatan_code='$kode_kegiatan' and status_hapus=false and detail_kegiatan in 
         //    ($kode_detail_kegiatan)
         // group by komponen_id,komponen_name,rekening_code,detail_name";
         $query = "(select komponen_id,komponen_name,count (*) as jumlah from ebudget.dinas_rincian_detail
                where unit_id ='$unit_id' and kegiatan_code='$kode_kegiatan' and status_hapus=false and detail_kegiatan in 
            ($kode_detail_kegiatan) and tipe <> 'FISIK'  
                group by komponen_id,komponen_name,rekening_code,detail_name)
         union
         (
              select komponen_id,komponen_name,count (*) as jumlah from ebudget.dinas_rincian_detail
              where unit_id ='$unit_id' and kegiatan_code='$kode_kegiatan' and status_hapus=false and detail_kegiatan in 
              ($kode_detail_kegiatan) and tipe <> 'FISIK'
              group by komponen_id,komponen_name,rekening_code
          )";
            //echo $query; exit;
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs_harga_belanja = $stmt->executeQuery();
            while ($rs_harga_belanja->next()) {
                $total_rekening_semula = $rs_harga_belanja->getInt('jumlah');
                $komponen_name= $rs_harga_belanja->getString('komponen_name');
               
            }  

           // die($total_rekening_semula);

            if ($total_rekening_semula > 1) {
                //echo $query . '<br><br>' . $query2; exit;
                $arr_error[$komponen_id] = "$komponen_name|$total_rekening_semula";
            }
            
        
        if ($arr_error)
            return $arr_error;
        else
            return FALSE;
    }

    public function getCekVolumeRealisasi($unit_id, $kode_kegiatan, $detail_no) {
        $totVolumeRealisasi = 0;

//        $query2 = "select sum(tb1.volume) as volume_realisasi
//        from  " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail rd
//        right join ((
//        select skpd.kode as skpd_kode, skpd.nama as skpd_nama, keg.kode as keg_kode, keg.nama as keg_nama, dkeg.kode_detail_kegiatan, dkeg.kode_komponen, dkeg.nama as komponen, dk.volume_kontrak as volume, dk.harga_realisasi_bulat as nilai
//        from  " . sfConfig::get('app_default_edelivery') . ".detail_komponen dk
//        join " . sfConfig::get('app_default_eproject') . ".detail_kegiatan dkeg
//        on dkeg.id = dk.detail_kegiatan_id
//        join " . sfConfig::get('app_default_eproject') . ".kegiatan keg
//        on keg.id = dkeg.kegiatan_id
//        join " . sfConfig::get('app_default_eproject') . ".skpd skpd
//        on skpd.id = keg.skpd_id
//        where dk.harga_realisasi > 0 and dk.detail_swakelola_id is null
//        )
//        UNION all
//        (
//        select skpd.kode as skpd_kode, skpd.nama as skpd_nama, keg.kode as keg_kode, keg.nama as keg_nama, dkeg.kode_detail_kegiatan, dkeg.kode_komponen, dkeg.nama as komponen, ds.volume_total as volume, ds.nilai
//        from " . sfConfig::get('app_default_edelivery') . ".detail_swakelola ds
//        join " . sfConfig::get('app_default_eproject') . ".detail_kegiatan dkeg
//        on dkeg.id = ds.detail_kegiatan_id
//        join " . sfConfig::get('app_default_eproject') . ".kegiatan keg
//        on keg.id = dkeg.kegiatan_id
//        join " . sfConfig::get('app_default_eproject') . ".skpd skpd
//        on skpd.id = keg.skpd_id
//        )) tb1
//        on tb1.kode_detail_kegiatan = rd.unit_id||'.'||rd.kegiatan_code||'.'||rd.detail_no
//        where     
//                tb1.skpd_kode <> '9999'  and tb1.kode_detail_kegiatan = '$unit_id'||'.'||'$kode_kegiatan'||'.'||'$detail_no'
//        group by tb1.skpd_kode, tb1.skpd_nama, tb1.keg_kode, tb1.keg_nama, tb1.kode_komponen, rd.nilai_anggaran, rd.komponen_harga,rd.rekening_code, rd.volume, rd.kegiatan_code, rd.komponen_name,rd.satuan,rd.detail_no
//        order by tb1.skpd_kode, tb1.skpd_nama, tb1.keg_kode, tb1.keg_nama, tb1.kode_komponen";
        //print_r($query2);//exit;
        //kasus khusu dikec sawahan 271016


        $kode_detail_kegiatan = "'$unit_id.$kode_kegiatan.$detail_no'";

        if (in_array($unit_id . '.' . $kode_kegiatan . '.' . $detail_no, array(''))) {
            return 0;
        }
        //******query swakelola
        $query = "select tb.skpd_kode, tb.skpd_nama, tb.keg_kode, tb.keg_nama, tb.kode_detail_kegiatan, tb.kode_komponen, tb.komponen, (sum(tb.volume)) as volume
        from (
        select skpd.kode as skpd_kode, skpd.nama as skpd_nama, keg.kode as keg_kode, keg.nama as keg_nama, dkeg.kode_detail_kegiatan, dkeg.kode_komponen, dkeg.nama as komponen, ds.volume_total as volume, ds.nilai
        from edelivery.detail_swakelola ds
        right join eproject.detail_kegiatan dkeg on dkeg.id = ds.detail_kegiatan_id
        left join eproject.detail_pekerjaan dp on dp.detail_kegiatan_id = dkeg.id
        left join eproject.pekerjaan p on p.id = dp.pekerjaan_id
        join eproject.kegiatan keg on keg.id = dkeg.kegiatan_id
        right join eproject.skpd skpd on skpd.id = keg.skpd_id
        join edelivery.paket_honor ph on ph.pekerjaan_id = p.id and ph.id = ds.paket_honor_id
        where dp.alokasi > 0 and dkeg.kode_detail_kegiatan in ('$unit_id.$kode_kegiatan.$detail_no') and p.metode in (6,7,10,51,52,53) 
             and (ds.is_jkn_jan is false or dkeg.is_iuran_jkn is true)
        )tb
        group by tb.skpd_kode, tb.skpd_nama, tb.keg_kode, tb.keg_nama, tb.kode_komponen, tb.kode_detail_kegiatan, tb.komponen";

        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query);
        $t = $stmt->executeQuery();
        
        while ($t->next()) {
            $jumlahRows = $t->getRow();
            if ($t->getString('volume') > 0) {
                $tot1 = $tot1 + $t->getString('volume');
            }
        }

        //******query non swakelola
        $query2 = "
        select tb0.skpd_kode, tb0.skpd_nama, tb0.keg_kode, tb0.keg_nama, tb0.kode_komponen, tb0.kode_detail_kegiatan, tb0.komponen, sum(tb0.volume) as volume_realisasi, sum(tb0.nilai) as nilai_realisasi
        from ((
            select skpd.kode as skpd_kode, skpd.nama as skpd_nama, keg.kode as keg_kode, keg.nama as keg_nama, dkeg.kode_detail_kegiatan, dkeg.kode_komponen,
            dkeg.nama as komponen, dk.volume_kontrak as volume, (dk.harga_realisasi_bulat + dk.lk + dk.biaya_lain) as nilai
            from  edelivery.detail_komponen dk
            join edelivery.kontraks k 
            on k.id = dk.kontrak_id
            right join eproject.detail_kegiatan dkeg
            on dkeg.id = dk.detail_kegiatan_id
            join  eproject.kegiatan keg
            on keg.id = dkeg.kegiatan_id
            right join  eproject.skpd skpd
            on skpd.id = keg.skpd_id
            where dk.harga_realisasi > 0 and k.status <> 110 and k.id not in (
                select k2.id
                from edelivery.kontraks k2
                join edelivery.pemutusan_kontrak pk on k2.id = pk.kontrak_id
                where k2.metode != 6
            )
            and dk.detail_swakelola_id is null and dkeg.kode_detail_kegiatan='$unit_id.$kode_kegiatan.$detail_no'
        )
        union all
        (
            select s2.kode as skpd_kode, s2.nama as skpd_nama, keg2.kode as keg_kode, keg2.nama as keg_nama, dk2.kode_detail_kegiatan, dk2.kode_komponen,
            dk2.nama as komponen, coalesce(dkmc2.volume_kontrak, dkom2.volume_kontrak) as volume, 
            coalesce(dkmc2.harga_realisasi_bulat, dkom2.harga_realisasi_bulat + dkom2.biaya_lain) as nilai
            from edelivery.kontraks k3 
            join edelivery.essact_sync es on k3.pekerjaan_id = es.kode_paket
            join edelivery.essact_komponen ek on es.id = ek.essact_sync_id
            join edelivery.detail_komponen dkom2 on dkom2.id = ek.detail_komponen_id
            join eproject.detail_pekerjaan dp2 on dp2.pekerjaan_id = k3.pekerjaan_id and dp2.detail_kegiatan_id = dkom2.detail_kegiatan_id
            join eproject.detail_kegiatan dk2 on dk2.id = dkom2.detail_kegiatan_id
            join eproject.pekerjaan p2 on p2.id = k3.pekerjaan_id
            join eproject.kegiatan keg2 on keg2.id = p2.kegiatan_id
            join eproject.skpd s2 on s2.id = keg2.skpd_id
            left join edelivery.det_kom_mc dkmc2 on dkmc2.id = dkom2.id
            where (k3.status = 110 or k3.id in (
                select k2.id
                from edelivery.kontraks k2
                join edelivery.pemutusan_kontrak pk on k2.id = pk.kontrak_id
                where k2.metode != 6
            )) and k3.metode != 6 and
            es.nilai_belanja > 0 and dk2.kode_detail_kegiatan = '$unit_id.$kode_kegiatan.$detail_no'
        )
        UNION all
        (
        select skpd.kode as skpd_kode, skpd.nama as skpd_nama, keg.kode as keg_kode, keg.nama as keg_nama, dkeg.kode_detail_kegiatan, dkeg.kode_komponen,dkeg.nama as komponen,
        case when p.metode in (6,7,10,51,52,53) then 0 else ds.volume_total end as volume,
        ds.nilai--, p.id, p.metode
        from edelivery.detail_swakelola ds
        right join eproject.detail_kegiatan dkeg
        on dkeg.id = ds.detail_kegiatan_id
        join eproject.kegiatan keg
        on keg.id = dkeg.kegiatan_id
        right join eproject.skpd skpd
        on skpd.id = keg.skpd_id
        right join edelivery.paket_honor ph
        on ph.id = ds.paket_honor_id
        right join eproject.pekerjaan p
        on p.id = ph.pekerjaan_id
        where p.metode != 11 and dkeg.kode_detail_kegiatan='$unit_id.$kode_kegiatan.$detail_no'
        )
        UNION all
        (
        select skpd.kode as skpd_kode, skpd.nama as skpd_nama, keg.kode as keg_kode, keg.nama as keg_nama, 
        dk.kode_detail_kegiatan , dk.kode_komponen , dk.nama as komponen ,
        coalesce (0) as volume, 
        case when eo.ppn = 10 then round(eod.nominal * 1.1) 
        else eod.nominal end as nilai 
        from edelivery.epls e
        join edelivery.epl_oe eo on eo.epl_id = e.id 
        join edelivery.epl_oe_detail eod  on eo.epl_id = eod.epl_id 
        join eproject.detail_kegiatan dk on dk.id = eod.detail_kegiatan_id 
        join eproject.detail_pekerjaan dp on dp.detail_kegiatan_id = dk.id and dp.pekerjaan_id = e.pekerjaan_id 
        join eproject.pekerjaan p on p.id = dp.pekerjaan_id
        join eproject.kegiatan keg on keg.id = p.kegiatan_id
        join eproject.skpd skpd on skpd.id = keg.skpd_id
        where e.status = 1 and dk.kode_detail_kegiatan = '$unit_id.$kode_kegiatan.$detail_no' and dp.alokasi > 0
        )
        ) tb0
        group by tb0.skpd_kode, tb0.skpd_nama, tb0.keg_kode, tb0.keg_nama, tb0.kode_komponen, tb0.kode_detail_kegiatan, tb0.komponen";
        $con = Propel::getConnection();
        $stmt1 = $con->prepareStatement($query2);
        $s = $stmt1->executeQuery();
        while ($s->next()) {
            $jumlahRows = $s->getRow();
            if ($s->getString('volume_realisasi') > 0) {
                $tot2 = $tot2 + $s->getString('volume_realisasi');
            }
        }

        //*****query sts
        $query3 = "
        select dkeg.kode_detail_kegiatan, dkeg.nama, sum(dks.nilai) as nilai_sts, sum(dks.volume) as volume
                    from edelivery.sts sts
                    join edelivery.detail_sts ds on ds.sts_id = sts.id
                    join edelivery.detail_komponen_sts dks on dks.sts_id = ds.sts_id and dks.transaksi_id = ds.transaksi_id
                    join edelivery.detail_komponen dk on dk.id = dks.detail_komponen_id
                    join eproject.detail_kegiatan dkeg on dkeg.id = dk.detail_kegiatan_id
                    where sts.status = 100 and sts.jenis = 1 
                    and dkeg.kode_detail_kegiatan = '$unit_id.$kode_kegiatan.$detail_no'
                    group by dkeg.kode_detail_kegiatan, dkeg.nama";

        $con = Propel::getConnection();
        $stmt2 = $con->prepareStatement($query3);
        $v = $stmt2->executeQuery();
        while ($v->next()) {
            $jumlahRows = $v->getRow();
            if ($v->getString('volume') > 0) {
                $tot3 = $tot3 + $v->getString('volume');
            }
        }

        $query4 = "select dk.kode_detail_kegiatan, coalesce(sum(lkk.jumlah),0) as nilai_lk, coalesce(sum(lkk.volume_lk),0) as volume_lk
                    from edelivery.kontrak_mc_addendum as kma
                    join edelivery.lebih_kurang_komponen lkk on lkk.kontrak_mc_addendum_id = kma.id
                    join eproject.detail_kegiatan dk on dk.id = lkk.detail_kegiatan_id
                    where dk.kode_detail_kegiatan = '$unit_id.$kode_kegiatan.$detail_no' and kma.status = 100
                    group by dk.kode_detail_kegiatan";

        $stmt3 = $con->prepareStatement($query4);
        $rs2 = $stmt3->executeQuery();
        while ($rs2->next()) {
            $jumlahRows = $rs2->getRow();
            $tot4 = $tot4 + $rs2->getString('volume_lk');
        }

       
        $totVolumeRealisasi = $tot1 + $tot2 - $tot3 + $tot4;
        // $totNilaiKontrak=500;
        return $totVolumeRealisasi;
    }

    public function getCekNilaiKontrakDelivery() {
        $totNilaiKontrak = 0;
        $jumlahRows = 0;
        $unit_id = $this->getUnitId();
        $kegiatan = $this->getKegiatanCode();
        $no = $this->getDetailNo();
        $sub = $this->getSub();
        $query2 = "select rd.unit_id,rd.kegiatan_code,rd.detail_no,rd.subtitle,rd.komponen_name,rd.kode_sub,
                 de.kode_detail_kegiatan,dk.detail_swakelola_id,dk.kontrak_id,
                 sum(rd.volume * rd.komponen_harga_awal * (100 + rd.pajak) / 100) as nilai_budgeting,
                 sum(dk.harga_realisasi*dk.volume_realisasi) as nilai_realisasi,
                 sum(dk.harga_kontrak*dk.volume_kontrak) as nilai_kontrak

                from " . sfConfig::get('app_default_edelivery') . ".detail_komponen dk, " . sfConfig::get('app_default_eproject') . ".detail_kegiatan de, " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail rd

                where rd.unit_id='$unit_id' and rd.kegiatan_code='$kegiatan' and rd.detail_no='$no'
                      and rd.subtitle ilike '$sub' and rd.volume>0 and rd.status_hapus=false
                      and de.id=dk.detail_kegiatan_id and substring(de.kode_detail_kegiatan,1,4)=rd.unit_id
                      and substring(de.kode_detail_kegiatan,6,4)=rd.kegiatan_code
                      and (substring(de.kode_detail_kegiatan,11))::integer=rd.detail_no

                group by rd.unit_id,rd.kegiatan_code,rd.detail_no,rd.subtitle,rd.komponen_name,rd.kode_sub,
                      de.kode_detail_kegiatan,dk.detail_swakelola_id,dk.kontrak_id
                      having sum(dk.harga_realisasi*dk.volume_realisasi)>0 or sum(dk.harga_kontrak*dk.volume_kontrak)>0
                      order by rd.unit_id,rd.kegiatan_code,rd.detail_no";
        //print_r($query2);//exit;

        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query2);
        $t = $stmt->executeQuery();
        //print_r($t);

        while ($t->next()) {
            $jumlahRows = $t->getRow();
            if ($t->getString('nilai_kontrak') > 0) {
                $totNilaiKontrak = $totNilaiKontrak + $t->getString('nilai_kontrak');
            }
        }
        // $totNilaiKontrak=500;
        return $totNilaiKontrak;
    }

    public function getCekNilaiDeliveryBelumAdaAturanPembayaran($unit_id, $kegiatan_code, $detail_no) {
        $kode_detail_kegiatan = $unit_id . '.' . $kegiatan_code . '.' . $detail_no;
        $arr_pekerjaan_id = array();

        //pekerjaan id komponen tersebut
        $query = "select (pekerjaan_id) as id from " . sfConfig::get('app_default_eproject') . ".detail_pekerjaan
                where detail_kegiatan_id in (select id from " . sfConfig::get('app_default_eproject') . ".detail_kegiatan 
                        where kode_detail_kegiatan='$kode_detail_kegiatan')";
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query);
        $rs_pekerjaan = $stmt->executeQuery();
        while ($rs_pekerjaan->next()) {
            $arr_pekerjaan_id[] = $rs_pekerjaan->getString('id');
        }
        $pekerjaan_id = implode(',', $arr_pekerjaan_id);

        //ambil nilai kontrak
        $query = "select sum(nilai) from " . sfConfig::get('app_default_edelivery') . ".kontraks where pekerjaan_id in ($pekerjaan_id)";
        $stmt = $con->prepareStatement($query);
        $rs_kontrak = $stmt->executeQuery();
        while ($rs_kontrak->next()) {
            $nilai_kontrak += $rs_kontrak->getString('nilai');
        }

        //ambil nilai budgeting selain yang divalidasi (kemudian ditambahkan nilai barunya)
        $query = "select sum(nilai_anggaran) as nilai from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail
                where unit_id||'.'||kegiatan_code||'.'||detail_no in
                (select kode_detail_kegiatan from " . sfConfig::get('app_default_eproject') . ".detail_kegiatan
                        where id in (select detail_kegiatan_id from " . sfConfig::get('app_default_eproject') . ".detail_pekerjaan
                                where pekerjaan_id in ($pekerjaan_id))) and unit_id||'.'||kegiatan_code||'.'||detail_no<>'$kode_detail_kegiatan'";
        $stmt = $con->prepareStatement($query);
        $rs = $stmt->executeQuery();
        while ($rs->next()) {
            $nilai_budgeting += $rs->getString('nilai');
        }

        return $nilai_kontrak - $nilai_budgeting;
    }

    public function getCekNilaiDeliveryBelumAdaAturanPembayaran2($unit_id, $kegiatan_code, $detail_no) {
        $kode_detail_kegiatan = $unit_id . '.' . $kegiatan_code . '.' . $detail_no;

        $query = "select count(*) as jml from " . sfConfig::get('app_default_edelivery') . ".kontraks
                where pekerjaan_id in (select (pekerjaan_id) as id from " . sfConfig::get('app_default_eproject') . ".detail_pekerjaan
                    where detail_kegiatan_id in (select id from " . sfConfig::get('app_default_eproject') . ".detail_kegiatan 
                        where kode_detail_kegiatan='$kode_detail_kegiatan') and alokasi>0) and metode <> 6 and bukti_perjanjian != 3";
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query);
        $rs = $stmt->executeQuery();
        while ($rs->next()) {
            $kontrak = $rs->getInt('jml');
        }
        if ($kontrak <= 0) {
            return 0;
        }
        $query = "select count(*) as jml from " . sfConfig::get('app_default_edelivery') . ".kontraks k, " . sfConfig::get('app_default_edelivery') . ".detail_komponen dk
                where k.id=dk.kontrak_id and k.pekerjaan_id in (select (pekerjaan_id) as id from " . sfConfig::get('app_default_eproject') . ".detail_pekerjaan
                    where detail_kegiatan_id in (select id from " . sfConfig::get('app_default_eproject') . ".detail_kegiatan 
                        where kode_detail_kegiatan='$kode_detail_kegiatan'))and k.metode <> 6";
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query);
        $rs = $stmt->executeQuery();
        while ($rs->next()) {
            $jml = $rs->getInt('jml');
        }
        if ($jml == 0)
            return 1;
        return 0;
    }

    //irul 14 feb 2014 - cek nilai alokasi
    public function getCekNilaiAlokasiProject($unit_id, $kegiatan_code, $detail_no) {
        $totNilaiAlokasi = 0;

        $query_eproject = "SELECT 
                            s.kode, k.kode, dk.kode_detail_kegiatan, dk.id_budgeting, sum(dp.ALOKASI) as jml
                            from
                            " . sfConfig::get('app_default_eproject') . ".detail_pekerjaan dp,
                            " . sfConfig::get('app_default_eproject') . ".pekerjaan p,
                            " . sfConfig::get('app_default_eproject') . ".detail_kegiatan dk,
                            " . sfConfig::get('app_default_eproject') . ".kegiatan k,
                            " . sfConfig::get('app_default_eproject') . ".skpd s
                            where
                            dp.pekerjaan_id = p.id and p.kegiatan_id = k.id and s.kode = '$unit_id' and k.kode = '$kegiatan_code' and dk.id_budgeting = '$detail_no'
                            and k.skpd_id = s.id and dp.detail_kegiatan_id = dk.id
                            group by s.kode, k.kode, dk.kode_detail_kegiatan, dk.id_budgeting";

        $con = Propel::getConnection();
        $statement = $con->prepareStatement($query_eproject);
        $rs_eprojects = $statement->executeQuery();

        while ($rs_eprojects->next()) {
            $totNilaiAlokasi = $totNilaiAlokasi + $rs_eprojects->getString('jml');
        }
        return $totNilaiAlokasi;
    }

    //irul 14 feb 2014 - cek nilai alokasi
    //irul 2 feb 2015 - cek lelang Eproject
    public function getCekLelangEproject($unit_id, $kegiatan_code, $detail_no) {
        $kodedetailkegiatan = $unit_id . '.' . $kegiatan_code . '.' . $detail_no;

        $query_eproject = "select count(*) as total 
            from " . sfConfig::get('app_default_eproject') . ".pekerjaan a, " . sfConfig::get('app_default_eproject') . ".detail_pekerjaan b, " . sfConfig::get('app_default_eproject') . ".detail_kegiatan c 
            where a.metode = 3 and a.pernah_realisasi = 1 
            and a.id = b.pekerjaan_id 
            and b.detail_kegiatan_id = c.id 
            and c.kode_detail_kegiatan = '$kodedetailkegiatan'";

        $con = Propel::getConnection();
        $statement = $con->prepareStatement($query_eproject);
        $rs_eprojects = $statement->executeQuery();

        while ($rs_eprojects->next()) {
            $jumlah_lelang = $rs_eprojects->getString('total');
        }
        return $jumlah_lelang;
    }

    //irul 2 feb 2015 - cek lelang Eproject

    public function getTotalKodeSub($kode_sub) {
        $unit_id = $this->getUnitId();
        $kegiatan_code = $this->getKegiatanCode();
        $kodeSub = $kode_sub;
        $query2 = "select sum(nilai_anggaran) as hasil_kali
                from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail
                where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and kode_sub='$kodeSub' and status_hapus=false";
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query2);
        $t = $stmt->executeQuery();
        while ($t->next()) {
            $hasilKali = $t->getString('hasil_kali');
        }
        return $hasilKali;
    }

    public function getTotalSub($sub) {
        $unit_id = $this->getUnitId();
        $kegiatan_code = $this->getKegiatanCode();
        $subKeg = $sub;
        $query2 = $query2 = "select sum(nilai_anggaran) as hasil_kali
                        from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail
                        where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and sub ilike '%$subKeg%' and status_hapus=false";
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query2);
        $t = $stmt->executeQuery();
        while ($t->next()) {
            $hasilKali = $t->getString('hasil_kali');
        }
        return $hasilKali;
    }

///sepertinya tidak dipakai
    public function getCekVolume4cekPaguRincian($unit_id, $kegiatan_code, $detail_no, $volume) {
        $queryPagu = "select sum(alokasi_dana) as nilai from " . sfConfig::get('app_default_schema') . ".dinas_master_kegiatan where unit_id='$unit_id'";
        $queryRd = "select sum(volume * komponen_harga_awal * (100 + pajak) / 100) as nilai from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail where unit_id='$unit_id' and status_hapus=FALSE";
        $con = Propel::getConnection();
        $statement0 = $con->prepareStatement($queryPagu);
        $rs_nilai = $statement0->executeQuery();
        while ($rs_nilai->next()) {
            $nilai_awal = $rs_nilai->getString('nilai');
        }

        $statement2 = $con->prepareStatement($queryRd);
        $rs_nilai2 = $statement2->executeQuery();
        while ($rs_nilai2->next()) {
            $nilai_rincian = $rs_nilai2->getString('nilai');
        }

        $querycekVolume = "select volume from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and detail_no='$detail_no' and status_hapus=FALSE";
        $statement = $con->prepareStatement($querycekVolume);
        $rs_volume = $statement->executeQuery();
        while ($rs_volume->next()) {
            $volume_rd = $rs_volume->getString('volume');
        }
        //tolak=1, lolos=0;

        if (($nilai_awal < $nilai_rincian) && ($volume > $volume_rd)) {
            $backValue = '1';
        } else {
            $backValue = '0';
        }
        //$tampung = $nilai_awal.' '.$nilai_rincian.' '.$volume.' '.$volume_rd.' '.$backValue;

        return $backValue;
    }

    //irul 3april 2014 
    public function getCekLelang2($unit_id, $kegiatan_code, $detail_no, $nilai_anggaran) {
        $i = 0;
        $pekerjaan = $unit_id . '.' . $kegiatan_code . '.' . $detail_no;
//        $query = "select count(vpd.status_lelang) as lelang "
//                . "from v_pekerjaan_dilelang vpd, " . sfConfig::get('app_default_eproject') . ".detail_pekerjaan dp, " . sfConfig::get('app_default_eproject') . ".detail_kegiatan dk "
//                . "where vpd.id = dp.pekerjaan_id and dp.detail_kegiatan_id = dk.id and dk.kode_detail_kegiatan = '$pekerjaan' "
//                . "and (vpd.status_lelang ilike '%Berjalan%' or vpd.status_lelang ilike '%Selesai%') "
//                . "and dp.alokasi > 0";

        $query = "select count(vpd.status_lelang) as lelang "
                . "from v_pekerjaan_dilelang vpd, " . sfConfig::get('app_default_eproject') . ".detail_pekerjaan dp, " . sfConfig::get('app_default_eproject') . ".detail_kegiatan dk "
                . "where vpd.id = dp.pekerjaan_id and dp.detail_kegiatan_id = dk.id and dk.kode_detail_kegiatan = '$pekerjaan' "
                . "and vpd.status_lelang in ('Lelang Sedang Berjalan') "
                . "and dp.alokasi > $nilai_anggaran "
                . "limit 1";

        $con = Propel::getConnection();
        $statement = $con->prepareStatement($query);
        $rs = $statement->executeQuery();
        while ($rs->next()) {
            $lelang = $rs->getString('lelang');
        }
        if ($lelang == 0) {
            return 0;
        } else {
            return 1;
        }
    }

    //irul 3april 2014 
    public function getCekLelang($unit_id, $kegiatan_code, $detail_no, $nilai_anggaran) {
        $pekerjaan = $unit_id . '.' . $kegiatan_code . '.' . $detail_no;

        $query = "select dk.kode_detail_kegiatan, dp.pekerjaan_id as id
                from " . sfConfig::get('app_default_eproject') . ".pekerjaan p, 
                     " . sfConfig::get('app_default_eproject') . ".detail_pekerjaan dp, 
                     " . sfConfig::get('app_default_eproject') . ".detail_kegiatan dk                    
                where dp.detail_kegiatan_id = dk.id and p.id=dp.pekerjaan_id and dp.alokasi > 0              
                and p.metode in (3,12,13,14,15,16) and dk.kode_detail_kegiatan='$pekerjaan'";

        $con = Propel::getConnection();
        $statement = $con->prepareStatement($query);
        $rs = $statement->executeQuery();
        $arr_id = array();      
        while ($rs->next()) {
            $arr_id[] = $rs->getString('id');
        }
        if (!$arr_id) {
            return 0;
        } else {
            $id = implode(',', $arr_id);           
        }
        
        $query2 = "select count(*) as lelang , sum(hps) as nilai_hps from v_paket_eproject_lelang_sby
                 where status_lelang in ('Lelang Sedang Berjalan') and id in ($id)";
        $con2 = Propel::getConnection('lpse');
        $statement = $con2->prepareStatement($query2);
        $rs = $statement->executeQuery();
        while ($rs->next()) {
            $lelang = $rs->getString('lelang');
            $nilai_hps = $rs->getString('nilai_hps');         
        }
        if ($lelang == 0) {
            return 0;
        } else {
            if($nilai_hps > $nilai_anggaran)
            return 1;
            else
            return 0;
        }
    }

    //irul 3april 2014 
    //irul 20april 2014 
    public function getCekNilaiDetKomMCDelivery($unit_id, $kode_kegiatan, $detail_no) {
        $totNilaiKontrak = 0;
        $query2 = "select rd.unit_id,rd.kegiatan_code,rd.detail_no,rd.subtitle,rd.komponen_name,rd.kode_sub,
                 de.kode_detail_kegiatan,dk.detail_swakelola_id,dk.kontrak_id,
                 sum(rd.nilai_anggaran) as nilai_budgeting,
                 sum(dk.harga_realisasi_bulat) as nilai_kontrak

                from " . sfConfig::get('app_default_edelivery') . ".detail_komponen dk,
                                     " . sfConfig::get('app_default_eproject') . ".detail_kegiatan de,
                                     " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail rd

                where rd.unit_id='$unit_id' and rd.kegiatan_code='$kode_kegiatan' and rd.detail_no='$detail_no'
                      and rd.volume>0 and rd.status_hapus=FALSE
                      and de.id=dk.detail_kegiatan_id 
                                      and de.kode_detail_kegiatan=rd.unit_id||'.'||rd.kegiatan_code||'.'||rd.detail_no

                group by rd.unit_id,rd.kegiatan_code,rd.detail_no,rd.subtitle,rd.komponen_name,rd.kode_sub,
                      de.kode_detail_kegiatan,dk.detail_swakelola_id,dk.kontrak_id
                      having  sum(dk.harga_realisasi_bulat)>0
                      order by rd.unit_id,rd.kegiatan_code,rd.detail_no";
//print_r($query2);exit;

        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query2);
        $t = $stmt->executeQuery();
        while ($t->next()) {
            $jumlahRows = $t->getRow();
            if ($t->getString('nilai_kontrak') > 0) {
                $totNilaiKontrak = $totNilaiKontrak + $t->getString('nilai_kontrak');
                //print_r($totNilaiKontrak);exit;
                //$warning=$totNilaiKontrak;
            }
        }
        return $totNilaiKontrak;
    }

    //irul 20april 2014 
    //ticket #28 ambil nilai HPS Per Komponen
    //created 19Juni2015
    public function getCekNilaiHPSKomponen($unit_id, $kegiatan_code, $detail_no) {
        return 0;

//        $i = 0;
//        $total_hps = 0;
//        $pekerjaan = $unit_id . '.' . $kegiatan_code . '.' . $detail_no;
//        $query = "select vhps.* 
//            from v_nilai_hps_komponen vhps, " . sfConfig::get('app_default_eproject') . ".detail_pekerjaan dp, " . sfConfig::get('app_default_eproject') . ".detail_kegiatan dk 
//            where vhps.paket_id = dp.pekerjaan_id and vhps.kode_subtitle = dk.kode_subtitle and vhps.nama = dk.nama and vhps.kode_sub2title = dk.kode_sub2title 
//            and dp.detail_kegiatan_id = dk.id 
//            and dk.kode_detail_kegiatan = '$pekerjaan' 
//            and dp.alokasi > 0  ";
//        $con = Propel::getConnection();
//        $statement = $con->prepareStatement($query);
//        $rs = $statement->executeQuery();
//        while ($rs->next()) {
//            $i++;
//            $total_hps = $total_hps + $rs->getFloat('harga_ppk');
//        }
//        if ($i == 0) {
//            return 0;
//        } else {
//            return 0;
//        }
    }

    //ticket #28 ambil nilai HPS Per Komponen
//ticket #50 - Validasi Status Lelang 'Selesai' & Aturan Pembayaran tidak terisi 

    public function getCekLelangTidakAdaAturanPembayaran1($unit_id, $kegiatan_code, $detail_no) {
        $i = 0;
        $pekerjaan = $unit_id . '.' . $kegiatan_code . '.' . $detail_no;
//        if($pekerjaan == '2600.1.03.28.0005.739')
//            return 0;
        $query = "select count(vpd.status_lelang) as lelang "
                . "from v_pekerjaan_dilelang vpd, " . sfConfig::get('app_default_eproject') . ".detail_pekerjaan dp, " . sfConfig::get('app_default_eproject') . ".detail_kegiatan dk "
                . "where vpd.id = dp.pekerjaan_id and dp.detail_kegiatan_id = dk.id and dk.kode_detail_kegiatan = '$pekerjaan' "
                . "and vpd.status_lelang in ('Lelang Sudah Selesai') "
                . "and dp.alokasi > 0 "
                . "limit 1";
        $con = Propel::getConnection();
        $statement = $con->prepareStatement($query);
        $rs = $statement->executeQuery();
        while ($rs->next()) {
            $lelang = $rs->getString('lelang');
        }

        $nilai_kontrak = $this->getCekNilaiKontrakDelivery2($unit_id, $kegiatan_code, $detail_no);

        if ($lelang > 0 && $nilai_kontrak == 0) {
            return 1;
        } else {
            return 0;
        }
    }

    public function getCekLelangTidakAdaAturanPembayaran($unit_id, $kegiatan_code, $detail_no) {
        $detail_kegiatan= $unit_id.'.'.$kegiatan_code.'.'.$detail_no;
        
        $query = "select dk.kode_detail_kegiatan, k.id, count(dkom.id) as aturan
                from " . sfConfig::get('app_default_eproject') . ".detail_kegiatan dk
                join " . sfConfig::get('app_default_eproject') . ".detail_pekerjaan dp on dp.detail_kegiatan_id = dk.id
                join " . sfConfig::get('app_default_eproject') . ".pekerjaan p on p.id = dp.pekerjaan_id
                left join " . sfConfig::get('app_default_edelivery') . ".detail_komponen dkom on dkom.detail_kegiatan_id = dk.id
                left join " . sfConfig::get('app_default_edelivery') . ".kontraks k on k.pekerjaan_id = p.id
                join " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail rd on rd.detail_kegiatan = dk.kode_detail_kegiatan
                where (p.metode in (2,8,14,15,16,9,11,17) or (p.metode = 10 and p.alokasi >= 50000000))
                and rd.volume>0 
                and rd.status_hapus=FALSE 
                and k.status=0
                and dp.alokasi > 0 and dk.kode_detail_kegiatan = '$detail_kegiatan'
                group by 1,2
                having count(dkom.id)=0 
                order by dk.kode_detail_kegiatan";

        $con = Propel::getConnection();
        $statement = $con->prepareStatement($query);
        $rs = $statement->executeQuery();
        while ($rs->next()) {    
                if($rs->getString('aturan')== 0)      
                {
                    return 1;
                } 
                                
        } 
        
        //jika tak ada hasil query
        return 0;       
    }

//ticket #50 - Validasi Status Lelang 'Selesai' & Aturan Pembayaran tidak terisi
    //VALIDASI BARU UNTUK APPROVE REVISI
    //validasi cek nilai alokasi dengan banyak komponen (array detail_no)
    public function getCekNilaiAlokasiProjectArray($unit_id, $kode_kegiatan, $detail_no) {
        $con = Propel::getConnection();

        //kumpulan kode detail kegiatan yang divalidasi
        $kumpulan_kode_detail_kegiatan = array();
        foreach ($detail_no as $no) {
            $kumpulan_kode_detail_kegiatan[] = "'$unit_id.$kode_kegiatan.$no'";
        }
        $kode = $kumpulan_kode_detail_kegiatan[0];
        $kode_detail_kegiatan = implode(',', $kumpulan_kode_detail_kegiatan);

        //cek alokasi
        $arr_totNilaiAlokasi = array();
        $query_eproject = "SELECT dk.kode_detail_kegiatan, sum(dp.ALOKASI) as jml
                from " . sfConfig::get('app_default_eproject') . ".detail_pekerjaan dp,
                     " . sfConfig::get('app_default_eproject') . ".detail_kegiatan dk
                where dk.kode_detail_kegiatan in ($kode_detail_kegiatan)
                and dp.detail_kegiatan_id = dk.id
                group by dk.kode_detail_kegiatan";
        $statement = $con->prepareStatement($query_eproject);
        $rs_eprojects = $statement->executeQuery();
        while ($rs_eprojects->next()) {
            $kode = $rs_eprojects->getString('kode_detail_kegiatan');
            $arr_totNilaiAlokasi[$kode] = $rs_eprojects->getString('jml');
        }
        return $arr_totNilaiAlokasi;
    }

    //validasi cek lelang berjalan dengan banyak komponen (array detail_no)
    public function getCekLelangArray($unit_id, $kode_kegiatan, $detail_no) {
        $con = Propel::getConnection();

        //kumpulan kode detail kegiatan yang divalidasi
        $kumpulan_kode_detail_kegiatan = array();
        foreach ($detail_no as $no) {

            $kumpulan_kode_detail_kegiatan[] = "'$unit_id.$kode_kegiatan.$no'";
        }
        $kode_detail_kegiatan = implode(',', $kumpulan_kode_detail_kegiatan);

        if($kode_detail_kegiatan == '2300.1.1.1.02.10.0001.10796') {
            return 0;
        }

        $query = "select dk.kode_detail_kegiatan, dp.pekerjaan_id as id, rd.nilai_anggaran as nilai_anggaran
                from " . sfConfig::get('app_default_eproject') . ".pekerjaan p, 
                     " . sfConfig::get('app_default_eproject') . ".detail_pekerjaan dp, 
                     " . sfConfig::get('app_default_eproject') . ".detail_kegiatan dk, 
                     " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail rd
                where dp.detail_kegiatan_id = dk.id and p.id=dp.pekerjaan_id
                and dk.kode_detail_kegiatan=rd.unit_id||'.'||rd.kegiatan_code||'.'||rd.detail_no and dp.alokasi > rd.nilai_anggaran
                and p.metode in (3,12,13,14,15,16) and dk.kode_detail_kegiatan in ($kode_detail_kegiatan)
                order by kode_detail_kegiatan";
        $statement = $con->prepareStatement($query);
        $rs = $statement->executeQuery();
        //$arr_id = array();
        // while ($rs->next()) {           
        //     if ($kode == $rs->getString('kode_detail_kegiatan')) {
        //         $arr_id[$kode] .= ',' . $rs->getString('id');
        //     } else {
        //         $kode = $rs->getString('kode_detail_kegiatan');
        //         $arr_id[$kode] = $rs->getString('id');
        //     }
        //     // $kode = $rs->getString('kode_detail_kegiatan');
        //     // $arr_id[$kode] = [ $rs->getString('id') , rs->getFloat('nilai_anggaran')];

        // }
        $data = array();
        while ($rs->next()) {
            if ($rs->getString('kode_detail_kegiatan')) {
                $data[$rs->getString('kode_detail_kegiatan')] = array (
                    $rs->getString('id')=>$rs->getString('nilai_anggaran')
                );
            } 

        }    
          
        $arr_error = array();
        foreach ($data as $kode_detail_kegiatan => $array_pekerjaan) {
            foreach ($array_pekerjaan as $id => $nilai_anggaran) {
                $query2 = "select count(*) as lelang from v_paket_eproject_lelang_sby
                 where status_lelang in ('Lelang Sedang Berjalan') and id=$id and hps > $nilai_anggaran";
                $con2 = Propel::getConnection('lpse');
                $statement = $con2->prepareStatement($query2);
                $rs = $statement->executeQuery();
                while ($rs->next()) {
                    $lelang = $rs->getString('lelang');
                }
                if ($lelang != 0) {
                    $arr_error[$kode_detail_kegiatan] = 1;
                }
            }
            
        }      
        return $arr_error;
    }

    //Validasi nilai swakelola dengan banyak komponen (array detail_no)
    public function getCekNilaiSwakelolaDeliveryArray($unit_id, $kode_kegiatan, $detail_no) {
        $con = Propel::getConnection();

        //kumpulan kode detail kegiatan yang divalidasi
        $kumpulan_kode_detail_kegiatan = array();
        foreach ($detail_no as $no) {
            $kumpulan_kode_detail_kegiatan[] = "'$unit_id.$kode_kegiatan.$no'";
        }
        $kode = $kumpulan_kode_detail_kegiatan[0];
        $kode_detail_kegiatan = implode(',', $kumpulan_kode_detail_kegiatan);

        //cek nilai swakelola
        $arr_totNilaiSwakelola = array();
        $query2 = "select de.kode_detail_kegiatan,dk.id,dk.nilai as nilai_realisasi
                from " . sfConfig::get('app_default_edelivery') . ".detail_swakelola dk,
                     " . sfConfig::get('app_default_eproject') . ".detail_kegiatan de,
                     " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail rd
                where unit_id||'.'||kegiatan_code||'.'||detail_no in ($kode_detail_kegiatan)
                 and rd.volume>0 and rd.status_hapus=FALSE
                 and de.id=dk.detail_kegiatan_id 
                 and de.kode_detail_kegiatan=rd.unit_id||'.'||rd.kegiatan_code||'.'||rd.detail_no
                 and dk.nilai >0
                order by kode_detail_kegiatan";

        $stmt = $con->prepareStatement($query2);
        $t = $stmt->executeQuery();
        while ($t->next()) {
            if ($kode != $t->getString('kode_detail_kegiatan')) {
                $arr_totNilaiSwakelola[$kode] = $totNilaiSwakelola;
                $totNilaiSwakelola = $t->getString('nilai_realisasi');
                $kode = $t->getString('kode_detail_kegiatan');
            } else {
                $totNilaiSwakelola += $t->getString('nilai_realisasi');
            }
        }

        $arr_totNilaiSwakelola[$kode] = $totNilaiSwakelola;

        $query3 = "select dkeg.kode_detail_kegiatan, dkeg.nama, sum(dks.nilai) as nilai_sts, sum(dks.volume) as volume
                    from " . sfConfig::get('app_default_edelivery') . ".sts sts
                    join " . sfConfig::get('app_default_edelivery') . ".detail_sts ds on ds.sts_id = sts.id
                    join " . sfConfig::get('app_default_edelivery') . ".detail_komponen_sts dks on dks.sts_id = ds.sts_id and dks.transaksi_id = ds.transaksi_id
                    join " . sfConfig::get('app_default_edelivery') . ".detail_komponen dk on dk.id = dks.detail_komponen_id
                    join " . sfConfig::get('app_default_eproject') . ".detail_kegiatan dkeg on dkeg.id = dk.detail_kegiatan_id
                    where sts.status = 100 and sts.jenis = 1 and dkeg.kode_detail_kegiatan in  ($kode_detail_kegiatan)
                    group by dkeg.kode_detail_kegiatan, dkeg.nama";
        $stmt = $con->prepareStatement($query3);
        $rs1 = $stmt->executeQuery();
        while ($rs1->next()) {
            $nilai_sts = $rs1->getFloat('nilai_sts');
            $kode_detail = $rs1->getString('kode_detail_kegiatan');
            if(isset($arr_totNilaiSwakelola[$kode_detail]))
                $arr_totNilaiSwakelola[$kode_detail] -= $nilai_sts;
            else
                $arr_totNilaiSwakelola[$kode_detail] = $nilai_sts;
        }


        return $arr_totNilaiSwakelola;
    }

    //Validasi nilai kontrak dengan banyak komponen (array detail_no)
    public function getCekNilaiKontrakDeliveryArray($unit_id, $kode_kegiatan, $detail_no) {
        $con = Propel::getConnection();

        //kumpulan kode detail kegiatan yang divalidasi
        $kumpulan_kode_detail_kegiatan = array();
        foreach ($detail_no as $no) {
            $kumpulan_kode_detail_kegiatan[] = "'$unit_id.$kode_kegiatan.$no'";
        }
        $kode = $kumpulan_kode_detail_kegiatan[0];
        $kode_detail_kegiatan = implode(',', $kumpulan_kode_detail_kegiatan);

        //cek nilai kontrak
        $arr_totNilaiKontrak = array();
        $query2 = "select rd.unit_id,rd.kegiatan_code,rd.detail_no,rd.subtitle,rd.komponen_name,rd.kode_sub,
                        de.kode_detail_kegiatan,dk.detail_swakelola_id,dk.kontrak_id,
                        sum(rd.nilai_anggaran) as nilai_budgeting,
                        sum(dk.harga_realisasi_bulat) as nilai_kontrak,
                        sum(dk.biaya_lain) as nilai_tambahan, 
                        sum(dk.lk) as adendum_perubahan,
                        sum(dk.pengurangan_sts) as pengembalian_sts 
                   from " . sfConfig::get('app_default_edelivery') . ".detail_komponen dk,
                        " . sfConfig::get('app_default_edelivery') . ".kontraks k,
                        " . sfConfig::get('app_default_eproject') . ".detail_kegiatan de,
                        " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail rd
                   where rd.unit_id||'.'||rd.kegiatan_code||'.'||rd.detail_no in ($kode_detail_kegiatan) and
                         rd.volume>0 and rd.status_hapus=FALSE
                         and de.id=dk.detail_kegiatan_id 
                         and de.kode_detail_kegiatan=rd.unit_id||'.'||rd.kegiatan_code||'.'||rd.detail_no
                         and dk.kontrak_id=k.id and dk.kontrak_id is not null
                   group by rd.unit_id,rd.kegiatan_code,rd.detail_no,rd.subtitle,rd.komponen_name,rd.kode_sub,
                         de.kode_detail_kegiatan,dk.detail_swakelola_id,dk.kontrak_id
                   having  sum(dk.harga_realisasi_bulat)>0
                   order by rd.unit_id,rd.kegiatan_code,rd.detail_no";
        $stmt = $con->prepareStatement($query2);
        $t = $stmt->executeQuery();
        while ($t->next()) {
            if ($kode != $t->getString('kode_detail_kegiatan')) {
                $arr_totNilaiKontrak[$kode] = $totNilaiKontrak;
                $totNilaiKontrak = $t->getString('nilai_kontrak') + $t->getString('nilai_tambahan') + $t->getString('adendum_perubahan');
                $kode = $t->getString('kode_detail_kegiatan');
            } else {
                $totNilaiKontrak += $t->getString('nilai_kontrak') + $t->getString('nilai_tambahan') + $t->getString('adendum_perubahan');
            }
        }
        $arr_totNilaiKontrak[$kode] = $totNilaiKontrak;

        //cek ada komponen yang multiyears
        $tahun_sekarang = sfConfig::get('app_tahun_default');
        $tahun_kemaren = $tahun_sekarang - 1;
        $query_cek_multiyears = "select unit_id||'.'||kegiatan_code||'.'||detail_no as kode, th_ke_multiyears from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail
                    where unit_id||'.'||kegiatan_code||'.'||detail_no in ($kode_detail_kegiatan) and th_ke_multiyears=2";
        $stmt_cek_multiyears = $con->prepareStatement($query_cek_multiyears);
        $t_cek_multiyears = $stmt_cek_multiyears->executeQuery();
        while ($t_cek_multiyears->next()) {
            $kode = $t_cek_multiyears->getString('kode');
            $query_sp2d_tahun_lalu = "select p.id, sum(es.nilai_belanja) as total 
                from " . sfConfig::get('app_default_eproject') . ".detail_kegiatan dk 
                left join " . sfConfig::get('app_default_eproject') . ".kegiatan k on k.id = dk.kegiatan_id 
                left join " . sfConfig::get('app_default_eproject') . ".pekerjaan p on p.kegiatan_id = k.id 
                left join " . sfConfig::get('app_default_edelivery') . ".essact_sync es on es.kode_paket = p.id 
                where p.id = (select dp.pekerjaan_id 
                        from " . sfConfig::get('app_default_eproject') . ".detail_pekerjaan dp,
                             " . sfConfig::get('app_default_eproject') . ".detail_kegiatan dk,
                             " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail rd 
                        where dk.kode_detail_kegiatan = '$kode' 
                        and dk.kode_detail_kegiatan = rd.unit_id||'.'||rd.kegiatan_code||'.'||rd.detail_no 
                        and dp.detail_kegiatan_id = dk.id) 
                and date_part('year',es.tanggal_sp2d) = '$tahun_kemaren' 
                group by p.id";
            $stmt_sp2d_tahun_lalu = $con->prepareStatement($query_sp2d_tahun_lalu);
            $t_sp2d_tahun_lalu = $stmt_sp2d_tahun_lalu->executeQuery();
            while ($t_sp2d_tahun_lalu->next()) {
                $sp2d_tahun_lalu = $t_sp2d_tahun_lalu->getString('total');
            }

            $arr_totNilaiKontrak[$kode] -= $sp2d_tahun_lalu;
        }

        return $arr_totNilaiKontrak;
    }

    //validasi cek nilai realisasi dengan banyak komponen (array detail_no)
    public function getCekRealisasiArray($unit_id, $kode_kegiatan, $detail_no) {
        $con = Propel::getConnection();
        $nilai = 0;

        //kumpulan kode detail kegiatan yang divalidasi
        $kumpulan_kode_detail_kegiatan = array();
        $pkumpulan_kode_detail_kegiatan = array();

        foreach ($detail_no as $no) {
            $kumpulan_kode_detail_kegiatan[] = "'$unit_id.$kode_kegiatan.$no'";
        }
        $kode = $kumpulan_kode_detail_kegiatan[0];
        $pkode = $kumpulan_kode_detail_kegiatan[0];

        $kode_detail_kegiatan = implode(',', $kumpulan_kode_detail_kegiatan);
        $pkode_detail_kegiatan = implode(',', $kumpulan_kode_detail_kegiatan);
        
       
        //cek nilai realisasi menjadi
        $arr_totNilaiRealisasi = array();
        $query2 = "select tb0.skpd_kode, tb0.skpd_nama, tb0.keg_kode, tb0.keg_nama, tb0.kode_komponen, tb0.kode_detail_kegiatan, tb0.komponen, sum(tb0.volume) as volume_realisasi, sum(tb0.nilai) as nilai_realisasi
        from (
        (
            select skpd.kode as skpd_kode, skpd.nama as skpd_nama, keg.kode as keg_kode, keg.nama as keg_nama, dkeg.kode_detail_kegiatan, dkeg.kode_komponen,
            dkeg.nama as komponen, dk.volume_kontrak as volume, (dk.harga_realisasi_bulat + dk.lk + dk.biaya_lain) as nilai
            from  edelivery.detail_komponen dk
            join edelivery.kontraks k 
            join eproject.pekerjaan p on k.pekerjaan_id = p.id
            on k.id = dk.kontrak_id
            right join eproject.detail_kegiatan dkeg
            on dkeg.id = dk.detail_kegiatan_id
            join  eproject.kegiatan keg
            on keg.id = dkeg.kegiatan_id
            right join  eproject.skpd skpd
            on skpd.id = keg.skpd_id
            where dk.harga_realisasi > 0 and ( k.status not in(110,100) or 
            (p.metode not in(11) and p.jenis in(4,12,13,14,15,16,1,3,5) and (k.status = 100 ) and k.bentuk_kontrak = 1 ) or
            (p.metode not in(11) and p.jenis in(4,12,13,14,15,16,1) and (k.status = 100 ) and k.bentuk_kontrak in(2,3,13))
            or (k.status = 100 and p.metode = 11) ) and k.id not in (
                select k2.id
                from edelivery.kontraks k2
                join edelivery.pemutusan_kontrak pk on k2.id = pk.kontrak_id
                where k2.metode != 6
            )
            and dk.detail_swakelola_id is null and dkeg.kode_detail_kegiatan in ($kode_detail_kegiatan)
        )
        union all
        (
        select skpd.kode as skpd_kode, skpd.nama as skpd_nama, keg.kode as keg_kode, keg.nama as keg_nama, dkeg.kode_detail_kegiatan, dkeg.kode_komponen,
            dkeg.nama as komponen, dk.volume_kontrak as volume, 
            coalesce((dkmc.harga_realisasi_bulat + dkmc.biaya_lain + coalesce(dkmc.lk,0)), (dk.harga_realisasi_bulat + coalesce(dk.lk,0) + dk.biaya_lain)) as realisasi
            from  edelivery.detail_komponen dk
            left join edelivery.det_kom_mc dkmc on dkmc.id = dk.id
            join edelivery.kontraks k on k.id = dk.kontrak_id
            right join eproject.detail_kegiatan dkeg on dkeg.id = dk.detail_kegiatan_id
            join  eproject.kegiatan keg on keg.id = dkeg.kegiatan_id
            right join  eproject.skpd skpd on skpd.id = keg.skpd_id
            join eproject.pekerjaan p on k.pekerjaan_id = p.id
            where dk.harga_realisasi > 0 and k.id not in (
                select k2.id
                from edelivery.kontraks k2
                join edelivery.pemutusan_kontrak pk on k2.id = pk.kontrak_id
                where k2.metode != 6
            ) and (p.metode not in(11) and p.jenis not in(4,12,13,14,15,16,1) and k.status = 100 and k.bentuk_kontrak in(2,3,13))
            and dk.detail_swakelola_id is null and dkeg.kode_detail_kegiatan='$unit_id.$kode_kegiatan.$detail_no' --and k.pekerjaan_id = 21000184
        )
        union all
        (
            select s2.kode as skpd_kode, s2.nama as skpd_nama, keg2.kode as keg_kode, keg2.nama as keg_nama, dk2.kode_detail_kegiatan, dk2.kode_komponen,
            dk2.nama as komponen, coalesce(dkmc2.volume_kontrak, dkom2.volume_kontrak) as volume, 
            coalesce(dkmc2.harga_realisasi_bulat, dkom2.harga_realisasi_bulat + dkom2.biaya_lain) as nilai
            from edelivery.kontraks k3 
            join edelivery.essact_sync es on k3.pekerjaan_id = es.kode_paket
            join edelivery.essact_komponen ek on es.id = ek.essact_sync_id
            join edelivery.detail_komponen dkom2 on dkom2.id = ek.detail_komponen_id
            join eproject.detail_pekerjaan dp2 on dp2.pekerjaan_id = k3.pekerjaan_id and dp2.detail_kegiatan_id = dkom2.detail_kegiatan_id
            join eproject.detail_kegiatan dk2 on dk2.id = dkom2.detail_kegiatan_id
            join eproject.pekerjaan p2 on p2.id = k3.pekerjaan_id
            join eproject.kegiatan keg2 on keg2.id = p2.kegiatan_id
            join eproject.skpd s2 on s2.id = keg2.skpd_id
            left join edelivery.det_kom_mc dkmc2 on dkmc2.id = dkom2.id
            where (k3.status = 110 or k3.id in (
                select k2.id
                from edelivery.kontraks k2
                join edelivery.pemutusan_kontrak pk on k2.id = pk.kontrak_id
                where k2.metode != 6
            )) and k3.metode != 6 and
            es.nilai_belanja > 0 and dk2.kode_detail_kegiatan in ($kode_detail_kegiatan)
        )
        UNION all
        (
        select skpd.kode as skpd_kode, skpd.nama as skpd_nama, keg.kode as keg_kode, keg.nama as keg_nama, dkeg.kode_detail_kegiatan, dkeg.kode_komponen,
        dkeg.nama as komponen, ds.volume_total as volume, ds.nilai
        from edelivery.detail_swakelola ds
        right join eproject.detail_kegiatan dkeg
        on dkeg.id = ds.detail_kegiatan_id
        join eproject.kegiatan keg
        on keg.id = dkeg.kegiatan_id
        right join eproject.skpd skpd
        on skpd.id = keg.skpd_id
        right join edelivery.paket_honor ph
        on ph.id = ds.paket_honor_id
        right join eproject.pekerjaan p
        on p.id = ph.pekerjaan_id
        where p.metode != 11 and dkeg.kode_detail_kegiatan in ($kode_detail_kegiatan)
        )
        UNION all
        (
        select skpd.kode as skpd_kode, skpd.nama as skpd_nama, keg.kode as keg_kode, keg.nama as keg_nama, 
        dk.kode_detail_kegiatan , dk.kode_komponen , dk.nama as komponen ,
        coalesce (0) as volume, 
        case when eo.ppn = 10 then round(eod.nominal * 1.1) 
        else eod.nominal end as nilai 
        from edelivery.epls e
        join edelivery.epl_oe eo on eo.epl_id = e.id 
        join edelivery.epl_oe_detail eod  on eo.epl_id = eod.epl_id 
        join eproject.detail_kegiatan dk on dk.id = eod.detail_kegiatan_id 
        join eproject.detail_pekerjaan dp on dp.detail_kegiatan_id = dk.id and dp.pekerjaan_id = e.pekerjaan_id 
        join eproject.pekerjaan p on p.id = dp.pekerjaan_id
        join eproject.kegiatan keg on keg.id = p.kegiatan_id
        join eproject.skpd skpd on skpd.id = keg.skpd_id
        where e.status = 1 and dk.kode_detail_kegiatan in ($kode_detail_kegiatan) and dp.alokasi > 0
        )
        ) tb0
        group by tb0.skpd_kode, tb0.skpd_nama, tb0.keg_kode, tb0.keg_nama, tb0.kode_komponen, tb0.kode_detail_kegiatan, tb0.komponen";

       //cek nilai realisasi semula
        // $arr_totNilaiRealisasi = array();
        // $query2 = "select tb0.skpd_kode, tb0.skpd_nama, tb0.keg_kode, tb0.keg_nama, tb0.kode_komponen, tb0.kode_detail_kegiatan, tb0.komponen, sum(tb0.volume) as volume_realisasi, sum(tb0.nilai) as nilai_realisasi
        // from ((
        //     select skpd.kode as skpd_kode, skpd.nama as skpd_nama, keg.kode as keg_kode, keg.nama as keg_nama, dkeg.kode_detail_kegiatan, dkeg.kode_komponen,
        //     dkeg.nama as komponen, dk.volume_kontrak as volume, (dk.harga_realisasi_bulat + dk.lk + dk.biaya_lain) as nilai
        //     from  edelivery.detail_komponen dk
        //     join edelivery.kontraks k 
        //     on k.id = dk.kontrak_id
        //     right join eproject.detail_kegiatan dkeg
        //     on dkeg.id = dk.detail_kegiatan_id
        //     join  eproject.kegiatan keg
        //     on keg.id = dkeg.kegiatan_id
        //     right join  eproject.skpd skpd
        //     on skpd.id = keg.skpd_id
        //     where dk.harga_realisasi > 0 and k.status <> 110 and k.id not in (
        //         select k2.id
        //         from edelivery.kontraks k2
        //         join edelivery.pemutusan_kontrak pk on k2.id = pk.kontrak_id
        //         where k2.metode != 6
        //     )
        //     and dk.detail_swakelola_id is null and dkeg.kode_detail_kegiatan in ($kode_detail_kegiatan)
        // )
        // union all
        // (
        //     select s2.kode as skpd_kode, s2.nama as skpd_nama, keg2.kode as keg_kode, keg2.nama as keg_nama, dk2.kode_detail_kegiatan, dk2.kode_komponen,
        //     dk2.nama as komponen, coalesce(dkmc2.volume_kontrak, dkom2.volume_kontrak) as volume, 
        //     coalesce(dkmc2.harga_realisasi_bulat, dkom2.harga_realisasi_bulat + dkom2.biaya_lain) as nilai
        //     from edelivery.kontraks k3 
        //     join edelivery.essact_sync es on k3.pekerjaan_id = es.kode_paket
        //     join edelivery.essact_komponen ek on es.id = ek.essact_sync_id
        //     join edelivery.detail_komponen dkom2 on dkom2.id = ek.detail_komponen_id
        //     join eproject.detail_pekerjaan dp2 on dp2.pekerjaan_id = k3.pekerjaan_id and dp2.detail_kegiatan_id = dkom2.detail_kegiatan_id
        //     join eproject.detail_kegiatan dk2 on dk2.id = dkom2.detail_kegiatan_id
        //     join eproject.pekerjaan p2 on p2.id = k3.pekerjaan_id
        //     join eproject.kegiatan keg2 on keg2.id = p2.kegiatan_id
        //     join eproject.skpd s2 on s2.id = keg2.skpd_id
        //     left join edelivery.det_kom_mc dkmc2 on dkmc2.id = dkom2.id
        //     where (k3.status = 110 or k3.id in (
        //         select k2.id
        //         from edelivery.kontraks k2
        //         join edelivery.pemutusan_kontrak pk on k2.id = pk.kontrak_id
        //         where k2.metode != 6
        //     )) and k3.metode != 6 and
        //     es.nilai_belanja > 0 and dk2.kode_detail_kegiatan in ($kode_detail_kegiatan)
        // )
        // UNION all
        // (
        // select skpd.kode as skpd_kode, skpd.nama as skpd_nama, keg.kode as keg_kode, keg.nama as keg_nama, dkeg.kode_detail_kegiatan, dkeg.kode_komponen,
        // dkeg.nama as komponen, ds.volume_total as volume, ds.nilai--, p.id, p.metode
        // from edelivery.detail_swakelola ds
        // right join eproject.detail_kegiatan dkeg
        // on dkeg.id = ds.detail_kegiatan_id
        // join eproject.kegiatan keg
        // on keg.id = dkeg.kegiatan_id
        // right join eproject.skpd skpd
        // on skpd.id = keg.skpd_id
        // right join edelivery.paket_honor ph
        // on ph.id = ds.paket_honor_id
        // right join eproject.pekerjaan p
        // on p.id = ph.pekerjaan_id
        // where p.metode != 11 and dkeg.kode_detail_kegiatan in ($kode_detail_kegiatan)
        // )
        // UNION all
        // (
        // select skpd.kode as skpd_kode, skpd.nama as skpd_nama, keg.kode as keg_kode, keg.nama as keg_nama, 
        // dk.kode_detail_kegiatan , dk.kode_komponen , dk.nama as komponen ,
        // coalesce (0) as volume, 
        // case when eo.ppn = 10 then round(eod.nominal * 1.1) 
        // else eod.nominal end as nilai 
        // from edelivery.epls e
        // join edelivery.epl_oe eo on eo.epl_id = e.id 
        // join edelivery.epl_oe_detail eod  on eo.epl_id = eod.epl_id 
        // join eproject.detail_kegiatan dk on dk.id = eod.detail_kegiatan_id 
        // join eproject.detail_pekerjaan dp on dp.detail_kegiatan_id = dk.id and dp.pekerjaan_id = e.pekerjaan_id 
        // join eproject.pekerjaan p on p.id = dp.pekerjaan_id
        // join eproject.kegiatan keg on keg.id = p.kegiatan_id
        // join eproject.skpd skpd on skpd.id = keg.skpd_id
        // where e.status = 1 and dk.kode_detail_kegiatan in ($kode_detail_kegiatan) and dp.alokasi > 0
        // )
        // ) tb0
        // group by tb0.skpd_kode, tb0.skpd_nama, tb0.keg_kode, tb0.keg_nama, tb0.kode_komponen, tb0.kode_detail_kegiatan, tb0.komponen";

        $stmt = $con->prepareStatement($query2);
        $t = $stmt->executeQuery();
        while ($t->next()) {
            $kode = $t->getString('kode_detail_kegiatan');
            $totRealisasi = $t->getFloat('nilai_realisasi');
            if(isset($arr_totNilaiRealisasi[$kode]))
                $arr_totNilaiRealisasi[$kode] += $totRealisasi;
            else
                $arr_totNilaiRealisasi[$kode] = $totRealisasi;
            // if ($kode != $t->getString('kode_detail_kegiatan')) {
            //     $arr_totNilaiRealisasi[$kode] = $totRealisasi;
            //     $totRealisasi = $t->getFloat('nilai_realisasi');
            //     $kode = $t->getString('kode_detail_kegiatan');
            // } else {
            //     $totRealisasi += $t->getString('nilai_realisasi');
            // }
            // var_dump("kode: ".$kode);
            // var_dump("arr_totNilaiRealisasi: ".$arr_totNilaiRealisasi);
            // var_dump("totRealisasi: ".$totRealisasi);
        }
        // $arr_totNilaiRealisasi[$kode] = $totRealisasi;
        // var_dump("arr_totNilaiRealisasi[kode] terakhir: ".$arr_totNilaiRealisasi[$kode]);
        // die();
        // query mencari nilai yang putus kontrak
        $query = "select kode_detail_kegiatan, sum(nilai) as nilai                                                        
        from (                                     
                select k.id, dk.kode_detail_kegiatan, coalesce((mc.harga_realisasi_bulat + mc.lk + mc.biaya_lain), (dkom.harga_realisasi_bulat + dkom.lk + dkom.biaya_lain)) as nilai
                from " . sfConfig::get('app_default_edelivery') . ".detail_komponen dkom
                left join " . sfConfig::get('app_default_edelivery') . ".det_kom_mc mc on mc.id = dkom.id
                join " . sfConfig::get('app_default_edelivery') . ".kontraks k on k.id = dkom.kontrak_id
                join " . sfConfig::get('app_default_eproject') . ".detail_kegiatan dk on dk.id = dkom.detail_kegiatan_id
                join (select distinct dk.kode_detail_kegiatan, k.id, (case when pk.jumlah > 0 then pk.termin_ke else pk.termin_ke - 1 end) as termin_ke
                        from " . sfConfig::get('app_default_edelivery') . ".kontraks k 
                        join " . sfConfig::get('app_default_eproject') . ".pekerjaan p on p.id = k.pekerjaan_id
                        join " . sfConfig::get('app_default_eproject') . ".detail_pekerjaan dp on dp.pekerjaan_id = p.id
                        join " . sfConfig::get('app_default_eproject') . ".detail_kegiatan dk on dk.id = dp.detail_kegiatan_id
                        left join " . sfConfig::get('app_default_edelivery') . ".pemutusan_kontrak pk on pk.kontrak_id = k.id
                        where k.metode not in (6,7) and k.status=110 and dk.kode_detail_kegiatan in ($kode_detail_kegiatan)
                        group by k.id, dk.kode_detail_kegiatan, pk.termin_ke, pk.jumlah) a ON k.id = a.id and dkom.termin_ke <= a.termin_ke and dk.kode_detail_kegiatan = a.kode_detail_kegiatan
                where k.metode not in (6,7) and k.status=110 and dk.kode_detail_kegiatan in ($kode_detail_kegiatan)
        ) komponen
        group by kode_detail_kegiatan";
        $stmt = $con->prepareStatement($query);
        $rs = $stmt->executeQuery();
        while ($rs->next()) {
            $nilai = $rs->getFloat('nilai');
            $kode_detail = $rs->getString('kode_detail_kegiatan');
            if(isset($arr_totNilaiRealisasi[$kode_detail]))
                $arr_totNilaiRealisasi[$kode_detail] += $nilai;
            else
                $arr_totNilaiRealisasi[$kode_detail] = $nilai;
        }

        $query3 = "select dkeg.kode_detail_kegiatan, dkeg.nama, sum(dks.nilai) as nilai_sts, sum(dks.volume) as volume
                    from " . sfConfig::get('app_default_edelivery') . ".sts sts
                    join " . sfConfig::get('app_default_edelivery') . ".detail_sts ds on ds.sts_id = sts.id
                    join " . sfConfig::get('app_default_edelivery') . ".detail_komponen_sts dks on dks.sts_id = ds.sts_id and dks.transaksi_id = ds.transaksi_id
                    join " . sfConfig::get('app_default_edelivery') . ".detail_komponen dk on dk.id = dks.detail_komponen_id
                    join " . sfConfig::get('app_default_eproject') . ".detail_kegiatan dkeg on dkeg.id = dk.detail_kegiatan_id
                    where sts.status = 100 and sts.jenis = 1 and dkeg.kode_detail_kegiatan in  ($kode_detail_kegiatan)
                    group by dkeg.kode_detail_kegiatan, dkeg.nama";
        $stmt = $con->prepareStatement($query3);
        $rs1 = $stmt->executeQuery();
        while ($rs1->next()) {
            $nilai_sts = $rs1->getFloat('nilai_sts');
            $kode_detail = $rs1->getString('kode_detail_kegiatan');
            if(isset($arr_totNilaiRealisasi[$kode_detail]))
                $arr_totNilaiRealisasi[$kode_detail] -= $nilai_sts;
            else
                $arr_totNilaiRealisasi[$kode_detail] = $nilai_sts;
        }

         //*****query lk menjadi
        $query4 = "select dk.kode_detail_kegiatan, coalesce(sum(lkk.jumlah),0) as nilai_lk, coalesce(sum(lkk.volume_lk),0) as volume_lk
        from edelivery.kontrak_mc_addendum as kma
        join edelivery.lebih_kurang_komponen lkk on lkk.kontrak_mc_addendum_id = kma.id
        join eproject.detail_kegiatan dk on dk.id = lkk.detail_kegiatan_id
        join edelivery.kontraks k on kma.kontrak_id = k.id
        join eproject.pekerjaan p on k.pekerjaan_id = p.id 
        where dk.kode_detail_kegiatan in ($kode_detail_kegiatan) and 
        ( k.status not in(100) or
        (p.metode not in(11) and p.jenis in(4,12,13,14,15,16,1,3,5) and (k.status = 100 ) and k.bentuk_kontrak = 1 ) or
        (p.metode not in(11) and p.jenis in(4,12,13,14,15,16,1) and (k.status = 100 ) and k.bentuk_kontrak in(2,3,13))
        or (k.status = 100 and p.metode = 11) )
        group by dk.kode_detail_kegiatan";

        //*****query lk
        // $query4 = "select dk.kode_detail_kegiatan, coalesce(sum(lkk.jumlah),0) as nilai_lk, coalesce(sum(lkk.volume_lk),0) as volume_lk
        //             from " . sfConfig::get('app_default_edelivery') . ".kontrak_mc_addendum as kma
        //             join " . sfConfig::get('app_default_edelivery') . ".lebih_kurang_komponen lkk on lkk.kontrak_mc_addendum_id = kma.id
        //             join " . sfConfig::get('app_default_eproject') . ".detail_kegiatan dk on dk.id = lkk.detail_kegiatan_id
        //             where dk.kode_detail_kegiatan in ($kode_detail_kegiatan) and kma.status = 100
        //             group by 1";
        $stmt2 = $con->prepareStatement($query4);
        $rs2 = $stmt2->executeQuery();
        while ($rs2->next()) {
            $nilai_lk = $rs2->getFloat('nilai_lk');
            $kode_detail = $rs2->getString('kode_detail_kegiatan');
            if(isset($arr_totNilaiRealisasi[$kode_detail]))
                $arr_totNilaiRealisasi[$kode_detail] += $nilai_lk;
            else
                $arr_totNilaiRealisasi[$kode_detail] = $nilai_lk;
        }

        return $arr_totNilaiRealisasi;
    }

    //validasi cek volume realisasi dengan banyak komponen (array detail_no)
    public function getCekVolumeRealisasiArray($unit_id, $kode_kegiatan, $detail_no) {
        $con = Propel::getConnection();

        //kumpulan kode detail kegiatan yang divalidasi
        $kumpulan_kode_detail_kegiatan = array();
        foreach ($detail_no as $no) {
            if (!in_array($unit_id . '.' . $kode_kegiatan . '.' . $no, array(''))) {
                $kumpulan_kode_detail_kegiatan[] = "'$unit_id.$kode_kegiatan.$no'";
            }
        }
       
        $kode = $kumpulan_kode_detail_kegiatan[0];
        $kode_detail_kegiatan = implode(',', $kumpulan_kode_detail_kegiatan);

        //die($kode_detail_kegiatan);

        

        //******query swakelola
        $query = "select tb.skpd_kode, tb.skpd_nama, tb.keg_kode, tb.keg_nama, tb.kode_detail_kegiatan, tb.kode_komponen, tb.komponen, (sum(tb.volume)) as volume
        from (
        select skpd.kode as skpd_kode, skpd.nama as skpd_nama, keg.kode as keg_kode, keg.nama as keg_nama, dkeg.kode_detail_kegiatan, dkeg.kode_komponen, dkeg.nama as komponen, ds.volume_total as volume, ds.nilai
        from edelivery.detail_swakelola ds
        right join eproject.detail_kegiatan dkeg on dkeg.id = ds.detail_kegiatan_id
        left join eproject.detail_pekerjaan dp on dp.detail_kegiatan_id = dkeg.id
        left join eproject.pekerjaan p on p.id = dp.pekerjaan_id
        join eproject.kegiatan keg on keg.id = dkeg.kegiatan_id
        right join eproject.skpd skpd on skpd.id = keg.skpd_id
        join edelivery.paket_honor ph on ph.pekerjaan_id = p.id and ph.id = ds.paket_honor_id
        where dp.alokasi > 0 and dkeg.kode_detail_kegiatan in ($kode_detail_kegiatan) and p.metode in (6,7,10,51,52,53) 
             and (ds.is_jkn_jan is false or dkeg.is_iuran_jkn is true)
        )tb
        group by tb.skpd_kode, tb.skpd_nama, tb.keg_kode, tb.keg_nama, tb.kode_komponen, tb.kode_detail_kegiatan, tb.komponen";

        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query);
        $t = $stmt->executeQuery();
        while ($t->next()) {
            if ($kode != $t->getString('kode_detail_kegiatan')) {
                $arr_totVolumeRealisasi[$kode] = $totVolume;
                $totVolume = $t->getString('volume');
                $kode = $t->getString('kode_detail_kegiatan');
            } else {
                $totVolume += $t->getString('volume');
            }
        }
        $arr_totVolumeRealisasi[$kode] = $totVolume;

        //******query non swakelola
        $query2 = "
        select tb0.skpd_kode, tb0.skpd_nama, tb0.keg_kode, tb0.keg_nama, tb0.kode_komponen, tb0.kode_detail_kegiatan, tb0.komponen, sum(tb0.volume) as volume_realisasi, sum(tb0.nilai) as nilai_realisasi
        from ((
            select skpd.kode as skpd_kode, skpd.nama as skpd_nama, keg.kode as keg_kode, keg.nama as keg_nama, dkeg.kode_detail_kegiatan, dkeg.kode_komponen,
            dkeg.nama as komponen, dk.volume_kontrak as volume, (dk.harga_realisasi_bulat + dk.lk + dk.biaya_lain) as nilai
            from  edelivery.detail_komponen dk
            join edelivery.kontraks k 
            on k.id = dk.kontrak_id
            right join eproject.detail_kegiatan dkeg
            on dkeg.id = dk.detail_kegiatan_id
            join  eproject.kegiatan keg
            on keg.id = dkeg.kegiatan_id
            right join  eproject.skpd skpd
            on skpd.id = keg.skpd_id
            where dk.harga_realisasi > 0 and k.status <> 110 and k.id not in (
                select k2.id
                from edelivery.kontraks k2
                join edelivery.pemutusan_kontrak pk on k2.id = pk.kontrak_id
                where k2.metode != 6
            )
            and dk.detail_swakelola_id is null and dkeg.kode_detail_kegiatan in ($kode_detail_kegiatan)
        )
        union all
        (
            select s2.kode as skpd_kode, s2.nama as skpd_nama, keg2.kode as keg_kode, keg2.nama as keg_nama, dk2.kode_detail_kegiatan, dk2.kode_komponen,
            dk2.nama as komponen, coalesce(dkmc2.volume_kontrak, dkom2.volume_kontrak) as volume, 
            coalesce(dkmc2.harga_realisasi_bulat, dkom2.harga_realisasi_bulat + dkom2.biaya_lain) as nilai
            from edelivery.kontraks k3 
            join edelivery.essact_sync es on k3.pekerjaan_id = es.kode_paket
            join edelivery.essact_komponen ek on es.id = ek.essact_sync_id
            join edelivery.detail_komponen dkom2 on dkom2.id = ek.detail_komponen_id
            join eproject.detail_pekerjaan dp2 on dp2.pekerjaan_id = k3.pekerjaan_id and dp2.detail_kegiatan_id = dkom2.detail_kegiatan_id
            join eproject.detail_kegiatan dk2 on dk2.id = dkom2.detail_kegiatan_id
            join eproject.pekerjaan p2 on p2.id = k3.pekerjaan_id
            join eproject.kegiatan keg2 on keg2.id = p2.kegiatan_id
            join eproject.skpd s2 on s2.id = keg2.skpd_id
            left join edelivery.det_kom_mc dkmc2 on dkmc2.id = dkom2.id
            where (k3.status = 110 or k3.id in (
                select k2.id
                from edelivery.kontraks k2
                join edelivery.pemutusan_kontrak pk on k2.id = pk.kontrak_id
                where k2.metode != 6
            )) and k3.metode != 6 and
            es.nilai_belanja > 0 and dk2.kode_detail_kegiatan in ($kode_detail_kegiatan)
        )
        UNION all
        (
        select skpd.kode as skpd_kode, skpd.nama as skpd_nama, keg.kode as keg_kode, keg.nama as keg_nama, dkeg.kode_detail_kegiatan, dkeg.kode_komponen,dkeg.nama as komponen,
        case when p.metode in (6,7,10,51,52,53) then 0 else ds.volume_total end as volume,
        ds.nilai--, p.id, p.metode
        from edelivery.detail_swakelola ds
        right join eproject.detail_kegiatan dkeg
        on dkeg.id = ds.detail_kegiatan_id
        join eproject.kegiatan keg
        on keg.id = dkeg.kegiatan_id
        right join eproject.skpd skpd
        on skpd.id = keg.skpd_id
        right join edelivery.paket_honor ph
        on ph.id = ds.paket_honor_id
        right join eproject.pekerjaan p
        on p.id = ph.pekerjaan_id
        where p.metode != 11 and dkeg.kode_detail_kegiatan in ($kode_detail_kegiatan)
        )
        UNION all
        (
        select skpd.kode as skpd_kode, skpd.nama as skpd_nama, keg.kode as keg_kode, keg.nama as keg_nama, 
        dk.kode_detail_kegiatan , dk.kode_komponen , dk.nama as komponen ,
        coalesce (0) as volume, 
        case when eo.ppn = 10 then round(eod.nominal * 1.1) 
        else eod.nominal end as nilai 
        from edelivery.epls e
        join edelivery.epl_oe eo on eo.epl_id = e.id 
        join edelivery.epl_oe_detail eod  on eo.epl_id = eod.epl_id 
        join eproject.detail_kegiatan dk on dk.id = eod.detail_kegiatan_id 
        join eproject.detail_pekerjaan dp on dp.detail_kegiatan_id = dk.id and dp.pekerjaan_id = e.pekerjaan_id 
        join eproject.pekerjaan p on p.id = dp.pekerjaan_id
        join eproject.kegiatan keg on keg.id = p.kegiatan_id
        join eproject.skpd skpd on skpd.id = keg.skpd_id
        where e.status = 1 and dk.kode_detail_kegiatan in ($kode_detail_kegiatan) and dp.alokasi > 0
        )
        ) tb0
        group by tb0.skpd_kode, tb0.skpd_nama, tb0.keg_kode, tb0.keg_nama, tb0.kode_komponen, tb0.kode_detail_kegiatan, tb0.komponen";
        $con = Propel::getConnection();
        $stmt1 = $con->prepareStatement($query2);
        $s = $stmt1->executeQuery();
        while ($s->next()) {
            $volume_nsk = $s->getFloat('volume_realisasi');
            $kode = $s->getString('kode_detail_kegiatan');
            if(isset($arr_totVolumeRealisasi[$kode]))
                $arr_totVolumeRealisasi[$kode] += $volume_nsk;
            else
                $arr_totVolumeRealisasi[$kode] = $volume_nsk;
        }

        //cek volume realisasi   (lama ditutup per 110321)     
        // $query2 = "                        (select tb0.skpd_kode, tb0.skpd_nama, tb0.keg_kode, tb0.keg_nama, tb0.kode_komponen, tb0.kode_detail_kegiatan, tb0.komponen,  sum(tb0.volume) as volume_realisasi, sum(tb0.nilai) as nilai_realisasi
        //     from ((
        //     select tb.skpd_kode, tb.skpd_nama, tb.keg_kode, tb.keg_nama, tb.kode_detail_kegiatan, tb.kode_komponen, tb.komponen, 
        //     (case 
        //         when tb.jenis = 1 then 1 
        //         when tb.jenis = 13 then 1
        //         when tb.jenis = 12 then 1
        //         else sum(tb.volume)
        //         end) 
        //     as volume, sum(tb.nilai) as nilai
        //     from (
        //         select p.jenis, skpd.kode as skpd_kode, skpd.nama as skpd_nama, keg.kode as keg_kode, keg.nama as keg_nama, dkeg.kode_detail_kegiatan, dkeg.kode_komponen, dkeg.nama as komponen, dk.volume_kontrak as volume, (dk.harga_realisasi_bulat + dk.lk + dk.biaya_lain) as nilai
        //         from  " . sfConfig::get('app_default_edelivery') . ".detail_komponen dk
        //         right join " . sfConfig::get('app_default_eproject') . ".detail_kegiatan dkeg
        //         on dkeg.id = dk.detail_kegiatan_id
        //         left join " . sfConfig::get('app_default_eproject') . ".detail_pekerjaan dp
        //         on dp.detail_kegiatan_id = dkeg.id
        //         left join " . sfConfig::get('app_default_eproject') . ".pekerjaan p
        //         on p.id = dp.pekerjaan_id
        //         join " . sfConfig::get('app_default_eproject') . ".kegiatan keg on keg.id = dkeg.kegiatan_id
        //         join " . sfConfig::get('app_default_eproject') . ".skpd skpd on skpd.id = keg.skpd_id
        //         join " . sfConfig::get('app_default_edelivery') . ".kontraks kn on p.id = kn.pekerjaan_id and kn.id = dk.kontrak_id
        //         where dk.harga_realisasi > 0 and kn.status <> 110 and dkeg.satuan<>'Paket' and dk.detail_swakelola_id is null and dkeg.kode_detail_kegiatan in ($kode_detail_kegiatan) and kn.id not in (
        //             select id from " . sfConfig::get('app_default_edelivery') . ".kontraks where metode = 10 and bukti_perjanjian = 3 and is_spl_baru is false
        //             ) 
        //         )tb
        //         group by tb.jenis, tb.skpd_kode, tb.skpd_nama, tb.keg_kode, tb.keg_nama, tb.kode_komponen, tb.kode_detail_kegiatan, tb.komponen
        //     )
        //     UNION all
        //     (
        //         select skpd.kode as skpd_kode, skpd.nama as skpd_nama, keg.kode as keg_kode, keg.nama as keg_nama, dkeg.kode_detail_kegiatan, dkeg.kode_komponen, dkeg.nama as komponen, ds.volume_total as volume, ds.nilai
        //         from " . sfConfig::get('app_default_edelivery') . ".detail_swakelola ds
        //         right join " . sfConfig::get('app_default_eproject') . ".detail_kegiatan dkeg
        //         on dkeg.id = ds.detail_kegiatan_id
        //         left join " . sfConfig::get('app_default_eproject') . ".detail_pekerjaan dp
        //         on dp.detail_kegiatan_id = dkeg.id
        //         left join " . sfConfig::get('app_default_eproject') . ".pekerjaan p
        //         on p.id = dp.pekerjaan_id
        //         join " . sfConfig::get('app_default_eproject') . ".kegiatan keg
        //         on keg.id = dkeg.kegiatan_id
        //         right join " . sfConfig::get('app_default_eproject') . ".skpd skpd
        //         on skpd.id = keg.skpd_id
        //         join " . sfConfig::get('app_default_edelivery') . ".paket_honor ph
        //         on ph.pekerjaan_id = p.id and ph.id = ds.paket_honor_id
        //         where dp.alokasi > 0 and dkeg.satuan<>'Paket' and dkeg.kode_detail_kegiatan in ($kode_detail_kegiatan) and ds.is_jkn_jan is false and p.metode != 11
        //     )) tb0
        //     group by tb0.skpd_kode, tb0.skpd_nama, tb0.keg_kode, tb0.keg_nama, tb0.kode_komponen, tb0.kode_detail_kegiatan, tb0.komponen
        //     )";
        // $stmt = $con->prepareStatement($query2);
        // $t = $stmt->executeQuery();
        // while ($t->next()) {
        //     if ($kode != $t->getString('kode_detail_kegiatan')) {
        //         $arr_totVolumeRealisasi[$kode] = $totVolume;
        //         $totVolume = $t->getString('volume_realisasi');
        //         $kode = $t->getString('kode_detail_kegiatan');
        //     } else {
        //         $totVolume += $t->getString('volume_realisasi');
        //     }
        // }
        // $arr_totVolumeRealisasi[$kode] = $totVolume;

        $query3 = "SELECT SUM(detail_swakelola.volume_total) AS volume_total_jkn_jan
        FROM " . sfConfig::get('app_default_edelivery') . ".detail_swakelola
        INNER JOIN " . sfConfig::get('app_default_eproject') . ".detail_kegiatan ON (" . sfConfig::get('app_default_edelivery') . ".detail_swakelola.detail_kegiatan_id=" . sfConfig::get('app_default_eproject') . ".detail_kegiatan.id)
        INNER JOIN " . sfConfig::get('app_default_edelivery') . ".paket_honor ph ON (" . sfConfig::get('app_default_edelivery') . ".detail_swakelola.paket_honor_id=ph.id)
        WHERE " . sfConfig::get('app_default_edelivery') . ".detail_swakelola.is_jkn_jan=true AND 
              " . sfConfig::get('app_default_eproject') . ".detail_kegiatan.is_potong_bpjs=true AND 
              " . sfConfig::get('app_default_eproject') . ".detail_kegiatan.kode_detail_kegiatan in ($kode_detail_kegiatan)
        GROUP BY " . sfConfig::get('app_default_eproject') . ".detail_kegiatan.kode_detail_kegiatan
        ORDER BY kode_detail_kegiatan";
        $stmt1 = $con->prepareStatement($query3);
        $s = $stmt1->executeQuery();
        while ($s->next()) {
            $totVolume = $s->getString('volume_total_jkn_jan');
            $arr_totVolumeRealisasi[$kode] -= $totVolume;
        }

        $query4 = "
        select dkeg.kode_detail_kegiatan, dkeg.nama, sum(dks.nilai) as nilai_sts, sum(dks.volume) as volume
                    from " . sfConfig::get('app_default_edelivery') . ".sts sts
                    join " . sfConfig::get('app_default_edelivery') . ".detail_sts ds on ds.sts_id = sts.id
                    join " . sfConfig::get('app_default_edelivery') . ".detail_komponen_sts dks on dks.sts_id = ds.sts_id and dks.transaksi_id = ds.transaksi_id
                    join " . sfConfig::get('app_default_edelivery') . ".detail_komponen dk on dk.id = dks.detail_komponen_id
                    join " . sfConfig::get('app_default_eproject') . ".detail_kegiatan dkeg on dkeg.id = dk.detail_kegiatan_id
                    where sts.status = 100 and sts.jenis = 1 and dkeg.kode_detail_kegiatan in ($kode_detail_kegiatan)
                    group by dkeg.kode_detail_kegiatan,dkeg.nama";
        $con = Propel::getConnection();
        $stmt2 = $con->prepareStatement($query4);
        $v = $stmt2->executeQuery();
        while ($v->next()) {
              $totVolume = $v->getString('volume');
              $arr_totVolumeRealisasi[$kode] -= $totVolume;
        }

        $query5 = "select dk.kode_detail_kegiatan, coalesce(sum(lkk.jumlah),0) as nilai_lk, coalesce(sum(lkk.volume_lk),0) as volume_lk
                    from " . sfConfig::get('app_default_edelivery') . ".kontrak_mc_addendum as kma
                    join " . sfConfig::get('app_default_edelivery') . ".lebih_kurang_komponen lkk on lkk.kontrak_mc_addendum_id = kma.id
                    join " . sfConfig::get('app_default_eproject') . ".detail_kegiatan dk on dk.id = lkk.detail_kegiatan_id
                    where dk.kode_detail_kegiatan in ($kode_detail_kegiatan) and kma.status = 100
                    group by 1";
        $stmt2 = $con->prepareStatement($query5);
        $rs2 = $stmt2->executeQuery();
        while ($rs2->next()) {
            $volume_lk = $rs2->getFloat('volume_lk');
            $kode = $rs2->getString('kode_detail_kegiatan');
            if(isset($arr_totVolumeRealisasi[$kode]))
                $arr_totVolumeRealisasi[$kode] += $volume_lk;
            else
                $arr_totVolumeRealisasi[$kode] = $volume_lk;
        }

        $array_lepas_cek = array('');

        if(in_array($kode, $array_lepas_cek)) { 
            $arr_totVolumeRealisasi[$kode]=0;
        }

        return $arr_totVolumeRealisasi;
    }

    //validasi cek lelang yang tidak ada aturan pembayaran dengan banyak komponen (array detail_no)
    public function getCekLelangTidakAdaAturanPembayaranArray($unit_id, $kode_kegiatan, $detail_no) {
        $con = Propel::getConnection();

        //kumpulan kode detail kegiatan yang divalidasi
        $kumpulan_kode_detail_kegiatan = array();
        foreach ($detail_no as $no) {
            $kumpulan_kode_detail_kegiatan[] = "'$unit_id.$kode_kegiatan.$no'";
        }
        $kode_detail_kegiatan = implode(',', $kumpulan_kode_detail_kegiatan);

        $query = "select dk.kode_detail_kegiatan, k.id, count(dkom.id) as aturan
                from " . sfConfig::get('app_default_eproject') . ".detail_kegiatan dk
                join " . sfConfig::get('app_default_eproject') . ".detail_pekerjaan dp on dp.detail_kegiatan_id = dk.id
                join " . sfConfig::get('app_default_eproject') . ".pekerjaan p on p.id = dp.pekerjaan_id
                left join " . sfConfig::get('app_default_edelivery') . ".detail_komponen dkom on dkom.detail_kegiatan_id = dk.id
                left join " . sfConfig::get('app_default_edelivery') . ".kontraks k on k.pekerjaan_id = p.id
                join " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail rd on rd.detail_kegiatan = dk.kode_detail_kegiatan
                where (p.metode in (2,8,14,15,16,9,11,17) or (p.metode = 10 and p.alokasi >= 50000000))
                and rd.volume>0 
                and rd.status_hapus=FALSE 
                and k.status=0
                and dp.alokasi > 0 and dk.kode_detail_kegiatan in ($kode_detail_kegiatan) 
                group by 1,2
                having count(dkom.id)=0 
                order by dk.kode_detail_kegiatan";
        $statement = $con->prepareStatement($query);
        $rs = $statement->executeQuery();
        $arr = array();
        while ($rs->next()) {
            $arr[$rs->getString('kode_detail_kegiatan')] = 1;
        }
        return $arr;
    }

    //validasi cek delivery yang tidak ada aturan pembayaran dengan banyak komponen (array detail_no)
    public function getCekNilaiDeliveryBelumAdaAturanPembayaranArray($unit_id, $kode_kegiatan, $detail_no) {
        $con = Propel::getConnection();

        //kumpulan kode detail kegiatan yang divalidasi
        $kumpulan_kode_detail_kegiatan = array();
        foreach ($detail_no as $no) {
            $kumpulan_kode_detail_kegiatan[] = "'$unit_id.$kode_kegiatan.$no'";
        }
        $kode_detail_kegiatan = implode(',', $kumpulan_kode_detail_kegiatan);

        $arr = array();
        $query = "select dk.kode_detail_kegiatan, count(*) as jml 
            from " . sfConfig::get('app_default_eproject') . ".detail_pekerjaan dp, 
                 " . sfConfig::get('app_default_eproject') . ".detail_kegiatan dk, 
                 " . sfConfig::get('app_default_edelivery') . ".kontraks k
            where dp.detail_kegiatan_id=dk.id and k.pekerjaan_id=dp.pekerjaan_id and
            dk.kode_detail_kegiatan in ($kode_detail_kegiatan)
            and dp.alokasi>0 and k.metode <> 6 and k.is_spl_baru is false
            group by dk.kode_detail_kegiatan
            having count(*)>0
            order by kode_detail_kegiatan";
        $stmt = $con->prepareStatement($query);
        $rs = $stmt->executeQuery();
        while ($rs->next()) {
            $kode = $rs->getString('kode_detail_kegiatan');
            $query2 = "select count(*) as jml from " . sfConfig::get('app_default_edelivery') . ".kontraks k, " . sfConfig::get('app_default_edelivery') . ".detail_komponen dk
                        where k.id=dk.kontrak_id and k.pekerjaan_id in (select (pekerjaan_id) as id from " . sfConfig::get('app_default_eproject') . ".detail_pekerjaan
                            where detail_kegiatan_id in (select id from " . sfConfig::get('app_default_eproject') . ".detail_kegiatan 
                                where kode_detail_kegiatan = '$kode')) and k.metode <> 6
                      having count(*)=0";
            $stmt2 = $con->prepareStatement($query2);
            $rs2 = $stmt2->executeQuery();
            while ($rs2->next()) {
                if($rs2->getString('jml') != '0')
                    $arr[$kode] = 1;
            }
        }
        return $arr;
    }

    //validasi cek ada komponen yang tidak tercentang
    public function cekKomponenTertinggal($unit_id, $kode_kegiatan, $status_level, $arr_detail_no) {
        $jumlah = 0;
        $kumpulan_detail_no = implode(',', $arr_detail_no);
        // $query = "select 
        //         (
        //         select count(*)
        //         from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail drd, " . sfConfig::get('app_default_schema') . ".rincian_detail rd
        //         where rd.unit_id=drd.unit_id and rd.kegiatan_code=drd.kegiatan_code and rd.detail_no=drd.detail_no and
        //         drd.unit_id='$unit_id' and drd.kegiatan_code='$kode_kegiatan' and drd.status_level<=$status_level and drd.detail_no not in($kumpulan_detail_no) and
        //         (drd.nilai_anggaran<>rd.nilai_anggaran or drd.rekening_code<>rd.rekening_code or drd.subtitle<>rd.subtitle or 
        //         drd.komponen_name<>rd.komponen_name or drd.keterangan_koefisien<>rd.keterangan_koefisien or 
        //         drd.detail_name<>rd.detail_name)
        //         )+(
        //         select count(*)
        //         from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail
        //         where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and status_level<=$status_level and detail_no not in($kumpulan_detail_no) and volume>0 and status_hapus=false and nilai_anggaran > 0 and 
        //         detail_no not in (select detail_no from " . sfConfig::get('app_default_schema') . ".rincian_detail where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan')
        //         ) as nilai";
        // kalau sudah revisi dua 2018 pakai yang atas
        $tabel_semula = 'rincian_detail';
        $c = new Criteria();
        $c->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
        $c->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
        $master_kegiatan = DinasMasterKegiatanPeer::doSelectOne($c);
        if($master_kegiatan) {
            if($master_kegiatan->getTahap() != sfConfig::get('app_tahap_detail') && sfConfig::get('app_tahap_edit') != 'murni') {
                $tabel_semula = 'prev_rincian_detail';
            } else {
                $tabel_semula = 'rincian_detail';
            }
            if ($master_kegiatan->getIsPernahRka()) {
                $tabel_semula = 'prev_rincian_detail';
            }
        }

        $query = "select 
                (
                select count(*)
                from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail drd, " . sfConfig::get('app_default_schema') . ".$tabel_semula rd
                where rd.unit_id=drd.unit_id and rd.kegiatan_code=drd.kegiatan_code and rd.detail_no=drd.detail_no and
                drd.unit_id='$unit_id' and drd.kegiatan_code='$kode_kegiatan' and drd.status_level<=$status_level and drd.detail_no not in($kumpulan_detail_no) and
                (drd.nilai_anggaran<>rd.nilai_anggaran or drd.rekening_code<>rd.rekening_code or 
                drd.komponen_name<>rd.komponen_name or drd.keterangan_koefisien<>rd.keterangan_koefisien or 
                drd.detail_name<>rd.detail_name)
                )+(
                select count(*)
                from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail
                where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and status_level<=$status_level and detail_no not in($kumpulan_detail_no) and volume>0 and status_hapus=false and nilai_anggaran > 0 and 
                detail_no not in (select detail_no from " . sfConfig::get('app_default_schema') . ".$tabel_semula where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan')
                ) as nilai";
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query);
        $rs = $stmt->executeQuery();
        while ($rs->next()) {
            $jumlah = $rs->getInt('nilai');
        }
        if ($jumlah > 0) {
            return TRUE;
        }
        
        return FALSE;
    }

}
