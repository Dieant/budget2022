<?php

/**
 * Subclass for representing a row from the 'budget2012.revisi5_master_kegiatan' table.
 *
 * 
 *
 * @package lib.model.budgeting
 */ 
class Revisi5MasterKegiatan extends BaseRevisi5MasterKegiatan
{
    public function getKodeNamaKegiatan()
    {
        return $this->getKodeKegiatan().'-'.$this->getNamaKegiatan();
    }
}
