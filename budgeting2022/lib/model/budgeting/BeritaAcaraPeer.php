<?php

/**
 * Subclass for performing query and update operations on the '" . sfConfig::get('app_default_schema') . ".berita_acara' table.
 *
 * 
 *
 * @package lib.model.budgeting
 */
class BeritaAcaraPeer extends BaseBeritaAcaraPeer {

    public static function saveUsulanDinas($unit_id, $kode_kegiatan) {
        $con = Propel::getConnection();
        $con->begin();
        try {
            $query = "select is_pernah_rka, tahap from " . sfConfig::get('app_default_schema') . ".dinas_master_kegiatan where unit_id='$unit_id' and kode_kegiatan='$kode_kegiatan'";
            $stmt = $con->prepareStatement($query);
            $rs_tahap = $stmt->executeQuery();
            if ($rs_tahap->next()) {
                $tahap = $rs_tahap->getString('tahap');
                $pernahrka = $rs_tahap->getBoolean('is_pernah_rka');
            } else {
                $tahap = 'murni';
                $pernahrka = false;
            }

            $tabel_semula = 'rincian_detail';
            if ($pernahrka) {
                $tabel_semula = 'prev_rincian_detail';
            }

            $query = "select max(id) as id from " . sfConfig::get('app_default_schema') . ".berita_acara";
            $stmt = $con->prepareStatement($query);
            $rs_id = $stmt->executeQuery();
            if ($rs_id->next()) {
                $id_baru = $rs_id->getInt('id') + 1;
            } else {
                $id_baru = 1;
            }
            $token = md5(rand());

            $berita_acara = new BeritaAcara();
            $berita_acara->setId($id_baru);
            $berita_acara->setUnitId($unit_id);
            $berita_acara->setKegiatanCode($kode_kegiatan);
            $berita_acara->setToken($token);
            $berita_acara->setWaktu(date('Y-m-d H:i:s'));
            $berita_acara->setTahap($tahap);
            $berita_acara->save();

//            $query = "(
//            SELECT
//                rekening.rekening_code,
//                rekening.rekening_code as rekening_code2,
//                rekening.rekening_name as nama_rekening2,
//                detail2.komponen_name || ' ' || detail2.detail_name as detail_name22,
//                detail2.komponen_harga_awal as detail_harga2,
//                detail2.pajak as pajak2,
//                detail2.komponen_id as komponen_id2,
//                detail2.detail_name as detail_name2,
//                detail2.komponen_name as komponen_name2,
//                detail2.volume as volume2,
//                detail2.subtitle as subtitle_name2,
//                detail2.satuan as detail_satuan2,
//                replace(detail2.keterangan_koefisien,'<*>','X') as keterangan_koefisien2,
//                detail2.detail_no as detail_no2,
//                detail2.volume * detail2.komponen_harga_awal as hasil2,
//                (detail2.nilai_anggaran) as hasil_kali2,
//
//                detail.komponen_name || ' ' || detail.detail_name as detail_name2,
//                detail.komponen_harga_awal as detail_harga,
//                detail.pajak as pajak,
//                detail.komponen_id as komponen_id,
//                detail.detail_name as detail_name,
//                detail.komponen_name as komponen_name,
//                detail.volume as volume,
//                detail.subtitle as subtitle_name,
//                detail.satuan as detail_satuan,
//                replace(detail.keterangan_koefisien,'<*>','X') as keterangan_koefisien,
//                detail.detail_no as detail_no,
//                detail.volume * detail.komponen_harga_awal as hasil,
//                (detail.nilai_anggaran) as hasil_kali,
//                detail2.detail_name as detail_name_rd,
//                detail2.sub as sub2,
//                detail.sub,
//                detail2.note_skpd as note_skpd,
//                detail2.note_peneliti as note_peneliti,
//                detail2.status_level as status_level
//
//
//            FROM
//                " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail detail2, " . sfConfig::get('app_default_schema') . ".rekening rekening, " . sfConfig::get('app_default_schema') . ".$tabel_semula detail
//
//            WHERE
//                detail2.kegiatan_code = '$kode_kegiatan' and
//                detail2.unit_id='$unit_id'
//                and detail2.kegiatan_code=detail.kegiatan_code
//                and detail2.unit_id=detail.unit_id
//                and detail2.subtitle=detail.subtitle
//                and detail2.rekening_code=detail.rekening_code
//                and rekening.rekening_code=detail2.rekening_code
//                and detail2.detail_no=detail.detail_no
//                and detail.status_hapus<>true
//                and detail2.status_hapus<>true
//                and detail2.status_level>=4
//                and (detail2.nilai_anggaran <> detail.nilai_anggaran
//                or detail2.rekening_code <> detail.rekening_code 
//                or detail2.subtitle<>detail.subtitle
//                or detail2.komponen_name <> detail.komponen_name
//                or detail2.keterangan_koefisien <> detail.keterangan_koefisien
//                or detail2.detail_name <> detail.detail_name)
//
//
//            ORDER BY
//
//                rekening.rekening_code,
//                detail2.subtitle ,
//                detail2.komponen_name
//            )
//            union
//            (
//            SELECT
//                rekening.rekening_code,rekening.rekening_code as rekening_code2,
//                rekening.rekening_name as nama_rekening2,
//                detail2.komponen_name || ' ' || detail2.detail_name as detail_name22,
//                detail2.komponen_harga_awal as detail_harga2,
//                detail2.pajak as pajak2,
//                detail2.komponen_id as komponen_id2,
//                detail2.detail_name as detail_name2,
//                detail2.komponen_name as komponen_name2,
//                detail2.volume as volume2,
//                detail2.subtitle as subtitle_name2,
//                detail2.satuan as detail_satuan2,
//                replace(detail2.keterangan_koefisien,'<*>','X') as keterangan_koefisien2,
//                detail2.detail_no as detail_no2,
//                detail2.volume * detail2.komponen_harga_awal as hasil2,
//                (detail2.nilai_anggaran) as hasil_kali2,
//
//                '' as detail_name,
//                0 as detail_harga,
//                0 as pajak,
//                '' as komponen_id,
//                '' as detail_name,
//                '' as komponen_name,
//                0 as volume,
//                '' as subtitle_name,
//                '' as detail_satuan,
//                '' as keterangan_koefisien,
//                0 as detail_no,
//                0 as hasil,
//                0 as hasil_kali,
//                detail2.detail_name as detail_name_rd,
//                detail2.sub as sub2,
//                '' as sub,
//                detail2.note_skpd as note_skpd,
//                detail2.note_peneliti as note_peneliti,
//                detail2.status_level as status_level
//
//
//            FROM
//                " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail detail2, " . sfConfig::get('app_default_schema') . ".rekening rekening, " . sfConfig::get('app_default_schema') . ".$tabel_semula detail
//
//            WHERE
//                detail2.kegiatan_code = '$kode_kegiatan' and
//                detail2.unit_id='$unit_id'
//                and detail2.kegiatan_code=detail.kegiatan_code
//                and detail2.unit_id=detail.unit_id
//                and detail2.subtitle=detail.subtitle
//                and detail2.rekening_code=detail.rekening_code
//                and rekening.rekening_code=detail2.rekening_code
//                and detail2.detail_no=detail.detail_no
//                and detail.status_hapus=true
//                and detail2.status_hapus<>true
//                and detail2.status_level>=4
//                and (detail2.nilai_anggaran <> detail.nilai_anggaran
//                or detail2.rekening_code <> detail.rekening_code 
//                or detail2.subtitle<>detail.subtitle
//                or detail2.komponen_name <> detail.komponen_name
//                or detail2.keterangan_koefisien <> detail.keterangan_koefisien
//                or detail2.detail_name <> detail.detail_name)
//
//
//            ORDER BY
//
//                rekening.rekening_code,
//                detail2.subtitle ,
//                detail2.komponen_name
//            )
//            union
//            (
//            SELECT
//                rekening.rekening_code,rekening.rekening_code as rekening_code2,
//                rekening.rekening_name as nama_rekening2,
//                detail2.komponen_name || ' ' || detail2.detail_name as detail_name22,
//                detail2.komponen_harga_awal as detail_harga2,
//                detail2.pajak as pajak2,
//                detail2.komponen_id as komponen_id2,
//                detail2.detail_name as detail_name2,
//                detail2.komponen_name as komponen_name2,
//                detail2.volume as volume2,
//                detail2.subtitle as subtitle_name2,
//                detail2.satuan as detail_satuan2,
//                replace(detail2.keterangan_koefisien,'<*>','X') as keterangan_koefisien2,
//                detail2.detail_no as detail_no2,
//                detail2.volume * detail2.komponen_harga_awal as hasil2,
//                (detail2.nilai_anggaran) as hasil_kali2,
//
//                '' as detail_name,
//                0 as detail_harga,
//                0 as pajak,
//                '' as komponen_id,
//                '' as detail_name,
//                '' as komponen_name,
//                0 as volume,
//                '' as subtitle_name,
//                '' as detail_satuan,
//                '' as keterangan_koefisien,
//                0 as detail_no,
//                0 as hasil,
//                0 as hasil_kali,
//                detail2.detail_name as detail_name_rd,
//                detail2.sub as sub2,
//                '' as sub,
//                detail2.note_skpd as note_skpd,
//                detail2.note_peneliti as note_peneliti,
//                detail2.status_level as status_level
//
//
//            FROM
//                " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail detail2, " . sfConfig::get('app_default_schema') . ".rekening rekening
//
//            WHERE
//                detail2.kegiatan_code = '$kode_kegiatan' and
//                detail2.unit_id='$unit_id'
//
//                and detail2.status_hapus=false
//                and rekening.rekening_code=detail2.rekening_code
//                and detail2.status_level>=4
//                and detail2.nilai_anggaran>0
//                and (detail2.unit_id||detail2.kegiatan_code||detail2.detail_no) not in
//                (
//                select (detail.unit_id||detail.kegiatan_code||detail.detail_no) from " . sfConfig::get('app_default_schema') . ".$tabel_semula detail 
//                where detail.kegiatan_code = '$kode_kegiatan' and
//                detail.unit_id='$unit_id'
//                )
//
//            ORDER BY
//
//                rekening.rekening_code,
//                detail2.subtitle ,
//                detail2.komponen_name)
//            union
//            (
//            SELECT
//                rekening.rekening_code,rekening.rekening_code as rekening_code2,
//                rekening.rekening_name as nama_rekening2,
//                '' as detail_name22,
//                0 as detail_harga2,
//                0 as pajak2,
//                '' as komponen_id2,
//                '' as detail_name2,
//                '' as komponen_name2,
//                0 as volume2,
//                '' as subtitle_name2,
//                '' as detail_satuan2,
//                '' as keterangan_koefisien2,
//                detail.detail_no as detail_no2,
//                0 as hasil2,
//                0 as hasil_kali2,
//
//                detail.komponen_name || ' ' || detail.detail_name as detail_name2,
//                detail.komponen_harga_awal as detail_harga,
//                detail.pajak as pajak,
//                detail.komponen_id as komponen_id,
//                detail.detail_name as detail_name,
//                detail.komponen_name as komponen_name,
//                detail.volume as volume,
//                detail.subtitle as subtitle_name,
//                detail.satuan as detail_satuan,
//                replace(detail.keterangan_koefisien,'<*>','X') as keterangan_koefisien,
//                detail.detail_no as detail_no,
//                detail.volume * detail.komponen_harga_awal as hasil,
//                (detail.nilai_anggaran) as hasil_kali,
//                '' as detail_name_rd,
//                '' as sub2,
//                detail.sub,
//                '' as note_skpd,
//                '' as note_peneliti,
//                0 as status_level
//
//
//            FROM
//                " . sfConfig::get('app_default_schema') . ".$tabel_semula detail, " . sfConfig::get('app_default_schema') . ".rekening rekening
//
//            WHERE
//                detail.kegiatan_code = '$kode_kegiatan' and
//                detail.unit_id='$unit_id'
//
//                and detail.status_hapus=false
//                and rekening.rekening_code=detail.rekening_code
//                and (detail.unit_id||detail.kegiatan_code||detail.detail_no) not in
//                (
//                select (detail2.unit_id||detail2.kegiatan_code||detail2.detail_no) from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail detail2 
//                where detail2.kegiatan_code = '$kode_kegiatan' and
//                detail2.unit_id='$unit_id' and detail2.status_hapus<>true
//                )
//
//            ORDER BY
//
//                rekening.rekening_code,
//                detail.subtitle ,
//                detail.komponen_name)
//            ORDER BY sub2 ASC, sub DESC";

            $where_anggaran = " and detail2.nilai_anggaran>0 ";
            $where_anggaran2 = " ";
            $c_kegiatan_anggaran = new Criteria();
            $c_kegiatan_anggaran->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
            $c_kegiatan_anggaran->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
            $c_kegiatan_anggaran->add(DinasMasterKegiatanPeer::IS_PERNAH_RKA, TRUE);
            if (DinasMasterKegiatanPeer::doSelectOne($c_kegiatan_anggaran)) {
                $where_anggaran = "";
                $where_anggaran2 = "or detail2.nilai_anggaran<>(select nilai_anggaran from " . sfConfig::get('app_default_schema') . ".rincian_detail where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and detail_no=detail2.detail_no) ";
            }
            $query = "(
            SELECT
                    detail.rekening_code,detail2.rekening_code as rekening_code2,
                    rekening.rekening_name as nama_rekening2,
                    detail2.komponen_name || ' ' || detail2.detail_name as detail_name22,
                    detail2.komponen_harga_awal as detail_harga2,
                    detail2.pajak as pajak2,
                    detail2.komponen_id as komponen_id2,
                    detail2.detail_name as detail_name2,
                    detail2.komponen_name as komponen_name2,
                    detail2.volume as volume2,
                    detail2.subtitle as subtitle_name2,
                    detail2.satuan as detail_satuan2,
                    replace(detail2.keterangan_koefisien,'<*>','X') as keterangan_koefisien2,
                    detail2.detail_no as detail_no2,
                    detail2.volume * detail2.komponen_harga_awal as hasil2,
                    (detail2.nilai_anggaran) as hasil_kali2,

                    detail.komponen_name || ' ' || detail.detail_name as detail_name2,
                    detail.komponen_harga_awal as detail_harga,
                    detail.pajak as pajak,
                    detail.komponen_id as komponen_id,
                    detail.detail_name as detail_name,
                    detail.komponen_name as komponen_name,
                    detail.volume as volume,
                    detail.subtitle as subtitle_name,
                    detail.satuan as detail_satuan,
                    replace(detail.keterangan_koefisien,'<*>','X') as keterangan_koefisien,
                    detail.detail_no as detail_no,
                    detail.volume * detail.komponen_harga_awal as hasil,
                    (detail.nilai_anggaran) as hasil_kali,
                    detail2.detail_name as detail_name_rd,
                    detail2.sub as sub2,
                    detail.sub,
                    detail2.note_skpd as note_skpd,
                    detail2.note_peneliti as note_peneliti,
                    detail2.note_tapd as note_tapd,
                    detail2.note_bappeko as note_bappeko,
                    detail2.status_level as status_level,
                    detail2.status_level_tolak,
                    detail2.status_sisipan,
                    detail2.status_lelang,
                    detail2.is_tapd_setuju,
                    detail2.is_bappeko_setuju,
                    detail2.is_penyelia_setuju,
                    detail.sumber_dana_id,
                    detail2.sumber_dana_id as sumber_dana_id2,
                    detail.is_covid,
                    detail2.is_covid as is_covid2


            FROM
                    " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail detail2, " . sfConfig::get('app_default_schema') . ".rekening rekening, " . sfConfig::get('app_default_schema') . ".$tabel_semula detail

            WHERE
                    detail2.kegiatan_code = '$kode_kegiatan' and
                    detail2.unit_id='$unit_id'
                    and detail2.kegiatan_code=detail.kegiatan_code
                    and detail2.unit_id=detail.unit_id
                    and rekening.rekening_code=detail2.rekening_code
                    and detail2.detail_no=detail.detail_no
                    and detail.status_hapus<>true
                    and detail2.status_hapus<>true
                    and detail2.status_level >= 4
                    and (detail2.nilai_anggaran <> detail.nilai_anggaran
                    or detail2.rekening_code <> detail.rekening_code 
                    or detail2.subtitle<>detail.subtitle
                    or detail2.komponen_name <> detail.komponen_name
                    or detail2.keterangan_koefisien <> detail.keterangan_koefisien
                    or detail2.detail_name <> detail.detail_name
                    $where_anggaran2)


            ORDER BY

                    rekening.rekening_code,
                    detail2.subtitle ,
                    detail2.komponen_name
            )
            union
            (
            SELECT
                    rekening.rekening_code,rekening.rekening_code as rekening_code2,
                    rekening.rekening_name as nama_rekening2,
                    detail2.komponen_name || ' ' || detail2.detail_name as detail_name22,
                    detail2.komponen_harga_awal as detail_harga2,
                    detail2.pajak as pajak2,
                    detail2.komponen_id as komponen_id2,
                    detail2.detail_name as detail_name2,
                    detail2.komponen_name as komponen_name2,
                    detail2.volume as volume2,
                    detail2.subtitle as subtitle_name2,
                    detail2.satuan as detail_satuan2,
                    replace(detail2.keterangan_koefisien,'<*>','X') as keterangan_koefisien2,
                    detail2.detail_no as detail_no2,
                    detail2.volume * detail2.komponen_harga_awal as hasil2,
                    (detail2.nilai_anggaran) as hasil_kali2,

                    '' as detail_name,
                    0 as detail_harga,
                    0 as pajak,
                    '' as komponen_id,
                    '' as detail_name,
                    '' as komponen_name,
                    0 as volume,
                    '' as subtitle_name,
                    '' as detail_satuan,
                    '' as keterangan_koefisien,
                    0 as detail_no,
                    0 as hasil,
                    0 as hasil_kali,
                    detail2.detail_name as detail_name_rd,
                    detail2.sub as sub2,
                    '' as sub,
                    detail2.note_skpd as note_skpd,
                    detail2.note_peneliti as note_peneliti,
                    detail2.note_tapd as note_tapd,
                    detail2.note_bappeko as note_bappeko,
                    detail2.status_level as status_level,
                    detail2.status_level_tolak,
                    detail2.status_sisipan,
                    detail2.status_lelang,
                    detail2.is_tapd_setuju,
                    detail2.is_bappeko_setuju,
                    detail2.is_penyelia_setuju,
                    11 as sumber_dana_id,
                    detail2.sumber_dana_id as sumber_dana_id2,
                    false as is_covid,
                    detail2.is_covid as is_covid2


            FROM
                    " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail detail2, " . sfConfig::get('app_default_schema') . ".rekening rekening, " . sfConfig::get('app_default_schema') . ".$tabel_semula detail

            WHERE
                    detail2.kegiatan_code = '$kode_kegiatan' and
                    detail2.unit_id='$unit_id'
                    and detail2.kegiatan_code=detail.kegiatan_code
                    and detail2.unit_id=detail.unit_id
                    and detail2.rekening_code=detail.rekening_code
                    and rekening.rekening_code=detail2.rekening_code
                    and detail2.detail_no=detail.detail_no
                    and detail.status_hapus=true
                    and detail2.status_hapus<>true
                    and detail2.status_level >= 4
                    and detail2.tahap='$tahap'
                    and (detail2.nilai_anggaran <> detail.nilai_anggaran
                    or detail2.rekening_code <> detail.rekening_code 
                    or detail2.subtitle<>detail.subtitle
                    or detail2.komponen_name <> detail.komponen_name
                    or detail2.keterangan_koefisien <> detail.keterangan_koefisien
                    or detail2.detail_name <> detail.detail_name
                    $where_anggaran2)


            ORDER BY

                    rekening.rekening_code,
                    detail2.subtitle ,
                    detail2.komponen_name
            )
            union
            (
            SELECT

                    rekening.rekening_code,rekening.rekening_code as rekening_code2,
                    rekening.rekening_name as nama_rekening2,
                    '' as detail_name22,
                    0 as detail_harga2,
                    0 as pajak2,
                    '' as komponen_id2,
                    '' as detail_name2,
                    '' as komponen_name2,
                    0 as volume2,
                    '' as subtitle_name2,
                    '' as detail_satuan2,
                    '' as keterangan_koefisien2,
                    detail.detail_no as detail_no2,
                    0 as hasil2,
                    0 as hasil_kali2,

                    detail.komponen_name || ' ' || detail.detail_name as detail_name2,
                    detail.komponen_harga_awal as detail_harga,
                    detail.pajak as pajak,
                    detail.komponen_id as komponen_id,
                    detail.detail_name as detail_name,
                    detail.komponen_name as komponen_name,
                    detail.volume as volume,
                    detail.subtitle as subtitle_name,
                    detail.satuan as detail_satuan,
                    replace(detail.keterangan_koefisien,'<*>','X') as keterangan_koefisien,
                    detail.detail_no as detail_no,
                    detail.volume * detail.komponen_harga_awal as hasil,
                    (detail.nilai_anggaran) as hasil_kali,
                    detail.detail_name as detail_name_rd,
                    '' as sub2,
                    detail.sub,
                    detail2.note_skpd as note_skpd,
                    detail2.note_peneliti as note_peneliti,
                    detail2.note_tapd as note_tapd,
                    detail2.note_bappeko as note_bappeko,
                    detail2.status_level as status_level,
                    detail2.status_level_tolak,
                    detail2.status_sisipan,
                    detail2.status_lelang,
                    detail2.is_tapd_setuju,
                    detail2.is_bappeko_setuju,
                    detail2.is_penyelia_setuju,
                    11 as sumber_dana_id,
                    detail2.sumber_dana_id as sumber_dana_id2,
                    false as is_covid,
                    detail2.is_covid as is_covid2


            FROM
                    " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail detail2, " . sfConfig::get('app_default_schema') . ".rekening rekening, " . sfConfig::get('app_default_schema') . ".$tabel_semula detail

            WHERE
                    detail2.kegiatan_code = '$kode_kegiatan' and
                    detail2.unit_id='$unit_id'

                    and detail2.kegiatan_code=detail.kegiatan_code
                    and detail2.unit_id=detail.unit_id
                    and detail2.rekening_code=detail.rekening_code
                    and rekening.rekening_code=detail2.rekening_code
                    and detail2.detail_no=detail.detail_no
                    and detail.status_hapus<>true
                    and detail2.status_hapus=true
                    and detail2.status_level >= 4
                    and detail2.tahap='$tahap'
                    and (detail2.nilai_anggaran <> detail.nilai_anggaran
                    or detail2.rekening_code <> detail.rekening_code 
                    or detail2.subtitle<>detail.subtitle
                    or detail2.komponen_name <> detail.komponen_name
                    or detail2.keterangan_koefisien <> detail.keterangan_koefisien
                    or detail2.detail_name <> detail.detail_name
                    $where_anggaran2)


            ORDER BY

                    rekening.rekening_code,
                    detail2.subtitle ,
                    detail2.komponen_name
            )
            union
            (
            SELECT

                    rekening.rekening_code,rekening.rekening_code as rekening_code2,
                    rekening.rekening_name as nama_rekening2,
                    detail2.komponen_name || ' ' || detail2.detail_name as detail_name22,
                    detail2.komponen_harga_awal as detail_harga2,
                    detail2.pajak as pajak2,
                    detail2.komponen_id as komponen_id2,
                    detail2.detail_name as detail_name2,
                    detail2.komponen_name as komponen_name2,
                    detail2.volume as volume2,
                    detail2.subtitle as subtitle_name2,
                    detail2.satuan as detail_satuan2,
                    replace(detail2.keterangan_koefisien,'<*>','X') as keterangan_koefisien2,
                    detail2.detail_no as detail_no2,
                    detail2.volume * detail2.komponen_harga_awal as hasil2,
                    (detail2.nilai_anggaran) as hasil_kali2,

                    '' as detail_name,
                    0 as detail_harga,
                    0 as pajak,
                    '' as komponen_id,
                    '' as detail_name,
                    '' as komponen_name,
                    0 as volume,
                    '' as subtitle_name,
                    '' as detail_satuan,
                    '' as keterangan_koefisien,
                    0 as detail_no,
                    0 as hasil,
                    0 as hasil_kali,
                    detail2.detail_name as detail_name_rd,
                    detail2.sub as sub2,
                    '' as sub,
                    detail2.note_skpd as note_skpd,
                    detail2.note_peneliti as note_peneliti,
                    detail2.note_tapd as note_tapd,
                    detail2.note_bappeko as note_bappeko,
                    detail2.status_level as status_level,
                    detail2.status_level_tolak,
                    detail2.status_sisipan,
                    detail2.status_lelang,
                    detail2.is_tapd_setuju,
                    detail2.is_bappeko_setuju,
                    detail2.is_penyelia_setuju,
                    11 as sumber_dana_id,
                    detail2.sumber_dana_id as sumber_dana_id2,
                    false as is_covid,
                    detail2.is_covid as is_covid2


            FROM
                    " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail detail2, " . sfConfig::get('app_default_schema') . ".rekening rekening

            WHERE
                    detail2.kegiatan_code = '$kode_kegiatan' and
                    detail2.unit_id='$unit_id'

                    and detail2.status_hapus=false
                    and rekening.rekening_code=detail2.rekening_code
                    and detail2.status_level >= 4

                    and (detail2.unit_id||detail2.kegiatan_code||detail2.detail_no) not in
                    (
                    select (detail.unit_id||detail.kegiatan_code||detail.detail_no) from " . sfConfig::get('app_default_schema') . ".$tabel_semula detail where detail.kegiatan_code = '$kode_kegiatan' and
                    detail.unit_id='$unit_id'
                    )

            ORDER BY

                    rekening.rekening_code,
                    detail2.subtitle ,
                    detail2.komponen_name)
            union
            (
            SELECT
                    rekening.rekening_code,rekening.rekening_code as rekening_code2,
                    rekening.rekening_name as nama_rekening2,
                    '' as detail_name22,
                    0 as detail_harga2,
                    0 as pajak2,
                    '' as komponen_id2,
                    '' as detail_name2,
                    '' as komponen_name2,
                    0 as volume2,
                    '' as subtitle_name2,
                    '' as detail_satuan2,
                    '' as keterangan_koefisien2,
                    detail.detail_no as detail_no2,
                    0 as hasil2,
                    0 as hasil_kali2,

                    detail.komponen_name || ' ' || detail.detail_name as detail_name2,
                    detail.komponen_harga_awal as detail_harga,
                    detail.pajak as pajak,
                    detail.komponen_id as komponen_id,
                    detail.detail_name as detail_name,
                    detail.komponen_name as komponen_name,
                    detail.volume as volume,
                    detail.subtitle as subtitle_name,
                    detail.satuan as detail_satuan,
                    replace(detail.keterangan_koefisien,'<*>','X') as keterangan_koefisien,
                    detail.detail_no as detail_no,
                    detail.volume * detail.komponen_harga_awal as hasil,
                    (detail.nilai_anggaran) as hasil_kali,
                    '' as detail_name_rd,
                    '' as sub2,
                    detail.sub,
                    '' as note_skpd,
                    '' as note_peneliti,
                    '' as note_tapd,
                    '' as note_bappeko,
                    null as status_level,
                    null,
                    false,
                    null,
                    null,
                    null,
                    null,
                    11 as sumber_dana_id,
                    11 as sumber_dana_id2,
                    false as is_covid,                    
                    false as is_covid2


            FROM
                    " . sfConfig::get('app_default_schema') . ".$tabel_semula detail, " . sfConfig::get('app_default_schema') . ".rekening rekening

            WHERE
                    detail.kegiatan_code = '$kode_kegiatan' and
                    detail.unit_id='$unit_id'

                    and detail.status_hapus=false
                    and rekening.rekening_code=detail.rekening_code
                    and (detail.unit_id||detail.kegiatan_code||detail.detail_no) not in
                    (
                    select (detail2.unit_id||detail2.kegiatan_code||detail2.detail_no) from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail detail2 where detail2.kegiatan_code = '$kode_kegiatan' and
                    detail2.unit_id='$unit_id' and detail2.status_hapus<>true
                    )

            ORDER BY

                    rekening.rekening_code,
                    detail.subtitle ,
                    detail.komponen_name)
            ORDER BY sub2 ASC, sub ASC, detail_name22 ASC";

            $stmt = $con->prepareStatement($query);
            $rs = $stmt->executeQuery();
            while ($rs->next()) {

                //untuk masukkan data sumber dana dan taggng is_covid dian 160621
                $c = new Criteria();
                $c->add(MasterSumberDanaPeer::ID, $rs->getString('sumber_dana_id'));
                $c->addAscendingOrderByColumn(MasterSumberDanaPeer::SUMBER_DANA);
                $v = MasterSumberDanaPeer::doSelectOne($c);
                if($v->getSumberDana() == APBD)
                {
                    $sumber_dana_id_semula= '';
                }
                else
                {
                    $sumber_dana_id_semula= '('.$v->getSumberDana().')';
                }

                $c1 = new Criteria();
                $c1->add(MasterSumberDanaPeer::ID, $rs->getString('sumber_dana_id2'));
                $c1->addAscendingOrderByColumn(MasterSumberDanaPeer::SUMBER_DANA);
                $v1 = MasterSumberDanaPeer::doSelectOne($c1);
                if($v1->getSumberDana() == APBD)
                {
                    $sumber_dana_id_menjadi= '';
                }
                else
                {
                    $sumber_dana_id_menjadi= '('.$v1->getSumberDana().')';
                }

                if($rs->getString('is_covid') == t)
                {
                    $is_covid_semula='(COVID-19)';
                }
                else
                {
                    $is_covid_semula='';
                }

                 if($rs->getString('is_covid2') == t)
                {
                    $is_covid_menjadi='(COVID-19)';
                }
                else
                {
                    $is_covid_menjadi='';
                }
                $detail_ba = new BeritaAcaraDetail();
                $detail_ba->setIdBeritaAcara($id_baru);
                $detail_ba->setSubtitle($rs->getString('subtitle_name2'));
                $detail_ba->setDetailNo($rs->getString('detail_no2'));
                $detail_ba->setRekeningCode($rs->getString('rekening_code2'));
                $detail_ba->setRekeningCodeSemula($rs->getString('rekening_code'));
                if ($rs->getString('sub') && $rs->getString('komponen_name')) {
                    $subnya = '[' . $rs->getString('sub') . '] ';
                }
                if ($rs->getString('sub2') && $rs->getString('komponen_name2')) {
                    $subnya2 = '[' . $rs->getString('sub2') . '] ';
                }
                
                // $detail_ba->setSemulaKomponen($subnya . $rs->getString('komponen_name') . ' ' . $rs->getString('detail_name').' '.$sumber_dana_id_semula.''.$is_covid_semula);
                // $detail_ba->setMenjadiKomponen($subnya2 . $rs->getString('komponen_name2') . ' ' . $rs->getString('detail_name_rd').' '.$sumber_dana_id_menjadi.''.$is_covid_menjadi);
                $detail_ba->setSemulaKomponen($rs->getString('komponen_name') . ' ' . $rs->getString('detail_name').' '.$sumber_dana_id_semula.''.$is_covid_semula);
                $detail_ba->setMenjadiKomponen($rs->getString('komponen_name2') . ' ' . $rs->getString('detail_name_rd').' '.$sumber_dana_id_menjadi.''.$is_covid_menjadi);
                $detail_ba->setSemulaSatuan($rs->getString('detail_satuan'));
                $detail_ba->setMenjadiSatuan($rs->getString('detail_satuan2'));
                $detail_ba->setSemulaKoefisien($rs->getString('keterangan_koefisien'));
                $detail_ba->setMenjadiKoefisien($rs->getString('keterangan_koefisien2'));

                $detail_ba->setSemulaHarga($rs->getString('detail_harga'));
                $detail_ba->setMenjadiHarga($rs->getString('detail_harga2'));

                $hasil1 = $rs->getString('detail_harga') * $rs->getString('volume');
                $hasil2 = $rs->getString('detail_harga2') * $rs->getString('volume2');
                $detail_ba->setSemulaHasil($hasil1);
                $detail_ba->setMenjadiHasil($hasil2);

                $detail_ba->setSemulaPajak($rs->getString('pajak'));
                $detail_ba->setMenjadiPajak($rs->getString('pajak2'));

//                $total1 = $hasil1 + (($rs->getString('pajak') / 100) * $hasil1);
//                $total2 = $hasil2 + (($rs->getString('pajak2') / 100) * $hasil2);
                $detail_ba->setSemulaTotal($rs->getString('hasil_kali'));
                $detail_ba->setMenjadiTotal($rs->getString('hasil_kali2'));
                $detail_ba->setCatatan($rs->getString('note_skpd'));
                $detail_ba->setBelanja(substr($rs->getString('rekening_code'), 0, 6));
                $detail_ba->save();
            }
            $con->commit();
            return TRUE;
        } catch (Exception $ex) {
            //echo $ex->getMessage(); exit;
            //echo $ex->getMessage(); exit;
            $con->rollback();
            return FALSE;
        }
    }

}
