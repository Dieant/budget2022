<?php

/**
 * Subclass for performing query and update operations on the 'ebudget.dinas_master_kegiatan' table.
 *
 * 
 *
 * @package lib.model.budgeting
 */
class DinasMasterKegiatanPeer extends BaseDinasMasterKegiatanPeer {

    public static function ubahTahapAngka($tahap) {
        if ($tahap == 'murni' || $tahap == 'murni_bukuputih' || $tahap == 'murni_bukubiru') {
            return 0;
        } elseif ($tahap == 'revisi1' || $tahap == 'revisi1_1') {
            return 1;
        } elseif ($tahap == 'revisi2' || $tahap == 'revisi2_1' || $tahap == 'revisi2_2') {
            return 2;
        } elseif ($tahap == 'revisi3_0') {
            return 30;
        } elseif ($tahap == 'revisi3_1') {
            return 31;
        } elseif ($tahap == 'revisi3' || $tahap == 'revisi3_2') {
            return 3;
        } elseif ($tahap == 'revisi4' || $tahap == 'revisi4_1') {
            return 4;
        } elseif ($tahap == 'revisi5') {
            return 5;
        } elseif ($tahap == 'revisi6') {
            return 6;
        } elseif ($tahap == 'revisi7') {
            return 7;
        } elseif ($tahap == 'revisi8') {
            return 8;
        } elseif ($tahap == 'revisi9') {
            return 9;
        } elseif ($tahap == 'revisi10') {
            return 10;
        } elseif ($tahap == 'revisi11') {
            return 11;
        } elseif ($tahap == 'revisi12') {
            return 12;
        } elseif ($tahap == 'revisi13') {
            return 13;
        } elseif ($tahap == 'revisi14') {
            return 14;
        } elseif ($tahap == 'revisi15') {
            return 15;
        } elseif ($tahap == 'revisi16') {
            return 16;
        } elseif ($tahap == 'revisi17') {
            return 17;
        } elseif ($tahap == 'revisi18') {
            return 18;
        } elseif ($tahap == 'revisi19') {
            return 19;
        } elseif ($tahap == 'revisi20') {
            return 20;
        } elseif ($tahap == 'revisi21') {
            return 21;
        } elseif ($tahap == 'revisi22') {
            return 22;
        } elseif ($tahap == 'revisi23') {
            return 23;
        } elseif ($tahap == 'revisi24') {
            return 24;
        } elseif ($tahap == 'pak' || $tahap == 'pak_bukuputih' || $tahap == 'pak_bukubiru') {
            return 100;
        }
        return -1;
    }

    public static function getCatatanPembahasanKegiatan($unit_id, $kode_kegiatan) {

        $c_tahap = new Criteria();
        $c_tahap->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
        $c_tahap->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
        if ($tahap_kegiatan = DinasMasterKegiatanPeer::doSelectOne($c_tahap)) {

             return $tahap_kegiatan->getCatatanPembahasan();
        }
        return 0;

     }

    public static function getTahapKegiatan($unit_id, $kode_kegiatan) {
        $c_tahap = new Criteria();
        $c_tahap->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
        $c_tahap->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
        if ($tahap_kegiatan = DinasMasterKegiatanPeer::doSelectOne($c_tahap)) {
            if ($tahap_kegiatan->getTahap() == 'murni' || $tahap_kegiatan->getTahap() == 'murni_bukuputih' || $tahap_kegiatan->getTahap() == 'murni_bukubiru') {
                // echo $tahap_kegiatan->getTahap();die();
                return 0;
            } elseif ($tahap_kegiatan->getTahap() == 'revisi1' || $tahap_kegiatan->getTahap() == 'revisi1_1') {
                return 1;
            } elseif ($tahap_kegiatan->getTahap() == 'revisi2' || $tahap_kegiatan->getTahap() == 'revisi2_1' || $tahap_kegiatan->getTahap() == 'revisi2_2') {
                return 2;
            } elseif ($tahap_kegiatan->getTahap() == 'revisi3' || $tahap_kegiatan->getTahap() == 'revisi3_1') {
                return 3;
            } elseif ($tahap_kegiatan->getTahap() == 'revisi4' || $tahap_kegiatan->getTahap() == 'revisi4_1') {
                return 4;
            } elseif ($tahap_kegiatan->getTahap() == 'revisi5') {
                return 5;
            } elseif ($tahap_kegiatan->getTahap() == 'revisi6') {
                return 6;
            } elseif ($tahap_kegiatan->getTahap() == 'revisi7') {
                return 7;
            } elseif ($tahap_kegiatan->getTahap() == 'revisi8') {
                return 8;
            } elseif ($tahap_kegiatan->getTahap() == 'revisi9') {
                return 9;
            } elseif ($tahap_kegiatan->getTahap() == 'revisi10') {
                return 10;
            } elseif ($tahap_kegiatan->getTahap() == 'revisi11') {
                return 11;
            } elseif ($tahap_kegiatan->getTahap() == 'revisi12') {
                return 12;
            } elseif ($tahap_kegiatan->getTahap() == 'revisi13') {
                return 13;
            } elseif ($tahap_kegiatan->getTahap() == 'revisi14') {
                return 14;
            } elseif ($tahap_kegiatan->getTahap() == 'revisi15') {
                return 15;
            } elseif ($tahap_kegiatan->getTahap() == 'revisi16') {
                return 16;
            } elseif ($tahap_kegiatan->getTahap() == 'revisi17') {
                return 17;
            } elseif ($tahap_kegiatan->getTahap() == 'revisi18') {
                return 18;
            } elseif ($tahap_kegiatan->getTahap() == 'revisi19') {
                return 19;
            } elseif ($tahap_kegiatan->getTahap() == 'revisi20') {
                return 20;
            } elseif ($tahap_kegiatan->getTahap() == 'revisi21') {
                return 21;
            } elseif ($tahap_kegiatan->getTahap() == 'revisi22') {
                return 22;
            } elseif ($tahap_kegiatan->getTahap() == 'revisi23') {
                return 23;
            } elseif ($tahap_kegiatan->getTahap() == 'revisi24') {
                return 24;
            } elseif ($tahap_kegiatan->getTahap() == 'pak' || $tahap_kegiatan->getTahap() == 'pak_bukuputih' || $tahap_kegiatan->getTahap() == 'pak_bukubiru') {
                return 100;
            } 
        }
        return 0;
    }

    public static function getTahapDetail($unit_id, $kode_kegiatan) {
        $c_tahap = new Criteria();
        $c_tahap->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
        $c_tahap->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
        if ($tahap_kegiatan = DinasMasterKegiatanPeer::doSelectOne($c_tahap)) {
            return $tahap_kegiatan->getTahap();
        }

        return '';
    }

}
