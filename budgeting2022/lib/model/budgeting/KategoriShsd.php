<?php

/**
 * Subclass for representing a row from the 'budget2010.kategori_shsd' table.
 *
 * 
 *
 * @package lib.model.budget2010
 */ 
class KategoriShsd extends BaseKategoriShsd
{
    public function getKategoriShsdId() 
    {
        $q1 = "select kategori_shsd_id,kategori_shsd_name
            from ". sfConfig::get('app_default_schema').".kategori_shsd";
        $con=Propel::getConnection();
        $statement=$con->prepareStatement($q1);
        $rs_nilai = $statement->executeQuery();
//        $rs_nilai->next();
        while($rs_nilai->next())
        {
                $kategori_id = $rs_nilai->getString('kategori_shsd_id');
        }
        return $kategori_id;
        
    }
}
