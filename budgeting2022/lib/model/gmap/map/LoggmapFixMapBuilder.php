<?php



class LoggmapFixMapBuilder {

	
	const CLASS_NAME = 'lib.model.gmap.map.LoggmapFixMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('gmap');

		$tMap = $this->dbMap->addTable('loggmap_fix');
		$tMap->setPhpName('LoggmapFix');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('IDLOG_SERIAL', 'IdlogSerial', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('IDGEOLOKASI', 'Idgeolokasi', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('NMUSER', 'Nmuser', 'string', CreoleTypes::VARCHAR, false, 50);

		$tMap->addColumn('DATALAMA', 'Datalama', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('DATABARU', 'Databaru', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('TANGGAL', 'Tanggal', 'int', CreoleTypes::TIMESTAMP, false, null);

		$tMap->addColumn('STATUS', 'Status', 'int', CreoleTypes::SMALLINT, false, null);

		$tMap->addColumn('IPADDRESS', 'Ipaddress', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('FORWARDEDIPADDRESS', 'Forwardedipaddress', 'string', CreoleTypes::LONGVARCHAR, false, null);

	} 
} 