<?php



class GeodataRoadMapBuilder {

	
	const CLASS_NAME = 'lib.model.gmap.map.GeodataRoadMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('gmap');

		$tMap = $this->dbMap->addTable('geodata_road');
		$tMap->setPhpName('GeodataRoad');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('NAMA', 'Nama', 'string', CreoleTypes::VARCHAR, false, 500);

		$tMap->addColumn('LATLONG', 'Latlong', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('TIPE', 'Tipe', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('GEOJSON', 'Geojson', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('TANGGAL', 'Tanggal', 'int', CreoleTypes::TIMESTAMP, false, null);

		$tMap->addColumn('DELETEFLAG', 'Deleteflag', 'int', CreoleTypes::INTEGER, false, null);

	} 
} 