<?php



class GeojsonlokasiMapBuilder {

	
	const CLASS_NAME = 'lib.model.gmap.map.GeojsonlokasiMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('gmap');

		$tMap = $this->dbMap->addTable('geojsonlokasi');
		$tMap->setPhpName('Geojsonlokasi');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('KODE_LOKASI', 'KodeLokasi', 'string', CreoleTypes::VARCHAR, true, 50);

		$tMap->addColumn('GEOJSON', 'Geojson', 'string', CreoleTypes::VARCHAR, true, null);

		$tMap->addColumn('TIPEGEOGRAFIS', 'Tipegeografis', 'string', CreoleTypes::VARCHAR, false, 50);

		$tMap->addColumn('TIPELOKASI', 'Tipelokasi', 'string', CreoleTypes::VARCHAR, false, 100);

		$tMap->addColumn('IDUSER', 'Iduser', 'string', CreoleTypes::VARCHAR, false, 100);

	} 
} 