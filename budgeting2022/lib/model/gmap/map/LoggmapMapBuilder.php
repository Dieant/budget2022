<?php



class LoggmapMapBuilder {

	
	const CLASS_NAME = 'lib.model.gmap.map.LoggmapMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('gmap');

		$tMap = $this->dbMap->addTable('loggmap');
		$tMap->setPhpName('Loggmap');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('IDLOG', 'Idlog', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('KODE_LOKASI', 'KodeLokasi', 'string', CreoleTypes::VARCHAR, false, 500);

		$tMap->addColumn('IDUSER', 'Iduser', 'string', CreoleTypes::VARCHAR, false, 50);

		$tMap->addColumn('DATALAMA', 'Datalama', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('DATABARU', 'Databaru', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('TANGGAL', 'Tanggal', 'int', CreoleTypes::TIMESTAMP, false, null);

		$tMap->addColumn('STATUS', 'Status', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('IPADDRESS', 'Ipaddress', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('FORWARDEDIPADDRESS', 'Forwardedipaddress', 'string', CreoleTypes::VARCHAR, false, null);

	} 
} 