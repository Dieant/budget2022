<?php



class GeojsonlokasiFixMapBuilder {

	
	const CLASS_NAME = 'lib.model.gmap.map.GeojsonlokasiFixMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('gmap');

		$tMap = $this->dbMap->addTable('geojsonlokasi_fix');
		$tMap->setPhpName('GeojsonlokasiFix');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('IDGEOLOKASI', 'Idgeolokasi', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('UNIT_ID', 'UnitId', 'string', CreoleTypes::VARCHAR, true, 8);

		$tMap->addColumn('KEGATAN_CODE', 'KegatanCode', 'string', CreoleTypes::VARCHAR, true, 20);

		$tMap->addColumn('DETAIL_NO', 'DetailNo', 'int', CreoleTypes::SMALLINT, true, null);

		$tMap->addColumn('SATUAN', 'Satuan', 'string', CreoleTypes::VARCHAR, false, 50);

		$tMap->addColumn('VOLUME', 'Volume', 'double', CreoleTypes::DOUBLE, false, null);

		$tMap->addColumn('NILAI_ANGGARAN', 'NilaiAnggaran', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('TAHUN', 'Tahun', 'string', CreoleTypes::VARCHAR, false, 5);

		$tMap->addColumn('MLOKASI', 'Mlokasi', 'string', CreoleTypes::VARCHAR, true, 50);

		$tMap->addColumn('ID_KELOMPOK', 'IdKelompok', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('GEOJSON', 'Geojson', 'string', CreoleTypes::VARCHAR, true, null);

		$tMap->addColumn('KETERANGAN', 'Keterangan', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('NMUSER', 'Nmuser', 'string', CreoleTypes::VARCHAR, false, 50);

		$tMap->addColumn('LEVEL', 'Level', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('KOMPONEN_NAME', 'KomponenName', 'string', CreoleTypes::VARCHAR, false, 500);

	} 
} 