<?php


abstract class BaseGeojsonlokasiFix extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $idgeolokasi;


	
	protected $unit_id;


	
	protected $kegatan_code;


	
	protected $detail_no;


	
	protected $satuan;


	
	protected $volume;


	
	protected $nilai_anggaran;


	
	protected $tahun;


	
	protected $mlokasi;


	
	protected $id_kelompok;


	
	protected $geojson;


	
	protected $keterangan;


	
	protected $nmuser;


	
	protected $level;


	
	protected $komponen_name;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getIdgeolokasi()
	{

		return $this->idgeolokasi;
	}

	
	public function getUnitId()
	{

		return $this->unit_id;
	}

	
	public function getKegatanCode()
	{

		return $this->kegatan_code;
	}

	
	public function getDetailNo()
	{

		return $this->detail_no;
	}

	
	public function getSatuan()
	{

		return $this->satuan;
	}

	
	public function getVolume()
	{

		return $this->volume;
	}

	
	public function getNilaiAnggaran()
	{

		return $this->nilai_anggaran;
	}

	
	public function getTahun()
	{

		return $this->tahun;
	}

	
	public function getMlokasi()
	{

		return $this->mlokasi;
	}

	
	public function getIdKelompok()
	{

		return $this->id_kelompok;
	}

	
	public function getGeojson()
	{

		return $this->geojson;
	}

	
	public function getKeterangan()
	{

		return $this->keterangan;
	}

	
	public function getNmuser()
	{

		return $this->nmuser;
	}

	
	public function getLevel()
	{

		return $this->level;
	}

	
	public function getKomponenName()
	{

		return $this->komponen_name;
	}

	
	public function setIdgeolokasi($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->idgeolokasi !== $v) {
			$this->idgeolokasi = $v;
			$this->modifiedColumns[] = GeojsonlokasiFixPeer::IDGEOLOKASI;
		}

	} 
	
	public function setUnitId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->unit_id !== $v) {
			$this->unit_id = $v;
			$this->modifiedColumns[] = GeojsonlokasiFixPeer::UNIT_ID;
		}

	} 
	
	public function setKegatanCode($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kegatan_code !== $v) {
			$this->kegatan_code = $v;
			$this->modifiedColumns[] = GeojsonlokasiFixPeer::KEGATAN_CODE;
		}

	} 
	
	public function setDetailNo($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->detail_no !== $v) {
			$this->detail_no = $v;
			$this->modifiedColumns[] = GeojsonlokasiFixPeer::DETAIL_NO;
		}

	} 
	
	public function setSatuan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->satuan !== $v) {
			$this->satuan = $v;
			$this->modifiedColumns[] = GeojsonlokasiFixPeer::SATUAN;
		}

	} 
	
	public function setVolume($v)
	{

		if ($this->volume !== $v) {
			$this->volume = $v;
			$this->modifiedColumns[] = GeojsonlokasiFixPeer::VOLUME;
		}

	} 
	
	public function setNilaiAnggaran($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->nilai_anggaran !== $v) {
			$this->nilai_anggaran = $v;
			$this->modifiedColumns[] = GeojsonlokasiFixPeer::NILAI_ANGGARAN;
		}

	} 
	
	public function setTahun($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->tahun !== $v) {
			$this->tahun = $v;
			$this->modifiedColumns[] = GeojsonlokasiFixPeer::TAHUN;
		}

	} 
	
	public function setMlokasi($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->mlokasi !== $v) {
			$this->mlokasi = $v;
			$this->modifiedColumns[] = GeojsonlokasiFixPeer::MLOKASI;
		}

	} 
	
	public function setIdKelompok($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id_kelompok !== $v) {
			$this->id_kelompok = $v;
			$this->modifiedColumns[] = GeojsonlokasiFixPeer::ID_KELOMPOK;
		}

	} 
	
	public function setGeojson($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->geojson !== $v) {
			$this->geojson = $v;
			$this->modifiedColumns[] = GeojsonlokasiFixPeer::GEOJSON;
		}

	} 
	
	public function setKeterangan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->keterangan !== $v) {
			$this->keterangan = $v;
			$this->modifiedColumns[] = GeojsonlokasiFixPeer::KETERANGAN;
		}

	} 
	
	public function setNmuser($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nmuser !== $v) {
			$this->nmuser = $v;
			$this->modifiedColumns[] = GeojsonlokasiFixPeer::NMUSER;
		}

	} 
	
	public function setLevel($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->level !== $v) {
			$this->level = $v;
			$this->modifiedColumns[] = GeojsonlokasiFixPeer::LEVEL;
		}

	} 
	
	public function setKomponenName($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->komponen_name !== $v) {
			$this->komponen_name = $v;
			$this->modifiedColumns[] = GeojsonlokasiFixPeer::KOMPONEN_NAME;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->idgeolokasi = $rs->getInt($startcol + 0);

			$this->unit_id = $rs->getString($startcol + 1);

			$this->kegatan_code = $rs->getString($startcol + 2);

			$this->detail_no = $rs->getInt($startcol + 3);

			$this->satuan = $rs->getString($startcol + 4);

			$this->volume = $rs->getFloat($startcol + 5);

			$this->nilai_anggaran = $rs->getInt($startcol + 6);

			$this->tahun = $rs->getString($startcol + 7);

			$this->mlokasi = $rs->getString($startcol + 8);

			$this->id_kelompok = $rs->getInt($startcol + 9);

			$this->geojson = $rs->getString($startcol + 10);

			$this->keterangan = $rs->getString($startcol + 11);

			$this->nmuser = $rs->getString($startcol + 12);

			$this->level = $rs->getInt($startcol + 13);

			$this->komponen_name = $rs->getString($startcol + 14);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 15; 
		} catch (Exception $e) {
			throw new PropelException("Error populating GeojsonlokasiFix object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(GeojsonlokasiFixPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			GeojsonlokasiFixPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(GeojsonlokasiFixPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = GeojsonlokasiFixPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += GeojsonlokasiFixPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = GeojsonlokasiFixPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = GeojsonlokasiFixPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getIdgeolokasi();
				break;
			case 1:
				return $this->getUnitId();
				break;
			case 2:
				return $this->getKegatanCode();
				break;
			case 3:
				return $this->getDetailNo();
				break;
			case 4:
				return $this->getSatuan();
				break;
			case 5:
				return $this->getVolume();
				break;
			case 6:
				return $this->getNilaiAnggaran();
				break;
			case 7:
				return $this->getTahun();
				break;
			case 8:
				return $this->getMlokasi();
				break;
			case 9:
				return $this->getIdKelompok();
				break;
			case 10:
				return $this->getGeojson();
				break;
			case 11:
				return $this->getKeterangan();
				break;
			case 12:
				return $this->getNmuser();
				break;
			case 13:
				return $this->getLevel();
				break;
			case 14:
				return $this->getKomponenName();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = GeojsonlokasiFixPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getIdgeolokasi(),
			$keys[1] => $this->getUnitId(),
			$keys[2] => $this->getKegatanCode(),
			$keys[3] => $this->getDetailNo(),
			$keys[4] => $this->getSatuan(),
			$keys[5] => $this->getVolume(),
			$keys[6] => $this->getNilaiAnggaran(),
			$keys[7] => $this->getTahun(),
			$keys[8] => $this->getMlokasi(),
			$keys[9] => $this->getIdKelompok(),
			$keys[10] => $this->getGeojson(),
			$keys[11] => $this->getKeterangan(),
			$keys[12] => $this->getNmuser(),
			$keys[13] => $this->getLevel(),
			$keys[14] => $this->getKomponenName(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = GeojsonlokasiFixPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setIdgeolokasi($value);
				break;
			case 1:
				$this->setUnitId($value);
				break;
			case 2:
				$this->setKegatanCode($value);
				break;
			case 3:
				$this->setDetailNo($value);
				break;
			case 4:
				$this->setSatuan($value);
				break;
			case 5:
				$this->setVolume($value);
				break;
			case 6:
				$this->setNilaiAnggaran($value);
				break;
			case 7:
				$this->setTahun($value);
				break;
			case 8:
				$this->setMlokasi($value);
				break;
			case 9:
				$this->setIdKelompok($value);
				break;
			case 10:
				$this->setGeojson($value);
				break;
			case 11:
				$this->setKeterangan($value);
				break;
			case 12:
				$this->setNmuser($value);
				break;
			case 13:
				$this->setLevel($value);
				break;
			case 14:
				$this->setKomponenName($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = GeojsonlokasiFixPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setIdgeolokasi($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setUnitId($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setKegatanCode($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setDetailNo($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setSatuan($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setVolume($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setNilaiAnggaran($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setTahun($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setMlokasi($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setIdKelompok($arr[$keys[9]]);
		if (array_key_exists($keys[10], $arr)) $this->setGeojson($arr[$keys[10]]);
		if (array_key_exists($keys[11], $arr)) $this->setKeterangan($arr[$keys[11]]);
		if (array_key_exists($keys[12], $arr)) $this->setNmuser($arr[$keys[12]]);
		if (array_key_exists($keys[13], $arr)) $this->setLevel($arr[$keys[13]]);
		if (array_key_exists($keys[14], $arr)) $this->setKomponenName($arr[$keys[14]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(GeojsonlokasiFixPeer::DATABASE_NAME);

		if ($this->isColumnModified(GeojsonlokasiFixPeer::IDGEOLOKASI)) $criteria->add(GeojsonlokasiFixPeer::IDGEOLOKASI, $this->idgeolokasi);
		if ($this->isColumnModified(GeojsonlokasiFixPeer::UNIT_ID)) $criteria->add(GeojsonlokasiFixPeer::UNIT_ID, $this->unit_id);
		if ($this->isColumnModified(GeojsonlokasiFixPeer::KEGATAN_CODE)) $criteria->add(GeojsonlokasiFixPeer::KEGATAN_CODE, $this->kegatan_code);
		if ($this->isColumnModified(GeojsonlokasiFixPeer::DETAIL_NO)) $criteria->add(GeojsonlokasiFixPeer::DETAIL_NO, $this->detail_no);
		if ($this->isColumnModified(GeojsonlokasiFixPeer::SATUAN)) $criteria->add(GeojsonlokasiFixPeer::SATUAN, $this->satuan);
		if ($this->isColumnModified(GeojsonlokasiFixPeer::VOLUME)) $criteria->add(GeojsonlokasiFixPeer::VOLUME, $this->volume);
		if ($this->isColumnModified(GeojsonlokasiFixPeer::NILAI_ANGGARAN)) $criteria->add(GeojsonlokasiFixPeer::NILAI_ANGGARAN, $this->nilai_anggaran);
		if ($this->isColumnModified(GeojsonlokasiFixPeer::TAHUN)) $criteria->add(GeojsonlokasiFixPeer::TAHUN, $this->tahun);
		if ($this->isColumnModified(GeojsonlokasiFixPeer::MLOKASI)) $criteria->add(GeojsonlokasiFixPeer::MLOKASI, $this->mlokasi);
		if ($this->isColumnModified(GeojsonlokasiFixPeer::ID_KELOMPOK)) $criteria->add(GeojsonlokasiFixPeer::ID_KELOMPOK, $this->id_kelompok);
		if ($this->isColumnModified(GeojsonlokasiFixPeer::GEOJSON)) $criteria->add(GeojsonlokasiFixPeer::GEOJSON, $this->geojson);
		if ($this->isColumnModified(GeojsonlokasiFixPeer::KETERANGAN)) $criteria->add(GeojsonlokasiFixPeer::KETERANGAN, $this->keterangan);
		if ($this->isColumnModified(GeojsonlokasiFixPeer::NMUSER)) $criteria->add(GeojsonlokasiFixPeer::NMUSER, $this->nmuser);
		if ($this->isColumnModified(GeojsonlokasiFixPeer::LEVEL)) $criteria->add(GeojsonlokasiFixPeer::LEVEL, $this->level);
		if ($this->isColumnModified(GeojsonlokasiFixPeer::KOMPONEN_NAME)) $criteria->add(GeojsonlokasiFixPeer::KOMPONEN_NAME, $this->komponen_name);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(GeojsonlokasiFixPeer::DATABASE_NAME);

		$criteria->add(GeojsonlokasiFixPeer::IDGEOLOKASI, $this->idgeolokasi);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getIdgeolokasi();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setIdgeolokasi($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setUnitId($this->unit_id);

		$copyObj->setKegatanCode($this->kegatan_code);

		$copyObj->setDetailNo($this->detail_no);

		$copyObj->setSatuan($this->satuan);

		$copyObj->setVolume($this->volume);

		$copyObj->setNilaiAnggaran($this->nilai_anggaran);

		$copyObj->setTahun($this->tahun);

		$copyObj->setMlokasi($this->mlokasi);

		$copyObj->setIdKelompok($this->id_kelompok);

		$copyObj->setGeojson($this->geojson);

		$copyObj->setKeterangan($this->keterangan);

		$copyObj->setNmuser($this->nmuser);

		$copyObj->setLevel($this->level);

		$copyObj->setKomponenName($this->komponen_name);


		$copyObj->setNew(true);

		$copyObj->setIdgeolokasi(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new GeojsonlokasiFixPeer();
		}
		return self::$peer;
	}

} 