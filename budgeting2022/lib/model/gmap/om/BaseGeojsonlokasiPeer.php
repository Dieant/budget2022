<?php


abstract class BaseGeojsonlokasiPeer {

	
	const DATABASE_NAME = 'gmap';

	
	const TABLE_NAME = 'geojsonlokasi';

	
	const CLASS_DEFAULT = 'lib.model.gmap.Geojsonlokasi';

	
	const NUM_COLUMNS = 5;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const KODE_LOKASI = 'geojsonlokasi.KODE_LOKASI';

	
	const GEOJSON = 'geojsonlokasi.GEOJSON';

	
	const TIPEGEOGRAFIS = 'geojsonlokasi.TIPEGEOGRAFIS';

	
	const TIPELOKASI = 'geojsonlokasi.TIPELOKASI';

	
	const IDUSER = 'geojsonlokasi.IDUSER';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('KodeLokasi', 'Geojson', 'Tipegeografis', 'Tipelokasi', 'Iduser', ),
		BasePeer::TYPE_COLNAME => array (GeojsonlokasiPeer::KODE_LOKASI, GeojsonlokasiPeer::GEOJSON, GeojsonlokasiPeer::TIPEGEOGRAFIS, GeojsonlokasiPeer::TIPELOKASI, GeojsonlokasiPeer::IDUSER, ),
		BasePeer::TYPE_FIELDNAME => array ('kode_lokasi', 'geojson', 'tipegeografis', 'tipelokasi', 'iduser', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('KodeLokasi' => 0, 'Geojson' => 1, 'Tipegeografis' => 2, 'Tipelokasi' => 3, 'Iduser' => 4, ),
		BasePeer::TYPE_COLNAME => array (GeojsonlokasiPeer::KODE_LOKASI => 0, GeojsonlokasiPeer::GEOJSON => 1, GeojsonlokasiPeer::TIPEGEOGRAFIS => 2, GeojsonlokasiPeer::TIPELOKASI => 3, GeojsonlokasiPeer::IDUSER => 4, ),
		BasePeer::TYPE_FIELDNAME => array ('kode_lokasi' => 0, 'geojson' => 1, 'tipegeografis' => 2, 'tipelokasi' => 3, 'iduser' => 4, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/gmap/map/GeojsonlokasiMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.gmap.map.GeojsonlokasiMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = GeojsonlokasiPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(GeojsonlokasiPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(GeojsonlokasiPeer::KODE_LOKASI);

		$criteria->addSelectColumn(GeojsonlokasiPeer::GEOJSON);

		$criteria->addSelectColumn(GeojsonlokasiPeer::TIPEGEOGRAFIS);

		$criteria->addSelectColumn(GeojsonlokasiPeer::TIPELOKASI);

		$criteria->addSelectColumn(GeojsonlokasiPeer::IDUSER);

	}

	const COUNT = 'COUNT(geojsonlokasi.KODE_LOKASI)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT geojsonlokasi.KODE_LOKASI)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(GeojsonlokasiPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(GeojsonlokasiPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = GeojsonlokasiPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = GeojsonlokasiPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return GeojsonlokasiPeer::populateObjects(GeojsonlokasiPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			GeojsonlokasiPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = GeojsonlokasiPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return GeojsonlokasiPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(GeojsonlokasiPeer::KODE_LOKASI);
			$selectCriteria->add(GeojsonlokasiPeer::KODE_LOKASI, $criteria->remove(GeojsonlokasiPeer::KODE_LOKASI), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(GeojsonlokasiPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(GeojsonlokasiPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof Geojsonlokasi) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(GeojsonlokasiPeer::KODE_LOKASI, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(Geojsonlokasi $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(GeojsonlokasiPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(GeojsonlokasiPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(GeojsonlokasiPeer::DATABASE_NAME, GeojsonlokasiPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = GeojsonlokasiPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(GeojsonlokasiPeer::DATABASE_NAME);

		$criteria->add(GeojsonlokasiPeer::KODE_LOKASI, $pk);


		$v = GeojsonlokasiPeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(GeojsonlokasiPeer::KODE_LOKASI, $pks, Criteria::IN);
			$objs = GeojsonlokasiPeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BaseGeojsonlokasiPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/gmap/map/GeojsonlokasiMapBuilder.php';
	Propel::registerMapBuilder('lib.model.gmap.map.GeojsonlokasiMapBuilder');
}
