<?php


abstract class BaseGeojsonlokasi extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $kode_lokasi;


	
	protected $geojson;


	
	protected $tipegeografis;


	
	protected $tipelokasi;


	
	protected $iduser;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getKodeLokasi()
	{

		return $this->kode_lokasi;
	}

	
	public function getGeojson()
	{

		return $this->geojson;
	}

	
	public function getTipegeografis()
	{

		return $this->tipegeografis;
	}

	
	public function getTipelokasi()
	{

		return $this->tipelokasi;
	}

	
	public function getIduser()
	{

		return $this->iduser;
	}

	
	public function setKodeLokasi($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_lokasi !== $v) {
			$this->kode_lokasi = $v;
			$this->modifiedColumns[] = GeojsonlokasiPeer::KODE_LOKASI;
		}

	} 
	
	public function setGeojson($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->geojson !== $v) {
			$this->geojson = $v;
			$this->modifiedColumns[] = GeojsonlokasiPeer::GEOJSON;
		}

	} 
	
	public function setTipegeografis($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->tipegeografis !== $v) {
			$this->tipegeografis = $v;
			$this->modifiedColumns[] = GeojsonlokasiPeer::TIPEGEOGRAFIS;
		}

	} 
	
	public function setTipelokasi($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->tipelokasi !== $v) {
			$this->tipelokasi = $v;
			$this->modifiedColumns[] = GeojsonlokasiPeer::TIPELOKASI;
		}

	} 
	
	public function setIduser($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->iduser !== $v) {
			$this->iduser = $v;
			$this->modifiedColumns[] = GeojsonlokasiPeer::IDUSER;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->kode_lokasi = $rs->getString($startcol + 0);

			$this->geojson = $rs->getString($startcol + 1);

			$this->tipegeografis = $rs->getString($startcol + 2);

			$this->tipelokasi = $rs->getString($startcol + 3);

			$this->iduser = $rs->getString($startcol + 4);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 5; 
		} catch (Exception $e) {
			throw new PropelException("Error populating Geojsonlokasi object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(GeojsonlokasiPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			GeojsonlokasiPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(GeojsonlokasiPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = GeojsonlokasiPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += GeojsonlokasiPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = GeojsonlokasiPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = GeojsonlokasiPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getKodeLokasi();
				break;
			case 1:
				return $this->getGeojson();
				break;
			case 2:
				return $this->getTipegeografis();
				break;
			case 3:
				return $this->getTipelokasi();
				break;
			case 4:
				return $this->getIduser();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = GeojsonlokasiPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getKodeLokasi(),
			$keys[1] => $this->getGeojson(),
			$keys[2] => $this->getTipegeografis(),
			$keys[3] => $this->getTipelokasi(),
			$keys[4] => $this->getIduser(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = GeojsonlokasiPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setKodeLokasi($value);
				break;
			case 1:
				$this->setGeojson($value);
				break;
			case 2:
				$this->setTipegeografis($value);
				break;
			case 3:
				$this->setTipelokasi($value);
				break;
			case 4:
				$this->setIduser($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = GeojsonlokasiPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setKodeLokasi($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setGeojson($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setTipegeografis($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setTipelokasi($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setIduser($arr[$keys[4]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(GeojsonlokasiPeer::DATABASE_NAME);

		if ($this->isColumnModified(GeojsonlokasiPeer::KODE_LOKASI)) $criteria->add(GeojsonlokasiPeer::KODE_LOKASI, $this->kode_lokasi);
		if ($this->isColumnModified(GeojsonlokasiPeer::GEOJSON)) $criteria->add(GeojsonlokasiPeer::GEOJSON, $this->geojson);
		if ($this->isColumnModified(GeojsonlokasiPeer::TIPEGEOGRAFIS)) $criteria->add(GeojsonlokasiPeer::TIPEGEOGRAFIS, $this->tipegeografis);
		if ($this->isColumnModified(GeojsonlokasiPeer::TIPELOKASI)) $criteria->add(GeojsonlokasiPeer::TIPELOKASI, $this->tipelokasi);
		if ($this->isColumnModified(GeojsonlokasiPeer::IDUSER)) $criteria->add(GeojsonlokasiPeer::IDUSER, $this->iduser);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(GeojsonlokasiPeer::DATABASE_NAME);

		$criteria->add(GeojsonlokasiPeer::KODE_LOKASI, $this->kode_lokasi);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getKodeLokasi();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setKodeLokasi($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setGeojson($this->geojson);

		$copyObj->setTipegeografis($this->tipegeografis);

		$copyObj->setTipelokasi($this->tipelokasi);

		$copyObj->setIduser($this->iduser);


		$copyObj->setNew(true);

		$copyObj->setKodeLokasi(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new GeojsonlokasiPeer();
		}
		return self::$peer;
	}

} 