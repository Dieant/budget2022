<?php


abstract class BaseLoggmapFix extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $idlog_serial;


	
	protected $idgeolokasi;


	
	protected $nmuser;


	
	protected $datalama;


	
	protected $databaru;


	
	protected $tanggal;


	
	protected $status;


	
	protected $ipaddress;


	
	protected $forwardedipaddress;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getIdlogSerial()
	{

		return $this->idlog_serial;
	}

	
	public function getIdgeolokasi()
	{

		return $this->idgeolokasi;
	}

	
	public function getNmuser()
	{

		return $this->nmuser;
	}

	
	public function getDatalama()
	{

		return $this->datalama;
	}

	
	public function getDatabaru()
	{

		return $this->databaru;
	}

	
	public function getTanggal($format = 'Y-m-d H:i:s')
	{

		if ($this->tanggal === null || $this->tanggal === '') {
			return null;
		} elseif (!is_int($this->tanggal)) {
						$ts = strtotime($this->tanggal);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [tanggal] as date/time value: " . var_export($this->tanggal, true));
			}
		} else {
			$ts = $this->tanggal;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getStatus()
	{

		return $this->status;
	}

	
	public function getIpaddress()
	{

		return $this->ipaddress;
	}

	
	public function getForwardedipaddress()
	{

		return $this->forwardedipaddress;
	}

	
	public function setIdlogSerial($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->idlog_serial !== $v) {
			$this->idlog_serial = $v;
			$this->modifiedColumns[] = LoggmapFixPeer::IDLOG_SERIAL;
		}

	} 
	
	public function setIdgeolokasi($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->idgeolokasi !== $v) {
			$this->idgeolokasi = $v;
			$this->modifiedColumns[] = LoggmapFixPeer::IDGEOLOKASI;
		}

	} 
	
	public function setNmuser($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nmuser !== $v) {
			$this->nmuser = $v;
			$this->modifiedColumns[] = LoggmapFixPeer::NMUSER;
		}

	} 
	
	public function setDatalama($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->datalama !== $v) {
			$this->datalama = $v;
			$this->modifiedColumns[] = LoggmapFixPeer::DATALAMA;
		}

	} 
	
	public function setDatabaru($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->databaru !== $v) {
			$this->databaru = $v;
			$this->modifiedColumns[] = LoggmapFixPeer::DATABARU;
		}

	} 
	
	public function setTanggal($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [tanggal] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->tanggal !== $ts) {
			$this->tanggal = $ts;
			$this->modifiedColumns[] = LoggmapFixPeer::TANGGAL;
		}

	} 
	
	public function setStatus($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->status !== $v) {
			$this->status = $v;
			$this->modifiedColumns[] = LoggmapFixPeer::STATUS;
		}

	} 
	
	public function setIpaddress($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->ipaddress !== $v) {
			$this->ipaddress = $v;
			$this->modifiedColumns[] = LoggmapFixPeer::IPADDRESS;
		}

	} 
	
	public function setForwardedipaddress($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->forwardedipaddress !== $v) {
			$this->forwardedipaddress = $v;
			$this->modifiedColumns[] = LoggmapFixPeer::FORWARDEDIPADDRESS;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->idlog_serial = $rs->getInt($startcol + 0);

			$this->idgeolokasi = $rs->getInt($startcol + 1);

			$this->nmuser = $rs->getString($startcol + 2);

			$this->datalama = $rs->getString($startcol + 3);

			$this->databaru = $rs->getString($startcol + 4);

			$this->tanggal = $rs->getTimestamp($startcol + 5, null);

			$this->status = $rs->getInt($startcol + 6);

			$this->ipaddress = $rs->getString($startcol + 7);

			$this->forwardedipaddress = $rs->getString($startcol + 8);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 9; 
		} catch (Exception $e) {
			throw new PropelException("Error populating LoggmapFix object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(LoggmapFixPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			LoggmapFixPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(LoggmapFixPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = LoggmapFixPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += LoggmapFixPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = LoggmapFixPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = LoggmapFixPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getIdlogSerial();
				break;
			case 1:
				return $this->getIdgeolokasi();
				break;
			case 2:
				return $this->getNmuser();
				break;
			case 3:
				return $this->getDatalama();
				break;
			case 4:
				return $this->getDatabaru();
				break;
			case 5:
				return $this->getTanggal();
				break;
			case 6:
				return $this->getStatus();
				break;
			case 7:
				return $this->getIpaddress();
				break;
			case 8:
				return $this->getForwardedipaddress();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = LoggmapFixPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getIdlogSerial(),
			$keys[1] => $this->getIdgeolokasi(),
			$keys[2] => $this->getNmuser(),
			$keys[3] => $this->getDatalama(),
			$keys[4] => $this->getDatabaru(),
			$keys[5] => $this->getTanggal(),
			$keys[6] => $this->getStatus(),
			$keys[7] => $this->getIpaddress(),
			$keys[8] => $this->getForwardedipaddress(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = LoggmapFixPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setIdlogSerial($value);
				break;
			case 1:
				$this->setIdgeolokasi($value);
				break;
			case 2:
				$this->setNmuser($value);
				break;
			case 3:
				$this->setDatalama($value);
				break;
			case 4:
				$this->setDatabaru($value);
				break;
			case 5:
				$this->setTanggal($value);
				break;
			case 6:
				$this->setStatus($value);
				break;
			case 7:
				$this->setIpaddress($value);
				break;
			case 8:
				$this->setForwardedipaddress($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = LoggmapFixPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setIdlogSerial($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setIdgeolokasi($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setNmuser($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setDatalama($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setDatabaru($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setTanggal($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setStatus($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setIpaddress($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setForwardedipaddress($arr[$keys[8]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(LoggmapFixPeer::DATABASE_NAME);

		if ($this->isColumnModified(LoggmapFixPeer::IDLOG_SERIAL)) $criteria->add(LoggmapFixPeer::IDLOG_SERIAL, $this->idlog_serial);
		if ($this->isColumnModified(LoggmapFixPeer::IDGEOLOKASI)) $criteria->add(LoggmapFixPeer::IDGEOLOKASI, $this->idgeolokasi);
		if ($this->isColumnModified(LoggmapFixPeer::NMUSER)) $criteria->add(LoggmapFixPeer::NMUSER, $this->nmuser);
		if ($this->isColumnModified(LoggmapFixPeer::DATALAMA)) $criteria->add(LoggmapFixPeer::DATALAMA, $this->datalama);
		if ($this->isColumnModified(LoggmapFixPeer::DATABARU)) $criteria->add(LoggmapFixPeer::DATABARU, $this->databaru);
		if ($this->isColumnModified(LoggmapFixPeer::TANGGAL)) $criteria->add(LoggmapFixPeer::TANGGAL, $this->tanggal);
		if ($this->isColumnModified(LoggmapFixPeer::STATUS)) $criteria->add(LoggmapFixPeer::STATUS, $this->status);
		if ($this->isColumnModified(LoggmapFixPeer::IPADDRESS)) $criteria->add(LoggmapFixPeer::IPADDRESS, $this->ipaddress);
		if ($this->isColumnModified(LoggmapFixPeer::FORWARDEDIPADDRESS)) $criteria->add(LoggmapFixPeer::FORWARDEDIPADDRESS, $this->forwardedipaddress);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(LoggmapFixPeer::DATABASE_NAME);

		$criteria->add(LoggmapFixPeer::IDLOG_SERIAL, $this->idlog_serial);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getIdlogSerial();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setIdlogSerial($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setIdgeolokasi($this->idgeolokasi);

		$copyObj->setNmuser($this->nmuser);

		$copyObj->setDatalama($this->datalama);

		$copyObj->setDatabaru($this->databaru);

		$copyObj->setTanggal($this->tanggal);

		$copyObj->setStatus($this->status);

		$copyObj->setIpaddress($this->ipaddress);

		$copyObj->setForwardedipaddress($this->forwardedipaddress);


		$copyObj->setNew(true);

		$copyObj->setIdlogSerial(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new LoggmapFixPeer();
		}
		return self::$peer;
	}

} 