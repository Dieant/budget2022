<?php


abstract class BaseGeojsonlokasiFixPeer {

	
	const DATABASE_NAME = 'gmap';

	
	const TABLE_NAME = 'geojsonlokasi_fix';

	
	const CLASS_DEFAULT = 'lib.model.gmap.GeojsonlokasiFix';

	
	const NUM_COLUMNS = 15;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const IDGEOLOKASI = 'geojsonlokasi_fix.IDGEOLOKASI';

	
	const UNIT_ID = 'geojsonlokasi_fix.UNIT_ID';

	
	const KEGATAN_CODE = 'geojsonlokasi_fix.KEGATAN_CODE';

	
	const DETAIL_NO = 'geojsonlokasi_fix.DETAIL_NO';

	
	const SATUAN = 'geojsonlokasi_fix.SATUAN';

	
	const VOLUME = 'geojsonlokasi_fix.VOLUME';

	
	const NILAI_ANGGARAN = 'geojsonlokasi_fix.NILAI_ANGGARAN';

	
	const TAHUN = 'geojsonlokasi_fix.TAHUN';

	
	const MLOKASI = 'geojsonlokasi_fix.MLOKASI';

	
	const ID_KELOMPOK = 'geojsonlokasi_fix.ID_KELOMPOK';

	
	const GEOJSON = 'geojsonlokasi_fix.GEOJSON';

	
	const KETERANGAN = 'geojsonlokasi_fix.KETERANGAN';

	
	const NMUSER = 'geojsonlokasi_fix.NMUSER';

	
	const LEVEL = 'geojsonlokasi_fix.LEVEL';

	
	const KOMPONEN_NAME = 'geojsonlokasi_fix.KOMPONEN_NAME';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('Idgeolokasi', 'UnitId', 'KegatanCode', 'DetailNo', 'Satuan', 'Volume', 'NilaiAnggaran', 'Tahun', 'Mlokasi', 'IdKelompok', 'Geojson', 'Keterangan', 'Nmuser', 'Level', 'KomponenName', ),
		BasePeer::TYPE_COLNAME => array (GeojsonlokasiFixPeer::IDGEOLOKASI, GeojsonlokasiFixPeer::UNIT_ID, GeojsonlokasiFixPeer::KEGATAN_CODE, GeojsonlokasiFixPeer::DETAIL_NO, GeojsonlokasiFixPeer::SATUAN, GeojsonlokasiFixPeer::VOLUME, GeojsonlokasiFixPeer::NILAI_ANGGARAN, GeojsonlokasiFixPeer::TAHUN, GeojsonlokasiFixPeer::MLOKASI, GeojsonlokasiFixPeer::ID_KELOMPOK, GeojsonlokasiFixPeer::GEOJSON, GeojsonlokasiFixPeer::KETERANGAN, GeojsonlokasiFixPeer::NMUSER, GeojsonlokasiFixPeer::LEVEL, GeojsonlokasiFixPeer::KOMPONEN_NAME, ),
		BasePeer::TYPE_FIELDNAME => array ('idgeolokasi', 'unit_id', 'kegatan_code', 'detail_no', 'satuan', 'volume', 'nilai_anggaran', 'tahun', 'mlokasi', 'id_kelompok', 'geojson', 'keterangan', 'nmuser', 'level', 'komponen_name', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('Idgeolokasi' => 0, 'UnitId' => 1, 'KegatanCode' => 2, 'DetailNo' => 3, 'Satuan' => 4, 'Volume' => 5, 'NilaiAnggaran' => 6, 'Tahun' => 7, 'Mlokasi' => 8, 'IdKelompok' => 9, 'Geojson' => 10, 'Keterangan' => 11, 'Nmuser' => 12, 'Level' => 13, 'KomponenName' => 14, ),
		BasePeer::TYPE_COLNAME => array (GeojsonlokasiFixPeer::IDGEOLOKASI => 0, GeojsonlokasiFixPeer::UNIT_ID => 1, GeojsonlokasiFixPeer::KEGATAN_CODE => 2, GeojsonlokasiFixPeer::DETAIL_NO => 3, GeojsonlokasiFixPeer::SATUAN => 4, GeojsonlokasiFixPeer::VOLUME => 5, GeojsonlokasiFixPeer::NILAI_ANGGARAN => 6, GeojsonlokasiFixPeer::TAHUN => 7, GeojsonlokasiFixPeer::MLOKASI => 8, GeojsonlokasiFixPeer::ID_KELOMPOK => 9, GeojsonlokasiFixPeer::GEOJSON => 10, GeojsonlokasiFixPeer::KETERANGAN => 11, GeojsonlokasiFixPeer::NMUSER => 12, GeojsonlokasiFixPeer::LEVEL => 13, GeojsonlokasiFixPeer::KOMPONEN_NAME => 14, ),
		BasePeer::TYPE_FIELDNAME => array ('idgeolokasi' => 0, 'unit_id' => 1, 'kegatan_code' => 2, 'detail_no' => 3, 'satuan' => 4, 'volume' => 5, 'nilai_anggaran' => 6, 'tahun' => 7, 'mlokasi' => 8, 'id_kelompok' => 9, 'geojson' => 10, 'keterangan' => 11, 'nmuser' => 12, 'level' => 13, 'komponen_name' => 14, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/gmap/map/GeojsonlokasiFixMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.gmap.map.GeojsonlokasiFixMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = GeojsonlokasiFixPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(GeojsonlokasiFixPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(GeojsonlokasiFixPeer::IDGEOLOKASI);

		$criteria->addSelectColumn(GeojsonlokasiFixPeer::UNIT_ID);

		$criteria->addSelectColumn(GeojsonlokasiFixPeer::KEGATAN_CODE);

		$criteria->addSelectColumn(GeojsonlokasiFixPeer::DETAIL_NO);

		$criteria->addSelectColumn(GeojsonlokasiFixPeer::SATUAN);

		$criteria->addSelectColumn(GeojsonlokasiFixPeer::VOLUME);

		$criteria->addSelectColumn(GeojsonlokasiFixPeer::NILAI_ANGGARAN);

		$criteria->addSelectColumn(GeojsonlokasiFixPeer::TAHUN);

		$criteria->addSelectColumn(GeojsonlokasiFixPeer::MLOKASI);

		$criteria->addSelectColumn(GeojsonlokasiFixPeer::ID_KELOMPOK);

		$criteria->addSelectColumn(GeojsonlokasiFixPeer::GEOJSON);

		$criteria->addSelectColumn(GeojsonlokasiFixPeer::KETERANGAN);

		$criteria->addSelectColumn(GeojsonlokasiFixPeer::NMUSER);

		$criteria->addSelectColumn(GeojsonlokasiFixPeer::LEVEL);

		$criteria->addSelectColumn(GeojsonlokasiFixPeer::KOMPONEN_NAME);

	}

	const COUNT = 'COUNT(geojsonlokasi_fix.IDGEOLOKASI)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT geojsonlokasi_fix.IDGEOLOKASI)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(GeojsonlokasiFixPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(GeojsonlokasiFixPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = GeojsonlokasiFixPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = GeojsonlokasiFixPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return GeojsonlokasiFixPeer::populateObjects(GeojsonlokasiFixPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			GeojsonlokasiFixPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = GeojsonlokasiFixPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return GeojsonlokasiFixPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(GeojsonlokasiFixPeer::IDGEOLOKASI);
			$selectCriteria->add(GeojsonlokasiFixPeer::IDGEOLOKASI, $criteria->remove(GeojsonlokasiFixPeer::IDGEOLOKASI), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(GeojsonlokasiFixPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(GeojsonlokasiFixPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof GeojsonlokasiFix) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(GeojsonlokasiFixPeer::IDGEOLOKASI, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(GeojsonlokasiFix $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(GeojsonlokasiFixPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(GeojsonlokasiFixPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(GeojsonlokasiFixPeer::DATABASE_NAME, GeojsonlokasiFixPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = GeojsonlokasiFixPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(GeojsonlokasiFixPeer::DATABASE_NAME);

		$criteria->add(GeojsonlokasiFixPeer::IDGEOLOKASI, $pk);


		$v = GeojsonlokasiFixPeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(GeojsonlokasiFixPeer::IDGEOLOKASI, $pks, Criteria::IN);
			$objs = GeojsonlokasiFixPeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BaseGeojsonlokasiFixPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/gmap/map/GeojsonlokasiFixMapBuilder.php';
	Propel::registerMapBuilder('lib.model.gmap.map.GeojsonlokasiFixMapBuilder');
}
