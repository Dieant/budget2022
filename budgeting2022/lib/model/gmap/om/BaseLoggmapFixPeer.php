<?php


abstract class BaseLoggmapFixPeer {

	
	const DATABASE_NAME = 'gmap';

	
	const TABLE_NAME = 'loggmap_fix';

	
	const CLASS_DEFAULT = 'lib.model.gmap.LoggmapFix';

	
	const NUM_COLUMNS = 9;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const IDLOG_SERIAL = 'loggmap_fix.IDLOG_SERIAL';

	
	const IDGEOLOKASI = 'loggmap_fix.IDGEOLOKASI';

	
	const NMUSER = 'loggmap_fix.NMUSER';

	
	const DATALAMA = 'loggmap_fix.DATALAMA';

	
	const DATABARU = 'loggmap_fix.DATABARU';

	
	const TANGGAL = 'loggmap_fix.TANGGAL';

	
	const STATUS = 'loggmap_fix.STATUS';

	
	const IPADDRESS = 'loggmap_fix.IPADDRESS';

	
	const FORWARDEDIPADDRESS = 'loggmap_fix.FORWARDEDIPADDRESS';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('IdlogSerial', 'Idgeolokasi', 'Nmuser', 'Datalama', 'Databaru', 'Tanggal', 'Status', 'Ipaddress', 'Forwardedipaddress', ),
		BasePeer::TYPE_COLNAME => array (LoggmapFixPeer::IDLOG_SERIAL, LoggmapFixPeer::IDGEOLOKASI, LoggmapFixPeer::NMUSER, LoggmapFixPeer::DATALAMA, LoggmapFixPeer::DATABARU, LoggmapFixPeer::TANGGAL, LoggmapFixPeer::STATUS, LoggmapFixPeer::IPADDRESS, LoggmapFixPeer::FORWARDEDIPADDRESS, ),
		BasePeer::TYPE_FIELDNAME => array ('idlog_serial', 'idgeolokasi', 'nmuser', 'datalama', 'databaru', 'tanggal', 'status', 'ipaddress', 'forwardedipaddress', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('IdlogSerial' => 0, 'Idgeolokasi' => 1, 'Nmuser' => 2, 'Datalama' => 3, 'Databaru' => 4, 'Tanggal' => 5, 'Status' => 6, 'Ipaddress' => 7, 'Forwardedipaddress' => 8, ),
		BasePeer::TYPE_COLNAME => array (LoggmapFixPeer::IDLOG_SERIAL => 0, LoggmapFixPeer::IDGEOLOKASI => 1, LoggmapFixPeer::NMUSER => 2, LoggmapFixPeer::DATALAMA => 3, LoggmapFixPeer::DATABARU => 4, LoggmapFixPeer::TANGGAL => 5, LoggmapFixPeer::STATUS => 6, LoggmapFixPeer::IPADDRESS => 7, LoggmapFixPeer::FORWARDEDIPADDRESS => 8, ),
		BasePeer::TYPE_FIELDNAME => array ('idlog_serial' => 0, 'idgeolokasi' => 1, 'nmuser' => 2, 'datalama' => 3, 'databaru' => 4, 'tanggal' => 5, 'status' => 6, 'ipaddress' => 7, 'forwardedipaddress' => 8, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/gmap/map/LoggmapFixMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.gmap.map.LoggmapFixMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = LoggmapFixPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(LoggmapFixPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(LoggmapFixPeer::IDLOG_SERIAL);

		$criteria->addSelectColumn(LoggmapFixPeer::IDGEOLOKASI);

		$criteria->addSelectColumn(LoggmapFixPeer::NMUSER);

		$criteria->addSelectColumn(LoggmapFixPeer::DATALAMA);

		$criteria->addSelectColumn(LoggmapFixPeer::DATABARU);

		$criteria->addSelectColumn(LoggmapFixPeer::TANGGAL);

		$criteria->addSelectColumn(LoggmapFixPeer::STATUS);

		$criteria->addSelectColumn(LoggmapFixPeer::IPADDRESS);

		$criteria->addSelectColumn(LoggmapFixPeer::FORWARDEDIPADDRESS);

	}

	const COUNT = 'COUNT(loggmap_fix.IDLOG_SERIAL)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT loggmap_fix.IDLOG_SERIAL)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(LoggmapFixPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(LoggmapFixPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = LoggmapFixPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = LoggmapFixPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return LoggmapFixPeer::populateObjects(LoggmapFixPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			LoggmapFixPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = LoggmapFixPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return LoggmapFixPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(LoggmapFixPeer::IDLOG_SERIAL);
			$selectCriteria->add(LoggmapFixPeer::IDLOG_SERIAL, $criteria->remove(LoggmapFixPeer::IDLOG_SERIAL), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(LoggmapFixPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(LoggmapFixPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof LoggmapFix) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(LoggmapFixPeer::IDLOG_SERIAL, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(LoggmapFix $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(LoggmapFixPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(LoggmapFixPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(LoggmapFixPeer::DATABASE_NAME, LoggmapFixPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = LoggmapFixPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(LoggmapFixPeer::DATABASE_NAME);

		$criteria->add(LoggmapFixPeer::IDLOG_SERIAL, $pk);


		$v = LoggmapFixPeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(LoggmapFixPeer::IDLOG_SERIAL, $pks, Criteria::IN);
			$objs = LoggmapFixPeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BaseLoggmapFixPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/gmap/map/LoggmapFixMapBuilder.php';
	Propel::registerMapBuilder('lib.model.gmap.map.LoggmapFixMapBuilder');
}
