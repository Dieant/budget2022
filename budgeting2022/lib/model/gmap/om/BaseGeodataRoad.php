<?php


abstract class BaseGeodataRoad extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id;


	
	protected $nama;


	
	protected $latlong;


	
	protected $tipe;


	
	protected $geojson;


	
	protected $tanggal;


	
	protected $deleteflag;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getId()
	{

		return $this->id;
	}

	
	public function getNama()
	{

		return $this->nama;
	}

	
	public function getLatlong()
	{

		return $this->latlong;
	}

	
	public function getTipe()
	{

		return $this->tipe;
	}

	
	public function getGeojson()
	{

		return $this->geojson;
	}

	
	public function getTanggal($format = 'Y-m-d H:i:s')
	{

		if ($this->tanggal === null || $this->tanggal === '') {
			return null;
		} elseif (!is_int($this->tanggal)) {
						$ts = strtotime($this->tanggal);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [tanggal] as date/time value: " . var_export($this->tanggal, true));
			}
		} else {
			$ts = $this->tanggal;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getDeleteflag()
	{

		return $this->deleteflag;
	}

	
	public function setId($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = GeodataRoadPeer::ID;
		}

	} 
	
	public function setNama($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nama !== $v) {
			$this->nama = $v;
			$this->modifiedColumns[] = GeodataRoadPeer::NAMA;
		}

	} 
	
	public function setLatlong($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->latlong !== $v) {
			$this->latlong = $v;
			$this->modifiedColumns[] = GeodataRoadPeer::LATLONG;
		}

	} 
	
	public function setTipe($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->tipe !== $v) {
			$this->tipe = $v;
			$this->modifiedColumns[] = GeodataRoadPeer::TIPE;
		}

	} 
	
	public function setGeojson($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->geojson !== $v) {
			$this->geojson = $v;
			$this->modifiedColumns[] = GeodataRoadPeer::GEOJSON;
		}

	} 
	
	public function setTanggal($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [tanggal] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->tanggal !== $ts) {
			$this->tanggal = $ts;
			$this->modifiedColumns[] = GeodataRoadPeer::TANGGAL;
		}

	} 
	
	public function setDeleteflag($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->deleteflag !== $v) {
			$this->deleteflag = $v;
			$this->modifiedColumns[] = GeodataRoadPeer::DELETEFLAG;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getInt($startcol + 0);

			$this->nama = $rs->getString($startcol + 1);

			$this->latlong = $rs->getString($startcol + 2);

			$this->tipe = $rs->getInt($startcol + 3);

			$this->geojson = $rs->getString($startcol + 4);

			$this->tanggal = $rs->getTimestamp($startcol + 5, null);

			$this->deleteflag = $rs->getInt($startcol + 6);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 7; 
		} catch (Exception $e) {
			throw new PropelException("Error populating GeodataRoad object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(GeodataRoadPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			GeodataRoadPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(GeodataRoadPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = GeodataRoadPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += GeodataRoadPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = GeodataRoadPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = GeodataRoadPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getNama();
				break;
			case 2:
				return $this->getLatlong();
				break;
			case 3:
				return $this->getTipe();
				break;
			case 4:
				return $this->getGeojson();
				break;
			case 5:
				return $this->getTanggal();
				break;
			case 6:
				return $this->getDeleteflag();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = GeodataRoadPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getNama(),
			$keys[2] => $this->getLatlong(),
			$keys[3] => $this->getTipe(),
			$keys[4] => $this->getGeojson(),
			$keys[5] => $this->getTanggal(),
			$keys[6] => $this->getDeleteflag(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = GeodataRoadPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setNama($value);
				break;
			case 2:
				$this->setLatlong($value);
				break;
			case 3:
				$this->setTipe($value);
				break;
			case 4:
				$this->setGeojson($value);
				break;
			case 5:
				$this->setTanggal($value);
				break;
			case 6:
				$this->setDeleteflag($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = GeodataRoadPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setNama($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setLatlong($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setTipe($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setGeojson($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setTanggal($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setDeleteflag($arr[$keys[6]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(GeodataRoadPeer::DATABASE_NAME);

		if ($this->isColumnModified(GeodataRoadPeer::ID)) $criteria->add(GeodataRoadPeer::ID, $this->id);
		if ($this->isColumnModified(GeodataRoadPeer::NAMA)) $criteria->add(GeodataRoadPeer::NAMA, $this->nama);
		if ($this->isColumnModified(GeodataRoadPeer::LATLONG)) $criteria->add(GeodataRoadPeer::LATLONG, $this->latlong);
		if ($this->isColumnModified(GeodataRoadPeer::TIPE)) $criteria->add(GeodataRoadPeer::TIPE, $this->tipe);
		if ($this->isColumnModified(GeodataRoadPeer::GEOJSON)) $criteria->add(GeodataRoadPeer::GEOJSON, $this->geojson);
		if ($this->isColumnModified(GeodataRoadPeer::TANGGAL)) $criteria->add(GeodataRoadPeer::TANGGAL, $this->tanggal);
		if ($this->isColumnModified(GeodataRoadPeer::DELETEFLAG)) $criteria->add(GeodataRoadPeer::DELETEFLAG, $this->deleteflag);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(GeodataRoadPeer::DATABASE_NAME);

		$criteria->add(GeodataRoadPeer::ID, $this->id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setNama($this->nama);

		$copyObj->setLatlong($this->latlong);

		$copyObj->setTipe($this->tipe);

		$copyObj->setGeojson($this->geojson);

		$copyObj->setTanggal($this->tanggal);

		$copyObj->setDeleteflag($this->deleteflag);


		$copyObj->setNew(true);

		$copyObj->setId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new GeodataRoadPeer();
		}
		return self::$peer;
	}

} 