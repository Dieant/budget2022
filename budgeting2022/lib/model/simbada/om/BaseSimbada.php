<?php


abstract class BaseSimbada extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id;


	
	protected $tipe_kib;


	
	protected $unit_id;


	
	protected $kode_lokasi;


	
	protected $nama_lokasi;


	
	protected $no_register;


	
	protected $kode_barang;


	
	protected $nama_barang;


	
	protected $kondisi;


	
	protected $merk;


	
	protected $tipe;


	
	protected $alamat;


	
	protected $keterangan;


	
	protected $tahun;


	
	protected $created_at;


	
	protected $updated_at;


	
	protected $deleted_at;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getId()
	{

		return $this->id;
	}

	
	public function getTipeKib()
	{

		return $this->tipe_kib;
	}

	
	public function getUnitId()
	{

		return $this->unit_id;
	}

	
	public function getKodeLokasi()
	{

		return $this->kode_lokasi;
	}

	
	public function getNamaLokasi()
	{

		return $this->nama_lokasi;
	}

	
	public function getNoRegister()
	{

		return $this->no_register;
	}

	
	public function getKodeBarang()
	{

		return $this->kode_barang;
	}

	
	public function getNamaBarang()
	{

		return $this->nama_barang;
	}

	
	public function getKondisi()
	{

		return $this->kondisi;
	}

	
	public function getMerk()
	{

		return $this->merk;
	}

	
	public function getTipe()
	{

		return $this->tipe;
	}

	
	public function getAlamat()
	{

		return $this->alamat;
	}

	
	public function getKeterangan()
	{

		return $this->keterangan;
	}

	
	public function getTahun()
	{

		return $this->tahun;
	}

	
	public function getCreatedAt($format = 'Y-m-d H:i:s')
	{

		if ($this->created_at === null || $this->created_at === '') {
			return null;
		} elseif (!is_int($this->created_at)) {
						$ts = strtotime($this->created_at);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [created_at] as date/time value: " . var_export($this->created_at, true));
			}
		} else {
			$ts = $this->created_at;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getUpdatedAt($format = 'Y-m-d H:i:s')
	{

		if ($this->updated_at === null || $this->updated_at === '') {
			return null;
		} elseif (!is_int($this->updated_at)) {
						$ts = strtotime($this->updated_at);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [updated_at] as date/time value: " . var_export($this->updated_at, true));
			}
		} else {
			$ts = $this->updated_at;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getDeletedAt($format = 'Y-m-d H:i:s')
	{

		if ($this->deleted_at === null || $this->deleted_at === '') {
			return null;
		} elseif (!is_int($this->deleted_at)) {
						$ts = strtotime($this->deleted_at);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [deleted_at] as date/time value: " . var_export($this->deleted_at, true));
			}
		} else {
			$ts = $this->deleted_at;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function setId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = SimbadaPeer::ID;
		}

	} 
	
	public function setTipeKib($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->tipe_kib !== $v) {
			$this->tipe_kib = $v;
			$this->modifiedColumns[] = SimbadaPeer::TIPE_KIB;
		}

	} 
	
	public function setUnitId($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->unit_id !== $v) {
			$this->unit_id = $v;
			$this->modifiedColumns[] = SimbadaPeer::UNIT_ID;
		}

	} 
	
	public function setKodeLokasi($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_lokasi !== $v) {
			$this->kode_lokasi = $v;
			$this->modifiedColumns[] = SimbadaPeer::KODE_LOKASI;
		}

	} 
	
	public function setNamaLokasi($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nama_lokasi !== $v) {
			$this->nama_lokasi = $v;
			$this->modifiedColumns[] = SimbadaPeer::NAMA_LOKASI;
		}

	} 
	
	public function setNoRegister($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->no_register !== $v) {
			$this->no_register = $v;
			$this->modifiedColumns[] = SimbadaPeer::NO_REGISTER;
		}

	} 
	
	public function setKodeBarang($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kode_barang !== $v) {
			$this->kode_barang = $v;
			$this->modifiedColumns[] = SimbadaPeer::KODE_BARANG;
		}

	} 
	
	public function setNamaBarang($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nama_barang !== $v) {
			$this->nama_barang = $v;
			$this->modifiedColumns[] = SimbadaPeer::NAMA_BARANG;
		}

	} 
	
	public function setKondisi($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->kondisi !== $v) {
			$this->kondisi = $v;
			$this->modifiedColumns[] = SimbadaPeer::KONDISI;
		}

	} 
	
	public function setMerk($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->merk !== $v) {
			$this->merk = $v;
			$this->modifiedColumns[] = SimbadaPeer::MERK;
		}

	} 
	
	public function setTipe($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->tipe !== $v) {
			$this->tipe = $v;
			$this->modifiedColumns[] = SimbadaPeer::TIPE;
		}

	} 
	
	public function setAlamat($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->alamat !== $v) {
			$this->alamat = $v;
			$this->modifiedColumns[] = SimbadaPeer::ALAMAT;
		}

	} 
	
	public function setKeterangan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->keterangan !== $v) {
			$this->keterangan = $v;
			$this->modifiedColumns[] = SimbadaPeer::KETERANGAN;
		}

	} 
	
	public function setTahun($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->tahun !== $v) {
			$this->tahun = $v;
			$this->modifiedColumns[] = SimbadaPeer::TAHUN;
		}

	} 
	
	public function setCreatedAt($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [created_at] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->created_at !== $ts) {
			$this->created_at = $ts;
			$this->modifiedColumns[] = SimbadaPeer::CREATED_AT;
		}

	} 
	
	public function setUpdatedAt($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [updated_at] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->updated_at !== $ts) {
			$this->updated_at = $ts;
			$this->modifiedColumns[] = SimbadaPeer::UPDATED_AT;
		}

	} 
	
	public function setDeletedAt($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [deleted_at] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->deleted_at !== $ts) {
			$this->deleted_at = $ts;
			$this->modifiedColumns[] = SimbadaPeer::DELETED_AT;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getString($startcol + 0);

			$this->tipe_kib = $rs->getString($startcol + 1);

			$this->unit_id = $rs->getString($startcol + 2);

			$this->kode_lokasi = $rs->getString($startcol + 3);

			$this->nama_lokasi = $rs->getString($startcol + 4);

			$this->no_register = $rs->getString($startcol + 5);

			$this->kode_barang = $rs->getString($startcol + 6);

			$this->nama_barang = $rs->getString($startcol + 7);

			$this->kondisi = $rs->getString($startcol + 8);

			$this->merk = $rs->getString($startcol + 9);

			$this->tipe = $rs->getString($startcol + 10);

			$this->alamat = $rs->getString($startcol + 11);

			$this->keterangan = $rs->getString($startcol + 12);

			$this->tahun = $rs->getString($startcol + 13);

			$this->created_at = $rs->getTimestamp($startcol + 14, null);

			$this->updated_at = $rs->getTimestamp($startcol + 15, null);

			$this->deleted_at = $rs->getTimestamp($startcol + 16, null);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 17; 
		} catch (Exception $e) {
			throw new PropelException("Error populating Simbada object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(SimbadaPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			SimbadaPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
    if ($this->isNew() && !$this->isColumnModified(SimbadaPeer::CREATED_AT))
    {
      $this->setCreatedAt(time());
    }

    if ($this->isModified() && !$this->isColumnModified(SimbadaPeer::UPDATED_AT))
    {
      $this->setUpdatedAt(time());
    }

		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(SimbadaPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = SimbadaPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += SimbadaPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = SimbadaPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = SimbadaPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getTipeKib();
				break;
			case 2:
				return $this->getUnitId();
				break;
			case 3:
				return $this->getKodeLokasi();
				break;
			case 4:
				return $this->getNamaLokasi();
				break;
			case 5:
				return $this->getNoRegister();
				break;
			case 6:
				return $this->getKodeBarang();
				break;
			case 7:
				return $this->getNamaBarang();
				break;
			case 8:
				return $this->getKondisi();
				break;
			case 9:
				return $this->getMerk();
				break;
			case 10:
				return $this->getTipe();
				break;
			case 11:
				return $this->getAlamat();
				break;
			case 12:
				return $this->getKeterangan();
				break;
			case 13:
				return $this->getTahun();
				break;
			case 14:
				return $this->getCreatedAt();
				break;
			case 15:
				return $this->getUpdatedAt();
				break;
			case 16:
				return $this->getDeletedAt();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = SimbadaPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getTipeKib(),
			$keys[2] => $this->getUnitId(),
			$keys[3] => $this->getKodeLokasi(),
			$keys[4] => $this->getNamaLokasi(),
			$keys[5] => $this->getNoRegister(),
			$keys[6] => $this->getKodeBarang(),
			$keys[7] => $this->getNamaBarang(),
			$keys[8] => $this->getKondisi(),
			$keys[9] => $this->getMerk(),
			$keys[10] => $this->getTipe(),
			$keys[11] => $this->getAlamat(),
			$keys[12] => $this->getKeterangan(),
			$keys[13] => $this->getTahun(),
			$keys[14] => $this->getCreatedAt(),
			$keys[15] => $this->getUpdatedAt(),
			$keys[16] => $this->getDeletedAt(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = SimbadaPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setTipeKib($value);
				break;
			case 2:
				$this->setUnitId($value);
				break;
			case 3:
				$this->setKodeLokasi($value);
				break;
			case 4:
				$this->setNamaLokasi($value);
				break;
			case 5:
				$this->setNoRegister($value);
				break;
			case 6:
				$this->setKodeBarang($value);
				break;
			case 7:
				$this->setNamaBarang($value);
				break;
			case 8:
				$this->setKondisi($value);
				break;
			case 9:
				$this->setMerk($value);
				break;
			case 10:
				$this->setTipe($value);
				break;
			case 11:
				$this->setAlamat($value);
				break;
			case 12:
				$this->setKeterangan($value);
				break;
			case 13:
				$this->setTahun($value);
				break;
			case 14:
				$this->setCreatedAt($value);
				break;
			case 15:
				$this->setUpdatedAt($value);
				break;
			case 16:
				$this->setDeletedAt($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = SimbadaPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setTipeKib($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setUnitId($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setKodeLokasi($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setNamaLokasi($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setNoRegister($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setKodeBarang($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setNamaBarang($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setKondisi($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setMerk($arr[$keys[9]]);
		if (array_key_exists($keys[10], $arr)) $this->setTipe($arr[$keys[10]]);
		if (array_key_exists($keys[11], $arr)) $this->setAlamat($arr[$keys[11]]);
		if (array_key_exists($keys[12], $arr)) $this->setKeterangan($arr[$keys[12]]);
		if (array_key_exists($keys[13], $arr)) $this->setTahun($arr[$keys[13]]);
		if (array_key_exists($keys[14], $arr)) $this->setCreatedAt($arr[$keys[14]]);
		if (array_key_exists($keys[15], $arr)) $this->setUpdatedAt($arr[$keys[15]]);
		if (array_key_exists($keys[16], $arr)) $this->setDeletedAt($arr[$keys[16]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(SimbadaPeer::DATABASE_NAME);

		if ($this->isColumnModified(SimbadaPeer::ID)) $criteria->add(SimbadaPeer::ID, $this->id);
		if ($this->isColumnModified(SimbadaPeer::TIPE_KIB)) $criteria->add(SimbadaPeer::TIPE_KIB, $this->tipe_kib);
		if ($this->isColumnModified(SimbadaPeer::UNIT_ID)) $criteria->add(SimbadaPeer::UNIT_ID, $this->unit_id);
		if ($this->isColumnModified(SimbadaPeer::KODE_LOKASI)) $criteria->add(SimbadaPeer::KODE_LOKASI, $this->kode_lokasi);
		if ($this->isColumnModified(SimbadaPeer::NAMA_LOKASI)) $criteria->add(SimbadaPeer::NAMA_LOKASI, $this->nama_lokasi);
		if ($this->isColumnModified(SimbadaPeer::NO_REGISTER)) $criteria->add(SimbadaPeer::NO_REGISTER, $this->no_register);
		if ($this->isColumnModified(SimbadaPeer::KODE_BARANG)) $criteria->add(SimbadaPeer::KODE_BARANG, $this->kode_barang);
		if ($this->isColumnModified(SimbadaPeer::NAMA_BARANG)) $criteria->add(SimbadaPeer::NAMA_BARANG, $this->nama_barang);
		if ($this->isColumnModified(SimbadaPeer::KONDISI)) $criteria->add(SimbadaPeer::KONDISI, $this->kondisi);
		if ($this->isColumnModified(SimbadaPeer::MERK)) $criteria->add(SimbadaPeer::MERK, $this->merk);
		if ($this->isColumnModified(SimbadaPeer::TIPE)) $criteria->add(SimbadaPeer::TIPE, $this->tipe);
		if ($this->isColumnModified(SimbadaPeer::ALAMAT)) $criteria->add(SimbadaPeer::ALAMAT, $this->alamat);
		if ($this->isColumnModified(SimbadaPeer::KETERANGAN)) $criteria->add(SimbadaPeer::KETERANGAN, $this->keterangan);
		if ($this->isColumnModified(SimbadaPeer::TAHUN)) $criteria->add(SimbadaPeer::TAHUN, $this->tahun);
		if ($this->isColumnModified(SimbadaPeer::CREATED_AT)) $criteria->add(SimbadaPeer::CREATED_AT, $this->created_at);
		if ($this->isColumnModified(SimbadaPeer::UPDATED_AT)) $criteria->add(SimbadaPeer::UPDATED_AT, $this->updated_at);
		if ($this->isColumnModified(SimbadaPeer::DELETED_AT)) $criteria->add(SimbadaPeer::DELETED_AT, $this->deleted_at);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(SimbadaPeer::DATABASE_NAME);

		$criteria->add(SimbadaPeer::ID, $this->id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setTipeKib($this->tipe_kib);

		$copyObj->setUnitId($this->unit_id);

		$copyObj->setKodeLokasi($this->kode_lokasi);

		$copyObj->setNamaLokasi($this->nama_lokasi);

		$copyObj->setNoRegister($this->no_register);

		$copyObj->setKodeBarang($this->kode_barang);

		$copyObj->setNamaBarang($this->nama_barang);

		$copyObj->setKondisi($this->kondisi);

		$copyObj->setMerk($this->merk);

		$copyObj->setTipe($this->tipe);

		$copyObj->setAlamat($this->alamat);

		$copyObj->setKeterangan($this->keterangan);

		$copyObj->setTahun($this->tahun);

		$copyObj->setCreatedAt($this->created_at);

		$copyObj->setUpdatedAt($this->updated_at);

		$copyObj->setDeletedAt($this->deleted_at);


		$copyObj->setNew(true);

		$copyObj->setId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new SimbadaPeer();
		}
		return self::$peer;
	}

} 