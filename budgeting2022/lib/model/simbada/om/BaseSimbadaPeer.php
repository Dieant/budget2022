<?php


abstract class BaseSimbadaPeer {

	
	const DATABASE_NAME = 'simbada';

	
	const TABLE_NAME = 'simbada';

	
	const CLASS_DEFAULT = 'lib.model.simbada.Simbada';

	
	const NUM_COLUMNS = 17;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const ID = 'simbada.ID';

	
	const TIPE_KIB = 'simbada.TIPE_KIB';

	
	const UNIT_ID = 'simbada.UNIT_ID';

	
	const KODE_LOKASI = 'simbada.KODE_LOKASI';

	
	const NAMA_LOKASI = 'simbada.NAMA_LOKASI';

	
	const NO_REGISTER = 'simbada.NO_REGISTER';

	
	const KODE_BARANG = 'simbada.KODE_BARANG';

	
	const NAMA_BARANG = 'simbada.NAMA_BARANG';

	
	const KONDISI = 'simbada.KONDISI';

	
	const MERK = 'simbada.MERK';

	
	const TIPE = 'simbada.TIPE';

	
	const ALAMAT = 'simbada.ALAMAT';

	
	const KETERANGAN = 'simbada.KETERANGAN';

	
	const TAHUN = 'simbada.TAHUN';

	
	const CREATED_AT = 'simbada.CREATED_AT';

	
	const UPDATED_AT = 'simbada.UPDATED_AT';

	
	const DELETED_AT = 'simbada.DELETED_AT';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('Id', 'TipeKib', 'UnitId', 'KodeLokasi', 'NamaLokasi', 'NoRegister', 'KodeBarang', 'NamaBarang', 'Kondisi', 'Merk', 'Tipe', 'Alamat', 'Keterangan', 'Tahun', 'CreatedAt', 'UpdatedAt', 'DeletedAt', ),
		BasePeer::TYPE_COLNAME => array (SimbadaPeer::ID, SimbadaPeer::TIPE_KIB, SimbadaPeer::UNIT_ID, SimbadaPeer::KODE_LOKASI, SimbadaPeer::NAMA_LOKASI, SimbadaPeer::NO_REGISTER, SimbadaPeer::KODE_BARANG, SimbadaPeer::NAMA_BARANG, SimbadaPeer::KONDISI, SimbadaPeer::MERK, SimbadaPeer::TIPE, SimbadaPeer::ALAMAT, SimbadaPeer::KETERANGAN, SimbadaPeer::TAHUN, SimbadaPeer::CREATED_AT, SimbadaPeer::UPDATED_AT, SimbadaPeer::DELETED_AT, ),
		BasePeer::TYPE_FIELDNAME => array ('id', 'tipe_kib', 'unit_id', 'kode_lokasi', 'nama_lokasi', 'no_register', 'kode_barang', 'nama_barang', 'kondisi', 'merk', 'tipe', 'alamat', 'keterangan', 'tahun', 'created_at', 'updated_at', 'deleted_at', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('Id' => 0, 'TipeKib' => 1, 'UnitId' => 2, 'KodeLokasi' => 3, 'NamaLokasi' => 4, 'NoRegister' => 5, 'KodeBarang' => 6, 'NamaBarang' => 7, 'Kondisi' => 8, 'Merk' => 9, 'Tipe' => 10, 'Alamat' => 11, 'Keterangan' => 12, 'Tahun' => 13, 'CreatedAt' => 14, 'UpdatedAt' => 15, 'DeletedAt' => 16, ),
		BasePeer::TYPE_COLNAME => array (SimbadaPeer::ID => 0, SimbadaPeer::TIPE_KIB => 1, SimbadaPeer::UNIT_ID => 2, SimbadaPeer::KODE_LOKASI => 3, SimbadaPeer::NAMA_LOKASI => 4, SimbadaPeer::NO_REGISTER => 5, SimbadaPeer::KODE_BARANG => 6, SimbadaPeer::NAMA_BARANG => 7, SimbadaPeer::KONDISI => 8, SimbadaPeer::MERK => 9, SimbadaPeer::TIPE => 10, SimbadaPeer::ALAMAT => 11, SimbadaPeer::KETERANGAN => 12, SimbadaPeer::TAHUN => 13, SimbadaPeer::CREATED_AT => 14, SimbadaPeer::UPDATED_AT => 15, SimbadaPeer::DELETED_AT => 16, ),
		BasePeer::TYPE_FIELDNAME => array ('id' => 0, 'tipe_kib' => 1, 'unit_id' => 2, 'kode_lokasi' => 3, 'nama_lokasi' => 4, 'no_register' => 5, 'kode_barang' => 6, 'nama_barang' => 7, 'kondisi' => 8, 'merk' => 9, 'tipe' => 10, 'alamat' => 11, 'keterangan' => 12, 'tahun' => 13, 'created_at' => 14, 'updated_at' => 15, 'deleted_at' => 16, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/simbada/map/SimbadaMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.simbada.map.SimbadaMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = SimbadaPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(SimbadaPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(SimbadaPeer::ID);

		$criteria->addSelectColumn(SimbadaPeer::TIPE_KIB);

		$criteria->addSelectColumn(SimbadaPeer::UNIT_ID);

		$criteria->addSelectColumn(SimbadaPeer::KODE_LOKASI);

		$criteria->addSelectColumn(SimbadaPeer::NAMA_LOKASI);

		$criteria->addSelectColumn(SimbadaPeer::NO_REGISTER);

		$criteria->addSelectColumn(SimbadaPeer::KODE_BARANG);

		$criteria->addSelectColumn(SimbadaPeer::NAMA_BARANG);

		$criteria->addSelectColumn(SimbadaPeer::KONDISI);

		$criteria->addSelectColumn(SimbadaPeer::MERK);

		$criteria->addSelectColumn(SimbadaPeer::TIPE);

		$criteria->addSelectColumn(SimbadaPeer::ALAMAT);

		$criteria->addSelectColumn(SimbadaPeer::KETERANGAN);

		$criteria->addSelectColumn(SimbadaPeer::TAHUN);

		$criteria->addSelectColumn(SimbadaPeer::CREATED_AT);

		$criteria->addSelectColumn(SimbadaPeer::UPDATED_AT);

		$criteria->addSelectColumn(SimbadaPeer::DELETED_AT);

	}

	const COUNT = 'COUNT(simbada.ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT simbada.ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(SimbadaPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(SimbadaPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = SimbadaPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = SimbadaPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return SimbadaPeer::populateObjects(SimbadaPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			SimbadaPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = SimbadaPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return SimbadaPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(SimbadaPeer::ID);
			$selectCriteria->add(SimbadaPeer::ID, $criteria->remove(SimbadaPeer::ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(SimbadaPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(SimbadaPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof Simbada) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(SimbadaPeer::ID, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(Simbada $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(SimbadaPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(SimbadaPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(SimbadaPeer::DATABASE_NAME, SimbadaPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = SimbadaPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(SimbadaPeer::DATABASE_NAME);

		$criteria->add(SimbadaPeer::ID, $pk);


		$v = SimbadaPeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(SimbadaPeer::ID, $pks, Criteria::IN);
			$objs = SimbadaPeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BaseSimbadaPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/simbada/map/SimbadaMapBuilder.php';
	Propel::registerMapBuilder('lib.model.simbada.map.SimbadaMapBuilder');
}
