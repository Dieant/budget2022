<?php



class JabatanStrukturalMapBuilder {

	
	const CLASS_NAME = 'lib.model.performance.map.JabatanStrukturalMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('performance');

		$tMap = $this->dbMap->addTable('eperformance.jabatan_struktural');
		$tMap->setPhpName('JabatanStruktural');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('NAMA', 'Nama', 'string', CreoleTypes::VARCHAR, false, null);

		$tMap->addColumn('ESELON', 'Eselon', 'string', CreoleTypes::VARCHAR, false, 5);

		$tMap->addColumn('JENIS_INDIKATOR_KINERJA', 'JenisIndikatorKinerja', 'int', CreoleTypes::SMALLINT, false, null);

		$tMap->addColumn('SKPD_ID', 'SkpdId', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('NOMER', 'Nomer', 'int', CreoleTypes::SMALLINT, false, null);

		$tMap->addColumn('IS_ASISTEN', 'IsAsisten', 'boolean', CreoleTypes::BOOLEAN, true, null);

		$tMap->addColumn('NOMER_URUT', 'NomerUrut', 'int', CreoleTypes::SMALLINT, false, null);

	} 
} 