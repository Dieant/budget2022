<?php



class MasterSkpdMapBuilder {

	
	const CLASS_NAME = 'lib.model.performance.map.MasterSkpdMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('performance');

		$tMap = $this->dbMap->addTable('eperformance.master_skpd');
		$tMap->setPhpName('MasterSkpd');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('SKPD_ID', 'SkpdId', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('SKPD_KODE', 'SkpdKode', 'string', CreoleTypes::VARCHAR, true, 8);

	} 
} 