<?php



class PegawaiPerformanceMapBuilder {

	
	const CLASS_NAME = 'lib.model.performance.map.PegawaiPerformanceMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('performance');

		$tMap = $this->dbMap->addTable('eperformance.pegawai');
		$tMap->setPhpName('PegawaiPerformance');

		$tMap->setUseIdGenerator(false);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('NIP', 'Nip', 'string', CreoleTypes::VARCHAR, false, 30);

		$tMap->addColumn('NAMA', 'Nama', 'string', CreoleTypes::VARCHAR, false, 60);

		$tMap->addColumn('GOLONGAN', 'Golongan', 'string', CreoleTypes::VARCHAR, false, 50);

		$tMap->addColumn('TMT', 'Tmt', 'int', CreoleTypes::DATE, false, null);

		$tMap->addColumn('PENDIDIKAN_TERAKHIR', 'PendidikanTerakhir', 'string', CreoleTypes::VARCHAR, false, 256);

		$tMap->addColumn('SKPD_ID', 'SkpdId', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('ATASAN_ID', 'AtasanId', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('IS_ATASAN', 'IsAtasan', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('LEVEL_STRUKTURAL', 'LevelStruktural', 'int', CreoleTypes::SMALLINT, false, null);

		$tMap->addColumn('PASSWORD', 'Password', 'string', CreoleTypes::VARCHAR, false, 36);

		$tMap->addColumn('USER_ID', 'UserId', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('JUMLAH_ANAK_BUAH', 'JumlahAnakBuah', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('NIP_LAMA', 'NipLama', 'string', CreoleTypes::VARCHAR, false, 30);

		$tMap->addColumn('STATUS', 'Status', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('CREATED_AT', 'CreatedAt', 'int', CreoleTypes::TIMESTAMP, false, null);

		$tMap->addColumn('DELETED_AT', 'DeletedAt', 'int', CreoleTypes::TIMESTAMP, false, null);

		$tMap->addColumn('UPDATED_AT', 'UpdatedAt', 'int', CreoleTypes::TIMESTAMP, false, null);

		$tMap->addColumn('USERNAME_EPROJECT', 'UsernameEproject', 'string', CreoleTypes::VARCHAR, false, 255);

		$tMap->addColumn('ID_FP', 'IdFp', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('IS_OPERASIONAL', 'IsOperasional', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('TGL_MASUK', 'TglMasuk', 'int', CreoleTypes::DATE, false, null);

		$tMap->addColumn('IS_OUT', 'IsOut', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('JABATAN_ID', 'JabatanId', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('TGL_KELUAR', 'TglKeluar', 'int', CreoleTypes::DATE, false, null);

		$tMap->addColumn('BOBOT_JABATAN', 'BobotJabatan', 'double', CreoleTypes::NUMERIC, false, null);

		$tMap->addColumn('IS_KA_UPTD', 'IsKaUptd', 'boolean', CreoleTypes::BOOLEAN, true, null);

		$tMap->addColumn('IS_STRUKTURAL', 'IsStruktural', 'boolean', CreoleTypes::BOOLEAN, true, null);

		$tMap->addColumn('IS_KUNCI_AKTIFITAS', 'IsKunciAktifitas', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('LOCK_CAPAIAN_INDIKATOR', 'LockCapaianIndikator', 'boolean', CreoleTypes::BOOLEAN, false, null);

		$tMap->addColumn('PELAYANAN_KESEKRETARIATAN', 'PelayananKesekretariatan', 'int', CreoleTypes::SMALLINT, true, null);

		$tMap->addColumn('JABATAN_STRUKTURAL_ID', 'JabatanStrukturalId', 'int', CreoleTypes::INTEGER, false, null);

		$tMap->addColumn('JABATAN_LAIN', 'JabatanLain', 'string', CreoleTypes::LONGVARCHAR, false, null);

		$tMap->addColumn('IS_ONLY_SKP', 'IsOnlySkp', 'boolean', CreoleTypes::BOOLEAN, true, null);

	} 
} 