<?php

/**
 * Subclass for representing a row from the 'eperformance.pegawai' table.
 *
 * 
 *
 * @package lib.model.performance
 */
class PegawaiPerformance extends BasePegawaiPerformance {

    public function getNipNama() {

        return $this->nip . ' - ' . $this->nama;
    }
    
    public function getNipNamaBappeko() {

        return 'b' . $this->nip . ' - ' . $this->nama;
    }

}
