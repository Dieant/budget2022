<?php


abstract class BasePegawaiPerformancePeer {

	
	const DATABASE_NAME = 'performance';

	
	const TABLE_NAME = 'eperformance.pegawai';

	
	const CLASS_DEFAULT = 'lib.model.performance.PegawaiPerformance';

	
	const NUM_COLUMNS = 34;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const ID = 'eperformance.pegawai.ID';

	
	const NIP = 'eperformance.pegawai.NIP';

	
	const NAMA = 'eperformance.pegawai.NAMA';

	
	const GOLONGAN = 'eperformance.pegawai.GOLONGAN';

	
	const TMT = 'eperformance.pegawai.TMT';

	
	const PENDIDIKAN_TERAKHIR = 'eperformance.pegawai.PENDIDIKAN_TERAKHIR';

	
	const SKPD_ID = 'eperformance.pegawai.SKPD_ID';

	
	const ATASAN_ID = 'eperformance.pegawai.ATASAN_ID';

	
	const IS_ATASAN = 'eperformance.pegawai.IS_ATASAN';

	
	const LEVEL_STRUKTURAL = 'eperformance.pegawai.LEVEL_STRUKTURAL';

	
	const PASSWORD = 'eperformance.pegawai.PASSWORD';

	
	const USER_ID = 'eperformance.pegawai.USER_ID';

	
	const JUMLAH_ANAK_BUAH = 'eperformance.pegawai.JUMLAH_ANAK_BUAH';

	
	const NIP_LAMA = 'eperformance.pegawai.NIP_LAMA';

	
	const STATUS = 'eperformance.pegawai.STATUS';

	
	const CREATED_AT = 'eperformance.pegawai.CREATED_AT';

	
	const DELETED_AT = 'eperformance.pegawai.DELETED_AT';

	
	const UPDATED_AT = 'eperformance.pegawai.UPDATED_AT';

	
	const USERNAME_EPROJECT = 'eperformance.pegawai.USERNAME_EPROJECT';

	
	const ID_FP = 'eperformance.pegawai.ID_FP';

	
	const IS_OPERASIONAL = 'eperformance.pegawai.IS_OPERASIONAL';

	
	const TGL_MASUK = 'eperformance.pegawai.TGL_MASUK';

	
	const IS_OUT = 'eperformance.pegawai.IS_OUT';

	
	const JABATAN_ID = 'eperformance.pegawai.JABATAN_ID';

	
	const TGL_KELUAR = 'eperformance.pegawai.TGL_KELUAR';

	
	const BOBOT_JABATAN = 'eperformance.pegawai.BOBOT_JABATAN';

	
	const IS_KA_UPTD = 'eperformance.pegawai.IS_KA_UPTD';

	
	const IS_STRUKTURAL = 'eperformance.pegawai.IS_STRUKTURAL';

	
	const IS_KUNCI_AKTIFITAS = 'eperformance.pegawai.IS_KUNCI_AKTIFITAS';

	
	const LOCK_CAPAIAN_INDIKATOR = 'eperformance.pegawai.LOCK_CAPAIAN_INDIKATOR';

	
	const PELAYANAN_KESEKRETARIATAN = 'eperformance.pegawai.PELAYANAN_KESEKRETARIATAN';

	
	const JABATAN_STRUKTURAL_ID = 'eperformance.pegawai.JABATAN_STRUKTURAL_ID';

	
	const JABATAN_LAIN = 'eperformance.pegawai.JABATAN_LAIN';

	
	const IS_ONLY_SKP = 'eperformance.pegawai.IS_ONLY_SKP';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('Id', 'Nip', 'Nama', 'Golongan', 'Tmt', 'PendidikanTerakhir', 'SkpdId', 'AtasanId', 'IsAtasan', 'LevelStruktural', 'Password', 'UserId', 'JumlahAnakBuah', 'NipLama', 'Status', 'CreatedAt', 'DeletedAt', 'UpdatedAt', 'UsernameEproject', 'IdFp', 'IsOperasional', 'TglMasuk', 'IsOut', 'JabatanId', 'TglKeluar', 'BobotJabatan', 'IsKaUptd', 'IsStruktural', 'IsKunciAktifitas', 'LockCapaianIndikator', 'PelayananKesekretariatan', 'JabatanStrukturalId', 'JabatanLain', 'IsOnlySkp', ),
		BasePeer::TYPE_COLNAME => array (PegawaiPerformancePeer::ID, PegawaiPerformancePeer::NIP, PegawaiPerformancePeer::NAMA, PegawaiPerformancePeer::GOLONGAN, PegawaiPerformancePeer::TMT, PegawaiPerformancePeer::PENDIDIKAN_TERAKHIR, PegawaiPerformancePeer::SKPD_ID, PegawaiPerformancePeer::ATASAN_ID, PegawaiPerformancePeer::IS_ATASAN, PegawaiPerformancePeer::LEVEL_STRUKTURAL, PegawaiPerformancePeer::PASSWORD, PegawaiPerformancePeer::USER_ID, PegawaiPerformancePeer::JUMLAH_ANAK_BUAH, PegawaiPerformancePeer::NIP_LAMA, PegawaiPerformancePeer::STATUS, PegawaiPerformancePeer::CREATED_AT, PegawaiPerformancePeer::DELETED_AT, PegawaiPerformancePeer::UPDATED_AT, PegawaiPerformancePeer::USERNAME_EPROJECT, PegawaiPerformancePeer::ID_FP, PegawaiPerformancePeer::IS_OPERASIONAL, PegawaiPerformancePeer::TGL_MASUK, PegawaiPerformancePeer::IS_OUT, PegawaiPerformancePeer::JABATAN_ID, PegawaiPerformancePeer::TGL_KELUAR, PegawaiPerformancePeer::BOBOT_JABATAN, PegawaiPerformancePeer::IS_KA_UPTD, PegawaiPerformancePeer::IS_STRUKTURAL, PegawaiPerformancePeer::IS_KUNCI_AKTIFITAS, PegawaiPerformancePeer::LOCK_CAPAIAN_INDIKATOR, PegawaiPerformancePeer::PELAYANAN_KESEKRETARIATAN, PegawaiPerformancePeer::JABATAN_STRUKTURAL_ID, PegawaiPerformancePeer::JABATAN_LAIN, PegawaiPerformancePeer::IS_ONLY_SKP, ),
		BasePeer::TYPE_FIELDNAME => array ('id', 'nip', 'nama', 'golongan', 'tmt', 'pendidikan_terakhir', 'skpd_id', 'atasan_id', 'is_atasan', 'level_struktural', 'password', 'user_id', 'jumlah_anak_buah', 'nip_lama', 'status', 'created_at', 'deleted_at', 'updated_at', 'username_eproject', 'id_fp', 'is_operasional', 'tgl_masuk', 'is_out', 'jabatan_id', 'tgl_keluar', 'bobot_jabatan', 'is_ka_uptd', 'is_struktural', 'is_kunci_aktifitas', 'lock_capaian_indikator', 'pelayanan_kesekretariatan', 'jabatan_struktural_id', 'jabatan_lain', 'is_only_skp', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('Id' => 0, 'Nip' => 1, 'Nama' => 2, 'Golongan' => 3, 'Tmt' => 4, 'PendidikanTerakhir' => 5, 'SkpdId' => 6, 'AtasanId' => 7, 'IsAtasan' => 8, 'LevelStruktural' => 9, 'Password' => 10, 'UserId' => 11, 'JumlahAnakBuah' => 12, 'NipLama' => 13, 'Status' => 14, 'CreatedAt' => 15, 'DeletedAt' => 16, 'UpdatedAt' => 17, 'UsernameEproject' => 18, 'IdFp' => 19, 'IsOperasional' => 20, 'TglMasuk' => 21, 'IsOut' => 22, 'JabatanId' => 23, 'TglKeluar' => 24, 'BobotJabatan' => 25, 'IsKaUptd' => 26, 'IsStruktural' => 27, 'IsKunciAktifitas' => 28, 'LockCapaianIndikator' => 29, 'PelayananKesekretariatan' => 30, 'JabatanStrukturalId' => 31, 'JabatanLain' => 32, 'IsOnlySkp' => 33, ),
		BasePeer::TYPE_COLNAME => array (PegawaiPerformancePeer::ID => 0, PegawaiPerformancePeer::NIP => 1, PegawaiPerformancePeer::NAMA => 2, PegawaiPerformancePeer::GOLONGAN => 3, PegawaiPerformancePeer::TMT => 4, PegawaiPerformancePeer::PENDIDIKAN_TERAKHIR => 5, PegawaiPerformancePeer::SKPD_ID => 6, PegawaiPerformancePeer::ATASAN_ID => 7, PegawaiPerformancePeer::IS_ATASAN => 8, PegawaiPerformancePeer::LEVEL_STRUKTURAL => 9, PegawaiPerformancePeer::PASSWORD => 10, PegawaiPerformancePeer::USER_ID => 11, PegawaiPerformancePeer::JUMLAH_ANAK_BUAH => 12, PegawaiPerformancePeer::NIP_LAMA => 13, PegawaiPerformancePeer::STATUS => 14, PegawaiPerformancePeer::CREATED_AT => 15, PegawaiPerformancePeer::DELETED_AT => 16, PegawaiPerformancePeer::UPDATED_AT => 17, PegawaiPerformancePeer::USERNAME_EPROJECT => 18, PegawaiPerformancePeer::ID_FP => 19, PegawaiPerformancePeer::IS_OPERASIONAL => 20, PegawaiPerformancePeer::TGL_MASUK => 21, PegawaiPerformancePeer::IS_OUT => 22, PegawaiPerformancePeer::JABATAN_ID => 23, PegawaiPerformancePeer::TGL_KELUAR => 24, PegawaiPerformancePeer::BOBOT_JABATAN => 25, PegawaiPerformancePeer::IS_KA_UPTD => 26, PegawaiPerformancePeer::IS_STRUKTURAL => 27, PegawaiPerformancePeer::IS_KUNCI_AKTIFITAS => 28, PegawaiPerformancePeer::LOCK_CAPAIAN_INDIKATOR => 29, PegawaiPerformancePeer::PELAYANAN_KESEKRETARIATAN => 30, PegawaiPerformancePeer::JABATAN_STRUKTURAL_ID => 31, PegawaiPerformancePeer::JABATAN_LAIN => 32, PegawaiPerformancePeer::IS_ONLY_SKP => 33, ),
		BasePeer::TYPE_FIELDNAME => array ('id' => 0, 'nip' => 1, 'nama' => 2, 'golongan' => 3, 'tmt' => 4, 'pendidikan_terakhir' => 5, 'skpd_id' => 6, 'atasan_id' => 7, 'is_atasan' => 8, 'level_struktural' => 9, 'password' => 10, 'user_id' => 11, 'jumlah_anak_buah' => 12, 'nip_lama' => 13, 'status' => 14, 'created_at' => 15, 'deleted_at' => 16, 'updated_at' => 17, 'username_eproject' => 18, 'id_fp' => 19, 'is_operasional' => 20, 'tgl_masuk' => 21, 'is_out' => 22, 'jabatan_id' => 23, 'tgl_keluar' => 24, 'bobot_jabatan' => 25, 'is_ka_uptd' => 26, 'is_struktural' => 27, 'is_kunci_aktifitas' => 28, 'lock_capaian_indikator' => 29, 'pelayanan_kesekretariatan' => 30, 'jabatan_struktural_id' => 31, 'jabatan_lain' => 32, 'is_only_skp' => 33, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/performance/map/PegawaiPerformanceMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.performance.map.PegawaiPerformanceMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = PegawaiPerformancePeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(PegawaiPerformancePeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(PegawaiPerformancePeer::ID);

		$criteria->addSelectColumn(PegawaiPerformancePeer::NIP);

		$criteria->addSelectColumn(PegawaiPerformancePeer::NAMA);

		$criteria->addSelectColumn(PegawaiPerformancePeer::GOLONGAN);

		$criteria->addSelectColumn(PegawaiPerformancePeer::TMT);

		$criteria->addSelectColumn(PegawaiPerformancePeer::PENDIDIKAN_TERAKHIR);

		$criteria->addSelectColumn(PegawaiPerformancePeer::SKPD_ID);

		$criteria->addSelectColumn(PegawaiPerformancePeer::ATASAN_ID);

		$criteria->addSelectColumn(PegawaiPerformancePeer::IS_ATASAN);

		$criteria->addSelectColumn(PegawaiPerformancePeer::LEVEL_STRUKTURAL);

		$criteria->addSelectColumn(PegawaiPerformancePeer::PASSWORD);

		$criteria->addSelectColumn(PegawaiPerformancePeer::USER_ID);

		$criteria->addSelectColumn(PegawaiPerformancePeer::JUMLAH_ANAK_BUAH);

		$criteria->addSelectColumn(PegawaiPerformancePeer::NIP_LAMA);

		$criteria->addSelectColumn(PegawaiPerformancePeer::STATUS);

		$criteria->addSelectColumn(PegawaiPerformancePeer::CREATED_AT);

		$criteria->addSelectColumn(PegawaiPerformancePeer::DELETED_AT);

		$criteria->addSelectColumn(PegawaiPerformancePeer::UPDATED_AT);

		$criteria->addSelectColumn(PegawaiPerformancePeer::USERNAME_EPROJECT);

		$criteria->addSelectColumn(PegawaiPerformancePeer::ID_FP);

		$criteria->addSelectColumn(PegawaiPerformancePeer::IS_OPERASIONAL);

		$criteria->addSelectColumn(PegawaiPerformancePeer::TGL_MASUK);

		$criteria->addSelectColumn(PegawaiPerformancePeer::IS_OUT);

		$criteria->addSelectColumn(PegawaiPerformancePeer::JABATAN_ID);

		$criteria->addSelectColumn(PegawaiPerformancePeer::TGL_KELUAR);

		$criteria->addSelectColumn(PegawaiPerformancePeer::BOBOT_JABATAN);

		$criteria->addSelectColumn(PegawaiPerformancePeer::IS_KA_UPTD);

		$criteria->addSelectColumn(PegawaiPerformancePeer::IS_STRUKTURAL);

		$criteria->addSelectColumn(PegawaiPerformancePeer::IS_KUNCI_AKTIFITAS);

		$criteria->addSelectColumn(PegawaiPerformancePeer::LOCK_CAPAIAN_INDIKATOR);

		$criteria->addSelectColumn(PegawaiPerformancePeer::PELAYANAN_KESEKRETARIATAN);

		$criteria->addSelectColumn(PegawaiPerformancePeer::JABATAN_STRUKTURAL_ID);

		$criteria->addSelectColumn(PegawaiPerformancePeer::JABATAN_LAIN);

		$criteria->addSelectColumn(PegawaiPerformancePeer::IS_ONLY_SKP);

	}

	const COUNT = 'COUNT(eperformance.pegawai.ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT eperformance.pegawai.ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(PegawaiPerformancePeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(PegawaiPerformancePeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = PegawaiPerformancePeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = PegawaiPerformancePeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return PegawaiPerformancePeer::populateObjects(PegawaiPerformancePeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			PegawaiPerformancePeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = PegawaiPerformancePeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return PegawaiPerformancePeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(PegawaiPerformancePeer::ID);
			$selectCriteria->add(PegawaiPerformancePeer::ID, $criteria->remove(PegawaiPerformancePeer::ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(PegawaiPerformancePeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(PegawaiPerformancePeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof PegawaiPerformance) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(PegawaiPerformancePeer::ID, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(PegawaiPerformance $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(PegawaiPerformancePeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(PegawaiPerformancePeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(PegawaiPerformancePeer::DATABASE_NAME, PegawaiPerformancePeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = PegawaiPerformancePeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(PegawaiPerformancePeer::DATABASE_NAME);

		$criteria->add(PegawaiPerformancePeer::ID, $pk);


		$v = PegawaiPerformancePeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(PegawaiPerformancePeer::ID, $pks, Criteria::IN);
			$objs = PegawaiPerformancePeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BasePegawaiPerformancePeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/performance/map/PegawaiPerformanceMapBuilder.php';
	Propel::registerMapBuilder('lib.model.performance.map.PegawaiPerformanceMapBuilder');
}
