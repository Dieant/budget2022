<?php


abstract class BaseMasterSkpd extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $skpd_id;


	
	protected $skpd_kode;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getSkpdId()
	{

		return $this->skpd_id;
	}

	
	public function getSkpdKode()
	{

		return $this->skpd_kode;
	}

	
	public function setSkpdId($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->skpd_id !== $v) {
			$this->skpd_id = $v;
			$this->modifiedColumns[] = MasterSkpdPeer::SKPD_ID;
		}

	} 
	
	public function setSkpdKode($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->skpd_kode !== $v) {
			$this->skpd_kode = $v;
			$this->modifiedColumns[] = MasterSkpdPeer::SKPD_KODE;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->skpd_id = $rs->getInt($startcol + 0);

			$this->skpd_kode = $rs->getString($startcol + 1);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 2; 
		} catch (Exception $e) {
			throw new PropelException("Error populating MasterSkpd object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(MasterSkpdPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			MasterSkpdPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(MasterSkpdPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = MasterSkpdPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += MasterSkpdPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = MasterSkpdPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = MasterSkpdPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getSkpdId();
				break;
			case 1:
				return $this->getSkpdKode();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = MasterSkpdPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getSkpdId(),
			$keys[1] => $this->getSkpdKode(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = MasterSkpdPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setSkpdId($value);
				break;
			case 1:
				$this->setSkpdKode($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = MasterSkpdPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setSkpdId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setSkpdKode($arr[$keys[1]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(MasterSkpdPeer::DATABASE_NAME);

		if ($this->isColumnModified(MasterSkpdPeer::SKPD_ID)) $criteria->add(MasterSkpdPeer::SKPD_ID, $this->skpd_id);
		if ($this->isColumnModified(MasterSkpdPeer::SKPD_KODE)) $criteria->add(MasterSkpdPeer::SKPD_KODE, $this->skpd_kode);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(MasterSkpdPeer::DATABASE_NAME);

		$criteria->add(MasterSkpdPeer::SKPD_ID, $this->skpd_id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getSkpdId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setSkpdId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setSkpdKode($this->skpd_kode);


		$copyObj->setNew(true);

		$copyObj->setSkpdId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new MasterSkpdPeer();
		}
		return self::$peer;
	}

} 