<?php


abstract class BasePegawaiPerformance extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id;


	
	protected $nip;


	
	protected $nama;


	
	protected $golongan;


	
	protected $tmt;


	
	protected $pendidikan_terakhir;


	
	protected $skpd_id;


	
	protected $atasan_id;


	
	protected $is_atasan = false;


	
	protected $level_struktural;


	
	protected $password;


	
	protected $user_id;


	
	protected $jumlah_anak_buah;


	
	protected $nip_lama;


	
	protected $status = 0;


	
	protected $created_at;


	
	protected $deleted_at;


	
	protected $updated_at;


	
	protected $username_eproject;


	
	protected $id_fp;


	
	protected $is_operasional;


	
	protected $tgl_masuk;


	
	protected $is_out = false;


	
	protected $jabatan_id;


	
	protected $tgl_keluar;


	
	protected $bobot_jabatan;


	
	protected $is_ka_uptd = false;


	
	protected $is_struktural = false;


	
	protected $is_kunci_aktifitas = true;


	
	protected $lock_capaian_indikator = false;


	
	protected $pelayanan_kesekretariatan = 100;


	
	protected $jabatan_struktural_id;


	
	protected $jabatan_lain;


	
	protected $is_only_skp = false;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getId()
	{

		return $this->id;
	}

	
	public function getNip()
	{

		return $this->nip;
	}

	
	public function getNama()
	{

		return $this->nama;
	}

	
	public function getGolongan()
	{

		return $this->golongan;
	}

	
	public function getTmt($format = 'Y-m-d')
	{

		if ($this->tmt === null || $this->tmt === '') {
			return null;
		} elseif (!is_int($this->tmt)) {
						$ts = strtotime($this->tmt);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [tmt] as date/time value: " . var_export($this->tmt, true));
			}
		} else {
			$ts = $this->tmt;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getPendidikanTerakhir()
	{

		return $this->pendidikan_terakhir;
	}

	
	public function getSkpdId()
	{

		return $this->skpd_id;
	}

	
	public function getAtasanId()
	{

		return $this->atasan_id;
	}

	
	public function getIsAtasan()
	{

		return $this->is_atasan;
	}

	
	public function getLevelStruktural()
	{

		return $this->level_struktural;
	}

	
	public function getPassword()
	{

		return $this->password;
	}

	
	public function getUserId()
	{

		return $this->user_id;
	}

	
	public function getJumlahAnakBuah()
	{

		return $this->jumlah_anak_buah;
	}

	
	public function getNipLama()
	{

		return $this->nip_lama;
	}

	
	public function getStatus()
	{

		return $this->status;
	}

	
	public function getCreatedAt($format = 'Y-m-d H:i:s')
	{

		if ($this->created_at === null || $this->created_at === '') {
			return null;
		} elseif (!is_int($this->created_at)) {
						$ts = strtotime($this->created_at);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [created_at] as date/time value: " . var_export($this->created_at, true));
			}
		} else {
			$ts = $this->created_at;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getDeletedAt($format = 'Y-m-d H:i:s')
	{

		if ($this->deleted_at === null || $this->deleted_at === '') {
			return null;
		} elseif (!is_int($this->deleted_at)) {
						$ts = strtotime($this->deleted_at);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [deleted_at] as date/time value: " . var_export($this->deleted_at, true));
			}
		} else {
			$ts = $this->deleted_at;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getUpdatedAt($format = 'Y-m-d H:i:s')
	{

		if ($this->updated_at === null || $this->updated_at === '') {
			return null;
		} elseif (!is_int($this->updated_at)) {
						$ts = strtotime($this->updated_at);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [updated_at] as date/time value: " . var_export($this->updated_at, true));
			}
		} else {
			$ts = $this->updated_at;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getUsernameEproject()
	{

		return $this->username_eproject;
	}

	
	public function getIdFp()
	{

		return $this->id_fp;
	}

	
	public function getIsOperasional()
	{

		return $this->is_operasional;
	}

	
	public function getTglMasuk($format = 'Y-m-d')
	{

		if ($this->tgl_masuk === null || $this->tgl_masuk === '') {
			return null;
		} elseif (!is_int($this->tgl_masuk)) {
						$ts = strtotime($this->tgl_masuk);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [tgl_masuk] as date/time value: " . var_export($this->tgl_masuk, true));
			}
		} else {
			$ts = $this->tgl_masuk;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getIsOut()
	{

		return $this->is_out;
	}

	
	public function getJabatanId()
	{

		return $this->jabatan_id;
	}

	
	public function getTglKeluar($format = 'Y-m-d')
	{

		if ($this->tgl_keluar === null || $this->tgl_keluar === '') {
			return null;
		} elseif (!is_int($this->tgl_keluar)) {
						$ts = strtotime($this->tgl_keluar);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [tgl_keluar] as date/time value: " . var_export($this->tgl_keluar, true));
			}
		} else {
			$ts = $this->tgl_keluar;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getBobotJabatan()
	{

		return $this->bobot_jabatan;
	}

	
	public function getIsKaUptd()
	{

		return $this->is_ka_uptd;
	}

	
	public function getIsStruktural()
	{

		return $this->is_struktural;
	}

	
	public function getIsKunciAktifitas()
	{

		return $this->is_kunci_aktifitas;
	}

	
	public function getLockCapaianIndikator()
	{

		return $this->lock_capaian_indikator;
	}

	
	public function getPelayananKesekretariatan()
	{

		return $this->pelayanan_kesekretariatan;
	}

	
	public function getJabatanStrukturalId()
	{

		return $this->jabatan_struktural_id;
	}

	
	public function getJabatanLain()
	{

		return $this->jabatan_lain;
	}

	
	public function getIsOnlySkp()
	{

		return $this->is_only_skp;
	}

	
	public function setId($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = PegawaiPerformancePeer::ID;
		}

	} 
	
	public function setNip($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nip !== $v) {
			$this->nip = $v;
			$this->modifiedColumns[] = PegawaiPerformancePeer::NIP;
		}

	} 
	
	public function setNama($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nama !== $v) {
			$this->nama = $v;
			$this->modifiedColumns[] = PegawaiPerformancePeer::NAMA;
		}

	} 
	
	public function setGolongan($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->golongan !== $v) {
			$this->golongan = $v;
			$this->modifiedColumns[] = PegawaiPerformancePeer::GOLONGAN;
		}

	} 
	
	public function setTmt($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [tmt] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->tmt !== $ts) {
			$this->tmt = $ts;
			$this->modifiedColumns[] = PegawaiPerformancePeer::TMT;
		}

	} 
	
	public function setPendidikanTerakhir($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->pendidikan_terakhir !== $v) {
			$this->pendidikan_terakhir = $v;
			$this->modifiedColumns[] = PegawaiPerformancePeer::PENDIDIKAN_TERAKHIR;
		}

	} 
	
	public function setSkpdId($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->skpd_id !== $v) {
			$this->skpd_id = $v;
			$this->modifiedColumns[] = PegawaiPerformancePeer::SKPD_ID;
		}

	} 
	
	public function setAtasanId($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->atasan_id !== $v) {
			$this->atasan_id = $v;
			$this->modifiedColumns[] = PegawaiPerformancePeer::ATASAN_ID;
		}

	} 
	
	public function setIsAtasan($v)
	{

		if ($this->is_atasan !== $v || $v === false) {
			$this->is_atasan = $v;
			$this->modifiedColumns[] = PegawaiPerformancePeer::IS_ATASAN;
		}

	} 
	
	public function setLevelStruktural($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->level_struktural !== $v) {
			$this->level_struktural = $v;
			$this->modifiedColumns[] = PegawaiPerformancePeer::LEVEL_STRUKTURAL;
		}

	} 
	
	public function setPassword($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->password !== $v) {
			$this->password = $v;
			$this->modifiedColumns[] = PegawaiPerformancePeer::PASSWORD;
		}

	} 
	
	public function setUserId($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->user_id !== $v) {
			$this->user_id = $v;
			$this->modifiedColumns[] = PegawaiPerformancePeer::USER_ID;
		}

	} 
	
	public function setJumlahAnakBuah($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->jumlah_anak_buah !== $v) {
			$this->jumlah_anak_buah = $v;
			$this->modifiedColumns[] = PegawaiPerformancePeer::JUMLAH_ANAK_BUAH;
		}

	} 
	
	public function setNipLama($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nip_lama !== $v) {
			$this->nip_lama = $v;
			$this->modifiedColumns[] = PegawaiPerformancePeer::NIP_LAMA;
		}

	} 
	
	public function setStatus($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->status !== $v || $v === 0) {
			$this->status = $v;
			$this->modifiedColumns[] = PegawaiPerformancePeer::STATUS;
		}

	} 
	
	public function setCreatedAt($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [created_at] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->created_at !== $ts) {
			$this->created_at = $ts;
			$this->modifiedColumns[] = PegawaiPerformancePeer::CREATED_AT;
		}

	} 
	
	public function setDeletedAt($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [deleted_at] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->deleted_at !== $ts) {
			$this->deleted_at = $ts;
			$this->modifiedColumns[] = PegawaiPerformancePeer::DELETED_AT;
		}

	} 
	
	public function setUpdatedAt($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [updated_at] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->updated_at !== $ts) {
			$this->updated_at = $ts;
			$this->modifiedColumns[] = PegawaiPerformancePeer::UPDATED_AT;
		}

	} 
	
	public function setUsernameEproject($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->username_eproject !== $v) {
			$this->username_eproject = $v;
			$this->modifiedColumns[] = PegawaiPerformancePeer::USERNAME_EPROJECT;
		}

	} 
	
	public function setIdFp($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id_fp !== $v) {
			$this->id_fp = $v;
			$this->modifiedColumns[] = PegawaiPerformancePeer::ID_FP;
		}

	} 
	
	public function setIsOperasional($v)
	{

		if ($this->is_operasional !== $v) {
			$this->is_operasional = $v;
			$this->modifiedColumns[] = PegawaiPerformancePeer::IS_OPERASIONAL;
		}

	} 
	
	public function setTglMasuk($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [tgl_masuk] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->tgl_masuk !== $ts) {
			$this->tgl_masuk = $ts;
			$this->modifiedColumns[] = PegawaiPerformancePeer::TGL_MASUK;
		}

	} 
	
	public function setIsOut($v)
	{

		if ($this->is_out !== $v || $v === false) {
			$this->is_out = $v;
			$this->modifiedColumns[] = PegawaiPerformancePeer::IS_OUT;
		}

	} 
	
	public function setJabatanId($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->jabatan_id !== $v) {
			$this->jabatan_id = $v;
			$this->modifiedColumns[] = PegawaiPerformancePeer::JABATAN_ID;
		}

	} 
	
	public function setTglKeluar($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [tgl_keluar] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->tgl_keluar !== $ts) {
			$this->tgl_keluar = $ts;
			$this->modifiedColumns[] = PegawaiPerformancePeer::TGL_KELUAR;
		}

	} 
	
	public function setBobotJabatan($v)
	{

		if ($this->bobot_jabatan !== $v) {
			$this->bobot_jabatan = $v;
			$this->modifiedColumns[] = PegawaiPerformancePeer::BOBOT_JABATAN;
		}

	} 
	
	public function setIsKaUptd($v)
	{

		if ($this->is_ka_uptd !== $v || $v === false) {
			$this->is_ka_uptd = $v;
			$this->modifiedColumns[] = PegawaiPerformancePeer::IS_KA_UPTD;
		}

	} 
	
	public function setIsStruktural($v)
	{

		if ($this->is_struktural !== $v || $v === false) {
			$this->is_struktural = $v;
			$this->modifiedColumns[] = PegawaiPerformancePeer::IS_STRUKTURAL;
		}

	} 
	
	public function setIsKunciAktifitas($v)
	{

		if ($this->is_kunci_aktifitas !== $v || $v === true) {
			$this->is_kunci_aktifitas = $v;
			$this->modifiedColumns[] = PegawaiPerformancePeer::IS_KUNCI_AKTIFITAS;
		}

	} 
	
	public function setLockCapaianIndikator($v)
	{

		if ($this->lock_capaian_indikator !== $v || $v === false) {
			$this->lock_capaian_indikator = $v;
			$this->modifiedColumns[] = PegawaiPerformancePeer::LOCK_CAPAIAN_INDIKATOR;
		}

	} 
	
	public function setPelayananKesekretariatan($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->pelayanan_kesekretariatan !== $v || $v === 100) {
			$this->pelayanan_kesekretariatan = $v;
			$this->modifiedColumns[] = PegawaiPerformancePeer::PELAYANAN_KESEKRETARIATAN;
		}

	} 
	
	public function setJabatanStrukturalId($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->jabatan_struktural_id !== $v) {
			$this->jabatan_struktural_id = $v;
			$this->modifiedColumns[] = PegawaiPerformancePeer::JABATAN_STRUKTURAL_ID;
		}

	} 
	
	public function setJabatanLain($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->jabatan_lain !== $v) {
			$this->jabatan_lain = $v;
			$this->modifiedColumns[] = PegawaiPerformancePeer::JABATAN_LAIN;
		}

	} 
	
	public function setIsOnlySkp($v)
	{

		if ($this->is_only_skp !== $v || $v === false) {
			$this->is_only_skp = $v;
			$this->modifiedColumns[] = PegawaiPerformancePeer::IS_ONLY_SKP;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getInt($startcol + 0);

			$this->nip = $rs->getString($startcol + 1);

			$this->nama = $rs->getString($startcol + 2);

			$this->golongan = $rs->getString($startcol + 3);

			$this->tmt = $rs->getDate($startcol + 4, null);

			$this->pendidikan_terakhir = $rs->getString($startcol + 5);

			$this->skpd_id = $rs->getInt($startcol + 6);

			$this->atasan_id = $rs->getInt($startcol + 7);

			$this->is_atasan = $rs->getBoolean($startcol + 8);

			$this->level_struktural = $rs->getInt($startcol + 9);

			$this->password = $rs->getString($startcol + 10);

			$this->user_id = $rs->getInt($startcol + 11);

			$this->jumlah_anak_buah = $rs->getInt($startcol + 12);

			$this->nip_lama = $rs->getString($startcol + 13);

			$this->status = $rs->getInt($startcol + 14);

			$this->created_at = $rs->getTimestamp($startcol + 15, null);

			$this->deleted_at = $rs->getTimestamp($startcol + 16, null);

			$this->updated_at = $rs->getTimestamp($startcol + 17, null);

			$this->username_eproject = $rs->getString($startcol + 18);

			$this->id_fp = $rs->getInt($startcol + 19);

			$this->is_operasional = $rs->getBoolean($startcol + 20);

			$this->tgl_masuk = $rs->getDate($startcol + 21, null);

			$this->is_out = $rs->getBoolean($startcol + 22);

			$this->jabatan_id = $rs->getInt($startcol + 23);

			$this->tgl_keluar = $rs->getDate($startcol + 24, null);

			$this->bobot_jabatan = $rs->getFloat($startcol + 25);

			$this->is_ka_uptd = $rs->getBoolean($startcol + 26);

			$this->is_struktural = $rs->getBoolean($startcol + 27);

			$this->is_kunci_aktifitas = $rs->getBoolean($startcol + 28);

			$this->lock_capaian_indikator = $rs->getBoolean($startcol + 29);

			$this->pelayanan_kesekretariatan = $rs->getInt($startcol + 30);

			$this->jabatan_struktural_id = $rs->getInt($startcol + 31);

			$this->jabatan_lain = $rs->getString($startcol + 32);

			$this->is_only_skp = $rs->getBoolean($startcol + 33);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 34; 
		} catch (Exception $e) {
			throw new PropelException("Error populating PegawaiPerformance object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(PegawaiPerformancePeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			PegawaiPerformancePeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
    if ($this->isNew() && !$this->isColumnModified(PegawaiPerformancePeer::CREATED_AT))
    {
      $this->setCreatedAt(time());
    }

    if ($this->isModified() && !$this->isColumnModified(PegawaiPerformancePeer::UPDATED_AT))
    {
      $this->setUpdatedAt(time());
    }

		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(PegawaiPerformancePeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = PegawaiPerformancePeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += PegawaiPerformancePeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = PegawaiPerformancePeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = PegawaiPerformancePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getNip();
				break;
			case 2:
				return $this->getNama();
				break;
			case 3:
				return $this->getGolongan();
				break;
			case 4:
				return $this->getTmt();
				break;
			case 5:
				return $this->getPendidikanTerakhir();
				break;
			case 6:
				return $this->getSkpdId();
				break;
			case 7:
				return $this->getAtasanId();
				break;
			case 8:
				return $this->getIsAtasan();
				break;
			case 9:
				return $this->getLevelStruktural();
				break;
			case 10:
				return $this->getPassword();
				break;
			case 11:
				return $this->getUserId();
				break;
			case 12:
				return $this->getJumlahAnakBuah();
				break;
			case 13:
				return $this->getNipLama();
				break;
			case 14:
				return $this->getStatus();
				break;
			case 15:
				return $this->getCreatedAt();
				break;
			case 16:
				return $this->getDeletedAt();
				break;
			case 17:
				return $this->getUpdatedAt();
				break;
			case 18:
				return $this->getUsernameEproject();
				break;
			case 19:
				return $this->getIdFp();
				break;
			case 20:
				return $this->getIsOperasional();
				break;
			case 21:
				return $this->getTglMasuk();
				break;
			case 22:
				return $this->getIsOut();
				break;
			case 23:
				return $this->getJabatanId();
				break;
			case 24:
				return $this->getTglKeluar();
				break;
			case 25:
				return $this->getBobotJabatan();
				break;
			case 26:
				return $this->getIsKaUptd();
				break;
			case 27:
				return $this->getIsStruktural();
				break;
			case 28:
				return $this->getIsKunciAktifitas();
				break;
			case 29:
				return $this->getLockCapaianIndikator();
				break;
			case 30:
				return $this->getPelayananKesekretariatan();
				break;
			case 31:
				return $this->getJabatanStrukturalId();
				break;
			case 32:
				return $this->getJabatanLain();
				break;
			case 33:
				return $this->getIsOnlySkp();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = PegawaiPerformancePeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getNip(),
			$keys[2] => $this->getNama(),
			$keys[3] => $this->getGolongan(),
			$keys[4] => $this->getTmt(),
			$keys[5] => $this->getPendidikanTerakhir(),
			$keys[6] => $this->getSkpdId(),
			$keys[7] => $this->getAtasanId(),
			$keys[8] => $this->getIsAtasan(),
			$keys[9] => $this->getLevelStruktural(),
			$keys[10] => $this->getPassword(),
			$keys[11] => $this->getUserId(),
			$keys[12] => $this->getJumlahAnakBuah(),
			$keys[13] => $this->getNipLama(),
			$keys[14] => $this->getStatus(),
			$keys[15] => $this->getCreatedAt(),
			$keys[16] => $this->getDeletedAt(),
			$keys[17] => $this->getUpdatedAt(),
			$keys[18] => $this->getUsernameEproject(),
			$keys[19] => $this->getIdFp(),
			$keys[20] => $this->getIsOperasional(),
			$keys[21] => $this->getTglMasuk(),
			$keys[22] => $this->getIsOut(),
			$keys[23] => $this->getJabatanId(),
			$keys[24] => $this->getTglKeluar(),
			$keys[25] => $this->getBobotJabatan(),
			$keys[26] => $this->getIsKaUptd(),
			$keys[27] => $this->getIsStruktural(),
			$keys[28] => $this->getIsKunciAktifitas(),
			$keys[29] => $this->getLockCapaianIndikator(),
			$keys[30] => $this->getPelayananKesekretariatan(),
			$keys[31] => $this->getJabatanStrukturalId(),
			$keys[32] => $this->getJabatanLain(),
			$keys[33] => $this->getIsOnlySkp(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = PegawaiPerformancePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setNip($value);
				break;
			case 2:
				$this->setNama($value);
				break;
			case 3:
				$this->setGolongan($value);
				break;
			case 4:
				$this->setTmt($value);
				break;
			case 5:
				$this->setPendidikanTerakhir($value);
				break;
			case 6:
				$this->setSkpdId($value);
				break;
			case 7:
				$this->setAtasanId($value);
				break;
			case 8:
				$this->setIsAtasan($value);
				break;
			case 9:
				$this->setLevelStruktural($value);
				break;
			case 10:
				$this->setPassword($value);
				break;
			case 11:
				$this->setUserId($value);
				break;
			case 12:
				$this->setJumlahAnakBuah($value);
				break;
			case 13:
				$this->setNipLama($value);
				break;
			case 14:
				$this->setStatus($value);
				break;
			case 15:
				$this->setCreatedAt($value);
				break;
			case 16:
				$this->setDeletedAt($value);
				break;
			case 17:
				$this->setUpdatedAt($value);
				break;
			case 18:
				$this->setUsernameEproject($value);
				break;
			case 19:
				$this->setIdFp($value);
				break;
			case 20:
				$this->setIsOperasional($value);
				break;
			case 21:
				$this->setTglMasuk($value);
				break;
			case 22:
				$this->setIsOut($value);
				break;
			case 23:
				$this->setJabatanId($value);
				break;
			case 24:
				$this->setTglKeluar($value);
				break;
			case 25:
				$this->setBobotJabatan($value);
				break;
			case 26:
				$this->setIsKaUptd($value);
				break;
			case 27:
				$this->setIsStruktural($value);
				break;
			case 28:
				$this->setIsKunciAktifitas($value);
				break;
			case 29:
				$this->setLockCapaianIndikator($value);
				break;
			case 30:
				$this->setPelayananKesekretariatan($value);
				break;
			case 31:
				$this->setJabatanStrukturalId($value);
				break;
			case 32:
				$this->setJabatanLain($value);
				break;
			case 33:
				$this->setIsOnlySkp($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = PegawaiPerformancePeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setNip($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setNama($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setGolongan($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setTmt($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setPendidikanTerakhir($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setSkpdId($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setAtasanId($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setIsAtasan($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setLevelStruktural($arr[$keys[9]]);
		if (array_key_exists($keys[10], $arr)) $this->setPassword($arr[$keys[10]]);
		if (array_key_exists($keys[11], $arr)) $this->setUserId($arr[$keys[11]]);
		if (array_key_exists($keys[12], $arr)) $this->setJumlahAnakBuah($arr[$keys[12]]);
		if (array_key_exists($keys[13], $arr)) $this->setNipLama($arr[$keys[13]]);
		if (array_key_exists($keys[14], $arr)) $this->setStatus($arr[$keys[14]]);
		if (array_key_exists($keys[15], $arr)) $this->setCreatedAt($arr[$keys[15]]);
		if (array_key_exists($keys[16], $arr)) $this->setDeletedAt($arr[$keys[16]]);
		if (array_key_exists($keys[17], $arr)) $this->setUpdatedAt($arr[$keys[17]]);
		if (array_key_exists($keys[18], $arr)) $this->setUsernameEproject($arr[$keys[18]]);
		if (array_key_exists($keys[19], $arr)) $this->setIdFp($arr[$keys[19]]);
		if (array_key_exists($keys[20], $arr)) $this->setIsOperasional($arr[$keys[20]]);
		if (array_key_exists($keys[21], $arr)) $this->setTglMasuk($arr[$keys[21]]);
		if (array_key_exists($keys[22], $arr)) $this->setIsOut($arr[$keys[22]]);
		if (array_key_exists($keys[23], $arr)) $this->setJabatanId($arr[$keys[23]]);
		if (array_key_exists($keys[24], $arr)) $this->setTglKeluar($arr[$keys[24]]);
		if (array_key_exists($keys[25], $arr)) $this->setBobotJabatan($arr[$keys[25]]);
		if (array_key_exists($keys[26], $arr)) $this->setIsKaUptd($arr[$keys[26]]);
		if (array_key_exists($keys[27], $arr)) $this->setIsStruktural($arr[$keys[27]]);
		if (array_key_exists($keys[28], $arr)) $this->setIsKunciAktifitas($arr[$keys[28]]);
		if (array_key_exists($keys[29], $arr)) $this->setLockCapaianIndikator($arr[$keys[29]]);
		if (array_key_exists($keys[30], $arr)) $this->setPelayananKesekretariatan($arr[$keys[30]]);
		if (array_key_exists($keys[31], $arr)) $this->setJabatanStrukturalId($arr[$keys[31]]);
		if (array_key_exists($keys[32], $arr)) $this->setJabatanLain($arr[$keys[32]]);
		if (array_key_exists($keys[33], $arr)) $this->setIsOnlySkp($arr[$keys[33]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(PegawaiPerformancePeer::DATABASE_NAME);

		if ($this->isColumnModified(PegawaiPerformancePeer::ID)) $criteria->add(PegawaiPerformancePeer::ID, $this->id);
		if ($this->isColumnModified(PegawaiPerformancePeer::NIP)) $criteria->add(PegawaiPerformancePeer::NIP, $this->nip);
		if ($this->isColumnModified(PegawaiPerformancePeer::NAMA)) $criteria->add(PegawaiPerformancePeer::NAMA, $this->nama);
		if ($this->isColumnModified(PegawaiPerformancePeer::GOLONGAN)) $criteria->add(PegawaiPerformancePeer::GOLONGAN, $this->golongan);
		if ($this->isColumnModified(PegawaiPerformancePeer::TMT)) $criteria->add(PegawaiPerformancePeer::TMT, $this->tmt);
		if ($this->isColumnModified(PegawaiPerformancePeer::PENDIDIKAN_TERAKHIR)) $criteria->add(PegawaiPerformancePeer::PENDIDIKAN_TERAKHIR, $this->pendidikan_terakhir);
		if ($this->isColumnModified(PegawaiPerformancePeer::SKPD_ID)) $criteria->add(PegawaiPerformancePeer::SKPD_ID, $this->skpd_id);
		if ($this->isColumnModified(PegawaiPerformancePeer::ATASAN_ID)) $criteria->add(PegawaiPerformancePeer::ATASAN_ID, $this->atasan_id);
		if ($this->isColumnModified(PegawaiPerformancePeer::IS_ATASAN)) $criteria->add(PegawaiPerformancePeer::IS_ATASAN, $this->is_atasan);
		if ($this->isColumnModified(PegawaiPerformancePeer::LEVEL_STRUKTURAL)) $criteria->add(PegawaiPerformancePeer::LEVEL_STRUKTURAL, $this->level_struktural);
		if ($this->isColumnModified(PegawaiPerformancePeer::PASSWORD)) $criteria->add(PegawaiPerformancePeer::PASSWORD, $this->password);
		if ($this->isColumnModified(PegawaiPerformancePeer::USER_ID)) $criteria->add(PegawaiPerformancePeer::USER_ID, $this->user_id);
		if ($this->isColumnModified(PegawaiPerformancePeer::JUMLAH_ANAK_BUAH)) $criteria->add(PegawaiPerformancePeer::JUMLAH_ANAK_BUAH, $this->jumlah_anak_buah);
		if ($this->isColumnModified(PegawaiPerformancePeer::NIP_LAMA)) $criteria->add(PegawaiPerformancePeer::NIP_LAMA, $this->nip_lama);
		if ($this->isColumnModified(PegawaiPerformancePeer::STATUS)) $criteria->add(PegawaiPerformancePeer::STATUS, $this->status);
		if ($this->isColumnModified(PegawaiPerformancePeer::CREATED_AT)) $criteria->add(PegawaiPerformancePeer::CREATED_AT, $this->created_at);
		if ($this->isColumnModified(PegawaiPerformancePeer::DELETED_AT)) $criteria->add(PegawaiPerformancePeer::DELETED_AT, $this->deleted_at);
		if ($this->isColumnModified(PegawaiPerformancePeer::UPDATED_AT)) $criteria->add(PegawaiPerformancePeer::UPDATED_AT, $this->updated_at);
		if ($this->isColumnModified(PegawaiPerformancePeer::USERNAME_EPROJECT)) $criteria->add(PegawaiPerformancePeer::USERNAME_EPROJECT, $this->username_eproject);
		if ($this->isColumnModified(PegawaiPerformancePeer::ID_FP)) $criteria->add(PegawaiPerformancePeer::ID_FP, $this->id_fp);
		if ($this->isColumnModified(PegawaiPerformancePeer::IS_OPERASIONAL)) $criteria->add(PegawaiPerformancePeer::IS_OPERASIONAL, $this->is_operasional);
		if ($this->isColumnModified(PegawaiPerformancePeer::TGL_MASUK)) $criteria->add(PegawaiPerformancePeer::TGL_MASUK, $this->tgl_masuk);
		if ($this->isColumnModified(PegawaiPerformancePeer::IS_OUT)) $criteria->add(PegawaiPerformancePeer::IS_OUT, $this->is_out);
		if ($this->isColumnModified(PegawaiPerformancePeer::JABATAN_ID)) $criteria->add(PegawaiPerformancePeer::JABATAN_ID, $this->jabatan_id);
		if ($this->isColumnModified(PegawaiPerformancePeer::TGL_KELUAR)) $criteria->add(PegawaiPerformancePeer::TGL_KELUAR, $this->tgl_keluar);
		if ($this->isColumnModified(PegawaiPerformancePeer::BOBOT_JABATAN)) $criteria->add(PegawaiPerformancePeer::BOBOT_JABATAN, $this->bobot_jabatan);
		if ($this->isColumnModified(PegawaiPerformancePeer::IS_KA_UPTD)) $criteria->add(PegawaiPerformancePeer::IS_KA_UPTD, $this->is_ka_uptd);
		if ($this->isColumnModified(PegawaiPerformancePeer::IS_STRUKTURAL)) $criteria->add(PegawaiPerformancePeer::IS_STRUKTURAL, $this->is_struktural);
		if ($this->isColumnModified(PegawaiPerformancePeer::IS_KUNCI_AKTIFITAS)) $criteria->add(PegawaiPerformancePeer::IS_KUNCI_AKTIFITAS, $this->is_kunci_aktifitas);
		if ($this->isColumnModified(PegawaiPerformancePeer::LOCK_CAPAIAN_INDIKATOR)) $criteria->add(PegawaiPerformancePeer::LOCK_CAPAIAN_INDIKATOR, $this->lock_capaian_indikator);
		if ($this->isColumnModified(PegawaiPerformancePeer::PELAYANAN_KESEKRETARIATAN)) $criteria->add(PegawaiPerformancePeer::PELAYANAN_KESEKRETARIATAN, $this->pelayanan_kesekretariatan);
		if ($this->isColumnModified(PegawaiPerformancePeer::JABATAN_STRUKTURAL_ID)) $criteria->add(PegawaiPerformancePeer::JABATAN_STRUKTURAL_ID, $this->jabatan_struktural_id);
		if ($this->isColumnModified(PegawaiPerformancePeer::JABATAN_LAIN)) $criteria->add(PegawaiPerformancePeer::JABATAN_LAIN, $this->jabatan_lain);
		if ($this->isColumnModified(PegawaiPerformancePeer::IS_ONLY_SKP)) $criteria->add(PegawaiPerformancePeer::IS_ONLY_SKP, $this->is_only_skp);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(PegawaiPerformancePeer::DATABASE_NAME);

		$criteria->add(PegawaiPerformancePeer::ID, $this->id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setNip($this->nip);

		$copyObj->setNama($this->nama);

		$copyObj->setGolongan($this->golongan);

		$copyObj->setTmt($this->tmt);

		$copyObj->setPendidikanTerakhir($this->pendidikan_terakhir);

		$copyObj->setSkpdId($this->skpd_id);

		$copyObj->setAtasanId($this->atasan_id);

		$copyObj->setIsAtasan($this->is_atasan);

		$copyObj->setLevelStruktural($this->level_struktural);

		$copyObj->setPassword($this->password);

		$copyObj->setUserId($this->user_id);

		$copyObj->setJumlahAnakBuah($this->jumlah_anak_buah);

		$copyObj->setNipLama($this->nip_lama);

		$copyObj->setStatus($this->status);

		$copyObj->setCreatedAt($this->created_at);

		$copyObj->setDeletedAt($this->deleted_at);

		$copyObj->setUpdatedAt($this->updated_at);

		$copyObj->setUsernameEproject($this->username_eproject);

		$copyObj->setIdFp($this->id_fp);

		$copyObj->setIsOperasional($this->is_operasional);

		$copyObj->setTglMasuk($this->tgl_masuk);

		$copyObj->setIsOut($this->is_out);

		$copyObj->setJabatanId($this->jabatan_id);

		$copyObj->setTglKeluar($this->tgl_keluar);

		$copyObj->setBobotJabatan($this->bobot_jabatan);

		$copyObj->setIsKaUptd($this->is_ka_uptd);

		$copyObj->setIsStruktural($this->is_struktural);

		$copyObj->setIsKunciAktifitas($this->is_kunci_aktifitas);

		$copyObj->setLockCapaianIndikator($this->lock_capaian_indikator);

		$copyObj->setPelayananKesekretariatan($this->pelayanan_kesekretariatan);

		$copyObj->setJabatanStrukturalId($this->jabatan_struktural_id);

		$copyObj->setJabatanLain($this->jabatan_lain);

		$copyObj->setIsOnlySkp($this->is_only_skp);


		$copyObj->setNew(true);

		$copyObj->setId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new PegawaiPerformancePeer();
		}
		return self::$peer;
	}

} 