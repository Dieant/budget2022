<?php


abstract class BaseJabatanStruktural extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $id;


	
	protected $nama;


	
	protected $eselon;


	
	protected $jenis_indikator_kinerja;


	
	protected $skpd_id;


	
	protected $nomer;


	
	protected $is_asisten = false;


	
	protected $nomer_urut;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getId()
	{

		return $this->id;
	}

	
	public function getNama()
	{

		return $this->nama;
	}

	
	public function getEselon()
	{

		return $this->eselon;
	}

	
	public function getJenisIndikatorKinerja()
	{

		return $this->jenis_indikator_kinerja;
	}

	
	public function getSkpdId()
	{

		return $this->skpd_id;
	}

	
	public function getNomer()
	{

		return $this->nomer;
	}

	
	public function getIsAsisten()
	{

		return $this->is_asisten;
	}

	
	public function getNomerUrut()
	{

		return $this->nomer_urut;
	}

	
	public function setId($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = JabatanStrukturalPeer::ID;
		}

	} 
	
	public function setNama($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nama !== $v) {
			$this->nama = $v;
			$this->modifiedColumns[] = JabatanStrukturalPeer::NAMA;
		}

	} 
	
	public function setEselon($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->eselon !== $v) {
			$this->eselon = $v;
			$this->modifiedColumns[] = JabatanStrukturalPeer::ESELON;
		}

	} 
	
	public function setJenisIndikatorKinerja($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->jenis_indikator_kinerja !== $v) {
			$this->jenis_indikator_kinerja = $v;
			$this->modifiedColumns[] = JabatanStrukturalPeer::JENIS_INDIKATOR_KINERJA;
		}

	} 
	
	public function setSkpdId($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->skpd_id !== $v) {
			$this->skpd_id = $v;
			$this->modifiedColumns[] = JabatanStrukturalPeer::SKPD_ID;
		}

	} 
	
	public function setNomer($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->nomer !== $v) {
			$this->nomer = $v;
			$this->modifiedColumns[] = JabatanStrukturalPeer::NOMER;
		}

	} 
	
	public function setIsAsisten($v)
	{

		if ($this->is_asisten !== $v || $v === false) {
			$this->is_asisten = $v;
			$this->modifiedColumns[] = JabatanStrukturalPeer::IS_ASISTEN;
		}

	} 
	
	public function setNomerUrut($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->nomer_urut !== $v) {
			$this->nomer_urut = $v;
			$this->modifiedColumns[] = JabatanStrukturalPeer::NOMER_URUT;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->id = $rs->getInt($startcol + 0);

			$this->nama = $rs->getString($startcol + 1);

			$this->eselon = $rs->getString($startcol + 2);

			$this->jenis_indikator_kinerja = $rs->getInt($startcol + 3);

			$this->skpd_id = $rs->getInt($startcol + 4);

			$this->nomer = $rs->getInt($startcol + 5);

			$this->is_asisten = $rs->getBoolean($startcol + 6);

			$this->nomer_urut = $rs->getInt($startcol + 7);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 8; 
		} catch (Exception $e) {
			throw new PropelException("Error populating JabatanStruktural object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(JabatanStrukturalPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			JabatanStrukturalPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(JabatanStrukturalPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = JabatanStrukturalPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setNew(false);
				} else {
					$affectedRows += JabatanStrukturalPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = JabatanStrukturalPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = JabatanStrukturalPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getNama();
				break;
			case 2:
				return $this->getEselon();
				break;
			case 3:
				return $this->getJenisIndikatorKinerja();
				break;
			case 4:
				return $this->getSkpdId();
				break;
			case 5:
				return $this->getNomer();
				break;
			case 6:
				return $this->getIsAsisten();
				break;
			case 7:
				return $this->getNomerUrut();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = JabatanStrukturalPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getNama(),
			$keys[2] => $this->getEselon(),
			$keys[3] => $this->getJenisIndikatorKinerja(),
			$keys[4] => $this->getSkpdId(),
			$keys[5] => $this->getNomer(),
			$keys[6] => $this->getIsAsisten(),
			$keys[7] => $this->getNomerUrut(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = JabatanStrukturalPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setNama($value);
				break;
			case 2:
				$this->setEselon($value);
				break;
			case 3:
				$this->setJenisIndikatorKinerja($value);
				break;
			case 4:
				$this->setSkpdId($value);
				break;
			case 5:
				$this->setNomer($value);
				break;
			case 6:
				$this->setIsAsisten($value);
				break;
			case 7:
				$this->setNomerUrut($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = JabatanStrukturalPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setNama($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setEselon($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setJenisIndikatorKinerja($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setSkpdId($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setNomer($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setIsAsisten($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setNomerUrut($arr[$keys[7]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(JabatanStrukturalPeer::DATABASE_NAME);

		if ($this->isColumnModified(JabatanStrukturalPeer::ID)) $criteria->add(JabatanStrukturalPeer::ID, $this->id);
		if ($this->isColumnModified(JabatanStrukturalPeer::NAMA)) $criteria->add(JabatanStrukturalPeer::NAMA, $this->nama);
		if ($this->isColumnModified(JabatanStrukturalPeer::ESELON)) $criteria->add(JabatanStrukturalPeer::ESELON, $this->eselon);
		if ($this->isColumnModified(JabatanStrukturalPeer::JENIS_INDIKATOR_KINERJA)) $criteria->add(JabatanStrukturalPeer::JENIS_INDIKATOR_KINERJA, $this->jenis_indikator_kinerja);
		if ($this->isColumnModified(JabatanStrukturalPeer::SKPD_ID)) $criteria->add(JabatanStrukturalPeer::SKPD_ID, $this->skpd_id);
		if ($this->isColumnModified(JabatanStrukturalPeer::NOMER)) $criteria->add(JabatanStrukturalPeer::NOMER, $this->nomer);
		if ($this->isColumnModified(JabatanStrukturalPeer::IS_ASISTEN)) $criteria->add(JabatanStrukturalPeer::IS_ASISTEN, $this->is_asisten);
		if ($this->isColumnModified(JabatanStrukturalPeer::NOMER_URUT)) $criteria->add(JabatanStrukturalPeer::NOMER_URUT, $this->nomer_urut);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(JabatanStrukturalPeer::DATABASE_NAME);

		$criteria->add(JabatanStrukturalPeer::ID, $this->id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setNama($this->nama);

		$copyObj->setEselon($this->eselon);

		$copyObj->setJenisIndikatorKinerja($this->jenis_indikator_kinerja);

		$copyObj->setSkpdId($this->skpd_id);

		$copyObj->setNomer($this->nomer);

		$copyObj->setIsAsisten($this->is_asisten);

		$copyObj->setNomerUrut($this->nomer_urut);


		$copyObj->setNew(true);

		$copyObj->setId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new JabatanStrukturalPeer();
		}
		return self::$peer;
	}

} 