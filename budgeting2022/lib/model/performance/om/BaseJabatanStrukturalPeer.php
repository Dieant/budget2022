<?php


abstract class BaseJabatanStrukturalPeer {

	
	const DATABASE_NAME = 'performance';

	
	const TABLE_NAME = 'eperformance.jabatan_struktural';

	
	const CLASS_DEFAULT = 'lib.model.performance.JabatanStruktural';

	
	const NUM_COLUMNS = 8;

	
	const NUM_LAZY_LOAD_COLUMNS = 0;


	
	const ID = 'eperformance.jabatan_struktural.ID';

	
	const NAMA = 'eperformance.jabatan_struktural.NAMA';

	
	const ESELON = 'eperformance.jabatan_struktural.ESELON';

	
	const JENIS_INDIKATOR_KINERJA = 'eperformance.jabatan_struktural.JENIS_INDIKATOR_KINERJA';

	
	const SKPD_ID = 'eperformance.jabatan_struktural.SKPD_ID';

	
	const NOMER = 'eperformance.jabatan_struktural.NOMER';

	
	const IS_ASISTEN = 'eperformance.jabatan_struktural.IS_ASISTEN';

	
	const NOMER_URUT = 'eperformance.jabatan_struktural.NOMER_URUT';

	
	private static $phpNameMap = null;


	
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('Id', 'Nama', 'Eselon', 'JenisIndikatorKinerja', 'SkpdId', 'Nomer', 'IsAsisten', 'NomerUrut', ),
		BasePeer::TYPE_COLNAME => array (JabatanStrukturalPeer::ID, JabatanStrukturalPeer::NAMA, JabatanStrukturalPeer::ESELON, JabatanStrukturalPeer::JENIS_INDIKATOR_KINERJA, JabatanStrukturalPeer::SKPD_ID, JabatanStrukturalPeer::NOMER, JabatanStrukturalPeer::IS_ASISTEN, JabatanStrukturalPeer::NOMER_URUT, ),
		BasePeer::TYPE_FIELDNAME => array ('id', 'nama', 'eselon', 'jenis_indikator_kinerja', 'skpd_id', 'nomer', 'is_asisten', 'nomer_urut', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, )
	);

	
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('Id' => 0, 'Nama' => 1, 'Eselon' => 2, 'JenisIndikatorKinerja' => 3, 'SkpdId' => 4, 'Nomer' => 5, 'IsAsisten' => 6, 'NomerUrut' => 7, ),
		BasePeer::TYPE_COLNAME => array (JabatanStrukturalPeer::ID => 0, JabatanStrukturalPeer::NAMA => 1, JabatanStrukturalPeer::ESELON => 2, JabatanStrukturalPeer::JENIS_INDIKATOR_KINERJA => 3, JabatanStrukturalPeer::SKPD_ID => 4, JabatanStrukturalPeer::NOMER => 5, JabatanStrukturalPeer::IS_ASISTEN => 6, JabatanStrukturalPeer::NOMER_URUT => 7, ),
		BasePeer::TYPE_FIELDNAME => array ('id' => 0, 'nama' => 1, 'eselon' => 2, 'jenis_indikator_kinerja' => 3, 'skpd_id' => 4, 'nomer' => 5, 'is_asisten' => 6, 'nomer_urut' => 7, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, )
	);

	
	public static function getMapBuilder()
	{
		include_once 'lib/model/performance/map/JabatanStrukturalMapBuilder.php';
		return BasePeer::getMapBuilder('lib.model.performance.map.JabatanStrukturalMapBuilder');
	}
	
	public static function getPhpNameMap()
	{
		if (self::$phpNameMap === null) {
			$map = JabatanStrukturalPeer::getTableMap();
			$columns = $map->getColumns();
			$nameMap = array();
			foreach ($columns as $column) {
				$nameMap[$column->getPhpName()] = $column->getColumnName();
			}
			self::$phpNameMap = $nameMap;
		}
		return self::$phpNameMap;
	}
	
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants TYPE_PHPNAME, TYPE_COLNAME, TYPE_FIELDNAME, TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	
	public static function alias($alias, $column)
	{
		return str_replace(JabatanStrukturalPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	
	public static function addSelectColumns(Criteria $criteria)
	{

		$criteria->addSelectColumn(JabatanStrukturalPeer::ID);

		$criteria->addSelectColumn(JabatanStrukturalPeer::NAMA);

		$criteria->addSelectColumn(JabatanStrukturalPeer::ESELON);

		$criteria->addSelectColumn(JabatanStrukturalPeer::JENIS_INDIKATOR_KINERJA);

		$criteria->addSelectColumn(JabatanStrukturalPeer::SKPD_ID);

		$criteria->addSelectColumn(JabatanStrukturalPeer::NOMER);

		$criteria->addSelectColumn(JabatanStrukturalPeer::IS_ASISTEN);

		$criteria->addSelectColumn(JabatanStrukturalPeer::NOMER_URUT);

	}

	const COUNT = 'COUNT(eperformance.jabatan_struktural.ID)';
	const COUNT_DISTINCT = 'COUNT(DISTINCT eperformance.jabatan_struktural.ID)';

	
	public static function doCount(Criteria $criteria, $distinct = false, $con = null)
	{
				$criteria = clone $criteria;

				$criteria->clearSelectColumns()->clearOrderByColumns();
		if ($distinct || in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->addSelectColumn(JabatanStrukturalPeer::COUNT_DISTINCT);
		} else {
			$criteria->addSelectColumn(JabatanStrukturalPeer::COUNT);
		}

				foreach($criteria->getGroupByColumns() as $column)
		{
			$criteria->addSelectColumn($column);
		}

		$rs = JabatanStrukturalPeer::doSelectRS($criteria, $con);
		if ($rs->next()) {
			return $rs->getInt(1);
		} else {
						return 0;
		}
	}
	
	public static function doSelectOne(Criteria $criteria, $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = JabatanStrukturalPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	
	public static function doSelect(Criteria $criteria, $con = null)
	{
		return JabatanStrukturalPeer::populateObjects(JabatanStrukturalPeer::doSelectRS($criteria, $con));
	}
	
	public static function doSelectRS(Criteria $criteria, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if (!$criteria->getSelectColumns()) {
			$criteria = clone $criteria;
			JabatanStrukturalPeer::addSelectColumns($criteria);
		}

				$criteria->setDbName(self::DATABASE_NAME);

						return BasePeer::doSelect($criteria, $con);
	}
	
	public static function populateObjects(ResultSet $rs)
	{
		$results = array();
	
				$cls = JabatanStrukturalPeer::getOMClass();
		$cls = Propel::import($cls);
				while($rs->next()) {
		
			$obj = new $cls();
			$obj->hydrate($rs);
			$results[] = $obj;
			
		}
		return $results;
	}
	
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	
	public static function getOMClass()
	{
		return JabatanStrukturalPeer::CLASS_DEFAULT;
	}

	
	public static function doInsert($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} else {
			$criteria = $values->buildCriteria(); 		}


				$criteria->setDbName(self::DATABASE_NAME);

		try {
									$con->begin();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollback();
			throw $e;
		}

		return $pk;
	}

	
	public static function doUpdate($values, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; 
			$comparison = $criteria->getComparison(JabatanStrukturalPeer::ID);
			$selectCriteria->add(JabatanStrukturalPeer::ID, $criteria->remove(JabatanStrukturalPeer::ID), $comparison);

		} else { 			$criteria = $values->buildCriteria(); 			$selectCriteria = $values->buildPkeyCriteria(); 		}

				$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}
		$affectedRows = 0; 		try {
									$con->begin();
			$affectedRows += BasePeer::doDeleteAll(JabatanStrukturalPeer::TABLE_NAME, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	 public static function doDelete($values, $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(JabatanStrukturalPeer::DATABASE_NAME);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; 		} elseif ($values instanceof JabatanStruktural) {

			$criteria = $values->buildPkeyCriteria();
		} else {
						$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(JabatanStrukturalPeer::ID, (array) $values, Criteria::IN);
		}

				$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; 
		try {
									$con->begin();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public static function doValidate(JabatanStruktural $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(JabatanStrukturalPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(JabatanStrukturalPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		$res =  BasePeer::doValidate(JabatanStrukturalPeer::DATABASE_NAME, JabatanStrukturalPeer::TABLE_NAME, $columns);
    if ($res !== true) {
        $request = sfContext::getInstance()->getRequest();
        foreach ($res as $failed) {
            $col = JabatanStrukturalPeer::translateFieldname($failed->getColumn(), BasePeer::TYPE_COLNAME, BasePeer::TYPE_PHPNAME);
            $request->setError($col, $failed->getMessage());
        }
    }

    return $res;
	}

	
	public static function retrieveByPK($pk, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$criteria = new Criteria(JabatanStrukturalPeer::DATABASE_NAME);

		$criteria->add(JabatanStrukturalPeer::ID, $pk);


		$v = JabatanStrukturalPeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	
	public static function retrieveByPKs($pks, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(self::DATABASE_NAME);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria();
			$criteria->add(JabatanStrukturalPeer::ID, $pks, Criteria::IN);
			$objs = JabatanStrukturalPeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} 
if (Propel::isInit()) {
			try {
		BaseJabatanStrukturalPeer::getMapBuilder();
	} catch (Exception $e) {
		Propel::log('Could not initialize Peer: ' . $e->getMessage(), Propel::LOG_ERR);
	}
} else {
			require_once 'lib/model/performance/map/JabatanStrukturalMapBuilder.php';
	Propel::registerMapBuilder('lib.model.performance.map.JabatanStrukturalMapBuilder');
}
