<?php
class HttpHelper {
  public static function postData($xmlData,$host,$port,$post,$body,$header){
    $sock = fsockopen($host, $port, $errno, $errstr, 30);
    if (!$sock) die("$errstr ($errno)\n");
    fwrite($sock, "POST $post HTTP/1.0\r\n");
    fwrite($sock, "Host: $host\r\n");
    fwrite($sock, "Content-type: application/x-www-form-urlencoded\r\n");
    fwrite($sock, "Content-length: " . strlen($xmlData) . "\r\n");
    fwrite($sock, "Accept: */*\r\n");
    fwrite($sock, "\r\n");
    fwrite($sock, "$xmlData\r\n");
    fwrite($sock, "\r\n");
    $headers = "";
    while ($str = trim(fgets($sock, 4096))) $headers .= "$str\n";
    echo "\n";
    $body = "";
    while (!feof($sock))  $body .= fgets($sock, 4096);
    fclose($sock);
    $pjg_ok=(strlen($body)>0);
    return $pjg_ok;
  }
  public static function httpGet($host,$port,$url,$body,$headers){
    $berhasil=false;
    try {
      $sock = fsockopen($host, $port, $errno, $errstr, 300);
      if ($sock) {
        $message="GET $url HTTP/1.0\r\n".
        "Host: $host\r\n".
        "User-Agent: eProject HTTP Helper\r\n".
        "Accept: */*\r\n\r\n";        
        fwrite($sock, $message);
        $headers = "";
        while ($str = trim(fgets($sock, 4096))) $headers .= "$str\n";
        $body = "";
        while (!feof($sock))  $body .= fgets($sock, 40960);
        fclose($sock);
        $berhasil=true;
      } else {
        sfContext::getInstance()->getLogger()->err("{eProject} Error creating socket while trying to connect to $host:$port using url $url");
      }  
    } catch (Exception $e) {
        sfContext::getInstance()->getLogger()->err("{eProject} HTTP Get Error  while trying to connect to $host:$port using url $url\n".
          $e->getMessage());
    }  
    return $berhasil;
  }
}