<?php

class myUser extends sfBasicSecurityUser {

    public function logOut() {
        $this->getAttributeHolder()->removeNamespace($this->getCredentialMember());
        $this->getAttributeHolder()->removeNamespace('lokasi');
        $this->getAttributeHolder()->removeNamespace('lokasi_baru');
        $this->getAttributeHolder()->removeNamespace('simbada');
        $this->getAttributeHolder()->removeNamespace('simbada_baru');
        $this->getAttributeHolder()->removeNamespace('otorisasi');
        $this->setAuthenticated(false);
        $this->clearCredentials();
    }

    public function getCredentialMember() {
        if ($this->hasCredential('dinas') || $this->hasCredential('pptk') || $this->hasCredential('kpa') || $this->hasCredential('admindinas') || $this->hasCredential('pa')) {
            $credential = 'dinas';
        } elseif ($this->hasCredential('peneliti')) {
            $credential = 'peneliti';
        } elseif ($this->hasCredential('dewan')) {
            $credential = 'dewan';
        }
        //elseif($this->hasCredential('super-admin'))
//		{
//			$credential='super-admin';
//		}
        elseif ($this->hasCredential('admin')) {
            $credential = 'admin';
        } elseif ($this->hasCredential('admin_super')) {
            $credential = 'admin_super';
        } elseif ($this->hasCredential('data')) {
            $credential = 'data';
        } elseif ($this->hasCredential('viewer') || $this->hasCredential('bappeko')) {
            $credential = 'viewer';
        } else {
            $credential = 'none';
        }
        return $credential;
    }

    public function getUnitId() {
        return $this->getAttribute('unit_id', '0000', $this->getCredentialMember());
    }

    public function getNamaUser() {
        return $this->getAttribute('nama_user', 'pengguna', $this->getCredentialMember());
    }

    public function getNamaLengkap() {//untuk ambil nama lengkap
        return $this->getAttribute('nama_lengkap', 'nama lengkap', $this->getCredentialMember());
    }

    public function getNamaLogin() {//untuk ambil nama dinas
        return $this->getAttribute('nama_login', 'pengguna', $this->getCredentialMember());
    }

    public function getUserLogin() { //untuk session username
        return $this->getAttribute('user_login', 'username', $this->getCredentialMember());
    }

}
