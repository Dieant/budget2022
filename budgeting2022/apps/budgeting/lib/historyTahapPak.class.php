<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class historyTahapPak
{
    public static function cekTahap($unit_id,$kode_kegiatan)
    {   //unit_id=$master_kegiatan->getUnitId().'&kode_kegiatan='.$master_kegiatan->getKodeKegiatan()
        $queryTahap = "select tahap from ". sfConfig::get('app_default_schema') .".master_kegiatan where unit_id='$unit_id' and kode_kegiatan='$kode_kegiatan'";
        $con=Propel::getConnection();
        $statement2=$con->prepareStatement($queryTahap);
        $rs_tahap = $statement2->executeQuery();
        while($rs_tahap->next())
        {
                $tahap = $rs_tahap->getString('tahap');
        }
        return $tahap;
    }
    public static function delPakRevisi1($unit_id,$kode_kegiatan)
    {
        $con=Propel::getConnection();
        $sql1 = "delete from ". sfConfig::get('app_default_schema') .".pak_revisi1_master_kegiatan where unit_id='".$unit_id."'  and kode_kegiatan='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."';";
        $stmt = $con->prepareStatement($sql1);
        $stmt->executeQuery();

        $sql2 = "delete from ". sfConfig::get('app_default_schema') .".pak_revisi1_subtitle_indikator where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."';";
        $stmt = $con->prepareStatement($sql2);
        $stmt->executeQuery();

        $sql3 = "delete from ". sfConfig::get('app_default_schema') .".pak_revisi1_rincian_detail where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."';";
        $stmt = $con->prepareStatement($sql3);
        $stmt->executeQuery();

        $sql4 = "delete from ". sfConfig::get('app_default_schema') .".pak_revisi1_rincian where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."';";
        $stmt = $con->prepareStatement($sql4);
        $stmt->executeQuery();

        $sql5 = "delete from ". sfConfig::get('app_default_schema') .".pak_revisi1_rincian_sub_parameter where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."';";
        $stmt = $con->prepareStatement($sql5);
        $stmt->executeQuery();

        $sql6 = "delete from ". sfConfig::get('app_default_schema') .".pak_revisi1_rka_member where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."';";
        $stmt = $con->prepareStatement($sql6);
        $stmt->executeQuery();
    
    }
    public static function delPakRevisi2($unit_id,$kode_kegiatan)
    {
        $con=Propel::getConnection();
        $sql1 = "delete from ". sfConfig::get('app_default_schema') .".pak_revisi2_master_kegiatan where unit_id='".$unit_id."'  and kode_kegiatan='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."';";
        $stmt = $con->prepareStatement($sql1);
        $stmt->executeQuery();

        $sql2 = "delete from ". sfConfig::get('app_default_schema') .".pak_revisi2_subtitle_indikator where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."';";
        $stmt = $con->prepareStatement($sql2);
        $stmt->executeQuery();

        $sql3 = "delete from ". sfConfig::get('app_default_schema') .".pak_revisi2_rincian_detail where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."';";
        $stmt = $con->prepareStatement($sql3);
        $stmt->executeQuery();

        $sql4 = "delete from ". sfConfig::get('app_default_schema') .".pak_revisi2_rincian where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."';";
        $stmt = $con->prepareStatement($sql4);
        $stmt->executeQuery();

        $sql5 = "delete from ". sfConfig::get('app_default_schema') .".pak_revisi2_rincian_sub_parameter where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."';";
        $stmt = $con->prepareStatement($sql5);
        $stmt->executeQuery();

        $sql6 = "delete from ". sfConfig::get('app_default_schema') .".pak_revisi2_rka_member where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."';";
        $stmt = $con->prepareStatement($sql6);
        $stmt->executeQuery();
    
    }
    public static function delPakRevisi3($unit_id,$kode_kegiatan)
    {
        $con=Propel::getConnection();
        $sql1 = "delete from ". sfConfig::get('app_default_schema') .".pak_revisi3_master_kegiatan where unit_id='".$unit_id."'  and kode_kegiatan='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."';";
        $stmt = $con->prepareStatement($sql1);
        $stmt->executeQuery();

        $sql2 = "delete from ". sfConfig::get('app_default_schema') .".pak_revisi3_subtitle_indikator where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."';";
        $stmt = $con->prepareStatement($sql2);
        $stmt->executeQuery();

        $sql3 = "delete from ". sfConfig::get('app_default_schema') .".pak_revisi3_rincian_detail where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."';";
        $stmt = $con->prepareStatement($sql3);
        $stmt->executeQuery();

        $sql4 = "delete from ". sfConfig::get('app_default_schema') .".pak_revisi3_rincian where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."';";
        $stmt = $con->prepareStatement($sql4);
        $stmt->executeQuery();

        $sql5 = "delete from ". sfConfig::get('app_default_schema') .".pak_revisi3_rincian_sub_parameter where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."';";
        $stmt = $con->prepareStatement($sql5);
        $stmt->executeQuery();

        $sql6 = "delete from ". sfConfig::get('app_default_schema') .".pak_revisi3_rka_member where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."';";
        $stmt = $con->prepareStatement($sql6);
        $stmt->executeQuery();
    
    }
    public static function delPakRevisi4($unit_id,$kode_kegiatan)
    {
        $con=Propel::getConnection();
        $sql1 = "delete from ". sfConfig::get('app_default_schema') .".pak_revisi4_master_kegiatan where unit_id='".$unit_id."'  and kode_kegiatan='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."';";
        $stmt = $con->prepareStatement($sql1);
        $stmt->executeQuery();

        $sql2 = "delete from ". sfConfig::get('app_default_schema') .".pak_revisi4_subtitle_indikator where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."';";
        $stmt = $con->prepareStatement($sql2);
        $stmt->executeQuery();

        $sql3 = "delete from ". sfConfig::get('app_default_schema') .".pak_revisi4_rincian_detail where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."';";
        $stmt = $con->prepareStatement($sql3);
        $stmt->executeQuery();

        $sql4 = "delete from ". sfConfig::get('app_default_schema') .".pak_revisi4_rincian where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."';";
        $stmt = $con->prepareStatement($sql4);
        $stmt->executeQuery();

        $sql5 = "delete from ". sfConfig::get('app_default_schema') .".pak_revisi4_rincian_sub_parameter where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."';";
        $stmt = $con->prepareStatement($sql5);
        $stmt->executeQuery();

        $sql6 = "delete from ". sfConfig::get('app_default_schema') .".pak_revisi4_rka_member where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."';";
        $stmt = $con->prepareStatement($sql6);
        $stmt->executeQuery();
    
    }
    public static function delPakrevisi5($unit_id,$kode_kegiatan)
    {
        $con=Propel::getConnection();
        $sql1 = "delete from ". sfConfig::get('app_default_schema') .".pak_revisi5_master_kegiatan where unit_id='".$unit_id."'  and kode_kegiatan='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."';";
        $stmt = $con->prepareStatement($sql1);
        $stmt->executeQuery();

        $sql2 = "delete from ". sfConfig::get('app_default_schema') .".pak_revisi5_subtitle_indikator where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."';";
        $stmt = $con->prepareStatement($sql2);
        $stmt->executeQuery();

        $sql3 = "delete from ". sfConfig::get('app_default_schema') .".pak_revisi5_rincian_detail where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."';";
        $stmt = $con->prepareStatement($sql3);
        $stmt->executeQuery();

        $sql4 = "delete from ". sfConfig::get('app_default_schema') .".pak_revisi5_rincian where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."';";
        $stmt = $con->prepareStatement($sql4);
        $stmt->executeQuery();

        $sql5 = "delete from ". sfConfig::get('app_default_schema') .".pak_revisi5_rincian_sub_parameter where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."';";
        $stmt = $con->prepareStatement($sql5);
        $stmt->executeQuery();

        $sql6 = "delete from ". sfConfig::get('app_default_schema') .".pak_revisi5_rka_member where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."';";
        $stmt = $con->prepareStatement($sql6);
        $stmt->executeQuery();
    
    }
    public static function delNext($unit_id,$kode_kegiatan)
    {
        $con=Propel::getConnection();
        $sql1 = "delete from ". sfConfig::get('app_default_schema') .".next_master_kegiatan where unit_id='".$unit_id."'  and kode_kegiatan='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."';";
        $stmt = $con->prepareStatement($sql1);
        $stmt->executeQuery();

        $sql2 = "delete from ". sfConfig::get('app_default_schema') .".next_subtitle_indikator where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."';";
        $stmt = $con->prepareStatement($sql2);
        $stmt->executeQuery();

        $sql3 = "delete from ". sfConfig::get('app_default_schema') .".next_rincian_detail where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."';";
        $stmt = $con->prepareStatement($sql3);
        $stmt->executeQuery();

        $sql4 = "delete from ". sfConfig::get('app_default_schema') .".next_rincian where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."';";
        $stmt = $con->prepareStatement($sql4);
        $stmt->executeQuery();

        $sql5 = "delete from ". sfConfig::get('app_default_schema') .".next_rincian_sub_parameter where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."';";
        $stmt = $con->prepareStatement($sql5);
        $stmt->executeQuery();

        $sql6 = "delete from ". sfConfig::get('app_default_schema') .".next_rka_member where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."';";
        $stmt = $con->prepareStatement($sql6);
        $stmt->executeQuery();
    }
    public static function delPrev($unit_id,$kode_kegiatan)
    {   
        //var_dump('dor');
         //       print_r('crot');exit;
        $con=Propel::getConnection();
        $sql1 = "delete from ". sfConfig::get('app_default_schema') .".prev_master_kegiatan where unit_id='".$unit_id."'  and kode_kegiatan='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."';";
        $stmt = $con->prepareStatement($sql1);
        $stmt->executeQuery();
        //print_r($sql1);
        $sql2 = "delete from ". sfConfig::get('app_default_schema') .".prev_subtitle_indikator where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."';";
        $stmt = $con->prepareStatement($sql2);
        $stmt->executeQuery();
//print_r($sql2);
        $sql3 = "delete from ". sfConfig::get('app_default_schema') .".prev_rincian_detail where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."';";
        $stmt = $con->prepareStatement($sql3);
        $stmt->executeQuery();
//print_r($sql3);
        $sql4 = "delete from ". sfConfig::get('app_default_schema') .".prev_rincian where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."';";
        $stmt = $con->prepareStatement($sql4);
        $stmt->executeQuery();
//print_r($sql4);
        $sql5 = "delete from ". sfConfig::get('app_default_schema') .".prev_rincian_sub_parameter where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."';";
        $stmt = $con->prepareStatement($sql5);
        $stmt->executeQuery();
//print_r($sql5);
        $sql6 = "delete from ". sfConfig::get('app_default_schema') .".prev_rka_member where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."';";
        $stmt = $con->prepareStatement($sql6);
        $stmt->executeQuery();
//print_r($sql6);      
  //      print_r('crot');exit;
    }
    
    public static function insertNext($unit_id,$kode_kegiatan)
    {   
        $con=Propel::getConnection();
        $insert1 = "insert into ". sfConfig::get('app_default_schema') .".next_master_kegiatan select * from ". sfConfig::get('app_default_schema') .".master_kegiatan
            where unit_id='".$unit_id."' and kode_kegiatan='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."';";
        $stmt = $con->prepareStatement($insert1);
      //  echo $insert1;exit;
        $stmt->executeQuery();

        $insert2 = "insert into ". sfConfig::get('app_default_schema') .".next_subtitle_indikator select * from ". sfConfig::get('app_default_schema') .".subtitle_indikator
            where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."';";
        $stmt = $con->prepareStatement($insert2);
        $stmt->executeQuery();

        $insert3 = "insert into ". sfConfig::get('app_default_schema') .".next_rincian_detail select * from ". sfConfig::get('app_default_schema') .".rincian_detail
            where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."';";
        $stmt = $con->prepareStatement($insert3);
        $stmt->executeQuery();

        $insert4 = "insert into ". sfConfig::get('app_default_schema') .".next_rincian select * from ". sfConfig::get('app_default_schema') .".rincian
            where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."';";
        $stmt = $con->prepareStatement($insert4);
        $stmt->executeQuery();

        $insert5 = "insert into ". sfConfig::get('app_default_schema') .".next_rincian_sub_parameter select * from ". sfConfig::get('app_default_schema') .".rincian_sub_parameter
            where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."';";
        $stmt = $con->prepareStatement($insert5);
        $stmt->executeQuery();
        $insert6 = "insert into ". sfConfig::get('app_default_schema') .".next_rka_member select * from ". sfConfig::get('app_default_schema') .".rka_member
            where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."';";

        $stmt = $con->prepareStatement($insert6);
        $stmt->executeQuery();
    }
    public static function insertPrev($unit_id,$kode_kegiatan,$tahapInsert)
    {   
        $con=Propel::getConnection();
        $insert1 = "insert into ". sfConfig::get('app_default_schema') .".prev_master_kegiatan select * from ". sfConfig::get('app_default_schema') .".".$tahapInsert."_master_kegiatan
            where unit_id='".$unit_id."' and kode_kegiatan='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."';";
        $stmt = $con->prepareStatement($insert1);
      //  echo $insert1;exit;
        $stmt->executeQuery();

        $insert2 = "insert into ". sfConfig::get('app_default_schema') .".prev_subtitle_indikator select * from ". sfConfig::get('app_default_schema') .".".$tahapInsert."_subtitle_indikator
            where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."';";
        $stmt = $con->prepareStatement($insert2);
        $stmt->executeQuery();

        $insert3 = "insert into ". sfConfig::get('app_default_schema') .".prev_rincian_detail select * from ". sfConfig::get('app_default_schema') .".".$tahapInsert."_rincian_detail
            where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."';";
        $stmt = $con->prepareStatement($insert3);
        $stmt->executeQuery();

        $insert4 = "insert into ". sfConfig::get('app_default_schema') .".prev_rincian select * from ". sfConfig::get('app_default_schema') .".".$tahapInsert."_rincian
            where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."';";
        $stmt = $con->prepareStatement($insert4);
        $stmt->executeQuery();

        $insert5 = "insert into ". sfConfig::get('app_default_schema') .".prev_rincian_sub_parameter select * from ". sfConfig::get('app_default_schema') .".".$tahapInsert."_rincian_sub_parameter
            where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."';";
        $stmt = $con->prepareStatement($insert5);
        $stmt->executeQuery();
        $insert6 = "insert into ". sfConfig::get('app_default_schema') .".prev_rka_member select * from ". sfConfig::get('app_default_schema') .".".$tahapInsert."_rka_member
            where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."';";

        $stmt = $con->prepareStatement($insert6);
        $stmt->executeQuery();
    }
    public static function insertPrevDrLive($unit_id,$kode_kegiatan)
    {   
        $con=Propel::getConnection();
        $insert1 = "insert into ". sfConfig::get('app_default_schema') .".prev_master_kegiatan select * from ". sfConfig::get('app_default_schema') .".master_kegiatan
            where unit_id='".$unit_id."' and kode_kegiatan='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."';";
        $stmt = $con->prepareStatement($insert1);
      //  echo $insert1;exit;
        $stmt->executeQuery();

        $insert2 = "insert into ". sfConfig::get('app_default_schema') .".prev_subtitle_indikator select * from ". sfConfig::get('app_default_schema') .".subtitle_indikator
            where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."';";
        $stmt = $con->prepareStatement($insert2);
        $stmt->executeQuery();

        $insert3 = "insert into ". sfConfig::get('app_default_schema') .".prev_rincian_detail select * from ". sfConfig::get('app_default_schema') .".rincian_detail
            where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."';";
        $stmt = $con->prepareStatement($insert3);
        $stmt->executeQuery();

        $insert4 = "insert into ". sfConfig::get('app_default_schema') .".prev_rincian select * from ". sfConfig::get('app_default_schema') .".rincian
            where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."';";
        $stmt = $con->prepareStatement($insert4);
        $stmt->executeQuery();

        $insert5 = "insert into ". sfConfig::get('app_default_schema') .".prev_rincian_sub_parameter select * from ". sfConfig::get('app_default_schema') .".rincian_sub_parameter
            where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."';";
        $stmt = $con->prepareStatement($insert5);
        $stmt->executeQuery();
        $insert6 = "insert into ". sfConfig::get('app_default_schema') .".prev_rka_member select * from ". sfConfig::get('app_default_schema') .".rka_member
            where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."';";

        $stmt = $con->prepareStatement($insert6);
        $stmt->executeQuery();
    }
}
    
?>
