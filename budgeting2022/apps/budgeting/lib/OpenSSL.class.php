<?php
/*

CLASS OpenSSL

A wrapper class for a simple subset of the PHP OpenSSL functions. Use for public key encryption jobs.

=== Includes source code from many contributors to the PHP.net manual ===

....usage examples below....

Alex Poole 2005

php ~at~ wwwcrm.com

*/


// DEFINE('OPEN_SSL_CONF_PATH', 'C:/php/openssl/openssl.cnf'); // point to your config file
DEFINE('OPEN_SSL_CONF_PATH', '/usr/lib/ssl/openssl.cnf');   // point to your config file
DEFINE('OPEN_SSL_CERT_DAYS_VALID', 365);                    // 1 year
DEFINE('OPEN_SSL_IS_FILE', 1);

class OpenSSL
{

  private $private_key;             // resource or string private key
  private $private_key_passphrase;  // private key passphrase
  private $public_key;              // resource or string public key
  private $plain_text;
  private $crypted_text;
  private $envelope_key;            // ekey - set by encryption, required by decryption
  private $signature;
  private $csr;                     // certificate signing request string generated with keys
  private $config;
  private $public_key_mapper;
    
  public function OpenSSL()
  {
    $this->config = array('config' => OPEN_SSL_CONF_PATH);
    $this->public_key = array();
    $this->envelope_key = array();
    $this->public_key_mapper = array();
  }
    
  public function readf($path)
  {
    //return file contents
    $fp = fopen($path, 'r');
    $ret = fread($fp, 8192);
    fclose($fp);
    return $ret;
  }
    
  //private key can be text or file path
  public function setPrivateKey($private_key, $is_file = false, $key_passphrase = '')
  {
    if ($key_passphrase) {
      $this->private_key_passphrase = $key_passphrase;
    } 
    
    if ($is_file) {
      $private_key = $this->readf($private_key);        
    }
    $this->private_key = openssl_get_privatekey($private_key, $this->private_key_passphrase);
  }
    
  public function getPrivateKey()
  {
    return $this->private_key;
  }

  public function setPrivateKeyPassphrase($private_key_passphrase)
  {
    $this->private_key_passphrase = $private_key_passphrase;
  }
    
  public function getPrivateKeyPassphrase()
  {
    return $this->private_key_passphrase;
  }

  //public key can be text or file path
  public function addPublicKey($key, $public_key, $is_file = false)
  {
    if ($is_file) {
      $public_key = $this->readf($public_key);      
    }
    $this->public_key[$key] = openssl_get_publickey($public_key);
    $index = count($this->public_key_mapper);
    $this->public_key_mapper[$key] = $index++;
  }
    
  public function getPublicKey($key)
  {
    return $this->public_key[$key];
  }

/*
  public function setPublicKey($public_key, $is_file = false)
  {
    if ($is_file) {
      $public_key = $this->readf($public_key);      
    }
    $this->public_key = openssl_get_publickey($public_key);
  }
    
  public function getPublicKey()
  {
    return $this->public_key;
  }
*/

  public function setPlainText($text)
  {
    $this->plain_text = $text;
  }
    
  function getPlainText()
  {
    return $this->plain_text;
  }
  
  public function setCryptedText($text)
  {
    $this->crypted_text = $text;
  }
    
  public function getCryptedText()
  {
    return $this->crypted_text;
  }
  
  public function setEnvelopeKey($key, $envelope_key)
  {
    $this->envelope_key[$this->public_key_mapper[$key]] = $envelope_key;
  }
    
  public function getEnvelopeKey($key = 0)
  {
    return $this->envelope_key[$this->public_key_mapper[$key]];
  }
  
  public function setSignature($signature)
  {
    $this->signature = $signature;
  }
    
  public function getSignature()
  {
    return $this->signature;
  }
  
  public function sign($plain = '')
  {
    if ($plain) {
      $this->plain_text = $plain; 
    }
    
    $signature = null;
    openssl_sign($this->plain_text, $signature, $this->private_key);
    
    $this->signature = $signature;
  }
    
  public function encrypt($plain = '')
  {
    if ($plain) {
      $this->plain_text = $plain; 
    }
    
    $envelope_key = null;
    openssl_seal($this->plain_text, $this->crypted_text, $envelope_key, $this->public_key);
    
    $this->envelope_key = $envelope_key;
  }
    
  public function decrypt($key, $crypted_text = '', $envelope_key = '')
  {
    if ($crypted_text and $envelope_key) {
      return openssl_open($crypted_text, $this->plain_text, $envelope_key, $this->private_key);
    }
    else {
      if ($crypted_text) {
        $this->crypted_text = $crypted_text; 
      }
      if ($envelope_key) {
        $this->envelope_key = $envelope_key;
      }
      return openssl_open($this->crypted_text, $this->plain_text, $this->envelope_key[$this->public_key_mapper[$key]], $this->private_key);      
    }

  }
    
  public function createCSR(
    $countryName = 'UK',
    $stateOrProvinceName = 'London',
    $localityName = 'Blah',
    $organizationName = 'Blah1',
    $organizationalUnitName = 'Blah2',
    $commonName = 'Joe Bloggs',
    $emailAddress = 'openssl@domain.com')
  {
    $dn = Array(
      'countryName'             => $countryName,
      'stateOrProvinceName'     => $stateOrProvinceName,
      'localityName'            => $localityName,
      'organizationName'        => $organizationName,
      'organizationalUnitName'  => $organizationalUnitName,
      'commonName'              => $commonName,
      'emailAddress'            => $emailAddress
    );
    $private_key = openssl_pkey_new($this->config);
    $csr = openssl_csr_new($dn, $private_key, $this->config);
    $sscert = openssl_csr_sign($csr, null, $private_key, OPEN_SSL_CERT_DAYS_VALID, $this->config);
    openssl_x509_export($sscert, $this->public_key);
    openssl_pkey_export($private_key, $this->private_key, $this->private_key_passphrase, $this->config);
    openssl_csr_export($csr, $this->csr);
  }

  /*
  public function checkPrivateKey($key)
  {
    return openssl_x509_check_private_key($this->getPublicKey($key), $this->getPrivateKey());
  }
  */

  public static function checkPrivateKey($certificate = null, $private_key = null, $passphrase = null)
  {
    if (!$certificate or !$private_key) {
    	return false;
    }
    return openssl_x509_check_private_key($certificate, openssl_pkey_get_private($private_key, $passphrase));
  }

  public function hasPublicKey($key)
  { 
    if (!$key) {
    	return false;
    }
    if (!isset($this->public_key[$key])) {
    	return false;
    }
    return true;
  }

  public static function getInformation($certificate)
  {
    return openssl_x509_parse($certificate);
  }
  
  public static function getSubjectInformation($certificate)
  {
    $information = self::getInformation($certificate);
    return $information['subject'];
  }

    public static function verify($data, $signature, $certificate)
    {
            return openssl_verify($data, $signature, $certificate);
    }
}
?>
