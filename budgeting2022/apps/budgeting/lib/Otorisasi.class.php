<?php
class Otorisasi {
    const OTORISASI_IKP_GETCERT_URL='https://www.ikp-surabaya.com/index.php/repository/getCertByEmail.html?id=';
    public static function downloadPublicKey() {
        $certificate=false;
        $sf_user=sfContext::getInstance()->getUser();
        $email=$sf_user->getAttribute('email',null,'otorisasi'); //email user harus disimpan saat usernya login
        if ($email) {
            $certificate=file_get_contents(self::OTORISASI_IKP_GETCERT_URL.$email); //ambil public key dari situs IKP
            $kodeError=substr($certificate,0,10);
            if ($kodeError != '-----BEGIN') { //jika public key valid
                $certificate=false;
            }
        }
        return $certificate;

    }
    public static function validatePrivateKey( $private_key, $passphrase) {
        $return=false;
        $certificate=self::downloadPublicKey();
        if ($certificate) {
            $return=OpenSSL::checkPrivateKey($certificate, $private_key, $passphrase);
        }
        return $return;
    }
    public static function isValid(){
        $sf_user=sfContext::getInstance()->getUser();
        $privateKey=$sf_user->getAttribute('private_key',null,'otorisasi');
        $return=( $privateKey != null );
        return $return;
    }
    public static function reset(){
        $sf_user=sfContext::getInstance()->getUser();
        $sf_user->setAttribute('private_key','','otorisasi');
        $sf_user->setAttribute('passphrase','','otorisasi');
    }
}
?>
