<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class historyUserLog {

    public static function unlock_komponen_kegiatan($unit, $kegiatan) {
        $ip_address = $_SERVER['REMOTE_ADDR'];
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip_address .= ' / ' . $_SERVER['HTTP_X_FORWARDED_FOR'];
        }
        $c_insert_history_user = new HistoryUser();
        $c_insert_history_user->setUsername(sfContext::getInstance()->getUser()->getNamaLogin());
        $c_insert_history_user->setTimeAct(time());
        $c_insert_history_user->setIp($ip_address);
        $c_insert_history_user->setUnitId($unit);
        $c_insert_history_user->setKegiatanCode($kegiatan);
        $c_insert_history_user->setDescription('UNLOCK seluruh komponen di kegiatan ' . $kegiatan);
        $c_insert_history_user->setAksi('UNLOCK KOMPONEN KEGIATAN');
        $c_insert_history_user->save();
    }

    public static function lock_komponen_kegiatan($unit, $kegiatan) {
        $ip_address = $_SERVER['REMOTE_ADDR'];
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip_address .= ' / ' . $_SERVER['HTTP_X_FORWARDED_FOR'];
        }
        $c_insert_history_user = new HistoryUser();
        $c_insert_history_user->setUsername(sfContext::getInstance()->getUser()->getNamaLogin());
        $c_insert_history_user->setTimeAct(time());
        $c_insert_history_user->setIp($ip_address);
        $c_insert_history_user->setUnitId($unit);
        $c_insert_history_user->setKegiatanCode($kegiatan);
        $c_insert_history_user->setDescription('LOCK seluruh komponen di kegiatan ' . $kegiatan);
        $c_insert_history_user->setAksi('LOCK KOMPONEN KEGIATAN');
        $c_insert_history_user->save();
    }

    public static function unlock_per_komponen($unit, $kegiatan, $detail) {
        $ip_address = $_SERVER['REMOTE_ADDR'];
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip_address .= ' / ' . $_SERVER['HTTP_X_FORWARDED_FOR'];
        }

        $c_cari_rd = new Criteria();
        $c_cari_rd->add(RincianDetailPeer::UNIT_ID, $unit);
        $c_cari_rd->add(RincianDetailPeer::KEGIATAN_CODE, $kegiatan);
        $c_cari_rd->add(RincianDetailPeer::DETAIL_NO, $detail);
        $dapet_rd = RincianDetailPeer::doSelectOne($c_cari_rd);
        if ($dapet_rd) {
            $c_insert_history_user = new HistoryUser();
            $c_insert_history_user->setUsername(sfContext::getInstance()->getUser()->getNamaLogin());
            $c_insert_history_user->setTimeAct(time());
            $c_insert_history_user->setIp($ip_address);
            $c_insert_history_user->setUnitId($unit);
            $c_insert_history_user->setKegiatanCode($kegiatan);
            $c_insert_history_user->setDetailNo($detail);
            $c_insert_history_user->setDescription('UNLOCK komponen pada kegiatan ' . $kegiatan . ' untuk komponen '.$dapet_rd->getKomponenName().' '.$dapet_rd->getDetailName());
            $c_insert_history_user->setAksi('UNLOCK PERKOMPONEN');
            $c_insert_history_user->save();
        }
    }

    public static function lock_per_komponen($unit, $kegiatan, $detail) {
        $ip_address = $_SERVER['REMOTE_ADDR'];
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip_address .= ' / ' . $_SERVER['HTTP_X_FORWARDED_FOR'];
        }

        $c_cari_rd = new Criteria();
        $c_cari_rd->add(RincianDetailPeer::UNIT_ID, $unit);
        $c_cari_rd->add(RincianDetailPeer::KEGIATAN_CODE, $kegiatan);
        $c_cari_rd->add(RincianDetailPeer::DETAIL_NO, $detail);
        $dapet_rd = RincianDetailPeer::doSelectOne($c_cari_rd);
        if ($dapet_rd) {
            $c_insert_history_user = new HistoryUser();
            $c_insert_history_user->setUsername(sfContext::getInstance()->getUser()->getNamaLogin());
            $c_insert_history_user->setTimeAct(time());
            $c_insert_history_user->setIp($ip_address);
            $c_insert_history_user->setUnitId($unit);
            $c_insert_history_user->setKegiatanCode($kegiatan);
            $c_insert_history_user->setDetailNo($detail);
            $c_insert_history_user->setDescription('LOCK komponen pada kegiatan ' . $kegiatan . ' untuk komponen '.$dapet_rd->getKomponenName().' '.$dapet_rd->getDetailName());
            $c_insert_history_user->setAksi('LOCK PERKOMPONEN');
            $c_insert_history_user->save();
        }
    }

    public static function unlock_kegiatan_admin($unit, $kegiatan) {
        $ip_address = $_SERVER['REMOTE_ADDR'];
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip_address .= ' / ' . $_SERVER['HTTP_X_FORWARDED_FOR'];
        }
        $c_insert_history_user = new HistoryUser();
        $c_insert_history_user->setUsername(sfContext::getInstance()->getUser()->getNamaLogin());
        $c_insert_history_user->setTimeAct(time());
        $c_insert_history_user->setIp($ip_address);
        $c_insert_history_user->setUnitId($unit);
        $c_insert_history_user->setKegiatanCode($kegiatan);
        $c_insert_history_user->setDescription('UNLOCK kegiatan ' . $kegiatan);
        $c_insert_history_user->setAksi('UNLOCK KEGIATAN');
        $c_insert_history_user->save();
    }

    public static function lock_kegiatan_admin($unit, $kegiatan) {
        $ip_address = $_SERVER['REMOTE_ADDR'];
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip_address .= ' / ' . $_SERVER['HTTP_X_FORWARDED_FOR'];
        }
        $c_insert_history_user = new HistoryUser();
        $c_insert_history_user->setUsername(sfContext::getInstance()->getUser()->getNamaLogin());
        $c_insert_history_user->setTimeAct(time());
        $c_insert_history_user->setIp($ip_address);
        $c_insert_history_user->setUnitId($unit);
        $c_insert_history_user->setKegiatanCode($kegiatan);
        $c_insert_history_user->setDescription('LOCK kegiatan ' . $kegiatan);
        $c_insert_history_user->setAksi('LOCK KEGIATAN');
        $c_insert_history_user->save();
    }

    public static function buka_kegiatan_ke_dinas($unit, $kegiatan) {
        $ip_address = $_SERVER['REMOTE_ADDR'];
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip_address .= ' / ' . $_SERVER['HTTP_X_FORWARDED_FOR'];
        }
        $c_insert_history_user = new HistoryUser();
        $c_insert_history_user->setUsername(sfContext::getInstance()->getUser()->getNamaLogin());
        $c_insert_history_user->setTimeAct(time());
        $c_insert_history_user->setIp($ip_address);
        $c_insert_history_user->setUnitId($unit);
        $c_insert_history_user->setKegiatanCode($kegiatan);
        $c_insert_history_user->setDescription('Membuka Kegiatan ' . $kegiatan . ' ke posisi dinas');
        $c_insert_history_user->setAksi('BUKA KEGIATAN KE DINAS');
        $c_insert_history_user->save();
    }

    public static function tutup_kegiatan_ke_penyelia($unit, $kegiatan) {
        $ip_address = $_SERVER['REMOTE_ADDR'];
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip_address .= ' / ' . $_SERVER['HTTP_X_FORWARDED_FOR'];
        }
        $c_insert_history_user = new HistoryUser();
        $c_insert_history_user->setUsername(sfContext::getInstance()->getUser()->getNamaLogin());
        $c_insert_history_user->setTimeAct(time());
        $c_insert_history_user->setIp($ip_address);
        $c_insert_history_user->setUnitId($unit);
        $c_insert_history_user->setKegiatanCode($kegiatan);
        $c_insert_history_user->setDescription('Mengunci Kegiatan ' . $kegiatan . ' ke posisi penyelia');
        $c_insert_history_user->setAksi('KUNCI KEGIATAN KE PENYELIA');
        $c_insert_history_user->save();
    }

    public static function hapus_sub_subtitle($unit, $kegiatan, $komponen_name, $sub_lama) {
        $ip_address = $_SERVER['REMOTE_ADDR'];
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip_address .= ' / ' . $_SERVER['HTTP_X_FORWARDED_FOR'];
        }

        $c_insert_history_user = new HistoryUser();
        $c_insert_history_user->setUsername(sfContext::getInstance()->getUser()->getNamaLogin());
        $c_insert_history_user->setTimeAct(time());
        $c_insert_history_user->setIp($ip_address);
        $c_insert_history_user->setUnitId($unit);
        $c_insert_history_user->setKegiatanCode($kegiatan);
        $c_insert_history_user->setDescription('Menghapus Sub Subtitle Komponen ' . $komponen_name . ' ' . $sub_lama);
        $c_insert_history_user->setAksi('HAPUS SUB SUBTITLE');
        $c_insert_history_user->save();
    }

    public static function set_prioritas_subtitle($unit, $kegiatan, $subtitle, $prioritas) {
        $ip_address = $_SERVER['REMOTE_ADDR'];
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip_address .= ' / ' . $_SERVER['HTTP_X_FORWARDED_FOR'];
        }

        $dapat_subtitle = SubtitleIndikatorPeer::retrieveByPK($subtitle);
        if ($dapat_subtitle) {
            $c_insert_history_user = new HistoryUser();
            $c_insert_history_user->setUsername(sfContext::getInstance()->getUser()->getNamaLogin());
            $c_insert_history_user->setTimeAct(time());
            $c_insert_history_user->setIp($ip_address);
            $c_insert_history_user->setUnitId($unit);
            $c_insert_history_user->setKegiatanCode($kegiatan);
            $c_insert_history_user->setDescription('Memberi Prioritas ' . $prioritas . ' pada Subtitle ' . $dapat_subtitle->getSubtitle());
            $c_insert_history_user->setAksi('SET PRIORITAS SUBTITLE');
            $c_insert_history_user->save();
        }
    }

#52 - save Login user       

    public static function user_login() {
        $ip_address = $_SERVER['REMOTE_ADDR'];
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip_address .= ' / ' . $_SERVER['HTTP_X_FORWARDED_FOR'];
        }
        $c_insert_history_user = new HistoryUser();
        $c_insert_history_user->setUsername(sfContext::getInstance()->getUser()->getNamaLogin());
        $c_insert_history_user->setTimeAct(time());
        $c_insert_history_user->setIp($ip_address);
        $c_insert_history_user->setDescription('LOGIN pada aplikasi eBudgeting');
        $c_insert_history_user->setAksi('LOGIN');
        $c_insert_history_user->save();
    }

    /*
     * historyUserLog::user_login();
     */
#52 - save Login user
#53 - save Tambah Komponen Baru di eRevisi

    public static function tambah_komponen_revisi($unit, $kegiatan, $detno) {
        $ip_address = $_SERVER['REMOTE_ADDR'];
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip_address .= ' / ' . $_SERVER['HTTP_X_FORWARDED_FOR'];
        }

        $c_cari_rd = new Criteria();
        $c_cari_rd->add(DinasRincianDetailPeer::UNIT_ID, $unit);
        $c_cari_rd->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kegiatan);
        $c_cari_rd->add(DinasRincianDetailPeer::DETAIL_NO, $detno);
        $dapet_rd = DinasRincianDetailPeer::doSelectOne($c_cari_rd);

        $c_insert_history_user = new HistoryUser();
        $c_insert_history_user->setUsername(sfContext::getInstance()->getUser()->getNamaLogin());
        $c_insert_history_user->setTimeAct(time());
        $c_insert_history_user->setIp($ip_address);
        $c_insert_history_user->setUnitId($unit);
        $c_insert_history_user->setKegiatanCode($kegiatan);
        $c_insert_history_user->setDetailNo($detno);
        $c_insert_history_user->setDescription('Menambah komponen (eRevisi) ' . $dapet_rd->getKomponenName() . ' ' . $dapet_rd->getDetailName() . ' dengan volume ' . $dapet_rd->getVolume() . ' pada Subtitle ' . $dapet_rd->getSubtitle());
        $c_insert_history_user->setAksi('MENAMBAH KOMPONEN REVISI');
        $c_insert_history_user->save();
    }

    /*
     * historyUserLog::tambah_komponen_revisi($unit,$kegiatan,$detno);
     */
       
#53 - save Tambah Komponen Baru di eRevisi
#53 - save Tambah Komponen Baru                   

    public static function tambah_komponen($unit, $kegiatan, $detno) {
        $ip_address = $_SERVER['REMOTE_ADDR'];
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip_address .= ' / ' . $_SERVER['HTTP_X_FORWARDED_FOR'];
        }

        $c_cari_rd = new Criteria();
        $c_cari_rd->add(RincianDetailPeer::UNIT_ID, $unit);
        $c_cari_rd->add(RincianDetailPeer::KEGIATAN_CODE, $kegiatan);
        $c_cari_rd->add(RincianDetailPeer::DETAIL_NO, $detno);
        $dapet_rd = RincianDetailPeer::doSelectOne($c_cari_rd);

        $c_insert_history_user = new HistoryUser();
        $c_insert_history_user->setUsername(sfContext::getInstance()->getUser()->getNamaLogin());
        $c_insert_history_user->setTimeAct(time());
        $c_insert_history_user->setIp($ip_address);
        $c_insert_history_user->setUnitId($unit);
        $c_insert_history_user->setKegiatanCode($kegiatan);
        $c_insert_history_user->setDetailNo($detno);
        $c_insert_history_user->setDescription('Menambah komponen ' . $dapet_rd->getKomponenName() . ' ' . $dapet_rd->getDetailName() . ' dengan volume ' . $dapet_rd->getVolume() . ' pada Subtitle ' . $dapet_rd->getSubtitle());
        $c_insert_history_user->setAksi('MENAMBAH KOMPONEN');
        $c_insert_history_user->save();
    }

    /*
     * historyUserLog::tambah_komponen($unit,$kegiatan,$detno);
     */
#53 - save Tambah Komponen Baru  
#53 - save Hapus Komponen

    public static function hapus_komponen($unit, $kegiatan, $detno) {
        $ip_address = $_SERVER['REMOTE_ADDR'];
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip_address .= ' / ' . $_SERVER['HTTP_X_FORWARDED_FOR'];
        }

        $c_cari_rd = new Criteria();
        $c_cari_rd->add(DinasRincianDetailPeer::UNIT_ID, $unit);
        $c_cari_rd->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kegiatan);
        $c_cari_rd->add(DinasRincianDetailPeer::DETAIL_NO, $detno);
        $dapet_rd = DinasRincianDetailPeer::doSelectOne($c_cari_rd);

        $c_insert_history_user = new HistoryUser();
        $c_insert_history_user->setUsername(sfContext::getInstance()->getUser()->getNamaLogin());
        $c_insert_history_user->setTimeAct(time());
        $c_insert_history_user->setIp($ip_address);
        $c_insert_history_user->setUnitId($unit);
        $c_insert_history_user->setKegiatanCode($kegiatan);
        $c_insert_history_user->setDetailNo($detno);
        $c_insert_history_user->setDescription('Menghapus komponen ' . $dapet_rd->getKomponenName() . ' ' . $dapet_rd->getDetailName() . ' dengan volume ' . $dapet_rd->getVolume() . ' pada Subtitle ' . $dapet_rd->getSubtitle());
        $c_insert_history_user->setAksi('MENGHAPUS KOMPONEN');
        $c_insert_history_user->save();
    }

    /*
     * historyUserLog::hapus_komponen($unit,$kegiatan,$detno);
     */
#53 - save Hapus Komponen
#53 - save Ubah Volume Komponen

    public static function ubah_komponen_volume($unit, $kegiatan, $detno, $volume_awal) {
        $ip_address = $_SERVER['REMOTE_ADDR'];
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip_address .= ' / ' . $_SERVER['HTTP_X_FORWARDED_FOR'];
        }

        $c_cari_rd = new Criteria();
        $c_cari_rd->add(RincianDetailPeer::UNIT_ID, $unit);
        $c_cari_rd->add(RincianDetailPeer::KEGIATAN_CODE, $kegiatan);
        $c_cari_rd->add(RincianDetailPeer::DETAIL_NO, $detno);
        $dapet_rd = RincianDetailPeer::doSelectOne($c_cari_rd);

        $c_insert_history_user = new HistoryUser();
        $c_insert_history_user->setUsername(sfContext::getInstance()->getUser()->getNamaLogin());
        $c_insert_history_user->setTimeAct(time());
        $c_insert_history_user->setIp($ip_address);
        $c_insert_history_user->setUnitId($unit);
        $c_insert_history_user->setKegiatanCode($kegiatan);
        $c_insert_history_user->setDetailNo($detno);
        $c_insert_history_user->setDescription('Mengubah komponen ' . $dapet_rd->getKomponenName() . ' ' . $dapet_rd->getDetailName() . ' dari volume ' . $volume_awal . ' menjadi ' . $dapet_rd->getVolume() . ' pada Subtitle ' . $dapet_rd->getSubtitle());
        $c_insert_history_user->setAksi('MENGUBAH KOMPONEN');
        $c_insert_history_user->save();
    }

    /*
     * historyUserLog::ubah_komponen_volume($unit, $kegiatan, $detno, $volume_awal);
     */
#53 - save Ubah Volume Komponen
##53 - save Ubah Volume Komponen Revisi

    public static function ubah_komponen_volume_revisi($unit, $kegiatan, $detno, $volume_awal) {
        $ip_address = $_SERVER['REMOTE_ADDR'];
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip_address .= ' / ' . $_SERVER['HTTP_X_FORWARDED_FOR'];
        }

        $c_cari_rd = new Criteria();
        $c_cari_rd->add(DinasRincianDetailPeer::UNIT_ID, $unit);
        $c_cari_rd->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kegiatan);
        $c_cari_rd->add(DinasRincianDetailPeer::DETAIL_NO, $detno);
        $dapet_rd = DinasRincianDetailPeer::doSelectOne($c_cari_rd);

        $c_insert_history_user = new HistoryUser();
        $c_insert_history_user->setUsername(sfContext::getInstance()->getUser()->getNamaLogin());
        $c_insert_history_user->setTimeAct(time());
        $c_insert_history_user->setIp($ip_address);
        $c_insert_history_user->setUnitId($unit);
        $c_insert_history_user->setKegiatanCode($kegiatan);
        $c_insert_history_user->setDetailNo($detno);
        $c_insert_history_user->setDescription('Mengubah komponen (eRevisi) ' . $dapet_rd->getKomponenName() . ' ' . $dapet_rd->getDetailName() . ' dari volume ' . $volume_awal . ' menjadi ' . $dapet_rd->getVolume() . ' pada Subtitle ' . $dapet_rd->getSubtitle());
        $c_insert_history_user->setAksi('MENGUBAH KOMPONEN REVISI');
        $c_insert_history_user->save();
    }

    public static function ubah_komponen_harga_revisi($unit, $kegiatan, $detno, $harga_awal) {
        $ip_address = $_SERVER['REMOTE_ADDR'];
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip_address .= ' / ' . $_SERVER['HTTP_X_FORWARDED_FOR'];
        }

        $c_cari_rd = new Criteria();
        $c_cari_rd->add(DinasRincianDetailPeer::UNIT_ID, $unit);
        $c_cari_rd->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kegiatan);
        $c_cari_rd->add(DinasRincianDetailPeer::DETAIL_NO, $detno);
        $dapet_rd = DinasRincianDetailPeer::doSelectOne($c_cari_rd);

        $c_insert_history_user = new HistoryUser();
        $c_insert_history_user->setUsername(sfContext::getInstance()->getUser()->getNamaLogin());
        $c_insert_history_user->setTimeAct(time());
        $c_insert_history_user->setIp($ip_address);
        $c_insert_history_user->setUnitId($unit);
        $c_insert_history_user->setKegiatanCode($kegiatan);
        $c_insert_history_user->setDetailNo($detno);
        $c_insert_history_user->setDescription('Mengubah komponen (eRevisi) ' . $dapet_rd->getKomponenName() . ' ' . $dapet_rd->getDetailName() . ' dari anggaran ' . $harga_awal . ' menjadi ' . $dapet_rd->getNilaiAnggaran() . ' pada Subtitle ' . $dapet_rd->getSubtitle());
        $c_insert_history_user->setAksi('MENGUBAH KOMPONEN REVISI');
        $c_insert_history_user->save();
    }


    /*
     * historyUserLog::ubah_komponen_volume_revisi($unit, $kegiatan, $detno, $volume_awal);
     */
#53 - save Ubah Volume Komponen Revisi
#53 - save Lock Harga Dasar Komponen

    public static function sisa_lelang_komponen($unit, $kegiatan, $detno, $harga_awal, $volume_awal, $satuan_awal) {
        $ip_address = $_SERVER['REMOTE_ADDR'];
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip_address .= ' / ' . $_SERVER['HTTP_X_FORWARDED_FOR'];
        }

        $c_cari_rd = new Criteria();
        $c_cari_rd->add(RincianDetailPeer::UNIT_ID, $unit);
        $c_cari_rd->add(RincianDetailPeer::KEGIATAN_CODE, $kegiatan);
        $c_cari_rd->add(RincianDetailPeer::DETAIL_NO, $detno);
        $dapet_rd = RincianDetailPeer::doSelectOne($c_cari_rd);

        $c_insert_history_user = new HistoryUser();
        $c_insert_history_user->setUsername(sfContext::getInstance()->getUser()->getNamaLogin());
        $c_insert_history_user->setTimeAct(time());
        $c_insert_history_user->setIp($ip_address);
        $c_insert_history_user->setUnitId($unit);
        $c_insert_history_user->setKegiatanCode($kegiatan);
        $c_insert_history_user->setDetailNo($detno);
        $c_insert_history_user->setDescription('Penggunaan Sisa Lelang komponen ' . $dapet_rd->getKomponenName() . ' ' . $dapet_rd->getDetailName() . ' dari harga ' . $harga_awal . ' dengan koefisien ' . $volume_awal . ' ' . $satuan_awal . ' menjadi ' . $dapet_rd->getKomponenHargaAwal() . ' dengan koefisien 1 Paket pada Subtitle ' . $dapet_rd->getSubtitle());
        $c_insert_history_user->setAksi('SISA LELANG KOMPONEN');
        $c_insert_history_user->save();
    }

    /*
     * historyUserLog::ubah_komponen_volume($unit, $kegiatan, $detno, $volume_awal);
     */
#53 - save Lock Harga Dasar Komponen
#53 - save Lock Harga Dasar Komponen Revisi

    public static function sisa_lelang_komponen_revisi($unit, $kegiatan, $detno, $harga_awal, $volume_awal, $satuan_awal) {
        $ip_address = $_SERVER['REMOTE_ADDR'];
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip_address .= ' / ' . $_SERVER['HTTP_X_FORWARDED_FOR'];
        }

        $c_cari_rd = new Criteria();
        $c_cari_rd->add(DinasRincianDetailPeer::UNIT_ID, $unit);
        $c_cari_rd->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kegiatan);
        $c_cari_rd->add(DinasRincianDetailPeer::DETAIL_NO, $detno);
        $dapet_rd = DinasRincianDetailPeer::doSelectOne($c_cari_rd);

        $c_insert_history_user = new HistoryUser();
        $c_insert_history_user->setUsername(sfContext::getInstance()->getUser()->getNamaLogin());
        $c_insert_history_user->setTimeAct(time());
        $c_insert_history_user->setIp($ip_address);
        $c_insert_history_user->setUnitId($unit);
        $c_insert_history_user->setKegiatanCode($kegiatan);
        $c_insert_history_user->setDetailNo($detno);
        $c_insert_history_user->setDescription('Penggunaan Sisa Lelang komponen ' . $dapet_rd->getKomponenName() . ' ' . $dapet_rd->getDetailName() . ' dari harga ' . $harga_awal . ' dengan koefisien ' . $volume_awal . ' ' . $satuan_awal . ' menjadi ' . $dapet_rd->getKomponenHargaAwal() . ' dengan koefisien 1 Paket pada Subtitle ' . $dapet_rd->getSubtitle());
        $c_insert_history_user->setAksi('SISA LELANG KOMPONEN');
        $c_insert_history_user->save();
    }

    /*
     * historyUserLog::ubah_komponen_volume($unit, $kegiatan, $detno, $volume_awal);
     */
#53 - save Lock Harga Dasar Komponen Revisi 
#53 - save Tambah Komponen Penyusun Baru                   

    public static function tambah_komponen_penyusun($unit, $kegiatan, $detno) {
        $ip_address = $_SERVER['REMOTE_ADDR'];
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip_address .= ' / ' . $_SERVER['HTTP_X_FORWARDED_FOR'];
        }

        $c_cari_rd = new Criteria();
        $c_cari_rd->add(RincianDetailPeer::UNIT_ID, $unit);
        $c_cari_rd->add(RincianDetailPeer::KEGIATAN_CODE, $kegiatan);
        $c_cari_rd->add(RincianDetailPeer::DETAIL_NO, $detno);
        $dapet_rd = RincianDetailPeer::doSelectOne($c_cari_rd);

        $c_insert_history_user = new HistoryUser();
        $c_insert_history_user->setUsername(sfContext::getInstance()->getUser()->getNamaLogin());
        $c_insert_history_user->setTimeAct(time());
        $c_insert_history_user->setIp($ip_address);
        $c_insert_history_user->setUnitId($unit);
        $c_insert_history_user->setKegiatanCode($kegiatan);
        $c_insert_history_user->setDetailNo($detno);
        $c_insert_history_user->setDescription('Menambah komponen penyusun ' . $dapet_rd->getKomponenName() . ' ' . $dapet_rd->getDetailName() . ' dengan volume ' . $dapet_rd->getVolume() . ' pada Subtitle ' . $dapet_rd->getSubtitle());
        $c_insert_history_user->setAksi('MENAMBAH KOMPONEN');
        $c_insert_history_user->save();
    }

    /*
     * historyUserLog::tambah_komponen_penyusun($unit,$kegiatan,$detno);
     */
#53 - save Tambah Komponen Penyusun Baru    
#53 - save Tambah Komponen Penyusun Baru Revisi

    public static function tambah_komponen_penyusun_revisi($unit, $kegiatan, $detno) {
        $ip_address = $_SERVER['REMOTE_ADDR'];
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip_address .= ' / ' . $_SERVER['HTTP_X_FORWARDED_FOR'];
        }

        $c_cari_rd = new Criteria();
        $c_cari_rd->add(DinasRincianDetailPeer::UNIT_ID, $unit);
        $c_cari_rd->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kegiatan);
        $c_cari_rd->add(DinasRincianDetailPeer::DETAIL_NO, $detno);
        $dapet_rd = DinasRincianDetailPeer::doSelectOne($c_cari_rd);

        $c_insert_history_user = new HistoryUser();
        $c_insert_history_user->setUsername(sfContext::getInstance()->getUser()->getNamaLogin());
        $c_insert_history_user->setTimeAct(time());
        $c_insert_history_user->setIp($ip_address);
        $c_insert_history_user->setUnitId($unit);
        $c_insert_history_user->setKegiatanCode($kegiatan);
        $c_insert_history_user->setDetailNo($detno);
        $c_insert_history_user->setDescription('Menambah komponen penyusun (eRevisi) ' . $dapet_rd->getKomponenName() . ' ' . $dapet_rd->getDetailName() . ' dengan volume ' . $dapet_rd->getVolume() . ' pada Subtitle ' . $dapet_rd->getSubtitle());
        $c_insert_history_user->setAksi('MENAMBAH KOMPONEN REVISI');
        $c_insert_history_user->save();
    }

    /*
     * historyUserLog::tambah_komponen_penyusun_revisi($unit,$kegiatan,$detno);
     */
#53 - save Tambah Komponen Penyusun Baru Revisi  
#53 - save Tambah Komponen Penyusun Baru

    public static function tambah_komponen_fisik_no_lokasi($unit, $kegiatan, $detno) {
        $ip_address = $_SERVER['REMOTE_ADDR'];
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip_address .= ' / ' . $_SERVER['HTTP_X_FORWARDED_FOR'];
        }

        $c_cari_rd = new Criteria();
        $c_cari_rd->add(RincianDetailPeer::UNIT_ID, $unit);
        $c_cari_rd->add(RincianDetailPeer::KEGIATAN_CODE, $kegiatan);
        $c_cari_rd->add(RincianDetailPeer::DETAIL_NO, $detno);
        $dapet_rd = RincianDetailPeer::doSelectOne($c_cari_rd);

        $c_insert_history_user = new HistoryUser();
        $c_insert_history_user->setUsername(sfContext::getInstance()->getUser()->getNamaLogin());
        $c_insert_history_user->setTimeAct(time());
        $c_insert_history_user->setIp($ip_address);
        $c_insert_history_user->setUnitId($unit);
        $c_insert_history_user->setKegiatanCode($kegiatan);
        $c_insert_history_user->setDetailNo($detno);
        $c_insert_history_user->setDescription('Menyembunyikan komponen fisik ' . $dapet_rd->getKomponenName() . ' ' . $dapet_rd->getDetailName() . ' dengan volume ' . $dapet_rd->getVolume() . ' pada Subtitle ' . $dapet_rd->getSubtitle());
        $c_insert_history_user->setAksi('KOMPONEN FISIK HIDDEN');
        $c_insert_history_user->save();
    }

    /*
     * historyUserLog::tambah_komponen_fisik_no_lokasi($unit,$kegiatan,$detno);
     */
#53 - save Tambah Komponen Penyusun Baru
#53 - save Tambah Komponen Penyusun Baru Revisi

    public static function tambah_komponen_fisik_no_lokasi_revisi($unit, $kegiatan, $detno) {
        $ip_address = $_SERVER['REMOTE_ADDR'];
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip_address .= ' / ' . $_SERVER['HTTP_X_FORWARDED_FOR'];
        }

        $c_cari_rd = new Criteria();
        $c_cari_rd->add(DinasRincianDetailPeer::UNIT_ID, $unit);
        $c_cari_rd->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kegiatan);
        $c_cari_rd->add(DinasRincianDetailPeer::DETAIL_NO, $detno);
        $dapet_rd = DinasRincianDetailPeer::doSelectOne($c_cari_rd);

        $c_insert_history_user = new HistoryUser();
        $c_insert_history_user->setUsername(sfContext::getInstance()->getUser()->getNamaLogin());
        $c_insert_history_user->setTimeAct(time());
        $c_insert_history_user->setIp($ip_address);
        $c_insert_history_user->setUnitId($unit);
        $c_insert_history_user->setKegiatanCode($kegiatan);
        $c_insert_history_user->setDetailNo($detno);
        $c_insert_history_user->setDescription('Menyembunyikan komponen fisik (eRevisi) ' . $dapet_rd->getKomponenName() . ' ' . $dapet_rd->getDetailName() . ' dengan volume ' . $dapet_rd->getVolume() . ' pada Subtitle ' . $dapet_rd->getSubtitle());
        $c_insert_history_user->setAksi('KOMPONEN FISIK HIDDEN REVISI');
        $c_insert_history_user->save();
    }

    /*
     * historyUserLog::tambah_komponen_fisik_no_lokasi_revisi($unit,$kegiatan,$detno);
     */
#53 - save Tambah Komponen Penyusun Baru Revisi
#55 - save Tambah Lokasi GMAP         

    public static function tambah_lokasi($unit, $kegiatan, $detno) {
        $ip_address = $_SERVER['REMOTE_ADDR'];
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip_address .= ' / ' . $_SERVER['HTTP_X_FORWARDED_FOR'];
        }

        $c_cari_rd = new Criteria();
        $c_cari_rd->add(RincianDetailPeer::UNIT_ID, $unit);
        $c_cari_rd->add(RincianDetailPeer::KEGIATAN_CODE, $kegiatan);
        $c_cari_rd->add(RincianDetailPeer::DETAIL_NO, $detno);
        $dapet_rd = RincianDetailPeer::doSelectOne($c_cari_rd);

        $c_insert_history_user = new HistoryUser();
        $c_insert_history_user->setUsername(sfContext::getInstance()->getUser()->getNamaLogin());
        $c_insert_history_user->setTimeAct(time());
        $c_insert_history_user->setIp($ip_address);
        $c_insert_history_user->setUnitId($unit);
        $c_insert_history_user->setKegiatanCode($kegiatan);
        $c_insert_history_user->setDetailNo($detno);
        $c_insert_history_user->setAksi('MENAMBAH LOKASI & MEMUNCULKAN KOMPONEN FISIK');
        $c_insert_history_user->setDescription('Menambah lokasi pada GMAP dan memunculkan komponen ' . $dapet_rd->getKomponenName() . ' ' . $dapet_rd->getDetailName() . ' pada Subtitle ' . $dapet_rd->getSubtitle());
        $c_insert_history_user->save();
    }

    /*
     * historyUserLog::tambah_lokasi($unit_id,$kegiatan_code,$detail_no);
     */
#55 - save Tambah Lokasi GMAP                    
#55 - save Ubah Lokasi GMAP      

    public static function ubah_lokasi($unit, $kegiatan, $detno) {
        $ip_address = $_SERVER['REMOTE_ADDR'];
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip_address .= ' / ' . $_SERVER['HTTP_X_FORWARDED_FOR'];
        }

        $c_cari_rd = new Criteria();
        $c_cari_rd->add(RincianDetailPeer::UNIT_ID, $unit);
        $c_cari_rd->add(RincianDetailPeer::KEGIATAN_CODE, $kegiatan);
        $c_cari_rd->add(RincianDetailPeer::DETAIL_NO, $detno);
        $dapet_rd = RincianDetailPeer::doSelectOne($c_cari_rd);

        $c_insert_history_user = new HistoryUser();
        $c_insert_history_user->setUsername(sfContext::getInstance()->getUser()->getNamaLogin());
        $c_insert_history_user->setTimeAct(time());
        $c_insert_history_user->setIp($ip_address);
        $c_insert_history_user->setUnitId($unit);
        $c_insert_history_user->setKegiatanCode($kegiatan);
        $c_insert_history_user->setDetailNo($detno);
        $c_insert_history_user->setAksi('MENGUBAH LOKASI');
        $c_insert_history_user->setDescription('Mengubah lokasi pada GMAP untuk komponen ' . $dapet_rd->getKomponenName() . ' ' . $dapet_rd->getDetailName() . ' pada Subtitle ' . $dapet_rd->getSubtitle());
        $c_insert_history_user->save();
    }

    /*
     * historyUserLog::ubah_lokasi($unit_id,$kegiatan_code,$detail_no);
     */
#55 - save Ubah Lokasi GMAP                        
#55 - Hapus Lokasi GMAP   

    public static function hapus_lokasi($unit, $kegiatan, $detno) {
        $ip_address = $_SERVER['REMOTE_ADDR'];
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip_address .= ' / ' . $_SERVER['HTTP_X_FORWARDED_FOR'];
        }

        $c_cari_rd = new Criteria();
        $c_cari_rd->add(RincianDetailPeer::UNIT_ID, $unit);
        $c_cari_rd->add(RincianDetailPeer::KEGIATAN_CODE, $kegiatan);
        $c_cari_rd->add(RincianDetailPeer::DETAIL_NO, $detno);
        $dapet_rd = RincianDetailPeer::doSelectOne($c_cari_rd);

        $c_insert_history_user = new HistoryUser();
        $c_insert_history_user->setUsername(sfContext::getInstance()->getUser()->getNamaLogin());
        $c_insert_history_user->setTimeAct(time());
        $c_insert_history_user->setIp($ip_address);
        $c_insert_history_user->setUnitId($unit);
        $c_insert_history_user->setKegiatanCode($kegiatan);
        $c_insert_history_user->setDetailNo($detno);
        $c_insert_history_user->setAksi('MENGHAPUS LOKASI');
        $c_insert_history_user->setDescription('Menghapus lokasi pada GMAP untuk komponen ' . $dapet_rd->getKomponenName() . ' ' . $dapet_rd->getDetailName() . ' pada Subtitle ' . $dapet_rd->getSubtitle());
        $c_insert_history_user->save();
    }

    /*
     * historyUserLog::hapus_lokasi($unit_id,$kegiatan_code,$detail_no);
     */
#55 - Hapus Lokasi GMAP                   

    public static function edit_komponen_survey($shsd_id, $surveyor) {
        $ip_address = $_SERVER['REMOTE_ADDR'];
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip_address .= ' / ' . $_SERVER['HTTP_X_FORWARDED_FOR'];
        }

        $c = new Criteria();
        $c->add(ShsdSurveyPeer::SHSD_ID, $shsd_id);
        $shsd = ShsdSurveyPeer::doSelectOne($c);

        $c_insert_history_user = new HistoryUser();
        $c_insert_history_user->setUsername(sfContext::getInstance()->getUser()->getNamaLogin());
        $c_insert_history_user->setTimeAct(time());
        $c_insert_history_user->setIp($ip_address);
        $c_insert_history_user->setUnitId('');
        $c_insert_history_user->setKegiatanCode('');
        $c_insert_history_user->setDetailNo(0);
        $c_insert_history_user->setAksi('MENGEDIT KOMPONEN SURVEY');
        $c_insert_history_user->setDescription('Mengedit komponen SSH ' . $shsd->getShsdId() . ' - ' . $shsd->getShsdName() . ' (' . $shsd->getSpec() . ') By '. $surveyor );
        $c_insert_history_user->save();
    }

    public static function upload_excel_survey($file_excel) {
        $ip_address = $_SERVER['REMOTE_ADDR'];
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip_address .= ' / ' . $_SERVER['HTTP_X_FORWARDED_FOR'];
        }

        $c_insert_history_user = new HistoryUser();
        $c_insert_history_user->setUsername(sfContext::getInstance()->getUser()->getNamaLogin());
        $c_insert_history_user->setTimeAct(time());
        $c_insert_history_user->setIp($ip_address);
        $c_insert_history_user->setUnitId('');
        $c_insert_history_user->setKegiatanCode('');
        $c_insert_history_user->setDetailNo(0);
        $c_insert_history_user->setAksi('MENGEDIT KOMPONEN SURVEY');
        $c_insert_history_user->setDescription('Mengupload Excel Survey SSH (' . $file_excel . ')');
        $c_insert_history_user->save();
    } 
     /*
     * historyUserLog::buka komponen khusus //sumber dana,penanda,tenaga kontrak, output,dan komponen yang terkunci lainnya//($unit_id,$kegiatan_code,$detail_no);
     */
    #56 - Tambahan buka komponen khusus 

     public static function unlock_per_komponen_khusus($unit, $kegiatan, $detail, $aksi) {
        $ip_address = $_SERVER['REMOTE_ADDR'];
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip_address .= ' / ' . $_SERVER['HTTP_X_FORWARDED_FOR'];
        }

        $c_cari_rd = new Criteria();
        $c_cari_rd->add(DinasRincianDetailPeer::UNIT_ID, $unit);
        $c_cari_rd->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kegiatan);
        $c_cari_rd->add(DinasRincianDetailPeer::DETAIL_NO, $detail);
        $dapet_rd = DinasRincianDetailPeer::doSelectOne($c_cari_rd);
        if ($dapet_rd) {
            $c_insert_history_user = new HistoryUser();
            $c_insert_history_user->setUsername(sfContext::getInstance()->getUser()->getNamaLogin());
            $c_insert_history_user->setTimeAct(time());
            $c_insert_history_user->setIp($ip_address);
            $c_insert_history_user->setUnitId($unit);
            $c_insert_history_user->setKegiatanCode($kegiatan);
            $c_insert_history_user->setDetailNo($detail);
            $c_insert_history_user->setDescription('UNLOCK komponen pada kegiatan ' . $kegiatan . ' untuk komponen '.$dapet_rd->getKomponenName().' '.$dapet_rd->getDetailName().' '.$dapet_rd->getDetailKegiatan());
            $c_insert_history_user->setAksi($aksi);
            $c_insert_history_user->save();
        }
    }

     public static function update_sumber_dana($unit, $kegiatan, $detail, $penanda_id) {
        $ip_address = $_SERVER['REMOTE_ADDR'];
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip_address .= ' / ' . $_SERVER['HTTP_X_FORWARDED_FOR'];
        }

        $c_cari_rd = new Criteria();
        $c_cari_rd->add(DinasRincianDetailPeer::UNIT_ID, $unit);
        $c_cari_rd->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kegiatan);
        $c_cari_rd->add(DinasRincianDetailPeer::DETAIL_NO, $detail);
        $dapet_rd = DinasRincianDetailPeer::doSelectOne($c_cari_rd);
        if ($dapet_rd) {
            $c_insert_history_user = new HistoryUser();
            $c_insert_history_user->setUsername(sfContext::getInstance()->getUser()->getNamaLogin());
            $c_insert_history_user->setTimeAct(time());
            $c_insert_history_user->setIp($ip_address);
            $c_insert_history_user->setUnitId($unit);
            $c_insert_history_user->setKegiatanCode($kegiatan);
            $c_insert_history_user->setDetailNo($detail);
            $c_insert_history_user->setDescription('Update Sumber dana id pada kode sub kegiatan ' . $kegiatan . ' untuk komponen '.$dapet_rd->getKomponenName().' '.$dapet_rd->getDetailName().' '.$dapet_rd->getDetailKegiatan().'dengan sumber dana id baru : '. $penanda_id);
            $c_insert_history_user->setAksi($aksi);
            $c_insert_history_user->save();
        }
    }
    

}

?>
