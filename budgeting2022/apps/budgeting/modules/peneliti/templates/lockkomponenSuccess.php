<?php use_helper('Url', 'Javascript', 'Form', 'Object'); ?>
<div class="btn-group">
    <?php
    if ($nilai_gembok == 0) {
        echo link_to_function('<i class="fa fa-lock"></i>', '', array('class' => 'btn btn-success btn-flat btn-sm', 'disable' => true));
        echo link_to_function('Buka Komponen', 'execLockKomponen(1,"' . $id . '","' . $no . '","' . $unit_id . '","' . $kegiatan . '")', array('class' => 'btn btn-success btn-flat btn-sm'));
    } else {
        echo link_to_function('Kunci Komponen', 'execLockKomponen(0,"' . $id . '","' . $no . '","' . $unit_id . '","' . $kegiatan . '")', array('class' => 'btn btn-danger btn-flat btn-sm'));
    }
    ?>    
</div>
