<?php use_helper('I18N', 'Date', 'Url', 'Javascript', 'Form', 'Object', 'Number', 'Validation') ?>

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>Buat Lokasi</h1>
</section>

<!-- Main content -->
<section class="content">
    <?php include_partial('peneliti/list_messages') ?>
    <!-- Default box -->
    <div class="box box-primary box-solid">
        <div class="box-body">
            <div id="sf_admin_container">
                <div id="sf_admin_content" class="table-responsive">
                    <?php echo form_tag('peneliti/lokasiBaru') ?>        
                    <table cellspacing="0" class="sf_admin_list">
                        <thead>
                            <tr>
                                <th style="width: 10%"><b>Nama</b></th>
                                <th width="5px">&nbsp;</th>
                                <th><b>Isian</b></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="sf_admin_row_0" align='right'>
                                <td>Usulan SKPD</td>
                                <td align="center">:</td>
                                <td align="left">
                                    <?php
                                    $e = new Criteria();
                                    $nama = $sf_user->getNamaUser();
                                    $b = new Criteria;
                                    $b->add(UserHandleV2Peer::USER_ID, $nama);
                                    $es = UserHandleV2Peer::doSelect($b);
                                    $satuan_kerja = $es;
                                    $unit_kerja = Array();
                                    foreach ($satuan_kerja as $x) {
                                        $unit_kerja[] = $x->getUnitId();
                                    }
                                    $e->add(UnitKerjaPeer::UNIT_ID, $unit_kerja[0], criteria::ILIKE);
                                    for ($i = 1; $i < count($unit_kerja); $i++) {
                                        $e->addOr(UnitKerjaPeer::UNIT_ID, $unit_kerja[$i], criteria::ILIKE);
                                    }
                                    $e->addAscendingOrderByColumn(UnitKerjaPeer::UNIT_NAME);
                                    $v = UnitKerjaPeer::doSelect($e);
                                    echo select_tag('unit_id', objects_for_select($v, 'getUnitId', 'getUnitName', '', 'include_custom=---Pilih SKPD---'), array('class' => 'js-example-basic-single', 'style' => 'width:100%', 'required' => 'true'));
                                    ?>
                                </td>
                            </tr>
                            <tr class="sf_admin_row_0" align='right'>
                                <td><span style="color:red;">*</span> Nama Jalan</td>
                                <td align="center">:</td>
                                <td align="left">
                                    <div class="input-group input-group-sm">
                                        <span class="input-group-addon">JL.</span>
                                        <?php echo input_tag('lokasi_jalan', null, array('class' => 'form-control', 'placeholder' => 'Nama Jalan')) ?>
                                    </div>
                                </td>
                            </tr>
                            <tr class="sf_admin_row_1" align='right'>
                                <td>Gang/Blok/Kavling</td>
                                <td align="center">:</td>
                                <td align="left">
                                    <select class="js-example-basic-single" style="width:100%" name="tipe_gang" id="tipe_gang">
                                        <option value="GG">---</option>
                                        <option value="GG">GANG</option>
                                        <option value="BLOK">BLOK</option>
                                        <option value="KAV">KAVLING</option>
                                    </select>
                                </td>
                            </tr>
                            <tr class="sf_admin_row_0" align='right'>
                                <td>Nama Gang/Blok/Kavling</td>
                                <td align="center">:</td>
                                <td align="left"><?php echo input_tag('lokasi_gang', null, array('class' => 'form-control', 'placeholder' => 'Nama Gang/Blok/Kavling')) ?></td>
                            </tr>
                            <tr class="sf_admin_row_1" align='right'>
                                <td>Nomor Lokasi</td>
                                <td align="center">:</td>
                                <td align="left">
                                    <div class="input-group input-group-sm">
                                        <span class="input-group-addon">NO</span>
                                        <?php echo input_tag('lokasi_nomor', null, array('class' => 'form-control', 'placeholder' => 'Nomor Lokasi')) ?>
                                    </div>
                                </td>
                            </tr>
                            <tr class="sf_admin_row_0" align='right'>
                                <td>RW</td>
                                <td align="center">:</td>
                                <td align="left"><?php echo input_tag('lokasi_rw', null, array('class' => 'form-control', 'placeholder' => 'RW')) ?></td>
                            </tr>
                            <tr class="sf_admin_row_1" align='right'>
                                <td>RT</td>
                                <td align="center">:</td>
                                <td align="left"><?php echo input_tag('lokasi_rt', null, array('class' => 'form-control', 'placeholder' => 'RT')) ?></td>
                            </tr>
                            <tr class="sf_admin_row_0" align='right'>
                                <td><span style="color:red;">*</span> Nama Bangunan/Saluran</td>
                                <td align="center">:</td>
                                <td align="left"><?php echo input_tag('lokasi_tempat', null, array('class' => 'form-control', 'placeholder' => 'Nama Bangunan/Saluran')) ?></td>
                            </tr>
                            <tr class="sf_admin_row_1" align='right'>
                                <td>Keterangan Lokasi</td>
                                <td align="center">:</td>
                                <td align="left"><?php echo input_tag('lokasi_keterangan', null, array('class' => 'form-control', 'placeholder' => 'Keterangan Lokasi')) ?></td>
                            </tr>

                            <tr class="sf_admin_row_0" align='right'>
                                <td>Kecamatan</td>
                                <td align="center">:</td>
                                <td align="left">
                                    <?php
                                    echo select_tag('kecamatan', objects_for_select($nama_kecamatan_kel, 'getId', 'getNama', '', 'include_custom=---Pilih Kecamatan---'), array('class' => 'js-example-basic-single', 'style' => 'width:100%', 'required' => 'true'));
                                    ?>
                                </td>
                            </tr>				
                            <tr class="sf_admin_row_1" align='right'>
                                <td>Kelurahan</td>
                                <td align="center">:</td>
                                <td align="left">
                                    <div id="indicator" style="display:none;" align="center"><dt>&nbsp;</dt><dd><b>Mohon Tunggu </b><?php echo image_tag('loading.gif', array('align' => 'absmiddle')) ?></dd></div>
                                    <?php
                                    echo select_tag('kelurahan', options_for_select(array(), '', 'include_custom=---Pilih Kecamatan Dulu---'), Array('id' => 'kelurahan', 'class' => 'js-example-basic-single', 'style' => 'width:100%', 'required' => 'true'));
                                    ?>
                                </td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr class="sf_admin_row_0" align='center'>
                                <td colspan="3">
                                    <p id="preview" style="font-size: 12pt"></p>
                                </td>
                            </tr>
                            <tr class="sf_admin_row_0" align='left' valign="top">
                                <td colspan="3">
                                    <p><span style="color:red;">*</span> Nama Jalan atau Nama Bangunan harus terisi salah satu</p>

                                </td>
                            </tr>
                        </tfoot>
                    </table>
                    <?php echo link_to_function('Preview', 'previewLokasi()', array('class' => 'btn btn-xs btn-primary')); ?>
                    <?php echo submit_tag('Simpan', array('name' => 'simpan', 'class' => 'btn btn-lg btn-success')); ?>
                    <?php echo '</form>'; ?>
                </div>
            </div>
        </div>
    </div>

</section>
<script>
    $(document).ready(function () {
        $(".js-example-basic-single").select2();
        $(".js-example-basic-multiple").select2();
    });
    $("#kecamatan").change(function () {
        var id = $(this).val();
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/peneliti/pilihKelurahan/b/" + id + ".html",
            context: document.body}).done(function (msg) {
            $('#kelurahan').html(msg);
        });
    });

    function previewLokasi() {
        var lokasi_jalan = $('#lokasi_jalan').val();
        var tipe_gang = $('#tipe_gang').val();
        var lokasi_gang = $('#lokasi_gang').val();
        var lokasi_nomor = $('#lokasi_nomor').val();
        var lokasi_rw = $('#lokasi_rw').val();
        var lokasi_rt = $('#lokasi_rt').val();
        var lokasi_tempat = $('#lokasi_tempat').val();
        var lokasi_keterangan = $('#lokasi_keterangan').val();
        var kecamatan = $('#kecamatan').val();
        var kelurahan = $('#kelurahan').val();
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/peneliti/previewLokasi.html?lokasi_jalan=" + lokasi_jalan + "&tipe_gang=" + tipe_gang + "&lokasi_gang=" + lokasi_gang + "&lokasi_nomor=" + lokasi_nomor + "&lokasi_rw=" + lokasi_rw + "&lokasi_rt=" + lokasi_rt + "&lokasi_tempat=" + lokasi_tempat + "&lokasi_keterangan=" + lokasi_keterangan + "&kecamatan=" + kecamatan + "&kelurahan=" + kelurahan,
            context: document.body
        }).done(function (msg) {
            $('#preview').html(msg);
        });
    }
</script>