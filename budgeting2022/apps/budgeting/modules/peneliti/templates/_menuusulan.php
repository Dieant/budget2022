<?php use_helper('I18N', 'Date', 'Object', 'Javascript', 'Validation') ?>
<?php use_stylesheet('/sf/sf_admin/css/main') ?>
<p align="right">
    <?php
    if ($sf_user->getNamaUser() == 'atistya' || $sf_user->getNamaUser() == 'awang') {
        echo button_to('List Token Usulan', 'peneliti/usulantoken') . ' | ';
    }
    ?>
    <?php echo button_to('List Konfirmasi Usulan', 'peneliti/usulansshkonfirmasi') . ' | '; ?>
    <?php echo button_to('List Verifikasi Usulan', 'peneliti/usulansshverifikasi') . ' | '; ?>
    <?php echo button_to('List Semua Usulan', 'peneliti/usulansshlist') . ' | '; ?>
    <?php echo button_to('Tambah Usulan Baru', 'peneliti/usulansshbaru'); ?>
</p>