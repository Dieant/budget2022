<?php use_helper('I18N', 'Date', 'Url', 'Javascript', 'Form', 'Object', 'Number', 'Validation') ?>
<?php
$unit_id = $rs_rinciandetail->getUnitId();
$kegiatan_code = $rs_rinciandetail->getKegiatanCode();
$detail_no = $rs_rinciandetail->getDetailNo();
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>Daftar Isian Rincian Komponen Kegiatan SKPD</h1>
</section>

<!-- Main content -->
<section class="content">
    <?php include_partial('peneliti/list_messages'); ?>
    <?php
//    fungsi warning lengkap
    $lelang = 0;
    $totNilaiAlokasi = 0;
    $totNilaiKontrak = 0;
    $totNilaiRealisasi = 0;
    $totVolumeRealisasi = 0;
    $totNilaiSwakelola = 0;
    $totNilaiHps = 0;
    $ceklelangselesaitidakaturanpembayaran = 0;
    if (!($unit_id == '2600' && $kegiatan_code == '1.03.31.0002' && ($rs_rinciandetail->getKomponenId() == '23.02.04.04.98'))) {
        if (sfConfig::get('app_fasilitas_cekeProject') == 'buka') {
            $totNilaiAlokasi = $rs_rinciandetail->getCekNilaiAlokasiProject($rs_rinciandetail->getUnitId(), $rs_rinciandetail->getKegiatanCode(), $rs_rinciandetail->getDetailNo());
            if (sfConfig::get('app_fasilitas_cekServer') == 'buka') {
                $lelang = $rs_rinciandetail->getCekLelang($rs_rinciandetail->getUnitId(), $rs_rinciandetail->getKegiatanCode(), $rs_rinciandetail->getDetailNo(), $rs_rinciandetail->getNilaiAnggaran());
                if (sfConfig::get('app_fasilitas_cekeDelivery') == 'buka') {
                    $totNilaiSwakelola = $rs_rinciandetail->getCekNilaiSwakelolaDelivery2($rs_rinciandetail->getUnitId(), $rs_rinciandetail->getKegiatanCode(), $rs_rinciandetail->getDetailNo());
                    $totNilaiKontrak = $rs_rinciandetail->getCekNilaiKontrakDelivery2($rs_rinciandetail->getUnitId(), $rs_rinciandetail->getKegiatanCode(), $rs_rinciandetail->getDetailNo());
                    $totNilaiRealisasi = $rs_rinciandetail->getCekRealisasi($rs_rinciandetail->getUnitId(), $rs_rinciandetail->getKegiatanCode(), $rs_rinciandetail->getDetailNo());
                    $totVolumeRealisasi = $rs_rinciandetail->getCekVolumeRealisasi($rs_rinciandetail->getUnitId(), $rs_rinciandetail->getKegiatanCode(), $rs_rinciandetail->getDetailNo());
                    $ceklelangselesaitidakaturanpembayaran = $rs_rinciandetail->getCekLelangTidakAdaAturanPembayaran($rs_rinciandetail->getUnitId(), $rs_rinciandetail->getKegiatanCode(), $rs_rinciandetail->getDetailNo());
                    $totNilaiKontrakTidakAdaAturanPembayaran = $rs_rinciandetail->getCekNilaiDeliveryBelumAdaAturanPembayaran2($unit_id, $kode_kegiatan, $detail_no);
                }
            }
        }
    }

    if ($totNilaiAlokasi > 0 || $totNilaiSwakelola > 0 || $totNilaiKontrak > 0 || $totNilaiRealisasi > 0 || $totVolumeRealisasi > 0 || $lelang > 0 || $totNilaiHps > 0 || $ceklelangselesaitidakaturanpembayaran > 0 || $totNilaiKontrakTidakAdaAturanPembayaran == 1) {
        ?>
        <div class="alert alert-danger alert-dismissable">
            <h4><i class="icon fa fa-ban"></i> Alert!</h4>
            <?php
            if ($totNilaiAlokasi > 0) {
                echo '<p>Nilai yang dialokasikan di eProject sebesar = Rp ' . number_format($totNilaiAlokasi, 0, ',', '.') . '</p>';
            }
            if ($totNilaiSwakelola > 0 || $totNilaiKontrak > 0 || $totNilaiRealisasi > 0) {
                if ($totNilaiRealisasi > 0) {
                    echo '<p>Nilai yang dialokasikan di eDelivery sebesar = Rp ' . number_format($totNilaiRealisasi, 0, ',', '.') . '</p>';
                } else if ($totNilaiSwakelola == 0) {
                    echo '<p>Nilai yang dialokasikan di eDelivery sebesar = Rp ' . number_format($totNilaiKontrak, 0, ',', '.') . '</p>';
                } else if ($totNilaiKontrak == 0) {
                    echo '<p>Nilai yang dialokasikan di eDelivery sebesar = Rp ' . number_format($totNilaiSwakelola, 0, ',', '.') . '</p>';
                } else {
                    echo '<p>Nilai yang dialokasikan di eDelivery sebesar = Rp ' . number_format($totNilaiKontrak, 0, ',', '.') . '</p>';
                }
            }
            if ($lelang > 0) {
                echo '<p>Komponen ini dalam Proses Lelang</p>';
            }
            if ($totNilaiHps > 0) {
                echo '<p>Nilai HPS Komponen sebesar = Rp ' . number_format($totNilaiHps, 0, ',', '.') . '</p>';
            }
            if ($ceklelangselesaitidakaturanpembayaran == 1) {
                echo '<p>Proses Lelang  Selesai & Belum ada Aturan Pembayaran</p>';
            }
            if ($totNilaiKontrakTidakAdaAturanPembayaran == 1) {
                echo '<p>Komponen ini belum ada isian aturan pembayaran di eDelivery. Silahkan mengisi Aturan Pembayaran terlebih dahulu.</p>';
            }
            if ($totVolumeRealisasi > 0) {
                echo '<p>Nilai yang dialokasikan di eDelivery dengan volume ' . number_format($totVolumeRealisasi, 0, ',', '.') . '</p>';
            }
            ?>
        </div>
        <?php
    }
//    fungsi warning lengkap
    ?>
    <div class="box box-primary box-solid">
        <div class="box-header with-border">
            <h3 class="box-title">Form Edit Komponen</h3>
            <!-- <div class="box-tools pull-right">
                <?php echo link_to('Batalkan Perubahan', 'peneliti/batalRevisiKomponen?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code . '&detail_no=' . $detail_no, array('class' => 'btn btn-danger', 'confirm' => 'Anda yakin ingin mengembalikan komponen ke nilai sebelum revisi?')); ?>
            </div> -->
        </div>
        <div class="box-body">
            <div id="sf_admin_container" class="table-responsive">
                <?php echo form_tag('peneliti/editKegiatanRevisi') ?>
                <table cellspacing="0" class="sf_admin_list">
                    <thead>  
                        <tr>
                            <th style="width: 19%"><b>Nama</b></th>
                            <th style="width: 1%">&nbsp;</th>
                            <th style="width: 80%"><b>Isian</b></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="sf_admin_row_0" align='right'>
                            <td>Kelompok Belanja</td>
                            <td align="center">:</td>
                            <td align="left">
                                <?php
                                $rekening_code = $rs_rinciandetail->getRekeningCode();
                                $belanja_code = substr($rekening_code, 0, 5);
                                $c = new Criteria();
                                $c->add(KelompokBelanjaPeer::BELANJA_CODE, $belanja_code);
                                $rs_belanja = KelompokBelanjaPeer::doSelectOne($c);
                                if ($rs_belanja) {
                                    echo $rs_belanja->getBelanjaName();
                                }
                                ?></td>
                        </tr>
                        <tr class="sf_admin_row_1" align='right'>
                            <td>Kode Rekening</td>
                            <td align="center">:</td>
                            <td align="left">
                                <?php
                                $c = new Criteria();
                                $c->add(RekeningPeer::REKENING_CODE, $rekening_code);
                                $rs_rekening = RekeningPeer::doSelectOne($c);
                                if ($rs_rekening) {
                                    $pajak_rekening = $rs_rekening->getRekeningPpn();
                                    echo $rekening_code . ' ' . $rs_rekening->getRekeningName();
                                }
                                ?></td>
                        </tr>
                        <tr class="sf_admin_row_0" align='right'>
                            <td>Komponen</td>
                            <td align="center">:</td>
                            <td align="left"><?php echo $rs_rinciandetail->getKomponenName(); ?></td>
                            <div id="<?php echo 'tempat_ajax_' . $no ?>">
                    <?php
                    $kegiatan = $rs_rinciandetail->getKegiatanCode();
                    $array_skpd_lock = array('9999', '2600');                    
                    if (in_array($unit_id, $array_skpd_lock) && sfConfig::get('app_tahun_default') == '2016') {
                        if ($rs_rinciandetail->getStatusLelang() == 'unlock') {
                            echo "<div class='btn-group'>";
                            echo link_to_function('<i class="fa fa-money"></i>', '', array('class' => 'btn btn-success btn-flat btn-sm', 'disable' => true));
                            if ($sf_user->getNamaUser() != 'peninjau' && $sf_user->getNamaUser() != 'dppk' && $sf_user->getNamaUser() != 'bppk' && $sf_user->getNamaUser() != 'wawali' && $sf_user->getNamaUser() != 'walikota' && $sf_user->getNamaUser() != 'sekda' && $sf_user->getNamaUser() != 'asisten' && $sf_user->getNamaUser() != 'masger') {
                                echo link_to_function('Lock Harga Dasar', 'execLockHargaDasarRevisi("lock","' . $id . '","' . $no . '","' . $unit_id . '","' . $kegiatan . '")', array('class' => 'btn btn-success btn-flat btn-sm'));
                            }
                            echo "</div>";
                        } else {
                            echo "<div class='btn-group'>";
                            if ($sf_user->getNamaUser() != 'peninjau' && $sf_user->getNamaUser() != 'dppk' && $sf_user->getNamaUser() != 'bppk' && $sf_user->getNamaUser() != 'wawali' && $sf_user->getNamaUser() != 'walikota' && $sf_user->getNamaUser() != 'sekda' && $sf_user->getNamaUser() != 'asisten' && $sf_user->getNamaUser() != 'masger') {
                                echo link_to_function('Unlock Harga Dasar', 'execLockHargaDasarRevisi("unlock","' . $id . '","' . $no . '","' . $unit_id . '","' . $kegiatan . '")', array('class' => 'btn btn-success btn-flat btn-sm'));
                            }
                            echo "</div>";
                        }
                    }
                    ?>
           </div>
               
                        </tr>
                        <tr class="sf_admin_row_1" align='right'>
                            <td>Harga</td>
                            <td align="center">:</td>
                            <?php
                            $komponen_id = $rs_rinciandetail->getKomponenId();
                            $komponen_harga = $rs_rinciandetail->getKomponenHargaAwal();
                            if ($rs_rinciandetail->getTipe() == 'FISIK') {
                                $c = new Criteria();
                                $c->add(KomponenPeer::KOMPONEN_ID, $rs_rinciandetail->getKomponenId(), Criteria::ILIKE);
                                $rs_komponen = KomponenPeer::doSelectOne($c);
                                $komponen_harga = $rs_komponen->getKomponenHargaBulat();
                            }
                            if ($komponen_harga != floor($komponen_harga)) {
                                ?>
                                <td align='left' ><?php
                                    if ($rs_rinciandetail->getSatuan() == '%') {
                                        echo $komponen_harga;
                                    } else {
                                        echo number_format($komponen_harga, 3, ',', '.');
                                    }echo input_hidden_tag('harga', $komponen_harga);
                                    ?></td>
                                <?php
                            } else {
                                ?>
                                <td align='left' ><?php
                                    if ($rs_rinciandetail->getSatuan() == '%') {
                                        echo $komponen_harga;
                                    } else {
                                        echo number_format($komponen_harga, 0, ',', '.');
                                    }echo input_hidden_tag('harga', $komponen_harga);
                                    ?> </td>
                                <?php
                            }
                            ?>
                        </tr>
                        <tr class="sf_admin_row_0" align='right'>
                            <td>Satuan</td>
                            <td align="center">:</td>
                            <td align="left"><?php echo $rs_rinciandetail->getSatuan(); ?></td>
                        </tr>
                        <tr class="sf_admin_row_1" align='right'>
                            <td>Pajak</td>
                            <td align="center">:</td>
                            <td align="left" ><?php
                                echo $rs_rinciandetail->getPajak() . '%';
                                echo input_hidden_tag('pajakx', $rs_rinciandetail->getPajak());
                                ?></td>
                        </tr>
                        <?php if ($rs_rinciandetail->getTipe2() == 'KONSTRUKSI' ||$rs_rinciandetail->getTipe2() == 'TANAH'|| $rs_rinciandetail->getTipe2() == 'ATRIBUSI' || $rs_rinciandetail->getTipe2() == 'PERENCANAAN' || $rs_rinciandetail->getTipe2() == 'PENGAWASAN'): ?>
                            <tr class="sf_admin_row_0" align='right'>
                                <td><span style="color:red;">*</span> Kode Akrual</td>
                                <td align="center">:</td>
                                <td align="left">
                                    <?php
                                    $sub = "char_length(akrual_code)>10";
                                    $c = new Criteria();
                                    $c->add(AkrualPeer::AKRUAL_CODE, $sub, Criteria::CUSTOM);
                                    if ($rs_rinciandetail->getTipe2() == 'KONSTRUKSI'||$rs_rinciandetail->getTipe2() == 'TANAH') {
                                        $akrual_rek = '';
                                        $d = new Criteria();
                                        $d->add(RekeningPeer::REKENING_CODE, $rs_rinciandetail->getRekeningCode());
                                        if ($rs_rekening = RekeningPeer::doSelectOne($d)) {
                                            $akrual_rek = $rs_rekening->getAkrualKonstruksi();
                                        }
                                        $c->addAnd(AkrualPeer::AKRUAL_CODE, $akrual_rek . '%', Criteria::ILIKE);
                                    } elseif ($rs_rinciandetail->getTipe2() == 'ATRIBUSI' || $rs_rinciandetail->getTipe2() == 'PERENCANAAN' || $rs_rinciandetail->getTipe2() == 'PENGAWASAN') {
                                        $c->addAnd(AkrualPeer::AKRUAL_CODE, '1.3.1%', Criteria::ILIKE);
                                        $c->addOr(AkrualPeer::AKRUAL_CODE, '1.3.3%', Criteria::ILIKE);
                                        $c->addOr(AkrualPeer::AKRUAL_CODE, '1.3.4%', Criteria::ILIKE);
                                        $c->addOr(AkrualPeer::AKRUAL_CODE, '1.3.5%', Criteria::ILIKE);
                                        $c->addOr(AkrualPeer::AKRUAL_CODE, '1.5.3%', Criteria::ILIKE);
                                    }
                                    $c->addAscendingOrderByColumn(AkrualPeer::AKRUAL_CODE);
                                    $rs_akrualcode = AkrualPeer::doSelect($c);
                                    $this->rs_akrualcode = $rs_akrualcode;

                                    $akrual_code = null;
                                    if ($rs_rinciandetail->getAkrualCode()) {
                                        $kode = explode('|', $rs_rinciandetail->getAkrualCode());
                                        $akrual_code = $kode[0];
                                    }

                                    echo select_tag('akrual_code', objects_for_select($rs_akrualcode, 'getAkrualCode', 'getAkrualKodeNama', $akrual_code, 'include_custom=---Pilih Kode Akrual---'), array('class' => 'js-example-basic-single', 'style' => 'width:100%'));
                                    ?>
                                </td>
                            </tr>
                        <?php elseif ($rs_rinciandetail->getTipe2() == 'NONKONSTRUKSI'): ?>
                            <tr class="sf_admin_row_0" align='right'>
                                <td>Kode Akrual</td>
                                <td align="center">:</td>
                                <td align="left">
                                    <?php
                                    if ($rs_rinciandetail->getAkrualCode()) {
                                        echo KomponenPeer::buatKodeNamaAkrual($rs_rinciandetail->getAkrualCode());
                                        echo input_hidden_tag('akrual_code', $rs_rinciandetail->getAkrualCode());
                                    } else {
                                        $c = new Criteria();
                                        $c->add(KomponenPeer::KOMPONEN_ID, $rs_rinciandetail->getKomponenId(), Criteria::ILIKE);
                                        if ($rs_komponen = KomponenPeer::doSelectOne($c)) {
                                            echo KomponenPeer::buatKodeNamaAkrual($rs_komponen->getAkrualCode());
                                            echo input_hidden_tag('akrual_code', $rs_komponen->getAkrualCode());
                                        } else {
                                            echo input_hidden_tag('akrual_code', null);
                                        }
                                        //echo input_hidden_tag('akrual_code', '');
                                    }
                                    ?>
                                </td>
                            </tr>
                        <?php endif; ?>
    <!--<tr class="sf_admin_row_0" align='right'>
        <td>Kode Akrual</td>
        <td align="center">:</td>
        <td align="left"><?php
                        $kode = explode('|', $rs_rinciandetail->getAkrualCode());
                        $c = new Criteria();
                        $c->add(AkrualPeer::AKRUAL_CODE, $kode[0]);
                        if ($rs_nama_akrual = AkrualPeer::doSelectOne($c)) {
                            echo str_replace('|', '.', $rs_rinciandetail->getAkrualCode()) . ' - ' . $rs_nama_akrual->getNama();
                            echo input_hidden_tag('akrual_code', $rs_rinciandetail->getAkrualCode());
                        }
                        ?></td>
    </tr>-->
                        <tr class="sf_admin_row_1" align='right'>
                            <td><span style="color:red;">*</span> Subtitle</td>
                            <td align="center">:</td>
                            <td align="left">
                                <?php
                                $nama_subtitle = $rs_rinciandetail->getSubtitle();
                                $spasi_nama_subtitle = trim($nama_subtitle);
                                $query = "select subtitle,sub_id from " . sfConfig::get('app_default_schema') . ".dinas_subtitle_indikator "
                                        . "where unit_id='" . $sf_params->get('unit') . "' and kegiatan_code='" . $sf_params->get('kegiatan') . "' and subtitle = '$spasi_nama_subtitle'";
                                //print_r($query);
                                $con = Propel::getConnection();
                                $stmt = $con->prepareStatement($query);
                                $rs1 = $stmt->executeQuery();
                                while ($rs1->next()) {
                                    $kode_sub = $rs1->getString('sub_id');
                                }
//                        echo select_tag('subtitle', objects_for_select($rs_subtitleindikator, 'getSubId', 'getSubtitle', $kode_sub, 'include_custom=---Pilih Subtitle---'), Array('onChange' => remote_function(Array('update' => 'sub1', 'url' => 'peneliti/pilihsubx?kegiatan_code=' . $sf_params->get('kegiatan') . '&unit_id=' . $sf_params->get('unit'), 'with' => "'b='+this.options[this.selectedIndex].value", 'loading' => "Element.show('indicator');Element.hide('sub1');", 'complete' => "Element.hide('indicator');Element.show('sub1');"))));

                                echo select_tag('subtitle', objects_for_select($rs_subtitleindikator, 'getSubId', 'getSubtitle', $kode_sub, 'include_custom=---Pilih Subtitle---'), array('class' => 'js-example-basic-single', 'style' => 'width:100%'));
                                ?>
                            </td>
                        </tr>
                        <tr class="sf_admin_row_0" align='right'>
                            <td>Sub - Subtitle</td>
                            <td align="center">:</td>
                            <td align="left">
                                <div id="indicator" style="display:none;" align="center"><dt>&nbsp;</dt><dd><b>Mohon Tunggu </b><?php echo image_tag('loading.gif', array('align' => 'absmiddle')) ?></dd></div>
                                <?php
                                if ($rs_rinciandetail->getKodeSub()) {
                                    echo select_tag('sub', options_for_select(array($rs_rinciandetail->getKodeSub() => $rs_rinciandetail->getSub()), $rs_rinciandetail->getKodeSub(), 'include_custom=---Pilih Subtitle Dulu---'), Array('id' => 'sub1', 'class' => 'js-example-basic-single', 'style' => 'width:100%'));
                                } elseif (!$rs_rinciandetail->getKodeSub()) {
                                    echo select_tag('sub', options_for_select(array(), '', 'include_custom=---Pilih Subtitle Dulu---'), Array('id' => 'sub1', 'class' => 'js-example-basic-single', 'style' => 'width:100%'));
                                }
                                ?>
                            </td>
                        </tr>           
                        <?php
                        $tipe = $rs_rinciandetail->getTipe();
                        $tipe2 = $rs_rinciandetail->getTipe2();
                        //irul 2may 2014 awal-comment  untuk menampilka opsi lokasi pada estimasi pembuatan separator jalan
                        $estimasi_opsi_lokasi = array('');
                        //irul 2may 2014 awal-comment  untuk menampilka opsi lokasi pada estimasi pembuatan separator jalan
                        if (($tipe2 == 'KONSTRUKSI' || $tipe2 == 'TANAH' || $tipe == 'FISIK' || $est_fisik) || ($sf_params->get('lokasi')) || in_array($rs_rinciandetail->getKomponenId(), $estimasi_opsi_lokasi)) {
                            //ditutup sementara spy dinas tidak bisa edit lokasi
                            ?>
                            <tr class="sf_admin_row_0" align='right' valign="top">
                                <td>Lokasi</td>
                                <td align="center">:</td>
                                <td align="left">
                                    <div class="row col-xs-12 text-bold text-center">
                                        <div class="row col-xs-2 text-center">Lokasi Sekarang</div>
                                        <div class="row col-xs-10">
                                            <?php echo input_tag('lokasi', $rs_rinciandetail->getDetailName(), array('class' => 'form-control', 'style' => 'width:100%')); //submit_tag('cari', 'name=cari').'<br><font color="magenta">[untuk multi lokasi, tambahkan lokasi dengan klik "cari" lagi]<br></font>';  ?>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="row col-xs-12 text-bold text-center">
                                        <div class="row col-xs-12">
                                            <hr/>
                                            Silahkan pilih dari data yang sudah ada
                                            <hr/>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="row col-xs-12 text-bold text-center">
                                        <div class="row col-xs-12">
                                            <?php echo select_tag('lokasi_lama', objects_for_select($rs_jalan, 'getLokasi', 'getLokasi', '', 'include_custom=---Pilih Jalan---'), array('class' => 'js-example-basic-multiple', 'style' => 'width:100%', 'multiple' => 'multiple')); ?>                                                
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="row col-xs-12 text-bold text-center">
                                        <div class="row col-xs-12">
                                            <hr/>
                                            Silahkan menambah data lokasi (apabila tidak ada dipilihan atas)
                                            <hr/>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="row col-xs-12">
                                        <?php
                                        foreach ($rs_geojson as $value_geojson) {
                                            if ($value_geojson->getGang() <> '') {
                                                $array_gang = explode('.', $value_geojson->getGang());
                                                $tipe_gang = $array_gang[0];
                                                $nama_gang = trim($array_gang[1]);
                                            }
                                            ?>
                                            <div class="div-induk-lokasi row">
                                                <div>
                                                    <div class="form-group form-group-options col-xs-5 col-sm-5 col-md-5">
                                                        <div class="input-group input-group-option col-xs-12">
                                                            <font class="text-bold">Nama Jalan</font><br/>
                                                            <div class="input-group input-group-sm">
                                                                <span class="input-group-addon">JL.</span>
                                                                <input type="text" name="lokasi_jalan[]" class="form-control lokasi_jalan" placeholder="Nama Jalan (*wajib diisi apabila lokasi berupa Jalan)" value="<?php echo $value_geojson->getJalan() ?>" >
                                                            </div>
                                                            <ul class="jalan_list_id"></ul>                                                        
                                                        </div>
                                                    </div>
                                                    <div class="form-group form-group-options col-xs-1 col-sm-1 col-md-1">
                                                        <div class="input-group input-group-option col-xs-12">
                                                            <font class="text-bold">Gang/Blok/Kavling</font><br/>
                                                            <select class="form-control" name="tipe_gang[]">                                                                
                                                                <option selected value="<?php echo $tipe_gang ?>"><?php echo $tipe_gang ?></option>
                                                                <option value="GG">---</option>
                                                                <option value="GG">GANG</option>
                                                                <option value="BLOK">BLOK</option>
                                                                <option value="KAV">KAVLING</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group form-group-options col-xs-2 col-sm-2 col-md-2">
                                                        <div class="input-group input-group-option col-xs-12">
                                                            <font class="text-bold">Nama Gang/Blok/Kavling</font><br/>
                                                            <input type="text" name="lokasi_gang[]" class="form-control" placeholder="Nama Gang" value="<?php echo $nama_gang ?>">
                                                        </div>
                                                    </div>
                                                    <div class="form-group form-group-options col-xs-2 col-sm-2 col-md-2">
                                                        <div class="input-group input-group-option col-xs-12">
                                                            <font class="text-bold">Nomor Lokasi</font><br/>
                                                            <div class="input-group input-group-sm">
                                                                <span class="input-group-addon">NO.</span>
                                                                <input type="text" name="lokasi_nomor[]" class="form-control" placeholder="Nomor" value="<?php echo $value_geojson->getNomor() ?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group form-group-options col-xs-1 col-sm-1 col-md-1">
                                                        <div class="input-group input-group-option col-xs-12">
                                                            <font class="text-bold">RW</font><br/>
                                                            <input type="text" name="lokasi_rw[]" class="form-control" placeholder="RW" value="<?php echo $value_geojson->getRw() ?>">
                                                        </div>
                                                    </div>
                                                    <div class="form-group form-group-options col-xs-1 col-sm-1 col-md-1">
                                                        <div class="input-group input-group-option col-xs-12">
                                                            <font class="text-bold">RT</font><br/>
                                                            <input type="text" name="lokasi_rt[]" class="form-control" placeholder="RT" value="<?php echo $value_geojson->getRt() ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div>
                                                    <div class="form-group form-group-options col-xs-6 col-sm-6 col-md-6">
                                                        <div class="input-group input-group-option col-xs-12">
                                                            <font class="text-bold">Nama Bangunan/Saluran</font><br/>
                                                            <input type="text" name="lokasi_tempat[]" class="form-control" placeholder="Nama Bangunan/Saluran (*wajib diisi apabila lokasi berupa bangunan/saluran/tempat)" value="<?php echo $value_geojson->getTempat() ?>">                                                                
                                                        </div>
                                                    </div>
                                                    <div class="form-group form-group-options col-xs-6 col-sm-6 col-md-6">
                                                        <div class="input-group input-group-option col-xs-12">
                                                            <font class="text-bold">Keterangan Lokasi</font><br/>
                                                            <input type="text" name="lokasi_keterangan[]" class="form-control" placeholder="Keterangan Lokasi" value="<?php echo $value_geojson->getKeterangan() ?>">
                                                            <span class="input-group-addon input-group-addon-add">
                                                                <span class="glyphicon glyphicon-plus"></span>
                                                            </span>
                                                            <span class="input-group-addon input-group-addon-remove">
                                                                <span class="glyphicon glyphicon-remove"></span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>                                                
                                        <?php }
                                        ?>
                                    </div>
                                    <!--<div class="row col-xs-12 table-responsive">
                                        <table class="table table-condensed table-bordered">
                                            <thead>
                                                <tr>
                                                    <td class="text-bold text-center">Kolom</td>
                                                    <td class="text-bold text-center">DiIsi Dengan</td>
                                                    <td class="text-bold text-center">Larangan</td>
                                                    <td class="text-bold text-center">Otomatis Diisi</td>
                                                    <td class="text-bold text-center">Harus Diisi?</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>"Nama Jalan"</td>
                                                    <td>Hanya nama jalan</td>
                                                    <td>Isi tanpa kata "JL."</td>
                                                    <td>Otomatis ditambahkan "JL."</td>
                                                    <td>Iya, tapi dapat digantikan dengan pengisian "Nama Tempat"</td>
                                                </tr>
                                                <tr>
                                                    <td>Dropdown Gang/Blok/Kavling</td>
                                                    <td>"Gang", "BLOK", atau "Kavling"</td>
                                                    <td>-</td>
                                                    <td>"---" akan default terpilih "Gang"</td>
                                                    <td>Tidak</td>
                                                </tr>
                                                <tr>
                                                    <td>"Nama Gang/Blok/Kavling"</td>
                                                    <td>data nama gang atau nama blok atau nama kavling</td>
                                                    <td>Isi tanpa kata "GG.", "BLOK.", atau "KAV."</td>
                                                    <td>Otomatis ditambahkan "Gang", "BLOK", atau "Kavling"</td>
                                                    <td>Tidak</td>
                                                </tr>
                                                <tr>
                                                    <td>"Nomor"</td>
                                                    <td>data nomor lokasi</td>
                                                    <td>Isi tanpa kata "Nomor."</td>
                                                    <td>Otomatis ditambahkan "NO."</td>
                                                    <td>Tidak</td>
                                                </tr>
                                                <tr>
                                                    <td>"RW"</td>
                                                    <td>data RW</td>
                                                    <td>Isi tanpa kata "RW."</td>
                                                    <td>Otomatis ditambahkan "RW."</td>
                                                    <td>Tidak</td>
                                                </tr>
                                                <tr>
                                                    <td>"RT"</td>
                                                    <td>data RT</td>
                                                    <td>Isi tanpa kata "RT."</td>
                                                    <td>Otomatis ditambahkan "RT."</td>
                                                    <td>Tidak</td>
                                                </tr>
                                                <tr>
                                                    <td>"Keterangan Lokasi"</td>
                                                    <td>data keterangan pemerjelas lokasi pekerjaan (seperti sebelah barat)</td>
                                                    <td>-</td>
                                                    <td>-</td>
                                                    <td>Tidak</td>
                                                </tr>
                                                <tr>
                                                    <td>"Nama Tempat"</td>
                                                    <td>data nama bangunan atau tempat</td>
                                                    <td>-</td>
                                                    <td>-</td>
                                                    <td>Iya, tapi dapat digantikan dengan pengisian "Nama Jalan"</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>-->
                                    <?php
                                    echo '<br> Usulan dari : ';
                                    echo select_tag('jasmas', objects_for_select($rs_jasmas, 'getKodeJasmas', 'getNama', $rs_rinciandetail->getKecamatan(), 'include_custom=--Pilih--'), array('class' => 'js-example-basic-single', 'style' => 'width:100%'));
                                    ?>                                    
                                </td>
                            </tr>
                            <tr class="sf_admin_row_0" align='right'>
                                <td><span style="color:red;">*</span> Kecamatan</td>
                                <td align="center">:</td>
                                <td align="left">
                                    <?php
                                    $kec = new Criteria();
                                    $kec->addAscendingOrderByColumn(KecamatanPeer::NAMA);
                                    $rs_kec = KecamatanPeer::doSelect($kec);
                                    $nama_kecamatan_kel = $rs_kec;
                                    if ($rs_rinciandetail->getLokasiKecamatan()) {
                                        $kec_ini = new Criteria();
                                        $kec_ini->add(KecamatanPeer::NAMA, $rs_rinciandetail->getLokasiKecamatan());
                                        $rs_kec_ini = KecamatanPeer::doSelectOne($kec_ini);
                                        echo select_tag('kecamatan', objects_for_select($nama_kecamatan_kel, 'getId', 'getNama', $rs_kec_ini->getId(), 'include_custom=---Pilih Kecamatan---'), array('class' => 'js-example-basic-single', 'style' => 'width:100%'));
                                    } else {
                                        echo select_tag('kecamatan', objects_for_select($nama_kecamatan_kel, 'getId', 'getNama', '', 'include_custom=---Pilih Kecamatan---'), array('class' => 'js-example-basic-single', 'style' => 'width:100%'));
                                    }
                                    ?>
                                    <span style="color:red;">* untuk Komponen FISIK, diharuskan untuk mengisi data Kecamatan.</span>
                                </td>
                            </tr>				
                            <tr class="sf_admin_row_1" align='right'>
                                <td>Kelurahan</td>
                                <td align="center">:</td>
                                <td align="left">
                                    <div id="indicator" style="display:none;" align="center"><dt>&nbsp;</dt><dd><b>Mohon Tunggu </b><?php echo image_tag('loading.gif', array('align' => 'absmiddle')) ?></dd></div>
                                    <?php
                                    if ($rs_rinciandetail->getLokasiKelurahan() && $rs_rinciandetail->getLokasiKecamatan()) {

                                        $kec_kel_ini = new Criteria();
                                        $kec_kel_ini->add(KelurahanKecamatanPeer::NAMA_KELURAHAN, $rs_rinciandetail->getLokasiKelurahan());
                                        $kec_kel_ini->add(KelurahanKecamatanPeer::NAMA_KECAMATAN, $rs_rinciandetail->getLokasiKecamatan());
                                        $rs_kec_kel_ini = KelurahanKecamatanPeer::doSelectOne($kec_kel_ini);

//                                        echo select_tag('kelurahan', options_for_select(array($rs_kec_kel_ini->getOid() => $rs_rinciandetail->getLokasiKelurahan()), $rs_kec_kel_ini->getOid(), 'include_custom=---Pilih Kecamatan Dulu---'), Array('id' => 'kelurahan1', 'class' => 'js-example-basic-single', 'style' => 'width:100%'));                                        

                                        $kel_ini = new Criteria();
                                        $kel_ini->add(KelurahanKecamatanPeer::NAMA_KECAMATAN, $rs_rinciandetail->getLokasiKecamatan());
                                        $nama_kel_ini = KelurahanKecamatanPeer::doSelect($kel_ini);
                                        $options = array();
                                        foreach ($nama_kel_ini as $kel) {
                                            $options[$kel->getOid()] = $kel->getNamaKelurahan();
                                        }
                                        echo select_tag('kelurahan', options_for_select($options, $rs_kec_kel_ini->getOid(), 'include_custom=---Pilih Kecamatan Dulu---'), Array('id' => 'kelurahan1', 'class' => 'js-example-basic-single', 'style' => 'width:100%'));
                                    } else {
                                        echo select_tag('kelurahan', options_for_select(array(), '', 'include_custom=---Pilih Kecamatan Dulu---'), Array('id' => 'kelurahan1', 'class' => 'js-example-basic-single', 'style' => 'width:100%'));
                                    }
                                    ?>
                                    <span style="color:red;">* untuk Komponen FISIK, diharuskan untuk mengisi data Kelurahan.</span>
                                </td>
                            </tr>
                            <?php
                        } else {
                            ?>
                            <tr class="sf_admin_row_0" align='right' valign="top">
                                <td>Keterangan</td>
                                <td align="center">:</td>
                                <td align='left'>
                                    <?php echo textarea_tag('keterangan', $rs_rinciandetail->getDetailName(), 'size=140x3') ?> </td>
                            </tr>
                            <?php
                        }
                        ?>
                        <tr class="sf_admin_row_1" align='right' valign="top">
                            <td><span style="color:red;">*</span>Volume
                            </td>
                            <td align="center">:</td>
                            <td align="left">                        
                                <b>Gunakan tanda titik (.) untuk menyatakan bilangan koma</b><br/><br/>
                                <?php
                                $keterangan_koefisien = $rs_rinciandetail->getKeteranganKoefisien();
                                $pisah_kali = explode('X', $keterangan_koefisien);



                                for ($i = 0; $i < 4; $i++) {
                                    $satuan = '';
                                    $volume = '';
                                    $nama_input = 'vol' . ($i + 1);
                                    $nama_pilih = 'volume' . ($i + 1);
                                    ;
                                    if (!empty($pisah_kali[$i])) {
                                        $pisah_spasi = explode(' ', $pisah_kali[$i]);
                                        $j = 0;

                                        for ($s = 0; $s < count($pisah_spasi); $s++) {
                                            if ($pisah_spasi[$s] != NULL) {
                                                if ($j == 0) {
                                                    $volume = $pisah_spasi[$s];
                                                    $j++;
                                                } elseif ($j == 1) {
                                                    $satuan = $pisah_spasi[$s];
                                                    $j++;
                                                } else {
                                                    $satuan.=' ' . $pisah_spasi[$s];
                                                }
                                            }
                                        }
                                    }
                                    if ($i !== 3) {
                                        echo input_tag($nama_input, $volume, array('onChange' => 'hitungTotal()')) . ' ' . select_tag($nama_pilih, objects_for_select($rs_satuan, 'getSatuanName', 'getSatuanName', $satuan, 'include_custom=---Pilih Satuan--'), array('class' => 'js-example-basic-single', 'style' => 'width:20%')) . '<br />  X <br />';
                                    } else {
                                        echo input_tag($nama_input, $volume, array('onChange' => 'hitungTotal()')) . ' ' . select_tag($nama_pilih, objects_for_select($rs_satuan, 'getSatuanName', 'getSatuanName', $satuan, 'include_custom=---Pilih Satuan--'), array('class' => 'js-example-basic-single', 'style' => 'width:20%'));
                                    }
                                }
                                ?>
                                <br/>
                            </td>
                        </tr>
                        <tr class="sf_admin_row_1" align='right' valign="top">
                            <td>Total</td>
                            <td align="center">:</td>
                            <td align="left"><?php echo input_tag('total', $rs_rinciandetail->getNilaiAnggaran(), array('readonly' => 'true')) ?></td>
                        </tr>
                        <?php if (sfConfig::get('app_fasilitas_bukaCatatanPergeseran') == 'buka') { ?>
                            <!-- irul 18maret 2014 - simpan catatan -->
                            <tr class="sf_admin_row_0" align='right' valign="top">
                                <td><span style="color:red;">*</span> Catatan Pergeseran Anggaran<br/>(Catatan Peneliti)</td>
                                <td align="center">:</td>
                                <td align='left'>
                                    <?php echo textarea_tag('catatan', $rs_rinciandetail->getNotePeneliti(), array('style' => 'width:550px', 'size' => '100px')) ?><br/>
                                    <i style="color: red">Minimal 15 Karakter</i>
                                </td>
                            </tr>
                            <!-- irul 18maret 2014 - simpan catatan -->
                            <?php
                        } else {
                            $c = new Criteria();
                            $c->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                            $c->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kegiatan_code);
                            $rs_kegiatan = DinasMasterKegiatanPeer::doSelectOne($c);
                            if ($rs_kegiatan->getTahap() == 'murni') {
                                ?>
                                <tr class="sf_admin_row_0" align='right' valign="top">
                                    <td><span style="color:red;">*</span> catatan usulan Anggaran<br/>(Catatan Peneliti)</td>
                                    <td align="center">:</td>
                                    <td align='left'>
                                        <?php echo textarea_tag('catatan', $rs_rinciandetail->getNotePeneliti(), array('style' => 'width:550px', 'size' => '100px')) ?><br/>
                                        <i style="color: red">Mohon diisi alasan usulan anggaran (Minimal 15 Karakter)</i>
                                    </td>
                                </tr>
                                <?php
                            }
                        }
                        ?>
                        <?php
                        $unit_id = $rs_rinciandetail->getUnitId();
                        if ($unit_id == '1800' || $unit_id == '0300') {
                            ?>
                            <tr class="sf_admin_row_1" align='right' valign="top">
                                <td>Komponen BLUD</td>
                                <td align="center">:</td>
                                <td align="left">
                                    <?php
                                    $nilai_blud = $rs_rinciandetail->getIsBlud();
                                    if ($nilai_blud == true) {
                                        echo checkbox_tag('blud', 1, TRUE);
                                    } else {
                                        echo checkbox_tag('blud');
                                    }
                                    ?>
                                    <font style="color: green"> *Centang jika termasuk komponen BLUD</font> </td>
                            </tr>    
                        <?php }
                        ?>
                        <tr class="sf_admin_row_0" align='right' valign="top">
                            <td>Komponen Musrenbang</td>
                            <td align="center">:</td>
                            <td align="left">
                                <?php
                                if ($rs_rinciandetail->getIsMusrenbang() == true) {
                                    echo checkbox_tag('musrenbang', 1, TRUE);
                                } else {
                                    echo checkbox_tag('musrenbang');
                                }
                                ?>
                                <font style="color: green"> *Centang jika termasuk komponen Musrenbang</font> 
                            </td>
                        </tr>    
                        <tr class="sf_admin_row_0" align='right' valign="top">
                            <td>Komponen Hibah KUA-PPAS</td>
                            <td align="center">:</td>
                            <td align="left">
                                <?php
                                if ($rs_rinciandetail->getIsHibah() == true) {
                                    echo checkbox_tag('hibah', 1, TRUE);
                                } else {
                                    echo checkbox_tag('hibah');
                                }
                                ?>
                                <font style="color: green"> *Centang jika termasuk komponen  Hibah KUA-PPAS</font> 
                            </td>
                        </tr>    
                    </tbody>
                    <tfoot>
                        <tr class="sf_admin_row_0" align='right' valign="top">
                            <td>&nbsp; </td>
                            <td>
                                <?php
                                echo input_hidden_tag('kegiatan', $sf_params->get('kegiatan'));
                                echo input_hidden_tag('unit', $sf_params->get('unit'));
                                echo input_hidden_tag('id', $sf_params->get('id'));
                                echo input_hidden_tag('referer', $sf_request->getAttribute('referer'));
                                ?>
                            </td>
                            <td>
                                <?php
                                if ($lelang == 1 || $ceklelangselesaitidakaturanpembayaran == 1) {
                                    echo button_to('kembali', '#', array('onClick' => "javascript:history.back()"));
                                } else if ($lelang == 0 && $ceklelangselesaitidakaturanpembayaran == 0) {
                                    echo submit_tag('simpan', 'name=simpan') . ' ' . button_to('kembali', '#', array('onClick' => "javascript:history.back()"));
                                }
                                ?>
                            </td>
                        </tr>
                    </tfoot>
                </table>
                <?php echo '</form>'; ?>
            </div>
        </div>
    </div>
</section>
<style>
    .select2-container-multi .select2-choices .select2-search-choice {
        padding: 3px 5px 3px 18px;
        margin: 3px 0 3px 5px;
        line-height: 20px;
    }
</style>
<script>
    $(document).ready(function () {
        $(".js-example-basic-single").select2();
        $(".js-example-basic-multiple").select2();
    });

    $(function () {
        $(document).on('click', 'div.form-group-options .input-group-addon-add', function () {
            var divIluminati = $(this).parents('.div-induk-lokasi');
            var sDivIluminatiHtml = divIluminati.html();
            var sInputGroupClasses = divIluminati.attr('class');
            //Gambiarra pra nao ficar criando mil inputs
            if (divIluminati.next().length >= 1)
                return;
            divIluminati.parent().append('<div class="' + sInputGroupClasses + '">' + sDivIluminatiHtml + '</div>');
        });
        $(document).on('click', 'div.form-group-options .input-group-addon-remove', function () {
            var divIluminati = $(this).parents('.div-induk-lokasi');
            divIluminati.remove();
        });
    });

    $("#subtitle").change(function () {
        var id = $(this).val();
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/peneliti/pilihsubxrevisi/kegiatan_code/<?php echo $sf_params->get('kegiatan') ?>/unit_id/<?php echo $sf_params->get('unit') ?>/b/" + id + ".html",
            context: document.body
        }).done(function (msg) {
            $('#sub1').html(msg);
        });

    });

    $("#kecamatan").change(function () {
        var id = $(this).val();
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/peneliti/pilihKelurahan/b/" + id + ".html",
            context: document.body
        }).done(function (msg) {
            $('#kelurahan1').html(msg);
        });

    });

    function hitungTotal() {
        var harga = $('#harga').val();
        var pajakx = $('#pajakx').val();
        var vol1 = $('#vol1').val();
        var vol2 = $('#vol2').val();
        var vol3 = $('#vol3').val();
        var vol4 = $('#vol4').val();
        var volume;
        var hitung;


        if (vol1 !== '' || vol2 !== '' || vol3 !== '' || vol4 !== '') {
            if (vol2 === '') {
                vol2 = 1;
                volume = vol1 * vol2;
            } else if (vol2 !== '') {
                volume = vol1 * vol2;
            }
            if (vol3 === '') {
                vol3 = 1;
                volume = volume * vol3;
            } else if (vol3 !== '') {
                volume = vol1 * vol2 * vol3;
            }
            if (vol4 === '') {
                vol4 = 1;
                volume = volume * vol4;
            } else if (vol4 !== '') {
                volume = vol1 * vol2 * vol3 * vol4;
            }
        }

        if (pajakx == 10) {
            hitung = harga * volume * 1.1;
        } else if (pajakx == 0) {
            hitung = harga * volume * 1;
        }

        $('#total').val(hitung);

    }
</script>
