<?php use_helper('Url', 'Javascript', 'Form', 'Object'); ?>
<?php
$i = 0;
?>
<tr class="mirip2" style="text-align: center; font-weight: bold">
    <th>SKPD</th>
    <th>Kegiatan</th>
    <th>Subtitle</th>
    <!--<th>Sub Subtitle</th>-->
    <th>Keterangan</th>
    <th>Koefisien</th>
</tr>
<?php
foreach ($list as $value) {
    ?>
    <tr class="mirip2" style="text-align: center">
        <td><?php echo $value['unit_name'] ?></td>
        <td><?php echo $value['kode_kegiatan'] ?></td>
        <td><?php echo $value['subtitle'] ?></td>
        <!--<td><?php echo $value['sub'] ?></td>-->
        <td><?php echo $value['detail_name'] ?></td>
        <td><?php echo $value['keterangan_koefisien'] ?></td>
    </tr>
    <?php
}
?>
<tr class="mirip2">
    <td colspan="13">&nbsp;</td>
</tr>