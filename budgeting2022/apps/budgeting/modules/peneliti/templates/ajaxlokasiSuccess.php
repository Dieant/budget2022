<?php use_helper('I18N', 'Date') ?>
<?php use_stylesheet('/sf/sf_admin/css/main') ?>
<?php echo use_helper('Object', 'Javascript', 'Validation') ?>


<?php

$jalan_keyword = '%' . $jalan . '%';
$index = $index;
$options = array();
$x = new Criteria();
$x->add(HistoryPekerjaanV2Peer::JALAN, $jalan_keyword, Criteria::ILIKE);
$x->addAscendingOrderByColumn(HistoryPekerjaanV2Peer::JALAN);
$rs_jalan = HistoryPekerjaanV2Peer::doSelect($x);

foreach ($rs_jalan as $jal) {
    $options[$jal->getJalan()] = $jal->getJalan();
    echo '<li onclick="set_item(\'' . $jal->getJalan() . '\')">' . $jal->getJalan() . '</li>';
}
?>
