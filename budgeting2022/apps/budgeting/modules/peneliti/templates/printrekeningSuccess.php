<?php use_helper('Javascript', 'Number') ?>
<?php
//use_stylesheet('/css/style.css');
//use_stylesheet('/sf/sf_admin/css/main');
//echo javascript_tag("window.print();");
?>
<div style="font-family: Verdana">
    <?php
    $kode_kegiatan = $sf_params->get('kode_kegiatan');
    $unit_id = $sf_params->get('unit_id');
    $query = "select mk.kode_kegiatan, mk.nama_kegiatan, mk.alokasi_dana, mk.kode_program, 
	mk.kode_program2, mk.benefit, mk.impact, mk.kode_program2, mk.kode_misi, mk.kode_tujuan, 
	mk.outcome, mk.target_outcome, mk.catatan, mk.ranking,uk.unit_name, mk.kode_urusan, 
	uk.kode_permen, mk.lokasi, mk.masukan, mk.output
	from " . sfConfig::get('app_default_schema') . ".murni_master_kegiatan mk,unit_kerja uk
	where mk.kode_kegiatan = '" . $kode_kegiatan . "' and mk.unit_id = '" . $unit_id . "'
	and uk.unit_id=mk.unit_id";
    $con = Propel::getConnection();
    $stmt = $con->prepareStatement($query);
    $rs = $stmt->executeQuery();
    $ranking = '';
    while ($rs->next()) {
        $kegiatan_kode = $rs->getString('kode_kegiatan');
        $nama_kegiatan = $rs->getString('nama_kegiatan');
        $alokasi = $rs->getString('alokasi_dana');
        $organisasi = $rs->getString('unit_name');
        $benefit = $rs->getString('benefit');
        $impact = $rs->getString('impact');
        $kode_program = $rs->getString('kode_program');
        $program_p_13 = $rs->getString('kode_program2');
        //$urusan = $rs->getString('urusan');
        $kode_misi = $rs->getString('kode_misi');
        $kode_tujuan = $rs->getString('kode_tujuan');
        $outcome = $rs->getString('outcome');
        $masukan = $rs->getString('masukan');
        $output = $rs->getString('output');
        $target_outcome = $rs->getString('target_outcome');
        $catatan = $rs->getString('catatan');
        $ranking = $rs->getInt('ranking');
        $kode_organisasi = $rs->getString('kode_urusan');
        $kode_program2 = $rs->getString('kode_program2');
        $kode_permen = $rs->getString('kode_permen');
        $lokasi = $rs->getString('lokasi');

        $kode_per = explode(".", $kode_organisasi);

        $kode_program_dpa = '';

        if (substr($kode_program2, 0, 4) == 'X.XX') {
            $kode_program_dpa = substr($rs->getString('kode_urusan'), 0, 4);
        } else {
            $kode_program_dpa = $kode_program2;
        }

        $cp = new Criteria();
        $cp->add(MasterProgram2Peer::KODE_PROGRAM2, $kode_program2);
        $program2 = MasterProgram2Peer::doSelectOne($cp);

        $query = "select * from " . sfConfig::get('app_default_schema') . ".master_program2 ms where ms.kode_program2='" . $kode_program2 . "'";
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query);
        $rs1 = $stmt->executeQuery();
        while ($rs1->next()) {
            $nama_program2 = $rs1->getString('nama_program2');
        }

        $kode_program22 = substr($kode_program2, 5, 2);
        if (substr($kode_program2, 0, 4) == 'X.XX') {
            $kode_urusan = substr($kode_per[0] . '.' . $kode_per[1], 0, 4);
        } else {
            $kode_urusan = substr($kode_program2, 0, 4);
        }
    }

    $total = 0;
    $query2 = "select sum(volume * komponen_harga_awal * (100+pajak)/100) as hasil
	from " . sfConfig::get('app_default_schema') . ".murni_rincian_detail rd
	where rd.kegiatan_code = '" . $kode_kegiatan . "' and rd.unit_id = '" . $unit_id . "' and rd.status_hapus=false";
    $stmt5 = $con->prepareStatement($query2);
    $total_rekening = $stmt5->executeQuery();
    while ($total_rekening->next()) {
        $total = $total_rekening->getString('hasil');
    }

    $cp->clear();
    $cp->add(UnitKerjaPeer::UNIT_ID, $unit_id);
    $pengguna_anggaran = UnitKerjaPeer::doSelectOne($cp);

    $temp_kode1 = explode('.', $kode_program2);
    $no_dppa_skpd = $temp_kode1[0] . '.' . $temp_kode1[1] . '.' . $kode_permen . '.' . $kode_program22 . '.' . $kegiatan_kode;
    ?>
    <body>
        <table style="empty-cells: show;" border="0" cellpadding="3" cellspacing="0" width="100%">
            <tbody>
                <tr><td colspan="5" align="center">
    <?php echo image_tag('logosby.png', array('width' => '100')) ?><h5 style="margin-top: 10px">PEMERINTAH KOTA SURABAYA</h5>

                        <h4 style="margin: 25px 0px 20px 0px">PENJABARAN RAPBD</h4>

                        <h4 style="margin: 0px 0px 20px 0px">SATUAN KERJA PERANGKAT DAERAH ( SKPD )</h4>
                        <h4 style="margin: 0px 0px 40px 0px">TAHUN ANGGARAN <?php echo sfConfig::get('app_tahun_default'); ?></h4>
                        <h2 style="margin: 0px 0px 15px 0px">BELANJA LANGSUNG</h2>
                        <!--<h5 style="margin: 0px 0px 25px 0px">NO RKA SKPD : <?php echo $no_dppa_skpd ?></h5>-->
                    </td></tr>
                <tr>
                    <td align="center" colspan="5">
                        <table align="center" style="font-size: smaller; font-weight: bold;" cellspacing="10px">
                            <tr>
<?php
$temp_ambil_urusan = explode(".", $kode_permen);
$kode_ambil_urusan = $temp_ambil_urusan[0] . '.' . $temp_ambil_urusan[1];
$c = new Criteria();
//$c->add(MasterUrusanPeer::KODE_URUSAN,$kode_per[0].'.'.$kode_per[1]);
$c->add(MasterUrusanPeer::KODE_URUSAN, $kode_ambil_urusan);
$urusan = MasterUrusanPeer::doSelectOne($c);
if ($kode_ambil_urusan == '1.20')
    $urusan_pasti = 'Pemerintahan Umum';
?>
                                <td>URUSAN PEMERINTAHAN </td><td>:</td><td> <?php echo '( ' . $kode_ambil_urusan . ' ) ';
echo ($kode_ambil_urusan == '1.20') ? $urusan_pasti : $urusan->getNamaUrusan() ?> </td>
                            </tr>
                            <tr>
                                <td>ORGANISASI </td><td>:</td><td> <?php echo '( ' . $kode_permen . ' ) ' . $organisasi ?></td>
                            </tr>
                            <tr>
                                <td>PROGRAM </td><td>:</td><td> <?php echo '( ' . $kode_program_dpa . ' ) ' . $program2->getNamaProgram2() ?></td>
                            </tr>
                            <tr>
                                <td>KEGIATAN </td><td>:</td><td> <?php echo '( ' . $kode_urusan . '.' . $kode_program22 . '.' . $kegiatan_kode . ' ) ' . $nama_kegiatan ?></td>
                            </tr>
                            <!--<tr>
                            <td>LOKASI KEGIATAN </td><td>: <?php echo $lokasi ?></td>
                            </tr>-->
                            <!--<tr>
                            <td>SUMBER DANA </td><td>: <?php echo '' ?></td>
                            </tr>-->
                            <tr>
                                <td>JUMLAH ANGGARAN </td><td>:</td><td> Rp. <?php echo number_format($total, 0, ',', '.') ?></td>
                            </tr>
                            <tr>
                                <td>TERBILANG </td><td>:</td><td> ( <?php echo ucwords(myTools::terbilang(round($total))) ?> Rupiah )</td>
                            </tr>
                            <tr>
                                <td>PENGGUNA ANGGARAN/<br>KUASA PENGGUNA ANGGARAN </td><td></td><td> </td>
                            </tr>
                            <tr>
                                <td>NAMA </td><td>:</td><td> <?php echo $pengguna_anggaran->getKepalaNama() ?></td>
                            </tr>
                            <tr>
                                <td>NIP </td><td>:</td><td> <?php echo $pengguna_anggaran->getKepalaNip() ?></td>
                            </tr>
                            <tr>
                                <td>JABATAN </td><td>:</td><td> <?php echo $pengguna_anggaran->getKepalaPangkat() ?></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <!-- ditutup dulu waktu ada pembahasan di dewan
                <tr>
        
              <td colspan="3" style="border-top: 1px solid; border-left: 1px solid;"><div align="center"><span class="style1 font12"><strong><span class="style3">RENCANA KERJA DAN ANGGARAN</span><br>
                </strong></span>        
                </div>        <div align="center"><span class="font12 style1 style2"><span class="font12 style3 style1"><strong>SATUAN KERJA PERANGKAT DAERAH </strong></span></span></div></td>
              <td style="border-top: 1px solid; border-left: 1px solid; border-right: 1px solid"><div class="style3" align="center"><strong>NOMOR RKA SKPD<br>
<?php echo $no_dppa_skpd ?> </strong></div></td>
            </tr>-->
                <tr>

                    <td colspan="5" style="border-top: 1px solid; font-weight: bold; border-left: 1px solid; border-right: 1px solid"><div align="center"><span class="style3">PEMERINTAH KOTA SURABAYA <br>
                            </span></div>        
                        <div align="center"><span class="style3">TAHUN ANGGARAN <?php echo sfConfig::get('app_tahun_default'); ?></span></div></td>
                </tr>
                <tr>
                    <td colspan="4" style="border: 1px solid rgb(0, 0, 0);"><table border="0" width="100%">
                            <tbody style="font-size: smaller;"><tr align="left" valign="top">
                                    <td nowrap="nowrap" width="194"><span class="style3">Urusan Pemerintahan </span></td>
                                    <td width="10"><span class="style3">:</span></td>
                                    <td width="990"><span class="style3"><?php echo '( ' . $kode_ambil_urusan . ' ) ';
echo ($kode_ambil_urusan == '1.20') ? $urusan_pasti : $urusan->getNamaUrusan() ?></span></td>
                                </tr>
                                <tr align="left" valign="top">
                                    <td><span class="style3">Organisasi</span></td>
                                    <td><span class="style3">:</span></td>
                                    <td><span class="style3">( <?php echo /* $kode_program2 */$kode_permen . ' ) ' . $organisasi ?> </span></td>

                                </tr>

                                <tr align="left" valign="top">
                                    <td><span class="style3">Program</span></td>
                                    <td><span class="style3">:</span></td>
                                    <td><span class="style3"><?php echo '( ' . $kode_program_dpa . ' ) ' . $program2->getNamaProgram2() ?></span></td>

                                </tr>

                                <tr align="left" valign="top">
                                    <td><span class="style3">Kegiatan</span></td>
                                    <td><span class="style3">:</span></td>
                                    <td><span class="Font8v style3">( <?php echo $kode_urusan . '.' . $kode_program22 . '.' . $kegiatan_kode . ' ' ?> )</span><span class="style3"> <?php echo $nama_kegiatan ?></span></td>
                                </tr>
                                <!--<tr align="left" valign="top">
                                  <td><span class="style3">Waktu Pelaksanaan</span></td>
                                  <td><span class="style3">:</span></td>
                        
                                  <td><span class="style3"> &nbsp;  </span></td>
                                </tr>-->
                                        <!--<tr align="left" valign="top">
                                  <td><span class="style3">Lokasi Kegiatan</span></td>
                                  <td><span class="style3">:</span></td>
                        
                                  <td><span class="style3"> &nbsp;  </span></td>
                                </tr>-->
                                        <!--<tr align="left" valign="top">
                                  <td><span class="style3">Jumlah Tahun n-1</span></td>
                                  <td><span class="style3">:</span></td>
                        
                                  <td><span class="style3">Rp. &nbsp;  </span></td>
                                </tr>
                                        <tr align="left" valign="top">
                                  <td><span class="style3">Jumlah Tahun n</span></td>
                                  <td><span class="style3">:</span></td>
                        
                                  <td><span class="style3">Rp. &nbsp;  </span></td>
                                </tr>
                                        <tr align="left" valign="top">
                                  <td><span class="style3">Jumlah Tahun n-1</span></td>
                                  <td><span class="style3">:</span></td>
                        
                                  <td><span class="style3">Rp. &nbsp;  </span></td>
                                </tr>-->
                                <tr>
                                    <td>Latar belakang perubahan/dianggarkan dalam perubahan APBD</td>
                                    <td>:</td><td><span class="style3"> &nbsp;  </span></td>
                                </tr>
                            </tbody>
                        </table>
                        <table bgcolor="#333333" border="0" cellpadding="3" cellspacing="1" width="100%" style="font-size: smaller">
                            <tr align='center' valign='top'>
                                <td colspan="3" bgcolor="#FFFFFF"><b>Indikator & Tolak Ukur Kinerja Belanja Langsung</b></td>
                            </tr>
                            <tr align='center' valign='top'>
                                <td bgcolor="#FFFFFF" width="10%"><b>Indikator</b></td>
                                <td bgcolor="#FFFFFF" width="45%"><b>Tolak Ukur Kinerja</b></td>
                                <td bgcolor="#FFFFFF" width="45%"><b>Target Kinerja</b></td>
                            </tr>
                            <tr valign='top'>
                                <td align='left' bgcolor="#FFFFFF">Capaian Program</td>
                                <td bgcolor="#FFFFFF"><?php echo $benefit ?></td>
                                <td bgcolor="#FFFFFF"><?php echo $impact ?></td>
                            </tr> 
                            <!--ditutup dulu waktu pembahasan di dewan
                            <tr valign='top'>
                                    <td align='left' bgcolor="#FFFFFF">Masukan</td>
                                    <td bgcolor="#FFFFFF"><?php echo $masukan ?></td>
                                    <td bgcolor="#FFFFFF">Rp. <?php echo number_format($total, 0, ',', '.') ?></td>
                            </tr> -->
                            <tr valign='top'>
                                <td align='left' bgcolor="#FFFFFF">Keluaran</td>
<?php $tmp_outcome = explode('|', $outcome) ?>
                                <td bgcolor="#FFFFFF"><?php echo $tmp_outcome[0] ?></td>
                                <td bgcolor="#FFFFFF"><?php echo $tmp_outcome[1] . $tmp_outcome[2] ?></td>
                            </tr> 
                            <tr valign='top'>
                                <td align='left' bgcolor="#FFFFFF">Hasil</td>
<?php
$tmp1 = explode('#', $output);
$tmp_target_output = array();
$tmp_tolok_output = array();
for ($i = 0; $i < count($tmp1); $i++) {
    $tmp2 = explode('|', $tmp1[$i]);
    $tmp_target_output[] = $tmp2[1] . ' ' . $tmp2[2];
    $tmp_tolok_output[] = $tmp2[0];
}
?>
                                <td bgcolor="#FFFFFF"><?php foreach ($tmp_tolok_output as $ttl)
                                    echo "- " . $ttl . "<br>"; ?></td>
                                <td bgcolor="#FFFFFF"><?php foreach ($tmp_target_output as $tto)
                                    echo "- " . $tto . "<br>"; ?></td>
                            </tr> 
                        </table>
                    </td>

                </tr>
                <tr>
                    <td style="font-weight: bold; font-size: smaller; border-left: 1px solid; border-right: 1px solid" colspan="5"><span class="style3">Kelompok Sasaran Kegiatan :  </span></td>
                </tr>

                <tr>
                    <td colspan="4" style="border-right: 1px solid; border-left: 1px solid; border-top: 1px solid; font-weight: bold; font-size: smaller"><div align="center"><span class="style3">PENJABARAN RAPBD<!--RINCIAN DOKUMEN PELAKSANAAN ANGGARAN--></span></div>
                        <div align="center"><span class="style3">BELANJA LANGSUNG SATUAN KERJA PERANGKAT DAERAH </span></div></td>
                </tr>
            </tbody>
        </table>
        <table bgcolor="#333333" border="0" cellpadding="3" cellspacing="1" width="100%" style="font-size: smaller">        
            <tr >
                    <!--ditutup dulu ada pembahasan di dewan<td align="center" bgcolor="#FFFFFF" rowspan="2">&nbsp;</td>-->
                <td align="center" bgcolor="#FFFFFF" rowspan="2" width="30%"><span class="style3"><strong>Kode Rekening</strong></span></td>
                <!--ditutup dulu ada pembahasan di dewan-->
                <!--<td align="center" bgcolor="#FFFFFF" rowspan="2" width="40%"><span class="style3"><strong>Uraian</strong></span></td>
                <td align="center" bgcolor="#FFFFFF" colspan="3" width="15%"><span class="style3"><strong>Rincian Penghitungan</strong></span></td>-->
                <td align="center" bgcolor="#FFFFFF" rowspan="2" width="15%"><span class="style3"><strong>Jumlah</strong></span></td>
            </tr>

            <tr>
                <!--ditutup dulu ada pembahasan di dewan
                <td align="center" bgcolor="#FFFFFF"><span class="style3"><strong>Harga Satuan</strong></span></td>
                <td align="center" bgcolor="#FFFFFF"><span class="style3"><strong>Volume</strong></span></td>
                <td align="center" bgcolor="#FFFFFF"><span class="style3"><strong>Satuan</strong></span></td>-->
            </tr>
<?php
$query = "
					select distinct(rekening.rekening_code), detail2.rekening_code as rekening 
					from 
					(" . sfConfig::get('app_default_schema') . ".murni_rincian_detail detail2 inner join " . sfConfig::get('app_default_schema') . ".rekening rekening on rekening.rekening_code = detail2.rekening_code)
					where detail2.kegiatan_code = '" . $kode_kegiatan . "' and detail2.unit_id = '" . $unit_id . "'	and detail2.status_hapus=false		
					order by
						rekening.rekening_code
				";

$con = Propel::getConnection();
$stmt = $con->prepareStatement($query);
$rs = $stmt->executeQuery();

$temp_belanja_01 = '';
$temp_belanja_1 = '';
$temp_belanja_2 = '';

$temp_belanja_01_prev = '';
$temp_belanja_1_prev = '';
$temp_belanja_2_prev = '';

$subtitle = '';
$subtitle_prev = '';

$temp_subtitle = '';
$temp_subtitle_prev = '';

$tahap = 0;
$awal = 0;
while ($rs->next()) {
    if ($awal == 0) {
        $belanja_0 = substr($rs->getString('rekening'), 0, 1);

        $d = new Criteria();
        $d->add(KelompokRekeningPeer::REKENING_CODE, $belanja_0);
        $ds = KelompokRekeningPeer::doSelectOne($d);
        if ($ds) {
            $nama_belanja_pegawai = $ds->getRekeningName();
        } else {
            $nama_belanja_pegawai = '';
        }
        $query2 = "
							select 
							sum(detail2.volume * detail2.komponen_harga_awal * (100+detail2.pajak)/100) as hasil_kali
							
							FROM 
							(" . sfConfig::get('app_default_schema') . ".murni_rincian_detail detail2 inner join 
							" . sfConfig::get('app_default_schema') . ".rekening rekening on rekening.rekening_code = detail2.rekening_code)
						WHERE 
							detail2.kegiatan_code = '" . $kode_kegiatan . "' and 
							detail2.unit_id='" . $unit_id . "'  and detail2.status_hapus=false
							
							and detail2.rekening_code ilike '" . $belanja_0 . "%'		
							";
        //print_r($query2);exit;
        $stmt = $con->prepareStatement($query2);
        $rs1 = $stmt->executeQuery();
        while ($rs1->next()) {
            $harga = $rs1->getString('hasil_kali');
        }
        if ($belanja_0 == '2') {
            $harga = '0';
        }
        ?>
                    <tr align='center' bgcolor="white">
                            <!-- ditutup dulu waktu ada pembahasan di dewan<td>&nbsp;</td>-->
                        <td  align='left'><?php echo $belanja_0 . '&nbsp;' . $nama_belanja_pegawai ?></td>
                        <!-- ditutup dulu waktu ada pembahasan di dewan
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>-->
                        <td align='right'><?php echo number_format($harga, 3, ',', '.') ?></td>
                    </tr>

                    <?php
                    $awal = 1;
                }

                $belanja_01 = substr($rs->getString('rekening'), 0, 3);

                if (($belanja_01 == '') && ($belanja_01_prev != '')) {
                    $belanja_01 = $belanja_01_prev;
                }

                if (($temp_belanja_01 != $belanja_01)) {
                    $temp_belanja_01 = $belanja_01;

                    $d = new Criteria();
                    $d->add(KelompokRekeningPeer::REKENING_CODE, $temp_belanja_01);
                    $ds = KelompokRekeningPeer::doSelectOne($d);
                    $nama_belanja_pegawai = '';
                    if ($ds) {
                        $nama_belanja_pegawai = $ds->getRekeningName();
                    } else {
                        $nama_belanja_pegawai = 'BELANJA LANGSUNG';
                    }

                    $query2 = "
								select 
								sum(detail2.volume * detail2.komponen_harga_awal * (100+detail2.pajak)/100) as hasil_kali
								
								FROM 
							(" . sfConfig::get('app_default_schema') . ".murni_rincian_detail detail2 inner join 
							" . sfConfig::get('app_default_schema') . ".rekening rekening on rekening.rekening_code = detail2.rekening_code)
						WHERE 
							detail2.kegiatan_code = '" . $kode_kegiatan . "' and 
							detail2.unit_id='" . $unit_id . "' and detail2.status_hapus=false 
							
							and detail2.rekening_code ilike '" . $temp_belanja_01 . "%'		
							";
                    $stmt = $con->prepareStatement($query2);
                    $rs1 = $stmt->executeQuery();
                    $harga = 0;
                    while ($rs1->next()) {
                        $harga += $rs1->getString('hasil_kali');
                    }
                    $total = $harga;
                    if ($belanja_01 == '2') {
                        $harga = '0';
                    }
                    ?>
                    <tr align='center' bgcolor="white">
                            <!-- ditutup dulu waktu ada pembahasan di dewan<td>&nbsp;</td>-->
                        <td align='left'><?php echo $belanja_01 . '&nbsp;' . $nama_belanja_pegawai ?></td>
                        <!--ditutup dulu ada pembahasan didewan
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>-->
                        <td align='right'><?php echo number_format($harga, 3, ',', '.') ?></td>
                    </tr>

                    <?php
                }

                //exit;
                $belanja_1 = substr($rs->getString('rekening'), 0, 5);

                if ($tahap == 0) {
                    if (($temp_belanja_1 != $belanja_1)) {
                        $temp_belanja_1 = $belanja_1;

                        $e = new Criteria();
                        $e->add(KelompokRekeningPeer::REKENING_CODE, $belanja_1);
                        $es = KelompokRekeningPeer::doSelectOne($e);
                        if ($es) {
                            $nama_belanja_pegawai1 = $es->getRekeningName();
                        }
                        $query2 = "
								select 
								sum(detail2.volume * detail2.komponen_harga_awal * (100+detail2.pajak)/100) as hasil_kali_prev
								
								from 
									" . sfConfig::get('app_default_schema') . ".murni_rincian_detail detail2 
								
								where detail2.kegiatan_code = '" . $kode_kegiatan . "' and detail2.unit_id = '" . $unit_id . "'	and detail2.rekening_code ilike '" . $belanja_1 . "%' 
									and detail2.status_hapus=false		
							";
                        $stmt = $con->prepareStatement($query2);
                        $rs1 = $stmt->executeQuery();
                        while ($rs1->next()) {
                            $harga = $rs1->getString('hasil_kali_prev');
                        }
                        if ($belanja_1 == '2') {
                            $harga = '0';
                        }
                        ?>
                        <tr align='center' bgcolor="white">
                                <!-- ditutup dulu waktu ada pembahasan di dewan<td>&nbsp;</td>-->
                            <td align='left'><?php echo $belanja_1 . '&nbsp;' . $nama_belanja_pegawai1 ?></td>
                            <!--
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>-->
                            <td align='right'><?php echo number_format($harga, 3, ',', '.') ?></td>
                        </tr>

                        <?php
                    }
                    $tahap = 1;
                }
                //exit;	
                $belanja_2 = substr($rs->getString('rekening'), 0, 8);

                if ($tahap == 1) {
                    if (($temp_belanja_2 != $belanja_2)) {

                        $temp_belanja_2 = $belanja_2;
                        //print_r($temp_belanja_2_prev);exit;
                        $e = new Criteria();
                        $e->add(KelompokRekeningPeer::REKENING_CODE, $belanja_2);
                        $es = KelompokRekeningPeer::doSelectOne($e);
                        if ($es) {
                            $nama_belanja_pegawai1 = $es->getRekeningName();
                        }
                        $query2 = "
								select 
								sum(detail2.volume * detail2.komponen_harga_awal * (100+detail2.pajak)/100) as hasil_kali_prev
								
								from 
									" . sfConfig::get('app_default_schema') . ".murni_rincian_detail detail2 
								
								where detail2.kegiatan_code = '" . $kode_kegiatan . "' and detail2.unit_id = '" . $unit_id . "'	and detail2.rekening_code ilike '" . $belanja_2 . "%'
									and detail2.status_hapus=false		
							";
                        $stmt = $con->prepareStatement($query2);
                        $rs1 = $stmt->executeQuery();
                        while ($rs1->next()) {
                            $harga = $rs1->getString('hasil_kali_prev');
                        }

                        if ($belanja_2 == '2') {
                            $harga = '0';
                        }
                        ?>
                        <tr align='center' bgcolor="white">
                                <!-- ditutup dulu waktu ada pembahasan di dewan<td>&nbsp;</td>-->
                            <td align='left'><?php echo $belanja_2 . '&nbsp;' . $nama_belanja_pegawai1 ?></td>
                            <!--
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>-->
                            <td align='right'><?php echo number_format($harga, 3, ',', '.') ?></td>
                        </tr>

                        <?php
                    }
                    $tahap = 2;
                }
                //exit;
                $belanja_3 = $rs->getString('rekening');

                if ($tahap == 2) {
                    $e = new Criteria();
                    $e->add(RekeningPeer::REKENING_CODE, $belanja_3);
                    $es = RekeningPeer::doSelectOne($e);
                    if ($es) {
                        $nama_belanja_pegawai1 = $es->getRekeningName();
                    }
                    $query2 = "
							select 
							sum(detail2.volume * detail2.komponen_harga_awal * (100+detail2.pajak)/100) as hasil_kali_prev
							
							from 
								" . sfConfig::get('app_default_schema') . ".murni_rincian_detail detail2 
							
							where detail2.kegiatan_code = '" . $kode_kegiatan . "' and detail2.unit_id = '" . $unit_id . "'	and detail2.rekening_code ilike '" . $belanja_3 . "'
								and detail2.status_hapus=false		
						";
                    $stmt = $con->prepareStatement($query2);
                    $rs1 = $stmt->executeQuery();
                    while ($rs1->next()) {
                        $harga = $rs1->getString('hasil_kali_prev');
                    }

                    if ($belanja_3 == '') {
                        $belanja_3 = $belanja_3_prev;
                        $nama_belanja_pegawai1 = $nama_belanja_pegawai_prev1;
                        $harga = '0';
                    }
                    //print_r($belanja_3_prev);exit;
                    ?>
                    <tr bgcolor="white" align='center'>
                            <!-- ditutup dulu waktu ada pembahasan di dewan<td>&nbsp;</td>-->
                        <td align='left'><?php echo $belanja_3 . '&nbsp;' . $nama_belanja_pegawai1 ?></td>
                        <!--ditutup dulu ada pembahasan didewan
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>-->

                        <td align='right'><?php echo number_format($harga, 3, ',', '.') ?></td>
                    </tr>

                    <?php
                    //exit;
                    $tahap = 0;

                    $query3 = "SELECT 

							rekening.rekening_code,rekening.rekening_code as rekening_code2,
							rekening.rekening_name as nama_rekening2,
							detail2.komponen_name || ' ' || detail2.detail_name as detail_name22,
							detail2.komponen_harga_awal as detail_harga2,
							detail2.pajak as pajak2,
							detail2.komponen_id as komponen_id2,
							detail2.detail_name as detail_name2,
							detail2.komponen_name as komponen_name2,
							detail2.volume as volume2,
							detail2.subtitle as subtitle_name2,
							detail2.satuan as detail_satuan2,
							replace(detail2.keterangan_koefisien,'<*>','X') as keterangan_koefisien2,
							detail2.detail_no as detail_no2,
							detail2.sub as sub,
							detail2.volume * detail2.komponen_harga_awal as hasil2,
							(detail2.volume * detail2.komponen_harga_awal * (100+detail2.pajak)/100) as hasil_kali2
							
									
						FROM 
							(" . sfConfig::get('app_default_schema') . ".murni_rincian_detail detail2 inner join 
							" . sfConfig::get('app_default_schema') . ".rekening rekening on rekening.rekening_code = detail2.rekening_code) 
						WHERE 
							detail2.kegiatan_code = '" . $kode_kegiatan . "' and 
							detail2.unit_id='" . $unit_id . "' and detail2.status_hapus=false
							
							and detail2.rekening_code='" . $rs->getString("rekening_code") . "'
							
						ORDER BY 
							
							rekening.rekening_code,
							detail2.subtitle ,
							detail2.komponen_name,
							detail2.sub";
                    //print_r($query3);exit;
                    $stmt3 = $con->prepareStatement($query3);
                    $rs3 = $stmt3->executeQuery();
                    $temp_sub = '';
                    $sub = '';
                    while ($rs3->next()) {
                        $temp_subtitle = $rs3->getString('subtitle_name2');
                        $temp_sub = $rs3->getString('sub');

                        if ($temp_subtitle != $subtitle) {
                            $subtitle = $temp_subtitle;

                            if ($sf_user->getNamaUser() != 'parlemen2' && $sf_user->getNamaUser() != 'inspect'  && $sf_user->getNamaUser() != 'peninjau' && $sf_user->getNamaUser() != 'dppk' && $sf_user->getNamaUser() != 'bppk') {
                                ?>
                                <tr bgcolor="white" align='center'>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td align='left'><?php echo '<b><i>::' . $subtitle . '</i></b>' ?></td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <?php
                            }
                        }
                        if ($temp_sub != '') {
                            if ($sub != $temp_sub) {
                                $sub = $temp_sub;
                                //$subtitle = $sub;

                                $s = new Criteria();
                                $s->add(RincianSubParameterPeer::NEW_SUBTITLE, $sub);
                                $sub_pilih = RincianSubParameterPeer::doSelectOne($s);
                                if ($sub_pilih) {
                                    $sub_name = $sub_pilih->getSubKegiatanName();
                                    $keterangan_sub = $sub_pilih->getKeterangan();
                                    $detail_name_sub = $sub_pilih->getDetailName();
                                }

                                if ($sf_user->getNamaUser() != 'parlemen2' && $sf_user->getNamaUser() != 'inspect' &&  $sf_user->getNamaUser() != 'peninjau' && $sf_user->getNamaUser() != 'dppk' && $sf_user->getNamaUser() != 'bppk') {
                                    ?>
                                    <tr bgcolor="white" align='center'>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td align='left'><?php echo '<i>&nbsp;&nbsp;' . $sub_name . ' ' . $detail_name_sub . '</i>' ?></td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <?php
                                }
                            }
                        }
                        if ($sf_user->getNamaUser() != 'parlemen2' && $sf_user->getNamaUser() != 'inspect' && $sf_user->getNamaUser() != 'peninjau' && $sf_user->getNamaUser() != 'dppk' && $sf_user->getNamaUser() != 'bppk') {
                            ?>
                            <tr bgcolor="white" align='center'>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td align="left"><?php echo $rs3->getString('detail_name22') ?> </td>
                                <td><?php echo $rs3->getString('detail_harga2') ?> </td>
                                <td><?php echo $rs3->getString('volume2') ?> </td>
                                <td><?php echo $rs3->getString('detail_satuan2') ?> </td>
                                <td align='right'><?php echo number_format($rs3->getString('hasil_kali2'), 3, ',', '.') ?> </td>
                            </tr>
                <?php
            }
            //exit;	
        }
        if ($sf_user->getNamaUser() != 'parlemen2' && $sf_user->getNamaUser() != 'inspect' && $sf_user->getNamaUser() != 'peninjau' && $sf_user->getNamaUser() != 'dppk' && $sf_user->getNamaUser() != 'bppk') {
            ?>
                        <tr bgcolor="white" align='center'>
                            <td colspan='10'>&nbsp;</td>
                        </tr>
                        <?php
                    }

                    $subtitle = '';
                    $sub = '';
                    $temp_sub = '';
                }
                /*
                  $query2="select sum(volume * komponen_harga_awal * (100+pajak)/100) as hasil
                  from ". sfConfig::get('app_default_schema') .".rincian_detail rd
                  where rd.kegiatan_code = '".$kode_kegiatan."' and rd.unit_id = '".$unit_id."' and rekening_code ='".$rs->getString("rekening_code")."' ";

                  $con = Propel::getConnection();
                  $stmt5 = $con->prepareStatement($query2);
                  $total_rekening = $stmt5->executeQuery();
                  while($total_rekening->next())
                  {
                  echo '<tr bgcolor="#ffffff">';
                  echo '<td colspan="6" class="Font8v style3" align="right">';
                  echo '<b>Total '.$belanja_3.'&nbsp;'.$nama_belanja_pegawai1.':</b></td>';
                  echo '<td class="Font8v style3" align="right" nowrap="nowrap"><b>'.number_format(round($total_rekening->getString('hasil'),0), 0, ',', '.').'</b></td>';

                  echo '</tr>';
                  }
                 */
            }
            $query2 = "select sum(nilai_anggaran) as hasil
							from " . sfConfig::get('app_default_schema') . ".murni_rincian_detail rd
							where rd.kegiatan_code = '" . $kode_kegiatan . "' and rd.unit_id = '" . $unit_id . "' and rd.status_hapus=false";

            $con = Propel::getConnection();
            $stmt5 = $con->prepareStatement($query2);
            $total_rekening = $stmt5->executeQuery();
            while ($total_rekening->next()) {
                echo '<tr bgcolor="#ffffff">';
                echo '<td colspan="1" class="Font8v style3" align="right">';
                echo '<b>Total  :</b></td>';
                echo '<td class="Font8v style3" align="right" nowrap="nowrap"><b>' . number_format($total_rekening->getString('hasil'), 3, ',', '.') . '</b></td>';

                echo '</tr>';
            }
            ?>


        </table>
        <table border="0" width="100%">
            <tbody><tr>
            <?php
            $c = new Criteria();
            $c->add(UnitKerjaPeer::UNIT_ID, $unit_id);
            $cs = UnitKerjaPeer::doSelectOne($c);
            if ($cs) {
                $kepala = $cs->getKepalaPangkat();
                $nama = $cs->getKepalaNama();
                $nip = $cs->getKepalaNip();
                $unit_name = $cs->getUnitName();
            }
            ?>
                    <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0);" width="60%">&nbsp;</td>
                    <td class="style4" style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0);" align="left" width="30%">Surabaya, 
                        <p align="center" style="font-weight: bold">
            <?php if ($unit_id == '0700') {
                echo strtoupper('INSPEKTUR');
            } else {
                ?>
                                KEPALA <?php echo strtoupper($unit_name);
        } ?> </p> 
                        <br>
                        <br>

                        <br>
                        <br>
            <center><?php echo $nama ?><br>
            <?php echo $kepala ?><br>
                NIP : <?php echo $nip ?></center>  </td>
            </tr>

            <tr>
                <td colspan="2">
                    <table border="0"  cellspacing="0" width="100%">
                        <tbody><tr>
                                <td colspan="5" style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0);">Keterangan : </td>
                            </tr>
                            <tr>
                                <td colspan="5" style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0);">Tanggal Pembahasan : </td>


                            </tr>

                            <tr>
                                <td colspan="5" style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0);">Catatan Hasil Pembahasan : </td>

                            </tr>
                            <tr>
                                <td colspan="5" style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0);">1. </td>

                            </tr>
                            <tr>

                                <td colspan="5" style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0);">2. </td>

                            </tr>
                            <tr>
                                <td colspan="5" style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0);">3. </td>

                            </tr>
                            <tr>
                                <td colspan="5" style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0);">4. </td>


                            </tr>
                            <tr>
                                <td colspan="5" style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0);">5. </td>

                            </tr>
                            <tr>
                                <td colspan="5" style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0);"><div class="style3" align="center">
                                        <div align="center">Tim Anggaran Pemerintah Daerah </div>
                                    </div></td>


                            </tr>
                            <tr>
                                <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0);" width="3%"><div align="center"><span class="style3">No</span></div></td>
                                <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0);" width="51%"><div align="center"><span class="style3">Nama</span></div></td>
                                <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0);" width="11%"><div align="center"><span class="style3">NIP</span></div></td>
                                <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0);" width="21%"><div align="center"><span class="style3">Jabatan</span></div></td>
                                <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0);" width="14%"><div align="center"><span class="style3">Tanda Tangan </span></div></td>

                            </tr>
                            <tr>
                                <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0);" height="50px">1</td>
                                <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0);">&nbsp;</td>
                                <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0);">&nbsp;</td>
                                <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0);">&nbsp;</td>
                                <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0);">&nbsp;</td>
                            </tr>

                            <tr>
                                <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0);" height="50px">2</td>
                                <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0);">&nbsp;</td>
                                <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0);">&nbsp;</td>
                                <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0);">&nbsp;</td>
                                <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0);">&nbsp;</td>
                            </tr>
                            <tr>

                                <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0);" height="50px">3</td>
                                <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0);">&nbsp;</td>
                                <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0);">&nbsp;</td>
                                <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0);">&nbsp;</td>
                                <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0);">&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0);" height="50px">4</td>

                                <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0);">&nbsp;</td>
                                <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0);">&nbsp;</td>
                                <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0);">&nbsp;</td>
                                <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0);">&nbsp;</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>

                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                        </tbody></table></td>
            </tr>
            </tbody></table>
    </body>
</div>
