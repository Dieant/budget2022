<?php use_helper('I18N', 'Date', 'Url', 'Javascript', 'Form', 'Object') ?>

<section class="content-header">
    <h1>List Konversi</h1>
</section>

<!-- Main content -->
<section class="content">
    <?php include_partial('peneliti/list_messages'); ?>
    <!-- Default box -->    
    <div class="box box-primary box-solid">
        <div class="box-header with-border">
            <h3 class="box-title">Filters</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body">
            <?php echo form_tag('peneliti/konversilist', array('method' => 'get', 'class' => 'form-horizontal')) ?>
            <div class="form-group">
                <label class="col-sm-2 control-label">Satuan Kerja</label>
                <div class="col-sm-10">
                    <?php
//                    $c = new Criteria();
//                    $c->add(UnitKerjaPeer::UNIT_ID, '9999', Criteria::NOT_EQUAL);
//                    $c->addAscendingOrderByColumn(UnitKerjaPeer::UNIT_NAME);
//                    $v = UnitKerjaPeer::doSelect($c);
                    $e = new Criteria();
                    $nama = $sf_user->getNamaUser();
                    $b = new Criteria;
                    $b->add(UserHandleV2Peer::USER_ID, $nama);
                    $es = UserHandleV2Peer::doSelect($b);
                    $satuan_kerja = $es;
                    $unit_kerja = Array();
                    foreach ($satuan_kerja as $x) {
                        $unit_kerja[] = $x->getUnitId();
                    }
                    $e->add(UnitKerjaPeer::UNIT_ID, $unit_kerja[0], criteria::ILIKE);
                    for ($i = 1; $i < count($unit_kerja); $i++) {
                        $e->addOr(UnitKerjaPeer::UNIT_ID, $unit_kerja[$i], criteria::ILIKE);
                    }
                    //$e->add(UnitKerjaPeer::UNIT_NAME, 'Latihan', criteria::NOT_EQUAL);
                    $e->addAscendingOrderByColumn(UnitKerjaPeer::UNIT_NAME);
                    //$v = UnitKerjaPeer::doSelect($e);
                    $v = UnitKerjaPeer::doSelect($e);
                    echo select_tag('filters[unit_id]', objects_for_select($v, 'getUnitId', 'getUnitName', isset($filters['unit_id']) ? $filters['unit_id'] : null, array('include_custom' => '------Semua Dinas------')), array('class' => 'form-control'));
                    ?>
                </div>
            </div>

            <div id="sf_admin_container">
                <ul class="sf_admin_actions">
                    <li><?php echo button_to(__('reset'), 'peneliti/konversilist?filter=filter', 'class=sf_admin_action_reset_filter') ?></li>
                    <li><?php echo submit_tag(__('cari'), 'name=filter class=sf_admin_action_filter') ?></li>
                </ul>
            </div>

            <?php echo '</form>'; ?>
        </div>
    </div>
    <div class="box box-primary box-solid">
        <div class="box-body">
            <?php if (!$pager->getNbResults()): ?>
                <?php echo __('no result') ?>
            <?php else: ?>
                <div id="sf_admin_container" class="table-responsive">
                    <table cellspacing="0" class="sf_admin_list">    
                        <thead>
                            <tr>
                                <th colspan="12">
                                    <div class="float-right">
                                        <?php if ($pager->haveToPaginate()): ?>
                                            <?php echo link_to(image_tag(sfConfig::get('sf_admin_web_dir') . '/images/first.png', array('align' => 'absmiddle', 'alt' => __('First'), 'title' => __('First'))), 'peneliti/konversilist?page=1'); ?>
                                            <?php echo link_to(image_tag(sfConfig::get('sf_admin_web_dir') . '/images/previous.png', array('align' => 'absmiddle', 'alt' => __('Previous'), 'title' => __('Previous'))), 'peneliti/konversilist?page=' . $pager->getPreviousPage()); ?>

                                            <?php foreach ($pager->getLinks() as $page): ?>
                                                <?php echo link_to_unless($page == $pager->getPage(), $page, 'peneliti/konversilist?page={$page}'); ?>
                                            <?php endforeach; ?>

                                            <?php echo link_to(image_tag(sfConfig::get('sf_admin_web_dir') . '/images/next.png', array('align' => 'absmiddle', 'alt' => __('Next'), 'title' => __('Next'))), 'peneliti/konversilist?page=' . $pager->getNextPage()); ?>
                                            <?php echo link_to(image_tag(sfConfig::get('sf_admin_web_dir') . '/images/last.png', array('align' => 'absmiddle', 'alt' => __('Last'), 'title' => __('Last'))), 'peneliti/konversilist?page=' . $pager->getLastPage()); ?>
                                        <?php endif; ?>
                                    </div>
                                    <?php echo format_number_choice('[0] no result|[1] 1 result|(1,+Inf] %1% results', array('%1%' => $pager->getNbResults()), $pager->getNbResults()) ?>
                                </th>
                            </tr>
                            <tr class="sf_admin_row_3">
                                <td colspan="12"></td>
                            </tr>
                            <tr>
                                <th>SKPD Awal</th>
                                <th>Kode Kegiatan Awal</th>
                                <th>Subtitle Awal</th>
                                <th>SKPD Menjadi</th>
                                <th>Kode Kegiatan Menjadi</th>
                                <th>Subtitle Menjadi</th>
                                <th>HAPUS</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i = 1;
                            foreach ($pager->getResults() as $data):
                                $odd = fmod( ++$i, 2);
                                $c = new Criteria();
                                $c->add(UnitKerjaPeer::UNIT_ID, $data->getUnitIdLama());
                                if ($rs_skpd = UnitKerjaPeer::doSelectOne($c)) {
                                    $skpd_lama = $rs_skpd->getUnitName();
                                }
                                $c = new Criteria();
                                $c->add(UnitKerjaPeer::UNIT_ID, $data->getUnitIdBaru());
                                if ($rs_skpd = UnitKerjaPeer::doSelectOne($c)) {
                                    $skpd_baru = $rs_skpd->getUnitName();
                                }
                                ?>
                                <tr class="sf_admin_row_<?php echo $odd ?>">
                                    <td><?php echo '(' . $data->getUnitIdLama() . ') ' . $skpd_lama ?></td>
                                    <td><?php echo $data->getKegiatanCodeLama() ?></td>
                                    <td><?php echo $data->getSubtitleLama() ?></td>
                                    <td><?php echo '(' . $data->getUnitIdBaru() . ') ' . $skpd_baru ?></td>
                                    <td><?php echo $data->getKegiatanCodeBaru() ?></td>
                                    <td><?php echo $data->getSubtitleBaru() ?></td>
                                    <td><?php echo link_to(image_tag('/images/cancel.png', array('alt' => __('Delete Konversi'), 'title' => __('Delete Konversi'), 'class' => 'btn btn-sm', 'height' => '26px')), 'peneliti/hapusKonversi?unit_id=' . $data->getUnitIdLama() . '&kegiatan_code=' . $data->getKegiatanCodeLama() . '&subtitle=' . $data->getSubtitleLama(), array('confirm' => 'Yakin untuk menghapus?')); ?></td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th colspan="12">
                                    <div class="float-right">
                                        <?php if ($pager->haveToPaginate()): ?>
                                            <?php echo link_to(image_tag(sfConfig::get('sf_admin_web_dir') . '/images/first.png', array('align' => 'absmiddle', 'alt' => __('First'), 'title' => __('First'))), 'peneliti/konversilist?page=1') ?>
                                            <?php echo link_to(image_tag(sfConfig::get('sf_admin_web_dir') . '/images/previous.png', array('align' => 'absmiddle', 'alt' => __('Previous'), 'title' => __('Previous'))), 'peneliti/konversilist?page=' . $pager->getPreviousPage()) ?>

                                            <?php foreach ($pager->getLinks() as $page): ?>
                                                <?php echo link_to_unless($page == $pager->getPage(), $page, 'peneliti/konversilist?page=' . $page) ?>
                                            <?php endforeach; ?>

                                            <?php echo link_to(image_tag(sfConfig::get('sf_admin_web_dir') . '/images/next.png', array('align' => 'absmiddle', 'alt' => __('Next'), 'title' => __('Next'))), 'peneliti/konversilist?page=' . $pager->getNextPage()) ?>
                                            <?php echo link_to(image_tag(sfConfig::get('sf_admin_web_dir') . '/images/last.png', array('align' => 'absmiddle', 'alt' => __('Last'), 'title' => __('Last'))), 'peneliti/konversilist?page=' . $pager->getLastPage()) ?>
                                        <?php endif; ?>
                                    </div>
                                    <?php echo format_number_choice('[0] no result|[1] 1 result|(1,+Inf] %1% results', array('%1%' => $pager->getNbResults()), $pager->getNbResults()) ?>
                                </th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            <?php endif; ?>
        </div>
    </div>
</section>