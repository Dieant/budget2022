<?php
$boleh = false;
$c_user_handle = new Criteria();
$c_user_handle->add(UserHandleV2Peer::USER_ID, $sf_user->getNamaLogin(), Criteria::EQUAL);
$user_handles = UserHandleV2Peer::doSelect($c_user_handle);
// $user_kecuali = array('adam.yulian','adhitiya');

foreach($user_handles as $user_handle){
    if($user_handle->getStatusUser() != 'shs'){
        $boleh = true;
    }
}
?>
<td class="tombol_actions">
    <?php
        if ($sf_user->getNamaUser() == 'bpk' && $sf_user->getNamaUser() == 'peninjau' || $sf_user->getNamaUser() == 'inspect') {
            echo link_to('<i class="fa fa-edit"></i>', 'peneliti/edit?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan(), array('alt' => __('Mengubah Nilai Anggaran'), 'title' => __('Mengubah Nilai Anggaran'), 'class' => 'btn btn-outline-info btn-sm'));
        } else {
            if ($sf_user->getNamaUser() != 'masger') {
                if($boleh == true){
                    echo link_to('<i class="fa fa-edit"></i>', 'peneliti/edit?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan(). '&tahap=' . $master_kegiatan->getTahap(), array('alt' => __('Mengubah Nilai Anggaran'), 'title' => __('Mengubah Nilai Anggaran'), 'class' => 'btn btn-outline-info btn-sm'));
                    echo link_to('<i class="fa fa-th-list"></i>', 'report/tampillaporan?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan(), array('alt' => __('Laporan Perbandingan Komponen'), 'title' => __('Laporan Perbandingan Komponen'), 'class' => 'btn btn-outline-secondary btn-sm'));
                    if ($sf_user->getNamaUser() != 'bpk') {
                        echo link_to('<i class="fa fa-list-ol"></i>', 'report/bandingRekeningSimple?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan(), array('alt' => __('Laporan Perbandingan Rekening'), 'title' => __('Laporan Perbandingan Rekening'), 'class' => 'btn btn-outline-success btn-sm'));
                    }
                }
                // echo link_to('<i class="fa fa-print"></i>', 'report/TemplateRKA?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan(), array('alt' => __('Mencetak Hasil Rincian'), 'title' => __('Mencetak Hasil Rincian'), 'class' => 'btn btn-outline-warning btn-sm'));
            }
        }
        if ($sf_user->getNamaUser() == 'parlemen2' or $sf_user->getNamaUser() == 'parlemen_a' or $sf_user->getNamaUser() == 'parlemen_b' or $sf_user->getNamaUser() == 'parlemen_c' or $sf_user->getNamaUser() == 'parlemen_d' or $sf_user->getNamaUser() == 'wawali' or $sf_user->getNamaUser() == 'masger') {
            if ($sf_user->getNamaUser() != 'masger') {
                // echo link_to('<i class="fa fa-print"></i>', 'report/TemplateRKA?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan(), array('alt' => __('Mencetak Hasil Rincian'), 'title' => __('Mencetak Hasil Rincian'), 'class' => 'btn btn-outline-warning btn-sm'));
            } else {
                echo link_to('<i class="fa fa-th-list"></i>', 'report/tampillaporan?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan(), array('alt' => __('Laporan Perbandingan Komponen'), 'title' => __('Laporan Perbandingan Komponen'), 'class' => 'btn btn-outline-warning btn-sm'));
            }
        }
        $status = $sf_user->getAttribute('status', '', 'status_peneliti');
        $kegiatan_code = $master_kegiatan->getKodeKegiatan();
        $unit_id = $master_kegiatan->getUnitId();

        $c = new Criteria();
        $c->add(RincianPeer::KEGIATAN_CODE, $kegiatan_code);
        $c->add(RincianPeer::UNIT_ID, $unit_id);
        $rs_rincian = RincianPeer::doSelectOne($c);
        if ($rs_rincian) {
            $posisi = $rs_rincian->getRincianLevel();
        }

        if ($posisi == '2') {
            if ($sf_user->getNamaUser() != 'bpk' && $sf_user->getNamaUser() != 'peninjau' && $sf_user->getNamaUser() != 'inspect' && $sf_user->getNamaUser() != 'dppk' && $sf_user->getNamaUser() != 'bppk' && $sf_user->getNamaUser() != 'wawali' && $sf_user->getNamaUser() != 'walikota' && $sf_user->getNamaUser() != 'sekda' && $sf_user->getNamaUser() != 'asisten' && $sf_user->getNamaUser() != 'parlemen2' && $sf_user->getNamaUser() != 'parlemen_a' && $sf_user->getNamaUser() != 'parlemen_b' && $sf_user->getNamaUser() != 'parlemen_c' && $sf_user->getNamaUser() != 'parlemen_d' && $sf_user->getNamaUser() != 'masger') {
                ?>
                <?php
                    // echo link_to(image_tag('/sf/sf_admin/images/lock.png', array('alt' => __('Klik untuk buka ke Peneliti'), 'title' => __('Klik untuk buka ke Peneliti'))), 'peneliti/kuncirka?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan() . '&kunci=' . md5('dinas'));
                }
            } else if ($posisi == '3') {
                if ($sf_user->getNamaUser() != 'bpk' && $sf_user->getNamaUser() != 'peninjau' && $sf_user->getNamaUser() != 'inspect' && $sf_user->getNamaUser() != 'dppk' && $sf_user->getNamaUser() != 'bppk' && $sf_user->getNamaUser() != 'wawali' && $sf_user->getNamaUser() != 'walikota' && $sf_user->getNamaUser() != 'sekda' && $sf_user->getNamaUser() != 'asisten' && $sf_user->getNamaUser() != 'parlemen2' && $sf_user->getNamaUser() != 'parlemen_a' && $sf_user->getNamaUser() != 'parlemen_b' && $sf_user->getNamaUser() != 'parlemen_c' && $sf_user->getNamaUser() != 'parlemen_d' && $sf_user->getNamaUser() != 'masger') {
                    // echo link_to(image_tag('/sf/sf_admin/images/unlock.png', array('alt' => __('Klik untuk buka ke Dinas'), 'title' => __('Klik untuk buka ke Dinas'))), 'peneliti/bukarka?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan() . '&buka=' . md5('dinas'));
                }
            }
        if ($sf_user->getNamaUser() != 'bpk' && $sf_user->getNamaUser() != 'peninjau' && $sf_user->getNamaUser() != 'inspect' && $sf_user->getNamaUser() != 'dppk' && $sf_user->getNamaUser() != 'bppk' && $sf_user->getNamaUser() != 'wawali' && $sf_user->getNamaUser() != 'walikota' && $sf_user->getNamaUser() != 'sekda' && $sf_user->getNamaUser() != 'asisten' && $sf_user->getNamaUser() != 'parlemen2' && $sf_user->getNamaUser() != 'masger') {
            //irul 10 feb 2014 - lock unlock komponen
            $terkunci = 0;
            $terbuka = 0;
            $kegiatan_code = $master_kegiatan->getKodeKegiatan();
            $unit_id = $master_kegiatan->getUnitId();

            $query = "select count(*) as jumlah from " . sfConfig::get('app_default_schema') . ".rincian_detail where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and status_hapus = false and lock_subtitle = 'LOCK' and rekening_code <> '5.2.1.04.01'";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs1 = $stmt->executeQuery();
            while ($rs1->next()) {
                $terkunci = $rs1->getString('jumlah');
            }
            $query = "select count(*) as jumlah from " . sfConfig::get('app_default_schema') . ".rincian_detail where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and status_hapus = false and lock_subtitle = '' and rekening_code <> '5.2.1.04.01'";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs1 = $stmt->executeQuery();
            while ($rs1->next()) {
                $terbuka = $rs1->getString('jumlah');
            }
            // if ($terbuka == 0 && $terkunci > 1) {
            //     echo '' . link_to(image_tag('/images/Lock-Unlock-icon.png', array('width' => '18', 'height' => '18', 'alt' => __('Klik untuk buka Kunci Komponen'), 'title' => __('Klik untuk buka Kunci Komponen'))), 'peneliti/bukaKomponenAll?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan()) . '&nbsp;';
            // } else if ($terkunci == 0 && $terbuka > 1) {
            //     echo '' . link_to(image_tag('/images/Lock-Lock-icon.png', array('width' => '18', 'height' => '18', 'alt' => __('Klik untuk tutup Kunci Komponen'), 'title' => __('Klik untuk tutup Kunci Komponen'))), 'peneliti/tutupKomponenAll?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan()) . '&nbsp;';
            // } else if ($terkunci > $terbuka) {
            //     echo '' . link_to(image_tag('/images/Lock-Unlock-icon.png', array('width' => '18', 'height' => '18', 'alt' => __('Klik untuk buka Kunci Komponen'), 'title' => __('Klik untuk buka Kunci Komponen'))), 'peneliti/bukaKomponenAll?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan()) . '&nbsp;';
            // } else if ($terkunci < $terbuka) {
            //     echo '' . link_to(image_tag('/images/Lock-Lock-icon.png', array('width' => '18', 'height' => '18', 'alt' => __('Klik untuk tutup Kunci Komponen'), 'title' => __('Klik untuk tutup Kunci Komponen'))), 'peneliti/tutupKomponenAll?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan()) . '&nbsp;';
            // } else if ($terbuka == $terkunci) {
            //     echo '' . link_to(image_tag('/images/Lock-Lock-icon.png', array('width' => '18', 'height' => '18', 'alt' => __('Klik untuk tutup Kunci Komponen'), 'title' => __('Klik untuk tutup Kunci Komponen'))), 'peneliti/tutupKomponenAll?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan()) . '&nbsp;';
            // }
            //irul 10 feb 2014 - lock unlock komponen
        }
        if ($sf_user->getNamaUser() != 'bpk' && $sf_user->getNamaUser() != 'peninjau' && $sf_user->getNamaUser() != 'inspect' && $sf_user->getNamaUser() != 'dppk' && $sf_user->getNamaUser() != 'bppk' && $sf_user->getNamaUser() != 'wawali' && $sf_user->getNamaUser() != 'walikota' && $sf_user->getNamaUser() != 'sekda' && $sf_user->getNamaUser() != 'asisten' && $sf_user->getNamaUser() != 'parlemen2' && $sf_user->getNamaUser() != 'masger' && $boleh==true) 
        { 
        echo link_to('<i class="fa fa-history"></i>', 'historyuser/historylist?unit_id=' . $master_kegiatan->getUnitId() . '&kegiatan_code=' . $master_kegiatan->getKodeKegiatan(),  array('alt' => __('History User'), 'title' => __('History User'), 'class' => 'btn btn-outline-danger btn-sm'));
        } 
    ?>
</td>