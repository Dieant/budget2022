<td class="tombol_actions">
    <?php
        $boleh = false;
        $c_user_handle = new Criteria();
        $c_user_handle->add(UserHandleV2Peer::USER_ID, $sf_user->getNamaLogin(), Criteria::EQUAL);
        $user_handles = UserHandleV2Peer::doSelect($c_user_handle);
        // $user_kecuali = array('adam.yulian','adhitiya');

        foreach($user_handles as $user_handle){
            if($user_handle->getStatusUser() != 'shs'){
                $boleh = true;
            }
        }
    ?>
    <?php 
        echo link_to('<i class="fa fa-edit"></i>', 'peneliti/editRevisi?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan() . '&tahap=' . (isset($filters['tahap']) ? $filters['tahap'] : ''), array('alt' => __('edit'), 'title' => __('edit'), 'class' => 'btn btn-outline-success btn-sm'));            
        if (!(isset($filters['tahap']) && $filters['tahap'] <> '')) {
            echo link_to('<i class="fa fa-th-list"></i>', 'report/tampillaporanasli?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan(), array('alt' => __('laporan perbandingan asli-draft'), 'title' => __('laporan perbandingan asli-draft'), 'class' => 'btn btn-outline-info btn-sm'));
            if(sfConfig::get('app_tahap_edit') == 'pak' || sfConfig::get('app_tahap_edit') == 'murni') {                   
                    echo link_to('<i class="fa fa-th-list"></i>', 'report/tampillaporanBukuputih?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan(), array('alt' => __('laporan perbandingan asli-bukuputih'), 'title' => __('laporan perbandingan murni bukuputih-bukubiru'), 'class' => 'btn btn-outline-secondary btn-sm'));
                }
        } else {
            echo link_to('<i class="fa fa-list-alt"></i>', 'report/tampillaporanrevisi?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan() . '&tahap=' . $filters['tahap'], array('alt' => __('laporan perbandingan revisi'), 'title' => __('laporan perbandingan revisi'), 'class' => 'btn btn-outline-warning btn-sm'));
            echo link_to('<i class="fa fa-th-list"></i>', 'report/tampillaporanaslisemua?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan() . '&tahap=' . $filters['tahap'], array('alt' => __('laporan perbandingan asli-draft'), 'title' => __('laporan perbandingan asli-draft'), 'class' => 'btn btn-outline-info btn-sm'));
        }
        if (!(isset($filters['tahap']) && $filters['tahap'] <> '')) {
            $kegiatan_code = $master_kegiatan->getKodeKegiatan();
            $unit_id = $master_kegiatan->getUnitId();

            $c = new Criteria();
            $c->add(DinasRincianPeer::KEGIATAN_CODE, $kegiatan_code);
            $c->add(DinasRincianPeer::UNIT_ID, $unit_id);
            $rs_rincian = DinasRincianPeer::doSelectOne($c);
            if ($rs_rincian) {
                $posisi = $rs_rincian->getRincianLevel();
                $lock = $rs_rincian->getLock();
            }

            if ($lock == FALSE) {
                if ($posisi == '2' || $posisi  == '1') {
                    if ($sf_user->getNamaUser() != 'peninjau' && $sf_user->getNamaUser() != 'inspect' && $sf_user->getNamaUser() != 'dppk' && $sf_user->getNamaUser() != 'bppk' && $sf_user->getNamaUser() != 'wawali' && $sf_user->getNamaUser() != 'walikota' && $sf_user->getNamaUser() != 'sekda' && $sf_user->getNamaUser() != 'asisten' && $sf_user->getNamaUser() != 'parlemen2' && $sf_user->getNamaUser() != 'parlemen_a' && $sf_user->getNamaUser() != 'parlemen_b' && $sf_user->getNamaUser() != 'parlemen_c' && $sf_user->getNamaUser() != 'parlemen_d' && $sf_user->getNamaUser() != 'masger') {

                        echo link_to('<i class="fa fa-lock"></i>',  'peneliti/kuncirevisi?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan() . '&kunci=' . md5('dinas'), array('alt' => __('Klik untuk kunci ke Peneliti'), 'title' => __('Klik untuk kunci ke Peneliti'), 'class' => 'btn btn-outline-danger btn-sm'));
                    }
                } else if ($posisi == '3') {
                    if ($sf_user->getNamaUser() != 'peninjau' && $sf_user->getNamaUser() != 'inspect' && $sf_user->getNamaUser() != 'dppk' && $sf_user->getNamaUser() != 'bppk' && $sf_user->getNamaUser() != 'wawali' && $sf_user->getNamaUser() != 'walikota' && $sf_user->getNamaUser() != 'sekda' && $sf_user->getNamaUser() != 'asisten' && $sf_user->getNamaUser() != 'parlemen2' && $sf_user->getNamaUser() != 'parlemen_a' && $sf_user->getNamaUser() != 'parlemen_b' && $sf_user->getNamaUser() != 'parlemen_c' && $sf_user->getNamaUser() != 'parlemen_d' && $sf_user->getNamaUser() != 'masger') {

                        echo link_to('<i class="fa fa-unlock"></i>', 'peneliti/bukarevisi?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan() . '&buka=' . md5('dinas'), array('alt' => __('Klik untuk buka ke Dinas'), 'title' => __('Klik untuk buka ke Dinas'), 'class' => 'btn btn-outline-primary btn-sm'));

                    }
                }
            }
            if(sfConfig::get('app_tahap_edit') == 'murni') {
                // echo link_to('<i class="fa fa-tags"></i>', 'report/menuSetPrioritas?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan() . '&tahap=' . (isset($filters['tahap']) ? $filters['tahap'] : ''), array('alt' => __('Set Prioritas Komponen'), 'title' => __('Set Prioritas Komponen'), 'class' => 'btn btn-outline-info btn-sm'));
               echo link_to('<i class="fa fa-chalkboard-teacher"></i>', 'report/menuViewDevplan?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan() . '&tahap=' . (isset($filters['tahap']) ? $filters['tahap'] : ''), array('alt' => __('Melihat Komponen Devplan'), 'title' => __('Melihat Komponen Devplan'), 'class' => 'btn btn-outline-primary btn-sm'));
            }
        }
        echo link_to('<i class="fa fa-magnet"></i>', 'report/reportDetailKegiatan?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan(), array('alt' => __('Report Detail Kegiatan'), 'title' => __('Report Detail Kegiatan'), 'class' => 'btn btn-outline-danger btn-sm'));
        
        if (!(isset($filters['tahap']) && $filters['tahap'] <> '')) {
            if($boleh == true){
                echo link_to('<i class="fa fa-shield-virus"></i>', 'peneliti/penandaCovid?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan(), array('alt' => __('Penanda Covid'), 'title' => __('Penanda Covid'), 'class' => 'btn btn-outline-info btn-sm'));
                echo link_to('<i class="fa fa-exclamation-triangle"></i>', 'report/penandaPrioritas?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan(), array('alt' => __('Penanda Prioritas'), 'title' => __('Penanda Prioritas'), 'class' => 'btn btn-outline-warning btn-sm'));
                echo ''.link_to('<i class="fa fa-history"></i>', 'historyuser/historylist?unit_id=' . $master_kegiatan->getUnitId() . '&kegiatan_code=' . $master_kegiatan->getKodeKegiatan(),  array('alt' => __('History User'), 'title' => __('History User'), 'class' => 'btn btn-outline-primary btn-sm')).'';
            }
            echo link_to('<i class="fa fa-hand-holding-usd"></i>', 'report/editRasionalisasi?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan(), array('alt' => __('Edit Nilai Rasionalisasi'), 'title' => __('Edit Nilai Rasionalisasi'), 'class' => 'btn btn-outline-info btn-sm'));
        }
    ?>
</td>