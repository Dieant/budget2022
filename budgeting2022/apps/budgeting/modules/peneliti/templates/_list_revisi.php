<?php
    $menu = $sf_params->get('menu');
    if (isset($filters['tahap']) && $filters['tahap'] == 'pakbp') {
        $tabel_semula = 'revisi6_';
        $tabel_dpn = 'pak_bukuputih_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'pakbb') {
        $tabel_dpn = 'pak_bukubiru_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'murni') {
        $tabel_semula='murni_bukubiru_';
        $tabel_dpn = 'murni_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'murnibp') {
        $tabel_semula='murni_bukuputih_';
        $tabel_dpn = 'murni_bukuputih_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'murnibb') {
        $tabel_semula='murni_bukuputih_';
        $tabel_dpn = 'murni_bukubiru_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'murnibbpraevagub') {
        $tabel_dpn = 'murni_bukubiru_praevagub_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi1') {
        $tabel_semula='murni_';
        $tabel_dpn = 'revisi1_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi1_1') {
        $tabel_dpn = 'revisi1_1_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi2') {
        $tabel_semula = 'revisi1_';
        $tabel_dpn = 'revisi2_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi3') {
        $tabel_semula = 'revisi2_';
        $tabel_dpn = 'revisi3_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi3_1') {
        $tabel_semula = 'revisi2_';
        $tabel_dpn = 'revisi3_1_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi4') {
        $tabel_semula = 'revisi3_';
        $tabel_dpn = 'revisi4_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi5') {
        $tabel_semula = 'revisi4_';
        $tabel_dpn = 'revisi5_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi6') {
        $tabel_semula = 'revisi5_';
        $tabel_dpn = 'revisi6_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi7') {
        $tabel_dpn = 'revisi7_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi8') {
        $tabel_dpn = 'revisi8_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi9') {
        $tabel_dpn = 'revisi9_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi10') {
        $tabel_dpn = 'revisi10_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'rkua') {
        $tabel_dpn = 'rkua_';
    } else {
        $tabel_semula='';
        $tabel_dpn = 'dinas_';
    }

    $n="and kode_kegiatan not ilike '%-%'";
    $m="and kegiatan_code not ilike '%-%'";

    if($menu == 'Pendapatan')
    {
        $n = "and kode_kegiatan ilike '%-%'";
        $m = "and kegiatan_code ilike '%-%'";
    }
?>
<div class="card-body table-responsive p-0">
    <table class="table table-hover">
        <thead class="head_peach">
            <tr>
                <?php include_partial('list_th_tabular_revisi') ?>
                <th> <?php echo __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php
            $i = 1;
            foreach ($pager->getResults() as $master_kegiatan): $odd = fmod(++$i, 2)
                ?>
                <tr>
                    <?php include_partial('list_td_tabular_revisi', array('master_kegiatan' => $master_kegiatan, 'odd' => $odd, 'filters' => $filters)) ?>
                    <?php
                    if ($sf_user->getNamaUser() != 'parlemen2'):
                        include_partial('list_td_actions_revisi', array('master_kegiatan' => $master_kegiatan, 'odd' => $odd, 'filters' => $filters));
                    endif;
                    ?>
                </tr>
            <?php endforeach; ?>
            <tr >
                <td colspan="2">&nbsp;</td>
                <?php if (sfConfig::get('app_tahap_edit') == 'murni') { ?>
                <td align="right">
                    <?php
                        $kode_kegiatan = $master_kegiatan->getKodeKegiatan();
                        $unit_id = $master_kegiatan->getUnitId();
                        $thp = $filters['tahap'];
                        if($thp == "murnibp"){
                            // karena kode kegiatan yang diubah
                            $query = "select sum(alokasi) as nilai from " . sfConfig::get('app_default_schema') . ".pagu where unit_id='$unit_id' $n";
                        } else {
                            // karena kode kegiatan lama tdk ikut
                            $query = "select sum(alokasi) as nilai from " . sfConfig::get('app_default_schema') . ".pagu where unit_id='$unit_id' $n";
                        }
                        $con = Propel::getConnection(RincianDetailPeer::DATABASE_NAME);
                        $statement = $con->prepareStatement($query);
                        $rs_nilai = $statement->executeQuery();
                        while ($rs_nilai->next()) {
                            $nilai_awal = $rs_nilai->getString('nilai');
                        }

                        $query = "select sum(tambahan_pagu) as nilai from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "master_kegiatan where unit_id='$unit_id' $n";
                        $con = Propel::getConnection(RincianDetailPeer::DATABASE_NAME);
                        $statement = $con->prepareStatement($query);
                        $rs_tambahan = $statement->executeQuery();
                        while ($rs_tambahan->next()) {
                            $tambahan = $rs_tambahan->getString('nilai');
                        }

                        //penyesuaian untuk pegu murni
                        // $query = "select sum(tambahan_pagu) as nilai from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "master_kegiatan where unit_id='$unit_id' $n";
                        // $stmt = $con->prepareStatement($query);
                        // $rs_tambahan = $stmt->executeQuery();
                        // while ($rs_tambahan->next()) {
                        //     $nilai_tambahan = $rs_tambahan->getString('nilai');
                        // }

                        // $pagu_baru = intval($nilai_awal) + intval($nilai_tambahan);

                        echo '<b>'.number_format($nilai_awal, 0, ',', '.').'</b>';
                        ?>
                </td>
                <?php } ?>
                <?php if (sfConfig::get('app_tahap_edit') != 'murni') { ?>
                <td align="right">
                    <?php
                    $kode_kegiatan = $master_kegiatan->getKodeKegiatan();
                    $unit_id = $master_kegiatan->getUnitId();
                    $query = "select sum(nilai_anggaran) as nilai from " . sfConfig::get('app_default_schema') .  "." . $tabel_semula . "rincian_detail where unit_id='$unit_id' and status_hapus=FALSE $m";
                    $con = Propel::getConnection(RincianDetailPeer::DATABASE_NAME);
                    $statement = $con->prepareStatement($query);
                    $rs_nilai = $statement->executeQuery();
                    while ($rs_nilai->next()) {
                        $nilai_awal = $rs_nilai->getString('nilai');
                    }

                    $query = "select sum(tambahan_pagu) as nilai from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "master_kegiatan where unit_id='$unit_id' $n";
                    $con = Propel::getConnection(RincianDetailPeer::DATABASE_NAME);
                    $statement = $con->prepareStatement($query);
                    $rs_tambahan = $statement->executeQuery();
                    while ($rs_tambahan->next()) {
                        $tambahan = $rs_tambahan->getString('nilai');
                    }
                    echo "<b>".number_format($nilai_awal, 0, ',', '.')."</b>";
                    ?>
                </td>
                <?php } ?>
                <?php if (sfConfig::get('app_tahap_edit') == 'pak') { ?>
                    <td align="right">
                        <?php
                        $kode_kegiatan = $master_kegiatan->getKodeKegiatan();
                        $unit_id = $master_kegiatan->getUnitId();
                        $query = "select sum(pagu) as nilai from " . sfConfig::get('app_default_schema') . ".pagu_pak where unit_id='$unit_id' $n ";
                        $con = Propel::getConnection(RincianDetailPeer::DATABASE_NAME);
                        $statement = $con->prepareStatement($query);
                        $rs_nilai = $statement->executeQuery();
                        while ($rs_nilai->next()) {
                            $nilai_awal = $rs_nilai->getString('nilai');
                        }
                        echo "<b>".number_format($nilai_awal, 0, ',', '.')."</b>";
                        ?>
                    </td>
                <?php } ?>
                <?php if (sfConfig::get('app_tahap_edit') != 'pak' && sfConfig::get('app_tahap_edit') != 'murni' ) { ?>
                <td align="right">
                    <?php
                    $kode_kegiatan = $master_kegiatan->getKodeKegiatan();
                    $unit_id = $master_kegiatan->getUnitId();
                    if (sfConfig::get('app_tahap_edit') == 'murni') {
                        //digunakan ketika awal anggaran
                        $query = "select sum(alokasi_dana) as nilai from " . sfConfig::get('app_default_schema') . ".dinas_master_kegiatan where unit_id='$unit_id' $n";
                        //digunakan ketika sudah mulai akan dibandingkan dan tidak awal anggaran namun masih dalam tahap murni
                    } else if (sfConfig::get('app_tahap_edit') != 'murni') {
                        $query = "select sum(nilai_anggaran) as nilai from " . sfConfig::get('app_default_schema') . "." . $tabel_semula . "rincian_detail where unit_id='$unit_id' and status_hapus=false $m";
                    }
                    $con = Propel::getConnection(RincianDetailPeer::DATABASE_NAME);
                    $statement = $con->prepareStatement($query);
                    $rs_nilai = $statement->executeQuery();
                    while ($rs_nilai->next()) {
                        $nilai_menjadi = $rs_nilai->getString('nilai');
                    }

                    $query = "select sum(tambahan_pagu) as nilai from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "master_kegiatan where unit_id='$unit_id' $n";
                        $con = Propel::getConnection(RincianDetailPeer::DATABASE_NAME);
                        $statement = $con->prepareStatement($query);
                        $rs_tambahan = $statement->executeQuery();
                        while ($rs_tambahan->next()) {
                            $tambahan = $rs_tambahan->getString('nilai');
                        }

                    $Nilai_menjadih=$nilai_menjadi+$tambahan;
                    
                    echo "<b>".number_format($Nilai_menjadih, 0, ',', '.')."</b>";
                    ?>
                </td>
                <?php } ?>
                <td align="right">
                    <?php
                    $kode_kegiatan = $master_kegiatan->getKodeKegiatan();
                    $unit_id = $master_kegiatan->getUnitId();
                    $query = "select sum(nilai_anggaran) as nilai from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "rincian_detail where unit_id='$unit_id' and status_hapus=FALSE $m";
                    $con = Propel::getConnection(RincianDetailPeer::DATABASE_NAME);
                    $statement = $con->prepareStatement($query);
                    $rs_nilai = $statement->executeQuery();
                    while ($rs_nilai->next()) {
                        $nilai = $rs_nilai->getString('nilai');
                    }
                    echo "<b>".number_format($nilai, 0, ',', '.')."</b>";
                    ?>
                </td>
                <td align="right">
                    <?php
                     $query = "select sum(tambahan_pagu) as nilai from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "master_kegiatan where unit_id='$unit_id'  $n";
                    $stmt = $con->prepareStatement($query);
                    $rs_tambahan = $stmt->executeQuery();
                    while ($rs_tambahan->next()) {
                        $tambahan = $rs_tambahan->getString('nilai');
                    }                    

                    if ((sfConfig::get('app_tahap_edit') == 'murni') ||(sfConfig::get('app_tahap_edit') == 'pak')) {
                        $selisih = ($nilai_awal + $tambahan) - $nilai;
                    } else if (sfConfig::get('app_tahap_edit') != 'murni') {
                        $selisih = $nilai - ($nilai_menjadi + $tambahan);
                    }
                    
                    echo "<b>".number_format($selisih, 0, ',', '.')."</b>";
                    ?>
                </td>
                <?php if (sfConfig::get('app_tahap_edit') == 'pak') { ?>
             <td align="right">
                    <?php
                    $query = "select sum(tambahan_pagu) as nilai from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "master_kegiatan where unit_id='$unit_id' $n";
                    $stmt = $con->prepareStatement($query);
                    $rs_tambahan = $stmt->executeQuery();
                    while ($rs_tambahan->next()) {
                        $nilai = $rs_tambahan->getString('nilai');
                    }
                    echo "<b>".number_format($nilai, 0, ',', '.')."</b>";
                    ?>
                </td>
                <?php } ?>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
        </tbody>
    </table>
</div>
<div class="card-footer clearfix">
    <ul class="pagination pagination-sm m-0 float-left">
        <?php
            echo format_number_choice('[0] no result|[1] 1 result|(1,+Inf] %1% results', array('%1%' => $pager->getNbResults()), $pager->getNbResults());
        ?>
    </ul>
    <ul class="pagination pagination-sm m-0 float-right">
        <?php 
        if ($pager->haveToPaginate()): 
            echo '<li class="page-item">'.link_to('Previous', 'peneliti/listRevisi?menu='.$menu.'&page=' . $pager->getPreviousPage(), array('class' => 'page-link', 'align' => 'absmiddle', 'alt' => __('Previous'), 'title' => __('Previous'))).'</li>';

            foreach ($pager->getLinks() as $page):
                $activeclass = ($page == $pager->getPage()) ? 'active' : '';
                echo '<li class="page-item '.$activeclass.'">'.link_to_unless($page == $pager->getPage(), $page, "peneliti/listRevisi?menu=".$menu."&page={$page}", array('class' => 'page-link')).'</li>';
            endforeach;
            echo '<li class="page-item">'.link_to('Next', 'peneliti/listRevisi?menu='.$menu.'&page=' . $pager->getNextPage(), array('class' => 'page-link', 'align' => 'absmiddle', 'alt' => __('Next'), 'title' => __('Next'))).'</li>'; 
            endif;
        ?>
    </ul>
</div>