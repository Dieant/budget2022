<?php
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header("Content-Disposition: attachment; filename=\"Komponen_Revisi_".date("Y-m-d").".xls\"");
header("Content-Transfer-Encoding: binary");
header("Pragma: no-cache");
header("Expires: 0");
?>
<div id="sf_admin_container" class="table-responsive">
    <table cellspacing="0" class="sf_admin_list">    
        <thead>
            <tr>
                <th id="sf_admin_list_th_kompoen_id">
                    Unit ID
                </th>
                <th id="sf_admin_list_th_kompoen_id">
                    SKPD
                </th>
                <th id="sf_admin_list_th_komponen_name">
                    Kode Kegiatan
                </th>
                <th id="sf_admin_list_th_kompoen_id">
                    Kode Komponen
                </th>
                <th id="sf_admin_list_th_kompoen_id">
                    Kode Detail Kegiatan
                </th>
                <th id="sf_admin_list_th_komponen_name">
                    Nama Komponen
                </th>
                <th id="sf_admin_list_th_pajak">
                    Volume
                </th>
                <th id="sf_admin_list_th_pajak">
                    Keterangan Koefisien
                </th>
                <th id="sf_admin_list_th_komponen_harga">
                    Komponen Harga
                </th>
                <th id="sf_admin_list_th_rekening">
                    Rekening
                </th>  
                <th id="sf_admin_list_th_pajak">
                    Pajak
                </th>  
                <th id="sf_admin_list_th_pajak">
                    Nilai Anggaran
                </th>
                <th id="sf_admin_list_th_pajak">
                    Tahap
                </th>
                <th id="sf_admin_list_th_pajak">
                    Nilai Realisasi
                </th>
            </tr>
        </thead>
        <tbody>
            <?php
            $i = 1;
            foreach ($pager as $komponen): $odd = fmod(++$i, 2)
                ?>
                <tr class="sf_admin_row_<?php echo $odd ?>">
                    <td style="text-align: center"><?php echo $komponen->getUnitId() ?></td>
                    <td><?php echo UnitKerjaPeer::getStringUnitKerja($komponen->getUnitId()) ?></td>
                    <td><?php echo $komponen->getKegiatanId() ?></td>
                    <td><?php echo $komponen->getKomponenId() ?></td>
                    <td><?php echo $komponen->getDetailKegiatan() ?></td>
                    <td><?php echo $komponen->getKomponenName() ?></td>
                    <td style="text-align: center"><?php echo $komponen->getVolume() ?></td>
                    <td style="text-align: center"><?php echo $komponen->getKeteranganKoefisien() ?></td>
                    <td style="text-align: right">
                        <?php
                        $komp_id = $komponen->getKomponenId();
                        $sub_id = substr($komp_id, 0,11);
                        $komp_name = $komponen->getKomponenName();
                        $arr_komp = array('2.1.1.01.02.02.003.029');
                        $arr_name = array('Tenaga Operasional','Tenaga Administrasi 3','Tenaga Programmer 3','Tenaga Administrasi 3','Petugas Keamanan','Tenaga Operator 3','Tenaga Satgas','Pembantu Paramedis');

                        if ($komponen->getSatuan() == '%') {
                            echo $komponen->getKomponenHargaAwal();
                        } 
                        elseif (
                            in_array($komp_name, $arr_name) || 
                            in_array($komp_id,$arr_komp) ||
                            $sub_id =='2.1.1.01.01.01.008'  
                        ) {
                            $c = new Criteria();
                            $c->add(KomponenPeer::KOMPONEN_ID, $komp_id);
                            $cs = KomponenPeer::doSelectOne($c);
                            if ($cs) {
                                $a = $cs->getKomponenHarga();
                            }
                            else{
                                $a = $komponen->getKomponenHargaAwal();
                            }
                            echo number_format($a, 2, ',', '.');
                        } 
                        elseif($komponen->getKomponenHargaAwal() != floor($komponen->getKomponenHargaAwal())){
                            $a = round($komponen->getKomponenHargaAwal());
                            echo $a;
                        } else {
                            echo $komponen->getKomponenHargaAwal();
                        }
                        ?>
                    </td>
                    <td style="text-align: center"><?php echo $komponen->getRekeningCode() ?></td>
                    <td style="text-align: center"><?php echo $komponen->getPajak() ?></td>
                    <td style="text-align: right"><?php echo $komponen->getNilaiAnggaran()?></td>
                    <td style="text-align: center"><?php echo $komponen->getTahap() ?></td>
                    
                    <?php
                    
                        // itung realisasi
                        $unit_id = $komponen->getUnitId();
                        $kode_kegiatan = $komponen->getKegiatanCode();
                        $detail_no = $komponen->getDetailNo();
                        
                        $totNilaiAlokasi = 0;
                        $totNilaiRealisasi = 0;
                        $totNilaiKontrak = 0;
                        $totNilaiSwakelola = 0;
                        $nilairealisasi = 0;
                        $rs_rinciandetail = new DinasRincianDetail;
                        $totNilaiAlokasi = $rs_rinciandetail->getCekNilaiAlokasiProject($unit_id, $kode_kegiatan, $detail_no);
                        $totNilaiRealisasi = $rs_rinciandetail->getCekRealisasi($unit_id, $kode_kegiatan, $detail_no);
                        $totNilaiKontrak = $rs_rinciandetail->getCekNilaiKontrakDelivery2($unit_id, $kode_kegiatan, $detail_no);
                        $totNilaiSwakelola = $rs_rinciandetail->getCekNilaiSwakelolaDelivery2($unit_id, $kode_kegiatan, $detail_no);
                        if ($totNilaiRealisasi > 0) {
                            $nilairealisasi = $totNilaiRealisasi;
                        } else if ($totNilaiKontrak > 0) {
                            $nilairealisasi = $totNilaiKontrak;
                        } else if ($totNilaiSwakelola > 0) {
                            $nilairealisasi = $totNilaiSwakelola;
                        } else if ($totNilaiSwakelola > 0 && $totNilaiKontrak > 0) {
                            $nilairealisasi = $totNilaiSwakelola;
                        }
                    
                    ?>
                    <td style="text-align: right"><?php echo $nilairealisasi ?></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>    
</div>