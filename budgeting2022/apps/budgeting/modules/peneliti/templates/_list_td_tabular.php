<?php
$boleh = false;
$c_user_handle = new Criteria();
$c_user_handle->add(UserHandleV2Peer::USER_ID, $sf_user->getNamaLogin(), Criteria::EQUAL);
$user_handles = UserHandleV2Peer::doSelect($c_user_handle);
// $user_kecuali = array('adam.yulian','adhitiya');

foreach($user_handles as $user_handle){
    if($user_handle->getStatusUser() != 'shs'){
        $boleh = true;
    }
}

$kode = $master_kegiatan->getKegiatanId();
$unit = $master_kegiatan->getUnitId();

$query = "SELECT * from " . sfConfig::get('app_default_schema') . ".rincian_detail 
where ((note_peneliti is not Null and note_peneliti <> '') or (note_skpd is not Null and note_skpd <> '')) and unit_id = '$unit' and kegiatan_code = '$kode' and status_hapus = false ";
//diambil nilai terpakai
$con = Propel::getConnection();
$statement = $con->prepareStatement($query);
$rs = $statement->executeQuery();
$jml = $rs->getRecordCount();

$query2 = "SELECT * from " . sfConfig::get('app_default_schema') . ".master_kegiatan 
where catatan is not Null and trim(catatan) <> '' and unit_id = '$unit' and kode_kegiatan = '$kode' ";//diambil nilai terpakai
$con = Propel::getConnection();
$statement2 = $con->prepareStatement($query2);
$rs = $statement2->executeQuery();
$jml2 = $rs->getRecordCount();

if ($jml > 0 or $jml2 > 0) {
    ?>
    <td style="background: #e8f3f1"><?php echo $master_kegiatan->getKodeKegiatan() ?></td>
    <td style="background: #e8f3f1"><?php echo $master_kegiatan->getNamakegiatan();
    echo ' <font color=#FF0000>(' . $master_kegiatan->getUserId() . ')</font><br/>';
    echo 'Kode: [ '.$master_kegiatan->getKegiatanId().' ]<br/>';
    if ($sf_user->getNamaUser() != 'bpk' && $master_kegiatan->getUserId() != '' && $sf_user->getNamaUser() != 'inspect' && $sf_user->getNamaUser() != 'peninjau' && $sf_user->getNamaUser() != 'dppk' && $sf_user->getNamaUser() != 'bppk' && $sf_user->getNamaUser() != 'wawali' && $sf_user->getNamaUser() != 'walikota' && $sf_user->getNamaUser() != 'sekda' && $sf_user->getNamaUser() != 'asisten' && $sf_user->getNamaUser() != 'parlemen2' && $sf_user->getNamaUser() != 'masger') {
        if (sfConfig::get('app_fasilitas_cekeProject') == 'buka') {
                $e = new Criteria;
                $e->add(RincianBpPeer::KEGIATAN_CODE, $master_kegiatan->getKodeKegiatan());
                $e->add(RincianBpPeer::UNIT_ID, $master_kegiatan->getUnitId());
                $x = RincianBpPeer::doSelectOne($e);
                if ($x) {
                    echo '<br/><strong>Sinkronisasi e-Project ( ' . date('j F Y h:i:s A', strtotime($x->getLastUpdateTime())) . ' )</strong>';
                }
        }
    }
    ?>
    </td>
    <?php if (sfConfig::get('app_tahap_edit') == 'murni') { ?>
        <td align="right" style="background: #e8f3f1">
            <?php echo number_format($master_kegiatan->getAlokasidana(), 0, ',', '.'); ?>
        </td>
    <?php } else if (sfConfig::get('app_tahap_edit') != 'murni') { ?>
        <td align="right" style="background: #e8f3f1"><?php echo get_partial('nilaisemula', array('type' => 'list', 'master_kegiatan' => $master_kegiatan)) ?> 
    </td>
    <?php } ?> 
    <td align="right" style="background: #e8f3f1"><?php echo get_partial('nilairincian', array('type' => 'list', 'master_kegiatan' => $master_kegiatan)) ?></td>
    <td align="right" style="background: #e8f3f1"><?php echo get_partial('selisih', array('type' => 'list', 'master_kegiatan' => $master_kegiatan)) ?></td>

    <?php if ($sf_user->getNamaUser() != 'bpk' && $sf_user->getNamaUser() != 'parlemen2' && $sf_user->getNamaUser() != 'parlemen_a' && $sf_user->getNamaUser() != 'parlemen_b' && $sf_user->getNamaUser() != 'parlemen_c' && $sf_user->getNamaUser() != 'parlemen_d' && $sf_user->getNamaUser() != 'wawali' && $sf_user->getNamaUser() != 'masger'): ?>
    <td align="right" style="background: #e8f3f1"><?php echo get_partial('persentasi', array('type' => 'list', 'master_kegiatan' => $master_kegiatan)); ?></td>
    <td style="background: #e8f3f1">
        <div id="<?php echo 'tempat_ajax2_' . str_replace('.', '_', $master_kegiatan->getKodeKegiatan()) ?>" align="right">
            <?php
            echo number_format($master_kegiatan->getTambahanPagu(), 0, ',', '.') . '<br>';
            ?>
        </div>
    </td>
    <td style="background: #e8f3f1">
        <div id="<?php echo 'tempat_ajax_' . str_replace('.', '_', $master_kegiatan->getKodeKegiatan()) ?>">
            <?php
            echo $master_kegiatan->getCatatan() . '<br/>';
            if ($sf_user->getNamaUser() != 'inspect' && $sf_user->getNamaUser() != 'peninjau' && $sf_user->getNamaUser() != 'dppk' && $sf_user->getNamaUser() != 'bppk' && $sf_user->getNamaUser() != 'wawali' && $sf_user->getNamaUser() != 'walikota' && $sf_user->getNamaUser() != 'sekda' && $sf_user->getNamaUser() != 'asisten' && $sf_user->getNamaUser() != 'masger' && $boleh==true) {
                echo link_to_function(image_tag('/images/edit_icon2.png'), 'showCatatan("edit","' . $master_kegiatan->getCatatan() . '","' . $master_kegiatan->getUnitId() . '","' . $master_kegiatan->getKodeKegiatan() . '","' . str_replace('.', '_', $master_kegiatan->getKodeKegiatan()) . '")');
            }
            ?>
        </div>
    </td> 
    <td style="background: #e8f3f1">
    <?php
    if ($jml > 0) {
        echo image_tag(sfConfig::get('sf_admin_web_dir') . '/images/tick.png');
    };
    ?></td>
<?php endif; ?>
<?php } else {
    ?>
    <td><?php echo $master_kegiatan->getKodeKegiatan(); ?></td>
    <td><?php echo $master_kegiatan->getNamakegiatan();
    echo ' <font color=#FF0000>(' . $master_kegiatan->getUserId() . ')</font><br/>';
    echo 'Kode: [ '.$master_kegiatan->getKegiatanId().' ]<br/>';
    if ($sf_user->getNamaUser() != 'bpk' && $master_kegiatan->getUserId() != '' && $sf_user->getNamaUser() != 'inspect' && $sf_user->getNamaUser() != 'peninjau' && $sf_user->getNamaUser() != 'dppk' && $sf_user->getNamaUser() != 'bppk' && $sf_user->getNamaUser() != 'wawali' && $sf_user->getNamaUser() != 'walikota' && $sf_user->getNamaUser() != 'sekda' && $sf_user->getNamaUser() != 'asisten' && $sf_user->getNamaUser() != 'parlemen2' && $sf_user->getNamaUser() != 'masger') {
        if (sfConfig::get('app_fasilitas_cekeProject') == 'buka') {
                $e = new Criteria;
                $e->add(RincianPeer::KEGIATAN_CODE, $master_kegiatan->getKodeKegiatan());
                $e->add(RincianPeer::UNIT_ID, $master_kegiatan->getUnitId());
                $x = RincianPeer::doSelectOne($e);
                if ($x) {
                    echo '<br/><strong>Sinkronisasi e-Project ( ' . date('j F Y h:i:s A', strtotime($x->getLastUpdateTime())) . ' )</strong>';
                }
        }
    }
    ?></td>
    <?php if (sfConfig::get('app_tahap_edit') == 'murni') { ?>
        <td align="right" >
            <?php echo number_format($master_kegiatan->getAlokasidana(), 0, ',', '.'); ?>
        </td>
        <?php } else if (sfConfig::get('app_tahap_edit') != 'murni') { ?>
        <td align="right" >
            <?php echo get_partial('nilaisemula', array('type' => 'list', 'master_kegiatan' => $master_kegiatan)) ?> 
        </td>
    <?php } ?>
    <td align="right"><?php echo get_partial('nilairincian', array('type' => 'list', 'master_kegiatan' => $master_kegiatan)) ?></td>
    <td align="right"><?php echo get_partial('selisih', array('type' => 'list', 'master_kegiatan' => $master_kegiatan)) ?></td>

    <?php if ($sf_user->getNamaUser() != 'bpk' && $sf_user->getNamaUser() != 'parlemen2' && $sf_user->getNamaUser() != 'parlemen_a' && $sf_user->getNamaUser() != 'parlemen_b' && $sf_user->getNamaUser() != 'parlemen_c' && $sf_user->getNamaUser() != 'parlemen_d' && $sf_user->getNamaUser() != 'wawali' && $sf_user->getNamaUser() != 'masger'): ?>
    <td align="right"><?php echo get_partial('persentasi', array('type' => 'list', 'master_kegiatan' => $master_kegiatan)); ?></td>
    <td>
        <div id="<?php echo 'tempat_ajax2_' . str_replace('.', '_', $master_kegiatan->getKodeKegiatan()) ?>" align="right">
            <?php
            echo number_format($master_kegiatan->getTambahanPagu(), 0, ',', '.');
            ?>
        </div>
    </td>
    <td>
        <div id="<?php echo 'tempat_ajax_' . str_replace('.', '_', $master_kegiatan->getKodeKegiatan()) ?>">
            <?php
            echo $master_kegiatan->getCatatan() . '<br/>';
            if ($sf_user->getNamaUser() != 'bpk' && $sf_user->getNamaUser() != 'inspect' && $sf_user->getNamaUser() != 'peninjau' && $sf_user->getNamaUser() != 'dppk' && $sf_user->getNamaUser() != 'bppk' && $sf_user->getNamaUser() != 'wawali' && $sf_user->getNamaUser() != 'walikota' && $sf_user->getNamaUser() != 'sekda' && $sf_user->getNamaUser() != 'asisten' && $sf_user->getNamaUser() != 'masger' && $boleh==true) {
                echo link_to_function(image_tag('/images/edit_icon2.png'), 'showCatatan("edit","' . $master_kegiatan->getCatatan() . '","' . $master_kegiatan->getUnitId() . '","' . $master_kegiatan->getKodeKegiatan() . '","' . str_replace('.', '_', $master_kegiatan->getKodeKegiatan()) . '")');
            }
            ?>
        </div>
    </td> 
    <td><?php echo get_partial('noteRincian', array('type' => 'list', 'master_kegiatan' => $master_kegiatan)); ?></td>
    <?php endif; ?>
<?php } ?>

<script>
    function showTambahanPagu(edit, pagu, unit, kegiatan, id) {
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/peneliti/tambahanPagu/edit/" + edit + "/tambahPagu/" + pagu + "/unit_id/" + unit + "/kode_kegiatan/" + kegiatan + ".html",
            context: document.body
        }).done(function (msg) {
            $('#tempat_ajax2_' + id).html(msg);
        });
    }

    function showCatatan(edit, catatan, unit, kegiatan, id) {
        if (catatan === '') {
            catatan = ' ';
        }

        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/peneliti/catatan/edit/" + edit + "/catatan/" + catatan + "/unit_id/" + unit + "/kode_kegiatan/" + kegiatan + ".html",
            context: document.body
        }).done(function (msg) {
            $('#tempat_ajax_' + id).html(msg);
        });
    }
</script>