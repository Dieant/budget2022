<?php echo form_tag('peneliti/simpanCatatan') ?>
<?php
echo input_hidden_tag('unit_id', $unit_id);
echo input_hidden_tag('kode_kegiatan', $kode_kegiatan);
echo input_hidden_tag('detail_no', $detail_no);
?>
<textarea name="catatan" placeholder="Isikan catatan" class="form-control" rows="3"><?php echo $catatan; ?></textarea>
<?php
echo submit_tag('Simpan', array('class' => 'btn btn-success btn-flat'));
echo '</form>';
?>