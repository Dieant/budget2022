<?php
    if (isset($filters['tahap']) && $filters['tahap'] == 'pakbp') {
        $tabel_semula = 'revisi6_';
        $tabel_dpn = 'pak_bukuputih_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'pakbb') {
        $tabel_dpn = 'pak_bukubiru_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'murni') {
        $tabel_semula='murni_bukubiru_';
        $tabel_dpn = 'murni_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'murnibp') {
        $tabel_semula='murni_bukuputih_';
        $tabel_dpn = 'murni_bukuputih_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'murnibb') {
        $tabel_semula='murni_bukuputih_';
        $tabel_dpn = 'murni_bukubiru_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'murnibbpraevagub') {
        $tabel_dpn = 'murni_bukubiru_praevagub_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi1') {
        $tabel_semula='murni_';
        $tabel_dpn = 'revisi1_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi1_1') {
        $tabel_dpn = 'revisi1_1_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi2') {
        $tabel_semula = 'revisi1_';
        $tabel_dpn = 'revisi2_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi3') {
        $tabel_semula = 'revisi2_';
        $tabel_dpn = 'revisi3_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi3_1') {
        $tabel_semula = 'revisi2_';
        $tabel_dpn = 'revisi3_1_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi4') {
        $tabel_semula = 'revisi3_';
        $tabel_dpn = 'revisi4_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi5') {
        $tabel_semula = 'revisi4_';
        $tabel_dpn = 'revisi5_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi6') {
        $tabel_semula = 'revisi5_';
        $tabel_dpn = 'revisi6_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi7') {
        $tabel_dpn = 'revisi7_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi8') {
        $tabel_dpn = 'revisi8_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi9') {
        $tabel_dpn = 'revisi9_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi10') {
        $tabel_dpn = 'revisi10_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'rkua') {
        $tabel_dpn = 'rkua_';
    } else {
        $tabel_semula='';
        $tabel_dpn = 'dinas_';
    }

$kode = $master_kegiatan->getKodeKegiatan();
$unit = $master_kegiatan->getUnitId();

$query = "SELECT * from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "rincian_detail 
where ((note_peneliti is not Null and note_peneliti <> '') or (note_skpd is not Null and note_skpd <> '')) and unit_id = '$unit' and kegiatan_code = '$kode' and status_hapus = false ";
$con = Propel::getConnection();
$statement = $con->prepareStatement($query);
$rs = $statement->executeQuery();
$jml = $rs->getRecordCount();

$query2 = "SELECT * from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "master_kegiatan 
where ((catatan is not Null and trim(catatan) <> '') or (catatan_pembahasan is not Null and trim(catatan_pembahasan) <> '')) and unit_id = '$unit' and kode_kegiatan = '$kode' and tahap='". $$filters['tahap'] ."'";
$statement2 = $con->prepareStatement($query2);
$rs = $statement2->executeQuery();
$jml2 = $rs->getRecordCount();

$query = "select max(status_level) as max "
. " from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "rincian_detail "
. " where unit_id='" . $master_kegiatan->getUnitId() . "' and kegiatan_code='" . $master_kegiatan->getKodeKegiatan() . "'";
$stmt = $con->prepareStatement($query);
$rs_max = $stmt->executeQuery();
if ($rs_max->next()) {
    $max = $rs_max->getInt('max');
    if ($max == 0 || $max == null) {
        $max = $master_kegiatan->getStatusLevel();
    }
}
$sisipan = '';
if ($max < 7 && $master_kegiatan->getIsPernahRka()) {
    $sisipan = 'S';
}

if ($jml > 0 or $jml2 > 0) {
    ?>
    <td style="background: #e8f3f1"><?php echo $master_kegiatan->getKodekegiatan(); ?></td>
    <td style="background: #e8f3f1"><?php
    $e = new Criteria;
    $e->add(DinasRincianPeer::KEGIATAN_CODE, $master_kegiatan->getKodeKegiatan());
    $e->add(DinasRincianPeer::UNIT_ID, $master_kegiatan->getUnitId());
    $x = DinasRincianPeer::doSelectOne($e);
    if ($x) {
        if ($x->getLock() == TRUE) {
            $gambar = (image_tag('gembok.gif', array('width' => '25', 'height' => '25')));
        } elseif ($x->getLock() == FALSE) {
            $gambar = '';
        }
    }
    echo $master_kegiatan->getNamakegiatan() . $gambar;
    echo ' <font color=#FF0000>(' . $master_kegiatan->getUserId() . ')</font><br/>';
    echo 'Kode: [ '.$master_kegiatan->getKegiatanId().' ]';
    ?>
    </td>
    <?php if (sfConfig::get('app_tahap_edit') == 'murni') { ?>
        <td align="right" style="background: #e8f3f1">
            <?php
            $query = "select alokasi from " . sfConfig::get('app_default_schema') . ".pagu where unit_id='$unit' and kode_kegiatan='$kode'";
            $stmt = $con->prepareStatement($query);
            $rs = $stmt->executeQuery();
            if ($rs->next()) {
                // $pagu_sekarang = $rs->getString('alokasi');
                // $tambahan_pagu = $master_kegiatan->getTambahanPagu();

                // $pagu_baru = intval($pagu_sekarang) + intval($tambahan_pagu);

                // echo number_format($pagu_baru, 0, ',', '.');
                echo number_format($rs->getString('alokasi'), 0, ',', '.');
            }
            ?>
        </td>
    <?php } else if (sfConfig::get('app_tahap_edit') != 'murni') { ?>
          
        <td align="right" style="background: #e8f3f1"><?php echo get_partial('nilaisemula', array('type' => 'list', 'master_kegiatan' => $master_kegiatan, 'filters' => $filters)) ?></td>
         <?php if (sfConfig::get('app_tahap_edit') != 'pak') { ?>
        <td align="right" style="background: #e8f3f1"><?php echo get_partial('nilaimenjadi', array('type' => 'list', 'master_kegiatan' => $master_kegiatan, 'filters' => $filters)) ?></td>
        <?php } ?>
        
<?php } ?>
<?php if (sfConfig::get('app_tahap_edit') == 'pak') { ?>
    <td align="right" style="background: #e8f3f1">
        <?php
        $c = new Criteria();
        $c->add(PaguPakPeer::UNIT_ID, $master_kegiatan->getUnitId());
        $c->add(PaguPakPeer::KEGIATAN_CODE, $master_kegiatan->getKodeKegiatan());
        $pagu_pak = PaguPakPeer::doSelectOne($c);
        echo number_format($pagu_pak->getPagu(), 0, ',', '.');
        ?>
    </td>
<?php } ?>
    <td align="right" style="background: #e8f3f1"><?php echo get_partial('nilairincian_revisi', array('type' => 'list', 'master_kegiatan' => $master_kegiatan, 'filters' => $filters)) ?></td>
    <td align="right" style="background: #e8f3f1"><?php echo get_partial('selisih_revisi', array('type' => 'list', 'master_kegiatan' => $master_kegiatan, 'filters' => $filters)) ?></td>
    <?php if (sfConfig::get('app_tahap_edit') == 'pak') { ?>
    <td align="right" style="background: #e8f3f1">
        <?php       
        echo number_format($master_kegiatan->getTambahanPagu(), 0, ',', '.');
        ?>
    </td>
    <?php } ?>
    <td align="right" style="background: #e8f3f1"><?php echo get_partial('posisi2', array('type' => 'list', 'master_kegiatan' => $master_kegiatan, 'filters' => $filters)) ?></td>
    <td align="right" style="background: #e8f3f1"><?php echo get_partial('posisi_revisi', array('type' => 'list', 'master_kegiatan' => $master_kegiatan, 'filters' => $filters)) ?></td>
    <td align="right" style="background: #e8f3f1"><b><?php
    $tabel_dpn = 'dinas_';
    if (isset($filters['tahap']) && $filters['tahap'] == 'pakbp') {
        echo 'PAK (Buku Putih)';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'pakbb') {
        echo 'PAK (Buku Biru)';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'murni') {
        echo 'Murni';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'murnibp') {
        echo 'Murni (Buku Putih)';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'murnibb') {
        echo 'Murni (Buku Biru)';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'murnibbpraevagub') {
        echo 'Murni (Buku Biru Pra Evaluasi Gubernur)';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi1') {
        echo 'Revisi 1';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi1_1') {
        echo 'Penyesuaian Komponen 1';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi2') {
        echo 'Revisi 2';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi2_1') {
        echo 'Penyesuaian Komponen 2';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi2_2') {
        echo 'Penyesuaian Komponen 3';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi3') {
        echo 'Revisi 3';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi3_1') {
        echo 'Penyesuaian Komponen 4';
    }  elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi4') {
        echo 'Revisi 4';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi5') {
        echo 'Revisi 5';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi6') {
        echo 'Revisi 6';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi7') {
        echo 'Revisi 7';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi8') {
        echo 'Revisi 8';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi9') {
        echo 'Revisi 9';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi10') {
        echo 'Revisi 10';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'rkua') {
        echo 'RKUA';
    } else {
        echo strtoupper($master_kegiatan->getTahap() . $sisipan);
    }

    if(isset($filters['tahap']) && !empty($filters['tahap'])) {
        $tabel_dpn = $filters['tahap'].'_';

        if($filters['tahap'] == 'pakbp')
            $tabel_dpn = 'pak_bukuputih_';
        else if($filters['tahap'] == 'pakbb')
            $tabel_dpn = 'pak_bukubiru_';
        else if($filters['tahap'] == 'murnibp')
            $tabel_dpn = 'murni_bukuputih_';
        else if($filters['tahap'] == 'murnibb')
            $tabel_dpn = 'murni_bukubiru_';
        else if($filters['tahap'] == 'murnibbpraevagub')
            $tabel_dpn = 'murni_bukubiru_praevagub_';
    }

    echo "<br>";
    $q = "select * "
    . " from " . sfConfig::get('app_default_schema') . ".master_kegiatan mk"
    . " join " . sfConfig::get('app_default_schema') . ".dinas_master_kegiatan dmk on dmk.kode_kegiatan = mk.kode_kegiatan and dmk.unit_id = mk.unit_id "
    . " where dmk.unit_id='" . $master_kegiatan->getUnitId() . "' and dmk.kode_kegiatan='" . $master_kegiatan->getKodeKegiatan() . "' and dmk.output <> mk.output ";
    $stmt = $con->prepareStatement($q);
    $rs_output = $stmt->executeQuery();
    if($rs_output->next()){
        echo "Terdapat Perubahan Output";
    }

    echo "<br>";
    $qf = "select * "
    . " from " . sfConfig::get('app_default_schema') . ".".$tabel_dpn."master_kegiatan dmk"
    . " where dmk.unit_id='" . $master_kegiatan->getUnitId() . "' and dmk.kode_kegiatan='" . $master_kegiatan->getKodeKegiatan() . "' and dmk.ubah_f1_dinas = true ";
    if($tabel_dpn != 'dinas_') {
        $qf .= " and dmk.tahap = '".substr($tabel_dpn, 0, strlen($tabel_dpn) - 1)."'";
    }
    $stmt = $con->prepareStatement($qf);
    $rs_f = $stmt->executeQuery();
    if($rs_f->next()){
        echo "Terdapat Perubahan F1";
    }
    ?></b>
    </td>
<?php } else {
    ?>
    <td><?php echo $master_kegiatan->getKodekegiatan();?></td>
    <td><?php
    $e = new Criteria;
    $e->add(DinasRincianPeer::KEGIATAN_CODE, $master_kegiatan->getKodeKegiatan());
    $e->add(DinasRincianPeer::UNIT_ID, $master_kegiatan->getUnitId());
    $x = DinasRincianPeer::doSelectOne($e);
    if ($x) {
        if ($x->getLock() == TRUE) {
            $gambar = (image_tag('gembok.gif', array('width' => '25', 'height' => '25')));
        } elseif ($x->getLock() == FALSE) {
            $gambar = '';
        }
    }
    echo $master_kegiatan->getNamakegiatan() . $gambar;
    echo ' <font color=#FF0000>(' . $master_kegiatan->getUserId() . ')</font><br/>';
    echo 'Kode: [ '.$master_kegiatan->getKegiatanId().' ]';
    ?>
    </td>

    <?php if (sfConfig::get('app_tahap_edit') == 'murni') { ?>
        <td align="right" >
            <?php
            $query = "select alokasi from " . sfConfig::get('app_default_schema') . ".pagu where unit_id='$unit' and kode_kegiatan='$kode'";
            $stmt = $con->prepareStatement($query);
            $rs = $stmt->executeQuery();
            if ($rs->next()) {
                // $pagu_sekarang = $rs->getString('alokasi');
                // $tambahan_pagu = $master_kegiatan->getTambahanPagu();

                // $pagu_baru = intval($pagu_sekarang) + intval($tambahan_pagu);

                // echo number_format($pagu_baru, 0, ',', '.');

                echo number_format($rs->getString('alokasi'), 0, ',', '.');
            }
            ?>
        </td>
        <!-- <td align="right" >
        <?php // echo number_format($master_kegiatan->getAlokasidana(), 0, ',', '.'); ?>
        </td> -->
    <?php } else if (sfConfig::get('app_tahap_edit') != 'murni') { ?>    
        <td align="right"><?php echo get_partial('nilaisemula', array('type' => 'list', 'master_kegiatan' => $master_kegiatan, 'filters' => $filters)) ?></td>
        <?php if (sfConfig::get('app_tahap_edit') != 'pak') { ?>
        <td align="right"><?php echo get_partial('nilaimenjadi', array('type' => 'list', 'master_kegiatan' => $master_kegiatan, 'filters' => $filters)) ?></td>  
        <?php } ?>

    <?php } ?>
    <?php if (sfConfig::get('app_tahap_edit') == 'pak') { ?>
    <td align="right">
        <?php
        $c = new Criteria();
        $c->add(PaguPakPeer::UNIT_ID, $master_kegiatan->getUnitId());
        $c->add(PaguPakPeer::KEGIATAN_CODE, $master_kegiatan->getKodeKegiatan());
        $pagu_pak = PaguPakPeer::doSelectOne($c);
        echo number_format($pagu_pak->getPagu(), 0, ',', '.');
        ?>
    </td>
    <?php } ?>
    <td align="right"><?php echo get_partial('nilairincian_revisi', array('type' => 'list', 'master_kegiatan' => $master_kegiatan, 'filters' => $filters)) ?></td>
    <td align="right"><?php echo get_partial('selisih_revisi', array('type' => 'list', 'master_kegiatan' => $master_kegiatan, 'filters' => $filters)) ?></td>
    <?php if (sfConfig::get('app_tahap_edit') != 'murni') { ?>
        <?php if (sfConfig::get('app_tahap_edit') == 'pak') { ?>
   <td align="right">
        <?php       
        echo number_format($master_kegiatan->getTambahanPagu(), 0, ',', '.');
        ?>
    </td>
    <?php } ?>
    <?php } ?>
    <td align="right"><?php echo get_partial('posisi2', array('type' => 'list', 'master_kegiatan' => $master_kegiatan, 'filters' => $filters)) ?></td>
    <td align="right"><?php echo get_partial('posisi_revisi', array('type' => 'list', 'master_kegiatan' => $master_kegiatan, 'filters' => $filters)) ?></td>
    <td align="right"><b><?php
    if (isset($filters['tahap']) && $filters['tahap'] == 'pakbp') {
        echo 'PAK (Buku Putih)';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'pakbb') {
        echo 'PAK (Buku Biru)';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'murni') {
        echo 'Murni';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'murnibp') {
        echo 'Murni (Buku Putih)';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'murnibb') {
        echo 'Murni (Buku Biru)';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'murnibbpraevagub') {
        echo 'Murni (Buku Biru Pra Evaluasi Gubernur)';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi1') {
        echo 'Revisi 1';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi1_1') {
        echo 'Penyesuaian Komponen 1';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi2') {
        echo 'Revisi 2';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi2_1') {
        echo 'Penyesuaian Komponen 2';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi2_2') {
        echo 'Penyesuaian Komponen 3';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi3') {
        echo 'Revisi 3';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi3_1') {
        echo 'Penyesuaian Komponen 4';
    }  elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi4') {
        echo 'Revisi 4';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi5') {
        echo 'Revisi 5';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi6') {
        echo 'Revisi 6';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi7') {
        echo 'Revisi 7';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi8') {
        echo 'Revisi 8';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi9') {
        echo 'Revisi 9';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi10') {
        echo 'Revisi 10';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'rkua') {
        echo 'RKUA';
    } else {
        echo strtoupper($master_kegiatan->getTahap() . $sisipan);
    }

    echo "<br><br>";
    $q = "select * "
    . " from " . sfConfig::get('app_default_schema') . ".master_kegiatan mk"
    . " join " . sfConfig::get('app_default_schema') . ".dinas_master_kegiatan dmk on dmk.kode_kegiatan = mk.kode_kegiatan and dmk.unit_id = mk.unit_id "
    . " where dmk.unit_id='" . $master_kegiatan->getUnitId() . "' and dmk.kode_kegiatan='" . $master_kegiatan->getKodeKegiatan() . "' and dmk.output <> mk.output ";
    $stmt = $con->prepareStatement($q);
    $rs_output = $stmt->executeQuery();
    if($rs_output->next()){
        echo "Terdapat Perubahan Output";
    }

    echo "<br>";
    $qf = "select * "
    . " from " . sfConfig::get('app_default_schema') . ".dinas_master_kegiatan dmk"
    . " where dmk.unit_id='" . $master_kegiatan->getUnitId() . "' and dmk.kode_kegiatan='" . $master_kegiatan->getKodeKegiatan() . "' and dmk.ubah_f1_dinas = true ";
    $stmt = $con->prepareStatement($qf);
    $rs_f = $stmt->executeQuery();
    if($rs_f->next()){
        echo "Terdapat Perubahan F1";
    }
    ?></b></td>
<?php } ?>
<!--fungsine iki--> 
<!--tombol e sing image pagu tambahan + catatan-->
<script>
    function showTambahanPagu(edit, pagu, unit, kegiatan, id) {
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/peneliti/tambahanPaguRevisi/edit/" + edit + "/tambahPagu/" + pagu + "/unit_id/" + unit + "/kode_kegiatan/" + kegiatan + ".html",
            context: document.body
        }).done(function (msg) {
            $('#tempat_ajax2_' + id).html(msg);
        });
    }
</script>