unoloc<?php use_helper('Url', 'Javascript', 'Form', 'Object'); ?>
<?php
if ($tahap == 'pakbp') {
    $tabel_dpn = 'pak_bukuputih_';
    $status = 'LOCK';
    $kunci_admin = 'LOCK';
} elseif ($tahap == 'pakbb') {
    $tabel_dpn = 'pak_bukubiru_';
    $status = 'LOCK';
    $kunci_admin = 'LOCK';
} elseif ($tahap == 'murni') {
    $tabel_dpn = 'murni_';
    $status = 'LOCK';
    $kunci_admin = 'LOCK';
} elseif ($tahap == 'murnibp') {
    $tabel_dpn = 'murni_bukuputih_';
    $status = 'LOCK';
    $kunci_admin = 'LOCK';
} elseif ($tahap == 'murnibb') {
    $tabel_dpn = 'murni_bukubiru_';
    $status = 'LOCK';
    $kunci_admin = 'LOCK';
} elseif ($tahap == 'revisi1') {
    $tabel_dpn = 'revisi1_';
    $status = 'LOCK';
    $kunci_admin = 'LOCK';
} elseif ($tahap == 'revisi1_1') {
    $tabel_dpn = 'revisi1_1_';
    $status = 'LOCK';
    $kunci_admin = 'LOCK';
} elseif ($tahap == 'revisi2') {
    $tabel_dpn = 'revisi2_';
    $status = 'LOCK';
    $kunci_admin = 'LOCK';
} elseif ($tahap == 'revisi2_1') {
    $tabel_dpn = 'revisi2_1_';
    $status = 'LOCK';
    $kunci_admin = 'LOCK';
}  elseif ($tahap == 'revisi2_2') {
    $tabel_dpn = 'revisi2_2_';
    $status = 'LOCK';
    $kunci_admin = 'LOCK';
} elseif ($tahap == 'revisi3') {
    $tabel_dpn = 'revisi3_';
    $status = 'LOCK';
    $kunci_admin = 'LOCK';
} elseif ($tahap == 'revisi3_1') {
    $tabel_dpn = 'revisi3_1_';
    $status = 'LOCK';
    $kunci_admin = 'LOCK';
} elseif ($tahap == 'revisi4') {
    $tabel_dpn = 'revisi4_';
    $status = 'LOCK';
    $kunci_admin = 'LOCK';
} elseif ($tahap == 'revisi5') {
    $tabel_dpn = 'revisi5_';
    $status = 'LOCK';
    $kunci_admin = 'LOCK';
} elseif ($tahap == 'revisi6') {
    $tabel_dpn = 'revisi6_';
    $status = 'LOCK';
    $kunci_admin = 'LOCK';
} elseif ($tahap == 'revisi7') {
    $tabel_dpn = 'revisi7_';
    $status = 'LOCK';
    $kunci_admin = 'LOCK';
} elseif ($tahap == 'revisi8') {
    $tabel_dpn = 'revisi8_';
    $status = 'LOCK';
    $kunci_admin = 'LOCK';
} elseif ($tahap == 'revisi9') {
    $tabel_dpn = 'revisi9_';
    $status = 'LOCK';
    $kunci_admin = 'LOCK';
} elseif ($tahap == 'revisi10') {
    $tabel_dpn = 'revisi10_';
    $status = 'LOCK';
    $kunci_admin = 'LOCK';
} elseif ($tahap == 'rkua') {
    $tabel_dpn = 'rkua_';
    $status = 'LOCK';
    $kunci_admin = 'LOCK';
} else 
{
    $tabel_dpn = 'dinas_';
    $status = $sf_user->getAttribute('status', '', 'status_admin');
    $c = new Criteria();
    $c->add(DinasRincianPeer::KEGIATAN_CODE, $kode_kegiatan);
    $c->add(DinasRincianPeer::UNIT_ID, $unit_id);
    $v = DinasRincianPeer::doSelectOne($c);
    if ($v) {
        if ($v->getLock() == TRUE) {
            $kunci_admin = 'LOCK';
        } else {
            $kunci_admin = 'OPEN';
        }
    }
    $i = 0;
    $kode_sub = '';
    $temp_rekening = '';
    $c = new Criteria();
    $c->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
    $c->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
    $c->add(DinasRincianDetailPeer::STATUS_LEVEL, 4, Criteria::GREATER_THAN);
    if (DinasRincianDetailPeer::doSelectOne($c))
        $status_kunci = FALSE;
    else
        $status_kunci = TRUE;

    if (sfConfig::get('app_fasilitas_cekeDelivery') == 'buka') 
    {
        $arr_kode_detail_kegiatan_lelang = array();
        $query = "(
        select kode_detail_kegiatan ,sum(nilai)
        from (
        select k.id,dk.kode_detail_kegiatan, coalesce((mc.harga_realisasi_bulat + mc.lk + mc.biaya_lain), (dkom.harga_realisasi_bulat + dkom.lk + dkom.biaya_lain)) as nilai
        from " . sfConfig::get('app_default_edelivery') . ".detail_komponen dkom
        left join " . sfConfig::get('app_default_edelivery') . ".det_kom_mc mc on mc.id = dkom.id
        join " . sfConfig::get('app_default_edelivery') . ".kontraks k on k.id = dkom.kontrak_id
        join " . sfConfig::get('app_default_eproject') . ".detail_kegiatan dk on dk.id = dkom.detail_kegiatan_id
        join (select k.id, pk.termin_ke
        from " . sfConfig::get('app_default_edelivery') . ".kontraks k 
        join " . sfConfig::get('app_default_eproject') . ".pekerjaan p on p.id = k.pekerjaan_id
        join " . sfConfig::get('app_default_eproject') . ".detail_pekerjaan dp on dp.pekerjaan_id = p.id
        join " . sfConfig::get('app_default_eproject') . ".detail_kegiatan dk on dk.id = dp.detail_kegiatan_id
        left join " . sfConfig::get('app_default_edelivery') . ".pemutusan_kontrak pk on pk.kontrak_id = k.id
        where k.metode in (14,15,16) and k.status=110 and dk.kode_detail_kegiatan like '$unit_id.$kegiatan_code.%'
        group by k.id, pk.termin_ke) a ON k.id = a.id and dkom.termin_ke <= a.termin_ke
        where k.metode in (14,15,16) and k.status=110 and dk.kode_detail_kegiatan like '$unit_id.$kegiatan_code.%'
        ) komponen
        group by kode_detail_kegiatan
        )
        union
        (
        select kode_detail_kegiatan, sum(nilai)
        from (
        select k.id, dk.kode_detail_kegiatan, coalesce((mc.harga_realisasi_bulat + mc.lk + mc.biaya_lain), (dkom.harga_realisasi_bulat + dkom.lk + dkom.biaya_lain)) as nilai
        from " . sfConfig::get('app_default_edelivery') . ".detail_komponen dkom
        left join " . sfConfig::get('app_default_edelivery') . ".det_kom_mc mc on mc.id = dkom.id
        join " . sfConfig::get('app_default_edelivery') . ".kontraks k on k.id = dkom.kontrak_id
        join " . sfConfig::get('app_default_eproject') . ".detail_kegiatan dk on dk.id = dkom.detail_kegiatan_id
        where k.metode in (14,15,16) and k.status<>110 and dk.kode_detail_kegiatan like '$unit_id.$kegiatan_code.%'
        ) komponen
        group by kode_detail_kegiatan
        )";
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query);
        $rs = $stmt->executeQuery();
        while ($rs->next()) {
            $arr_kode_detail_kegiatan_lelang[] = $rs->getString('kode_detail_kegiatan');
        }
    }
}
foreach ($rs_rd as $rd):
    $c = new Criteria();
    $c->add(KomponenPeer::KOMPONEN_ID, $rd->getKomponenId());
    if ($rs_est_fisik = KomponenPeer::doSelectOne($c))
        $est_fisik = $rs_est_fisik->getIsEstFisik();

    $odd = fmod($i++, 2);
    $unit_id = $rd->getUnitId();
    $kegiatan_code = $rd->getKegiatanCode();

    if ($kode_sub != $rd->getKodeSub()) 
    {
        $kode_sub = $rd->getKodeSub();
        $sub = $rd->getSub();
        $cekKodeSub = substr($kode_sub, 0, 4);

        if ($cekKodeSub == 'RKAM') 
        {//RKA Member
            $C_RKA = new Criteria();
            $C_RKA->add(DinasRkaMemberPeer::KODE_SUB, $kode_sub);
            $C_RKA->addAscendingOrderByColumn(DinasRkaMemberPeer::KODE_SUB);
            $rs_rkam = DinasRkaMemberPeer::doSelectOne($C_RKA);
            if ($rs_rkam) 
            {
                ?>
                <tr class="pekerjaans_<?php echo $id ?>" bgcolor="#e6e6e6">
                    <td colspan="7"><?php
                    if (($status == 'OPEN') and ( $rd->getLockSubtitle() <> 'LOCK' and substr($sf_user->getNamaLogin(), 0, 3) <> 'pa_')) {
                        echo link_to_function('<i class="fa fa-edit"></i>', 'editHeaderKegiatan(' . $id . ',"' . $kegiatan_code . '","' . $unit_id . '","' . $kode_sub . '")', array('class' => 'btn btn-default btn-flat btn-sm'));
                        echo link_to_function('<i class="fa fa-ban"></i>', 'hapusHeaderKegiatan(' . $id . ',"' . $kegiatan_code . '","' . $unit_id . '","' . $kode_sub . '")', array('class' => 'btn btn-default btn-flat btn-sm'));
                    }
                        ?>
                        <div id="<?php echo 'header_' . $rs_rkam->getKodeSub() ?>"><b> .:. <?php echo $rs_rkam->getKomponenName() . ' ' . $rs_rkam->getDetailName(); ?></b></div>
                    </td>
                    <td align="right">
                        <?php
                        echo number_format($rd->getTotalKodeSub($kode_sub), 0, ',', '.');
                        ?>
                    </td>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <?php
            }
        } 
        else 
        { //else form SUB
            $c = new Criteria();
            $c->add(DinasRincianSubParameterPeer::KODE_SUB, $kode_sub);
            $rs_subparameter = DinasRincianSubParameterPeer::doSelectOne($c);
            if ($rs_subparameter) 
            {
                ?>
                <tr class="pekerjaans_<?php echo $id ?>" bgcolor="#e6e6e6">
                    <td colspan="7">
                        <b> :. <?php echo $rs_subparameter->getSubKegiatanName() . ' ' . $rs_subparameter->getDetailName(); ?></b>
                    </td>
                    <td align="right">
                        <?php echo number_format($rd->getTotalKodeSub($kode_sub), 0, ',', '.'); ?>
                    </td>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <?php
            } else {
                $ada = 'tidak';
                $query = "select * from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "rincian_sub_parameter "
                . "where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and new_subtitle ilike '%$sub%'";
                $con = Propel::getConnection();
                $stmt = $con->prepareStatement($query);
                $t = $stmt->executeQuery();
                while ($t->next()) 
                {
                    if ($t->getString('kode_sub')) 
                    {
                        ?>
                        <tr class="pekerjaans_<?php echo $id ?>" bgcolor="#e6e6e6">
                            <td colspan="7">
                                <b> :. <?php echo $t->getString('sub_kegiatan_name') . ' ' . $t->getString('detail_name'); ?></b>
                            </td>
                            <td align="right">
                                <?php
                                echo number_format($rd->getTotalSub($sub), 0, ',', '.');
                                $ada = 'ada';
                                ?> 
                            </td>
                            <td colspan="3">&nbsp;</td>
                         </tr>
                            <?php
                    }
                }

                if ($ada == 'tidak') 
                {
                    if ($kode_sub != '') 
                    {
                        $query = "select * from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "rincian_detail "
                        . "where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and sub = '$sub' and status_hapus=false";
                        $con = Propel::getConnection();
                        $stmt = $con->prepareStatement($query);
                        $t = $stmt->executeQuery();
                        while ($t->next()) 
                        {
                            if ($t->getString('kode_sub')) 
                            {
                                ?>
                                <tr class="pekerjaans_<?php echo $id ?>" bgcolor="#e6e6e6">
                                    <td colspan="7"><b> :. <?php echo $t->getString('komponen_name') . ' ' . $t->getString('detail_name'); ?></b></td>
                                    <td align="right">
                                        <?php
                                        echo number_format($rd->getTotalSub($sub), 0, ',', '.');
                                        $ada = 'ada';
                                        ?>
                                    </td>
                                    <td colspan="3">&nbsp;</td>
                                </tr>
                            <?php
                            }
                        }
                    }
                }
            }
        }
    }

    $rekening_code = $rd->getRekeningCode();
    if ($temp_rekening != $rekening_code) {
        $temp_rekening = $rekening_code;
        $c = new Criteria();
        $c->add(RekeningPeer::REKENING_CODE, $rekening_code);
        $rs_rekening = RekeningPeer::doSelectOne($c);
        if ($rs_rekening) {
            $rekening_name = $rs_rekening->getRekeningName();
            ?>
            <tr class="pekerjaans_<?php echo $id ?>" bgcolor="#e6e6e6">
                <td colspan="7"><i><?php echo $rekening_code . ' ' . $rekening_name ?></i> </td>
                <td colspan="3">&nbsp;</td>
            </tr>
            <?php
        }
    }
    ?>
    <tr class="pekerjaans_<?php echo $id ?>">
        <td>
            <?php
            if ($tahap == '') 
            {
                $kegiatan = $rd->getKegiatanCode();
                $unit = $rd->getUnitId();
                $no = $rd->getDetailNo();
                $sub = $rd->getSubtitle();
                $komponen_id = $rd->getKomponenId();
                if ($status_kunci) {
                    if (sfConfig::get('app_fasilitas_gantiRekening4Peneliti') == 'buka') 
                    {
                        $query = "(select distinct rekening_code "
                        . "from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "rincian_detail "
                        . "where unit_id='$unit' and kegiatan_code='$kegiatan' and status_hapus=false "
                        . "order by rekening_code)"
                        . " union all "
                        . "(select distinct rekening_code from " . sfConfig::get('app_default_schema') . ".rekening "
                        . "where rekening_code in ('5.2.2.01.14','5.2.2.01.23') order by rekening_code) ";

                        if ($rd->getTipe2() == 'ATRIBUSI' || $rd->getTipe2() == 'PERENCANAAN' || $rd->getTipe2() == 'PENGAWASAN') {
                            $query = "select distinct rekening_code "
                            . "from " . sfConfig::get('app_default_schema') . ".rekening "
                            . "where rekening_code like '5.2.3%' "
                            . "order by rekening_code";
                        } elseif ($rd->getUnitId() == '2600') {
                            $query .= " union all
                            (select distinct rekening_code from " . sfConfig::get('app_default_schema') . ".rekening 
                            where rekening_code like '5.2.3%' )
                            order by rekening_code";
                        }
                        //--list rekening diambil dari ssh itu punya rekening apa saja
                        $con = Propel::getConnection();
                        $stmt = $con->prepareStatement($query);
                        $ts = $stmt->executeQuery();
                        $pilih = array();
                        $ada = false;
                        $select_str = "<select name='rek_$no'>";
                        while ($ts->next()) {
                            $ada = true;
                            $r = $ts->getString('rekening_code');
                            $select_str.="<option value='$r'>$r</option>";
                        }
                        $select_str.="</select>";
                    }

                    $kegiatan = $rd->getKegiatanCode();
                    $unit = $rd->getUnitId();
                    $no = $rd->getDetailNo();
                    $sub = $rd->getSubtitle();
                    $benar_musrenbang = 0;
                    if ($rd->getIsMusrenbang() == 'TRUE') {
                        $benar_musrenbang = 1;
                    }
                    $benar_prioritas = 0;
                    if ($rd->getPrioritasWali() == 'TRUE') {
                        $benar_prioritas = 1;
                    }
                    $benar_output = 0;
                    if ($rd->getIsOutput() == 'TRUE') {
                        $benar_output = 1;
                    }
                    $benar_multiyears = 0;
                    if ($rd->getThKeMultiyears() <> null && $rd->getThKeMultiyears() > 0) {
                        $benar_multiyears = 1;
                    }
                    $benar_hibah = 0;
                    if ($rd->getIsHibah() == 'TRUE') {
                        $benar_hibah = 1;
                    }
                    $status_kunci_baru = FALSE;
                    $array_skpd = array('9999', '2300', '2600');

                    $array_rekening_dikunci = array('5.2.2.02.01', '5.2.2.20.11');
                    if (in_array($rd->getRekeningCode(), $array_rekening_dikunci) && !(in_array($rd->getUnitId(), $array_skpd))) {
                        $status_kunci_baru = TRUE;
                    }
                    if ($kunci_admin == 'OPEN' &&  $status_kunci_baru =='TRUE' && !$status_kunci_baru && !(($rd->getSubtitle() == 'Penunjang Kinerja' || $rd->getKomponenId() == '2.1.1.01.01.01.001.010' || $rd->getKomponenId() == '2.1.1.01.01.01.001.011')) ) {
                        ?>
                        <div class="btn-group">
                            <?php
                            echo link_to('<i class="fa fa-edit"></i> Edit', 'peneliti/editKegiatanRevisi?id=' . $rd->getDetailNo() . '&unit=' . $rd->getUnitId() . '&kegiatan=' . $rd->getKegiatanCode() . '&edit=' . md5('ubah'), array('class' => 'btn btn-default btn-flat btn-sm'));
                            if (sfConfig::get('app_tahap_edit') == 'murni' && $benar_hibah == 0 && $benar_musrenbang == 0 && $benar_multiyears == 0 && substr($sf_user->getNamaLogin(), 0, 3) <> 'pa_') {
                                ?>
                                <button type="button" class="btn btn-default dropdown-toggle btn-flat btn-sm" data-toggle="dropdown">
                                    <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <div class="dropdown-menu" role="menu">
                                    <?php
                                        echo link_to('<i class="fa fa-trash"></i> Hapus', 'peneliti/hapusPekerjaansRevisi?id=' . $id . '&no=' . $rd->getDetailNo() . '&unit=' . $rd->getUnitId() . '&kegiatan=' . $rd->getKegiatanCode(), Array('confirm' => 'Yakin untuk menghapus Komponen ' . $rd->getKomponenName() . ' ' . $rd->getDetailName() . ' ?', 'class' => 'dropdown-item'));
                                    ?>
                                </div>
                                <?php
                            }
                            ?>
                        </div>
                        <div class="clearfix"></div>
                        <?php
                    }
                    echo link_to_function('<i class="fas fa-pencil-ruler"></i> Buat Catatan', 'execBuatNote("' . $rd->getDetailNo() . '","' . $unit_id . '","' . $rd->getKegiatanCode() . '", "' . str_replace('.', '_', $kegiatan_code) . '_' . $rd->getDetailNo() . '")', array('class' => 'btn btn-default btn-flat btn-sm', 'disable' => true));
                    //irul 25agustus 2014 - fungsi GMAP
                    if ($rd->getTipe2() == 'PERENCANAAN' || $rd->getTipe2() == 'PENGAWASAN' || $rd->getTipe2() == 'KONSTRUKSI' || $rd->getTipe2() == 'TANAH' || $rd->getTipe() == 'FISIK' || $est_fisik) 
                    {
                        $con = Propel::getConnection();
                        $kode_rka = $unit_id . '.' . $kegiatan_code . '.' . $rd->getDetailNo();
                        $query_cek_waiting = "select id_waiting from " . sfConfig::get('app_default_schema') . ".waitinglist_pu where kode_rka = '" . $kode_rka . "' ";
                        $stmt_cek_waiting = $con->prepareStatement($query_cek_waiting);
                        $rs_cek_waiting = $stmt_cek_waiting->executeQuery();
                        if ($rs_cek_waiting->next()) {
                            ?>
                            <div id="tempat_ajax_<?php echo str_replace('.', '_', $kegiatan_code) . '_' . $rd->getDetailNo() ?>">
                                <?php echo link_to_function('<i class="fa fa-trash"></i> Kembalikan ke Waiting List', 'execKembalikanWaitingRevisi("' . $rd->getDetailNo() . '","' . $rd->getUnitId() . '","' . $rd->getKegiatanCode() . '", "' . str_replace('.', '_', $kegiatan_code) . '_' . $rd->getDetailNo() . '")', array('class' => 'btn btn-default btn-flat btn-sm')); ?>
                            </div>
                        <?php
                        }
                        $id_kelompok = 0;
                        $tot = 0;
                        $query = "select count(*) as tot "
                        . "from " . sfConfig::get('app_default_gis') . ".geojsonlokasi_rev1 "
                        . "where unit_id='" . $rd->getUnitId() . "' and kegiatan_code='" . $rd->getKegiatanCode() . "' and detail_no ='" . $rd->getDetailNo() . "' "
                        . "and tahun = '" . sfConfig::get('app_tahun_default') . "' and status_hapus = FALSE";
                        $stmt = $con->prepareStatement($query);
                        $rs = $stmt->executeQuery();
                        while ($rs->next()) {
                            $tot = $rs->getString('tot');
                        }
                        if ($tot == 0) 
                        {
                            $con = Propel::getConnection();
                            $c2 = new Criteria();
                            $crit1 = $c2->getNewCriterion(KomponenPeer::KOMPONEN_TIPE, 'FISIK');
                            $crit1->addOr($c2->getNewCriterion(KomponenPeer::KOMPONEN_TIPE, 'EST'));
                            $crit1->addOr($c2->getNewCriterion(KomponenPeer::KOMPONEN_TIPE2, 'KONSTRUKSI'));
                            $crit1->addOr($c2->getNewCriterion(KomponenPeer::KOMPONEN_TIPE2, 'TANAH'));
                            $crit1->addOr($c2->getNewCriterion(KomponenPeer::KOMPONEN_TIPE2, 'PENGAWASAN'));
                            $crit1->addOr($c2->getNewCriterion(KomponenPeer::KOMPONEN_TIPE2, 'PERENCANAAN'));
                            $c2->add($c2->getNewCriterion(KomponenPeer::KOMPONEN_NAME, $rd->getKomponenName(), Criteria::ILIKE));
                            $c2->addAnd($crit1);
                            $rd2 = KomponenPeer::doSelectOne($c2);
                            if ($rd2) {
                                $komponen_id = $rd2->getKomponenId();
                                $satuan = $rd2->getSatuan();
                            } else {
                                $komponen_id = '0';
                                $satuan = '';
                            }

                            if ($komponen_id == '0') {
                                $query2 = "select * from master_kelompok_gmap "
                                . "where '" . $rd->getKomponenName() . "' ilike nama_objek||'%'";
                            } else {
                                $query2 = "select * from master_kelompok_gmap "
                                . "where '" . $komponen_id . "' ilike kode_kelompok||'%'";
                            }
                            $stmt2 = $con->prepareStatement($query2);
                            $rs2 = $stmt2->executeQuery();
                            while ($rs2->next()) {
                                $id_kelompok = $rs2->getString('id_kelompok');
                            }

                            if ($id_kelompok == '' || $id_kelompok == 0 || $id_kelompok == null) {
                                $id_kelompok = 19;
                                if (in_array($satuan, array('Kegiatan', 'Lokasi', 'M2', 'M²', 'm3', 'Paket', 'Set'))) {
                                    $id_kelompok = 100;
                                } elseif (in_array($satuan, array('m', 'M', 'M1', 'Meter', 'Titik', 'Unit'))) {
                                    $id_kelompok = 101;
                                }
                            }
                        } 
                        else 
                        {
                            $con = Propel::getConnection();
                            $query = "select * from " . sfConfig::get('app_default_gis') . ".geojsonlokasi_rev1 
                            where unit_id='" . $rd->getUnitId() . "' and kegiatan_code='" . $rd->getKegiatanCode() . "' and detail_no ='" . $rd->getDetailNo() . "' and tahun = '" . sfConfig::get('app_tahun_default') . "'";
                            $stmt = $con->prepareStatement($query);
                            $rs = $stmt->executeQuery();
                            while ($rs->next()) {
                                $mlokasi = $rs->getString('mlokasi');
                                $id_kelompok = $rs->getString('id_kelompok');
                            }

                            $query = "select max(lokasi_ke) as total_lokasi from " . sfConfig::get('app_default_gis') . ".geojsonlokasi_rev1 
                            where unit_id='" . $rd->getUnitId() . "' and kegiatan_code='" . $rd->getKegiatanCode() . "' and detail_no ='" . $rd->getDetailNo() . "' and tahun = '" . sfConfig::get('app_tahun_default') . "'";
                            $stmt = $con->prepareStatement($query);
                            $rs = $stmt->executeQuery();
                            while ($rs->next()) {
                                $total_lokasi = $rs->getString('total_lokasi');
                            }
                        }
                        ?>
                        <div class="btn-group">
                            <?php
                            echo link_to_function('<i class="fa fa-map-marker"></i>', '', array('class' => 'btn btn-default btn-flat btn-sm', 'disable' => true));
                            if ($tot == 0) {
                                $array_bersih_kurung = array('(', ')');
                                $lokasi_bersih_kurung = str_replace($array_bersih_kurung, '', $lokasi);
                                echo link_to('<i class="fa fa-edit"></i> Input Lokasi', sfConfig::get('app_path_gmap') . 'insertBaru_revisi.php?unit_id=' . $rd->getUnitId() . '&kode_kegiatan=' . $rd->getKegiatanCode() . '&detail_no=' . $rd->getDetailNo() . '&satuan=' . $rd->getSatuan() . '&volume=' . $rd->getVolume() . '&nilai_anggaran=' . $rd->getNilaiAnggaran() . '&tahun=' . sfConfig::get('app_tahun_default') . '&mlokasi=&id_kelompok=' . $id_kelompok . '&th_load=0&level=1&nm_user=' . $sf_user->getNamaLogin() . '&lokasi_ke=1', array('class' => 'btn btn-default btn-flat btn-sm'));
                            } else {
                                echo link_to('<i class="fa fa-edit"></i> Edit Lokasi', sfConfig::get('app_path_gmap') . 'updateData_revisi.php?unit_id=' . $rd->getUnitId() . '&kode_kegiatan=' . $rd->getKegiatanCode() . '&detail_no=' . $rd->getDetailNo() . '&satuan=' . $rd->getSatuan() . '&volume=' . $rd->getVolume() . '&nilai_anggaran=' . $rd->getNilaiAnggaran() . '&tahun=' . sfConfig::get('app_tahun_default') . '&mlokasi=' . $mlokasi . '&id_kelompok=' . $id_kelompok . '&th_load=0&level=1&nm_user=' . $sf_user->getNamaLogin() . '&total_lokasi=' . $total_lokasi . '&lokasi_ke=1', array('class' => 'btn btn-default btn-flat btn-sm'));
                                ?>
                                <button type="button" class="btn btn-default dropdown-toggle btn-flat btn-sm" data-toggle="dropdown">
                                    <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <div class="dropdown-menu" role="menu">
                                    <?php
                                    echo link_to('<i class="fa fa-trash"></i> Hapus Lokasi', 'peneliti/hapusLokasi?id=' . $id . '&no=' . $rd->getDetailNo() . '&unit=' . $rd->getUnitId() . '&kegiatan=' . $rd->getKegiatanCode(), Array('confirm' => 'Yakin untuk menghapus Lokasi untuk Komponen ' . $rd->getKomponenName() . ' ' . $rd->getDetailName() . ' ?', 'class' => 'dropdown-item'));
                                    ?>
                                </div>
                            <?php
                            }
                            ?>
                        </div>
                    <?php
                    }
                }
            }
            ?>
        </td>
        <?php
        if ($rd->getNotePeneliti() != '' && $rd->getNotePeneliti() != NULL && $rd->getStatusHapus() == false) {
            $warna = 'lightyellow';
        } elseif ($rd->getNoteSkpd() != '' and $rd->getNoteSkpd() != NULL and $rd->getStatusHapus() == false) {
            $warna = '#e8f3f1';
        } else {
            $warna = 'none';
        }
        ?>
        <td style="background: <?php echo $warna ?>">
            <?php
            if ($rd->getIsMusrenbang() == 'TRUE') {
                echo '&nbsp;<span class="badge badge-success">Musrenbang</span>';
            }
            if ($rd->getIsOutput() == 'TRUE') {
                echo '&nbsp;<span class="badge badge-info">Output</span>';
            }
            if ($rd->getPrioritasWali() == 'TRUE') {
                echo '&nbsp;<span class="badge badge-warning">Prioritas</span>';
            }
            if ($rd->getIsHibah() == 'TRUE') {
                echo '&nbsp;<span class="badge badge-success">Hibah</span>';
            }
            if ($rd->getKecamatan() <> '') {
                echo '&nbsp;<span class="badge badge-warning">Jasmas</span>';
            }
            if ($rd->getThKeMultiyears() <> null && $rd->getThKeMultiyears() > 0) {
                echo '&nbsp;<span class="badge badge-primary">Multiyears Tahun ke ' . $rd->getThKeMultiyears() . '</span>';
            }
            // if ($rd->getIsBlud() == 1) {
            //     echo '&nbsp;<span class="badge badge-info">BLUD</span>';
            // }
            echo '<br/>';
            echo $rd->getKomponenName();
            if (sfConfig::get('app_fasilitas_keteranganKomponen') == 'buka') {
                echo ' ' . $rd->getDetailName() . '<br/>';
                echo '&nbsp;<span class="badge badge-danger">' . (($rd->getTahap() == 'pak') ? 'PAK' : ($rd->getTahap() == 'murni' ? 'Murni' : $rd->getTahap())) . '</span><br/>';
                    if ($rd->getSumberDanaId() == 1) {
                        echo '&nbsp;<span class="badge badge-info">DAK</span>';
                    }   
                    if ($rd->getSumberDanaId() == 2) {
                        echo '&nbsp;<span class="badge badge-info">>DBHCT</span>';
                    }
                    if ($rd->getSumberDanaId() == 3) {
                        echo '&nbsp;<span class="badge badge-info">DID</span>';
                    }
                    if ($rd->getSumberDanaId() == 4) {
                        echo '&nbsp;<span class="badge badge-info">DBH</span>';
                    }
                    if ($rd->getSumberDanaId() == 5) {
                        echo '&nbsp;<span class="badge badge-info">Pajak Rokok</span>';
                    }
                    if ($rd->getSumberDanaId() == 6) {
                        echo '&nbsp;<span class="badge badge-info">BLUD</span>';
                    }
                    if ($rd->getSumberDanaId() == 7) {
                        echo '&nbsp;<span class="badge badge-info">Bantuan Keuangan Provinsi</span>';
                    }
                    if ($rd->getSumberDanaId() == 8) {
                        echo '&nbsp;<span class="badge badge-info">Kapitasi JKN</span>';
                    }
                    if ($rd->getSumberDanaId() == 9) {
                        echo '&nbsp;<span class="badge badge-info">BOS</span>';
                    }
                    if ($rd->getSumberDanaId() == 10) {
                        echo '&nbsp;<span class="badge badge-info">DAU</span>';
                    }
                    if ($rd->getSumberDanaId() == 12) {
                        echo '&nbsp;<span class="badge badge-info">Hibah Pariwisata</span>';
                    }
                    if ($rd->getIsCovid() == true) {
                        echo '&nbsp;<span class="badge badge-info">Covid-19</span>';
                    }
                if ($rd->getTipe2() == 'KONSTRUKSI' || $rd->getTipe2() == 'TANAH' || $rd->getTipe() == 'FISIK' || $est_fisik) {
                    if ($rd->getLokasiKecamatan() <> '' && $rd->getLokasiKelurahan() <> '') {
                        echo '[' . $rd->getLokasiKelurahan() . ' - ' . $rd->getLokasiKecamatan() . ']';
                    }
                }
            }
            $query2 = "select tahap from " . sfConfig::get('app_default_schema') . ".komponen "
            . "where komponen_id='" . $rd->getKomponenId() . "'";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query2);
            $t = $stmt->executeQuery();
            while ($t->next()) {
                if ($t->getString('tahap') == DinasMasterKegiatanPeer::getTahapKegiatan($rd->getUnitId(), $rd->getKegiatanCode()) && sfConfig::get('app_tahap_edit') != 'murni') {
                    echo image_tag('/images/newanima.gif');
                }
            }
            if (in_array($rd->getDetailNo(), $komponen_serupa)) {
                echo image_tag('/images/warning.gif', array('title' => 'Ada komponen serupa', 'alt' => 'Ada komponen serupa'));
            }
            ?>
            <div id="<?php echo 'tempat_ajax_' . $no ?>">
                <?php
                if ($tahap == '' && sfConfig::get('app_tahap_edit') != 'murni') {
                    $kegiatan = $rd->getKegiatanCode();
                    $array_skpd_lock = array('2600','2000', '0900', '0300','3200','1400','0400','2300','2800','0302','2400','3000','1221', '0305','0310','1100','2500','2200','1800','0308','2700','1700','0309');

                    //lepas array komponen lelang 300821
                     $arr_kode_detail_kegiatan_lelang=array('');

                    if (in_array($unit_id, $array_skpd_lock) ) {
                        //buka lock harga dasar disini
                        if ($rd->getStatusLelang() == 'unlock') {
                            echo "<div class='btn-group'>";
                            echo link_to_function('<i class="fa fa-money"></i>', '', array('class' => 'btn btn-outline-success btn-sm', 'disable' => true));
                            if ($sf_user->getNamaUser() != 'peninjau' && $sf_user->getNamaUser() != 'dppk' && $sf_user->getNamaUser() != 'bppk' && $sf_user->getNamaUser() != 'wawali' && $sf_user->getNamaUser() != 'walikota' && $sf_user->getNamaUser() != 'sekda' && $sf_user->getNamaUser() != 'asisten' && $sf_user->getNamaUser() != 'masger') {
                                echo link_to_function('Lock Harga Dasar', 'execLockHargaDasarRevisi("lock","' . $id . '","' . $no . '","' . $unit_id . '","' . $kegiatan . '", 0)', array('class' => 'btn btn-outline-success btn-sm'));
                            }
                            echo "</div>";
                        } else {

                            echo "<div class='btn-group'>";
                            if ($sf_user->getNamaUser() != 'peninjau' && $sf_user->getNamaUser() != 'dppk' && $sf_user->getNamaUser() != 'bppk' && $sf_user->getNamaUser() != 'wawali' && $sf_user->getNamaUser() != 'walikota' && $sf_user->getNamaUser() != 'sekda' && $sf_user->getNamaUser() != 'asisten' && $sf_user->getNamaUser() != 'masger') {
                                if (sfConfig::get('app_fasilitas_cekeDelivery') == 'buka' && in_array($unit_id . '.' . $kegiatan . '.' . $no, $arr_kode_detail_kegiatan_lelang)) {
                                    echo link_to_function('Unlock Harga Dasar', 'show_unlock_modal("' . $id . '","' . $unit_id . '","' . $kegiatan . '","' . $no . '")', array('class' => 'btn btn-outline-success btn-sm'));
                                } else {
                                    echo link_to_function('Unlock Harga Dasar', 'execLockHargaDasarRevisi("unlock","' . $id . '","' . $no . '","' . $unit_id . '","' . $kegiatan . '", 0)', array('class' => 'btn btn-outline-success btn-sm'));
                                }
                            }
                            echo "</div>";
                        }
                    }
                }
            ?>
            </div>
        </td>
        <td style="background: <?php echo $warna ?>" align="center"><?php echo $rd->getSatuan(); ?></td>
        <?php if( $rd->getAccres() > 1) 
        {
            $keterangan_koefisien= '('.$rd->getKeteranganKoefisien().') + '.$rd->getAccres().'%';
        }
        else
        {
            $keterangan_koefisien=$rd->getKeteranganKoefisien();
        }
        ?>
        <td style="background: <?php echo $warna ?>" align="center">
            <?php 
            echo $keterangan_koefisien; 
            if($rd->getSatuan() != '%' && $rd->getVolumeOrang() != 0){
                echo '<br>Volume Orang: '.$rd->getVolumeOrang();
            }
            ?>
        </td>
        <td style="background: <?php echo $warna ?>" align="right">
            <?php
            if ($rd->getSatuan() == '%') {
                echo $rd->getKomponenHargaAwal();
            } elseif ($rd->getKomponenHargaAwal() != floor($rd->getKomponenHargaAwal())) {
                echo number_format($rd->getKomponenHargaAwal(), 2, ',', '.');
            } else {
                echo number_format($rd->getKomponenHargaAwal(), 0, ',', '.');
            }
            if ($rd->getLockSubtitle() <> 'LOCK' && $rd->getSatuan()=='Paket' && $rd->getVolume()==1 ) {
                if ($rd->getStatusLelang() == 'lock' ) {
                    echo image_tag('/images/bahaya2.gif');
                    echo '<br/><span class="badge badge-danger">[Telah dilakukan Penggunaan Sisa Lelang, tidak dapat mengedit volume]</span>';
                } else if ($rd->getStatusLelang() == 'unlock') {
                    $lelang = 0;
                    $ceklelangselesaitidakaturanpembayaran = 0;
                    if (sfConfig::get('app_fasilitas_cekeProject') == 'buka') {
                        if (sfConfig::get('app_fasilitas_cekServer') == 'buka') {
                            $lelang = $rd->getCekLelang($rd->getUnitId(), $rd->getKegiatanCode(), $rd->getDetailNo(), $rd->getNilaiAnggaran());
                            if (sfConfig::get('app_fasilitas_cekeDelivery') == 'buka') {
                                $ceklelangselesaitidakaturanpembayaran = $rd->getCekLelangTidakAdaAturanPembayaran($rd->getUnitId(), $rd->getKegiatanCode(), $rd->getDetailNo());
                            }
                        }
                    }
                    if ($lelang == 0 && $ceklelangselesaitidakaturanpembayaran == 0) {
                        echo form_tag("peneliti/ubahhargadasar?id=$id&unit_id=$unit_id&kegiatan_code=$kegiatan_code&detail_no=$no");
                        echo input_tag("komponen_harga_awal", '', array('style' => 'width:50px', 'value' => $rd->getKomponenHargaAwal()));
                        echo "<br>";
                        echo submit_tag(' OK ');
                        echo "</form>";
                    } else {
                        if ($lelang > 0) {
                            echo '<br/><span class="badge badge-danger">[Lelang Sedang Berjalan]</span>';
                        }
                        if ($ceklelangselesaitidakaturanpembayaran == 1) {
                            echo '<br/><span class="badge badge-danger">[Belum ada Aturan Pembayaran]</span>';
                        }
                    }
                    
                    if ($rd->getSatuan()=='Paket' && $rd->getVolume()==1)
                    {
                         echo '<br/><span class="badge badge-danger">[Telah dilakukan Penggunaan Sisa Lelang, tidak dapat mengedit volume]</span>';
                    }
                }
                if($rd->getIsHpsp()) {
                    echo '<br/><span class="badge badge-primary">[Merupakan komponen Hasil PSP]</span>';
                }
            }
            ?>
        </td>
        <td style="background: <?php echo $warna ?>" align="right">
            <?php
            $volume = $rd->getVolume();
            $harga = $rd->getKomponenHargaAwal();
            $hasil = $volume * $harga;
            echo number_format($hasil, 0, ',', '.');
            ?>
        </td>
        <td style="background: <?php echo $warna ?>" align="right">
            <?php echo $rd->getPajak() . '%'; ?>
        </td>
        <td style="background: <?php echo $warna ?>" align="right">
            <?php
            $volume = $rd->getVolume();
            $harga = $rd->getKomponenHargaAwal();
            $pajak = $rd->getPajak();
            $total = $rd->getNilaiAnggaran();
            echo number_format($total, 0, ',', '.');
            ?>
        </td>
        <td style="background: <?php echo $warna ?>" align="center">
            <?php
            $rekening = $rd->getRekeningCode();
            $rekening_code = substr($rekening, 0, 6);
            $c = new Criteria();
            $c->add(KelompokBelanjaPeer::BELANJA_CODE, $rekening_code);
            $rs_rekening = KelompokBelanjaPeer::doSelectOne($c);
            if ($rs_rekening) {
                echo $rs_rekening->getBelanjaName();
            }
            ?>
        </td>
        <?php if(sfConfig::get('app_tahap_edit') != 'murni'): ?>
            <td style="background: <?php echo $warna ?>">
                <?php
                if ($rd->getLockSubtitle() <> 'LOCK' && $rd->getStatusLelang() == 'unlock' && $rd->getNoteSkpd() == '') {
                    $detno = $rd->getDetailNo();
                    echo 'Catatan SKPD:<br/>';
                    echo form_tag("peneliti/simpanCatatan?unit_id=$unit_id&kegiatan_code=$kegiatan_code&detail_no=$detno");
                    echo input_tag("catatan_skpd", '', array('style' => 'width:100px'));
                    echo "<br>";
                    echo submit_tag(' OK ');
                    echo "</form>";
                    echo "<br/><font style='font-weight: bold; color: red'>-Isi kolom ini untuk membuka Field Penggunaan Sisa Lelang.-</font>";
                }
                ?>
            </td>
        <?php endif; ?>
    </tr>
<?php endforeach; ?>
<script>
    $(".saveGantiRekening").click(function () { // changed
        var id_form = $(this).closest("form").attr("id"); //parent form
        var detailno = id_form.split("_").pop();
        $.ajax({
            type: "POST",
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/peneliti/gantiRekeningRevisi.html",
            data: $(this).parent().serialize(), // changed
            success: function () {
                $('#action_pekerjaans_' + detailno).html('<br/><font style="color: green;font-weight:bold">{Silahkan Refresh Halaman}</font><br/><br/>');
            }
        });
        return false; // avoid to execute the actual submit of the form.
    });
    function execLockHargaDasarRevisi(act, id, detNo, unitId, kegCode,vol, approve) {
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/peneliti/lockhargadasarrevisi/act/" + act + "/id/" + id + "/detail_no/" + detNo + "/unit_id/" + unitId + "/kegiatan_code/" + kegCode + "/vol/" + vol + "/approve/" + approve + ".html",
            context: document.body
        }).done(function (msg) {
            $('#tempat_ajax_' + detNo).html(msg);
        });
    }

    function execKembalikanWaitingRevisi(detailno, unitid, kodekegiatan, id) {
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/peneliti/kembalikanWaitingRevisiEdit/unitid/" + unitid + "/kodekegiatan/" + kodekegiatan + "/detailno/" + detailno + ".html",
            context: document.body
        }).done(function (msg) {
            $('#tempat_ajax_' + id).html(msg);
        });
    }

    function execBuatNote(detailno, unitid, kodekegiatan, id) {
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/peneliti/buatCatatan/unitid/" + unitid + "/kodekegiatan/" + kodekegiatan + "/detailno/" + detailno + ".html",
            context: document.body
        }).done(function (msg) {
            $('#tempat_note_' + id).html(msg);
        });
    }

    function hitungAnggaran() {
        var harga = $('#harga').val();
        var pajakx = $('#pajakx').val();
        var detail_kegiatan = $('#detail_kegiatan').val();
        //var volume = $('#volume_rasionalisasi').val();
        var vol = 'volume_rasionalisasi_'+detail_kegiatan+'';
        var volume =  $('[name="'+ vol +'"]').val();       
        var hitung;   

        alert(vol+'-'+volume+'-'+harga+'-'+pajakx+'-'+detail_kegiatan);

   }
</script>