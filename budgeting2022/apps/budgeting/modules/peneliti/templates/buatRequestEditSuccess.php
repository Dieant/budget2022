<?php use_helper('I18N', 'Date', 'Object', 'Javascript', 'Validation') ?>
<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Request Penyelia</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Lembar Kerja</a></li>
          <li class="breadcrumb-item active">Request Penyelia</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>
<!-- Main content -->
<section class="content">
    <?php include_partial('peneliti/list_messages'); ?>
    <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">
                        <i class="fab fa-foursquare"></i> Form
                    </h3>
                </div>
                <div class="card-body">
                    <?php echo form_tag('peneliti/buatRequest', array('multipart' => true, 'class' => 'form-horizontal')); ?>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Nama Peneliti</label>
                                <?php
                                    echo input_tag('penyelia', $sf_user->getNamaUser(), array('class' => 'form-control', 'readonly' => 'true'));
                                ?>
                            </div>
                        </div>
                        <!-- /.col -->
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Tipe Request</label>
                                <?php
                                    echo select_tag('tipe', options_for_select(array('include_custom' => '--Pilih Tipe--','0' => 'Buka Kunci Kegiatan', '1' => 'Tambah Pagu', '2' => 'Upload Dokumen'), null), array('class' => 'form-control select2'));
                                ?>
                            </div>
                        </div>
                        <!-- /.col -->
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Perangkat Daerah</label>
                                <?php
                                    $e = new Criteria();
                                    $nama = $sf_user->getNamaUser();
                                    $b = new Criteria;
                                    $b->add(UserHandleV2Peer::USER_ID, $nama);
                                    $es = UserHandleV2Peer::doSelect($b);
                                    $satuan_kerja = $es;
                                    $unit_kerja = Array();
                                    foreach ($satuan_kerja as $x) {
                                        $unit_kerja[] = $x->getUnitId();
                                    }
                                    $e->add(UnitKerjaPeer::UNIT_ID, $unit_kerja[0], criteria::ILIKE);
                                    for ($i = 1; $i < count($unit_kerja); $i++) {
                                        $e->addOr(UnitKerjaPeer::UNIT_ID, $unit_kerja[$i], criteria::ILIKE);
                                    }
                                    $e->addAscendingOrderByColumn(UnitKerjaPeer::UNIT_NAME);
                                    $v = UnitKerjaPeer::doSelect($e);
                                    echo select_tag('unit_id', objects_for_select($v, 'getUnitId', 'getUnitName', null, array('include_custom' => '--Pilih Perangkat Daerah--')), array('class' => 'form-control js-example-basic-single select2'));
                                ?>
                            </div>
                        </div>
                        <!-- /.col -->
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Nama Sub-Kegiatan</label>
                                <?php
                                    echo select_tag('kode_kegiatan', options_for_select(array(), '', null), array('id' => 'keg', 'class' => 'form-control js-example-basic-multiple select2', 'multiple' => 'multiple'));
                                ?>
                            </div>
                        </div>
                        <!-- /.col -->
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Alasan</label>
                                <?php
                                    echo textarea_tag('catatan', null, array('class' => 'form-control'));
                                ?>
                            </div>
                        </div>
                        <!-- /.col -->
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Data Pendukung <br/>(Gunakan file xls, xlsx, zip, rar, jpg, png, atau pdf)</label>
                                <?php
                                    echo input_file_tag('file', array('class' => 'form-control'));
                                ?>
                            </div>
                        </div>
                        <!-- /.col -->
                    </div>                   
                </div>
                <div class="card-footer">
                    <div class="col-md-12 text-right">
                        <div class="form-group">
                            <button type="submit" name="submit" class="btn btn-outline-primary btn-sm">Simpan <i class="fas fa-save"></i></button>
                            <button type="reset" name="reset" class="btn btn-outline-danger btn-sm"/>Reset <i class="fa fa-backspace"></i></button>
                        </div>
                        <!-- /.form-group -->
                    </div>
                    <!-- /.col --> 
                </div>
                 <?php echo '</form>' ?>
            </div>
        </div>
    </div>
</section>
<script>
    $(document).ready(function () {
        $(".js-example-basic-multiple").select2();
    });

    $("#unit_id").change(function () {
        var id = $(this).val();
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/peneliti/pilihKegiatan/id/" + id + ".html",
            context: document.body
        }).done(function (msg) {
            $('#keg').html(msg);
        });

    });
</script>