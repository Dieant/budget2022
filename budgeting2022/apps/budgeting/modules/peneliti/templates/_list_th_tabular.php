<?php
// auto-generated by sfPropelAdmin
// date: 2009/02/19 14:40:39
?>
<th id="sf_admin_list_th_kodekegiatan">
    <?php echo __('ID') ?>
</th>
<th id="sf_admin_list_th_namakegiatan">
    <?php echo __('Nama Sub Kegiatan') ?>
</th>
<?php if ($sf_user->getNamaUser() != 'bpk') { ?>
    <!-- <th id="sf_admin_list_th_alokasidana">
        <?php echo __('Buku Putih') ?>
    </th> -->
<?php } ?>
<?php if (sfConfig::get('app_tahap_edit') == 'murni') { ?>
    <th id="sf_admin_list_th_alokasidana">
        <?php  echo __('Pagu') ?>
    </th>
<?php } else { ?>
    <th id="sf_admin_list_th_selisih">
    <?php echo __('Semula') ?>
</th>
<?php } ?>
<?php if ($sf_user->getNamaUser() != 'bpk') { ?>
    <th id="sf_admin_list_th_nilairincian">
        <?php echo __('Rincian') ?>
    </th>
<?php } else { ?>
    <th id="sf_admin_list_th_nilairincian">
        <?php echo __('Menjadi') ?>
    </th>
<?php } ?>
<th id="sf_admin_list_th_selisih">
    <?php echo __('Selisih') ?>
</th>
<?php if ($sf_user->getNamaUser() != 'bpk' && $sf_user->getNamaUser() != 'wawali' && $sf_user->getNamaUser() != 'masger') { ?>
    <th id="sf_admin_list_th_posisi">
        <?php echo __('Persen (5.2.2.01)') ?>
    </th> 
    <th id="sf_admin_list_th_catatan">
        <?php echo __('Pagu Tambahan') ?>
    </th>        
    <th id="sf_admin_list_th_catatan">
        <?php echo __('Catatan') ?>
    </th>
    <th id="sf_admin_list_th_catatan">
        <?php echo __('Catatan Rincian') ?>
    </th>
    <?php ?>
    <th id="sf_admin_list_th_tahap">
        <?php echo __('Tahap') ?>
    </th>          
<?php } ?>
<?php if ($sf_user->getNamaUser() == 'bpk'){ ?>
    <th id="sf_admin_list_th_tahap">
        <?php echo __('Tahap') ?>
    </th>      
<?php } ?>