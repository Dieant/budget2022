<?php use_helper('I18N', 'Date', 'Object', 'Javascript', 'Validation') ?>
<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Request Penyelia</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Lembar Kerja</a></li>
          <li class="breadcrumb-item active">Request Penyelia</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>
<!-- Main content -->
<section class="content">
    <?php include_partial('admin/list_messages'); ?>
    <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">
                        <i class="fas fa-filter"></i> Filter
                    </h3>
                </div>
                <div class="card-body">
                    <?php echo form_tag('peneliti/requestlist', array('method' => 'get', 'class' => 'form-horizontal')); ?>
                    <div class="row">
                      <div class="col-md-3">
                        <div class="form-group">
                            <label>Perangkat Daerah</label>
                            <?php
                                $e = new Criteria();
                                $nama = $sf_user->getNamaUser();
                                //print_r ($nama); exit;
                                $b = new Criteria;
                                $b->add(UserHandleV2Peer::USER_ID, $nama);
                                $es = UserHandleV2Peer::doSelect($b);
                                $satuan_kerja = $es;
                                $unit_kerja = Array();
                                foreach ($satuan_kerja as $x) {
                                    $unit_kerja[] = $x->getUnitId();
                                }
                                $e->add(UnitKerjaPeer::UNIT_ID, $unit_kerja[0], criteria::ILIKE);
                                for ($i = 1; $i < count($unit_kerja); $i++) {
                                    $e->addOr(UnitKerjaPeer::UNIT_ID, $unit_kerja[$i], criteria::ILIKE);
                                }
                                $e->addAscendingOrderByColumn(UnitKerjaPeer::UNIT_NAME);
                                $v = UnitKerjaPeer::doSelect($e);

                                echo select_tag('filters[unit_id]', objects_for_select($v, 'getUnitId', 'getUnitName', isset($filters['unit_id']) ? $filters['unit_id'] : null, array('include_custom' => '--Perangkat Daerah--')), array('class' => 'form-control select2'));
                            ?>
                        </div>
                      </div>
                      <!-- /.col -->
                      <div class="col-md-3">
                        <div class="form-group">
                            <label>Tipe</label>
                            <?php
                                echo select_tag('filters[tipe]', options_for_select(array('0' => 'Buka Kunci Kegiatan', '1' => 'Tambah Pagu', '2' => 'Upload Dokumen'), isset($filters['tipe']) ? $filters['tipe'] : null, array('include_custom' => '--Semua Tipe--')), array('class' => 'form-control select2'));
                            ?>
                        </div>
                      </div>
                      <!-- /.col -->
                      <div class="col-md-2">
                        <div class="form-group">
                            <label class="tombol_filter">Tombol Filter</label><br/>
                            <button type="submit" name="filter" class="btn btn-outline-primary btn-sm">Filter <i class="fas fa-search"></i></button>
                            <?php
                                echo link_to('Reset <i class="fa fa-backspace"></i>', 'peneliti/requestlist?filter=filter', array('class' => 'btn btn-outline-danger btn-sm'));
                            ?>
                        </div>
                        <!-- /.form-group -->
                      </div>
                      <!-- /.col -->                      
                    </div>
                    <?php echo '</form>'; ?>
                </div>
            </div>
          </div>
        </div>
        <div class="row">
            <div class="col-md-12 stretch-card">
                <div class="card">
                    <?php if (!$pager->getNbResults()): ?>
                    <?php echo __('no result') ?>
                    <?php else: ?>
                        <?php include_partial("peneliti/requestlist", array('pager' => $pager)) ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>