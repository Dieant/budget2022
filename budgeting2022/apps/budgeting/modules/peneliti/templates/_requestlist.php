<div class="card-body table-responsive p-0">
    <table class="table table-hover">
        <thead class="head_peach">
            <tr>
                <th>Perangkat Daerah</th>
                <th>Kode Kegiatan</th>
                <th>Nama Kegiatan</th>
                <th>Tipe Request</th>
                <th>Penyelia</th>
                <th>Tanggal</th>
                <th>Catatan</th>
                <th>Status</th>
                <th>File</th>
                <th>Hapus</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $i = 1;
            foreach ($pager->getResults() as $data):
            ?>
            <div class="modal fade" id="myModal<?php echo $data->getId() ?>" tabindex="-1" role="dialog"
                aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="myModalLabel">Alasan Tolak</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <?php echo $data->getCatatanTolak() ?>
                        </div>
                    </div>
                </div>
            </div>
            <?php
                $odd = fmod(++$i, 2);
                $c = new Criteria();
                $c->add(UnitKerjaPeer::UNIT_ID, $data->getUnitId());
                if ($rs_skpd = UnitKerjaPeer::doSelectOne($c)) {
                    $skpd = $rs_skpd->getUnitName();
                }
                $arr_nama_kegiatan = array();
                $arr_kegiatan = explode('|', $data->getKegiatanCode());
                foreach ($arr_kegiatan as $kode_kegiatan) {
                    $c = new Criteria();
                    $c->add(DinasMasterKegiatanPeer::UNIT_ID, $data->getUnitId());
                    $c->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                    if ($rs_kegiatan = DinasMasterKegiatanPeer::doSelectOne($c)) {
                        $arr_nama_kegiatan[] = $rs_kegiatan->getNamaKegiatan();
                    }
                }

                if ($data->getTipe() == 0) {
                    $tipe = 'Buka Kunci Kegiatan';
                } elseif ($data->getTipe() == 1) {
                    $tipe = 'Tambah Pagu';
                } elseif ($data->getTipe() == 2) {
                    $tipe = 'Upload Dokumen';
                }

                if ($data->getStatus() == 0) {
                    $status = 'Pending';
                } elseif ($data->getStatus() == 1) {
                    $status = '<font style="color:green">Diterima</font>';
                } elseif ($data->getStatus() == 2) {
                    $status = '<button type="button" class="btn btn-link btn-xs" data-toggle="modal" data-target="#myModal' . $data->getId() . '">Ditolak</button>';
                }
                ?>
            <tr>
                <td><?php echo '(' . $data->getUnitId() . ') ' . $skpd ?></td>
                <td><?php echo implode(', ', $arr_kegiatan) ?></td>
                <td><?php echo implode(', ', $arr_nama_kegiatan) ?></td>
                <td><?php echo $tipe ?></td>
                <td><?php echo $data->getPenyelia() ?></td>
                <td><?php echo $data->getUpdatedAt() ?></td>
                <td><?php echo $data->getCatatan() ?></td>
                <td><?php echo $status ?></td>
                <td><?php
                        if ($data->getIsKhusus()) {
                            echo 'KHUSUS';
                        } else {
                            echo link_to('Download File', sfConfig::get('app_path_default_sf') . 'uploads/log/' . $data->getPath());
                        }
                        ?>
                </td>
                <td>
                    <?php
                        if ($data->getStatus() != 1) {
                            echo link_to('<i class="fa fa-times-circle"></i>', 'peneliti/hapusRequest?id=' . $data->getId(),  array('confirm' => 'Apakah anda Yakin untuk menghapus?', 'alt' => __('Hapus'), 'title' => __('Hapus'), 'class' => 'btn btn-outline-danger btn-sm'));
                        }
                        ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
<div class="card-footer clearfix">
    <ul class="pagination pagination-sm m-0 float-left">
        <?php
        echo format_number_choice('[0] no result|[1] 1 result|(1,+Inf] %1% results', array('%1%' => $pager->getNbResults()), $pager->getNbResults()) 
        ?>
    </ul>
    <ul class="pagination pagination-sm m-0 float-right">
        <?php 
        if ($pager->haveToPaginate()): 
            echo '<li class="page-item">'.link_to('Previous', 'peneliti/requestlist?page=' . $pager->getPreviousPage(), array('class' => 'page-link', 'align' => 'absmiddle', 'alt' => __('Previous'), 'title' => __('Previous'))).'</li>';

            foreach ($pager->getLinks() as $page):
                echo '<li class="page-item">'.link_to_unless($page == $pager->getPage(), $page, "entri/requestlist?page={$page}", array('class' => 'page-link')).'</li>';
            endforeach;
            echo '<li class="page-item">'.link_to('Next', 'entri/requestlist?page=' . $pager->getNextPage(), array('class' => 'page-link', 'align' => 'absmiddle', 'alt' => __('Next'), 'title' => __('Next'))).'</li>'; 
        endif;
        ?>
    </ul>
</div>