<?php use_helper('Object', 'Javascript', 'I18N', 'Date'); ?>

<?php use_stylesheet('/sf/sf_admin/css/main') ?>
<?php echo use_helper('Object', 'Javascript', 'Validation') ?>
<?php

$options = array();

while ($master_kegiatan->next()) {
    $options[$master_kegiatan->getString('kegiatan_code')] = $master_kegiatan->getString('kegiatan_code') . ' - '. $master_kegiatan->getString('nama_kegiatan');
}

echo options_for_select($options, '', array('include_blank' => true));
?>