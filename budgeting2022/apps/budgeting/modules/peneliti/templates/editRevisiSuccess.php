<?php use_helper('I18N', 'Date', 'Url', 'Javascript', 'Form', 'Object') ?>
<?php
if ($tahap == 'pakbp') {
    $tabel_dpn = 'pak_bukuputih_';
} elseif ($tahap == 'pakbb') {
    $tabel_dpn = 'pak_bukubiru_';
} elseif ($tahap == 'murni') {
    $tabel_dpn = 'murni_';
} elseif ($tahap == 'murnibp') {
    $tabel_dpn = 'murni_bukuputih_';
} elseif ($tahap == 'murnibb') {
    $tabel_dpn = 'murni_bukubiru_';
} elseif ($tahap == 'revisi1') {
    $tabel_dpn = 'revisi1_';
} elseif ($tahap == 'revisi1_1') {
    $tabel_dpn = 'revisi1_1_';
} elseif ($tahap == 'revisi2') {
    $tabel_dpn = 'revisi2_';
} elseif ($tahap == 'revisi2_1') {
    $tabel_dpn = 'revisi2_1_';
} elseif ($tahap == 'revisi2_2') {
    $tabel_dpn = 'revisi2_2_';
} elseif ($tahap == 'revisi3') {
    $tabel_dpn = 'revisi3_';
} elseif ($tahap == 'revisi3_1') {
    $tabel_dpn = 'revisi3_1_';
}  elseif ($tahap == 'revisi4') {
    $tabel_dpn = 'revisi4_';
} elseif ($tahap == 'revisi5') {
    $tabel_dpn = 'revisi5_';
} elseif ($tahap == 'revisi6') {
    $tabel_dpn = 'revisi6_';
} elseif ($tahap == 'revisi7') {
    $tabel_dpn = 'revisi7_';
} elseif ($tahap == 'revisi8') {
    $tabel_dpn = 'revisi8_';
} elseif ($tahap == 'revisi9') {
    $tabel_dpn = 'revisi9_';
} elseif ($tahap == 'revisi10') {
    $tabel_dpn = 'revisi10_';
} elseif ($tahap == 'rkua') {
    $tabel_dpn = 'rkua_';
} else {
    $tabel_dpn = 'dinas_';
}

$kegiatan_code = $kode_kegiatan = $sf_params->get('kode_kegiatan');
$unit_id = $sf_params->get('unit_id');
$sub_id = 1;
$cetak = FALSE;

$status = 'OPEN';
$sf_user->removeCredential('status_dinas');
$sf_user->addCredential('status_dinas');
$sf_user->setAttribute('status', $status, 'status_dinas');

//khusus kalau sisipan
if ($tahap == 'pakbp') {
    $c = new Criteria();
    $c->add(PakBukuPutihMasterKegiatanPeer::KODE_KEGIATAN, $kegiatan_code);
    $c->add(PakBukuPutihMasterKegiatanPeer::UNIT_ID, $unit_id);
    $rs_kegiatan = PakBukuPutihMasterKegiatanPeer::doSelectOne($c);
    $status = 'CLOSED';
} elseif ($tahap == 'pakbb') {
    $c = new Criteria();
    $c->add(PakBukuBiruMasterKegiatanPeer::KODE_KEGIATAN, $kegiatan_code);
    $c->add(PakBukuBiruMasterKegiatanPeer::UNIT_ID, $unit_id);
    $rs_kegiatan = PakBukuBiruMasterKegiatanPeer::doSelectOne($c);
    $status = 'CLOSED';
} elseif ($tahap == 'murni') {
    $c = new Criteria();
    $c->add(MurniMasterKegiatanPeer::KODE_KEGIATAN, $kegiatan_code);
    $c->add(MurniMasterKegiatanPeer::UNIT_ID, $unit_id);
    $rs_kegiatan = MurniMasterKegiatanPeer::doSelectOne($c);
    $status = 'CLOSED';
} elseif ($tahap == 'murnibp') {
    $c = new Criteria();
    $c->add(MurniBukuPutihMasterKegiatanPeer::KODE_KEGIATAN, $kegiatan_code);
    $c->add(MurniBukuPutihMasterKegiatanPeer::UNIT_ID, $unit_id);
    $rs_kegiatan = MurniBukuPutihMasterKegiatanPeer::doSelectOne($c);
    $status = 'CLOSED';
} elseif ($tahap == 'murnibb') {
    $c = new Criteria();
    $c->add(MurniBukuBiruMasterKegiatanPeer::KODE_KEGIATAN, $kegiatan_code);
    $c->add(MurniBukuBiruMasterKegiatanPeer::UNIT_ID, $unit_id);
    $rs_kegiatan = MurniBukuBiruMasterKegiatanPeer::doSelectOne($c);
    $status = 'CLOSED';
} elseif ($tahap == 'revisi1') {
    $c = new Criteria();
    $c->add(Revisi1MasterKegiatanPeer::KODE_KEGIATAN, $kegiatan_code);
    $c->add(Revisi1MasterKegiatanPeer::UNIT_ID, $unit_id);
    $rs_kegiatan = Revisi1MasterKegiatanPeer::doSelectOne($c);
    $status = 'CLOSED';
} elseif ($tahap == 'revisi1_1') {
    $c = new Criteria();
    $c->add(Revisi1bMasterKegiatanPeer::KODE_KEGIATAN, $kegiatan_code);
    $c->add(Revisi1bMasterKegiatanPeer::UNIT_ID, $unit_id);
    $rs_kegiatan = Revisi1bMasterKegiatanPeer::doSelectOne($c);
    $status = 'CLOSED';
} elseif ($tahap == 'revisi2') {
    $c = new Criteria();
    $c->add(Revisi2MasterKegiatanPeer::KODE_KEGIATAN, $kegiatan_code);
    $c->add(Revisi2MasterKegiatanPeer::UNIT_ID, $unit_id);
    $rs_kegiatan = Revisi2MasterKegiatanPeer::doSelectOne($c);
    $status = 'CLOSED';
} elseif ($tahap == 'revisi2_1') {
    $c = new Criteria();
    $c->add(Revisi21MasterKegiatanPeer::KODE_KEGIATAN, $kegiatan_code);
    $c->add(Revisi21MasterKegiatanPeer::UNIT_ID, $unit_id);
    $rs_kegiatan = Revisi21MasterKegiatanPeer::doSelectOne($c);
    $status = 'CLOSED';
} elseif ($tahap == 'revisi2_2') {
    $c = new Criteria();
    $c->add(Revisi22MasterKegiatanPeer::KODE_KEGIATAN, $kegiatan_code);
    $c->add(Revisi22MasterKegiatanPeer::UNIT_ID, $unit_id);
    $rs_kegiatan = Revisi22MasterKegiatanPeer::doSelectOne($c);
    $status = 'CLOSED';
} elseif ($tahap == 'revisi3') {
    $c = new Criteria();
    $c->add(Revisi3MasterKegiatanPeer::KODE_KEGIATAN, $kegiatan_code);
    $c->add(Revisi3MasterKegiatanPeer::UNIT_ID, $unit_id);
    $rs_kegiatan = Revisi3MasterKegiatanPeer::doSelectOne($c);
    $status = 'CLOSED';
} elseif ($tahap == 'revisi3_1') {
    $c = new Criteria();
    $c->add(Revisi31MasterKegiatanPeer::KODE_KEGIATAN, $kegiatan_code);
    $c->add(Revisi31MasterKegiatanPeer::UNIT_ID, $unit_id);
    $rs_kegiatan = Revisi31MasterKegiatanPeer::doSelectOne($c);
    $status = 'CLOSED';
} elseif ($tahap == 'revisi4') {
    $c = new Criteria();
    $c->add(Revisi4MasterKegiatanPeer::KODE_KEGIATAN, $kegiatan_code);
    $c->add(Revisi4MasterKegiatanPeer::UNIT_ID, $unit_id);
    $rs_kegiatan = Revisi4MasterKegiatanPeer::doSelectOne($c);
    $status = 'CLOSED';
} elseif ($tahap == 'revisi5') {
    $c = new Criteria();
    $c->add(Revisi5MasterKegiatanPeer::KODE_KEGIATAN, $kegiatan_code);
    $c->add(Revisi5MasterKegiatanPeer::UNIT_ID, $unit_id);
    $rs_kegiatan = Revisi5MasterKegiatanPeer::doSelectOne($c);
    $status = 'CLOSED';
} elseif ($tahap == 'revisi6') {
    $c = new Criteria();
    $c->add(Revisi6MasterKegiatanPeer::KODE_KEGIATAN, $kegiatan_code);
    $c->add(Revisi6MasterKegiatanPeer::UNIT_ID, $unit_id);
    $rs_kegiatan = Revisi6MasterKegiatanPeer::doSelectOne($c);
    $status = 'CLOSED';
} elseif ($tahap == 'revisi7') {
    $c = new Criteria();
    $c->add(Revisi7MasterKegiatanPeer::KODE_KEGIATAN, $kegiatan_code);
    $c->add(Revisi7MasterKegiatanPeer::UNIT_ID, $unit_id);
    $rs_kegiatan = Revisi7MasterKegiatanPeer::doSelectOne($c);
    $status = 'CLOSED';
} elseif ($tahap == 'revisi8') {
    $c = new Criteria();
    $c->add(Revisi8MasterKegiatanPeer::KODE_KEGIATAN, $kegiatan_code);
    $c->add(Revisi8MasterKegiatanPeer::UNIT_ID, $unit_id);
    $rs_kegiatan = Revisi8MasterKegiatanPeer::doSelectOne($c);
    $status = 'CLOSED';
} elseif ($tahap == 'revisi9') {
    $c = new Criteria();
    $c->add(Revisi9MasterKegiatanPeer::KODE_KEGIATAN, $kegiatan_code);
    $c->add(Revisi9MasterKegiatanPeer::UNIT_ID, $unit_id);
    $rs_kegiatan = Revisi9MasterKegiatanPeer::doSelectOne($c);
    $status = 'CLOSED';
} elseif ($tahap == 'revisi10') {
    $c = new Criteria();
    $c->add(Revisi10MasterKegiatanPeer::KODE_KEGIATAN, $kegiatan_code);
    $c->add(Revisi10MasterKegiatanPeer::UNIT_ID, $unit_id);
    $rs_kegiatan = Revisi10MasterKegiatanPeer::doSelectOne($c);
    $status = 'CLOSED';
} elseif ($tahap == 'rkua') {
    $c = new Criteria();
    $c->add(RkuaMasterKegiatanPeer::KODE_KEGIATAN, $kegiatan_code);
    $c->add(RkuaMasterKegiatanPeer::UNIT_ID, $unit_id);
    $rs_kegiatan = RkuaMasterKegiatanPeer::doSelectOne($c);
    $status = 'CLOSED';
} else {
    $c = new Criteria();
    $c->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kegiatan_code);
    $c->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
    $rs_kegiatan = DinasMasterKegiatanPeer::doSelectOne($c);
}
$tabel = 'rincian_detail';

//cek apa sudah ada yang diapprove tim anggaran
$c = new Criteria();
$c->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
$c->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
$c->add(DinasRincianDetailPeer::STATUS_LEVEL, 4, Criteria::GREATER_THAN);
if (DinasRincianDetailPeer::doSelectOne($c)){
    $status_kunci = FALSE;
}else{
    $status_kunci = TRUE;
}


$is_penyelia_bpkad = false;
$penyelia_bpkad = new Criteria();
$penyelia_bpkad->add(PenyeliaBpkadPeer::NAMA, $sf_user->getNamaLogin(), Criteria::EQUAL);
if ($rs_penyelia = PenyeliaBpkadPeer::doSelectOne($penyelia_bpkad)) {
    $is_penyelia_bpkad = true;
}
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Ubah Sub Kegiatan - (<?php echo $rs_kegiatan->getKegiatanId(); ?>)</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="#">Lembar Kerja</a></li>
                  <li class="breadcrumb-item active">Ubah Sub-Kegiatan</li>
              </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<!-- Main content -->
<section class="content">
    <?php include_partial('peneliti/list_messages'); ?>
    <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">       
                <div class="card-body text-center" id="headerrka_<?php echo $sub_id ?>">
                    <div class="btn-group btn-group-justified" role="group">
                        <?php
                        echo link_to_function('<span>' . image_tag('down.png', array('name' => 'img_' . $sub_id, 'id' => 'img_' . $sub_id, 'border' => 0)) . '</span> Melihat Header Kegiatan', 'inKeterangan(' . $sub_id . ',"' . $kode_kegiatan . '","' . $unit_id . '")', array('class' => 'btn btn-outline-info btn-sm text-bold col-xs-4'));
                        if ($tahap != '') {
                            // $penyelia_bpkad = array('winda','judi','tutik','febri','wike','lucky');
                            // if(in_array($sf_user->getNamaLogin(), $penyelia_bpkad)){
                            if($is_penyelia_bpkad == true){
                                echo link_to('<i class="fas fa-print"></i> Format RKA Berdasarkan Subtitle', 'report/TemplateRKA?kode_kegiatan=' . $kode_kegiatan . '&unit_id=' . $unit_id . '&tahap=' . $tahap, array('class' => 'btn btn-outline-info btn-sm text-bold col-xs-4', 'target' => '_blank'));
                                echo link_to('<i class="fas fa-print"></i> Format RKA Berdasarkan Rekening', 'report/printrekening?kode_kegiatan=' . $kode_kegiatan . '&unit_id=' . $unit_id. '&tahap=' . $tahap, array('class' => 'btn btn-outline-info btn-sm text-bold col-xs-4', 'target' => '_blank'));
                                echo link_to('<i class="fas fa-print"></i> Format RKA Tanpa Komponen', 'report/printrekeningtanpakomponen?kode_kegiatan=' . $kode_kegiatan . '&unit_id=' . $unit_id. '&tahap=' . $tahap, array('class' => 'btn btn-outline-info btn-sm text-bold col-xs-4', 'target' => '_blank'));
                            }
                        } else {
                            // $penyelia_bpkad = array('winda','judi','tutik','febri','wike','lucky');
                            // if(in_array($sf_user->getNamaLogin(), $penyelia_bpkad)){
                            if($is_penyelia_bpkad == true){
                                echo link_to('<i class="fas fa-print"></i> Format RKA Berdasarkan Subtitle', 'report/TemplateRKApptk?kode_kegiatan=' . $kode_kegiatan . '&unit_id=' . $unit_id, array('class' => 'btn btn-outline-info btn-sm text-bold col-xs-4', 'target' => '_blank'));
                                echo link_to('<i class="fas fa-print"></i> Format RKA Berdasarkan Rekening', 'report/printrekeningpptk?kode_kegiatan=' . $kode_kegiatan . '&unit_id=' . $unit_id, array('class' => 'btn btn-outline-info btn-sm text-bold col-xs-4', 'target' => '_blank'));
                                echo link_to('<i class="fas fa-print"></i> Format RKA Tanpa Komponen', 'report/printrekeningtanpakomponen?kode_kegiatan=' . $kode_kegiatan . '&unit_id=' . $unit_id, array('class' => 'btn btn-outline-info btn-sm text-bold col-xs-4', 'target' => '_blank'));
                            }
                        }
                        ?>
                    </div>
                </div>
                <div class="card-body table-responsive p-0">
                    <?php if (!$rs_subtitle): ?>
                        <?php echo __('Tidak Ada Subtitle untuk SKPD dengan Kegiatan ini') ?>
                    <?php else: ?>
                        <table class="table table-hover">
                            <thead class="head_peach">
                                <tr>
                                    <th>Subtitle | Rekening</th>
                                    <th>Komponen</th>
                                    <th>Satuan</th>
                                    <th>Koefisien</th>
                                    <th>Harga</th>
                                    <th>Hasil</th>
                                    <th>PPN</th>
                                    <th>Total</th>
                                    <th>Belanja</th>
                                    <?php if(sfConfig::get('app_tahap_edit') != 'murni'): ?>
                                        <th>Catatan Tim Anggaran</th>
                                    <?php endif; ?>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 1;
                                foreach ($rs_subtitle as $subtitle_indikator):
                                    $unit_id = $subtitle_indikator->getUnitId();
                                    $kode_kegiatan = $subtitle_indikator->getKegiatanCode();
                                    $subtitle = $subtitle_indikator->getSubtitle();
                                    $sub_id = $subtitle_indikator->getSubId();
                                    $nama_subtitle = trim($subtitle);
                                    $odd = fmod($i++, 2);
                                    $query = "SELECT * from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "rincian_detail 
                                    where ((note_peneliti is not Null and note_peneliti <> '') or (note_skpd is not Null and note_skpd <> '')) and unit_id = '$unit_id' and kegiatan_code = '$kode_kegiatan' and subtitle ilike '" . str_replace('\'', '%', $nama_subtitle) . "' and status_hapus = false ";
                                    $con = Propel::getConnection();
                                    $statement = $con->prepareStatement($query);
                                    $rs = $statement->executeQuery();
                                    $jml = $rs->getRecordCount();
                                    if ($jml > 0) 
                                    {
                                        ?>
                                        <tr id="subtitle_<?php print $sub_id ?>">
                                            <td style="background: #e8f3f1" colspan="7">
                                                <b><i>
                                                    <?php echo link_to_function(image_tag('b_plus.png', array('name' => 'img_' . $sub_id, 'id' => 'img_' . $sub_id, 'border' => 0)), 'showHideKegiatan(' . $sub_id . ')') . ' ' . $subtitle_indikator->getSubtitle(); ?>
                                                </i></b>
                                                <?php
                                                $prioritas = array('Pilih', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30');
                                                echo form_tag('peneliti/setPrioritas');
                                                echo ' ' . select_tag('prioritas', options_for_select($prioritas, $subtitle_indikator->getPrioritas()), Array('id' => 'prioritasSubtitle'));
                                                echo input_hidden_tag('kode_kegiatan', $kode_kegiatan);
                                                echo input_hidden_tag('unit_id', $unit_id);
                                                echo input_hidden_tag('sub_id', $sub_id);
                                                echo "&nbsp;";
                                                echo submit_tag('Set', array('name' => 'setPrio', 'id' => 'setPrio', 'class' => 'btn btn-outline-primary btn-xs'));
                                                echo '</form>';
                                               ?>
                                            </td>
                                            <td style="background: #e8f3f1" align="right">
                                                <?php
                                                    $query2 = "select sum(nilai_anggaran) as hasil_kali
                                                    from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "rincian_detail
                                                    where kegiatan_code ='$kode_kegiatan' and unit_id='$unit_id' and subtitle ilike '" . str_replace('\'', '%', $nama_subtitle) . "' and status_hapus=FALSE";
                                                    $con = Propel::getConnection();
                                                    $stmt = $con->prepareStatement($query2);
                                                    $t = $stmt->executeQuery();
                                                    while ($t->next()) {
                                                        echo number_format($t->getString('hasil_kali'), 0, ',', '.');
                                                    }
                                                    ?>
                                            </td>
                                            <td style="background: #e8f3f1" colspan="5">&nbsp;</td>
                                        </tr>
                                    <?php } else { ?>
                                        <tr id="subtitle_<?php print $sub_id ?>">
                                            <td colspan="7">
                                                <b><i>
                                                    <?php echo link_to_function(image_tag('b_plus.png', array('name' => 'img_' . $sub_id, 'id' => 'img_' . $sub_id, 'border' => 0)), 'showHideKegiatan(' . $sub_id . ')') . ' ' . $subtitle_indikator->getSubtitle(); ?>
                                                </i></b>
                                                <?php
                                                    $prioritas = array('Pilih', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30');
                                                    echo form_tag('peneliti/setPrioritas');
                                                    echo ' ' . select_tag('prioritas', options_for_select($prioritas, $subtitle_indikator->getPrioritas()), Array('id' => 'prioritasSubtitle'));
                                                    echo input_hidden_tag('kode_kegiatan', $kode_kegiatan);
                                                    echo input_hidden_tag('unit_id', $unit_id);
                                                    echo input_hidden_tag('sub_id', $sub_id);
                                                    echo "&nbsp;";
                                                    echo submit_tag('Set', array('name' => 'setPrio', 'id' => 'setPrio', 'class' => 'btn btn-outline-primary btn-xs'));
                                                    echo '</form>';
                                                ?>
                                            </td>
                                            <td align="right">
                                                <?php
                                                $query2 = "select sum(nilai_anggaran) as hasil_kali
                                                from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "rincian_detail
                                                where kegiatan_code ='$kode_kegiatan' and unit_id='$unit_id' and subtitle ilike '" . str_replace('\'', '%', $nama_subtitle) . "' and status_hapus=FALSE";
                                                $con = Propel::getConnection();
                                                $stmt = $con->prepareStatement($query2);
                                                $t = $stmt->executeQuery();
                                                while ($t->next()) {
                                                    echo number_format($t->getString('hasil_kali'), 0, ',', '.');
                                                }
                                                ?>
                                                    
                                            </td>
                                            <td colspan="5">&nbsp;</td>
                                        </tr> 
                                    <?php } ?>
                                        <tr id="indicator_<?php echo $sub_id ?>" style="display:none;" align="center">
                                            <td colspan="11">
                                                <dt>&nbsp;</dt><dd><b>Mohon Tunggu </b><?php echo image_tag('loading.gif', array('align' => 'absmiddle')) ?></dd>
                                            </td>
                                        </tr>
                                <?php endforeach; ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="7" align="right">Total Keseluruhan:</td>
                                    <td align="right">
                                        <?php
                                        $unit_id = $subtitle_indikator->getUnitId();
                                        $kode_kegiatan = $subtitle_indikator->getKegiatanCode();
                                        $subtitle = $subtitle_indikator->getSubtitle();
                                        $nama_subtitle = trim($subtitle);
                                        $query2 = "select sum(nilai_anggaran) as hasil_kali
                                        from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "rincian_detail
                                        where kegiatan_code ='$kode_kegiatan' and unit_id='$unit_id' and status_hapus=false";
                                        $con = Propel::getConnection();
                                        $stmt = $con->prepareStatement($query2);
                                        $t = $stmt->executeQuery();
                                        while ($t->next()) {
                                            echo "<b>".number_format($t->getString('hasil_kali'), 0, ',', '.')."</b>";
                                        }
                                        ?>      
                                    </td>
                                    <td colspan="5">&nbsp;</td>
                                </tr>
                            </tfoot>
                        </table>
                    <?php endif; ?>
                </div>
            </div>
          </div>
        </div>
    </div>
</section><!-- /.content -->

<script>
    image1 = new Image();
    image1.src = '/<?php echo sfConfig::get('app_default_coding'); ?>/images/b_plus.png';

    image2 = new Image();
    image2.src = '/<?php echo sfConfig::get('app_default_coding'); ?>/images/b_minus.png';

    function showHideKegiatan(id) {
        var row = $('#subtitle_' + id);
        var img = $('#img_' + id);

        if (img) {
            var src = document.getElementById('img_' + id).getAttribute('src');
            var minus = src.indexOf('/<?php echo sfConfig::get('app_default_coding'); ?>/images/b_minus.png');
            if (minus !== -1) {
                src = '/<?php echo sfConfig::get('app_default_coding'); ?>/images/b_plus.png';
            } else {
                src = '/<?php echo sfConfig::get('app_default_coding'); ?>/images/b_minus.png';
            }
            img.attr('src', src);
        }

        if (minus === -1) {
            var kegiatan_id = 'subtitle_' + id;
            var pekerjaans = $('.pekerjaans_' + id);
            var n = pekerjaans.length;

            if (n > 0) {
                for (var i = 0; i < pekerjaans.length; i++) {
                    var pekerjaan = pekerjaans[i];
                    pekerjaan.style.display = 'table-row';
                }
            } else {
                $('#indicator_' + id).show();
                $.ajax({
                    url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/peneliti/getPekerjaansRevisi/id/" + id + "/tahap/<?php echo $tahap; ?>.html",
                    context: document.body
                }).done(function (msg) {
                    $('#subtitle_' + id).after(msg);
                    $('#indicator_' + id).remove();
                });
            }
        } else {
            $('.pekerjaans_' + id).remove();
            minus = -1;
        }
    }

    function showHideKegiatanMasalah(id, kegiatan, unit) {
        var row = $('#subtitle_' + id);
        var img = $('#img_' + id);

        if (img) {
            var src = document.getElementById('img_' + id).getAttribute('src');
            var minus = src.indexOf('/<?php echo sfConfig::get('app_default_coding'); ?>/images/b_minus.png');
            if (minus !== -1) {
                src = '/<?php echo sfConfig::get('app_default_coding'); ?>/images/b_plus.png';
            } else {
                src = '/<?php echo sfConfig::get('app_default_coding'); ?>/images/b_minus.png';
            }
            img.attr('src', src);
        }


        if (minus === -1) {
            var kegiatan_id = 'subtitle_' + id;
            var pekerjaans = $('.pekerjaans_' + id);
            var n = pekerjaans.length;

            if (n > 0) {
                for (var i = 0; i < pekerjaans.length; i++) {
                    var pekerjaan = pekerjaans[i];
                    pekerjaan.style.display = 'table-row';
                }
            } else {
                $('#indicator_' + id).show();
                $.ajax({
                    url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/peneliti/getPekerjaansMasalah/id/" + id + ".html",
                    context: document.body
                }).done(function (msg) {
                    $('#subtitle_0').after(msg);
                    $('#indicator_' + id).remove();
                });
            }
        } else {
            $('.pekerjaans_' + id).remove();
            minus = -1;
        }
    }

    function hapusHeaderKegiatan(id, kegiatan, unit, kodesub) {
        var a = confirm('Apakah anda yakin akan menghapus Header Sub Subtitle kegiatan ini??');
        if (a === true)
        {
            $('.pekerjaans_' + id).remove();
            $.ajax({
                url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/peneliti/hapusHeaderPekerjaans/id/" + id + "/kegiatan/" + kegiatan + "/unit/" + unit + "/no/" + kodesub + ".html"
            }).done();

        }
    }

    function editHeaderKegiatan(id, kegiatan, unit, kodesub) {
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/peneliti/ajaxEditHeaderRevisi/act/editHeader/kodeSub/" + kodesub + "/unitId/" + unit + "/kegiatanCode/" + kegiatan + ".html",
            context: document.body
        }).done(function (msg) {
            $('#header_' + kodesub).before(msg);
            $('#header_' + kodesub).remove();
        });
    }

    function hapusKegiatan(id, kegiatan, unit, no) {
        var a = confirm('Apakah anda yakin akan menghapus kegiatan ini??');
        if (a === true) {
            $('.pekerjaans_' + id).remove();
            $.ajax({
                url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/peneliti/hapusPekerjaans/id/" + id + "/kegiatan/" + kegiatan + "/unit/" + unit + "/no/" + no + ".html"
            }).done();
        }
    }

    function editSubtitle(id) {
        var loading = $('#indicator_' + id);
        loading.show();
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/peneliti/getPekerjaans/id/" + id + ".html",
            context: document.body
        }).done(function (msg) {
            loading.remove();
        });

    }

    function inKeterangan(id, kegiatan, unit) {
        var row = $('headerrka_' + id);
        var img = $('#img_' + id);
        var pekerjaans = $('#depan_' + id);
        if (img) {
            var src = document.getElementById('img_' + id).getAttribute('src');
            var minus = src.indexOf('/<?php echo sfConfig::get('app_default_coding'); ?>/images/up.png');
            if (minus !== -1) {
                src = '/<?php echo sfConfig::get('app_default_coding'); ?>/images/down.png';
            } else {
                src = '/<?php echo sfConfig::get('app_default_coding'); ?>/images/up.png';
            }
            img.attr('src', src);
        }


        if (minus === -1) {
            var kegiatan_id = 'headerrka_' + id;
            var pekerjaans = $('#depan_' + id);
            var n = pekerjaans.length;

            if (n > 0) {
                for (var i = 0; i < pekerjaans.length; i++) {
                    var pekerjaan = pekerjaans[i];
                    pekerjaan.style.display = 'table-row';
                }
            } else {
                $('#indicator').show();
                $.ajax({
                    url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/peneliti/getHeader/id/" + id + "/kegiatan/" + kegiatan + "/unit/" + unit + "/tahap/<?php echo $tahap; ?>.html",
                    context: document.body
                }).done(function (msg) {
                    $('#indicator').remove();
                    $("#headerrka_" + id).after(msg);
                });
            }
        } else {
            $('#depan_' + id).remove();
        }
    }

    function hapusSubKegiatan(id, kegiatan, unit, kodesub) {
        var a = confirm('Apakah anda yakin akan menghapus SUB kegiatan ini??');
        if (a === true) {
            var kegiatan_id = 'subtitle_' + id;
            var pekerjaans = $('.pekerjaans_' + id);
            for (var i = 0; i < pekerjaans.length; i++) {
                var pekerjaan = pekerjaans[i];
                pekerjaan.style.display = 'none';
            }
            $.ajax({
                url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/peneliti/hapusSubPekerjaans/id/" + id + "/kegiatan/" + kegiatan + "/unit/" + unit + "/no/" + kodesub + ".html",
                context: document.body
            }).done();
        }
    }
</script>