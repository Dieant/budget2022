<?php use_helper('I18N', 'Date', 'Url', 'Javascript', 'Form', 'Object', 'Number', 'Validation') ?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>Daftar Isian Rincian Komponen Kegiatan SKPD</h1>
</section>

<!-- Main content -->
<section class="content">
    <?php include_partial('peneliti/list_messages') ?>
    <!-- Default box -->
    <div class="box box-primary box-solid">
        <div class="box-header with-border">
            <h3 class="box-title">Mencari Komponen Berdasarkan Nama Komponen</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body">
            <?php echo form_tag('peneliti/carikomponenRevisi', array('method' => 'get', 'class' => 'form-horizontal')) ?>
            <div class="form-group">
                <label class="col-sm-2 control-label">Nama Komponen</label>
                <div class="col-sm-10">
                    <?php echo input_tag('filters[nama_komponen]', isset($filters['nama_komponen']) ? $filters['nama_komponen'] : null, array('class' => 'form-control', 'placeholder' => 'Nama Komponen')); ?>
                </div>                        
                <?php
                echo input_hidden_tag('kegiatan', $sf_params->get('kegiatan'));
                echo input_hidden_tag('unit', $sf_params->get('unit'));
                ?>
            </div>
            <div id="sf_admin_container">
                <ul class="sf_admin_actions">
                    <li><?php echo submit_tag('cari', 'name=filter class=sf_admin_action_filter') ?></li>
                </ul>
            </div>
            <?php echo '</form>'; ?> 
        </div><!-- /.box-body -->
    </div><!-- /.box -->
    <?php
    $belanja = substr(trim($sf_params->get('rekening')), 0, 5);
    if ($belanja == '5.2.2' || $belanja == '5.2.3') {
        ?>
        <div class="box box-primary box-solid">
            <div class="box-header with-border label-danger">
                <h3 class="box-title">Komponen yang Menyerupai</h3>
            </div>
            <div class="box-body">
                <div id="sf_admin_container" class="table-responsive">
                    <table cellspacing="0" class="sf_admin_list">
                        <tr id="usulan" style="width: 100%">
                            <td colspan="13"><?php echo link_to_function(image_tag('b_plus.png', array('name' => 'img_usulan', 'id' => 'img_usulan', 'border' => 0)), 'showHideKomponen()') . ' '; ?><b>List Komponen Non-Fisik yang Menyerupai</b></td>
                        </tr>
                        <tr id="usulan2" style="width: 100%">
                            <td colspan="13"><?php echo link_to_function(image_tag('b_plus.png', array('name' => 'img_usulan2', 'id' => 'img_usulan2', 'border' => 0)), 'showHideKomponenFisik()') . ' '; ?><b>List Komponen Fisik yang Menyerupai</b></td>
                        </tr>
                        <!--<tr id="usulan3" style="width: 100%">
                            <td colspan="13"><?php echo link_to_function(image_tag('b_plus.png', array('name' => 'img_usulan3', 'id' => 'img_usulan3', 'border' => 0)), 'showHideKomponenTahunLalu()') . ' '; ?><b>Komponen Tahun Lalu</b></td>
                        </tr>-->
                    </table>
                </div>
            </div>
            <div class="box-body"id="indicator" style="display:none;" align="center">
                <dt>&nbsp;</dt><dd><b>Mohon Tunggu </b><?php echo image_tag('loading.gif', array('align' => 'absmiddle')) ?></dd>
            </div>
        </div>
    <?php } ?>

    <!-- Default box -->
    <div class="box box-primary box-solid">
        <div class="box-body">
            <div id="sf_admin_container">
                <div id="sf_admin_content">      
                    <?php echo form_tag('peneliti/baruKegiatanRevisi') ?>
                    <table cellspacing="0" class="sf_admin_list">
                        <thead>
                            <tr>
                                <th><b>Nama</b></th>
                                <th>&nbsp;</th>
                                <th><b>Isian</b></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="sf_admin_row_0" align='right'>
                                <td>Kelompok Belanja</td>
                                <td align="center">:</td>
                                <td align="left">
                                    <?php
                                    $rekening_code = trim($sf_params->get('rekening'));
                                    $belanja_code = substr($rekening_code, 0, 5);
                                    $c = new Criteria();
                                    $c->add(KelompokBelanjaPeer::BELANJA_CODE, $belanja_code);
                                    $rs_belanja = KelompokBelanjaPeer::doSelectOne($c);
                                    if ($rs_belanja) {
                                        echo $rs_belanja->getBelanjaName();
                                    }
                                    ?></td>
                            </tr>
                            <tr class="sf_admin_row_1" align='right'>
                                <td>Kode Rekening</td>
                                <td align="center">:</td>
                                <td align="left">
                                    <?php
                                    $rs_rekening = RekeningPeer::retrieveByPk($rekening_code);
                                    if ($rs_rekening) {
                                        $pajak_rekening = $rs_rekening->getRekeningPpn();
                                        echo $rekening_code . ' ' . $rs_rekening->getRekeningName();
                                    }
                                    ?>
                                </td>
                            </tr>
                            <tr class="sf_admin_row_0" align='right'>
                                <td>Komponen</td>
                                <td align="center">:</td>
                                <td align="left"><?php echo $rs_komponen->getKomponenName() ?></td>
                            </tr>
                            <tr class="sf_admin_row_1" align='right'>
                                <td>Harga</td>
                                <td align="center">:</td>
                                <?php
                                $komponen_id = $rs_komponen->getKomponenId();
                                $komponen_harga = $rs_komponen->getKomponenHarga();
                                if ($rs_komponen->getKomponenTipe() == 'FISIK' && sfConfig::get('app_tahun_default') != '2016') {
                                    $komponen_harga = $rs_komponen->getKomponenHargaBulat();
                                }
                                if ($komponen_harga != floor($komponen_harga)) {
                                    ?>
                                    <td align='left'><?php echo number_format($komponen_harga, 3, ',', '.') ?></td>
                                    <?php
                                } else {
                                    ?>
                                    <td align='left'><?php echo number_format($komponen_harga, 0, ',', '.') ?></td>
                                    <?php
                                }
                                echo input_hidden_tag('harga', $komponen_harga);
                                ?>
                            </tr>
                            <tr class="sf_admin_row_0" align='right'>
                                <td>Satuan</td>
                                <td align="center">:</td>
                                <td align="left"><?php echo $rs_komponen->getSatuan(); ?></td>
                            </tr>
                            <tr class="sf_admin_row_1" align='right'>
                                <td>Pajak</td>
                                <td align="center">:</td>
                                <td align="left"><?php
                                    echo $sf_params->get('pajak') . '%';
                                    echo input_hidden_tag('pajakx', $sf_params->get('pajak'));
                                    ?></td>
                            </tr>
                            <tr class="sf_admin_row_0" align='right'>
                                <td><span style="color:red;">*</span> Subtitle</td>
                                <td align="center">:</td>
                                <td align="left">
                                    <?php
                                    $kode_sub = '';
                                    if ($sf_params->get('subtitle')) {
                                        $kode_sub = $sf_params->get('subtitle');
                                    }

                                    echo select_tag('subtitle', objects_for_select($rs_subtitleindikator, 'getSubId', 'getSubtitle', $kode_sub, 'include_custom=---Pilih Subtitle---'), array('class' => 'js-example-basic-single', 'style' => 'width:100%'));
                                    ?>
                                </td>
                            </tr>
                            <tr class="sf_admin_row_1" align='right'>
                                <td>Sub - Subtitle</td>
                                <td align="center">:</td>
                                <td align="left">
                                    <div id="indicator" style="display:none;" align="center"><dt>&nbsp;</dt><dd><b>Mohon Tunggu </b><?php echo image_tag('loading.gif', array('align' => 'absmiddle')) ?></dd></div>
                                    <?php
                                    if ($sf_params->get('sub')) {
                                        $kode_subsubtitle = $sf_params->get('sub');
                                        $d = new Criteria();
                                        $d->add(DinasRincianSubParameterPeer::KODE_SUB, $kode_subsubtitle);
                                        $rs_rinciansubparameter = DinasRincianSubParameter::doSelectOne($d);
                                        if ($rs_rinciansubparameter) {
                                            echo select_tag('sub', options_for_select(array($rs_rinciansubparameter->getKodeSub() => $rs_rinciansubparameter->getNewSubtitle()), $rs_rinciansubparameter->getKodeSub(), 'include_custom=---Pilih Subtitle Dulu---'), Array('id' => 'sub1', 'class' => 'js-example-basic-single', 'style' => 'width:100%'));
                                        }
                                    } elseif (!$sf_params->get('sub')) {       //select_tag('sub',options_for_select(array(), '','include_custom=---Pilih Subtitle Dulu---'), Array('id'=>'sub1'));
                                        echo select_tag('sub', options_for_select(array(), '', 'include_custom=---Pilih Subtitle Dulu---'), Array('id' => 'sub1', 'class' => 'js-example-basic-single', 'style' => 'width:100%'));
                                    }
                                    ?>

                                </td>
                            </tr>
                            <?php if ($rs_komponen->getKomponenTipe2() == 'KONSTRUKSI' || $rs_komponen->getKomponenTipe2() == 'TANAH' || $rs_komponen->getKomponenTipe2() == 'ATRIBUSI' || $rs_komponen->getKomponenTipe2() == 'PERENCANAAN' || $rs_komponen->getKomponenTipe2() == 'PENGAWASAN'): ?>
                                <tr class="sf_admin_row_0" align='right'>
                                    <td><span style="color:red;">*</span> Kode Akrual</td>
                                    <td align="center">:</td>
                                    <td align="left">
                                        <?php
                                        $akrual_code = '';
                                        if ($sf_params->get('akrual_code')) {
                                            $akrual_code = $sf_params->get('akrual_code');
                                        }

                                        echo select_tag('akrual_code', objects_for_select($rs_akrualcode, 'getAkrualCode', 'getAkrualKodeNama', $akrual_code, 'include_custom=---Pilih Kode Akrual---'), array('class' => 'js-example-basic-single', 'style' => 'width:100%'));
                                        ?>
                                    </td>
                                </tr>
                            <?php elseif ($rs_komponen->getKomponenTipe2() == 'NONKONSTRUKSI'): ?>
                                <tr class="sf_admin_row_0" align='right'>
                                    <td>Kode Akrual</td>
                                    <td align="center">:</td>
                                    <td align="left">
                                        <?php
                                        if ($rs_komponen->getAkrualCode()) {
                                            echo KomponenPeer::buatKodeNamaAkrual($rs_komponen->getAkrualCode());
                                            echo input_hidden_tag('akrual_code', $rs_komponen->getAkrualCode());
                                        } else {
                                            echo input_hidden_tag('akrual_code', '');
                                        }
                                        ?>
                                    </td>
                                </tr>
                            <?php endif; ?>

                            <?php
                            $tipe = $sf_params->get('tipe');
                            //irul 2may 2014 awal-comment  untuk menampilka opsi lokasi pada estimasi pembuatan separator jalan
                            $estimasi_opsi_lokasi = array('');
                            //irul 2may 2014 awal-comment  untuk menampilka opsi lokasi pada estimasi pembuatan separator jalan
                            if (($rs_komponen->getKomponenTipe2() == 'KONSTRUKSI' || $rs_komponen->getKomponenTipe2() == 'TANAH' || $rs_komponen->getKomponenTipe() == 'FISIK' || $rs_komponen->getIsEstFisik()) || in_array($rs_komponen->getKomponenId(), $estimasi_opsi_lokasi) || ($sf_params->get('lokasi'))) {
                                //irul 2may 2014 awal-comment  untuk menampilka opsi lokasi pada estimasi pembuatan separator jalan    
                                ?>
                                <tr class="sf_admin_row_0" align='right' valign="top">
                                    <td>Lokasi</td>
                                    <td align="center">:</td>
                                    <td align="left">
                                        <div class="row col-xs-12 text-bold text-center">
                                            <div class="row col-xs-12">
                                                <hr/>
                                                Silahkan pilih dari data yang sudah ada
                                                <hr/>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="row col-xs-12 text-bold text-center">
                                            <div class="row col-xs-12">
                                                <?php echo select_tag('lokasi_lama', objects_for_select($rs_jalan, 'getLokasi', 'getLokasi', '', 'include_custom=---Pilih Jalan---'), array('class' => 'js-example-basic-multiple', 'style' => 'width:100%', 'multiple' => 'multiple')); ?>                                                
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="row col-xs-12 text-bold text-center">
                                            <div class="row col-xs-12">
                                                <hr/>
                                                Silahkan menambah data lokasi apabila tidak ada dipilihan atas
                                                <hr/>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="row col-xs-12">
                                            <div class="div-induk-lokasi row">
                                                <div>
                                                    <div class="form-group form-group-options col-xs-5 col-sm-5 col-md-5">
                                                        <div class="input-group input-group-option col-xs-12">
                                                            <font class="text-bold">Nama Jalan</font><br/>
                                                            <div class="input-group input-group-sm">
                                                                <span class="input-group-addon">JL.</span>
                                                                <input type="text" name="lokasi_jalan[]" class="form-control lokasi_jalan_isian" placeholder="Nama Jalan (*wajib diisi apabila lokasi berupa Jalan)">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group form-group-options col-xs-1 col-sm-1 col-md-1">
                                                        <div class="input-group input-group-option col-xs-12">
                                                            <font class="text-bold">Gang/Blok/Kavling</font><br/>
                                                            <select class="js-example-basic-single tipe_gang_isian" name="tipe_gang[]">
                                                                <option value="GG">---</option>
                                                                <option value="GG">GANG</option>
                                                                <option value="BLOK">BLOK</option>
                                                                <option value="KAV">KAVLING</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group form-group-options col-xs-2 col-sm-2 col-md-2">
                                                        <div class="input-group input-group-option" style="width: 100%">
                                                            <font class="text-bold">Nama Gang/Blok/Kavling</font><br/>
                                                            <input type="text" name="lokasi_gang[]" class="form-control lokasi_gang_isian" placeholder="Nama Gang/Blok/Kavling">
                                                        </div>
                                                    </div>
                                                    <div class="form-group form-group-options col-xs-2 col-sm-2 col-md-2">
                                                        <div class="input-group input-group-option col-xs-12">
                                                            <font class="text-bold">Nomor Lokasi</font><br/>
                                                            <div class="input-group input-group-sm">
                                                                <span class="input-group-addon">NO.</span>
                                                                <input type="text" name="lokasi_nomor[]" class="form-control lokasi_nomor_isian" placeholder="Nomor">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group form-group-options col-xs-1 col-sm-1 col-md-1">
                                                        <div class="input-group input-group-option col-xs-12">
                                                            <font class="text-bold">RW</font><br/>
                                                            <input type="text" name="lokasi_rw[]" class="form-control lokasi_rw_isian" placeholder="RW">
                                                        </div>
                                                    </div>
                                                    <div class="form-group form-group-options col-xs-1 col-sm-1 col-md-1">
                                                        <div class="input-group input-group-option col-xs-12">
                                                            <font class="text-bold">RT</font><br/>
                                                            <input type="text" name="lokasi_rt[]" class="form-control lokasi_rt_isian" placeholder="RT">
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div>
                                                    <div class="form-group form-group-options col-xs-6 col-sm-6 col-md-6">
                                                        <div class="input-group input-group-option col-xs-12">
                                                            <font class="text-bold">Nama Bangunan/Saluran</font><br/>
                                                            <input type="text" name="lokasi_tempat[]" class="form-control lokasi_tempat_isian" placeholder="Nama Bangunan/Saluran (*wajib diisi apabila lokasi berupa bangunan/saluran/tempat)">                                                            
                                                        </div>
                                                    </div>
                                                    <div class="form-group form-group-options col-xs-6 col-sm-6 col-md-6">
                                                        <div class="input-group input-group-option col-xs-12">
                                                            <font class="text-bold">Keterangan Lokasi</font><br/>
                                                            <input type="text" name="lokasi_keterangan[]" class="form-control lokasi_keterangan_isian" placeholder="Keterangan Lokasi">
                                                            <span class="input-group-addon input-group-addon-add">
                                                                <span class="glyphicon glyphicon-plus"></span>
                                                            </span>
                                                            <span class="input-group-addon input-group-addon-remove">
                                                                <span class="glyphicon glyphicon-remove"></span>
                                                            </span>
                                                        </div>

                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>

                                            </div>
                                        </div>
                                        <!--<div class="row col-xs-12">
                                            <table class="table table-condensed table-bordered">
                                                <thead>
                                                    <tr>
                                                        <td class="text-bold text-center">Kolom</td>
                                                        <td class="text-bold text-center">DiIsi Dengan</td>
                                                        <td class="text-bold text-center">Larangan</td>
                                                        <td class="text-bold text-center">Otomatis Diisi</td>
                                                        <td class="text-bold text-center">Harus Diisi?</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>"Nama Jalan"</td>
                                                        <td>Hanya nama jalan</td>
                                                        <td>Isi tanpa kata "JL."</td>
                                                        <td>Otomatis ditambahkan "JL."</td>
                                                        <td>Iya, tapi dapat digantikan dengan pengisian "Nama Tempat"</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Dropdown Gang/Blok/Kavling</td>
                                                        <td>"Gang", "BLOK", atau "Kavling"</td>
                                                        <td>-</td>
                                                        <td>"---" akan default terpilih "Gang"</td>
                                                        <td>Tidak</td>
                                                    </tr>
                                                    <tr>
                                                        <td>"Nama Gang/Blok/Kavling"</td>
                                                        <td>data nama gang atau nama blok atau nama kavling</td>
                                                        <td>Isi tanpa kata "GG.", "BLOK.", atau "KAV."</td>
                                                        <td>Otomatis ditambahkan "Gang", "BLOK", atau "Kavling"</td>
                                                        <td>Tidak</td>
                                                    </tr>
                                                    <tr>
                                                        <td>"Nomor"</td>
                                                        <td>data nomor lokasi</td>
                                                        <td>Isi tanpa kata "Nomor."</td>
                                                        <td>Otomatis ditambahkan "NO."</td>
                                                        <td>Tidak</td>
                                                    </tr>
                                                    <tr>
                                                        <td>"RW"</td>
                                                        <td>data RW</td>
                                                        <td>Isi tanpa kata "RW."</td>
                                                        <td>Otomatis ditambahkan "RW."</td>
                                                        <td>Tidak</td>
                                                    </tr>
                                                    <tr>
                                                        <td>"RT"</td>
                                                        <td>data RT</td>
                                                        <td>Isi tanpa kata "RT."</td>
                                                        <td>Otomatis ditambahkan "RT."</td>
                                                        <td>Tidak</td>
                                                    </tr>
                                                    <tr>
                                                        <td>"Keterangan Lokasi"</td>
                                                        <td>data keterangan pemerjelas lokasi pekerjaan (seperti sebelah barat)</td>
                                                        <td>-</td>
                                                        <td>-</td>
                                                        <td>Tidak</td>
                                                    </tr>
                                                    <tr>
                                                        <td>"Nama Tempat"</td>
                                                        <td>data nama bangunan atau tempat</td>
                                                        <td>-</td>
                                                        <td>-</td>
                                                        <td>Iya, tapi dapat digantikan dengan pengisian "Nama Jalan"</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>-->
                                        <div class="row col-xs-12">
                                            <?php
//                                        lokasi lama
//                                        echo '<br/>';
//                                        echo textarea_tag('lokasi', $lokasi, 'size=140x3, readonly=true') . submit_tag('cari', 'name=cari') . '<br><font color="magenta">[untuk multi lokasi, tambahkan lokasi dengan klik "cari" lagi]<br></font>';
//                                        lokasi lama         
                                            echo '<br> Usulan dari : ';
                                            echo select_tag('jasmas', objects_for_select($rs_jasmas, 'getKodeJasmas', 'getNama', $sf_params->get('jasmas'), 'include_custom=---Pilih Jasmas Apabila Ada---'), array('class' => 'js-example-basic-single', 'style' => 'width:100%'));
                                            echo '<br />';
                                            if (!$lokasi == '') {
                                                
                                            }
                                            ?>
                                        </div>
                                    </td>
                                </tr>                         
                                <tr class="sf_admin_row_0" align='right'>
                                    <td><span style="color:red;">*</span> Kecamatan</td>
                                    <td align="center">:</td>
                                    <td align="left">
                                        <?php
                                        $kec = new Criteria();
                                        $kec->addAscendingOrderByColumn(KecamatanPeer::NAMA);
                                        $rs_kec = KecamatanPeer::doSelect($kec);
                                        $nama_kecamatan_kel = $rs_kec;
                                        echo select_tag('kecamatan', objects_for_select($nama_kecamatan_kel, 'getId', 'getNama', '', 'include_custom=---Pilih Kecamatan---'), array('class' => 'js-example-basic-single', 'style' => 'width:100%'));
                                        ?>
                                        <span style="color:red;">* untuk Komponen FISIK, diharuskan untuk mengisi data Kecamatan.</span>
                                    </td>
                                </tr>				
                                <tr class="sf_admin_row_1" align='right'>
                                    <td>Kelurahan</td>
                                    <td align="center">:</td>
                                    <td align="left">
                                        <div id="indicator" style="display:none;" align="center"><dt>&nbsp;</dt><dd><b>Mohon Tunggu </b><?php echo image_tag('loading.gif', array('align' => 'absmiddle')) ?></dd></div>
                                        <?php
                                        echo select_tag('kelurahan', options_for_select(array(), '', 'include_custom=---Pilih Kecamatan Dulu---'), Array('id' => 'kelurahan1', 'class' => 'js-example-basic-single', 'style' => 'width:100%'));
                                        ?>
                                        <span style="color:red;">* untuk Komponen FISIK, diharuskan untuk mengisi data Kelurahan.</span>
                                    </td>
                                </tr>
                                <!-- irul untuk REVISI -->
                                <!-- irul 18maret 2014 - simpan catatan -->
                                <?php if (sfConfig::get('app_fasilitas_bukaCatatanPergeseran') == 'buka') { ?>
                                    <tr class="sf_admin_row_0" align='right' valign="top">
                                        <td><span style="color:red;">*</span> Catatan Pergeseran Anggaran</td>
                                        <td align="center">:</td>
                                        <td align='left'>
                                            <?php echo textarea_tag('catatan', $sf_params->get('catatan'), array('style' => 'width:550px', 'size' => '100px')) ?><br/>
                                            <i style="color: red">Minimal 15 Karakter</i>
                                        </td>
                                    </tr>
                                    <?php
                                } else {
                                    $c = new Criteria();
                                    $c->add(DinasMasterKegiatanPeer::UNIT_ID, $sf_params->get('unit'));
                                    $c->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $sf_params->get('kegiatan'));
                                    $rs_kegiatan = DinasMasterKegiatanPeer::doSelectOne($c);
                                    if ($rs_kegiatan->getTahap() == 'murni') {
                                        ?>
                                        <tr class="sf_admin_row_0" align='right' valign="top">
                                            <td><span style="color:red;">*</span> catatan usulan Anggaran<br/>(Catatan SKPD)</td>
                                            <td align="center">:</td>
                                            <td align='left'>
                                                <?php echo textarea_tag('catatan', $sf_params->get('catatan'), array('style' => 'width:550px', 'size' => '100px')) ?><br/>
                                                <i style="color: red">Mohon diisi alasan usulan anggaran (Minimal 15 Karakter)</i>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                                <!-- irul 18maret 2014 - simpan catatan -->
                                <!-- irul untuk REVISI -->
                                <?php
                            } else {
                                ?>
                                <tr class="sf_admin_row_1" align='right' valign="top">
                                    <td>Keterangan</td>
                                    <td align="center">:</td>
                                    <td align='left'><?php echo input_tag('keterangan', $sf_params->get('keterangan')) ?></td>
                                </tr>                    
                                <!-- irul untuk REVISI -->
                                <!-- irul 18maret 2014 - simpan catatan -->
                                <?php if (sfConfig::get('app_fasilitas_bukaCatatanPergeseran') == 'buka') { ?>
                                    <tr class="sf_admin_row_0" align='right' valign="top">
                                        <td><span style="color:red;">*</span> Catatan Pergeseran Anggaran</td>
                                        <td align="center">:</td>
                                        <td align='left'>
                                            <?php echo textarea_tag('catatan', $sf_params->get('catatan'), array('style' => 'width:550px', 'size' => '100px')) ?><br/>
                                            <i style="color: red">Minimal 15 Karakter</i>
                                        </td>
                                    </tr>
                                <?php } 
                                     ?>
                                <!-- irul 18maret 2014 - simpan catatan -->
                                <!-- irul untuk REVISI -->
                                <?php
                            }
                            ?>
                            <?php if ($sf_params->get('unit') == '1800' || $sf_params->get('unit') == '0300') {
                                ?>
                                <tr class="sf_admin_row_0" align='right' valign="top">
                                    <td>Komponen BLUD</td>
                                    <td align="center">:</td>
                                    <td align="left"><?php echo checkbox_tag('blud') ?><font style="color: green"> *Centang jika termasuk komponen BLUD</font> </td>
                                </tr>
                            <?php }
                            ?>
                            <tr class="sf_admin_row_0" align='right' valign="top">
                                <td>Komponen Musrenbang</td>
                                <td align="center">:</td>
                                <td align="left"><?php echo checkbox_tag('musrenbang') ?><font style="color: green"> *Centang jika termasuk komponen Musrenbang</font> </td>
                            </tr>    
                            <tr class="sf_admin_row_0" align='right' valign="top">
                                <td>Komponen Hibah KUA-PPAS</td>
                                <td align="center">:</td>
                                <td align="left"><?php echo checkbox_tag('hibah') ?><font style="color: green"> *Centang jika termasuk komponen  Hibah KUA-PPAS</font> </td>
                            </tr>    
                            <tr class="sf_admin_row_1" align='right' valign="top">
                                <td><span style="color:red;">*</span>Volume</td>
                                <td align="center">:</td>
                                <td align="left">
                                    <?php
                                    $keterangan_koefisien = $sf_params->get('keterangan_koefisien');
                                    $pisah_kali = explode('X', $keterangan_koefisien);
                                    for ($i = 0; $i < 4; $i++) {
                                        $satuan = '';
                                        $volume = '';
                                        $nama_input = 'vol' . ($i + 1);
                                        $nama_pilih = 'volume' . ($i + 1);

                                        if (!empty($pisah_kali[$i])) {
                                            $pisah_spasi = explode(' ', $pisah_kali[$i]);
                                            $j = 0;

                                            for ($s = 0; $s < count($pisah_spasi); $s++) {
                                                if ($pisah_spasi[$s] != NULL) {
                                                    if ($j == 0) {
                                                        $volume = $pisah_spasi[$s];
                                                        $j++;
                                                    } elseif ($j == 1) {
                                                        $satuan = $pisah_spasi[$s];
                                                        $j++;
                                                    } else {
                                                        $satuan.=' ' . $pisah_spasi[$s];
                                                    }
                                                }
                                            }
                                        }
                                        if ($i !== 3) {
                                            echo input_tag($nama_input, $volume, array('onChange' => 'hitungTotal()')) . ' ' . select_tag($nama_pilih, objects_for_select($rs_satuan, 'getSatuanName', 'getSatuanName', $satuan, 'include_custom=---Pilih Satuan--'), array('class' => 'js-example-basic-single', 'style' => 'width:20%')) . '<br />  X <br />';
                                        } else {
                                            echo input_tag($nama_input, $volume, array('onChange' => 'hitungTotal()')) . ' ' . select_tag($nama_pilih, objects_for_select($rs_satuan, 'getSatuanName', 'getSatuanName', $satuan, 'include_custom=---Pilih Satuan--'), array('class' => 'js-example-basic-single', 'style' => 'width:20%'));
                                        }
                                    }
                                    ?></td>
                            </tr>
                            <tr class="sf_admin_row_1" align='right' valign="top">
                                <td>Total</td>
                                <td align="center">:</td>
                                <td align="left"><?php echo input_tag('total', '', array('readonly' => 'true')) ?></td>
                            </tr>
                            <?php
                            //centang2................
                            $sub_koRek = substr($rekening_code, 0, 5);
                            //if($sub_koRek=='5.2.3'):
                            if ($sub_koRek == '5.2.3' || $sub_koRek == '5.2.2' || $rekening_code == '5.2.2.01.01' || $rekening_code == '5.2.2.01.03' || $rekening_code == '5.2.2.01.10' || $rekening_code == '5.2.2.01.10' || $rekening_code == '5.2.2.01.14' || $rekening_code == '5.2.2.01.15' || $rekening_code == '5.2.2.01.16' || $rekening_code == '5.2.2.01.18' || $rekening_code == '5.2.2.01.19' || $rekening_code == '5.2.2.02.01' || $rekening_code == '5.2.2.02.02' || $rekening_code == '5.2.2.02.05' || $rekening_code == '5.2.2.19.01' || $rekening_code == '5.2.2.19.02' || $rekening_code == '5.2.2.19.03' || $rekening_code == '5.2.2.19.04'):
                                //irul 2may 2014 awal-comment  untuk menampilka opsi lokasi pada estimasi pembuatan separator jalan
                                $estimasi_opsi_lokasi = array('');
                                //irul 2may 2014 awal-comment  untuk menampilka opsi lokasi pada estimasi pembuatan separator jalan
                                if ($sub_koRek == '5.2.3' || ($rs_komponen->getKomponenTipe2() == 'KONSTRUKSI' || $rs_komponen->getKomponenTipe2() == 'TANAH' || $rs_komponen->getKomponenTipe() == 'FISIK' || $est_fisik) || in_array($rs_komponen->getKomponenId(), $estimasi_opsi_lokasi)) {
                                    ?>
                                    <tr class="sf_admin_row_0" align='right' valign="top">
                                        <td>Komponen Penyusun</td>
                                        <td align="center">:</td>
                                        <td align="left">
                                            <div id="komsun">
                                                <table cellspacing="0" class="sf_admin_list">
                                                    <thead>
                                                        <tr>
                                                            <th></th>
                                                            <th>Nama Komponen</th>
                                                            <th>Harga Satuan</th>
                                                            <th>Koefisien</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                        $row = 0;
                                                        foreach ($rs_penyusun as $penyusun):
                                                            $kodeChekBox = str_replace(".", "_", $penyusun->getKomponenId());
                                                            $volPenyu = 'volPenyu_' . $kodeChekBox;
                                                            ?>
                                                            <tr class="sf_admin_row_<?php echo $row ?>">
                                                                <td width="4%" align="center"a><?php echo checkbox_tag('penyusun[]', $penyusun->getKomponenId()); ?></td>
                                                                <td><?php echo $penyusun->getKomponenName(); ?></td>
                                                                <td align="right"><?php echo $penyusun->getKomponenHarga(); //number_format($penyusun->getKomponenHarga(),0,',','.');                                      ?></td>
                                                                <td><?php
                                                                    echo input_tag($volPenyu, '', array('size' => 15, 'maxlength' => 15)) . ' ' . $penyusun->getSatuan();
                                                                    echo input_hidden_tag('bisma', 'seru')
                                                                    ?></td>

                                                            </tr>
                                                            <?php
                                                            if ($row == 0) {
                                                                $row = 1;
                                                            } elseif ($row == 1) {
                                                                $row = 0;
                                                            }
                                                        endforeach;
                                                        ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                }
                            endif;
                            ?>
                        </tbody>
                        <tfoot>
                            <tr class="sf_admin_row_0" align='right' valign="top">
                                <td>&nbsp; </td>
                                <td>
                                    <?php
                                    echo input_hidden_tag('id_login', $sf_user->getNamaUser());
                                    echo input_hidden_tag('kegiatan', $sf_params->get('kegiatan'));
                                    echo input_hidden_tag('unit', $sf_params->get('unit'));
                                    echo input_hidden_tag('id', $sf_params->get('komponen'));
                                    echo input_hidden_tag('pajak', $sf_params->get('pajak'));
                                    echo input_hidden_tag('tipe', $sf_params->get('tipe'));
                                    echo input_hidden_tag('rekening', $sf_params->get('rekening'));
                                    echo input_hidden_tag('referer', $sf_request->getAttribute('referer'));
                                    ?>
                                </td>
                                <td><?php echo submit_tag('simpan', 'name=simpan') . ' ' . button_to('kembali', '#', array('onClick' => "javascript:history.back()")) ?></li></td>
                            </tr>
                        </tfoot>
                    </table>
                    </form>

                </div>
            </div>
        </div>
    </div>
</section>
<style>
    .select2-container-multi .select2-choices .select2-search-choice {
        padding: 3px 5px 3px 18px;
        margin: 3px 0 3px 5px;
        line-height: 20px;
    }
</style>
<script>
    function showHideKomponen() {
        var row = $('#usulan');
        var img = $('#img_usulan');

        if (img) {
            var src = document.getElementById('img_usulan').getAttribute('src');
            var minus = src.indexOf('/<?php echo sfConfig::get('app_default_coding'); ?>/images/b_minus.png');
            if (minus !== -1) {
                src = '/<?php echo sfConfig::get('app_default_coding'); ?>/images/b_plus.png';
            } else {
                src = '/<?php echo sfConfig::get('app_default_coding'); ?>/images/b_minus.png';
            }
            img.attr('src', src);
        }


        if (minus === -1) {
            var usulan_id = 'usulan';
            var mirip = $('.mirip');
            var n = mirip.length;

            if (n > 0) {
                for (var i = 0; i < mirip.length; i++) {
                    var list_mirip = mirip[i];
                    list_mirip.style.display = 'table-row';
                }
            } else {
                $('#indicator').show();
                $.ajax({
                    url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/peneliti/getKomponenSerupa/unit/<?php echo $sf_params->get('unit'); ?>/komponen/<?php echo $sf_params->get('komponen'); ?>/rekening/<?php echo $sf_params->get('rekening'); ?>.html",
                    context: document.body
                }).done(function (msg) {
                    $('#usulan').after(msg);
                    $('#indicator').remove();
                });
            }
        } else {
            $('.mirip').remove();
            minus = -1;
        }
    }
    function showHideKomponenFisik() {
        var row = $('#usulan2');
        var img = $('#img_usulan2');

        if (img) {
            var src = document.getElementById('img_usulan2').getAttribute('src');
            var minus = src.indexOf('/<?php echo sfConfig::get('app_default_coding'); ?>/images/b_minus.png');
            if (minus !== -1) {
                src = '/<?php echo sfConfig::get('app_default_coding'); ?>/images/b_plus.png';
            } else {
                src = '/<?php echo sfConfig::get('app_default_coding'); ?>/images/b_minus.png';
            }
            img.attr('src', src);
        }


        if (minus === -1) {
            var usulan_id = 'usulan2';
            var mirip = $('.mirip2');
            var n = mirip.length;

            if (n > 0) {
                for (var i = 0; i < mirip.length; i++) {
                    var list_mirip = mirip[i];
                    list_mirip.style.display = 'table-row';
                }
            } else {
                $('#indicator').show();
                $.ajax({
                    url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/peneliti/getKomponenSerupaFisik/komponen/<?php echo $sf_params->get('komponen'); ?>.html",
                    context: document.body
                }).done(function (msg) {
                    $('#usulan2').after(msg);
                    $('#indicator').remove();
                });
            }
        } else {
            $('.mirip2').remove();
            minus = -1;
        }
    }
//    function showHideKomponenTahunLalu() {
//        var row = $('#usulan3');
//        var img = $('#img_usulan3');
//
//        if (img) {
//            var src = document.getElementById('img_usulan3').getAttribute('src');
//            var minus = src.indexOf('/<?php //echo sfConfig::get('app_default_coding'); ?>/images/b_minus.png');
//            if (minus !== -1) {
//                src = '/<?php //echo sfConfig::get('app_default_coding'); ?>/images/b_plus.png';
//            } else {
//                src = '/<?php //echo sfConfig::get('app_default_coding'); ?>/images/b_minus.png';
//            }
//            img.attr('src', src);
//        }
//
//
//        if (minus === -1) {
//            var usulan_id = 'usulan3';
//            var mirip = $('.mirip3');
//            var n = mirip.length;
//
//            if (n > 0) {
//                for (var i = 0; i < mirip.length; i++) {
//                    var list_mirip = mirip[i];
//                    list_mirip.style.display = 'table-row';
//                }
//            } else {
//                $('#indicator').show();
//                $.ajax({
//                    url: "/<?php //echo sfConfig::get('app_default_coding'); ?>/index.php/peneliti/getKomponenTahunLalu/komponen/<?php echo $sf_params->get('komponen'); ?>.html",
//                    context: document.body
//                }).done(function (msg) {
//                    $('#usulan3').after(msg);
//                    $('#indicator').remove();
//                });
//            }
//        } else {
//            $('.mirip3').remove();
//            minus = -1;
//        }
//    }
    $(document).ready(function () {
        $(".js-example-basic-single").select2();
        $(".js-example-basic-multiple").select2();
    });
    $(function () {
        $(document).on('click', 'div.form-group-options .input-group-addon-add', function () {
            var divIluminati = $(this).parents('.div-induk-lokasi');
            var sDivIluminatiHtml = divIluminati.html();
            var sInputGroupClasses = divIluminati.attr('class');
            //Gambiarra pra nao ficar criando mil inputs
            if (divIluminati.next().length >= 1)
                return;
            divIluminati.parent().append('<div class="' + sInputGroupClasses + '">' + sDivIluminatiHtml + '</div>');
        });
        $(document).on('click', 'div.form-group-options .input-group-addon-remove', function () {
            var divIluminati = $(this).parents('.div-induk-lokasi');
            divIluminati.remove();
        });
    });
    $("#subtitle").change(function () {
        var id = $(this).val();
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/peneliti/pilihsubx/kegiatan_code/<?php echo $sf_params->get('kegiatan') ?>/unit_id/<?php echo $sf_params->get('unit') ?>/b/" + id + ".html",
            context: document.body
        }).done(function (msg) {
            $('#sub1').html(msg);
        });
    });
    $("#kecamatan").change(function () {
        var id = $(this).val();
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/peneliti/pilihKelurahan/b/" + id + ".html",
            context: document.body
        }).done(function (msg) {
            $('#kelurahan1').html(msg);
        });
    });
    function hitungTotal() {
        var harga = $('#harga').val();
        var pajakx = $('#pajakx').val();
        var vol1 = $('#vol1').val();
        var vol2 = $('#vol2').val();
        var vol3 = $('#vol3').val();
        var vol4 = $('#vol4').val();
        var volume;
        var hitung;


        if (vol1 !== '' || vol2 !== '' || vol3 !== '' || vol4 !== '') {
            if (vol2 === '') {
                vol2 = 1;
                volume = vol1 * vol2;
            } else if (vol2 !== '') {
                volume = vol1 * vol2;
            }
            if (vol3 === '') {
                vol3 = 1;
                volume = volume * vol3;
            } else if (vol3 !== '') {
                volume = vol1 * vol2 * vol3;
            }
            if (vol4 === '') {
                vol4 = 1;
                volume = volume * vol4;
            } else if (vol4 !== '') {
                volume = vol1 * vol2 * vol3 * vol4;
            }
        }

        if (pajakx == 10) {
            hitung = harga * volume * 1.1;
        } else if (pajakx == 0) {
            hitung = harga * volume * 1;
        }

        $('#total').val(hitung);

    }
</script>