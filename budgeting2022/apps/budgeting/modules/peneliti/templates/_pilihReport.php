<?php
echo select_tag('report', options_for_select(array('rd' => 'Laporan Per Dinas',
    'rdk' => 'Laporan Per Dinas Per Kegiatan',
    'rdkpk' => 'Laporan Per Dinas Per Kegiatan Penunjang Kinerja',
    //'pd'=>'Laporan Per Dinas Per Kegiatan Per Belanja',
    'rb' => 'Laporan Per Belanja',
    'rbr' => 'Laporan Per Belanja Per Rekening',
    'rpd' => 'Laporan Per Program Per Dinas',
    'rp13' => 'Laporan Per Program P.13',
    'rp' => 'Laporan Per Program',
    'rpk' => 'Laporan Per Program Per Kegiatan',
    'rdks' => 'Laporan Per Dinas Per Kegiatan Per Subtitle',
    //'rpks'=>'Laporan Per Program Per Kegiatan Per Subtitle',
    //'rrk'=>'Laporan Per Rekening Per Komponen',
    //'pr'=>'Laporan Per Rekening Per Komponen Per Kegiatan',
    'rdsr' => 'Laporan Per Dinas Per Subtitle Per Rekening',
                // 'test'=>'tes',
                ), '', array('include_custom' => 'Pilih Jenis Report')), Array('id' => 'combo_pilih', 'onChange' => remote_function(Array('update' => 'isi', 'url' => 'peneliti/tampilReport', 'with' => "'b='+this.options[this.selectedIndex].value", 'loading' => "Element.show('indicator');Element.hide('isi');Element.hide('isi_dalam');", 'complete' => "Element.hide('indicator');Element.show('isi');" . visual_effect('highlight', 'isi')))));
?>
<hr style="margin-bottom: 10px">