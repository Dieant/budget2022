<?php
use_helper('I18N', 'Url', 'Javascript', 'Form', 'Object');
if ($act == 'setuju') {
    echo link_to_function(image_tag('/images/down.png', array('alt' => __('Tidak Setuju'), 'title' => __('Tidak Setuju'), 'class' => 'btn btn-sm', 'height' => '26px')), 'execVerifikasiLokasi(' . $id_lokasi . ',"tidak")');
} elseif ($act == 'tidak') {
    echo link_to_function(image_tag('/images/up.png', array('alt' => __('Setuju'), 'title' => __('Setuju'), 'class' => 'btn btn-sm', 'height' => '26px')), 'execVerifikasiLokasi(' . $id_lokasi . ',"setuju")');
}
?>