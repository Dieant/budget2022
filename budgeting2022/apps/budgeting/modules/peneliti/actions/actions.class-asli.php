<?php

/**
 * peneliti actions.
 *
 * @package    budgeting
 * @subpackage peneliti
 * @author     pemkot
 * @version    SVN: $Id: actions.class.php 2288 2006-10-02 15:22:13Z fabien $
 */
class penelitiActions extends autopenelitiActions {

    public function executeLockhargadasar() {
        $sql = "update " . sfConfig::get('app_default_schema') . ".rincian_detail set status_lelang='" .
                $this->act = $this->getRequestParameter('act') . "' where unit_id='" .
                $this->act = $this->getRequestParameter('unit_id') . "' and kegiatan_code='" .
                $this->act = $this->getRequestParameter('kegiatan_code') . "' and detail_no= " .
                $this->act = $this->getRequestParameter('detail_no');

        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($sql);
        budgetLogger::log('Mengunci harga dasar untuk lelang pada unit id =' . $this->act = $this->getRequestParameter('unit_id') . ' kode kegiatan=' . $this->act = $this->getRequestParameter('kegiatan_code') .
                ' dan detail no=' . $this->act = $this->getRequestParameter('detail_no'));
        $stmt->executeQuery();

        $this->no = $this->getRequestParameter('detail_no');
        $this->unit_id = $this->getRequestParameter('unit_id');
        $this->id = $this->getRequestParameter('id');
        $this->kegiatan = $this->getRequestParameter('kegiatan_code');

        $this->ajax_id = $this->getRequestParameter('ajax_id');
        $this->act = $this->getRequestParameter('act');
    }

    public function executeBataltarikdelivery() {
        if ($this->getRequestParameter('edit') == md5('bataltarik')) {
            $unit_id = $this->getRequestParameter('unit_id');
            $kode_kegiatan = $this->getRequestParameter('kegiatan_code');
            $detail_no = $this->getRequestParameter('detail_no');

            $query = "delete
				from " . sfConfig::get('app_default_schema') . ".rincian_detail r
				where unit_id='" . $unit_id . "' and kegiatan_code='$kode_kegiatan' and detail_no=$detail_no";
            //print_r($query);exit;
            $con = Propel::getConnection();
            budgetLogger::log('Membatalkan tarik delivery pada unit_id=' . $unit_id . ' dan kegiatan_code=' . $kode_kegiatan);
            $stmt = $con->prepareStatement($query);
            $rs1 = $stmt->executeQuery();


            $query = "insert into " . sfConfig::get('app_default_schema') . ".rincian_detail select *
				from " . sfConfig::get('app_default_schema') . ".rincian_detail_bp
				where unit_id='" . $unit_id . "' and kegiatan_code='$kode_kegiatan' and detail_no=$detail_no";
            //print_r($query);exit;
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs1 = $stmt->executeQuery();

            $query = "update
				" . sfConfig::get('app_default_schema') . ".rincian_detail set status_lelang='edelivery'
				where unit_id='" . $unit_id . "' and kegiatan_code='$kode_kegiatan' and detail_no=$detail_no";
            //print_r($query);exit;
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs1 = $stmt->executeQuery();

            $this->setFlash('berhasil', 'berhasil membatalkan proses menarik dari e-delivery');
            return $this->redirect('peneliti/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kode_kegiatan);
        }
    }

    public function executeTarikdelivery() {
        if ($this->getRequestParameter('edit') == md5('tarik')) {
            $unit_id = $this->getRequestParameter('unit_id');
            $kode_kegiatan = $this->getRequestParameter('kegiatan_code');
            $detail_no = $this->getRequestParameter('detail_no');

            $query = "select detail_no,volume,komponen_harga_awal,pajak,keterangan_koefisien
				from " . sfConfig::get('app_default_schema') . ".rincian_detail r
				where unit_id='" . $unit_id . "' and kegiatan_code='$kode_kegiatan' and detail_no=$detail_no";
            //print_r($query);exit;
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs1 = $stmt->executeQuery();
            while ($rs1->next()) {

                $detail_no = $rs1->getString('detail_no');
                $komponen_harga_awal = $rs1->getString('komponen_harga_awal');

                $keterangan_koefisien = $rs1->getString('keterangan_koefisien');
                $pajak = $rs1->getString('pajak');
                $volume = $rs1->getString('volume');
                $total = $komponen_harga_awal * $volume * (100 + $pajak) / 100;
                $komponens = simplexml_load_file('http://delivery.surabaya2excellence.or.id/2010/delivery_dev.php/sinkronisasi/sinkron_budgeting/kode/' . $unit_id . '-' . $kode_kegiatan . '-10-' . $detail_no);
//					
                $tot_rp = 0;
                $tot_vol = 0;
                $nomor_lelang = '';
                foreach ($komponens->komponen as $komponen) {
                    $tot_rp = (float) $tot_rp + (float) ((float) $komponen['harga_kontrak'] * (float) $komponen['volume_kontrak']);
                    $tot_vol = $tot_vol + $komponen['volume_kontrak'];
                    $pajak = $komponen['pajak'];
                }
                //print_r($tot_rp);exit;

                if ($tot_rp > 0) {

                    /* if(($tot_vol/$volume)> 0) $status_lelang='lelang_25';
                      if(($tot_vol/$volume) == 1) $status_lelang='lelang_100';
                      if(($tot_vol/$volume)< 1 and ($tot_vol/$vol)>= 0.75) $status_lelang='lelang_75';
                      if(($tot_vol/$volume)< 0.75 and ($tot_vol/$vol)>= 0.5) $status_lelang='lelang_50';
                     */

                    $status_lelang = 'lock edelivery ' . intval(10 * $tot_vol / $volume);

                    $query2 = "update " . sfConfig::get('app_default_schema') . ".rincian_detail
						set volume=1, keterangan_koefisien='1 Paket',satuan='Paket', komponen_harga_awal=$tot_rp,
						status_lelang='$status_lelang',pajak=$pajak,total_semula=$total,volume_semula=$volume, 
						harga_semula=$komponen_harga_awal, koefisien_semula='$keterangan_koefisien'
						where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and detail_no=$detail_no";


                    //print_r($query2);exit;
                    $con2 = Propel::getConnection();
                    $stmt2 = $con2->prepareStatement($query2);
                    budgetLogger::log('Berhasil menarik dari delivery pada unit_id=' . $unit_id . ' dan kode_kegiatan' . $kode_kegiatan);
                    $stmt2->executeQuery();
                }
            }
            $this->setFlash('berhasil', 'berhasil menarik dari e-delivery');
            return $this->redirect('peneliti/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kode_kegiatan);
        }
    }

    public function executeTarikdeliveryall() {
        $unit_id = $this->getRequestParameter('unit_id');
        $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');

        $query = "select detail_no,volume,komponen_harga_awal,pajak,keterangan_koefisien
				from " . sfConfig::get('app_default_schema') . ".rincian_detail r
				where unit_id='" . $unit_id . "' and kegiatan_code='$kode_kegiatan' 
				order by detail_no";
        //print_r($query);exit;
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query);
        $rs1 = $stmt->executeQuery();
        while ($rs1->next()) {

            $detail_no = $rs1->getString('detail_no');
            $komponen_harga_awal = $rs1->getString('komponen_harga_awal');

            $keterangan_koefisien = $rs1->getString('keterangan_koefisien');
            $pajak = $rs1->getString('pajak');
            $volume = $rs1->getString('volume');
            $total = $komponen_harga_awal * $volume * (100 + $pajak) / 100;

            /* $query_lelang="
              select rd.unit_id, rd.kegiatan_code, rd.detail_no, rd.subtitle, rd.komponen_name,
              de.kode_detail_kegiatan, dk.kontrak_id,dk.harga_kontrak,dk.volume_kontrak,dk.pajak,
              sum(rd.volume * rd.komponen_harga_awal * (100 + rd.pajak) / 100) as nilai_budgeting, sum(dk.harga_kontrak*dk.volume_kontrak) as nilai_kontrak
              from edelivery.detail_komponen dk, edelivery.kontraks k, eproject.detail_kegiatan de, ". sfConfig::get('app_default_schema') .".rincian_detail rd
              where rd.unit_id='".$unit_id."' and rd.kegiatan_code='$kode_kegiatan' and rd.detail_no=$detail_no and dk.kontrak_id=k.id and k.metode=3 and de.id=dk.detail_kegiatan_id and
              substring(de.kode_detail_kegiatan,1,4)=rd.unit_id and substring(de.kode_detail_kegiatan,6,4)=rd.kegiatan_code and
              (substring(de.kode_detail_kegiatan,11))::integer=rd.detail_no and (substring(de.kode_detail_kegiatan,11))::integer=$detail_no
              group by  rd.unit_id,rd.kegiatan_code,rd.detail_no,rd.subtitle,rd.komponen_name,rd.kode_sub,de.kode_detail_kegiatan,dk.kontrak_id,dk.harga_kontrak,dk.volume_kontrak,dk.pajak
              having  sum(dk.harga_kontrak*dk.volume_kontrak)>0
              order by rd.unit_id,rd.kegiatan_code,rd.detail_no
              ";

              $con = Propel::getConnection();
              $stmt_lelang = $con->prepareStatement($query_lelang);
              $rs_lelang = $stmt_lelang->executeQuery();
              print_r($query_lelang.'<br>');
              //print_r($rs_lelang);exit;
              $tot_rp=0;
              $tot_vol=0;
              $nomor_lelang='';
              while($rs_lelang->next())
              {
              $harga_kontrak=$rs_lelang->getString('harga_kontrak');
              $volume_kontrak=$rs_lelang->getString('volume_kontrak');
              $pajak_kontrak=$rs_lelang->getString('pajak');
              $tot_rp=$tot_rp+ ($harga_kontrak*$volume_kontrak);
              $tot_vol=$tot_vol+ $volume_kontrak;
              $pajak=$pajak_kontrak;
              } */
            $komponens = simplexml_load_file('http://delivery.surabaya2excellence.or.id/2010/delivery_dev.php/sinkronisasi/sinkron_budgeting/kode/' . $unit_id . '-' . $kode_kegiatan . '-10-' . $detail_no);
            $tot_rp = 0;
            $tot_vol = 0;
            $nomor_lelang = '';
            foreach ($komponens->komponen as $komponen) {
                $tot_rp = $tot_rp + ($komponen['harga_kontrak'] * $komponen['volume_kontrak']);
                $tot_vol = $tot_vol + $komponen['volume_kontrak'];
                $pajak = $komponen['pajak'];
            }


            if ($tot_rp > 0) {
                //	print_r($tot_rp);
                /* if(($tot_vol/$volume)> 0) $status_lelang='lelang_25';
                  if(($tot_vol/$volume) == 1) $status_lelang='lelang_100';
                  if(($tot_vol/$volume)< 1 and ($tot_vol/$vol)>= 0.75) $status_lelang='lelang_75';
                  if(($tot_vol/$volume)< 0.75 and ($tot_vol/$vol)>= 0.5) $status_lelang='lelang_50';
                 */

                $status_lelang = 'lock edelivery ' . intval(10 * $tot_vol / $volume);

                $query2 = "update " . sfConfig::get('app_default_schema') . ".rincian_detail 
						set volume=1, keterangan_koefisien='1 Paket',satuan='Paket', komponen_harga_awal=$tot_rp,
						status_lelang='$status_lelang',pajak=$pajak,total_semula=$total,volume_semula=$volume, 
						harga_semula=$komponen_harga_awal, koefisien_semula='$keterangan_koefisien'
						where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and detail_no=$detail_no and 
						status_lelang like 'lock edelivery '";
                //print_r($query2);exit;
                $con2 = Propel::getConnection();
                $stmt2 = $con2->prepareStatement($query2);
                $stmt2->executeQuery();

                $query2 = "update " . sfConfig::get('app_default_schema') . ".rincian_detail 
						set status_lelang='edelivery'
						where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and detail_no=$detail_no and 
						(status_lelang is null or status_lelang='')";
                //print_r($query);exit;
                $con2 = Propel::getConnection();
                $stmt2 = $con2->prepareStatement($query2);
                $stmt2->executeQuery();
            }
        }
        $this->setFlash('berhasil', 'Kegiatan nomor ' . $kode_kegiatan . ' telah menarik lagi data edelivery');
        $this->forward('peneliti', 'list');
    }

    public function executeBukaSubtitle() {
        //if($this->getRequestParameter('kunci')==md5('buka'))
        //{
        $subtitle = $this->getRequestParameter('subtitle');
        $unit_id = $this->getRequestParameter('unit_id');
        $kegiatan_code = $this->getRequestParameter('kegiatan_code');

        $sql = "update " . sfConfig::get('app_default_schema') . ".rincian_detail set last_update_ip='' where subtitle='$subtitle' and unit_id='$unit_id' and kegiatan_code='$kegiatan_code'";
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($sql);
        budgetLogger::log('Membuka kunci Subtitle ' . $subtitle . ' pada unit_id=' . $unit_id . ' dan kegiatan_code=' . $kegiatan_code);
        $stmt->executeQuery();

        $this->setFlash('berhasil', 'Subtitle ' . $subtitle . ' telah dibuka');
        //$this->forward('/peneliti','edit?unit_id='.$unit_id.'&kode_kegiatan='.$kegiatan_code);
        return $this->redirect('peneliti/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);

        //}	
    }

    public function executeKunciSubtitle() {
        //if($this->getRequestParameter('kunci')==md5('tutup'))
        //{
        $subtitle = $this->getRequestParameter('subtitle');
        $unit_id = $this->getRequestParameter('unit_id');
        $kegiatan_code = $this->getRequestParameter('kegiatan_code');

        $sql = "update " . sfConfig::get('app_default_schema') . ".rincian_detail set last_update_ip='LOCK' where subtitle='$subtitle' and unit_id='$unit_id' and kegiatan_code='$kegiatan_code'";
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($sql);
        budgetLogger::log('Mengunci Subtitle ' . $subtitle . ' pada unit_id=' . $unit_id . ' dan kegiatan_code=' . $kegiatan_code);
        $stmt->executeQuery();

        $this->setFlash('berhasil', 'Subtitle ' . $subtitle . ' telah dikunci');
        //$this->forward('peneliti','edit?unit_id='.$unit_id.'&kode_kegiatan='.$kegiatan_code);
        return $this->redirect('peneliti/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);

        //}	
    }

    public function executePersonil() { //dj
        $unit_id = $this->unit_id = $this->getRequestParameter('unit_id');
        $kegiatan_code = $this->kegiatan_code = $this->getRequestParameter('kegiatan_code');
        $subtitle = $this->subtitle = $this->getRequestParameter('subtitle');
        $act = $this->act = $this->getRequestParameter('act');

        if ($act == 'tambah') {

            $c = new Criteria();
            $c->add(PersonilRkaPeer::UNIT_ID, $unit_id);
            $c->add(PersonilRkaPeer::KEGIATAN_CODE, $kegiatan_code);
            $c->add(PersonilRkaPeer::SUBTITLE, $subtitle);
            $x = PersonilRkaPeer::doSelect($c);
            $this->x = $x;
        }
    }

    public function executeNolSubtitle() {
        if ($this->getRequestParameter('unit')) {
            $unit_id = $this->getRequestParameter('unit');
            $kegiatan_code = $this->getRequestParameter('kegiatan');
            $sub_id = $this->getRequestParameter('idsub');

            $c = new Criteria();
            $c->add(SubtitleIndikatorPeer::UNIT_ID, $unit_id);
            $c->add(SubtitleIndikatorPeer::KEGIATAN_CODE, $kegiatan_code);
            $c->add(SubtitleIndikatorPeer::SUB_ID, $sub_id);
            $subtitle_indikator = SubtitleIndikatorPeer::doSelectOne($c);
            if ($subtitle_indikator) {
                if ($subtitle_indikator->getSubtitle()) {
                    $subtitle = $subtitle_indikator->getSubtitle();
                    $query = "update " . sfConfig::get('app_default_schema') . ".rincian_detail set status_hapus=true  where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and subtitle='$subtitle'";
                    $con = Propel::getConnection(RincianDetailPeer::DATABASE_NAME);
                    //$result2 = @pg_query($conn_id,$query2);
                    $stmt = $con->prepareStatement($query);
                    //while($rows2 = @pg_fetch_assoc($result2))	extract($rows2,EXTR_OVERWRITE);
                    $stmt->executeQuery();
                }
            }
            $this->subid = $sub_id;

            $d = new Criteria();
            $d->add(SubtitleIndikatorPeer::UNIT_ID, $unit_id);
            $d->add(SubtitleIndikatorPeer::KEGIATAN_CODE, $kegiatan_code);
            $this->rs_subtitle = SubtitleIndikatorPeer::doSelect($d);
            //return $this->redirect('peneliti/edit?unit_id='.$unit_id.'&kode_kegiatan='.$kegiatan_code);
        }
    }

    public function executeSpecial() {//dj
        $act = $this->act = $this->getRequestParameter('act');
        $id = $this->id = $this->getRequestParameter('id');
        $unit = $this->unit = $this->getRequestParameter('unit');
        $kegiatan = $this->kegiatan = $this->getRequestParameter('kegiatan');
        $harga = $this->harga = $this->getRequestParameter('harga');
        $hargaBaru = $this->hargaBaru = $this->getRequestParameter('hargaBaru_' . $id);

        if ($act == 'simpan') {
            //echo $id.' '.$unit.' '.$kegiatan.' '.$hargaBaru;
            $query = "UPDATE " . sfConfig::get('app_default_schema') . ".rincian_detail set komponen_harga_awal=$hargaBaru where unit_id='$unit' and kegiatan_code='$kegiatan' and detail_no=$id  ";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            budgetLogger::log('Mengubah komppnen harga awal khusus mas Rizki pada unit_id=' . $unit . ' dan kegiatan_code=' . $kegiatan . ' dan detail_no=' . $id);
            $stmt->executeQuery();
            $this->setFlash('berhasil', "Mas riski yang baik Harganya  sudah berhasil di update loh!!");
            return $this->redirect('peneliti/edit?unit_id=' . $unit . '&kode_kegiatan=' . $kegiatan);
        }
    }

    public function executeSaran() {//dj untuk saran bappeko tentang saran subtitle di budget 2009
        //$this->keterangan_koefisien=$this->getRequestParameter('keterangan_koefisien');
        //$this->detail_no=$this->getRequestParameter('detail_no');
        $cek = $this->unit_id = $this->getRequestParameter('unit_id');
        $cek2 = $this->id = $this->getRequestParameter('id');
        $cek3 = $this->kegiatan_code = $this->getRequestParameter('kegiatan_code');
        $cek4 = $this->subtitle_lama = $this->getRequestParameter('subLama');
        $act = $this->act = $this->getRequestParameter('act');

        //  echo $cek2;
        if ($act == 'edit') {
            $query = "select *
            from " . sfConfig::get('app_default_schema') . ".saran_subtitle
            where unit_id='$cek' and kegiatan_code = '$cek3'  and subtitle_lama='$cek4' and sub_id='$cek2' ";

            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $saran = $stmt->executeQuery();
            while ($saran->next()) {
                $this->saran_subtitle = $saran->getString('subtitle_baru');
            }
        }
    }

    public function executeHapusSubtitle() {//dj menghapus saran subtitle di budget 2009
        $act = $this->act = $this->getRequestParameter('act');
        $del = $this->delete_id = $this->getRequestParameter('sub_id');
        $unit_id = $this->unit_id = $this->getRequestParameter('unit_id');
        $kegiatan_code = $this->kegiatan_code = $this->getRequestParameter('keg_code');
        //echo $act.' sub id '.$del;
        //echo $delete_id;
        if ($act == 'hapus') {
            $query2 = "delete from " . sfConfig::get('app_default_schema') . ".saran_subtitle where sub_id='$del'";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query2);

            $deleteSaran = $stmt->executeQuery();
            $this->setFlash('berhasil', "Saran subtitle telah berhasil di hapus pada sub id ($del)");
            return $this->redirect('peneliti/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
        }
    }

    public function executeChangeSubtitle() {//dj untuk menambahkan saran mengenai subtitle dari peneliti ke bappeko tahun 2009
        $unit_id = $this->getRequestParameter('unit_id');
        $kegiatan_code = $this->getRequestParameter('kegiatan_code');
        $subtitle_lama = $this->getRequestParameter('subLama');
        $sub_id = $this->getRequestParameter('id');
        $subtitle_baru = $this->getRequestParameter('editSubtitle_' . $sub_id);

        if ($subtitle_baru != '') {
            $query = "insert into " . sfConfig::get('app_default_schema') . ".saran_subtitle (sub_id,unit_id,kegiatan_code,subtitle_lama,subtitle_baru) values ($sub_id,'$unit_id','$kegiatan_code','$subtitle_lama','$subtitle_baru')";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $stmt->executeQuery();
            $this->setFlash('berhasil', "Pengisian saran subtitle  Telah Berhasil Dilakukan pada sub id ( $sub_id )");
            return $this->redirect('peneliti/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
        } else {
            $this->setFlash('gagal', "Saran subtitle yang anda masukkan kosong, silahkan ulangi lagi");
            return $this->redirect('peneliti/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
        }
    }

    public function executeTampillaporan() {

        if ($this->getRequestParameter('unit_id')) {
            $this->executeBanding();
            $this->setTemplate('banding');
        }
    }

    public function executePrintBanding() {
        $unit_id = $this->getRequestParameter('unit_id');
        $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
        $this->kode_kegiatan = $kode_kegiatan;
        $c = new Criteria();
        $c->add(MasterKegiatanPeer::UNIT_ID, $unit_id);
        $c->add(MasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
        $master_kegiatan = MasterKegiatanPeer::doSelectOne($c);
        if ($master_kegiatan) {
            $this->kode_program22 = substr($master_kegiatan->getKodeProgram2(), 5, 2);
            if (substr($master_kegiatan->getKodeProgram2(), 0, 4) == 'X.XX') {
                $this->kode_urusan = substr($master_kegiatan->getKodeUrusan(), 0, 4);
            } else {
                $this->kode_urusan = substr($master_kegiatan->getKodeProgram2(), 0, 4);
            }
            //print_r($kode_program22);
            //exit;
            $e = new Criteria();
            $e->add(UnitKerjaPeer::UNIT_ID, $master_kegiatan->getUnitId());
            $es = UnitKerjaPeer::doSelectOne($e);
            if ($es) {
                $this->kode_permen = $es->getKodePermen();
            }


            $this->kode = $this->kode_urusan . '.' . $this->kode_permen . '.' . $this->kode_program22 . '.' . $master_kegiatan->getKodeKegiatan();
            $this->kode_kegiatan2 = $this->kode_urusan . '.' . $this->kode_program . '.' . $this->kode_program22 . '.' . $master_kegiatan->getKodeKegiatan();
            $this->nama_kegiatan = $master_kegiatan->getNamaKegiatan();
            $u = new Criteria();
            $u->add(MasterUrusanPeer::KODE_URUSAN, $this->kode_urusan);
            $us = MasterUrusanPeer::doSelectOne($u);
            if ($us) {
                $this->nama_urusan = $us->getNamaUrusan();
            }

            $this->kode_program = $master_kegiatan->getKodeProgram();
            $this->unit_id = $unit_id;
            $u = new Criteria();
            $u->add(UnitKerjaPeer::UNIT_ID, $unit_id);
            $us = UnitKerjaPeer::doSelectOne($u);
            if ($us) {
                $this->unit_kerja = $us->getUnitName();
            }

            $query = "select *
				from " . sfConfig::get('app_default_schema') . ".master_program kp
				where kp.kode_program='" . $this->kode_program . "' and kp.kode_tujuan ilike '" . $master_kegiatan->getKodeTujuan() . "'";
            //print_r($query);exit;
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs1 = $stmt->executeQuery();
            while ($rs1->next()) {
                $this->kode_program1 = $rs1->getString('kode_program');
                $this->nama_program = $rs1->getString('nama_program');
            }

            $query = "select *
				from " . sfConfig::get('app_default_schema') . ".master_program2 kp2
				where kp2.kode_program='" . $this->kode_program . "' and kp2.kode_program2 ilike '" . $this->kode_program22 . "'";
            //print_r($query);exit;
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs1 = $stmt->executeQuery();
            while ($rs1->next()) {
                $this->nama_program22 = $rs1->getString('nama_program2');
            }

            $query = "select *
				from " . sfConfig::get('app_default_schema') . ".master_sasaran kp2
				where kp2.kode_sasaran='" . $master_kegiatan->getKodeSasaran() . "'";
            //print_r($query);exit;
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs1 = $stmt->executeQuery();
            while ($rs1->next()) {
                $this->nama_sasaran = $rs1->getString('nama_sasaran');
                $this->kode_sasaran = $rs1->getString('kode_sasaran');
            }

            $query = "select sum(rd.volume * rd.komponen_harga_awal * (100 + rd.pajak) / 100) as nilai from " . sfConfig::get('app_default_schema') . ".rincian_detail_bp rd
						where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan'";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs1 = $stmt->executeQuery();
            while ($rs1->next()) {
                $this->total_semula = $rs1->getString('nilai');
            }

            $query = "select sum(rd.volume * rd.komponen_harga_awal * (100 + rd.pajak) / 100) as nilai from " . sfConfig::get('app_default_schema') . ".rincian_detail rd
						where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan'";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs1 = $stmt->executeQuery();
            while ($rs1->next()) {
                $this->total_sekarang = $rs1->getString('nilai');
            }

            $this->setLayout('kosong');
            $this->getResponse()->addStylesheet('tampilan_print2', '', array('media' => 'print'));
        }
    }

    public function executeBanding() {
        $unit_id = $this->getRequestParameter('unit_id');
        $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
        $this->kode_kegiatan = $kode_kegiatan;
        $c = new Criteria();
        $c->add(MasterKegiatanPeer::UNIT_ID, $unit_id);
        $c->add(MasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
        $master_kegiatan = MasterKegiatanPeer::doSelectOne($c);
        if ($master_kegiatan) {
            $this->kode_program22 = substr($master_kegiatan->getKodeProgram2(), 5, 2);
            if (substr($master_kegiatan->getKodeProgram2(), 0, 4) == 'X.XX') {
                $this->kode_urusan = substr($master_kegiatan->getKodeUrusan(), 0, 4);
            } else {
                $this->kode_urusan = substr($master_kegiatan->getKodeProgram2(), 0, 4);
            }
            //print_r($kode_program22);
            //exit;
            $e = new Criteria();
            $e->add(UnitKerjaPeer::UNIT_ID, $master_kegiatan->getUnitId());
            $es = UnitKerjaPeer::doSelectOne($e);
            if ($es) {
                $this->kode_permen = $es->getKodePermen();
            }


            $this->kode = $this->kode_urusan . '.' . $this->kode_permen . '.' . $this->kode_program22 . '.' . $master_kegiatan->getKodeKegiatan();
            $this->kode_kegiatan2 = $this->kode_urusan . '.' . $this->kode_program . '.' . $this->kode_program22 . '.' . $master_kegiatan->getKodeKegiatan();
            $this->nama_kegiatan = $master_kegiatan->getNamaKegiatan();
            $u = new Criteria();
            $u->add(MasterUrusanPeer::KODE_URUSAN, $this->kode_urusan);
            $us = MasterUrusanPeer::doSelectOne($u);
            if ($us) {
                $this->nama_urusan = $us->getNamaUrusan();
            }

            $this->kode_program = $master_kegiatan->getKodeProgram();
            $this->unit_id = $unit_id;
            $u = new Criteria();
            $u->add(UnitKerjaPeer::UNIT_ID, $unit_id);
            $us = UnitKerjaPeer::doSelectOne($u);
            if ($us) {
                $this->unit_kerja = $us->getUnitName();
            }

            $query = "select *
				from " . sfConfig::get('app_default_schema') . ".master_program kp
				where kp.kode_program='" . $this->kode_program . "' and kp.kode_tujuan ilike '" . $master_kegiatan->getKodeTujuan() . "'";
            //print_r($query);exit;
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs1 = $stmt->executeQuery();
            while ($rs1->next()) {
                $this->kode_program1 = $rs1->getString('kode_program');
                $this->nama_program = $rs1->getString('nama_program');
            }

            $query = "select *
				from " . sfConfig::get('app_default_schema') . ".master_program2 kp2
				where kp2.kode_program='" . $this->kode_program . "' and kp2.kode_program2 ilike '" . $this->kode_program22 . "'";
            //print_r($query);exit;
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs1 = $stmt->executeQuery();
            while ($rs1->next()) {
                $this->nama_program22 = $rs1->getString('nama_program2');
            }

            $query = "select *
				from " . sfConfig::get('app_default_schema') . ".master_sasaran kp2
				where kp2.kode_sasaran='" . $master_kegiatan->getKodeSasaran() . "'";
            //print_r($query);exit;
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs1 = $stmt->executeQuery();
            while ($rs1->next()) {
                $this->nama_sasaran = $rs1->getString('nama_sasaran');
                $this->kode_sasaran = $rs1->getString('kode_sasaran');
            }

//				$query="select sum(rd.volume * rd.komponen_harga_awal * (100 + rd.pajak) / 100) as nilai from ". sfConfig::get('app_default_schema') .".rincian_detail_bp rd
//						where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan'";
            $query = "select sum(rd.volume * rd.komponen_harga_awal * (100 + rd.pajak) / 100) as nilai from " . sfConfig::get('app_default_schema') . ".rincian_detail_bp rd
						where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan'";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs1 = $stmt->executeQuery();
            while ($rs1->next()) {
                $this->total_semula = $rs1->getString('nilai');
            }

            $query = "select sum(rd.volume * rd.komponen_harga_awal * (100 + rd.pajak) / 100) as nilai from " . sfConfig::get('app_default_schema') . ".rincian_detail rd
						where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan'";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs1 = $stmt->executeQuery();
            while ($rs1->next()) {
                $this->total_sekarang = $rs1->getString('nilai');
            }
        }
    }

    public function executeReport() {
        $this->kirim = 3;
        return sfView::SUCCESS;
    }

    public function executeTampilReport() {
        $jenis = $this->getRequestParameter('b');
        switch ($jenis) {
            case 'rd':
                $this->executeReportDinas();
                $this->setTemplate('reportDinas');
                break;
            case 'pd':
                $this->executePilihDinas();
                $this->setTemplate('pilihDinas');
                break;
            case 'rdkb':
                $this->executeReportDinasKegiatanBelanja($this->getRequestParameter('dinas'));
                $this->setTemplate('reportDinasKegiatanBelanja');
                break;
            case 'rdk':
                $this->executeReportDinasKegiatan();
                $this->setTemplate('reportDinasKegiatan');
                break;
            case 'rb':
                $this->executeReportBelanja();
                $this->setTemplate('reportBelanja');
                break;
            case 'rbr':
                $this->executeReportBelanjaRekening();
                $this->setTemplate('reportBelanjaRekening');
                break;
            case 'rpd':
                $this->executeReportProgramDinas();
                $this->setTemplate('reportProgramDinas');
                break;
            case 'rp13':
                $this->executeReportProgramP13();
                $this->setTemplate('reportProgramP13');
                break;
            case 'rp':
                $this->executeReportProgram();
                $this->setTemplate('reportProgram');
                break;
            case 'rpk':
                $this->executeReportProgramKegiatan();
                $this->setTemplate('reportProgramKegiatan');
                break;
            case 'rdks':
                $this->executeReportDinasKegiatanSubtitle();
                $this->setTemplate('reportDinasKegiatanSubtitle');
                break;
            case 'rpks':
                $this->executeReportProgramKegiatanSubtitle();
                $this->setTemplate('reportProgramKegiatanSubtitle');
                break;
            case 'rrk':
                $this->executeReportRekeningKomponen();
                $this->setTemplate('reportRekeningKomponen');
                break;
            case 'pr':
                $this->executePilihRekening();
                $this->setTemplate('pilihRekening');
                break;
            case 'rrkk':
                $this->executeReportRekeningKomponenKegiatan();
                $this->setTemplate('reportRekeningKomponenKegiatan');
                break;
            default:
                break;
        }
    }

    public function executeReportRekeningKomponenKegiatan() {
        $active_rek = $this->getRequestParameter('rek');
        $query = "select rekening_code as old_rekening_code,rekening_name as old_rekening_name,komponen_name ,sum((jumlah_draft*jumlah_non_lanjutan)) as total_draft3,sum((jumlah_setuju*jumlah_non_lanjutan)) as total_locked3,
 		sum((nilai_draft*jumlah_non_lanjutan)) as nilai_draft3, sum((nilai_draft*jumlah_setuju*jumlah_non_lanjutan)) as nilai_locked3 
		FROM " . sfConfig::get('app_default_schema') . ".v_rekening_komponen
		WHERE unit_id<>'9999' and unit_id<>'4444' and rekening_code='$active_rek'
		GROUP BY rekening_code,rekening_name,komponen_name
		ORDER BY rekening_code,rekening_name,komponen_name";
        $add1 = " and substring(k.kode_kegiatan,1,1)='0' ";

        $query2 = "select sum(komponen_harga_awal*volume*(100+pajak)/100) as sum_draft
	 	from " . sfConfig::get('app_default_schema') . ".rincian r, " . sfConfig::get('app_default_schema') . ".master_kegiatan k, " . sfConfig::get('app_default_schema') . ".rincian_detail d 
		where k.kode_kegiatan=r.kegiatan_code and k.unit_id=r.unit_id and 
		d.kegiatan_code=r.kegiatan_code and d.unit_id=r.unit_id and r.unit_id<>'9999'  and r.unit_id<>'4444' $add1";
        //$result2 = @pg_query($conn_id,$query2);
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query2);
        //while($rows2 = @pg_fetch_assoc($result2))	extract($rows2,EXTR_OVERWRITE);
        $rs = $stmt->executeQuery();
        while ($rs->next()) {
            $sum_draft = $rs->getString('sum_draft');
        }

        $query2 = "select sum(komponen_harga_awal*volume*(100+pajak)/100) as sum_locked
	 	from " . sfConfig::get('app_default_schema') . ".rincian r," . sfConfig::get('app_default_schema') . ".master_kegiatan k," . sfConfig::get('app_default_schema') . ".rincian_detail d 
		where k.kode_kegiatan=r.kegiatan_code and k.unit_id=r.unit_id and 
		d.kegiatan_code=r.kegiatan_code and d.unit_id=r.unit_id and r.unit_id<>'9999' and r.unit_id<>'4444'  and r.rincian_level=3 $add1";
        //$result2 = @pg_query($conn_id,$query2);
        $stmt = $con->prepareStatement($query2);
        $rs = $stmt->executeQuery();
        //while($rows2 = @pg_fetch_assoc($result2))	extract($rows2,EXTR_OVERWRITE);
        while ($rs->next()) {
            $sum_locked = $rs->getString('sum_locked');
        }


        //$result = @pg_query($conn_id,$query);
        $stmt = $con->prepareStatement($query);
        //if(!$result) echo @pg_last_error($conn_id);
        //$rs=pg_fetch_all($result);
        $rs = $stmt->executeQuery();
        $this->rs = $rs;

        if ($rs) {
            if ($sum_locked == 0 or !$sum_locked)
                $sum_locked = 99999999999;
            if ($sum_draft == 0 or !$sum_draft)
                $sum_draft = 99999999999;
            $i = 0;
            $j = 0;
            $rekening_code = '';
            $nilai_draft = 0;
            $nilai_draft2 = 0;
            $nilai_locked = 0;
            $nilai_locked2 = 0;
            $persen_draft = 0;
            $persen_locked = 0;

            $persen_draft_arr = array();
            $this->persen_draft = array();

            $persen_locked_arr = array();
            $this->persen_locked = array();

            $nilai_draft_arr = array();
            $this->nilai_draft = array();

            $nilai_locked_arr = array();
            $this->nilai_locked = array();

            $dinas_ok_arr = array();
            $this->dinas_ok = array();

            $rekening_code_arr = array();
            $this->rekening_code = array();

            $rekening_name_arr = array();
            $this->rekening_name = array();

            $rekening_name = '';
            $persen_draft3 = 0;

            $persen_draft3_arr = array();
            $this->persen_draft3 = array();
            $persen_locked3 = 0;

            $persen_locked3_arr = array();
            $this->persen_locked3 = array();

            $nilai_draft3_arr = array();
            $this->nilai_draft3 = array();

            $units_arr = array();
            $this->units = array();

            $nilai_locked3_arr = array();
            $this->nilai_locked3 = array();

            $this->komponen_name = array();
            $komponen_name = array();
            //foreach($rs as $s)
            while ($rs->next()) {
                $komponen_name[$i] = $rs->getString('komponen_name');
                if ($rekening_code != $rs->getString('old_rekening_code')) {
                    $nilai_draft2+=$nilai_draft;
                    $nilai_locked2+=$nilai_locked;
                    $persen_draft = $nilai_draft * 100 / $sum_draft;
                    //$rs[$i-$j]['persen_draft']=number_format($persen_draft,2,",",".");
                    $persen_draft_arr[$i - $j] = number_format($persen_draft, 2, ",", ".");
                    $persen_locked = $nilai_locked * 100 / $sum_locked;
                    //$rs[$i-$j]['persen_locked']=number_format($persen_locked,2,",",".");
                    $persen_locked_arr[$i - $j] = number_format($persen_locked, 2, ",", ".");

                    //$rs[$i-$j]['nilai_draft']=number_format($nilai_draft,0,",",".");
                    $nilai_draft_arr[$i - $j] = number_format($nilai_draft, 0, ",", ".");
                    //$rs[$i-$j]['nilai_locked']=number_format($nilai_locked,0,",",".");
                    $nilai_locked_arr[$i - $j] = number_format($nilai_locked, 0, ",", ".");
                    //$rs[$i-$j]['dinas_ok']='t';
                    $dinas_ok_arr[$i - $j] = 't';
                    //$rs[$i-$j]['rekening_code']=$rekening_code;
                    $rekening_code_arr[$i - $j] = $rekening_code;
                    //$rs[$i-$j]['rekening_name']=$rekening_name;
                    $rekening_name_arr[$i - $j] = $rekening_name;
                    $j = 0;
                    $nilai_draft = 0;
                    $nilai_locked = 0;
                }
                //$rekening_code=$rs[$i]['old_rekening_code'];
                $rekening_code = $rs->getString('old_rekening_code');
                //$rekening_name=$rs[$i]['old_rekening_name'];
                $rekening_name = $rs->getString('old_rekening_name');

                //$nilai_draft+=$rs[$i]['nilai_draft3'];
                $nilai_draft+=$rs->getString('nilai_draft3');
                //$nilai_locked+=$rs[$i]['nilai_locked3'];
                $nilai_locked+=$rs->getString('nilai_locked3');
                //$persen_draft3=$rs[$i]['nilai_draft3'] * 100 / $sum_draft;
                $persen_draft3 = $rs->getString('nilai_draft3') * 100 / $sum_draft;
                //$rs[$i]['persen_draft3']=number_format($persen_draft3,2,",",".");
                $persen_draft3_arr[$i] = number_format($persen_draft3, 2, ",", ".");
                //$persen_locked3=$rs[$i]['nilai_locked3'] * 100 / $sum_locked; 
                $persen_locked3 = $rs->getString('nilai_locked3') * 100 / $sum_locked;
                //$rs[$i]['persen_locked3']=number_format($persen_locked3,2,",",".");
                $persen_locked3_arr[$i] = number_format($persen_locked3, 2, ",", ".");


                //$rs[$i]['nilai_draft3']=number_format($rs[$i]['nilai_draft3'],0,",",".");
                $nilai_draft3_arr[$i] = number_format($rs->getString('nilai_draft3'), 0, ",", ".");
                //$rs[$i]['nilai_locked3']=number_format($rs[$i]['nilai_locked3'],0,",",".");
                $nilai_locked3_arr[$i] = number_format($rs->getString('nilai_locked3'), 0, ",", ".");

                $q10 = "SELECT distinct unit_name,nama_kegiatan from " . sfConfig::get('app_default_schema') . ".rincian_detail r," . sfConfig::get('app_default_schema') . ".master_kegiatan k,unit_kerja u where k.unit_id=r.unit_id and k.kode_kegiatan=r.kegiatan_code and r.unit_id=u.unit_id and r.komponen_name='" . $rs->getString('komponen_name') . "' and r.unit_id<>'9999' and r.unit_id<>'4444'";
                //echo $q10;exit;
                //$r10 = @pg_query($conn_id,$q10	);
                $stmt = $con->prepareStatement($q10);
                //$rs10=pg_fetch_all($r10);
                $rs10 = $stmt->executeQuery();
                //print_r ($rs10);exit;
                $t10 = '<table border=1>';
                //foreach($rs10 as $s10)
                while ($rs10->next()) {
                    $t10.="<tr><td>" . $rs10->getString('unit_name') . "</td><td>" . $rs10->getString('nama_kegiatan') . '</td></tr>';
                }
                //$rs[$i]['units']= $t10.'</table>';
                $units_arr[$i] = $t10 . '</table>';


                $i+=1;
                $j+=1;
            }
            $nilai_draft2+=$nilai_draft;
            $nilai_locked2+=$nilai_locked;
            $persen_draft = $nilai_draft * 100 / $sum_draft;
            //$rs[$i-$j]['persen_draft']=number_format($persen_draft,2,",",".");
            $persen_draft_arr[$i - $j] = number_format($persen_draft, 2, ",", ".");
            $persen_locked = $nilai_locked * 100 / $sum_locked;
            //$rs[$i-$j]['persen_locked']=number_format($persen_locked,2,",",".");
            $persen_locked_arr[$i - $j] = number_format($persen_locked, 2, ",", ".");
            //$rs[$i-$j]['nilai_draft']=number_format($nilai_draft,0,",",".");
            $nilai_draft_arr[$i - $j] = number_format($nilai_draft, 0, ",", ".");
            //$rs[$i-$j]['nilai_locked']=number_format($nilai_locked,0,",",".");
            $nilai_locked_arr[$i - $j] = number_format($nilai_locked, 0, ",", ".");
            //$rs[$i-$j]['dinas_ok']='t';
            $dinas_ok_arr[$i - $j] = 't';
            //$rs[$i-$j]['rekening_code']=$rekening_code;
            $rekening_code_arr[$i - $j] = $rekening_code;
            //$rs[$i-$j]['rekening_name']=$rekening_name;
            $rekening_name_arr[$i - $j] = $rekening_name;

            $this->nilai_draft2 = number_format($nilai_draft2, 0, ",", ".");
            $this->nilai_locked2 = number_format($nilai_locked2, 0, ",", ".");

            $this->komponen_name = $komponen_name;
            $this->persen_draft = $persen_draft_arr;
            $this->persen_locked = $persen_locked_arr;
            $this->nilai_draft = $nilai_draft_arr;
            $this->nilai_locked = $nilai_locked_arr;
            $this->dinas_ok = $dinas_ok_arr;
            $this->rekening_code = $rekening_code_arr;
            $this->rekening_name = $rekening_name_arr;
            $this->persen_draft3 = $persen_draft3_arr;
            $this->persen_locked3 = $persen_locked3_arr;
            $this->nilai_draft3 = $nilai_draft3_arr;
            $this->units = $units_arr;
            $this->nilai_locked3 = $nilai_locked3_arr;
        }
    }

    public function executePilihRekening() {
        $query = "select rekening_code || ' ' || rekening_name as rekening_combo,rekening_code from " . sfConfig::get('app_default_schema') . ".rekening order by rekening_code";
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query);
        $rs = $stmt->executeQuery();
        $this->rek = array();
        $rek = array();
        while ($rs->next()) {
            $rek[$rs->getString('rekening_code')] = $rs->getString("rekening_combo");
        }
        $this->rek = $rek;
    }

    public function executeReportRekeningKomponen() {
        $query = "select rekening_code as old_rekening_code,rekening_name as old_rekening_name,komponen_name ,sum((jumlah_draft*jumlah_non_lanjutan)) as total_draft3,sum((jumlah_setuju*jumlah_non_lanjutan)) as total_locked3,
 		sum((nilai_draft*jumlah_non_lanjutan)) as nilai_draft3, sum((nilai_draft*jumlah_setuju*jumlah_non_lanjutan)) as nilai_locked3 
			FROM " . sfConfig::get('app_default_schema') . ".v_rekening_komponen
			WHERE unit_id<>'9999'
			GROUP BY rekening_code,rekening_name,komponen_name
			ORDER BY rekening_code,rekening_name,komponen_name";
        $add1 = " and substring(k.kode_kegiatan,1,1)='0' ";

        $query2 = "select sum(komponen_harga_awal*volume*(100+pajak)/100) as sum_draft
	 	from " . sfConfig::get('app_default_schema') . ".rincian r," . sfConfig::get('app_default_schema') . ".master_kegiatan k," . sfConfig::get('app_default_schema') . ".rincian_detail d 
		where k.kode_kegiatan=r.kegiatan_code and k.unit_id=r.unit_id and 
		d.kegiatan_code=r.kegiatan_code and d.unit_id=r.unit_id and r.unit_id<>'9999' $add1";
        //$result2 = @pg_query($conn_id,$query2);
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query2);
        //while($rows2 = @pg_fetch_assoc($result2))	extract($rows2,EXTR_OVERWRITE);
        $rs = $stmt->executeQuery();
        while ($rs->next()) {
            $sum_draft = $rs->getString('sum_draft');
        }

        $query2 = "select sum(komponen_harga_awal*volume*(100+pajak)/100) as sum_locked
	 	from " . sfConfig::get('app_default_schema') . ".rincian r," . sfConfig::get('app_default_schema') . ".master_kegiatan k," . sfConfig::get('app_default_schema') . ".rincian_detail d 
		where k.kode_kegiatan=r.kegiatan_code and k.unit_id=r.unit_id and 
		d.kegiatan_code=r.kegiatan_code and d.unit_id=r.unit_id and r.unit_id<>'9999' and r.rincian_level=3 $add1";
        //$result2 = @pg_query($conn_id,$query2);
        $stmt = $con->prepareStatement($query2);
        $rs = $stmt->executeQuery();
        while ($rs->next()) {
            $sum_locked = $rs->getString('sum_locked');
        }
        //while($rows2 = @pg_fetch_assoc($result2))	extract($rows2,EXTR_OVERWRITE);
        //$result = @pg_query($conn_id,$query);
        $stmt = $con->prepareStatement($query);
        //if(!$result) echo @pg_last_error($conn_id);
        //$rs=pg_fetch_all($result);
        $rs = $stmt->executeQuery();
        $this->rs = $rs;

        if ($sum_locked == 0 or !$sum_locked)
            $sum_locked = 99999999999;
        if ($sum_draft == 0 or !$sum_draft)
            $sum_draft = 99999999999;
        $i = 0;
        $j = 0;
        $rekening_code = '';
        //foreach($rs as $s)
        $nilai_draft2 = 0;
        $nilai_draft = 0;
        $nilai_locked2 = 0;
        $nilai_locked = 0;
        $persen_draft = 0;
        $persen_locked = 0;

        $persen_draft_arr = array();
        $this->persen_draft = array();

        $persen_locked_arr = array();
        $this->persen_locked = array();

        $nilai_draft_arr = array();
        $this->nilai_draft = array();

        $nilai_locked_arr = array();
        $this->nilai_locked = array();

        $dinas_ok_arr = array();
        $this->dinas_ok = array();

        $rekening_code_arr = array();
        $this->rekening_code = array();


        $rekening_code = '';

        $rekening_name_arr = array();
        $this->rekening_name = array();

        $rekening_name = '';
        $persen_draft3 = 0;

        $persen_draft3_arr = array();
        $this->persen_draft3 = array();

        $persen_locked3 = 0;

        $persen_locked3_arr = array();
        $this->persen_locked3 = array();

        $nilai_draft3_arr = array();
        $this->nilai_draft3 = array();

        $nilai_locked3_arr = array();
        $this->nilai_locked3 = array();

        $komponen_name_arr = array();
        $this->komponen_name = array();

        while ($rs->next()) {
            $komponen_name_arr[$i] = $rs->getString('komponen_name');
            //if ($rekening_code!=$rs[$i]['old_rekening_code'])
            if ($rekening_code != $rs->getString('old_rekening_code')) {

                $nilai_draft2+=$nilai_draft;
                $nilai_locked2+=$nilai_locked;
                $persen_draft = $nilai_draft * 100 / $sum_draft;
                //$rs[$i-$j]['persen_draft']=number_format($persen_draft,2,",",".");
                $persen_draft_arr[$i - $j] = number_format($persen_draft, 2, ",", ".");
                $persen_locked = $nilai_locked * 100 / $sum_locked;
                //$rs[$i-$j]['persen_locked']=number_format($persen_locked,2,",",".");
                $persen_locked_arr[$i - $j] = number_format($persen_locked, 2, ",", ".");

                //$rs[$i-$j]['nilai_draft']=number_format($nilai_draft,0,",",".");
                $nilai_draft_arr[$i - $j] = number_format($nilai_draft, 0, ",", ".");
                //$rs[$i-$j]['nilai_locked']=number_format($nilai_locked,0,",",".");
                $nilai_locked_arr[$i - $j] = number_format($nilai_locked, 0, ",", ".");
                //$rs[$i-$j]['dinas_ok']='t';
                $dinas_ok_arr[$i - $j] = 't';
                //$rs[$i-$j]['rekening_code']=$rekening_code;
                $rekening_code_arr[$i - $j] = $rekening_code;
                //$rs[$i-$j]['rekening_name']=$rekening_name;
                $rekening_name_arr[$i - $j] = $rekening_name;
                $j = 0;
                $nilai_draft = 0;
                $nilai_locked = 0;
            }
            //$rekening_code=$rs[$i]['old_rekening_code'];
            $rekening_code = $rs->getString('old_rekening_code');
            //$rekening_name=$rs[$i]['old_rekening_name'];
            $rekening_name = $rs->getString('old_rekening_name');

            //$nilai_draft+=$rs[$i]['nilai_draft3'];
            $nilai_draft+=$rs->getString('nilai_draft3');
            //$nilai_locked+=$rs[$i]['nilai_locked3'];
            $nilai_locked+=$rs->getString('nilai_locked3');
            //$persen_draft3=$rs[$i]['nilai_draft3'] * 100 / $sum_draft;
            $persen_draft3 = $rs->getString('nilai_draft3') * 100 / $sum_draft;
            //$rs[$i]['persen_draft3']=number_format($persen_draft3,2,",",".");
            $persen_draft3_arr[$i] = number_format($persen_draft3, 2, ",", ".");
            //$persen_locked3=$rs[$i]['nilai_locked3'] * 100 / $sum_locked;
            $persen_locked3 = $rs->getString('nilai_locked3') * 100 / $sum_locked;
            //$rs[$i]['persen_locked3']=number_format($persen_locked3,2,",",".");
            $persen_locked3_arr[$i] = number_format($persen_locked3, 2, ",", ".");

            //$rs[$i]['nilai_draft3']=number_format($rs[$i]['nilai_draft3'],0,",",".");
            $nilai_draft3_arr[$i] = number_format($rs->getString('nilai_draft3'), 0, ",", ".");
            //$rs[$i]['nilai_locked3']=number_format($rs[$i]['nilai_locked3'],0,",",".");
            $nilai_locked3_arr[$i] = number_format($rs->getString('nilai_locked3'), 0, ",", ".");

            $i+=1;
            $j+=1;
        }
        $nilai_draft2+=$nilai_draft;
        $nilai_locked2+=$nilai_locked;
        $persen_draft = $nilai_draft * 100 / $sum_draft;
        //$rs[$i-$j]['persen_draft']=number_format($persen_draft,2,",",".");
        $persen_draft_arr[$i - $j] = number_format($persen_draft, 2, ",", ".");
        $persen_locked = $nilai_locked * 100 / $sum_locked;
        //$rs[$i-$j]['persen_locked']=number_format($persen_locked,2,",",".");
        $persen_locked_arr[$i - $j] = number_format($persen_locked, 2, ",", ".");

        //$rs[$i-$j]['nilai_draft']=number_format($nilai_draft,0,",",".");
        $nilai_draft_arr[$i - $j] = number_format($nilai_draft, 0, ",", ".");
        //$rs[$i-$j]['nilai_locked']=number_format($nilai_locked,0,",",".");
        $nilai_locked_arr[$i - $j] = number_format($nilai_locked, 0, ",", ".");
        //$rs[$i-$j]['dinas_ok']='t';
        $dinas_ok_arr[$i - $j] = 't';
        //$rs[$i-$j]['rekening_code']=$rekening_code;
        $rekening_code_arr[$i - $j] = $rekening_code;
        //$rs[$i-$j]['rekening_name']=$rekening_name;
        $rekening_name_arr[$i - $j] = $rekening_name;

        $this->nilai_draft2 = number_format($nilai_draft2, 0, ",", ".");
        $this->nilai_locked2 = number_format($nilai_locked2, 0, ",", ".");

        $this->persen_draft = $persen_draft_arr;

        $this->persen_locked = $persen_locked_arr;

        $this->nilai_draft = $nilai_draft_arr;

        $this->nilai_locked = $nilai_locked_arr;

        $this->dinas_ok = $dinas_ok_arr;

        $this->rekening_code = $rekening_code_arr;

        $this->rekening_name = $rekening_name_arr;

        $this->persen_draft3 = $persen_draft3_arr;

        $this->persen_locked3 = $persen_locked3_arr;

        $this->nilai_draft3 = $nilai_draft3_arr;

        $this->nilai_locked3 = $nilai_locked3_arr;

        $this->komponen_name = $komponen_name_arr;
    }

    public function executeReportProgramKegiatanSubtitle() {
        $query = "select s.subtitle,s.indikator,s.nilai,s.satuan,k.kode_kegiatan,k.unit_id,
		(select nilai_draft from " . sfConfig::get('app_default_schema') . ".v_dinas_kegiatan r where r.unit_id=k.unit_id and r.kode_kegiatan=k.kode_kegiatan) as total_nilai,(select sum(r2.komponen_harga_awal*r2.volume*(r2.pajak+100)/100) as nilai from " . sfConfig::get('app_default_schema') . ".rincian_detail r2 where r2.unit_id=k.unit_id and r2.kegiatan_code=k.kode_kegiatan and r2.subtitle=s.subtitle) as nilai_sub,k.nama_kegiatan, s.nilai,u.unit_name,u.unit_id,p.nama_program,p.kode_program
        from " . sfConfig::get('app_default_schema') . ".subtitle_indikator s," . sfConfig::get('app_default_schema') . ".master_kegiatan k, unit_kerja u," . sfConfig::get('app_default_schema') . ".master_program p
		where s.unit_id<>'9999'and s.unit_id=k.unit_id and s.kegiatan_code=k.kode_kegiatan and u.unit_id=k.unit_id and p.kode_program=k.kode_program order by k.kode_program,k.unit_id,k.kode_kegiatan,s.subtitle";

        //$result = @pg_query($conn_id,$query);
        //$rs=pg_fetch_all($result);
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query);
        $rs = $stmt->executeQuery();
        $this->rs = $rs;
        //$countarr=count($rs);
        //for($i=0;$i<$countarr;$i++)
        $kode_program = '';
        $lompat = 0;
        $unit_id = '';
        $kode_kegiatan = '';
        $this->program_ok = array();
        $i = 0;
        $this->total_program = array();
        $total_program = 0;
        $this->kegiatan_ok = array();
        $total_alokasi_dana = 0;
        $this->total_nilai = array();
        $this->nilai_sub = array();
        $this->unit_name = array();
        $this->total_alokasi_dana = 0;
        while ($rs->next()) {

            //if($rs[$i]['kode_program']<>$rs[$i-1]['kode_program']) 
            if ($rs->getString('kode_program') <> $kode_program) {
                $kode_program = $rs->getString('kode_program');

                //$rs[$i]['program_ok']=1;
                $this->program_ok[$i] = 1;
                //$rs[$i-$lompat+1]['total_program']=number_format($total_program,0,',','.');
                $this->total_program[$i - $lompat + 1] = number_format($total_program, 0, ',', '.');
                //$rs[$i-$lompat-1]['total_program']=number_format($total_program,0,',','.');
                $this->total_program[$i - $lompat - 1] = number_format($total_program, 0, ',', '.');
                //$rs[$i-$lompat]['total_program']=number_format($total_program,0,',','.');
                $this->total_program[$i - $lompat] = number_format($total_program, 0, ',', '.');
                $lompat = 0;
                $total_program = 0;
            } else {
                $lompat++;
            }
            //if($rs[$i]['unit_id']==$rs[$i-1]['unit_id']) $rs[$i]['unit_name']="";
            if ($rs->getString('unit_id') == $unit_id) {
                $unit_id = $rs->getString('unit_id');
                //$rs[$i]['unit_name']="";
                $this->unit_name[$i] = "";
            }
            //if(($rs[$i]['kode_kegiatan']<>$rs[$i-1]['kode_kegiatan'] and $rs[$i]['unit_id']==$rs[$i-1]['unit_id']) or $rs[$i]['unit_id']<>$rs[$i-1]['unit_id']) 
            if (($rs->getString('kode_kegiatan') <> $kode_kegiatan and $rs->getString('unit_id') == $unit_id) or $rs->getString('unit_id') <> $unit_id) {
                $kode_kegiatan = $rs->getString('kode_kegiatan');
                $unit_id = $rs->getString('unit_id');
                //$rs[$i]['kegiatan_ok']=1;
                $this->kegiatan_ok[$i] = 1;
                //$total_alokasi_dana+=$rs[$i]['total_nilai'];
                $total_alokasi_dana+=$rs->getString('total_nilai');
                //$total_program+=$rs[$i]['total_nilai'];
                $total_program+=$rs->getString('total_nilai');
            }
            //$rs[$i]['total_nilai']=number_format($rs[$i]['total_nilai'],0,',','.');
            $this->total_nilai[$i] = number_format($rs->getString('total_nilai'), 0, ',', '.');
            //$rs[$i]['nilai_sub']=number_format($rs[$i]['nilai_sub'],0,',','.');
            $this->nilai_sub[$i] = number_format($rs->getString('nilai_sub'), 0, ',', '.');
        }
        $this->total_alokasi_dana = number_format($total_alokasi_dana, 0, ',', '.');
        $i++;
    }

    public function executeReportDinasKegiatanSubtitle() {
        $query = "select unit_id as old_unit_id, unit_name as old_unit_name,kode_kegiatan as old_kode_kegiatan, nama_kegiatan as old_nama_kegiatan,subtitle ,sum((jumlah_draft*jumlah_non_lanjutan)) as total_draft4,sum((jumlah_setuju*jumlah_non_lanjutan)) as total_locked4,
 		sum((nilai_draft*jumlah_non_lanjutan)) as nilai_draft4, sum((nilai_draft*jumlah_setuju*jumlah_non_lanjutan)) as nilai_locked4 
		FROM " . sfConfig::get('app_default_schema') . ".v_kegiatan_rekening
		WHERE unit_id<>'9999'
		GROUP BY unit_id,unit_name,kode_kegiatan,nama_kegiatan,subtitle
		ORDER BY unit_id,unit_name,kode_kegiatan,nama_kegiatan,subtitle";

        $query2 = "Select unit_id,kode_kegiatan from " . sfConfig::get('app_default_schema') . ".master_kegiatan order by unit_id,kode_kegiatan limit 1";
        //$result = @pg_query($conn_id,$query2);
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query2);
        $rs = $stmt->executeQuery();
        //while($rows = @pg_fetch_assoc($result))extract($rows,EXTR_OVERWRITE);
        while ($rs->next()) {
            $unit_id = $rs->getString('unit_id');
            $kode_kegiatan = $rs->getString('kode_kegiatan');
        }

        //$result = @pg_query($conn_id,$query);
        $stmt = $con->prepareStatement($query);
        $rs = $stmt->executeQuery();
        $this->rs = $rs;
        //if(!$result) echo @pg_last_error($conn_id);
        //$rs=pg_fetch_all($result);

        $i = 0;
        $j = 0;
        $k = 0;
        //foreach($rs as $s)
        $nilai_draft3 = 0;
        $nilai_locked3 = 0;
        $nilai_draft = 0;
        $nilai_locked = 0;

        $subtitle_arr = array();
        $this->subtitle = array();

        $unit_id_arr = array();
        $this->unit_id = array();

        $unit_name_arr = array();
        $this->unit_name = array();

        $dinas_ok_arr = array();
        $this->dinas_ok = array();

        $kegiatan_ok_arr = array();
        $this->kegiatan_ok = array();

        $nilai_draft_arr = array();
        $this->nilai_draft = array();

        $nilai_locked_arr = array();
        $this->nilai_locked = array();

        $kode_kegiatan_arr = array();
        $this->kode_kegiatan = array();

        $nama_kegiatan_arr = array();
        $this->nama_kegiatan = array();

        $nilai_draft3_arr = array();
        $this->nilai_draft3 = array();

        $nilai_locked3_arr = array();
        $this->nilai_locked3 = array();

        $nilai_draft4_arr = array();
        $this->nilai_draft4 = array();

        $nilai_locked4_arr = array();
        $this->nilai_locked4 = array();
        $nilai_draft2 = 0;
        $nilai_locked2 = 0;
        $unit_name = '';
        $nama_kegiatan = '';
        while ($rs->next()) {
            $subtitle_arr[$i] = $rs->getString('subtitle');
            if ($kode_kegiatan != $rs->getString('old_kode_kegiatan')) {
                $udah = 'f';
                if ($unit_id != $rs->getString('old_unit_id')) {
                    $nilai_draft+=$nilai_draft3;
                    $nilai_locked+=$nilai_locked3;
                    $nilai_draft2+=$nilai_draft;
                    $nilai_locked2+=$nilai_locked;

                    //$rs[$i-$k]['nilai_draft']=number_format($nilai_draft,0,",",".");
                    $nilai_draft_arr[$i - $k] = number_format($nilai_draft, 0, ",", ".");
                    //$rs[$i-$k]['nilai_locked']=number_format($nilai_locked,0,",",".");
                    $nilai_locked_arr[$i - $k] = number_format($nilai_locked, 0, ",", ".");
                    //$rs[$i-$k]['dinas_ok']='t';
                    $dinas_ok_arr[$i - $k] = 't';
                    //$rs[$i-$k]['unit_id']=$unit_id;
                    $unit_id_arr[$i - $k] = $unit_id;
                    //$rs[$i-$k]['unit_name']=$unit_name;
                    $unit_name_arr[$i - $k] = $unit_name;
                    $k = 0;
                    $nilai_draft = 0;
                    $nilai_locked = 0;
                    $udah = 't';
                    //$rs[$i-$j]['nilai_draft3']=number_format($nilai_draft3,0,",",".");
                    $nilai_draft3_arr[$i - $j] = number_format($nilai_draft3, 0, ",", ".");
                    //$rs[$i-$j]['nilai_locked3']=number_format($nilai_locked3,0,",",".");
                    $nilai_locked3_arr[$i - $j] = number_format($nilai_locked3, 0, ",", ".");
                    //$rs[$i-$j]['kegiatan_ok']='t';
                    $kegiatan_ok_arr[$i - $j] = 't';
                    //$rs[$i-$j]['kode_kegiatan']=$kode_kegiatan;
                    $kode_kegiatan_arr[$i - $j] = $kode_kegiatan;
                    //$rs[$i-$j]['nama_kegiatan']=$nama_kegiatan;
                    $nama_kegiatan_arr[$i - $j] = $nama_kegiatan;
                    $nilai_draft3 = 0;
                    $nilai_locked3 = 0;
                    $j = 0;
                }
                $unit_id = $rs->getString('old_unit_id');
                $unit_name = $rs->getString('old_unit_name');
                //$rs[$i]['unit_id']=$unit_id;
                $unit_id_arr[$i] = $unit_id;

                $nilai_draft+=$nilai_draft3;
                $nilai_locked+=$nilai_locked3;

                if ($udah != 't') {
                    //$rs[$i-$j]['nilai_draft3']=number_format($nilai_draft3,0,",",".");
                    $nilai_draft3_arr[$i - $j] = number_format($nilai_draft3, 0, ",", ".");
                    //$rs[$i-$j]['nilai_locked3']=number_format($nilai_locked3,0,",",".");
                    $nilai_locked3_arr[$i - $j] = number_format($nilai_locked3, 0, ",", ".");
                    //$rs[$i-$j]['kegiatan_ok']='t';
                    $kegiatan_ok_arr[$i - $j] = 't';
                    //$rs[$i-$j]['kode_kegiatan']=$kode_kegiatan;
                    $kode_kegiatan_arr[$i - $j] = $kode_kegiatan;
                    //$rs[$i-$j]['nama_kegiatan']=$nama_kegiatan;
                    $nama_kegiatan_arr[$i - $j] = $nama_kegiatan;
                    $j = 0;
                    $nilai_draft3 = 0;
                    $nilai_locked3 = 0;
                }
            }
            //$kode_kegiatan=$rs[$i]['old_kode_kegiatan'];
            $kode_kegiatan = $rs->getString('old_kode_kegiatan');
            //$nama_kegiatan=$rs[$i]['old_nama_kegiatan'];
            $nama_kegiatan = $rs->getString('old_nama_kegiatan');

            //$nilai_draft3+=$rs[$i]['nilai_draft4'];
            $nilai_draft3+=$rs->getString('nilai_draft4');
            //$nilai_locked3+=$rs[$i]['nilai_locked4'];
            $nilai_locked3+=$rs->getString('nilai_locked4');

            //$rs[$i]['nilai_draft4']=number_format($rs[$i]['nilai_draft4'],0,",",".");
            $nilai_draft4_arr[$i] = number_format($rs->getString('nilai_draft4'), 0, ",", ".");
            //$rs[$i]['nilai_locked4']=number_format($rs[$i]['nilai_locked4'],0,",",".");
            $nilai_locked4_arr[$i] = number_format($rs->getString('nilai_locked4'), 0, ",", ".");
            $i+=1;
            $j+=1;
            $k+=1;
        }

        $nilai_draft+=$nilai_draft3;
        $nilai_locked+=$nilai_locked3;

        //$rs[$i-$j]['nilai_draft3']=number_format($nilai_draft3,0,",",".");
        $nilai_draft3_arr[$i - $j] = number_format($nilai_draft3, 0, ",", ".");
        //$rs[$i-$j]['nilai_locked3']=number_format($nilai_locked3,0,",",".");
        $nilai_locked3_arr[$i - $j] = number_format($nilai_locked3, 0, ",", ".");
        //$rs[$i-$j]['kegiatan_ok']='t';
        $kegiatan_ok_arr[$i - $j] = 't';
        //$rs[$i-$j]['kode_kegiatan']=$kode_kegiatan;
        $kode_kegiatan_arr[$i - $j] = $kode_kegiatan;
        //$rs[$i-$j]['nama_kegiatan']=$nama_kegiatan;
        $nama_kegiatan_arr[$i - $j] = $nama_kegiatan;

        $nilai_draft2+=$nilai_draft;
        $nilai_locked2+=$nilai_locked;

        //$rs[$i-$k]['nilai_draft']=number_format($nilai_draft,0,",",".");
        $nilai_draft_arr[$i - $k] = number_format($nilai_draft, 0, ",", ".");
        //$rs[$i-$k]['nilai_locked']=number_format($nilai_locked,0,",",".");
        $nilai_locked_arr[$i - $k] = number_format($nilai_locked, 0, ",", ".");
        //$rs[$i-$k]['dinas_ok']='t';
        $dinas_ok_arr[$i - $k] = 't';
        //$rs[$i-$k]['unit_id']=$unit_id;
        $unit_id_arr[$i - $k] = $unit_id;
        //$rs[$i-$k]['unit_name']=$unit_name;
        $unit_name_arr[$i - $k] = $unit_name;

        $this->nilai_draft2 = number_format($nilai_draft2, 0, ",", ".");
        $this->nilai_locked2 = number_format($nilai_locked2, 0, ",", ".");

        $this->unit_id = $unit_id_arr;

        $this->unit_name = $unit_name_arr;

        $this->dinas_ok = $dinas_ok_arr;

        $this->kegiatan_ok = $kegiatan_ok_arr;

        $this->nilai_draft = $nilai_draft_arr;

        $this->nilai_locked = $nilai_locked_arr;

        $this->kode_kegiatan = $kode_kegiatan_arr;

        $this->nama_kegiatan = $nama_kegiatan_arr;

        $this->nilai_draft3 = $nilai_draft3_arr;

        $this->nilai_locked3 = $nilai_locked3_arr;

        $this->nilai_draft4 = $nilai_draft4_arr;

        $this->nilai_locked4 = $nilai_locked4_arr;

        $this->subtitle = $subtitle_arr;
    }

    public function executeReportProgramKegiatan() {
        $query = "select kode_program as old_kode_program,nama_program as old_nama_program, unit_id as old_unit_id, unit_name as old_unit_name,kode_kegiatan,nama_kegiatan,((jumlah_draft*jumlah_non_lanjutan)) as total_draft4,((jumlah_setuju*jumlah_non_lanjutan)) as total_locked4,((nilai_draft*jumlah_non_lanjutan)) as nilai_draft4, ((nilai_setuju*jumlah_setuju*jumlah_non_lanjutan)) as nilai_locked4
			FROM " . sfConfig::get('app_default_schema') . ".v_dinas_kegiatan_rincian4
			WHERE unit_id<>'9999'
			ORDER BY kode_program,nama_program ,unit_id,unit_name,kode_kegiatan,nama_kegiatan ";

        $query2 = "select sum(komponen_harga_awal*volume*(100+pajak)/100) as sum_draft
	 	from " . sfConfig::get('app_default_schema') . ".rincian r," . sfConfig::get('app_default_schema') . ".master_kegiatan k," . sfConfig::get('app_default_schema') . ".rincian_detail d 
		where k.kode_kegiatan=r.kegiatan_code and k.unit_id=r.unit_id and 
		d.kegiatan_code=r.kegiatan_code and d.unit_id=r.unit_id and r.unit_id<>'9999'";
        //$result2 = @pg_query($conn_id,$query2);
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query2);
        $rs = $stmt->executeQuery();
        //while($rows2 = @pg_fetch_assoc($result2))	extract($rows2,EXTR_OVERWRITE);
        while ($rs->next()) {
            $sum_draft = $rs->getString('sum_draft');
        }

        $query2 = "select sum(komponen_harga_awal*volume*(100+pajak)/100) as sum_locked
	 	from " . sfConfig::get('app_default_schema') . ".rincian r," . sfConfig::get('app_default_schema') . ".master_kegiatan k," . sfConfig::get('app_default_schema') . ".rincian_detail d 
		where k.kode_kegiatan=r.kegiatan_code and k.unit_id=r.unit_id and 
		d.kegiatan_code=r.kegiatan_code and d.unit_id=r.unit_id and r.unit_id<>'9999' and r.rincian_level=3";
        //$result2 = @pg_query($conn_id,$query2);
        $stmt = $con->prepareStatement($query2);
        $rs = $stmt->executeQuery();
        //while($rows2 = @pg_fetch_assoc($result2))	extract($rows2,EXTR_OVERWRITE);
        while ($rs->next()) {
            $sum_locked = $rs->getString('sum_locked');
        }

        $query2 = "Select kode_program,nama_program from " . sfConfig::get('app_default_schema') . ".master_program order by kode_program limit 1";
        //$result = @pg_query($conn_id,$query2);
        $stmt = $con->prepareStatement($query2);
        $rs = $stmt->executeQuery();
        //while($rows = @pg_fetch_assoc($result))extract($rows,EXTR_OVERWRITE);
        while ($rs->next()) {
            $kode_program = $rs->getString('kode_program');
            $nama_program = $rs->getString('nama_program');
        }

        $query2 = "SELECT unit_id ,unit_name FROM " . sfConfig::get('app_default_schema') . ".v_dinas_kegiatan WHERE kode_program='$kode_program' order by 	kode_program,unit_id LIMIT 1";
        //$result2 = @pg_query($conn_id,$query2);
        $stmt = $con->prepareStatement($query2);
        $rs = $stmt->executeQuery();
        //while($rows2 = @pg_fetch_assoc($result2))extract($rows2,EXTR_OVERWRITE);
        while ($rs->next()) {
            $unit_id = $rs->getString('unit_id');
            $unit_name = $rs->getString('unit_name');
        }

        //$result = @pg_query($conn_id,$query);
        $stmt = $con->prepareStatement($query);
        $rs = $stmt->executeQuery();
        //if(!$result) echo @pg_last_error($conn_id);
        //$rs=pg_fetch_all($result);
        $this->rs = $rs;

        if ($sum_locked == 0 or !$sum_locked)
            $sum_locked = 99999999999;
        if ($sum_draft == 0 or !$sum_draft)
            $sum_draft = 99999999999;
        $i = 0;
        $j = 0;
        $k = 0;


        //foreach($rs as $s)
        $nilai_draft3 = 0;
        $nilai_locked3 = 0;
        $nilai_draft = 0;
        $nilai_locked = 0;

        $kode_program_arr = array();
        $this->kode_program = array();

        $nama_program_arr = array();
        $this->nama_program = array();

        $nilai_draft_arr = array();
        $this->nilai_draft = array();

        $persen_draft_arr = array();
        $this->persen_draft = array();

        $nilai_locked_arr = array();
        $this->nilai_locked = array();

        $persen_locked_arr = array();
        $this->persen_locked = array();

        $unit_id_arr = array();
        $this->unit_id = array();

        $unit_name_arr = array();
        $this->unit_name = array();

        $nilai_draft3_arr = array();
        $this->nilai_draft3 = array();

        $persen_draft3_arr = array();
        $this->persen_draft3 = array();

        $nilai_locked3_arr = array();
        $this->nilai_locked3 = array();

        $persen_locked3_arr = array();
        $this->persen_locked3 = array();

        $nilai_draft4_arr = array();
        $this->nilai_draft4 = array();

        $persen_draft4_arr = array();
        $this->persen_draft4 = array();

        $nilai_locked4 = array();
        $this->nilai_locked4 = array();

        $persen_locked4_arr = array();
        $this->persen_locked4 = array();

        $dinas_ok_arr = array();
        $this->dinas_ok = array();

        $kegiatan_ok_arr = array();
        $this->kegiatan_ok = array();
        $nilai_draft2 = 0;
        $nilai_locked2 = 0;
        $persen_draft3 = 0;
        while ($rs->next()) {
            if ($unit_id != $rs->getString('old_unit_id')) {
                $udah = 'f';
                if ($kode_program != $rs->getString('old_kode_program')) {
                    $nilai_draft+=$nilai_draft3;
                    $nilai_locked+=$nilai_locked3;

                    $nilai_draft2+=$nilai_draft;
                    $nilai_locked2+=$nilai_locked;

                    $persen_draft = $nilai_draft * 100 / $sum_draft;
                    //$rs[$i-$k]['persen_draft']=number_format($persen_draft,2,",",".");
                    $persen_draft_arr[$i - $k] = number_format($persen_draft, 2, ",", ".");
                    $persen_locked = $nilai_locked * 100 / $sum_locked;
                    //$rs[$i-$k]['persen_locked']=number_format($persen_locked,2,",",".");
                    $persen_locked_arr[$i - $k] = number_format($persen_locked, 2, ",", ".");
                    //$rs[$i-$k]['nilai_draft']=number_format($nilai_draft,0,",",".");
                    $nilai_draft_arr[$i - $k] = number_format($nilai_draft, 0, ",", ".");
                    //$rs[$i-$k]['nilai_locked']=number_format($nilai_locked,0,",",".");
                    $nilai_locked_arr[$i - $k] = number_format($nilai_locked, 0, ",", ".");
                    //$rs[$i-$k]['dinas_ok']='t';
                    $dinas_ok_arr[$i - $k] = 't';
                    //$rs[$i-$k]['kode_program']=$kode_program;
                    $kode_program_arr[$i - $k] = $kode_program;
                    //$rs[$i-$k]['nama_program']=$nama_program;
                    $nama_program_arr[$i - $k] = $nama_program;
                    $k = 0;
                    $nilai_draft = 0;
                    $nilai_locked = 0;
                    $udah = 't';
                    $persen_draft3 = $nilai_draft3 * 100 / $sum_draft;
                    $persen_draft3_arr[$i - $j] = number_format($persen_draft3, 2, ",", ".");
                    $persen_locked3 = $nilai_locked3 * 100 / $sum_locked;
                    $persen_locked3_arr[$i - $j] = number_format($persen_locked3, 2, ",", ".");
                    //$rs[$i-$j]['nilai_draft3']=number_format($nilai_draft3,0,",",".");
                    $nilai_draft3_arr[$i - $j] = number_format($nilai_draft3, 0, ",", ".");
                    //$rs[$i-$j]['nilai_locked3']=number_format($nilai_locked3,0,",",".");
                    $nilai_locked3_arr[$i - $j] = number_format($nilai_locked3, 0, ",", ".");
                    //$rs[$i-$j]['kegiatan_ok']='t';
                    $kegiatan_ok_arr[$i - $j] = 't';
                    //$rs[$i-$j]['unit_id']=$unit_id;
                    $unit_id_arr[$i - $j] = $unit_id;
                    //$rs[$i-$j]['unit_name']=$unit_name;
                    $unit_name_arr[$i - $j] = $unit_name;
                    $j = 0;
                    $nilai_draft3 = 0;
                    $nilai_locked3 = 0;
                }
                //$kode_program=$rs[$i]['old_kode_program'];
                $kode_program = $rs->getString('old_kode_program');
                //$nama_program=$rs[$i]['old_nama_program'];
                $nama_program = $rs->getString('old_nama_program');
                //$rs[$i]['kode_program']=$kode_program;
                $kode_program_arr[$i] = $kode_program;
                $nilai_draft+=$nilai_draft3;
                $nilai_locked+=$nilai_locked3;

                if ($udah != 't') {
                    $persen_draft3 = $nilai_draft3 * 100 / $sum_draft;
                    //$rs[$i-$j]['persen_draft3']=number_format($persen_draft3,2,",",".");
                    $persen_draft3_arr[$i - $j] = number_format($persen_draft3, 2, ",", ".");
                    $persen_locked3 = $nilai_locked3 * 100 / $sum_locked;
                    //$rs[$i-$j]['persen_locked3']=number_format($persen_locked3,2,",",".");
                    $persen_locked3_arr[$i - $j] = number_format($persen_locked3, 2, ",", ".");
                    //$rs[$i-$j]['nilai_draft3']=number_format($nilai_draft3,0,",",".");
                    $nilai_draft3_arr[$i - $j] = number_format($nilai_draft3, 0, ",", ".");
                    //$rs[$i-$j]['nilai_locked3']=number_format($nilai_locked3,0,",",".");
                    $nilai_locked3_arr[$i - $j] = number_format($nilai_locked3, 0, ",", ".");
                    //$rs[$i-$j]['kegiatan_ok']='t';
                    $kegiatan_ok_arr[$i - $j] = 't';
                    //$rs[$i-$j]['unit_id']=$unit_id;
                    $unit_id_arr[$i - $j] = $unit_id;
                    //$rs[$i-$j]['unit_name']=$unit_name;
                    $unit_name_arr[$i - $j] = $unit_name;
                    $j = 0;
                    $nilai_draft3 = 0;
                    $nilai_locked3 = 0;
                }
            }
            $unit_id = $rs->getString('old_unit_id');
            $unit_name = $rs->getString('old_unit_name');

            $nilai_draft3+=$rs->getString('nilai_draft4');
            $nilai_locked3+=$rs->getString('nilai_locked4');
            $persen_draft4 = $rs->getString('nilai_draft4') * 100 / $sum_draft;
            //$rs[$i]['persen_draft4']=number_format($persen_draft4,2,",",".");
            $persen_draft4_arr[$i] = number_format($persen_draft4, 2, ",", ".");
            $persen_locked4 = $rs->getString('nilai_locked4') * 100 / $sum_locked;
            //$rs[$i]['persen_locked4']=number_format($persen_locked4,2,",",".");
            $persen_locked4_arr[$i] = number_format($persen_locked4, 2, ",", ".");
            //$rs[$i]['nilai_draft4']=number_format($rs[$i]['nilai_draft4'],0,",",".");
            $nilai_draft4_arr[$i] = number_format($rs->getString('nilai_draft4'), 0, ",", ".");
            //$rs[$i]['nilai_locked4']=number_format($rs[$i]['nilai_locked4'],0,",",".");
            $nilai_locked4_arr[$i] = number_format($rs->getString('nilai_locked4'), 0, ",", ".");
            $i+=1;
            $j+=1;
            $k+=1;
        }

        $nilai_draft+=$nilai_draft3;
        $nilai_locked+=$nilai_locked3;

        $persen_draft3 = $nilai_draft3 * 100 / $sum_draft;
        //$rs[$i-$j]['persen_draft3']=number_format($persen_draft3,2,",",".");
        $persen_draft3_arr[$i - $j] = number_format($persen_draft3, 2, ",", ".");
        $persen_locked3 = $nilai_locked3 * 100 / $sum_locked;
        //$rs[$i-$j]['persen_locked3']=number_format($persen_locked3,2,",",".");
        $persen_locked3_arr[$i - $j] = number_format($persen_locked3, 2, ",", ".");
        //$rs[$i-$j]['nilai_draft3']=number_format($nilai_draft3,0,",",".");
        $nilai_draft3_arr[$i - $j] = number_format($nilai_draft3, 0, ",", ".");
        //$rs[$i-$j]['nilai_locked3']=number_format($nilai_locked3,0,",",".");
        $nilai_locked3_arr[$i - $j] = number_format($nilai_locked3, 0, ",", ".");
        //$rs[$i-$j]['kegiatan_ok']='t';
        $kegiatan_ok_arr[$i - $j] = 't';
        //$rs[$i-$j]['unit_id']=$unit_id;
        $unit_id_arr[$i - $j] = $unit_id;
        //$rs[$i-$j]['unit_name']=$unit_name;
        $unit_name_arr[$i - $j] = $unit_name;

        $nilai_draft2+=$nilai_draft;
        $nilai_locked2+=$nilai_locked;

        $persen_draft = $nilai_draft * 100 / $sum_draft;
        //$rs[$i-$k]['persen_draft']=number_format($persen_draft,2,",",".");
        $persen_draft_arr[$i - $k] = number_format($persen_draft, 2, ",", ".");
        $persen_locked = $nilai_locked * 100 / $sum_locked;
        //$rs[$i-$k]['persen_locked']=number_format($persen_locked,2,",",".");
        $persen_locked_arr[$i - $k] = number_format($persen_locked, 2, ",", ".");
        //$rs[$i-$k]['nilai_draft']=number_format($nilai_draft,0,",",".");
        $nilai_draft_arr[$i - $k] = number_format($nilai_draft, 0, ",", ".");
        //$rs[$i-$k]['nilai_locked']=number_format($nilai_locked,0,",",".");
        $nilai_locked_arr[$i - $k] = number_format($nilai_locked, 0, ",", ".");
        //$rs[$i-$k]['dinas_ok']='t';
        $dinas_ok_arr[$i - $k] = 't';
        //$rs[$i-$k]['kode_program']=$kode_program;
        $kode_program_arr[$i - $k] = $kode_program;
        //$rs[$i-$k]['nama_program']=$nama_program;
        $nama_program_arr[$i - $k] = $nama_program;

        $this->nilai_draft2 = number_format($nilai_draft2, 0, ",", ".");
        $this->nilai_locked2 = number_format($nilai_locked2, 0, ",", ".");


        $this->kode_program = $kode_program_arr;

        $this->nama_program = $nama_program_arr;

        $this->nilai_draft = $nilai_draft_arr;

        $this->persen_draft = $persen_draft_arr;

        $this->nilai_locked = $nilai_locked_arr;

        $this->persen_locked = $persen_locked_arr;

        $this->unit_id = $unit_id_arr;

        $this->unit_name = $unit_name_arr;

        $this->nilai_draft3 = $nilai_draft3_arr;

        $this->persen_draft3 = $persen_draft3_arr;

        $this->nilai_locked3 = $nilai_locked3_arr;

        $this->persen_locked3 = $persen_locked3_arr;

        $this->nilai_draft4 = $nilai_draft4_arr;

        $this->persen_draft4 = $persen_draft4_arr;

        $this->nilai_locked4 = $nilai_locked4_arr;

        $this->persen_locked4 = $persen_locked4_arr;

        $this->dinas_ok = $dinas_ok_arr;

        $this->kegiatan_ok = $kegiatan_ok_arr;
    }

    public function executeReportProgram() {
        $query = "select kode_program, nama_program ,sum((jumlah_draft*jumlah_non_lanjutan)) as total_draft3,sum((jumlah_setuju*jumlah_non_lanjutan)) as total_locked3, sum((nilai_draft*jumlah_non_lanjutan)) as nilai_draft3, sum((nilai_setuju*jumlah_setuju*jumlah_non_lanjutan)) as nilai_locked3
			FROM " . sfConfig::get('app_default_schema') . ".v_dinas_kegiatan_rincian4
			WHERE unit_id<>'9999'
			GROUP BY kode_program, nama_program
			ORDER BY kode_program, nama_program";

        $query2 = "select sum(komponen_harga_awal*volume*(100+pajak)/100) as sum_draft
	 	from " . sfConfig::get('app_default_schema') . ".rincian r," . sfConfig::get('app_default_schema') . ".master_kegiatan k," . sfConfig::get('app_default_schema') . ".rincian_detail d 
		where k.kode_kegiatan=r.kegiatan_code and k.unit_id=r.unit_id and 
		d.kegiatan_code=r.kegiatan_code and d.unit_id=r.unit_id and r.unit_id<>'9999'";
        //$result2 = @pg_query($conn_id,$query2);
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query2);
        $rs = $stmt->executeQuery();
        //while($rows2 = @pg_fetch_assoc($result2))	extract($rows2,EXTR_OVERWRITE);
        while ($rs->next()) {
            $sum_draft = $rs->getString('sum_draft');
        }

        $query2 = "select sum(komponen_harga_awal*volume*(100+pajak)/100) as sum_locked
	 	from " . sfConfig::get('app_default_schema') . ".rincian r," . sfConfig::get('app_default_schema') . ".master_kegiatan k," . sfConfig::get('app_default_schema') . ".rincian_detail d 
		where k.kode_kegiatan=r.kegiatan_code and k.unit_id=r.unit_id and 
		d.kegiatan_code=r.kegiatan_code and d.unit_id=r.unit_id and r.unit_id<>'9999' and r.rincian_level=3";
        //$result2 = @pg_query($conn_id,$query2);
        $stmt = $con->prepareStatement($query2);
        $rs = $stmt->executeQuery();
        //while($rows2 = @pg_fetch_assoc($result2))	extract($rows2,EXTR_OVERWRITE);
        while ($rs->next()) {
            $sum_locked = $rs->getString('sum_locked');
        }

        //$result = @pg_query($conn_id,$query);
        $stmt = $con->prepareStatement($query);
        //$rs=pg_fetch_all($result);
        $rs = $stmt->executeQuery();
        $this->rs = $rs;

        if ($sum_locked == 0 or !$sum_locked)
            $sum_locked = 99999999999;
        if ($sum_draft == 0 or !$sum_draft)
            $sum_draft = 99999999999;
        $i = 0;

        //foreach($rs as $s)
        $nilai_draft3_arr = array();
        $this->nilai_draft3 = array();

        $persen_draft3_arr = array();
        $this->persen_draft3 = array();

        $nilai_locked3_arr = array();
        $this->nilai_locked3 = array();

        $persen_locked3_arr = array();
        $this->persen_locked3 = array();
        $nilai_draft2 = 0;
        $nilai_locked2 = 0;
        $total_draft2 = 0;
        $total_locked2 = 0;
        while ($rs->next()) {
            //$rs[$i]['persen_draft3']=$rs[$i]['nilai_draft3'] * 100 / $sum_draft; 
            $persen_draft3_arr[$i] = $rs->getString('nilai_draft3') * 100 / $sum_draft;
            //$rs[$i]['persen_draft3']=number_format($rs[$i]['persen_draft3'],2,",",".");
            $persen_draft3_arr[$i] = number_format($persen_draft3_arr[$i], 2, ",", ".");
            //$rs[$i]['persen_locked3']=$rs[$i]['nilai_locked3'] * 100 / $sum_locked; 
            $persen_locked3_arr[$i] = $rs->getString('nilai_locked3') * 100 / $sum_locked;
            //$rs[$i]['persen_locked3']=number_format($rs[$i]['persen_locked3'],2,",",".");
            $persen_locked3_arr[$i] = number_format($persen_locked3_arr[$i], 2, ",", ".");

            //$nilai_draft2+=$rs[$i]['nilai_draft3'];
            $nilai_draft2+=$rs->getString('nilai_draft3');
            //$total_draft2+=$rs[$i]['total_draft3'];
            $total_draft2+=$rs->getString('total_draft3');
            //$nilai_locked2+=$rs[$i]['nilai_locked3'];
            $nilai_locked2+=$rs->getString('nilai_locked3');
            //$total_locked2+=$rs[$i]['total_locked3'];
            $total_locked2+=$rs->getString('total_locked3');
            //$rs[$i]['nilai_draft3']=number_format($rs[$i]['nilai_draft3'],0,",",".");
            $nilai_draft3_arr[$i] = number_format($rs->getString('nilai_draft3'), 0, ",", ".");
            //$rs[$i]['nilai_locked3']=number_format($rs[$i]['nilai_locked3'],0,",",".");
            $nilai_locked3_arr[$i] = number_format($rs->getString('nilai_locked3'), 0, ",", ".");
            $i+=1;
        }

        $this->nilai_draft2 = number_format($nilai_draft2, 0, ",", ".");
        $this->nilai_locked2 = number_format($nilai_locked2, 0, ",", ".");

        $this->nilai_draft3 = $nilai_draft3_arr;

        $this->persen_draft3 = $persen_draft3_arr;

        $this->nilai_locked3 = $nilai_locked3_arr;

        $this->persen_locked3 = $persen_locked3_arr;
    }

    public function executeReportProgramP13() {
        $query = "select kode_bidang, nama_bidang ,sum((jumlah_draft*jumlah_non_lanjutan)) as total_draft3,sum((jumlah_setuju*jumlah_non_lanjutan)) as total_locked3, sum((nilai_draft*jumlah_non_lanjutan)) as nilai_draft3, sum((nilai_setuju*jumlah_setuju*jumlah_non_lanjutan)) as nilai_locked3
			FROM " . sfConfig::get('app_default_schema') . ".v_dinas_kegiatan_rincian4
			WHERE unit_id<>'9999'
			GROUP BY kode_bidang, nama_bidang
			ORDER BY kode_bidang, nama_bidang";

        $query2 = "select sum(komponen_harga_awal*volume*(100+pajak)/100) as sum_draft
		from " . sfConfig::get('app_default_schema') . ".rincian r," . sfConfig::get('app_default_schema') . ".master_kegiatan k," . sfConfig::get('app_default_schema') . ".rincian_detail d 
		where k.kode_kegiatan=r.kegiatan_code and k.unit_id=r.unit_id and 
		d.kegiatan_code=r.kegiatan_code and d.unit_id=r.unit_id and r.unit_id<>'9999'";
        //$result2 = @pg_query($conn_id,$query2);
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query2);
        //while($rows2 = @pg_fetch_assoc($result2))	extract($rows2,EXTR_OVERWRITE);
        $rs = $stmt->executeQuery();
        while ($rs->next()) {
            $sum_draft = $rs->getString('sum_draft');
        }

        $query2 = "select sum(komponen_harga_awal*volume*(100+pajak)/100) as sum_locked
	 	from " . sfConfig::get('app_default_schema') . ".rincian r," . sfConfig::get('app_default_schema') . ".master_kegiatan k," . sfConfig::get('app_default_schema') . ".rincian_detail d 
		where k.kode_kegiatan=r.kegiatan_code and k.unit_id=r.unit_id and 
		d.kegiatan_code=r.kegiatan_code and d.unit_id=r.unit_id and r.unit_id<>'9999' and r.rincian_level=3";
        //$result2 = @pg_query($conn_id,$query2);
        $stmt = $con->prepareStatement($query2);
        //while($rows2 = @pg_fetch_assoc($result2))	extract($rows2,EXTR_OVERWRITE);
        $rs = $stmt->executeQuery();
        while ($rs->next()) {
            $sum_locked = $rs->getString('sum_locked');
        }

        //$result = @pg_query($conn_id,$query);
        $stmt = $con->prepareStatement($query);
        //$rs=pg_fetch_all($result);
        $rs = $stmt->executeQuery();
        $this->rs = $rs;

        if ($sum_locked == 0 or !$sum_locked)
            $sum_locked = 99999999999;
        if ($sum_draft == 0 or !$sum_draft)
            $sum_draft = 99999999999;
        $i = 0;

        //foreach($rs as $s)
        $nilai_draft3_arr = array();
        $this->nilai_draft3 = array();

        $persen_draft3_arr = array();
        $this->persen_draft3 = array();

        $nilai_locked3_arr = array();
        $this->nilai_locked3 = array();

        $persen_locked3_arr = array();
        $this->persen_locked3 = array();
        $nilai_draft2 = 0;
        $total_draft2 = 0;
        $nilai_locked2 = 0;
        $total_locked2 = 0;
        while ($rs->next()) {
            //$rs[$i]['persen_draft3']=$rs[$i]['nilai_draft3'] * 100 / $sum_draft; 
            $persen_draft3_arr[$i] = $rs->getString('nilai_draft3') * 100 / $sum_draft;
            //$rs[$i]['persen_draft3']=number_format($rs[$i]['persen_draft3'],2,",",".");
            $persen_draft3_arr[$i] = number_format($persen_draft3_arr[$i], 2, ",", ".");
            //$rs[$i]['persen_locked3']=$rs[$i]['nilai_locked3'] * 100 / $sum_locked; 
            $persen_locked3_arr[$i] = $rs->getString('nilai_locked3') * 100 / $sum_locked;
            //$rs[$i]['persen_locked3']=number_format($rs[$i]['persen_locked3'],2,",",".");
            $persen_locked3_arr[$i] = number_format($persen_locked3_arr[$i], 2, ",", ".");

            //$nilai_draft2+=$rs[$i]['nilai_draft3'];
            $nilai_draft2+=$rs->getString('nilai_draft3');
            //$total_draft2+=$rs[$i]['total_draft3'];
            $total_draft2+=$rs->getString('total_draft3');
            //$nilai_locked2+=$rs[$i]['nilai_locked3'];
            $nilai_locked2+=$rs->getString('nilai_locked3');
            //$total_locked2+=$rs[$i]['total_locked3'];
            $total_locked2+=$rs->getString('total_locked3');
            //$rs[$i]['nilai_draft3']=number_format($rs[$i]['nilai_draft3'],0,",",".");
            $nilai_draft3_arr[$i] = number_format($rs->getString('nilai_draft3'), 0, ",", ".");
            //$rs[$i]['nilai_locked3']=number_format($rs[$i]['nilai_locked3'],0,",",".");
            $nilai_locked3_arr[$i] = number_format($rs->getString('nilai_locked3'), 0, ",", ".");
            $i+=1;
        }

        //$nilai_draft2=number_format($nilai_draft2,0,",",".");
        $this->nilai_draft2 = number_format($nilai_draft2, 0, ",", ".");
        $this->nilai_locked2 = number_format($nilai_locked2, 0, ",", ".");

        $this->nilai_draft3 = $nilai_draft3_arr;

        $this->persen_draft3 = $persen_draft3_arr;

        $this->nilai_locked3 = $nilai_locked3_arr;

        $this->persen_locked3 = $persen_locked3_arr;
    }

    public function executeReportDinasKegiatanBelanja($dinas) {
        $bek = '';
        if ($dinas != 'semua')
            $bek = " and unit_id=" . $dinas;

        $query = "select unit_id as old_unit_id, unit_name as old_unit_name,kode_kegiatan as old_kode_kegiatan, nama_kegiatan as old_nama_kegiatan,belanja_name,sum ((jumlah_draft)) as total_draft4,sum((jumlah_setuju)) as total_locked4,
 		sum((nilai_draft)) as nilai_draft4, sum((nilai_draft*jumlah_setuju)) as nilai_locked4 
		FROM " . sfConfig::get('app_default_schema') . ".v_kegiatan_rekening
		WHERE unit_id<>'9999' $bek
		GROUP BY unit_id,unit_name,kode_kegiatan,nama_kegiatan,belanja_name
		ORDER BY unit_id,unit_name,kode_kegiatan,nama_kegiatan,belanja_name";

        $query2 = "Select unit_id,kode_kegiatan from " . sfConfig::get('app_default_schema') . ".master_kegiatan order by unit_id,kode_kegiatan limit 1";
        //$result = @pg_query($conn_id,$query2);
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query2);
        $rs = $stmt->executeQuery();
        //while($rows = @pg_fetch_assoc($result))extract($rows,EXTR_OVERWRITE);
        $unit_name = '';
        $nama_kegiatan = '';
        $nilai_draft = 0;
        $this->nilai_draft3 = 0;
        $nilai_locked = 0;
        $this->nilai_locked3 = 0;
        $this->nilai_draft2 = 0;
        $this->nilai_locked2 = 0;
        $this->nilai_draft = array();
        $this->nilai_locked = array();
        $this->dinas_ok = array();
        $this->unit_id = array();
        $this->unit_name = array();
        $this->nilai_draft3 = array();
        $this->nilai_locked3 = array();
        $this->kegiatan_ok = array();
        $this->kode_kegiatan = array();
        $this->nama_kegiatan = array();
        $this->nilai_draft4 = array();
        $this->nilai_locked4 = array();
        $nilai_draft3 = 0;
        $nilai_locked3 = 0;
        $nilai_draft2 = 0;
        $nilai_locked2 = 0;
        $this->belanja_name = array();
        while ($rs->next()) {
            $unit_id = $rs->getString('unit_id');
            $kode_kegiatan = $rs->getString('kode_kegiatan');
        }

        //$result = @pg_query($conn_id,$query);
        $stmt = $con->prepareStatement($query);
        $rs2 = $stmt->executeQuery();
        $this->rs = $rs2;

        //if (pg_num_rows($result)>0)
        //if($rs2->next())
        //{
        $i = 0;
        $j = 0;
        $k = 0;
        //foreach($rs as $s)
        while ($rs2->next()) {
            $this->belanja_name[$i] = $rs2->getString('belanja_name');
            if ($kode_kegiatan != $rs2->getString('old_kode_kegiatan')) {
                $udah = 'f';
                if ($unit_id != $rs2->getString('old_unit_id')) {

                    $nilai_draft+=$nilai_draft3;
                    $nilai_locked+=$nilai_locked3;
                    $this->nilai_draft2+=$nilai_draft;
                    $this->nilai_locked2+=$nilai_locked;

                    $this->nilai_draft[$i - $k] = number_format($nilai_draft, 0, ",", ".");
                    $this->nilai_locked[$i - $k] = number_format($nilai_locked, 0, ",", ".");
                    $this->dinas_ok[$i - $k] = 't';
                    $this->unit_id[$i - $k] = $unit_id;
                    $this->unit_name[$i - $k] = $unit_name;
                    $k = 0;
                    $nilai_draft = 0;
                    $nilai_locked = 0;
                    $udah = 't';
                    $this->nilai_draft3[$i - $j] = number_format($nilai_draft3, 0, ",", ".");
                    $this->nilai_locked3[$i - $j] = number_format($nilai_locked3, 0, ",", ".");
                    $this->kegiatan_ok[$i - $j] = 't';
                    $this->kode_kegiatan[$i - $j] = $kode_kegiatan;
                    $this->nama_kegiatan[$i - $j] = $nama_kegiatan;
                    $nilai_draft3 = 0;
                    $nilai_locked3 = 0;
                    $j = 0;
                }
                $unit_id = $rs2->getString('old_unit_id');
                $unit_name = $rs2->getString('old_unit_name');
                $this->unit_id[$i] = $unit_id;

                $nilai_draft+=$nilai_draft3;
                $nilai_locked+=$nilai_locked3;

                if ($udah != 't') {

                    $this->nilai_draft3[$i - $j] = number_format($nilai_draft3, 0, ",", ".");
                    $this->nilai_locked3[$i - $j] = number_format($nilai_locked3, 0, ",", ".");
                    $this->kegiatan_ok[$i - $j] = 't';
                    $this->kode_kegiatan[$i - $j] = $kode_kegiatan;
                    $this->nama_kegiatan[$i - $j] = $nama_kegiatan;
                    $j = 0;
                    $nilai_draft3 = 0;
                    $nilai_locked3 = 0;
                }
            }
            $kode_kegiatan = $rs2->getString('old_kode_kegiatan');
            $nama_kegiatan = $rs2->getString('old_nama_kegiatan');

            $nilai_draft3+=$rs2->getString('nilai_draft4');
            $nilai_locked3+=$rs2->getString('nilai_locked4');

            $this->nilai_draft4[$i] = number_format($rs2->getString('nilai_draft4'), 0, ",", ".");
            $this->nilai_locked4[$i] = number_format($rs2->getString('nilai_locked4'), 0, ",", ".");
            $i+=1;
            $j+=1;
            $k+=1;
        }

        $nilai_draft+=$nilai_draft3;
        $nilai_locked+=$nilai_locked3;

        $this->nilai_draft3[$i - $j] = number_format($nilai_draft3, 0, ",", ".");
        $this->nilai_locked3[$i - $j] = number_format($nilai_locked3, 0, ",", ".");
        $this->kegiatan_ok[$i - $j] = 't';
        $this->kode_kegiatan[$i - $j] = $kode_kegiatan;
        $this->nama_kegiatan[$i - $j] = $nama_kegiatan;

        $nilai_draft2+=$nilai_draft;
        $nilai_locked2+=$nilai_locked;

        $this->nilai_draft[$i - $k] = number_format($nilai_draft, 0, ",", ".");
        $this->nilai_locked[$i - $k] = number_format($nilai_locked, 0, ",", ".");
        $this->dinas_ok[$i - $k] = 't';
        $this->unit_id[$i - $k] = $unit_id;
        $this->unit_name[$i - $k] = $unit_name;

        $this->nilai_draft2 = number_format($nilai_draft2, 0, ",", ".");
        $this->nilai_locked2 = number_format($nilai_locked2, 0, ",", ".");
        //}
    }

    public function executePilihDinas() {
        $query = "select unit_name,unit_id from unit_kerja order by unit_name";
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query);
        $rs = $stmt->executeQuery();
        $this->dinas = array();
        $this->dinas['semua'] = '***Tampilkan Semua***';
        while ($rs->next()) {
            $this->dinas[$rs->getString('unit_id')] = $rs->getString('unit_name');
        }
        //$this->dinas=UnitKerjaPeer::doSelect(new Criteria());
    }

    public function executeReportProgramDinas() {
        $query = "select kode_program as old_kode_program,nama_program as old_nama_program, unit_id, unit_name,sum((jumlah_draft*jumlah_non_lanjutan)) as total_draft3,sum((jumlah_setuju*jumlah_non_lanjutan)) as total_locked3,
 		sum((nilai_draft*jumlah_non_lanjutan)) as nilai_draft3, sum((nilai_setuju*jumlah_setuju*jumlah_non_lanjutan)) as nilai_locked3 FROM " . sfConfig::get('app_default_schema') . ".v_dinas_kegiatan_rincian4 WHERE unit_id<>'9999' GROUP BY kode_program,nama_program ,unit_id,unit_name	ORDER BY kode_program,nama_program ,unit_id,unit_name ";

        $query2 = "Select kode_program,nama_program from " . sfConfig::get('app_default_schema') . ".master_program order by kode_program limit 1";
        //$result = @pg_query($conn_id,$query2);
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query2);
        $rs = $stmt->executeQuery();
        //while($rows = @pg_fetch_assoc($result))extract($rows,EXTR_OVERWRITE);
        while ($rs->next()) {
            $kode_program = $rs->getString('kode_program');
            $nama_program = $rs->getString('nama_program');
        }

        $query2 = "select sum(komponen_harga_awal*volume*(100+pajak)/100) as sum_draft
	 	from " . sfConfig::get('app_default_schema') . ".rincian r," . sfConfig::get('app_default_schema') . ".master_kegiatan k," . sfConfig::get('app_default_schema') . ".rincian_detail d where k.kode_kegiatan=r.kegiatan_code and k.unit_id=r.unit_id and d.kegiatan_code=r.kegiatan_code and d.unit_id=r.unit_id and r.unit_id<>'9999'";
        //$result2 = @pg_query($conn_id,$query2);
        $stmt = $con->prepareStatement($query2);
        //while($rows2 = @pg_fetch_assoc($result2))	extract($rows2,EXTR_OVERWRITE);
        $rs2 = $stmt->executeQuery();
        while ($rs2->next()) {
            $sum_draft = $rs2->getString('sum_draft');
        }

        $query2 = "select sum(komponen_harga_awal*volume*(100+pajak)/100) as sum_locked
	 	from " . sfConfig::get('app_default_schema') . ".rincian r," . sfConfig::get('app_default_schema') . ".master_kegiatan k," . sfConfig::get('app_default_schema') . ".rincian_detail d 
		where k.kode_kegiatan=r.kegiatan_code and k.unit_id=r.unit_id and 
		d.kegiatan_code=r.kegiatan_code and d.unit_id=r.unit_id and r.unit_id<>'9999' and r.rincian_level=3";
        //$result2 = @pg_query($conn_id,$query2);
        $stmt = $con->prepareStatement($query2);
        $rs3 = $stmt->executeQuery();
        //while($rows2 = @pg_fetch_assoc($result2))	extract($rows2,EXTR_OVERWRITE);
        while ($rs3->next()) {
            $sum_locked = $rs3->getString('sum_locked');
        }

        //$result = @pg_query($conn_id,$query);
        $stmt = $con->prepareStatement($query);
        $rs4 = $stmt->executeQuery();
        $this->rs = $rs4;
        //if(!$result) echo @pg_last_error($conn_id);
        //$rs=pg_fetch_all($result);

        if ($sum_locked == 0 or !$sum_locked)
            $sum_locked = 99999999999;
        if ($sum_draft == 0 or !$sum_draft)
            $sum_draft = 99999999999;
        $i = 0;
        $j = 0;
        //foreach($rs as $s)
        $nilai_draft = $nilai_locked = $this->nilai_draft2 = $this->nilai_locked2 = 0;

        $persen_draft_arr = array();
        $this->persen_draft = array();

        $persen_locked_arr = array();
        $this->persen_locked = array();

        $nilai_draft_arr = array();
        $this->nilai_draft = array();

        $nilai_locked_arr = array();
        $this->nilai_locked = array();

        $kode_program_arr = array();
        $this->kode_program = array();

        $nama_program_arr = array();
        $this->nama_program = array();

        $dinas_ok_arr = array();
        $this->dinas_ok = array();

        $persen_draft3_arr = array();
        $this->persen_draft3 = array();

        $persen_locked3_arr = array();
        $this->persen_locked3 = array();

        $nilai_draft3_arr = array();
        $this->nilai_draft3 = array();

        $nilai_locked3_arr = array();
        $this->nilai_locked3 = array();
        $unit_id = '';
        while ($rs4->next()) {
            if ($kode_program != $rs4->getString('old_kode_program')) {

                $this->nilai_draft2+=$nilai_draft;
                $this->nilai_locked2+=$nilai_locked;
                $persen_draft = $nilai_draft * 100 / $sum_draft;
                $persen_draft_arr[$i - $j] = number_format($persen_draft, 2, ",", ".");
                $persen_locked = $nilai_locked * 100 / $sum_locked;
                $persen_locked_arr[$i - $j] = number_format($persen_locked, 2, ",", ".");

                $nilai_draft_arr[$i - $j] = number_format($nilai_draft, 0, ",", ".");
                $nilai_locked_arr[$i - $j] = number_format($nilai_locked, 0, ",", ".");
                $dinas_ok_arr[$i - $j] = 't';
                $kode_program_arr[$i - $j] = $kode_program;
                $nama_program_arr[$i - $j] = $nama_program;
                $j = 0;
                $nilai_draft = 0;
                $nilai_locked = 0;
            }
            $kode_program = $rs4->getString('old_kode_program');
            $nama_program = $rs4->getString('old_nama_program');
            $kode_program_arr[$i] = $unit_id;
            $nilai_draft+=$rs4->getString('nilai_draft3');
            $nilai_locked+=$rs4->getString('nilai_locked3');

            $persen_draft3 = $rs4->getString('nilai_draft3') * 100 / $sum_draft;
            $persen_draft3_arr[$i] = number_format($persen_draft3, 2, ",", ".");
            $persen_locked3 = $rs4->getString('nilai_locked3') * 100 / $sum_locked;
            $persen_locked3_arr[$i] = number_format($persen_locked3, 2, ",", ".");

            $nilai_draft3_arr[$i] = number_format($rs4->getString('nilai_draft3'), 0, ",", ".");
            $nilai_locked3_arr[$i] = number_format($rs4->getString('nilai_locked3'), 0, ",", ".");

            $i+=1;
            $j+=1;
        }
        $this->nilai_draft2+=$nilai_draft;
        $this->nilai_locked2+=$nilai_locked;
        $persen_draft = $nilai_draft * 100 / $sum_draft;
        $persen_draft_arr[$i - $j] = number_format($persen_draft, 2, ",", ".");
        $persen_locked = $nilai_locked * 100 / $sum_locked;
        $persen_locked_arr[$i - $j] = number_format($persen_locked, 2, ",", ".");

        $nilai_draft_arr[$i - $j] = number_format($nilai_draft, 0, ",", ".");
        $nilai_locked_arr[$i - $j] = number_format($nilai_locked, 0, ",", ".");
        $dinas_ok_arr[$i - $j] = 't';
        $kode_program_arr[$i - $j] = $kode_program;
        $nama_program_arr[$i - $j] = $nama_program;

        $this->nilai_draft2 = number_format($this->nilai_draft2, 0, ",", ".");
        $this->nilai_locked2 = number_format($this->nilai_locked2, 0, ",", ".");

        $this->persen_draft = $persen_draft_arr;

        $this->persen_locked = $persen_locked_arr;

        $this->nilai_draft = $nilai_draft_arr;

        $this->nilai_locked = $nilai_locked_arr;

        $this->kode_program = $kode_program_arr;

        $this->nama_program = $nama_program_arr;

        $this->dinas_ok = $dinas_ok_arr;

        $this->persen_draft3 = $persen_draft3_arr;

        $this->persen_locked3 = $persen_locked3_arr;

        $this->nilai_draft3 = $nilai_draft3_arr;

        $this->nilai_locked3 = $nilai_locked3_arr;
    }

    public function executeReportBelanjaRekening() {
        $this->setFlash('RBK', 'selected');
        //$query="select belanja_id as old_belanja_id, belanja_name as old_belanja_name,rekening_code,rekening_name,sum ((jumlah_draft)) as total_draft3,sum((jumlah_setuju)) as total_locked3,sum((nilai_draft)) as nilai_draft3, sum((nilai_draft*jumlah_setuju)) as nilai_locked3 FROM ". sfConfig::get('app_default_schema') .".v_kegiatan_rekening WHERE unit_id<>'9999' GROUP BY belanja_id,belanja_name,rekening_code,rekening_name ORDER BY belanja_id,belanja_name,rekening_code,rekening_name";

        $query = "select belanja_id as old_belanja_id, belanja_name as old_belanja_name,rekening_code,rekening_name ,sum((jumlah_draft*jumlah_non_lanjutan)) as total_draft3,sum((jumlah_setuju*jumlah_non_lanjutan)) as total_locked3,
 					sum((nilai_draft*jumlah_non_lanjutan)) as nilai_draft3, sum((nilai_draft*jumlah_setuju*jumlah_non_lanjutan)) as nilai_locked3 
			FROM " . sfConfig::get('app_default_schema') . ".v_kegiatan_rekening
			WHERE unit_id<>'9999'
			GROUP BY belanja_id,belanja_name,rekening_code,rekening_name
			ORDER BY belanja_id,belanja_name,rekening_code,rekening_name";

        $query2 = "select sum(komponen_harga_awal*volume*(100+pajak)/100) as sum_draft from " . sfConfig::get('app_default_schema') . ".rincian r," . sfConfig::get('app_default_schema') . ".master_kegiatan k," . sfConfig::get('app_default_schema') . ".rincian_detail d	where k.kode_kegiatan=r.kegiatan_code and k.unit_id=r.unit_id and 
	d.kegiatan_code=r.kegiatan_code and d.unit_id=r.unit_id and r.unit_id<>'9999'";
        //$result2 = @pg_query($conn_id,$query2);
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query2);
        $rs = $stmt->executeQuery();
        //while($rows2 = @pg_fetch_assoc($result2))	extract($rows2,EXTR_OVERWRITE);
        while ($rs->next())
            $sum_draft = $rs->getString('sum_draft');

        $query2 = "select sum(komponen_harga_awal*volume*(100+pajak)/100) as sum_locked from " . sfConfig::get('app_default_schema') . ".rincian r," . sfConfig::get('app_default_schema') . ".master_kegiatan k," . sfConfig::get('app_default_schema') . ".rincian_detail d where k.kode_kegiatan=r.kegiatan_code and k.unit_id=r.unit_id and d.kegiatan_code=r.kegiatan_code and d.unit_id=r.unit_id and r.unit_id<>'9999' and r.rincian_level=3";
        //$result2 = @pg_query($conn_id,$query2);
        $stmt = $con->prepareStatement($query2);
        $rs2 = $stmt->executeQuery();
        //while($rows2 = @pg_fetch_assoc($result2))	extract($rows2,EXTR_OVERWRITE);
        while ($rs2->next())
            $sum_locked = $rs2->getString('sum_locked');

        //$result = @pg_query($conn_id,$query);
        $stmt = $con->prepareStatement($query);
        $rs3 = $stmt->executeQuery();
        $this->rs = $rs3;

        //if(!$result) echo @pg_last_error($conn_id);
        //$rs=pg_fetch_all($result);

        if ($sum_locked == 0 or !$sum_locked)
            $sum_locked = 99999999999;
        if ($sum_draft == 0 or !$sum_draft)
            $sum_draft = 99999999999;
        $i = 0;
        $j = 0;
        //foreach($rs as $s)
        $belanja_id = '';
        $nilai_draft = 0;
        $nilai_locked = 0;
        $belanja_name = '';
        //$this->nilai_draft2=0;
        //$this->nilai_locked2=0;
        $unit_id = '';
        //$this->dinas_ok='';

        $persen_draft_arr = array();
        $this->persen_draft = array();

        //$persen_locked=0;
        $persen_locked_arr = array();
        $this->persen_locked = array();

        $nilai_locked_arr = array();
        $this->nilai_locked = array();

        $dinas_ok_arr = array();
        $this->dinas_ok = array();

        $belanja_id_arr = array();
        $this->belanja_id = array();

        $belanja_name_arr = array();
        $this->belanja_name = array();

        $nilai_draft3_arr = array();
        $this->nilai_draft3 = array();

        $nilai_locked3_arr = array();
        $this->nilai_locked3 = array();

        $persen_draft3_arr = array();
        $this->persen_draft3 = array();

        $persen_locked3_arr = array();
        $this->persen_locked3 = array();

        $nilai_draft_arr = array();
        $this->nilai_draft = array();

        $rekening_code_arr = array();
        $this->rekening_code = array();

        $rekening_name_arr = array();
        $this->rekening_name = array();

        //print_r($query);exit;
        while ($rs3->next()) {

            $rekening_code_arr[$i] = $rs3->getString('rekening_code');
            $rekening_name_arr[$i] = $rs3->getString('rekening_name');

            if ($belanja_id != $rs3->getString('old_belanja_id')) {
                $this->nilai_draft2+=$nilai_draft;
                $this->nilai_locked2+=$nilai_locked;
                $persen_draft = $nilai_draft * 100 / $sum_draft;
                //$rs[$i-$j]['persen_draft']=number_format($persen_draft,2,",",".");
                $persen_draft_arr[$i - $j] = number_format($persen_draft, 2, ",", ".");
                $persen_locked = $nilai_locked * 100 / $sum_locked;
                //$rs[$i-$j]['persen_locked']=number_format($persen_locked,2,",",".");
                $persen_locked_arr[$i - $j] = number_format($persen_locked, 2, ",", ".");

                //$rs[$i-$j]['nilai_draft']=number_format($nilai_draft,0,",",".");
                $nilai_draft_arr[$i - $j] = number_format($nilai_draft, 0, ",", ".");
                //$rs[$i-$j]['nilai_locked']=number_format($nilai_locked,0,",",".");
                $nilai_locked_arr[$i - $j] = number_format($nilai_locked, 0, ",", ".");
                //$rs[$i-$j]['dinas_ok']='t';
                $dinas_ok_arr[$i - $j] = 't';
                //$rs[$i-$j]['belanja_id']=$belanja_id;
                $belanja_id_arr[$i - $j] = $belanja_id;
                //$rs[$i-$j]['belanja_name']=$belanja_name;
                $belanja_name_arr[$i - $j] = $belanja_name;
                $j = 0;
                $nilai_draft = 0;
                $nilai_locked = 0;
            }
            //$belanja_id=$rs[$i]['old_belanja_id'];
            $belanja_id = $rs3->getString('old_belanja_id');

            //$belanja_name=$rs[$i]['old_belanja_name'];
            $belanja_name = $rs3->getString('old_belanja_name');
            //$rs[$i]['belanja_id']=$unit_id;
            $belanja_id_arr[$i] = $unit_id;
            //$nilai_draft+=$rs[$i]['nilai_draft3'];
            $nilai_draft = $nilai_draft + $rs3->getString('nilai_draft3');
            //$nilai_locked+=$rs[$i]['nilai_locked3'];
            $nilai_locked+=$rs3->getString('nilai_locked3');
            //$persen_draft3=$rs[$i]['nilai_draft3'] * 100 / $sum_draft; 
            $persen_draft3 = $rs3->getString('nilai_draft3') * 100 / $sum_draft;
            //$rs[$i]['persen_draft3']=number_format($persen_draft3,2,",",".");
            $persen_draft3_arr[$i] = number_format($persen_draft3, 2, ",", ".");
            //$persen_locked3=$rs[$i]['nilai_locked3'] * 100 / $sum_locked; 
            $persen_locked3 = $rs3->getString('nilai_locked3') * 100 / $sum_locked;
            //$rs[$i]['persen_locked3']=number_format($persen_locked3,2,",",".");
            $persen_locked3_arr[$i] = number_format($persen_locked3, 2, ",", ".");

            //$rs[$i]['nilai_draft3']=number_format($rs[$i]['nilai_draft3'],0,",",".");
            $nilai_draft3_arr[$i] = number_format($rs3->getString('nilai_draft3'), 0, ",", ".");
            //$rs[$i]['nilai_locked3']=number_format($rs[$i]['nilai_locked3'],0,",",".");
            $nilai_locked3_arr[$i] = number_format($rs3->getString('nilai_locked3'), 0, ",", ".");
            //print_r($belanja_id);exit;
            $i+=1;
            $j+=1;
        }

        $this->nilai_draft2+=$nilai_draft;
        $this->nilai_locked2+=$nilai_locked;
        $persen_draft = $nilai_draft * 100 / $sum_draft;
        //$rs[$i-$j]['persen_draft']=number_format($persen_draft,2,",",".");
        $persen_draft_arr[$i - $j] = number_format($persen_draft, 2, ",", ".");
        $persen_locked = $nilai_locked * 100 / $sum_locked;
        //$rs[$i-$j]['persen_locked']=number_format($persen_locked,2,",",".");
        $persen_locked_arr[$i - $j] = number_format($persen_locked, 2, ",", ".");

        //$rs[$i-$j]['nilai_draft']=number_format($nilai_draft,0,",",".");
        $nilai_draft_arr[$i - $j] = number_format($nilai_draft, 0, ",", ".");
        //$rs[$i-$j]['nilai_locked']=number_format($nilai_locked,0,",",".");
        $nilai_locked_arr[$i - $j] = number_format($nilai_locked, 0, ",", ".");
        //$rs[$i-$j]['dinas_ok']='t';
        $dinas_ok_arr[$i - $j] = 't';
        //$rs[$i-$j]['belaja_id']=$belanja_id;
        $belanja_id_arr[$i - $j] = $belanja_id;
        //$rs[$i-$j]['belanja_name']=$belanja_name;
        $belanja_name_arr[$i - $j] = $belanja_name;

        //$nilai_draft2=number_format($nilai_draft2,0,",",".");
        $this->nilai_draft2 = number_format($this->nilai_draft2, 0, ",", ".");
        //$nilai_locked2=number_format($nilai_locked2,0,",",".");
        $this->nilai_locked2 = number_format($this->nilai_locked2, 0, ",", ".");

        $this->persen_draft = $persen_draft_arr;

        $this->persen_locked = $persen_locked_arr;

        $this->nilai_locked = $nilai_locked_arr;

        $this->dinas_ok = $dinas_ok_arr;

        $this->belanja_id = $belanja_id_arr;

        $this->belanja_name = $belanja_name_arr;

        $this->nilai_draft3 = $nilai_draft3_arr;

        $this->nilai_locked3 = $nilai_locked3_arr;

        $this->persen_draft3 = $persen_draft3_arr;

        $this->persen_locked3 = $persen_locked3_arr;

        $this->nilai_draft = $nilai_draft_arr;

        $this->rekening_code = $rekening_code_arr;

        $this->rekening_name = $rekening_name_arr;
    }

    public function executeReportBelanja() {
        $this->setFlash('RB', 'selected');
        $query = "select  belanja_id, belanja_name,sum((jumlah_draft)) as total_draft,sum((jumlah_setuju)) as total_locked,
 					sum((nilai_draft)) as nilai_draft, sum((nilai_draft*jumlah_setuju)) as nilai_locked
			FROM " . sfConfig::get('app_default_schema') . ".v_kegiatan_rekening
			WHERE unit_id<>'9999'
			GROUP BY belanja_id, belanja_name
			ORDER BY belanja_id, belanja_name";

        $query2 = "select sum(komponen_harga_awal*volume*(100+pajak)/100) as sum_draft
		from " . sfConfig::get('app_default_schema') . ".rincian r," . sfConfig::get('app_default_schema') . ".master_kegiatan k," . sfConfig::get('app_default_schema') . ".rincian_detail d where (k.kode_kegiatan=r.kegiatan_code) and (k.unit_id=r.unit_id) and (d.kegiatan_code=r.kegiatan_code) and (d.unit_id=r.unit_id) and (r.unit_id<>'9999') ";
        $con = Propel::getConnection();
        //$result2 = @pg_query($conn_id,$query2);
        $stmt = $con->prepareStatement($query2);
        //while($rows2 = @pg_fetch_assoc($result2))	extract($rows2,EXTR_OVERWRITE);
        $rs = $stmt->executeQuery();
        while ($rs->next())
            $sum_draft = $rs->getString('sum_draft');

        $query2 = "select sum(komponen_harga_awal*volume*(100+pajak)/100) as sum_locked
	 	from " . sfConfig::get('app_default_schema') . ".rincian r," . sfConfig::get('app_default_schema') . ".master_kegiatan k," . sfConfig::get('app_default_schema') . ".rincian_detail d 
		where (k.kode_kegiatan=r.kegiatan_code) and (k.unit_id=r.unit_id) and 
		(d.kegiatan_code=r.kegiatan_code) and (d.unit_id=r.unit_id) and (r.unit_id<>'9999') and (r.rincian_level=3) ";
        //$result2 = @pg_query($conn_id,$query2);
        $stmt = $con->prepareStatement($query2);
        //while($rows2 = @pg_fetch_assoc($result2))	extract($rows2,EXTR_OVERWRITE);
        $rs2 = $stmt->executeQuery();
        while ($rs2->next())
            $sum_locked = $rs2->getString('sum_locked');


        //$result = @pg_query($conn_id,$query);
        $stmt = $con->prepareStatement($query);
        //if(!$result) echo @pg_last_error($conn_id);
        //$rs=pg_fetch_all($result);
        $rs3 = $stmt->executeQuery();
        $this->rs = $rs3;

        if ($sum_locked == 0 or !$sum_locked)
            $sum_locked = 99999999999;
        if ($sum_draft == 0 or !$sum_draft)
            $sum_draft = 99999999999;

        $i = 0;

        //foreach($rs as $s)

        $persen_draft_arr = array();
        $this->persen_draft = array();

        $persen_locked_arr = array();
        $this->persen_locked = array();

        $nilai_draft_arr = array();
        $this->nilai_draft = array();

        $nilai_locked_arr = array();
        $this->nilai_locked = array();

        $belanja_id_arr = array();
        $this->belanja_id = array();

        $belanja_name_arr = array();
        $this->belanja_name = array();

        while ($rs3->next()) {
            $belanja_name_arr[$i] = $rs3->getString('belanja_name');
            $belanja_id_arr[$i] = $rs3->getString('belanja_id');
            //$rs[$i]['persen_draft']=$rs[$i]['nilai_draft'] * 100 / $sum_draft; 
            $persen_draft_arr[$i] = $rs3->getString('nilai_draft') * 100 / $sum_draft;
            //$rs[$i]['persen_draft']=number_format($rs[$i]['persen_draft'],2,",",".");
            $persen_draft_arr[$i] = number_format($persen_draft_arr[$i], 2, ",", ".");
            //$rs[$i]['persen_locked']=$rs[$i]['nilai_locked'] * 100 / $sum_locked; 
            $persen_locked_arr[$i] = $rs3->getString('nilai_locked') * 100 / $sum_locked;
            //$rs[$i]['persen_locked']=number_format($rs[$i]['persen_locked'],2,",",".");
            $persen_locked_arr[$i] = number_format($persen_locked_arr[$i], 2, ",", ".");

            //$nilai_draft2+=$rs[$i]['nilai_draft'];
            $this->nilai_draft2+=$rs3->getString('nilai_draft');
            //$total_draft2+=$rs[$i]['total_draft'];
            $this->total_draft2+=$rs3->getString('total_draft');
            //$nilai_locked2+=$rs[$i]['nilai_locked'];
            $this->nilai_locked2+=$rs3->getString('nilai_locked');
            //$total_locked2+=$rs[$i]['total_locked'];
            $this->total_locked2+=$rs3->getString('total_locked');
            //$rs[$i]['nilai_draft']=number_format($rs[$i]['nilai_draft'],0,",",".");
            $nilai_draft_arr[$i] = number_format($rs3->getString('nilai_draft'), 0, ",", ".");
            //$rs[$i]['nilai_locked']=number_format($rs[$i]['nilai_locked'],0,",",".");
            $nilai_locked_arr[$i] = number_format($rs3->getString('nilai_locked'), 0, ",", ".");
            $i+=1;
        }

        $this->nilai_draft2 = number_format($this->nilai_draft2, 0, ",", ".");
        $this->nilai_locked2 = number_format($this->nilai_locked2, 0, ",", ".");

        $this->persen_draft = $persen_draft_arr;

        $this->persen_locked = $persen_locked_arr;

        $this->nilai_draft = $nilai_draft_arr;

        $this->nilai_locked = $nilai_locked_arr;
        $this->belanja_id = $belanja_id_arr;
        $this->belanja_name = $belanja_name_arr;
    }

    public function executeReportDinas() {
        $this->setFlash('RD', 'selected');
        /**
         * if (($this->getRequestParameter('tipe_anggaran')==2) or (!$this->getRequestParameter('tipe_anggaran'))) 
         * 		 {
         * 			 $query="select unit_id, unit_name ,sum((jumlah_draft*jumlah_non_lanjutan)) as total_draft,sum((jumlah_setuju*jumlah_non_lanjutan)) as total_locked,sum((nilai_draft*jumlah_non_lanjutan)) as nilai_draft, sum((nilai_draft*jumlah_setuju*jumlah_non_lanjutan)) as nilai_locked FROM ". sfConfig::get('app_default_schema') .".v_dinas_kegiatan
         * 			WHERE unit_id<>'9999'
         * 			GROUP BY unit_id, unit_name
         * 			ORDER BY unit_id,unit_name";
         * 			  $this->a2="SELECTED";
         * 		 }
         * 		elseif ($tipe_anggaran==3)
         * 		{
         * 			$query="select unit_id, unit_name ,sum((jumlah_draft*jumlah_lanjutan)) as total_draft,sum((jumlah_setuju*jumlah_lanjutan)) as total_locked,sum((nilai_draft*jumlah_lanjutan)) as nilai_draft, sum((nilai_draft*jumlah_setuju*jumlah_lanjutan)) as nilai_locked 
         * 						FROM ". sfConfig::get('app_default_schema') .".v_dinas_kegiatan
         * 						WHERE unit_id<>'9999'
         * 						GROUP BY unit_id, unit_name
         * 						ORDER BY unit_id,unit_name";
         * 			 $this->a3="SELECTED";
         * 		}
         * 		else
         * 		{
         */
        $query = "select unit_id, unit_name ,sum((jumlah_draft)) as total_draft,sum((jumlah_setuju)) as total_locked,
			 					sum((nilai_setuju)) as nilai_draft, sum((nilai_setuju*jumlah_setuju)) as nilai_locked
						FROM " . sfConfig::get('app_default_schema') . ".v_dinas_kegiatan_rincian3
						WHERE unit_id<>'9999'
						GROUP BY unit_id, unit_name
						ORDER BY unit_id,unit_name";

        //$this->a1="SELECTED";
        //}

        $query2 = "select sum(komponen_harga_awal*volume*(100+pajak)/100) as sum_draft
			 from " . sfConfig::get('app_default_schema') . ".rincian r," . sfConfig::get('app_default_schema') . ".master_kegiatan k," . sfConfig::get('app_default_schema') . ".rincian_detail d 
			where k.kode_kegiatan=r.kegiatan_code and k.unit_id=r.unit_id  and 
			d.kegiatan_code=r.kegiatan_code and d.unit_id=r.unit_id and r.unit_id<>'9999'";

        $con = Propel::getConnection();
        $stmt2 = $con->prepareStatement($query2);
        $rs2 = $stmt2->executeQuery();
        while ($rs2->next()) {
            $sum_draft = $rs2->getString('sum_draft');
        }
        //$result2 = @pg_query($conn_id,$query2);
        //while($rows2 = @pg_fetch_assoc($result2))	extract($rows2,EXTR_OVERWRITE);

        $query2 = "select sum(komponen_harga_awal*volume*(100+pajak)/100) as sum_locked
			 from " . sfConfig::get('app_default_schema') . ".rincian r, " . sfConfig::get('app_default_schema') . ".master_kegiatan k, " . sfConfig::get('app_default_schema') . ".rincian_detail d 
			where k.kode_kegiatan=r.kegiatan_code and k.unit_id=r.unit_id and 
			d.kegiatan_code=r.kegiatan_code and d.unit_id=r.unit_id and r.unit_id<>'9999' and r.rincian_level=3";

        $stmt3 = $con->prepareStatement($query2);
        $rs3 = $stmt3->executeQuery();
        while ($rs3->next()) {
            $sum_locked = $rs3->getString('sum_locked');
        }

        //$result2 = @pg_query($conn_id,$query2);
        //while($rows2 = @pg_fetch_assoc($result2))	extract($rows2,EXTR_OVERWRITE);

        $stmt = $con->prepareStatement($query);
        $rs = $stmt->executeQuery();
        $this->rs = $rs;
        //$result = @pg_query($conn_id,$query);
        //$rs=pg_fetch_all($result);

        if (!$sum_draft)
            $sum_draft = 9999999999;
        if (!$sum_locked)
            $sum_locked = 9999999999;

        $i = 0;
        $this->total_pagu = 0;
        $this->total_selisih = 0;

        //foreach($rs as $s)
        $persen_draft_arr = array();
        $this->persen_draft = array();

        $persen_locked_arr = array();
        $this->persen_locked = array();

        $pagu_arr = array();
        $this->pagu = array();

        $nilai_draft_arr = array();
        $this->nilai_draft = array();

        $nilai_locked_arr = array();
        $this->nilai_locked = array();

        $selisih_arr = array();
        $this->selisih = array();

        $nilai_usulan_arr = array();
        $this->nilai_usulan = array();

        while ($rs->next()) {
            //$rs[$i]['persen_draft']=$rs[$i]['nilai_draft'] * 100 / $sum_draft; 
            $persen_draft_arr[$i] = $rs->getString('nilai_draft') * 100 / $sum_draft;
            //$rs[$i]['persen_draft']=number_format($rs[$i]['persen_draft'],2,",",".");
            $persen_draft_arr[$i] = number_format($persen_draft_arr[$i], 2, ",", ".");
            //$rs[$i]['persen_locked']=$rs[$i]['nilai_locked'] * 100 / $sum_locked; 
            $persen_locked_arr[$i] = $rs->getString('nilai_locked') * 100 / $sum_locked;
            //$rs[$i]['persen_locked']=number_format($rs[$i]['persen_locked'],2,",",".");
            $persen_locked_arr[$i] = number_format($persen_locked_arr[$i], 2, ",", ".");

            //$query2="select pagu from unit_kerja where unit_id='".$rs->getString('unit_id')."'";
            $query2 = "select sum(alokasi_dana) as pagu  from " . sfConfig::get('app_default_schema') . ".master_kegiatan where unit_id='" . $rs->getString('unit_id') . "'";
            //$result2 = @pg_query($conn_id,$query2);
            $stmt4 = $con->prepareStatement($query2);
            $rs4 = $stmt4->executeQuery();

            //while($rows2 = $rs4->next())	extract($rows2,EXTR_OVERWRITE);
            while ($rs4->next()) {
                $pagu_arr[$i] = $rs4->getString('pagu');
            }

            $query3 = "select sum(rd.volume*rd.komponen_harga_awal*(100+rd.pajak)/100) as nilai_usulan from " . sfConfig::get('app_default_schema') . ".rincian_detail rd where rd.unit_id='" . $rs->getString('unit_id') . "'";
            $stmt5 = $con->prepareStatement($query3);
            $rs5 = $stmt5->executeQuery();

            //while($rows2 = $rs4->next())	extract($rows2,EXTR_OVERWRITE);
            while ($rs5->next()) {
                $nilai_usulan_arr[$i] = number_format($rs5->getString('nilai_usulan'), 0, ",", ".");
                $this->nilai_draft2+=$rs5->getString('nilai_usulan');
            }
            //$nilai_draft2+=$rs[$i]['nilai_draft'];
            //$this->nilai_draft2+=$rs->getString('nilai_draft');
            $this->total_pagu+=$pagu_arr[$i];
            $selisih_arr[$i] = $pagu_arr[$i] - $rs->getString('nilai_draft');
            $this->total_selisih+=$selisih_arr[$i];
            $this->total_draft2+=$rs->getString('total_draft');
            $this->nilai_locked2+=$rs->getString('nilai_locked');
            $this->total_locked2+=$rs->getString('total_locked');
            $nilai_draft_arr[$i] = number_format($rs->getString('nilai_draft'), 0, ",", ".");
            $nilai_locked_arr[$i] = number_format($rs->getString('nilai_locked'), 0, ",", ".");
            $pagu_arr[$i] = number_format($pagu_arr[$i], 0, ",", ".");
            $selisih_arr[$i] = number_format($selisih_arr[$i], 0, ",", ".");
            $i+=1;
        }

        $this->nilai_draft2 = number_format($this->nilai_draft2, 0, ",", ".");
        $this->nilai_locked2 = number_format($this->nilai_locked2, 0, ",", ".");

        $this->total_pagu = number_format($this->total_pagu, 0, ",", ".");
        $this->total_selisih = number_format($this->total_selisih, 0, ",", ".");

        $this->persen_draft = $persen_draft_arr;

        $this->persen_locked = $persen_locked_arr;

        $this->pagu = $pagu_arr;

        $this->nilai_draft = $nilai_draft_arr;

        $this->nilai_locked = $nilai_locked_arr;

        $this->selisih = $selisih_arr;

        $this->nilai_usulan = $nilai_usulan_arr;
    }

    public function executeReportDinasKegiatan() {
        $this->setFlash('RDK', 'selected');
        $query = "select unit_id as old_unit_id, unit_name as old_unit_name,kode_kegiatan,nama_kegiatan ,kode_program,kode_bidang,kode_sasaran,((jumlah_draft*jumlah_non_lanjutan)) as total_draft3,((jumlah_setuju*jumlah_non_lanjutan)) as total_locked3,((nilai_draft*jumlah_non_lanjutan)) as nilai_draft3, ((nilai_setuju*jumlah_setuju*jumlah_non_lanjutan)) as nilai_locked3 FROM " . sfConfig::get('app_default_schema') . ".v_dinas_kegiatan_rincian3 WHERE unit_id<>'9999'	ORDER BY unit_id,unit_name,kode_kegiatan,nama_kegiatan,kode_program,kode_bidang,kode_sasaran ";

        //parse_str(decode($coded));
        //if($error_message) $onload = "alert('$error_message');";

        $query2 = "Select unit_id,unit_name from unit_kerja order by unit_id limit 1";
        //$result = @pg_query($conn_id,$query2);
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query2);
        $rs = $stmt->executeQuery();
        //while($rs->next()) extract($rs,EXTR_OVERWRITE);

        while ($rs->next()) {
            $unit_id = $rs->getString('unit_id');
            $unit_name = $rs->getString('unit_name');
        }

        //$result = @pg_query($conn_id,$query);
        $stmt = $con->prepareStatement($query);
        $rs2 = $stmt->executeQuery();
        $this->rs = $rs2;
        //if(!$rs) echo @pg_last_error($conn_id);
        //$rs=pg_fetch_all($result);

        $i = 0;
        $j = 0;
        //foreach($rs as $s)
        $nilai_draft_arr = array();
        $this->nilai_draft = array();

        $nilai_locked_arr = array();
        $this->nilai_locked = array();

        $dinas_ok_arr = array();
        $this->dinas_ok = array();

        $unit_id_arr = array();
        $this->unit_id = array();

        $unit_name_arr = array();
        $this->unit_name = array();

        $nilai_draft3_arr = array();
        $this->nilai_draft3 = array();



        $nilai_locked3_arr = array();
        $this->nilai_locked3 = array();

        $nilai_usulan_unit_arr = array();
        $this->nilai_usulan_unit = array();

        $nilai_usulan_kegiatan_arr = array();
        $this->nilai_usulan_kegiatan = array();
        $this->nilai_draft2 = 0;
        $this->nilai_draft4 = 0;
        $this->nilai_locked2 = 0;
        $nilai_draft = 0;
        $nilai_locked = 0;
        //$this->nilai_draft2=$this->nilai_locked2=0;
        while ($rs2->next()) {
            if ($unit_id != $rs2->getString('old_unit_id')) {

                $this->nilai_draft2+=$nilai_draft;
                $this->nilai_locked2+=$nilai_locked;
                $query3 = "select sum(rd.volume*rd.komponen_harga_awal*(100+rd.pajak)/100) as nilai_usulan from " . sfConfig::get('app_default_schema') . ".rincian_detail rd where rd.unit_id='" . $unit_id . "'";
                $stmt3 = $con->prepareStatement($query3);
                $rs3 = $stmt3->executeQuery();
                while ($rs3->next()) {
                    $nilai_usulan_unit_arr[$i] = number_format($rs3->getString('nilai_usulan'), 0, ",", ".");
                }
                $nilai_draft_arr[$i - $j] = number_format($nilai_draft, 0, ",", ".");
                $nilai_locked_arr[$i - $j] = number_format($nilai_locked, 0, ",", ".");
                $dinas_ok_arr[$i - $j] = 't';
                $unit_id_arr[$i - $j] = $unit_id;
                $unit_name_arr[$i - $j] = $unit_name;
                $j = 0;
                $nilai_draft = 0;
                $nilai_locked = 0;
            }
            $unit_id = $rs2->getString('old_unit_id');
            $unit_name = $rs2->getString('old_unit_name');
            $unit_id_arr[$i] = $unit_id;
            $nilai_draft+=$rs2->getString('nilai_draft3');
            $nilai_locked+=$rs2->getString('nilai_locked3');

            $query4 = "select sum(rd.volume*rd.komponen_harga_awal*(100+rd.pajak)/100) as nilai_usulan_kegiatan from " . sfConfig::get('app_default_schema') . ".rincian_detail rd where rd.unit_id='" . $unit_id . "' and kegiatan_code='" . $rs2->getString('kode_kegiatan') . "'";
            $stmt4 = $con->prepareStatement($query4);
            $rs4 = $stmt4->executeQuery();
            while ($rs4->next()) {
                $nilai_usulan_kegiatan_arr[$i] = number_format($rs4->getString('nilai_usulan_kegiatan'), 0, ",", ".");
                $this->nilai_draft4+=$rs4->getString('nilai_usulan_kegiatan');
            }

            $nilai_draft3_arr[$i] = number_format($rs2->getString('nilai_draft3'), 0, ",", ".");
            $nilai_locked3_arr[$i] = number_format($rs2->getString('nilai_locked3'), 0, ",", ".");

            $i+=1;
            $j+=1;
        }
        $this->nilai_draft2+=$nilai_draft;
        $this->nilai_locked2+=$nilai_locked;

        //$rs[$i-$j]['nilai_draft']=number_format($nilai_draft,0,",",".");
        $nilai_draft_arr[$i - $j] = number_format($nilai_draft, 0, ",", ".");
        //$rs[$i-$j]['nilai_locked']=number_format($nilai_locked,0,",",".");
        $nilai_locked_arr[$i - $j] = number_format($nilai_locked, 0, ",", ".");
        //$rs[$i-$j]['dinas_ok']='t';
        $dinas_ok_arr[$i - $j] = 't';
        //$rs[$i-$j]['unit_id']=$unit_id;
        $unit_id_arr[$i - $j] = $unit_id;
        //$rs[$i-$j]['unit_name']=$unit_name;
        $unit_name_arr[$i - $j] = $unit_name;

        $this->nilai_draft2 = number_format($this->nilai_draft2, 0, ",", ".");
        $this->nilai_locked2 = number_format($this->nilai_locked2, 0, ",", ".");
        $this->nilai_draft4 = number_format($this->nilai_draft4, 0, ",", ".");

        $this->nilai_draft = $nilai_draft_arr;

        $this->nilai_locked = $nilai_locked_arr;

        $this->dinas_ok = $dinas_ok_arr;

        $this->unit_id = $unit_id_arr;

        $this->unit_name = $unit_name_arr;

        $this->nilai_draft3 = $nilai_draft3_arr;

        $this->nilai_locked3 = $nilai_locked3_arr;

        $this->nilai_usulan_unit = $nilai_usulan_unit_arr;

        $this->nilai_usulan_kegiatan = $nilai_usulan_kegiatan_arr;
    }

    public function executeUbahPass() {
        if ($this->getRequest()->getMethod() == sfRequest::POST) {
            $member = MasterUserV2Peer::retrieveByPK($this->getUser()->getNamaUser());

            $member->setUserPassword(md5($this->getRequestParameter('pass_baru')));
            budgetLogger::log('Merubah password');
            $member->save();
            $this->habis_ganti = true;
        }
    }

    public function handleErrorUbahPass() {
        return sfView::SUCCESS;
    }

    public function executeSetting() {
        $dinas = $this->getRequest()->getCookie('nama');
        $c = new Criteria();
        $c->add(UnitKerjaPeer::UNIT_ID, $dinas);
        $x = UnitKerjaPeer::doSelectOne($c);
        if ($x) {
            $this->kepala_nama = $x->getKepalaNama();
            $this->kepala_pangkat = $x->getKepalaPangkat();
            $this->kepala_nip = $x->getKepalaNip();
        }
        $this->set = 3;
        return sfView::SUCCESS;
    }

    public function executeSimpanset() {
        $dinas = $this->getRequest()->getCookie('nama');
        $nama_kepala = $this->getRequestParameter('kepala_nama');
        $kepala_pangkat = $this->getRequestParameter('kepala_pangkat');
        $kepala_nip = $this->getRequestParameter('kepala_nip');

        $query = "update unit_kerja set kepala_nama='" . $nama_kepala . "', kepala_pangkat='" . $kepala_pangkat . "', kepala_nip='" . $kepala_nip . "' where unit_id='" . $dinas . "'";
        //echo $query;
        //exit;
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query);

        if (($nama_kepala == '') || ($kepala_pangkat == '') || ($kepala_nip == '')) {
            return $this->redirect('peneliti/setting?salah=1');
        } else {
            budgetLogger::log('Merubah setting dari dinas, unit_id' . $dinas);
            $stmt->executeQuery();
        }
        return $this->redirect('peneliti/setting');
    }

    public function executeSabomedit() {
        $coded = $this->getRequestParameter('coded', '');
        return sfView::SUCCESS;
    }

    public function executeSabomlist() {

        $this->processSortsabom();

        $this->processFilterssabom();

        $this->filters = $this->getUser()->getAttributeHolder()->getAll('sf_admin/komponen/filters');

        // pager
        $this->pager = new sfPropelPager('Komponen', 20);
        $c = new Criteria();
        $c->add(KomponenPeer::KOMPONEN_TIPE, 'OM');

        $this->addSortCriteriasabom($c);
        $this->addFiltersCriteriasabom($c);
        $c->addAscendingOrderByColumn(KomponenPeer::KOMPONEN_ID);
        $this->pager->setCriteria($c);
        $this->pager->setPage($this->getRequestParameter('page', 1));
        $this->pager->init();
    }

    protected function processFilterssabom() {
        if ($this->getRequest()->hasParameter('filter')) {
            $filters = $this->getRequestParameter('filters');

            $this->getUser()->getAttributeHolder()->removeNamespace('sf_admin/komponen/filters');
            $this->getUser()->getAttributeHolder()->add($filters, 'sf_admin/komponen/filters');
        }
    }

    protected function processSortsabom() {
        if ($this->getRequestParameter('sort')) {
            $this->getUser()->setAttribute('sort', $this->getRequestParameter('sort'), 'sf_admin/komponen/sort');
            $this->getUser()->setAttribute('type', $this->getRequestParameter('type', 'asc'), 'sf_admin/komponen/sort');
        }

        if (!$this->getUser()->getAttribute('sort', null, 'sf_admin/komponen/sort')) {
            
        }
    }

    protected function addFiltersCriteriasabom($c) {
        $cek = $this->getRequestParameter('search_option');
        if ($cek == 'shsd_id') {
            if (isset($this->filters['komponen_id_is_empty'])) {
                $criterion = $c->getNewCriterion(KomponenPeer::KOMPONEN_ID, '');
                $criterion->addOr($c->getNewCriterion(KomponenPeer::KOMPONEN_ID, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['komponen_id']) && $this->filters['komponen_id'] !== '') {
                $kata = '%' . $this->filters['komponen_id'] . '%';
                $c->add(KomponenPeer::KOMPONEN_ID, strtr($kata, '*', '%'), Criteria::ILIKE);
            }
        } else {
            if (isset($this->filters['komponen_id_is_empty'])) {
                $criterion = $c->getNewCriterion(KomponenPeer::KOMPONEN_NAME, '');
                $criterion->addOr($c->getNewCriterion(KomponenPeer::KOMPONEN_NAME, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['komponen_id']) && $this->filters['komponen_id'] !== '') {
                $kata = '%' . $this->filters['komponen_id'] . '%';
                $c->add(KomponenPeer::KOMPONEN_NAME, strtr($kata, '*', '%'), Criteria::ILIKE);
            }
        }
    }

    protected function addSortCriteriasabom($c) {
        if ($sort_column = $this->getUser()->getAttribute('sort', null, 'sf_admin/komponen/sort')) {
            $sort_column = KomponenPeer::translateFieldName($sort_column, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_COLNAME);
            if ($this->getUser()->getAttribute('type', null, 'sf_admin/komponen/sort') == 'asc') {
                $c->addAscendingOrderByColumn($sort_column);
            } else {
                $c->addDescendingOrderByColumn($sort_column);
            }
        }
    }

    public function executeDetailsublist() {
        $coded = $this->getRequestParameter('coded', '');
        if ($coded != '') {
            $this->setLayout('kosong');
        }
        $sub_id = $this->getRequestParameter('id');
        $input = array();
        $c = new Criteria();
        $c->add(SubKegiatanPeer::SUB_KEGIATAN_ID, $sub_id);
        //$c -> add (SubKegiatanPeer::STATUS, 'Close') ;
        $cs = SubKegiatanPeer::doSelectOne($c);
        if ($cs) {
            $this->sub_kegiatan_id = $cs->getSubKegiatanId();
            $this->sub_kegiatan_name = $cs->getSubKegiatanName();
            $this->param = $cs->getParam();
            $this->satuan = $cs->getSatuan();
        }

        $d = new Criteria();
        $d->add(KomponenMemberPeer::KOMPONEN_ID, $sub_id);
        $d->addAscendingOrderByColumn(KomponenMemberPeer::SUBTITLE);
        $d->addAscendingOrderByColumn(KomponenMemberPeer::TIPE);
        $ds = KomponenMemberPeer::doSelect($d);
        $this->komponen_penyusun = $ds;
        $total = 0;
        foreach ($ds as $x) {
            $total = $total + $x->getMemberTotal();
        }
        $this->total_penyusun = $total;
        $sql = "select 
			sum(m.volume*m.komponen_harga_awal*(100+m.pajak)/100) as nilai_satuan
		from 
			" . sfConfig::get('app_default_schema') . ".sub_kegiatan_member m
		where 
			m.sub_kegiatan_id = '" . $sub_id . "'";
        //print_r($sql);exit;
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($sql);
        $rs = $stmt->executeQuery();
        while ($rs->next()) {
            $nilai_satuan = $rs->getString('nilai_satuan');
        }
        $this->nilai_satuan = $nilai_satuan;

        $query = "
	SELECT 
		rekening.rekening_code,
		detail.detail_name as detail_name,
		detail.komponen_name,
		detail.komponen_name || ' ' || detail.detail_name as detail_name2,
		detail.komponen_harga_awal as detail_harga,
		detail.pajak,
		detail.komponen_id,
	 detail.subtitle ,
	 detail_no,koefisien,param,
		detail.satuan as detail_satuan,
		replace(detail.keterangan_koefisien,'<*>','X') as keterangan_koefisien,
		detail.volume * detail.komponen_harga_awal as hasil,
		(detail.volume * detail.komponen_harga_awal  * (100+detail.pajak)/100) as hasil_kali,
		

		(SELECT '2' FROM " . sfConfig::get('app_default_schema') . ".komponen where komponen.komponen_id=detail.komponen_id AND komponen.komponen_tipe='EST')
		 as x,
		(SELECT 'bahaya' FROM " . sfConfig::get('app_default_schema') . ".komponen where komponen.komponen_id=detail.komponen_id AND komponen_show=FALSE)
		 as bahaya,
		 substring(kb.belanja_name,8) as belanja_name
		 
	FROM 
		" . sfConfig::get('app_default_schema') . ".rekening rekening ,
		 " . sfConfig::get('app_default_schema') . ".sub_kegiatan_member detail ,
		 " . sfConfig::get('app_default_schema') . ".kelompok_belanja kb
		
	WHERE 
	rekening.rekening_code = detail.rekening_code and
		detail.sub_kegiatan_id = '" . $sub_id . "' and 
		 kb.belanja_id=rekening.belanja_id 
		
	ORDER BY 
		
		belanja_urutan,
		komponen_name
	
	";
        //echo $query;
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query);
        $rs = $stmt->executeQuery();
        $this->r = $rs;
        //$this->input = Array();
        $keterangan_koefisien = array();
        $detail_harga = array();
        $hasil_kali = array();
        $hasil = array();
        $detail_name = array();
        $detail_satuan = array();
        $pajak = array();
        $belanja_name = array();
        $this->keterangan_koefisien = Array();
        $this->detail_harga = Array();
        $this->hasil_kali = Array();
        $this->hasil = Array();
        $this->detail_name = array();
        $this->detail_satuan = array();
        $this->pajak = array();
        $this->belanja_name = array();
        $i = 0;
        while ($rs->next()) {
            $pars = explode("|", $rs->getString('param'));

            for ($j = 0; $j < count($pars); $j++) {
                if ($j == 0)
                    $inputs = $pars[$j];
                else
                    $inputs = $inputs . ',<BR>' . $pars[$j];
            }
            //echo $r['param'];
            $input[$i] = $inputs;
            //print_r($inputs);exit;

            $detail_name[$i] = $rs->getString('detail_name2');
            $detail_satuan[$i] = $rs->getString('detail_satuan');
            $pajak[$i] = $rs->getString('pajak');
            $belanja_name[$i] = $rs->getString('belanja_name');

            $keterangan_koefisien[$i] = $rs->getString('keterangan_koefisien');
            $nilai_satuan+=$rs->getString('hasil_kali');

            $detail_harga[$i] = number_format($rs->getString('detail_harga'), 0, ",", ".");
            $hasil_kali[$i] = number_format($rs->getString('hasil_kali'), 0, ",", ".");

            $hasil[$i] = number_format($rs->getString('hasil'), 0, ",", ".");
            $i = $i + 1;
        }
        $this->banyak = $i;
        //$this->nilai_satuan = $nilai_satuan;
        $this->input = $input;
        $this->keterangan_koefisien = $keterangan_koefisien;
        $this->detail_harga = $detail_harga;
        $this->hasil_kali = $hasil_kali;
        $this->hasil = $hasil;
        $this->detail_name = $detail_name;
        $this->detail_satuan = $detail_satuan;
        $this->pajak = $pajak;
        $this->belanja_name = $belanja_name;
        return sfView::SUCCESS;
    }

    public function executeSublist() {
        $this->processSortsub();

        $this->processFilterssub();

        $this->filters = $this->getUser()->getAttributeHolder()->getAll('sf_admin/sub_kegiatan/filters');

        // pager
        $this->pager = new sfPropelPager('SubKegiatan', 20);
        $c = new Criteria();
        $c->add(SubKegiatanPeer::STATUS, 'Close');
        $this->addSortCriteriasub($c);
        $this->addFiltersCriteriasub($c);

        $this->pager->setCriteria($c);
        $this->pager->setPage($this->getRequestParameter('page', 1));
        $this->pager->init();
    }

    protected function processSortsub() {
        if ($this->getRequestParameter('sort')) {
            $this->getUser()->setAttribute('sort', $this->getRequestParameter('sort'), 'sf_admin/sub_kegiatan/sort');
            $this->getUser()->setAttribute('type', $this->getRequestParameter('type', 'asc'), 'sf_admin/sub_kegiatan/sort');
        }

        if (!$this->getUser()->getAttribute('sort', null, 'sf_admin/sub_kegiatan/sort')) {
            
        }
    }

    protected function addFiltersCriteriasub($c) {
        if (isset($this->filters['sub_kegiatan_id_is_empty'])) {
            $criterion = $c->getNewCriterion(SubKegiatanPeer::SUB_KEGIATAN_ID, '');
            $criterion->addOr($c->getNewCriterion(SubKegiatanPeer::SUB_KEGIATAN_ID, null, Criteria::ISNULL));
            $c->add($criterion);
        } else if (isset($this->filters['sub_kegiatan_id']) && $this->filters['sub_kegiatan_id'] !== '') {
            $kata = '%' . $this->filters['sub_kegiatan_id'] . '%';
            $c->add(SubKegiatanPeer::SUB_KEGIATAN_ID, strtr($kata, '*', '%'), Criteria::ILIKE);
        }
        if (isset($this->filters['sub_kegiatan_name_is_empty'])) {
            $criterion = $c->getNewCriterion(SubKegiatanPeer::SUB_KEGIATAN_NAME, '');
            $criterion->addOr($c->getNewCriterion(SubKegiatanPeer::SUB_KEGIATAN_NAME, null, Criteria::ISNULL));
            $c->add($criterion);
        } else if (isset($this->filters['sub_kegiatan_name']) && $this->filters['sub_kegiatan_name'] !== '') {
            $kata = '%' . $this->filters['sub_kegiatan_name'] . '%';
            $c->add(SubKegiatanPeer::SUB_KEGIATAN_NAME, strtr($kata, '*', '%'), Criteria::ILIKE);
        }
    }

    protected function addSortCriteriasub($c) {
        if ($sort_column = $this->getUser()->getAttribute('sort', null, 'sf_admin/sub_kegiatan/sort')) {
            $sort_column = SubKegiatanPeer::translateFieldName($sort_column, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_COLNAME);
            if ($this->getUser()->getAttribute('type', null, 'sf_admin/sub_kegiatan/sort') == 'asc') {
                $c->addAscendingOrderByColumn($sort_column);
            } else {
                $c->addDescendingOrderByColumn($sort_column);
            }
        }
    }

    protected function processFilterssub() {
        if ($this->getRequest()->hasParameter('filter')) {
            $filters = $this->getRequestParameter('filters');

            $this->getUser()->getAttributeHolder()->removeNamespace('sf_admin/sub_kegiatan/filters');
            $this->getUser()->getAttributeHolder()->add($filters, 'sf_admin/sub_kegiatan/filters');
        }
    }

    public function executeSabfisikedit() {
        $coded = $this->getRequestParameter('coded', '');

        return sfView::SUCCESS;
    }

    public function executeSablist() {
        $this->processSortsab();

        $this->processFilterssab();

        $this->filters = $this->getUser()->getAttributeHolder()->getAll('sf_admin/komponen/filters');

        // pager
        $this->pager = new sfPropelPager('Komponen', 20);
        $c = new Criteria();
        $c->add(KomponenPeer::KOMPONEN_TIPE, 'FISIK');
        $this->addSortCriteriasab($c);
        $this->addFiltersCriteriasab($c);
        $c->addAscendingOrderByColumn(KomponenPeer::KOMPONEN_ID);
        $this->pager->setCriteria($c);
        $this->pager->setPage($this->getRequestParameter('page', 1));
        $this->pager->init();
    }

    protected function processFilterssab() {
        if ($this->getRequest()->hasParameter('filter')) {
            $filters = $this->getRequestParameter('filters');

            $this->getUser()->getAttributeHolder()->removeNamespace('sf_admin/komponen/filters');
            $this->getUser()->getAttributeHolder()->add($filters, 'sf_admin/komponen/filters');
        }
    }

    protected function processSortsab() {
        if ($this->getRequestParameter('sort')) {
            $this->getUser()->setAttribute('sort', $this->getRequestParameter('sort'), 'sf_admin/komponen/sort');
            $this->getUser()->setAttribute('type', $this->getRequestParameter('type', 'asc'), 'sf_admin/komponen/sort');
        }

        if (!$this->getUser()->getAttribute('sort', null, 'sf_admin/komponen/sort')) {
            
        }
    }

    protected function addFiltersCriteriasab($c) {
        if (isset($this->filters['komponen_id_is_empty'])) {
            $criterion = $c->getNewCriterion(KomponenPeer::KOMPONEN_ID, '');
            $criterion->addOr($c->getNewCriterion(KomponenPeer::KOMPONEN_ID, null, Criteria::ISNULL));
            $c->add($criterion);
        } else if (isset($this->filters['komponen_id']) && $this->filters['komponen_id'] !== '') {
            $kata = '%' . $this->filters['komponen_id'] . '%';
            $c->add(KomponenPeer::KOMPONEN_ID, strtr($kata, '*', '%'), Criteria::ILIKE);
        }
        if (isset($this->filters['komponen_name_is_empty'])) {
            $criterion = $c->getNewCriterion(KomponenPeer::KOMPONEN_NAME, '');
            $criterion->addOr($c->getNewCriterion(KomponenPeer::KOMPONEN_NAME, null, Criteria::ISNULL));
            $c->add($criterion);
        } else if (isset($this->filters['komponen_name']) && $this->filters['komponen_name'] !== '') {
            $kata = '%' . $this->filters['komponen_name'] . '%';
            $c->add(KomponenPeer::KOMPONEN_NAME, strtr($kata, '*', '%'), Criteria::ILIKE);
        }
    }

    protected function addSortCriteriasab($c) {
        if ($sort_column = $this->getUser()->getAttribute('sort', null, 'sf_admin/komponen/sort')) {
            $sort_column = KomponenPeer::translateFieldName($sort_column, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_COLNAME);
            if ($this->getUser()->getAttribute('type', null, 'sf_admin/komponen/sort') == 'asc') {
                $c->addAscendingOrderByColumn($sort_column);
            } else {
                $c->addDescendingOrderByColumn($sort_column);
            }
        }
    }

    public function executeHspkedit() {
        return sfView::SUCCESS;
    }

    public function executeHspklist() {
        $this->processSorthspk();

        $this->processFiltershspk();

        $this->filters = $this->getUser()->getAttributeHolder()->getAll('sf_admin/komponen/filters');

        // pager
        $this->pager = new sfPropelPager('Komponen', 20);
        $c = new Criteria();
        $this->addSortCriteriahspk($c);
        $c->add(KomponenPeer::KOMPONEN_TIPE, 'HSPK');
        $c->addAscendingOrderByColumn(KomponenPeer::KOMPONEN_ID);
        $this->addFiltersCriteriahspk($c);
        $this->pager->setCriteria($c);
        $this->pager->setPage($this->getRequestParameter('page', 1));
        $this->pager->init();
    }

    protected function processFiltershspk() {
        if ($this->getRequest()->hasParameter('filter')) {
            $filters = $this->getRequestParameter('filters');

            $this->getUser()->getAttributeHolder()->removeNamespace('sf_admin/komponen/filters');
            $this->getUser()->getAttributeHolder()->add($filters, 'sf_admin/komponen/filters');
        }
    }

    protected function processSorthspk() {
        if ($this->getRequestParameter('sort')) {
            $this->getUser()->setAttribute('sort', $this->getRequestParameter('sort'), 'sf_admin/komponen/sort');
            $this->getUser()->setAttribute('type', $this->getRequestParameter('type', 'asc'), 'sf_admin/komponen/sort');
        }

        if (!$this->getUser()->getAttribute('sort', null, 'sf_admin/komponen/sort')) {
            
        }
    }

    protected function addFiltersCriteriahspk($c) {
        if (isset($this->filters['komponen_id_is_empty'])) {
            $criterion = $c->getNewCriterion(KomponenPeer::KOMPONEN_ID, '');
            $criterion->addOr($c->getNewCriterion(KomponenPeer::KOMPONEN_ID, null, Criteria::ISNULL));
            $c->add($criterion);
        } else if (isset($this->filters['komponen_id']) && $this->filters['komponen_id'] !== '') {
            $kata = '%' . $this->filters['komponen_id'] . '%';
            $c->add(KomponenPeer::KOMPONEN_ID, strtr($kata, '*', '%'), Criteria::ILIKE);
        }
        if (isset($this->filters['komponen_name_is_empty'])) {
            $criterion = $c->getNewCriterion(KomponenPeer::KOMPONEN_NAME, '');
            $criterion->addOr($c->getNewCriterion(KomponenPeer::KOMPONEN_NAME, null, Criteria::ISNULL));
            $c->add($criterion);
        } else if (isset($this->filters['komponen_name']) && $this->filters['komponen_name'] !== '') {
            $kata = '%' . $this->filters['komponen_name'] . '%';
            $c->add(KomponenPeer::KOMPONEN_NAME, strtr($kata, '*', '%'), Criteria::ILIKE);
        }
        if (isset($this->filters['komponen_barang']) && $this->filters['komponen_barang'] !== '') {
            $kode = trim($this->filters['komponen_barang'] . '%');
            $c->add(KomponenPeer::KOMPONEN_ID, strtr($kode, '*', '%'), Criteria::ILIKE);
        }
    }

    protected function addSortCriteriahspk($c) {
        if ($sort_column = $this->getUser()->getAttribute('sort', null, 'sf_admin/komponen/sort')) {
            $sort_column = KomponenPeer::translateFieldName($sort_column, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_COLNAME);
            if ($this->getUser()->getAttribute('type', null, 'sf_admin/komponen/sort') == 'asc') {
                $c->addAscendingOrderByColumn($sort_column);
            } else {
                $c->addDescendingOrderByColumn($sort_column);
            }
        }
    }

    public function executeShsdlist() {
        $this->processSortshsd();

        $this->processFiltersshsd();

        $this->filters = $this->getUser()->getAttributeHolder()->getAll('sf_admin/shsd/filters');

        // pager
        $this->pager = new sfPropelPager('Shsd', 20);
        $c = new Criteria();
        //$this->addSortCriteria($c);
        $dinas_array = $this->getUser()->getAttributeHolder()->getAll('peneliti');
        //print_r($dinas_array['nama']);exit;
        if (($dinas_array['nama'] != 'diana') and ($dinas_array['nama'] != 'dede') and ($dinas_array['nama'] != 'tria')) {
            //print_r($dinas_array['nama']);exit;
            $c->add(ShsdPeer::SHSD_ID, '23.01.01.05.01.08', Criteria::NOT_EQUAL);
        }
        $this->addFiltersShsdCriteria($c);
        $c->addAscendingOrderByColumn(ShsdPeer::SHSD_ID);

        $this->pager->setCriteria($c);
        $this->pager->setPage($this->getRequestParameter('page', 1));
        $this->pager->init();
    }

    protected function processFiltersshsd() {
        if ($this->getRequest()->hasParameter('filter')) {
            $filters = $this->getRequestParameter('filters');

            $this->getUser()->getAttributeHolder()->removeNamespace('sf_admin/shsd/filters');
            $this->getUser()->getAttributeHolder()->add($filters, 'sf_admin/shsd/filters');
        }
    }

    protected function processSortshsd() {
        if ($this->getRequestParameter('sort')) {
            $this->getUser()->setAttribute('sort', $this->getRequestParameter('sort'), 'sf_admin/shsd/sort');
            $this->getUser()->setAttribute('type', $this->getRequestParameter('type', 'asc'), 'sf_admin/shsd/sort');
        }

        if (!$this->getUser()->getAttribute('sort', null, 'sf_admin/shsd/sort')) {
            
        }
    }

    protected function addFiltersShsdCriteria($c) {
        if (isset($this->filters['shsd_id_is_empty'])) {
            $criterion = $c->getNewCriterion(ShsdPeer::SHSD_ID, '');
            $criterion->addOr($c->getNewCriterion(ShsdPeer::SHSD_ID, null, Criteria::ISNULL));
            $c->add($criterion);
        } else if (isset($this->filters['shsd_id']) && $this->filters['shsd_id'] !== '') {
            $kata = '%' . $this->filters['shsd_id'] . '%';
            $c->add(ShsdPeer::SHSD_ID, strtr($kata, '*', '%'), Criteria::ILIKE);
            $c->addAscendingOrderByColumn(ShsdPeer::SHSD_ID);
        }
        if (isset($this->filters['shsd_name_is_empty'])) {
            $criterion = $c->getNewCriterion(ShsdPeer::SHSD_NAME, '');
            $criterion->addOr($c->getNewCriterion(ShsdPeer::SHSD_NAME, null, Criteria::ISNULL));
            $c->addAscendingOrderByColumn(ShsdPeer::SHSD_ID);
            $c->add($criterion);
        } else if (isset($this->filters['shsd_name']) && $this->filters['shsd_name'] !== '') {
            $kata = '%' . $this->filters['shsd_name'] . '%';
            $c->add(ShsdPeer::SHSD_NAME, strtr($kata, '*', '%'), Criteria::ILIKE);
            $c->addAscendingOrderByColumn(ShsdPeer::SHSD_ID);
        }
    }

    public function executeShsdlockedlist() {
        $this->processSortshsdlocked();

        $this->processFiltersshsdlocked();

        $this->filters = $this->getUser()->getAttributeHolder()->getAll('sf_admin/komponen/filters');

        // pager
        $this->pager = new sfPropelPager('Komponen', 100);
        $c = new Criteria();
        $c->add(KomponenPeer::KOMPONEN_TIPE, 'SHSD');
        $c->addAscendingOrderByColumn(KomponenPeer::KOMPONEN_ID);
        $this->addSortCriteriashsdlocked($c);
        $this->addFiltersCriteriashsdlocked($c);
        $dinas_array = $this->getUser()->getAttributeHolder()->getAll('peneliti');

        //print_r($dinas_array['nama']);exit;
        if (($dinas_array['nama'] <> 'diana') and ($dinas_array['nama'] <> 'dede') and ($dinas_array['nama'] <> 'trie')) {
            $c->add(KomponenPeer::KOMPONEN_ID, '23.01.01.05.01.08', Criteria::NOT_EQUAL);
        }
        $this->pager->setCriteria($c);
        $this->pager->setPage($this->getRequestParameter('page', 1));
        $this->pager->init();
    }

    protected function processFiltersshsdlocked() {
        if ($this->getRequest()->hasParameter('filter')) {
            $filters = $this->getRequestParameter('filters');

            $this->getUser()->getAttributeHolder()->removeNamespace('sf_admin/komponen/filters');
            $this->getUser()->getAttributeHolder()->add($filters, 'sf_admin/komponen/filters');
        }
    }

    protected function processSortshsdlocked() {
        if ($this->getRequestParameter('sort')) {
            $this->getUser()->setAttribute('sort', $this->getRequestParameter('sort'), 'sf_admin/komponen/sort');
            $this->getUser()->setAttribute('type', $this->getRequestParameter('type', 'asc'), 'sf_admin/komponen/sort');
        }

        if (!$this->getUser()->getAttribute('sort', null, 'sf_admin/komponen/sort')) {
            
        }
    }

    protected function addFiltersCriteriashsdlocked($c) {
        $cek = $this->getRequestParameter('search_option');
        //print_r($cek);exit;
        if ($cek == 'shsd_id') {
            if (isset($this->filters['komponen_id_is_empty'])) {
                $criterion = $c->getNewCriterion(KomponenPeer::KOMPONEN_ID, '');
                $criterion->addOr($c->getNewCriterion(KomponenPeer::KOMPONEN_ID, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['komponen_id']) && $this->filters['komponen_id'] !== '') {
                $kata = '%' . $this->filters['komponen_id'] . '%';
                $c->add(KomponenPeer::KOMPONEN_ID, strtr($kata, '*', '%'), Criteria::ILIKE);
            }
        } elseif ($cek == 'shsd_name') {
            if (isset($this->filters['komponen_id_is_empty'])) {
                $criterion = $c->getNewCriterion(KomponenPeer::KOMPONEN_NAME, '');
                $criterion->addOr($c->getNewCriterion(KomponenPeer::KOMPONEN_NAME, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['komponen_id']) && $this->filters['komponen_id'] !== '') {
                $kata = '%' . $this->filters['komponen_id'] . '%';
                $c->add(KomponenPeer::KOMPONEN_NAME, strtr($kata, '*', '%'), Criteria::ILIKE);
            }
        } else {
            if (isset($this->filters['komponen_id_is_empty'])) {
                $criterion = $c->getNewCriterion(KomponenPeer::KOMPONEN_NAME, '');
                $criterion->addOr($c->getNewCriterion(KomponenPeer::KOMPONEN_NAME, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['komponen_id']) && $this->filters['komponen_id'] !== '') {
                $kata = '%' . $this->filters['komponen_id'] . '%';
                $c->add(KomponenPeer::KOMPONEN_NAME, strtr($kata, '*', '%'), Criteria::ILIKE);
            }
        }
    }

    protected function addSortCriteriashsdlocked($c) {
        if ($sort_column = $this->getUser()->getAttribute('sort', null, 'sf_admin/komponen/sort')) {
            $sort_column = KomponenPeer::translateFieldName($sort_column, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_COLNAME);
            if ($this->getUser()->getAttribute('type', null, 'sf_admin/komponen/sort') == 'asc') {
                $c->addAscendingOrderByColumn($sort_column);
            } else {
                $c->addDescendingOrderByColumn($sort_column);
            }
        }
    }

    public function executeLihatSAB() {
        //$coded='';
        $coded = $this->getRequestParameter('coded', '');

        $komponen_id = $this->getRequestParameter('komponen_id');
        //print_r(substr(trim($komponen_id),0,2));exit;
        if (substr(trim($komponen_id), 0, 2) <= 28) {
            return $this->redirect('peneliti/lihatsabfisik?id=' . $komponen_id . '&coded=' . $coded);
        } elseif (substr($komponen_id, 0, 2) < 30) {
            return $this->redirect('peneliti/lihatsublist?id=' . $komponen_id . '&coded=' . $coded);
        } elseif (substr($komponen_id, 0, 2) >= 30) {

            return $this->redirect('peneliti/lihatsabom?id=' . $komponen_id . '&coded=' . $coded);
        }
    }

    public function executeLihatsabfisik() {
        $coded = $this->getRequestParameter('coded', '');
        $this->komponen_id = $this->getRequestParameter('komponen_id');
    }

    public function executeLihatsublist() {
        $coded = $this->getRequestParameter('coded', '');
        $sub_id = $this->getRequestParameter('id');
        $input = array();
        $c = new Criteria();
        $c->add(SubKegiatanPeer::SUB_KEGIATAN_ID, $sub_id);
        //$c -> add (SubKegiatanPeer::STATUS, 'Close') ;
        $cs = SubKegiatanPeer::doSelectOne($c);
        if ($cs) {
            $this->sub_kegiatan_id = $cs->getSubKegiatanId();
            $this->sub_kegiatan_name = $cs->getSubKegiatanName();
            $this->param = $cs->getParam();
            $this->satuan = $cs->getSatuan();
        }

        $d = new Criteria();
        $d->add(KomponenMemberPeer::KOMPONEN_ID, $sub_id);
        $d->addAscendingOrderByColumn(KomponenMemberPeer::SUBTITLE);
        $d->addAscendingOrderByColumn(KomponenMemberPeer::TIPE);
        $ds = KomponenMemberPeer::doSelect($d);
        $this->komponen_penyusun = $ds;
        $total = 0;
        foreach ($ds as $x) {
            $total = $total + $x->getMemberTotal();
        }
        $this->total_penyusun = $total;
        $sql = "select 
		sum(m.volume*m.komponen_harga_awal*(100+m.pajak)/100) as nilai_satuan
		from 
		" . sfConfig::get('app_default_schema') . ".sub_kegiatan_member m
		where 
		m.sub_kegiatan_id = '" . $sub_id . "'";
        //print_r($sql);exit;
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($sql);
        $rs = $stmt->executeQuery();
        while ($rs->next()) {
            $nilai_satuan = $rs->getString('nilai_satuan');
        }
        $this->nilai_satuan = $nilai_satuan;

        $query = "
		SELECT 
		rekening.rekening_code,
		detail.detail_name as detail_name,
		detail.komponen_name,
		detail.komponen_name || ' ' || detail.detail_name as detail_name2,
		detail.komponen_harga_awal as detail_harga,
		detail.pajak,
		detail.komponen_id,
		detail.subtitle ,
		detail_no,koefisien,param,
		detail.satuan as detail_satuan,
		replace(detail.keterangan_koefisien,'<*>','X') as keterangan_koefisien,
		detail.volume * detail.komponen_harga_awal as hasil,
		(detail.volume * detail.komponen_harga_awal  * (100+detail.pajak)/100) as hasil_kali,
		
		
		(SELECT '2' FROM " . sfConfig::get('app_default_schema') . ".komponen where komponen.komponen_id=detail.komponen_id AND komponen.komponen_tipe='EST')
		as x,
		(SELECT 'bahaya' FROM " . sfConfig::get('app_default_schema') . ".komponen where komponen.komponen_id=detail.komponen_id AND komponen_show=FALSE)
		as bahaya,
		substring(kb.belanja_name,8) as belanja_name
		
		FROM 
		" . sfConfig::get('app_default_schema') . ".rekening rekening ,
		" . sfConfig::get('app_default_schema') . ".sub_kegiatan_member detail ,
		" . sfConfig::get('app_default_schema') . ".kelompok_belanja kb
		
		WHERE 
		rekening.rekening_code = detail.rekening_code and
		detail.sub_kegiatan_id = '" . $sub_id . "' and 
		kb.belanja_id=rekening.belanja_id 
		
		ORDER BY 
		
		belanja_urutan,
		komponen_name
		
		";
        //echo $query;
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query);
        $rs = $stmt->executeQuery();
        $this->r = $rs;
        //$this->input = Array();
        $keterangan_koefisien = array();
        $detail_harga = array();
        $hasil_kali = array();
        $hasil = array();
        $detail_name = array();
        $detail_satuan = array();
        $pajak = array();
        $belanja_name = array();
        $this->keterangan_koefisien = Array();
        $this->detail_harga = Array();
        $this->hasil_kali = Array();
        $this->hasil = Array();
        $this->detail_name = array();
        $this->detail_satuan = array();
        $this->pajak = array();
        $this->belanja_name = array();
        $i = 0;
        while ($rs->next()) {
            $pars = explode("|", $rs->getString('param'));

            for ($j = 0; $j < count($pars); $j++) {
                if ($j == 0)
                    $inputs = $pars[$j];
                else
                    $inputs = $inputs . ',<BR>' . $pars[$j];
            }
            //echo $r['param'];
            $input[$i] = $inputs;
            //print_r($inputs);exit;

            $detail_name[$i] = $rs->getString('detail_name2');
            $detail_satuan[$i] = $rs->getString('detail_satuan');
            $pajak[$i] = $rs->getString('pajak');
            $belanja_name[$i] = $rs->getString('belanja_name');

            $keterangan_koefisien[$i] = $rs->getString('keterangan_koefisien');
            $nilai_satuan+=$rs->getString('hasil_kali');

            $detail_harga[$i] = number_format($rs->getString('detail_harga'), 0, ",", ".");
            $hasil_kali[$i] = number_format($rs->getString('hasil_kali'), 0, ",", ".");

            $hasil[$i] = number_format($rs->getString('hasil'), 0, ",", ".");
            $i = $i + 1;
        }
        $this->banyak = $i;
        //$this->nilai_satuan = $nilai_satuan;
        $this->input = $input;
        $this->keterangan_koefisien = $keterangan_koefisien;
        $this->detail_harga = $detail_harga;
        $this->hasil_kali = $hasil_kali;
        $this->hasil = $hasil;
        $this->detail_name = $detail_name;
        $this->detail_satuan = $detail_satuan;
        $this->pajak = $pajak;
        $this->belanja_name = $belanja_name;
        return sfView::SUCCESS;
    }

    public function executeLihatsabom() {
        $coded = $this->getRequestParameter('coded', '');
        return sfView::SUCCESS;
    }

    public function executeSimpanSubKegiatan() {
        $sub_kegiatan_id = $this->getRequestParameter('sub_kegiatan_id');
        $kodesub = $this->getRequestParameter('kodesub');
        $kegiatan_code = $this->getRequestParameter('kegiatan_code');
        $unit_id = $this->getRequestParameter('unit_id');

        if ($this->getRequestParameter('lokasipeta') == 'lokasi') {
            $user = $this->getUser();
            $user->removeCredential('lokasi');
            $user->addCredential('lokasi');
            $user->setAttribute('nama', $this->getRequestParameter('keterangan'), 'lokasi');
            $user->setAttribute('id', $sub_kegiatan_id, 'lokasi');
            $user->setAttribute('kegiatan', $kegiatan_code, 'lokasi');
            $user->setAttribute('unit', $unit_id, 'lokasi');
            $user->setAttribute('subtitle', $this->getRequestParameter('subtitle'), 'lokasi');
            $user->setAttribute('tipe', $this->getRequestParameter('tipe'), 'lokasi');
            $user->setAttribute('barusub', 'barusub', 'lokasi');

            return $this->forward('lokasi', 'list');
        }
        if ($this->getRequestParameter('simpansub') == 'simpan') {
            $sql = "select max(kode_sub) as kode_sub from " . sfConfig::get('app_default_schema') . ".rincian_sub_parameter where kode_sub ilike 'RSUB%'";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($sql);
            $rs = $stmt->executeQuery();
            while ($rs->next()) {
                $kodesub = $rs->getString('kode_sub');
            }
            $kode = substr($kodesub, 4, 5);
            $kode+=1;
            if ($kode < 10) {
                $kodesub = 'RSUB0000' . $kode;
            } elseif ($kode < 100) {
                $kodesub = 'RSUB000' . $kode;
            } elseif ($kode < 1000) {
                $kodesub = 'RSUB00' . $kode;
            } elseif ($kode < 10000) {
                $kodesub = 'RSUB0' . $kode;
            } elseif ($kode < 100000) {
                $kodesub = 'RSUB' . $kode;
            }

            if ($this->getRequestParameter('keterangan')) {
                $m_lokasi_ada = 'tidak ada';
                $lokasi = $this->getRequestParameter('keterangan');
                $query = "select (1) as ada from v_lokasi where nama ilike '$lokasi'";
                $con = Propel::getConnection();
                $stmt = $con->prepareStatement($query);
                $rs_cek = $stmt->executeQuery();
                while ($rs_cek->next()) {
                    if ($rs_cek->getString('ada')) {
                        $m_lokasi_ada = 'ada';
                    }
                }
            }
            //print_r($m_lokasi_ada);exit;
            if ($m_lokasi_ada == 'tidak ada') {
                $this->setFlash('error_lokasi', 'Lokasi yang dimasukkan tidak ada dalam G.I.S');
                return $this->redirect("peneliti/pilihSubKegiatan?id=$sub_kegiatan_id&kode_kegiatan=$kegiatan_code&unit_id=$unit_id&sub=Pilih");
            }

            if ($this->getRequestParameter('subtitle')) {
                $kode_sub = $this->getRequestParameter('subtitle');
                $c = new Criteria();
                $c->add(SubtitleIndikatorPeer::UNIT_ID, $unit_id);
                $c->add(SubtitleIndikatorPeer::KEGIATAN_CODE, $kegiatan_code);
                $c->add(SubtitleIndikatorPeer::SUB_ID, $kode_sub);
                $rs_subtitle = SubtitleIndikatorPeer::doSelectOne($c);
                if ($rs_subtitle) {
                    $subtitle = $rs_subtitle->getSubtitle();
                }
            }

            $keter = $this->getRequestParameter('keterangan1') . ' ' . $this->getRequestParameter('keterangan');
            $detail_name = str_replace("'", " ", $keter);
            //$subtitle = $this->getRequestParameter('subtitle');
            $query = "SELECT * from " . sfConfig::get('app_default_schema') . ".sub_kegiatan where sub_kegiatan_id='" . $sub_kegiatan_id . "'";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs = $stmt->executeQuery();

            while ($rs->next()) {
                $sub_kegiatan_name = $rs->getString('sub_kegiatan_name');
                $subtitle2 = $sub_kegiatan_name . " " . $keter;
                $param = $rs->getString('param');
                $satuan = $rs->getString('satuan');
                $pembagi = $rs->getString('pembagi');
                $new_subtitle = $sub_kegiatan_name . " " . $keter . " " . $subtitle;
                if (($sub_kegiatan_id != '') && ($new_subtitle != '')) {
                    $query = "select kode_sub from " . sfConfig::get('app_default_schema') . ".rincian_sub_parameter where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and subtitle='$subtitle' and from_sub_kegiatan='$sub_kegiatan_id' and detail_name='$detail_name'";
                    $con = Propel::getConnection();
                    $stmt = $con->prepareStatement($query);
                    $rs_cek = $stmt->executeQuery();
                    while ($rs_cek->next()) {
                        if ($rs_cek->getString('kode_sub')) {
                            $kodesub = $rs_cek->getString('kode_sub');
                            $this->setFlash('error_lokasi', 'ASB Non Fisik dengan Subtitle dengan Kegiatan Sudah Ada. Mohon Diperhatikan Kembali');
                            return $this->redirect("peneliti/pilihSubKegiatan?id=$sub_kegiatan_id&kode_kegiatan=$kegiatan_code&unit_id=$unit_id&sub=Pilih");
                        }
                    }
                }
            }
            $arrparamsub = explode("|", $param);
            $arr_pembagi = explode("|", $pembagi);
            $keterangan = '';
            $arr_input = '';
            $jumlah = count($arrparamsub);
            //$j=0;
            for ($i = 0; $i < $jumlah; $i++) {
                //print_r(str_replace(' ','',$arrparamsub[$i]).'pppppp<br>');
                //echo $this->getRequestParameter('param_'.str_replace(' ','',$arrparamsub[$i]));

                if ($this->getRequestParameter('param_' . $i) != '') {
                    if ($i == 0) {

                        $arr_input = $this->getRequestParameter('param_' . $i);
                        //print_r($arr_input).'<br>';
                    }
                    if ($i != 0) {
                        $arr_input = $arr_input . '|' . $this->getRequestParameter('param_' . $i);
                    }
                }
            }
            //print_r($arr_input);exit;
            $arrsatuansub = explode("|", $satuan);
            //$arr_input=implode("|",$inp);
            $inp = array();
            $inp = explode("|", $arr_input);
            $arr_pembagi = explode("|", $pembagi);
            $pembagi = 1;
            $ket_pembagi = '';
            //$pembagi='';
            for ($j = 0; $j < count($arrparamsub); $j++) {
                if ($j >= 0) {
                    if (isset($inp[$j])) {
                        if ($inp[$j] != 0)
                            $keterangan = $keterangan . '<tr><td class="Font8v">' . $arrparamsub[$j] . '</td><td class="Font8v">' . $inp[$j] . ' ' . $arrsatuansub[$j] . '</td></tr>';
                    }else {
                        if ((isset($arrparamsub[$j])) && (isset($arrsatuansub[$j])) && (isset($inp[$j]))) {
                            $keterangan = '<table><tr><td class="Font8v">' . $arrparamsub[$j] . '</td><td class="Font8v">' . $inp[$j] . ' ' . $arrsatuansub[$j] . '</td></tr>';
                        }
                    }
                }
                if (isset($arr_pembagi[$j])) {
                    if ($arr_pembagi[$j] == 't') {
                        if ($inp[$j] and $inp[$j] != 0)
                            $pembagi = $pembagi * $inp[$j];
                        $ket_pembagi = $ket_pembagi . "," . $arrparamsub[$j];
                    }
                }
            }
            $keterangan = $keterangan . '</table>';
            $ket_pembagi = substr($ket_pembagi, 1);

            $query = "INSERT INTO " . sfConfig::get('app_default_schema') . ".rincian_sub_parameter(unit_id,kegiatan_code,from_sub_kegiatan,sub_kegiatan_name,subtitle,detail_name,new_subtitle,param,keterangan,ket_pembagi,pembagi,kode_sub) VALUES(
	'" . $unit_id . "','" . $kegiatan_code . "','" . $sub_kegiatan_id . "','" . $sub_kegiatan_name . "','" . $subtitle . "','" . $keter . "', '" . $new_subtitle . "','" . $arr_input . "','" . $keterangan . "','" . $ket_pembagi . "'," . $pembagi . ",'" . $kodesub . "')";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            //echo $query.'<BR>';
            $stmt->executeQuery();

            $query = "SELECT *, satuan as satuan_asli from " . sfConfig::get('app_default_schema') . ".sub_kegiatan_member where sub_kegiatan_id='" . $sub_kegiatan_id . "'";

            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs = $stmt->executeQuery();
            //$volume=1;
            while ($rs->next()) {
                if ($rs->getString('param') == '') {
                    $keterangan_koefisien = $rs->getString('keterangan_koefisien');
                    $volume = $rs->getString('volume');
                } else {
                    $arrparam = array();
                    $arrparam = explode("|", $rs->getString('param'));

                    $volume = $rs->getString('volume');

                    $keterangan_koefisien = '';
                    if ($rs->getString('keterangan_koefisien')) {
                        $keterangan_koefisien = $rs->getString('keterangan_koefisien');
                    }
                    for ($j = 0; $j < count($arrparam); $j++) {

                        for ($i = 0; $i < count($arrparamsub); $i++) {

                            if (str_replace(' ', '', $arrparam[$j]) == str_replace(' ', '', $arrparamsub[$i])) {

                                $volume = $volume * $inp[$i];

                                if ($keterangan_koefisien != '') {

                                    $keterangan_koefisien = $keterangan_koefisien . ' X ' . $inp[$i] . ' ' . $arrsatuansub[$i] . '(Fix)';
                                } else {

                                    $keterangan_koefisien = $inp[$i] . ' ' . $arrsatuansub[$i] . '(Fix)';
                                }
                            }
                        }
                    }
                }


                if (stristr($keterangan_koefisien, "'")) {
                    $keterangan_koefisien = addslashes($keterangan_koefisien);
                    $detail_name = addslashes($rs->getString('detail_name'));
                }
                $query1 = "SELECT max(detail_no) AS maxno FROM " . sfConfig::get('app_default_schema') . ".rincian_detail 
				 WHERE kegiatan_code='" . $kegiatan_code . "'  and unit_id='" . $unit_id . "'";


                $con = Propel::getConnection();
                $stmt1 = $con->prepareStatement($query1);
                $rs1 = $stmt1->executeQuery();
                //$maxno=0;
                //echo $query1;exit; 
                while ($rs1->next()) {
                    $maxno = $rs1->getInt('maxno');
                    if (!$maxno) {
                        $maxno = 1;
                    } else {
                        $maxno = $maxno + 1;
                    }
                }
                $pajakawal = $rs->getInt('pajak');

                if (!$pajakawal) {
                    $pajakawal = 0;
                }

                $rekening_code = $rs->getString('rekening_code');
                $komponen_id = $rs->getString('komponen_id');
                $detail_name = $rs->getString('detail_name');
                $komponen_harga_awal = $rs->getString('komponen_harga_awal');
                $komp = new Criteria();
                $komp->add(KomponenPeer::KOMPONEN_ID, $komponen_id);
                $terpilih_komponen = KomponenPeer::doSelectOne($komp);
                if ($terpilih_komponen) {
                    $komponen_name = $terpilih_komponen->getKomponenName();
                }

                if ($this->getRequestParameter('subtitle')) {
                    $kode_sub = $this->getRequestParameter('subtitle');
                    $c = new Criteria();
                    $c->add(SubtitleIndikatorPeer::UNIT_ID, $unit_id);
                    $c->add(SubtitleIndikatorPeer::KEGIATAN_CODE, $kegiatan_code);
                    $c->add(SubtitleIndikatorPeer::SUB_ID, $kode_sub);
                    $rs_subtitle = SubtitleIndikatorPeer::doSelectOne($c);
                    if ($rs_subtitle) {
                        $subtitle = $rs_subtitle->getSubtitle();
                    }
                }
                $user = $this->getUser();
                $dinas = $user->setAttribute('nama', '', 'peneliti');

                $query2 = "
		INSERT INTO " . sfConfig::get('app_default_schema') . ".rincian_detail 
		(kegiatan_code,tipe,unit_id,detail_no,rekening_code,komponen_id,detail_name,volume,keterangan_koefisien,subtitle,komponen_harga,pajak,komponen_harga_awal,komponen_name,satuan,from_sub_kegiatan,sub,kode_sub,last_update_user) 
		VALUES 
		('" . $kegiatan_code . "','SUB','" . $unit_id . "'," . $maxno . ",'" . $rekening_code . "','" . $komponen_id . "','" . $detail_name . "'," . $volume . ",'" . $keterangan_koefisien . "','" . $subtitle . "'," . $komponen_harga_awal * (($pajakawal + 100) / 100) . "," . $pajakawal . "," . $komponen_harga_awal . ",'" . $komponen_name . "','" . $rs->getString('satuan_asli') . "','" . $sub_kegiatan_id . "','" . $new_subtitle . "','" . $kodesub . "','" . $dinas . "')
		";

                if (($volume > 0) and ($keterangan_koefisien != '')) {
                    $con = Propel::getConnection();
                    $stmt = $con->prepareStatement($query2);
                    $stmt->executeQuery();
                }
            }
            $this->setFlash('berhasil', 'Sub Kegiatan Baru Telah Tersimpan');
            return $this->redirect("peneliti/edit?unit_id=" . $unit_id . "&kode_kegiatan=" . $kegiatan_code);
        }
    }

    public function executePilihSubKegiatan() {
        $sql = "select max(kode_sub) as kode_sub from " . sfConfig::get('app_default_schema') . ".rincian_detail where kode_sub ilike 'RSUB' and sub<>''";
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($sql);
        $rs = $stmt->executeQuery();
        while ($rs->next()) {
            $kodesub = $rs->getString('kode_sub');
        }
        $kode = substr($kodesub, 4, 5);
        $kode+=1;
        if ($kode < 10) {
            $kodesub = 'RSUB0000' . $kode;
        } elseif ($kode < 100) {
            $kodesub = 'RSUB000' . $kode;
        } elseif ($kode < 1000) {
            $kodesub = 'RSUB00' . $kode;
        } elseif ($kode < 10000) {
            $kodesub = 'RSUB0' . $kode;
        } elseif ($kode < 100000) {
            $kodesub = 'RSUB' . $kode;
        }
        $this->kodesub = $kodesub;
        return sfView::SUCCESS;
    }

    public function executeSatukanSubPekerjaans() {
        if ($this->getRequestParameter('id')) {

            $kode_sub = $this->getRequestParameter('no');
            $unit_id = $this->getRequestParameter('unit');
            $kegiatan_code = $this->getRequestParameter('kegiatan');

            $query = "select (1) as ada,subtitle,sub_kegiatan_name,detail_name,unit_id,kegiatan_code,from_sub_kegiatan,new_subtitle from " . sfConfig::get('app_default_schema') . ".rincian_sub_parameter where kegiatan_code='$kegiatan_code' and unit_id='$unit_id' and kode_sub='$kode_sub'";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs = $stmt->executeQuery();
            while ($rs->next()) {
                if ($rs->getString('ada')) {
                    $komponen_name = $rs->getString('sub_kegiatan_name');
                    $detail_name = $rs->getString('detail_name');
                    $komponen_id = $rs->getString('from_sub_kegiatan');
                    $sub = $rs->getString('new_subtitle');
                    $subtitle = $rs->getString('subtitle');
                    $query = "insert into " . sfConfig::get('app_default_schema') . ".rincian_detail_gabung_non_fisik select * from " . sfConfig::get('app_default_schema') . ".rincian_detail where kegiatan_code='$kegiatan_code' and unit_id='$unit_id' and kode_sub='$kode_sub'";
                    $con = Propel::getConnection();
                    $stmt = $con->prepareStatement($query);
                    $stmt->executeQuery();

                    $query = "select sum(volume*komponen_harga_awal*(100+pajak)/100) as jumlah
 					from " . sfConfig::get('app_default_schema') . ".rincian_detail where kegiatan_code='$kegiatan_code' and unit_id='$unit_id' and kode_sub='$kode_sub'";
                    $con = Propel::getConnection();
                    $stmt = $con->prepareStatement($query);
                    $rs2 = $stmt->executeQuery();
                    while ($rs2->next()) {
                        $harga_satu = $rs2->getString('jumlah');
                    }

                    $query = "select max(detail_no) as id
 					from " . sfConfig::get('app_default_schema') . ".rincian_detail where kegiatan_code='$kegiatan_code' and unit_id='$unit_id'";
                    $con = Propel::getConnection();
                    $stmt = $con->prepareStatement($query);
                    $rs3 = $stmt->executeQuery();
                    while ($rs3->next()) {
                        $id = $rs3->getString('id');
                    }
                    $id+=1;
                    $query = "select distinct(kode_sub) as kode
 					from " . sfConfig::get('app_default_schema') . ".rincian_detail where kegiatan_code='$kegiatan_code' and unit_id='$unit_id' and kode_sub='$kode_sub'";
                    $con = Propel::getConnection();
                    $stmt = $con->prepareStatement($query);
                    $rs4 = $stmt->executeQuery();
                    while ($rs4->next()) {
                        $kode_sub = $rs4->getString('kode');
                    }

                    $query = "update " . sfConfig::get('app_default_schema') . ".rincian_detail set volume=0,keterangan_koefisien='0',kode_sub='',sub='' where kegiatan_code='$kegiatan_code' and unit_id='$unit_id' and kode_sub='$kode_sub'";
                    $con = Propel::getConnection();
                    $stmt = $con->prepareStatement($query);
                    $stmt->executeQuery();


                    $rekening_code = '5.2.2.05.11';
                    $query2 = "
		INSERT INTO " . sfConfig::get('app_default_schema') . ".rincian_detail 
		(kegiatan_code,tipe,unit_id,detail_no,rekening_code,komponen_id,detail_name,volume,keterangan_koefisien,subtitle,komponen_harga,pajak,komponen_harga_awal,komponen_name,satuan,from_sub_kegiatan,sub,kode_sub) 
		VALUES 
		('$kegiatan_code','GBG','$unit_id',$id,'$rekening_code','$komponen_id', '$detail_name', 1,'1 Paket','$subtitle', $harga_satu, 0, $harga_satu,'$komponen_name','Paket','$komponen_id','$sub','$kode_sub')
		";

                    $con = Propel::getConnection();
                    $stmt = $con->prepareStatement($query2);
                    $stmt->executeQuery();
                }
            }
            $nama_subtitle = $subtitle;
            $query = "select * 
					  from " . sfConfig::get('app_default_schema') . ".rincian_detail 
					  where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and subtitle ilike '$nama_subtitle' and volume>0 order by sub,rekening_code,komponen_name";
            $con = Propel::getConnection(RincianDetailPeer::DATABASE_NAME);
            $statement = $con->prepareStatement($query);
            $rs_rinciandetail = $statement->executeQuery();
            $this->rs_rinciandetail = $rs_rinciandetail;
            $this->id = $sub_id;
            $this->rinciandetail = 'ada';
            $this->setLayout('kosong');
        }
    }

    public function executeHapusSubPekerjaans() {
        if ($this->getRequestParameter('id')) {

            $kode_sub = $this->getRequestParameter('no');
            $unit_id = $this->getRequestParameter('unit');
            $kegiatan_code = $this->getRequestParameter('kegiatan');

            $tahap = '';
            $c = new Criteria();
            $c->add(MasterKegiatanPeer::UNIT_ID, $unit_id);
            $c->add(MasterKegiatanPeer::KODE_KEGIATAN, $kegiatan_code);
            $rs_kegiatan = MasterKegiatanPeer::doSelectOne($c);
            if ($rs_kegiatan) {
                $tahap = $rs_kegiatan->getTahap();
            }
            /*
              if($tahap=='')
              {
              $query = "delete from ". sfConfig::get('app_default_schema') .".rincian_detail
              where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and kode_sub='$kode_sub'";
              // print_r($query);exit;
              $con=Propel::getConnection(RincianDetailPeer::DATABASE_NAME);
              $statement=$con->prepareStatement($query);
              $statement->executeQuery();


              $query = "delete from ". sfConfig::get('app_default_schema') .".rincian_sub_parameter
              where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and kode_sub='$kode_sub'";
              // print_r($query);exit;
              $con=Propel::getConnection(RincianSubParameterPeer::DATABASE_NAME);
              $statement=$con->prepareStatement($query);
              $statement->executeQuery();
              }
              else if($tahap!='')
              {
              $query = "update ". sfConfig::get('app_default_schema') .".rincian_detail set volume=0,keterangan_koefisien='0'
              where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and kode_sub='$kode_sub'";
              $con=Propel::getConnection(RincianDetailPeer::DATABASE_NAME);
              $statement=$con->prepareStatement($query);
              $statement->executeQuery();

              $query = "update ". sfConfig::get('app_default_schema') .".rincian_detail set sub='',kode_sub=''
              where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and kode_sub='$kode_sub'";
              // print_r($query);exit;
              $con=Propel::getConnection(RincianDetailPeer::DATABASE_NAME);
              $statement=$con->prepareStatement($query);
              $statement->executeQuery();

              $query = "delete from ". sfConfig::get('app_default_schema') .".rincian_sub_parameter
              where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and kode_sub='$kode_sub'";
              // print_r($query);exit;
              $con=Propel::getConnection(RincianSubParameterPeer::DATABASE_NAME);
              $statement=$con->prepareStatement($query);
              $statement->executeQuery();

              $query = "delete from ". sfConfig::get('app_default_schema') .".rincian_sub_parameter
              where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and kode_sub='$kode_sub'";
              // print_r($query);exit;
              $con=Propel::getConnection(RincianSubParameterPeer::DATABASE_NAME);
              $statement=$con->prepareStatement($query);
              $statement->executeQuery();
              }
             */

            $volume = 0;
            $keterangan_koefisien = '0';
            $sub = '';
            $kode_sub = '';
            $kode_jasmas = '';
            $nilaiBaru = 0;
            $c = new Criteria();
            $c->add(RincianDetailPeer::UNIT_ID, $unit_id);
            $c->add(RincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
            $c->add(RincianDetailPeer::KODE_SUB, $kode_sub);
            $jumlah = RincianDetailPeer::doCount($c);
            if ($jumlah == 1) {
                $rincian_detail = RincianDetailPeer::doSelectOne($c);
                if ($rincian_detail) {
                    $pajak = $rincian_detail->getPajak();
                    $harga = $rincian_detail->getKomponenHargaAwal();
                    $nilaiBaru = round($harga * $volume * (100 + $pajak) / 100);
                    sfContext::getInstance()->getLogger()->debug('{eProject} rincian detail ketemu, nilai baru = ' . $nilaiBaru);
                    $rincian_detail->setKeteranganKoefisien($keterangan_koefisien);
                    $rincian_detail->setVolume($volume);
                    $rincian_detail->setDetailName($detail_name);
                    $rincian_detail->setSubtitle($subtitle);
                    $rincian_detail->setSub($sub);
                    $rincian_detail->setKodeSub($kode_sub);
                    $rincian_detail->setKecamatan($kode_jasmas);
                    //alamat host harap diubah dahulu
                    $host = 'pengendalian.surabaya2excellence.or.id/eproject2009';
                    $port = 8080;
                    $body = '';
                    $headers = '';
                    $tahun = sfConfig::get('app_sinkronisasi_tahun', date('Y'));
                    $tahun = substr($tahun, 2, 2); //ambil 2 digit terakhir;
                    $kode_detail_kegiatan = sprintf("%s.%s.%s.%s", $unit_id, $kegiatan_code, '09', $detail_no);
                    $url = 'http://pengendalian.surabaya2excellence.or.id/eproject2009/sinkronisasi/ubahDetailKegiatan.shtml?kode_detail_kegiatan=' . $kode_detail_kegiatan;
                    sfContext::getInstance()->getLogger()->debug('{eProject}akan mengambil data dari eproject ' . $kode_detail_kegiatan);
                    $cek_eproject = HttpHelper::httpGet($host, $port, $url, $body, $headers); //hasilnya bisa true bisa false
                    //hasil xml dari eproject masuk ke variabel body
                    $nilaiTerpakai = 0;
                    $httpOK = false;
                    if ($cek_eproject && ( strpos($body, '<?xml') !== false ) && ( strpos($body, 'nilai="') !== false )) {
                        $doc = new DomDocument("1.0");
                        $doc->loadXml($body);
                        $detail_kegiatan = $doc->documentElement; //root xml
                        $nilaiTerpakai = $detail_kegiatan->getAttribute('nilai');
                        $httpOK = true;
                    }
                    sfContext::getInstance()->getLogger()->debug('{eProject} rincian terpakai ketemu, nilai terpakai = ' . $nilaiTerpakai);
                    if ($nilaiBaru < $nilaiTerpakai) {
                        // $this->getRequest()->setError('nilai_terpakai','Mohon maaf , untuk Rincian Kegiatan ini sudah terpakai di pekerjaan, sejumlah '.number_format($nilaiTerpakai,0,',','.'));
                        //$this->getRequest()->setParameter('unit_id', $unit_id  );
                        //$this->getRequest()->setParameter('kode_kegiatan', $kegiatan_code  );
                        $this->setFlash('gagal', 'Mohon maaf , untuk Rincian Kegiatan ini sudah terpakai di pekerjaan, sejumlah ' . number_format($nilaiTerpakai, 0, ',', '.'));
                        return $this->redirect('peneliti/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
                    }


                    $rincian_detail->save();
                    $this->setFlash('berhasil', 'Perubahan Telah Berhasil Dilakukan');
                    return $this->redirect('peneliti/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
                } else {
                    sfContext::getInstance()->getLogger()->debug{'{eProject} rincian detail tidak ketemu, nilai baru = ' . $nilaiBaru};
                    $this->setFlash('gagal', 'Mohon maaf, rincian yang dicari tidak ditemukan dalam database');
                    return $this->redirect('peneliti/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
                }
            } elseif ($jumlah > 1) {
                $rincian_detail = RincianDetailPeer::doSelect($c);
                $gagal = 'salah';
                foreach ($rincian_detail as $rincian_details) {
                    $detail_no = $rincian_details->getDetailNo();
                    //cek paketan
                    $d = new Criteria();
                    $d->add(RincianDetailPeer::UNIT_ID, $unit_id);
                    $d->add(RincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
                    $d->add(RincianDetailPeer::DETAIL_NO, $detail_no);
                    $cari_rincian_detail = RincianDetailPeer::doSelectOne($d);
                    if ($cari_rincian_detail) {
                        $host = 'pengendalian.surabaya2excellence.or.id/eproject2009';
                        $port = 8080;
                        $body = '';
                        $headers = '';
                        $tahun = sfConfig::get('app_sinkronisasi_tahun', date('Y'));

                        $tahun = substr($tahun, 2, 2); //ambil 2 digit terakhir;
                        $kode_detail_kegiatan = sprintf("%s.%s.%s.%s", $unit_id, $kegiatan_code, '09', $detail_no);
                        $url = 'http://pengendalian.surabaya2excellence.or.id/eproject2009/sinkronisasi/ubahDetailKegiatan.shtml?kode_detail_kegiatan=' . $kode_detail_kegiatan;
                        sfContext::getInstance()->getLogger()->debug('{eProject}akan mengambil data dari eproject ' . $kode_detail_kegiatan);
                        $cek_eproject = HttpHelper::httpGet($host, $port, $url, $body, $headers); //hasilnya bisa true bisa false
                        //hasil xml dari eproject masuk ke variabel body
                        $nilaiTerpakai = 0;
                        $httpOK = false;
                        if ($cek_eproject && ( strpos($body, '<?xml') !== false ) && ( strpos($body, 'nilai="') !== false )) {
                            $doc = new DomDocument("1.0");
                            $doc->loadXml($body);
                            $detail_kegiatan = $doc->documentElement; //root xml
                            $nilaiTerpakai = $detail_kegiatan->getAttribute('nilai');
                            $httpOK = true;
                        }
                        sfContext::getInstance()->getLogger()->debug('{eProject} rincian terpakai ketemu, nilai terpakai = ' . $nilaiTerpakai);
                        if ($nilaiBaru < $nilaiTerpakai) {
                            $gagal = 'benar';
                        }
                    }
                }

                if ($gagal == 'benar') {
                    $this->setFlash('gagal', 'Mohon maaf , untuk Rincian ASB Non Fisik Kegiatan ini sudah terpakai di pekerjaan');
                    return $this->redirect('peneliti/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
                } elseif ($gagal == 'salah') {
                    $rincian_detail->setKeteranganKoefisien($keterangan_koefisien);
                    $rincian_detail->setVolume($volume);
                    $rincian_detail->setDetailName($detail_name);
                    $rincian_detail->setSubtitle($subtitle);
                    $rincian_detail->setSub($sub);
                    $rincian_detail->setKodeSub($kode_sub);
                    $rincian_detail->setKecamatan($kode_jasmas);
                    $rincian_detail->save();
                    //$this->setFlash('berhasil', 'Perubahan Telah Berhasil Dilakukan');
                    //return $this->redirect('peneliti/edit?unit_id='.$unit_id.'&kode_kegiatan='.$kegiatan_code);
                }
            } else {
                sfContext::getInstance()->getLogger()->debug{'{eProject} rincian detail tidak ketemu, nilai baru = ' . $nilaiBaru};
                $this->setFlash('gagal', 'Mohon maaf, rincian yang dicari tidak ditemukan dalam database');
                return $this->redirect('peneliti/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
            }

            $sub_id = $this->getRequestParameter('id');
            $c = new Criteria();
            $c->add(SubtitleIndikatorPeer::SUB_ID, $sub_id);
            $rs_subtitle = SubtitleIndikatorPeer::doSelectOne($c);
            if ($rs_subtitle) {
                $unit_id = $rs_subtitle->getUnitId();
                $kegiatan_code = $rs_subtitle->getKegiatanCode();
                $subtitle = $rs_subtitle->getSubtitle();
                $nama_subtitle = trim($subtitle);
            }

            $query = "select * 
					  from " . sfConfig::get('app_default_schema') . ".rincian_detail 
					  where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and subtitle ilike '$nama_subtitle' and volume>0 order by sub,rekening_code,komponen_name";
            $con = Propel::getConnection(RincianDetailPeer::DATABASE_NAME);
            $statement = $con->prepareStatement($query);
            $rs_rinciandetail = $statement->executeQuery();
            $this->rs_rinciandetail = $rs_rinciandetail;
            $this->id = $sub_id;
            $this->rinciandetail = 'ada';
            $this->setLayout('kosong');
        }
    }

    public function executeHapusPekerjaans() {
        if ($this->getRequestParameter('id')) {
            $detail_no = $this->getRequestParameter('no');
            $unit_id = $this->getRequestParameter('unit');
            $kegiatan_code = $this->getRequestParameter('kegiatan');
            $tahap = '';
            $c = new Criteria();
            $c->add(MasterKegiatanPeer::UNIT_ID, $unit_id);
            $c->add(MasterKegiatanPeer::KODE_KEGIATAN, $kegiatan_code);
            $rs_kegiatan = MasterKegiatanPeer::doSelectOne($c);
            if ($rs_kegiatan) {
                $tahap = $rs_kegiatan->getTahap();
            }
            /*
              if($tahap=='')
              {
              $query = "delete from ". sfConfig::get('app_default_schema') .".rincian_detail
              where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and detail_no=$detail_no";
              // print_r($query);exit;
              $con=Propel::getConnection(RincianDetailPeer::DATABASE_NAME);
              $statement=$con->prepareStatement($query);
              $statement->executeQuery();
              }
              else if($tahap!='')
              {
              $query = "update ". sfConfig::get('app_default_schema') .".rincian_detail set volume=0,keterangan_koefisien='0'
              where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and detail_no=$detail_no";
              $con=Propel::getConnection(RincianDetailPeer::DATABASE_NAME);
              $statement=$con->prepareStatement($query);
              $statement->executeQuery();
              }
             */
            $volume = 0;
            $keterangan_koefisien = '0';
            $sub = '';
            $kode_sub = '';
            $kode_jasmas = '';
            $c = new Criteria();
            $c->add(RincianDetailPeer::UNIT_ID, $unit_id);
            $c->add(RincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
            $c->add(RincianDetailPeer::DETAIL_NO, $detail_no);
            $rincian_detail = RincianDetailPeer::doSelectOne($c);
            if ($rincian_detail) {
                $pajak = $rincian_detail->getPajak();
                $harga = $rincian_detail->getKomponenHargaAwal();
                $nilaiBaru = round($harga * $volume * (100 + $pajak) / 100);
                sfContext::getInstance()->getLogger()->debug('{eProject} rincian detail ketemu, nilai baru = ' . $nilaiBaru);
                $rincian_detail->setKeteranganKoefisien($keterangan_koefisien);
                $rincian_detail->setVolume($volume);
                $rincian_detail->setDetailName($detail_name);
                $rincian_detail->setSubtitle($subtitle);
                $rincian_detail->setSub($sub);
                $rincian_detail->setKodeSub($kode_sub);
                $rincian_detail->setKecamatan($kode_jasmas);
                //alamat host harap diubah dahulu
                $host = 'pengendalian.surabaya2excellence.or.id/eproject2009';
                $port = 8080;
                $body = '';
                $headers = '';
                $tahun = sfConfig::get('app_sinkronisasi_tahun', date('Y'));
                $tahun = substr($tahun, 2, 2); //ambil 2 digit terakhir;
                $kode_detail_kegiatan = sprintf("%s.%s.%s.%s", $unit_id, $kegiatan_code, '09', $detail_no);
                $url = 'http://pengendalian.surabaya2excellence.or.id/eproject2009/sinkronisasi/ubahDetailKegiatan.shtml?kode_detail_kegiatan=' . $kode_detail_kegiatan;
                sfContext::getInstance()->getLogger()->debug('{eProject}akan mengambil data dari eproject ' . $kode_detail_kegiatan);
                $cek_eproject = HttpHelper::httpGet($host, $port, $url, $body, $headers); //hasilnya bisa true bisa false
                //hasil xml dari eproject masuk ke variabel body
                $nilaiTerpakai = 0;
                $httpOK = false;
                if ($cek_eproject && ( strpos($body, '<?xml') !== false ) && ( strpos($body, 'nilai="') !== false )) {
                    $doc = new DomDocument("1.0");
                    $doc->loadXml($body);
                    $detail_kegiatan = $doc->documentElement; //root xml
                    $nilaiTerpakai = $detail_kegiatan->getAttribute('nilai');
                    $httpOK = true;
                }
                sfContext::getInstance()->getLogger()->debug('{eProject} rincian terpakai ketemu, nilai terpakai = ' . $nilaiTerpakai);
                if ($nilaiBaru < $nilaiTerpakai) {
                    // $this->getRequest()->setError('nilai_terpakai','Mohon maaf , untuk Rincian Kegiatan ini sudah terpakai di pekerjaan, sejumlah '.number_format($nilaiTerpakai,0,',','.'));
                    //$this->getRequest()->setParameter('unit_id', $unit_id  );
                    //$this->getRequest()->setParameter('kode_kegiatan', $kegiatan_code  );
                    $this->setFlash('gagal', 'Mohon maaf , untuk Rincian Kegiatan ini sudah terpakai di pekerjaan, sejumlah ' . number_format($nilaiTerpakai, 0, ',', '.'));
                    return $this->redirect('peneliti/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
                }


                $rincian_detail->save();
                //$this->setFlash('berhasil', 'Perubahan Telah Berhasil Dilakukan');
                //return $this->redirect('peneliti/edit?unit_id='.$unit_id.'&kode_kegiatan='.$kegiatan_code);
            } else {
                sfContext::getInstance()->getLogger()->debug{'{eProject} rincian detail tidak ketemu, nilai baru = ' . $nilaiBaru};
                $this->setFlash('gagal', 'Mohon maaf, rincian yang dicari tidak ditemukan dalam database');
                return $this->redirect('peneliti/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
            }


            $sub_id = $this->getRequestParameter('id');
            $c = new Criteria();
            $c->add(SubtitleIndikatorPeer::SUB_ID, $sub_id);
            $rs_subtitle = SubtitleIndikatorPeer::doSelectOne($c);
            if ($rs_subtitle) {
                $unit_id = $rs_subtitle->getUnitId();
                $kegiatan_code = $rs_subtitle->getKegiatanCode();
                $subtitle = $rs_subtitle->getSubtitle();
                $nama_subtitle = trim($subtitle);
            }
            /*
              // query lama : volume diganti status hapus
              $query = "select *
              from ". sfConfig::get('app_default_schema') .".rincian_detail
              where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and subtitle ilike '$nama_subtitle' and volume>0 order by sub,rekening_code,komponen_name";

             */
            $query = "select *
				  from " . sfConfig::get('app_default_schema') . ".rincian_detail
				  where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and subtitle ilike '$nama_subtitle' and status_hapus=false order by sub,rekening_code,komponen_name";

            $con = Propel::getConnection(RincianDetailPeer::DATABASE_NAME);
            $statement = $con->prepareStatement($query);
            $rs_rinciandetail = $statement->executeQuery();
            $this->rs_rinciandetail = $rs_rinciandetail;
            $this->id = $sub_id;
            $this->rinciandetail = 'ada';
            $this->setLayout('kosong');
        }
    }

    public function executeBaruKegiatan() {
        if ($this->getRequestParameter('cari') == 'cari') {
            //print_r($this->getRequestParameter('lokasi'));exit;
            $user = $this->getUser();
            $user->removeCredential('lokasi');
            $user->addCredential('lokasi');
            $user->setAttribute('nama', $this->getRequestParameter('lokasi'), 'lokasi');
            $user->setAttribute('id', $this->getRequestParameter('id'), 'lokasi');
            $user->setAttribute('kegiatan', $this->getRequestParameter('kegiatan'), 'lokasi');
            $user->setAttribute('unit', $this->getRequestParameter('unit'), 'lokasi');
            $user->setAttribute('subtitle', $this->getRequestParameter('subtitle'), 'lokasi');
            $user->setAttribute('sub', $this->getRequestParameter('sub'), 'lokasi');
            $user->setAttribute('pajak', $this->getRequestParameter('pajak'), 'lokasi');
            $user->setAttribute('rekening', $this->getRequestParameter('rekening'), 'lokasi');
            $user->setAttribute('tipe', $this->getRequestParameter('tipe'), 'lokasi');
            $user->setAttribute('baru', 'baru', 'lokasi');

            return $this->forward('lokasi', 'list');
        }
        if ($this->getRequestParameter('simpan') == 'simpan') {
            $unit_id = $this->getRequestParameter('unit');
            $kegiatan_code = $this->getRequestParameter('kegiatan');
            $subtitle = $this->getRequestParameter('subtitle');
            //$sub = $this->getRequestParameter('sub');
            $tipe = $this->getRequestParameter('tipe');
            $rekening = $this->getRequestParameter('rekening');
            $pajak = $this->getRequestParameter('pajak');
            $komponen_id = $this->getRequestParameter('id');

            $user = $this->getUser();
            $dinas = $user->setAttribute('nama', '', 'peneliti');

            $c = new Criteria();
            $c->add(KomponenPeer::KOMPONEN_ID, $komponen_id);
            $rs_komponen = KomponenPeer::doSelectOne($c);
            if ($rs_komponen) {
                $komponen_harga = $rs_komponen->getKomponenHarga();
                $komponen_name = $rs_komponen->getKomponenName();
                $satuan = $rs_komponen->getSatuan();
            }
            $detail_no = 0;
            $query = "select max(detail_no) as nilai from " . sfConfig::get('app_default_schema') . ".rincian_detail where unit_id='$unit_id' and kegiatan_code='$kegiatan_code'";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs_max = $stmt->executeQuery();
            while ($rs_max->next()) {
                $detail_no = $rs_max->getString('nilai');
            }
            $detail_no+=1;
            $detail_name = '';
            $kode_jasmas = '';
            if ($tipe == 'FISIK') {
                $detail_name = $this->getRequestParameter('lokasi');
                $kode_jasmas = $this->getRequestParameter('jasmas');
            } else if ($tipe != 'FISIK') {
                $detail_name = $this->getRequestParameter('keterangan');
            }

            if (!$this->getRequestParameter('subtitle')) {
                if ($tipe == 'FISIK') {
                    $this->setFlash('subtitletidakada', 'Subtitle Belum Dipilih');
                    return $this->redirect("peneliti/buatbaru?lokasi=$detail_name&kegiatan=$kegiatan_code&subtitle=$subtitle&sub=$sub&rekening=$rekening&pajak=$pajak&unit=$unit_id&tipe=$tipe&komponen=$komponen_id&baru=" . md5('terbaru') . "&commit=Pilih");
                } else {
                    $this->setFlash('subtitletidakada', 'Subtitle Belum Dipilih');
                    return $this->redirect("peneliti/buatbaru?keterangan=$detail_name&kegiatan=$kegiatan_code&subtitle=$subtitle&sub=$sub&rekening=$rekening&pajak=$pajak&unit=$unit_id&tipe=$tipe&komponen=$komponen_id&baru=" . md5('terbaru') . "&commit=Pilih");
                }
            }


            $detail_name = '';
            $kode_sub = '';
            $sub = '';
            if ($this->getRequestParameter('lokasi')) {
                $kode_lokasi = '';
                $detail_name = $this->getRequestParameter('lokasi');
                $c = new Criteria();
                $c->add(VLokasiPeer::NAMA, $detail_name);
                $rs_lokasi = VLokasiPeer::doSelectOne($c);
                if ($rs_lokasi) {
                    $kode_lokasi = $rs_lokasi->getKode();
                }

                if ($kode_lokasi == '') {
                    $this->setFlash('lokasitidakada', 'Lokasi Tidak Ada atau Tidak Valid dengan Data G.I.S');
                    return $this->redirect("peneliti/buatbaru?lokasi=$detail_name&kegiatan=$kegiatan_code&subtitle=$subtitle&rekening=$rekening&pajak=$pajak&unit=$unit_id&tipe=$tipe&komponen=$komponen_id&baru=" . md5('terbaru') . "&commit=Pilih");
                }
            }

            if ($tipe == 'FISIK') {
                if (!$this->getRequestParameter('lokasi')) {
                    $this->setFlash('lokasitidakada', 'Untuk Komponen Fisik diharapkan Mengisi Data Lokasi');
                    return $this->redirect("peneliti/buatbaru?kegiatan=$kegiatan_code&subtitle=$subtitle&rekening=$rekening&pajak=$pajak&unit=$unit_id&tipe=$tipe&komponen=$komponen_id&baru=" . md5('terbaru') . "&commit=Pilih");
                }
            }
            if ($this->getRequestParameter('keterangan')) {
                $detail_name = $this->getRequestParameter('keterangan');
            }
            if ($this->getRequestParameter('sub')) {
                $kode_sub = $this->getRequestParameter('sub');
                $d = new Criteria();
                $d->add(RincianSubParameterPeer::KODE_SUB, $kode_sub);
                $d->add(RincianSubParameterPeer::KEGIATAN_CODE, $kegiatan_code);
                $d->add(RincianSubParameterPeer::UNIT_ID, $unit_id);
                $rs_rinciansubparameter = RincianSubParameterPeer::doSelectOne($d);
                if ($rs_rinciansubparameter) {
                    $sub = $rs_rinciansubparameter->getNewSubtitle();
                    $kodesub = $rs_rinciansubparameter->getKodeSub();
                }
            }

            $kode_subtitle = $this->getRequestParameter('subtitle');

            $c = new Criteria();
            $c->add(SubtitleIndikatorPeer::SUB_ID, $kode_subtitle);
            $rs_subtitle = SubtitleIndikatorPeer::doSelectOne($c);
            if ($rs_subtitle) {
                $subtitle = $rs_subtitle->getSubtitle();
            }

            $volume = 0;
            $keterangan_koefisien = '';

            if ($this->getRequestParameter('vol1') || $this->getRequestParameter('vol2') || $this->getRequestParameter('vol3') || $this->getRequestParameter('vol4')) {
                if ($this->getRequestParameter('vol2') == '') {
                    $vol2 = 1;
                    $volume = $this->getRequestParameter('vol1') * $vol2;
                    $keterangan_koefisien = $this->getRequestParameter('vol1') . ' ' . $this->getRequestParameter('volume1');
                } else if (!$this->getRequestParameter('vol2') == '') {
                    $volume = $this->getRequestParameter('vol1') * $this->getRequestParameter('vol2');
                    $keterangan_koefisien = $this->getRequestParameter('vol1') . ' ' . $this->getRequestParameter('volume1') . ' X ' . $this->getRequestParameter('vol2') . ' ' . $this->getRequestParameter('volume2');
                }
                if ($this->getRequestParameter('vol3') == '') {
                    $vol3 = 1;
                    $volume = $volume * $vol3;
                } else if (!$this->getRequestParameter('vol3') == '') {
                    $volume = $this->getRequestParameter('vol1') * $this->getRequestParameter('vol2') * $this->getRequestParameter('vol3');
                    $keterangan_koefisien = $this->getRequestParameter('vol1') . ' ' . $this->getRequestParameter('volume1') . ' X ' . $this->getRequestParameter('vol2') . ' ' . $this->getRequestParameter('volume2') . ' X ' . $this->getRequestParameter('vol3') . ' ' . $this->getRequestParameter('volume3');
                }
                if ($this->getRequestParameter('vol4') == '') {
                    $vol4 = 1;
                    $volume = $volume * $vol4;
                } else if (!$this->getRequestParameter('vol4') == '') {
                    $volume = $this->getRequestParameter('vol1') * $this->getRequestParameter('vol2') * $this->getRequestParameter('vol3') * $this->getRequestParameter('vol4');
                    $keterangan_koefisien = $this->getRequestParameter('vol1') . ' ' . $this->getRequestParameter('volume1') . ' X ' . $this->getRequestParameter('vol2') . ' ' . $this->getRequestParameter('volume2') . ' X ' . $this->getRequestParameter('vol3') . ' ' . $this->getRequestParameter('volume3') . ' X ' . $this->getRequestParameter('vol4') . ' ' . $this->getRequestParameter('volume4');
                }
            }

            if ($this->getRequestParameter('status') == 'pending') {
                $detail_no = 0;
                $query = "select max(detail_no) as nilai from " . sfConfig::get('app_default_schema') . ".rincian_detail_masalah where unit_id='$unit_id' and kegiatan_code='$kegiatan_code'";
                $con = Propel::getConnection();
                $stmt = $con->prepareStatement($query);
                $rs_max = $stmt->executeQuery();
                while ($rs_max->next()) {
                    $detail_no = $rs_max->getString('nilai');
                }
                $detail_no+=1;
                $query = "insert into " . sfConfig::get('app_default_schema') . ".rincian_detail_masalah (kegiatan_code, tipe, detail_no, rekening_code, komponen_id, detail_name, volume, keterangan_koefisien, subtitle, komponen_harga, komponen_harga_awal, komponen_name, satuan, pajak, unit_id, kode_sub, sub, kecamatan, last_update_user, last_update_time) values ('" . $kegiatan_code . "', '" . $tipe . "', " . $detail_no . ", '" . $rekening . "', '" . $komponen_id . "', '" . $detail_name . "', " . $volume . ", '" . $keterangan_koefisien . "', '" . $subtitle . "', " . $komponen_harga . ", " . $komponen_harga . ",'" . $komponen_name . "', '" . $satuan . "', " . $pajak . ",'" . $unit_id . "','" . $kodesub . "', '" . $sub . "', '" . $kode_jasmas . "', '" . $dinas . "', now())";

                $con = Propel::getConnection();
                $stmt = $con->prepareStatement($query);
                $stmt->executeQuery();
                $this->setFlash('berhasil', 'Penyimpanan Telah Berhasil, Tetapi Tidak Masuk Ke RKA, Harap Hubungi Penyelia Anda');
                return $this->redirect('peneliti/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
            } else if ($this->getRequestParameter('status') == 'ok') {
                $query = "insert into " . sfConfig::get('app_default_schema') . ".rincian_detail (kegiatan_code, tipe, detail_no, rekening_code, komponen_id, detail_name, volume, keterangan_koefisien, subtitle, komponen_harga, komponen_harga_awal, komponen_name, satuan, pajak, unit_id, kode_sub, sub, kecamatan, last_update_user, last_update_time) values ('" . $kegiatan_code . "', '" . $tipe . "', " . $detail_no . ", '" . $rekening . "', '" . $komponen_id . "', '" . $detail_name . "', " . $volume . ", '" . $keterangan_koefisien . "', '" . $subtitle . "', " . $komponen_harga . ", " . $komponen_harga . ",'" . $komponen_name . "', '" . $satuan . "', " . $pajak . ",'" . $unit_id . "','" . $kodesub . "', '" . $sub . "', '" . $kode_jasmas . "', '" . $dinas . "', now())";

                $con = Propel::getConnection();
                $stmt = $con->prepareStatement($query);
                $stmt->executeQuery();
                $this->setFlash('berhasil', 'Komponen Telah Berhasil Disimpan');
                return $this->redirect('peneliti/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
            } else {
                $query = "insert into " . sfConfig::get('app_default_schema') . ".rincian_detail (kegiatan_code, tipe, detail_no, rekening_code, komponen_id, detail_name, volume, keterangan_koefisien, subtitle, komponen_harga, komponen_harga_awal, komponen_name, satuan, pajak, unit_id, kode_sub, sub, kecamatan, last_update_user, last_update_time) values ('" . $kegiatan_code . "', '" . $tipe . "', " . $detail_no . ", '" . $rekening . "', '" . $komponen_id . "', '" . $detail_name . "', " . $volume . ", '" . $keterangan_koefisien . "', '" . $subtitle . "', " . $komponen_harga . ", " . $komponen_harga . ",'" . $komponen_name . "', '" . $satuan . "', " . $pajak . ",'" . $unit_id . "','" . $kodesub . "', '" . $sub . "', '" . $kode_jasmas . "', '" . $dinas . "', now())";

                $con = Propel::getConnection();
                $stmt = $con->prepareStatement($query);
                $stmt->executeQuery();
                $this->setFlash('berhasil', 'Komponen Telah Berhasil Disimpan');
                return $this->redirect('peneliti/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
            }
            return $this->redirect('peneliti/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
        }
    }

    public function executeBuatbaru() {
        if ($this->getRequestParameter('baru') == md5('terbaru')) {
            $user = $this->getUser();
            $user->removeCredential('lokasi');

            $kode_kegiatan = $this->getRequestParameter('kegiatan');
            $unit_id = $this->getRequestParameter('unit');
            $pajak = $this->getRequestParameter('pajak');
            $komponen_id = $this->getRequestParameter('komponen');
            //print_r($komponen_id);exit;
            $tipe = $this->getRequestParameter('tipe');
            $kode_rekening = $this->getRequestParameter('rekening');

            $komponen_id = trim($komponen_id);
            $c = new Criteria();
            $c->add(KomponenPeer::KOMPONEN_ID, $komponen_id, Criteria::ILIKE);
            $rs_komponen = KomponenPeer::doSelectOne($c);
            if ($rs_komponen) {
                $this->rs_komponen = $rs_komponen;
            }

            $d = new Criteria();
            $d->add(SubtitleIndikatorPeer::UNIT_ID, $unit_id);
            $d->add(SubtitleIndikatorPeer::KEGIATAN_CODE, $kode_kegiatan);
            $d->addAscendingOrderByColumn(SubtitleIndikatorPeer::SUBTITLE);
            $rs_subtitleindikator = SubtitleIndikatorPeer::doSelect($d);
            $this->rs_subtitleindikator = $rs_subtitleindikator;

            $e = new Criteria();
            $e->addAscendingOrderByColumn(SatuanPeer::SATUAN_NAME);
            $rs_satuan = SatuanPeer::doSelect($e);
            $this->rs_satuan = $rs_satuan;

            $f = new Criteria();
            $f->addAscendingOrderByColumn(JasmasPeer::NAMA);
            $rs_jasmas = JasmasPeer::doSelect($f);
            $this->rs_jasmas = $rs_jasmas;
        }
    }

    public function executeCarikomponen() {
        $user = $this->getUser();
        $user->removeCredential('lokasi');
        $kegiatan_code = $this->getRequestParameter('kegiatan');
        $unit_id = $this->getRequestParameter('unit');
        $status = 'LOCK';
        $c = new Criteria();
        $c->add(RincianPeer::KEGIATAN_CODE, $kegiatan_code);
        $c->add(RincianPeer::UNIT_ID, $unit_id);
        $rs_rinciankegiatan = RincianPeer::doSelectOne($c);
        $v = RincianPeer::doSelectOne($c);
        if ($v) {
            if ($v->getRincianLevel() == 1) {
                $status = 'LOCK';
            } else if ($v->getRincianLevel() == 2) {
                $status = 'LOCK';
            } else if ($v->getRincianLevel() == 3) {
                $status = 'OPEN';
            }
        }
        if ($status == 'OPEN') {
            $this->filters = $this->getRequestParameter('filters');
            if (isset($this->filters['nama_komponen']) && $this->filters['nama_komponen'] !== '') {
                $cari = $this->filters['nama_komponen'];
            }
            $this->cari = $cari;
        } else {
            $this->setFlash('berhasil', 'Anda Tidak Berhak Memasuki Halaman Sebelumnya');
            $this->redirect("peneliti/edit?kode_kegiatan=$kode_kegiatan&unit_id=$unit_id");
        }
    }

    public function executePilihsubx() {
        $x = new Criteria();
        $x->addSelectColumn(RincianDetailPeer::SUBTITLE);
        $x->add(RincianDetailPeer::KEGIATAN_CODE, $this->getRequestParameter('kegiatan_code'));
        $x->add(RincianDetailPeer::UNIT_ID, $this->getRequestParameter('unit_id'));
        if ($this->getRequestParameter('b') != '') {
            $s = new Criteria();
            $s->add(RincianDetailPeer::KEGIATAN_CODE, $this->getRequestParameter('kegiatan_code'));
            $s->add(RincianDetailPeer::UNIT_ID, $this->getRequestParameter('unit_id'));
            $s->setDistinct();
            $r = RincianDetailPeer::doselect($s);
            $kodekegiatan = $this->getRequestParameter('kegiatan_code');
            $unitid = $this->getRequestParameter('unit_id');
            $kode_sub = $this->getRequestParameter('b');

            $c = new Criteria();
            $c->add(SubtitleIndikatorPeer::SUB_ID, $kode_sub);
            $rs_subtitle = SubtitleIndikatorPeer::doSelectOne($c);
            if ($rs_subtitle) {
                $subtitle = $rs_subtitle->getSubtitle();
            }

            $queryku = "select distinct kode_sub,sub from " . sfConfig::get('app_default_schema') . ".rincian_detail where subtitle ilike '" . $subtitle . "%' and kegiatan_code='$kodekegiatan' and unit_id='$unitid'";



            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($queryku);
            $rcsb = $stmt->executeQuery();

            $arr_tampung = array();

            $this->isi_baru = array();
            while ($rcsb->next()) {
                if ($rcsb->getString('sub') != '') {
                    $arr_tampung[$rcsb->getString('kode_sub')] = $rcsb->getString('sub');
                }
            }
            $this->isi_baru = $arr_tampung;
        }
    }

    public function executeEditKegiatan() {
        //print_r($this->getRequest());exit;
        $user = $this->getUser();
        $user->removeCredential('lokasi');
        if ($this->getRequestParameter('edit') == md5('ubah')) {
            $unit_id = $this->getRequestParameter('unit');
            $kegiatan_code = $this->getRequestParameter('kegiatan');
            $detail_no = $this->getRequestParameter('id');

            $c = new Criteria();
            $c->add(RincianDetailPeer::UNIT_ID, $unit_id);
            $c->add(RincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
            $c->add(RincianDetailPeer::DETAIL_NO, $detail_no);
            $rs_rinciandetail = RincianDetailPeer::doSelectOne($c);
            if ($rs_rinciandetail) {
                $this->rs_rinciandetail = $rs_rinciandetail;
            }

            $d = new Criteria();
            $d->add(SubtitleIndikatorPeer::UNIT_ID, $unit_id);
            $d->add(SubtitleIndikatorPeer::KEGIATAN_CODE, $kegiatan_code);
            $d->addAscendingOrderByColumn(SubtitleIndikatorPeer::SUBTITLE);
            $rs_subtitleindikator = SubtitleIndikatorPeer::doSelect($d);
            $this->rs_subtitleindikator = $rs_subtitleindikator;

            $e = new Criteria();
            $e->addAscendingOrderByColumn(SatuanPeer::SATUAN_NAME);
            $rs_satuan = SatuanPeer::doSelect($e);
            $this->rs_satuan = $rs_satuan;

            $f = new Criteria();
            $f->addAscendingOrderByColumn(JasmasPeer::NAMA);
            $rs_jasmas = JasmasPeer::doSelect($f);
            $this->rs_jasmas = $rs_jasmas;
        }
        if ($this->getRequestParameter('cari') == 'cari') {
            //print_r($this->getRequestParameter('lokasi'));exit;
            $user = $this->getUser();
            $user->removeCredential('lokasi');
            $user->addCredential('lokasi');
            $user->setAttribute('nama', $this->getRequestParameter('lokasi'), 'lokasi');
            $user->setAttribute('kegiatan', $this->getRequestParameter('kegiatan'), 'lokasi');
            $user->setAttribute('unit', $this->getRequestParameter('unit'), 'lokasi');
            $user->setAttribute('id', $this->getRequestParameter('id'), 'lokasi');
            $user->setAttribute('ubah', 'ubah', 'lokasi');

            return $this->forward('lokasi', 'list');
        }
        if ($this->getRequestParameter('simpan') == 'simpan') {
            $unit_id = $this->getRequestParameter('unit');
            $kegiatan_code = $this->getRequestParameter('kegiatan');
            $detail_no = $this->getRequestParameter('id');

            /*
              if($this->getRequestParameter('status')=='pending')
              {
              $this->setFlash('gagal', 'Perubahan Tidak Berhasil Dilakukan, Sudah Ada Lokasi yang Sama di Tahun yang Sama');
              return $this->redirect('peneliti/edit?unit_id='.$unit_id.'&kode_kegiatan='.$kegiatan_code);
              }
             */
            if (!$this->getRequestParameter('subtitle')) {
                if ($tipe == 'FISIK') {
                    $this->setFlash('subtitletidakada', 'Subtitle Belum Dipilih');
                    return $this->redirect("peneliti/editKegiatan?id=$detail_no&unit=$unit_id&kegiatan=$kegiatan_code&edit=" . md5('ubah'));
                } else {
                    $this->setFlash('subtitletidakada', 'Subtitle Belum Dipilih');
                    return $this->redirect("peneliti/editKegiatan?id=$detail_no&unit=$unit_id&kegiatan=$kegiatan_code&edit=" . md5('ubah'));
                }
            }


            $detail_name = '';
            $kode_sub = '';
            $sub = '';
            $kode_jasmas = '';
            if ($this->getRequestParameter('lokasi')) {
                $kode_lokasi = '';
                $detail_name = $this->getRequestParameter('lokasi');
                $kode_jasmas = $this->getRequestParameter('jasmas');
                $c = new Criteria();
                $c->add(VLokasiPeer::NAMA, $detail_name);
                $rs_lokasi = VLokasiPeer::doSelectOne($c);
                if ($rs_lokasi) {
                    $kode_lokasi = $rs_lokasi->getKode();
                }

                if ($kode_lokasi == '') {
                    $this->setFlash('lokasitidakada', 'Lokasi Tidak Ada atau Tidak Valid dengan Data G.I.S');
                    return $this->redirect("peneliti/editKegiatan?id=$detail_no&unit=$unit_id&kegiatan=$kegiatan_code&edit=" . md5('ubah'));
                }
            }
            if ($this->getRequestParameter('keterangan')) {
                $detail_name = $this->getRequestParameter('keterangan');
            }
            if ($this->getRequestParameter('sub')) {
                $kode_sub = $this->getRequestParameter('sub');
                //print_r($kode_sub);exit;
                $d = new Criteria();
                $d->add(RincianSubParameterPeer::KODE_SUB, $kode_sub);
                $rs_rinciansubparameter = RincianSubParameterPeer::doSelectOne($d);
                if ($rs_rinciansubparameter) {
                    $sub = $rs_rinciansubparameter->getNewSubtitle();
                }
            }
            $kode_subtitle = $this->getRequestParameter('subtitle');

            $c = new Criteria();
            $c->add(SubtitleIndikatorPeer::SUB_ID, $kode_subtitle);
            $rs_subtitle = SubtitleIndikatorPeer::doSelectOne($c);
            if ($rs_subtitle) {
                $subtitle = $rs_subtitle->getSubtitle();
            }

            $volume = 0;
            $keterangan_koefisien = '';

            if ($this->getRequestParameter('vol1') || $this->getRequestParameter('vol2') || $this->getRequestParameter('vol3') || $this->getRequestParameter('vol4')) {
                if ($this->getRequestParameter('vol2') == '') {
                    $vol2 = 1;
                    $volume = $this->getRequestParameter('vol1') * $vol2;
                    $keterangan_koefisien = $this->getRequestParameter('vol1') . ' ' . $this->getRequestParameter('volume1');
                } else if (!$this->getRequestParameter('vol2') == '') {
                    $volume = $this->getRequestParameter('vol1') * $this->getRequestParameter('vol2');
                    $keterangan_koefisien = $this->getRequestParameter('vol1') . ' ' . $this->getRequestParameter('volume1') . ' X ' . $this->getRequestParameter('vol2') . ' ' . $this->getRequestParameter('volume2');
                }
                if ($this->getRequestParameter('vol3') == '') {
                    $vol3 = 1;
                    $volume = $volume * $vol3;
                } else if (!$this->getRequestParameter('vol3') == '') {
                    $volume = $this->getRequestParameter('vol1') * $this->getRequestParameter('vol2') * $this->getRequestParameter('vol3');
                    $keterangan_koefisien = $this->getRequestParameter('vol1') . ' ' . $this->getRequestParameter('volume1') . ' X ' . $this->getRequestParameter('vol2') . ' ' . $this->getRequestParameter('volume2') . ' X ' . $this->getRequestParameter('vol3') . ' ' . $this->getRequestParameter('volume3');
                }
                if ($this->getRequestParameter('vol4') == '') {
                    $vol4 = 1;
                    $volume = $volume * $vol4;
                } else if (!$this->getRequestParameter('vol4') == '') {
                    $volume = $this->getRequestParameter('vol1') * $this->getRequestParameter('vol2') * $this->getRequestParameter('vol3') * $this->getRequestParameter('vol4');
                    $keterangan_koefisien = $this->getRequestParameter('vol1') . ' ' . $this->getRequestParameter('volume1') . ' X ' . $this->getRequestParameter('vol2') . ' ' . $this->getRequestParameter('volume2') . ' X ' . $this->getRequestParameter('vol3') . ' ' . $this->getRequestParameter('volume3') . ' X ' . $this->getRequestParameter('vol4') . ' ' . $this->getRequestParameter('volume4');
                }
            }

            $c = new Criteria();
            $c->add(RincianDetailPeer::UNIT_ID, $unit_id);
            $c->add(RincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
            $c->add(RincianDetailPeer::DETAIL_NO, $detail_no);
            $rincian_detail = RincianDetailPeer::doSelectOne($c);
            if ($rincian_detail) {
                $pajak = $rincian_detail->getPajak();
                $harga = $rincian_detail->getKomponenHargaAwal();
                $nilaiBaru = round($harga * $volume * (100 + $pajak) / 100);
                sfContext::getInstance()->getLogger()->debug('{eProject} rincian detail ketemu, nilai baru = ' . $nilaiBaru);
                $rincian_detail->setKeteranganKoefisien($keterangan_koefisien);
                $rincian_detail->setVolume($volume);
                $rincian_detail->setDetailName($detail_name);
                $rincian_detail->setSubtitle($subtitle);
                $rincian_detail->setSub($sub);
                $rincian_detail->setKodeSub($kode_sub);
                $rincian_detail->setKecamatan($kode_jasmas);
                //alamat host harap diubah dahulu
                $host = 'pengendalian.surabaya2excellence.or.id/eproject2009';
                $port = 8080;
                $body = '';
                $headers = '';
                $tahun = sfConfig::get('app_sinkronisasi_tahun', date('Y'));
                $tahun = substr($tahun, 2, 2); //ambil 2 digit terakhir;
                $kode_detail_kegiatan = sprintf("%s.%s.%s.%s", $unit_id, $kegiatan_code, '09', $detail_no);
                $url = 'http://pengendalian.surabaya2excellence.or.id/eproject2009/sinkronisasi/ubahDetailKegiatan.shtml?kode_detail_kegiatan=' . $kode_detail_kegiatan;
                sfContext::getInstance()->getLogger()->debug('{eProject}akan mengambil data dari eproject ' . $kode_detail_kegiatan);
                $cek_eproject = HttpHelper::httpGet($host, $port, $url, $body, $headers); //hasilnya bisa true bisa false
                //hasil xml dari eproject masuk ke variabel body
                $nilaiTerpakai = 0;
                $httpOK = false;
                if ($cek_eproject && ( strpos($body, '<?xml') !== false ) && ( strpos($body, 'nilai="') !== false )) {
                    $doc = new DomDocument("1.0");
                    $doc->loadXml($body);
                    $detail_kegiatan = $doc->documentElement; //root xml
                    $nilaiTerpakai = $detail_kegiatan->getAttribute('nilai');
                    $httpOK = true;
                }
                sfContext::getInstance()->getLogger()->debug('{eProject} rincian terpakai ketemu, nilai terpakai = ' . $nilaiTerpakai);
                if ($nilaiBaru < $nilaiTerpakai) {
                    // $this->getRequest()->setError('nilai_terpakai','Mohon maaf , untuk Rincian Kegiatan ini sudah terpakai di pekerjaan, sejumlah '.number_format($nilaiTerpakai,0,',','.'));
                    //$this->getRequest()->setParameter('unit_id', $unit_id  );
                    //$this->getRequest()->setParameter('kode_kegiatan', $kegiatan_code  );
                    $this->setFlash('gagal', 'Mohon maaf , untuk Rincian Kegiatan ini sudah terpakai di pekerjaan, sejumlah ' . number_format($nilaiTerpakai, 0, ',', '.'));
                    return $this->redirect('peneliti/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
                }


                $rincian_detail->save();
                //$this->setFlash('berhasil', 'Perubahan Telah Berhasil Dilakukan');
                //return $this->redirect('peneliti/edit?unit_id='.$unit_id.'&kode_kegiatan='.$kegiatan_code);
            } else {
                sfContext::getInstance()->getLogger()->debug{'{eProject} rincian detail tidak ketemu, nilai baru = ' . $nilaiBaru};
                $this->setFlash('gagal', 'Mohon maaf, rincian yang dicari tidak ditemukan dalam database');
                return $this->redirect('peneliti/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
            }
            if ($this->getRequestParameter('status') == 'pending') {
                $query = "update " . sfConfig::get('app_default_schema') . ".rincian_detail set keterangan_koefisien='$keterangan_koefisien', volume=$volume, detail_name='$detail_name', subtitle='$subtitle', sub='$sub',kode_sub='$kode_sub',kecamatan='$kode_jasmas' where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and detail_no=$detail_no";

                $con = Propel::getConnection();
                $stmt = $con->prepareStatement($query);
                $stmt->executeQuery();

                $query = "select max(detail_no) as urut from " . sfConfig::get('app_default_schema') . ".rincian_detail_masalah where unit_id='$unit_id' and kegiatan_code='$kegiatan_code'";

                $con = Propel::getConnection();
                $stmt = $con->prepareStatement($query);
                $rs = $stmt->executeQuery();
                while ($rs->next()) {
                    $detail = $rs->getString('urut');
                }
                $detail+=1;

                $query = "insert into " . sfConfig::get('app_default_schema') . ".rincian_detail_masalah select * from " . sfConfig::get('app_default_schema') . ".rincian_detail where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and detail_no = $detail_no";
                $con = Propel::getConnection();
                $stmt = $con->prepareStatement($query);
                $stmt->executeQuery();

                $query = "update " . sfConfig::get('app_default_schema') . ".rincian_detail_masalah set detail_no=$detail where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and detail_no=$detail_no";

                $con = Propel::getConnection();
                $stmt = $con->prepareStatement($query);
                $stmt->executeQuery();

                $query = "update " . sfConfig::get('app_default_schema') . ".rincian_detail set keterangan_koefisien='0', volume=0, detail_name='$detail_name', subtitle='$subtitle', sub='$sub',kode_sub='$kode_sub' where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and detail_no=$detail_no";

                $con = Propel::getConnection();
                $stmt = $con->prepareStatement($query);
                $stmt->executeQuery();
                $this->setFlash('berhasil', 'Perubahan yang dilakukan menyebabkan usulan Tidak Masuk ke RKA, Harap Hubungi Penyelia Anda');
            } else {
                $query = "update " . sfConfig::get('app_default_schema') . ".rincian_detail set keterangan_koefisien='$keterangan_koefisien', volume=$volume, detail_name='$detail_name', subtitle='$subtitle', sub='$sub',kode_sub='$kode_sub',kecamatan='$kode_jasmas' where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and detail_no=$detail_no";

                $con = Propel::getConnection();
                $stmt = $con->prepareStatement($query);
                $stmt->executeQuery();

                $this->setFlash('berhasil', 'Perubahan Telah Berhasil Dilakukan');
            }
            return $this->redirect('peneliti/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
        }
    }

    public function executeGetPekerjaans() {
        if ($this->getRequestParameter('id')) {
            $sub_id = $this->getRequestParameter('id');
            $c = new Criteria();
            $c->add(SubtitleIndikatorPeer::SUB_ID, $sub_id);
            $rs_subtitle = SubtitleIndikatorPeer::doSelectOne($c);
            if ($rs_subtitle) {
                $unit_id = $rs_subtitle->getUnitId();
                $kegiatan_code = $rs_subtitle->getKegiatanCode();
                $subtitle = $rs_subtitle->getSubtitle();
                $nama_subtitle = trim($subtitle);
            }
            /*
              $query = "select *
              from ". sfConfig::get('app_default_schema') .".rincian_detail
              where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and subtitle ilike '$nama_subtitle' and volume>0 order by sub,rekening_code,komponen_name";

             */
            $query = "select *
                                  from " . sfConfig::get('app_default_schema') . ".rincian_detail
				  where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and subtitle ilike '$nama_subtitle' and status_hapus=false order by sub,rekening_code,komponen_name";

            $con = Propel::getConnection(RincianDetailPeer::DATABASE_NAME);
            $statement = $con->prepareStatement($query);
            $rs_rinciandetail = $statement->executeQuery();
            $this->rs_rinciandetail = $rs_rinciandetail;
            $this->id = $sub_id;
            $this->rinciandetail = 'ada';
            $this->setLayout('kosong');
        }
    }

    public function executeGetPekerjaansMasalah() {
        if ($this->getRequestParameter('id') == '0') {
            $sub_id = $this->getRequestParameter('id');
            $kegiatan_code = $this->getRequestParameter('kegiatan');
            $unit_id = $this->getRequestParameter('unit');

            $query = "select * 
					  from " . sfConfig::get('app_default_schema') . ".rincian_detail_masalah
					  where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' order by sub,kode_sub,rekening_code,komponen_name";
            //print($query);exit;
            $con = Propel::getConnection();
            $statement = $con->prepareStatement($query);
            $rs_rinciandetail = $statement->executeQuery();

            $this->rs_rinciandetail = $rs_rinciandetail;
            $this->id = $sub_id;
            $this->rinciandetail = 'ada';
            $this->setLayout('kosong');
        }
    }

    public function executeEdit() {
        if ($this->getRequestParameter('unit_id')) {
            $unit_id = $this->getRequestParameter('unit_id');
            $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');

            $c = new Criteria();
            $c->add(SubtitleIndikatorPeer::UNIT_ID, $unit_id);
            $c->add(SubtitleIndikatorPeer::KEGIATAN_CODE, $kode_kegiatan);
            $c->addAscendingOrderByColumn(SubtitleIndikatorPeer::SUBTITLE);
            $rs_subtitle = SubtitleIndikatorPeer::doSelect($c);

            $this->rs_subtitle = $rs_subtitle;
            $this->rinciandetail = '';
            $this->rs_rinciandetail = '';

            //untuk warning
            $query = "select sum(komponen_harga_awal*volume*(pajak+100)/100) as tot
					  from " . sfConfig::get('app_default_schema') . ".rincian_detail 
					  where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rekening_code like '5.2.1%'";
            $con = Propel::getConnection();
            $statement = $con->prepareStatement($query);
            $rs = $statement->executeQuery();
            while ($rs->next())
                $total_menjadi_1 = $rs->getString('tot');

            $query = "select sum(komponen_harga_awal*volume*(pajak+100)/100) as tot
					  from " . sfConfig::get('app_default_schema') . ".rincian_detail 
					  where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rekening_code like '5.2.2%'";
            $con = Propel::getConnection();
            $statement = $con->prepareStatement($query);
            $rs = $statement->executeQuery();
            while ($rs->next())
                $total_menjadi_2 = $rs->getString('tot');

            $query = "select sum(komponen_harga_awal*volume*(pajak+100)/100) as tot
					  from " . sfConfig::get('app_default_schema') . ".rincian_detail 
					  where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rekening_code like '5.2.3%'";
            $con = Propel::getConnection();
            $statement = $con->prepareStatement($query);
            $rs = $statement->executeQuery();
            while ($rs->next())
                $total_menjadi_3 = $rs->getString('tot');

            $query = "select sum(komponen_harga_awal*volume*(pajak+100)/100) as tot
					  from " . sfConfig::get('app_default_schema') . ".rincian_detail_bp
					  where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rekening_code like '5.2.1%'";
            $con = Propel::getConnection();
            $statement = $con->prepareStatement($query);
            $rs = $statement->executeQuery();
            while ($rs->next())
                $total_semula_1 = $rs->getString('tot');

            $query = "select sum(komponen_harga_awal*volume*(pajak+100)/100) as tot
					  from " . sfConfig::get('app_default_schema') . ".rincian_detail_bp
					  where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rekening_code like '5.2.2%'";
            $con = Propel::getConnection();
            $statement = $con->prepareStatement($query);
            $rs = $statement->executeQuery();
            while ($rs->next())
                $total_semula_2 = $rs->getString('tot');

            $query = "select sum(komponen_harga_awal*volume*(pajak+100)/100) as tot
					  from " . sfConfig::get('app_default_schema') . ".rincian_detail_bp
					  where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rekening_code like '5.2.3%'";
            $con = Propel::getConnection();
            $statement = $con->prepareStatement($query);
            $rs = $statement->executeQuery();
            while ($rs->next())
                $total_semula_3 = $rs->getString('tot');

            $this->selisih_1 = number_format($total_menjadi_1 - $total_semula_1, 0, ',', '.');
            $this->selisih_2 = number_format($total_menjadi_2 - $total_semula_2, 0, ',', '.');
            $this->selisih_3 = number_format($total_menjadi_3 - $total_semula_3, 0, ',', '.');

            $this->total_menjadi_1 = number_format($total_menjadi_1, 0, ',', '.');
            $this->total_menjadi_2 = number_format($total_menjadi_2, 0, ',', '.');
            $this->total_menjadi_3 = number_format($total_menjadi_3, 0, ',', '.');

            $this->total_semula_1 = number_format($total_semula_1, 0, ',', '.');
            $this->total_semula_2 = number_format($total_semula_2, 0, ',', '.');
            $this->total_semula_3 = number_format($total_semula_3, 0, ',', '.');
        }
    }

    public function executeTarikrka() {
        if ($this->getRequestParameter('tarik') == md5('project')) {
            $unit_id = $this->getRequestParameter('unit_id');
            $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');

            //------------------------------------------------------------------------------------------------------------------

            $con = Propel::getConnection();
            $con->begin();
            try {
                $sql = "delete from " . sfConfig::get('app_default_schema') . ".master_kegiatan_bp where unit_id='" . $unit_id . "' and kode_kegiatan='$kode_kegiatan'";

                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();

                $sql = "delete from " . sfConfig::get('app_default_schema') . ".subtitle_indikator_bp where unit_id='" . $unit_id . "' and kegiatan_code='$kode_kegiatan'";

                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();

                $sql = "delete from " . sfConfig::get('app_default_schema') . ".rincian_detail_bp where unit_id='" . $unit_id . "' and kegiatan_code='$kode_kegiatan'";

                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();

                $sql = "delete from " . sfConfig::get('app_default_schema') . ".rincian_bp where unit_id='" . $unit_id . "' and kegiatan_code='$kode_kegiatan'";

                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();

                $sql = "delete from " . sfConfig::get('app_default_schema') . ".rincian_sub_parameter_bp where unit_id='" . $unit_id . "' and kegiatan_code='$kode_kegiatan'";

                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();


                $sql = "insert into " . sfConfig::get('app_default_schema') . ".master_kegiatan_bp select * from " . sfConfig::get('app_default_schema') . ".master_kegiatan where unit_id='" . $unit_id . "' and kode_kegiatan='$kode_kegiatan'";

                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();

                $sql = "insert into " . sfConfig::get('app_default_schema') . ".subtitle_indikator_bp select * from " . sfConfig::get('app_default_schema') . ".subtitle_indikator where unit_id='" . $unit_id . "' and kegiatan_code='$kode_kegiatan'";

                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();

                $sql = "insert into " . sfConfig::get('app_default_schema') . ".rincian_detail_bp select * from " . sfConfig::get('app_default_schema') . ".rincian_detail where unit_id='" . $unit_id . "' and kegiatan_code='$kode_kegiatan'";

                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();

                $sql = "insert into " . sfConfig::get('app_default_schema') . ".rincian_bp select * from " . sfConfig::get('app_default_schema') . ".rincian where unit_id='" . $unit_id . "' and kegiatan_code='$kode_kegiatan'";

                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();

                $sql = "insert into " . sfConfig::get('app_default_schema') . ".rincian_sub_parameter_bp select * from " . sfConfig::get('app_default_schema') . ".rincian_sub_parameter where unit_id='" . $unit_id . "' and kegiatan_code='$kode_kegiatan'";

                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();
                $con->commit();
                $this->setFlash('berhasil', 'Kegiatan Telah Berhasil disiapkan Untuk Dapat ditarik oleh e - Project');
            } catch (Exception $e) {
                $con->rollback();
                $this->setFlash('gagal', 'Kegiatan GAGAL disiapkan Untuk Dapat ditarik oleh e - Project karena ' . $e->getMessage());
            }

            $this->forward('peneliti', 'list');
        }
    }

    public function executeBukarka() {
        if ($this->getRequestParameter('buka') == md5('dinas')) {
            $unit_id = $this->getRequestParameter('unit_id');
            $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');

            $sql = "update " . sfConfig::get('app_default_schema') . ".rincian set rincian_level='2' where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan'";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($sql);
            $stmt->executeQuery();

            $this->forward('peneliti', 'list');
        }
    }

    public function executeKuncirka() {
        if ($this->getRequestParameter('kunci') == md5('dinas')) {
            $unit_id = $this->getRequestParameter('unit_id');
            $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');

            $sql = "update " . sfConfig::get('app_default_schema') . ".rincian set rincian_level='3' where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan'";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($sql);
            $stmt->executeQuery();
            $this->forward('peneliti', 'list');
        }
    }

    public function executeList() {
        $this->processSort();

        $this->processFilters();

        $this->filters = $this->getUser()->getAttributeHolder()->getAll('sf_admin/master_kegiatan/filters');

        if ($this->getRequestParameter('kunci') == 'kunci') {
            //print_r('masuk');exit;
            $this->prosesKunci();
        }

        if ($this->getRequestParameter('buka') == 'buka kunci') {
            $this->prosesBukakunci();
        }
        // pager
        $this->pager = new sfPropelPager('MasterKegiatan', 20);
        $c = new Criteria();
        $this->addSortCriteria($c);
        $nama = '';
        //$sf_user->getNamaUser()
        //$nama = $this->getRequest()->getCookie('nama_user');
        $nama = $this->getUser()->getNamaUser();
        //print_r($this->getUser()->getNamaUser());exit;
        //print_r($nama);exit;
        if ($nama != '') {
            //$nama =$this->getRequestParameter('dinas');
            //$nama di sini berisi kode unit id yang didapat pd saat user login dari tabel user handle
            $e = new Criteria;
            $e->add(UserHandleV2Peer::USER_ID, $nama);
            $es = UserHandleV2Peer::doSelect($e);
            $this->satuan_kerja = $es;
            $unit_kerja = Array();
            foreach ($es as $x) {
                $unit_kerja[] = $x->getUnitId();
            }

            $c->add(MasterKegiatanPeer::UNIT_ID, $unit_kerja[0], Criteria::ILIKE);
            for ($i = 1; $i < count($unit_kerja); $i++) {
                $c->addOr(MasterKegiatanPeer::UNIT_ID, $unit_kerja[$i], Criteria::ILIKE);
            }
        }
        $unit_id = $this->getRequestParameter('unit_id');
        if (isset($unit_id)) {
            $c->add(MasterKegiatanPeer::UNIT_ID, $unit_id);
        }
        $c->addAscendingOrderByColumn(MasterKegiatanPeer::KODE_KEGIATAN);
        $this->addFiltersCriteria($c);
        $this->pager->setCriteria($c);
        $this->pager->setPage($this->getRequestParameter('page', 1));
        $this->pager->init();
    }

    protected function prosesKunci() {

        if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
            $unit = $this->filters['unit_id'];
            $unit_id = $unit;

            $sql = "update " . sfConfig::get('app_default_schema') . ".rincian set rincian_level='3' where unit_id='$unit_id'";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($sql);
            $stmt->executeQuery();
        }
    }

    protected function prosesBukakunci() {
        if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
            $unit = $this->filters['unit_id'];

            $sql = "update " . sfConfig::get('app_default_schema') . ".rincian set rincian_level=2 where unit_id='$unit'";
            //print_r($sql);exit;
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($sql);
            $stmt->executeQuery();
        }
    }

    protected function addFiltersCriteria($c) {
        //print_r($this->getRequestParameter('unit_name'));exit;
        if (isset($this->filters['kode_kegiatan_is_empty'])) {
            $criterion = $c->getNewCriterion(MasterKegiatanPeer::KODE_KEGIATAN, '');
            $criterion->addOr($c->getNewCriterion(MasterKegiatanPeer::KODE_KEGIATAN, null, Criteria::ISNULL));
            $c->add($criterion);
        } else if (isset($this->filters['kode_kegiatan']) && $this->filters['kode_kegiatan'] !== '') {
            $kode_kegiatan = $this->filters['kode_kegiatan'];
            $arr = explode('.', $kode_kegiatan);
            $jum_array = count($arr);
            //if($jum_array<2)
            //{
            $cton1 = $c->getNewCriterion(MasterKegiatanPeer::KODE_URUSAN, $kode_kegiatan . '%', Criteria::LIKE);
            $cton2 = $c->getNewCriterion(MasterKegiatanPeer::KODE_PROGRAM2, $kode_kegiatan . '%', Criteria::LIKE);
            $cton1->addOr($cton2);
            //$c->add($cton1);
            //}
            if ($jum_array == 2) {
                //$c->add(MasterKegiatanPeer::KODE_PROGRAM,$arr[1],Criteria::LIKE);
                $cton3 = $c->getNewCriterion(MasterKegiatanPeer::KODE_PROGRAM, $arr[1] . '%', Criteria::LIKE);
                $cton1->addAnd($cton3);
            }
            $c->add($cton1);
        }
        if (isset($this->filters['nama_kegiatan_is_empty'])) {
            $criterion = $c->getNewCriterion(MasterKegiatanPeer::NAMA_KEGIATAN, '');
            $criterion->addOr($c->getNewCriterion(MasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
            $c->add($criterion);
        } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
            $c->add(MasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
        }
        if (isset($this->filters['posisi_is_empty'])) {
            $criterion = $c->getNewCriterion(MasterKegiatanPeer::POSISI, '');
            $criterion->addOr($c->getNewCriterion(MasterKegiatanPeer::POSISI, null, Criteria::ISNULL));
            $c->add($criterion);
        } else if (isset($this->filters['posisi']) && $this->filters['posisi'] !== '') {
            //$query="select *
            //from ". sfConfig::get('app_default_schema') .".rincian r, ". sfConfig::get('app_default_schema') .".master_kegiatan mk
            //where r.rincian_level=".$this->filters['posisi']." and r.kegiatan_code=mk.kode_kegiatan";
            $crt1 = new Criteria();
            $crt1->add(RincianPeer::RINCIAN_LEVEL, $this->filters['posisi']);
            $rincian = RincianPeer::doSelect($crt1);
            foreach ($rincian as $r) {
                if ($r->getKegiatanCode() != '') {
                    $c->add(MasterKegiatanPeer::KODE_KEGIATAN, $r->getKegiatanCode());
                }
            }
        }
        if (isset($this->filters['tahap_is_empty'])) {
            $criterion = $c->getNewCriterion(MasterKegiatanPeer::TAHAP, '');
            $criterion->addOr($c->getNewCriterion(MasterKegiatanPeer::TAHAP, null, Criteria::ISNULL));
            $c->add($criterion);
        } else if (isset($this->filters['tahap']) && $this->filters['tahap'] !== '') {
            $c->add(MasterKegiatanPeer::TAHAP, $this->filters['tahap'] . '%', Criteria::LIKE);
        }
        if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
            $unit = $this->filters['unit_id'];
            $c->add(MasterKegiatanPeer::UNIT_ID, $unit);
        }
    }

    public function executeTemplateRKA() {
        //$this->setTemplate('templateRKA');
        $this->setLayout('kosong');
        $this->getResponse()->addStylesheet('tampilan_print2', '', array('media' => 'print'));
    }

    public function executePrintrekening() {
        $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
        $unit_id = $this->getRequestParameter('unit_id');
        $this->kode_kegiatan = $kode_kegiatan;
        $this->unit_id = $unit_id;
        $this->setLayout('kosong');
    }

    public function executeGetHeader() {
        $c = new Criteria();
        $c->add(MasterKegiatanPeer::KODE_KEGIATAN, $this->getRequestParameter('kegiatan'));
        $c->add(MasterKegiatanPeer::UNIT_ID, $this->getRequestParameter('unit'));
        $master_kegiatan = MasterKegiatanPeer::doSelectOne($c);
        if ($master_kegiatan) {
            $this->master_kegiatan = $master_kegiatan;
        }
        $this->setLayout('kosong');
    }

    public function executeGantiRekening() {

        $unit_id = $this->getRequestParameter('unit_id');

        $kegiatan_code = $this->getRequestParameter('kegiatan_code');
        $detail_no = $this->getRequestParameter('detail_no');
        $rekening_code = $this->getRequestParameter('rek_' . $detail_no);

        if ($rekening_code) {

            $query = "select max(detail_no) as urut from " . sfConfig::get('app_default_schema') . ".rincian_detail where unit_id='$unit_id' and kegiatan_code='$kegiatan_code'";

            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs = $stmt->executeQuery();
            while ($rs->next()) {
                $detail_no_baru = $rs->getString('urut');
            }
            $detail_no_baru+=1;

            $query = "
insert into " . sfConfig::get('app_default_schema') . ".rincian_detail(
  kegiatan_code,
  tipe,
  detail_no,
  rekening_code,
  komponen_id ,
  detail_name,
  volume,
  keterangan_koefisien ,
  subtitle ,
  komponen_harga,
  komponen_harga_awal ,
  komponen_name ,
  satuan ,
  pajak,
  unit_id ,
  from_kegiatan_code,
  from_sub_kegiatan,
  kecamatan,
  sub,
  kode_sub,
  last_update_user,
  last_update_time,
  last_update_ip ,
  tahap ,
  tahap_edit ,
  tahap_new ,
  status_lelang ,
  nomor_lelang ,
  koefisien_semula,
  volume_semula,
  harga_semula,
  total_semula )
select 
kegiatan_code,
  tipe,
  $detail_no_baru,
  '$rekening_code',
  komponen_id ,
  detail_name,
  volume,
  keterangan_koefisien ,
  subtitle ,
  komponen_harga,
  komponen_harga_awal ,
  komponen_name ,
  satuan ,
  pajak,
  unit_id ,
  from_kegiatan_code,
  from_sub_kegiatan,
  kecamatan,
  sub,
  kode_sub,
  last_update_user,
  last_update_time,
  last_update_ip ,
  tahap ,
  tahap_edit ,
  tahap_new ,
  status_lelang ,
  nomor_lelang ,
  koefisien_semula,
  volume_semula,
  harga_semula,
  total_semula 
from " . sfConfig::get('app_default_schema') . ".rincian_detail
 where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and detail_no=$detail_no";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $stmt->executeQuery();


            //$query = "update ". sfConfig::get('app_default_schema') .".rincian_detail set rekening_code='$rekening_code' where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and detail_no=$detail_no";

            $query = "update " . sfConfig::get('app_default_schema') . ".rincian_detail set volume=0,keterangan_koefisien='0' where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and detail_no=$detail_no";

            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $stmt->executeQuery();
        }

        $this->setFlash('berhasil', "Penggantian rekening berhasil  ( ke rekening $rekening_code )");

        return $this->redirect('peneliti/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
    }

}
