<?php

/**
 * lokasi_waitinglist actions.
 *
 * @package    budgeting
 * @subpackage lokasi_waitinglist
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 2288 2006-10-02 15:22:13Z fabien $
 */
class lokasi_waitinglistActions extends autolokasi_waitinglistActions {

    public function executePilih() {
        $kode_lokasi = $this->getRequestParameter('kode');
        $user = $this->getUser();
        $nama = $user->getNamaUser();

        $c = new Criteria();
        $c->add(SchemaAksesV2Peer::USER_ID, $nama, criteria::ILIKE);
        $cs = SchemaAksesV2Peer::doSelectOne($c);
        if ($cs) {
            $level = $cs->getLevelId();
            if ($level == 1) {
                if ($user->getAttribute('ubah', '', 'lokasi')) {
                    $unit = $user->getAttribute('unit', '', 'lokasi');
                    $id = $user->getAttribute('id', '', 'lokasi');
                    $c = new Criteria();
                    $c->add(VLokasiPeer::KODE, $kode_lokasi);
                    $rs_lokasi = VLokasiPeer::doSelectOne($c);
                    if ($rs_lokasi) {
                        $nama_lokasi = $rs_lokasi->getNama();
                    }
                    $user->removeCredential('lokasi');
                    //print_r('xxxx'.$nama_lokasi);exit;
                    $nama_lokasi = base64_encode($nama_lokasi);
                    return $this->redirect("waitinglist_pu/waitingedit?id=$id&unit=$unit&lokasi=$nama_lokasi&edit=" . md5('ubah'));
                }
                if ($user->getAttribute('baru', '', 'lokasi')) {
                    $unit = $user->getAttribute('unit', '', 'lokasi');
                    $id = $user->getAttribute('id', '', 'lokasi');
                    $subtitle = $user->getAttribute('subtitle', '', 'lokasi');
                    $sub = $user->getAttribute('sub', '', 'lokasi');
                    $rekening = $user->getAttribute('rekening', '', 'lokasi');
                    $pajak = $user->getAttribute('pajak', '', 'lokasi');
                    $tipe = $user->getAttribute('tipe', '', 'lokasi');

                    $c = new Criteria();
                    $c->add(VLokasiPeer::KODE, $kode_lokasi);
                    $rs_lokasi = VLokasiPeer::doSelectOne($c);
                    if ($rs_lokasi) {
                        $nama_lokasi = $rs_lokasi->getNama();
                    }
                    $user->removeCredential('lokasi');
                    $nama_lokasi = base64_encode($nama_lokasi);
                    return $this->redirect("waitinglist_pu/waitingbaru?lokasi=$nama_lokasi&rekening=$rekening&pajak=$pajak&unit=$unit&tipe=$tipe&komponen=$id&baru=" . md5('terbaru') . "&commit=Pilih");
                }

                if ($user->getAttribute('barusub', '', 'lokasi')) {
                    $unit = $user->getAttribute('unit', '', 'lokasi');
                    $sub_kegiatan_id = $user->getAttribute('id', '', 'lokasi');
                    $subtitle = $user->getAttribute('subtitle', '', 'lokasi');
                    $sub = $user->getAttribute('sub', '', 'lokasi');
                    $rekening = $user->getAttribute('rekening', '', 'lokasi');
                    $pajak = $user->getAttribute('pajak', '', 'lokasi');
                    $tipe = $user->getAttribute('tipe', '', 'lokasi');

                    $c = new Criteria();
                    $c->add(VLokasiPeer::KODE, $kode_lokasi);
                    $rs_lokasi = VLokasiPeer::doSelectOne($c);
                    if ($rs_lokasi) {
                        $nama_lokasi = $rs_lokasi->getNama();
                    }
                    $user->removeCredential('lokasi');
                    $nama_lokasi = base64_encode($nama_lokasi);
                    return $this->redirect("waitinglist_pu/pilihSubKegiatan?lokasi=$nama_lokasi&kode_kegiatan=$kegiatan&unit_id=$unit&id=$sub_kegiatan_id&sub=Pilih");
                }
            } elseif ($level == 2) {
                if ($user->getAttribute('ubah', '', 'lokasi')) {
                    $unit = $user->getAttribute('unit', '', 'lokasi');
                    $id = $user->getAttribute('id', '', 'lokasi');
                    $c = new Criteria();
                    $c->add(VLokasiPeer::KODE, $kode_lokasi);
                    $rs_lokasi = VLokasiPeer::doSelectOne($c);
                    if ($rs_lokasi) {
                        $nama_lokasi = $rs_lokasi->getNama();
                    }
                    $user->removeCredential('lokasi');
                    $nama_lokasi = base64_encode($nama_lokasi);
                    return $this->redirect("waitinglist_pu/waitingedit?id=$id&unit=$unit&lokasi=$nama_lokasi&edit=" . md5('ubah'));
                }
                if ($user->getAttribute('baru', '', 'lokasi')) {
                    $kegiatan = $user->getAttribute('kegiatan', '', 'lokasi');
                    $unit = $user->getAttribute('unit', '', 'lokasi');
                    $id = $user->getAttribute('id', '', 'lokasi');
                    $subtitle = $user->getAttribute('subtitle', '', 'lokasi');
                    $sub = $user->getAttribute('sub', '', 'lokasi');
                    $rekening = $user->getAttribute('rekening', '', 'lokasi');
                    $pajak = $user->getAttribute('pajak', '', 'lokasi');
                    $tipe = $user->getAttribute('tipe', '', 'lokasi');

                    $c = new Criteria();
                    $c->add(VLokasiPeer::KODE, $kode_lokasi);
                    $rs_lokasi = VLokasiPeer::doSelectOne($c);
                    if ($rs_lokasi) {
                        $nama_lokasi = $rs_lokasi->getNama();
                    }
                    $user->removeCredential('lokasi');
                    $nama_lokasi = base64_encode($nama_lokasi);
                    return $this->redirect("waitinglist_pu/waitingbaru?lokasi=$nama_lokasi&rekening=$rekening&pajak=$pajak&unit=$unit&tipe=$tipe&komponen=$id&baru=" . md5('terbaru') . "&commit=Pilih");
                }

                if ($user->getAttribute('barusub', '', 'lokasi')) {
                    $kegiatan = $user->getAttribute('kegiatan', '', 'lokasi');
                    $unit = $user->getAttribute('unit', '', 'lokasi');
                    $sub_kegiatan_id = $user->getAttribute('id', '', 'lokasi');
                    $subtitle = $user->getAttribute('subtitle', '', 'lokasi');
                    $sub = $user->getAttribute('sub', '', 'lokasi');
                    $rekening = $user->getAttribute('rekening', '', 'lokasi');
                    $pajak = $user->getAttribute('pajak', '', 'lokasi');
                    $tipe = $user->getAttribute('tipe', '', 'lokasi');

                    $c = new Criteria();
                    $c->add(VLokasiPeer::KODE, $kode_lokasi);
                    $rs_lokasi = VLokasiPeer::doSelectOne($c);
                    if ($rs_lokasi) {
                        $nama_lokasi = $rs_lokasi->getNama();
                    }
                    $user->removeCredential('lokasi');
                    $nama_lokasi = base64_encode($nama_lokasi);
                    return $this->redirect("waitinglist_pu/pilihSubKegiatan?lokasi=$nama_lokasi&kode_kegiatan=$kegiatan&unit_id=$unit&id=$sub_kegiatan_id&sub=Pilih");
                }
            } elseif ($level == 9) {
                if ($user->getAttribute('ubah', '', 'lokasi')) {
                    $kegiatan = $user->getAttribute('kegiatan', '', 'lokasi');
                    $unit = $user->getAttribute('unit', '', 'lokasi');
                    $id = $user->getAttribute('id', '', 'lokasi');
                    $c = new Criteria();
                    $c->add(VLokasiPeer::KODE, $kode_lokasi);
                    $rs_lokasi = VLokasiPeer::doSelectOne($c);
                    if ($rs_lokasi) {
                        $nama_lokasi = $rs_lokasi->getNama();
                    }
                    $user->removeCredential('lokasi');
                    $nama_lokasi = base64_encode($nama_lokasi);
                    return $this->redirect("waitinglist_pu/waitingedit?id=$id&unit=$unit&lokasi=$nama_lokasi&edit=" . md5('ubah'));
                }
                if ($user->getAttribute('baru', '', 'lokasi')) {
                    $kegiatan = $user->getAttribute('kegiatan', '', 'lokasi');
                    $unit = $user->getAttribute('unit', '', 'lokasi');
                    $id = $user->getAttribute('id', '', 'lokasi');
                    $subtitle = $user->getAttribute('subtitle', '', 'lokasi');
                    $sub = $user->getAttribute('sub', '', 'lokasi');
                    $rekening = $user->getAttribute('rekening', '', 'lokasi');
                    $pajak = $user->getAttribute('pajak', '', 'lokasi');
                    $tipe = $user->getAttribute('tipe', '', 'lokasi');

                    $c = new Criteria();
                    $c->add(VLokasiPeer::KODE, $kode_lokasi);
                    $rs_lokasi = VLokasiPeer::doSelectOne($c);
                    if ($rs_lokasi) {
                        $nama_lokasi = $rs_lokasi->getNama();
                    }
                    $user->removeCredential('lokasi');
                    $nama_lokasi = base64_encode($nama_lokasi);
                    return $this->redirect("waitinglist_pu/waitingbaru?lokasi=$nama_lokasi&rekening=$rekening&pajak=$pajak&unit=$unit&tipe=$tipe&komponen=$id&baru=" . md5('terbaru') . "&commit=Pilih");
                }

                if ($user->getAttribute('barusub', '', 'lokasi')) {
                    $kegiatan = $user->getAttribute('kegiatan', '', 'lokasi');
                    $unit = $user->getAttribute('unit', '', 'lokasi');
                    $sub_kegiatan_id = $user->getAttribute('id', '', 'lokasi');
                    $subtitle = $user->getAttribute('subtitle', '', 'lokasi');
                    $sub = $user->getAttribute('sub', '', 'lokasi');
                    $rekening = $user->getAttribute('rekening', '', 'lokasi');
                    $pajak = $user->getAttribute('pajak', '', 'lokasi');
                    $tipe = $user->getAttribute('tipe', '', 'lokasi');

                    $c = new Criteria();
                    $c->add(VLokasiPeer::KODE, $kode_lokasi);
                    $rs_lokasi = VLokasiPeer::doSelectOne($c);
                    if ($rs_lokasi) {
                        $nama_lokasi = $rs_lokasi->getNama();
                    }
                    $user->removeCredential('lokasi');
                    $nama_lokasi = base64_encode($nama_lokasi);
                    return $this->redirect("waitinglist_pu/pilihSubKegiatan?lokasi=$nama_lokasi&kode_kegiatan=$kegiatan&unit_id=$unit&id=$sub_kegiatan_id&sub=Pilih");
                }
            }
        } else {

            if ($user->getAttribute('ubah', '', 'lokasi')) {
                $kegiatan = $user->getAttribute('kegiatan', '', 'lokasi');
                $unit = $user->getAttribute('unit', '', 'lokasi');
                $id = $user->getAttribute('id', '', 'lokasi');
                $c = new Criteria();
                $c->add(VLokasiPeer::KODE, $kode_lokasi);
                $rs_lokasi = VLokasiPeer::doSelectOne($c);
                if ($rs_lokasi) {
                    $nama_lokasi = $rs_lokasi->getNama();
                }
                $user->removeCredential('lokasi');
                $nama_lokasi = base64_encode($nama_lokasi);
                return $this->redirect("waitinglist_pu/waitingedit?id=$id&unit=$unit&lokasi=$nama_lokasi&edit=" . md5('ubah'));
                //return $this->redirect("waitinglist_pu/waitingedit?id=$id&kegiatan=$kegiatan&unit=$unit&lokasi=$nama_lokasi&editLokasi=".md5('ubahLokasi'));
            }
            if ($user->getAttribute('baru', '', 'lokasi')) {
                $unit = $user->getAttribute('unit', '', 'lokasi');
                $id = $user->getAttribute('id', '', 'lokasi');
                $subtitle = $user->getAttribute('subtitle', '', 'lokasi');
                $sub = $user->getAttribute('sub', '', 'lokasi');
                $rekening = $user->getAttribute('rekening', '', 'lokasi');
                $pajak = $user->getAttribute('pajak', '', 'lokasi');
                $tipe = $user->getAttribute('tipe', '', 'lokasi');

                $c = new Criteria();
                $c->add(VLokasiPeer::KODE, $kode_lokasi);
                $rs_lokasi = VLokasiPeer::doSelectOne($c);
                if ($rs_lokasi) {
                    $nama_lokasi = $rs_lokasi->getNama();
                }
                $user->removeCredential('lokasi');
                $nama_lokasi = base64_encode($nama_lokasi);
                return $this->redirect("waitinglist_pu/waitingbaru?lokasi=$nama_lokasi&rekening=$rekening&pajak=$pajak&unit=$unit&tipe=$tipe&komponen=$id&baru=" . md5('terbaru') . "&commit=Pilih");
            }

            if ($user->getAttribute('barusub', '', 'lokasi')) {

                $kegiatan = $user->getAttribute('kegiatan', '', 'lokasi');
                $unit = $user->getAttribute('unit', '', 'lokasi');
                $sub_kegiatan_id = $user->getAttribute('id', '', 'lokasi');
                $subtitle = $user->getAttribute('subtitle', '', 'lokasi');
                $sub = $user->getAttribute('sub', '', 'lokasi');
                $rekening = $user->getAttribute('rekening', '', 'lokasi');
                $pajak = $user->getAttribute('pajak', '', 'lokasi');
                $tipe = $user->getAttribute('tipe', '', 'lokasi');

                $c = new Criteria();
                $c->add(VLokasiPeer::KODE, $kode_lokasi);
                $rs_lokasi = VLokasiPeer::doSelectOne($c);
                if ($rs_lokasi) {
                    $nama_lokasi = $rs_lokasi->getNama();
                }
                $user->removeCredential('lokasi');
                $nama_lokasi = base64_encode($nama_lokasi);
                return $this->redirect("waitinglist_pu/pilihSubKegiatan?lokasi=$nama_lokasi&kode_kegiatan=$kegiatan&unit_id=$unit&id=$sub_kegiatan_id&sub=Pilih");
            }
        }
    }

    public function executeList() {
        $this->processSort();

        $this->processFilters();

        $this->filters = $this->getUser()->getAttributeHolder()->getAll('sf_admin/v_lokasi/filters');

        $user = $this->getUser();
        $nama = '';

        if ($user->getAttribute('ubah', '', 'lokasi')) {
            $kegiatan = $user->getAttribute('kegiatan', '', 'lokasi');
            $unit = $user->getAttribute('unit', '', 'lokasi');
            $id = $user->getAttribute('id', '', 'lokasi');
            $nama = $user->getAttribute('nama', '', 'lokasi');
        }

        if ($user->getAttribute('baru', '', 'lokasi')) {
            $kegiatan = $user->getAttribute('kegiatan', '', 'lokasi');
            $unit = $user->getAttribute('unit', '', 'lokasi');
            $nama = $user->getAttribute('nama', '', 'lokasi');
            $id = $user->getAttribute('id', '', 'lokasi');
        }

        if ($user->getAttribute('barusub', '', 'lokasi')) {
            $kegiatan = $user->getAttribute('kegiatan', '', 'lokasi');
            $unit = $user->getAttribute('unit', '', 'lokasi');
            $nama = $user->getAttribute('nama', '', 'lokasi');
            $id = $user->getAttribute('id', '', 'lokasi');
        }
        // pager
        $this->pager = new sfPropelPager('VLokasi', 20);
        $c = new Criteria();
        $this->addSortCriteria($c);
        if ($this->getRequestParameter('filter') == 'filter') {
            $this->addFiltersCriteria($c);
        } else {
            $this->addFiltersCriteria2($c, $user);
        }
        $this->pager->setCriteria($c);
        $this->pager->setPage($this->getRequestParameter('page', 1));
        $this->pager->init();
    }

    protected function addFiltersCriteria2($c, $user) {
        if (isset($this->filters['nama_is_empty'])) {
            $criterion = $c->getNewCriterion(VLokasiPeer::NAMA, '');
            $criterion->addOr($c->getNewCriterion(VLokasiPeer::NAMA, null, Criteria::ISNULL));
            $c->add($criterion);
        } else if (isset($this->filters['nama']) && $this->filters['nama'] !== '') {
            $nama = '%' . $this->filters['nama'] . '%';
            $c->add(VLokasiPeer::NAMA, strtr($nama, '*', '%'), Criteria::ILIKE);
        } else {
            if ($user->getAttribute('nama', '', 'lokasi')) {
                $nama = $user->getAttribute('nama', '', 'lokasi');
                $nama = '%' . $nama . '%';
                $c->add(VLokasiPeer::NAMA, strtr($nama, '*', '%'), Criteria::ILIKE);
            }
        }
    }

    protected function addFiltersCriteria($c) {
        if (isset($this->filters['nama_is_empty'])) {
            $criterion = $c->getNewCriterion(VLokasiPeer::NAMA, '');
            $criterion->addOr($c->getNewCriterion(VLokasiPeer::NAMA, null, Criteria::ISNULL));
            $c->add($criterion);
        } else if (isset($this->filters['nama']) && $this->filters['nama'] !== '') {
            $nama = '%' . $this->filters['nama'] . '%';
            $c->add(VLokasiPeer::NAMA, strtr($nama, '*', '%'), Criteria::ILIKE);
        }
    }

}
