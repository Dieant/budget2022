<?php
// auto-generated by sfPropelAdmin
// date: 2011/07/21 19:19:53
?>
<?php

/**
 * autoSimbada actions.
 *
 * @package    ##PROJECT_NAME##
 * @subpackage autoSimbada
 * @author     Fabien Potencier <fabien.potencier@symfony-project.com>
 * @version    SVN: $Id: actions.class.php 9855 2008-06-25 11:26:01Z FabianLange $
 */
class simbadaActions extends autosimbadaActions
{ 
  public function executeTest()
  {
      print_r('testing');exit;
  }
  public function executePilih()
  {
        if($this->getRequestParameter('pilih')=='Pilih')
        {         
        $idSimbada = $this->getRequestParameter('id');
        $user = $this->getUser();
       
        $nama = '%'.$user->getNamaLogin().'%';
        $c = new Criteria();
        $c -> add(SchemaAksesV2Peer::USER_ID, strtr($nama, '*', '%'), criteria::ILIKE);
        $cs = SchemaAksesV2Peer::doSelectOne($c);
        //print_r('dor'.$user);exit;
        if ($cs)
        {
                $level = $cs->getLevelId();

                if($level==1){
                    ////////////////////////
                    if($user->getAttribute('baru', '', 'simbada'))
                    {
                       
                        $kegiatan = $user->getAttribute('kegiatan', '', 'simbada');
                        $unit = $user->getAttribute('unit', '', 'simbada');
                        $id = $user->getAttribute('id', '', 'simbada');
                        $subtitle = $user->getAttribute('subtitle', '', 'simbada');
                        $sub = $user->getAttribute('sub', '', 'simbada');
                        $rekening = $user->getAttribute('rekening', '', 'simbada');
                        $pajak = $user->getAttribute('pajak', '', 'simbada');
                        $tipe = $user->getAttribute('tipe', '', 'simbada');

                        $komponen_simbah = $this->getRequestParameter('simbah');
                        $count=0;
                        foreach ($komponen_simbah as $simbah_deh)
                            {
                                $c = new Criteria();
                                $c->add(KibPeer::ID,$simbah_deh);
                                $rs_barang = KibPeer::doSelectOne($c);
                                if($rs_barang)
                                {
                                    $namaBarang = $rs_barang->getNamaBarang();
                                    $keteranganBarang = $rs_barang->getKeterangan();
                                    $simbada=$namaBarang.'('.$keteranganBarang.'),';
                                }
                                $returnSimbada=$returnSimbada.' '.$simbada;
                                
                                $count++;
                            }
                            $namaBarang = base64_encode($returnSimbada);
                            
                            //$namaBarang = base64_encode($returnSimbada);
                            return $this->redirect("dinas/buatbaru?simbada=$namaBarang&jumKoef=$count&kegiatan=$kegiatan&rekening=$rekening&pajak=$pajak&unit=$unit&tipe=$tipe&komponen=$id&baru=".md5('terbaru')."&commit=Pilih");
                        //var_dump($komponen_simbah);
                        //print_r('dor'.$namaBarang. base64_decode($namaBarang).$jumlah);exit;
                    }
                    
                    ////////////////////////
                    /*
                    if($user->getAttribute('baru', '', 'simbada'))
			{       
				$kegiatan = $user->getAttribute('kegiatan', '', 'simbada');
				$unit = $user->getAttribute('unit', '', 'simbada');
				$id = $user->getAttribute('id', '', 'simbada');
				$subtitle = $user->getAttribute('subtitle', '', 'simbada');
				$sub = $user->getAttribute('sub', '', 'simbada');
				$rekening = $user->getAttribute('rekening', '', 'simbada');
				$pajak = $user->getAttribute('pajak', '', 'simbada');
				$tipe = $user->getAttribute('tipe', '', 'simbada');
				
                                $komponen_simbah = $this->getRequestParameter('simbah');
                                foreach ($komponen_simbah as $simbah_deh){
                                    $c = new Criteria();
                                    $c->add(KibPeer::ID,$simbah_deh);
                                    $rs_barang = KibPeer::doSelectOne($c);
                                    if($rs_barang)
                                    {
                                            $namaBarang = $rs_barang->getNamaBarang();
                                            $keteranganBarang = $rs_barang->getKeterangan();
                                            $simbada=$namaBarang.'('.$keteranganBarang.'),';
                                    }
                                    $returnSimbada=$returnSimbada.' '.$simbada;
                                }
                                
				//$user->removeCredential('simbada');
				$namaBarang = base64_encode($returnSimbada);
				return $this->redirect("dinas/buatbaru?simbada=$namaBarang&kegiatan=$kegiatan&rekening=$rekening&pajak=$pajak&unit=$unit&tipe=$tipe&komponen=$id&baru=".md5('terbaru')."&commit=Pilih");
			}*/
                }
                elseif($level==2){
                    print_r('Mohon maaf Anda tidak mempunyai hak akses');exit;
                }
        }
        }
  }
  public function executeIndex()
  {
    return $this->forward('simbada', 'list');
  }
  
  public function executeList()
  {
    $this->processSort();

    $this->processFilters();

    $this->filters = $this->getUser()->getAttributeHolder()->getAll('sf_admin/kib/filters');

    // pager
    $this->pager = new sfPropelPager('Kib', 100);
    $c = new Criteria();
    if($this->getUser()->getAttribute('unit', '', 'simbada') != '9999')
    {
        $c->add(KibPeer::UNIT_ID,$this->getUser()->getAttribute('unit', '', 'simbada'));
    }
    $this->addSortCriteria($c);
    $this->addFiltersCriteria($c);
    $this->pager->setCriteria($c);
    $this->pager->setPage($this->getRequestParameter('page', $this->getUser()->getAttribute('page', 1, 'sf_admin/kib')));
    $this->pager->init();
    // save page
    if ($this->getRequestParameter('page')) {
        $this->getUser()->setAttribute('page', $this->getRequestParameter('page'), 'sf_admin/kib');
    }
  }

  public function executeCreate()
  {
    return $this->forward('simbada', 'edit');
  }

  public function executeSave()
  {
    return $this->forward('simbada', 'edit');
  }

  public function executeEdit()
  {
    $this->kib = $this->getKibOrCreate();

    if ($this->getRequest()->getMethod() == sfRequest::POST)
    {
      $this->updateKibFromRequest();

      $this->saveKib($this->kib);

      $this->setFlash('notice', 'Your modifications have been saved');

      if ($this->getRequestParameter('save_and_add'))
      {
        return $this->redirect('simbada/create');
      }
      else if ($this->getRequestParameter('save_and_list'))
      {
        return $this->redirect('simbada/list');
      }
      else
      {
        return $this->redirect('simbada/edit?no_register='.$this->kib->getNoRegister().'&kode_barang='.$this->kib->getKodeBarang());
      }
    }
    else
    {
      $this->labels = $this->getLabels();
    }
  }

  public function executeDelete()
  {
    $this->kib = KibPeer::retrieveByPk($this->getRequestParameter('no_register'),
                                  $this->getRequestParameter('kode_barang'));
    $this->forward404Unless($this->kib);

    try
    {
      $this->deleteKib($this->kib);
    }
    catch (PropelException $e)
    {
      $this->getRequest()->setError('delete', 'Could not delete the selected Kib. Make sure it does not have any associated items.');
      return $this->forward('simbada', 'list');
    }

    return $this->redirect('simbada/list');
  }

  public function handleErrorEdit()
  {
    $this->preExecute();
    $this->kib = $this->getKibOrCreate();
    $this->updateKibFromRequest();

    $this->labels = $this->getLabels();

    return sfView::SUCCESS;
  }

  protected function saveKib($kib)
  {
    $kib->save();

  }

  protected function deleteKib($kib)
  {
    $kib->delete();
  }

  protected function updateKibFromRequest()
  {
    $kib = $this->getRequestParameter('kib');

    if (isset($kib['tipe_kib']))
    {
      $this->kib->setTipeKib($kib['tipe_kib']);
    }
    if (isset($kib['unit_id']))
    {
      $this->kib->setUnitId($kib['unit_id']);
    }
    if (isset($kib['kode_lokasi']))
    {
      $this->kib->setKodeLokasi($kib['kode_lokasi']);
    }
    if (isset($kib['nama_lokasi']))
    {
      $this->kib->setNamaLokasi($kib['nama_lokasi']);
    }
    if (isset($kib['nama_barang']))
    {
      $this->kib->setNamaBarang($kib['nama_barang']);
    }
    if (isset($kib['kondisi']))
    {
      $this->kib->setKondisi($kib['kondisi']);
    }
    if (isset($kib['merk']))
    {
      $this->kib->setMerk($kib['merk']);
    }
    if (isset($kib['tipe']))
    {
      $this->kib->setTipe($kib['tipe']);
    }
    if (isset($kib['alamat']))
    {
      $this->kib->setAlamat($kib['alamat']);
    }
    if (isset($kib['konstruksi']))
    {
      $this->kib->setKonstruksi($kib['konstruksi']);
    }
    if (isset($kib['tahun']))
    {
      $this->kib->setTahun($kib['tahun']);
    }
  }

  protected function getKibOrCreate($no_register = 'no_register', $kode_barang = 'kode_barang')
  {
    if (!$this->getRequestParameter($no_register)
     || !$this->getRequestParameter($kode_barang))
    {
      $kib = new Kib();
    }
    else
    {
      $kib = KibPeer::retrieveByPk($this->getRequestParameter($no_register),
                                       $this->getRequestParameter($kode_barang));

      $this->forward404Unless($kib);
    }

    return $kib;
  }

  protected function processFilters()
  {
    if ($this->getRequest()->hasParameter('filter'))
    {
      $filters = $this->getRequestParameter('filters');

      $this->getUser()->getAttributeHolder()->removeNamespace('sf_admin/kib');
      $this->getUser()->getAttributeHolder()->removeNamespace('sf_admin/kib/filters');
      $this->getUser()->getAttributeHolder()->add($filters, 'sf_admin/kib/filters');
    }
  }

  protected function processSort()
  {
    if ($this->getRequestParameter('sort'))
    {
      $this->getUser()->setAttribute('sort', $this->getRequestParameter('sort'), 'sf_admin/kib/sort');
      $this->getUser()->setAttribute('type', $this->getRequestParameter('type', 'asc'), 'sf_admin/kib/sort');
    }

    if (!$this->getUser()->getAttribute('sort', null, 'sf_admin/kib/sort'))
    {
    }
  }

  protected function addFiltersCriteria($c)
  {
    if (isset($this->filters['nama_barang_is_empty']))
    {
      $criterion = $c->getNewCriterion(KibPeer::NAMA_BARANG, '');
      $criterion->addOr($c->getNewCriterion(KibPeer::NAMA_BARANG, null, Criteria::ISNULL));
      $c->add($criterion);
    }
    else if (isset($this->filters['nama_barang']) && $this->filters['nama_barang'] !== '')
    {
        $nama = '%'.$this->filters['nama_barang'].'%';
        $c->add(KibPeer::NAMA_BARANG, strtr($nama, '*', '%'), Criteria::ILIKE);
        if($this->getUser()->getAttribute('unit', '', 'simbada') != '9999')
        {
            $c->add(KibPeer::UNIT_ID,$this->getUser()->getAttribute('unit', '', 'simbada'));
        }
    }
  }

  protected function addSortCriteria($c)
  {
    if ($sort_column = $this->getUser()->getAttribute('sort', null, 'sf_admin/kib/sort'))
    {
      $sort_column = KibPeer::translateFieldName($sort_column, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_COLNAME);
      if ($this->getUser()->getAttribute('type', null, 'sf_admin/kib/sort') == 'asc')
      {
        $c->addAscendingOrderByColumn($sort_column);
      }
      else
      {
        $c->addDescendingOrderByColumn($sort_column);
      }
    }
  }

  protected function getLabels()
  {
    return array(
      'kib{tipe_kib}' => 'Tipe kib:',
      'kib{unit_id}' => 'Unit:',
      'kib{kode_lokasi}' => 'Kode lokasi:',
      'kib{nama_lokasi}' => 'Nama lokasi:',
      'kib{no_register}' => 'No register:',
      'kib{kode_barang}' => 'Kode barang:',
      'kib{nama_barang}' => 'Nama barang:',
      'kib{kondisi}' => 'Kondisi:',
      'kib{merk}' => 'Merk:',
      'kib{tipe}' => 'Tipe:',
      'kib{alamat}' => 'Alamat:',
      'kib{konstruksi}' => 'Konstruksi:',
      'kib{tahun}' => 'Tahun:',
    );
  }
}