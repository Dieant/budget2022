<?php
// auto-generated by sfPropelAdmin
// date: 2008/05/20 10:08:26
?>
<div class="table-responsive">
<table cellspacing="0" class="sf_admin_list">    
    <thead>
        <tr>
            <th>Usulan</th>
            <th>Nama</th>
            <th>Spesifikasi</th>
            <th>Hidden Spec</th>
            <th>Merek</th>
            <th>Rekening</th>
            <th>Satuan</th>
            <th>Harga</th>
            <th>Pajak</th>
            <th>Waktu</th>
            <th>Status Verifikasi</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $i = 1;
        foreach ($pager->getResults() as $waitinglist): $odd = fmod(++$i, 2)
            ?>
        <!--        <tr class="sf_admin_row_<?php echo $odd ?>">
                        <td>
            <?php echo 'Oleh : <b>' . UsulanSPJMPeer::getStringPenyelia($waitinglist->getIdSpjm()) . '</b><br/>Dinas  : <b>(' . $waitinglist->getUnitId() . ') ' . UnitKerjaPeer::getStringUnitKerja($waitinglist->getUnitId()) . '</b>'; ?>
                    </td>
                    <td><?php echo $waitinglist->getNama() ?></td>
                    <td><?php echo $waitinglist->getSpec() ?></td>
                    <td><?php echo $waitinglist->getHiddenSpec() ?></td>
                    <td style="text-align: center"><?php echo $waitinglist->getMerek() ?></td>
                    <td style="text-align: center"><?php echo $waitinglist->getRekening() ?></td>
                    <td style="text-align: center"><?php echo $waitinglist->getSatuan() ?></td>
                    <td style="text-align: right"><?php echo number_format($waitinglist->getHarga()) ?></td>
                    <td style="text-align: center"><?php echo $waitinglist->getPajak() ?></td>
                    <td>
            <?php echo 'Created : ' . date("d-m-Y H:i", strtotime($waitinglist->getCreatedAt())) . '<br/>'; ?>
            <?php echo 'Updated : ' . date("d-m-Y H:i", strtotime($waitinglist->getUpdatedAt())); ?>
                    </td>
                    <td style="text-align: center; font-weight: bold">
            <?php
            if ($waitinglist->getStatusHapus() == t) {
                echo '<font style="color:red">Ditolak</font>';
            } else {
                if ($waitinglist->getStatusVerifikasi() == t) {
                    echo '<font style="color:green">Terverifikasi</font>';
                } else {
                    echo '<font style="color:blue">Antrian</font>';
                }
            }
            ?>
                    </td>
                </tr>-->
        <?php endforeach; ?>
    </tbody>
    <tfoot>
        <tr>
            <th colspan="11">
    <div class="float-right">
        <?php if ($pager->haveToPaginate()): ?>
            <?php echo link_to(image_tag(sfConfig::get('sf_admin_web_dir') . '/images/first.png', array('align' => 'absmiddle', 'alt' => __('First'), 'title' => __('First'))), 'waitinglist/waitinglist?page=1') ?>
            <?php echo link_to(image_tag(sfConfig::get('sf_admin_web_dir') . '/images/previous.png', array('align' => 'absmiddle', 'alt' => __('Previous'), 'title' => __('Previous'))), 'waitinglist/waitinglist?page=' . $pager->getPreviousPage()) ?>

            <?php foreach ($pager->getLinks() as $page): ?>
                <?php echo link_to_unless($page == $pager->getPage(), $page, 'waitinglist/waitinglist?page=' . $page) ?>
            <?php endforeach; ?>

            <?php echo link_to(image_tag(sfConfig::get('sf_admin_web_dir') . '/images/next.png', array('align' => 'absmiddle', 'alt' => __('Next'), 'title' => __('Next'))), 'waitinglist/waitinglist?page=' . $pager->getNextPage()) ?>
            <?php echo link_to(image_tag(sfConfig::get('sf_admin_web_dir') . '/images/last.png', array('align' => 'absmiddle', 'alt' => __('Last'), 'title' => __('Last'))), 'waitinglist/waitinglist?page=' . $pager->getLastPage()) ?>
        <?php endif; ?>
    </div>
    <?php echo format_number_choice('[0] no result|[1] 1 result|(1,+Inf] %1% results', array('%1%' => $pager->getNbResults()), $pager->getNbResults()) ?>
</th>
</tr>
</tfoot>
</table>
</div>