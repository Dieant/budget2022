<?php use_helper('I18N', 'Date', 'Object', 'Javascript', 'Validation'); ?>
<section class="content-header">
    <h1>Riwayat Waiting List PU</h1>
</section>

<!-- Main content -->
<section class="content">
    <?php include_partial('waitinglist/list_messages'); ?>
    <!-- Default box -->    
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Filters</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body">
            <?php echo form_tag('waitinglist/seluruhwaitinglist', array('method' => 'get', 'class' => 'form-horizontal')) ?>
            <div class="form-group">
                <?php
                $arrOptions = array(
                    1 => 'Nama Komponen',
                    2 => 'Nama Lokasi',
                    3 => 'Kode Kegiatan'
                );
                echo "<div class='col-xs-3'>";
                echo select_tag('filters[select]', options_for_select($arrOptions, @$filters['select'] ? $filters['select'] : '', array('class' => 'form-control')), array('class' => 'form-control'));
                echo "</div>";
                echo "<div class='col-xs-9'>";
                echo input_tag('filters[komponen]', isset($filters['komponen']) ? $filters['komponen'] : null, array('class' => 'form-control'));
                echo "</div>";
                ?>	
            </div>
            <div id="sf_admin_container">
                <ul class="sf_admin_actions">
                    <?php
                    echo '<li><button type="submit" name="filter" class="btn btn-default btn-flat btn-sm"><i class="fa fa-search"></i> Search</button></li>';        
                    echo '<li>'. link_to('<i class="fa fa-refresh"></i> Reset', 'waitinglist/seluruhwaitinglist?filter=filter', array('class' => 'btn btn-default btn-flat btn-sm')). '</li>';
                    ?>
                </ul>
            </div>            
            <?php echo '</form>'; ?>
        </div>
    </div>
    <div class="box box-info">
        <div class="box-body">
            <?php if (!$pager->getNbResults()): ?>
                <?php echo __('no result') ?>
            <?php else: ?>
                <div id="sf_admin_container">
                    <?php include_partial('waitinglist/seluruhwaitinglist', array('pager' => $pager)) ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</section>