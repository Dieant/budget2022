<?php use_helper('I18N', 'Date', 'Url', 'Javascript', 'Form', 'Object', 'Number', 'Validation'); ?>
<section class="content-header">
    <h1>Edit Waiting List PU</h1>
</section>
<section class="content">
    <?php include_partial('waitinglist/list_messages'); ?>
    <!-- Default box -->
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Form Edit Waiting List</h3>
        </div>
        <div class="box-body">
            <div id="sf_admin_container" class="table-responsive">        
                <?php echo form_tag('waitinglist/waitingedit') ?>
                <table cellspacing="0" class="sf_admin_list">
                    <thead>
                        <tr>
                            <th style="width: 9%"><b>Nama</b></th>
                            <th style="width: 1%">&nbsp;</th>
                            <th style="width: 90%"><b>Isian</b></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="sf_admin_row_1" align='right'>
                            <td>Kode Rekening</td>
                            <td align="center">:</td>
                            <td align="left">
                                <?php echo select_tag('rekening', objects_for_select($rs_rekening, 'getRekeningCode', 'getRekeningCode', $waitinglist->getKomponenRekening(), 'include_custom=---Pilih Rekening--'), array('class' => 'js-example-basic-single', 'style' => 'width:100%')); ?>
                            </td>
                        </tr>
                        <tr class="sf_admin_row_0" align='right'>
                            <td>Komponen</td>
                            <td align="center">:</td>
                            <td align="left"><?php echo $waitinglist->getKomponenName() ?></td>
                        </tr>
                        <tr class="sf_admin_row_1" align='right'>
                            <td>Harga</td>
                            <td align="center">:</td>
                            <?php
                            $komponen_id = $waitinglist->getKomponenId();
                            $komponen_harga = $waitinglist->getKomponenHarga();
                            if ((substr($komponen_id, 0, 14) == '23.01.01.04.12') || (substr($komponen_id, 0, 14) == '23.01.01.04.13') || (substr($komponen_id, 0, 11) == '23.04.04.01')) {
                                ?>
                                <td align='left'><?php echo number_format($komponen_harga, 3, ',', '.') ?></td>
                                <?php
                            } else {
                                ?>
                                <td align='left'><?php echo '&nbsp;' . number_format($komponen_harga, 0, ',', '.') ?> </td>
                                <?php
                            }
                            ?>
                        </tr>
                        <tr class="sf_admin_row_0" align='right'>
                            <td>Satuan</td>
                            <td align="center">:</td>
                            <td align="left"><?php echo $waitinglist->getKomponenSatuan(); ?></td>
                        </tr>
                        <tr class="sf_admin_row_1" align='right'>
                            <td>Pajak</td>
                            <td align="center">:</td>
                            <td align="left"><?php echo $waitinglist->getPajak() . '%' ?></td>
                        </tr>
                        <tr class="sf_admin_row_0" align='right'>
                            <td><span style="color:red;">*</span> Kegiatan</td>
                            <td align="center">:</td>
                            <td align="left">
                                <?php
                                echo input_hidden_tag('unit_id', $sf_params->get('unit'));
                                $kegiatan_code = $waitinglist->getKegiatanCodeLama();
                                echo select_tag('kode_kegiatan', objects_for_select($rs_masterkegiatan, 'getKodeKegiatan', 'getNamaKegiatan', $kegiatan_code, 'include_custom=---Pilih Kegiatan---'), array('class' => 'js-example-basic-single', 'style' => 'width:100%'));
                                ?>
                            </td>
                        </tr>
                        <tr class="sf_admin_row_1" align='right'>
                            <td><span style="color:red;">*</span> Subtitle</td>
                            <td align="center">:</td>
                            <td align="left">
                                <div id="indicator" style="display:none;" align="center"><dt>&nbsp;</dt><dd><b>Mohon Tunggu </b><?php echo image_tag('loading.gif', array('align' => 'absmiddle')) ?></dd></div>
                                <?php
//                                if ($waitinglist->getSubtitle()) {
//                                    echo select_tag('subtitle', options_for_select(array($waitinglist->getSubtitle() => $waitinglist->getSubtitle()), $waitinglist->getSubtitle()), Array('id' => 'sub1', 'class' => 'js-example-basic-single', 'style' => 'width:100%'));
//                                } else {
//                                    echo select_tag('subtitle', options_for_select(array(), '', 'include_custom=---Pilih Kegiatan Dulu---'), Array('id' => 'sub1', 'class' => 'js-example-basic-single', 'style' => 'width:100%'));
//                                }
                                ?>
                                <?php
                                if ($sf_params->get('kode_kegiatan')) {
                                    $d = new Criteria();
                                    $d->add(SubtitleIndikatorPeer::UNIT_ID, $sf_params->get('unit'));
                                    $d->add(SubtitleIndikatorPeer::KEGIATAN_CODE, $sf_params->get('kode_kegiatan'));
                                    $rs_subtitleindikator = SubtitleIndikatorPeer::doSelectOne($d);
                                    if ($rs_subtitleindikator) {
                                        echo select_tag('subtitle', options_for_select(array($rs_subtitleindikator->getSubtitle()), $rs_subtitleindikator->getSubtitle(), 'include_custom=---Pilih Subtitle Dulu---'), Array('id' => 'sub1', 'class' => 'js-example-basic-single', 'style' => 'width:100%'));
                                    }
                                } elseif (!$sf_params->get('kode_kegiatan')) {
                                    $query = "select subtitle from " . sfConfig::get('app_default_schema') . ".subtitle_indikator where unit_id='2600' and kegiatan_code='" . $waitinglist->getKegiatanCode() . "'";
                                    $con = Propel::getConnection();
                                    $stmt = $con->prepareStatement($query);
                                    $rs = $stmt->executeQuery();
                                    $sub = array();
                                    while ($rs->next()) {
                                        $sub[$rs->getString('subtitle')] = $rs->getString('subtitle');
                                    }
                                    echo select_tag('subtitle', options_for_select($sub, $waitinglist->getSubtitle()), Array('id' => 'sub1', 'class' => 'js-example-basic-single', 'style' => 'width:100%'));
                                }
                                ?>
                            </td>
                        </tr>    
                        <tr class="sf_admin_row_0" align='right' valign="top">
                            <td>Lokasi</td>
                            <td align="center">:</td>
                            <td align="left">
                                <div class="row col-xs-12 text-bold text-center">
                                    <div class="row col-xs-2 text-center">Lokasi Sekarang</div>
                                    <div class="row col-xs-10">
                                        <?php echo input_tag('lokasi', $waitinglist->getKomponenLokasi(), array('class' => 'form-control')); //submit_tag('cari', 'name=cari').'<br><font color="magenta">[untuk multi lokasi, tambahkan lokasi dengan klik "cari" lagi]<br></font>';   ?>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="row col-xs-12 text-bold text-center">
                                    <div class="row col-xs-12">
                                        <hr/>
                                        Silahkan pilih dari data yang sudah ada
                                        <hr/>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="row col-xs-12 text-bold text-center">
                                    <div class="row col-xs-12">
                                        <?php echo select_tag('lokasi_lama', objects_for_select($rs_jalan, 'getLokasi', 'getLokasi', '', 'include_custom=---Pilih Jalan---'), array('class' => 'js-example-basic-multiple', 'style' => 'width:100%', 'multiple' => 'multiple')); ?>                                                
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="row col-xs-12 text-bold text-center">
                                    <div class="row col-xs-12">
                                        <hr/>
                                        Silahkan menambah data lokasi (apabila tidak ada dipilihan atas)
                                        <hr/>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="row col-xs-12">
                                    <?php
                                    foreach ($rs_geojson as $value_geojson) {
                                        if ($value_geojson->getGang() <> '') {
                                            $array_gang = explode('.', $value_geojson->getGang());
                                            $tipe_gang = $array_gang[0];
                                            $nama_gang = trim($array_gang[1]);
                                        }
                                        ?>
                                        <div class="div-induk-lokasi row">
                                            <div>
                                                <div class="form-group form-group-options col-xs-5 col-sm-5 col-md-5">
                                                    <div class="input-group input-group-option col-xs-12">
                                                        <font class="text-bold">Nama Jalan</font><br/>
                                                        <div class="input-group input-group-sm">
                                                            <span class="input-group-addon">JL.</span>
                                                            <input type="text" name="lokasi_jalan[]" class="form-control lokasi_jalan" placeholder="Nama Jalan (*wajib diisi apabila lokasi berupa Jalan)" value="<?php echo $value_geojson->getJalan() ?>" >
                                                        </div>
                                                        <ul class="jalan_list_id"></ul>                                                        
                                                    </div>
                                                </div>
                                                <div class="form-group form-group-options col-xs-1 col-sm-1 col-md-1">
                                                    <div class="input-group input-group-option col-xs-12">
                                                        <font class="text-bold">Gang/Blok/Kavling</font><br/>
                                                        <select class="form-control" name="tipe_gang[]">                                                                
                                                            <option selected value="<?php echo $tipe_gang ?>"><?php echo $tipe_gang ?></option>
                                                            <option value="GG">---</option>
                                                            <option value="GG">GANG</option>
                                                            <option value="BLOK">BLOK</option>
                                                            <option value="KAV">KAVLING</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group form-group-options col-xs-2 col-sm-2 col-md-2">
                                                    <div class="input-group input-group-option col-xs-12">
                                                        <font class="text-bold">Nama Gang/Blok/Kavling</font><br/>
                                                        <input type="text" name="lokasi_gang[]" class="form-control" placeholder="Nama Gang" value="<?php echo $nama_gang ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group form-group-options col-xs-2 col-sm-2 col-md-2">
                                                    <div class="input-group input-group-option col-xs-12">
                                                        <font class="text-bold">Nomor Lokasi</font><br/>
                                                        <div class="input-group input-group-sm">
                                                            <span class="input-group-addon">NO.</span>
                                                            <input type="text" name="lokasi_nomor[]" class="form-control" placeholder="Nomor" value="<?php echo $value_geojson->getNomor() ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group form-group-options col-xs-1 col-sm-1 col-md-1">
                                                    <div class="input-group input-group-option col-xs-12">
                                                        <font class="text-bold">RW</font><br/>
                                                        <input type="text" name="lokasi_rw[]" class="form-control" placeholder="RW" value="<?php echo $value_geojson->getRw() ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group form-group-options col-xs-1 col-sm-1 col-md-1">
                                                    <div class="input-group input-group-option col-xs-12">
                                                        <font class="text-bold">RT</font><br/>
                                                        <input type="text" name="lokasi_rt[]" class="form-control" placeholder="RT" value="<?php echo $value_geojson->getRt() ?>">
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <div class="form-group form-group-options col-xs-6 col-sm-6 col-md-6">
                                                    <div class="input-group input-group-option col-xs-12">
                                                        <font class="text-bold">Nama Bangunan/Saluran</font><br/>
                                                        <input type="text" name="lokasi_tempat[]" class="form-control" placeholder="Nama Bangunan/Saluran (*wajib diisi apabila lokasi berupa bangunan/saluran/tempat)" value="<?php echo $value_geojson->getTempat() ?>">                                                                
                                                    </div>
                                                </div>
                                                <div class="form-group form-group-options col-xs-6 col-sm-6 col-md-6">
                                                    <div class="input-group input-group-option col-xs-12">
                                                        <font class="text-bold">Keterangan Lokasi</font><br/>
                                                        <input type="text" name="lokasi_keterangan[]" class="form-control" placeholder="Keterangan Lokasi" value="<?php echo $value_geojson->getKeterangan() ?>">
                                                        <span class="input-group-addon input-group-addon-add">
                                                            <span class="glyphicon glyphicon-plus"></span>
                                                        </span>
                                                        <span class="input-group-addon input-group-addon-remove">
                                                            <span class="glyphicon glyphicon-remove"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>                                                
                                    <?php }
                                    ?>
                                </div>
                                <!--<div class="row col-xs-12 table-responsive">
                                    <table class="table table-condensed table-bordered">
                                        <thead>
                                            <tr>
                                                <td class="text-bold text-center">Kolom</td>
                                                <td class="text-bold text-center">DiIsi Dengan</td>
                                                <td class="text-bold text-center">Larangan</td>
                                                <td class="text-bold text-center">Otomatis Diisi</td>
                                                <td class="text-bold text-center">Harus Diisi?</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>"Nama Jalan"</td>
                                                <td>Hanya nama jalan</td>
                                                <td>Isi tanpa kata "JL."</td>
                                                <td>Otomatis ditambahkan "JL."</td>
                                                <td>Iya, tapi dapat digantikan dengan pengisian "Nama Tempat"</td>
                                            </tr>
                                            <tr>
                                                <td>Dropdown Gang/Blok/Kavling</td>
                                                <td>"Gang", "BLOK", atau "Kavling"</td>
                                                <td>-</td>
                                                <td>"---" akan default terpilih "Gang"</td>
                                                <td>Tidak</td>
                                            </tr>
                                            <tr>
                                                <td>"Nama Gang/Blok/Kavling"</td>
                                                <td>data nama gang atau nama blok atau nama kavling</td>
                                                <td>Isi tanpa kata "GG.", "BLOK.", atau "KAV."</td>
                                                <td>Otomatis ditambahkan "Gang", "BLOK", atau "Kavling"</td>
                                                <td>Tidak</td>
                                            </tr>
                                            <tr>
                                                <td>"Nomor"</td>
                                                <td>data nomor lokasi</td>
                                                <td>Isi tanpa kata "Nomor."</td>
                                                <td>Otomatis ditambahkan "NO."</td>
                                                <td>Tidak</td>
                                            </tr>
                                            <tr>
                                                <td>"RW"</td>
                                                <td>data RW</td>
                                                <td>Isi tanpa kata "RW."</td>
                                                <td>Otomatis ditambahkan "RW."</td>
                                                <td>Tidak</td>
                                            </tr>
                                            <tr>
                                                <td>"RT"</td>
                                                <td>data RT</td>
                                                <td>Isi tanpa kata "RT."</td>
                                                <td>Otomatis ditambahkan "RT."</td>
                                                <td>Tidak</td>
                                            </tr>
                                            <tr>
                                                <td>"Keterangan Lokasi"</td>
                                                <td>data keterangan pemerjelas lokasi pekerjaan (seperti sebelah barat)</td>
                                                <td>-</td>
                                                <td>-</td>
                                                <td>Tidak</td>
                                            </tr>
                                            <tr>
                                                <td>"Nama Tempat"</td>
                                                <td>data nama bangunan atau tempat</td>
                                                <td>-</td>
                                                <td>-</td>
                                                <td>Iya, tapi dapat digantikan dengan pengisian "Nama Jalan"</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>-->
                            </td>
                        </tr>
                        <tr class="sf_admin_row_1" align='right'>
                            <td><span style="color:red;">*</span> Kecamatan</td>
                            <td align="center">:</td>
                            <td align="left">
                                <?php
                                $kec = new Criteria();
                                $kec->addAscendingOrderByColumn(KecamatanPeer::NAMA);
                                $rs_kec = KecamatanPeer::doSelect($kec);
                                $nama_kecamatan_kel = $rs_kec;
                                if ($waitinglist->getKecamatan()) {
                                    $kec_ini = new Criteria();
                                    $kec_ini->add(KecamatanPeer::NAMA, $waitinglist->getKecamatan());
                                    $rs_kec_ini = KecamatanPeer::doSelectOne($kec_ini);
                                    echo select_tag('kecamatan', objects_for_select($nama_kecamatan_kel, 'getId', 'getNama', $rs_kec_ini->getId(), 'include_custom=---Pilih Kecamatan---'), array('class' => 'js-example-basic-single', 'style' => 'width:100%'));
                                } else {
                                    echo select_tag('kecamatan', objects_for_select($nama_kecamatan_kel, 'getId', 'getNama', '', 'include_custom=---Pilih Kecamatan---'), array('class' => 'js-example-basic-single', 'style' => 'width:100%'));
                                }
                                ?>
                                <span style="color:red;">* untuk Komponen FISIK, diharuskan untuk mengisi data Kecamatan.</span>
                            </td>
                        </tr>				
                        <tr class="sf_admin_row_0" align='right'>
                            <td>Kelurahan</td>
                            <td align="center">:</td>
                            <td align="left">
                                <div id="indicator" style="display:none;" align="center"><dt>&nbsp;</dt><dd><b>Mohon Tunggu </b><?php echo image_tag('loading.gif', array('align' => 'absmiddle')) ?></dd></div>
                                <?php
                                if ($waitinglist->getKelurahan() && $waitinglist->getKecamatan()) {
                                    $kec_kel_ini = new Criteria();
                                    $kec_kel_ini->add(KelurahanKecamatanPeer::NAMA_KELURAHAN, $waitinglist->getKelurahan());
                                    $kec_kel_ini->add(KelurahanKecamatanPeer::NAMA_KECAMATAN, $waitinglist->getKecamatan());
                                    $rs_kec_kel_ini = KelurahanKecamatanPeer::doSelectOne($kec_kel_ini);
//                                    echo select_tag('kelurahan', options_for_select(array($rs_kec_kel_ini->getOid() => $waitinglist->getKelurahan()), $rs_kec_kel_ini->getOid(), 'include_custom=---Pilih Kecamatan Dulu---'), Array('id' => 'kelurahan1', 'class' => 'js-example-basic-single', 'style' => 'width:100%'));

                                    $kel_ini = new Criteria();
                                    $kel_ini->add(KelurahanKecamatanPeer::NAMA_KECAMATAN, $waitinglist->getKecamatan());
                                    $nama_kel_ini = KelurahanKecamatanPeer::doSelect($kel_ini);
                                    $options = array();
                                    foreach ($nama_kel_ini as $kel) {
                                        $options[$kel->getOid()] = $kel->getNamaKelurahan();
                                    }
                                    echo select_tag('kelurahan', options_for_select($options, $rs_kec_kel_ini->getOid(), 'include_custom=---Pilih Kecamatan Dulu---'), Array('id' => 'kelurahan1', 'class' => 'js-example-basic-single', 'style' => 'width:100%'));
                                } else {
                                    echo select_tag('kelurahan', options_for_select(array(), '', 'include_custom=---Pilih Kecamatan Dulu---'), Array('id' => 'kelurahan1', 'class' => 'js-example-basic-single', 'style' => 'width:100%'));
                                }
                                ?>
                                <span style="color:red;">* untuk Komponen FISIK, diharuskan untuk mengisi data Kelurahan.</span>
                            </td>
                        </tr>
                        <tr class="sf_admin_row_1" align='right' valign="top">
                            <td><span style="color:red;">*</span>Volume</td>
                            <td align="center">:</td>
                            <td align="left">
                                <?php
                                $keterangan_koefisien = $waitinglist->getKoefisien();
                                $pisah_kali = explode('X', $keterangan_koefisien);
                                for ($i = 0; $i < 4; $i++) {
                                    $satuan = '';
                                    $volume = '';
                                    $nama_input = 'vol' . ($i + 1);
                                    $nama_pilih = 'volume' . ($i + 1);
                                    ;
                                    if (!empty($pisah_kali[$i])) {
                                        $pisah_spasi = explode(' ', $pisah_kali[$i]);
                                        $j = 0;

                                        for ($s = 0; $s < count($pisah_spasi); $s++) {
                                            if ($pisah_spasi[$s] != NULL) {
                                                if ($j == 0) {
                                                    $volume = $pisah_spasi[$s];
                                                    $j++;
                                                } elseif ($j == 1) {
                                                    $satuan = $pisah_spasi[$s];
                                                    $j++;
                                                } else {
                                                    $satuan.=' ' . $pisah_spasi[$s];
                                                }
                                            }
                                        }
                                    }
                                    if ($i !== 3) {
                                        echo input_tag($nama_input, $volume) . ' ' . select_tag($nama_pilih, objects_for_select($rs_satuan, 'getSatuanName', 'getSatuanName', $satuan, 'include_custom=---Pilih Satuan--'), array('class' => 'js-example-basic-single', 'style' => 'width:20%')) . '<br />  X <br />';
                                    } else {
                                        echo input_tag($nama_input, $volume) . ' ' . select_tag($nama_pilih, objects_for_select($rs_satuan, 'getSatuanName', 'getSatuanName', $satuan, 'include_custom=---Pilih Satuan--'), array('class' => 'js-example-basic-single', 'style' => 'width:20%'));
                                    }
                                }
                                ?>
                            </td>
                        </tr>          
                        <tr class="sf_admin_row_0" align='right' valign="top">
                            <td>Total</td>
                            <td align="center">:</td>
                            <td align="left"><?php echo input_tag('total', $waitinglist->getNilaiAnggaran(), array('readonly' => 'true', 'class' => 'form-control')) ?></td>
                        </tr>
                        <tr class="sf_admin_row_1" align='right'>
                            <td>Nilai EE</td>
                            <td align="center">:</td>
                            <td align="left">
                                <?php echo input_tag('nilai_ee', $waitinglist->getNilaiEe(), array('class' => 'form-control')); ?>
                            </td>
                        </tr>                
                        <tr class="sf_admin_row_0" align='right'>
                            <td>keterangan</td>
                            <td align="center">:</td>
                            <td align="left">
                                <?php echo textarea_tag('keterangan', $waitinglist->getKeterangan(), array('class' => 'form-control')); ?>
                            </td>
                        </tr>
                        <tr class="sf_admin_row_1" align='right' valign="top">
                            <td>Komponen Musrenbang</td>
                            <td align="center">:</td>
                            <td align="left">
                                <?php
                                if ($waitinglist->getIsMusrenbang() == TRUE) {
                                    echo checkbox_tag('musrenbang', 1, TRUE);
                                } else {
                                    echo checkbox_tag('musrenbang');
                                }
                                ?>
                                <font style="color: green"> *Centang jika termasuk komponen Musrenbang</font> 
                            </td>
                        </tr>
                        <tr class="sf_admin_row_0" align='right'>
                            <td>Usulan Jasmas dari</td>
                            <td align="center">:</td>
                            <td align="left">
                                <?php echo select_tag('jasmas', objects_for_select($rs_jasmas, 'getKodeJasmas', 'getNama', $waitinglist->getKodeJasmas(), 'include_custom=--Pilih--'), array('class' => 'js-example-basic-single', 'style' => 'width:100%')); ?>
                            </td>
                        </tr>

<!--                        <tr class="sf_admin_row_1" align='right'>
<td>Apakah masuk dalam list Musrenbang ?</td>
<td align="center">:</td>
<td align="left">
                        <?php
                        if ($waitinglist->getIsMusrenbang() == TRUE) {
                            $nilai_musrenbang = 1;
                            $redaksi_musrenbang = 'Termasuk Musrenbang';
                        } else {
                            $nilai_musrenbang = 0;
                            $redaksi_musrenbang = 'Tidak Musrenbang';
                        }
                        ?>
<select name="is_musrenbang" >
    <option value="<?php echo $nilai_musrenbang ?>" selected><?php echo $redaksi_musrenbang ?></option>
    <option value="0">Tidak Musrenbang</option>
    <option value="1">Termasuk Musrenbang</option>
</select>
</td>
</tr>-->
                    </tbody>
                    <tfoot>
                        <tr class="sf_admin_row_0" align='right' valign="top">
                            <td>&nbsp; </td>
                            <td>
                                <?php
                                echo input_hidden_tag('unit', '2600');
                                echo input_hidden_tag('id', $sf_params->get('id'));
                                echo input_hidden_tag('referer', $sf_request->getAttribute('referer'));
                                ?>
                            </td>
                            <td><?php echo submit_tag('simpan', 'name=simpan') . ' ' . button_to('kembali', '#', array('onClick' => "javascript:history.back()")); ?></li></td>
                        </tr>
                    </tfoot>
                </table>
                <?php echo '</form>'; ?>
            </div>
        </div>
    </div>
</section>
<style>
    .select2-container-multi .select2-choices .select2-search-choice {
        padding: 3px 5px 3px 18px;
        margin: 3px 0 3px 5px;
        line-height: 20px;
    }
</style>
<script>
    $(document).ready(function () {
        $(".js-example-basic-single").select2();
        $(".js-example-basic-multiple").select2();
    });

    $(function () {
        $(document).on('click', 'div.form-group-options .input-group-addon-add', function () {
            var divIluminati = $(this).parents('.div-induk-lokasi');
            var sDivIluminatiHtml = divIluminati.html();
            var sInputGroupClasses = divIluminati.attr('class');
            //Gambiarra pra nao ficar criando mil inputs
            if (divIluminati.next().length >= 1)
                return;
            divIluminati.parent().append('<div class="' + sInputGroupClasses + '">' + sDivIluminatiHtml + '</div>');
        });
        $(document).on('click', 'div.form-group-options .input-group-addon-remove', function () {
            var divIluminati = $(this).parents('.div-induk-lokasi');
            divIluminati.remove();
        });
    });

    $("#kode_kegiatan").change(function () {
        var id = $(this).val();
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/waitinglist/pilihkegiatan/unit_id/<?php echo $sf_params->get('unit') ? $sf_params->get('unit') : '2600' ?>/b/" + id + ".html",
            context: document.body
        }).done(function (msg) {
            $('#sub1').html(msg);
        });

    });
    $("#kecamatan").change(function () {
        var id = $(this).val();
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/waitinglist/pilihKelurahan/b/" + id + ".html",
            context: document.body
        }).done(function (msg) {
            $('#kelurahan1').html(msg);
        });

    });

    function hitungTotal() {
        var harga = $('harga').value;
        var pajakx = $('pajakx').value;
        var vol1 = $('vol1').value;
        var vol2 = $('vol2').value;
        var vol3 = $('vol3').value;
        var vol4 = $('vol4').value;
        var volume;

        if (vol1 !== '' || vol2 !== '' || vol3 !== '' || vol4 !== '') {
            if (vol2 === '') {
                vol2 = 1;
                volume = vol1 * vol2;
            } else if (vol2 !== '') {
                volume = vol1 * vol2;
            }
            if (vol3 === '') {
                vol3 = 1;
                volume = volume * vol3;
            } else if (vol3 !== '') {
                volume = vol1 * vol2 * vol3;
            }
            if (vol4 === '') {
                vol4 = 1;
                volume = volume * vol4;
            } else if (vol4 !== '') {
                volume = vol1 * vol2 * vol3 * vol4;
            }
        }

        if (pajakx === 10) {
            var hitung = (harga * volume * (110) / 100);
        } else if (pajakx === 0) {
            var hitung = (harga * volume * 1);
        }

        $('total').value = hitung;

    }
</script>