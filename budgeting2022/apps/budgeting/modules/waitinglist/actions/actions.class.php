<?php

/**
 * waitinglist actions.
 *
 * @package    budgeting
 * @subpackage waitinglist
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 2692 2006-11-15 21:03:55Z fabien $
 */
class waitinglistActions extends sfActions {

    /**
     * Executes index action
     *
     */
    public function executeIndex() {
        $this->forward('default', 'module');
    }

    public function executeDetailKomponenPenyusun() {
        $komponen_id = $this->getRequestParameter('komponen');
        $c = new Criteria();
        $c->add(KomponenPeer::KOMPONEN_ID, $komponen_id);
        $dapat = KomponenPeer::doSelectOne($c);
        echo $dapat->getKomponenHarga() . '|' . $dapat->getSatuan();
        exit;
    }

    public function executeCekUserPU() {
        $json_coba = '{"type":"FeatureCollection","features":[{"type":"Feature","geometry":{"type":"LineString","coordinates":[[-7.257817077517828,112.74664252996445],[-7.2581789350764,112.74808019399643]]},"properties":{"jarak":163.78835122092661}}],"keterangan":"","keteranganalamat":""}';
        echo 'encode ' . json_encode($json_coba) . '<br/>';
        echo 'decode ' . json_decode($json_coba);
        exit();
    }

//    ticket #32 - waiting list PU posisi antrian
//    created at 23juni2015
    public function executeWaitinglist() {
        $d = new Criteria();
        $d->clearSelectColumns();
        $d->addSelectColumn(WaitingListPeer::UNIT_ID);
        $d->addSelectColumn(WaitingListPeer::KEGIATAN_CODE_LAMA);
        $d->addAscendingOrderByColumn(WaitingListPeer::KEGIATAN_CODE_LAMA);
        $d->setDistinct();
        $waiting = WaitingListPeer::doSelectRS($d);
        $this->waiting = $waiting;
    }

//    ticket #32 - waiting list PU posisi antrian
//    ticket #33 - javascript memilih kegiatan muncul subtitle
//    created at 24juni2015        
    public function executePilihkegiatan() {
        if ($this->getRequestParameter('b') != '') {
            $unitid = $this->getRequestParameter('unit_id');
            $kode_kegiatan = $this->getRequestParameter('b');

            $queryku = "select subtitle from " . sfConfig::get('app_default_schema') . ".subtitle_indikator 
                        where kegiatan_code='$kode_kegiatan' and unit_id='$unitid' order by subtitle";

            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($queryku);
            $rcsb = $stmt->executeQuery();

            $arr_tampung = array();

            $this->isi_baru = array();
            while ($rcsb->next()) {
                if ($rcsb->getString('subtitle') != '') {
                    $arr_tampung[$rcsb->getString('subtitle')] = $rcsb->getString('subtitle');
                }
            }
            $this->isi_baru = $arr_tampung;
        }
    }

//    ticket #33 - javascript memilih kegiatan muncul subtitle
//    ticket #33 - javascript dropdown list data waiting list
//    created at 24juni2015        
    public function executeGetList() {
        if ($this->getRequestParameter('id')) {
            $kode_kegiatan = str_replace('_', '.', $this->getRequestParameter('id'));

            $c = new Criteria();
            $c->add(WaitingListPeer::STATUS_HAPUS, FALSE);
            $c->add(WaitingListPeer::STATUS_WAITING, 0);
            $c->add(WaitingListPeer::KEGIATAN_CODE_LAMA, $kode_kegiatan);
            $c->addAscendingOrderByColumn(WaitingListPeer::PRIORITAS);
            $rs_waiting = WaitingListPeer::doSelect($c);
            $this->rs_waiting = $rs_waiting;

            $this->id = $this->getRequestParameter('id');
            $this->kode_kegiatan = $kode_kegiatan;
            $this->setLayout('kosong');
        }
    }

//    ticket #33 - javascript dropdown list data waiting list
//    ticket #34 - proses set prioritas
//    created at 24juni2015        
    public function executeSetPrioritas() {
        if ($this->getRequest()->getMethod() == sfRequest::POST) {
            $prioritas = $this->getRequestParameter('prioritas');
            $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
            $id_waiting = $this->getRequestParameter('id_waiting');
            $prio_sekarang = $this->getRequestParameter('prio_sekarang');

            try {
                $c = new Criteria();
                $c->add(WaitingListPeer::ID_WAITING, $id_waiting);
                $waitinglist = WaitingListPeer::doSelectOne($c);

                $query = "update " . sfConfig::get('app_default_schema') . ".waitinglist "
                        . "set prioritas = " . $prio_sekarang . " "
                        . "where prioritas = " . $prioritas . " "
                        . "and kegiatan_code = '" . $kode_kegiatan . "' ";
                $con = Propel::getConnection();
                $stmt = $con->prepareStatement($query);
                $stmt->executeQuery();

                $query = "update " . sfConfig::get('app_default_schema') . ".waitinglist "
                        . "set prioritas = " . $prioritas . " "
                        . "where id_waiting = " . $id_waiting . " "
                        . "and kegiatan_code = '" . $kode_kegiatan . "' ";
                $con = Propel::getConnection();
                $stmt = $con->prepareStatement($query);
                $stmt->executeQuery();
                $this->setFlash('berhasil', 'Prioritas sudah berhasil dirubah');


                budgetLogger::log('Mengubah set Prioritas data Waiting List PU Kegiatan ' . $waitinglist->getKegiatanCode() . ' Komponen ' . $waitinglist->getKomponenName() . ' ' . $waitinglist->getKomponenLokasi() . '  id::' . $waitinglist->getIdWaiting());
            } catch (Exception $exc) {
                $this->setFlash('gagal', 'Gagal karena ' . $exc->getMessage());
            }
            $this->redirect('waitinglist/waitinglist');
        }
    }

    public function executeWaitingedit() {
        if ($this->getRequestParameter('edit') == md5('ubah')) {
            $id_waiting = $this->getRequestParameter('id');

            $c = new Criteria();
            $c->add(WaitingListPeer::ID_WAITING, $id_waiting);
            $waitinglist = WaitingListPeer::doSelectOne($c);
            if ($waitinglist) {
                $this->waitinglist = $waitinglist;
                $tahunInput = $waitinglist->getTahunInput();
            }

            $d = new Criteria();
            $d->add(MasterKegiatanPeer::UNIT_ID, $waitinglist->getUnitId());
            $d->add(MasterKegiatanPeer::KODE_KEGIATAN, $waitinglist->getKegiatanCodeLama());
            $d->addAscendingOrderByColumn(MasterKegiatanPeer::KODE_KEGIATAN);
            $rs_masterkegiatan = MasterKegiatanPeer::doSelect($d);
            $this->rs_masterkegiatan = $rs_masterkegiatan;

            $e = new Criteria();
            $e->addAscendingOrderByColumn(SatuanPeer::SATUAN_NAME);
            $rs_satuan = SatuanPeer::doSelect($e);
            $this->rs_satuan = $rs_satuan;

            $f = new Criteria();
            $f->add(KomponenRekeningPeer::KOMPONEN_ID, $waitinglist->getKomponenId());
            $f->add(KomponenRekeningPeer::REKENING_CODE, '', Criteria::ALT_NOT_EQUAL);
            $f->addAscendingOrderByColumn(KomponenRekeningPeer::REKENING_CODE);
            $rs_rekening = KomponenRekeningPeer::doSelect($f);
            $this->rs_rekening = $rs_rekening;

            $f = new Criteria();
            $f->addAscendingOrderByColumn(JasmasPeer::NAMA);
            $rs_jasmas = JasmasPeer::doSelect($f);
            $this->rs_jasmas = $rs_jasmas;

            $kode_rka = 'WL' . $waitinglist->getUnitId() . '.' . $waitinglist->getKegiatanCodeLama() . '.' . $waitinglist->getIdWaiting();

            $g = new Criteria();
            $g->add(HistoryPekerjaanV2Peer::KODE_RKA, $kode_rka);
            //$g->addAnd(HistoryPekerjaanV2Peer::TAHUN, sfConfig::get('app_tahun_default'));
            if ($tahunInput) {
                $g->addAnd(HistoryPekerjaanV2Peer::TAHUN, $tahunInput);
            } else {
                $g->addAnd(HistoryPekerjaanV2Peer::TAHUN, sfConfig::get('app_tahun_default'));
            }
            $g->addAnd(HistoryPekerjaanV2Peer::STATUS_HAPUS, FALSE);
            $g->addAscendingOrderByColumn(HistoryPekerjaanV2Peer::ID_HISTORY);
            $geojson = HistoryPekerjaanV2Peer::doSelect($g);
            if ($geojson) {
                $this->rs_geojson = $geojson;
            }

            $h = new Criteria();
            $h->setDistinct(HistoryPekerjaanV2Peer::LOKASI);
            $h->add(HistoryPekerjaanV2Peer::STATUS_HAPUS, FALSE);
            $h->add(HistoryPekerjaanV2Peer::TAHUN, 2015, Criteria::GREATER_THAN);
            $sub = "char_length(lokasi)>10";
            $h->addAnd(HistoryPekerjaanV2Peer::LOKASI, $sub, Criteria::CUSTOM);
            $h->addAscendingOrderByColumn(HistoryPekerjaanV2Peer::LOKASI);
            $this->rs_jalan = $rs_jalan = HistoryPekerjaanV2Peer::doSelect($h);
        }
        if ($this->getRequestParameter('simpan') == 'simpan') {
            $id_waiting = $this->getRequestParameter('id');
            $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');

            $c_waiting = new Criteria();
            $c_waiting->add(WaitingListPeer::ID_WAITING, $id_waiting);
            $waitinglist = WaitingListPeer::doSelectOne($c_waiting);

            $c_komponen = new Criteria();
            $c_komponen->add(KomponenPeer::KOMPONEN_ID, $waitinglist->getKomponenId());
            $komponen = KomponenPeer::doSelectOne($c_komponen);
            if (!$komponen) {
                $this->setFlash('gagal', 'Komponen tidak ada. Silahkan mengganti komponen ini dengan mengambil komponen baru');
                return $this->redirect("waitinglist/waitingedit?id=$id_waiting&edit=" . md5('ubah'));
            } else {
                if (round($waitinglist->getKomponenHarga() - $komponen->getKomponenHarga()) <> 0) {
                    $this->setFlash('gagal', 'Komponen terjadi perubahan harga. Silahkan mengganti komponen ini dengan mengambil komponen baru dan harga baru');
                    return $this->redirect("waitinglist/waitingedit?id=$id_waiting&edit=" . md5('ubah'));
                }
            }

            $is_musrenbang = 'FALSE';
            if (is_null($this->getRequestParameter('musrenbang'))) {
                $is_musrenbang = 'FALSE';
            } else if ($this->getRequestParameter('musrenbang') == 1) {
                $is_musrenbang = 'TRUE';
            }

            if (!$this->getRequestParameter('lokasi_jalan') && !$this->getRequestParameter('lokasi_lama')) {
                $this->setFlash('gagal', 'Lokasi Belum Dipilih');
                return $this->redirect("waitinglist/waitingedit?id=$id_waiting&edit=" . md5('ubah'));
            }

            $nilai_ee = $this->getRequestParameter('nilai_ee');
            if ($nilai_ee == '') {
                $nilai_ee = 0;
            }

            if (!$this->getRequestParameter('kode_kegiatan')) {
                $this->setFlash('gagal', 'Kode Kegiatan Belum Dipilih');
                return $this->redirect("waitinglist/waitingedit?id=$id_waiting&edit=" . md5('ubah'));
            }
            if (!$this->getRequestParameter('subtitle')) {
                $this->setFlash('gagal', 'Subtitle Belum Dipilih');
                return $this->redirect("waitinglist/waitingedit?id=$id_waiting&edit=" . md5('ubah'));
            } else {
                $subtitle = $this->getRequestParameter('subtitle');
            }
            if (!$this->getRequestParameter('rekening')) {
                $this->setFlash('gagal', 'Rekening Belum Dipilih');
                return $this->redirect("waitinglist/waitingedit?id=$id_waiting&edit=" . md5('ubah'));
            } else {
                $rekening = $this->getRequestParameter('rekening');
            }
            if ((!$this->getRequestParameter('kecamatan') || !$this->getRequestParameter('kelurahan'))) {
                $this->setFlash('gagal', 'Untuk komponen Fisik, silahkan mengisi keterangan Kecamatan & Kelurahan');
                return $this->redirect("waitinglist/waitingedit?id=$id_waiting&edit=" . md5('ubah'));
            } else {
                $lokasi_kec = $this->getRequestParameter('kecamatan');
                $lokasi_kel = $this->getRequestParameter('kelurahan');
                $kec = new Criteria();
                $kec->add(KecamatanPeer::ID, $lokasi_kec);
                $kec->addAscendingOrderByColumn(KecamatanPeer::NAMA);
                $rs_kec = KecamatanPeer::doSelectOne($kec);
                if ($rs_kec) {
                    $kecamatan = $rs_kec->getNama();
                }

                $kel = new Criteria();
                $kel->add(KelurahanKecamatanPeer::OID, $lokasi_kel);
                $kel->addAscendingOrderByColumn(KelurahanKecamatanPeer::NAMA_KECAMATAN);
                $rs_kel = KelurahanKecamatanPeer::doSelectOne($kel);
                if ($rs_kel) {
                    $kelurahan = $rs_kel->getNamaKelurahan();
                } else {
                    $kecamatan = '';
                    $kelurahan = '';
                }
            }

            $jasmas = $this->getRequestParameter('jasmas');

            if ($this->getRequestParameter('keterangan')) {
                $keterangan = $this->getRequestParameter('keterangan');
            } else {
                if ($nilai_ee == 0) {
                    $this->setFlash('gagal', 'Isi keterangan apabila tidak ada nilai ee');
                    return $this->redirect("waitinglist/waitingedit?id=$id_waiting&edit=" . md5('ubah'));
                }
            }

//lokasi
            $keisi = 0;
            $lokasi_baru = '';

//                    ambil lama
            $lokasi_lama = $this->getRequestParameter('lokasi_lama');

            if (count($lokasi_lama) > 0) {
                foreach ($lokasi_lama as $value_lokasi_lama) {
                    $c_cari_lokasi = new Criteria();
                    $c_cari_lokasi->add(HistoryPekerjaanV2Peer::LOKASI, $value_lokasi_lama);
                    $c_cari_lokasi->addAnd(HistoryPekerjaanV2Peer::STATUS_HAPUS, FALSE);
                    $dapat_lokasi_lama = HistoryPekerjaanV2Peer::doSelectOne($c_cari_lokasi);
                    if ($dapat_lokasi_lama) {

                        $jalan_fix = '';
                        $gang_fix = '';
                        $nomor_fix = '';
                        $rw_fix = '';
                        $rt_fix = '';
                        $keterangan_fix = '';
                        $tempat_fix = '';

                        $jalan_lama = $dapat_lokasi_lama->getJalan();
                        $gang_lama = $dapat_lokasi_lama->getGang();
                        $nomor_lama = $dapat_lokasi_lama->getNomor();
                        $rw_lama = $dapat_lokasi_lama->getRw();
                        $rt_lama = $dapat_lokasi_lama->getRt();
                        $keterangan_lama = $dapat_lokasi_lama->getKeterangan();
                        $tempat_lama = $dapat_lokasi_lama->getTempat();

                        if ($jalan_lama <> '') {
                            $jalan_fix = 'JL. ' . strtoupper($jalan_lama) . ' ';
                        }

                        if ($tempat_lama <> '') {
                            $tempat_fix = '(' . strtoupper($tempat_lama) . ') ';
                        }

                        if ($gang_lama <> '') {
                            $gang_fix = $gang_lama . ' ';
                        }

                        if ($nomor_lama <> '') {
                            $nomor_fix = 'NO ' . strtoupper($nomor_lama) . ' ';
                        }

                        if ($rw_lama <> '') {
                            $rw_fix = 'RW ' . strtoupper($rw_lama) . ' ';
                        }

                        if ($rt_lama <> '') {
                            $rt_fix = 'RT ' . strtoupper($rt_lama) . ' ';
                        }

                        if ($keterangan_lama <> '') {
                            $keterangan_fix = '' . strtoupper($keterangan_lama) . ' ';
                        }

                        if ($keisi == 0) {
                            $lokasi_baru = $tempat_fix . '' . $jalan_fix . '' . $gang_fix . '' . $nomor_fix . '' . $rw_fix . '' . $rt_fix . '' . $keterangan_fix;
                            $keisi++;
                        } else {
                            $lokasi_baru = $lokasi_baru . ', ' . $tempat_fix . '' . $jalan_fix . '' . $gang_fix . '' . $nomor_fix . '' . $rw_fix . '' . $rt_fix . '' . $keterangan_fix;
                            $keisi++;
                        }
                    }
                }
            }

//                    buat baru

            $lokasi_jalan = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_jalan')));
            $lokasi_gang = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_gang')));
            $tipe_gang = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('tipe_gang')));
            $lokasi_nomor = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_nomor')));
            $lokasi_rw = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_rw')));
            $lokasi_rt = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_rt')));
            $lokasi_keterangan = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_keterangan')));
            $lokasi_tempat = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_tempat')));

            $total_array_lokasi = count($lokasi_jalan);

            for ($i = 0; $i < $total_array_lokasi; $i++) {
                $jalan_fix = '';
                $gang_fix = '';
                $tipe_gang_fix = '';
                $nomor_fix = '';
                $rw_fix = '';
                $rt_fix = '';
                $keterangan_fix = '';
                $tempat_fix = '';
                if (trim($lokasi_jalan[$i]) <> '' || trim($lokasi_tempat[$i]) <> '') {

                    if (trim($lokasi_jalan[$i]) <> '') {
                        $jalan_fix = 'JL. ' . strtoupper(trim($lokasi_jalan[$i])) . ' ';
                    }

                    if (trim($lokasi_tempat[$i]) <> '') {
                        $tempat_fix = '(' . strtoupper(trim($lokasi_tempat[$i])) . ') ';
                    }

                    if (trim($tipe_gang[$i]) <> '') {
                        $tipe_gang_fix = strtoupper(trim($tipe_gang[$i])) . '. ';
                    } else {
                        $tipe_gang_fix = 'GG. ';
                    }

                    if (trim($lokasi_gang[$i]) <> '') {
                        $gang_fix = $tipe_gang_fix . '' . strtoupper(trim($lokasi_gang[$i])) . ' ';
                    }

                    if (trim($lokasi_nomor[$i]) <> '') {
                        $nomor_fix = 'NO ' . strtoupper(trim($lokasi_nomor[$i])) . ' ';
                    }

                    if (trim($lokasi_rw[$i]) <> '') {
                        $rw_fix = 'RW ' . strtoupper(trim($lokasi_rw[$i])) . ' ';
                    }

                    if (trim($lokasi_rt[$i]) <> '') {
                        $rt_fix = 'RT ' . strtoupper(trim($lokasi_rt[$i])) . ' ';
                    }

                    if (trim($lokasi_keterangan[$i]) <> '') {
                        $keterangan_fix = '' . strtoupper(trim($lokasi_keterangan[$i])) . ' ';
                    }

                    if ($keisi == 0) {
                        $lokasi_baru = $tempat_fix . '' . $jalan_fix . '' . $gang_fix . '' . $nomor_fix . '' . $rw_fix . '' . $rt_fix . '' . $keterangan_fix;
                        $keisi++;
                    } else {
                        $lokasi_baru = $lokasi_baru . ', ' . $tempat_fix . '' . $jalan_fix . '' . $gang_fix . '' . $nomor_fix . '' . $rw_fix . '' . $rt_fix . '' . $keterangan_fix;
                        $keisi++;
                    }
                }
            }
            if ($keisi == 0) {
                $this->setFlash('gagal', 'Lokasi Belum Dipilih');
                return $this->redirect("waitinglist/waitingedit?id=$id_waiting&edit=" . md5('ubah'));
            }
//lokasi

            if ($lokasi_baru == '') {
                $detail_name = '';
            } else {
                $detail_name = '(' . $lokasi_baru . ')';
            }

            $volume = 0;
            $keterangan_koefisien = '';
            if ($this->getRequestParameter('vol1') || $this->getRequestParameter('vol2') || $this->getRequestParameter('vol3') || $this->getRequestParameter('vol4')) {
                $vol1 = $this->getRequestParameter('vol1');
                $keterangan_koefisien = $this->getRequestParameter('vol1') . ' ' . $this->getRequestParameter('volume1');
                if ($this->getRequestParameter('vol2') == '') {
                    $vol2 = 1;
                    $volume = $this->getRequestParameter('vol1') * $vol2;
                } else if (!$this->getRequestParameter('vol2') == '') {
                    $vol2 = $this->getRequestParameter('vol2');
                    $volume = $this->getRequestParameter('vol1') * $this->getRequestParameter('vol2');
                    $keterangan_koefisien = $this->getRequestParameter('vol1') . ' ' . $this->getRequestParameter('volume1') . ' X ' . $this->getRequestParameter('vol2') . ' ' . $this->getRequestParameter('volume2');
                }
                if ($this->getRequestParameter('vol3') == '') {
                    $vol3 = 1;
                    $volume = $volume * $vol3;
                } else if (!$this->getRequestParameter('vol3') == '') {
                    $vol3 = $this->getRequestParameter('vol3');
                    $volume = $this->getRequestParameter('vol1') * $this->getRequestParameter('vol2') * $this->getRequestParameter('vol3');
                    $keterangan_koefisien = $this->getRequestParameter('vol1') . ' ' . $this->getRequestParameter('volume1') . ' X ' . $this->getRequestParameter('vol2') . ' ' . $this->getRequestParameter('volume2') . ' X ' . $this->getRequestParameter('vol3') . ' ' . $this->getRequestParameter('volume3');
                }
                if ($this->getRequestParameter('vol4') == '') {
                    $vol4 = 1;
                    $volume = $volume * $vol4;
                } else if (!$this->getRequestParameter('vol4') == '') {
                    $vol4 = $this->getRequestParameter('vol4');
                    $volume = $this->getRequestParameter('vol1') * $this->getRequestParameter('vol2') * $this->getRequestParameter('vol3') * $this->getRequestParameter('vol4');
                    $keterangan_koefisien = $this->getRequestParameter('vol1') . ' ' . $this->getRequestParameter('volume1') . ' X ' . $this->getRequestParameter('vol2') . ' ' . $this->getRequestParameter('volume2') . ' X ' . $this->getRequestParameter('vol3') . ' ' . $this->getRequestParameter('volume3') . ' X ' . $this->getRequestParameter('vol4') . ' ' . $this->getRequestParameter('volume4');
                }
            }

            $sekarang = date('Y-m-d H:i:s');

            $c = new Criteria();
            $c->add(WaitingListPeer::ID_WAITING, $id_waiting);
            $waitinglist = WaitingListPeer::doSelectOne($c);

            $con = Propel::getConnection();
            $con->begin();
            try {
                $query = "update " . sfConfig::get('app_default_schema') . ".waitinglist "
                        . "set koefisien='$keterangan_koefisien', volume=$volume, komponen_lokasi = '$detail_name', subtitle='$subtitle', "
                        . "kegiatan_code = '$kode_kegiatan', nilai_ee = $nilai_ee, keterangan = '$keterangan',updated_at = '$sekarang', komponen_rekening = '$rekening', "
                        . "kode_jasmas = '$jasmas', kecamatan = '$kecamatan', kelurahan = '$kelurahan', is_musrenbang = '" . $is_musrenbang . "' "
                        . "where id_waiting = $id_waiting";
                $stmt = $con->prepareStatement($query);
                $stmt->executeQuery();
                $con->commit();

                budgetLogger::log('mengedit Data Waiting List untuk kegiatan ' . $kode_kegiatan . ' Komponen ' . $waitinglist->getKomponenName() . ' ' . $detail_name . '  id::' . $waitinglist->getIdWaiting());


                $rd_cari = new Criteria();
                $rd_cari->add(WaitingListPeer::ID_WAITING, $id_waiting);
                $rd_dapat = WaitingListPeer::doSelectOne($rd_cari);

                $kode_rka_fix = 'XXX2600' . '.' . $kode_kegiatan . '.' . $id_waiting;

                $c_cari_history = new Criteria();
                $c_cari_history->addAnd(HistoryPekerjaanV2Peer::KODE_RKA, $kode_rka_fix);
                $c_cari_history->addAnd(HistoryPekerjaanV2Peer::TAHUN, sfConfig::get('app_tahun_default'));
                $dapat_history = HistoryPekerjaanV2Peer::doSelect($c_cari_history);
                if ($dapat_history) {
                    foreach ($dapat_history as $value_history) {
                        $id_history = $value_history->getIdHistory();

                        $c_cari_hapus_history = new Criteria();
                        $c_cari_hapus_history->add(HistoryPekerjaanV2Peer::ID_HISTORY, $id_history);
                        $dapat_history_hapus = HistoryPekerjaanV2Peer::doSelectOne($c_cari_hapus_history);
                        if ($dapat_history_hapus) {
                            $dapat_history_hapus->delete();
                        }
                    }
                }

                $keisi = 0;

//                    ambil lama
                $lokasi_lama = $this->getRequestParameter('lokasi_lama');

                if (count($lokasi_lama) > 0) {
                    foreach ($lokasi_lama as $value_lokasi_lama) {
                        $c_cari_lokasi = new Criteria();
                        $c_cari_lokasi->add(HistoryPekerjaanV2Peer::LOKASI, $value_lokasi_lama);
                        $c_cari_lokasi->addAnd(HistoryPekerjaanV2Peer::STATUS_HAPUS, FALSE);
                        $dapat_lokasi_lama = HistoryPekerjaanV2Peer::doSelectOne($c_cari_lokasi);
                        if ($dapat_lokasi_lama) {

                            $jalan_fix = '';
                            $gang_fix = '';
                            $nomor_fix = '';
                            $rw_fix = '';
                            $rt_fix = '';
                            $keterangan_fix = '';
                            $tempat_fix = '';

                            $jalan_lama = $dapat_lokasi_lama->getJalan();
                            $gang_lama = $dapat_lokasi_lama->getGang();
                            $nomor_lama = $dapat_lokasi_lama->getNomor();
                            $rw_lama = $dapat_lokasi_lama->getRw();
                            $rt_lama = $dapat_lokasi_lama->getRt();
                            $keterangan_lama = $dapat_lokasi_lama->getKeterangan();
                            $tempat_lama = $dapat_lokasi_lama->getTempat();

                            if ($jalan_lama <> '') {
                                $jalan_fix = 'JL. ' . strtoupper($jalan_lama) . ' ';
                            }

                            if ($tempat_lama <> '') {
                                $tempat_fix = '(' . strtoupper($tempat_lama) . ') ';
                            }

                            if ($gang_lama <> '') {
                                $gang_fix = $gang_lama . ' ';
                            }

                            if ($nomor_lama <> '') {
                                $nomor_fix = 'NO ' . strtoupper($nomor_lama) . ' ';
                            }

                            if ($rw_lama <> '') {
                                $rw_fix = 'RW ' . strtoupper($rw_lama) . ' ';
                            }

                            if ($rt_lama <> '') {
                                $rt_fix = 'RT ' . strtoupper($rt_lama) . ' ';
                            }

                            if ($keterangan_lama <> '') {
                                $keterangan_fix = '' . strtoupper($keterangan_lama) . ' ';
                            }

                            $lokasi_baru = $tempat_fix . '' . $jalan_fix . '' . $gang_fix . '' . $nomor_fix . '' . $rw_fix . '' . $rt_fix . '' . $keterangan_fix;

                            $rka_lokasi = 'XXX2600' . '.' . $kode_kegiatan . '.' . $id_waiting;
                            $komponen_lokasi = $rd_dapat->getKomponenName() . ' ' . $rd_dapat->getKomponenLokasi();
                            $kecamatan_lokasi = $rd_dapat->getKecamatan();
                            $kelurahan_lokasi = $rd_dapat->getKelurahan();
                            $lokasi_per_titik = $lokasi_baru;

                            $c_insert_gis = new HistoryPekerjaanV2();
                            $c_insert_gis->setTahun(sfConfig::get('app_tahun_default'));
                            $c_insert_gis->setKodeRka($rka_lokasi);
                            $c_insert_gis->setStatusHapus(FALSE);
                            $c_insert_gis->setJalan(strtoupper($jalan_lama));
                            $c_insert_gis->setGang(strtoupper($gang_lama));
                            $c_insert_gis->setNomor(strtoupper($nomor_lama));
                            $c_insert_gis->setRw(strtoupper($rw_lama));
                            $c_insert_gis->setRt(strtoupper($rt_lama));
                            $c_insert_gis->setKeterangan(strtoupper($keterangan_lama));
                            $c_insert_gis->setTempat(strtoupper($tempat_lama));
                            $c_insert_gis->setKomponen($komponen_lokasi);
                            $c_insert_gis->setKecamatan($kecamatan_lokasi);
                            $c_insert_gis->setKelurahan($kelurahan_lokasi);
                            $c_insert_gis->setLokasi($lokasi_per_titik);
                            $c_insert_gis->save();
                        }
                    }
                }

//                    buat baru
                $lokasi_jalan = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_jalan')));
                $lokasi_gang = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_gang')));
                $tipe_gang = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('tipe_gang')));
                $lokasi_nomor = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_nomor')));
                $lokasi_rw = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_rw')));
                $lokasi_rt = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_rt')));
                $lokasi_keterangan = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_keterangan')));
                $lokasi_tempat = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_tempat')));

                $total_array_lokasi = count($lokasi_jalan);

                for ($i = 0; $i < $total_array_lokasi; $i++) {
                    $jalan_fix = '';
                    $gang_fix = '';
                    $tipe_gang_fix = '';
                    $nomor_fix = '';
                    $rw_fix = '';
                    $rt_fix = '';
                    $keterangan_fix = '';
                    $tempat_fix = '';
                    if (trim($lokasi_jalan[$i]) <> '' || trim($lokasi_tempat[$i]) <> '') {

                        if (trim($lokasi_jalan[$i]) <> '') {
                            $jalan_fix = 'JL. ' . strtoupper(trim($lokasi_jalan[$i])) . ' ';
                        }

                        if (trim($lokasi_tempat[$i]) <> '') {
                            $tempat_fix = '(' . strtoupper(trim($lokasi_tempat[$i])) . ') ';
                        }

                        if (trim($tipe_gang[$i]) <> '') {
                            $tipe_gang_fix = strtoupper(trim($tipe_gang[$i])) . '. ';
                        } else {
                            $tipe_gang_fix = 'GG. ';
                        }

                        if (trim($lokasi_gang[$i]) <> '') {
                            $gang_fix = $tipe_gang_fix . '' . strtoupper(trim($lokasi_gang[$i])) . ' ';
                        }

                        if (trim($lokasi_nomor[$i]) <> '') {
                            $nomor_fix = 'NO ' . strtoupper(trim($lokasi_nomor[$i])) . ' ';
                        }

                        if (trim($lokasi_rw[$i]) <> '') {
                            $rw_fix = 'RW ' . strtoupper(trim($lokasi_rw[$i])) . ' ';
                        }

                        if (trim($lokasi_rt[$i]) <> '') {
                            $rt_fix = 'RT ' . strtoupper(trim($lokasi_rt[$i])) . ' ';
                        }

                        if (trim($lokasi_keterangan[$i]) <> '') {
                            $keterangan_fix = '' . strtoupper(trim($lokasi_keterangan[$i])) . ' ';
                        }


                        $lokasi_baru = $tempat_fix . '' . $jalan_fix . '' . $gang_fix . '' . $nomor_fix . '' . $rw_fix . '' . $rt_fix . '' . $keterangan_fix;

                        $rka_lokasi = 'XXX2600' . '.' . $kode_kegiatan . '.' . $id_waiting;
                        $komponen_lokasi = $rd_dapat->getKomponenName() . ' ' . $rd_dapat->getKomponenLokasi();
                        $kecamatan_lokasi = $rd_dapat->getKecamatan();
                        $kelurahan_lokasi = $rd_dapat->getKelurahan();
                        $lokasi_per_titik = $lokasi_baru;

                        $c_insert_gis = new HistoryPekerjaanV2();
                        $c_insert_gis->setTahun(sfConfig::get('app_tahun_default'));
                        $c_insert_gis->setKodeRka($rka_lokasi);
                        $c_insert_gis->setStatusHapus(FALSE);
                        $c_insert_gis->setJalan(strtoupper(trim($lokasi_jalan[$i])));
                        $c_insert_gis->setGang(strtoupper($gang_fix));
                        $c_insert_gis->setNomor(strtoupper(trim($lokasi_nomor[$i])));
                        $c_insert_gis->setRw(strtoupper(trim($lokasi_rw[$i])));
                        $c_insert_gis->setRt(strtoupper(trim($lokasi_rt[$i])));
                        $c_insert_gis->setKeterangan(strtoupper(trim($lokasi_keterangan[$i])));
                        $c_insert_gis->setTempat(strtoupper(trim($lokasi_tempat[$i])));
                        $c_insert_gis->setKomponen($komponen_lokasi);
                        $c_insert_gis->setKecamatan($kecamatan_lokasi);
                        $c_insert_gis->setKelurahan($kelurahan_lokasi);
                        $c_insert_gis->setLokasi($lokasi_per_titik);
                        $c_insert_gis->save();
                    }
                }
                $this->setFlash('berhasil', 'Perubahan berhasil tersimpan');
            } catch (Exception $exc) {
                $con->rollback();
                $this->setFlash('gagal', 'Gagal Karena ' . $exc->getMessage());
            }

            return $this->redirect('waitinglist/waitinglist');
        }
    }

//    ticket #37 - proses insert waiting list by upload
//    created at 24juni2015      
    public function executeSeluruhwaitinglist() {
        $this->processFiltersseluruhwaitinglist();
        $this->filters = $this->getUser()->getAttributeHolder()->getAll('sf_admin/seluruhwaitinglist/filters');

        $pagers = new sfPropelPager('WaitingList', 20);
        $c = new Criteria();
        $c->addDescendingOrderByColumn(WaitingListPeer::UPDATED_AT);
        $this->addFiltersCriteriaseluruhwaitinglist($c);

        $pagers->setCriteria($c);
        $pagers->setPage($this->getRequestParameter('page', 1));
        $pagers->init();

        $this->pager = $pagers;
    }

//    ticket #37 - proses insert waiting list by upload
//    ticket #37 - proses insert waiting list by upload
//    created at 24juni2015      
    protected function processFiltersseluruhwaitinglist() {
        if ($this->getRequest()->hasParameter('filter')) {
            $filters = $this->getRequestParameter('filters');
            $this->getUser()->getAttributeHolder()->removeNamespace('sf_admin/seluruhwaitinglist/filters');
            $this->getUser()->getAttributeHolder()->add($filters, 'sf_admin/seluruhwaitinglist/filters');
        }
    }

//    ticket #37 - proses insert waiting list by upload
//    ticket #37 - proses insert waiting list by upload
//    created at 24juni2015      
    protected function addFiltersCriteriaseluruhwaitinglist($c) {
        if (isset($this->filters['select'])) {
            if ($this->filters['select'] == 1) {
                if (isset($this->filters['nama_komponen_is_empty'])) {
                    $criterion = $c->getNewCriterion(WaitingListPeer::KOMPONEN_NAME, '');
                    $criterion->addOr($c->getNewCriterion(WaitingListPeer::KOMPONEN_NAME, null, Criteria::ISNULL));
                    $c->add($criterion);
                } else if (isset($this->filters['komponen']) && $this->filters['komponen'] !== '') {
                    $kata = '%' . $this->filters['komponen'] . '%';
                    $c->add(WaitingListPeer::KOMPONEN_NAME, strtr($kata, '*', '%'), Criteria::ILIKE);
                }
            } elseif ($this->filters['select'] == 2) {
                if (isset($this->filters['nama_lokasi_is_empty'])) {
                    $criterion = $c->getNewCriterion(WaitingListPeer::KOMPONEN_LOKASI, '');
                    $criterion->addOr($c->getNewCriterion(WaitingListPeer::KOMPONEN_LOKASI, null, Criteria::ISNULL));
                    $c->add($criterion);
                } else if (isset($this->filters['komponen']) && $this->filters['komponen'] !== '') {
                    $kata = '%' . $this->filters['komponen'] . '%';
                    $c->add(WaitingListPeer::KOMPONEN_LOKASI, strtr($kata, '*', '%'), Criteria::ILIKE);
                }
            } elseif ($this->filters['select'] == 3) {
                if (isset($this->filters['kode_kegiatan_is_empty'])) {
                    $criterion = $c->getNewCriterion(WaitingListPeer::KEGIATAN_CODE, '');
                    $criterion->addOr($c->getNewCriterion(WaitingListPeer::KEGIATAN_CODE, null, Criteria::ISNULL));
                    $c->add($criterion);
                } else if (isset($this->filters['komponen']) && $this->filters['komponen'] !== '') {
                    $kata = '%' . $this->filters['komponen'] . '%';
                    $c->add(WaitingListPeer::KEGIATAN_CODE, strtr($kata, '*', '%'), Criteria::ILIKE);
                }
            } else {
                if (isset($this->filters['komponen_is_empty'])) {
                    echo 'is empty';
                    $criterion = $c->getNewCriterion(WaitingListPeer::KOMPONEN_NAME, '');
                    $criterion->addOr($c->getNewCriterion(WaitingListPeer::KOMPONEN_NAME, null, Criteria::ISNULL));
                    $c->add($criterion);
                }
            }
        }
    }

//    ticket #37 - proses insert waiting list by upload
//    ticket #38 - proses delete waiting list
//    created at 24juni2015          
    function executeProseshapus() {
        $sekarang = date('Y-m-d H:i:s');
        $id_waiting = $this->getRequestParameter('id');
        if (is_null($id_waiting) || ($this->getRequest()->hasParameter('key') != md5('hapus_waiting'))) {
            $this->setFlash('gagal', 'Gagal karena parameter kurang');
        } else {
            $con = Propel::getConnection();
            $con->begin();
            try {
                $c_waiting = new Criteria();
                $c_waiting->add(WaitingListPeer::ID_WAITING, $id_waiting);
                $waitinglist = WaitingListPeer::doSelectOne($c_waiting);
                if ($waitinglist) {
                    $waitinglist->setUpdatedAt($sekarang);
                    $waitinglist->setStatusHapus(TRUE);
                    $waitinglist->save();

                    budgetLogger::log('Menghapus data Waiting List PU Kegiatan ' . $waitinglist->getKegiatanCode() . ' Komponen ' . $waitinglist->getKomponenName() . ' ' . $waitinglist->getKomponenLokasi() . ' id::' . $waitinglist->getIdWaiting());
                }

                $kode_rka = $waitinglist->getUnitId() . '.' . $waitinglist->getKegiatanCode() . '.' . $id_waiting;

                $c_history = new Criteria();
                $c_history->add(HistoryPekerjaanV2Peer::KODE_RKA, $kode_rka);
                $historywaitinglist = HistoryPekerjaanV2Peer::doSelectOne($c_history);
                if ($historywaitinglist) {
                    $historywaitinglist->setStatusHapus(TRUE);
                    $historywaitinglist->save();

                    budgetLogger::log('Menghapus data History Waiting List PU Kegiatan ' . $waitinglist->getKegiatanCode() . ' Komponen ' . $waitinglist->getKomponenName() . ' ' . $waitinglist->getKomponenLokasi() . ' kode_rka::' . $kode_rka);
                }

                $query_delete = "update " . sfConfig::get('app_default_gis') . ".geojsonlokasi_waitinglist "
                        . "set status_hapus = true, last_edit_time = '$sekarang' "
                        . "where id_waiting = $id_waiting";
                $stmt_delete = $con->prepareStatement($query_delete);
                $stmt_delete->executeQuery();

                $con->commit();
                $this->setFlash('berhasil', 'Menghapus Data Waiting List berhasil');
            } catch (Exception $exc) {
                $con->rollback();
                $this->setFlash('gagal', 'Gagal karena ' . $exc->getMessage());
            }
        }
        $this->redirect('waitinglist/waitinglist');
    }

//    ticket #38 - proses delete waiting list

    public function executeWaitingGoRKA() {

        if ($this->getRequestParameter('cek_rka') == md5('rka_waiting')) {
            $id_waiting = $this->getRequestParameter('id');

            $c = new Criteria();
            $c->add(WaitingListPeer::ID_WAITING, $id_waiting);
            if ($waitinglist = WaitingListPeer::doSelectOne($c)) {
                $this->waitinglist = $waitinglist;
                $tahunInput = $waitinglist->getTahunInput();
            }

            $d = new Criteria();
            $d->add(MasterKegiatanPeer::UNIT_ID, $waitinglist->getUnitId());
            $d->add(MasterKegiatanPeer::KODE_KEGIATAN, $waitinglist->getKegiatanCodeLama());
            $d->addAscendingOrderByColumn(MasterKegiatanPeer::KODE_KEGIATAN);
            $rs_masterkegiatan = MasterKegiatanPeer::doSelect($d);
            $this->rs_masterkegiatan = $rs_masterkegiatan;

            $e = new Criteria();
            $e->addAscendingOrderByColumn(SatuanPeer::SATUAN_NAME);
            $rs_satuan = SatuanPeer::doSelect($e);
            $this->rs_satuan = $rs_satuan;

            $f = new Criteria();
            $f->add(KomponenRekeningPeer::KOMPONEN_ID, $waitinglist->getKomponenIdLama());
            $f->add(KomponenRekeningPeer::REKENING_CODE, '', Criteria::ALT_NOT_EQUAL);
            $f->addAscendingOrderByColumn(KomponenRekeningPeer::REKENING_CODE);
            $rs_rekening = KomponenRekeningPeer::doSelect($f);
            $this->rs_rekening = $rs_rekening;

            $f = new Criteria();
            $f->addAscendingOrderByColumn(JasmasPeer::NAMA);
            $rs_jasmas = JasmasPeer::doSelect($f);
            $this->rs_jasmas = $rs_jasmas;

            $kode_rka = 'WL' . $waitinglist->getUnitId() . '.' . $waitinglist->getKegiatanCodeLama() . '.' . $waitinglist->getIdWaiting();

            $g = new Criteria();
            $g->add(HistoryPekerjaanV2Peer::KODE_RKA, $kode_rka);
            if ($tahunInput) {
                $g->addAnd(HistoryPekerjaanV2Peer::TAHUN, $tahunInput);
            } else {
                $g->addAnd(HistoryPekerjaanV2Peer::TAHUN, sfConfig::get('app_tahun_default'));
            }
            $g->addAnd(HistoryPekerjaanV2Peer::STATUS_HAPUS, FALSE);
            $g->addAscendingOrderByColumn(HistoryPekerjaanV2Peer::ID_HISTORY);
            $geojson = HistoryPekerjaanV2Peer::doSelect($g);
            if ($geojson) {
                $this->rs_geojson = $geojson;
            }

            $h = new Criteria();
            $h->setDistinct(HistoryPekerjaanV2Peer::LOKASI);
            $h->add(HistoryPekerjaanV2Peer::STATUS_HAPUS, FALSE);
            $h->add(HistoryPekerjaanV2Peer::TAHUN, 2015, Criteria::GREATER_THAN);
            $sub = "char_length(lokasi)>10";
            $h->addAnd(HistoryPekerjaanV2Peer::LOKASI, $sub, Criteria::CUSTOM);
            $h->addAscendingOrderByColumn(HistoryPekerjaanV2Peer::LOKASI);
            $this->rs_jalan = $rs_jalan = HistoryPekerjaanV2Peer::doSelect($h);
        }
        if ($this->getRequestParameter('simpan') == 'simpan') {

            $id_waiting = $this->getRequestParameter('id');
            $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
            $akrual_code = $this->getRequestParameter('akrual_code');

            $c_waiting = new Criteria();
            $c_waiting->add(WaitingListPeer::ID_WAITING, $id_waiting);
            $waitinglist = WaitingListPeer::doSelectOne($c_waiting);

            $c_komponen = new Criteria();
            $c_komponen->add(KomponenPeer::KOMPONEN_ID, $waitinglist->getKomponenId());
            $komponen = KomponenPeer::doSelectOne($c_komponen);
            if (!$komponen) {
                $this->setFlash('gagal', 'Komponen tidak ada. Silahkan mengganti komponen ini dengan mengambil komponen baru');
                return $this->redirect("waitinglist/waitingGoRKA?id=$id_waiting&cek_rka=" . md5('rka_waiting'));
            } else {
                if (round($waitinglist->getKomponenHarga() - $komponen->getKomponenHarga()) <> 0) {
                    $this->setFlash('gagal', 'Komponen terjadi perubahan harga. Silahkan mengganti komponen ini dengan mengambil komponen baru dan harga baru');
                    return $this->redirect("waitinglist/waitingGoRKA?id=$id_waiting&cek_rka=" . md5('rka_waiting'));
                }
            }

            $tipe_komponen = $komponen->getKomponenTipe();

            $is_musrenbang = 'FALSE';
            if (is_null($this->getRequestParameter('musrenbang'))) {
                $is_musrenbang = 'FALSE';
            } else if ($this->getRequestParameter('musrenbang') == 1) {
                $is_musrenbang = 'TRUE';
            }

            $c_cari_geojson = new Criteria();
            $c_cari_geojson->add(GeojsonlokasiWaitinglistPeer::ID_WAITING, $id_waiting);
            $c_cari_geojson->addAnd(GeojsonlokasiWaitinglistPeer::KEGIATAN_CODE, $kode_kegiatan);
            $c_cari_geojson->addAnd(GeojsonlokasiWaitinglistPeer::UNIT_ID, 'XXX2600');
            $c_cari_geojson->addAnd(GeojsonlokasiWaitinglistPeer::STATUS_HAPUS, FALSE);
            $dapat_geojson = GeojsonlokasiWaitinglistPeer::doSelect($c_cari_geojson);
            if (!$dapat_geojson) {
                $this->setFlash('gagal', 'Belum pernah ada Pemetaan pada GMAP. Silahkan koordinasi dengan Bagian Perancangan untuk Pemetaan GMAP.');
                return $this->redirect("waitinglist/waitingGoRKA?id=$id_waiting&cek_rka=" . md5('rka_waiting'));
            }

            if (!$this->getRequestParameter('lokasi_jalan') && !$this->getRequestParameter('lokasi_lama')) {
                $this->setFlash('gagal', 'Lokasi Belum Dipilih');
                return $this->redirect("waitinglist/waitingGoRKA?id=$id_waiting&cek_rka=" . md5('rka_waiting'));
            }

            $nilai_ee = $this->getRequestParameter('nilai_ee');
            if ($nilai_ee == '') {
                $nilai_ee = 0;
            }

            if (!$this->getRequestParameter('kode_kegiatan')) {
                $this->setFlash('gagal', 'Kode Kegiatan Belum Dipilih');
                return $this->redirect("waitinglist/waitingGoRKA?id=$id_waiting&cek_rka=" . md5('rka_waiting'));
            }
            if (!$this->getRequestParameter('subtitle')) {
                $this->setFlash('gagal', 'Subtitle Belum Dipilih');
                return $this->redirect("waitinglist/waitingGoRKA?id=$id_waiting&cek_rka=" . md5('rka_waiting'));
            } else {
                $subtitle = $this->getRequestParameter('subtitle');
            }
            if (!$this->getRequestParameter('rekening')) {
                $this->setFlash('gagal', 'Rekening Belum Dipilih');
                return $this->redirect("waitinglist/waitingGoRKA?id=$id_waiting&cek_rka=" . md5('rka_waiting'));
            } else {
                $rekening = $this->getRequestParameter('rekening');
            }
            if ((!$this->getRequestParameter('kecamatan') || !$this->getRequestParameter('kelurahan'))) {
                $this->setFlash('gagal', 'Untuk komponen Fisik, silahkan mengisi keterangan Kecamatan & Kelurahan');
                return $this->redirect("waitinglist/waitingGoRKA?id=$id_waiting&cek_rka=" . md5('rka_waiting'));
            } else {
                $lokasi_kec = $this->getRequestParameter('kecamatan');
                $lokasi_kel = $this->getRequestParameter('kelurahan');
                $kec = new Criteria();
                $kec->add(KecamatanPeer::ID, $lokasi_kec);
                $kec->addAscendingOrderByColumn(KecamatanPeer::NAMA);
                $rs_kec = KecamatanPeer::doSelectOne($kec);
                if ($rs_kec) {
                    $kecamatan = $rs_kec->getNama();
                }

                $kel = new Criteria();
                $kel->add(KelurahanKecamatanPeer::OID, $lokasi_kel);
                $kel->addAscendingOrderByColumn(KelurahanKecamatanPeer::NAMA_KECAMATAN);
                $rs_kel = KelurahanKecamatanPeer::doSelectOne($kel);
                if ($rs_kel) {
                    $kelurahan = $rs_kel->getNamaKelurahan();
                } else {
                    $kecamatan = '';
                    $kelurahan = '';
                }
            }

            $jasmas = $this->getRequestParameter('jasmas');

            if ($this->getRequestParameter('keterangan')) {
                $keterangan = $this->getRequestParameter('keterangan');
            } else {
                if ($nilai_ee == 0) {
                    $this->setFlash('gagal', 'Isi keterangan apabila tidak ada nilai ee');
                    return $this->redirect("waitinglist/waitingGoRKA?id=$id_waiting&cek_rka=" . md5('rka_waiting'));
                }
            }

//lokasi
            $keisi = 0;
            $lokasi_baru = '';

//                    ambil lama
            $lokasi_lama = $this->getRequestParameter('lokasi_lama');

            if (count($lokasi_lama) > 0) {
                foreach ($lokasi_lama as $value_lokasi_lama) {
                    $c_cari_lokasi = new Criteria();
                    $c_cari_lokasi->add(HistoryPekerjaanV2Peer::LOKASI, $value_lokasi_lama);
                    $c_cari_lokasi->addAnd(HistoryPekerjaanV2Peer::STATUS_HAPUS, FALSE);
                    $dapat_lokasi_lama = HistoryPekerjaanV2Peer::doSelectOne($c_cari_lokasi);
                    if ($dapat_lokasi_lama) {

                        $jalan_fix = '';
                        $gang_fix = '';
                        $nomor_fix = '';
                        $rw_fix = '';
                        $rt_fix = '';
                        $keterangan_fix = '';
                        $tempat_fix = '';

                        $jalan_lama = $dapat_lokasi_lama->getJalan();
                        $gang_lama = $dapat_lokasi_lama->getGang();
                        $nomor_lama = $dapat_lokasi_lama->getNomor();
                        $rw_lama = $dapat_lokasi_lama->getRw();
                        $rt_lama = $dapat_lokasi_lama->getRt();
                        $keterangan_lama = $dapat_lokasi_lama->getKeterangan();
                        $tempat_lama = $dapat_lokasi_lama->getTempat();

                        if ($jalan_lama <> '') {
                            $jalan_fix = 'JL. ' . strtoupper($jalan_lama) . ' ';
                        }

                        if ($tempat_lama <> '') {
                            $tempat_fix = '(' . strtoupper($tempat_lama) . ') ';
                        }

                        if ($gang_lama <> '') {
                            $gang_fix = $gang_lama . ' ';
                        }

                        if ($nomor_lama <> '') {
                            $nomor_fix = 'NO ' . strtoupper($nomor_lama) . ' ';
                        }

                        if ($rw_lama <> '') {
                            $rw_fix = 'RW ' . strtoupper($rw_lama) . ' ';
                        }

                        if ($rt_lama <> '') {
                            $rt_fix = 'RT ' . strtoupper($rt_lama) . ' ';
                        }

                        if ($keterangan_lama <> '') {
                            $keterangan_fix = '' . strtoupper($keterangan_lama) . ' ';
                        }

                        if ($keisi == 0) {
                            $lokasi_baru = $tempat_fix . '' . $jalan_fix . '' . $gang_fix . '' . $nomor_fix . '' . $rw_fix . '' . $rt_fix . '' . $keterangan_fix;
                            $keisi++;
                        } else {
                            $lokasi_baru = $lokasi_baru . ', ' . $tempat_fix . '' . $jalan_fix . '' . $gang_fix . '' . $nomor_fix . '' . $rw_fix . '' . $rt_fix . '' . $keterangan_fix;
                            $keisi++;
                        }
                    }
                }
            }

//                    buat baru

            $lokasi_jalan = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_jalan')));
            $lokasi_gang = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_gang')));
            $tipe_gang = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('tipe_gang')));
            $lokasi_nomor = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_nomor')));
            $lokasi_rw = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_rw')));
            $lokasi_rt = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_rt')));
            $lokasi_keterangan = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_keterangan')));
            $lokasi_tempat = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_tempat')));

            $total_array_lokasi = count($lokasi_jalan);

            for ($i = 0; $i < $total_array_lokasi; $i++) {
                $jalan_fix = '';
                $gang_fix = '';
                $tipe_gang_fix = '';
                $nomor_fix = '';
                $rw_fix = '';
                $rt_fix = '';
                $keterangan_fix = '';
                $tempat_fix = '';
                if (trim($lokasi_jalan[$i]) <> '' || trim($lokasi_tempat[$i]) <> '') {

                    if (trim($lokasi_jalan[$i]) <> '') {
                        $jalan_fix = 'JL. ' . strtoupper(trim($lokasi_jalan[$i])) . ' ';
                    }

                    if (trim($lokasi_tempat[$i]) <> '') {
                        $tempat_fix = '(' . strtoupper(trim($lokasi_tempat[$i])) . ') ';
                    }

                    if (trim($tipe_gang[$i]) <> '') {
                        $tipe_gang_fix = strtoupper(trim($tipe_gang[$i])) . '. ';
                    } else {
                        $tipe_gang_fix = 'GG. ';
                    }

                    if (trim($lokasi_gang[$i]) <> '') {
                        $gang_fix = $tipe_gang_fix . '' . strtoupper(trim($lokasi_gang[$i])) . ' ';
                    }

                    if (trim($lokasi_nomor[$i]) <> '') {
                        $nomor_fix = 'NO ' . strtoupper(trim($lokasi_nomor[$i])) . ' ';
                    }

                    if (trim($lokasi_rw[$i]) <> '') {
                        $rw_fix = 'RW ' . strtoupper(trim($lokasi_rw[$i])) . ' ';
                    }

                    if (trim($lokasi_rt[$i]) <> '') {
                        $rt_fix = 'RT ' . strtoupper(trim($lokasi_rt[$i])) . ' ';
                    }

                    if (trim($lokasi_keterangan[$i]) <> '') {
                        $keterangan_fix = '' . strtoupper(trim($lokasi_keterangan[$i])) . ' ';
                    }

                    if ($keisi == 0) {
                        $lokasi_baru = $tempat_fix . '' . $jalan_fix . '' . $gang_fix . '' . $nomor_fix . '' . $rw_fix . '' . $rt_fix . '' . $keterangan_fix;
                        $keisi++;
                    } else {
                        $lokasi_baru = $lokasi_baru . ', ' . $tempat_fix . '' . $jalan_fix . '' . $gang_fix . '' . $nomor_fix . '' . $rw_fix . '' . $rt_fix . '' . $keterangan_fix;
                        $keisi++;
                    }
                }
            }
            if ($keisi == 0) {
                $this->setFlash('gagal', 'Lokasi Belum Dipilih');
                return $this->redirect("waitinglist/waitingGoRKA?id=$id_waiting&cek_rka=" . md5('rka_waiting'));
            }
//lokasi

            if ($lokasi_baru == '') {
                $detail_name = '';
            } else {
                $detail_name = '(' . $lokasi_baru . ')';
            }
            $detail_name_rd = $lokasi_baru;

            $volume = 0;
            $keterangan_koefisien = '';
            if ($this->getRequestParameter('vol1') || $this->getRequestParameter('vol2') || $this->getRequestParameter('vol3') || $this->getRequestParameter('vol4')) {
                $vol1 = $this->getRequestParameter('vol1');
                $keterangan_koefisien = $this->getRequestParameter('vol1') . ' ' . $this->getRequestParameter('volume1');
                if ($this->getRequestParameter('vol2') == '') {
                    $vol2 = 1;
                    $volume = $this->getRequestParameter('vol1') * $vol2;
                } else if (!$this->getRequestParameter('vol2') == '') {
                    $vol2 = $this->getRequestParameter('vol2');
                    $volume = $this->getRequestParameter('vol1') * $this->getRequestParameter('vol2');
                    $keterangan_koefisien = $this->getRequestParameter('vol1') . ' ' . $this->getRequestParameter('volume1') . ' X ' . $this->getRequestParameter('vol2') . ' ' . $this->getRequestParameter('volume2');
                }
                if ($this->getRequestParameter('vol3') == '') {
                    $vol3 = 1;
                    $volume = $volume * $vol3;
                } else if (!$this->getRequestParameter('vol3') == '') {
                    $vol3 = $this->getRequestParameter('vol3');
                    $volume = $this->getRequestParameter('vol1') * $this->getRequestParameter('vol2') * $this->getRequestParameter('vol3');
                    $keterangan_koefisien = $this->getRequestParameter('vol1') . ' ' . $this->getRequestParameter('volume1') . ' X ' . $this->getRequestParameter('vol2') . ' ' . $this->getRequestParameter('volume2') . ' X ' . $this->getRequestParameter('vol3') . ' ' . $this->getRequestParameter('volume3');
                }
                if ($this->getRequestParameter('vol4') == '') {
                    $vol4 = 1;
                    $volume = $volume * $vol4;
                } else if (!$this->getRequestParameter('vol4') == '') {
                    $vol4 = $this->getRequestParameter('vol4');
                    $volume = $this->getRequestParameter('vol1') * $this->getRequestParameter('vol2') * $this->getRequestParameter('vol3') * $this->getRequestParameter('vol4');
                    $keterangan_koefisien = $this->getRequestParameter('vol1') . ' ' . $this->getRequestParameter('volume1') . ' X ' . $this->getRequestParameter('vol2') . ' ' . $this->getRequestParameter('volume2') . ' X ' . $this->getRequestParameter('vol3') . ' ' . $this->getRequestParameter('volume3') . ' X ' . $this->getRequestParameter('vol4') . ' ' . $this->getRequestParameter('volume4');
                }
            }

            $komponen_penyusun = $this->getRequestParameter('penyusun');

            $volumePenyusun = array();
            foreach ($komponen_penyusun as $penyusun) {
                $penyusun_id = $this->getRequestParameter('komponenPenyu_' . $penyusun);
                $cekPenyusun = new Criteria();
                $cekPenyusun->add(KomponenPeer::KOMPONEN_ID, $penyusun_id);
                $rs_cekPenyusun = KomponenPeer::doSelect($cekPenyusun);
                foreach ($rs_cekPenyusun as $komPenyusun) {
                    $nilaiDariWeb = $this->getRequestParameter('volPenyu_' . $penyusun);
                    $volumePenyusun[$komPenyusun->getKomponenId()] = $nilaiDariWeb;
                }
            }

            $sekarang = date('Y-m-d H:i:s');

            $dinas = sfContext::getInstance()->getUser()->getNamaLogin();

            $c = new Criteria();
            $c->add(WaitingListPeer::ID_WAITING, $id_waiting);
            $waitinglist = WaitingListPeer::doSelectOne($c);

            $unit_id_rd = '2600';
            $status_pagu_rincian = 1;
//menambah fasilitas jika nilai rincian lebih dari pagu, maka dinas tidak bisa mengedit.
            $rd_function = new DinasRincianDetail();
            if (sfConfig::get('app_fasilitas_batasPaguDinas') == 'buka') {
                if (1 == 0) {
                    $status_pagu_rincian = 0;
                } else {
                    if (sfConfig::get('app_fasilitas_paguDinasBerdasarDinas') == 'buka') {
                        //batas pagu per dinas
                        $status_pagu_rincian = $rd_function->getBatasPaguPerDinas($unit_id_rd, $waitinglist->getKomponenId(), $waitinglist->getPajak(), $vol1, $vol2, $vol3, $vol4, $komponen_penyusun, $volumePenyusun);
                    } else if (sfConfig::get('app_fasilitas_paguDinasBerdasarKegiatan') == 'buka') {
                        //batas pagu per kegiatan 
                        $status_pagu_rincian = $rd_function->getBatasPaguPerKegiatan($unit_id_rd, $kode_kegiatan, $waitinglist->getKomponenId(), $waitinglist->getPajak(), $vol1, $vol2, $vol3, $vol4, $komponen_penyusun, $volumePenyusun);
                    }
                }
            } else if (sfConfig::get('app_fasilitas_batasPaguDinas') == 'tutup') {
                $status_pagu_rincian = 0;
            }

            if ($status_pagu_rincian == 1) {
                $this->setFlash('gagal', 'Komponen tidak berhasil ditambahkan karena. nilai total RKA Melebihi total Pagu SKPD.');
                return $this->redirect("waitinglist/waitingGoRKA?id=$id_waiting&cek_rka=" . md5('rka_waiting'));
            }

            if ($akrual_code) {
                $akrual_code_baru = $akrual_code . '|01';
                $no_akrual_code = DinasRincianDetailPeer::AmbilUrutanAkrual($akrual_code_baru);
                $akrual_code_baru = $akrual_code_baru . $no_akrual_code;
            }

            $con = Propel::getConnection();
            $con->begin();
            try {
                $query = "update " . sfConfig::get('app_default_schema') . ".waitinglist "
                        . "set koefisien='$keterangan_koefisien', volume=$volume, komponen_lokasi = '$detail_name', subtitle='$subtitle', "
                        . "kegiatan_code = '$kode_kegiatan', nilai_ee = $nilai_ee, keterangan = '$keterangan',updated_at = '$sekarang', komponen_rekening = '$rekening', "
                        . "kode_jasmas = '$jasmas', kecamatan = '$kecamatan', kelurahan = '$kelurahan', is_musrenbang = '" . $is_musrenbang . "' "
                        . "where id_waiting = $id_waiting";
                $stmt = $con->prepareStatement($query);
                $stmt->executeQuery();

                budgetLogger::log('mengedit Data Waiting List untuk kegiatan ' . $kode_kegiatan . ' Komponen ' . $waitinglist->getKomponenName() . ' ' . $detail_name . '  id::' . $waitinglist->getIdWaiting());

                $rd_cari = new Criteria();
                $rd_cari->add(WaitingListPeer::ID_WAITING, $id_waiting);
                $rd_dapat = WaitingListPeer::doSelectOne($rd_cari);
                $komponen_id = $rd_dapat->getKomponenId();
                $komponen_name = $rd_dapat->getKomponenName();
                $komponen_harga = $rd_dapat->getKomponenHarga();
                $satuan = $rd_dapat->getKomponenSatuan();
                $pajak = $rd_dapat->getPajak();
                $prioritas_pilih = $rd_dapat->getPrioritas();

                $kode_rka_fix = 'XXX2600' . '.' . $kode_kegiatan . '.' . $id_waiting;

//            cek kode_sub rka-member untuk sub2title
                $sql = "select max(kode_sub) as kode_sub from " . sfConfig::get('app_default_schema') . ".dinas_rka_member "
                        . "where kode_sub ilike 'RKAM%' ";
                $stmt = $con->prepareStatement($sql);
                $rs = $stmt->executeQuery();
                while ($rs->next()) {
                    $kodesub = $rs->getString('kode_sub');
                }
                $kode = substr($kodesub, 4, 5);
                $kode+=1;
                if ($kode < 10) {
                    $kodesub = 'RKAM0000' . $kode;
                } elseif ($kode < 100) {
                    $kodesub = 'RKAM000' . $kode;
                } elseif ($kode < 1000) {
                    $kodesub = 'RKAM00' . $kode;
                } elseif ($kode < 10000) {
                    $kodesub = 'RKAM0' . $kode;
                } elseif ($kode < 100000) {
                    $kodesub = 'RKAM' . $kode;
                }
//            cek kode_sub rka-member untuk sub2title
//            cek detail_no rincian-detail untuk komponen
                $detail_no = 0;
                $queryDetailNo = "select max(detail_no) as nilai "
                        . "from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail "
                        . "where unit_id='2600' and kegiatan_code='$kode_kegiatan'";
                $stmt = $con->prepareStatement($queryDetailNo);
                $rs_max = $stmt->executeQuery();
                while ($rs_max->next()) {
                    $detail_no = $rs_max->getString('nilai');
                }
                $detail_no+=1;
//            cek detail_no rincian-detail untuk komponen
//            insert rka-member untuk sub2title                
                $queryInsert2RkaMember = " insert into " . sfConfig::get('app_default_schema') . ".dinas_rka_member 
                (kode_sub,unit_id,kegiatan_code,detail_no,komponen_id,komponen_name,detail_name,rekening_asli,tahun ) 
                values ('$kodesub','2600','$kode_kegiatan',$detail_no,'$komponen_id','$komponen_name','$detail_name_rd','$rekening','" . sfConfig::get('app_tahun_default') . "')";
                $stmt2 = $con->prepareStatement($queryInsert2RkaMember);
                $stmt2->executeQuery();

//            insert rka-member untuk sub2title                
                $subSubtitle = trim($komponen_name . ' ' . $detail_name_rd);

                $c_kegiatan = new Criteria();
                $c_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, '2600');
                $c_kegiatan->addAnd(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                $dapat_kegiatan = DinasMasterKegiatanPeer::doSelectOne($c_kegiatan);

//            insert rincian-detail untuk komponen                
                $queryInsert2RincianDetail = "insert into " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail  
                    (kegiatan_code, tipe, detail_no, rekening_code, komponen_id, detail_name, volume, keterangan_koefisien, subtitle, komponen_harga, komponen_harga, 
                    komponen_name, satuan, pajak, unit_id, kode_sub, sub, kecamatan, last_update_user, last_update_time, tahap_edit,tahun,lokasi_kecamatan,lokasi_kelurahan, note_skpd, tahap, tipe2, akrual_code) 
                    values 
                    ('" . $kode_kegiatan . "', '" . $tipe_komponen . "', " . $detail_no . ", '" . $rekening . "', '" . $komponen_id . "', '(" . $detail_name_rd . ")', " . $volume . ", '" . $keterangan_koefisien . "', '" . $subtitle . "', "
                        . "" . $komponen_harga . ", " . $komponen_harga . ",'" . $komponen_name . "', '" . $satuan . "', " . $pajak . ",'2600','" . $kodesub . "', '" . $subSubtitle . "', "
                        . "'" . $jasmas . "', '" . $dinas . "', '" . $sekarang . "','" . sfConfig::get('app_tahap_edit') . "','" . sfConfig::get('app_tahun_default') . "','" . $kecamatan . "','" . $kelurahan . "','Pengambilan dari Waiting List','" . $dapat_kegiatan->getTahap() . "', 'KONSTRUKSI', '$akrual_code_baru')";
                $stmt3 = $con->prepareStatement($queryInsert2RincianDetail);
                $stmt3->executeQuery();

//            insert rincian-detail untuk komponen                  
                $detailNamePenyusun = "''";
                //1 Juni 2016 -> pakai kode akrual -> tidak jadi
                $rekening_induk = $rekening;
                $temp_lain = 8;
                foreach ($komponen_penyusun as $penyusun) {
                    $penyusun_id = $this->getRequestParameter('komponenPenyu_' . $penyusun);
                    $cekPenyusun = new Criteria();
                    $cekPenyusun->add(KomponenPeer::KOMPONEN_ID, $penyusun_id);
                    $rs_cekPenyusun = KomponenPeer::doSelect($cekPenyusun);
                    //print_r($rs_cekPenyusun);
                    foreach ($rs_cekPenyusun as $komponenPenyusun) {
                        if ($akrual_code) {
                            if ($komponenPenyusun->getKodeAkrualKomponenPenyusun()) {
                                $akrual_code_penyusun = $akrual_code . '|02|' . $komponenPenyusun->getKodeAkrualKomponenPenyusun() . $no_akrual_code;
                            } else {
                                if ($temp_lain < 10)
                                    $akrual_code_penyusun = $akrual_code . '|02|0' . $temp_lain . $no_akrual_code;
                                else
                                    $akrual_code_penyusun = $akrual_code . '|02|' . $temp_lain . $no_akrual_code;
                                $temp_lain++;
                            }
                        }
                        $detail_no = 0;
                        $query = "select max(detail_no) as nilai "
                                . "from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail "
                                . "where unit_id='2600' and kegiatan_code='$kode_kegiatan'";
                        $con = Propel::getConnection();
                        $stmt = $con->prepareStatement($query);
                        $rs_max = $stmt->executeQuery();
                        while ($rs_max->next()) {
                            $detail_no = $rs_max->getString('nilai');
                        }
                        $detail_no+=1;

                        $querySisipan = "select max(status_level) as nilai "
                                . "from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail "
                                . "where unit_id='2600' and kegiatan_code='$kode_kegiatan'";
                        $stmt = $con->prepareStatement($querySisipan);
                        $rs_level = $stmt->executeQuery();
                        while ($rs_level->next()) {
                            $posisi_terjauh = $rs_level->getInt('nilai');
                        }
                        $sisipan = 'false';
                        if ($posisi_terjauh > 0) {
                            $sisipan = 'true';
                        }

                        if ($komponenPenyusun->getKomponenNonPajak() == TRUE) {
                            $pajakPenyusun = 0;
                        } else {
                            $pajakPenyusun = 10;
                        }

                        //$kodeChekBox = str_replace(".", "_", $komponenPenyusun->getKomponenId());

                        $subSubtitle = $komponen_name . ' ' . $detail_name;
                        $subSubtitle = trim($subSubtitle);
                        $query2 = "insert into " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail  
                                (kegiatan_code, tipe, detail_no, rekening_code, komponen_id, detail_name, volume, keterangan_koefisien, subtitle, komponen_harga, komponen_harga, 
                                komponen_name, satuan, pajak, unit_id, kode_sub, sub, kecamatan, last_update_user, last_update_time, tahap_edit,tahun,lokasi_kecamatan,lokasi_kelurahan, note_skpd, tahap, tipe2, akrual_code) 
                                values 
                                ('" . $kode_kegiatan . "', 'SSH', " . $detail_no . ", '" . $rekening_induk . "', '" . $komponenPenyusun->getKomponenId() . "', '" . $detailNamePenyusun . "', " . $this->getRequestParameter('volPenyu_' . $penyusun) . ", '" . $this->getRequestParameter('volPenyu_' . $penyusun) . ' ' . $komponenPenyusun->getSatuan() . "', '" . str_replace("'", "''", $subtitle) . "', "
                                . $komponenPenyusun->getKomponenHarga() . ", " . $komponenPenyusun->getKomponenHarga() . ",'" . $komponenPenyusun->getKomponenName() . "', '" . $komponenPenyusun->getSatuan() . "', " . $pajakPenyusun . ",'2600','" . $kodesub . "', '" . $subSubtitle . "', "
                                . "'" . $jasmas . "', '" . $dinas . "', '" . $sekarang . "','" . sfConfig::get('app_tahap_edit') . "','" . sfConfig::get('app_tahun_default') . "','" . $kecamatan . "','" . $kelurahan . "','Pengambilan dari Waiting List','" . $dapat_kegiatan->getTahap() . "', '" . $komponenPenyusun->getKomponenTipe2() . "', '$akrual_code_penyusun')";
                        $stmt = $con->prepareStatement($query2);
                        budgetLogger::log('Menambah Komponen penyusun baru (eRevisi) dengan komponen name ' . $komponenPenyusun->getKomponenName() . '(' . $komponenPenyusun->getKomponenId() . ') pada dinas: 2600 dengan kode kegiatan :' . $kode_kegiatan . ' pada KodeSub:' . $kodesub);

                        $stmt->executeQuery();

                        historyUserLog::tambah_komponen_penyusun_revisi($unit_id_rd, $kode_kegiatan, $detail_no);
                    }
                }
                $total_aktif = 0;
//                $query = "select count(*) as total "
//                        . "from " . sfConfig::get('app_default_schema') . ".waitinglist "
//                        . "where status_hapus = false and status_waiting = 0 "
//                        . "and unit_id = 'XXX2600' and kegiatan_code = '" . $kode_kegiatan . "'";
                $query = "select max(prioritas) as total "
                        . "from " . sfConfig::get('app_default_schema') . ".waitinglist "
                        . "where status_hapus = false and status_waiting = 0 "
                        . "and unit_id = 'XXX2600' and kegiatan_code = '" . $kode_kegiatan . "'";
                $stmt = $con->prepareStatement($query);
                $rs = $stmt->executeQuery();
                while ($rs->next()) {
                    $total_aktif = $rs->getString('total');
                }

                $rd_dapat->setStatusWaiting(1);
                $rd_dapat->setKodeRka('2600.' . $kode_kegiatan . '.' . $detail_no);
                $rd_dapat->setUserPengambil($dinas);
                $rd_dapat->setUpdatedAt($sekarang);
                $rd_dapat->setMetodeWaitinglist(1);
                $rd_dapat->save();

                $prioritas_diatas_satu = $prioritas_pilih + 1;
                for ($index = $prioritas_diatas_satu; $index <= $total_aktif; $index++) {
                    $index_kurang_satu = $index - 1;
                    $c_prioritas = new Criteria();
                    $c_prioritas->add(WaitingListPeer::UNIT_ID, 'XXX2600');
                    $c_prioritas->addAnd(WaitingListPeer::KEGIATAN_CODE, $kode_kegiatan);
                    $c_prioritas->addAnd(WaitingListPeer::PRIORITAS, $index);
                    $c_prioritas->addAnd(WaitingListPeer::STATUS_HAPUS, FALSE);
                    $c_prioritas->addAnd(WaitingListPeer::STATUS_WAITING, 0);
//echo $index.' '.$index_kurang_satu.' '.$total_aktif.'<br>';
                    if ($waiting_prio = WaitingListPeer::doSelectOne($c_prioritas)) {
//                    $waiting_prio = WaitingListPeer::doSelectOne($c_prioritas);
                        $waiting_prio->setPrioritas($index_kurang_satu);
                        $waiting_prio->save();
                    }
//echo $index.' '.$index_kurang_satu.' '.$total_aktif.'<br>';
                }


                $keisi = 0;

//                    ambil lama
                $lokasi_lama = $this->getRequestParameter('lokasi_lama');

                if (count($lokasi_lama) > 0) {
                    foreach ($lokasi_lama as $value_lokasi_lama) {
                        $c_cari_lokasi = new Criteria();
                        $c_cari_lokasi->add(HistoryPekerjaanV2Peer::LOKASI, $value_lokasi_lama);
                        $c_cari_lokasi->addAnd(HistoryPekerjaanV2Peer::STATUS_HAPUS, FALSE);
                        $dapat_lokasi_lama = HistoryPekerjaanV2Peer::doSelectOne($c_cari_lokasi);
                        if ($dapat_lokasi_lama) {

                            $jalan_fix = '';
                            $gang_fix = '';
                            $nomor_fix = '';
                            $rw_fix = '';
                            $rt_fix = '';
                            $keterangan_fix = '';
                            $tempat_fix = '';

                            $jalan_lama = $dapat_lokasi_lama->getJalan();
                            $gang_lama = $dapat_lokasi_lama->getGang();
                            $nomor_lama = $dapat_lokasi_lama->getNomor();
                            $rw_lama = $dapat_lokasi_lama->getRw();
                            $rt_lama = $dapat_lokasi_lama->getRt();
                            $keterangan_lama = $dapat_lokasi_lama->getKeterangan();
                            $tempat_lama = $dapat_lokasi_lama->getTempat();

                            if ($jalan_lama <> '') {
                                $jalan_fix = 'JL. ' . strtoupper($jalan_lama) . ' ';
                            }

                            if ($tempat_lama <> '') {
                                $tempat_fix = '(' . strtoupper($tempat_lama) . ') ';
                            }

                            if ($gang_lama <> '') {
                                $gang_fix = $gang_lama . ' ';
                            }

                            if ($nomor_lama <> '') {
                                $nomor_fix = 'NO ' . strtoupper($nomor_lama) . ' ';
                            }

                            if ($rw_lama <> '') {
                                $rw_fix = 'RW ' . strtoupper($rw_lama) . ' ';
                            }

                            if ($rt_lama <> '') {
                                $rt_fix = 'RT ' . strtoupper($rt_lama) . ' ';
                            }

                            if ($keterangan_lama <> '') {
                                $keterangan_fix = '' . strtoupper($keterangan_lama) . ' ';
                            }

                            $lokasi_baru = $tempat_fix . '' . $jalan_fix . '' . $gang_fix . '' . $nomor_fix . '' . $rw_fix . '' . $rt_fix . '' . $keterangan_fix;

                            $rd_cari_fix = new Criteria();
                            $rd_cari_fix->add(DinasRincianDetailPeer::UNIT_ID, '2600');
                            $rd_cari_fix->addAnd(DinasRincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
                            $rd_cari_fix->addAnd(DinasRincianDetailPeer::DETAIL_NO, $detail_no);
                            $rd_dapat_fix = DinasRincianDetailPeer::doSelectOne($rd_cari_fix);

                            $rka_lokasi_fix = '2600' . '.' . $kode_kegiatan . '.' . $detail_no;
                            $komponen_lokasi_fix = $rd_dapat_fix->getKomponenName() . ' ' . $rd_dapat_fix->getDetailName();
                            $kecamatan_lokasi_fix = $rd_dapat_fix->getLokasiKecamatan();
                            $kelurahan_lokasi_fix = $rd_dapat_fix->getLokasiKelurahan();
                            $lokasi_per_titik_fix = $lokasi_baru;

                            $c_insert_gis = new HistoryPekerjaanV2();
                            $c_insert_gis->setTahun(sfConfig::get('app_tahun_default'));
                            $c_insert_gis->setKodeRka($rka_lokasi_fix);
                            $c_insert_gis->setStatusHapus(FALSE);
                            $c_insert_gis->setJalan(strtoupper($jalan_lama));
                            $c_insert_gis->setGang(strtoupper($gang_lama));
                            $c_insert_gis->setNomor(strtoupper($nomor_lama));
                            $c_insert_gis->setRw(strtoupper($rw_lama));
                            $c_insert_gis->setRt(strtoupper($rt_lama));
                            $c_insert_gis->setKeterangan(strtoupper($keterangan_lama));
                            $c_insert_gis->setTempat(strtoupper($tempat_lama));
                            $c_insert_gis->setKomponen($komponen_lokasi_fix);
                            $c_insert_gis->setKecamatan($kecamatan_lokasi_fix);
                            $c_insert_gis->setKelurahan($kelurahan_lokasi_fix);
                            $c_insert_gis->setLokasi($lokasi_per_titik_fix);
                            $c_insert_gis->save();
                        }
                    }
                }

//                    buat baru
                $lokasi_jalan = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_jalan')));
                $lokasi_gang = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_gang')));
                $tipe_gang = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('tipe_gang')));
                $lokasi_nomor = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_nomor')));
                $lokasi_rw = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_rw')));
                $lokasi_rt = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_rt')));
                $lokasi_keterangan = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_keterangan')));
                $lokasi_tempat = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_tempat')));

                $total_array_lokasi = count($lokasi_jalan);

                for ($i = 0; $i < $total_array_lokasi; $i++) {
                    $jalan_fix = '';
                    $gang_fix = '';
                    $tipe_gang_fix = '';
                    $nomor_fix = '';
                    $rw_fix = '';
                    $rt_fix = '';
                    $keterangan_fix = '';
                    $tempat_fix = '';
                    if (trim($lokasi_jalan[$i]) <> '' || trim($lokasi_tempat[$i]) <> '') {

                        if (trim($lokasi_jalan[$i]) <> '') {
                            $jalan_fix = 'JL. ' . strtoupper(trim($lokasi_jalan[$i])) . ' ';
                        }

                        if (trim($lokasi_tempat[$i]) <> '') {
                            $tempat_fix = '(' . strtoupper(trim($lokasi_tempat[$i])) . ') ';
                        }

                        if (trim($tipe_gang[$i]) <> '') {
                            $tipe_gang_fix = strtoupper(trim($tipe_gang[$i])) . '. ';
                        } else {
                            $tipe_gang_fix = 'GG. ';
                        }

                        if (trim($lokasi_gang[$i]) <> '') {
                            $gang_fix = $tipe_gang_fix . '' . strtoupper(trim($lokasi_gang[$i])) . ' ';
                        }

                        if (trim($lokasi_nomor[$i]) <> '') {
                            $nomor_fix = 'NO ' . strtoupper(trim($lokasi_nomor[$i])) . ' ';
                        }

                        if (trim($lokasi_rw[$i]) <> '') {
                            $rw_fix = 'RW ' . strtoupper(trim($lokasi_rw[$i])) . ' ';
                        }

                        if (trim($lokasi_rt[$i]) <> '') {
                            $rt_fix = 'RT ' . strtoupper(trim($lokasi_rt[$i])) . ' ';
                        }

                        if (trim($lokasi_keterangan[$i]) <> '') {
                            $keterangan_fix = '' . strtoupper(trim($lokasi_keterangan[$i])) . ' ';
                        }


                        $lokasi_baru = $tempat_fix . '' . $jalan_fix . '' . $gang_fix . '' . $nomor_fix . '' . $rw_fix . '' . $rt_fix . '' . $keterangan_fix;

                        $rd_cari_fix = new Criteria();
                        $rd_cari_fix->add(DinasRincianDetailPeer::UNIT_ID, '2600');
                        $rd_cari_fix->addAnd(DinasRincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
                        $rd_cari_fix->addAnd(DinasRincianDetailPeer::DETAIL_NO, $detail_no);
                        $rd_dapat_fix = DinasRincianDetailPeer::doSelectOne($rd_cari_fix);

                        $rka_lokasi_fix = '2600' . '.' . $kode_kegiatan . '.' . $detail_no;
                        $komponen_lokasi_fix = $rd_dapat_fix->getKomponenName() . ' ' . $rd_dapat_fix->getDetailName();
                        $kecamatan_lokasi_fix = $rd_dapat_fix->getLokasiKecamatan();
                        $kelurahan_lokasi_fix = $rd_dapat_fix->getLokasiKelurahan();
                        $lokasi_per_titik_fix = $lokasi_baru;

                        $c_insert_gis = new HistoryPekerjaanV2();
                        $c_insert_gis->setTahun(sfConfig::get('app_tahun_default'));
                        $c_insert_gis->setKodeRka($rka_lokasi_fix);
                        $c_insert_gis->setStatusHapus(FALSE);
                        $c_insert_gis->setJalan(strtoupper(trim($lokasi_jalan[$i])));
                        $c_insert_gis->setGang(strtoupper($gang_fix));
                        $c_insert_gis->setNomor(strtoupper(trim($lokasi_nomor[$i])));
                        $c_insert_gis->setRw(strtoupper(trim($lokasi_rw[$i])));
                        $c_insert_gis->setRt(strtoupper(trim($lokasi_rt[$i])));
                        $c_insert_gis->setKeterangan(strtoupper(trim($lokasi_keterangan[$i])));
                        $c_insert_gis->setTempat(strtoupper(trim($lokasi_tempat[$i])));
                        $c_insert_gis->setKomponen($komponen_lokasi_fix);
                        $c_insert_gis->setKecamatan($kecamatan_lokasi_fix);
                        $c_insert_gis->setKelurahan($kelurahan_lokasi_fix);
                        $c_insert_gis->setLokasi($lokasi_per_titik_fix);
                        $c_insert_gis->save();
                    }
                }

                $c_unit = new Criteria();
                $c_unit->add(UnitKerjaPeer::UNIT_ID, '2600');
                $data_unit_kerja = UnitKerjaPeer::doSelectOne($c_unit);

                foreach ($dapat_geojson as $value_geojson) {
                    $geojson_baru = new GeojsonlokasiRev1();
                    $geojson_baru->setUnitId('2600');
                    $geojson_baru->setUnitName($data_unit_kerja->getUnitName());
                    $geojson_baru->setKegiatanCode($kode_kegiatan);
                    $geojson_baru->setDetailNo($detail_no);
                    $geojson_baru->setSatuan($rd_dapat_fix->getSatuan());
                    $geojson_baru->setVolume($rd_dapat_fix->getVolume());
                    $geojson_baru->setNilaiAnggaran($rd_dapat_fix->getNilaiAnggaran());
                    $geojson_baru->setTahun(sfConfig::get('app_tahun_default'));
                    $geojson_baru->setMlokasi($value_geojson->getMlokasi());
                    $geojson_baru->setIdKelompok($value_geojson->getIdKelompok());
                    $geojson_baru->setGeojson($value_geojson->getGeojson());
                    $geojson_baru->setKeterangan($value_geojson->getKeterangan());
                    $geojson_baru->setNmuser($value_geojson->getNmuser());
                    $geojson_baru->setLevel($value_geojson->getLevel());
                    $geojson_baru->setKomponenName($rd_dapat_fix->getKomponenName() . ' ' . $rd_dapat_fix->getDetailName());
                    $geojson_baru->setStatusHapus(FALSE);
                    $geojson_baru->setKeteranganAlamat($value_geojson->getKeteranganAlamat());
                    $geojson_baru->setLastCreateTime($sekarang);
                    $geojson_baru->setLastEditTime($sekarang);
                    $geojson_baru->setKoordinat($value_geojson->getKoordinat());
                    $geojson_baru->setLokasiKe($value_geojson->getLokasiKe());
                    $geojson_baru->setKodeDetailKegiatan('2600.' . $kode_kegiatan . '.' . $detail_no);
                    $geojson_baru->save();
                }
                $con->commit();

                $this->setFlash('berhasil', 'Telah berhasil memproses ' . $komponen_name . ' ' . $detail_name . ' menjadi eRevisi');
                return $this->redirect("waitinglist/waitinglist");
            } catch (Exception $exc) {
                $con->rollback();
                $this->setFlash('gagal', 'Gagal Karena ' . $exc->getMessage());
                return $this->redirect("waitinglist/waitinglist");
//                return $this->redirect("waitinglist/waitingGoRKA?id=$id_waiting&cek_rka=" . md5('rka_waiting'));
            }
        }
    }

    public function executePilihKelurahan() {
        $this->id_kecamatan = $this->getRequestParameter('b');
    }

}
