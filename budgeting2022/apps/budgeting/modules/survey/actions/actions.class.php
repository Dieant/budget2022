<?php

/**
 * survey actions.
 *
 * @package    budgeting
 * @subpackage shsd
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 2692 2006-11-15 21:03:55Z fabien $
 */
class surveyActions extends sfActions {

    /**
     * Executes index action
     *
     */
    public function executeIndex() {
        $this->forward('shsd', 'list');
    }

    public function executeEditKomponen() {
        $id = $this->getRequestParameter('id');
        $this->halaman = $this->getRequestParameter('page');

        $c = new Criteria();
        $c->add(ShsdSurveyPeer::SHSD_ID, $id);
        $this->shsd = ShsdSurveyPeer::doSelectOne($c);
 
        // $c1 = new Criteria();
        // $c1->add(UsulanSSHPeer::SHSD_ID, $id);
        // $shsd_usulan = UsulanSSHPeer::doSelectOne($c1);
        // $this->shsd_usulan = $shsd_usulan;
        
        // cek komponen_tipe2 di komponen
        $cri = new Criteria();
        $cri->add(KomponenPeer::KOMPONEN_ID,$id);
        $komponen = KomponenPeer::doSelectOne($cri);
        $this->komponen = $komponen; 
        // var_dump($result);die();

        // kode rekenings
        $crit = new Criteria();
        $crit->addAscendingOrderByColumn(RekeningPeer::REKENING_CODE);
        $rekenings = RekeningPeer::doSelect($crit);
        $this->rekening = array();

        $idx = 0;
        foreach ($rekenings as $rekening) {
            $this->rekening[$idx]['rekening_code'] = $rekening->getRekeningCode();
            $this->rekening[$idx]['rekening_name'] = $rekening->getRekeningName();
            $idx++;
        }
    }

    public function executeTolakVerifikasi() {
        $id = $this->getRequestParameter('id_shsd');
        $komentar = $this->getRequestParameter('alasan');

        $c_dapat = new Criteria();
        $c_dapat->add(ShsdSurveyPeer::SHSD_ID, $id);
        $dapat_catatan = ShsdSurveyPeer::doSelectOne($c_dapat);

        if ($dapat_catatan) {
            $dapat_catatan->setShsdCatatan($dapat_catatan->getShsdCatatan(). '<br> Alasan Tolak : ' . $komentar);
            $dapat_catatan->setStatusVerifikasi(FALSE);
            $dapat_catatan->setStatusSurvey(FALSE);
            $dapat_catatan->setIsSurveyBp(TRUE);
            $dapat_catatan->save();

            $this->setFlash('berhasil', 'SSH berhasil dikembalikan');
        } else {
            $this->setFlash('gagal', 'SSH gagal dikembalikan ');
        }

        return $this->redirect('survey/sshlocked');
    }

    public function executeEditHargaLelang() {
        $name = $this->getRequestParameter('name');
        $kode = $this->getRequestParameter('kode_komponen');
        $name = '%'.$name.'%';
        $name = strtr($name,' ','%');
        $c = new Criteria();
        $c->add(ShsdSurveyPeer::SHSD_NAME, $name, Criteria::ILIKE);
        $c->add(ShsdSurveyPeer::SHSD_ID, $kode);
        $shsd = ShsdSurveyPeer::doSelectOne($c);
        $lelang = array();
        $lelang['is_lelang'] = false;
        if($shsd->getShsdHargaLelang() === NULL){
            // tidak ada nilai harga lelang (bukan tipe konstruksi)
            // cari harga lelang di tabel shsd_lelang
            $con = Propel::getConnection();
            $query =  "select lelang.* from " .sfConfig::get('app_default_schema'). ".shsd_lelang lelang 
                        join ".sfConfig::get('app_default_schema').".shsd_survey s on s.shsd_id = lelang.komponen_code
                        where lelang.komponen_name ilike '".$name."%' and s.shsd_name ilike '".$name."%' and lelang.tahun = ".sfConfig::get('app_tahun_default');
            $stmt = $con->prepareStatement($query);
            $rs = $stmt->executeQuery();
            $code='';
            $number = 1;
            while ($rs->next()) {
                $lelang['komponen_code'] = $rs->getString('komponen_code');
                // komponen yang akan sebenernya dihitung pertama kali
                if($code == ''){
                    $code = $rs->getString('komponen_code');
                    $lelang['komponen_name'] = $rs->getString('komponen_name');
                    $lelang['komponen_harga'] = $rs->getString('komponen_harga');
                    $lelang['komponen_tipe'] = "NONKONSTRUKSI";
                    $lelang['is_lelang'] = true;

                    // $number++;
                }
                // jika komponen masih sama
                else if($code == $lelang['komponen_code'] || $lelang['komponen_name'] == $rs->getString('komponen_name')){
                    $lelang['komponen_harga'] += $rs->getString('komponen_harga');
                    $number++;
                }
            }
        }
        else{
            // ada nilai lelang (tipe konstruksi)
            $lelang['komponen_tipe'] = "KONSTRUKSI";
            $lelang['is_lelang'] = true;
        }


        $this->shsd  = $shsd;
        $this->number = $number;
        $this->lelang = $lelang;
    }

    public function executeGetHargaLelang() {
        // if ($this->getRequestParameter('unit') && $this->getRequestParameter('komponen') && $this->getRequestParameter('rekening')) {
        // }
        $komponen_name = $this->getRequestParameter('komponen');
        $tahun = $this->getRequestParameter('tahun');
        $con = Propel::getConnection();
        $query = "select u.unit_name, lelang.* from " .sfConfig::get('app_default_schema'). ".shsd_lelang lelang 
                    join unit_kerja u on u.unit_id = lelang.unit_id 
                    where lelang.komponen_name ilike '%".$komponen_name."%' and lelang.tahun = ".$tahun;
        // $query = "select * from " .sfConfig::get('app_default_schema'). ".shsd_lelang where nama_komponen = '".$komponen_name."' and tahun = ".$tahun;
        $stmt = $con->prepareStatement($query);
        $rs = $stmt->executeQuery();
        $this->list = $rs;
        $this->tahun = $tahun;
        $this->setLayout('kosong');
    }

    public function executeSaveEditHargaLelang()
    {
        $shsd_id = $this->getRequestParameter('kode_komponen');
        $shsd_name = $this->getRequestParameter('name');
        $shsd_harga_lelang = $this->getRequestParameter('shsd_harga_lelang');

    }

    public function executeSaveEditKomponen() {
        $is_construct = 0;
        $halaman = $this->getRequestParameter('page');
        $status_verif = $this->getRequestParameter('status');
        $keterangan = $this->getRequestParameter('keterangan_surveyor');
        $shsd_id = $this->getRequestParameter('shsd_id');
        $shsd_name = $this->getRequestParameter('shsd_name');
        $surveyor = $this->getRequestParameter('surveyor');
        $spec = $this->getRequestParameter('spec');
        $hidden_spec = $this->getRequestParameter('hidden_spec');
        
        $tanggal = $this->getRequestParameter('tanggal');
        $non_pajak = $this->getRequestParameter('non_pajak');

        $toko1 = $this->getRequestParameter('toko1');
        $merk1 = $this->getRequestParameter('merk1');
        $keterangan1 = $this->getRequestParameter('keterangan1');
        $harga1 = $this->getRequestParameter('harga1');
        $file1 = $this->getRequestParameter('file1_lama');
        $file1_2 = $this->getRequestParameter('file1_2_lama');

        $toko2 = $this->getRequestParameter('toko2');
        $merk2 = $this->getRequestParameter('merk2');
        $keterangan2 = $this->getRequestParameter('keterangan2');
        $harga2 = $this->getRequestParameter('harga2');
        $file2 = $this->getRequestParameter('file2_lama');
        $file2_2 = $this->getRequestParameter('file2_2_lama');

        $toko3 = $this->getRequestParameter('toko3');
        $merk3 = $this->getRequestParameter('merk3');
        $keterangan3 = $this->getRequestParameter('keterangan3');
        $harga3 = $this->getRequestParameter('harga3');
        $file3 = $this->getRequestParameter('file3_lama');
        $file3_2 = $this->getRequestParameter('file3_2_lama');

        $shsd_harga_pakai = $this->getRequestParameter('shsd_harga_pakai');
        $catatan_survey = $this->getRequestParameter('catatan_survey');

        $jmlPendukung = 0;
        if ($this->getRequest()->getFileName('file1') || $file1 != '')
            $jmlPendukung++;
        if ($this->getRequest()->getFileName('file2') || $file2 != '')
            $jmlPendukung++;
        if ($this->getRequest()->getFileName('file3') || $file3 != '')
            $jmlPendukung++;

        if($tanggal == '')
        {
            $this->setFlash('gagal', 'Tanggal survey mohon untuk diisi');
            return $this->redirect('survey/editKomponen?id='.$shsd_id.'&page=' . $halaman);
        }

        //untuk validasi file
        $cri = new Criteria();
        $cri->add(ShsdSurveyPeer::SHSD_ID, $shsd_id);
        $load_shsd = ShsdSurveyPeer::doSelectOne($cri);

        //Validasi untuk wajib mengisi minimal pendukung 1
        if($toko1 == '' || $merk1=='' || $keterangan1=='' || $harga1 == 0){
            $this->setFlash('gagal', 'Harap mengisi kelengkapan data pendukung 1');
            return $this->redirect('survey/editKomponen?id='.$shsd_id.'&page=' . $halaman);
        }

        //validasi untuk wajib mengisi merk, keterangan, harga, data pendukung 1 jika telah mengisi toko 1
        if($toko1 != '' || $merk1 != '' || $keterangan1 != '' || $harga1 != 0){
            if($toko1=='' || $merk1=='' || $keterangan1=='' || $harga1 == 0){
                $this->setFlash('gagal', 'Harap Lengkapi Data Pendukung 1 (Toko, Merk, Keterangan, Harga, Pendukung 1.1)');
                return $this->redirect('survey/editKomponen?id='.$shsd_id.'&page=' . $halaman);
            }

            //cek file 1
            if($load_shsd->getFile1()=='' &&  !$this->getRequest()->getFileName('file1')){
                $this->setFlash('gagal', 'Harap Lengkapi Data Pendukung 1 (Toko, Merk, Keterangan, Harga, Pendukung 1.1)');
                return $this->redirect('survey/editKomponen?id='.$shsd_id.'&page=' . $halaman);
            }


            // if($keterangan1 == ''){
            //     $this->setFlash('gagal', 'Harap mengisi field Keterangan 1');
            //     return $this->redirect('survey/editKomponen?id='.$shsd_id.'&page=' . $halaman);
            // }
            // if($harga1 == 0){
            //     $this->setFlash('gagal', 'Harap mengisi field Harga 1');
            //     return $this->redirect('survey/editKomponen?id='.$shsd_id.'&page=' . $halaman);
            // }
            // if(!$this->getRequest()->getFileName('file1')){
            //     $this->setFlash('gagal', 'Harap mengisi field Data Pendukung 1.1');
            //     return $this->redirect('survey/editKomponen?id='.$shsd_id.'&page=' . $halaman);
            // }
        }
        
        //validasi untuk wajib mengisi merk, keterangan, harga, data pendukung 2 jika telah mengisi toko 2
        if($toko2 != '' || $merk2 != '' || $keterangan2 != '' || $harga2 != 0){
            // if($toko1=='' || $merk1=='' || $keterangan1==''){
            //     $this->setFlash('gagal', 'Sebelum mengisi Data Pendukung 2, Mohon mengisi Data Pendukung 1 dahulu');
            //     return $this->redirect('survey/editKomponen?id='.$shsd_id.'&page=' . $halaman);
            // }

            if($toko2=='' || $merk2=='' || $keterangan2=='' || $harga2 == 0){
                $this->setFlash('gagal', 'Harap Lengkapi Data Pendukung 2 (Toko, Merk, Keterangan, Harga, Pendukung 2.1)');
                return $this->redirect('survey/editKomponen?id='.$shsd_id.'&page=' . $halaman);
            }

            //cek file 2
            if($load_shsd->getFile2()=='' &&  !$this->getRequest()->getFileName('file2')){
                $this->setFlash('gagal', 'Harap Lengkapi Data Pendukung 2 (Toko, Merk, Keterangan, Harga, Pendukung 2.1)');
                return $this->redirect('survey/editKomponen?id='.$shsd_id.'&page=' . $halaman);
            }

            // if($merk2 == ''){
            //     $this->setFlash('gagal', 'Harap mengisi field Merk 2');
            //     return $this->redirect('survey/editKomponen?id='.$shsd_id.'&page=' . $halaman);
            // }
            // if($keterangan2 == ''){
            //     $this->setFlash('gagal', 'Harap mengisi field Keterangan 2');
            //     return $this->redirect('survey/editKomponen?id='.$shsd_id.'&page=' . $halaman);
            // }
            // if($harga2 == 0){
            //     $this->setFlash('gagal', 'Harap mengisi field Harga 2');
            //     return $this->redirect('survey/editKomponen?id='.$shsd_id.'&page=' . $halaman);
            // }
            // if(!$this->getRequest()->getFileName('file2')){
            //     $this->setFlash('gagal', 'Harap mengisi field Data Pendukung 2.1');
            //     return $this->redirect('survey/editKomponen?id='.$shsd_id.'&page=' . $halaman);
            // }
        }
        
        //validasi untuk wajib mengisi merk, keterangan, harga, data pendukung 3 jika telah mengisi toko 3
        if($toko3 != '' || $merk3 != '' || $keterangan3 != '' || $harga3 != 0){
            // if($toko1=='' || $merk1=='' || $keterangan1==''){
            //     $this->setFlash('gagal', 'Sebelum mengisi Data Pendukung 3, Mohon mengisi Data Pendukung 1 dahulu');
            //     return $this->redirect('survey/editKomponen?id='.$shsd_id.'&page=' . $halaman);
            // }
            
            if($toko2=='' || $merk2=='' || $keterangan2=='' || $harga2==''){
                $this->setFlash('gagal', 'Sebelum mengisi Data Pendukung 3, Mohon mengisi Data Pendukung 2 dahulu');
                return $this->redirect('survey/editKomponen?id='.$shsd_id.'&page=' . $halaman);
            }

            if($toko3=='' || $merk3=='' || $keterangan3=='' || $harga3 == 0){
                $this->setFlash('gagal', 'Harap Lengkapi Data Pendukung 2 (Toko, Merk, Keterangan, Harga, Pendukung 3.1)');
                return $this->redirect('survey/editKomponen?id='.$shsd_id.'&page=' . $halaman);
            }

            //cek file 3
            if($load_shsd->getFile3()=='' &&  !$this->getRequest()->getFileName('file3')){
                $this->setFlash('gagal', 'Harap Lengkapi Data Pendukung 3 (Toko, Merk, Keterangan, Harga, Pendukung 3.1)');
                return $this->redirect('survey/editKomponen?id='.$shsd_id.'&page=' . $halaman);
            }

            // if($merk3 == ''){
            //     $this->setFlash('gagal', 'Harap mengisi field Merk 3');
            //     return $this->redirect('survey/editKomponen?id='.$shsd_id.'&page=' . $halaman);
            // }
            // if($keterangan3 == ''){
            //     $this->setFlash('gagal', 'Harap mengisi field Keterangan 3');
            //     return $this->redirect('survey/editKomponen?id='.$shsd_id.'&page=' . $halaman);
            // }
            // if($harga3 == 0){
            //     $this->setFlash('gagal', 'Harap mengisi field Harga 3');
            //     return $this->redirect('survey/editKomponen?id='.$shsd_id.'&page=' . $halaman);
            // }
            // if(!$this->getRequest()->getFileName('file3')){
            //     $this->setFlash('gagal', 'Harap mengisi field Data Pendukung 3.1');
            //     return $this->redirect('survey/editKomponen?id='.$shsd_id.'&page=' . $halaman);
            // }
        }

        if($surveyor == '') {
            $this->setFlash('gagal', 'Nama Surveyor harus di pilih');
            return $this->redirect('survey/editKomponen?id='.$shsd_id.'&page=' . $halaman);
        }
        // if($jmlPendukung < 1) {
        //     $this->setFlash('gagal', 'Data pendukung yang diupload tidak boleh kurang dari 1');
        //     return $this->redirect('survey/editKomponen?id='.$shsd_id.'&page=' . $halaman);
        // }

        if ($this->getRequest()->getFileName('file1')) {
            $array_file_pdf = explode('.', $this->getRequest()->getFileName('file1'));
            if (!in_array($array_file_pdf[count($array_file_pdf) - 1], array('zip', 'rar', 'jpg', 'pdf', 'png', 'xls', 'xlsx', 'JPG', 'PNG', 'jpeg', 'JPEG'))) {
                $this->setFlash('gagal', 'Gunakan file dengan format zip/rar/jpg/png/pdf');
                return $this->redirect('survey/editKomponen?id='.$shsd_id.'&page=' . $halaman);
            }

            $fileName_compress = 'file1_' . str_replace('.', '_', $shsd_id) . '_' . date('Y-m-d_H-i') . '.' . $array_file_pdf[count($array_file_pdf) - 1];

            $this->getRequest()->moveFile('file1', sfConfig::get('sf_root_dir') . '/web/uploads/survey/' . $fileName_compress);

            $file_path_pdf = sfConfig::get('sf_root_dir') . '/web/uploads/survey/' . $fileName_compress;

            if (!file_exists($file_path_pdf)) {
                unlink(sfConfig::get('sf_root_dir') . '/web/uploads/survey/' . $fileName_compress);
                $this->setFlash('gagal', 'CEK FILE ' . $fileName_compress);
                return $this->redirect('survey/editKomponen?id='.$shsd_id.'&page=' . $halaman);
            }
            $file1 = $fileName_compress;
        }
        if ($this->getRequest()->getFileName('file1_2')) {
            $array_file_pdf = explode('.', $this->getRequest()->getFileName('file1_2'));
            if (!in_array($array_file_pdf[count($array_file_pdf) - 1], array('zip', 'rar', 'jpg', 'pdf', 'png', 'xls', 'xlsx', 'JPG', 'PNG', 'jpeg', 'JPEG'))) {
                $this->setFlash('gagal', 'Gunakan file dengan format zip/rar/jpg/png/pdf');
               return $this->redirect('survey/editKomponen?id='.$shsd_id.'&page=' . $halaman);
            }

            $fileName_compress = 'file1_2_' . str_replace('.', '_', $shsd_id) . '_' . date('Y-m-d_H-i') . '.' . $array_file_pdf[count($array_file_pdf) - 1];

            $this->getRequest()->moveFile('file1_2', sfConfig::get('sf_root_dir') . '/web/uploads/survey/' . $fileName_compress);

            $file_path_pdf = sfConfig::get('sf_root_dir') . '/web/uploads/survey/' . $fileName_compress;

            if (!file_exists($file_path_pdf)) {
                unlink(sfConfig::get('sf_root_dir') . '/web/uploads/survey/' . $fileName_compress);
                $this->setFlash('gagal', 'CEK FILE ' . $fileName_compress);
                return $this->redirect('survey/editKomponen?id='.$shsd_id.'&page=' . $halaman);
            }
            $file1_2 = $fileName_compress;
        }

        if ($this->getRequest()->getFileName('file2')) {
            $array_file_pdf = explode('.', $this->getRequest()->getFileName('file2'));
            if (!in_array($array_file_pdf[count($array_file_pdf) - 1], array('zip', 'rar', 'jpg', 'pdf', 'png', 'xls', 'xlsx', 'JPG', 'PNG', 'jpeg', 'JPEG'))) {
                $this->setFlash('gagal', 'Gunakan file dengan format zip/rar/jpg/png/pdf');
                return $this->redirect('survey/editKomponen?id='.$shsd_id.'&page=' . $halaman);
            }

            $fileName_compress = 'file2_' . str_replace('.', '_', $shsd_id) . '_' . date('Y-m-d_H-i') . '.' . $array_file_pdf[count($array_file_pdf) - 1];

            $this->getRequest()->moveFile('file2', sfConfig::get('sf_root_dir') . '/web/uploads/survey/' . $fileName_compress);

            $file_path_pdf = sfConfig::get('sf_root_dir') . '/web/uploads/survey/' . $fileName_compress;

            if (!file_exists($file_path_pdf)) {
                unlink(sfConfig::get('sf_root_dir') . '/web/uploads/survey/' . $fileName_compress);
                $this->setFlash('gagal', 'CEK FILE ' . $fileName_compress);
                return $this->redirect('survey/editKomponen?id='.$shsd_id.'&page=' . $halaman);
            }
            $file2 = $fileName_compress;
        }
        if ($this->getRequest()->getFileName('file2_2')) {
            $array_file_pdf = explode('.', $this->getRequest()->getFileName('file2_2'));
            if (!in_array($array_file_pdf[count($array_file_pdf) - 1], array('zip', 'rar', 'jpg', 'pdf', 'png', 'xls', 'xlsx', 'JPG', 'PNG', 'jpeg', 'JPEG'))) {
                $this->setFlash('gagal', 'Gunakan file dengan format zip/rar/jpg/png/pdf');
                return $this->redirect('survey/editKomponen?id='.$shsd_id.'&page=' . $halaman);
            }

            $fileName_compress = 'file2_2_' . str_replace('.', '_', $shsd_id) . '_' . date('Y-m-d_H-i') . '.' . $array_file_pdf[count($array_file_pdf) - 1];

            $this->getRequest()->moveFile('file2_2', sfConfig::get('sf_root_dir') . '/web/uploads/survey/' . $fileName_compress);

            $file_path_pdf = sfConfig::get('sf_root_dir') . '/web/uploads/survey/' . $fileName_compress;

            if (!file_exists($file_path_pdf)) {
                unlink(sfConfig::get('sf_root_dir') . '/web/uploads/survey/' . $fileName_compress);
                $this->setFlash('gagal', 'CEK FILE ' . $fileName_compress);
                return $this->redirect('survey/editKomponen?id='.$shsd_id.'&page=' . $halaman);
            }
            $file2_2 = $fileName_compress;
        }

        if ($this->getRequest()->getFileName('file3')) {
            $array_file_pdf = explode('.', $this->getRequest()->getFileName('file3'));
            if (!in_array($array_file_pdf[count($array_file_pdf) - 1], array('zip', 'rar', 'jpg', 'pdf', 'png', 'xls', 'xlsx', 'JPG', 'PNG', 'jpeg', 'JPEG'))) {
                $this->setFlash('gagal', 'Gunakan file dengan format zip/rar/jpg/png/pdf');
                return $this->redirect('survey/editKomponen?id='.$shsd_id.'&page=' . $halaman);
            }

            $fileName_compress = 'file3_' . str_replace('.', '_', $shsd_id) . '_' . date('Y-m-d_H-i') . '.' . $array_file_pdf[count($array_file_pdf) - 1];

            $this->getRequest()->moveFile('file3', sfConfig::get('sf_root_dir') . '/web/uploads/survey/' . $fileName_compress);

            $file_path_pdf = sfConfig::get('sf_root_dir') . '/web/uploads/survey/' . $fileName_compress;

            if (!file_exists($file_path_pdf)) {
                unlink(sfConfig::get('sf_root_dir') . '/web/uploads/survey/' . $fileName_compress);
                $this->setFlash('gagal', 'CEK FILE ' . $fileName_compress);
               return $this->redirect('survey/editKomponen?id='.$shsd_id.'&page=' . $halaman);
            }
            $file3 = $fileName_compress;
        }
        if ($this->getRequest()->getFileName('file3_2')) {
            $array_file_pdf = explode('.', $this->getRequest()->getFileName('file3_2'));
            if (!in_array($array_file_pdf[count($array_file_pdf) - 1], array('zip', 'rar', 'jpg', 'pdf', 'png', 'xls', 'xlsx', 'JPG', 'PNG', 'jpeg', 'JPEG'))) {
                $this->setFlash('gagal', 'Gunakan file dengan format zip/rar/jpg/png/pdf');
                return $this->redirect('survey/editKomponen?id='.$shsd_id.'&page=' . $halaman);
            }

            $fileName_compress = 'file3_2_' . str_replace('.', '_', $shsd_id) . '_' . date('Y-m-d_H-i') . '.' . $array_file_pdf[count($array_file_pdf) - 1];

            $this->getRequest()->moveFile('file3_2', sfConfig::get('sf_root_dir') . '/web/uploads/survey/' . $fileName_compress);

            $file_path_pdf = sfConfig::get('sf_root_dir') . '/web/uploads/survey/' . $fileName_compress;

            if (!file_exists($file_path_pdf)) {
                unlink(sfConfig::get('sf_root_dir') . '/web/uploads/survey/' . $fileName_compress);
                $this->setFlash('gagal', 'CEK FILE ' . $fileName_compress);
                return $this->redirect('survey/editKomponen?id='.$shsd_id.'&page=' . $halaman);
            }
            $file3_2 = $fileName_compress;
        }

        // cek jika komponen konstruksi
        if($this->getRequestParameter('shsd_harga_lelang')){
            // var_dump("konstruksi");die();
            $is_construct = 1;
            $shsd_harga_lelang = $this->getRequestParameter('shsd_harga_lelang');

            // ubah untuk file pendukung analisa konstruksi
            if ($this->getRequest()->getFileName('file_lelang')) {
                $array_file_pdf = explode('.', $this->getRequest()->getFileName('file_lelang'));
                if (!in_array($array_file_pdf[count($array_file_pdf) - 1], array('zip', 'rar', 'jpg', 'pdf', 'png', 'xls', 'xlsx', 'JPG', 'PNG', 'jpeg', 'JPEG'))) {
                    $this->setFlash('gagal', 'Gunakan file dengan format zip/rar/jpg/png/pdf');
                    return $this->redirect('survey/sshlocked?page=' . $halaman);
                }

                $fileName_compress = 'file_lelang_' . str_replace('.', '_', $shsd_id) . '_' . date('Y-m-d_H-i') . '.' . $array_file_pdf[count($array_file_pdf) - 1];

                $this->getRequest()->moveFile('file_lelang', sfConfig::get('sf_root_dir') . '/web/uploads/survey/' . $fileName_compress);

                $file_path_pdf = sfConfig::get('sf_root_dir') . '/web/uploads/survey/' . $fileName_compress;

                if (!file_exists($file_path_pdf)) {
                    unlink(sfConfig::get('sf_root_dir') . '/web/uploads/survey/' . $fileName_compress);
                    $this->setFlash('gagal', 'CEK FILE ' . $fileName_compress);
                    return $this->redirect('survey/sshlocked?page=' . $halaman);
                }
                $file_lelang = $fileName_compress;
            }
        }
         $ids = (int)$shsd_id;         
         if (strpos($shsd_id, '.') === false) {           
            //The word was NOT found
            $c1 = new Criteria();
            $c1->add(UsulanSSHPeer::ID_USULAN, $ids);
            if($shsd_usulan = UsulanSSHPeer::doSelectOne($c1))
            {
                $shsd_usulan->setKeterangan($keterangan);
                $shsd_usulan->save();
            }          
        }
        else
        {
            $c1 = new Criteria();
            $c1->add(UsulanSSHPeer::SHSD_ID,  $shsd_id);
            if($shsd_usulan = UsulanSSHPeer::doSelectOne($c1))
            {
                $shsd_usulan->setKeterangan($keterangan);
                $shsd_usulan->save();
            }
        } 

        $c = new Criteria();
        $c->add(ShsdSurveyPeer::SHSD_ID, $shsd_id);
        if ($shsd = ShsdSurveyPeer::doSelectOne($c)) {
            $shsd->setShsdName($shsd_name);
            $shsd->setSurveyor($surveyor);
            $shsd->setSpec($spec);
            $shsd->setHiddenSpec($hidden_spec);
            $shsd->setTanggalSurvey($tanggal);
            // $shsd->setNonPajak($non_pajak ? TRUE : FALSE);
            $shsd->setToko1($toko1);
            $shsd->setMerk1($merk1);
            $shsd->setKeterangan1($keterangan1);
            $shsd->setHarga1($harga1);
            $shsd->setFile1($file1);
            $shsd->setToko2($toko2);
            $shsd->setMerk2($merk2);
            $shsd->setKeterangan2($keterangan2);
            $shsd->setHarga2($harga2);
            $shsd->setFile2($file2);
            $shsd->setToko3($toko3);
            $shsd->setMerk3($merk3);
            $shsd->setKeterangan3($keterangan3);
            $shsd->setHarga3($harga3);
            $shsd->setFile3($file3);
            $shsd->setShsdHargaPakai($shsd_harga_pakai);
            if($is_construct == 1){
                $shsd->setShsdHargaLelang($shsd_harga_lelang);
                $shsd->setFileLelang($file_lelang);
            }
            $shsd->setFile12($file1_2);
            $shsd->setFile22($file2_2);
            $shsd->setFile32($file3_2);
            $shsd->setShsdCatatan($catatan_survey);
            $shsd->setStatusVerifikasi($status_verif);
            $shsd->save();

            historyUserLog::edit_komponen_survey($shsd_id, $surveyor);
        }
        $this->setFlash('berhasil', 'Komponen survey ' . $shsd_name . ' telah berhasil diubah');
        return $this->redirect('survey/editKomponen?id='.$shsd_id.'&page=' . $halaman);
    }

    public function executeSshlocked() {
        $this->processFilterssshlocked();
        $this->filters = $this->getUser()->getAttributeHolder()->getAll('sf_admin/sshlocked/filters');
        $this->$user = $this->getUser()->getAttribute("id"); 

        $pagers = new sfPropelPager('ShsdSurvey', 25);
        $c = new Criteria();
        $c->add(ShsdSurveyPeer::SHSD_HARGA, 0, Criteria::GREATER_THAN);
        $c->addAscendingOrderByColumn(ShsdSurveyPeer::SHSD_ID);
        $c->setDistinct();
        $this->addFiltersCriteriasshlocked($c);

        // hitung jumlah data
        $this->ttl_harga = array();
        for($i = 1; $i <= 4; $i++) {
            if($i <= 3) {
                $query_harga1 =
                "SELECT COUNT(shsd_id) AS jumlah
                FROM " . sfConfig::get('app_default_schema') . ".shsd_survey
                WHERE harga".$i." <> 0 AND harga".$i." IS NOT NULL AND shsd_harga <> 0";
            } else {
                $query_harga1 =
                "SELECT COUNT(shsd_id) AS jumlah
                FROM " . sfConfig::get('app_default_schema') . ".shsd_survey
                WHERE shsd_harga_pakai <> 0 AND shsd_harga_pakai IS NOT NULL AND shsd_harga <> 0";
            }
            if (isset($this->filters['select'])) {
                if ($this->filters['select'] == 1) {
                    if (isset($this->filters['komponen_name_is_empty'])) {
                        $query_harga1 .= " AND (shsd_name = '' OR shsd_name IS NULL)";
                    } else if (isset($this->filters['komponen']) && $this->filters['komponen'] !== '') {
                        $kata = '%' . $this->filters['komponen'] . '%';
                        $query_harga1 .= " AND shsd_name ILIKE '$kata'";
                    }
                } elseif ($this->filters['select'] == 2) {
                    if (isset($this->filters['komponen_code_is_empty'])) {
                        $query_harga1 .= " AND (shsd_id = '' OR shsd_id IS NULL)";
                    } else if (isset($this->filters['komponen']) && $this->filters['komponen'] !== '') {
                        $kata = '%' . $this->filters['komponen'] . '%';
                        $query_harga1 .= " AND shsd_id ILIKE '$kata'";
                    }
                } elseif ($this->filters['select'] == 3) {
                    if (isset($this->filters['rekening_code_is_empty'])) {
                        $query_harga1 .= " AND (rekening_code = '' OR rekening_code IS NULL)";
                    } else if (isset($this->filters['komponen']) && $this->filters['komponen'] !== '') {
                        $kata = '%' . $this->filters['komponen'] . '%';
                        $query_harga1 .= " AND rekening_code ILIKE '$kata'";
                    }
                } else {
                    if (isset($this->filters['komponen_is_empty'])) {
                        echo 'is empty';
                        $query_harga1 .= " AND (shsd_name = '' OR shsd_name IS NULL)";
                    }
                }
            }
            if (isset($this->filters['kelompok1']) && $this->filters['kelompok1'] != 0) {
                if(isset($this->filters['kelompok2']) && $this->filters['kelompok2'] != 0) {
                    $kelompok = $this->filters['kelompok2'] . '.%';
                }
                else{
                    $kelompok = $this->filters['kelompok1'] . '.%';
                }

                $query_harga1 .= " AND shsd_id ILIKE '$kelompok'";
            }
            if(isset($this->filters['tipe']) && $this->filters['tipe'] != 0){
                if ($this->filters['tipe'] == 1 ) {
                    // var_dump(sfConfig::get('sf_root_dir'));die();
                    $kelompok .= '.%.F';
                    $query_harga1 .= " AND shsd_id ILIKE '$kelompok'";
                }
                if($this->filters['tipe'] == 2){
                    $kelompok = "%.F";
                    $query_harga1 .= " AND shsd_id NOT ILIKE '$kelompok'";
                }
            }
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query_harga1);
            $rs = $stmt->executeQuery();
            if($rs->next())
                $this->ttl_harga[$i] = $rs->getString('jumlah');
        }
        // end

        $pagers->setCriteria($c);
        $pagers->setPage($this->getRequestParameter('page', 1));
        $pagers->init();

        $this->pager = $pagers;

        $query = "select kategori_shsd_id, kategori_shsd_name "
                . " from " . sfConfig::get('app_default_schema') . ".kategori_shsd "
                . " where char_length(kategori_shsd_id)=2 "
                . " order by kategori_shsd_id";
        $arrKelompok = array(0 => '--- Pilih Kelompok Barang ---');
        //print_r($query);
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query);
        $rs = $stmt->executeQuery();
        while ($rs->next()) {
            $arrKelompok[$rs->getString('kategori_shsd_id')] = $rs->getString('kategori_shsd_id') . ' - ' . $rs->getString('kategori_shsd_name');
        }
        $this->arrKelompok = $arrKelompok;
        
        if($this->filters['kelompok1'] !== NULL && $this->filters['kelompok1'] != 0){
            if($this->filters['kelompok2'] !== NULL && $this->filters['kelompok2'] != 0){
                $this->$id_kelompok2 = $this->filters['kelompok2'];
            }
            else{
                $this->id_kelompok2 = null;
            }
            $query = "select kategori_shsd_id, kategori_shsd_name "
                . " from " . sfConfig::get('app_default_schema') . ".kategori_shsd "
                . " where char_length(kategori_shsd_id)=5 and kategori_shsd_id  ilike '".$this->filters['kelompok1'].".%'"
                . " order by kategori_shsd_id";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $arrKelompok2 = array();
            if($rs_kategori2 = $stmt->executeQuery()){
                while($rs_kategori2->next()){
                    $arrKelompok2[$rs_kategori2->getString('kategori_shsd_id')] = $rs_kategori2->getString('kategori_shsd_id').' - '.$rs_kategori2->getString('kategori_shsd_name');
                }
            }
            $this->arrKelompok2 = $arrKelompok2;
        }
    }

    protected function processFilterssshlocked() {
        if ($this->getRequest()->hasParameter('filter')) {
            $filters = $this->getRequestParameter('filters');
            $this->getUser()->getAttributeHolder()->removeNamespace('sf_admin/sshlocked/filters');
            $this->getUser()->getAttributeHolder()->add($filters, 'sf_admin/sshlocked/filters');
        }
    }

    protected function addFiltersCriteriasshlocked($c) {
        $kata = "";
        $kelompok = "";
        if (isset($this->filters['select'])) {
            if ($this->filters['select'] == 1) {
                if (isset($this->filters['komponen_name_is_empty'])) {
                    $criterion = $c->getNewCriterion(ShsdSurveyPeer::SHSD_NAME, '');
                    $criterion->addOr($c->getNewCriterion(ShsdSurveyPeer::SHSD_NAME, null, Criteria::ISNULL));
                    $c->add($criterion);
                } else if (isset($this->filters['komponen']) && $this->filters['komponen'] !== '') {
                    $kata = '%' . $this->filters['komponen'] . '%';
                    $c->add(ShsdSurveyPeer::SHSD_NAME, strtr($kata, '*', '%'), Criteria::ILIKE);
                }
            } elseif ($this->filters['select'] == 2) {
                if (isset($this->filters['komponen_code_is_empty'])) {
                    $criterion = $c->getNewCriterion(ShsdSurveyPeer::SHSD_ID, '');
                    $criterion->addOr($c->getNewCriterion(ShsdSurveyPeer::SHSD_ID, null, Criteria::ISNULL));
                    $c->add($criterion);
                } else if (isset($this->filters['komponen']) && $this->filters['komponen'] !== '') {
                    $kata = '%' . $this->filters['komponen'] . '%';
                    $c->add(ShsdSurveyPeer::SHSD_ID, strtr($kata, '*', '%'), Criteria::ILIKE);
                }
            } elseif ($this->filters['select'] == 3) {
                if (isset($this->filters['rekening_code_is_empty'])) {
                    $criterion = $c->getNewCriterion(ShsdSurveyPeer::REKENING_CODE, '');
                    $criterion->addOr($c->getNewCriterion(ShsdSurveyPeer::REKENING_CODE, null, Criteria::ISNULL));
                    $c->add($criterion);
                } else if (isset($this->filters['komponen']) && $this->filters['komponen'] !== '') {
                    $kata = '%' . $this->filters['komponen'] . '%';
                    $c->add(ShsdSurveyPeer::REKENING_CODE, strtr($kata, '*', '%'), Criteria::ILIKE);
                }
            } else {
                if (isset($this->filters['komponen_is_empty'])) {
                    echo 'is empty';
                    $criterion = $c->getNewCriterion(ShsdSurveyPeer::SHSD_NAME, '');
                    $criterion->addOr($c->getNewCriterion(ShsdSurveyPeer::SHSD_NAME, null, Criteria::ISNULL));
                    $c->add($criterion);
                }
            }
        }
        if (isset($this->filters['kelompok1']) && $this->filters['kelompok1'] != 0) {
            if(isset($this->filters['kelompok2']) && $this->filters['kelompok2'] != 0){
                $kelompok = $this->filters['kelompok2'] . '.%';
            }
            else{
                $kelompok = $this->filters['kelompok1'] . '.%';
            }

            $c->add(ShsdSurveyPeer::SHSD_ID, strtr($kelompok, '*', '%'), Criteria::ILIKE);
        }

        if(isset($this->filters['tipe']) && $this->filters['tipe'] != 0){
            if ($this->filters['tipe'] == 1 ) {
                // var_dump(sfConfig::get('sf_root_dir'));die();
                $kelompok .= '.%.F';
                $c->add(ShsdSurveyPeer::SHSD_ID, strtr($kelompok, '*', '%'), Criteria::ILIKE);
            }
            if($this->filters['tipe'] == 2){
                $c->addAnd(ShsdSurveyPeer::SHSD_ID,"%.F", Criteria::NOT_ILIKE);
            }
        }
    }

    public function executePilihKelompok()
    {
        $kategori_shsd_id = $this->getRequestParameter('kategori');
        $query = "select kategori_shsd_id, kategori_shsd_name "
                . " from " . sfConfig::get('app_default_schema') . ".kategori_shsd "
                . " where char_length(kategori_shsd_id)=5 and kategori_shsd_id  ilike '".$kategori_shsd_id.".%'"
                . " order by kategori_shsd_id";
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query);
        $arr_kategori2 = array();
        if($rs_kategori2 = $stmt->executeQuery()){
            while($rs_kategori2->next()){
                $arr_kategori2[$rs_kategori2->getString('kategori_shsd_id')] = $rs_kategori2->getString('kategori_shsd_id')." - ". $rs_kategori2->getString('kategori_shsd_name');
            }
        }
        if($kategori_shsd_id == '.F')
            $arr_kategori2['.F'] = 'Kelompok Komponen .F';
        $this->arr_kategori2 = $arr_kategori2;

    }

    public function executeSshusulan() {
        $this->processFilterssshusulan();
        $this->filters = $this->getUser()->getAttributeHolder()->getAll('sf_admin/sshusulan/filters');
        $this->$user = $this->getUser()->getAttribute("id"); 

        $pagers = new sfPropelPager('ShsdSurvey', 25);
        $c = new Criteria();
        $c->add(ShsdSurveyPeer::SHSD_HARGA, NULL, Criteria::ISNULL);
        $c->addAscendingOrderByColumn(ShsdSurveyPeer::SHSD_ID);
        $c->setDistinct();
        $this->addFiltersCriteriasshusulan($c);

        // hitung jumlah data
        $this->ttl_harga = array();
        for($i = 1; $i <= 4; $i++) {
            if($i <= 3) {
                $query_harga1 =
                "SELECT COUNT(shsd_id) AS jumlah
                FROM " . sfConfig::get('app_default_schema') . ".shsd_survey
                WHERE harga".$i." <> 0 AND harga".$i." IS NOT NULL";
            } else {
                $query_harga1 =
                "SELECT COUNT(shsd_id) AS jumlah
                FROM " . sfConfig::get('app_default_schema') . ".shsd_survey
                WHERE shsd_harga_pakai <> 0 AND shsd_harga_pakai IS NOT NULL";
            }
            if (isset($this->filters['select'])) {
                if ($this->filters['select'] == 1) {
                    if (isset($this->filters['komponen_name_is_empty'])) {
                        $query_harga1 .= " AND (shsd_name = '' OR shsd_name IS NULL)";
                    } else if (isset($this->filters['komponen']) && $this->filters['komponen'] !== '') {
                        $kata = '%' . $this->filters['komponen'] . '%';
                        $query_harga1 .= " AND shsd_name ILIKE '$kata'";
                    }
                } elseif ($this->filters['select'] == 2) {
                    if (isset($this->filters['komponen_code_is_empty'])) {
                        $query_harga1 .= " AND (shsd_id = '' OR shsd_id IS NULL)";
                    } else if (isset($this->filters['komponen']) && $this->filters['komponen'] !== '') {
                        $kata = '%' . $this->filters['komponen'] . '%';
                        $query_harga1 .= " AND shsd_id ILIKE '$kata'";
                    }
                } elseif ($this->filters['select'] == 3) {
                    if (isset($this->filters['rekening_code_is_empty'])) {
                        $query_harga1 .= " AND (rekening_code = '' OR rekening_code IS NULL)";
                    } else if (isset($this->filters['komponen']) && $this->filters['komponen'] !== '') {
                        $kata = '%' . $this->filters['komponen'] . '%';
                        $query_harga1 .= " AND rekening_code ILIKE '$kata'";
                    }
                } else {
                    if (isset($this->filters['komponen_is_empty'])) {
                        echo 'is empty';
                        $query_harga1 .= " AND (shsd_name = '' OR shsd_name IS NULL)";
                    }
                }
            }
            
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query_harga1);
            $rs = $stmt->executeQuery();
            if($rs->next())
                $this->ttl_harga[$i] = $rs->getString('jumlah');
        }
        // end

        $pagers->setCriteria($c);
        $pagers->setPage($this->getRequestParameter('page', 1));
        $pagers->init();

        $this->pager = $pagers;
    }

    protected function processFilterssshusulan() {
        if ($this->getRequest()->hasParameter('filter')) {
            $filters = $this->getRequestParameter('filters');
            $this->getUser()->getAttributeHolder()->removeNamespace('sf_admin/sshusulan/filters');
            $this->getUser()->getAttributeHolder()->add($filters, 'sf_admin/sshusulan/filters');
        }
    }

    protected function addFiltersCriteriasshusulan($c) {
        $kata = "";
        $kelompok = "";
        if (isset($this->filters['select'])) {
            if ($this->filters['select'] == 1) {
                if (isset($this->filters['komponen_name_is_empty'])) {
                    $criterion = $c->getNewCriterion(ShsdSurveyPeer::SHSD_NAME, '');
                    $criterion->addOr($c->getNewCriterion(ShsdSurveyPeer::SHSD_NAME, null, Criteria::ISNULL));
                    $c->add($criterion);
                } else if (isset($this->filters['komponen']) && $this->filters['komponen'] !== '') {
                    $kata = '%' . $this->filters['komponen'] . '%';
                    $c->add(ShsdSurveyPeer::SHSD_NAME, strtr($kata, '*', '%'), Criteria::ILIKE);
                }
            } elseif ($this->filters['select'] == 2) {
                if (isset($this->filters['komponen_code_is_empty'])) {
                    $criterion = $c->getNewCriterion(ShsdSurveyPeer::SHSD_ID, '');
                    $criterion->addOr($c->getNewCriterion(ShsdSurveyPeer::SHSD_ID, null, Criteria::ISNULL));
                    $c->add($criterion);
                } else if (isset($this->filters['komponen']) && $this->filters['komponen'] !== '') {
                    $kata = '%' . $this->filters['komponen'] . '%';
                    $c->add(ShsdSurveyPeer::SHSD_ID, strtr($kata, '*', '%'), Criteria::ILIKE);
                }
            } elseif ($this->filters['select'] == 3) {
                if (isset($this->filters['rekening_code_is_empty'])) {
                    $criterion = $c->getNewCriterion(ShsdSurveyPeer::REKENING_CODE, '');
                    $criterion->addOr($c->getNewCriterion(ShsdSurveyPeer::REKENING_CODE, null, Criteria::ISNULL));
                    $c->add($criterion);
                } else if (isset($this->filters['komponen']) && $this->filters['komponen'] !== '') {
                    $kata = '%' . $this->filters['komponen'] . '%';
                    $c->add(ShsdSurveyPeer::REKENING_CODE, strtr($kata, '*', '%'), Criteria::ILIKE);
                }
            } else {
                if (isset($this->filters['komponen_is_empty'])) {
                    echo 'is empty';
                    $criterion = $c->getNewCriterion(ShsdSurveyPeer::SHSD_NAME, '');
                    $criterion->addOr($c->getNewCriterion(ShsdSurveyPeer::SHSD_NAME, null, Criteria::ISNULL));
                    $c->add($criterion);
                }
            }
        }
    }

    public function handleErrorUbahPass() {
        return sfView::SUCCESS;
    }

    public function executeUploadSurvey() {
        return sfView::SUCCESS;
    }

    public function executeSaveUpload() {
        $con = Propel::getConnection();
        $con->begin();
        try {
            $array_file_xls = explode('.', $this->getRequest()->getFileName('file'));

            $fileName_xls = 'datasurvey_' . date('Y-m-d_H-i') . '.' . $array_file_xls[1];

            $this->getRequest()->moveFile('file', sfConfig::get('sf_root_dir') . '/web/uploads/data_survey/' . $fileName_xls);

            $file_path_xls = sfConfig::get('sf_root_dir') . '/web/uploads/data_survey/' . $fileName_xls;

            if (!file_exists($file_path_xls)) {
                unlink(sfConfig::get('sf_root_dir') . '/web/uploads/data_survey/' . $fileName_xls);
                $this->setFlash('gagal', 'CEK FILE ' . $fileName_xls);
                return $this->redirect('survey/uploadSurvey');
            }

            $objReader = new PHPExcel_Reader_Excel5;
            $objPHPExcel = $objReader->load($file_path_xls);
            $i = 0;
            $ada_error = array();
            while ($i < $objPHPExcel->getSheetCount()) {
                $objPHPExcel->setActiveSheetIndex($i);
                $objWorksheet = $objPHPExcel->getActiveSheet();
                $highR = $objWorksheet->getHighestRow();
                for ($row = 6; $row <= $highR; $row++) {

                    $komponen_id = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue()));
                    $nama_val = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue()));
                    $merk_val = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(2, $row)->getValue()));
                    $spec_val = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(3, $row)->getValue()));
                    $hidspec_val = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(4, $row)->getValue()));
                    $satuan_val = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(5, $row)->getValue()));
                    $harga_val = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(6, $row)->getValue()));
                    if(empty($harga_val))
                        $harga_val = 0;

                    $rekening_val = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(7, $row)->getValue()));
                    $pajak_val = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(8, $row)->getValue()));
                    if($pajak_val == 0)
                        $pajak_bol='true';
                    else
                        $pajak_bol='false';

                    $surveyor_val = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(9, $row)->getValue()));
                    $nama_val = addslashes($nama_val);
                    $merk_val = addslashes($merk_val);
                    $spec_val = addslashes($spec_val);
                    $hidspec_val = addslashes($hidspec_val);
                    $satuan_val = addslashes($satuan_val);
                    $komponen_name = $nama_val. ' '. $spec_val;
                    
                    if ($komponen_id != '') {                                           
                        $c_cek = new Criteria();
                        $c_cek->add(ShsdSurveyPeer::SHSD_ID, $komponen_id);   
                        $c_cek->add(ShsdSurveyPeer::SHSD_HARGA, 0, Criteria::GREATER_THAN);                      
                        if (ShsdSurveyPeer::doSelectOne($c_cek)) {
                            $ada_error[] = "kode komponen $komponen_id sudah ada";
                        } else {
                            $query = "INSERT INTO " . sfConfig::get('app_default_schema') . ".shsd_survey
                            (shsd_id, satuan, shsd_name, shsd_harga, rekening_code, shsd_merk, non_pajak, nama_dasar, spec, hidden_spec, surveyor)
                            VALUES ('$komponen_id', '$satuan_val', '$komponen_name', $harga_val, '$rekening_val','$merk_val', '$pajak_bol', '$nama_val', '$spec_val', '$hidspec_val', '$surveyor_val')";
                            // print_r($query);exit;
                            $stmt = $con->prepareStatement($query);
                            $stmt->executeQuery();               
                        }
                    }
                }
                $i++;
            }
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/data_survey/' . $fileName_xls);
            if ($ada_error == null) {
                $con->commit();
                $this->setFlash('berhasil', 'Data survey berhasil diupload dengan file Excel');
            } else {
                $con->rollback();
                $error = implode(' - ', $ada_error);
                $this->setFlash('gagal', 'Gagal karena ' . $error);
            }
        } catch (Exception $ex) {
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/data_survey/' . $fileName_xls);
            //echo $ex; exit;
            $con->rollback();
            $this->setFlash('gagal', 'Gagal karena ' . $ex->getMessage());
        }
        unlink(sfConfig::get('sf_root_dir') . '/web/uploads/data_survey/' . $fileName_xls);
        return $this->redirect('survey/uploadSurvey');
    }

    public function executeSaveUploadlama() {
        $file = $this->getRequest()->getFileName('file');

        if(!$file) {
            $this->setFlash('gagal', 'Data Excel Survey SSH harus diupload');
            return $this->redirect('survey/uploadSurvey');
        } else {
            $array_file_excel = explode('.', $file);
            if (!in_array($array_file_excel[count($array_file_excel) - 1], array('xls'))) {
                $this->setFlash('gagal', 'Gunakan file dengan format xls');
                return $this->redirect('survey/uploadSurvey');
            }

            $fileName_compress = 'file_excel_' . date('Y-m-d_H-i-s') . '.' . $array_file_excel[1];

            $this->getRequest()->moveFile('file', sfConfig::get('sf_root_dir') . '/web/uploads/data_survey/' . $fileName_compress);

            $file_path_excel = sfConfig::get('sf_root_dir') . '/web/uploads/data_survey/' . $fileName_compress;

            if (!file_exists($file_path_excel)) {
                unlink(sfConfig::get('sf_root_dir') . '/web/uploads/data_survey/' . $fileName_compress);
                $this->setFlash('gagal', 'CEK FILE ' . $fileName_compress);
                return $this->redirect('survey/uploadSurvey');
            }
        }

        // mulai baca excel
        $objReader = new PHPExcel_Reader_Excel5;
        $objPHPExcel = $objReader->load($file_path_excel);

        $objPHPExcel->getActiveSheetIndex(0);
        $objWorksheet = $objPHPExcel->getActiveSheet();

        // tentuin cells utk tiap data
        $shsd_id = 'A';
        $nama = 'B';
        $merk = 'C';
        $spec = 'D';
        $hidspec = 'E';
        $satuan = 'F';
        $harga = 'G';
        $rekening = 'H';
        $pajak = 'I';
        $surveyor = 'J';

        $mulai = 6;
        // end

        $komponen_id = '';
        do {
            $komponen_id = utf8_encode(trim($objWorksheet->getCell($shsd_id . $mulai)->getValue()));
            $nama_val = utf8_encode(trim($objWorksheet->getCell($nama . $mulai)->getValue()));
            $merk_val = utf8_encode(trim($objWorksheet->getCell($merk . $mulai)->getValue()));
            $spec_val = utf8_encode(trim($objWorksheet->getCell($spec . $mulai)->getValue()));
            $hidspec_val = utf8_encode(trim($objWorksheet->getCell($hidspec . $mulai)->getValue()));
            $satuan_val = utf8_encode(trim($objWorksheet->getCell($satuan . $mulai)->getValue()));
            $harga_val = utf8_encode(trim($objWorksheet->getCell($harga . $mulai)->getValue()));
            if(empty($harga_val))
                $harga_val = 0;

            $rekening_val = utf8_encode(trim($objWorksheet->getCell($rekening . $mulai)->getValue()));
            $pajak_val = utf8_encode(trim($objWorksheet->getCell($pajak . $mulai)->getValue()));
            if($pajak_val == 0)
                $pajak_bol='true';
            else
                $pajak_bol='false';

            $surveyor_val = utf8_encode(trim($objWorksheet->getCell($surveyor . $mulai)->getValue()));

            // $merk2_val = utf8_encode(trim($objWorksheet->getCell($merk2 . $mulai)->getValue()));
            // $keterangan2_val = utf8_encode(trim($objWorksheet->getCell($keterangan2 . $mulai)->getValue()));
            // $harga2_val = utf8_encode(trim($objWorksheet->getCell($harga2 . $mulai)->getValue()));
            // if(empty($harga2_val))
            //     $harga2_val = 0;
            // $toko2_val = utf8_encode(trim($objWorksheet->getCell($toko2 . $mulai)->getValue()));
            // $surveyor2_val = utf8_encode(trim($objWorksheet->getCell($surveyor2 . $mulai)->getValue()));

            // $merk3_val = utf8_encode(trim($objWorksheet->getCell($merk3 . $mulai)->getValue()));
            // $keterangan3_val = utf8_encode(trim($objWorksheet->getCell($keterangan3 . $mulai)->getValue()));
            // $harga3_val = utf8_encode(trim($objWorksheet->getCell($harga3 . $mulai)->getValue()));
            // if(empty($harga3_val))
            //     $harga3_val = 0;
            // $toko3_val = utf8_encode(trim($objWorksheet->getCell($toko3 . $mulai)->getValue()));
            // $surveyor3_val = utf8_encode(trim($objWorksheet->getCell($surveyor3 . $mulai)->getValue()));

            $nama_val = addslashes($nama_val);
            $merk_val = addslashes($merk_val);
            $spec_val = addslashes($spec_val);
            $hidspec_val = addslashes($hidspec_val);
            $satuan_val = addslashes($satuan_val);
            $komponen_name = $nama_val. ' '. $spec_val;
            // $toko3_val = addslashes($toko3_val);
            // $keterangan1_val = addslashes($keterangan1_val);
            // $keterangan2_val = addslashes($keterangan2_val);
            // $keterangan3_val = addslashes($keterangan3_val);

            // $con = Propel::getConnection();
            // $query =
            // "SELECT merk1, merk2, merk3, toko1, toko2, toko3, harga1, harga2, harga3
            // FROM " . sfConfig::get('app_default_schema') . ".shsd_survey
            // WHERE shsd_id = '$komponen_id'";
            // $stmt = $con->prepareStatement($query);
            // $rs = $stmt->executeQuery();
            $con = Propel::getConnection();
            $con->begin();
            try {

                $c_cek = new Criteria();                         
                $c_cek->add(ShsdSurveyPeer::SHSD_ID, $shsd_id); 
                $shsd_cek = ShsdSurveyPeer::doSelectOne($c_cek);
                if (!$shsd_cek) {
                    // print_r('cek');exit();
                    $query =
                    "INSERT INTO " . sfConfig::get('app_default_schema') . ".shsd_survey
                    (shsd_id, satuan, shsd_name, shsd_harga, rekening_code, shsd_merk, non_pajak, nama_dasar, spec, hidden_spec, surveyor)
                    VALUES ('$komponen_id', '$satuan_val', '$komponen_name', $harga_val, '$rekening_val','$merk_val', '$pajak_bol', '$nama_val', '$spec_val', '$hidspec_val', '$surveyor_val')
                    ";
                }
                // SET merk1 = CASE WHEN merk1 IS NULL OR TRIM(merk1) = '' THEN E'$merk1_val' ELSE merk1 END,
                // merk2 = CASE WHEN merk2 IS NULL OR TRIM(merk2) = '' THEN E'$merk2_val' ELSE merk2 END,
                // merk3 = CASE WHEN merk3 IS NULL OR TRIM(merk3) = '' THEN E'$merk3_val' ELSE merk3 END,
                // toko1 = CASE WHEN toko1 IS NULL OR TRIM(toko1) = '' THEN E'$toko1_val' ELSE toko1 END,
                // toko2 = CASE WHEN toko2 IS NULL OR TRIM(toko2) = '' THEN E'$toko2_val' ELSE toko2 END,
                // toko3 = CASE WHEN toko3 IS NULL OR TRIM(toko3) = '' THEN E'$toko3_val' ELSE toko3 END,
                // harga1 = CASE WHEN harga1 IS NULL OR harga1 = 0 THEN $harga1_val ELSE harga1 END,
                // harga2 = CASE WHEN harga2 IS NULL OR harga2 = 0 THEN $harga2_val ELSE harga2 END,
                // harga3 = CASE WHEN harga3 IS NULL OR harga3 = 0 THEN $harga3_val ELSE harga3 END,
                // keterangan1 = CASE WHEN keterangan1 IS NULL OR TRIM(keterangan1) = '' THEN E'$keterangan1_val' ELSE keterangan1 END,
                // keterangan2 = CASE WHEN keterangan2 IS NULL OR TRIM(keterangan2) = '' THEN E'$keterangan2_val' ELSE keterangan2 END,
                // keterangan3 = CASE WHEN keterangan3 IS NULL OR TRIM(keterangan3) = '' THEN E'$keterangan3_val' ELSE keterangan3 END
                // WHERE shsd_id = '$komponen_id'
                $stmt = $con->prepareStatement($query);
                $rs = $stmt->executeQuery();

                // $insert = new ShsdSurvey();
                // $insert->setShsdId($komponen_id);
                // $insert->setNamaDasar($nama_val);
                // $insert->setShsdMerk($merk_val);
                // $insert->setSpec($spec_val);
                // $insert->setShsdName($komponen_name);
                // $insert->setHiddenSpec($hidspec_val);
                // $insert->setSatuan($satuan_val);
                // $insert->setShsdHarga($harga_val);
                // $insert->setRekeningCode($rekening_val);
                // $insert->setNonPajak($pajak_val);
                // $insert->setSurveyor($surveyor_val);

                // $insert->save();
                $con->commit();
                
            } catch(Exception $e) {
                $con->rollback();
                $this->setFlash('gagal', 'Terjadi kesalahan ' . $e->getMessage());
                return $this->redirect('survey/uploadSurvey');
            }

            $mulai++;
        } while(!empty($komponen_id));

        historyUserLog::upload_excel_survey($fileName_compress);
        $this->setFlash('berhasil', 'Data survey berhasil diupload dengan file Excel');
        return $this->redirect('survey/uploadSurvey');
    }

    public function executeSshKonfirmasiAll() {
        $c = new Criteria();
        
        $c->add(ShsdSurveyPeer::STATUS_SURVEY, FALSE);
        $c->add(ShsdSurveyPeer::SHSD_HARGA, 0, Criteria::GREATER_THAN);
        $c->addAscendingOrderByColumn(ShsdSurveyPeer::SHSD_ID);
        $rs_sshSurvey = ShsdSurveyPeer::doSelect($c);
        $this->rs_sshSurvey = $rs_sshSurvey; 
    }

    public function executeSshkonfirmasi() {
        $this->processFilterssshkonfirmasi();
        $this->filters = $this->getUser()->getAttributeHolder()->getAll('sf_admin/sshkonfirmasi/filters');
        $this->$user = $this->getUser()->getAttribute("id");
        $user_surveyor = $this->getUser()->getNamaLengkap();

        $pagers = new sfPropelPager('ShsdSurvey', 25);
        $c = new Criteria();
        $c->add(ShsdSurveyPeer::SHSD_HARGA, 0, Criteria::GREATER_THAN);
        if ($this->getUser()->getNamaLengkap() == 'Tim Survey') {
            $c->add(ShsdSurveyPeer::STATUS_SURVEY, TRUE);
            $c->add(ShsdSurveyPeer::STATUS_VERIFIKASI, FALSE);
        }
        else 
        {
            $c->add(ShsdSurveyPeer::SURVEYOR, $user_surveyor, Criteria::ILIKE); 
        }
        $c->addAscendingOrderByColumn(ShsdSurveyPeer::SHSD_ID);
        $c->setDistinct();
        $this->addFiltersCriteriasshkonfirmasi($c);

        // hitung jumlah data
        $this->ttl_harga = array();
        for($i = 1; $i <= 4; $i++) {
            if($i <= 3) {
                $query_harga1 =
                "SELECT COUNT(shsd_id) AS jumlah
                FROM " . sfConfig::get('app_default_schema') . ".shsd_survey
                WHERE harga".$i." <> 0 AND harga".$i." IS NOT NULL";
            } else {
                $query_harga1 =
                "SELECT COUNT(shsd_id) AS jumlah
                FROM " . sfConfig::get('app_default_schema') . ".shsd_survey
                WHERE shsd_harga_pakai <> 0 AND shsd_harga_pakai IS NOT NULL";
            }
            if (isset($this->filters['select'])) {
                if ($this->filters['select'] == 1) {
                    if (isset($this->filters['komponen_name_is_empty'])) {
                        $query_harga1 .= " AND (shsd_name = '' OR shsd_name IS NULL)";
                    } else if (isset($this->filters['komponen']) && $this->filters['komponen'] !== '') {
                        $kata = '%' . $this->filters['komponen'] . '%';
                        $query_harga1 .= " AND shsd_name ILIKE '$kata'";
                    }
                } elseif ($this->filters['select'] == 2) {
                    if (isset($this->filters['komponen_code_is_empty'])) {
                        $query_harga1 .= " AND (shsd_id = '' OR shsd_id IS NULL)";
                    } else if (isset($this->filters['komponen']) && $this->filters['komponen'] !== '') {
                        $kata = '%' . $this->filters['komponen'] . '%';
                        $query_harga1 .= " AND shsd_id ILIKE '$kata'";
                    }
                } elseif ($this->filters['select'] == 3) {
                    if (isset($this->filters['rekening_code_is_empty'])) {
                        $query_harga1 .= " AND (rekening_code = '' OR rekening_code IS NULL)";
                    } else if (isset($this->filters['komponen']) && $this->filters['komponen'] !== '') {
                        $kata = '%' . $this->filters['komponen'] . '%';
                        $query_harga1 .= " AND rekening_code ILIKE '$kata'";
                    }
                } else {
                    if (isset($this->filters['komponen_is_empty'])) {
                        echo 'is empty';
                        $query_harga1 .= " AND (shsd_name = '' OR shsd_name IS NULL)";
                    }
                }
            }

            if (isset($this->filters['status'])) {
                if ($this->filters['status'] == 1) {
                    $query_harga1 .= " AND status_survey = TRUE AND status_verifikasi = TRUE";
                } elseif ($this->filters['status'] == 2) {
                    $query_harga1 .= " AND status_survey = TRUE";
                } elseif ($this->filters['status'] == 3) {
                    $query_harga1 .= " AND status_survey = FALSE AND status_verifikasi = FALSE";
                } elseif ($this->filters['status'] == 4) {
                    $query_harga1 .= " AND harga1 > 0 AND (harga2 = 0 OR harga2 IS NULL) AND (harga3 = 0 OR harga3 IS NULL)";
                } elseif ($this->filters['status'] == 5) {
                    $query_harga1 .= " AND harga1 > 0 AND harga2 > 0 AND (harga3 = 0 OR harga3 IS NULL)";
                } elseif ($this->filters['status'] == 6) {
                    $query_harga1 .= " AND harga1 > 0 AND harga2 > 0 AND harga3 > 0";
                }
            }
            
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query_harga1);
            $rs = $stmt->executeQuery();
            if($rs->next())
                $this->ttl_harga[$i] = $rs->getString('jumlah');
        }
        // end

        $pagers->setCriteria($c);
        $pagers->setPage($this->getRequestParameter('page', 1));
        $pagers->init();

        $this->pager = $pagers;
    }

    protected function processFilterssshkonfirmasi() {
        if ($this->getRequest()->hasParameter('filter')) {
            $filters = $this->getRequestParameter('filters');
            $this->getUser()->getAttributeHolder()->removeNamespace('sf_admin/sshkonfirmasi/filters');
            $this->getUser()->getAttributeHolder()->add($filters, 'sf_admin/sshkonfirmasi/filters');
        }
    }

    protected function addFiltersCriteriasshkonfirmasi($c) {
        $kata = "";
        $kelompok = "";
        if (isset($this->filters['select'])) {
            if ($this->filters['select'] == 1) {
                if (isset($this->filters['komponen_name_is_empty'])) {
                    $criterion = $c->getNewCriterion(ShsdSurveyPeer::SHSD_NAME, '');
                    $criterion->addOr($c->getNewCriterion(ShsdSurveyPeer::SHSD_NAME, null, Criteria::ISNULL));
                    $c->add($criterion);
                } else if (isset($this->filters['komponen']) && $this->filters['komponen'] !== '') {
                    $kata = '%' . $this->filters['komponen'] . '%';
                    $c->add(ShsdSurveyPeer::SHSD_NAME, strtr($kata, '*', '%'), Criteria::ILIKE);
                }
            } elseif ($this->filters['select'] == 2) {
                if (isset($this->filters['komponen_code_is_empty'])) {
                    $criterion = $c->getNewCriterion(ShsdSurveyPeer::SHSD_ID, '');
                    $criterion->addOr($c->getNewCriterion(ShsdSurveyPeer::SHSD_ID, null, Criteria::ISNULL));
                    $c->add($criterion);
                } else if (isset($this->filters['komponen']) && $this->filters['komponen'] !== '') {
                    $kata = '%' . $this->filters['komponen'] . '%';
                    $c->add(ShsdSurveyPeer::SHSD_ID, strtr($kata, '*', '%'), Criteria::ILIKE);
                }
            } elseif ($this->filters['select'] == 3) {
                if (isset($this->filters['rekening_code_is_empty'])) {
                    $criterion = $c->getNewCriterion(ShsdSurveyPeer::REKENING_CODE, '');
                    $criterion->addOr($c->getNewCriterion(ShsdSurveyPeer::REKENING_CODE, null, Criteria::ISNULL));
                    $c->add($criterion);
                } else if (isset($this->filters['komponen']) && $this->filters['komponen'] !== '') {
                    $kata = '%' . $this->filters['komponen'] . '%';
                    $c->add(ShsdSurveyPeer::REKENING_CODE, strtr($kata, '*', '%'), Criteria::ILIKE);
                }
            } else {
                if (isset($this->filters['komponen_is_empty'])) {
                    echo 'is empty';
                    $criterion = $c->getNewCriterion(ShsdSurveyPeer::SHSD_NAME, '');
                    $criterion->addOr($c->getNewCriterion(ShsdSurveyPeer::SHSD_NAME, null, Criteria::ISNULL));
                    $c->add($criterion);
                }
            }
        }

        if (isset($this->filters['status'])) {
                if ($this->filters['status'] == 1) {
                    $criterionSurvey = $c->getNewCriterion(ShsdSurveyPeer::STATUS_SURVEY, TRUE);
                    $criterionVerif = $c->getNewCriterion(ShsdSurveyPeer::STATUS_VERIFIKASI, TRUE);
                    $c->add($criterionSurvey);
                    $c->add($criterionVerif);
                    // $query_harga1 .= " AND (status_survey = TRUE AND status_verifikasi = TRUE)";
                } elseif ($this->filters['status'] == 2) {
                    $criterionSurvey = $c->getNewCriterion(ShsdSurveyPeer::STATUS_SURVEY, TRUE);
                    $criterionVerif = $c->getNewCriterion(ShsdSurveyPeer::STATUS_VERIFIKASI, FALSE);
                    $c->add($criterionSurvey);
                    $c->add($criterionVerif);
                    // $query_harga1 .= " AND (status_survey = TRUE)";
                } elseif ($this->filters['status'] == 3) {
                    $criterionSurvey = $c->getNewCriterion(ShsdSurveyPeer::STATUS_SURVEY, FALSE);
                    $criterionVerif = $c->getNewCriterion(ShsdSurveyPeer::STATUS_VERIFIKASI, FALSE);
                    $c->add($criterionSurvey);
                    $c->add($criterionVerif);
                    // $query_harga1 .= " AND (status_survey = FALSE AND status_verifikasi = FALSE)";
                } elseif ($this->filters['status'] == 4) {
                    $criterionHarga1 = $c->getNewCriterion(ShsdSurveyPeer::HARGA1, 0, Criteria::GREATER_THAN);
                    $criterionHarga2 = $c->getNewCriterion(ShsdSurveyPeer::HARGA2, null, Criteria::ISNULL);
                    $criterionHarga2->addOr($c->getNewCriterion(ShsdSurveyPeer::HARGA2, 0));
                    $criterionHarga3 = $c->getNewCriterion(ShsdSurveyPeer::HARGA3, null, Criteria::ISNULL);
                    $criterionHarga3->addOr($c->getNewCriterion(ShsdSurveyPeer::HARGA3, 0));
                    $c->add($criterionHarga1);
                    $c->add($criterionHarga2);
                    $c->add($criterionHarga3);
                } elseif ($this->filters['status'] == 5) {
                    $criterionHarga1 = $c->getNewCriterion(ShsdSurveyPeer::HARGA1, 0, Criteria::GREATER_THAN);
                    $criterionHarga2 = $c->getNewCriterion(ShsdSurveyPeer::HARGA2, 0, Criteria::GREATER_THAN);
                    $criterionHarga3 = $c->getNewCriterion(ShsdSurveyPeer::HARGA3, null, Criteria::ISNULL);
                    $criterionHarga3->addOr($c->getNewCriterion(ShsdSurveyPeer::HARGA3, 0));
                    $c->add($criterionHarga1);
                    $c->add($criterionHarga2);
                    $c->add($criterionHarga3);
                } elseif ($this->filters['status'] == 6) {
                    $criterionHarga1 = $c->getNewCriterion(ShsdSurveyPeer::HARGA1, 0, Criteria::GREATER_THAN);
                    $criterionHarga2 = $c->getNewCriterion(ShsdSurveyPeer::HARGA2, 0, Criteria::GREATER_THAN);
                    $criterionHarga3 = $c->getNewCriterion(ShsdSurveyPeer::HARGA3, 0, Criteria::GREATER_THAN);
                    $c->add($criterionHarga1);
                    $c->add($criterionHarga2);
                    $c->add($criterionHarga3);
                }
            }
    }

    public function executeFinalSurvey() {
        $id = $this->getRequestParameter('id');
        $c = new Criteria();
        $c->add(ShsdSurveyPeer::SHSD_ID, $id);
        $rs = ShsdSurveyPeer::doSelectOne($c);
        $rs->setStatusSurvey(TRUE);
        $rs->save();

        $this->setFlash('berhasil', 'Data Survey Telah DiAjukan Ke Verifikasi.');
        return $this->redirect("survey/sshkonfirmasi");
    }

    public function executeEditKomponenSpjm() {
        $id = $this->getRequestParameter('id');

        $c = new Criteria();
        $c->add(ShsdSurveyPeer::SHSD_ID, $id);
        $c->add(ShsdSurveyPeer::SHSD_HARGA, NULL, Criteria::ISNULL);
        $this->shsd = ShsdSurveyPeer::doSelectOne($c);
 
        // cek komponen_tipe2 di komponen
        $cri = new Criteria();
        $cri->add(KomponenPeer::KOMPONEN_ID,$id);

        $komponen = KomponenPeer::doSelectOne($cri);
        $this->komponen = $komponen; 
        // var_dump($result);die();

        // kode rekenings
        $crit = new Criteria();
        $crit->addAscendingOrderByColumn(RekeningPeer::REKENING_CODE);
        $rekenings = RekeningPeer::doSelect($crit);
        $this->rekening = array();

        $idx = 0;
        foreach ($rekenings as $rekening) {
            $this->rekening[$idx]['rekening_code'] = $rekening->getRekeningCode();
            $this->rekening[$idx]['rekening_name'] = $rekening->getRekeningName();
            $idx++;
        }
    }

    public function executeEditKonfirmasi() {
        $id = $this->getRequestParameter('id');
        $this->halaman = $this->getRequestParameter('page');

        $c = new Criteria();
        $c->add(ShsdSurveyPeer::SHSD_ID, $id);
        $this->shsd = ShsdSurveyPeer::doSelectOne($c);
 
        // cek komponen_tipe2 di komponen
        $cri = new Criteria();
        $cri->add(KomponenPeer::KOMPONEN_ID,$id);

        $komponen = KomponenPeer::doSelectOne($cri);
        $this->komponen = $komponen; 
        // var_dump($result);die();

        // kode rekenings
        $crit = new Criteria();
        $crit->addAscendingOrderByColumn(RekeningPeer::REKENING_CODE);
        $rekenings = RekeningPeer::doSelect($crit);
        $this->rekening = array();

        $idx = 0;
        foreach ($rekenings as $rekening) {
            $this->rekening[$idx]['rekening_code'] = $rekening->getRekeningCode();
            $this->rekening[$idx]['rekening_name'] = $rekening->getRekeningName();
            $idx++;
        }
    }


    public function executeSaveEditKonfirmasi() {
        $is_construct = 0;
        $halaman = $this->getRequestParameter('page');
        $shsd_id = $this->getRequestParameter('shsd_id');
        $shsd_name = $this->getRequestParameter('shsd_name');
        $spec = $this->getRequestParameter('spec');
        $shsd_name_baru = $this->getRequestParameter('shsd_name_baru');
        $spec_baru = $this->getRequestParameter('spec_baru');
        $satuan_baru = $this->getRequestParameter('satuan_baru');
        $hidden_spec = $this->getRequestParameter('hidden_spec');
        
        //$tanggal = $this->getRequestParameter('tanggal');
        $non_pajak = $this->getRequestParameter('non_pajak');

        $toko1 = $this->getRequestParameter('toko1');
        $merk1 = $this->getRequestParameter('merk1');
        $keterangan1 = $this->getRequestParameter('keterangan1');
        $harga1 = preg_replace("/[^0-9]/", "", $this->getRequestParameter('harga1'));
        $file1 = $this->getRequestParameter('file1_lama');
        $file1_2 = $this->getRequestParameter('file1_2_lama');
        $tgl_konfirmasi1 = $this->getRequestParameter('tgl_konfirmasi1'); 

        $toko2 = $this->getRequestParameter('toko2');
        $merk2 = $this->getRequestParameter('merk2');
        $keterangan2 = $this->getRequestParameter('keterangan2');
        $harga2 = preg_replace("/[^0-9]/", "", $this->getRequestParameter('harga2'));
        $file2 = $this->getRequestParameter('file2_lama');
        $file2_2 = $this->getRequestParameter('file2_2_lama');
        $tgl_konfirmasi2 = $this->getRequestParameter('tgl_konfirmasi2'); 

        $toko3 = $this->getRequestParameter('toko3');
        $merk3 = $this->getRequestParameter('merk3');
        $keterangan3 = $this->getRequestParameter('keterangan3');
        $harga3 = preg_replace("/[^0-9]/", "", $this->getRequestParameter('harga3'));
        $file3 = $this->getRequestParameter('file3_lama');
        $file3_2 = $this->getRequestParameter('file3_2_lama');
        $tgl_konfirmasi3 = $this->getRequestParameter('tgl_konfirmasi3'); 

        $shsd_harga_pakai = $this->getRequestParameter('shsd_harga_pakai');
        $catatan_survey = $this->getRequestParameter('catatan_survey');

        $jmlPendukung = 0;
        if ($this->getRequest()->getFileName('file1') || $file1 != '')
            $jmlPendukung++;
        if ($this->getRequest()->getFileName('file2') || $file2 != '')
            $jmlPendukung++;
        if ($this->getRequest()->getFileName('file3') || $file3 != '')
            $jmlPendukung++;

        if($harga1 <> 0 && $tgl_konfirmasi1 == '')
        {
            $this->setFlash('gagal', 'Tanggal Dukungan 1 survey mohon untuk diisi');
            return $this->redirect('survey/sshkonfirmasi?page=' . $halaman);
            // return $this->redirect('survey/editKonfirmasi?id=' . $shsd_id . '&page=' . $halaman);
        }

        if($harga2 <> 0 && $tgl_konfirmasi2 == '')
        {
            $this->setFlash('gagal', 'Tanggal Dukungan 2 survey mohon untuk diisi');
            return $this->redirect('survey/sshkonfirmasi?page=' . $halaman);
        }

        if($harga3 <> 0 && $tgl_konfirmasi3 == '')
        {
            $this->setFlash('gagal', 'Tanggal Dukungan 3 survey mohon untuk diisi');
            return $this->redirect('survey/sshkonfirmasi?page=' . $halaman);
        }


        // if($tanggal == '')
        // {
        //     $this->setFlash('gagal', 'Tanggal survey mohon untuk diisi');
        //     return $this->redirect('survey/sshkonfirmasi?page=' . $halaman);
        // }

        if($jmlPendukung < 1) {
            $this->setFlash('gagal', 'Data pendukung yang diupload tidak boleh kurang dari 1');
            return $this->redirect('survey/sshkonfirmasi?page=' . $halaman);
        }

        if ($this->getRequest()->getFileName('file1')) {
            $array_file_pdf = explode('.', $this->getRequest()->getFileName('file1'));
            if (!in_array($array_file_pdf[count($array_file_pdf) - 1], array('zip', 'rar', 'jpg', 'pdf', 'png', 'xls', 'xlsx', 'JPG', 'PNG', 'jpeg', 'JPEG'))) {
                $this->setFlash('gagal', 'Gunakan file dengan format zip/rar/jpg/png/pdf');
                return $this->redirect('survey/sshkonfirmasi?page=' . $halaman);
            }

            $fileName_compress = 'file1_' . str_replace('.', '_', $shsd_id) . '_' . date('Y-m-d_H-i') . '.' . $array_file_pdf[count($array_file_pdf) - 1];

            $this->getRequest()->moveFile('file1', sfConfig::get('sf_root_dir') . '/web/uploads/survey/' . $fileName_compress);

            $file_path_pdf = sfConfig::get('sf_root_dir') . '/web/uploads/survey/' . $fileName_compress;

            if (!file_exists($file_path_pdf)) {
                unlink(sfConfig::get('sf_root_dir') . '/web/uploads/survey/' . $fileName_compress);
                $this->setFlash('gagal', 'CEK FILE ' . $fileName_compress);
                return $this->redirect('survey/sshkonfirmasi?page=' . $halaman);
            }
            $file1 = $fileName_compress;
        }
        if ($this->getRequest()->getFileName('file1_2')) {
            $array_file_pdf = explode('.', $this->getRequest()->getFileName('file1_2'));
            if (!in_array($array_file_pdf[count($array_file_pdf) - 1], array('zip', 'rar', 'jpg', 'pdf', 'png', 'xls', 'xlsx', 'JPG', 'PNG', 'jpeg', 'JPEG'))) {
                $this->setFlash('gagal', 'Gunakan file dengan format zip/rar/jpg/png/pdf');
                return $this->redirect('survey/sshkonfirmasi?page=' . $halaman);
            }

            $fileName_compress = 'file1_2_' . str_replace('.', '_', $shsd_id) . '_' . date('Y-m-d_H-i') . '.' . $array_file_pdf[count($array_file_pdf) - 1];

            $this->getRequest()->moveFile('file1_2', sfConfig::get('sf_root_dir') . '/web/uploads/survey/' . $fileName_compress);

            $file_path_pdf = sfConfig::get('sf_root_dir') . '/web/uploads/survey/' . $fileName_compress;

            if (!file_exists($file_path_pdf)) {
                unlink(sfConfig::get('sf_root_dir') . '/web/uploads/survey/' . $fileName_compress);
                $this->setFlash('gagal', 'CEK FILE ' . $fileName_compress);
                return $this->redirect('survey/sshkonfirmasi?page=' . $halaman);
            }
            $file1_2 = $fileName_compress;
        }

        if ($this->getRequest()->getFileName('file2')) {
            $array_file_pdf = explode('.', $this->getRequest()->getFileName('file2'));
            if (!in_array($array_file_pdf[count($array_file_pdf) - 1], array('zip', 'rar', 'jpg', 'pdf', 'png', 'xls', 'xlsx', 'JPG', 'PNG', 'jpeg', 'JPEG'))) {
                $this->setFlash('gagal', 'Gunakan file dengan format zip/rar/jpg/png/pdf');
                return $this->redirect('survey/sshkonfirmasi?page=' . $halaman);
            }

            $fileName_compress = 'file2_' . str_replace('.', '_', $shsd_id) . '_' . date('Y-m-d_H-i') . '.' . $array_file_pdf[count($array_file_pdf) - 1];

            $this->getRequest()->moveFile('file2', sfConfig::get('sf_root_dir') . '/web/uploads/survey/' . $fileName_compress);

            $file_path_pdf = sfConfig::get('sf_root_dir') . '/web/uploads/survey/' . $fileName_compress;

            if (!file_exists($file_path_pdf)) {
                unlink(sfConfig::get('sf_root_dir') . '/web/uploads/survey/' . $fileName_compress);
                $this->setFlash('gagal', 'CEK FILE ' . $fileName_compress);
                return $this->redirect('survey/sshkonfirmasi?page=' . $halaman);
            }
            $file2 = $fileName_compress;
        }
        if ($this->getRequest()->getFileName('file2_2')) {
            $array_file_pdf = explode('.', $this->getRequest()->getFileName('file2_2'));
            if (!in_array($array_file_pdf[count($array_file_pdf) - 1], array('zip', 'rar', 'jpg', 'pdf', 'png', 'xls', 'xlsx', 'JPG', 'PNG', 'jpeg', 'JPEG'))) {
                $this->setFlash('gagal', 'Gunakan file dengan format zip/rar/jpg/png/pdf');
                return $this->redirect('survey/sshkonfirmasi?page=' . $halaman);
            }

            $fileName_compress = 'file2_2_' . str_replace('.', '_', $shsd_id) . '_' . date('Y-m-d_H-i') . '.' . $array_file_pdf[count($array_file_pdf) - 1];

            $this->getRequest()->moveFile('file2_2', sfConfig::get('sf_root_dir') . '/web/uploads/survey/' . $fileName_compress);

            $file_path_pdf = sfConfig::get('sf_root_dir') . '/web/uploads/survey/' . $fileName_compress;

            if (!file_exists($file_path_pdf)) {
                unlink(sfConfig::get('sf_root_dir') . '/web/uploads/survey/' . $fileName_compress);
                $this->setFlash('gagal', 'CEK FILE ' . $fileName_compress);
                return $this->redirect('survey/sshkonfirmasi?page=' . $halaman);
            }
            $file2_2 = $fileName_compress;
        }

        if ($this->getRequest()->getFileName('file3')) {
            $array_file_pdf = explode('.', $this->getRequest()->getFileName('file3'));
            if (!in_array($array_file_pdf[count($array_file_pdf) - 1], array('zip', 'rar', 'jpg', 'pdf', 'png', 'xls', 'xlsx', 'JPG', 'PNG', 'jpeg', 'JPEG'))) {
                $this->setFlash('gagal', 'Gunakan file dengan format zip/rar/jpg/png/pdf');
                return $this->redirect('survey/sshkonfirmasi?page=' . $halaman);
            }

            $fileName_compress = 'file3_' . str_replace('.', '_', $shsd_id) . '_' . date('Y-m-d_H-i') . '.' . $array_file_pdf[count($array_file_pdf) - 1];

            $this->getRequest()->moveFile('file3', sfConfig::get('sf_root_dir') . '/web/uploads/survey/' . $fileName_compress);

            $file_path_pdf = sfConfig::get('sf_root_dir') . '/web/uploads/survey/' . $fileName_compress;

            if (!file_exists($file_path_pdf)) {
                unlink(sfConfig::get('sf_root_dir') . '/web/uploads/survey/' . $fileName_compress);
                $this->setFlash('gagal', 'CEK FILE ' . $fileName_compress);
                return $this->redirect('survey/sshkonfirmasi?page=' . $halaman);
            }
            $file3 = $fileName_compress;
        }
        if ($this->getRequest()->getFileName('file3_2')) {
            $array_file_pdf = explode('.', $this->getRequest()->getFileName('file3_2'));
            if (!in_array($array_file_pdf[count($array_file_pdf) - 1], array('zip', 'rar', 'jpg', 'pdf', 'png', 'xls', 'xlsx', 'JPG', 'PNG', 'jpeg', 'JPEG'))) {
                $this->setFlash('gagal', 'Gunakan file dengan format zip/rar/jpg/png/pdf');
                return $this->redirect('survey/sshkonfirmasi?page=' . $halaman);
            }

            $fileName_compress = 'file3_2_' . str_replace('.', '_', $shsd_id) . '_' . date('Y-m-d_H-i') . '.' . $array_file_pdf[count($array_file_pdf) - 1];

            $this->getRequest()->moveFile('file3_2', sfConfig::get('sf_root_dir') . '/web/uploads/survey/' . $fileName_compress);

            $file_path_pdf = sfConfig::get('sf_root_dir') . '/web/uploads/survey/' . $fileName_compress;

            if (!file_exists($file_path_pdf)) {
                unlink(sfConfig::get('sf_root_dir') . '/web/uploads/survey/' . $fileName_compress);
                $this->setFlash('gagal', 'CEK FILE ' . $fileName_compress);
                return $this->redirect('survey/sshkonfirmasi?page=' . $halaman);
            }
            $file3_2 = $fileName_compress;
        }

        // cek jika komponen konstruksi
        if($this->getRequestParameter('shsd_harga_lelang')){
            // var_dump("konstruksi");die();
            $is_construct = 1;
            $shsd_harga_lelang = $this->getRequestParameter('shsd_harga_lelang');

            // ubah untuk file pendukung analisa konstruksi
            if ($this->getRequest()->getFileName('file_lelang')) {
                $array_file_pdf = explode('.', $this->getRequest()->getFileName('file_lelang'));
                if (!in_array($array_file_pdf[count($array_file_pdf) - 1], array('zip', 'rar', 'jpg', 'pdf', 'png', 'xls', 'xlsx', 'JPG', 'PNG', 'jpeg', 'JPEG'))) {
                    $this->setFlash('gagal', 'Gunakan file dengan format zip/rar/jpg/png/pdf');
                    return $this->redirect('survey/sshkonfirmasi?page=' . $halaman);
                }

                $fileName_compress = 'file_lelang_' . str_replace('.', '_', $shsd_id) . '_' . date('Y-m-d_H-i') . '.' . $array_file_pdf[count($array_file_pdf) - 1];

                $this->getRequest()->moveFile('file_lelang', sfConfig::get('sf_root_dir') . '/web/uploads/survey/' . $fileName_compress);

                $file_path_pdf = sfConfig::get('sf_root_dir') . '/web/uploads/survey/' . $fileName_compress;

                if (!file_exists($file_path_pdf)) {
                    unlink(sfConfig::get('sf_root_dir') . '/web/uploads/survey/' . $fileName_compress);
                    $this->setFlash('gagal', 'CEK FILE ' . $fileName_compress);
                    return $this->redirect('survey/sshkonfirmasi?page=' . $halaman);
                }
                $file_lelang = $fileName_compress;
            }
        }

        $c = new Criteria();
        $c->add(ShsdSurveyPeer::SHSD_ID, $shsd_id);
        if ($shsd = ShsdSurveyPeer::doSelectOne($c)) {
            $shsd->setShsdName($shsd_name);
            $shsd->setSpec($spec);
            $shsd->setHiddenSpec($hidden_spec);
            //$shsd->setTanggalSurvey($tanggal);

            $date_arr= array();            

            if($tgl_konfirmasi1!='')
            {
                //$shsd->setTanggalSurvey($tgl_konfirmasi1);
                $shsd->setTglKonfirm1($tgl_konfirmasi1);
                $tgl1= date('Y-m-d',strtotime($tgl_konfirmasi1));
                array_push($date_arr,$tgl1);

            }
            if ($tgl_konfirmasi2!='')
            {
                //$shsd->setTanggalSurvey($tgl_konfirmasi2);
                $shsd->setTglKonfirm2($tgl_konfirmasi2);
                $tgl2= date('Y-m-d',strtotime($tgl_konfirmasi2));
                array_push($date_arr,$tgl2);
            }
            if ($tgl_konfirmasi3!='')
            {
                ///$shsd->setTanggalSurvey($tgl_konfirmasi3);
                $shsd->setTglKonfirm3($tgl_konfirmasi3);
                $tgl3= date('Y-m-d',strtotime($tgl_konfirmasi3));
                array_push($date_arr,$tgl3);
            }
            // $shsd->setNonPajak($non_pajak ? TRUE : FALSE);
            //ambil tgl terbaru
            $tanggal_survey=max($date_arr);

            $shsd->setTanggalSurvey($tanggal_survey);
            // $shsd->setNonPajak($non_pajak ? TRUE : FALSE);
            $shsd->setToko1($toko1);
            $shsd->setMerk1($merk1);
            $shsd->setKeterangan1($keterangan1);
            $shsd->setHarga1($harga1);
            $shsd->setFile1($file1);
            $shsd->setToko2($toko2);
            $shsd->setMerk2($merk2);
            $shsd->setKeterangan2($keterangan2);
            $shsd->setHarga2($harga2);
            $shsd->setFile2($file2);
            $shsd->setToko3($toko3);
            $shsd->setMerk3($merk3);
            $shsd->setKeterangan3($keterangan3);
            $shsd->setHarga3($harga3);
            $shsd->setFile3($file3);
            if($is_construct == 1){
                $shsd->setShsdHargaLelang($shsd_harga_lelang);
                $shsd->setFileLelang($file_lelang);
            }
            $shsd->setFile12($file1_2);
            $shsd->setFile22($file2_2);
            $shsd->setFile32($file3_2);
            $shsd->setShsdCatatan($catatan_survey);
            $shsd->setNamaDasarBaru($shsd_name_baru);
            $shsd->setSpecBaru($spec_baru);
            $shsd->setSatuanBaru($satuan_baru);
            $shsd->save();

            historyUserLog::edit_komponen_survey($shsd_id, $surveyor);
        }
        $this->setFlash('berhasil', 'Komponen survey ' . $shsd_name . ' telah berhasil diubah');
        return $this->redirect('survey/sshkonfirmasi?page=' . $halaman);
    }


    public function executeUserlist() {
        $this->processFiltersuserlist();
        $this->filters = $this->getUser()->getAttributeHolder()->getAll('sf_admin/userlist/filters');

        $pagers = new sfPropelPager('MasterUserV2', 25);
//         $c = new Criteria();
//         $nama = $this->getUser()->getNamaUser();
//         $unit_kerja = Array();
//         if ($nama != '') {
//             $e = new Criteria;
//             $e->add(UserHandleV2Peer::USER_ID, $nama);
//             $es = UserHandleV2Peer::doSelect($e);
//             $this->satuan_kerja = $es;
//             foreach ($es as $x) {
//                 $unit_kerja[] = $x->getUnitId();
//             }
//         }
//         $c->addJoin(MasterUserV2Peer::USER_ID, UserHandleV2Peer::USER_ID);
//         $c->addJoin(MasterUserV2Peer::USER_ID, SchemaAksesV2Peer::USER_ID);
// //        $c->add(UserHandleV2Peer::UNIT_ID, $unit_kerja, Criteria::IN);
// //        $c->addOr(SchemaAksesV2Peer::LEVEL_ID, 13);
// //        $c->addOr(SchemaAksesV2Peer::LEVEL_ID, 14);
// //        $c->addOr(SchemaAksesV2Peer::LEVEL_ID, 15);
//         $crit0 = $c->getNewCriterion(SchemaAksesV2Peer::LEVEL_ID, 13);
//         $crit0->addOr($c->getNewCriterion(SchemaAksesV2Peer::LEVEL_ID, 14));
//         $crit3 = $c->getNewCriterion(UserHandleV2Peer::UNIT_ID, $unit_kerja, Criteria::IN);
//         $crit0->addAnd($crit3);
//         if (in_array('0600', $unit_kerja)) {
//             $crit4 = $c->getNewCriterion(SchemaAksesV2Peer::LEVEL_ID, 16);
//             $crit0->addOr($crit4);
//         }

        // $c->add($crit0);
        // $c->addDescendingOrderByColumn(MasterUserV2Peer::USER_ENABLE);
        // $c->addAscendingOrderByColumn(MasterUserV2Peer::USER_ID);
        // $c->setDistinct();
        // $this->addFiltersCriteriauserlist($c);

        $c = new Criteria();
        $c->add(MasterUserV2Peer::USER_ID, '%survey%', Criteria::ILIKE);
        $c->addDescendingOrderByColumn(MasterUserV2Peer::USER_ENABLE);
        $c->addAscendingOrderByColumn(MasterUserV2Peer::USER_ID);
        $c->setDistinct();
        $this->addFiltersCriteriauserlist($c);

        $pagers->setCriteria($c);
        $pagers->setPage($this->getRequestParameter('page', 1));
        $pagers->init();

        $this->pager = $pagers;
    }

    protected function processFiltersuserlist() {
        if ($this->getRequest()->hasParameter('filter')) {
            $filters = $this->getRequestParameter('filters');
            $this->getUser()->getAttributeHolder()->removeNamespace('sf_admin/userlist/filters');
            $this->getUser()->getAttributeHolder()->add($filters, 'sf_admin/userlist/filters');
        }
    }

    protected function addFiltersCriteriauserlist($c) {
        $users = $this->getUser()->getNamaUser();
        if ((isset($this->filters['userid']) && $this->filters['userid'] != '') || (isset($this->filters['username']) && $this->filters['username'] != '') || (isset($this->filters['level']) && $this->filters['level'] != '')) {

            if (isset($this->filters['userid']) && $this->filters['userid'] != '') {
                $kata = '%' . trim($this->filters['userid']) . '%';
                // $c->addAnd(MasterUserV2Peer::USER_ID, $kata, Criteria::ILIKE);

                if ((strpos($users, 'admin') !== FALSE )) {
                    $c->add(MasterUserV2Peer::USER_ID, '%superadmin%', Criteria::NOT_ILIKE);
                    $c->addAnd(MasterUserV2Peer::USER_ID, $kata, Criteria::ILIKE);
                }elseif ((strpos($users, 'survey') !== FALSE )) {
                    $c->add(MasterUserV2Peer::USER_ID, '%survey%', Criteria::ILIKE);
                    $c->addAnd(MasterUserV2Peer::USER_ID, $kata, Criteria::ILIKE);
                } 
                else {

                    if ($kata == '%admin%') {
                        $c->add(MasterUserV2Peer::USER_ID, $kata, Criteria::ILIKE);
                        $c->addAnd(MasterUserV2Peer::USER_ID, $users, Criteria::ILIKE);
                    } else
                        $c->add(MasterUserV2Peer::USER_ID, $kata, Criteria::ILIKE);
                }

                if (isset($this->filters['level']) && $this->filters['level'] != '') {
                    $level_id = $this->filters['level'];
                    $c->addJoin(MasterUserV2Peer::USER_ID, SchemaAksesV2Peer::USER_ID);
                    $c->addAnd(SchemaAksesV2Peer::LEVEL_ID, $level_id);
                    $c->addAnd(SchemaAksesV2Peer::SCHEMA_ID, 2);
                }
            } else if (isset($this->filters['level']) && $this->filters['level'] != '') {
                $level_id = $this->filters['level'];
                $c->addJoin(MasterUserV2Peer::USER_ID, SchemaAksesV2Peer::USER_ID);
                $c->addAnd(SchemaAksesV2Peer::LEVEL_ID, $level_id);
                $c->addAnd(SchemaAksesV2Peer::SCHEMA_ID, 2);
                if (isset($this->filters['userid']) && $this->filters['userid'] != '') {
                    $kata = '%' . trim($this->filters['userid']) . '%';
                    $c->addAnd(MasterUserV2Peer::USER_ID, $kata, Criteria::ILIKE);
                }
            }
            if (isset($this->filters['username']) && $this->filters['username'] != '') {
                $username = '%' . str_replace(" ", "%", trim($this->filters['username'])) . '%';
                $c->add(MasterUserV2Peer::USER_NAME, $username, Criteria::ILIKE);
            }
        }
    }

}
