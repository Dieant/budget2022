<div class="card-body table-responsive p-0">
    <table class="table table-hover">
        <thead class="head_peach">
            <tr>
                <th>Kode Barang</th> 
                <th>Nama Barang</th>
                <th>Spec</th>
                <th>Satuan</th>
                <th>Harga Lama</th>
                <th>Harga 1</th>
                <th>Harga 2</th>
                <th>Harga 3</th>
                <th>Harga Pakai</th>
                <th>Pajak</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php

            $i = 1;
            foreach ($pager->getResults() as $sshlocked): $odd = fmod(++$i, 2)
                ?>
            <tr class="sf_admin_row_<?php echo $odd ?>">
                <td>
                    <?php 
                        echo $sshlocked->getShsdId().' ';
                        if($sshlocked->getStatusVerifikasi() == TRUE)
                            echo "<span class='badge badge-success'>Verified</span>";
                    ?>
                </td>
                <td><?php echo $sshlocked->getNamaDasar(); ?></td>
                <td><?php echo $sshlocked->getSpec() ?></td>
                <td style="text-align: center"><?php echo $sshlocked->getSatuan() ?></td>
                <td style="text-align: right">
                    <?php
                        if ($sshlocked->getShsdHarga() != floor($sshlocked->getShsdHarga())) {
                            echo number_format($sshlocked->getShsdHarga(), 2, ',', '.');
                        } elseif ($sshlocked->getSatuan() != '%') {
                            echo number_format($sshlocked->getShsdHarga());
                        } else {
                            echo $sshlocked->getShsdHarga();
                        }
                        ?>
                </td>
                <td style="text-align: right">
                    <?php
                        if ($sshlocked->getHarga1() != floor($sshlocked->getHarga1())) {
                            echo number_format($sshlocked->getHarga1(), 2, ',', '.');
                        } elseif ($sshlocked->getSatuan() != '%') {
                            echo number_format($sshlocked->getHarga1());
                        } else {
                            echo $sshlocked->getHarga1();
                        }
                        ?>
                </td>
                <td style="text-align: right">
                    <?php
                        if ($sshlocked->getHarga2() != floor($sshlocked->getHarga2())) {
                            echo number_format($sshlocked->getHarga2(), 2, ',', '.');
                        } elseif ($sshlocked->getSatuan() != '%') {
                            echo number_format($sshlocked->getHarga2());
                        } else {
                            echo $sshlocked->getHarga2();
                        }
                        ?>
                </td>
                <td style="text-align: right">
                    <?php
                        if ($sshlocked->getHarga3() != floor($sshlocked->getHarga3())) {
                            echo number_format($sshlocked->getHarga3(), 2, ',', '.');
                        } elseif ($sshlocked->getSatuan() != '%') {
                            echo number_format($sshlocked->getHarga3());
                        } else {
                            echo $sshlocked->getHarga3();
                        }
                        ?>
                </td>
                <td style="text-align: right">
                    <?php 
                        if ($sshlocked->getShsdHargaPakai() != floor($sshlocked->getShsdHargaPakai())) { 
                            echo number_format($sshlocked->getShsdHargaPakai(), 2, ',', '.'); 
                        } elseif ($sshlocked->getSatuan() != '%') { 
                            echo number_format($sshlocked->getShsdHargaPakai()); 
                        } else { 
                            echo $sshlocked->getShsdHargaPakai(); 
                        } 
                        ?>
                </td>
                <td style="text-align: center">
                    <?php
                        if ($sshlocked->getNonPajak() == 'false') {
                            echo '0 %';
                        } else {
                            echo '10 %';
                        }
                        ?>
                </td>
                <td>
                    <?php 
                    echo link_to('<i class="fa fa-edit"></i>', 'survey/editKomponen?id=' . $sshlocked->getShsdId() . '&page=' . $pager->getPage(), array('class' => 'btn btn-outline-primary btn-sm')); 
                    ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
<div class="card-footer clearfix">
    <ul class="pagination pagination-sm m-0 float-left">
        <?php
            echo format_number_choice('[0] no result|[1] 1 result|(1,+Inf] %1% results', array('%1%' => $pager->getNbResults()), $pager->getNbResults());
        ?>
    </ul>
    <ul class="pagination pagination-sm m-0 float-right">
        <?php 
        if ($pager->haveToPaginate()): 
            echo '<li class="page-item">'.link_to('Previous', 'survey/sshlocked?page=' . $pager->getPreviousPage(), array('class' => 'page-link', 'align' => 'absmiddle', 'alt' => __('Previous'), 'title' => __('Previous'))).'</li>';

            foreach ($pager->getLinks() as $page):
                $activeclass = ($page == $pager->getPage()) ? 'active' : '';
                echo '<li class="page-item '.$activeclass.'">'.link_to_unless($page == $pager->getPage(), $page, "survey/sshlocked?page={$page}", array('class' => 'page-link')).'</li>';
            endforeach;

            echo '<li class="page-item">'.link_to('Next', 'survey/sshlocked?page=' . $pager->getNextPage(), array('class' => 'page-link', 'align' => 'absmiddle', 'alt' => __('Next'), 'title' => __('Next'))).'</li>'; 
            endif;
        ?>
    </ul>
</div>