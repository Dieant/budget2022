<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Edit Komponen</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Survey SHS</a></li>
                    <li class="breadcrumb-item active">Edit Komponen</li>
                </ol>
            </div>
        </div>
    </div>
</section>
<!-- Main content -->
<section class="content">
    <?php include_partial('survey/messages'); ?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="fab fa-foursquare"></i> Form Edit Survey SSH
                        </h3>
                    </div>
                    <div class="card-body">
                        <?php echo form_tag('survey/saveEditKonfirmasi', array('multipart' => true, 'class' => 'form-horizontal', 'id'=>'form-edit-konfirmasi')); ?>
                        <div class="row">
                        <div class="col-sm-6"> 
                            <div class="form-group">
                            <label>SHSD ID</label>
                                <?php echo input_tag('shsd_id', $shsd->getShsdId(), array('class' => 'form-control','readonly' => 'true')) ?>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                            <label>Harga 2020</label>
                                <?php echo input_tag('shsd_harga', number_format($shsd->getShsdHarga(), 5, ',', '.'), array('class' => 'form-control numb', 'readonly' => 'true')) ?>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                            <label>Nama Komponen</label>
                                <?php echo input_tag('shsd_name', $shsd->getShsdName(), array('class' => 'form-control', 'readonly' => 'true')) ?>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                            <label>Nama Komponen Baru</label>
                                <?php echo input_tag('shsd_name_baru', $shsd->getNamaDasarBaru(), array('class' => 'form-control')) ?>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                            <label>Spesifikasi</label>
                                <?php echo input_tag('spec', $shsd->getSpec(), array('class' => 'form-control', 'readonly' => 'true')) ?>
                            </div>
                        </div>
                        <div class="col-sm-6"> 
                            <div class="form-group">
                                <label>Spesifikasi Baru</label>
                                <?php echo input_tag('spec_baru', $shsd->getSpecBaru(), array('class' => 'form-control')) ?>
                            </div>
                        </div> 
                        <div class="col-sm-6">
                            <div class="form-group">
                            <label>Satuan</label>
                                <?php echo input_tag('satuan', $shsd->getSatuan(), array('class' => 'form-control', 'readonly' => 'true')) ?>
                            </div>
                        </div> 
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Satuan Baru</label>
                                <?php
                                    $c_all_satuan = new Criteria();
                                    $c_all_satuan->addAscendingOrderByColumn(SatuanPeer::SATUAN_NAME);
                                    $rs_all_satuan = SatuanPeer::doSelect($c_all_satuan);
                                ?>
                                <select name="satuan_baru" class="form-control select2">
                                    <option value="<?php echo $shsd->getSatuan() ?>" selected>
                                        <?php echo $shsd->getSatuan() ?>
                                    </option>
                                    <?php foreach ($rs_all_satuan as $value) { ?>
                                        <option value="<?php echo $value->getSatuanName() ?>">
                                            <?php echo $value->getSatuanName() ?>
                                        </option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                            <label>Keterangan / Hidden Spec</label>
                                <?php echo input_tag('hidden_spec', $shsd->getHiddenSpec(), array('class' => 'form-control', 'readonly' => 'true')) ?>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Surveyor </label>
                                <?php echo input_tag('surveyor', $shsd->getSurveyor(), array('class' => 'form-control', 'required' => 'true')) ?>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group" style="display:none">
                            <label><span style="color:red;">*</span>Tanggal Survey</label>
                               <!--  <?php echo input_date_tag('tanggal', $shsd->getTanggalSurvey() ? $shsd->getTanggalSurvey() : null, array('rich' => true, 'required' => 'true', 'class' => 'form-control')); ?> -->
                            </div>
                        </div>
                        <hr>
                        <div class="col-sm-6">
                            <div class="form-group">
                            <label><span style="color:red;">*</span>Penyedia / Toko 1</label>
                                <?php echo input_tag('toko1', $shsd->getToko1(), array('class' => 'form-control', 'required' => 'true')) ?>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                            <label><span style="color:red;">*</span>Merk 1</label>
                                <?php echo input_tag('merk1', $shsd->getMerk1(), array('class' => 'form-control', 'required' => 'true')) ?>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                            <label><span style="color:red;">*</span>Keterangan 1</label>
                                <?php echo input_tag('keterangan1', $shsd->getKeterangan1(), array('class' => 'form-control', 'required' => 'true')) ?>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                            <label><span style="color:red;">*</span>Harga 1</label>
                                <?php echo input_tag('harga1', $shsd->getHarga1() == 0  ? null : $shsd->getHarga1(), array('class' => 'form-control numb', 'type' => 'text', 'step' => 'any', 'required' => 'true')) ?>
                            </div>
                        </div>
                        <div class="col-sm-6">
                                <div class="form-group">
                                    <label><span style="color:red;">*</span>Tanggal Survey Dukungan 1</label>
                                    <?php echo input_date_tag('tgl_konfirmasi1', $shsd->getTglKonfirm1() ? $shsd->getTglKonfirm1() : null, array('rich' => true, 'class' => 'form-control')); ?>
                                </div>
                        </div>
                        <div class="col-sm-6">
                                <div class="form-group">
                                    
                                </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                            <label><span style="color:red;">*</span>Data Pendukung 1.1 (Gunakan file jpg, png, atau pdf)</label>
                                <?php
                                echo input_file_tag('file1', array('class' => 'form-control'));
                                echo input_hidden_tag('file1_lama', $shsd->getFile1());
                                if ($shsd->getFile1() != '') {
                                    echo link_to('Download File 1.1', sfConfig::get('app_path_default_sf') . 'uploads/survey/' . $shsd->getFile1(), array('class' => 'btn btn-link', 'target' => '_blank'));
                                }
                                ?>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                            <label><span style="color:red;">*</span>Data Pendukung 1.2 (Gunakan file jpg, png, atau pdf)</label>
                                <?php
                                if ($shsd->getFile12() != '' || $shsd->getFile1() != '') {
                                    echo input_file_tag('file1_2', array('class' => 'form-control'));
                                } else {
                                    echo input_file_tag('file1_2', array('disabled' => 'true', 'class' => 'form-control'));
                                }
                                echo input_hidden_tag('file1_2_lama', $shsd->getFile12());
                                if ($shsd->getFile12() != '') {
                                    echo link_to('Download File 1.2', sfConfig::get('app_path_default_sf') . 'uploads/survey/' . $shsd->getFile12(), array('class' => 'btn btn-link', 'target' => '_blank'));
                                }
                                ?>
                            </div>
                        </div>                        
                        <hr>
                        <div class="col-sm-6">
                            <div class="form-group">
                            <label>Penyedia / Toko 2</label>
                                <?php echo input_tag('toko2', $shsd->getToko2(), array('class' => 'form-control')) ?>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                            <label>Merk 2</label>
                                <?php echo input_tag('merk2', $shsd->getMerk2(), array('class' => 'form-control')) ?>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                            <label>Keterangan 2</label>
                                <?php echo input_tag('keterangan2', $shsd->getKeterangan2(), array('class' => 'form-control')) ?>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                            <label>Harga 2</label>
                                <?php echo input_tag('harga2', $shsd->getHarga2() == 0  ? null : $shsd->getHarga2(), array('class' => 'form-control numb', 'type' => 'text', 'step' => 'any')) ?>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                    <label><span style="color:red;">*</span>Tanggal Survey Dukungan 2</label>
                                    <?php echo input_date_tag('tgl_konfirmasi2',  $shsd->getTglKonfirm2() ? $shsd->getTglKonfirm2() : null, array('rich' => true, 'class' => 'form-control')); ?>
                            </div>
                        </div>
                        <div class="col-sm-6">
                                <div class="form-group">
                                    
                                </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                            <label>Data Pendukung 2.1 (Gunakan file jpg, png, atau pdf)</label>
                                <?php
                                echo input_file_tag('file2', array('class' => 'form-control'));
                                echo input_hidden_tag('file2_lama', $shsd->getFile2());
                                if ($shsd->getFile2() != '') {
                                    echo link_to('Download File 2.1', sfConfig::get('app_path_default_sf') . 'uploads/survey/' . $shsd->getFile2(), array('class' => 'btn btn-link', 'target' => '_blank'));
                                }
                                ?>
                            </div>
                        </div>
                        <div class="col-sm-6"> 
                            <div class="form-group">
                            <label>Data Pendukung 2.2 (Gunakan file jpg, png, atau pdf)</label>
                                <?php
                                if ($shsd->getFile22() != '' || $shsd->getFile2() != '') {
                                    echo input_file_tag('file2_2', array('class' => 'form-control'));
                                } else {
                                    echo input_file_tag('file2_2', array('disabled' => 'true', 'class' => 'form-control'));
                                }
                                echo input_hidden_tag('file2_2_lama', $shsd->getFile22());
                                if ($shsd->getFile22() != '') {
                                    echo link_to('Download File 2.2', sfConfig::get('app_path_default_sf') . 'uploads/survey/' . $shsd->getFile22(), array('class' => 'btn btn-link', 'target' => '_blank'));
                                }
                                ?>
                            </div>
                        </div>
                        <hr>
                        <div class="col-sm-6">
                            <div class="form-group">
                            <label>Penyedia / Toko 3</label>
                                <?php echo input_tag('toko3', $shsd->getToko3(), array('class' => 'form-control')) ?>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                            <label>Merk 3</label>
                                <?php echo input_tag('merk3', $shsd->getMerk3(), array('class' => 'form-control')) ?>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                            <label>Keterangan 3</label>
                                <?php echo input_tag('keterangan3', $shsd->getKeterangan3(), array('class' => 'form-control')) ?>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                            <label>Harga 3</label>
                                <?php echo input_tag('harga3', $shsd->getHarga3() == 0  ? null : $shsd->getHarga3(), array('class' => 'form-control numb', 'type' => 'text', 'step' => 'any')) ?>
                            </div>
                        </div>
                        <div class="col-sm-6">
                                <div class="form-group">
                                    <label><span style="color:red;">*</span>Tanggal Survey Dukungan 3</label>
                                    <?php echo input_date_tag('tgl_konfirmasi3', $shsd->getTglKonfirm3() ? $shsd->getTglKonfirm3() : null, array('rich' => true, 'class' => 'form-control')); ?>
                                </div>
                        </div>
                        <div class="col-sm-6">
                                <div class="form-group">
                                    
                                </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                            <label>Data Pendukung 3.1 (Gunakan file jpg, png, atau pdf)</label>
                                <?php
                                echo input_file_tag('file3',array('class' => 'form-control'));
                                echo input_hidden_tag('file3_lama', $shsd->getFile3());
                                if ($shsd->getFile3() != '') {
                                    echo link_to('Download File 3.1', sfConfig::get('app_path_default_sf') . 'uploads/survey/' . $shsd->getFile3(), array('class' => 'btn btn-link', 'target' => '_blank'));
                                }
                                ?>
                            </div>
                        </div>                         
                        <div class="col-sm-6">
                            <div class="form-group">
                            <label>Data Pendukung 3.2 (Gunakan file jpg, png, atau pdf)</label>
                                <?php
                                if ($shsd->getFile32() != '' || $shsd->getFile3() != '') {
                                    echo input_file_tag('file3_2', array('class' => 'form-control'));
                                } else {
                                    echo input_file_tag('file3_2', array('disabled' => 'true', 'class' => 'form-control'));
                                }
                                echo input_hidden_tag('file3_2_lama', $shsd->getFile32());
                                if ($shsd->getFile32() != '') {
                                    echo link_to('Download File 3.2', sfConfig::get('app_path_default_sf') . 'uploads/survey/' . $shsd->getFile32(), array('class' => 'btn btn-link', 'target' => '_blank'));
                                }
                                ?>
                            </div>
                        </div>
                        
                        <div class="col-sm-12">
                            <div class="form-group">
                            <label>Catatan Surveyor</label>
                                <?php echo input_tag('catatan_survey', $shsd->getShsdCatatan(), array('class' => 'form-control', 'required' => 'true')) ?>
                            </div>
                        </div> 
                        <?php
                        echo input_hidden_tag('page', $halaman);
                        if($komponen != NULL && $komponen->getKomponenTipe2() != NULL &&  $komponen->getKomponenTipe2() == "KONSTRUKSI") {  ?>
                        <div class="col-sm-12">
                            <div class="form-group">
                            <label><span style="color:red;">*</span>Harga Lelang</label>
                                <?php echo input_tag('shsd_harga_lelang', $shsd->getShsdHargaLelang(), array('class' => 'form-control', 'type' => 'number', 'step' => 'any')) ?>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <label><span style="color:red;">*</span>Pendukung Lelang (Gunakan file jpg, png, atau pdf)</label>
                            <div class="form-group">
                                <?php
                                echo input_file_tag('file_lelang', array('class' => 'form-control'));
                                echo input_hidden_tag('file_lelang_lama', $shsd->getFileLelang());
                                if ($shsd->getFileLelang() != '') {
                                    echo link_to('Download File Lelang', sfConfig::get('app_path_default_sf') . 'uploads/survey/' . $shsd->getFileLelang(), array('class' => 'btn btn-link', 'target' => '_blank'));
                                }
                                ?>
                            </div>
                        </div>
                        <?php
                        }
                        ?>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="tombol_filter">Tombol Filter</label><br />
                                <!-- <button type="submit" name="submit" class="btn btn-outline-primary btn-sm">Simpan <i class="far fa-save"></i></button> -->
                                <button type="button" class="btn btn-outline-primary btn-sm" onclick="simpanEditKonfirmasi();">Simpan <i class="far fa-save"></i></button>
                            </div>
                            <!-- /.form-group -->
                        </div>
                        <!-- /.col --> 
                        </div>
                        <?php echo '</form>' ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    var jmlPendukung = 0;
    <?php if($shsd->getFile1() != '') { ?>
        jmlPendukung++;
    <?php } ?>
    <?php if($shsd->getFile2() != '') { ?>
        jmlPendukung++;
    <?php } ?>
    <?php if($shsd->getFile3() != '') { ?>
        jmlPendukung++;
    <?php } ?>

    $('#file1').change(function() {
        cekJmlPendukung(this.id);
    });
    $('#file2').change(function() {
        cekJmlPendukung(this.id);
    });
    $('#file3').change(function() {
        cekJmlPendukung(this.id);
    });

    function cekJmlPendukung(id) {
        jmlPendukung++;

        if(jmlPendukung >= 2) {
            $('#shsd_harga_pakai').removeAttr('disabled');
        }

        $('#' + id + '_2').removeAttr('disabled');
    }

    $('form').on('focus', 'input[type=number]', function (e) {
        $(this).on('mousewheel.disableScroll', function (e) {
            e.preventDefault()
        })
    });
    $('form').on('blur', 'input[type=number]', function (e) {
        $(this).off('mousewheel.disableScroll')
    });

    var rupiah = document.getElementById('harga1');
        rupiah.addEventListener('keyup', function(e){
            // tambahkan 'Rp.' pada saat form di ketik
            // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
            rupiah.value = formatRupiah(this.value, 'Rp. ');
        });

    var rupiah2 = document.getElementById('harga2');
        rupiah2.addEventListener('keyup', function(e){
            // tambahkan 'Rp.' pada saat form di ketik
            // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
            rupiah2.value = formatRupiah(this.value, 'Rp. ');
        });

    var rupiah3 = document.getElementById('harga3');
        rupiah3.addEventListener('keyup', function(e){
            // tambahkan 'Rp.' pada saat form di ketik
            // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
            rupiah3.value = formatRupiah(this.value, 'Rp. ');
    });
 
        /* Fungsi formatRupiah */
        function formatRupiah(angka, prefix){
            var number_string = angka.replace(/[^,\d]/g, '').toString(),
            split           = number_string.split(','),
            sisa            = split[0].length % 3,
            rupiah          = split[0].substr(0, sisa),
            ribuan          = split[0].substr(sisa).match(/\d{3}/gi);
 
            // tambahkan titik jika yang di input sudah menjadi angka ribuan
            if(ribuan){
                separator = sisa ? '.' : '';
                rupiah += separator + ribuan.join('.');
            }
 
            rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
            return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
    }

    function simpanEditKonfirmasi(){
        var form = $('#form-edit-konfirmasi');
        var harga1 = $('#harga1').val();
        var harga1_arr = $('#harga1').val().split(' ');
        var tgl1 = $('#tgl_konfirmasi1').val();
        var harga2 = $('#harga2').val();
        var harga2_arr = $('#harga2').val().split(' ');
        var tgl2 = $('#tgl_konfirmasi2').val();
        var harga3 = $('#harga3').val();
        var harga3_arr = $('#harga3').val().split(' ');
        var tgl3 = $('#tgl_konfirmasi3').val();
        var catatan_survey = $("#catatan_survey").val();

        // console.log('1 : '+harga1+tgl1+', 2 : '+harga2+tgl2+', 3 : '+harga3+tgl3); return false;

        if(harga1 == '0'){
            toastr.error('Isi Harga 1 dahulu', 'Alert!');
            return false;
        }

        // untuk pendukung 1
        if(harga1 != ''){
            if(harga1_arr[1] != 0 && tgl1 == ''){
                console.log('Tgl1 kosong');
                toastr.error('Tanggal Dukungan 1 survey mohon untuk diisi', 'Alert!');
                return false;
            }
        }
        if(tgl1 != ''){
            if(harga1 == '' || harga1 == '0' || harga1_arr[1] == 0){
                toastr.error('harga 2 mohon untuk diisi', 'Alert!');
                return false;
            }
        }
        // cek file pendukung 1
        if($('#file1').val() == '' && $('#file1_lama').val() == ''){
            toastr.error('Data pendukung yang diupload tidak boleh kurang dari 1', 'Alert!');
            return false;
        }
        if($('#file1').val() != ''){
            var fileExtension = ['zip', 'rar', 'jpg', 'pdf', 'png', 'xls', 'xlsx', 'jpeg'];
            if ($.inArray($('#file1').val().split('.').pop().toLowerCase(), fileExtension) == -1) {
                alert("Gunakan file dengan format zip/rar/jpg/png/pdf");
            }
        }

        // untuk pendukung 2
        if(harga2 != ''){
            if(harga2_arr[1] != 0 && tgl2 == ''){
                console.log('Tgl2 kosong');
                toastr.error('Tanggal Dukungan 2 survey mohon untuk diisi', 'Alert!');
                return false;
            }
        }
        if(tgl2 != ''){
            if(harga2 == '' || harga2 == '0' || harga2_arr[1] == 0){
                toastr.error('harga 2 mohon untuk diisi', 'Alert!');
                return false;
            }
        }
        if($('#file2').val() != ''){
            var fileExtension = ['zip', 'rar', 'jpg', 'pdf', 'png', 'xls', 'xlsx', 'jpeg'];
            if ($.inArray($('#file2').val().split('.').pop().toLowerCase(), fileExtension) == -1) {
                alert("Gunakan file dengan format zip/rar/jpg/png/pdf");
            }
        }
        
        // untuk pendukung 3
        if(harga3 != ''){
            if(harga3_arr[1] != 0 && tgl3 == ''){
                console.log('Tgl3 kosong');
                toastr.error('Tanggal Dukungan 3 survey mohon untuk diisi', 'Alert!');
                return false;
            }
        }
        if(tgl3 != ''){
            if(harga3 == '' || harga3 == '0' || harga3_arr[1] == 0){
                toastr.error('harga 3 mohon untuk diisi', 'Alert!');
                return false;
            }
        }
        if($('#file3').val() != ''){
            var fileExtension = ['zip', 'rar', 'jpg', 'pdf', 'png', 'xls', 'xlsx', 'jpeg'];
            if ($.inArray($('#file3').val().split('.').pop().toLowerCase(), fileExtension) == -1) {
                alert("Gunakan file dengan format zip/rar/jpg/png/pdf");
            }
        }


        // if((harga1[1] != '0' || harga1 != undefined) && tgl1==''){
        //     console.log('Tgl1 kosong');
        //     toastr.error('Tanggal Dukungan 1 survey mohon untuk diisi', 'Alert!');
        //     return false;
        // }
        // if(harga2[1] != '0' && tgl2==''){
        //     console.log('Tgl2 kosong');
        //     toastr.error('Tanggal Dukungan 2 survey mohon untuk diisi', 'Alert!');
        //     return false;
        // }
        // if(harga3[1] != '0' && tgl3==''){
        //     console.log('Tgl3 kosong');
        //     toastr.error('Tanggal Dukungan 3 survey mohon untuk diisi', 'Alert!');
        //     return false;
        // }
        
        if(catatan_survey == ''){
            toastr.error('Isi Catatan Survey dahulu', 'Alert!');
            return false;
        }else{
            console.log('Submit');
            form.submit();
        }

            

        // form.submit();

    }

</script>