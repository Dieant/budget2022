<?php use_helper('I18N', 'Date', 'Object', 'Javascript', 'Validation') ?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Data Survey SHS</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Survey SHS</a></li>
                    <li class="breadcrumb-item active">Data Survey SHS</li>
                </ol>
            </div>
        </div>
    </div>
</section>
<!-- Main content -->
<section class="content">
    <?php include_partial('survey/messages'); ?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body table-responsive p-0">
                        <table class="table table-hover">
                            <thead class="head_peach">
                                <tr>
                                    <th>ID</th>
                                    <th>Nama</th>
                                    <th>Spesifikasi</th>
                                    <th>Hidden Spec</th>
                                    <th>Merek</th>
                                    <th>Jumlah Dukungan</th>
                                    <th>Satuan</th>
                                    <th>Harga</th>
                                    <th>Harga 1</th>
                                    <th>Harga 2</th>
                                    <th>Harga 3</th>
                                    <th>Pajak</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $user=$sf_user->getNamaLengkap();

                                foreach ($rs_sshSurvey as $ssh): $odd = fmod(++$i, 2);   
                                    $survey = $ssh->getSurveyor();
                                    if($user == $survey)
                                    { 
                                    $jmlPendukung = 0;
                                    if($ssh->getFile1() != '')
                                        $jmlPendukung++;
                                    if($ssh->getFile2() != '')
                                        $jmlPendukung++;
                                    if($ssh->getFile3() != '')
                                        $jmlPendukung++;

                                    $con = Propel::getConnection();
                                    $query_tolak = "select * from ebudget.shsd_survey where shsd_harga <> 0 and shsd_catatan ILIKE '%Alasan Tolak%' and status_survey = false";
                                    $stmt_tolak= $con->prepareStatement($query_tolak);
                                    $tolak = $stmt_tolak->executeQuery();
                                    while ($tolak->next()) {
                                        $shsd_id_tolak = $tolak->getString('shsd_id');
                                    }
                                    ?>
                                    <tr>
                                        <td><?php echo $ssh->getShsdId() ?></td> 
                                        <td>
                                            <?php 

                                            if($ssh->getShsdId() == $shsd_id_tolak) {
                                                echo "<span style='color: red'>".$ssh->getShsdName();
                                                echo "<br/>Komponen ini di Kembalikan oleh Verifikator</span>";
                                            } else {
                                                echo $ssh->getShsdName();
                                            }
                                            ?>
                                        </td>                   
                                        <td><?php echo $ssh->getSpec() ?></td>
                                        <td><?php echo $ssh->getHiddenSpec() ?></td>
                                        <td style="text-align: center"><?php echo $ssh->getShsdMerk() ?></td>
                                        <td style="text-align: center"><?php echo $jmlPendukung ?></td>
                                        <td style="text-align: center"><?php echo $ssh->getSatuan() ?></td>
                                        <td style="text-align: right"><?php echo number_format($ssh->getShsdHarga()) ?></td>
                                        <td style="text-align: right"><?php echo number_format($ssh->getHarga1()) ?></td>
                                        <td style="text-align: right"><?php echo number_format($ssh->getHarga2()) ?></td>
                                        <td style="text-align: right"><?php echo number_format($ssh->getHarga3()) ?></td>
                                        <td style="text-align: center">
                                            <?php
                                            if ($ssh->getNonPajak() == FALSE) {
                                                echo '10%';
                                            } else {
                                                echo '0%';
                                            }
                                            ?>
                                        </td>
                                        <td style="text-align: center; font-weight: bold">
                                            <div class="btn-group">
                                                <?php
                                                echo link_to('<i class="fa fa-check"></i> Cek', 'survey/editKonfirmasi?id=' . $ssh->getShsdId() . '&key=' . md5('cek_konfirmasi_usulan'), array('class' => 'btn btn-outline-primary btn-sm'));
                                                echo ' ';
                                                if($jmlPendukung >= 2){
                                                    echo link_to('<i class="fa fa-check"></i> Verif', 'survey/finalSurvey?id=' . $ssh->getShsdId() . '&key=' . md5('cek_konfirmasi_usulan'), array('class' => 'btn btn-outline-success btn-sm')); 
                                                } 

                                                ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php 
                                        }
                                    endforeach;
                                    ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>