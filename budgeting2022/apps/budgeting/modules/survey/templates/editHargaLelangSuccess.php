<?php use_helper('I18N', 'Form', 'Object', 'Javascript', 'Validation') ?>
<section class="content">
            <!-- insert ajax elemen here -->
    <div class="box box-primary box-solid">
        <div class="box-header with-border label-danger">
            <h3 class="box-title">Perbandingan Harga Lelang <?php echo $shsd->getShsdName(); ?></h3>
        </div>
        <div class="box-body">
            <div id="sf_admin_container" class="table-responsive">
                <table cellspacing="0" class="sf_admin_list">
                    <tr id="lelang2017" style="width: 100%">
                        <td colspan="13"><?php echo link_to_function(image_tag('b_plus.png', array('name' => 'img_lelang2017', 'id' => 'img_lelang2017', 'border' => 0)), 'showHideKomponen(2017)') . ' '; ?><b>2017</b></td>
                    </tr>
                    <tr id="lelang2016" style="width: 100%">
                        <td colspan="13"><?php echo link_to_function(image_tag('b_plus.png', array('name' => 'img_lelang2016', 'id' => 'img_lelang2016', 'border' => 0)), 'showHideKomponen(2016)') . ' '; ?><b>2016</b></td>
                    </tr>
                    <tr id="lelang2015" style="width: 100%">
                        <td colspan="13"><?php echo link_to_function(image_tag('b_plus.png', array('name' => 'img_lelang2015', 'id' => 'img_lelang2015', 'border' => 0)), 'showHideKomponen(2015)') . ' '; ?><b>2015</b></td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="box-body"id="indicator" style="display:none;" align="center">
            <dt>&nbsp;</dt><dd><b>Mohon Tunggu </b><?php echo image_tag('loading.gif', array('align' => 'absmiddle')) ?></dd>
        </div>
    </div>

    <div class="box box-primary box-solid">
    
        <div class="box-header with-border">
            <h3 class="box-title">Keterangan Harga <?php echo $shsd->getShsdName(); ?></h3>
        </div>
        <div class="box-body">
            <div id="sf_admin_container">
                <div class="table-responsive">
                    <table cellspacing="0" class="sf_admin_list">    
                        <thead>
                            <tr>
                                <th>Kode Barang</th>
                                <th>Nama Barang</th>
                                <th>Satuan</th>
                                <th>Harga Lama</th>
                                <th>Harga Lelang</th>
                                <th>Harga Survey</th>
                                <!-- <th>Harga Rata - Rata</th> -->
                                <?php 
                                if($lelang['is_lelang']){
                                    if($lelang['komponen_tipe'] == 'KONSTRUKSI'){
                                ?>
                                <th>Pendukung Lelang</th>
                                <?php 
                                    } 
                                } 
                                ?>

                            </tr>
                        </thead>
                        <tbody>
                            <tr class="sf_admin_row_1">
                                <td style="text-align: center"><?php echo $shsd->getShsdId(); ?></td>
                                <td style="text-align: center"><?php echo $shsd->getShsdName(); ?></td>
                                <td style="text-align: center"><?php echo $shsd->getSatuan(); ?></td>
                                <td style="text-align: center"><?php echo number_format($shsd->getShsdHarga(), 2, ',', '.'); ?></td>
                                <td style="text-align: center">
                                <!-- harga lelang -->
                                <?php
                                if($lelang['is_lelang']){

                                    if($lelang['komponen_tipe'] != 'KONSTRUKSI'){
                                        // jika ada lebih dari 1 harga maka di rata2
                                        if($number > 1) $rata_lelang = $lelang['komponen_harga']/$number;
                                        else $rata_lelang = $lelang['komponen_harga'];
                                        echo number_format($rata_lelang, 2, ',', '.'); 
                                    }
                                    else{
                                        $k_lelang = $shsd->getShsdHargaLelang();
                                        echo number_format($k_lelang, 2, ',', '.'); 
                                    }
                                }
                                else{
                                    echo number_format(0, 2, ',', '.'); 
                                } 
                                ?>
                                </td>
                                <td style="text-align: center">
                                <!-- Harga Survey -->
                                <?php echo number_format($shsd->getShsdHargaPakai(), 2, ',', '.'); ?></td>
                                <!-- <td style="text-align: center"> -->
                                <!-- Harga Rata2 -->
                                <!-- <?php
                                if ($shsd->getShsdHargaPakai() == NULL) {
                                    $survey = 0;
                                }
                                else{
                                    $survey = $shsd->getShsdHargaPakai();
                                }
                                if($lelang['is_lelang']){
                                    if($lelang['komponen_tipe'] != 'KONSTRUKSI'){
                                        echo number_format(round(($survey+$rata_lelang)/2) , 2, ',', '.'); 
                                    }
                                    else{
                                        echo number_format(round(($survey+$k_lelang)/2) , 2, ',', '.'); 
                                    }
                                }
                                else{
                                    echo number_format(round(($survey)/2) , 2, ',', '.');
                                }
                                ?> -->
                                <!-- </td> -->
                                <?php 
                                if($lelang['is_lelang']){
                                    if($lelang['komponen_tipe'] == 'KONSTRUKSI'){
                                ?>
                                <td style="text-align: center">
                                    <div style="text-align: center" class="col-xs-12">
                                        <?php
                                        // echo input_file_tag('file_lelang');
                                        // echo input_hidden_tag('file_lelang_lama', $shsd->getFileLelang());
                                        if ($shsd->getFileLelang() != '') {
                                            echo link_to('Download File Lelang', sfConfig::get('app_path_default_sf') . 'uploads/survey/' . $shsd->getFileLelang(), array('class' => 'btn btn-link', 'target' => '_blank'));
                                        }
                                        ?>
                                    </div>
                                </td>

                                <?php 
                                    } 
                                } 
                                ?>
                                

                            </tr>
                        </tbody>    
                    </table>
                </div>
            </div> 
        </div> <!-- /. box-body -->
    </div> <!-- /. box-primary -->
</section>


<script>
    function showHideKomponen(thn) {
        var idrow = 'lelang'+thn;
        var idimg = 'img_lelang'+thn;
        var row = $('#'+idrow);
        var img = $('#'+idimg);

        if (img) {
            var src = document.getElementById(idimg).getAttribute('src');
            var minus = src.indexOf('/<?php echo sfConfig::get('app_default_coding'); ?>/images/b_minus.png');
            if (minus !== -1) {
                src = '/<?php echo sfConfig::get('app_default_coding'); ?>/images/b_plus.png';
            } else {
                src = '/<?php echo sfConfig::get('app_default_coding'); ?>/images/b_minus.png';
            }
            img.attr('src', src);
        }
        
        if (minus === -1) {

            // alert("buka tabel");
            var lelang_id = idrow;
            var mirip = $('.mirip');
            var n = mirip.length;

            if (n > 0) {
                // alert("jika buka tabel > 1");
                for (var i = 0; i < mirip.length; i++) {
                    var list_mirip = mirip[i];
                    list_mirip.style.display = 'table-row';
                }
                
                $('#indicator').show();
                $.ajax({
                    url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/survey/getHargaLelang/komponen/<?php echo $sf_params->get('name'); ?>/tahun/"+thn+".html",
                    context: document.body
                }).done(function (msg) {
                    row.after(msg);
                    $('#indicator').remove();
                });

            } else {
                // alert("ambil data untuk tabel");
                $('#indicator').show();
                $.ajax({
                    url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/survey/getHargaLelang/komponen/<?php echo $sf_params->get('name'); ?>/tahun/"+thn+".html",
                    context: document.body
                }).done(function (msg) {
                    row.after(msg);
                    $('#indicator').remove();
                });
            }
        } else {
            // alert("tutup tabel");
            $('.mirip#'+idimg).remove();
            minus = -1;
        }
    }
    $(document).ready(function () {
        $(".js-example-basic-multiple").select2();
    });
</script>