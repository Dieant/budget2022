<?php use_helper('I18N', 'Form', 'Object', 'Javascript', 'Validation') ?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Edit Komponen</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Survey SPTJM</a></li>
                    <li class="breadcrumb-item active">Edit Komponen</li>
                </ol>
            </div>
        </div>
    </div>
</section>
<!-- Main content -->
<section class="content">
    <?php include_partial('survey/messages'); ?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="fab fa-foursquare"></i> Form Edit Survey SPTJM
                        </h3>
                    </div>
                    <div class="card-body">
                        <?php echo form_tag('survey/saveEditKomponen', array('multipart' => true, 'class' => 'form-horizontal')); ?>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>SHSD ID</label> 
                                    <?php echo input_tag('shsd_id', $shsd->getShsdId(), array('class' => 'form-control','readonly' => 'true')) ?>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Nama</label> 
                                    <?php echo input_tag('shsd_name', $shsd->getShsdName(), array('class' => 'form-control', 'readonly' => 'true')) ?>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Satuan</label> 
                                    <?php echo input_tag('satuan', $shsd->getSatuan(), array('class' => 'form-control', 'readonly' => 'true')) ?>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Spec</label> 
                                    <?php echo input_tag('spec', $shsd->getSpec(), array('class' => 'form-control', 'readonly' => 'true')) ?>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Keterangan / Hidden Spec</label> 
                                    <?php echo input_tag('hidden_spec', $shsd->getHiddenSpec(), array('class' => 'form-control', 'readonly' => 'true')) ?>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Non Pajak</label><br/> 
                                    <input type="checkbox" name="non_pajak" value="TRUE" <?php echo ($shsd->getNonPajak() == TRUE ? 'checked' : ''); ?> disabled> <i>*centang jika termasuk komponen non pajak</i>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Surveyor </label>
                                    <?php
                                    $c = new Criteria();
                                    $c->addAscendingOrderByColumn(SurveyorPeer::NAMA);
                                    $v = SurveyorPeer::doSelect($c);
                                    echo select_tag('surveyor', objects_for_select($v, 'getNama', 'getNama', $shsd->getSurveyor(), array('include_custom' => '---Pilih Surveyor---')), array('class' => 'form-control select2', 'style' => 'width:100%'));
                                    ?>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label><span style="color:red;">*</span>Tanggal Survey</label>
                                    <?php echo input_date_tag('tanggal', $shsd->getTanggalSurvey() ? $shsd->getTanggalSurvey(): null, array('rich' => true, 'readonly' => 'true', 'required' => 'true', 'class' => 'form-control')); ?>
                                </div>
                            </div> 
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label><span style="color:red;">*</span>Penyedia / Toko 1</label> 
                                    <?php echo input_tag('toko1', $shsd->getToko1(), array('class' => 'form-control')) ?>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label><span style="color:red;">*</span>Merk  1</label> 
                                    <?php echo input_tag('merk1', $shsd->getMerk1(), array('class' => 'form-control')) ?>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label><span style="color:red;">*</span>Keterangan 1</label> 
                                    <?php echo input_tag('keterangan1', $shsd->getKeterangan1(), array('class' => 'form-control')) ?>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label><span style="color:red;">*</span>Harga 1</label> 
                                    <?php echo input_tag('harga1', $shsd->getHarga1(), array('class' => 'form-control numb', 'type' => 'text', 'step' => 'any')) ?>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label><span style="color:red;">*</span>Data Pendukung 1.1 (Gunakan file jpg, png, atau pdf)</label> 
                                    <?php
                                    echo input_file_tag('file1', array('class' => 'form-control'));
                                    echo input_hidden_tag('file1_lama', $shsd->getFile1());
                                    if ($shsd->getFile1() != '') {
                                        echo link_to('Download File 1.1', sfConfig::get('app_path_default_sf') . 'uploads/survey/' . $shsd->getFile1(), array('class' => 'btn btn-link', 'target' => '_blank'));
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                <label><span style="color:red;">*</span>Data Pendukung 1.2 (Gunakan file jpg, png, atau pdf)</label>
                                    <?php
                                    if ($shsd->getFile12() != '' || $shsd->getFile1() != '') {
                                        echo input_file_tag('file1_2', array('class' => 'form-control'));
                                    } else {
                                        echo input_file_tag('file1_2', array('disabled' => 'true', 'class' => 'form-control'));
                                    }
                                    echo input_hidden_tag('file1_2_lama', $shsd->getFile12());
                                    if ($shsd->getFile12() != '') {
                                        echo link_to('Download File 1.2', sfConfig::get('app_path_default_sf') . 'uploads/survey/' . $shsd->getFile12(), array('class' => 'btn btn-link', 'target' => '_blank'));
                                    }
                                    ?>
                                </div>
                            </div>
                            <hr>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Penyedia / Toko 2 </label> 
                                    <?php echo input_tag('toko2', $shsd->getToko2(), array('class' => 'form-control')) ?>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Merk 2 </label> 
                                    <?php echo input_tag('merk2', $shsd->getMerk2(), array('class' => 'form-control')) ?>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Keterangan 2 </label>
                                    <?php echo input_tag('keterangan2', $shsd->getKeterangan2(), array('class' => 'form-control')) ?>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Harga 2 </label> 
                                    <?php echo input_tag('harga2', $shsd->getHarga2(), array('class' => 'form-control numb', 'type' => 'text', 'step' => 'any')) ?>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                <label><span style="color:red;">*</span>Data Pendukung 2.1 (Gunakan file jpg, png, atau pdf)</label>
                                <?php
                                echo input_file_tag('file2', array('class' => 'form-control'));
                                echo input_hidden_tag('file2_lama', $shsd->getFile2());
                                if ($shsd->getFile2() != '') {
                                    echo link_to('Download File 2.1', sfConfig::get('app_path_default_sf') . 'uploads/survey/' . $shsd->getFile2(), array('class' => 'btn btn-link', 'target' => '_blank'));
                                }
                                ?>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                <label><span style="color:red;">*</span>Data Pendukung 2.2 (Gunakan file jpg, png, atau pdf)</label>
                                <?php
                                if ($shsd->getFile22() != '' || $shsd->getFile2() != '') {
                                    echo input_file_tag('file2_2', array('class' => 'form-control'));
                                } else {
                                    echo input_file_tag('file2_2', array('disabled' => 'true', 'class' => 'form-control'));
                                }
                                echo input_hidden_tag('file2_2_lama', $shsd->getFile22());
                                if ($shsd->getFile22() != '') {
                                    echo link_to('Download File 2.2', sfConfig::get('app_path_default_sf') . 'uploads/survey/' . $shsd->getFile22(), array('class' => 'btn btn-link', 'target' => '_blank'));
                                }
                                ?>
                                </div>
                            </div>
                            <hr>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Penyedia / Toko 3 </label>
                                    <?php echo input_tag('toko3', $shsd->getToko3(), array('class' => 'form-control')) ?>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Merk 3 </label> 
                                    <?php echo input_tag('merk3', $shsd->getMerk3(), array('class' => 'form-control')) ?>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Keterangan 3 </label> 
                                    <?php echo input_tag('keterangan3', $shsd->getKeterangan3(), array('class' => 'form-control')) ?>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Harga 3 </label> 
                                    <?php echo input_tag('harga3', $shsd->getHarga3(), array('class' => 'form-control numb', 'type' => 'text', 'step' => 'any')) ?>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label><span style="color:red;">*</span>Data Pendukung 3.1 (Gunakan file jpg, png, atau pdf)</label>
                                    <?php
                                    echo input_file_tag('file3', array('class' => 'form-control'));
                                    echo input_hidden_tag('file3_lama', $shsd->getFile3());
                                    if ($shsd->getFile3() != '') {
                                        echo link_to('Download File 3.1', sfConfig::get('app_path_default_sf') . 'uploads/survey/' . $shsd->getFile3(), array('class' => 'btn btn-link', 'target' => '_blank'));
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                <label><span style="color:red;">*</span>Data Pendukung 3.2 (Gunakan file jpg, png, atau pdf)</label> 
                                <?php
                                if ($shsd->getFile32() != '' || $shsd->getFile3() != '') {
                                    echo input_file_tag('file3_2', array('class' => 'form-control'));
                                } else {
                                    echo input_file_tag('file3_2', array('disabled' => 'true', 'class' => 'form-control'));
                                }
                                echo input_hidden_tag('file3_2_lama', $shsd->getFile32());
                                if ($shsd->getFile32() != '') {
                                    echo link_to('Download File 3.2', sfConfig::get('app_path_default_sf') . 'uploads/survey/' . $shsd->getFile32(), array('class' => 'btn btn-link', 'target' => '_blank'));
                                }
                                ?>
                                </div>
                            </div> 
                            <div class="col-sm-6">
                                <div class="form-group">
                                <label><span style="color:red;">*</span>Harga Terpakai</label>
                                <?php
                                    $jmlPendukung = 0;
                                    if($shsd->getFile1() != '')
                                        $jmlPendukung++;
                                    if($shsd->getFile2() != '')
                                        $jmlPendukung++;
                                    if($shsd->getFile3() != '')
                                        $jmlPendukung++;

                                    $hargapakaimin = min(array($shsd->getHarga1(), $shsd->getHarga2(), $shsd->getHarga3()));
                                    if($jmlPendukung > 2 && $shsd->getShsdHargaPakai() == 0 && $shsd->getStatusVerifikasi() == FALSE)
                                        echo input_tag('shsd_harga_pakai', $hargapakaimin, array('class' => 'form-control numb', 'type' => 'text', 'step' => 'any'));
                                    else
                                        echo input_tag('shsd_harga_pakai', $shsd->getShsdHargaPakai(), array('class' => 'form-control numb', 'type' => 'text', 'step' => 'any', 'disabled' => 'true'));
                                ?>
                                </div>
                            </div>
                            <?php
                            if($komponen != NULL && $komponen->getKomponenTipe2() != NULL &&  $komponen->getKomponenTipe2() == "KONSTRUKSI") 
                            {  
                            ?>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label><span style="color:red;">*</span>Harga  Lelang </label>
                                    <?php echo input_tag('shsd_harga_lelang', $shsd->getShsdHargaLelang(), array('class' => 'form-control', 'type' => 'number', 'step' => 'any')) ?>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label><span style="color:red;">*</span>Pendukung Lelang (Gunakan file jpg, png, atau pdf)</label> 
                                    <?php
                                        echo input_file_tag('file_lelang', array('class' => 'form-control'));
                                        echo input_hidden_tag('file_lelang_lama', $shsd->getFileLelang());
                                        if ($shsd->getFileLelang() != '') {
                                            echo link_to('Download File Lelang', sfConfig::get('app_path_default_sf') . 'uploads/survey/' . $shsd->getFileLelang(), array('class' => 'btn btn-link', 'target' => '_blank'));
                                        }
                                    ?>
                                </div>
                            </div>
                            <?php 
                            } 
                            ?>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Catatan Survey </label> 
                                    <?php echo input_tag('catatan_survey', $shsd->getShsdCatatan(), array('class' => 'form-control')) ?>
                                </div>
                            </div>
                            <?php echo input_hidden_tag('page', $halaman); ?>
                        </div>
                        <?php echo '</form>'; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    var jmlPendukung = 0;
    <?php if($shsd->getFile1() != '') { ?>
        jmlPendukung++;
    <?php } ?>
    <?php if($shsd->getFile2() != '') { ?>
        jmlPendukung++;
    <?php } ?>
    <?php if($shsd->getFile3() != '') { ?>
        jmlPendukung++;
    <?php } ?>

    $('#file1').change(function() {
        cekJmlPendukung(this.id);
    });
    $('#file2').change(function() {
        cekJmlPendukung(this.id);
    });
    $('#file3').change(function() {
        cekJmlPendukung(this.id);
    });

    function cekJmlPendukung(id) {
        jmlPendukung++;

        if(jmlPendukung >= 2) {
            $('#shsd_harga_pakai').removeAttr('disabled');
        }

        $('#' + id + '_2').removeAttr('disabled');
    }

    $('form').on('focus', 'input[type=number]', function (e) {
        $(this).on('mousewheel.disableScroll', function (e) {
            e.preventDefault()
        })
    });
    $('form').on('blur', 'input[type=number]', function (e) {
        $(this).off('mousewheel.disableScroll')
    });
</script>