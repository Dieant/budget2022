<div class="card-body table-responsive p-0">
    <table class="table table-hover">
        <thead class="head_peach">
            <tr>
                <th>ID</th>
                <th>Nama</th>
                <th>Spesifikasi</th>
                <th>Hidden Spec</th>
                <th>Merek</th>
                <th>Satuan</th>
                <th>Harga</th>
                <th>Harga 1</th>
                <th>Harga 2</th>
                <th>Harga 3</th>
                <th>Pajak</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>     
            <?php
                foreach ($pager->getResults() as $ssh): 
                    $jmlPendukung = 0;
                    if($ssh->getFile1() != '')
                        $jmlPendukung++;
                    if($ssh->getFile2() != '')
                        $jmlPendukung++;
                    if($ssh->getFile3() != '')
                        $jmlPendukung++;
                    $con = Propel::getConnection();
                    $query_tolak = "select * from ebudget.shsd_survey where shsd_harga <> 0 and shsd_catatan ILIKE '%Alasan Tolak%' and status_survey = false";
                    $stmt_tolak= $con->prepareStatement($query_tolak);
                    $tolak = $stmt_tolak->executeQuery();
                    while ($tolak->next()) {
                        $shsd_id_tolak = $tolak->getString('shsd_id');
                    }
                    if($sf_user->getNamaLengkap() == $ssh->getSurveyor())
                    {
                    ?>
                    <tr>
                        <td><?php echo $ssh->getShsdId() ?></td> 
                        <td>
                            <?php 
                            if($ssh->getIsSurveyBp() == 'true') {
                                echo "<span style='color: red'>".$ssh->getShsdName();
                                echo "<br/><b>Komponen ini di Kembalikan oleh Verifikator</b></span>";
                            } else {
                                echo $ssh->getShsdName();
                            }
                            ?>
                        </td>
                        <td><?php echo $ssh->getSpec() ?></td>
                        <td><?php echo $ssh->getHiddenSpec() ?></td>
                        <td style="text-align: center"><?php echo $ssh->getShsdMerk() ?></td> 
                        <td style="text-align: center"><?php echo $ssh->getSatuan() ?></td>
                        <td style="text-align: right"><?php echo number_format($ssh->getShsdHarga()) ?></td>
                        <td style="text-align: right"><?php echo number_format($ssh->getHarga1()) ?></td>
                        <td style="text-align: right"><?php echo number_format($ssh->getHarga2()) ?></td>
                        <td style="text-align: right"><?php echo number_format($ssh->getHarga3()) ?></td>
                        <td style="text-align: center">
                            <?php
                            if ($ssh->getNonPajak() == FALSE) {
                                echo '10%';
                            } else {
                                echo '0%';
                            }
                            ?>
                        </td>
                        <td style="text-align: center; font-weight: bold">
                            <div class="btn-group">
                                <?php
                                if($ssh->getStatusSurvey() == 'true' && $ssh->getStatusVerifikasi() == 'true')
                                {
                                    echo "<span class='badge badge-success'>Verified</span>";
                                }
                                elseif($ssh->getStatusSurvey() == 'true')
                                {
                                    echo "<span class='badge badge-success'>Verified Surveyor</span>";
                                }
                                else
                                {
                                    echo link_to('<i class="fa fa-check"></i> Cek', 'survey/editKonfirmasi?id=' . $ssh->getShsdId() . '&page=' . $pager->getPage(), array('class' => 'btn btn-outline-primary btn-sm'));
                                    echo ' ';
                                    if($jmlPendukung >= 2){
                                        echo link_to('<i class="fa fa-check"></i> Verif', 'survey/finalSurvey?id=' . $ssh->getShsdId() . '&page=' . $pager->getPage(), array('class' => 'btn btn-outline-success btn-sm')); 
                                    } 
                                }
                                ?>
                            </div>
                        </td>
                    </tr>
                    <?php
                    }
                    else
                    {
                    ?>
                    <tr>
                        <td><?php echo $ssh->getShsdId() ?></td> 
                        <td>
                            <?php
                            if($ssh->getIsSurveyBp() == 'true') {
                                echo "<span style='color: red'>".$ssh->getShsdName();
                                echo "<br/><b>Komponen ini di Kembalikan oleh Verifikator</b></span>";
                            } else {
                                echo $ssh->getShsdName();
                            }
                            ?>
                        </td>
                        <td><?php echo $ssh->getSpec() ?></td>
                        <td><?php echo $ssh->getHiddenSpec() ?></td>
                        <td style="text-align: center"><?php echo $ssh->getShsdMerk() ?></td> 
                        <td style="text-align: center"><?php echo $ssh->getSatuan() ?></td>
                        <td style="text-align: right"><?php echo number_format($ssh->getShsdHarga()) ?></td>
                        <td style="text-align: right"><?php echo number_format($ssh->getHarga1()) ?></td>
                        <td style="text-align: right"><?php echo number_format($ssh->getHarga2()) ?></td>
                        <td style="text-align: right"><?php echo number_format($ssh->getHarga3()) ?></td>
                        <td style="text-align: center">
                            <?php
                            if ($ssh->getNonPajak() == FALSE) {
                                echo '10%';
                            } else {
                                echo '0%';
                            }
                            ?>
                        </td>
                        <td style="text-align: center; font-weight: bold">
                            <div class="btn-group">
                                <?php
                                if($ssh->getStatusSurvey() == 'true') 
                                {
                                    echo link_to('<i class="fa fa-check"></i> Cek', 'survey/editKomponen?id=' . $ssh->getShsdId() . '&page=' . $pager->getPage(), array('class' => 'btn btn-outline-primary btn-sm'));
                                }
                                ?>
                            </div>
                        </td>
                    </tr>
                    <?php
                    }
                endforeach;
                ?>
        </tbody>
    </table>
</div>
<div class="card-footer clearfix">
    <ul class="pagination pagination-sm m-0 float-left">
        <?php
            echo format_number_choice('[0] no result|[1] 1 result|(1,+Inf] %1% results', array('%1%' => $pager->getNbResults()), $pager->getNbResults());
        ?>
    </ul>
    <ul class="pagination pagination-sm m-0 float-right">
        <?php 
        if ($pager->haveToPaginate()): 
            echo '<li class="page-item">'.link_to('Previous', 'survey/sshkonfirmasi?page=' . $pager->getPreviousPage(), array('class' => 'page-link', 'align' => 'absmiddle', 'alt' => __('Previous'), 'title' => __('Previous'))).'</li>';

            foreach ($pager->getLinks() as $page):
                $activeclass = ($page == $pager->getPage()) ? 'active' : '';
                echo '<li class="page-item '.$activeclass.'">'.link_to_unless($page == $pager->getPage(), $page, "survey/sshkonfirmasi?page={$page}", array('class' => 'page-link')).'</li>';
            endforeach;
            echo '<li class="page-item">'.link_to('Next', 'survey/sshkonfirmasi?page=' . $pager->getNextPage(), array('class' => 'page-link', 'align' => 'absmiddle', 'alt' => __('Next'), 'title' => __('Next'))).'</li>'; 
            endif;
        ?>
    </ul>
</div>