<div class="card-body table-responsive p-0">
  <table class="table table-hover">
    <thead class="head_peach">
            <tr>
                <th class="text-bold text-center">User ID</th>
                <th class="text-bold text-center">Username</th>
                <th class="text-bold text-center">Status</th>
                <th class="text-bold text-center">Action</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($pager->getResults() as $userlist): ?>
                <tr>
                    <td><?php echo $userlist->getUserId() ?></td>
                    <td><?php echo $userlist->getUserName() ?></td>
                    <?php if ($userlist->getUserEnable() == true) { ?>
                        <td class="text-center text-bold bg-green-active">
                            Aktif
                        </td>    
                    <?php } else { ?>
                        <td class="text-center text-bold bg-red-active">
                            Non-Aktif
                        </td>    
                    <?php }
                    ?>
                    </td>
                    <td style="text-align: left">
                        <div class="btn-group">
                            <?php
                            // echo link_to('<i class="fa fa-edit"></i> Edit', 'survey/editUser?id=' . $userlist->getUserId(), array('class' => 'btn btn-success btn-flat btn-xs'));
                            // if ($userlist->getUserEnable() == true) {
                            //     echo link_to('<i class="fa fa-lock"></i> Kunci', 'survey/kunciUser?id=' . $userlist->getUserId(), array('class' => 'btn btn-default btn-flat btn-xs'));
                            // } else {
                            //     echo link_to('<i class="fa fa-unlock"></i> Buka', 'survey/bukaUser?id=' . $userlist->getUserId(), array('class' => 'btn btn-default btn-flat btn-xs'));
                            // }
                            echo link_to('<i class="fas fa-user"></i> Login', 'login/loginmonitoring?send_user=' . $userlist->getUserId() . '&passwordmd5=' . trim($userlist->getUserPassword()) . '&kunci=' . md5('monitor'), array('class' => 'btn btn-success btn-flat btn-xs', 'title' => $userlist->getUserId()));
                            ?>                            
                        </div>
                    </td>
                </tr>
            <?php endforeach;
            ?>
        </tbody>
    </table>
</div>
<div class="card-footer clearfix">
    <ul class="pagination pagination-sm m-0 float-left">
        <?php
            echo format_number_choice('[0] no result|[1] 1 result|(1,+Inf] %1% results', array('%1%' => $pager->getNbResults()), $pager->getNbResults()) 
        ?>
    </ul>
    <ul class="pagination pagination-sm m-0 float-right">
        <?php 
        if ($pager->haveToPaginate()): 
            echo '<li class="page-item">'.link_to('Previous', 'survey/userlist?page=' . $pager->getPreviousPage(), array('class' => 'page-link', 'align' => 'absmiddle', 'alt' => __('Previous'), 'title' => __('Previous'))).'</li>';

            foreach ($pager->getLinks() as $page):
                $activeclass = ($page == $pager->getPage()) ? 'active' : '';
                echo '<li class="page-item '.$activeclass.'">'.link_to_unless($page == $pager->getPage(), $page, "survey/userlist?page={$page}", array('class' => 'page-link')).'</li>';
            endforeach;
            echo '<li class="page-item">'.link_to('Next', 'survey/userlist?page=' . $pager->getNextPage(), array('class' => 'page-link', 'align' => 'absmiddle', 'alt' => __('Next'), 'title' => __('Next'))).'</li>'; 
            endif;
        ?>
    </ul>
</div>
