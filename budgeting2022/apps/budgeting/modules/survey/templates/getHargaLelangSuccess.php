<?php use_helper('Url', 'Javascript', 'Form', 'Object'); ?>
<?php
$clean = NULL;
$i = 0;
$one = '';
$harga = 0;
$total = 1;
?>
<tr class="mirip" id="img_lelang<?php echo $tahun;?>" style="text-align: center; font-weight: bold">
    <th>Nama SKPD</th>
    <th>Nama Pekerjaan</th>
    <th>Kode Komponen</th>
    <th>Nama Komponen</th>
    <th>Harga Komponen</th>
    <!-- <th>Koefisien</th> -->
</tr>
<?php
foreach ($list as $value) {
    $i++;
    if($i == 1) $one = $value['komponen_code'];
    if($value['komponen_code'] != $one){
        // sudah beda komponen
        $total = $i;
        exit();
    }
    if($value['komponen_code'] == $one){
        $harga += $value['komponen_harga'];
    ?>
        <tr class="mirip" id="img_lelang<?php echo $tahun;?>" style="text-align: center">
            <td>
                <?php echo $value['unit_name']; ?>
            </td>
            <td>
                <?php echo $value['job_id'].' - '.$value['job_name']; ?>
            </td>
            <td>
            <?php echo $value['komponen_code'];  ?>
            </td>
            <?php 
            // bersihkan nama yang dari tabel jika cocok print
            $clean = $value['komponen_name']; 
            $clean = str_replace("()","",$clean);
            ?>
            <td><?php echo $clean; ?></td>

            <td><?php echo number_format($value['komponen_harga'], 2, ',', '.'); ?></td>
        </tr>
    <?php
    }
}
?>

<tr class="mirip" id="img_lelang<?php echo $tahun;?>" >
    <td colspan="13">&nbsp;</td>
</tr>

<tr class="mirip" id="img_lelang<?php echo $tahun;?>">
    <td style="text-align: center; font-weight: bold" colspan="13">
        Harga Rata - Rata : <?php echo number_format(round($harga/$i), 2, ',', '.'); ?>
    </td>
</tr>

<tr class="mirip" id="img_lelang<?php echo $tahun;?>">
    <td colspan="13">&nbsp;</td>
</tr>

