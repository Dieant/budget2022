<div class="card-body table-responsive p-0">
    <table class="table table-hover">
        <thead class="head_peach">
            <tr>
                <th>Kode Barang</th>
                <th>Kode Rekening</th>
                <th>Nama Barang</th>
                <th>Spec</th>
                <th>Satuan</th>
                <th>Harga 1</th>
                <th>Harga 2</th>
                <th>Harga 3</th>
                <th>Pajak</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $i = 1;
            foreach ($pager->getResults() as $sshusulan): $odd = fmod(++$i, 2)
                ?>
            <tr class="sf_admin_row_<?php echo $odd ?>">
                <td><?php echo $sshusulan->getShsdId(); ?></td>
                <td><?php echo $sshusulan->getRekeningCode(); ?></td>
                <td><?php echo $sshusulan->getShsdName(); ?></td>
                <td><?php echo $sshusulan->getSpec() ?></td>
                <td style="text-align: center"><?php echo $sshusulan->getSatuan() ?></td>
                <td style="text-align: right">
                    <?php
                        if ($sshusulan->getHarga1() != floor($sshusulan->getHarga1())) {
                            echo number_format($sshusulan->getHarga1(), 2, ',', '.');
                        } elseif ($sshusulan->getSatuan() != '%') {
                            echo number_format($sshusulan->getHarga1());
                        } else {
                            echo $sshusulan->getHarga1();
                        }
                        ?>
                </td>
                <td style="text-align: right">
                    <?php
                        if ($sshusulan->getHarga2() != floor($sshusulan->getHarga2())) {
                            echo number_format($sshusulan->getHarga2(), 2, ',', '.');
                        } elseif ($sshusulan->getSatuan() != '%') {
                            echo number_format($sshusulan->getHarga2());
                        } else {
                            echo $sshusulan->getHarga2();
                        }
                        ?>
                </td>
                <td style="text-align: right">
                    <?php
                        if ($sshusulan->getHarga3() != floor($sshusulan->getHarga3())) {
                            echo number_format($sshusulan->getHarga3(), 2, ',', '.');
                        } elseif ($sshusulan->getSatuan() != '%') {
                            echo number_format($sshusulan->getHarga3());
                        } else {
                            echo $sshusulan->getHarga3();
                        }
                        ?>
                </td>
                <td style="text-align: center">
                    <?php
                        if ($sshusulan->getNonPajak() == 'false') {
                            echo '0 %';
                        } else {
                            echo '10 %';
                        }
                        ?>
                </td>
                <td>
                    <?php 
                        if($sf_user->getNamaLengkap() == $sshusulan->getSurveyor() || $sf_user->getNamaLengkap() == 'Tim Survey') {
                            echo link_to('<i class="fa fa-edit"></i>', 'survey/editKomponenSpjm?id=' . $sshusulan->getShsdId() . '&page=' . $pager->getPage(), array('class' => 'btn btn-outline-primary btn-sm'));
                        }
                        ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
<div class="card-footer clearfix">
    <ul class="pagination pagination-sm m-0 float-left">
        <?php
            echo format_number_choice('[0] no result|[1] 1 result|(1,+Inf] %1% results', array('%1%' => $pager->getNbResults()), $pager->getNbResults());
        ?>
    </ul>
    <ul class="pagination pagination-sm m-0 float-right">
        <?php 
        if ($pager->haveToPaginate()): 
            echo '<li class="page-item">'.link_to('Previous', 'survey/sshusulan?page=' . $pager->getPreviousPage(), array('class' => 'page-link', 'align' => 'absmiddle', 'alt' => __('Previous'), 'title' => __('Previous'))).'</li>';

            foreach ($pager->getLinks() as $page):
                $activeclass = ($page == $pager->getPage()) ? 'active' : '';
                echo '<li class="page-item '.$activeclass.'">'.link_to_unless($page == $pager->getPage(), $page, "survey/sshusulan?page={$page}", array('class' => 'page-link')).'</li>';
            endforeach;
            echo '<li class="page-item">'.link_to('Next', 'survey/sshusulan?page=' . $pager->getNextPage(), array('class' => 'page-link', 'align' => 'absmiddle', 'alt' => __('Next'), 'title' => __('Next'))).'</li>'; 
            endif;
        ?>
    </ul>
</div>