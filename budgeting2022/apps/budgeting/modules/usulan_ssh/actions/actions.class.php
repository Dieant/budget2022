<?php

/**
 * usulan_ssh actions.
 *
 * @package    budgeting
 * @subpackage usulan_ssh
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 2692 2006-11-15 21:03:55Z fabien $
 */
class usulan_sshActions extends sfActions {

    public function executeGetKomponenTahunLalu() {
        if ($this->getRequestParameter('id')) {
            $this->id = $id_usulan = $this->getRequestParameter('id');
            $c = new Criteria();
            $c->add(UsulanSSHPeer::ID_USULAN, $id_usulan, Criteria::EQUAL);
            $rs_usulan = UsulanSSHPeer::doSelectOne($c);

            $nama = str_replace(' ', '%', trim($rs_usulan->getNama()));

            $con = Propel::getConnection();
            $query = "select komponen_name, komponen_harga, komponen_non_pajak
                    from " . sfConfig::get('app_default_schema') . ".komponen_tahun_lalu
                    where komponen_name ilike '%$nama%'";
            $stmt = $con->prepareStatement($query);
            $rs = $stmt->executeQuery();
            $this->list = $rs;
            $this->setLayout('kosong');
        }
    }

    public function executePrintBuku() {
        try {
            $array_sheet = array('1.1.7','1.3.2.01', '1.3.2.02', '1.3.2.03', '1.3.2.04', '1.3.2.05', '1.3.2.06', '1.3.2.07', '1.3.2.08', '1.3.2.10', '1.3.2.15', '1.3.2.18', '1.3.2.19', '1.3.5.01', '1.3.5.02', '1.3.5.03', '1.3.5.05', '1.5.3.01', '2.1.1.01', '2.1.1.02', '2.1.1.03', 'EST');
            $objPHPExcel = new sfPhpExcel();

            for ($sheet_ke = 0; $sheet_ke < count($array_sheet); $sheet_ke++) {

                if ($array_sheet[$sheet_ke] <> 'EST') {
                    $c_kategory_shsd = new Criteria();
                    $c_kategory_shsd->add(KategoriShsdPeer::KATEGORI_SHSD_ID, $array_sheet[$sheet_ke], Criteria::EQUAL);
                    $data_kategori = KategoriShsdPeer::doSelectOne($c_kategory_shsd);
                }

                if ($sheet_ke > 0) {
                    $objPHPExcel->createSheet($sheet_ke);
                }

                $objPHPExcel->setActiveSheetIndex($sheet_ke);
                if ($array_sheet[$sheet_ke] <> 'EST') {
                    $objPHPExcel->getActiveSheet()->setTitle($array_sheet[$sheet_ke] . ' ' . ucwords(strtolower($data_kategori->getKategoriShsdName())));
                } else {
                    $objPHPExcel->getActiveSheet()->setTitle($array_sheet[$sheet_ke] . ' ' . ucwords('Estimasi'));
                }

                $objPHPExcel->getActiveSheet()->setCellValue('C5', 'Kode');
                $objPHPExcel->getActiveSheet()->setCellValue('D5', 'Nama');
                $objPHPExcel->getActiveSheet()->setCellValue('E5', 'Merk');
                $objPHPExcel->getActiveSheet()->setCellValue('F5', 'Spesifikasi');
                $objPHPExcel->getActiveSheet()->setCellValue('G5', 'Hidden Spec');
                $objPHPExcel->getActiveSheet()->setCellValue('H5', 'Satuan');
                $objPHPExcel->getActiveSheet()->setCellValue('I5', 'Harga Satuan');
                $objPHPExcel->getActiveSheet()->setCellValue('J5', 'Usulan SKPD');
                $objPHPExcel->getActiveSheet()->setCellValue('K5', 'Kode Rekening');
                $objPHPExcel->getActiveSheet()->setCellValue('L5', 'Nama Rekening');
                $objPHPExcel->getActiveSheet()->setCellValue('M5', 'Pajak');
                $objPHPExcel->getActiveSheet()->getStyle('C5')->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->getStyle('D5')->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->getStyle('E5')->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->getStyle('F5')->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->getStyle('G5')->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->getStyle('H5')->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->getStyle('I5')->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->getStyle('J5')->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->getStyle('K5')->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->getStyle('L5')->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->getStyle('M5')->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->getStyle('C5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $objPHPExcel->getActiveSheet()->getStyle('D5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $objPHPExcel->getActiveSheet()->getStyle('E5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $objPHPExcel->getActiveSheet()->getStyle('F5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $objPHPExcel->getActiveSheet()->getStyle('G5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $objPHPExcel->getActiveSheet()->getStyle('H5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $objPHPExcel->getActiveSheet()->getStyle('I5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $objPHPExcel->getActiveSheet()->getStyle('J5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $objPHPExcel->getActiveSheet()->getStyle('K5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $objPHPExcel->getActiveSheet()->getStyle('L5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $objPHPExcel->getActiveSheet()->getStyle('M5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                if ($array_sheet[$sheet_ke] <> 'EST') {
                    $query = "SELECT shsd_id, nama_dasar, shsd_harga, "
                            . "shsd_merk, satuan, spec, hidden_spec, "
                            . "usulan_skpd, rekening_code, rekening_name, "
                            . "non_pajak FROM " . sfConfig::get('app_default_schema') . ".v_buku_ssh "
                            . "where shsd_id like '$array_sheet[$sheet_ke]%'";
                    $con = Propel::getConnection();
                    $stmt = $con->prepareStatement($query);
                    $rs = $stmt->executeQuery();
                    $kolom_ke = 6;
                    while ($rs->next()) {
                        if (trim($rs->getString('shsd_harga')) == '') {
                            $objPHPExcel->getActiveSheet()->setCellValueExplicit('C' . $kolom_ke, $rs->getString('shsd_id'), PHPExcel_Cell_DataType::TYPE_STRING);
                            $objPHPExcel->getActiveSheet()->getStyle('C' . $kolom_ke)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                            $objPHPExcel->getActiveSheet()->getStyle('C' . $kolom_ke)->getFont()->setBold(true);
                            $objPHPExcel->getActiveSheet()->setCellValue('D' . $kolom_ke, $rs->getString('nama_dasar'));
                            $objPHPExcel->getActiveSheet()->mergeCells('D' . $kolom_ke . ':I' . $kolom_ke);
                            $objPHPExcel->getActiveSheet()->getStyle('D' . $kolom_ke)->getFont()->setBold(true);
                        } else {
                            $objPHPExcel->getActiveSheet()->setCellValueExplicit('C' . $kolom_ke, $rs->getString('shsd_id'), PHPExcel_Cell_DataType::TYPE_STRING);
                            $objPHPExcel->getActiveSheet()->getStyle('C' . $kolom_ke)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                            $objPHPExcel->getActiveSheet()->setCellValue('D' . $kolom_ke, $rs->getString('nama_dasar'));
                            $objPHPExcel->getActiveSheet()->setCellValue('E' . $kolom_ke, $rs->getString('shsd_merk'));
                            $objPHPExcel->getActiveSheet()->setCellValue('F' . $kolom_ke, $rs->getString('spec'));
                            $objPHPExcel->getActiveSheet()->getStyle('F' . $kolom_ke)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                            $objPHPExcel->getActiveSheet()->setCellValue('G' . $kolom_ke, $rs->getString('hidden_spec'));
                            $objPHPExcel->getActiveSheet()->setCellValue('H' . $kolom_ke, $rs->getString('satuan'));
                            $objPHPExcel->getActiveSheet()->getStyle('H' . $kolom_ke)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                            $objPHPExcel->getActiveSheet()->setCellValue('I' . $kolom_ke, number_format($rs->getString('shsd_harga'), 0, '.', ','));
                            $objPHPExcel->getActiveSheet()->getStyle('I' . $kolom_ke)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                            $objPHPExcel->getActiveSheet()->setCellValue('J' . $kolom_ke, $rs->getString('usulan_skpd'));
                            $objPHPExcel->getActiveSheet()->setCellValue('K' . $kolom_ke, $rs->getString('rekening_code'));
                            $objPHPExcel->getActiveSheet()->setCellValue('L' . $kolom_ke, $rs->getString('rekening_name'));
                            $objPHPExcel->getActiveSheet()->setCellValue('M' . $kolom_ke, $rs->getBoolean('non_pajak') ? '0' : '10');
                        }
                        $kolom_ke++;
                    }
                } else {
                    $objPHPExcel->getActiveSheet()->setCellValue('P5', 'Nama Sebelum');
                    $objPHPExcel->getActiveSheet()->setCellValue('Q5', 'Harga Sebelum');
                    $objPHPExcel->getActiveSheet()->setCellValue('R5', 'Alasan Perubahan');
                    $objPHPExcel->getActiveSheet()->getStyle('P5')->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle('Q5')->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle('R5')->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle('P5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                    $objPHPExcel->getActiveSheet()->getStyle('Q5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                    $objPHPExcel->getActiveSheet()->getStyle('R5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

                    $c_verifikasi = new Criteria();
                    $c_verifikasi->add(UsulanVerifikasiPeer::TIPE, 'SSH', Criteria::ALT_NOT_EQUAL);
                    $c_verifikasi->addAscendingOrderByColumn(UsulanVerifikasiPeer::SHSD_ID);
                    $data_verifikasi = UsulanVerifikasiPeer::doSelect($c_verifikasi);

                    $kolom_ke = 6;
                    foreach ($data_verifikasi as $value) {
                        $c_usulan_ssh = new Criteria();
                        $c_usulan_ssh->add(UsulanSSHPeer::ID_USULAN, $value->getIdUsulan());
                        $data_usulan_ssh = UsulanSSHPeer::doSelectOne($c_usulan_ssh);

                        $c_dinas_usulan_ssh = new Criteria();
                        $c_dinas_usulan_ssh->add(UnitKerjaPeer::UNIT_ID, $value->getUnitId());
                        $data_dinas_usulan_ssh = UnitKerjaPeer::doSelectOne($c_dinas_usulan_ssh);

                        if ($data_usulan_ssh) {
                            $objPHPExcel->getActiveSheet()->setCellValue('P' . $kolom_ke, $data_usulan_ssh->getNamaSebelum());
                            $objPHPExcel->getActiveSheet()->setCellValue('Q' . $kolom_ke, $data_usulan_ssh->getHargaSebelum());
                            $objPHPExcel->getActiveSheet()->setCellValue('R' . $kolom_ke, $data_usulan_ssh->getAlasanPerubahanHarga());
                        }

                        $objPHPExcel->getActiveSheet()->setCellValue('C' . $kolom_ke, $value->getShsdId());

                        $c_data_shsd = new Criteria();
                        $c_data_shsd->add(KomponenAllPeer::KOMPONEN_ID, $value->getShsdId());
                        $data_shsd = KomponenAllPeer::doSelectOne($c_data_shsd);
                        if ($data_shsd) {
                            $objPHPExcel->getActiveSheet()->setCellValue('D' . $kolom_ke, $data_shsd->getNamaDasar());
                        } else {
                            $objPHPExcel->getActiveSheet()->setCellValue('D' . $kolom_ke, $value->getShsdName());
                        }

                        $objPHPExcel->getActiveSheet()->getStyle('C' . $kolom_ke)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                        $objPHPExcel->getActiveSheet()->getStyle('F' . $kolom_ke)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                        $objPHPExcel->getActiveSheet()->getStyle('H' . $kolom_ke)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                        $objPHPExcel->getActiveSheet()->getStyle('I' . $kolom_ke)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

                        $objPHPExcel->getActiveSheet()->setCellValue('E' . $kolom_ke, $value->getMerek());
                        $objPHPExcel->getActiveSheet()->setCellValue('F' . $kolom_ke, $value->getSpec());
                        $objPHPExcel->getActiveSheet()->setCellValue('G' . $kolom_ke, $value->getHiddenSpec());
                        $objPHPExcel->getActiveSheet()->setCellValue('H' . $kolom_ke, $value->getSatuan());
                        $objPHPExcel->getActiveSheet()->setCellValue('I' . $kolom_ke, $value->getHarga());
                        $objPHPExcel->getActiveSheet()->setCellValue('J' . $kolom_ke, $data_dinas_usulan_ssh->getUnitName());
                        $objPHPExcel->getActiveSheet()->setCellValue('K' . $kolom_ke, $value->getRekening());
                        $objPHPExcel->getActiveSheet()->setCellValue('M' . $kolom_ke, $value->getPajak());
                        $kolom_ke++;
                    }
                }
            }

            $objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename=export_ssh_' . date('d-m-Y') . '.xls');
            header('Cache-Control: max-age=0');
            $objWriter->save('php://output');

            exit();
        } catch (Exception $exc) {
            echo $exc->getMessage();
            exit();
        }
    }

    public function executeTolakVerifikasiPenyeliaDenganAlasanAllSpjm() {
        $id_spjm = $this->getRequestParameter('id_spjm');
        $jenis = $this->getRequestParameter('jenis_alasan');

        $c_dapat_usulan = new Criteria();
        $c_dapat_usulan->add(UsulanSSHPeer::ID_SPJM, $id_spjm);
        $c_dapat_usulan->addAnd(UsulanSSHPeer::SHSD_ID, NULL);
        $c_dapat_usulan->addAnd(UsulanSSHPeer::STATUS_VERIFIKASI, FALSE);
        $c_dapat_usulan->addAnd(UsulanSSHPeer::STATUS_HAPUS, FALSE);
        $dapat_usulan = UsulanSSHPeer::doSelect($c_dapat_usulan);

        if ($dapat_usulan) {
            foreach ($dapat_usulan as $value) {
                $value->setStatusConfirmasiPenyelia(FALSE);
                $value->setKomentarVerifikator($jenis);
                $value->setStatusBaru(FALSE);
                $value->save();
            }
            $this->setFlash('berhasil', 'Usulan berhasil dikembalikan');
        } else {
            $this->setFlash('gagal', 'Usulan gagal dikembalikan ');
        }

        return $this->redirect('usulan_ssh/usulanssh');
    }

    public function executeTolakVerifikasiPenyeliaDenganAlasan() {
        $id_usulan = $this->getRequestParameter('id_usulan');
        $komentar = $this->getRequestParameter('alasan');
        $jenis = $this->getRequestParameter('jenis_alasan');

        $c_dapat_usulan = new Criteria();
        $c_dapat_usulan->add(UsulanSSHPeer::ID_USULAN, $id_usulan);
        $dapat_usulan = UsulanSSHPeer::doSelectOne($c_dapat_usulan);

        if ($dapat_usulan) {
            $dapat_usulan->setStatusConfirmasiPenyelia(FALSE);
            $dapat_usulan->setKomentarVerifikator($jenis . ' ' . $komentar);
            $dapat_usulan->setStatusBaru(FALSE);
            $dapat_usulan->save();

            $this->setFlash('berhasil', 'Usulan berhasil dikembalikan');
        } else {
            $this->setFlash('gagal', 'Usulan gagal dikembalikan ');
        }

        return $this->redirect('usulan_ssh/usulanssh');
    }

    public function executeTolakVerifikasiSurveyDenganAlasan() {
        $id_usulan = $this->getRequestParameter('id_usulan');
        $komentar = $this->getRequestParameter('alasan');
        $jenis = $this->getRequestParameter('jenis_alasan');

        $c_dapat_usulan = new Criteria();
        $c_dapat_usulan->add(UsulanSSHPeer::ID_USULAN, $id_usulan);
        $dapat_usulan = UsulanSSHPeer::doSelectOne($c_dapat_usulan);

        if ($dapat_usulan) {
            $dapat_usulan->setStatusConfirmasiPenyelia(FALSE);
            $dapat_usulan->setStatusSurvey(FALSE);
            $dapat_usulan->setKomentarSurvey($jenis . ' ' . $komentar);
            $dapat_usulan->setStatusBaru(FALSE);
            $dapat_usulan->save();

            $this->setFlash('berhasil', 'Usulan berhasil dikembalikan');
        } else {
            $this->setFlash('gagal', 'Usulan gagal dikembalikan ');
        }

        return $this->redirect('usulan_ssh/usulanssh');
    }

    public function executeAjaxKodeBarang() {
        $this->kode = $this->getRequestParameter('kode');
    }

    public function executeHapususulandinas() {
        $id_usulan = $this->getRequestParameter('id');

        $c_dapat_usulan = new Criteria();
        $c_dapat_usulan->add(UsulanDinasPeer::ID_USULAN, $id_usulan);
        $dapat_usulan = UsulanDinasPeer::doSelectOne($c_dapat_usulan);

        if ($dapat_usulan) {
            $dapat_usulan->setStatusHapus(TRUE);
            $dapat_usulan->save();
            $this->setFlash('berhasil', 'Usulan dinas berhasil dihapus');
        } else {
            $this->setFlash('gagal', 'Usulan dinas gagal dihapus');
        }


        return $this->redirect('usulan_ssh/usulandinaslist');
    }

    public function executeBatalkonfirmasipenyelia() {

        $id_usulan = $this->getRequestParameter('id');

        $c_dapat_usulan = new Criteria();
        $c_dapat_usulan->add(UsulanSSHPeer::ID_USULAN, $id_usulan);
        $dapat_usulan = UsulanSSHPeer::doSelectOne($c_dapat_usulan);

        if ($dapat_usulan) {
            $dapat_usulan->setStatusConfirmasiPenyelia(FALSE);
            $dapat_usulan->setStatusBaru(FALSE);
            $dapat_usulan->save();

            $this->setFlash('berhasil', 'Usulan berhasil dikembalikan');
        } else {
            $this->setFlash('gagal', 'Usulan gagal dikembalikan ');
        }

        return $this->redirect('usulan_ssh/usulanssh');
    }

    //ticket #9 - list terverifikasi
    public function executeUsulansshverifikasi() {
        $this->processFiltersusulansshverifikasi();
        $this->filters = $this->getUser()->getAttributeHolder()->getAll('sf_admin/usulansshverifikasi/filters');

        $pagers = new sfPropelPager('UsulanVerifikasi', 20);
        $c = new Criteria();
        $c->addDescendingOrderByColumn(UsulanVerifikasiPeer::CREATED_AT);
        $c->addJoin(UsulanVerifikasiPeer::UNIT_ID, UnitKerjaPeer::UNIT_ID, Criteria::INNER_JOIN);
        $c->addJoin(UsulanVerifikasiPeer::ID_USULAN, UsulanSSHPeer::ID_USULAN, Criteria::INNER_JOIN);
        $c->addJoin(UsulanVerifikasiPeer::ID_SPJM, UsulanSPJMPeer::ID_SPJM, Criteria::INNER_JOIN);
        $this->addFiltersCriteriausulansshverifikasi($c);

        $pagers->setCriteria($c);
        $pagers->setPage($this->getRequestParameter('page', 1));
        $pagers->init();

        $this->pager = $pagers;
    }

    //ticket #9 - list terverifikasi
    //ticket #9 - list terverifikasi
    protected function processFiltersusulansshverifikasi() {
        if ($this->getRequest()->hasParameter('filter')) {
            $filters = $this->getRequestParameter('filters');
            $this->getUser()->getAttributeHolder()->removeNamespace('sf_admin/usulansshverifikasi/filters');
            $this->getUser()->getAttributeHolder()->add($filters, 'sf_admin/usulansshverifikasi/filters');
        }
    }

    //ticket #9 - list terverifikasi
    //ticket #9 - list terverifikasi
    protected function addFiltersCriteriausulansshverifikasi($c) {
        if (isset($this->filters['select'])) {
            if ($this->filters['select'] == 1) {
                if (isset($this->filters['komponen_name_is_empty'])) {
                    $criterion = $c->getNewCriterion(UsulanVerifikasiPeer::SHSD_NAME, '');
                    $criterion->addOr($c->getNewCriterion(UsulanVerifikasiPeer::SHSD_NAME, null, Criteria::ISNULL));
                    $c->add($criterion);
                } else if (isset($this->filters['komponen']) && $this->filters['komponen'] !== '') {
                    $kata = '%' . $this->filters['komponen'] . '%';
                    $c->add(UsulanVerifikasiPeer::SHSD_NAME, strtr($kata, '*', '%'), Criteria::ILIKE);
                }
            } elseif ($this->filters['select'] == 2) {
                if (isset($this->filters['komponen_id_is_empty'])) {
                    $criterion = $c->getNewCriterion(UsulanVerifikasiPeer::SHSD_ID, '');
                    $criterion->addOr($c->getNewCriterion(UsulanVerifikasiPeer::SHSD_ID, null, Criteria::ISNULL));
                    $c->add($criterion);
                } else if (isset($this->filters['komponen']) && $this->filters['komponen'] !== '') {
                    $kata = '%' . $this->filters['komponen'] . '%';
                    $c->add(UsulanVerifikasiPeer::SHSD_ID, strtr($kata, '*', '%'), Criteria::ILIKE);
                }
            } elseif ($this->filters['select'] == 3) {
                if (isset($this->filters['rekening_code_is_empty'])) {
                    $criterion = $c->getNewCriterion(UsulanVerifikasiPeer::REKENING, '');
                    $criterion->addOr($c->getNewCriterion(UsulanVerifikasiPeer::REKENING, null, Criteria::ISNULL));
                    $c->add($criterion);
                } else if (isset($this->filters['komponen']) && $this->filters['komponen'] !== '') {
                    $kata = '%' . $this->filters['komponen'] . '%';
                    $c->add(UsulanVerifikasiPeer::REKENING, strtr($kata, '*', '%'), Criteria::ILIKE);
                }
            } elseif ($this->filters['select'] == 4) {
                if (isset($this->filters['komponen_harga_is_empty'])) {
                    $criterion = $c->getNewCriterion(UsulanVerifikasiPeer::HARGA, '');
                    $criterion->addOr($c->getNewCriterion(UsulanVerifikasiPeer::HARGA, null, Criteria::ISNULL));
                    $c->add($criterion);
                } else if (isset($this->filters['komponen']) && $this->filters['komponen'] !== '') {
                    $kata = '' . $this->filters['komponen'] . '';
                    $c->add(UsulanVerifikasiPeer::HARGA, $kata, Criteria::EQUAL);
                }
            } elseif ($this->filters['select'] == 5) {
                if (isset($this->filters['tipe_komponen_is_empty'])) {
                    $criterion = $c->getNewCriterion(UsulanVerifikasiPeer::TIPE, '');
                    $criterion->addOr($c->getNewCriterion(UsulanVerifikasiPeer::TIPE, null, Criteria::ISNULL));
                    $c->add($criterion);
                } else if (isset($this->filters['komponen']) && $this->filters['komponen'] !== '') {
                    $kata = '%' . $this->filters['komponen'] . '%';
                    $c->add(UsulanVerifikasiPeer::TIPE, strtr($kata, '*', '%'), Criteria::ILIKE);
                }
            } else {
                if (isset($this->filters['komponen_is_empty'])) {
                    echo 'is empty';
                    $criterion = $c->getNewCriterion(UsulanVerifikasiPeer::SHSD_NAME, '');
                    $criterion->addOr($c->getNewCriterion(UsulanVerifikasiPeer::SHSD_NAME, null, Criteria::ISNULL));
                    $c->add($criterion);
                }
            }
        }
    }

    //ticket #9 - list terverifikasi
    //ticket #7 - list usulan SSH
    public function executeUsulansshlist() {
        $this->processFiltersusulansshlist();
        $this->filters = $this->getUser()->getAttributeHolder()->getAll('sf_admin/usulansshlist/filters');

        $pagers = new sfPropelPager('UsulanSSH', 20);
        $c = new Criteria();
        $c->addDescendingOrderByColumn(UsulanSSHPeer::UPDATED_AT);
        $c->addJoin(UsulanSSHPeer::UNIT_ID, UnitKerjaPeer::UNIT_ID, Criteria::INNER_JOIN);
        $c->addJoin(UsulanSSHPeer::ID_SPJM, UsulanSPJMPeer::ID_SPJM, Criteria::INNER_JOIN);
        if ($dinas = $this->getUser()->getAttributeHolder()->getAll('dinas')) {
            $c->add(UnitKerjaPeer::UNIT_ID, $dinas['unit_id'], Criteria::EQUAL);
        }
        $this->addFiltersCriteriausulansshlist($c);

        $pagers->setCriteria($c);
        $pagers->setPage($this->getRequestParameter('page', 1));
        $pagers->init();

        $this->pager = $pagers;
    }

    public function executeUsulansshlistdinas() {
        $c = new Criteria();
        $c->add(UsulanSPJMPeer::STATUS_HAPUS, FALSE);
        //if ($dinas = $this->getUser()->getAttributeHolder()->getAll('dinas')) {
        if ($this->getUser()->getCredentialMember() == 'dinas') {
            $dinas = $this->getUser()->getAttributeHolder()->getAll('dinas');
            $c->add(UsulanSPJMPeer::SKPD, $dinas['unit_id'], Criteria::EQUAL);
        } else if ($this->getUser()->getCredentialMember() == 'peneliti') {
            $nama = $this->getUser()->getNamaUser();
            $c->add(UsulanSPJMPeer::PENYELIA, $nama, Criteria::EQUAL);
        }
        $c->addAscendingOrderByColumn(UsulanSPJMPeer::ID_SPJM);
        $rs_usulanSPJM = UsulanSPJMPeer::doSelect($c);

        $this->rs_usulanSPJM = $rs_usulanSPJM;
    }

    //ticket #7 - list usulan SSH
    //ticket #7 - list usulan SSH
    protected function processFiltersusulansshlist() {
        if ($this->getRequest()->hasParameter('filter')) {
            $filters = $this->getRequestParameter('filters');
            $this->getUser()->getAttributeHolder()->removeNamespace('sf_admin/usulansshlist/filters');
            $this->getUser()->getAttributeHolder()->add($filters, 'sf_admin/usulansshlist/filters');
        }
    }

    //ticket #7 - list usulan SSH
    //ticket #7 - list usulan SSH
    protected function addFiltersCriteriausulansshlist($c) {
        if (isset($this->filters['select'])) {
            if ($this->filters['select'] == 1) {
                if (isset($this->filters['usulan_nama_is_empty'])) {
                    $criterion = $c->getNewCriterion(UsulanSSHPeer::NAMA, '');
                    $criterion->addOr($c->getNewCriterion(UsulanSSHPeer::NAMA, null, Criteria::ISNULL));
                    $c->add($criterion);
                } else if (isset($this->filters['komponen']) && $this->filters['komponen'] !== '') {
                    $kata = '%' . $this->filters['komponen'] . '%';
                    $c->add(UsulanSSHPeer::NAMA, strtr($kata, '*', '%'), Criteria::ILIKE);
                }
            } elseif ($this->filters['select'] == 2) {
                if (isset($this->filters['usulan_spec_is_empty'])) {
                    $criterion = $c->getNewCriterion(UsulanSSHPeer::SPEC, '');
                    $criterion->addOr($c->getNewCriterion(UsulanSSHPeer::SPEC, null, Criteria::ISNULL));
                    $c->add($criterion);
                } else if (isset($this->filters['komponen']) && $this->filters['komponen'] !== '') {
                    $kata = '%' . $this->filters['komponen'] . '%';
                    $c->add(UsulanSSHPeer::SPEC, strtr($kata, '*', '%'), Criteria::ILIKE);
                }
            } elseif ($this->filters['select'] == 3) {
                if (isset($this->filters['rekening_code_is_empty'])) {
                    $criterion = $c->getNewCriterion(UsulanSSHPeer::REKENING, '');
                    $criterion->addOr($c->getNewCriterion(UsulanSSHPeer::REKENING, null, Criteria::ISNULL));
                    $c->add($criterion);
                } else if (isset($this->filters['komponen']) && $this->filters['komponen'] !== '') {
                    $kata = '%' . $this->filters['komponen'] . '%';
                    $c->add(UsulanSSHPeer::REKENING, strtr($kata, '*', '%'), Criteria::ILIKE);
                }
            } elseif ($this->filters['select'] == 4) {
                if (isset($this->filters['komponen_harga_is_empty'])) {
                    $criterion = $c->getNewCriterion(UsulanSSHPeer::HARGA, '');
                    $criterion->addOr($c->getNewCriterion(UsulanSSHPeer::HARGA, null, Criteria::ISNULL));
                    $c->add($criterion);
                } else if (isset($this->filters['komponen']) && $this->filters['komponen'] !== '') {
                    $kata = '' . $this->filters['komponen'] . '';
                    $c->add(UsulanSSHPeer::HARGA, $kata,Criteria::EQUAL);
                }
            }  else {
                if (isset($this->filters['komponen_is_empty'])) {
                    echo 'is empty';
                    $criterion = $c->getNewCriterion(UsulanSSHPeer::NAMA, '');
                    $criterion->addOr($c->getNewCriterion(UsulanSSHPeer::NAMA, null, Criteria::ISNULL));
                    $c->add($criterion);
                }
            }
        }
    }

    //ticket #7 - list usulan SSH
    //ticket #4 - form upload SPJM + usulan SSH
    public function executeUsulansshbaru() {
        $unit_kerja = new Criteria();
        $unit_kerja->addAscendingOrderByColumn(UnitKerjaPeer::UNIT_NAME);
        $this->unit_kerja = $rs_unit_kerja = UnitKerjaPeer::doSelect($unit_kerja);

        $nama = $this->getUser()->getNamaUser();
        $unit_kerja_handle = new Criteria();
        $unit_kerja_handle->add(UserHandleV2Peer::USER_ID, $nama, Criteria::EQUAL);
        $unit_kerja_handle->addJoin(UnitKerjaPeer::UNIT_ID, UserHandleV2Peer::UNIT_ID, Criteria::INNER_JOIN);
        $unit_kerja_handle->addAscendingOrderByColumn(UnitKerjaPeer::UNIT_NAME);
        $this->unit_kerja_handle = $rs_unit_kerja_handle = UnitKerjaPeer::doSelect($unit_kerja_handle);

        $c_token = new Criteria();
        $c_token->add(UsulanTokenPeer::PENYELIA, $nama, Criteria::EQUAL);
        $c_token->addAnd(UsulanTokenPeer::STATUS_USED, FALSE, Criteria::EQUAL);
        $c_token->addAscendingOrderByColumn(UsulanTokenPeer::REQUIRED_AT);
        $this->list_token = UsulanTokenPeer::doSelect($c_token);
    }

    public function executeUsulansshsurvey() {
        $c = new Criteria();
        
        $c->addAnd(UsulanSSHPeer::STATUS_CONFIRMASI_PENYELIA, TRUE);
        $c->addAnd(UsulanSSHPeer::STATUS_HAPUS, FALSE);
        $c->addAnd(UsulanSSHPeer::STATUS_SURVEY, FALSE);
        $c->addAnd(UsulanSSHPeer::STATUS_VERIFIKASI, FALSE);
        $rs_usulanSurvey = UsulanSSHPeer::doSelect($c);
        $this->rs_usulanSurvey = $rs_usulanSurvey;
    }

    protected function processFiltersusulansshsurvey() {
        if ($this->getRequest()->hasParameter('filter')) {
            $filters = $this->getRequestParameter('filters');
            $this->getUser()->getAttributeHolder()->removeNamespace('sf_admin/usulansshsurvey/filters');
            $this->getUser()->getAttributeHolder()->add($filters, 'sf_admin/usulansshsurvey/filters');
        }
    }

    protected function addFiltersCriteriausulansshsurvey($c) {
        if (isset($this->filters['select'])) {
            if ($this->filters['select'] == 1) {
                if (isset($this->filters['usulan_nama_is_empty'])) {
                    $criterion = $c->getNewCriterion(UsulanSSHPeer::NAMA, '');
                    $criterion->addOr($c->getNewCriterion(UsulanSSHPeer::NAMA, null, Criteria::ISNULL));
                    $c->add($criterion);
                } else if (isset($this->filters['komponen']) && $this->filters['komponen'] !== '') {
                    $kata = '%' . $this->filters['komponen'] . '%';
                    $c->add(UsulanSSHPeer::NAMA, strtr($kata, '*', '%'), Criteria::ILIKE);
                }
            } elseif ($this->filters['select'] == 2) {
                if (isset($this->filters['usulan_spec_is_empty'])) {
                    $criterion = $c->getNewCriterion(UsulanSSHPeer::SPEC, '');
                    $criterion->addOr($c->getNewCriterion(UsulanSSHPeer::SPEC, null, Criteria::ISNULL));
                    $c->add($criterion);
                } else if (isset($this->filters['komponen']) && $this->filters['komponen'] !== '') {
                    $kata = '%' . $this->filters['komponen'] . '%';
                    $c->add(UsulanSSHPeer::SPEC, strtr($kata, '*', '%'), Criteria::ILIKE);
                }
            } elseif ($this->filters['select'] == 3) {
                if (isset($this->filters['rekening_code_is_empty'])) {
                    $criterion = $c->getNewCriterion(UsulanSSHPeer::REKENING, '');
                    $criterion->addOr($c->getNewCriterion(UsulanSSHPeer::REKENING, null, Criteria::ISNULL));
                    $c->add($criterion);
                } else if (isset($this->filters['komponen']) && $this->filters['komponen'] !== '') {
                    $kata = '%' . $this->filters['komponen'] . '%';
                    $c->add(UsulanSSHPeer::REKENING, strtr($kata, '*', '%'), Criteria::ILIKE);
                }
            } else {
                if (isset($this->filters['komponen_is_empty'])) {
                    echo 'is empty';
                    $criterion = $c->getNewCriterion(UsulanSSHPeer::NAMA, '');
                    $criterion->addOr($c->getNewCriterion(UsulanSSHPeer::NAMA, null, Criteria::ISNULL));
                    $c->add($criterion);
                }
            }
        }
    }

    //ticket #4 - form upload SPJM + usulan SSH
    //ticket #6 - list antrian konfirmasi usulan penyelia
    public function executeUsulansshkonfirmasi() {
        $this->processFiltersusulansshkonfirmasi();
        $this->filters = $this->getUser()->getAttributeHolder()->getAll('sf_admin/usulansshkonfirmasi/filters');

        $pagers = new sfPropelPager('UsulanSSH', 50);
        $c = new Criteria();
        if ($this->getUser()->getNamaLogin() <> 'admin' && $this->getUser()->getNamaLogin() <> 'superadmin_ebudgeting') {
            $c->add(UsulanSPJMPeer::PENYELIA, $this->getUser()->getNamaLogin(), Criteria::EQUAL);
        }       
        $c->addAnd(UsulanSSHPeer::STATUS_VERIFIKASI, FALSE, Criteria::EQUAL);
        $c->addAnd(UsulanSSHPeer::STATUS_CONFIRMASI_PENYELIA, FALSE, Criteria::EQUAL);
        $c->addAnd(UsulanSSHPeer::STATUS_HAPUS, FALSE, Criteria::EQUAL);
        $c->addDescendingOrderByColumn(UsulanSSHPeer::UPDATED_AT);
        $c->addAscendingOrderByColumn(UsulanSSHPeer::NAMA);
        $c->addJoin(UsulanSSHPeer::UNIT_ID, UnitKerjaPeer::UNIT_ID, Criteria::INNER_JOIN);
        $c->addJoin(UsulanSSHPeer::ID_SPJM, UsulanSPJMPeer::ID_SPJM, Criteria::INNER_JOIN);
        $this->addFiltersCriteriausulansshkonfirmasi($c);

        $pagers->setCriteria($c);
        $pagers->setPage($this->getRequestParameter('page', 1));
        $pagers->init();

        $this->pager = $pagers;
    }

    //ticket #6 - list antrian konfirmasi usulan penyelia
    //ticket #6 - list antrian konfirmasi usulan penyelia
    protected function processFiltersusulansshkonfirmasi() {
        if ($this->getRequest()->hasParameter('filter')) {
            $filters = $this->getRequestParameter('filters');
            $this->getUser()->getAttributeHolder()->removeNamespace('sf_admin/usulansshkonfirmasi/filters');
            $this->getUser()->getAttributeHolder()->add($filters, 'sf_admin/usulansshkonfirmasi/filters');
        }
    }

    //ticket #6 - list antrian konfirmasi usulan penyelia
    //ticket #6 - list antrian konfirmasi usulan penyelia
    protected function addFiltersCriteriausulansshkonfirmasi($c) {
        if (isset($this->filters['select'])) {
            if ($this->filters['select'] == 1) {
                if (isset($this->filters['usulan_nama_is_empty'])) {
                    $criterion = $c->getNewCriterion(UsulanSSHPeer::NAMA, '');
                    $criterion->addOr($c->getNewCriterion(UsulanSSHPeer::NAMA, null, Criteria::ISNULL));
                    $c->add($criterion);
                } else if (isset($this->filters['komponen']) && $this->filters['komponen'] !== '') {
                    $kata = '%' . $this->filters['komponen'] . '%';
                    $c->add(UsulanSSHPeer::NAMA, strtr($kata, '*', '%'), Criteria::ILIKE);
                }
            } elseif ($this->filters['select'] == 2) {
                if (isset($this->filters['usulan_spec_is_empty'])) {
                    $criterion = $c->getNewCriterion(UsulanSSHPeer::SPEC, '');
                    $criterion->addOr($c->getNewCriterion(UsulanSSHPeer::SPEC, null, Criteria::ISNULL));
                    $c->add($criterion);
                } else if (isset($this->filters['komponen']) && $this->filters['komponen'] !== '') {
                    $kata = '%' . $this->filters['komponen'] . '%';
                    $c->add(UsulanSSHPeer::SPEC, strtr($kata, '*', '%'), Criteria::ILIKE);
                }
            } elseif ($this->filters['select'] == 3) {
                if (isset($this->filters['rekening_code_is_empty'])) {
                    $criterion = $c->getNewCriterion(UsulanSSHPeer::REKENING, '');
                    $criterion->addOr($c->getNewCriterion(UsulanSSHPeer::REKENING, null, Criteria::ISNULL));
                    $c->add($criterion);
                } else if (isset($this->filters['komponen']) && $this->filters['komponen'] !== '') {
                    $kata = '%' . $this->filters['komponen'] . '%';
                    $c->add(UsulanSSHPeer::REKENING, strtr($kata, '*', '%'), Criteria::ILIKE);
                }
            } else {
                if (isset($this->filters['komponen_is_empty'])) {
                    echo 'is empty';
                    $criterion = $c->getNewCriterion(UsulanSSHPeer::NAMA, '');
                    $criterion->addOr($c->getNewCriterion(UsulanSSHPeer::NAMA, null, Criteria::ISNULL));
                    $c->add($criterion);
                }
            }
        }
    }

    //ticket #6 - list antrian konfirmasi usulan penyelia
    //    ticket #9 - print semi buku
    function executePrintSemiBuku() {
        try {
            $array_sheet = array('1.1.7.01.01.01',  '1.1.7.01.01.02','1.1.7.01.01.04','1.1.7.01.01.05','1.1.7.01.01.08','1.1.7.01.01.09','1.1.7.01.01.10','1.1.7.01.01.11','1.1.7.01.01.12','1.1.7.01.02.01','1.1.7.01.02.02','1.1.7.01.02.06','1.1.7.01.02.08','1.1.7.01.02.10','1.1.7.01.02.11','1.1.7.01.02.12','1.1.7.01.02.13','1.1.7.01.02.14','1.1.7.01.02.15','1.1.7.01.02.16','1.1.7.01.03.01','1.1.7.01.03.02','1.1.7.01.03.03','1.1.7.01.03.04','1.1.7.01.03.05','1.1.7.01.03.06','1.1.7.01.03.07','1.1.7.01.03.08','1.1.7.01.03.09','1.1.7.01.03.11','1.1.7.01.03.12','1.1.7.01.03.13','1.1.7.01.04.01','1.1.7.01.04.02','1.1.7.01.07.01','1.1.7.01.07.02','1.1.7.01.07.03','1.1.7.01.07.04','1.1.7.01.07.05','1.1.7.01.07.06','1.1.7.01.10.01','1.1.7.01.10.02','1.1.7.01.10.03','1.1.7.01.10.04','1.1.7.02.02.01','1.1.7.02.02.06','1.1.7.02.02.07','1.3.2.01.01.01','1.3.2.01.01.02','1.3.2.01.01.03','1.3.2.01.01.04','1.3.2.01.01.05','1.3.2.01.01.06','1.3.2.01.01.07','1.3.2.01.01.08','1.3.2.01.01.09','1.3.2.01.01.10','1.3.2.01.01.11','1.3.2.01.01.12','1.3.2.01.02.01','1.3.2.01.02.02','1.3.2.01.02.03','1.3.2.01.02.04','1.3.2.01.02.05','1.3.2.01.02.06','1.3.2.01.03.01','1.3.2.01.03.02','1.3.2.01.03.03','1.3.2.01.03.04','1.3.2.01.03.05','1.3.2.01.03.06','1.3.2.01.03.07','1.3.2.01.03.08','1.3.2.01.03.09','1.3.2.01.03.16','1.3.2.02.01.01','1.3.2.02.01.02','1.3.2.02.01.03','1.3.2.02.01.04','1.3.2.02.01.05','1.3.2.02.01.06','1.3.2.02.01.09','1.3.2.02.02.01','1.3.2.02.02.02','1.3.2.02.02.04','1.3.2.02.03.01','1.3.2.02.03.02','1.3.2.02.03.03','1.3.2.02.03.05','1.3.2.02.04.01','1.3.2.02.04.02','1.3.2.02.04.03','1.3.2.02.04.04','1.3.2.03.01.01','1.3.2.03.01.02','1.3.2.03.01.03','1.3.2.03.01.04','1.3.2.03.01.05','1.3.2.03.01.06','1.3.2.03.01.07','1.3.2.03.01.08','1.3.2.03.01.10','1.3.2.03.02.01','1.3.2.03.02.02','1.3.2.03.02.03','1.3.2.03.02.04','1.3.2.03.02.05','1.3.2.03.02.06','1.3.2.03.02.07','1.3.2.03.02.08','1.3.2.03.02.09','1.3.2.03.02.10','1.3.2.03.02.11','1.3.2.03.02.13','1.3.2.03.03.01','1.3.2.03.03.02','1.3.2.03.03.03','1.3.2.03.03.04','1.3.2.03.03.05','1.3.2.03.03.06','1.3.2.03.03.07','1.3.2.03.03.08','1.3.2.03.03.10','1.3.2.03.03.11','1.3.2.03.03.21','1.3.2.04.01.01','1.3.2.04.01.02','1.3.2.04.01.03','1.3.2.04.01.04','1.3.2.04.01.05','1.3.2.04.01.06','1.3.2.04.01.07','1.3.2.04.01.08','1.3.2.04.01.09','1.3.2.04.01.10','1.3.2.05.01.01','1.3.2.05.01.02','1.3.2.05.01.03','1.3.2.05.01.04','1.3.2.05.01.05','1.3.2.05.02.01','1.3.2.05.02.02','1.3.2.05.02.03','1.3.2.05.02.04','1.3.2.05.02.05','1.3.2.05.02.06','1.3.2.05.02.07','1.3.2.06.01.01','1.3.2.06.01.02','1.3.2.06.01.03','1.3.2.06.01.04','1.3.2.06.01.05','1.3.2.06.01.06','1.3.2.06.02.01','1.3.2.06.02.11','1.3.2.06.03.01','1.3.2.06.03.47','1.3.2.06.03.48','1.3.2.07.01.01','1.3.2.07.01.02','1.3.2.07.01.03','1.3.2.07.01.04','1.3.2.07.01.05','1.3.2.07.01.06','1.3.2.07.01.07','1.3.2.07.01.08','1.3.2.07.01.09','1.3.2.07.01.10','1.3.2.07.01.13','1.3.2.07.01.14','1.3.2.07.01.19','1.3.2.07.01.20','1.3.2.07.01.21','1.3.2.07.01.22','1.3.2.07.01.29','1.3.2.07.02.01','1.3.2.07.02.02','1.3.2.07.02.03','1.3.2.07.02.04','1.3.2.07.02.05','1.3.2.07.02.06','1.3.2.07.02.07','1.3.2.08.01.01','1.3.2.08.01.02','1.3.2.08.01.03','1.3.2.08.01.04','1.3.2.08.01.05','1.3.2.08.01.06','1.3.2.08.01.07','1.3.2.08.01.08','1.3.2.08.01.09','1.3.2.08.01.10','1.3.2.08.01.11','1.3.2.08.01.13','1.3.2.08.01.18','1.3.2.08.01.19','1.3.2.08.01.20','1.3.2.08.01.41','1.3.2.08.01.58','1.3.2.08.01.61','1.3.2.08.01.64','1.3.2.08.03.01','1.3.2.08.03.02','1.3.2.08.03.03','1.3.2.08.03.04','1.3.2.08.03.05','1.3.2.08.03.06','1.3.2.08.03.07','1.3.2.08.03.08','1.3.2.08.03.09','1.3.2.08.03.10','1.3.2.08.03.11','1.3.2.08.03.12','1.3.2.08.03.13','1.3.2.08.03.15','1.3.2.08.03.16','1.3.2.08.07.04','1.3.2.08.07.06','1.3.2.10.01.01','1.3.2.10.01.02','1.3.2.10.02.01','1.3.2.10.02.03','1.3.2.10.02.04','1.3.2.10.02.05','1.3.2.15.01.01','1.3.2.15.01.02','1.3.2.15.01.03','1.3.2.15.02.01','1.3.2.15.02.02','1.3.2.15.02.03','1.3.2.15.02.04','1.3.2.15.02.05','1.3.2.15.02.06','1.3.2.15.03.01','1.3.2.15.03.02','1.3.2.18.01.01','1.3.2.18.01.02','1.3.2.18.01.03','1.3.2.18.01.04','1.3.2.18.01.05','1.3.2.18.01.06','1.3.2.19.01.01','1.3.2.19.01.02','1.3.2.19.01.03','1.3.2.19.01.04','1.3.2.19.01.05','1.3.2.19.01.06','1.3.5.01.01.01','1.3.5.01.01.02','1.3.5.01.01.03','1.3.5.01.01.04','1.3.5.01.01.05','1.3.5.01.01.06','1.3.5.01.01.07','1.3.5.01.01.08','1.3.5.01.01.09','1.3.5.01.01.10','1.3.5.01.01.12','1.3.5.01.02.01','1.3.5.01.02.02','1.3.5.01.03.01','1.3.5.01.03.02','1.3.5.01.03.03','1.3.5.02.01.01','1.3.5.02.01.02','1.3.5.02.01.03','1.3.5.02.01.04','1.3.5.02.02.01','1.3.5.02.02.02','1.3.5.02.02.03','1.3.5.02.03.01','1.3.5.02.03.02','1.3.5.03.01.01','1.3.5.03.01.02','1.3.5.03.01.03','1.3.5.03.01.04','1.3.5.03.02.01','1.3.5.03.02.03','1.3.5.03.02.04','1.3.5.05.01.01','1.5.3.01.01.01','2.1.1.01.01.01','2.1.1.01.01.02','2.1.1.01.01.03','2.1.1.01.02.01','2.1.1.01.02.02','2.1.1.01.02.03','2.1.1.01.03.01','2.1.1.01.03.02','2.1.1.01.03.03','2.1.1.02.01.01','2.1.1.02.01.02','2.1.1.02.02.01','2.1.1.02.02.02','2.1.1.02.03.01','2.1.1.02.04.01','2.1.1.02.04.02','2.1.1.02.04.03','2.1.1.03.01.01','2.1.1.03.01.02','2.1.1.03.02.01','2.1.1.03.02.02','2.1.1.03.02.03','2.1.1.03.02.04','2.1.1.03.02.05','2.1.1.03.02.06','2.1.1.03.02.07','2.1.1.03.03.01','2.1.1.03.04.01','2.1.1.03.04.02','2.1.1.03.05.01','2.1.2.01.01.01','EST');
            $objPHPExcel = new sfPhpExcel();

            for ($sheet_ke = 0; $sheet_ke < count($array_sheet); $sheet_ke++) {
                if ($array_sheet[$sheet_ke] <> 'EST') {
                    $c_kategory_shsd = new Criteria();
                    $c_kategory_shsd->add(KategoriShsdPeer::KATEGORI_SHSD_ID, $array_sheet[$sheet_ke], Criteria::EQUAL);
                    $data_kategori = KategoriShsdPeer::doSelectOne($c_kategory_shsd);
                }

                if ($sheet_ke > 0) {
                    $objPHPExcel->createSheet($sheet_ke);
                }

                $objPHPExcel->setActiveSheetIndex($sheet_ke);
                if ($array_sheet[$sheet_ke] <> 'EST') {
                    $objPHPExcel->getActiveSheet()->setTitle($array_sheet[$sheet_ke] . ' ' . ucwords(strtolower($data_kategori->getKategoriShsdName())));
                } else {
                    $objPHPExcel->getActiveSheet()->setTitle($array_sheet[$sheet_ke] . ' ' . ucwords('Estimasi'));
                }
                $objPHPExcel->getActiveSheet()->setCellValue('C5', 'No');
                $objPHPExcel->getActiveSheet()->setCellValue('D5', 'Nama Barang');
                $objPHPExcel->getActiveSheet()->setCellValue('E5', 'Merk');
                $objPHPExcel->getActiveSheet()->setCellValue('F5', 'Spesifikasi');
                $objPHPExcel->getActiveSheet()->setCellValue('G5', 'Hidden Spec');
                $objPHPExcel->getActiveSheet()->setCellValue('H5', 'Satuan');
                $objPHPExcel->getActiveSheet()->setCellValue('I5', 'Harga Satuan');
                $objPHPExcel->getActiveSheet()->setCellValue('J5', 'Usulan SKPD');
                $objPHPExcel->getActiveSheet()->setCellValue('L5', 'Kode Rekening');
                $objPHPExcel->getActiveSheet()->setCellValue('N5', 'Pajak');

                $objPHPExcel->getActiveSheet()->setCellValue('P5', 'Nama Sebelum');
                $objPHPExcel->getActiveSheet()->setCellValue('Q5', 'Harga Sebelum');
                $objPHPExcel->getActiveSheet()->setCellValue('R5', 'Alasan Perubahan');


                if ($array_sheet[$sheet_ke] <> 'EST') {
//                    $c_verifikasi = new Criteria();
//                    $c_verifikasi->add(ShsdPeer::SHSD_ID, $array_sheet[$sheet_ke] . '%', Criteria::ILIKE);
//                    $c_verifikasi->addJoin(ShsdPeer::SHSD_ID, UsulanVerifikasiPeer::SHSD_ID, Criteria::INNER_JOIN);
//                    $c_verifikasi->addAscendingOrderByColumn(ShsdPeer::SHSD_ID);
//                    $data_verifikasi = ShsdPeer::doSelect($c_verifikasi);
//
//                    $kolom_ke = 6;
//                    foreach ($data_verifikasi as $value) {
//                        $objPHPExcel->getActiveSheet()->setCellValue('C' . $kolom_ke, $value->getShsdId());
//                        $objPHPExcel->getActiveSheet()->setCellValue('D' . $kolom_ke, $value->getShsdName());
//                        $objPHPExcel->getActiveSheet()->setCellValue('E' . $kolom_ke, $value->getShsdMerk());
//                        $objPHPExcel->getActiveSheet()->setCellValue('F' . $kolom_ke, $value->getSpec());
//                        $objPHPExcel->getActiveSheet()->setCellValue('G' . $kolom_ke, $value->getHiddenSpec());
//                        $objPHPExcel->getActiveSheet()->setCellValue('H' . $kolom_ke, $value->getSatuan());
//                        $objPHPExcel->getActiveSheet()->setCellValue('I' . $kolom_ke, $value->getShsdHarga());
//                        $objPHPExcel->getActiveSheet()->setCellValue('J' . $kolom_ke, $value->getUsulanSkpd());
//                        $objPHPExcel->getActiveSheet()->setCellValue('L' . $kolom_ke, $value->getRekeningCode());
//                        if ($value->getNonPajak() == true) {
//                            $objPHPExcel->getActiveSheet()->setCellValue('N' . $kolom_ke, 0);
//                        } else {
//                            $objPHPExcel->getActiveSheet()->setCellValue('N' . $kolom_ke, 10);
//                        }
//
//                        $c_usulan_ssh = new Criteria();
//                        $c_usulan_ssh->add(UsulanSSHPeer::SHSD_ID, $value->getShsdId());
//                        $data_usulan_ssh = UsulanSSHPeer::doSelectOne($c_usulan_ssh);
//
//                        if ($data_usulan_ssh) {
//                            $objPHPExcel->getActiveSheet()->setCellValue('P' . $kolom_ke, $data_usulan_ssh->getNamaSebelum());
//                            $objPHPExcel->getActiveSheet()->setCellValue('Q' . $kolom_ke, $data_usulan_ssh->getHargaSebelum());
//                            $objPHPExcel->getActiveSheet()->setCellValue('R' . $kolom_ke, $data_usulan_ssh->getAlasanPerubahanHarga());
//                        }
//
//                        $kolom_ke++;
//                    }

                    $c_verifikasi = new Criteria();
                    $c_verifikasi->add(UsulanVerifikasiPeer::SHSD_ID, $array_sheet[$sheet_ke] . '%', Criteria::ILIKE);
                    $c_verifikasi->addAscendingOrderByColumn(UsulanVerifikasiPeer::SHSD_ID);
                    $data_verifikasi = UsulanVerifikasiPeer::doSelect($c_verifikasi);

                    $kolom_ke = 6;
                    foreach ($data_verifikasi as $value) {
                        $c_usulan_ssh = new Criteria();
                        $c_usulan_ssh->add(UsulanSSHPeer::ID_USULAN, $value->getIdUsulan());
                        $data_usulan_ssh = UsulanSSHPeer::doSelectOne($c_usulan_ssh);

                        $c_dinas_usulan_ssh = new Criteria();
                        $c_dinas_usulan_ssh->add(UnitKerjaPeer::UNIT_ID, $value->getUnitId());
                        $data_dinas_usulan_ssh = UnitKerjaPeer::doSelectOne($c_dinas_usulan_ssh);

                        if ($data_usulan_ssh) {
                            $objPHPExcel->getActiveSheet()->setCellValue('P' . $kolom_ke, $data_usulan_ssh->getNamaSebelum());
                            $objPHPExcel->getActiveSheet()->setCellValue('Q' . $kolom_ke, $data_usulan_ssh->getHargaSebelum());
                            $objPHPExcel->getActiveSheet()->setCellValue('R' . $kolom_ke, $data_usulan_ssh->getAlasanPerubahanHarga());
                        }

                        $objPHPExcel->getActiveSheet()->setCellValue('C' . $kolom_ke, $value->getShsdId());

                        $c_data_shsd = new Criteria();
                        $c_data_shsd->add(KomponenAllPeer::KOMPONEN_ID, $value->getShsdId());
                        $data_shsd = KomponenAllPeer::doSelectOne($c_data_shsd);
                        if ($data_shsd) {
                            $objPHPExcel->getActiveSheet()->setCellValue('D' . $kolom_ke, $data_shsd->getNamaDasar());
                        } else {
                            $objPHPExcel->getActiveSheet()->setCellValue('D' . $kolom_ke, $value->getShsdName());
                        }

                        $objPHPExcel->getActiveSheet()->setCellValue('E' . $kolom_ke, $value->getMerek());
                        $objPHPExcel->getActiveSheet()->setCellValue('F' . $kolom_ke, $value->getSpec());
                        $objPHPExcel->getActiveSheet()->setCellValue('G' . $kolom_ke, $value->getHiddenSpec());
                        $objPHPExcel->getActiveSheet()->setCellValue('H' . $kolom_ke, $value->getSatuan());
                        $objPHPExcel->getActiveSheet()->setCellValue('I' . $kolom_ke, $value->getHarga());
                        $objPHPExcel->getActiveSheet()->setCellValue('J' . $kolom_ke, $data_dinas_usulan_ssh->getUnitName());
                        $objPHPExcel->getActiveSheet()->setCellValue('L' . $kolom_ke, $value->getRekening());
                        $objPHPExcel->getActiveSheet()->setCellValue('N' . $kolom_ke, $value->getPajak());
                        $kolom_ke++;
                    }
                } else {
//                    $c_verifikasi = new Criteria();
//                    $c_verifikasi->add(KomponenPeer::KOMPONEN_TIPE, 'EST', Criteria::ILIKE);
//                    $c_verifikasi->addJoin(KomponenPeer::KOMPONEN_ID, UsulanVerifikasiPeer::SHSD_ID, Criteria::INNER_JOIN);
//                    $c_verifikasi->addAscendingOrderByColumn(KomponenPeer::KOMPONEN_ID);
//                    $data_verifikasi = KomponenPeer::doSelect($c_verifikasi);
//
//                    $kolom_ke = 6;
//                    foreach ($data_verifikasi as $value) {
//                        $objPHPExcel->getActiveSheet()->setCellValue('C' . $kolom_ke, $value->getKomponenId());
//                        $objPHPExcel->getActiveSheet()->setCellValue('D' . $kolom_ke, $value->getKomponenName());
//                        $objPHPExcel->getActiveSheet()->setCellValue('H' . $kolom_ke, $value->getSatuan());
//                        $objPHPExcel->getActiveSheet()->setCellValue('I' . $kolom_ke, $value->getKomponenHarga());
//                        $objPHPExcel->getActiveSheet()->setCellValue('J' . $kolom_ke, $value->getUsulanSkpd());
//                        $objPHPExcel->getActiveSheet()->setCellValue('L' . $kolom_ke, str_replace(',', '', $value->getRekening()));
//                        if ($value->getKomponenNonPajak() == true) {
//                            $objPHPExcel->getActiveSheet()->setCellValue('N' . $kolom_ke, 0);
//                        } else {
//                            $objPHPExcel->getActiveSheet()->setCellValue('N' . $kolom_ke, 10);
//                        }
//
//                        $c_usulan_ssh = new Criteria();
//                        $c_usulan_ssh->add(UsulanSSHPeer::SHSD_ID, $value->getKomponenId());
//                        $data_usulan_ssh = UsulanSSHPeer::doSelectOne($c_usulan_ssh);
//
//                        if ($data_usulan_ssh) {
//                            $objPHPExcel->getActiveSheet()->setCellValue('P' . $kolom_ke, $data_usulan_ssh->getNamaSebelum());
//                            $objPHPExcel->getActiveSheet()->setCellValue('Q' . $kolom_ke, $data_usulan_ssh->getHargaSebelum());
//                            $objPHPExcel->getActiveSheet()->setCellValue('R' . $kolom_ke, $data_usulan_ssh->getAlasanPerubahanHarga());
//                        }
//                        $kolom_ke++;
//                    }
                    $c_verifikasi = new Criteria();
                    $c_verifikasi->add(UsulanVerifikasiPeer::TIPE, 'SSH', Criteria::ALT_NOT_EQUAL);
                    $c_verifikasi->addAscendingOrderByColumn(UsulanVerifikasiPeer::SHSD_ID);
                    $data_verifikasi = UsulanVerifikasiPeer::doSelect($c_verifikasi);

                    $kolom_ke = 6;
                    foreach ($data_verifikasi as $value) {
                        $c_usulan_ssh = new Criteria();
                        $c_usulan_ssh->add(UsulanSSHPeer::ID_USULAN, $value->getIdUsulan());
                        $data_usulan_ssh = UsulanSSHPeer::doSelectOne($c_usulan_ssh);

                        $c_dinas_usulan_ssh = new Criteria();
                        $c_dinas_usulan_ssh->add(UnitKerjaPeer::UNIT_ID, $value->getUnitId());
                        $data_dinas_usulan_ssh = UnitKerjaPeer::doSelectOne($c_dinas_usulan_ssh);

                        if ($data_usulan_ssh) {
                            $objPHPExcel->getActiveSheet()->setCellValue('P' . $kolom_ke, $data_usulan_ssh->getNamaSebelum());
                            $objPHPExcel->getActiveSheet()->setCellValue('Q' . $kolom_ke, $data_usulan_ssh->getHargaSebelum());
                            $objPHPExcel->getActiveSheet()->setCellValue('R' . $kolom_ke, $data_usulan_ssh->getAlasanPerubahanHarga());
                        }

                        $objPHPExcel->getActiveSheet()->setCellValue('C' . $kolom_ke, $value->getShsdId());

                        $c_data_shsd = new Criteria();
                        $c_data_shsd->add(KomponenAllPeer::KOMPONEN_ID, $value->getShsdId());
                        $data_shsd = KomponenAllPeer::doSelectOne($c_data_shsd);
                        if ($data_shsd) {
                            $objPHPExcel->getActiveSheet()->setCellValue('D' . $kolom_ke, $data_shsd->getNamaDasar());
                        } else {
                            $objPHPExcel->getActiveSheet()->setCellValue('D' . $kolom_ke, $value->getShsdName());
                        }

                        $objPHPExcel->getActiveSheet()->setCellValue('E' . $kolom_ke, $value->getMerek());
                        $objPHPExcel->getActiveSheet()->setCellValue('F' . $kolom_ke, $value->getSpec());
                        $objPHPExcel->getActiveSheet()->setCellValue('G' . $kolom_ke, $value->getHiddenSpec());
                        $objPHPExcel->getActiveSheet()->setCellValue('H' . $kolom_ke, $value->getSatuan());
                        $objPHPExcel->getActiveSheet()->setCellValue('I' . $kolom_ke, $value->getHarga());
                        $objPHPExcel->getActiveSheet()->setCellValue('J' . $kolom_ke, $data_dinas_usulan_ssh->getUnitName());
                        $objPHPExcel->getActiveSheet()->setCellValue('L' . $kolom_ke, $value->getRekening());
                        $objPHPExcel->getActiveSheet()->setCellValue('N' . $kolom_ke, $value->getPajak());
                        $kolom_ke++;
                    }
                }
            }

            $objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename=export_ssh_' . date('d-m-Y') . '.xls');
            header('Cache-Control: max-age=0');
            $objWriter->save('php://output');
            exit();
        } catch (Exception $exc) {
            echo $exc->getMessage();
            exit();
        }
    }

    //    ticket #9 - print semi buku
    //    ticket #19 - print semi buku per hari
    function executePrintSemiBukuPerHari() {
        try {
            $array_sheet = array('1.1.7','1.3.2.01', '1.3.2.02', '1.3.2.03', '1.3.2.04', '1.3.2.05', '1.3.2.06', '1.3.2.07', '1.3.2.08', '1.3.2.10', '1.3.2.15', '1.3.2.18', '1.3.2.19', '1.3.5.01', '1.3.5.02', '1.3.5.03', '1.3.5.05', '1.5.3.01', '2.1.1.01', '2.1.1.02', '2.1.1.03', 'EST');
            $objPHPExcel = new sfPhpExcel();

            for ($sheet_ke = 0; $sheet_ke < count($array_sheet); $sheet_ke++) {
                if ($array_sheet[$sheet_ke] <> 'EST') {
                    $c_kategory_shsd = new Criteria();
                    $c_kategory_shsd->add(KategoriShsdPeer::KATEGORI_SHSD_ID, $array_sheet[$sheet_ke], Criteria::EQUAL);
                    $data_kategori = KategoriShsdPeer::doSelectOne($c_kategory_shsd);
                }

                if ($sheet_ke > 0) {
                    $objPHPExcel->createSheet($sheet_ke);
                }

                $objPHPExcel->setActiveSheetIndex($sheet_ke);
                if ($array_sheet[$sheet_ke] <> 'EST') {
                    $objPHPExcel->getActiveSheet()->setTitle($array_sheet[$sheet_ke] . ' ' . ucwords(strtolower($data_kategori->getKategoriShsdName())));
                } else {
                    $objPHPExcel->getActiveSheet()->setTitle($array_sheet[$sheet_ke] . ' ' . ucwords('Estimasi'));
                }
                $objPHPExcel->getActiveSheet()->setCellValue('C5', 'No');
                $objPHPExcel->getActiveSheet()->setCellValue('D5', 'Nama Dasar');
                $objPHPExcel->getActiveSheet()->setCellValue('E5', 'Merk');
                $objPHPExcel->getActiveSheet()->setCellValue('F5', 'Spesifikasi');
                $objPHPExcel->getActiveSheet()->setCellValue('G5', 'Hidden Spec');
                $objPHPExcel->getActiveSheet()->setCellValue('H5', 'Satuan');
                $objPHPExcel->getActiveSheet()->setCellValue('I5', 'Harga Satuan');
                $objPHPExcel->getActiveSheet()->setCellValue('J5', 'Usulan SKPD');
                $objPHPExcel->getActiveSheet()->setCellValue('L5', 'Kode Rekening');
                $objPHPExcel->getActiveSheet()->setCellValue('N5', 'Pajak');
                $objPHPExcel->getActiveSheet()->setCellValue('P5', 'Tahap');

                $objPHPExcel->getActiveSheet()->setCellValue('Q5', 'Nama Sebelum');
                $objPHPExcel->getActiveSheet()->setCellValue('R5', 'Harga Sebelum');
                $objPHPExcel->getActiveSheet()->setCellValue('S5', 'Alasan Perubahan');

                if ($array_sheet[$sheet_ke] <> 'EST') {
//                    $c_verifikasi = new Criteria();
//                    $c_verifikasi->add(ShsdPeer::SHSD_ID, $array_sheet[$sheet_ke] . '%', Criteria::ILIKE);
//                    $c_verifikasi->addAnd(UsulanVerifikasiPeer::CREATED_AT, date('Y-m-d 00:00:00'), Criteria::GREATER_THAN);
//                    $c_verifikasi->addAnd(UsulanVerifikasiPeer::CREATED_AT, date('Y-m-d 23:59:59'), Criteria::LESS_THAN);
//                    $c_verifikasi->addJoin(ShsdPeer::SHSD_ID, UsulanVerifikasiPeer::SHSD_ID, Criteria::INNER_JOIN);
//                    $c_verifikasi->addAscendingOrderByColumn(ShsdPeer::SHSD_ID);
//                    $data_verifikasi = ShsdPeer::doSelect($c_verifikasi);
//
//                    $kolom_ke = 6;
//                    foreach ($data_verifikasi as $value) {
//                        $c_usulan_ssh = new Criteria();
//                        $c_usulan_ssh->add(UsulanSSHPeer::SHSD_ID, $value->getShsdId());
//                        $data_usulan_ssh = UsulanSSHPeer::doSelectOne($c_usulan_ssh);
//
//                        if ($data_usulan_ssh) {
//                            $objPHPExcel->getActiveSheet()->setCellValue('P' . $kolom_ke, $data_usulan_ssh->getNamaSebelum());
//                            $objPHPExcel->getActiveSheet()->setCellValue('Q' . $kolom_ke, $data_usulan_ssh->getHargaSebelum());
//                            $objPHPExcel->getActiveSheet()->setCellValue('R' . $kolom_ke, $data_usulan_ssh->getAlasanPerubahanHarga());
//                        }
//
//                        $objPHPExcel->getActiveSheet()->setCellValue('C' . $kolom_ke, $value->getShsdId());
//                        $objPHPExcel->getActiveSheet()->setCellValue('D' . $kolom_ke, $value->getShsdName());
//                        $objPHPExcel->getActiveSheet()->setCellValue('E' . $kolom_ke, $value->getShsdMerk());
//                        $objPHPExcel->getActiveSheet()->setCellValue('F' . $kolom_ke, $value->getSpec());
//                        $objPHPExcel->getActiveSheet()->setCellValue('G' . $kolom_ke, $value->getHiddenSpec());
//                        $objPHPExcel->getActiveSheet()->setCellValue('H' . $kolom_ke, $value->getSatuan());
//                        $objPHPExcel->getActiveSheet()->setCellValue('I' . $kolom_ke, $value->getShsdHarga());
//                        $objPHPExcel->getActiveSheet()->setCellValue('J' . $kolom_ke, $value->getUsulanSkpd());
//                        $objPHPExcel->getActiveSheet()->setCellValue('L' . $kolom_ke, $value->getRekeningCode());
//                        if ($value->getNonPajak() == true) {
//                            $objPHPExcel->getActiveSheet()->setCellValue('N' . $kolom_ke, 0);
//                        } else {
//                            $objPHPExcel->getActiveSheet()->setCellValue('N' . $kolom_ke, 10);
//                        }
//                        $kolom_ke++;
//                    }
                    $c_verifikasi = new Criteria();
                    $c_verifikasi->add(UsulanVerifikasiPeer::SHSD_ID, $array_sheet[$sheet_ke] . '%', Criteria::ILIKE);
                    $c_verifikasi->addAnd(UsulanVerifikasiPeer::CREATED_AT, date('Y-m-d 00:00:00'), Criteria::GREATER_THAN);
                    $c_verifikasi->addAnd(UsulanVerifikasiPeer::CREATED_AT, date('Y-m-d 23:59:59'), Criteria::LESS_THAN);
                    $c_verifikasi->addAscendingOrderByColumn(UsulanVerifikasiPeer::SHSD_ID);
                    $data_verifikasi = UsulanVerifikasiPeer::doSelect($c_verifikasi);

                    $kolom_ke = 6;
                    foreach ($data_verifikasi as $value) {
                        $c_usulan_ssh = new Criteria();
                        $c_usulan_ssh->add(UsulanSSHPeer::ID_USULAN, $value->getIdUsulan());
                        $data_usulan_ssh = UsulanSSHPeer::doSelectOne($c_usulan_ssh);

                        $c_dinas_usulan_ssh = new Criteria();
                        $c_dinas_usulan_ssh->add(UnitKerjaPeer::UNIT_ID, $value->getUnitId());
                        $data_dinas_usulan_ssh = UnitKerjaPeer::doSelectOne($c_dinas_usulan_ssh);

                        if ($data_usulan_ssh) {                           

                            $objPHPExcel->getActiveSheet()->setCellValue('Q' . $kolom_ke, $data_usulan_ssh->getNamaSebelum());
                            $objPHPExcel->getActiveSheet()->setCellValue('R' . $kolom_ke, $data_usulan_ssh->getHargaSebelum());
                            $objPHPExcel->getActiveSheet()->setCellValue('S' . $kolom_ke, $data_usulan_ssh->getAlasanPerubahanHarga());
                        }

                        $objPHPExcel->getActiveSheet()->setCellValue('C' . $kolom_ke, $value->getShsdId());

                        $c_data_shsd = new Criteria();
                        $c_data_shsd->add(KomponenAllPeer::KOMPONEN_ID, $value->getShsdId());
                        $data_shsd = KomponenAllPeer::doSelectOne($c_data_shsd);
                        if ($data_shsd) {

                            $objPHPExcel->getActiveSheet()->setCellValue('D' . $kolom_ke, $data_shsd->getNamaDasar());

                            $c_tahap_usulan_ssh = new Criteria();
                            $c_tahap_usulan_ssh->add(MasterTahapPeer::TAHAP_ID,$data_shsd->getTahapId());
                            $data_tahap_usulan_ssh = MasterTahapPeer::doSelectOne($c_tahap_usulan_ssh);
                            if($data_tahap_usulan_ssh)
                            {
                                $objPHPExcel->getActiveSheet()->setCellValue('P' . $kolom_ke, $data_tahap_usulan_ssh->getTahapName());
                            }
                        } else {
                            $objPHPExcel->getActiveSheet()->setCellValue('D' . $kolom_ke, $value->getShsdName());
                        }

                        $objPHPExcel->getActiveSheet()->setCellValue('E' . $kolom_ke, $value->getMerek());
                        $objPHPExcel->getActiveSheet()->setCellValue('F' . $kolom_ke, $value->getSpec());
                        $objPHPExcel->getActiveSheet()->setCellValue('G' . $kolom_ke, $value->getHiddenSpec());
                        $objPHPExcel->getActiveSheet()->setCellValue('H' . $kolom_ke, $value->getSatuan());
                        $objPHPExcel->getActiveSheet()->setCellValue('I' . $kolom_ke, $value->getHarga());
                        $objPHPExcel->getActiveSheet()->setCellValue('J' . $kolom_ke, $data_dinas_usulan_ssh->getUnitName());
                        $objPHPExcel->getActiveSheet()->setCellValue('L' . $kolom_ke, $value->getRekening());
                        $objPHPExcel->getActiveSheet()->setCellValue('N' . $kolom_ke, $value->getPajak());
                        $kolom_ke++;
                    }
                } else {
//                    $c_verifikasi = new Criteria();
//                    $c_verifikasi->add(KomponenPeer::KOMPONEN_TIPE, 'EST', Criteria::ILIKE);
//                    $c_verifikasi->addAnd(UsulanVerifikasiPeer::CREATED_AT, date('Y-m-d 00:00:00'), Criteria::GREATER_THAN);
//                    $c_verifikasi->addAnd(UsulanVerifikasiPeer::CREATED_AT, date('Y-m-d 23:59:59'), Criteria::LESS_THAN);
//                    $c_verifikasi->addJoin(KomponenPeer::KOMPONEN_ID, UsulanVerifikasiPeer::SHSD_ID, Criteria::INNER_JOIN);
//                    $c_verifikasi->addAscendingOrderByColumn(KomponenPeer::KOMPONEN_ID);
//                    $data_verifikasi = KomponenPeer::doSelect($c_verifikasi);
//
//                    $kolom_ke = 6;
//                    foreach ($data_verifikasi as $value) {
//                        $c_usulan_ssh = new Criteria();
//                        $c_usulan_ssh->add(UsulanSSHPeer::SHSD_ID, $value->getKomponenId());
//                        $data_usulan_ssh = UsulanSSHPeer::doSelectOne($c_usulan_ssh);
//
//                        if ($data_usulan_ssh) {
//                            $objPHPExcel->getActiveSheet()->setCellValue('P' . $kolom_ke, $data_usulan_ssh->getNamaSebelum());
//                            $objPHPExcel->getActiveSheet()->setCellValue('Q' . $kolom_ke, $data_usulan_ssh->getHargaSebelum());
//                            $objPHPExcel->getActiveSheet()->setCellValue('R' . $kolom_ke, $data_usulan_ssh->getAlasanPerubahanHarga());
//                        }
//
//                        $objPHPExcel->getActiveSheet()->setCellValue('C' . $kolom_ke, $value->getKomponenId());
//                        $objPHPExcel->getActiveSheet()->setCellValue('D' . $kolom_ke, $value->getKomponenName());
//                        $objPHPExcel->getActiveSheet()->setCellValue('H' . $kolom_ke, $value->getSatuan());
//                        $objPHPExcel->getActiveSheet()->setCellValue('I' . $kolom_ke, $value->getKomponenHarga());
//                        $objPHPExcel->getActiveSheet()->setCellValue('J' . $kolom_ke, $value->getUsulanSkpd());
//                        $objPHPExcel->getActiveSheet()->setCellValue('L' . $kolom_ke, str_replace(',', '', $value->getRekening()));
//                        if ($value->getKomponenNonPajak() == true) {
//                            $objPHPExcel->getActiveSheet()->setCellValue('N' . $kolom_ke, 0);
//                        } else {
//                            $objPHPExcel->getActiveSheet()->setCellValue('N' . $kolom_ke, 10);
//                        }
//                        $kolom_ke++;
//                    }

                    $c_verifikasi = new Criteria();
                    $c_verifikasi->add(UsulanVerifikasiPeer::TIPE, 'SSH', Criteria::ALT_NOT_EQUAL);
                    $c_verifikasi->addAnd(UsulanVerifikasiPeer::CREATED_AT, date('Y-m-d 00:00:00'), Criteria::GREATER_THAN);
                    $c_verifikasi->addAnd(UsulanVerifikasiPeer::CREATED_AT, date('Y-m-d 23:59:59'), Criteria::LESS_THAN);
                    $c_verifikasi->addAscendingOrderByColumn(UsulanVerifikasiPeer::SHSD_ID);
                    $data_verifikasi = UsulanVerifikasiPeer::doSelect($c_verifikasi);

                    $kolom_ke = 6;
                    foreach ($data_verifikasi as $value) {
                        $c_usulan_ssh = new Criteria();
                        $c_usulan_ssh->add(UsulanSSHPeer::ID_USULAN, $value->getIdUsulan());
                        $data_usulan_ssh = UsulanSSHPeer::doSelectOne($c_usulan_ssh);

                        $c_dinas_usulan_ssh = new Criteria();
                        $c_dinas_usulan_ssh->add(UnitKerjaPeer::UNIT_ID, $value->getUnitId());
                        $data_dinas_usulan_ssh = UnitKerjaPeer::doSelectOne($c_dinas_usulan_ssh);

                        if ($data_usulan_ssh) {
                            $objPHPExcel->getActiveSheet()->setCellValue('Q' . $kolom_ke, $data_usulan_ssh->getNamaSebelum());
                            $objPHPExcel->getActiveSheet()->setCellValue('R' . $kolom_ke, $data_usulan_ssh->getHargaSebelum());
                            $objPHPExcel->getActiveSheet()->setCellValue('S' . $kolom_ke, $data_usulan_ssh->getAlasanPerubahanHarga());
                        }

                        $objPHPExcel->getActiveSheet()->setCellValue('C' . $kolom_ke, $value->getShsdId());

                        $c_data_shsd = new Criteria();
                        $c_data_shsd->add(KomponenAllPeer::KOMPONEN_ID, $value->getShsdId());
                        $data_shsd = KomponenAllPeer::doSelectOne($c_data_shsd);
                        if ($data_shsd) {
                            $objPHPExcel->getActiveSheet()->setCellValue('D' . $kolom_ke, $data_shsd->getNamaDasar());

                            $c_tahap_usulan_ssh = new Criteria();
                            $c_tahap_usulan_ssh->add(MasterTahapPeer::TAHAP_ID,$data_shsd->getTahapId());
                            $data_tahap_usulan_ssh = MasterTahapPeer::doSelectOne($c_tahap_usulan_ssh);
                            if($data_tahap_usulan_ssh)
                            {
                                $objPHPExcel->getActiveSheet()->setCellValue('P' . $kolom_ke, $data_tahap_usulan_ssh->getTahapName());
                            }

                        } else {
                            $objPHPExcel->getActiveSheet()->setCellValue('D' . $kolom_ke, $value->getShsdName());
                        }

                        $objPHPExcel->getActiveSheet()->setCellValue('E' . $kolom_ke, $value->getMerek());
                        $objPHPExcel->getActiveSheet()->setCellValue('F' . $kolom_ke, $value->getSpec());
                        $objPHPExcel->getActiveSheet()->setCellValue('G' . $kolom_ke, $value->getHiddenSpec());
                        $objPHPExcel->getActiveSheet()->setCellValue('H' . $kolom_ke, $value->getSatuan());
                        $objPHPExcel->getActiveSheet()->setCellValue('I' . $kolom_ke, $value->getHarga());
                        $objPHPExcel->getActiveSheet()->setCellValue('J' . $kolom_ke, $data_dinas_usulan_ssh->getUnitName());
                        $objPHPExcel->getActiveSheet()->setCellValue('L' . $kolom_ke, $value->getRekening());
                        $objPHPExcel->getActiveSheet()->setCellValue('N' . $kolom_ke, $value->getPajak());
                        $kolom_ke++;
                    }
                }
            }

            $objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename=export_ssh_per_hari_' . date('d-m-Y') . '.xls');
            header('Cache-Control: max-age=0');
            $objWriter->save('php://output');
            exit();
        } catch (Exception $exc) {
            echo $exc->getMessage();
            exit();
        }
    }

    //    ticket #19 - print semi buku per hari
    //ticket #3 - list token
    public function executeUsulantoken() {
        $this->processFiltersusulantoken();
        $this->filters = $this->getUser()->getAttributeHolder()->getAll('sf_admin/usulantoken/filters');

        $pagers = new sfPropelPager('UsulanToken', 25);
        $c = new Criteria();
        $c->add(UsulanTokenPeer::STATUS_USED, FALSE, Criteria::EQUAL);
        $c->addDescendingOrderByColumn(UsulanTokenPeer::REQUIRED_AT);
        $c->addDescendingOrderByColumn(UsulanTokenPeer::EXPIRED_AT);
        $this->addFiltersCriteriausulantoken($c);

        $pagers->setCriteria($c);
        $pagers->setPage($this->getRequestParameter('page', 1));
        $pagers->init();

        $this->pager = $pagers;
    }

    //ticket #3 - list token
    //ticket #3 - tambah token
    public function executeUsulantambahtoken() {
        $c = new Criteria();
//        $c->add(UnitKerjaPeer::UNIT_ID, '9999', Criteria::NOT_EQUAL);
        $c->addAscendingOrderByColumn(UnitKerjaPeer::UNIT_NAME);
        $list_skpd = UnitKerjaPeer::doSelect($c);
        $this->list_skpd = $list_skpd;

        $c = new Criteria();
        $c->add(MasterPenyeliaPeer::AKTIF, TRUE, Criteria::EQUAL);
        $c->addAscendingOrderByColumn(MasterPenyeliaPeer::USERNAME);
        $list_penyelia = MasterPenyeliaPeer::doSelect($c);
        $this->list_penyelia = $list_penyelia;
    }

    //ticket #3 - tambah token
    //ticket #3 - filter token
    protected function processFiltersusulantoken() {
        if ($this->getRequest()->hasParameter('filter')) {
            $filters = $this->getRequestParameter('filters');
            $this->getUser()->getAttributeHolder()->removeNamespace('sf_admin/usulantoken/filters');
            $this->getUser()->getAttributeHolder()->add($filters, 'sf_admin/usulantoken/filters');
        }
    }

    //ticket #3 - filter token
    //ticket #3 - filter token
    protected function addFiltersCriteriausulantoken($c) {
        if (isset($this->filters['select'])) {
            if ($this->filters['select'] == 1) {
                if (isset($this->filters['penyelia_is_empty'])) {
                    $criterion = $c->getNewCriterion(UsulanTokenPeer::PENYELIA, '');
                    $criterion->addOr($c->getNewCriterion(UsulanTokenPeer::PENYELIA, null, Criteria::ISNULL));
                    $c->add($criterion);
                } else if (isset($this->filters['komponen']) && $this->filters['komponen'] !== '') {
                    $kata = '%' . $this->filters['komponen'] . '%';
                    $c->add(UsulanTokenPeer::PENYELIA, strtr($kata, '*', '%'), Criteria::ILIKE);
                }
            } else {
                if (isset($this->filters['komponen_is_empty'])) {
                    echo 'is empty';
                    $criterion = $c->getNewCriterion(UsulanTokenPeer::PENYELIA, '');
                    $criterion->addOr($c->getNewCriterion(UsulanTokenPeer::PENYELIA, null, Criteria::ISNULL));
                    $c->add($criterion);
                }
            }
        }
    }

    //ticket #3 - filter token
    //ticket #3 - save token
    function executeSavetoken() {
        if ($this->getRequestParameter('jumlah') > 0) {

            $nomor_surat = str_replace(' ', '', trim($this->getRequestParameter('nomor_surat')));

            $query_cek_nomor_surat = "select count(*) as total "
                    . "from " . sfConfig::get('app_default_schema') . ".usulan_token "
                    . "where replace(trim(nomor_surat),' ','') ilike '$nomor_surat' ";
            $con = Propel::getConnection();
            $stmt_cek_nomor_surat = $con->prepareStatement($query_cek_nomor_surat);
            $rs1 = $stmt_cek_nomor_surat->executeQuery();
            while ($rs1->next()) {
                $total_cek_nomor = $rs1->getString('total');
            }

            if ($total_cek_nomor > 0) {
                $this->setFlash('gagal', 'Gagal karena Nomor Surat telah diisikan');
                return $this->redirect('usulan_ssh/usulantoken');
            }

            try {
                $sekarang = date('Y-m-d H:i:s');

                $besok = new DateTime($sekarang);
                $besok->modify('+ 3 day');
                $tanggal_expired = $besok->format('Y-m-d H:i:s');

                $c_penyelia = new Criteria();
                $c_penyelia->add(MasterPenyeliaPeer::ID_PENYELIA, $this->getRequestParameter('penyelia'), Criteria::EQUAL);
                $data_penyelia = MasterPenyeliaPeer::doSelectOne($c_penyelia);

                $c_skpd = new Criteria();
                $c_skpd->add(UnitKerjaPeer::UNIT_ID, $this->getRequestParameter('skpd'), Criteria::EQUAL);
                $data_skpd = UnitKerjaPeer::doSelectOne($c_skpd);

                do {
                    $token_dapet = $this->mr_random_string();

                    $c_token = new Criteria();
                    $c_token->add(UsulanTokenPeer::TOKEN, $token_dapet);
                    $count_token = UsulanTokenPeer::doCount($c_token);
                } while ($count_token > 0);

                $baru_token = new UsulanToken();
                $baru_token->setPenyelia($data_penyelia->getUsername());
                $baru_token->setRequiredAt($sekarang);
                $baru_token->setExpiredAt($tanggal_expired);
                $baru_token->setUnitId($data_skpd->getUnitId());
                $baru_token->setCreatedBy($this->getRequestParameter('verifikator'));
                $baru_token->setJumlahSsh($this->getRequestParameter('jumlah'));
//                ticket #11 - save nomor surat SPJM 
                $baru_token->setNomorSurat(trim($this->getRequestParameter('nomor_surat')));
//                ticket #11 - save nomor surat SPJM 
                $baru_token->setToken($token_dapet);
                $baru_token->save();

                $c_usulan_dinas = new Criteria();
                $c_usulan_dinas->add(UsulanDinasPeer::SKPD, $data_skpd->getUnitId(), Criteria::EQUAL);
                $c_usulan_dinas->add(UsulanDinasPeer::NOMOR_SURAT, trim($this->getRequestParameter('nomor_surat')), Criteria::EQUAL);
                $ada_usulan_dinas = UsulanDinasPeer::doCount($c_usulan_dinas);

                if ($ada_usulan_dinas > 0) {
//                    $data_usulan_dinas = UsulanDinasPeer::doSelectOne($c_usulan_dinas);
//                    $data_usulan_dinas->setUpdatedAt($sekarang);
////                    $data_usulan_dinas->setStatusVerifikasi(true);
//                    $data_usulan_dinas->save();
                }

                //fungsi user_log
                $namaprofil = $this->getUser()->getNamaUser();
                $username = $this->getUser()->getNamaLogin();
                budgetLogger::log($username . ' merequest token dengan kode : ' . $token_dapet . ' untuk penyelia : ' . $data_penyelia->getUsername() . ' Dinas : ' . $data_skpd->getUnitName() . ' untuk SSH sebanyak ' . $this->getRequestParameter('jumlah'));
                //fungsi user_log

                $this->setFlash('berhasil', 'Berhasil membuat token dengan kode : ' . $token_dapet . ' untuk penyelia : ' . $data_penyelia->getUsername() . ' Dinas : ' . $data_skpd->getUnitName() . ' untuk SSH sebanyak ' . $this->getRequestParameter('jumlah'));
            } catch (Exception $exc) {
                $this->setFlash('gagal', 'Gagal karena : ' . $exc->getMessage());
            }
        } else {
            $this->setFlash('gagal', 'Inputkan jumlah SSH yang tertulis dalam SPJM asli.');
        }
        return $this->redirect('usulan_ssh/usulantoken');
    }

    //ticket #3 - save token
    //ticket #3 - generate token
    function mr_random_string() {
        $length = rand(4, 9);
        // kumpulan random string yang akan diambil
        $string = '0987654321QWERTYUIOPASDFGHJKLZXCVBNM';
        // digunakan untuk fungsi mt_rand
        $str_len = strlen($string) - 1; // minus 1 karena urutan dimulai dari 0
        // variabel penyimpan output
        $out = '';
        // loop sebanyak parameter yang diinputkan (default => 8)
        for ($i = 0; $i < $length; $i++) {
            // posisi acak single string yang akan diambil
            // dari 0 sampai banyaknya string
            $pos = mt_rand(0, $str_len);
            // simpan pada output
            $out .= $string[$pos]; // cukup ambil satu string
        }
        // kembalkan output
        return $out;
    }

    //ticket #3 - generate token
    //ticket #4 - proses upload SPJM + usulan SSH
    function executeSaveusulan() {
        set_time_limit(0);

        $sekarang = date('Y-m-d H:i:s');

//        ambil parameter
        $tanggal_spjm = $this->getRequestParameter('tanggal_spjm');
        $penyelia = $this->getRequestParameter('nama_penyelia');
        $skpd = $this->getRequestParameter('skpd');
        $tipe_usulan = $this->getRequestParameter('kategori_usulan');
        $jenis_usulan = $this->getRequestParameter('jenis_usulan');
        $token = $this->getRequestParameter('token');
//        hanya dilogin Admin
        $spjm = $this->getRequestParameter('spjm');
        $jumlah_ssh = $this->getRequestParameter('jumlah_ssh');
//        hanya dilogin Admin
        $jumlah_dukungan = $this->getRequestParameter('jumlah_dukungan');
//        ambil parameter
//        kalo admin dibuatin token dulu
        
        if ($penyelia == 'admin') {
            $besok = new DateTime($sekarang);
            $besok->modify('+ 3 day');
            $tanggal_expired = $besok->format('Y-m-d H:i:s');

            do {
                $token_dapet = $this->mr_random_string();

                $c_token = new Criteria();
                $c_token->add(UsulanTokenPeer::TOKEN, $token_dapet);
                $count_token = UsulanTokenPeer::doCount($c_token);
            } while ($count_token > 0);

            $baru_token = new UsulanToken();
            $baru_token->setPenyelia($penyelia);
            $baru_token->setRequiredAt($sekarang);
            $baru_token->setExpiredAt($tanggal_expired);
            $baru_token->setUnitId($skpd);
            $baru_token->setCreatedBy($penyelia);
            $baru_token->setJumlahSsh($jumlah_ssh);
            $baru_token->setNomorSurat(trim($spjm));
            $baru_token->setToken($token_dapet);
            $baru_token->save();

            $token = $token_dapet;
        }
//        kalo admin dibuatin token dulu
//        cek ada token
        $count_token = new Criteria();
        $count_token->add(UsulanTokenPeer::TOKEN, $token, Criteria::EQUAL);
        $count_token->addAnd(UsulanTokenPeer::STATUS_USED, FALSE, Criteria::EQUAL);
        $count_ada_token = UsulanTokenPeer::doCount($count_token);
        if ($count_ada_token <> 1) {
            $this->setFlash('gagal', 'Token Anda Salah');
            return $this->redirect('usulan_ssh/usulansshbaru');
        }
        $c_token = new Criteria();
        $c_token->add(UsulanTokenPeer::TOKEN, $token, Criteria::EQUAL);
        $c_token->addAnd(UsulanTokenPeer::STATUS_USED, FALSE, Criteria::EQUAL);
        $data_token = UsulanTokenPeer::doSelectOne($c_token);
        $nomor_surat = $data_token->getNomorSurat();
//        cek ada token
//        kalo $penyelia == 'admin' gak dicek
        if ($penyelia <> 'admin') {
//        cek nomor surat dengan usulan dinas
            $c_update_usulandinas = new Criteria();
            $c_update_usulandinas->add(UsulanDinasPeer::NOMOR_SURAT, $nomor_surat, Criteria::EQUAL);
            $c_update_usulandinas->add(UsulanDinasPeer::STATUS_HAPUS, FALSE, Criteria::EQUAL);
            $c_update_usulandinas->add(UsulanDinasPeer::STATUS_TOLAK, FALSE, Criteria::EQUAL);
            $c_update_usulandinas->add(UsulanDinasPeer::STATUS_VERIFIKASI, FALSE, Criteria::EQUAL);
            $ada_usulan_dinas = UsulanDinasPeer::doCount($c_update_usulandinas);
            if ($ada_usulan_dinas == 0) {
                $this->setFlash('gagal', 'Nomor SPJM Usulan Anda tidak ada dalam Usulan Dinas');
                return $this->redirect('usulan_ssh/usulansshbaru');
            }
//        cek nomor surat dengan usulan dinas
        }
//        kalo $penyelia == 'admin' gak dicek
        if ($jumlah_dukungan == '' || $jumlah_dukungan == 0) {
            $this->setFlash('gagal', 'Jumlah dukungan harus ada.');
            return $this->redirect('usulan_ssh/usulansshbaru');
            //        cek jumlah dukungan
        } else if ($skpd <> $data_token->getUnitId()) {
            $this->setFlash('gagal', 'Token Anda Salah karena SKPD berbeda');
            return $this->redirect('usulan_ssh/usulansshbaru');
            //        cek SKPD
        } else if ($penyelia <> $data_token->getPenyelia()) {
            $this->setFlash('gagal', 'Token Anda Salah karena Penyelia berbeda');
            return $this->redirect('usulan_ssh/usulansshbaru');
            //        cek penyelia
        }

//        upload file excel 
        $array_file = explode('.', $this->getRequest()->getFileName('file'));
        $fileName = 'file_usulan_' . $this->getRequestParameter('nama_penyelia') . '_' . date('Y-m-d_H-i-s') . '.' . $array_file[count($array_file) - 1];
        $this->getRequest()->moveFile('file', sfConfig::get('sf_root_dir') . '/web/uploads/usulan_ssh/' . $fileName);
        $file_path = sfConfig::get('sf_root_dir') . '/web/uploads/usulan_ssh/' . $fileName;
        $saving_filepath = sfConfig::get('app_path_uploadsUsulanSsh') . $fileName;
//        upload file excel 
//        cek data upload
        if (!file_exists($file_path)) {
            $this->setFlash('gagal', 'CEK FILE ' . $file_path);
            return $this->redirect('usulan_ssh/usulansshbaru');
        }

//        cek data upload

        $objReader = new PHPExcel_Reader_Excel5;
        $objPHPExcel = $objReader->load($file_path);

        $objPHPExcel->getActiveSheetIndex(0);
        $objWorksheet = $objPHPExcel->getActiveSheet();

        $highR = $objWorksheet->getHighestRow();

        //        cek jumlah usulan
        $total = $highR - 15;
        if ($data_token->getJumlahSsh() <> $total) {
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/usulan_ssh/' . $fileName);

            $this->setFlash('gagal', 'Token Anda Salah karena Total Usulan berbeda');
            return $this->redirect('usulan_ssh/usulansshbaru');
        }
        //        cek jumlah usulan

        $con = Propel::getConnection();

        $salah = 0;
        $sama = 0;
        $string_salah = '';
        $string_sama = '';

//            cek isian excel
        for ($row = 16; $row <= $highR; $row++) {
             $tipe = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(2, $row)->getValue()));
            $nama = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(3, $row)->getValue()));
            $spec = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(4, $row)->getValue()));    
            $hidden_spec = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(5, $row)->getValue()));          
            $merek = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(6, $row)->getValue()));
            $satuan = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(7, $row)->getValue()));
            $harga = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(8, $row)->getValue()));
            $rekening = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(9, $row)->getValue()));
            $pajak = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(10, $row)->getValue()));
            $keterangan = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(11, $row)->getValue()));
            $kode_barang = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(12, $row)->getValue()));
            $nama_sebelum = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(13, $row)->getValue()));
            $harga_sebelum = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(14, $row)->getValue()));
            $alasan_perubahan = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(15, $row)->getValue()));
            $harga_pendukung1 = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(16, $row)->getValue()));
            $cv1 = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(17, $row)->getValue()));
            $harga_pendukung2 = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(18, $row)->getValue()));
            $cv2 = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(19, $row)->getValue()));
            $harga_pendukung3 = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(20, $row)->getValue()));
            $cv3 = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(21, $row)->getValue()));


//                cek nama
            $array_kode_barang = explode(')', $kode_barang);
            $kode_barang_fresh = str_replace('(', '', $array_kode_barang[0]);
            $c = new Criteria();
            $c->add(KategoriShsdPeer::KATEGORI_SHSD_ID, $kode_barang_fresh);
            if (!KategoriShsdPeer::doSelectOne($c)) {
                $salah++;
                $string_salah = $string_salah . 'salah pengisian kode barang, baris ke ' . $row . '|';
                $kode_barang = 'SALAH';
            }

            if (trim($nama == '')) {
                $salah++;
                $string_salah = $string_salah . 'salah pengisian nama, baris ke ' . $row . '|';
                $nama = 'SALAH';
            }
//                cek nama
//                cek spec
            // if (trim($spec) == '') {
            //     $salah++;
            //     $string_salah = $string_salah . 'salah pengisian spec, baris ke ' . $row . '|';
            //     $spec = 'SALAH';
            // }
//                cek spec
//                cek hidden_spec
            if (trim($hidden_spec) == '') {
                $salah++;
                $string_salah = $string_salah . 'salah pengisian hidden_spec, baris ke ' . $row . '|';
                $hidden_spec = 'SALAH';
            }
//                cek hidden_spec
//                cek merek
            if (trim($merek) == '') {
                $salah++;
                $string_salah = $string_salah . 'salah pengisian hidden_spec, baris ke ' . $row . '|';
                $merek = 'SALAH';
            }
//                cek merek
//                cek satuan
            $c = new Criteria();
            $c->add(SatuanPeer::SATUAN_NAME, $satuan);
            if (!SatuanPeer::doSelectOne($c)) {
                $salah++;
                $string_salah = $string_salah . 'salah pengisian satuan, baris ke ' . $row . '|';
                $satuan = 'SALAH';
            }
//                cek satuan
//                cek keterangan
            if (trim($keterangan) == '') {
                $salah++;
                $string_salah = $string_salah . 'salah pengisian keterangan, baris ke ' . $row . '|';
                $keterangan = 'SALAH';
            }
//                cek keterangan
//                cek rekening
            $array_rekening = explode(')', $rekening);
            $kode_rekening = str_replace('(', '', $array_rekening[0]);

            if (count($array_rekening) == 1 || $rekening == '') {
                $salah++;
                $string_salah = $string_salah . 'salah pemilihan rekening, baris ke ' . $row . '|';
                $rekening = 'SALAH';
            }

            $c = new Criteria();
            $c->add(RekeningPeer::REKENING_CODE, $kode_rekening);
            if (!RekeningPeer::doSelectOne($c)) {
                $salah++;
                $string_salah = $string_salah . 'salah pemilihan rekening (karena rekening sudah tidak ada), baris ke ' . $row . '|';
                $rekening = 'SALAH';
            }
//                cek rekening
//                cek kode_barang
            $array_kode_barang = explode(')', $kode_barang);
            $kode_barang_fresh = str_replace('(', '', $array_kode_barang[0]);

            if (count($array_kode_barang) == 1 || $kode_barang_fresh == '') {
                $salah++;
                $string_salah = $string_salah . 'salah pemilihan kode barang, baris ke ' . $row . '|';
                $kode_barang_fresh = 'SALAH';
            }
//                cek kode_barang
            $array_kode_barang = explode(')', $kode_barang);
            $kode_barang_fresh = str_replace('(', '', $array_kode_barang[0]);
//                cek tipe & kode_barang
            if ($tipe == '' || ($tipe <> 'SSH' && $tipe <> 'EST')) {
                $salah++;
                $string_salah = $string_salah . 'salah pemilihan kode barang dengan tipe, baris ke ' . $row . '|';
                $tipe = 'SALAH';
            } else {
//                if ($tipe == 'SSH' && ($kode_barang_fresh == '23.05.01.01' || $kode_barang_fresh == '23.06.01.01')) {
//                    $salah++;
//                    $string_salah = $string_salah . 'salah pemilihan kode barang dengan tipe, baris ke ' . $row . '|';
//                    $tipe = 'SALAH';
//                } else if ($kode_barang_fresh <> '23.05.01.01' && $kode_barang_fresh <> '23.06.01.01') {
//                    $salah++;
//                    $string_salah = $string_salah . 'salah pemilihan kode barang dengan tipe, baris ke ' . $row . '|';
//                    $tipe = 'SALAH';
//                }
            }
//                cek tipe & kode_barang
//                cek pajak                
            if ($pajak <> '0' && $pajak <> '10') {
                $salah++;
                $string_salah = $string_salah . 'salah pemilihan pajak, baris ke ' . $row . '|';
                $pajak = 'SALAH';
            }
//                cek pajak
//                cek harga
            $array_harga_koma = explode(',', $harga);
            $array_harga_titik = explode('.', $harga);
            if (count($array_harga_koma) > 1 || count($array_harga_titik) > 2 || $harga == '') {
                $salah++;
                $string_salah = $string_salah . 'salah penggunakan tanda koma (seharusnya tanda titik) dan pembilang ribuan tidak menggunakan tanda titik pada kolom harga, baris ke ' . $row . '|';
                $harga = 'SALAH';
            }
//                cek harga
//                cek perubahan harga 
            $array_perubahan_koma = explode(',', $harga_sebelum);
            $array_perubahan_titik = explode('.', $harga_sebelum);
            if ($harga_sebelum > 0) {
                if (count($array_harga_koma) > 1 || count($array_harga_titik) > 2 || $harga_sebelum == '') {
                    $salah++;
                    $string_salah = $string_salah . 'salah penggunakan tanda koma (seharusnya tanda titik) dan pembilang ribuan tidak menggunakan tanda titik pada kolom harga sebelum, baris ke ' . $row . '|';
                    $harga_sebelum = 'SALAH';
                }
                if (trim($alasan_perubahan) == '') {
                    $salah++;
                    $string_salah = $string_salah . 'Silahkan mengisi alasan perubahan harga untuk usulan perubahan harga komponen, baris ke ' . $row . '|';
                    $alasan_perubahan = 'SALAH';
                }
            }
//                cek perubahan harga 
//                cek harga pendukung
            $array_pendukung_koma1 = explode(',', $harga_pendukung1);
            $array_pendukung_titik1 = explode('.', $harga_pendukung1);

            if (count($array_pendukung_koma1) > 1 || count($array_pendukung_titik1) > 2 || $harga_pendukung1 == '') {
                $salah++;
                $string_salah = $string_salah . 'salah penggunakan tanda koma (seharusnya tanda titik) dan pembilang ribuan tidak menggunakan tanda titik pada kolom harga pendukung 1, baris ke ' . $row . '|';
                $harga_pendukung1 = 'SALAH';
            }

            $array_pendukung_koma2 = explode(',', $harga_pendukung2);
            $array_pendukung_titik2 = explode('.', $harga_pendukung2);

            if (count($array_pendukung_koma2) > 1 || count($array_pendukung_titik2) > 2 || $harga_pendukung2 == '') {
                $salah++;
                $string_salah = $string_salah . 'salah penggunakan tanda koma (seharusnya tanda titik) dan pembilang ribuan tidak menggunakan tanda titik pada kolom harga pendukung 2, baris ke ' . $row . '|';
                $harga_pendukung2 = 'SALAH';
            }

            $array_pendukung_koma3 = explode(',', $harga_pendukung3);
            $array_pendukung_titik3 = explode('.', $harga_pendukung3);

            if (count($array_pendukung_koma3) > 1 || count($array_pendukung_titik3) > 2 || $harga_pendukung3 == '') {
                $salah++;
                $string_salah = $string_salah . 'salah penggunakan tanda koma (seharusnya tanda titik) dan pembilang ribuan tidak menggunakan tanda titik pada kolom harga pendukung 3, baris ke ' . $row . '|';
                $harga_pendukung3 = 'SALAH';
            }

            if ($harga_pendukung1 <> $harga && $harga_pendukung2 <> $harga && $harga_pendukung3 <> $harga) {
                $salah++;
                $string_salah = $string_salah . 'harga pendukung berbeda dengan harga usulan dan tanpa disertai alasan perbedaan, baris ke ' . $row . '|';
                $harga_pendukung3 = 'SALAH';
                $harga_pendukung2 = 'SALAH';
                $harga_pendukung1 = 'SALAH';
            }
//                cek harga pendukung            
        }
//            cek isian excel

        if ($salah > 0) {
//                warning kalo salah satu salah
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/usulan_ssh/' . $fileName);

            $this->string_salah = $string_salah;
//                warning kalo salah satu salah                
        } else {
            $con->begin();
            try {
//                data token
                $c_update_token = new Criteria();
                $c_update_token->add(UsulanTokenPeer::TOKEN, $token, Criteria::EQUAL);
                $token_kepake = UsulanTokenPeer::doSelectOne($c_update_token);
                $id_token = $token_kepake->getIdToken();
                $nomor_surat = $token_kepake->getNomorSurat();
//                data token
//                save SPJM
                $insert_spjm = new UsulanSPJM();
                $insert_spjm->setTglSpjm($tanggal_spjm);
                $insert_spjm->setPenyelia($penyelia);
                $insert_spjm->setSkpd($skpd);
                $insert_spjm->setTipeUsulan($tipe_usulan);
                $insert_spjm->setJenisUsulan($jenis_usulan);
                $insert_spjm->setJumlahDukungan($jumlah_dukungan);
                $insert_spjm->setCreatedAt($sekarang);
                $insert_spjm->setUpdatedAt($sekarang);
                $insert_spjm->setStatusVerifikasi(FALSE);
                $insert_spjm->setStatusHapus(FALSE);
                $insert_spjm->setFilepath($saving_filepath);
                $insert_spjm->setIdToken($id_token);
//                ticket #11 - save nomor surat SPJM 
                $insert_spjm->setNomorSurat($nomor_surat);
//                ticket #11 - save nomor surat SPJM 
                $insert_spjm->save();
//                save SPJM
//                update using token
                $query_update_using_token = "update " . sfConfig::get('app_default_schema') . ".usulan_token "
                        . "set status_used = TRUE , used_at = '$sekarang' "
                        . "where id_token = $id_token ";
                $stmt_update_using_token = $con->prepareStatement($query_update_using_token);
                $stmt_update_using_token->executeQuery();
//                update using token       
//                kalo $penyelia=='admin' gak diupdate
                if ($penyelia <> 'admin') {
//                update usulan dinas                
                    $c_update_usulandinas = new Criteria();
                    $c_update_usulandinas->add(UsulanDinasPeer::NOMOR_SURAT, $nomor_surat, Criteria::EQUAL);
                    $c_update_usulandinas->add(UsulanDinasPeer::STATUS_HAPUS, FALSE, Criteria::EQUAL);
                    $usulan_dinas = UsulanDinasPeer::doSelectOne($c_update_usulandinas);
                    $usulan_dinas->setStatusVerifikasi(TRUE);
                    $usulan_dinas->setUpdatedAt($sekarang);
                    $usulan_dinas->save();
//                update usulan dinas                 
                }
//                kalo $penyelia=='admin' gak diupdate
//                fungsi user_log
                $namaprofil = $this->getUser()->getNamaUser();
                $username = $this->getUser()->getNamaLogin();
                budgetLogger::log($username . ' menambah SPJM Dinas ' . $skpd . ' dengan token ' . $token);
//                fungsi user_log

                for ($row = 16; $row <= $highR; $row++) {
                 $tipe = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(2, $row)->getValue()));
                $nama = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(3, $row)->getValue()));
                $spec = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(4, $row)->getValue()));    
                $hidden_spec = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(5, $row)->getValue()));          
                $merek = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(6, $row)->getValue()));
                $satuan = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(7, $row)->getValue()));
                $harga = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(8, $row)->getValue()));
                $rekening = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(9, $row)->getValue()));
                $pajak = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(10, $row)->getValue()));
                $keterangan = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(11, $row)->getValue()));
                $kode_barang = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(12, $row)->getValue()));
                $nama_sebelum = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(13, $row)->getValue()));
                $harga_sebelum = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(14, $row)->getValue()));
                $alasan_perubahan = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(15, $row)->getValue()));
                $harga_pendukung1 = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(16, $row)->getValue()));
                $cv1 = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(17, $row)->getValue()));
                $harga_pendukung2 = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(18, $row)->getValue()));
                $cv2 = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(19, $row)->getValue()));
                $harga_pendukung3 = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(20, $row)->getValue()));
                $cv3 = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(21, $row)->getValue()));



                    if ($harga_sebelum > 0 && $harga_sebelum <> '' && $nama_sebelum <> '' && $alasan_perubahan <> '') {
                        $jenis_usulan_item = 'PERUBAHAN HARGA';
                    } else {
                        $jenis_usulan_item = 'BARU';
                    }

//                    if ($jenis_usulan == 'PERUBAHAN HARGA') {
//                        $jenis_usulan_item = 'PERUBAHAN HARGA';
//                    } else {
//                        $jenis_usulan_item = 'BARU';
//                    }
                    //                cek tabel usulan
                    if ($jenis_usulan_item == 'BARU') {
                        // jika 
                        // (
                        // atau (nama ilike nama(spasi)spec) 
                        // atau (nama spec ilike nama) 
                        // atau (nama ilike nama dan spec ilike spec) 
                        // atau (nama ilike nama dan spec ilike hidden_spec)
                        // atau (nama ilike nama dan hidden_spec ilike spec)
                        // atau (nama ilike nama dan hidden_spec ilike hidden_spec)
                        // )
                        // dan (harga equal harga)
                        // dan (satuan ilike satuan)

                        $query_cek_usulan = "select count(*) as total "
                                . "from " . sfConfig::get('app_default_schema') . ".usulan_ssh "
                                . "where status_hapus = false "
                                . "and ("
                                . "trim(nama) ilike '" . $nama . " " . $spec . "' "
                                . "or trim(nama) ilike '" . $nama . "' "
                                . "or trim(nama)||' '||trim(spec) ilike '" . $nama . "' "
                                . "or (trim(nama) ilike '$nama' and trim(spec) ilike '$spec') "
                                . "or (trim(nama) ilike '$nama' and trim(hidden_spec) ilike '$spec') "
                                . "or (trim(nama) ilike '$nama' and trim(hidden_spec) ilike '$hidden_spec') "
                                . "or (trim(nama) ilike '$nama' and trim(spec) ilike '$hidden_spec')"
                                . ") "
                                . "and harga = $harga "
                                . "and satuan ilike '$satuan' ";
                        // end jika
                    } else if ($jenis_usulan_item == 'PERUBAHAN HARGA') {
                        // jika 
                        // (
                        // atau (nama ilike nama(spasi)spec) 
                        // atau (nama spec ilike nama)  
                        // atau (nama ilike nama dan spec ilike spec) 
                        // atau (nama ilike nama dan spec ilike hidden_spec)
                        // atau (nama ilike nama dan hidden_spec ilike spec)
                        // atau (nama ilike nama dan hidden_spec ilike hidden_spec)
                        // )
                        // dan (satuan ilike satuan)

                        $query_cek_usulan = "select count(*) as total "
                                . "from " . sfConfig::get('app_default_schema') . ".usulan_ssh "
                                . "where status_hapus = false "
                                . "and ("
                                . "trim(nama) ilike '" . $nama . " " . $spec . "' "
                                . "or trim(nama) ilike '" . $nama . "' "
                                . "or trim(nama)||' '||trim(spec) ilike '" . $nama . "' "
                                . "or (trim(nama) ilike '$nama' and trim(spec) ilike '$spec') "
                                . "or (trim(nama) ilike '$nama' and trim(hidden_spec) ilike '$spec') "
                                . "or (trim(nama) ilike '$nama' and trim(hidden_spec) ilike '$hidden_spec') "
                                . "or (trim(nama) ilike '$nama' and trim(spec) ilike '$hidden_spec')"
                                . ") "
                                . "and harga = $harga "
                                . "and satuan ilike '$satuan' ";
                        // end jika
                    }

                    $stmt_cek_usulan = $con->prepareStatement($query_cek_usulan);
                    $rs_cek_usulan = $stmt_cek_usulan->executeQuery();
                    while ($rs_cek_usulan->next()) {
                        $total_cek_usulan = $rs_cek_usulan->getString('total');
                    }
//                  cek tabel usulan
//                  cek tabel shsd
                    if ($jenis_usulan_item == 'BARU') {
                        // jika 
                        // (
                        // atau (nama ilike nama(spasi)spec) 
                        // atau (nama spec ilike nama)   
                        // atau (nama ilike nama dan spec ilike spec) 
                        // atau (nama ilike nama dan spec ilike hidden_spec)
                        // atau (nama ilike nama dan hidden_spec ilike spec)
                        // atau (nama ilike nama dan hidden_spec ilike hidden_spec)
                        // )
                        // dan (satuan ilike satuan)
                        $c = new Criteria();
                        $c->add(SatuanPeer::SATUAN_NAME, $satuan, Criteria::ILIKE);
                        $data_satuan = SatuanPeer::doSelectOne($c);
                        $satuan_id = $data_satuan->getSatuanId();
                        
                        $query_cek_ssh = "select count(*) as total "
                                . "from " . sfConfig::get('app_default_schema') . ".komponen_all "
                                . "where ("
                                . "trim(komponen_name) ilike '" . $nama . " " . $spec . "' "
                                . "or trim(komponen_name) ilike '" . $nama . "' "
                                . "or trim(komponen_name)||' '||trim(spec) ilike '" . $nama . "' "
                                . "or (trim(komponen_name) ilike '$nama' and trim(spec) ilike '$spec') "
                                . "or (trim(komponen_name) ilike '$nama' and trim(hidden_spec) ilike '$spec') "
                                . "or (trim(komponen_name) ilike '$nama' and trim(hidden_spec) ilike '$hidden_spec') "
                                . "or (trim(komponen_name) ilike '$nama' and trim(spec) ilike '$hidden_spec')"
                                . ") "
                                . "and komponen_harga = $harga "
                                . "and satuan_id = $satuan_id ";
                    } else if ($jenis_usulan_item == 'PERUBAHAN HARGA') {
                        // jika 
                        // (
                        // atau (nama ilike nama(spasi)spec) 
                        // atau (nama spec ilike nama)    
                        // atau (nama ilike nama dan spec ilike spec) 
                        // atau (nama ilike nama dan spec ilike hidden_spec)
                        // atau (nama ilike nama dan hidden_spec ilike spec)
                        // atau (nama ilike nama dan hidden_spec ilike hidden_spec)
                        // )
                        // dan (harga equal harga)
                        // dan (satuan ilike satuan)
                        $c = new Criteria();
                        $c->add(SatuanPeer::SATUAN_NAME, $satuan, Criteria::ILIKE);
                        $data_satuan = SatuanPeer::doSelectOne($c);
                        $satuan_id = $data_satuan->getSatuanId();
                        
                        $query_cek_ssh = "select count(*) as total "
                                . "from " . sfConfig::get('app_default_schema') . ".komponen_all "
                                . "where ("
                                . "trim(komponen_name) ilike '" . $nama . " " . $spec . "' "
                                . "or trim(komponen_name) ilike '" . $nama . "' "
                                . "or trim(komponen_name)||' '||trim(spec) ilike '" . $nama . "' "
                                . "or (trim(komponen_name) ilike '$nama  ' and trim(spec) ilike '$spec') "
                                . "or (trim(komponen_name) ilike ' $nama' and trim(hidden_spec) ilike '$spec') "
                                . "or (trim(komponen_name) ilike '$nama' and trim(hidden_spec) ilike '$hidden_spec') "
                                . "or (trim(komponen_name) ilike '$nama' and trim(spec) ilike '$hidden_spec')"
                                . ") "
                                . "and komponen_harga = $harga "
                                . "and satuan_id = $satuan_id ";
                    }

                    $stmt_cek_ssh = $con->prepareStatement($query_cek_ssh);
                    $rs_cek_ssh = $stmt_cek_ssh->executeQuery();
                    while ($rs_cek_ssh->next()) {
                        $total_cek_ssh = $rs_cek_ssh->getString('total');
                    }
//                  cek tabel shsd                                     

                    $array_rekening = explode(')', $rekening);
                    $kode_rekening = str_replace('(', '', $array_rekening [0]);

                    $array_kode_barang = explode(')', $kode_barang);
                    $kode_barang_fresh = str_replace('(', '', $array_kode_barang[0]);

//                        ambil data SPJM
                    $c_data_spjm = new Criteria();
                    $c_data_spjm->add(UsulanSPJMPeer::PENYELIA, $penyelia, Criteria::EQUAL);
                    $c_data_spjm->addAnd(UsulanSPJMPeer::TGL_SPJM, $tanggal_spjm, Criteria::EQUAL);
                    $c_data_spjm->addAnd(UsulanSPJMPeer::SKPD, $skpd, Criteria::EQUAL);
                    $c_data_spjm->addAnd(UsulanSPJMPeer::TIPE_USULAN, $tipe_usulan, Criteria::EQUAL);
                    $c_data_spjm->addAnd(UsulanSPJMPeer::JENIS_USULAN, $jenis_usulan, Criteria::EQUAL);
                    $c_data_spjm->addAnd(UsulanSPJMPeer::JUMLAH_DUKUNGAN, $jumlah_dukungan, Criteria::EQUAL);
                    $c_data_spjm->addAnd(UsulanSPJMPeer::CREATED_AT, $sekarang, Criteria::EQUAL);
                    $c_data_spjm->addAnd(UsulanSPJMPeer::UPDATED_AT, $sekarang, Criteria::EQUAL);
                    $data_spjm = UsulanSPJMPeer::doSelectOne($c_data_spjm);

                    $id_spjm = $data_spjm->getIdSpjm();
//                    ambil data SPJM
//                    cek satuan
                    $c = new Criteria();
                    $c->add(SatuanPeer::SATUAN_NAME, $satuan, Criteria::ILIKE);
                    $ada_satuan = SatuanPeer::doCount($c);

                    if ($ada_satuan > 0) {
//                    ambil data satuan
                        $c = new Criteria();
                        $c->add(SatuanPeer::SATUAN_NAME, $satuan, Criteria::ILIKE);
                        $data_satuan = SatuanPeer::doSelectOne($c);

                        $satuan_baru = $data_satuan->getSatuanName();
//                    ambil data satuan                            
                    } else {
                        $this->setFlash('gagal', 'Satuan tidak ada.');
                        return $this->redirect('usulan_ssh/usulansshbaru');
                         
                        //$satuan_baru = $satuan;
//                    ambil nilai satuan_id maximal                            
//                        $query_max_satuan = "select max(satuan_id) as max_id "
//                                . "from " . sfConfig::get('app_default_schema') . ".satuan ";
//                        $stmt_max_satuan = $con->prepareStatement($query_max_satuan);
//                        $rs_max_satuan = $stmt_max_satuan->executeQuery();
//                        while ($rs_max_satuan->next()) {
//                            $satuan_id = $rs_max_satuan->getString('max_id') + 1;
//                        }
//                    ambil nilai satuan_id maximal                            
//                    save satuan baru
//                        $insert_satuan = new Satuan();
//                        $insert_satuan->setSatuanId($satuan_id);
//                        $insert_satuan->setUserId($penyelia);
//                        $insert_satuan->setSatuanName($satuan);
//                        $insert_satuan->setWaktuAccess($sekarang);
//                        $insert_satuan->save();
//
//                        $satuan_baru = $satuan;
//                    save satuan baru
                    }
//                    cek satuan

                    $insert_usulan = new UsulanSSH();
                    $insert_usulan->setNama($nama);
                    $insert_usulan->setSpec(str_replace('tidak ada', '', $spec));
                    $insert_usulan->setHiddenSpec(str_replace('tidak ada', '', $hidden_spec));
                    $insert_usulan->setMerek(str_replace('tidak ada', '', $merek));
                    $insert_usulan->setSatuan($satuan_baru);
                    $insert_usulan->setHarga($harga);
                    $insert_usulan->setRekening($kode_rekening);
                    $insert_usulan->setPajak($pajak);
                    $insert_usulan->setKeterangan(str_replace('tidak ada', '', $keterangan));
                    $insert_usulan->setKodeBarang($kode_barang_fresh);
                    $insert_usulan->setIdSpjm($id_spjm);
                    $insert_usulan->setCreatedAt($sekarang);
                    $insert_usulan->setUpdatedAt($sekarang);
                    $insert_usulan->setStatusVerifikasi(FALSE);
                    $insert_usulan->setStatusSurvey(FALSE);
                    $insert_usulan->setStatusConfirmasiPenyelia(FALSE);
                    if ($total_cek_usulan > 0 || $total_cek_ssh > 0) {
                        $insert_usulan->setStatusHapus(TRUE);
                    } else {
                        $insert_usulan->setStatusHapus(FALSE);
                    }
                    if ($kode_barang_fresh == '2.1.2.01.01.01.001') {
                        $insert_usulan->setTipeUsulan('EST');
                    } else {
                        $insert_usulan->setTipeUsulan($tipe);
                    }
                    $insert_usulan->setUnitId($skpd);

                    // if (sfConfig::get('app_tahap_edit') == 'murni' && $token_kepake->getJumlahSsh() >= 50 && $skpd == '1700') {
                    //     $insert_usulan->setStatusConfirmasiPenyelia(FALSE);
                    // }

                    $insert_usulan->setNamaSebelum($nama_sebelum);
                    $insert_usulan->setHargaSebelum($harga_sebelum);
                    $insert_usulan->setAlasanPerubahanHarga($alasan_perubahan);
                    if ($nama_sebelum <> '' && $harga_sebelum > 0 && $alasan_perubahan <> '') {
                        $insert_usulan->setIsPerubahanHarga(TRUE);
                    } else {
                        $insert_usulan->setIsPerubahanHarga(FALSE);
                    }
                    $insert_usulan->setHargaPendukung1($harga_pendukung1);
                    $insert_usulan->setHargaPendukung2($harga_pendukung2);
                    $insert_usulan->setHargaPendukung3($harga_pendukung3);
                    $insert_usulan->setAlasanPerbedaanDinas($alasan_perbedaan);
                    $insert_usulan->setCV1($cv1);
                    $insert_usulan->setCV2($cv2);
                    $insert_usulan->setCV3($cv3);
                    $insert_usulan->setAlasanPerbedaanDinas($alasan_perbedaan);
                    if ($harga_pendukung1 == $harga || $harga_pendukung2 == $harga || $harga_pendukung3 == $harga) {
                        $insert_usulan->setIsPerbedaanPendukung(FALSE);
                    } else {
                        $insert_usulan->setIsPerbedaanPendukung(TRUE);
                    }
//                        ticket #12 simpan sebagai masalah - set komentar
                    if ($total_cek_usulan > 0) {
                        $insert_usulan->setKomentarVerifikator('Ditemukan telah ada usulan dengan nama atau harga yang sama.');
                    } else if ($total_cek_ssh > 0) {
                        $insert_usulan->setKomentarVerifikator('Ditemukan telah ada SSH dengan nama atau harga yang sama.');
                    }
//                        ticket #12 simpan sebagai masalah - set komentar
                    $insert_usulan->save();
//                            save usulan_baru
                    //fungsi user_log
                    $namaprofil = $this->getUser()->getNamaUser();
                    $username = $this->getUser()->getNamaLogin();
                    if ($total_cek_usulan > 0 || $total_cek_ssh > 0) {
                        budgetLogger::log($username . ' menambah Usulan SSH bermasalah dengan nama : ' . $nama . ' ' . $spec . ' Dinas ' . $skpd . ' (id SPJM :' . $id_spjm . ')');
                    } else {
                        budgetLogger::log($username . ' menambah Usulan SSH dengan nama : ' . $nama . ' ' . $spec . ' Dinas ' . $skpd . ' (id SPJM :' . $id_spjm . ')');
                    }

                    //cek count berdasarkan id_spjm dari tabel usulan_ssh
                    $c_cek_usulan_idspjm = new Criteria();
                    $c_cek_usulan_idspjm->add(UsulanSSHPeer::ID_SPJM, $id_spjm, Criteria::EQUAL);
                    $c_cek_usulan_idspjm->addAnd(UsulanSSHPeer::STATUS_VERIFIKASI, FALSE);
                    $c_cek_usulan_idspjm->addAnd(UsulanSSHPeer::SHSD_ID, NULL);
                    $c_cek_usulan_idspjm->addAnd(UsulanSSHPeer::STATUS_HAPUS, FALSE);
                    $count_cek_usulan_idspjm = UsulanSSHPeer::doCount($c_cek_usulan_idspjm);
                    //cek count berdasarkan id_spjm dari tabel usulan_ssh

                    if ($count_cek_usulan_idspjm == 0) {
//                        $query_update_konfirmasi_usulan_idspjm = "update " . sfConfig::get('app_default_schema') . ".usulan_spjm "
////                                . "set status_verifikasi = true, updated_at = '$sekarang' "
//                                . "where id_spjm = $id_spjm";
//                        $stmt_update_konfirmasi_usulan_idspjm = $con->prepareStatement($query_update_konfirmasi_usulan_idspjm);
//                        $stmt_update_konfirmasi_usulan_idspjm->executeQuery();
                    }
                }
                $con->commit();
                $this->setFlash('berhasil', 'Berhasil tersimpan usulan');
                return $this->redirect('usulan_ssh/usulansshkonfirmasi');
            } catch (Exception $exc) {
                $con->rollback();
//                unlink(sfConfig::get('sf_root_dir') . '/web/uploads/usulan_ssh/' . $fileName);
                $this->setFlash('gagal', $exc->getMessage());
                return $this->redirect('usulan_ssh/usulansshkonfirmasi');
            }
        }
    }

    //ticket #4 - proses upload SPJM + usulan SSH   
    //ticket #6 - hapus usulan saat konfirmasi
    function executeHapusSSH() {
        $con = Propel::getConnection();
        $con->begin();

        $id_usulan = $this->getRequestParameter('id');
        $key = $this->getRequestParameter('key');
        $md = md5('hapus_ssh');

        if ($key != $md) {
            $this->setFlash('gagal', 'Key Salah');
            return $this->redirect('usulan_ssh/usulansshkonfirmasi');
        } else {
            try {
                $sekarang = date('Y-m-d H:i:s');
                $hapus_usulan = "UPDATE " . sfConfig::get('app_default_schema') . ".usulan_ssh "
                        . "SET status_hapus = true, updated_at = '$sekarang', komentar_verifikator = 'Penyelia Menghapus' "
                        . "WHERE id_usulan = $id_usulan";
                $stmt_hapus_usulan = $con->prepareStatement($hapus_usulan);
                $stmt_hapus_usulan->executeQuery();

                $c = new Criteria();
                $c->add(UsulanSSHPeer::ID_USULAN, $id_usulan, Criteria::EQUAL);
                $rs_usulan = UsulanSSHPeer::doSelectOne($c);

                //cek count berdasarkan id_spjm dari tabel usulan_ssh

                $c_cek_usulan_idusulan = new Criteria();
                $c_cek_usulan_idusulan->add(UsulanSSHPeer::ID_USULAN, $id_usulan, Criteria::EQUAL);
                $data_usulan = UsulanSSHPeer::doSelectOne($c_cek_usulan_idusulan);
                $id_spjm = $data_usulan->getIdSpjm();

                $total_cek_nomor = 0;
                $query_cek_usulan_idspjm = "select count(*) as total from " . sfConfig::get('app_default_schema') . ".usulan_ssh "
                        . "where status_hapus = false and status_verifikasi = false and shsd_id is null "
                        . "and id_spjm = '$id_spjm'";
                $stmt_cek_usulan_idspjm = $con->prepareStatement($query_cek_usulan_idspjm);
                $rs1 = $stmt_cek_usulan_idspjm->executeQuery();
                while ($rs1->next()) {
                    $total_cek_nomor = $rs1->getString('total');
                }
                if ($total_cek_nomor == 0) {
                    $query_update_usulan_idspjm = "update " . sfConfig::get('app_default_schema') . ".usulan_spjm "
                            . "set updated_at = '$sekarang', status_verifikasi = true "
                            . "where id_spjm = '$id_spjm' ";
                    $stmt_cek_update_idspjm = $con->prepareStatement($query_update_usulan_idspjm);
                    $stmt_cek_update_idspjm->executeQuery();
                }
                $con->commit();

                //fungsi user_log
                $namaprofil = $this->getUser()->getNamaUser();
                $username = $this->getUser()->getNamaLogin();
                budgetLogger::log($username . ' menghapus Usulan SSH id :' . $id_usulan . ' dengan nama : ' . $rs_usulan->getNama() . ' ' . $rs_usulan->getSpec() . ' pada dinas :' . $rs_usulan->getUnitId());
                //fungsi user_log

                $this->setFlash('berhasil', 'Berhasil menghapus usulan SSH');
            } catch (Exception $exc) {
                $con->rollback();
                $this->setFlash('gagal', $exc->getMessage());
            }
        }
        return $this->redirect('usulan_ssh/usulansshkonfirmasi');
    }

    //ticket #6 - hapus usulan saat konfirmasi
    //ticket #6 - page konfirmasi
    function executeUsulanproseskonfirmasi() {
        $con = Propel::getConnection();

        $id_usulan = $this->getRequestParameter('id');

        $key = $this->getRequestParameter('key');
        $md = md5('cek_konfirmasi_usulan');

        if ($key != $md) {
            $this->setFlash('gagal', 'Key Salah');
            return $this->redirect('usulan_ssh/usulansshkonfirmasi');
        } else {
            try {
                $this->sekarang = $sekarang = date('Y-m-d H:i:s');
//                data usulan
                $c = new Criteria();
                $c->add(UsulanSSHPeer::ID_USULAN, $id_usulan, Criteria::EQUAL);
                $rs_usulan = UsulanSSHPeer::doSelectOne($c);
                $this->data_usulan_ssh = $rs_usulan;
//                data usulan
//                list satuan                
                $c_all_satuan = new Criteria();
                $c_all_satuan->addAscendingOrderByColumn(SatuanPeer::SATUAN_NAME);
                $rs_all_satuan = SatuanPeer::doSelect($c_all_satuan);
                $this->list_satuan = $rs_all_satuan;
//                list satuan
//                list rekening
                $c_all_rekening = new Criteria();
                $c_all_rekening->addAscendingOrderByColumn(RekeningPeer::REKENING_CODE);
                $rs_all_rekening = RekeningPeer::doSelect($c_all_rekening);
                $this->list_rekening = $rs_all_rekening;
//                list rekening
//                kode barang dengan 'X.X.X.X'
                $query_all_kode_barang = "select * "
                        . "from " . sfConfig::get('app_default_schema') . ".kategori_shsd "
                        . "where kategori_shsd_id ilike '%.%.%.%' "
                        . "order by kategori_shsd_id ";
                $stmt_all_kode_barang = $con->prepareStatement($query_all_kode_barang);
                $rs_all_kode_barang = $stmt_all_kode_barang->executeQuery();
                $this->list_kode_barang = $rs_all_kode_barang;
//                kode barang dengan 'X.X.X.X'

                $id_usulan = $this->getRequestParameter('id');

                $c = new Criteria();
                $c->add(UsulanSSHPeer::ID_USULAN, $id_usulan, Criteria::EQUAL);
                $rs_usulan = UsulanSSHPeer::doSelectOne($c);

                $cek = preg_replace("/[^[:alnum:]]/u", '|', $rs_usulan->getNama()) . '|' . preg_replace("/[^[:alnum:]]/u", '|', $rs_usulan->getSpec());
                $arr_cek = explode('|', $cek);
                $temp = "nama||' '||spec ilike '%" . $arr_cek[0] . "%'";
                for ($i = 1; $i < sizeof($arr_cek); $i++) {
                    $temp .= " and nama||' '||spec ilike '%" . $arr_cek[$i] . "%'";
                }

                $con = Propel::getConnection();
                $query_usulan_mirip = "select * "
                        . "from " . sfConfig::get('app_default_schema') . ".usulan_ssh "
                        . "where " . $temp . " "
                        . "and status_hapus = false "
                        . "and id_usulan != $id_usulan "
                        . "order by status_verifikasi, nama, spec ";
                $stmt_usulan_mirip = $con->prepareStatement($query_usulan_mirip);
                $rs_usulan_mirip = $stmt_usulan_mirip->executeQuery();
                $i = 0;
                foreach ($rs_usulan_mirip as $result) {
                    $i++;
                }
                if ($i > 0) {
                    $this->rs_usulan_mirip = 1;
                } else {
                    $this->rs_usulan_mirip = 0;
                }

                $c = new Criteria();
                $c->add(UsulanSSHPeer::ID_USULAN, $id_usulan, Criteria::EQUAL);
                $rs_usulan = UsulanSSHPeer::doSelectOne($c);

                $cek = preg_replace("/[^[:alnum:]]/u", '|', $rs_usulan->getNama()) . '|' . preg_replace("/[^[:alnum:]]/u", '|', $rs_usulan->getSpec());
                $arr_cek = explode('|', $cek);
                $temp = "komponen_name ilike '%" . $arr_cek[0] . "%'";
                for ($i = 1; $i < sizeof($arr_cek); $i++) {
                    $temp .= " and komponen_name ilike '%" . $arr_cek[$i] . "%'";
                }
                $con = Propel::getConnection();
                $query_ssh_mirip = "select * "
                        . "from " . sfConfig::get('app_default_schema') . ".komponen_all "
                        . "where " . $temp . " "
                        . "order by komponen_name, komponen_id ";
                $stmt_ssh_mirip = $con->prepareStatement($query_ssh_mirip);
                $rs_ssh_mirip = $stmt_ssh_mirip->executeQuery();
                $i = 0;
                foreach ($rs_ssh_mirip as $result) {
                    $i++;
                }
                if ($i > 0) {
                    $this->rs_ssh_mirip = 1;
                } else {
                    $this->rs_ssh_mirip = 0;
                }
            } catch (Exception $exc) {
                $this->setFlash('gagal', $exc->getMessage());
                return $this->redirect('usulan_ssh/usulansshkonfirmasi');
            }
        }
    }

    function executeUsulanprosessurvey() {
        $con = Propel::getConnection();

        $id_usulan = $this->getRequestParameter('id');

        $key = $this->getRequestParameter('key');
        $md = md5('cek_konfirmasi_usulan');

        if ($key != $md) {
            $this->setFlash('gagal', 'Key Salah');
            return $this->redirect('usulan_ssh/usulansshkonfirmasi');
        } else {
            try {
                $this->sekarang = $sekarang = date('Y-m-d H:i:s');
//                data usulan
                $c = new Criteria();
                $c->add(UsulanSSHPeer::ID_USULAN, $id_usulan, Criteria::EQUAL);
                $rs_usulan = UsulanSSHPeer::doSelectOne($c);
                $this->data_usulan_ssh = $rs_usulan;
//                data usulan
//                list satuan                
                $c_all_satuan = new Criteria();
                $c_all_satuan->addAscendingOrderByColumn(SatuanPeer::SATUAN_NAME);
                $rs_all_satuan = SatuanPeer::doSelect($c_all_satuan);
                $this->list_satuan = $rs_all_satuan;
//                list satuan
//                list rekening
                $c_all_rekening = new Criteria();
                $c_all_rekening->addAscendingOrderByColumn(RekeningPeer::REKENING_CODE);
                $rs_all_rekening = RekeningPeer::doSelect($c_all_rekening);
                $this->list_rekening = $rs_all_rekening;
//                list rekening
//                kode barang dengan 'X.X.X.X'
                $query_all_kode_barang = "select * "
                        . "from " . sfConfig::get('app_default_schema') . ".kategori_shsd "
                        . "where kategori_shsd_id ilike '%.%.%.%' "
                        . "order by kategori_shsd_id ";
                $stmt_all_kode_barang = $con->prepareStatement($query_all_kode_barang);
                $rs_all_kode_barang = $stmt_all_kode_barang->executeQuery();
                $this->list_kode_barang = $rs_all_kode_barang;
//                kode barang dengan 'X.X.X.X'

                $id_usulan = $this->getRequestParameter('id');

                $c = new Criteria();
                $c->add(UsulanSSHPeer::ID_USULAN, $id_usulan, Criteria::EQUAL);
                $rs_usulan = UsulanSSHPeer::doSelectOne($c);

                $cek = preg_replace("/[^[:alnum:]]/u", '|', $rs_usulan->getNama()) . '|' . preg_replace("/[^[:alnum:]]/u", '|', $rs_usulan->getSpec());
                $arr_cek = explode('|', $cek);
                $temp = "nama||' '||spec ilike '%" . $arr_cek[0] . "%'";
                for ($i = 1; $i < sizeof($arr_cek); $i++) {
                    $temp .= " and nama||' '||spec ilike '%" . $arr_cek[$i] . "%'";
                }

                $con = Propel::getConnection();
                $query_usulan_mirip = "select * "
                        . "from " . sfConfig::get('app_default_schema') . ".usulan_ssh "
                        . "where " . $temp . " "
                        . "and status_hapus = false "
                        . "and id_usulan != $id_usulan "
                        . "order by status_verifikasi, nama, spec ";
                $stmt_usulan_mirip = $con->prepareStatement($query_usulan_mirip);
                $rs_usulan_mirip = $stmt_usulan_mirip->executeQuery();
                $i = 0;
                foreach ($rs_usulan_mirip as $result) {
                    $i++;
                }
                if ($i > 0) {
                    $this->rs_usulan_mirip = 1;
                } else {
                    $this->rs_usulan_mirip = 0;
                }

                $c = new Criteria();
                $c->add(UsulanSSHPeer::ID_USULAN, $id_usulan, Criteria::EQUAL);
                $rs_usulan = UsulanSSHPeer::doSelectOne($c);

                $cek = preg_replace("/[^[:alnum:]]/u", '|', $rs_usulan->getNama()) . '|' . preg_replace("/[^[:alnum:]]/u", '|', $rs_usulan->getSpec());
                $arr_cek = explode('|', $cek);
                $temp = "komponen_name ilike '%" . $arr_cek[0] . "%'";
                for ($i = 1; $i < sizeof($arr_cek); $i++) {
                    $temp .= " and komponen_name ilike '%" . $arr_cek[$i] . "%'";
                }
                $con = Propel::getConnection();
                $query_ssh_mirip = "select * "
                        . "from " . sfConfig::get('app_default_schema') . ".komponen_all "
                        . "where " . $temp . " "
                        . "order by komponen_name, komponen_id ";
                $stmt_ssh_mirip = $con->prepareStatement($query_ssh_mirip);
                $rs_ssh_mirip = $stmt_ssh_mirip->executeQuery();
                $i = 0;
                foreach ($rs_ssh_mirip as $result) {
                    $i++;
                }
                if ($i > 0) {
                    $this->rs_ssh_mirip = 1;
                } else {
                    $this->rs_ssh_mirip = 0;
                }
            } catch (Exception $exc) {
                $this->setFlash('gagal', $exc->getMessage());
                return $this->redirect('usulan_ssh/usulansshkonfirmasi');
            }
        }
    }

    //ticket #6 - page konfirmasi
    //ticket #5 - droplist usulan SSH dengan nama yang mirip
    public function executeGetUsulanMirip() {
        if ($this->getRequestParameter('id')) {
            $id_usulan = $this->getRequestParameter('id');

            $c = new Criteria();
            $c->add(UsulanSSHPeer::ID_USULAN, $id_usulan, Criteria::EQUAL);
            $rs_usulan = UsulanSSHPeer::doSelectOne($c);

            $cek = preg_replace("/[^[:alnum:]]/u", '|', $rs_usulan->getNama()) . '|' . preg_replace("/[^[:alnum:]]/u", '|', $rs_usulan->getSpec());
            $arr_cek = explode('|', $cek);
            $temp = "nama||' '||spec ilike '%" . $arr_cek[0] . "%'";
            for ($i = 1; $i < sizeof($arr_cek); $i++) {
                $temp .= " and nama||' '||spec ilike '%" . $arr_cek[$i] . "%'";
            }

            $con = Propel::getConnection();
            $query_usulan_mirip = "select * "
                    . "from " . sfConfig::get('app_default_schema') . ".usulan_ssh "
                    . "where " . $temp . " "
                    . "and status_hapus = false "
                    . "and id_usulan != $id_usulan "
                    . "order by status_verifikasi, nama, spec ";
            $stmt_usulan_mirip = $con->prepareStatement($query_usulan_mirip);
            $rs_usulan_mirip = $stmt_usulan_mirip->executeQuery();
            $this->list_usulan_ssh_mirip = $rs_usulan_mirip;

            $this->id = $id_usulan;
            $this->setLayout('kosong');
        }
    }

    //ticket #5 - droplist usulan SSH dengan nama yang mirip
    //ticket #5 - droplist SSH yang memiliki nama yang mirip
    public function executeGetSSHMirip() {
        if ($this->getRequestParameter('id')) {
            $id_usulan = $this->getRequestParameter('id');

            $c = new Criteria();
            $c->add(UsulanSSHPeer::ID_USULAN, $id_usulan, Criteria::EQUAL);
            $rs_usulan = UsulanSSHPeer::doSelectOne($c);

            $cek = preg_replace("/[^[:alnum:]]/u", '|', $rs_usulan->getNama()) . '|' . preg_replace("/[^[:alnum:]]/u", '|', $rs_usulan->getSpec());
            $arr_cek = explode('|', $cek);
            $temp = "komponen_name ilike '%" . $arr_cek[0] . "%'";
            for ($i = 1; $i < sizeof($arr_cek); $i++) {
                $temp .= " and komponen_name ilike '%" . $arr_cek[$i] . "%'";
            }
            $con = Propel::getConnection();
            $query_ssh_mirip = "select ka.komponen_tipe, ka.komponen_id, ka.komponen_name, ka.spec, ka.hidden_spec, ka.merk, st.satuan_name, ka.komponen_harga, ka.non_pajak, ka.usulan_skpd "
                    . "from " . sfConfig::get('app_default_schema') . ".komponen_all ka "
                    . "INNER JOIN ebudget.satuan st ON ka.satuan_id = st.satuan_id where " . $temp . " "
                    . "and komponen_tipe = 'SHSD' "
                    . "order by komponen_name, komponen_id ";
            $stmt_ssh_mirip = $con->prepareStatement($query_ssh_mirip);
            $rs_ssh_mirip = $stmt_ssh_mirip->executeQuery();
            $this->list_ssh_mirip = $rs_ssh_mirip;

            $this->id = $id_usulan;
            $this->setLayout('kosong');
        }
    }

    //ticket #5 - droplist SSH yang memiliki nama yang mirip  
    //    ticket #6 - saving konfirmasi
    function executeSavekonfirmasi() {
        $con = Propel::getConnection();

        

        $sekarang = date('Y-m-d H:i:s');

        $id_usulan = $this->getRequestParameter('id_usulan');
        $id_spjm = $this->getRequestParameter('id_spjm');
        $kode_barang = $this->getRequestParameter('kode_barang');
        $nama = $this->getRequestParameter('nama');
        $spec = $this->getRequestParameter('spec');
        $hidden_spec = $this->getRequestParameter('hidden_spec');
        $merek = $this->getRequestParameter('merek');
        $satuan = $this->getRequestParameter('satuan');
        $harga = str_replace(',', '.', $this->getRequestParameter('harga'));
        $pajak = $this->getRequestParameter('pajak');
        $keterangan = $this->getRequestParameter('keterangan');
        $rekening = $this->getRequestParameter('rekening');
        $skpd = $this->getRequestParameter('skpd');
        $nama_sebelum = $this->getRequestParameter('nama_sebelum');
        $harga_sebelum = $this->getRequestParameter('harga_sebelum');
        $alasan_perubahan = $this->getRequestParameter('alasan_perubahan');
        $komponen_tipe2 = $this->getRequestParameter('komponen_tipe2');

        $c_usulan = new Criteria();
        $c_usulan->add(UsulanSSHPeer::ID_USULAN, $id_usulan, Criteria::EQUAL);
        $c_survey = UsulanSSHPeer::doSelectOne($c_usulan);
        $status_survey = $c_survey->getStatusSurvey();

        $con->begin();
        $c = new Criteria();
        $c->add(UsulanSSHPeer::ID_USULAN, $id_usulan);
        $c->add(UsulanSSHPeer::ID_SPJM, $id_spjm);
        $rs_usulanSSH = UsulanSSHPeer::doSelectOne($c);
        $rs_usulanSSH->setUpdatedAt($sekarang);
        $rs_usulanSSH->setNama($nama);
        $rs_usulanSSH->setKodeBarang($kode_barang);
        $rs_usulanSSH->setSpec($spec);
        $rs_usulanSSH->setHiddenSpec($hidden_spec);
        $rs_usulanSSH->setMerek($merek);
        $rs_usulanSSH->setSatuan($satuan);
        $rs_usulanSSH->setHarga($harga);
        $rs_usulanSSH->setRekening($rekening);
        $rs_usulanSSH->setPajak($pajak);
        $rs_usulanSSH->setKeterangan($keterangan);
        $rs_usulanSSH->setUnitId($skpd);
        $rs_usulanSSH->setStatusConfirmasiPenyelia(TRUE);

        //buat filter untuk nama+spec yang mirip dengan yang sudah ada di SHSD
        $c1 = new Criteria();
        $temp = preg_replace("/[^[:alnum:]]/u", '|', $nama) . '|' . preg_replace("/[^[:alnum:]]/u", '|', $spec);
        $arr_temp = explode('|', $temp);

        foreach ($arr_temp as $a) {
            $c1->addAnd(KomponenAllPeer::KOMPONEN_NAME, '%' . $a . '%', Criteria::ILIKE);
        }
        $rs_shsd = KomponenAllPeer::doSelect($c1);
        if ($rs_shsd != null) {
            $rs_usulanSSH->setStatusPending(TRUE);
        } else {
            $rs_usulanSSH->setStatusPending(FALSE);
        }
        if ($nama_sebelum <> '' && $harga_sebelum > 0 && $alasan_perubahan <> '') {
            $rs_usulanSSH->setAlasanPerubahanHarga($alasan_perubahan);
            $rs_usulanSSH->setNamaSebelum($nama_sebelum);
            $rs_usulanSSH->setHargaSebelum($harga_sebelum);
        }
        $rs_usulanSSH->setKomponenTipe2($komponen_tipe2);

        $rs_usulanSSH->save();

        //fungsi user_log
        $username = $this->getUser()->getNamaLogin();
        budgetLogger::log($username . ' mengkonfirmasi Usulan SSH id :' . $id_usulan . ' dengan nama : ' . $nama . ' ' . $spec . ' untuk dinas :' . $skpd);
        //fungsi user_log

        $con->commit();

        $this->setFlash('berhasil', 'Berhasil mengkonfirmasi Usulan SSH id :' . $id_usulan . ' dengan nama : ' . $nama . ' ' . $spec . ' untuk dinas :' . $skpd);

        return $this->redirect('usulan_ssh/usulansshkonfirmasi');
    }

    function executeSavekonfirmasisurvey() {
        $con = Propel::getConnection();
        $con->begin();
        $sekarang = date('Y-m-d H:i:s');

        $id_usulan = $this->getRequestParameter('id_usulan');
        $id_spjm = $this->getRequestParameter('id_spjm');
        $kode_barang = $this->getRequestParameter('kode_barang');
        $nama = $this->getRequestParameter('nama');
        $spec = $this->getRequestParameter('spec');
        $nama_ssh = $this->getRequestParameter('nama').' '.$this->getRequestParameter('spec');
        $hidden_spec = $this->getRequestParameter('hidden_spec');
        $merek = $this->getRequestParameter('merek');
        $satuan = $this->getRequestParameter('satuan');
        $harga = str_replace(',', '.', $this->getRequestParameter('harga'));
        $pajak = $this->getRequestParameter('pajak');
        $skpd = $this->getRequestParameter('skpd');
        $rekening = $this->getRequestParameter('rekening');
        $keterangan_surveyor = $this->getRequestParameter('keterangan_surveyor');
        $catatan_surveyor = $this->getRequestParameter('catatan_surveyor');
        $surveyor = $this->getRequestParameter('surveyor');
        $unit_id = $this->getRequestParameter('unit_id');

        $cv_survey1 = $this->getRequestParameter('cv_survey1');
        $harga_survey1 = preg_replace("/[^0-9]/", "", $this->getRequestParameter('harga_survey1'));
        $alamat1 = $this->getRequestParameter('alamat1');
        $penerima1 = $this->getRequestParameter('penerima1');
        $file1 = $this->getRequestParameter('dukungan1');
        $tgl_konfirmasi1 = $this->getRequestParameter('tgl_konfirmasi1');
       
        $cv_survey2 = $this->getRequestParameter('cv_survey2');
        $harga_survey2 = preg_replace("/[^0-9]/", "", $this->getRequestParameter('harga_survey2'));
        $alamat2 = $this->getRequestParameter('alamat2');
        $penerima2 = $this->getRequestParameter('penerima2');
        $file2 = $this->getRequestParameter('dukungan2');
        $tgl_konfirmasi2 = $this->getRequestParameter('tgl_konfirmasi2');

        $cv_survey3 = $this->getRequestParameter('cv_survey3');
        $harga_survey3 = preg_replace("/[^0-9]/", "", $this->getRequestParameter('harga_survey3'));
        $alamat3 = $this->getRequestParameter('alamat3');
        $penerima3 = $this->getRequestParameter('penerima3');
        $file3 = $this->getRequestParameter('dukungan3');
        $tgl_konfirmasi3 = $this->getRequestParameter('tgl_konfirmasi3');

        $catatan_surveyor = $this->getRequestParameter('catatan_surveyor');

        $query = "select unit_name as unit_name from unit_kerja u
                        where unit_id='$unit_id'";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs1 = $stmt->executeQuery();
            while ($rs1->next()) {
                $this->unit_name = $rs1->getString('unit_name');
        }
        if (is_null($harga_survey1) or $harga_survey1 =='' )
        {
            $harga_survey1 = 0;
        }
        if (is_null($harga_survey2) or $harga_survey2 =='' )
        {
            $harga_survey2 = 0;
        }
        if (is_null($harga_survey3) or $harga_survey3 =='')
        {
            $harga_survey3 = 0;
        } 
        if (is_null($tgl_konfirmasi2) or $tgl_konfirmasi2 =='')
        {
            $tgl_konfirmasi2='2000-01-01';
        }
        if (is_null($tgl_konfirmasi3) or $tgl_konfirmasi3 == '')
        {
            $tgl_konfirmasi3='2000-01-01';
        }
        $unit_name=$this->unit_name;

        if( $pajak == 0)
             $pajak='true';
        else
            $pajak='false';

        $query_hapus = 
        "DELETE from ".sfConfig::get('app_default_schema').".shsd_survey
        where shsd_id='".$id_usulan."'";
        $cons = Propel::getConnection();
        $stmt2 = $cons->prepareStatement($query_hapus);
        $stmt2->executeQuery();

        // upload file

        //Alert untuk memberi informasi harus mengisi 3  pendukung
        if($harga_survey1==0 || $harga_survey2 ==0 || $harga_survey3==0){
            $this->setFlash('gagal', 'Mohon mengisi 3 pendukung');
            return $this->redirect('usulan_ssh/usulansshsurvey');
        }

        if ($this->getRequest()->getFileName('dukungan1')) {
            $array_file_pdf = explode('.', $this->getRequest()->getFileName('dukungan1'));
            if (!in_array($array_file_pdf[count($array_file_pdf) - 1], array('zip', 'rar', 'jpg', 'pdf', 'png', 'xls', 'xlsx', 'JPG', 'PNG', 'jpeg', 'JPEG'))) {
                $this->setFlash('gagal', 'Gunakan file dengan format zip/rar/jpg/png/pdf');
                return $this->redirect('usulan_ssh/usulansshsurvey');
            }

            $fileName_compress = 'file1_' . str_replace('.', '_', $id_usulan) . '_' . date('Y-m-d_H-i') . '.' . $array_file_pdf[count($array_file_pdf) - 1];

            $this->getRequest()->moveFile('dukungan1', sfConfig::get('sf_root_dir') . '/web/uploads/survey/' . $fileName_compress);

            $file_path_pdf = sfConfig::get('sf_root_dir') . '/web/uploads/survey/' . $fileName_compress;

            if (!file_exists($file_path_pdf)) {
                unlink(sfConfig::get('sf_root_dir') . '/web/uploads/survey/' . $fileName_compress);
                $this->setFlash('gagal', 'CEK FILE ' . $fileName_compress);
                return $this->redirect('usulan_ssh/usulansshsurvey');
            }
            $file1 = $fileName_compress;
        }

        if ($this->getRequest()->getFileName('dukungan2')) {
            $array_file_pdf = explode('.', $this->getRequest()->getFileName('dukungan2'));
            if (!in_array($array_file_pdf[count($array_file_pdf) - 1], array('zip', 'rar', 'jpg', 'pdf', 'png', 'xls', 'xlsx', 'JPG', 'PNG', 'jpeg', 'JPEG'))) {
                $this->setFlash('gagal', 'Gunakan file dengan format zip/rar/jpg/png/pdf');
                return $this->redirect('usulan_ssh/usulansshsurvey');
            }

            $fileName_compress = 'file2_' . str_replace('.', '_', $id_usulan) . '_' . date('Y-m-d_H-i') . '.' . $array_file_pdf[count($array_file_pdf) - 1];

            $this->getRequest()->moveFile('dukungan2', sfConfig::get('sf_root_dir') . '/web/uploads/survey/' . $fileName_compress);

            $file_path_pdf = sfConfig::get('sf_root_dir') . '/web/uploads/survey/' . $fileName_compress;

            if (!file_exists($file_path_pdf)) {
                unlink(sfConfig::get('sf_root_dir') . '/web/uploads/survey/' . $fileName_compress);
                $this->setFlash('gagal', 'CEK FILE ' . $fileName_compress);
                return $this->redirect('usulan_ssh/usulansshsurvey');
            }
            $file2 = $fileName_compress;
        }

        if ($this->getRequest()->getFileName('dukungan3')) {
            $array_file_pdf = explode('.', $this->getRequest()->getFileName('dukungan3'));
            if (!in_array($array_file_pdf[count($array_file_pdf) - 1], array('zip', 'rar', 'jpg', 'pdf', 'png', 'xls', 'xlsx', 'JPG', 'PNG', 'jpeg', 'JPEG'))) {
                $this->setFlash('gagal', 'Gunakan file dengan format zip/rar/jpg/png/pdf');
                return $this->redirect('usulan_ssh/usulansshsurvey');
            }

            $fileName_compress = 'file3_' . str_replace('.', '_', $id_usulan) . '_' . date('Y-m-d_H-i') . '.' . $array_file_pdf[count($array_file_pdf) - 1];
            $this->getRequest()->moveFile('dukungan3', sfConfig::get('sf_root_dir') . '/web/uploads/survey/' . $fileName_compress);

            $file_path_pdf = sfConfig::get('sf_root_dir') . '/web/uploads/survey/' . $fileName_compress;

            if (!file_exists($file_path_pdf)) {
                unlink(sfConfig::get('sf_root_dir') . '/web/uploads/survey/' . $fileName_compress);
                $this->setFlash('gagal', 'CEK FILE ' . $fileName_compress);
                return $this->redirect('usulan_ssh/usulansshsurvey');
            }
            $file3 = $fileName_compress;
        }    
            
        $query_insert_shsdsurvey = 
        "INSERT INTO ".sfConfig::get('app_default_schema').".shsd_survey
        (shsd_id,satuan, shsd_name, shsd_locked, rekening_code, shsd_merk, non_pajak, nama_dasar , spec, hidden_spec,
        usulan_skpd,toko1,toko2,toko3,harga1,harga2,harga3,surveyor,file1,file2,file3,shsd_catatan,alamat1,alamat2,alamat3,tgl_konfirm1,tgl_konfirm2,tgl_konfirm3,penerima1,penerima2,penerima3)
        VALUES ('".$id_usulan."','".$satuan."','".$nama_ssh."',false,'".$rekening."','".$merek."',".$pajak.",'".$nama."', '".$spec."', '".$hidden_spec."','".$unit_name."','".$cv_survey1."','".$cv_survey2."','".$cv_survey3."',".$harga_survey1.",".$harga_survey2.",".$harga_survey3.",'".$surveyor."','".$file1."','".$file2."','".$file3."','".$catatan_surveyor."','".$alamat1."','".$alamat2."','".$alamat3."'
        , '".$tgl_konfirmasi1."','".$tgl_konfirmasi2."','".$tgl_konfirmasi3."','".$penerima1."','".$penerima2."','".$penerima3."')";
        $con = Propel::getConnection();
        $stmt1 = $con->prepareStatement($query_insert_shsdsurvey);
        $stmt1->executeQuery();
           
        if( is_null($keterangan_surveyor) or $keterangan_surveyor == '')
        { 
            $c = new Criteria();
            $c->add(UsulanSSHPeer::ID_USULAN, $id_usulan);
            $c->add(UsulanSSHPeer::ID_SPJM, $id_spjm);
            $rs_usulanSSH = UsulanSSHPeer::doSelectOne($c);        
            $rs_usulanSSH->setKomentarSurvey($catatan_surveyor);           
            $rs_usulanSSH->setStatusSurvey(TRUE);
            $rs_usulanSSH->setStatusConfirmasiPenyelia(FALSE);
        }
        else
        {

            $c = new Criteria();
            $c->add(UsulanSSHPeer::ID_USULAN, $id_usulan);
            $c->add(UsulanSSHPeer::ID_SPJM, $id_spjm);
            $rs_usulanSSH = UsulanSSHPeer::doSelectOne($c);        
            $rs_usulanSSH->setKomentarSurvey($catatan_surveyor);
            $rs_usulanSSH->setKeterangan($keterangan_surveyor);
            $rs_usulanSSH->setStatusSurvey(TRUE);
            $rs_usulanSSH->setStatusConfirmasiPenyelia(FALSE);
        }
        
        $rs_usulanSSH->save();

        //fungsi user_log
        $username = $this->getUser()->getNamaLogin();
        budgetLogger::log($surveyor . ' mensurvey Usulan SSH id :' . $id_usulan . ' dengan nama : ' . $nama . ' ' . $spec . ' untuk dinas :' . $skpd);
        //fungsi user_log

        $con->commit();

        $this->setFlash('berhasil', 'Berhasil mensurvey Usulan SSH id :' . $id_usulan . ' dengan nama : ' . $nama . ' ' . $spec . ' untuk dinas :' . $skpd);

        return $this->redirect('usulan_ssh/usulansshsurvey');
    }

//    ticket #6 - saving konfirmasi 
    //ticket #4 - list SPJM + usulan SSH
    public function executeUsulanssh() {
        $c = new Criteria();
        $c->add(UsulanSPJMPeer::STATUS_HAPUS, FALSE);
        $c->addAnd(UsulanSPJMPeer::FILEPATH_PDF, NULL);
        $c->addAnd(UsulanSPJMPeer::STATUS_VERIFIKASI, FALSE);
        $c->addAscendingOrderByColumn(UsulanSPJMPeer::ID_SPJM);
        $rs_usulanSPJM = UsulanSPJMPeer::doSelect($c);

        $this->rs_usulanSPJM = $rs_usulanSPJM;
    }


    public function executeBagiusulanssh() {
        $c = new Criteria();
        $c->addDescendingOrderByColumn(UsulanSPJMPeer::ID_SPJM);
        $c->add(UsulanSPJMPeer::STATUS_HAPUS, FALSE);
        $c->addAnd(UsulanSPJMPeer::STATUS_VERIFIKASI, FALSE);
        
        
        $rs_usulanSPJM = UsulanSPJMPeer::doSelect($c);

        $this->rs_usulanSPJM = $rs_usulanSPJM;
    }


    public function executeUbahbagiusulanssh() {
        $c = new Criteria();
        $c->addDescendingOrderByColumn(UsulanSPJMPeer::ID_SPJM);
        $c->add(UsulanSPJMPeer::STATUS_HAPUS, FALSE);
        $c->addAnd(UsulanSPJMPeer::STATUS_VERIFIKASI, FALSE);
        
        
        $rs_usulanSPJM = UsulanSPJMPeer::doSelect($c);

        $this->rs_usulanSPJM = $rs_usulanSPJM;
    }

    //ticket #4 - list SPJM + usulan SSH
    //ticket #4 - droplist usulan SSH di halaman list SPJM
    public function executeGetNamaSurveyor() {
        $id_usulan = $this->getRequestParameter('id');
        $surveyor = $this->getRequestParameter('surveyor');

        $c = new Criteria();
        $c->add(UsulanSSHPeer::ID_USULAN, $id_usulan);
        $rs = UsulanSSHPeer::doSelectOne($c);
        $rs->setSurveyor($surveyor);
        $rs->save();

        $this->setFlash('berhasil', 'Pembagian Survey Telah Disimpan.');
        return $this->redirect("usulan_ssh/bagiusulanssh");

        // $con = Propel::getConnection();
        // $con->begin();
        // if ($this->getRequestParameter('id')) {
        //     $id_usulan = $this->getRequestParameter('id');
        //     $surveyor = $this->getRequestParameter('surveyor');
            
        //     $u = "UPDATE " . sfConfig::get('app_default_schema') . ".usulan_ssh "
        //                 . "SET surveyor = '$surveyor' "
        //                 . "WHERE id_usulan = $id_usulan";
        //     $stmt1 = $con->prepareStatement($u);
        //     $stmt1->executeQuery();
              
     
        // }
    }
    
    public function executeSetNamaSurveyor() {
        $id_spjm = $this->getRequestParameter('id');
        $surveyor = $this->getRequestParameter('surveyor');

        // $c = new Criteria();
        // $c->add(UsulanSSHPeer::ID_SPJM, $id_usulan);
        // $c->add(UsulanSSHPeer::SURVEYOR, Criteria::ISNULL);
        // $rs = UsulanSSHPeer::doSelectOne($c);
        // $rs->setSurveyor($surveyor);
        // $rs->save();

        $con = Propel::getConnection();
        $con->begin();
        // $data=array();

        try{
            $query_update = "UPDATE ebudget.usulan_ssh SET surveyor='".$surveyor."' WHERE id_spjm=".$id_spjm." AND surveyor is null";
            $prosesSimpan = $con->prepareStatement($query_update);
            $prosesSimpan->executeQuery();
            $con->commit();

            // $data['sukses']=1;

            $this->setFlash('berhasil', 'Pembagian Survey Telah Disimpan.');
            return $this->redirect("usulan_ssh/bagiusulanssh");
        }catch (Exception $ex) {
            $con->rollback();
            // $data['sukses']=0;
            $this->setFlash("gagal", "Pembagian Survey Gagal Disimpan.");
            return $this->redirect('usulan_ssh/bagiusulanssh');
        }
        // return json_encode($data);
        

        // $con = Propel::getConnection();
        // $con->begin();
        // if ($this->getRequestParameter('id')) {
        //     $id_usulan = $this->getRequestParameter('id');
        //     $surveyor = $this->getRequestParameter('surveyor');
            
        //     $u = "UPDATE " . sfConfig::get('app_default_schema') . ".usulan_ssh "
        //                 . "SET surveyor = '$surveyor' "
        //                 . "WHERE id_usulan = $id_usulan";
        //     $stmt1 = $con->prepareStatement($u);
        //     $stmt1->executeQuery();
              
     
        // }
    }



    public function executeGetUsulan() {
        if ($this->getRequestParameter('id')) {
            $id_spjm = $this->getRequestParameter('id');

            $c = new Criteria();
            $c->add(UsulanSSHPeer::ID_SPJM, $id_spjm, Criteria::EQUAL);
            $c->addAnd(UsulanSSHPeer::STATUS_HAPUS, FALSE, Criteria::EQUAL);
            $c->addAnd(UsulanSSHPeer::STATUS_SURVEY, TRUE, Criteria::EQUAL);
            $c->addAnd(UsulanSSHPeer::STATUS_VERIFIKASI, FALSE, Criteria::EQUAL);
            $c->addAnd(UsulanSSHPeer::STATUS_PENDING, FALSE, Criteria::EQUAL);
            $c->addAscendingOrderByColumn(UsulanSSHPeer::STATUS_HAPUS);
            $c->addAscendingOrderByColumn(UsulanSSHPeer::STATUS_VERIFIKASI);
            $c->addAscendingOrderByColumn(UsulanSSHPeer::NAMA);
            $c->addAscendingOrderByColumn(UsulanSSHPeer::SPEC);
            $rs_usulan = UsulanSSHPeer::doSelect($c);
            $this->rs_usulan = $rs_usulan;

            $this->id = $id_spjm;
            $this->setLayout('kosong');
        }
    }

    public function executeGetUsulanAll() {
        if ($this->getRequestParameter('id')) {
            $id_spjm = $this->getRequestParameter('id');

            $c = new Criteria();
            $c->add(UsulanSSHPeer::ID_SPJM, $id_spjm, Criteria::EQUAL);
            $c->addAnd(UsulanSSHPeer::STATUS_VERIFIKASI, FALSE, Criteria::EQUAL);
            $c->addAnd(UsulanSSHPeer::STATUS_SURVEY, FALSE, Criteria::EQUAL);
            $c->addAnd(UsulanSSHPeer::STATUS_CONFIRMASI_PENYELIA, TRUE, Criteria::EQUAL);
            $c->addAscendingOrderByColumn(UsulanSSHPeer::STATUS_HAPUS);
            $c->addAscendingOrderByColumn(UsulanSSHPeer::STATUS_VERIFIKASI);
            $c->addAscendingOrderByColumn(UsulanSSHPeer::NAMA);
            $c->addAscendingOrderByColumn(UsulanSSHPeer::SPEC);
            $rs_usulan = UsulanSSHPeer::doSelect($c);
            $this->rs_usulan = $rs_usulan;

            $this->id = $id_spjm;
            $this->setLayout('kosong');
        }
    }

    public function executeGetUbahUsulanAll() {
        if ($this->getRequestParameter('id')) {
            $id_spjm = $this->getRequestParameter('id');

            $c = new Criteria();
            $c->add(UsulanSSHPeer::ID_SPJM, $id_spjm);
            $c->addAnd(UsulanSSHPeer::STATUS_VERIFIKASI, FALSE);
            $c->addAnd(UsulanSSHPeer::STATUS_SURVEY, FALSE);
            $c->addAnd(UsulanSSHPeer::STATUS_CONFIRMASI_PENYELIA, TRUE);
            $c->addAscendingOrderByColumn(UsulanSSHPeer::STATUS_HAPUS);
            $c->addAscendingOrderByColumn(UsulanSSHPeer::STATUS_VERIFIKASI);
            $c->addAscendingOrderByColumn(UsulanSSHPeer::NAMA);
            $c->addAscendingOrderByColumn(UsulanSSHPeer::SPEC);
            $rs_usulan = UsulanSSHPeer::doSelect($c);
            $this->rs_usulan = $rs_usulan;

            $this->id = $id_spjm;
            $this->setLayout('kosong');
        }
    }
//    ticket #6 - saving konfirmasi 
    //ticket #4 - list SPJM + usulan SSH
    public function executeUsulansshpending() {
        $c = new Criteria();
        $c->add(UsulanSPJMPeer::STATUS_HAPUS, FALSE);
        $c->addAnd(UsulanSPJMPeer::STATUS_VERIFIKASI, FALSE);
        $c->addAscendingOrderByColumn(UsulanSPJMPeer::ID_SPJM);
        $rs_usulanSPJM = UsulanSPJMPeer::doSelect($c);

        $this->rs_usulanSPJM = $rs_usulanSPJM;
    }

    //ticket #4 - list SPJM + usulan SSH
    //ticket #4 - droplist usulan SSH di halaman list SPJM
    public function executeGetUsulanPending() {
        if ($this->getRequestParameter('id')) {
            $id_spjm = $this->getRequestParameter('id');

            $c = new Criteria();
            $c->add(UsulanSSHPeer::ID_SPJM, $id_spjm, Criteria::EQUAL);
            $c->addAnd(UsulanSSHPeer::STATUS_HAPUS, FALSE, Criteria::EQUAL);
            $c->addAnd(UsulanSSHPeer::STATUS_VERIFIKASI, FALSE, Criteria::EQUAL);
            $c->addAnd(UsulanSSHPeer::STATUS_PENDING, TRUE, Criteria::EQUAL);
            $c->addAscendingOrderByColumn(UsulanSSHPeer::STATUS_HAPUS);
            $c->addAscendingOrderByColumn(UsulanSSHPeer::STATUS_VERIFIKASI);
            $c->addAscendingOrderByColumn(UsulanSSHPeer::NAMA);
            $c->addAscendingOrderByColumn(UsulanSSHPeer::SPEC);
            $rs_usulan = UsulanSSHPeer::doSelect($c);
            $this->rs_usulan = $rs_usulan;

            $this->id = $id_spjm;
            $this->setLayout('kosong');
        }
    }

    public function executeGetUsulanDinas() {
        if ($this->getRequestParameter('id')) {
            $id_spjm = $this->getRequestParameter('id');

            $c = new Criteria();
            $c->addDescendingOrderByColumn(UsulanSSHPeer::UPDATED_AT);
            $c->addJoin(UsulanSSHPeer::UNIT_ID, UnitKerjaPeer::UNIT_ID, Criteria::INNER_JOIN);
            $c->addJoin(UsulanSSHPeer::ID_SPJM, UsulanSPJMPeer::ID_SPJM, Criteria::INNER_JOIN);
            if ($this->getUser()->getCredentialMember() == 'peneliti') {
                $nama = $this->getUser()->getNamaUser();
                $b = new Criteria;
                $b->add(UserHandleV2Peer::USER_ID, $nama);
                $satuan_kerja = UserHandleV2Peer::doSelect($b);
                foreach ($satuan_kerja as $x) {
                    $c->addOr(UnitKerjaPeer::UNIT_ID, $x->getUnitId(), Criteria::EQUAL);
                }
            } elseif ($dinas = $this->getUser()->getAttributeHolder()->getAll('dinas')) {
                $c->add(UnitKerjaPeer::UNIT_ID, $dinas['unit_id'], Criteria::EQUAL);
            }
            $c->add(UsulanSSHPeer::ID_SPJM, $id_spjm, Criteria::EQUAL);
            $rs_usulan = UsulanSSHPeer::doSelect($c);
            $this->rs_usulan = $rs_usulan;

            $this->id = $id_spjm;
            $this->setLayout('kosong');
        }
    }

    //ticket #4 - droplist usulan SSH di halaman list SPJM
    //ticket #4 - hapus SPJM + seluruh usulan SSH didalamnya
    function executeHapusSPJM() {
        $con = Propel::getConnection();
        $con->begin();

        $id_spjm = $this->getRequestParameter('id');

        $key = $this->getRequestParameter('key');
        $md = md5('hapus_spjm');

        if ($key != $md) {
            $this->setFlash('gagal', 'Key Salah');
        } else {
            try {
                $sekarang = date('Y-m-d H:i:s');

                $hapus_spjm = "UPDATE " . sfConfig::get('app_default_schema') . ".usulan_spjm "
                        . "SET status_hapus = true, updated_at = '$sekarang' "
                        . "WHERE id_spjm = $id_spjm";
                $stmt_hapus_spjm = $con->prepareStatement($hapus_spjm);
                $stmt_hapus_spjm->executeQuery();

                $hapus_usulan = "UPDATE " . sfConfig::get('app_default_schema') . ".usulan_ssh "
                        . "SET status_hapus = true, updated_at = '$sekarang' "
                        . "WHERE id_spjm = $id_spjm";
                $stmt_hapus_usulan = $con->prepareStatement($hapus_usulan);
                $stmt_hapus_usulan->executeQuery();

                $c = new Criteria();
                $c->add(UsulanSPJMPeer::ID_SPJM, $id_spjm, Criteria::EQUAL);
                $rs_spjm = UsulanSPJMPeer::doSelectOne($c);

                $con->commit();

                //fungsi user_log
                $namaprofil = $this->getUser()->getNamaUser();
                $username = $this->getUser()->getNamaLogin();
                budgetLogger::log($username . ' menghapus Usulan SPJM id :' . $id_spjm . ' pada dinas :' . $rs_spjm->getSkpd());
                //fungsi user_log

                $this->setFlash('berhasil', 'Berhasil menghapus usulan SPJM');
            } catch (Exception $exc) {
                $con->rollback();
                $this->setFlash('gagal', $exc->getMessage());
            }
        }

        return $this->redirect('usulan_ssh/usulanssh');
    }

    //ticket #4 - hapus SPJM + seluruh usulan SSH didalamnya
    //ticket #5 - form verifikasi
    function executeUsulansshcek() {
        $con = Propel::getConnection();

        $id_usulan = $this->getRequestParameter('id');

        $key = $this->getRequestParameter('key');
        $md = md5('cek_ssh');

        if ($key != $md) {
            $this->setFlash('gagal', 'Key Salah');
            return $this->redirect('usulan_ssh/usulanssh');
        } else {
            try {
                $this->sekarang = $sekarang = date('Y-m-d H:i:s');
//                data usulan
                $c = new Criteria();
                $c->add(UsulanSSHPeer::ID_USULAN, $id_usulan, Criteria::EQUAL);
                $rs_usulan = UsulanSSHPeer::doSelectOne($c);
                $this->data_usulan_ssh = $rs_usulan;
//                data shsd survey
                $c1 = new Criteria();
                $c1->add(ShsdSurveyPeer::SHSD_ID, $id_usulan, Criteria::EQUAL);
                $rs_shsd = ShsdSurveyPeer::doSelectOne($c1);
                $this->data_shsd = $rs_shsd;
//                data usulan
//                list satuan                
                $c_all_satuan = new Criteria();
                $c_all_satuan->addAscendingOrderByColumn(SatuanPeer::SATUAN_NAME);
                $rs_all_satuan = SatuanPeer::doSelect($c_all_satuan);
                $this->list_satuan = $rs_all_satuan;
//                list satuan
//              list tahap
                $c_all_tahap = new Criteria();
                $c_all_tahap->addAscendingOrderByColumn(MasterTahapPeer::TAHAP_ID);
                $rs_all_tahap = MasterTahapPeer::doSelect($c_all_tahap);
                $this->list_tahap = $rs_all_tahap;
//
//                list rekening
                $c_all_rekening = new Criteria();
                $c_all_rekening->addAscendingOrderByColumn(RekeningPeer::REKENING_CODE);
                $rs_all_rekening = RekeningPeer::doSelect($c_all_rekening);
                $this->list_rekening = $rs_all_rekening;
//                list rekening
//                kode barang dengan 'X.X.X.X'
                $query_all_kode_barang = "select * "
                        . "from " . sfConfig::get('app_default_schema') . ".kategori_shsd "
                        . "where kategori_shsd_id ilike '%.%.%.%' "
                        . "order by kategori_shsd_id ";
                $stmt_all_kode_barang = $con->prepareStatement($query_all_kode_barang);
                $rs_all_kode_barang = $stmt_all_kode_barang->executeQuery();
                $this->list_kode_barang = $rs_all_kode_barang;
//                kode barang dengan 'X.X.X.X'
            } catch (Exception $exc) {
                $this->setFlash('gagal', $exc->getMessage());
                return $this->redirect('usulan_ssh/usulanssh');
            }
        }
    }

    //ticket #5 - form verifikasi
    //ticket #5 - droplist SSH yang berada pada satu kode barang yang sama
    public function executeGetSSHSatuKodeBarang() {
        if ($this->getRequestParameter('id')) {
            $id_usulan = $this->getRequestParameter('id');

            $c = new Criteria();
            $c->add(UsulanSSHPeer::ID_USULAN, $id_usulan, Criteria::EQUAL);
            $rs_usulan = UsulanSSHPeer::doSelectOne($c);

            $con = Propel::getConnection();
            $query_sama_kode = "select ka.komponen_tipe, ka.komponen_id, ka.komponen_name, ka.spec, ka.hidden_spec, ka.merk, st.satuan_name, ka.komponen_harga, ka.non_pajak, ka.usulan_skpd "
                    . "from " . sfConfig::get('app_default_schema') . ".komponen_all ka "
                    . "INNER JOIN ebudget.satuan st ON ka.satuan_id = st.satuan_id where komponen_id ilike '" . $rs_usulan->getKodeBarang() . "%' "
                    . "order by komponen_id DESC ";
            $stmt_sama_kode = $con->prepareStatement($query_sama_kode);
            $rs_sama_kode = $stmt_sama_kode->executeQuery();
            $this->list_ssh_satu_kode_barang = $rs_sama_kode;

            $this->id = $id_usulan;
            $this->setLayout('kosong');
        }
    }

    //ticket #5 - droplist SSH yang berada pada satu kode barang yang sama
    //ticket #5 - proses saving verifikasi
    function executeSavesshusulan() {
        $con = Propel::getConnection();

        $sekarang = date('Y-m-d H:i:s');
        $a=$this->getRequestParameter('spec');
        $nama = ucwords($this->getRequestParameter('nama')); 
        //cek jika spek mengandung angka
        if (preg_match('/[0-9]/', $a) !== false) {
             
             $spec=$this->getRequestParameter('spec');
             $nama_ssh =  $nama.' '.$spec;
        }
        else
        {
            $spec = ucwords($this->getRequestParameter('spec'));
            $nama_ssh = ucwords($this->getRequestParameter('nama_ssh'));
        }
        $id_usulan = $this->getRequestParameter('id_usulan');
        $id_spjm = $this->getRequestParameter('id_spjm');
        $tipe = $this->getRequestParameter('tipe');
        $kode_barang = $this->getRequestParameter('kode_barang');
        $id_ssh = trim($this->getRequestParameter('id_ssh'));       
               
        $hidden_spec = ucwords($this->getRequestParameter('hidden_spec'));
        $merek = ucwords($this->getRequestParameter('merek'));
        $satuan = $this->getRequestParameter('satuan');
        $harga = str_replace(',', '.', $this->getRequestParameter('harga'));
        $pajak = $this->getRequestParameter('pajak');
        $keterangan = $this->getRequestParameter('keterangan');
        $rekening = $this->getRequestParameter('rekening');
        $skpd = $this->getRequestParameter('skpd');
        $is_iuran_bpjs = $this->getRequestParameter('is_iuran_bpjs');
        $is_potong_bpjs = $this->getRequestParameter('is_potong_bpjs');
        $komponen_tipe2 = $this->getRequestParameter('komponen_tipe2');
        $tahap = $this->getRequestParameter('tahap');
        $tahap_buku = $this->getRequestParameter('tahap_buku');
        $user_id = $this->getRequestParameter('id_user');

        $id_komponen_perubahan = $this->getRequestParameter('id_komponen_perubahan');
        $alasan_perubahan = $this->getRequestParameter('alasan_perubahan');
        $is_perubahan_harga = $this->getRequestParameter('is_perubahan_harga');
        $nama_sebelum = $this->getRequestParameter('nama_sebelum');
        $harga_sebelum = $this->getRequestParameter('harga_sebelum');

        $alasan_perbedaan_dinas = $this->getRequestParameter('alasan_perbedaan_dinas');
        $alasan_perbedaan_penyelia = $this->getRequestParameter('alasan_perbedaan_penyelia');
        $is_perbedaan_pendukung = $this->getRequestParameter('is_perbedaan_pendukung');
        //die($id_spjm.'-'.$id_usulan);
        if (($is_perubahan_harga == 0 && $id_komponen_perubahan <> '') || ($is_perubahan_harga == 1 && $id_komponen_perubahan == '')) {
            $this->setFlash('gagal', 'Periksa untuk id Perubahan Harga dan isian iya/tidak perubahan harga');
            return $this->redirect('usulan_ssh/usulansshcek?id=' . $id_usulan . '&key=' . md5('cek_ssh'));
        }

        if (($is_perbedaan_pendukung == 0 && $alasan_perbedaan_penyelia <> '' && $alasan_perbedaan_dinas <> '') || ($is_perbedaan_pendukung == 1 && $alasan_perbedaan_penyelia == '' && $alasan_perbedaan_dinas == '')) {
            $this->setFlash('gagal', 'Periksa untuk perbedaan pendukung');
            return $this->redirect('usulan_ssh/usulansshcek?id=' . $id_usulan . '&key=' . md5('cek_ssh'));
        }

         if ($tahap_buku == '' || is_null($tahap_buku)) {
            $this->setFlash('gagal', 'Tahap Buku Tidak Boleh Kosong');
            return $this->redirect('usulan_ssh/usulansshcek?id=' . $id_usulan . '&key=' . md5('cek_ssh'));
        }

        if ($tipe == 'SSH' ||$tipe == 'EST') {
            $shsd_id = $kode_barang . '.' . $id_ssh;
        } else {
            $shsd_id = strtoupper($id_ssh);
        }

        if ($kode_barang == '2.1.2.01.01.01.001') {
            $shsd_id = strtoupper( $kode_barang . '.' . $id_ssh);
            $tipe = 'EST';
        }
        
        $con->begin();
        try {
            //cek count berdasarkan shsd_id dari tabel shsd
            $c_cek_ssh = new Criteria();
            $c_cek_ssh->add(KomponenAllPeer::KOMPONEN_ID, $shsd_id, Criteria::EQUAL);
            $count_cek_ssh = KomponenAllPeer::doCount($c_cek_ssh);
            //cek count berdasarkan shsd_id dari tabel shsd
            //cek count berdasarkan komponen_id dari tabel komponen
            $c_cek_komponen = new Criteria();
            $c_cek_komponen->add(KomponenPeer::KOMPONEN_ID, $shsd_id, Criteria::EQUAL);
            $count_cek_komponen = KomponenPeer::doCount($c_cek_komponen);
            //cek count berdasarkan komponen_id dari tabel komponen

            // $c = new Criteria();
            // $c->add(SatuanPeer::SATUAN_NAME, $satuan, Criteria::EQUAL);
            // $data_satuan = SatuanPeer::doSelectOne($c);
            // $satuan_id = $data_satuan->getSatuanId();

            $query_cek_satuan = "SELECT satuan_id FROM " . sfConfig::get('app_default_schema') . ".satuan WHERE satuan_name = '$satuan'";
            $stmt_cek_satuan = $con->prepareStatement($query_cek_satuan);
            $rsatuan = $stmt_cek_satuan->executeQuery();
            while ($rsatuan->next()) {
                $satuan_id = $rsatuan->getInt('satuan_id');
            }

            //cek satuan dari tabel satuan 
            if ($count_cek_ssh == 0 && $count_cek_komponen == 0) {

                //save usulan_verifikasi

                $verifikasi_baru = new UsulanVerifikasi();
                $verifikasi_baru->setShsdId($shsd_id);
                $verifikasi_baru->setShsdName($nama_ssh);
                $verifikasi_baru->setHarga($harga);
                $verifikasi_baru->setNama($nama);
                $verifikasi_baru->setSpec($spec);
                $verifikasi_baru->setHiddenSpec($hidden_spec);
                $verifikasi_baru->setMerek($merek);
                $verifikasi_baru->setSatuan($satuan);
                $verifikasi_baru->setRekening($rekening);
                $verifikasi_baru->setIdSpjm($id_spjm);
                $verifikasi_baru->setIdUsulan($id_usulan);
                $verifikasi_baru->setUnitId($skpd);
                $verifikasi_baru->setPajak($pajak);
                $verifikasi_baru->setKeterangan($keterangan);
                $verifikasi_baru->setCreatedAt($sekarang);
                $verifikasi_baru->setTipe($tipe);
                $verifikasi_baru->setTahapBuku($tahap_buku);
                $verifikasi_baru->save();
                //save usulan_verifikasi 
//                jikalau SSH
                if ($tipe == 'SSH') {
                    //save ssh
                    $ssh_baru = new KomponenAll();
                    $ssh_baru->setKomponenId($shsd_id);
                    $ssh_baru->setKomponenName($nama_ssh);
                    $ssh_baru->setSatuanId($satuan_id);
                    $ssh_baru->setUserId($user_id);
                    $ssh_baru->setNamaDasar($nama);
                    $ssh_baru->setSpec($spec);
                    $ssh_baru->setHiddenSpec($hidden_spec);
                    $ssh_baru->setMerk($merek);
                    $ssh_baru->setKomponenHarga($harga);
                    if ($pajak == 10) {
                        $ssh_baru->setNonPajak(FALSE);
                    } else {
                        $ssh_baru->setNonPajak(TRUE);
                    }
                    if ($is_potong_bpjs == 'iya') {
                        $ssh_baru->setIsPotongBpjs(TRUE);
                    } else {
                        $ssh_baru->setIsPotongBpjs(FALSE);
                    }
                    if ($is_iuran_bpjs == 'iya') {
                        $ssh_baru->setIsIuranBpjs(TRUE);
                    } else {
                        $ssh_baru->setIsIuranBpjs(FALSE);
                    }
                    $ssh_baru->setUsulanSkpd(UnitKerjaPeer::getStringUnitKerja($skpd));
                    $ssh_baru->setKomponenLocked(TRUE);
                    $ssh_baru->setKomponenTipe('SHSD');
                    $ssh_baru->setTahapId($tahap);
                    $ssh_baru->setTahapBuku($tahap_buku);
                    $ssh_baru->save();

                }
                else if ($tipe == 'EST') {
                    //save estimasi
                    $ssh_baru = new KomponenAll();
                    $ssh_baru->setKomponenId($shsd_id);
                    $ssh_baru->setKomponenName($nama_ssh);
                    $ssh_baru->setSatuanId($satuan_id);
                    $ssh_baru->setUserId($user_id);
                    $ssh_baru->setNamaDasar($nama);
                    $ssh_baru->setSpec($spec);
                    $ssh_baru->setHiddenSpec($hidden_spec);
                    $ssh_baru->setMerk($merek);
                    $ssh_baru->setKomponenHarga($harga);
                    if ($pajak == 10) {
                        $ssh_baru->setNonPajak(FALSE);
                    } else {
                        $ssh_baru->setNonPajak(TRUE);
                    }
                    if ($is_potong_bpjs == 'iya') {
                        $ssh_baru->setIsPotongBpjs(TRUE);
                    } else {
                        $ssh_baru->setIsPotongBpjs(FALSE);
                    }
                    if ($is_iuran_bpjs == 'iya') {
                        $ssh_baru->setIsIuranBpjs(TRUE);
                    } else {
                        $ssh_baru->setIsIuranBpjs(FALSE);
                    }
                    $ssh_baru->setUsulanSkpd(UnitKerjaPeer::getStringUnitKerja($skpd));
                    $ssh_baru->setKomponenLocked(TRUE);
                    $ssh_baru->setKomponenTipe('EST');
                    $ssh_baru->setTahapId($tahap);
                    $ssh_baru->setTahapBuku($tahap_buku);
                    $ssh_baru->save();
                }
//                jikalau SSH                
                //save komponen
                $komponen_baru = new Komponen();
                $komponen_baru->setKomponenId($shsd_id);
                if ($tipe == 'SSH') {
                    $komponen_baru->setShsdId($shsd_id);
                    $komponen_baru->setKomponenTipe('SHSD');
                } else {
                    $komponen_baru->setKomponenTipe('EST');
                }
                $komponen_baru->setKomponenName($nama_ssh);
                $komponen_baru->setSatuan($satuan);
                $komponen_baru->setRekening($rekening);
                $komponen_baru->setKomponenHarga($harga);
                $komponen_baru->setKomponenShow(TRUE);
                $komponen_baru->setKomponenConfirmed(TRUE);
                $komponen_baru->setWaktuAccess($sekarang);
                $komponen_baru->setStatusMasuk('baru');
                $komponen_baru->setUsulanSkpd(UnitKerjaPeer::getStringUnitKerja($skpd));
                if ($pajak == 10) {
                    $komponen_baru->setKomponenNonPajak(FALSE);
                } else {
                    $komponen_baru->setKomponenNonPajak(TRUE);
                }
                if ($is_potong_bpjs == 'iya') {
                    $komponen_baru->setIsPotongBpjs(TRUE);
                } else {
                    $komponen_baru->setIsPotongBpjs(FALSE);
                }
                if ($is_iuran_bpjs == 'iya') {
                    $komponen_baru->setIsIuranBpjs(TRUE);
                } else {
                    $komponen_baru->setIsIuranBpjs(FALSE);
                }
                if ($kode_barang == '23.06.01.01') {
                    $komponen_baru->setIsEstFisik(TRUE);
                }
                $komponen_baru->setTahap($tahap);
                $komponen_baru->setTahapBuku($tahap_buku);
                $komponen_baru->setKomponenTipe2($komponen_tipe2);
                $komponen_baru->save();
                //save komponen
                //save komponen_rekening
                $komponen_rekening = new KomponenRekening();
                $komponen_rekening->setKomponenId($shsd_id);
                $komponen_rekening->setRekeningCode($rekening);
                $komponen_rekening->save();
                //save komponen_rekening
                $c_update_usulan_ssh = new Criteria();
                $c_update_usulan_ssh->add(UsulanSSHPeer::ID_USULAN, $id_usulan, Criteria::EQUAL);
                $data_usulan_ssh = UsulanSSHPeer::doSelectOne($c_update_usulan_ssh);
                $data_usulan_ssh->setStatusVerifikasi(TRUE);
                $data_usulan_ssh->setUpdatedAt($sekarang);
                $data_usulan_ssh->setShsdId($shsd_id);
                $data_usulan_ssh->setAlasanPerubahanHarga($alasan_perubahan);
                $data_usulan_ssh->setNamaSebelum($nama_sebelum);
                $data_usulan_ssh->setHargaSebelum($harga_sebelum);
                if ($is_perubahan_harga == 'iya' || ($nama_sebelum <> '' && $harga_sebelum <> '')) {
                    $data_usulan_ssh->setIsPerubahanHarga(TRUE);
                }
                $data_usulan_ssh->save();

                
                //update tabel shsd_survey
                $total_cek_id = 0;
                $query_cek_idusulan = "select count(*) as id from " . sfConfig::get('app_default_schema') . ".shsd_survey "
                        . "WHERE shsd_id = '$id_usulan'";
                $stmt_cek_usulan_idusulan = $con->prepareStatement($query_cek_idusulan);
                $rs2 = $stmt_cek_usulan_idusulan->executeQuery();
                while ($rs2->next()) {
                    $total_cek_id = $rs2->getString('id');
                }
                if ($total_cek_id > 0) {
                    $query_update_usulan_idshsd = "update " . sfConfig::get('app_default_schema') . ".shsd_survey "
                            . "set shsd_id = '$shsd_id', status_verifikasi = true "
                            . "where shsd_id = '$id_usulan' ";
                    $stmt_cek_update_idshsd = $con->prepareStatement($query_update_usulan_idshsd);
                    $stmt_cek_update_idshsd->executeQuery();
                }
                //cek count berdasarkan id_spjm dari tabel usulan_ssh                
                $total_cek_nomor = 0;
                $query_cek_usulan_idspjm = "select count(*) as total from " . sfConfig::get('app_default_schema') . ".usulan_ssh "
                        . "where status_hapus = false and status_verifikasi = false and shsd_id is null "
                        . "and id_spjm = '$id_spjm'";
                $stmt_cek_usulan_idspjm = $con->prepareStatement($query_cek_usulan_idspjm);
                $rs1 = $stmt_cek_usulan_idspjm->executeQuery();
                while ($rs1->next()) {
                    $total_cek_nomor = $rs1->getString('total');
                }
                if ($total_cek_nomor == 0) {
                    $query_update_usulan_idspjm = "update " . sfConfig::get('app_default_schema') . ".usulan_spjm "
                            . "set updated_at = '$sekarang', status_verifikasi = true "
                            . "where id_spjm = '$id_spjm' ";
                    $stmt_cek_update_idspjm = $con->prepareStatement($query_update_usulan_idspjm);
                    $stmt_cek_update_idspjm->executeQuery();
                }

                //cek count berdasarkan id_spjm dari tabel usulan_ssh
                if ($is_perubahan_harga == 'iya' && $id_komponen_perubahan <> '') {

                    if ($nama_sebelum == '' || $harga_sebelum == '') {
                        $c_cek_komponen = new Criteria();
                        $c_cek_komponen->add(KomponenPeer::KOMPONEN_ID, $id_komponen_perubahan, Criteria::EQUAL);
                        $data_komponen = KomponenPeer::doSelectOne($c_cek_komponen);
                        $nama_lama = $data_komponen->getKomponenName();
                        $harga_lama = $data_komponen->getKomponenHarga();

                        $query_update_konfirmasi_usulan_ssh = "update " . sfConfig::get('app_default_schema') . ".usulan_ssh "
                                . "set nama_sebelum = '$nama_lama', harga_sebelum = $harga_lama "
                                . "where id_usulan = $id_usulan";
                        $stmt_update_konfirmasi_usulan_ssh = $con->prepareStatement($query_update_konfirmasi_usulan_ssh);
                        $stmt_update_konfirmasi_usulan_ssh->executeQuery();
                    }

                    $query_delete_komponen_rekening = "delete from " . sfConfig::get('app_default_schema') . ".komponen_rekening "
                            . "where komponen_id = '$id_komponen_perubahan'";
                    $stmt_delete_komponen_rekening = $con->prepareStatement($query_delete_komponen_rekening);
                    $stmt_delete_komponen_rekening->executeQuery();
                }
                //fungsi user_log
                $namaprofil = $this->getUser()->getNamaUser();
                $username = $this->getUser()->getNamaLogin();
                budgetLogger::log($username . ' MEMBUAT ' . $tipe . ' baru dengan kode: ' . $shsd_id . ' --  nama: ' . $nama_ssh);
                //fungsi user_log    
                $con->commit();
                $this->setFlash('berhasil', 'Berhasil membuat ' . $tipe . ' baru dengan kode : ' . $shsd_id . ' nama : ' . $nama_ssh);
            } else {
                $con->rollback();
                $this->setFlash('gagal', 'ID untuk ' . $shsd_id . ' telah digunakan');
                return $this->redirect('usulan_ssh/usulansshcek?id=' . $id_usulan . '&key=' . md5('cek_ssh'));
            }
        } catch (Exception $exc) {
            $con->rollback();
            $this->setFlash('gagal', 'Terjadi kesalahan ' . $exc->getMessage());
            return $this->redirect('usulan_ssh/usulansshcek?id=' . $id_usulan . '&key=' . md5('cek_ssh'));
        }
        return $this->redirect('usulan_ssh/usulanssh');
    }

//  ticket #5 - proses saving verifikasi
// ticket #16 - halaman edit SPJM
    public function executeUsulaneditSPJM() {
        $id_spjm = $this->getRequestParameter('id');

        $c_spjm = new Criteria();
        $c_spjm->add(UsulanSPJMPeer::ID_SPJM, $id_spjm, Criteria::EQUAL);
        $this->data_spjm = $data_spjm = UsulanSPJMPeer::doSelectOne($c_spjm);

        $c_usulan = new Criteria();
        $c_usulan->add(UsulanSSHPeer::ID_SPJM, $id_spjm, Criteria::EQUAL);
        $c_usulan->addAscendingOrderByColumn(UsulanSSHPeer::NAMA);
        $this->list_usulan = $list_usulan = UsulanSSHPeer::doSelect($c_usulan);
    }

    //    ticket #16 - halaman edit SPJM
    //    ticket #16 - save edit SPJM
    function executeUpdateSPJM() {
        $jumlah_dukungan = $this->getRequestParameter('jumlah_dukungan');
        $nomor_surat = $this->getRequestParameter('nomor_surat');
        $komentar_verifikator = $this->getRequestParameter('komentar_verifikator');
        $id_spjm = $this->getRequestParameter('id_spjm');

        $con = Propel::getConnection();
        $sekarang = date('Y-m-d H:i:s');
        $con->begin();
        try {
            $query_update_spjm = "update " . sfConfig::get('app_default_schema') . ".usulan_spjm "
                    . "set nomor_surat = '$nomor_surat', "
                    . "jumlah_dukungan = $jumlah_dukungan, "
                    . "komentar_verifikator = $komentar_verifikator, "
                    . "updated_at = '$sekarang' "
                    . "where id_spjm = $id_spjm";
            $stmt_update_spjm = $con->prepareStatement($query_update_spjm);
            $stmt_update_spjm->executeQuery();

            //fungsi user_log
            $namaprofil = $this->getUser()->getNamaUser();
            $username = $this->getUser()->getNamaLogin();
            budgetLogger::log($username . ' mengubah SPJM dengan id : ' . $id_spjm);
            //fungsi user_log

            $con->commit();

            $this->setFlash('berhasil', 'Berhasil mengubah SPJM dengan id : ' . $id_spjm);
            return $this->redirect('usulan_ssh/usulantoken');
        } catch (Exception $exc) {
            $con->rollback();
            echo $exc->getMessage();
        }
    }

    //    ticket #16 - save edit SPJM

    function executeHapusToken() {
        $token = $this->getRequestParameter('token');
        $con = Propel::getConnection();
        $con->begin();
        try {
            $query_delete_token = "delete from " . sfConfig::get('app_default_schema') . ".usulan_token "
                    . "where token = '$token' ";
            $stmt_delete_token = $con->prepareStatement($query_delete_token);
            $stmt_delete_token->executeQuery();

            //fungsi user_log
            $namaprofil = $this->getUser()->getNamaUser();
            $username = $this->getUser()->getNamaLogin();
            budgetLogger::log($username . ' menghapus token : ' . $token);
            //fungsi user_log
            $con->commit();
            $this->setFlash('berhasil', 'Berhasil menghapus token : ' . $token);
        } catch (Exception $exc) {
            $con->rollback();
            $this->setFlash('gagal', 'Eror : ' . $exc->getMessage());
        }
        return $this->redirect('usulan_ssh/usulantoken');
    }

//    ticket #39 - view saving usulan komponen
//    created at 25juni2015
    public function executeUsulandinas() {
        
    }

//    ticket #39 - view saving usulan komponen
    //    ticket #39 - proses saving usulan komponen
//    created at 25juni2015    
    function executeSaveusulandinas() {
        set_time_limit(0);
        $sekarang = date('Y-m-d H:i:s');

//        ambil parameter
        $tanggal_spjm = $this->getRequestParameter('tanggal_spjm');
        $user_dinas = trim($this->getRequestParameter('user_dinas'));
        $skpd = trim($this->getRequestParameter('skpd'));
        $jenis_usulan = $this->getRequestParameter('jenis_usulan');
        $jumlah_dukungan = trim($this->getRequestParameter('jumlah_dukungan'));
        $jumlah_komponen = trim($this->getRequestParameter('jumlah_komponen'));
        $nomor_surat = trim($this->getRequestParameter('nomor_surat'));
        $keterangan_dinas = trim($this->getRequestParameter('keterangan_dinas'));
       // $f = strlen($keterangan_dinas);
        //die($keterangan_dinas.' '.$f);

        $nomor_surat_cek = str_replace(' ', '', $nomor_surat);
        $query_cek_nomor_surat = "select count(*) as total "
                . "from " . sfConfig::get('app_default_schema') . ".usulan_dinas "
                . "where replace(trim(nomor_surat),' ','') ilike '$nomor_surat_cek' "
                . "and status_hapus = false ";
        $con = Propel::getConnection();
        $stmt_cek_nomor_surat = $con->prepareStatement($query_cek_nomor_surat);
        $rs1 = $stmt_cek_nomor_surat->executeQuery();
        while ($rs1->next()) {
            $total_cek_nomor = $rs1->getString('total');
        }

        if ($total_cek_nomor > 0) {
            $this->setFlash('gagal', 'Gagal karena Nomor Surat telah diisikan');
            return $this->redirect('usulan_ssh/usulandinas');
        }

        if (strlen($keterangan_dinas) == 0) {
            $this->setFlash('gagal', 'Gagal karena Penjelasan Komponen belum diisi');
            return $this->redirect('usulan_ssh/usulandinas');
        }

//        ambil parameter
//        ambil kode unit_id
        $c_unit = new Criteria();
        $c_unit->add(UnitKerjaPeer::UNIT_NAME, $skpd, Criteria::EQUAL);
        $dapet_unit = UnitKerjaPeer::doSelectOne($c_unit);
        $unit_id = $dapet_unit->getUnitId();
//        ambil kode unit_id
//        validasi awal
        if (!$this->getRequest()->getFileName('file') || !$this->getRequest()->getFileName('file_spjm')) {
            $this->setFlash('gagal', 'File Upload Usulan Komponen & File Data Pendukung harus diupload.');
            return $this->redirect('usulan_ssh/usulandinas');
        }

//        if ($this->getRequest()->getFileSize('file_spjm') > 3000000) {
//            $this->setFlash('gagal', 'Size File Data Pendukung maksimal 3 mb. Upload file compress beberapa data agar < 3 mb. Untuk file compress data pendukung lengkap Silahkan mengirim ke Penyelia Anda.');
//            return $this->redirect('usulan_ssh/usulandinas');
//        }

        if (substr($this->getRequest()->getFileName('file'), -3) != 'xls' || (substr($this->getRequest()->getFileName('file_spjm'), -3) != 'zip' && substr($this->getRequest()->getFileName('file_spjm'), -3) != 'rar')) {
            $this->setFlash('gagal', 'File Upload Usulan Komponen gunakan format xls hasil download & File Data Pendukung gunakan format rar atau zip.');
            return $this->redirect('usulan_ssh/usulandinas');
        }
        if ($jumlah_dukungan == 0 || $jumlah_komponen == 0) {
            $this->setFlash('gagal', 'Harap isi Isian Jumlah Dukungan & Jumlah Komponen.');
            return $this->redirect('usulan_ssh/usulandinas');
        }

//        upload file
        $array_file_xls = explode('.', $this->getRequest()->getFileName('file'));
        $array_file_pdf = explode('.', $this->getRequest()->getFileName('file_spjm'));

        $fileName_xls = 'file_usulan_' . $user_dinas . '_' . date('Y-m-d_H-i') . '.' . $array_file_xls[count($array_file_xls) - 1];
        $fileName_compress = 'file_pendukung_' . $user_dinas . '_' . date('Y-m-d_H-i') . '.' . $array_file_pdf[count($array_file_pdf) - 1];

        $this->getRequest()->moveFile('file', sfConfig::get('sf_root_dir') . '/web/uploads/usulan_ssh/' . $fileName_xls);
        $this->getRequest()->moveFile('file_spjm', sfConfig::get('sf_root_dir') . '/web/uploads/usulan_pendukung/' . $fileName_compress);

        $file_path_xls = sfConfig::get('sf_root_dir') . '/web/uploads/usulan_ssh/' . $fileName_xls;
        $file_path_pdf = sfConfig::get('sf_root_dir') . '/web/uploads/usulan_pendukung/' . $fileName_compress;

        $saving_filepath_xls = sfConfig::get('app_path_uploadsUsulanSsh') . '' . $fileName_xls;
        $saving_filepath_pdf = sfConfig::get('app_path_uploadsUsulanPendukung') . '' . $fileName_compress;
//        upload file excel 
//        cek data upload
        if (!file_exists($file_path_xls) || !file_exists($file_path_pdf)) {
            if (!file_exists($file_path_xls)) {
                unlink(sfConfig::get('sf_root_dir') . '/web/uploads/usulan_ssh/' . $fileName_xls);
            }
            if (!file_exists($file_path_pdf)) {
                unlink(sfConfig::get('sf_root_dir') . '/web/uploads/usulan_pendukung/' . $fileName_compress);
            }
            $this->setFlash('gagal', 'CEK FILE ' . $fileName_xls . ' ' . $fileName_compress);
            return $this->redirect('usulan_ssh/usulandinas');
        }
//        cek data upload

        $objReader = new PHPExcel_Reader_Excel5;
        $objPHPExcel = $objReader->load($file_path_xls);

        $objPHPExcel->getActiveSheetIndex(0);
        $objWorksheet = $objPHPExcel->getActiveSheet();

        $highR = $objWorksheet->getHighestRow();

//        cek jumlah komponen usulan
        $total = $highR - 15;
        if ($jumlah_komponen <> $total) {
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/usulan_ssh/' . $fileName_xls);
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/usulan_pendukung/' . $fileName_compress);
            $this->setFlash('gagal', 'Jumlah isian Jumlah Komponen & Jumlah pada file excel usulan komponen berbeda');
            return $this->redirect('usulan_ssh/usulandinas');
        }
//        cek jumlah komponen usulan

        $salah = 0;
        $string_salah = '';
//            cek isian excel
        for ($row = 16; $row <= $highR; $row++) {
            $tipe = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(2, $row)->getValue()));
            $nama = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(3, $row)->getValue()));
            $spec = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(4, $row)->getValue()));    
            $hidden_spec = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(5, $row)->getValue()));          
            $merek = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(6, $row)->getValue()));
            $satuan = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(7, $row)->getValue()));
            $harga = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(8, $row)->getValue()));
            $rekening = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(9, $row)->getValue()));
            $pajak = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(10, $row)->getValue()));
            $keterangan = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(11, $row)->getValue()));
            $kode_barang = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(12, $row)->getValue()));
            $nama_sebelum = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(13, $row)->getValue()));
            $harga_sebelum = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(14, $row)->getValue()));
            $alasan_perubahan = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(15, $row)->getValue()));
            $harga_pendukung1 = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(16, $row)->getValue()));
            $cv1 = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(17, $row)->getValue()));
            $harga_pendukung2 = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(18, $row)->getValue()));
            $cv2 = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(19, $row)->getValue()));
            $harga_pendukung3 = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(20, $row)->getValue()));
            $cv3 = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(21, $row)->getValue()));

//                cek nama
            $array_kode_barang = explode(')', $kode_barang);
            $kode_barang_fresh = str_replace('(', '', $array_kode_barang[0]);
            // die('|'.$tipe.'|'.$nama.'|'.$spec.'|'.$hidden_spec.'|'.$merek.'|'.$satuan.'|'.$harga.'|'.$rekening.'|'.$pajak.'|'.$kode_barang_fresh.'|'.$kode_barang).'|';
            // die('Harga :'.$harga.' Harga Pendukung 1: '.$harga_pendukung1.' Harga dukung 2:'.$harga_pendukung2.' Harga dukung 3:'.$harga_pendukung3);
            $c = new Criteria();
            $c->add(KategoriShsdPeer::KATEGORI_SHSD_ID, $kode_barang_fresh);
            if (!KategoriShsdPeer::doSelectOne($c)) {
                $salah++;
                $string_salah = $string_salah . 'salah pengisian kode barang, baris ke ' . $row . '|(cek kategori shsd)';
                $kode_barang = 'SALAH';
            }

            if (trim($nama == '')) {
                $salah++;
                $string_salah = $string_salah . 'salah pengisian nama, baris ke ' . $row . '|';
                $nama = 'SALAH';
            }
//                cek nama
//                cek spec
            // if (trim($spec) == '') {
            //     $salah++;
            //     $string_salah = $string_salah . 'salah pengisian spec, baris ke ' . $row . '|';
            //     $spec = 'SALAH';
            // }
//                cek spec
//                cek hidden_spec
            if (trim($hidden_spec) == '') {
                $salah++;
                $string_salah = $string_salah . 'salah pengisian hidden_spec, baris ke ' . $row . '|';
                $hidden_spec = 'SALAH';
            }
//                cek hidden_spec
//                cek merek
            if (trim($merek) == '') {
                $salah++;
                $string_salah = $string_salah . 'salah pengisian merek, baris ke ' . $row . '|';
                $merek = 'SALAH';
            }
//                cek merek
//                cek satuan
            $c = new Criteria();
            $c->add(SatuanPeer::SATUAN_NAME, $satuan);
            if (!SatuanPeer::doSelectOne($c)) {
                $salah++;
                $string_salah = $string_salah . 'salah pengisian satuan, baris ke ' . $row . '|';
                $satuan = 'SALAH';
            }
//                cek satuan
//                cek keterangan
            if (trim($keterangan) == '') {
                $salah++;
                $string_salah = $string_salah . 'salah pengisian keterangan, baris ke ' . $row . '|';
                $keterangan = 'SALAH';
            }
//                cek keterangan
//                cek rekening
            $array_rekening = explode(')', $rekening);
            $kode_rekening = str_replace('(', '', $array_rekening[0]);

            if (count($array_rekening) == 1 || $rekening == '') {
                $salah++;
                $string_salah = $string_salah . 'salah pemilihan rekening, baris ke ' . $row . '|';
                $rekening = 'SALAH';
            }

            $c = new Criteria();
            $c->add(RekeningPeer::REKENING_CODE, $kode_rekening);
            if (!RekeningPeer::doSelectOne($c)) {
                $salah++;
                $string_salah = $string_salah . 'salah pemilihan rekening (karena rekening sudah tidak ada), baris ke ' . $row . '|';
                $rekening = 'SALAH';
            }
//                cek rekening
//                cek kode_barang
            $array_kode_barang = explode(')', $kode_barang);
            $kode_barang_fresh = str_replace('(', '', $array_kode_barang[0]);

            if (count($array_kode_barang) == 1 || $kode_barang_fresh == '') {
                $salah++;
                $string_salah = $string_salah . 'salah pemilihan kode barang, baris ke ' . $row . '|';
                $kode_barang_fresh = 'SALAH';
            }
//                cek kode_barang
//                cek tipe & kode_barang
            if ($tipe == '' || ($tipe <> 'SSH' && $tipe <> 'EST')) {
                $salah++;
                $string_salah = $string_salah . 'salah pemilihan kode barang dengan tipe, baris ke ' . $row . '|(tipe isian bukan SSH atau EST)';
                $tipe = 'SALAH';
            } else {
                if ($tipe == 'SSH' && $kode_barang_fresh == '2.1.2.01.01.01.001') {
                    $salah++;
                    $string_salah = $string_salah . 'salah pemilihan kode barang dengan tipe, baris ke ' . $row . '| (isi tipe SSH kode barang biaya estimasi (2.1.2.01.01.01.001) )';
                    $tipe = 'SALAH';
                } else if ($tipe == 'EST' && $kode_barang_fresh <> '2.1.2.01.01.01.001') {
                    $salah++;
                    $string_salah = $string_salah . 'salah pemilihan kode barang dengan tipe, baris ke ' . $row . '| (isi tipe EST tapi kode barang bukan kode biaya estimasi (2.1.2.01.01.01.001))';
                    $tipe = 'SALAH';
                }
            }
//                cek tipe & kode_barang
//                cek pajak                
            if ($pajak <> '0' && $pajak <> '10') {
                $salah++;
                $string_salah = $string_salah . 'salah pemilihan pajak, baris ke ' . $row . '|';
                $pajak = 'SALAH';
            }
//                cek pajak
//                cek harga
            $array_harga_koma = explode(',', $harga);
            $array_harga_titik = explode('.', $harga);
            if (count($array_harga_koma) > 1 || count($array_harga_titik) > 2 || $harga == '' || $harga == 0) {
                $salah++;
                $string_salah = $string_salah . 'salah penggunakan tanda koma (seharusnya tanda titik) dan pembilang ribuan tidak menggunakan tanda titik pada kolom harga, baris ke ' . $row . '|';
                $harga = 'SALAH';
            }
//                cek harga
//                cek perubahan harga 
            $array_perubahan_koma = explode(',', $harga_sebelum);
            $array_perubahan_titik = explode('.', $harga_sebelum);
            if ($harga_sebelum > 0) {
                if (count($array_harga_koma) > 1 || count($array_harga_titik) > 2 || $harga_sebelum == '') {
                    $salah++;
                    $string_salah = $string_salah . 'salah penggunakan tanda koma (seharusnya tanda titik) dan pembilang ribuan tidak menggunakan tanda titik pada kolom harga sebelum, baris ke ' . $row . '|';
                    $harga_sebelum = 'SALAH';
                }
                if (trim($alasan_perubahan) == '') {
                    $salah++;
                    $string_salah = $string_salah . 'Silahkan mengisi alasan perubahan harga untuk usulan perubahan harga komponen, baris ke ' . $row . '|';
                    $alasan_perubahan = 'SALAH';
                }
            }
//                cek perubahan harga 
//                cek harga pendukung
            $array_pendukung_koma1 = explode(',', $harga_pendukung1);
            $array_pendukung_titik1 = explode('.', $harga_pendukung1);

            if (count($array_pendukung_koma1) > 1 || count($array_pendukung_titik1) > 2 || $harga_pendukung1 == '') {
                $salah++;
                $string_salah = $string_salah . 'salah penggunakan tanda koma (seharusnya tanda titik) dan pembilang ribuan tidak menggunakan tanda titik pada kolom harga pendukung 1, baris ke ' . $row . '|';
                $harga_pendukung1 = 'SALAH';
            }

            $array_pendukung_koma2 = explode(',', $harga_pendukung2);
            $array_pendukung_titik2 = explode('.', $harga_pendukung2);

            if (count($array_pendukung_koma2) > 1 || count($array_pendukung_titik2) > 2 || $harga_pendukung2 == '') {
                $salah++;
                $string_salah = $string_salah . 'salah penggunakan tanda koma (seharusnya tanda titik) dan pembilang ribuan tidak menggunakan tanda titik pada kolom harga pendukung 2, baris ke ' . $row . '|';
                $harga_pendukung2 = 'SALAH';
            }

            $array_pendukung_koma3 = explode(',', $harga_pendukung3);
            $array_pendukung_titik3 = explode('.', $harga_pendukung3);

            if (count($array_pendukung_koma3) > 1 || count($array_pendukung_titik3) > 2 || $harga_pendukung3 == '') {
                $salah++;
                $string_salah = $string_salah . 'salah penggunakan tanda koma (seharusnya tanda titik) dan pembilang ribuan tidak menggunakan tanda titik pada kolom harga pendukung 3, baris ke ' . $row . '|';
                $harga_pendukung3 = 'SALAH';
            }

            if ($harga_pendukung1 <> $harga && $harga_pendukung2 <> $harga && $harga_pendukung3 <> $harga) {
                $salah++;
                $string_salah = $string_salah . 'harga pendukung berbeda dengan harga usulan, baris ke ' . $row . '|';
                $harga_pendukung1 = 'SALAH';
                $harga_pendukung2 = 'SALAH';
                $harga_pendukung3 = 'SALAH';
            }
            //harga lebih dari 1
            if ($harga_pendukung2 == 0 || $harga_pendukung3 == 0) {
                $salah++;
                $string_salah = $string_salah . 'harga pendukung harus 3, baris ke ' . $row . '|';
                $harga_pendukung2 = 'SALAH';
            }
//            if ($harga_pendukung2 == $harga_pendukung1 || $harga_pendukung1 == $harga_pendukung2 || $harga_pendukung3 == $harga_pendukung1) {
//                $salah++;
//                $string_salah = $string_salah . 'Harga ada yang sama mohon mengisikan harga yang berbeda, baris ke ' . $row . '|';
//                $harga_pendukung2 = 'SALAH';
//            }
//                cek harga pendukung
        }

        if ($salah > 0) {
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/usulan_ssh/' . $fileName_xls);
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/usulan_pendukung/' . $fileName_compress);
            $this->string_salah = $string_salah;
        } else {
            $con = Propel::getConnection();
            $con->begin();
            try {
//                save Usulan Dinas
                $insert_usulan_dinas = new UsulanDinas();
                $insert_usulan_dinas->setTglUsulan($tanggal_spjm);
                $insert_usulan_dinas->setPenyelia($user_dinas);
                $insert_usulan_dinas->setSkpd($unit_id);
                $insert_usulan_dinas->setJenisUsulan($jenis_usulan);
                $insert_usulan_dinas->setJumlahDukungan($jumlah_dukungan);
                $insert_usulan_dinas->setJumlahUsulan($jumlah_komponen);
                $insert_usulan_dinas->setCreatedAt($sekarang);
                $insert_usulan_dinas->setUpdatedAt($sekarang);
                $insert_usulan_dinas->setStatusVerifikasi(FALSE);
                $insert_usulan_dinas->setStatusHapus(FALSE);
                $insert_usulan_dinas->setFilepath($saving_filepath_xls);
                $insert_usulan_dinas->setFilepathRar($saving_filepath_pdf);
                $insert_usulan_dinas->setNomorSurat($nomor_surat);
                $insert_usulan_dinas->setStatusTolak(FALSE);
                $insert_usulan_dinas->setKeteranganDinas($keterangan_dinas);
                $insert_usulan_dinas->save();
//                save SPJM

                $con->commit();
                $this->setFlash('berhasil', 'Usulan Komponen Berhasil Tersimpan. Silahkan mengkonfirmasi ke penyelia untuk Tahap Verifikasi Usulan.');
            } catch (Exception $exc) {
                $con->rollback();
                unlink(sfConfig::get('sf_root_dir') . '/web/uploads/usulan_ssh/' . $fileName_xls);
                unlink(sfConfig::get('sf_root_dir') . '/web/uploads/usulan_pendukung/' . $fileName_compress);
                $this->setFlash('gagal', $exc->getMessage());
            }
            return $this->redirect('usulan_ssh/usulandinas');
        }
//            cek isian excel        
    }

//    ticket #39 - proses saving usulan komponen
//    ticket #41 - list usulan dinas    
    public function executeUsulandinaslist() {
        $this->processFiltersusulandinaslist();
        $this->filters = $this->getUser()->getAttributeHolder()->getAll('sf_admin/usulandinas/filters');

        $pagers = new sfPropelPager('UsulanDinas', 50);
        $c = new Criteria();

        $c_user_handle = new Criteria();
        $c_user_handle->add(UserHandleV2Peer::USER_ID, $this->getUser()->getNamaLogin(), Criteria::EQUAL);
        $user_handle = UserHandleV2Peer::doSelect($c_user_handle);
        if ($user_handle) {
            foreach ($user_handle as $value) {
                $c->addOr(UsulanDinasPeer::SKPD, $value->getUnitId(), Criteria::EQUAL);
            }
        }
        $c->addAnd(UsulanDinasPeer::STATUS_HAPUS, FALSE, Criteria::EQUAL);
        $c->addAnd(UsulanDinasPeer::STATUS_VERIFIKASI, FALSE, Criteria::EQUAL);
        $c->addAscendingOrderByColumn(UsulanDinasPeer::STATUS_VERIFIKASI);
        $c->addDescendingOrderByColumn(UsulanDinasPeer::CREATED_AT);
        $this->addFiltersCriteriausulandinaslist($c);

        $pagers->setCriteria($c);
        $pagers->setPage($this->getRequestParameter('page', 1));
        $pagers->init();

        $this->pager = $pagers;
    }

    public function executeUsulanperbaikanlist() {
        $this->processFiltersusulandinaslist();
        $this->filters = $this->getUser()->getAttributeHolder()->getAll('sf_admin/usulandinas/filters');

        $pagers = new sfPropelPager('UsulanDinas', 50);
        $c = new Criteria();

        $c_user_handle = new Criteria();
        $c_user_handle->add(UserHandleV2Peer::USER_ID, $this->getUser()->getNamaLogin(), Criteria::EQUAL);
        $c_user_handle->add(UserHandleV2Peer::STATUS_USER, Criteria::ISNOTNULL);
        $user_handle = UserHandleV2Peer::doSelect($c_user_handle);
        if ($user_handle) {
            foreach ($user_handle as $value) {
                $c->addOr(UsulanDinasPeer::SKPD, $value->getUnitId(), Criteria::EQUAL);
            }
        }
        $c->addAnd(UsulanDinasPeer::STATUS_HAPUS, FALSE, Criteria::EQUAL);
        $c->addAnd(UsulanDinasPeer::STATUS_VERIFIKASI, TRUE, Criteria::EQUAL);
        $c->addAscendingOrderByColumn(UsulanDinasPeer::STATUS_VERIFIKASI);
        $c->addDescendingOrderByColumn(UsulanDinasPeer::CREATED_AT);
        $this->addFiltersCriteriausulandinaslist($c);

        $pagers->setCriteria($c);
        $pagers->setPage($this->getRequestParameter('page', 1));
        $pagers->init();

        $this->pager = $pagers;
    }

//    ticket #41 - list usulan dinas        
//    ticket #41 - list usulan dinas    
    protected function processFiltersusulandinaslist() {
        if ($this->getRequest()->hasParameter('filter')) {
            $filters = $this->getRequestParameter('filters');
            $this->getUser()->getAttributeHolder()->removeNamespace('sf_admin/usulandinas/filters');
            $this->getUser()->getAttributeHolder()->add($filters, 'sf_admin/usulandinas/filters');
        }
    }

//    ticket #41 - list usulan dinas    
//    ticket #41 - list usulan dinas        
    protected function addFiltersCriteriausulandinaslist($c) {
        if (isset($this->filters['select'])) {
            if ($this->filters['select'] == 1) {
                if (isset($this->filters['user_dinas_is_empty'])) {
                    $criterion = $c->getNewCriterion(UsulanDinasPeer::PENYELIA, '');
                    $criterion->addOr($c->getNewCriterion(UsulanDinasPeer::PENYELIA, null, Criteria::ISNULL));
                    $c->add($criterion);
                } else if (isset($this->filters['komponen']) && $this->filters['komponen'] !== '') {
                    $kata = '%' . $this->filters['komponen'] . '%';
                    $c->add(UsulanDinasPeer::PENYELIA, strtr($kata, '*', '%'), Criteria::ILIKE);
                }
            } else {
                if (isset($this->filters['komponen_is_empty'])) {
                    echo 'is empty';
                    $criterion = $c->getNewCriterion(UsulanDinasPeer::PENYELIA, '');
                    $criterion->addOr($c->getNewCriterion(UsulanDinasPeer::PENYELIA, null, Criteria::ISNULL));
                    $c->add($criterion);
                }
            }
        }
    }

//    ticket #41 - list usulan dinas    
//    ticket #41 - save list usulan dinas memberi nomor SPJM    
    public function executeSavelistusulandinas() {
        if (!is_null($this->getRequestParameter('id_usulan')) && !is_null($this->getRequestParameter('nomor_spjm'))) {
            $con = Propel::getConnection();
            $sekarang = date('Y-m-d H:i:s');
            $con->begin();

            try {
                $c_usulan_dinas = new Criteria();
                $c_usulan_dinas->add(UsulanDinasPeer::ID_USULAN, $this->getRequestParameter('id_usulan'), Criteria::EQUAL);
                $usulan_dinas = UsulanDinasPeer::doSelectOne($c_usulan_dinas);
                $usulan_dinas->setKomentarVerifikator($this->getRequestParameter('nomor_spjm'));
                $usulan_dinas->setUpdatedAt($sekarang);
                $usulan_dinas->save();

                $con->commit();

                $this->setFlash('berhasil', 'Silahkan request Token baru untuk Usulan Dinas Baru dengan Nomor SPJM : ' . $this->getRequestParameter('nomor_spjm'));
                return $this->redirect('usulan_ssh/usulandinaslist');
            } catch (Exception $exc) {
                $con->rollback();

                $this->setFlash('gagal', 'Gagal karena ' . $exc->getMessage());
                return $this->redirect('usulan_ssh/usulandinaslist');
            }
        } else {
            $this->setFlash('gagal', 'Gagal karena cek parameter Anda');
            return $this->redirect('usulan_ssh/usulandinaslist');
        }
    }

//    ticket #41 - save list usulan dinas memberi nomor SPJM
//  list cek usulan dinas oleh penyelia
    public function executeCeklistusulandinas() {
        $this->processFiltersceklistusulandinas();
        $this->filters = $this->getUser()->getAttributeHolder()->getAll('sf_admin/usulandinas/filters');

        $pagers = new sfPropelPager('UsulanDinas', 50);
        $c = new Criteria();

        $c_user_handle = new Criteria();
        $c_user_handle->add(UserHandleV2Peer::USER_ID, $this->getUser()->getNamaLogin(), Criteria::EQUAL);
        $c_user_handle->add(UserHandleV2Peer::STATUS_USER, Criteria::ISNOTNULL);
        $user_handle = UserHandleV2Peer::doSelect($c_user_handle);
        if ($user_handle) {
            foreach ($user_handle as $value) {
                $c->addOr(UsulanDinasPeer::SKPD, $value->getUnitId(), Criteria::EQUAL);
            }
        }
        $c->addAnd(UsulanDinasPeer::STATUS_HAPUS, FALSE, Criteria::EQUAL);
        $c->addAnd(UsulanDinasPeer::STATUS_VERIFIKASI, FALSE, Criteria::EQUAL);
        $c->addAnd(UsulanDinasPeer::STATUS_TOLAK, FALSE, Criteria::EQUAL);
        $c->addAscendingOrderByColumn(UsulanDinasPeer::STATUS_VERIFIKASI);
        $c->addDescendingOrderByColumn(UsulanDinasPeer::CREATED_AT);
        $this->addFiltersCriteriaceklistusulandinas($c);

        $pagers->setCriteria($c);
        $pagers->setPage($this->getRequestParameter('page', 1));
        $pagers->init();

        $this->pager = $pagers;
    }

    protected function processFiltersceklistusulandinas() {
        if ($this->getRequest()->hasParameter('filter')) {
            $filters = $this->getRequestParameter('filters');
            $this->getUser()->getAttributeHolder()->removeNamespace('sf_admin/usulandinas/filters');
            $this->getUser()->getAttributeHolder()->add($filters, 'sf_admin/usulandinas/filters');
        }
    }

    protected function addFiltersCriteriaceklistusulandinas($c) {
        if (isset($this->filters['select'])) {
            if ($this->filters['select'] == 1) {
                if (isset($this->filters['user_dinas_is_empty'])) {
                    $criterion = $c->getNewCriterion(UsulanDinasPeer::PENYELIA, '');
                    $criterion->addOr($c->getNewCriterion(UsulanDinasPeer::PENYELIA, null, Criteria::ISNULL));
                    $c->add($criterion);
                } else if (isset($this->filters['komponen']) && $this->filters['komponen'] !== '') {
                    $kata = '%' . $this->filters['komponen'] . '%';
                    $c->add(UsulanDinasPeer::PENYELIA, strtr($kata, '*', '%'), Criteria::ILIKE);
                }
            } else {
                if (isset($this->filters['komponen_is_empty'])) {
                    echo 'is empty';
                    $criterion = $c->getNewCriterion(UsulanDinasPeer::PENYELIA, '');
                    $criterion->addOr($c->getNewCriterion(UsulanDinasPeer::PENYELIA, null, Criteria::ISNULL));
                    $c->add($criterion);
                }
            }
        }
    }

//  list cek usulan dinas oleh penyelia    
//  proses penghapusan usulan dinas oleh penyelia    
    function executeHapususulanolehpenyelia() {
        $id_usulan = $this->getRequestParameter('id');
        $alasan = $this->getRequestParameter('alasan');
        $sekarang = date('Y-m-d H:i:s');
        $key = $this->getRequestParameter('key');
        $md = md5('delete_usulan_dinas');

        if ($key != $md) {
            $this->setFlash('gagal', 'Key Salah');
            return $this->redirect('usulan_ssh/ceklistusulandinas');
        } else {
            try {
                $con = Propel::getConnection();
                $c_usulan_dinas = new Criteria();
                $c_usulan_dinas->add(UsulanDinasPeer::ID_USULAN, $id_usulan, Criteria::EQUAL);
                $usulan_dinas = UsulanDinasPeer::doSelectOne($c_usulan_dinas);
                unlink(str_replace(sfConfig::get('app_path_uploadsUsulanSsh'), sfConfig::get('sf_root_dir') . '/web/uploads/usulan_ssh/', $usulan_dinas->getFilepath()));
                unlink(str_replace(sfConfig::get('app_path_uploadsUsulanPendukung'), sfConfig::get('sf_root_dir') . '/web/uploads/usulan_pendukung/', $usulan_dinas->getFilepathRar()));

                $usulan_dinas->setStatusTolak(TRUE);
                $usulan_dinas->setUpdatedAt($sekarang);
                $usulan_dinas->setFilepath(NULL);
                $usulan_dinas->setFilepathRar(NULL);
                $usulan_dinas->setKomentarVerifikator($alasan);
                $usulan_dinas->save();

                $this->setFlash('berhasil', 'Usulan berhasil ter Tolak');
            } catch (Exception $exc) {
                $this->setFlash('gagal', $exc->getMessage());
            }
            return $this->redirect('usulan_ssh/ceklistusulandinas');
        }
    }

//  proses penghapusan usulan dinas oleh penyelia    
//    proses save perbaikan usulan dinas oleh dinas setelah ditolak penyelia
    function executeSaveperbaikanusulandinas() {
        set_time_limit(0);
        $sekarang = date('Y-m-d H:i:s');

//        ambil parameter
        $id_spjm = $this->getRequestParameter('id_usulan');

        $c_usulan_dinas = new Criteria();
        $c_usulan_dinas->add(UsulanDinasPeer::ID_USULAN, $id_spjm, Criteria::EQUAL);
        $dapet_usulan_dinas = UsulanDinasPeer::doSelectOne($c_usulan_dinas);

        $tanggal_spjm = $dapet_usulan_dinas->getTglUsulan();
        $user_dinas = $dapet_usulan_dinas->getPenyelia();
        $skpd = $dapet_usulan_dinas->getSkpd();
        $jenis_usulan = $dapet_usulan_dinas->getJenisUsulan();
        $jumlah_dukungan = $dapet_usulan_dinas->getJumlahDukungan();
        $jumlah_komponen = $dapet_usulan_dinas->getJumlahUsulan();
        $nomor_surat = $dapet_usulan_dinas->getNomorSurat();
//        ambil parameter
//        ambil kode unit_id
        $c_unit = new Criteria();
        $c_unit->add(UnitKerjaPeer::UNIT_ID, $skpd, Criteria::EQUAL);
        $dapet_unit = UnitKerjaPeer::doSelectOne($c_unit);
        $unit_id = $dapet_unit->getUnitId();
//        ambil kode unit_id
//        validasi awal
        if (!$this->getRequest()->getFileName('file') || !$this->getRequest()->getFileName('file_spjm')) {
            $this->setFlash('gagal', 'File Upload Usulan Komponen & File Data Pendukung harus diupload.');
            return $this->redirect('usulan_ssh/usulandinaslist');
        }

        if ($this->getRequest()->getFileSize('file_spjm') > 6000000) {
            $this->setFlash('gagal', 'Size File Data Pendukung maksimal 3 mb. Upload file compress beberapa data agar < 3 mb. Untuk file compress data pendukung lengkap Silahkan mengirim ke Penyelia Anda.');
            return $this->redirect('usulan_ssh/usulandinaslist');
        }

        if (substr($this->getRequest()->getFileName('file'), -3) != 'xls' || (substr($this->getRequest()->getFileName('file_spjm'), -3) != 'zip' && substr($this->getRequest()->getFileName('file_spjm'), -3) != 'rar')) {
            $this->setFlash('gagal', 'File Upload Usulan Komponen gunakan format xls hasil download & File Data Pendukung gunakan format rar atau zip.');
            return $this->redirect('usulan_ssh/usulandinaslist');
        }
        if ($jumlah_dukungan == 0 || $jumlah_komponen == 0) {
            $this->setFlash('gagal', 'Harap isi Isian Jumlah Dukungan & Jumlah Komponen.');
            return $this->redirect('usulan_ssh/usulandinaslist');
        }

//        upload file
        $array_file_xls = explode('.', $this->getRequest()->getFileName('file'));
        $array_file_pdf = explode('.', $this->getRequest()->getFileName('file_spjm'));

        $fileName_xls = 'file_usulan_' . $user_dinas . '_' . date('Y-m-d_H-i') . '.' . $array_file_xls[count($array_file_xls) - 1];
        $fileName_compress = 'file_pendukung_' . $user_dinas . '_' . date('Y-m-d_H-i') . '.' . $array_file_pdf[count($array_file_pdf) - 1];

        $this->getRequest()->moveFile('file', sfConfig::get('sf_root_dir') . '/web/uploads/usulan_ssh/' . $fileName_xls);
        $this->getRequest()->moveFile('file_spjm', sfConfig::get('sf_root_dir') . '/web/uploads/usulan_pendukung/' . $fileName_compress);

        $file_path_xls = sfConfig::get('sf_root_dir') . '/web/uploads/usulan_ssh/' . $fileName_xls;
        $file_path_pdf = sfConfig::get('sf_root_dir') . '/web/uploads/usulan_pendukung/' . $fileName_compress;

        $saving_filepath_xls = sfConfig::get('app_path_uploadsUsulanSsh') . '' . $fileName_xls;
        $saving_filepath_pdf = sfConfig::get('app_path_uploadsUsulanPendukung') . '' . $fileName_compress;
//        upload file excel 
//        cek data upload
        if (!file_exists($file_path_xls) || !file_exists($file_path_pdf)) {
            if (!file_exists($file_path_xls)) {
                unlink(sfConfig::get('sf_root_dir') . '/web/uploads/usulan_ssh/' . $fileName_xls);
            }
            if (!file_exists($file_path_pdf)) {
                unlink(sfConfig::get('sf_root_dir') . '/web/uploads/usulan_pendukung/' . $fileName_compress);
            }
            $this->setFlash('gagal', 'CEK FILE ' . $fileName_xls . ' ' . $fileName_compress);
            return $this->redirect('usulan_ssh/usulandinaslist');
        }
//        cek data upload

        $objReader = new PHPExcel_Reader_Excel5;
        $objPHPExcel = $objReader->load($file_path_xls);

        $objPHPExcel->getActiveSheetIndex(0);
        $objWorksheet = $objPHPExcel->getActiveSheet();

        $highR = $objWorksheet->getHighestRow();

//        cek jumlah komponen usulan
        $total = $highR - 15;
        if ($jumlah_komponen <> $total) {
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/usulan_ssh/' . $fileName_xls);
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/usulan_pendukung/' . $fileName_compress);
            $this->setFlash('gagal', 'Jumlah isian Jumlah Komponen & Jumlah pada file excel usulan komponen berbeda');
            return $this->redirect('usulan_ssh/usulandinaslist');
        }
//        cek jumlah komponen usulan

        $salah = 0;
        $string_salah = '';
//            cek isian excel
        for ($row = 16; $row <= $highR; $row++) {
             $tipe = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(2, $row)->getValue()));
            $nama = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(3, $row)->getValue()));
            $spec = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(4, $row)->getValue()));    
            $hidden_spec = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(5, $row)->getValue()));          
            $merek = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(6, $row)->getValue()));
            $satuan = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(7, $row)->getValue()));
            $harga = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(8, $row)->getValue()));
            $rekening = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(9, $row)->getValue()));
            $pajak = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(10, $row)->getValue()));
            $keterangan = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(11, $row)->getValue()));
            $kode_barang = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(12, $row)->getValue()));
            $nama_sebelum = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(13, $row)->getValue()));
            $harga_sebelum = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(14, $row)->getValue()));
            $alasan_perubahan = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(15, $row)->getValue()));
            $harga_pendukung1 = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(16, $row)->getValue()));
            $cv1 = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(17, $row)->getValue()));
            $harga_pendukung2 = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(18, $row)->getValue()));
            $cv2 = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(19, $row)->getValue()));
            $harga_pendukung3 = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(20, $row)->getValue()));
            $cv3 = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(21, $row)->getValue()));


//                cek nama
            if (trim($nama == '')) {
                $salah++;
                $string_salah = $string_salah . 'salah pengisian nama, baris ke ' . $row . '|';
                $nama = 'SALAH';
            }
//                cek nama
//                cek spec
            if (trim($spec) == '') {
                $salah++;
                $string_salah = $string_salah . 'salah pengisian spec, baris ke ' . $row . '|';
                $spec = 'SALAH';
            }
//                cek spec
//                cek hidden_spec
            if (trim($hidden_spec) == '') {
                $salah++;
                $string_salah = $string_salah . 'salah pengisian hidden_spec, baris ke ' . $row . '|';
                $hidden_spec = 'SALAH';
            }
//                cek hidden_spec
//                cek merek
            if (trim($merek) == '') {
                $salah++;
                $string_salah = $string_salah . 'salah pengisian hidden_spec, baris ke ' . $row . '|';
                $merek = 'SALAH';
            }
//                cek merek
//                cek satuan
            if (trim($satuan) == '') {
                $salah++;
                $string_salah = $string_salah . 'salah pengisian satuan, baris ke ' . $row . '|';
                $satuan = 'SALAH';
            }
//                cek satuan
//                cek keterangan
            if (trim($keterangan) == '') {
                $salah++;
                $string_salah = $string_salah . 'salah pengisian keterangan, baris ke ' . $row . '|';
                $keterangan = 'SALAH';
            }
//                cek keterangan
//                cek rekening
            $array_rekening = explode(')', $rekening);
            $kode_rekening = str_replace('(', '', $array_rekening[0]);

            if (count($array_rekening) == 1 || $rekening == '') {
                $salah++;
                $string_salah = $string_salah . 'salah pemilihan rekening, baris ke ' . $row . '|';
                $rekening = 'SALAH';
            }

            $c = new Criteria();
            $c->add(RekeningPeer::REKENING_CODE, $kode_rekening);
            if (!RekeningPeer::doSelectOne($c)) {
                $salah++;
                $string_salah = $string_salah . 'salah pemilihan rekening (karena rekening sudah tidak ada), baris ke ' . $row . '|';
                $rekening = 'SALAH';
            }
//                cek rekening
//                cek kode_barang
            $array_kode_barang = explode(')', $kode_barang);
            $kode_barang_fresh = str_replace('(', '', $array_kode_barang[0]);

            if (count($array_kode_barang) == 1 || $kode_barang_fresh == '') {
                $salah++;
                $string_salah = $string_salah . 'salah pemilihan kode barang, baris ke ' . $row . '|';
                $kode_barang_fresh = 'SALAH';
            }
//                cek kode_barang
//                cek tipe & kode_barang
            if ($tipe == '' || ($tipe <> 'SSH' && $tipe <> 'EST')) {
                $salah++;
                $string_salah = $string_salah . 'salah pemilihan kode barang dengan tipe, baris ke ' . $row . '|';
                $tipe = 'SALAH';
            } else {
                if ($tipe == 'SSH' && ($kode_barang_fresh == '23.05.01.01' || $kode_barang_fresh == '23.06.01.01')) {
                    $salah++;
                    $string_salah = $string_salah . 'salah pemilihan kode barang dengan tipe, baris ke ' . $row . '|';
                    $tipe = 'SALAH';
                } else if ($tipe == 'EST' && $kode_barang_fresh <> '23.05.01.01' && $kode_barang_fresh <> '23.06.01.01') {
                    $salah++;
                    $string_salah = $string_salah . 'salah pemilihan kode barang dengan tipe, baris ke ' . $row . '|';
                    $tipe = 'SALAH';
                }
            }
//                cek tipe & kode_barang
//                cek pajak                
            if ($pajak <> '0' && $pajak <> '10') {
                $salah++;
                $string_salah = $string_salah . 'salah pemilihan pajak, baris ke ' . $row . '|';
                $pajak = 'SALAH';
            }
//                cek pajak
//                cek harga
            $array_harga_koma = explode(',', $harga);
            $array_harga_titik = explode('.', $harga);
            if (count($array_harga_koma) > 1 || count($array_harga_titik) > 2 || $harga == '' || $harga == 0) {
                $salah++;
                $string_salah = $string_salah . 'salah penggunakan tanda koma (seharusnya tanda titik) dan pembilang ribuan tidak menggunakan tanda titik pada kolom harga, baris ke ' . $row . '|';
                $harga = 'SALAH';
            }
//                cek harga
//                cek perubahan harga 
            $array_perubahan_koma = explode(',', $harga_sebelum);
            $array_perubahan_titik = explode('.', $harga_sebelum);
            if ($harga_sebelum > 0) {
                if (count($array_harga_koma) > 1 || count($array_harga_titik) > 2 || $harga_sebelum == '') {
                    $salah++;
                    $string_salah = $string_salah . 'salah penggunakan tanda koma (seharusnya tanda titik) dan pembilang ribuan tidak menggunakan tanda titik pada kolom harga sebelum, baris ke ' . $row . '|';
                    $harga_sebelum = 'SALAH';
                }
                if (trim($alasan_perubahan) == '') {
                    $salah++;
                    $string_salah = $string_salah . 'Silahkan mengisi alasan perubahan harga untuk usulan perubahan harga komponen, baris ke ' . $row . '|';
                    $alasan_perubahan = 'SALAH';
                }
            }
//                cek perubahan harga 
//                cek harga pendukung
            $array_pendukung_koma1 = explode(',', $harga_pendukung1);
            $array_pendukung_titik1 = explode('.', $harga_pendukung1);

            if (count($array_pendukung_koma1) > 1 || count($array_pendukung_titik1) > 2 || $harga_pendukung1 == '') {
                $salah++;
                $string_salah = $string_salah . 'salah penggunakan tanda koma (seharusnya tanda titik) dan pembilang ribuan tidak menggunakan tanda titik pada kolom harga pendukung 1, baris ke ' . $row . '|';
                $harga_pendukung1 = 'SALAH';
            }

            $array_pendukung_koma2 = explode(',', $harga_pendukung2);
            $array_pendukung_titik2 = explode('.', $harga_pendukung2);

            if (count($array_pendukung_koma2) > 1 || count($array_pendukung_titik2) > 2 || $harga_pendukung2 == '') {
                $salah++;
                $string_salah = $string_salah . 'salah penggunakan tanda koma (seharusnya tanda titik) dan pembilang ribuan tidak menggunakan tanda titik pada kolom harga pendukung 2, baris ke ' . $row . '|';
                $harga_pendukung2 = 'SALAH';
            }

            $array_pendukung_koma3 = explode(',', $harga_pendukung3);
            $array_pendukung_titik3 = explode('.', $harga_pendukung3);

            if (count($array_pendukung_koma3) > 1 || count($array_pendukung_titik3) > 2 || $harga_pendukung3 == '') {
                $salah++;
                $string_salah = $string_salah . 'salah penggunakan tanda koma (seharusnya tanda titik) dan pembilang ribuan tidak menggunakan tanda titik pada kolom harga pendukung 3, baris ke ' . $row . '|';
                $harga_pendukung3 = 'SALAH';
            }

            if ($harga_pendukung1 <> $harga && $harga_pendukung2 <> $harga && $harga_pendukung3 <> $harga) {
                $salah++;
                $string_salah = $string_salah . 'harga pendukung berbeda dengan harga usulan, baris ke ' . $row . '|';
                $harga_pendukung1 = 'SALAH';
                $harga_pendukung2 = 'SALAH';
                $harga_pendukung3 = 'SALAH';
            }

//                cek harga pendukung
        }

        if ($salah > 0) {
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/usulan_ssh/' . $fileName_xls);
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/usulan_pendukung/' . $fileName_compress);
            $this->string_salah = $string_salah;
        } else {
            $con = Propel::getConnection();
            $con->begin();
            try {
//              update Usulan Dinas
                $dapet_usulan_dinas->setUpdatedAt($sekarang);
                $dapet_usulan_dinas->setFilepath($saving_filepath_xls);
                $dapet_usulan_dinas->setFilepathRar($saving_filepath_pdf);
                $dapet_usulan_dinas->setStatusTolak(FALSE);
                $dapet_usulan_dinas->save();
//              update Usulan Dinas

                $con->commit();
                $this->setFlash('berhasil', 'Usulan Komponen Pembenaran Berhasil Tersimpan. Silahkan mengkonfirmasi ke penyelia untuk Tahap Verifikasi Usulan.');
            } catch (Exception $exc) {
                $con->rollback();
                unlink(sfConfig::get('sf_root_dir') . '/web/uploads/usulan_ssh/' . $fileName_xls);
                unlink(sfConfig::get('sf_root_dir') . '/web/uploads/usulan_pendukung/' . $fileName_compress);
                $this->setFlash('gagal', $exc->getMessage());
            }
            return $this->redirect('usulan_ssh/usulandinaslist');
        }
//            cek isian excel        
    }

    //perbaikan file pendukung oleh admin
    function executeSaveperbaikanusulan() {
        set_time_limit(0);
        $sekarang = date('Y-m-d H:i:s');

//        ambil parameter
        $id_spjm = $this->getRequestParameter('id_usulan');

        $c_usulan_dinas = new Criteria();
        $c_usulan_dinas->add(UsulanDinasPeer::ID_USULAN, $id_spjm, Criteria::EQUAL);
        $dapet_usulan_dinas = UsulanDinasPeer::doSelectOne($c_usulan_dinas);

        $tanggal_spjm = $dapet_usulan_dinas->getTglUsulan();
        $user_dinas = $dapet_usulan_dinas->getPenyelia();
        $skpd = $dapet_usulan_dinas->getSkpd();
        $jenis_usulan = $dapet_usulan_dinas->getJenisUsulan();
        $jumlah_dukungan = $dapet_usulan_dinas->getJumlahDukungan();
        $jumlah_komponen = $dapet_usulan_dinas->getJumlahUsulan();
        $nomor_surat = $dapet_usulan_dinas->getNomorSurat();
//        ambil parameter
//        ambil kode unit_id
        $c_unit = new Criteria();
        $c_unit->add(UnitKerjaPeer::UNIT_ID, $skpd, Criteria::EQUAL);
        $dapet_unit = UnitKerjaPeer::doSelectOne($c_unit);
        $unit_id = $dapet_unit->getUnitId();
//        ambil kode unit_id
//        validasi awal
        if (!$this->getRequest()->getFileName('file_spjm')) {
            $this->setFlash('gagal', 'File Data Pendukung harus diupload.');
            return $this->redirect('usulan_ssh/usulanperbaikanlist');
        }

        if ($this->getRequest()->getFileSize('file_spjm') > 120000000) {
            $this->setFlash('gagal', 'Size File Data Pendukung maksimal 60 mb. Upload file compress beberapa data agar < 60 mb. Untuk file compress data pendukung lengkap Silahkan mengirim ke Penyelia Anda.');
            return $this->redirect('usulan_ssh/usulanperbaikanlist');
        }

        if ((substr($this->getRequest()->getFileName('file_spjm'), -3) != 'zip' && substr($this->getRequest()->getFileName('file_spjm'), -3) != 'rar')) {
            $this->setFlash('gagal', 'File Upload Usulan Komponen gunakan format xls hasil download & File Data Pendukung gunakan format rar atau zip.');
            return $this->redirect('usulan_ssh/usulanperbaikanlist');
        }
        if ($jumlah_dukungan == 0 || $jumlah_komponen == 0) {
            $this->setFlash('gagal', 'Harap isi Isian Jumlah Dukungan & Jumlah Komponen.');
            return $this->redirect('usulan_ssh/usulanperbaikanlist');
        }

//        upload file
        $array_file_pdf = explode('.', $this->getRequest()->getFileName('file_spjm'));
        $fileName_compress = 'file_pendukung_' . $user_dinas . '_' . date('Y-m-d_H-i') . '.' . $array_file_pdf[1];
        $this->getRequest()->moveFile('file_spjm', sfConfig::get('sf_root_dir') . '/web/uploads/usulan_pendukung/' . $fileName_compress);
        $file_path_pdf = sfConfig::get('sf_root_dir') . '/web/uploads/usulan_pendukung/' . $fileName_compress;
        $saving_filepath_pdf = sfConfig::get('app_path_uploadsUsulanPendukung') . '' . $fileName_compress;
//        upload file excel 
//        cek data upload
        if (!file_exists($file_path_pdf)) {
            if (!file_exists($file_path_pdf)) {
                unlink(sfConfig::get('sf_root_dir') . '/web/uploads/usulan_pendukung/' . $fileName_compress);
            }
            $this->setFlash('gagal', 'CEK FILE ' . $fileName_compress);
            return $this->redirect('usulan_ssh/usulanperbaikanlist');
        }
        $con = Propel::getConnection();
        $con->begin();
        try {
            $dapet_usulan_dinas->setUpdatedAt($sekarang);
            $dapet_usulan_dinas->setFilepathRar($saving_filepath_pdf);
            $dapet_usulan_dinas->setStatusTolak(FALSE);
            $dapet_usulan_dinas->save();
            $con->commit();
            $this->setFlash('berhasil', 'Usulan Komponen Pembenaran Berhasil Tersimpan. Silahkan mengkonfirmasi ke penyelia untuk Tahap Verifikasi Usulan.');
        } catch (Exception $exc) {
            $con->rollback();
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/usulan_pendukung/' . $fileName_compress);
            $this->setFlash('gagal', $exc->getMessage());
        }
        return $this->redirect('usulan_ssh/usulanperbaikanlist');
    }

//    proses save perbaikan usulan dinas oleh dinas setelah ditolak penyelia

    function executePrintSemiBukuPerTanggal() {
        $tanggal = $this->getRequestParameter('tanggal_cari');

        try {
            // $array_sheet = array('1.1.7.01.01.01',  '1.1.7.01.01.02','1.1.7.01.01.04','1.1.7.01.01.05','1.1.7.01.01.08','1.1.7.01.01.09','1.1.7.01.01.10','1.1.7.01.01.11','1.1.7.01.01.12','1.1.7.01.02.01','1.1.7.01.02.02','1.1.7.01.02.06','1.1.7.01.02.08','1.1.7.01.02.10','1.1.7.01.02.11','1.1.7.01.02.12','1.1.7.01.02.13','1.1.7.01.02.14','1.1.7.01.02.15','1.1.7.01.02.16','1.1.7.01.03.01','1.1.7.01.03.02','1.1.7.01.03.03','1.1.7.01.03.04','1.1.7.01.03.05','1.1.7.01.03.06','1.1.7.01.03.07','1.1.7.01.03.08','1.1.7.01.03.09','1.1.7.01.03.11','1.1.7.01.03.12','1.1.7.01.03.13','1.1.7.01.04.01','1.1.7.01.04.02','1.1.7.01.07.01','1.1.7.01.07.02','1.1.7.01.07.03','1.1.7.01.07.04','1.1.7.01.07.05','1.1.7.01.07.06','1.1.7.01.10.01','1.1.7.01.10.02','1.1.7.01.10.03','1.1.7.01.10.04','1.1.7.02.02.01','1.1.7.02.02.06','1.1.7.02.02.07','1.3.2.01.01.01','1.3.2.01.01.02','1.3.2.01.01.03','1.3.2.01.01.04','1.3.2.01.01.05','1.3.2.01.01.06','1.3.2.01.01.07','1.3.2.01.01.08','1.3.2.01.01.09','1.3.2.01.01.10','1.3.2.01.01.11','1.3.2.01.01.12','1.3.2.01.02.01','1.3.2.01.02.02','1.3.2.01.02.03','1.3.2.01.02.04','1.3.2.01.02.05','1.3.2.01.02.06','1.3.2.01.03.01','1.3.2.01.03.02','1.3.2.01.03.03','1.3.2.01.03.04','1.3.2.01.03.05','1.3.2.01.03.06','1.3.2.01.03.07','1.3.2.01.03.08','1.3.2.01.03.09','1.3.2.01.03.16','1.3.2.02.01.01','1.3.2.02.01.02','1.3.2.02.01.03','1.3.2.02.01.04','1.3.2.02.01.05','1.3.2.02.01.06','1.3.2.02.01.09','1.3.2.02.02.01','1.3.2.02.02.02','1.3.2.02.02.04','1.3.2.02.03.01','1.3.2.02.03.02','1.3.2.02.03.03','1.3.2.02.03.05','1.3.2.02.04.01','1.3.2.02.04.02','1.3.2.02.04.03','1.3.2.02.04.04','1.3.2.03.01.01','1.3.2.03.01.02','1.3.2.03.01.03','1.3.2.03.01.04','1.3.2.03.01.05','1.3.2.03.01.06','1.3.2.03.01.07','1.3.2.03.01.08','1.3.2.03.01.10','1.3.2.03.02.01','1.3.2.03.02.02','1.3.2.03.02.03','1.3.2.03.02.04','1.3.2.03.02.05','1.3.2.03.02.06','1.3.2.03.02.07','1.3.2.03.02.08','1.3.2.03.02.09','1.3.2.03.02.10','1.3.2.03.02.11','1.3.2.03.02.13','1.3.2.03.03.01','1.3.2.03.03.02','1.3.2.03.03.03','1.3.2.03.03.04','1.3.2.03.03.05','1.3.2.03.03.06','1.3.2.03.03.07','1.3.2.03.03.08','1.3.2.03.03.10','1.3.2.03.03.11','1.3.2.03.03.21','1.3.2.04.01.01','1.3.2.04.01.02','1.3.2.04.01.03','1.3.2.04.01.04','1.3.2.04.01.05','1.3.2.04.01.06','1.3.2.04.01.07','1.3.2.04.01.08','1.3.2.04.01.09','1.3.2.04.01.10','1.3.2.05.01.01','1.3.2.05.01.02','1.3.2.05.01.03','1.3.2.05.01.04','1.3.2.05.01.05','1.3.2.05.02.01','1.3.2.05.02.02','1.3.2.05.02.03','1.3.2.05.02.04','1.3.2.05.02.05','1.3.2.05.02.06','1.3.2.05.02.07','1.3.2.06.01.01','1.3.2.06.01.02','1.3.2.06.01.03','1.3.2.06.01.04','1.3.2.06.01.05','1.3.2.06.01.06','1.3.2.06.02.01','1.3.2.06.02.11','1.3.2.06.03.01','1.3.2.06.03.47','1.3.2.06.03.48','1.3.2.07.01.01','1.3.2.07.01.02','1.3.2.07.01.03','1.3.2.07.01.04','1.3.2.07.01.05','1.3.2.07.01.06','1.3.2.07.01.07','1.3.2.07.01.08','1.3.2.07.01.09','1.3.2.07.01.10','1.3.2.07.01.13','1.3.2.07.01.14','1.3.2.07.01.19','1.3.2.07.01.20','1.3.2.07.01.21','1.3.2.07.01.22','1.3.2.07.01.29','1.3.2.07.02.01','1.3.2.07.02.02','1.3.2.07.02.03','1.3.2.07.02.04','1.3.2.07.02.05','1.3.2.07.02.06','1.3.2.07.02.07','1.3.2.08.01.01','1.3.2.08.01.02','1.3.2.08.01.03','1.3.2.08.01.04','1.3.2.08.01.05','1.3.2.08.01.06','1.3.2.08.01.07','1.3.2.08.01.08','1.3.2.08.01.09','1.3.2.08.01.10','1.3.2.08.01.11','1.3.2.08.01.13','1.3.2.08.01.18','1.3.2.08.01.19','1.3.2.08.01.20','1.3.2.08.01.41','1.3.2.08.01.58','1.3.2.08.01.61','1.3.2.08.01.64','1.3.2.08.03.01','1.3.2.08.03.02','1.3.2.08.03.03','1.3.2.08.03.04','1.3.2.08.03.05','1.3.2.08.03.06','1.3.2.08.03.07','1.3.2.08.03.08','1.3.2.08.03.09','1.3.2.08.03.10','1.3.2.08.03.11','1.3.2.08.03.12','1.3.2.08.03.13','1.3.2.08.03.15','1.3.2.08.03.16','1.3.2.08.07.04','1.3.2.08.07.06','1.3.2.10.01.01','1.3.2.10.01.02','1.3.2.10.02.01','1.3.2.10.02.03','1.3.2.10.02.04','1.3.2.10.02.05','1.3.2.15.01.01','1.3.2.15.01.02','1.3.2.15.01.03','1.3.2.15.02.01','1.3.2.15.02.02','1.3.2.15.02.03','1.3.2.15.02.04','1.3.2.15.02.05','1.3.2.15.02.06','1.3.2.15.03.01','1.3.2.15.03.02','1.3.2.18.01.01','1.3.2.18.01.02','1.3.2.18.01.03','1.3.2.18.01.04','1.3.2.18.01.05','1.3.2.18.01.06','1.3.2.19.01.01','1.3.2.19.01.02','1.3.2.19.01.03','1.3.2.19.01.04','1.3.2.19.01.05','1.3.2.19.01.06','1.3.5.01.01.01','1.3.5.01.01.02','1.3.5.01.01.03','1.3.5.01.01.04','1.3.5.01.01.05','1.3.5.01.01.06','1.3.5.01.01.07','1.3.5.01.01.08','1.3.5.01.01.09','1.3.5.01.01.10','1.3.5.01.01.12','1.3.5.01.02.01','1.3.5.01.02.02','1.3.5.01.03.01','1.3.5.01.03.02','1.3.5.01.03.03','1.3.5.02.01.01','1.3.5.02.01.02','1.3.5.02.01.03','1.3.5.02.01.04','1.3.5.02.02.01','1.3.5.02.02.02','1.3.5.02.02.03','1.3.5.02.03.01','1.3.5.02.03.02','1.3.5.03.01.01','1.3.5.03.01.02','1.3.5.03.01.03','1.3.5.03.01.04','1.3.5.03.02.01','1.3.5.03.02.03','1.3.5.03.02.04','1.3.5.05.01.01','1.5.3.01.01.01','2.1.1.01.01.01','2.1.1.01.01.02','2.1.1.01.01.03','2.1.1.01.02.01','2.1.1.01.02.02','2.1.1.01.02.03','2.1.1.01.03.01','2.1.1.01.03.02','2.1.1.01.03.03','2.1.1.02.01.01','2.1.1.02.01.02','2.1.1.02.02.01','2.1.1.02.02.02','2.1.1.02.03.01','2.1.1.02.04.01','2.1.1.02.04.02','2.1.1.02.04.03','2.1.1.03.01.01','2.1.1.03.01.02','2.1.1.03.02.01','2.1.1.03.02.02','2.1.1.03.02.03','2.1.1.03.02.04','2.1.1.03.02.05','2.1.1.03.02.06','2.1.1.03.02.07','2.1.1.03.03.01','2.1.1.03.04.01','2.1.1.03.04.02','2.1.1.03.05.01','2.1.2.01.01.01','EST');

            $array_sheet = array('1.1.7','1.3.2.01', '1.3.2.02', '1.3.2.03', '1.3.2.04', '1.3.2.05', '1.3.2.06', '1.3.2.07', '1.3.2.08', '1.3.2.10', '1.3.2.15', '1.3.2.18', '1.3.2.19', '1.3.5.01', '1.3.5.02', '1.3.5.03', '1.3.5.05', '1.5.3.01', '2.1.1.01', '2.1.1.02', '2.1.1.03','2.1.2.01');

            $objPHPExcel = new sfPhpExcel();

            for ($sheet_ke = 0; $sheet_ke < count($array_sheet); $sheet_ke++) {
                if ($array_sheet[$sheet_ke] <> 'EST') {
                    $c_kategory_shsd = new Criteria();
                    $c_kategory_shsd->add(KategoriShsdPeer::KATEGORI_SHSD_ID, $array_sheet[$sheet_ke], Criteria::EQUAL);
                    $data_kategori = KategoriShsdPeer::doSelectOne($c_kategory_shsd);
                }

                if ($sheet_ke > 0) {
                    $objPHPExcel->createSheet($sheet_ke);
                }

                $objPHPExcel->setActiveSheetIndex($sheet_ke);
                if ($array_sheet[$sheet_ke] <> 'EST') {
                    $objPHPExcel->getActiveSheet()->setTitle($array_sheet[$sheet_ke] . ' ' . ucwords(strtolower($data_kategori->getKategoriShsdName())));
                } else {
                    $objPHPExcel->getActiveSheet()->setTitle($array_sheet[$sheet_ke] . ' ' . ucwords('Estimasi'));
                }
                $objPHPExcel->getActiveSheet()->setCellValue('C5', 'No');
                $objPHPExcel->getActiveSheet()->setCellValue('D5', 'Nama Barang');
                $objPHPExcel->getActiveSheet()->setCellValue('E5', 'Merk');
                $objPHPExcel->getActiveSheet()->setCellValue('F5', 'Spesifikasi');
                $objPHPExcel->getActiveSheet()->setCellValue('G5', 'Hidden Spec');
                $objPHPExcel->getActiveSheet()->setCellValue('H5', 'Satuan');
                $objPHPExcel->getActiveSheet()->setCellValue('I5', 'Harga Satuan');
                $objPHPExcel->getActiveSheet()->setCellValue('J5', 'Usulan SKPD');
                $objPHPExcel->getActiveSheet()->setCellValue('K5', 'Keterangan');
                $objPHPExcel->getActiveSheet()->setCellValue('L5', 'Kode Rekening');
                $objPHPExcel->getActiveSheet()->setCellValue('N5', 'Pajak');
                $objPHPExcel->getActiveSheet()->setCellValue('O5', 'Tahap');

                $objPHPExcel->getActiveSheet()->setCellValue('P5', 'Nama Sebelum');
                $objPHPExcel->getActiveSheet()->setCellValue('Q5', 'Harga Sebelum');
                $objPHPExcel->getActiveSheet()->setCellValue('R5', 'Alasan Perubahan');
                $objPHPExcel->getActiveSheet()->setCellValue('S5', 'Harga Pendukung 1');
                $objPHPExcel->getActiveSheet()->setCellValue('T5', 'Harga Pendukung 2');
                $objPHPExcel->getActiveSheet()->setCellValue('U5', 'Harga Pendukung 3');
                $objPHPExcel->getActiveSheet()->setCellValue('V5', 'Penyedia 1');
                $objPHPExcel->getActiveSheet()->setCellValue('W5', 'Penyedia 2');
                $objPHPExcel->getActiveSheet()->setCellValue('X5', 'Penyedia 3');
                $objPHPExcel->getActiveSheet()->setCellValue('Y5', 'Tahap Buku');
                

                if ($array_sheet[$sheet_ke] <> 'EST') {
//                    $c_verifikasi = new Criteria();
//                    $c_verifikasi->add(ShsdPeer::SHSD_ID, $array_sheet[$sheet_ke] . '%', Criteria::ILIKE);
//                    $c_verifikasi->addAnd(UsulanVerifikasiPeer::CREATED_AT, date($tanggal . ' 00:00:00'), Criteria::GREATER_THAN);
//                    $c_verifikasi->addAnd(UsulanVerifikasiPeer::CREATED_AT, date($tanggal . ' 23:59:59'), Criteria::LESS_THAN);
//                    $c_verifikasi->addJoin(ShsdPeer::SHSD_ID, UsulanVerifikasiPeer::SHSD_ID, Criteria::INNER_JOIN);
//                    $c_verifikasi->addAscendingOrderByColumn(ShsdPeer::SHSD_ID);
//                    $data_verifikasi = ShsdPeer::doSelect($c_verifikasi);
//
//                    $kolom_ke = 6;
//                    foreach ($data_verifikasi as $value) {
//
//                        $c_usulan_ssh = new Criteria();
//                        $c_usulan_ssh->add(UsulanSSHPeer::SHSD_ID, $value->getShsdId());
//                        $data_usulan_ssh = UsulanSSHPeer::doSelectOne($c_usulan_ssh);
//
//                        $objPHPExcel->getActiveSheet()->setCellValue('C' . $kolom_ke, $value->getShsdId());
//                        $objPHPExcel->getActiveSheet()->setCellValue('D' . $kolom_ke, $value->getShsdName());
//                        $objPHPExcel->getActiveSheet()->setCellValue('E' . $kolom_ke, $value->getShsdMerk());
//                        $objPHPExcel->getActiveSheet()->setCellValue('F' . $kolom_ke, $value->getSpec());
//                        $objPHPExcel->getActiveSheet()->setCellValue('G' . $kolom_ke, $value->getHiddenSpec());
//                        $objPHPExcel->getActiveSheet()->setCellValue('H' . $kolom_ke, $value->getSatuan());
//                        $objPHPExcel->getActiveSheet()->setCellValue('I' . $kolom_ke, $value->getShsdHarga());
//                        $objPHPExcel->getActiveSheet()->setCellValue('J' . $kolom_ke, $value->getUsulanSkpd());
//                        $objPHPExcel->getActiveSheet()->setCellValue('L' . $kolom_ke, $value->getRekeningCode());
//                        if ($value->getNonPajak() == true) {
//                            $objPHPExcel->getActiveSheet()->setCellValue('N' . $kolom_ke, 0);
//                        } else {
//                            $objPHPExcel->getActiveSheet()->setCellValue('N' . $kolom_ke, 10);
//                        }
//
//                        if ($data_usulan_ssh) {
//                            $objPHPExcel->getActiveSheet()->setCellValue('P' . $kolom_ke, $data_usulan_ssh->getNamaSebelum());
//                            $objPHPExcel->getActiveSheet()->setCellValue('Q' . $kolom_ke, $data_usulan_ssh->getHargaSebelum());
//                            $objPHPExcel->getActiveSheet()->setCellValue('R' . $kolom_ke, $data_usulan_ssh->getAlasanPerubahanHarga());
//                        }
//
//
//                        $kolom_ke++;
//                    }

                    $c_verifikasi = new Criteria();
                    $c_verifikasi->add(UsulanVerifikasiPeer::SHSD_ID, $array_sheet[$sheet_ke] . '%', Criteria::ILIKE);
                    $c_verifikasi->addAnd(UsulanVerifikasiPeer::CREATED_AT, date($tanggal . ' 00:00:00'), Criteria::GREATER_THAN);
                    $c_verifikasi->addAnd(UsulanVerifikasiPeer::CREATED_AT, date($tanggal . ' 23:59:59'), Criteria::LESS_THAN);
                    $c_verifikasi->addAscendingOrderByColumn(UsulanVerifikasiPeer::SHSD_ID);
                    $data_verifikasi = UsulanVerifikasiPeer::doSelect($c_verifikasi);

                    $kolom_ke = 6;
                    foreach ($data_verifikasi as $value) {
                        $c_usulan_ssh = new Criteria();
                        $c_usulan_ssh->add(UsulanSSHPeer::ID_USULAN, $value->getIdUsulan());
                        $data_usulan_ssh = UsulanSSHPeer::doSelectOne($c_usulan_ssh);

                        $c_dinas_usulan_ssh = new Criteria();
                        $c_dinas_usulan_ssh->add(UnitKerjaPeer::UNIT_ID, $value->getUnitId());
                        $data_dinas_usulan_ssh = UnitKerjaPeer::doSelectOne($c_dinas_usulan_ssh);

                        if ($data_usulan_ssh) {
                            $objPHPExcel->getActiveSheet()->setCellValue('P' . $kolom_ke, $data_usulan_ssh->getNamaSebelum());
                            $objPHPExcel->getActiveSheet()->setCellValue('Q' . $kolom_ke, $data_usulan_ssh->getHargaSebelum());
                            $objPHPExcel->getActiveSheet()->setCellValue('R' . $kolom_ke, $data_usulan_ssh->getAlasanPerubahanHarga());
                            $objPHPExcel->getActiveSheet()->setCellValue('S' . $kolom_ke, $data_usulan_ssh->getHargaPendukung1());
                            $objPHPExcel->getActiveSheet()->setCellValue('T' . $kolom_ke, $data_usulan_ssh->getHargaPendukung2());
                            $objPHPExcel->getActiveSheet()->setCellValue('U' . $kolom_ke, $data_usulan_ssh->getHargaPendukung3());
                            $objPHPExcel->getActiveSheet()->setCellValue('V' . $kolom_ke, $data_usulan_ssh->getCV1());
                            $objPHPExcel->getActiveSheet()->setCellValue('W' . $kolom_ke, $data_usulan_ssh->getCV2());
                            $objPHPExcel->getActiveSheet()->setCellValue('X' . $kolom_ke, $data_usulan_ssh->getCV3());
                            $objPHPExcel->getActiveSheet()->setCellValue('Y' . $kolom_ke, $value->getTahapBuku());
                        }

                        $objPHPExcel->getActiveSheet()->setCellValue('C' . $kolom_ke, $value->getShsdId());

                        $c_data_shsd = new Criteria();
                        $c_data_shsd->add(KomponenAllPeer::KOMPONEN_ID, $value->getShsdId());
                        $data_shsd = KomponenAllPeer::doSelectOne($c_data_shsd);
                        if ($data_shsd) {
                            $objPHPExcel->getActiveSheet()->setCellValue('D' . $kolom_ke, $data_shsd->getNamaDasar());

                            $c_tahap_usulan_ssh = new Criteria();
                            $c_tahap_usulan_ssh->add(MasterTahapPeer::TAHAP_ID,$data_shsd->getTahapId());
                            $data_tahap_usulan_ssh = MasterTahapPeer::doSelectOne($c_tahap_usulan_ssh);
                            if($data_tahap_usulan_ssh)
                            {
                                $objPHPExcel->getActiveSheet()->setCellValue('O' . $kolom_ke, $data_tahap_usulan_ssh->getTahapName());
                            }
                        } else {
                            $objPHPExcel->getActiveSheet()->setCellValue('D' . $kolom_ke, $value->getShsdName());
                        }

                        $objPHPExcel->getActiveSheet()->setCellValue('E' . $kolom_ke, $value->getMerek());
                        $objPHPExcel->getActiveSheet()->setCellValue('F' . $kolom_ke, $value->getSpec());
                        $objPHPExcel->getActiveSheet()->setCellValue('G' . $kolom_ke, $value->getHiddenSpec());
                        $objPHPExcel->getActiveSheet()->setCellValue('H' . $kolom_ke, $value->getSatuan());
                        $objPHPExcel->getActiveSheet()->setCellValue('I' . $kolom_ke, $value->getHarga());
                        $objPHPExcel->getActiveSheet()->setCellValue('J' . $kolom_ke, $data_dinas_usulan_ssh->getUnitName());
                        $objPHPExcel->getActiveSheet()->setCellValue('K' . $kolom_ke, $value->getKeterangan());
                        $objPHPExcel->getActiveSheet()->setCellValue('L' . $kolom_ke, $value->getRekening());
                        $objPHPExcel->getActiveSheet()->setCellValue('N' . $kolom_ke, $value->getPajak());
                        $kolom_ke++;
                    }
                } else {
//                    $c_verifikasi = new Criteria();
//                    $c_verifikasi->add(KomponenPeer::KOMPONEN_TIPE, 'EST', Criteria::ILIKE);
//                    $c_verifikasi->addAnd(UsulanVerifikasiPeer::CREATED_AT, date($tanggal . ' 00:00:00'), Criteria::GREATER_THAN);
//                    $c_verifikasi->addAnd(UsulanVerifikasiPeer::CREATED_AT, date($tanggal . ' 23:59:59'), Criteria::LESS_THAN);
//                    $c_verifikasi->addJoin(KomponenPeer::KOMPONEN_ID, UsulanVerifikasiPeer::SHSD_ID, Criteria::INNER_JOIN);
//                    $c_verifikasi->addAscendingOrderByColumn(KomponenPeer::KOMPONEN_ID);
//                    $data_verifikasi = KomponenPeer::doSelect($c_verifikasi);
//
//                    $kolom_ke = 6;
//                    foreach ($data_verifikasi as $value) {
//
//                        $c_usulan_ssh = new Criteria();
//                        $c_usulan_ssh->add(UsulanSSHPeer::SHSD_ID, $value->getKomponenId());
//                        $data_usulan_ssh = UsulanSSHPeer::doSelectOne($c_usulan_ssh);
//
//                        $objPHPExcel->getActiveSheet()->setCellValue('C' . $kolom_ke, $value->getKomponenId());
//                        $objPHPExcel->getActiveSheet()->setCellValue('D' . $kolom_ke, $value->getKomponenName());
//                        $objPHPExcel->getActiveSheet()->setCellValue('H' . $kolom_ke, $value->getSatuan());
//                        $objPHPExcel->getActiveSheet()->setCellValue('I' . $kolom_ke, $value->getKomponenHarga());
//                        $objPHPExcel->getActiveSheet()->setCellValue('J' . $kolom_ke, $value->getUsulanSkpd());
//                        $objPHPExcel->getActiveSheet()->setCellValue('L' . $kolom_ke, str_replace(',', '', $value->getRekening()));
//                        if ($value->getKomponenNonPajak() == true) {
//                            $objPHPExcel->getActiveSheet()->setCellValue('N' . $kolom_ke, 0);
//                        } else {
//                            $objPHPExcel->getActiveSheet()->setCellValue('N' . $kolom_ke, 10);
//                        }
//
//                        if ($data_usulan_ssh) {
//                            $objPHPExcel->getActiveSheet()->setCellValue('P' . $kolom_ke, $data_usulan_ssh->getNamaSebelum());
//                            $objPHPExcel->getActiveSheet()->setCellValue('Q' . $kolom_ke, $data_usulan_ssh->getHargaSebelum());
//                            $objPHPExcel->getActiveSheet()->setCellValue('R' . $kolom_ke, $data_usulan_ssh->getAlasanPerubahanHarga());
//                        }
//
//                        $kolom_ke++;
//                    }

                    $c_verifikasi = new Criteria();
                    $c_verifikasi->add(UsulanVerifikasiPeer::TIPE, 'SSH', Criteria::ALT_NOT_EQUAL);
                    $c_verifikasi->addAnd(UsulanVerifikasiPeer::CREATED_AT, date($tanggal . ' 00:00:00'), Criteria::GREATER_THAN);
                    $c_verifikasi->addAnd(UsulanVerifikasiPeer::CREATED_AT, date($tanggal . ' 23:59:59'), Criteria::LESS_THAN);
                    $c_verifikasi->addAscendingOrderByColumn(UsulanVerifikasiPeer::SHSD_ID);
                    $data_verifikasi = UsulanVerifikasiPeer::doSelect($c_verifikasi);

                    $kolom_ke = 6;
                    foreach ($data_verifikasi as $value) {
                        $c_usulan_ssh = new Criteria();
                        $c_usulan_ssh->add(UsulanSSHPeer::ID_USULAN, $value->getIdUsulan());
                        $data_usulan_ssh = UsulanSSHPeer::doSelectOne($c_usulan_ssh);

                        $c_dinas_usulan_ssh = new Criteria();
                        $c_dinas_usulan_ssh->add(UnitKerjaPeer::UNIT_ID, $value->getUnitId());
                        $data_dinas_usulan_ssh = UnitKerjaPeer::doSelectOne($c_dinas_usulan_ssh);

                        if ($data_usulan_ssh) {
                            $objPHPExcel->getActiveSheet()->setCellValue('P' . $kolom_ke, $data_usulan_ssh->getNamaSebelum());
                            $objPHPExcel->getActiveSheet()->setCellValue('Q' . $kolom_ke, $data_usulan_ssh->getHargaSebelum());
                            $objPHPExcel->getActiveSheet()->setCellValue('R' . $kolom_ke, $data_usulan_ssh->getAlasanPerubahanHarga());
                        }

                        $objPHPExcel->getActiveSheet()->setCellValue('C' . $kolom_ke, $value->getShsdId());

                        $c_data_shsd = new Criteria();
                        $c_data_shsd->add(KomponenAllPeer::KOMPONEN_ID, $value->getShsdId());
                        $data_shsd = KomponenAllPeer::doSelectOne($c_data_shsd);
                        if ($data_shsd) {
                            $objPHPExcel->getActiveSheet()->setCellValue('D' . $kolom_ke, $data_shsd->getNamaDasar());

                            $c_tahap_usulan_ssh = new Criteria();
                            $c_tahap_usulan_ssh->add(MasterTahapPeer::TAHAP_ID,$data_shsd->getTahapId());
                            $data_tahap_usulan_ssh = MasterTahapPeer::doSelectOne($c_tahap_usulan_ssh);
                            if($data_tahap_usulan_ssh)
                            {
                                $objPHPExcel->getActiveSheet()->setCellValue('O' . $kolom_ke, $data_tahap_usulan_ssh->getTahapName());
                            }
                        } else {
                            $objPHPExcel->getActiveSheet()->setCellValue('D' . $kolom_ke, $value->getShsdName());
                        }

                        $objPHPExcel->getActiveSheet()->setCellValue('E' . $kolom_ke, $value->getMerek());
                        $objPHPExcel->getActiveSheet()->setCellValue('F' . $kolom_ke, $value->getSpec());
                        $objPHPExcel->getActiveSheet()->setCellValue('G' . $kolom_ke, $value->getHiddenSpec());
                        $objPHPExcel->getActiveSheet()->setCellValue('H' . $kolom_ke, $value->getSatuan());
                        $objPHPExcel->getActiveSheet()->setCellValue('I' . $kolom_ke, $value->getHarga());
                        $objPHPExcel->getActiveSheet()->setCellValue('J' . $kolom_ke, $data_dinas_usulan_ssh->getUnitName());
                        $objPHPExcel->getActiveSheet()->setCellValue('L' . $kolom_ke, $value->getRekening());
                        $objPHPExcel->getActiveSheet()->setCellValue('N' . $kolom_ke, $value->getPajak());
                        $kolom_ke++;
                    }
                }
            }

            $objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename=export_ssh_per_tanggal_' . $tanggal . '.xls');
            header('Cache-Control: max-age=0');
            $objWriter->save('php://output');
            exit();
        } catch (Exception $exc) {
            echo $exc->getMessage();
            exit();
        }
    }



function executePrintSemiBukuPerTahap() {
        $tahap_cari = $this->getRequestParameter('tahap_cari');

        try {
            $array_sheet = array('1.1.7','1.3.2.01', '1.3.2.02', '1.3.2.03', '1.3.2.04', '1.3.2.05', '1.3.2.06', '1.3.2.07', '1.3.2.08', '1.3.2.10', '1.3.2.15', '1.3.2.18', '1.3.2.19', '1.3.5.01', '1.3.5.02', '1.3.5.03', '1.3.5.05', '1.5.3.01', '2.1.1.01', '2.1.1.02', '2.1.1.03','2.1.2.01');
            
            $objPHPExcel = new sfPhpExcel();

            for ($sheet_ke = 0; $sheet_ke < count($array_sheet); $sheet_ke++) {
                if ($array_sheet[$sheet_ke] <> 'EST') {
                    $c_kategory_shsd = new Criteria();
                    $c_kategory_shsd->add(KategoriShsdPeer::KATEGORI_SHSD_ID, $array_sheet[$sheet_ke], Criteria::EQUAL);
                    $data_kategori = KategoriShsdPeer::doSelectOne($c_kategory_shsd);
                }

                if ($sheet_ke > 0) {
                    $objPHPExcel->createSheet($sheet_ke);
                }

                $objPHPExcel->setActiveSheetIndex($sheet_ke);
                if ($array_sheet[$sheet_ke] <> 'EST') {
                    $objPHPExcel->getActiveSheet()->setTitle($array_sheet[$sheet_ke] . ' ' . ucwords(strtolower($data_kategori->getKategoriShsdName())));
                } else {
                    $objPHPExcel->getActiveSheet()->setTitle($array_sheet[$sheet_ke] . ' ' . ucwords('Estimasi'));
                }
                $objPHPExcel->getActiveSheet()->setCellValue('C5', 'No');
                $objPHPExcel->getActiveSheet()->setCellValue('D5', 'Nama Barang');
                $objPHPExcel->getActiveSheet()->setCellValue('E5', 'Merk');
                $objPHPExcel->getActiveSheet()->setCellValue('F5', 'Spesifikasi');
                $objPHPExcel->getActiveSheet()->setCellValue('G5', 'Hidden Spec');
                $objPHPExcel->getActiveSheet()->setCellValue('H5', 'Satuan');
                $objPHPExcel->getActiveSheet()->setCellValue('I5', 'Harga Satuan');
                $objPHPExcel->getActiveSheet()->setCellValue('J5', 'Usulan SKPD');
                $objPHPExcel->getActiveSheet()->setCellValue('L5', 'Kode Rekening');
                $objPHPExcel->getActiveSheet()->setCellValue('N5', 'Pajak');
                $objPHPExcel->getActiveSheet()->setCellValue('O5', 'Tahap');

                $objPHPExcel->getActiveSheet()->setCellValue('P5', 'Nama Sebelum');
                $objPHPExcel->getActiveSheet()->setCellValue('Q5', 'Harga Sebelum');
                $objPHPExcel->getActiveSheet()->setCellValue('R5', 'Alasan Perubahan');
                $objPHPExcel->getActiveSheet()->setCellValue('S5', 'Harga Pendukung 1');
                $objPHPExcel->getActiveSheet()->setCellValue('T5', 'Harga Pendukung 2');
                $objPHPExcel->getActiveSheet()->setCellValue('U5', 'Harga Pendukung 3');
                $objPHPExcel->getActiveSheet()->setCellValue('V5', 'Penyedia 1');
                $objPHPExcel->getActiveSheet()->setCellValue('W5', 'Penyedia 2');
                $objPHPExcel->getActiveSheet()->setCellValue('X5', 'Penyedia 3');
                $objPHPExcel->getActiveSheet()->setCellValue('Y5', 'Tahap Buku');
                $objPHPExcel->getActiveSheet()->setCellValue('Z5', 'Keterangan');
                

                if ($array_sheet[$sheet_ke] <> 'ESTS') {//                   \

                    $c_verifikasi = new Criteria();
                    $c_verifikasi->add(UsulanVerifikasiPeer::SHSD_ID, $array_sheet[$sheet_ke] . '%', Criteria::ILIKE);
                    $c_verifikasi->addAnd(UsulanVerifikasiPeer::TAHAP_BUKU, $tahap_cari, Criteria::EQUAL);
                    $c_verifikasi->addAscendingOrderByColumn(UsulanVerifikasiPeer::SHSD_ID);
                    $data_verifikasi = UsulanVerifikasiPeer::doSelect($c_verifikasi);

                    $kolom_ke = 6;
                    foreach ($data_verifikasi as $value) {
                        $c_usulan_ssh = new Criteria();
                        $c_usulan_ssh->add(UsulanSSHPeer::ID_USULAN, $value->getIdUsulan());
                        $data_usulan_ssh = UsulanSSHPeer::doSelectOne($c_usulan_ssh);

                        $c_dinas_usulan_ssh = new Criteria();
                        $c_dinas_usulan_ssh->add(UnitKerjaPeer::UNIT_ID, $value->getUnitId());
                        $data_dinas_usulan_ssh = UnitKerjaPeer::doSelectOne($c_dinas_usulan_ssh);

                        if ($data_usulan_ssh) {
                            $objPHPExcel->getActiveSheet()->setCellValue('P' . $kolom_ke, $data_usulan_ssh->getNamaSebelum());
                            $objPHPExcel->getActiveSheet()->setCellValue('Q' . $kolom_ke, $data_usulan_ssh->getHargaSebelum());
                            $objPHPExcel->getActiveSheet()->setCellValue('R' . $kolom_ke, $data_usulan_ssh->getAlasanPerubahanHarga());
                            $objPHPExcel->getActiveSheet()->setCellValue('S' . $kolom_ke, $data_usulan_ssh->getHargaPendukung1());
                            $objPHPExcel->getActiveSheet()->setCellValue('T' . $kolom_ke, $data_usulan_ssh->getHargaPendukung2());
                            $objPHPExcel->getActiveSheet()->setCellValue('U' . $kolom_ke, $data_usulan_ssh->getHargaPendukung3());
                            $objPHPExcel->getActiveSheet()->setCellValue('V' . $kolom_ke, $data_usulan_ssh->getCV1());
                            $objPHPExcel->getActiveSheet()->setCellValue('W' . $kolom_ke, $data_usulan_ssh->getCV2());
                            $objPHPExcel->getActiveSheet()->setCellValue('X' . $kolom_ke, $data_usulan_ssh->getCV3());
                            $objPHPExcel->getActiveSheet()->setCellValue('Y' . $kolom_ke, $value->getTahapBuku());
                            $objPHPExcel->getActiveSheet()->setCellValue('Z' . $kolom_ke, $value->getKeterangan());
                        }

                        $objPHPExcel->getActiveSheet()->setCellValue('C' . $kolom_ke, $value->getShsdId());

                        $c_data_shsd = new Criteria();
                        $c_data_shsd->add(KomponenAllPeer::KOMPONEN_ID, $value->getShsdId());                       
                        $data_shsd = KomponenAllPeer::doSelectOne($c_data_shsd);
                        if ($data_shsd) {
                            $objPHPExcel->getActiveSheet()->setCellValue('D' . $kolom_ke, $data_shsd->getNamaDasar());

                            $c_tahap_usulan_ssh = new Criteria();
                            $c_tahap_usulan_ssh->add(MasterTahapPeer::TAHAP_ID,$data_shsd->getTahapId());
                            $data_tahap_usulan_ssh = MasterTahapPeer::doSelectOne($c_tahap_usulan_ssh);
                            if($data_tahap_usulan_ssh)
                            {
                                $objPHPExcel->getActiveSheet()->setCellValue('O' . $kolom_ke, $data_tahap_usulan_ssh->getTahapName());
                            }
                        } else {
                            $objPHPExcel->getActiveSheet()->setCellValue('D' . $kolom_ke, $value->getShsdName());
                        }

                        $objPHPExcel->getActiveSheet()->setCellValue('E' . $kolom_ke, $value->getMerek());
                        $objPHPExcel->getActiveSheet()->setCellValue('F' . $kolom_ke, $value->getSpec());
                        $objPHPExcel->getActiveSheet()->setCellValue('G' . $kolom_ke, $value->getHiddenSpec());
                        $objPHPExcel->getActiveSheet()->setCellValue('H' . $kolom_ke, $value->getSatuan());
                        $objPHPExcel->getActiveSheet()->setCellValue('I' . $kolom_ke, $value->getHarga());
                        $objPHPExcel->getActiveSheet()->setCellValue('J' . $kolom_ke, $data_dinas_usulan_ssh->getUnitName());
                        $objPHPExcel->getActiveSheet()->setCellValue('L' . $kolom_ke, $value->getRekening());
                        $objPHPExcel->getActiveSheet()->setCellValue('N' . $kolom_ke, $value->getPajak());
                        $kolom_ke++;
                    }
                } else {
//                   
                    $c_verifikasi = new Criteria();
                    $c_verifikasi->add(UsulanVerifikasiPeer::TIPE, 'SSH', Criteria::ALT_NOT_EQUAL);
                    $c_verifikasi->addAnd(UsulanVerifikasiPeer::TAHAP_BUKU, $tahap_cari, Criteria::EQUAL);
                    $c_verifikasi->addAscendingOrderByColumn(UsulanVerifikasiPeer::SHSD_ID);
                    $data_verifikasi = UsulanVerifikasiPeer::doSelect($c_verifikasi);

                    $kolom_ke = 6;
                    foreach ($data_verifikasi as $value) {
                        $c_usulan_ssh = new Criteria();
                        $c_usulan_ssh->add(UsulanSSHPeer::ID_USULAN, $value->getIdUsulan());
                        $data_usulan_ssh = UsulanSSHPeer::doSelectOne($c_usulan_ssh);

                        $c_dinas_usulan_ssh = new Criteria();
                        $c_dinas_usulan_ssh->add(UnitKerjaPeer::UNIT_ID, $value->getUnitId());
                        $data_dinas_usulan_ssh = UnitKerjaPeer::doSelectOne($c_dinas_usulan_ssh);

                        if ($data_usulan_ssh) {
                            $objPHPExcel->getActiveSheet()->setCellValue('P' . $kolom_ke, $data_usulan_ssh->getNamaSebelum());
                            $objPHPExcel->getActiveSheet()->setCellValue('Q' . $kolom_ke, $data_usulan_ssh->getHargaSebelum());
                            $objPHPExcel->getActiveSheet()->setCellValue('R' . $kolom_ke, $data_usulan_ssh->getAlasanPerubahanHarga());
                        }

                        $objPHPExcel->getActiveSheet()->setCellValue('C' . $kolom_ke, $value->getShsdId());

                        $c_data_shsd = new Criteria();
                        $c_data_shsd->add(KomponenAllPeer::KOMPONEN_ID, $value->getShsdId());
                        $data_shsd = KomponenAllPeer::doSelectOne($c_data_shsd);
                        if ($data_shsd) {
                            $objPHPExcel->getActiveSheet()->setCellValue('D' . $kolom_ke, $data_shsd->getNamaDasar());

                            $c_tahap_usulan_ssh = new Criteria();
                            $c_tahap_usulan_ssh->add(MasterTahapPeer::TAHAP_ID,$data_shsd->getTahapId());
                            $data_tahap_usulan_ssh = MasterTahapPeer::doSelectOne($c_tahap_usulan_ssh);
                            if($data_tahap_usulan_ssh)
                            {
                                $objPHPExcel->getActiveSheet()->setCellValue('O' . $kolom_ke, $data_tahap_usulan_ssh->getTahapName());
                            }
                        } else {
                            $objPHPExcel->getActiveSheet()->setCellValue('D' . $kolom_ke, $value->getShsdName());
                        }

                        $objPHPExcel->getActiveSheet()->setCellValue('E' . $kolom_ke, $value->getMerek());
                        $objPHPExcel->getActiveSheet()->setCellValue('F' . $kolom_ke, $value->getSpec());
                        $objPHPExcel->getActiveSheet()->setCellValue('G' . $kolom_ke, $value->getHiddenSpec());
                        $objPHPExcel->getActiveSheet()->setCellValue('H' . $kolom_ke, $value->getSatuan());
                        $objPHPExcel->getActiveSheet()->setCellValue('I' . $kolom_ke, $value->getHarga());
                        $objPHPExcel->getActiveSheet()->setCellValue('J' . $kolom_ke, $data_dinas_usulan_ssh->getUnitName());
                        $objPHPExcel->getActiveSheet()->setCellValue('L' . $kolom_ke, $value->getRekening());
                        $objPHPExcel->getActiveSheet()->setCellValue('N' . $kolom_ke, $value->getPajak());
                        $kolom_ke++;
                    }
                }
            }

            $objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename=export_ssh_per_tahapbuku_' . $tahap_cari . '.xls');
            header('Cache-Control: max-age=0');
            $objWriter->save('php://output');
            exit();
        } catch (Exception $exc) {
            echo $exc->getMessage();
            exit();
        }
    }

}




