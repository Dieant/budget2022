<div class="box box-info">        
    <div class="box-body text-center">
        <div class="btn-group btn-group-justified" role="group">
            <?php
            if ($sf_user->getCredentialMember() == 'admin' || $sf_user->getCredentialMember() == 'admin_super') {
                echo link_to('Export Per Hari', 'usulan_ssh/printSemiBukuPerHari', array('class' => 'btn btn-flat btn-default text-bold'));
                echo link_to('Export Semua', 'usulan_ssh/printSemiBuku', array('class' => 'btn btn-flat btn-default text-bold'));
            }
            ?>        

        </div>
        <div class="clearfix"></div>
    </div><!-- /.box-body -->
</div><!-- /.box -->