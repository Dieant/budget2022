<?php use_helper('I18N','Url', 'Form','Date', 'Object', 'Javascript', 'Validation') ?>
<tr style="text-align: center; font-weight: bold">
    <th>Tipe</th>
    <th>Nama</th>
    <th>Spesifikasi</th>
    <th>Hidden Spec</th>
    <th>Merek</th>
    <th>Kode Barang</th>
    <th>Rekening</th>
    <th>Satuan</th>
    <th>Harga</th>
    <th>Pajak</th>
    <th>Status</th>
    <!-- <th>Action</th> -->
</tr>
<?php
foreach ($rs_usulan as $usulan) {
    if ($usulan->getSurveyor() == '') {
        ?>
        <tr style="text-align: center">
            <td><?php echo $usulan->getTipeUsulan() ?></td>
            <td><?php echo $usulan->getNama() ?></td>
            <td><?php echo $usulan->getSpec() ?></td>
            <td><?php echo $usulan->getHiddenSpec() ?></td>
            <td><?php echo $usulan->getMerek() ?></td>
            <td><?php echo $usulan->getKodeBarang() ?></td>
            <td><?php echo $usulan->getRekening() ?></td>
            <td><?php echo $usulan->getSatuan() ?></td>
            <td style="text-align: right"><?php echo number_format($usulan->getHarga()) ?></td>
            <td><?php echo $usulan->getPajak() ?></td>
            <td style="font-weight: bold">
                <?php
                echo date("d-m-Y H:i", strtotime($usulan->getUpdatedAt())) . '<br/>';
                if ($usulan->getStatusHapus() == TRUE) {
                    echo '<center><font style="color:red;font-weight:bold">Ditolak</font></center>';
                }
                ?>
            </td>
            <!-- <td> -->
                <!-- <div class="btn-group"> -->
                    <?php
                    // if ($usulan->getStatusHapus() == FALSE) {
                    //     if ($usulan->getStatusVerifikasi() == FALSE) {
                            ?>
                            <!-- <div class="form-group">                                
                                <div class="col-sm-12"> -->
                                    <?php
                                    // $c = new Criteria();
                                    // $c->addAscendingOrderByColumn(SurveyorPeer::NAMA);
                                    // $v = SurveyorPeer::doSelect($c);      

                                    // echo select_tag('nama', objects_for_select($v, 'getNama', isset($filters['nama']) ? $filters['nama'] : null, array('include_custom' => '------Semua Nama Surveyor------')), array('class' => 'js-example-basic-single form-control select2', 'onChange' => 'showIdUsulan(' . $usulan->getidUsulan() . ',this)'));

                                    

                                    // echo select_tag('nama', objects_for_select($v, 'getNama', isset($filters['nama']) ? $filters['nama'] : null, array('include_custom' => '------Semua Nama Surveyor------')), array('class' => 'js-example-basic-single form-control select2' ,'onChange' => 'showIdUsulan(' . $usulan->getidUsulan() . ',this)'));

                                    // echo select_tag('filters[unit_id]', objects_for_select($v, 'getUnitId', 'getUnitName', isset($filters['unit_id']) ? $filters['unit_id'] : null, array('include_custom' => '--Pilih Perangkat Daerah--')), Array('class' => 'js-example-basic-single form-control select2'));                                   
                                    ?>

                                    <!-- coba select manual -->
                                    <!-- <select name="nama" id="nama" class="js-example-basic-single form-control select2" onchange="showIdUsulan('<?php //echo $usulan->getidUsulan() ?>',this)">
                                        <option value="">--Pilih Surveyor--</option>
                                        <?php //foreach($v as $surveyor) { ?>
                                        <option value="<?php //echo $surveyor->getNama(); ?>" <?php //((isset($filters['nama'])&&$filters['nama']==$surveyor->getNama()) ? 'selected' : '') ?> ><?php //echo $surveyor->getNama(); ?></option>
                                        <?php //} ?>
                                    </select> -->

                                <!-- </div>
                            </div>          -->
                            <?php
                    //     }
                    // }
                    ?>
                <!-- </div> -->
            <!-- </td> -->
        </tr>  
        <?php
    }
}
?>
<script>
    function showIdUsulan(id,_this) {
        var id_usulan=id;
        var surveyor=_this.value;
        console.log('id='+id_usulan+' & surveyor='+surveyor);

        $.ajax({
            type:'GET',
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/usulan_ssh/getNamaSurveyor/id/" + id_usulan + "/surveyor/" + surveyor + ".html",
            context: document.body
        }).done(function (msg) {
            $(id_usulan).html(msg);
        });
        window.location.reload();

        //alert(id+'-'+surveyor);
    }
</script>