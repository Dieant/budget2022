<?php use_helper('Url', 'Javascript', 'Form', 'Object'); ?>
<tr style="text-align: center; font-weight: bold">
    <th>Tipe</th>
    <th>Nama</th>
    <th>Spesifikasi</th>
    <th>Hidden Spec</th>
    <th>Merek</th>
    <th>Keterangan</th>
    <th>Kode Barang</th>
    <th>Rekening</th>
    <th>Satuan</th>
    <th>Harga</th>
    <th>Pajak</th>
    <th>Status</th>
    <th>Catatan Surveyor</th>
    <th>Action</th>
</tr>
<?php
foreach ($rs_usulan as $usulan) {
    if ($usulan->getStatusConfirmasiPenyelia() == true) {
        ?>
        <tr style="text-align: center">
            <td><?php echo $usulan->getTipeUsulan() ?></td>
            <td><?php echo $usulan->getNama() ?></td>
            <td><?php echo $usulan->getSpec() ?></td>
            <td><?php echo $usulan->getHiddenSpec() ?></td>
            <td><?php echo $usulan->getMerek() ?></td>
            <td><?php echo $usulan->getKeterangan() ?></td>
            <td><?php echo $usulan->getKodeBarang() ?></td>
            <td><?php echo $usulan->getRekening() ?></td>
            <td><?php echo $usulan->getSatuan() ?></td>
            <td style="text-align: right"><?php echo number_format($usulan->getHarga()) ?></td>
            <td><?php echo $usulan->getPajak() ?></td>
            <td style="font-weight: bold">
                <?php
                echo date("d-m-Y H:i", strtotime($usulan->getUpdatedAt())) . '<br/>';
                if ($usulan->getStatusHapus() == TRUE) {
                    echo '<center><font style="color:red;font-weight:bold">Ditolak</font></center>';
                } else {
                    if ($usulan->getStatusVerifikasi() == FALSE) {
                        if ($usulan->getStatusConfirmasiPenyelia() == false) {
                            echo '<center><font style="color:blue;font-weight:bold">Menunggu Konfirmasi</font></center>';
                        }
                    } else {
                        echo '<center><font style="color:green;font-weight:bold">Verifikasi</font></center>';
                    }
                }
                ?>
            </td>
            <td><?php echo $usulan->getKomentarSurvey()?></td>
            <td>
                <div class="btn-group">
                    <?php
                    if ($usulan->getStatusHapus() == FALSE) {
                        if ($usulan->getStatusVerifikasi() == FALSE) {
                            if ($usulan->getStatusConfirmasiPenyelia() == TRUE) {
                                echo link_to('<i class="fa fa-search"></i>', 'usulan_ssh/usulansshcek?id=' . $usulan->getIdUsulan() . '&key=' . md5('cek_ssh'), array('class' => 'btn btn-outline-primary btn-sm'));
                                ?>
                                <?php
                            }
                        }
                    }
                    ?>
                </div>
            </td>
        </tr>  
        <?php
    }
}
?>