<?php use_helper('I18N', 'Date', 'Object', 'Javascript', 'Validation'); ?>
<section class="content-header">
    <h1>Ditemukan kesalahan pada Data Upload Usulan Komponen</h1>
</section>

<!-- Main content -->
<section class="content">
    <!-- Default box -->
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Silahkan Cek Isian Data Excel Anda</h3>
        </div>
        <div class="box-body">
            <ul>
                <?php
                $array_salah = explode('|', $string_salah);
                foreach ($array_salah as $value) {
                    if ($value <> '') {
                        echo '<li>' . $value . '</li>';
                    }
                }
                ?>
            </ul>
        </div>
        <div class="box-footer">
            <button onclick="javascript:history.back(-1)">Kembali ke halaman sebelumnya</button>
        </div>
    </div>
</section>