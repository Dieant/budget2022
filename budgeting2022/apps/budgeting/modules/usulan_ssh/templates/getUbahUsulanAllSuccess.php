<?php use_helper('Url', 'Javascript', 'Form', 'Object'); ?>
<tr style="text-align: center; font-weight: bold">
    <th>Tipe</th>
    <th>Nama</th>
    <th>Spesifikasi</th>
    <th>Hidden Spec</th>
    <th>Merek</th>
    <th>Kode Barang</th>
    <th>Rekening</th>
    <th>Satuan</th>
    <th>Harga</th>
    <th>Pajak</th>
    <th>Status</th>
    <th>Action</th>
</tr>
<?php
foreach ($rs_usulan as $usulan) {
    if (!is_null($usulan->getSurveyor())) {
        ?>
        <tr style="text-align: center">
            <td><?php echo $usulan->getTipeUsulan() ?></td>
            <td><?php echo $usulan->getNama() ?></td>
            <td><?php echo $usulan->getSpec() ?></td>
            <td><?php echo $usulan->getHiddenSpec() ?></td>
            <td><?php echo $usulan->getMerek() ?></td>
            <td><?php echo $usulan->getKodeBarang() ?></td>
            <td><?php echo $usulan->getRekening() ?></td>
            <td><?php echo $usulan->getSatuan() ?></td>
            <td style="text-align: right"><?php echo number_format($usulan->getHarga()) ?></td>
            <td><?php echo $usulan->getPajak() ?></td>
            <td style="font-weight: bold">
                <?php
                echo date("d-m-Y H:i", strtotime($usulan->getUpdatedAt())) . '<br/>';
                if ($usulan->getStatusHapus() == TRUE) {
                    echo '<center><font style="color:red;font-weight:bold">Ditolak</font></center>';
                }
                ?>
            </td>
            <td>
                <div class="btn-group">
                    <?php
                    if ($usulan->getStatusHapus() == FALSE) {
                        if ($usulan->getStatusVerifikasi() == FALSE) {
                            ?>
                            <div class="form-group">                                
                                <div class="col-sm-12">
                                    <?php
                                    $c = new Criteria();
                                    $c->addAscendingOrderByColumn(SurveyorPeer::NAMA);
                                    $v = SurveyorPeer::doSelect($c);            
                                    echo select_tag('nama', objects_for_select($v, 'getNama', isset($filters['nama']) ? $filters['nama'] : null, array('include_custom' => '------Semua Nama Surveyor------')), array('class' => 'form-control select2' ,'onChange' => 'showIdUsulan(' . $usulan->getidUsulan() . ',this)'));
                                    ?>
                                </div>
                            </div>         
                            <?php
                        }
                    }
                    ?>
                </div>
            </td>
        </tr>  
        <?php
    }
}
?>

<script>
    $(document).ready(function() {
        $(".js-example-basic-single").select2();
    });

    function showIdUsulan(id,_this) {
        var id_usulan=id;
        var surveyor=_this.value;

        $.ajax({
            type:'GET',
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/usulan_ssh/getNamaSurveyor/id/" + id_usulan + "/surveyor/" + surveyor + ".html",
            context: document.body
        }).done(function (msg) {
            $(id_usulan).html(msg);
        });
    }
</script>