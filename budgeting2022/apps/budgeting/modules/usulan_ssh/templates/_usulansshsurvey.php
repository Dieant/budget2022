<?php
// auto-generated by sfPropelAdmin
// date: 2008/05/20 10:08:26
?>
<div id="sf_admin_container" class="table-responsive">
    <table cellspacing="0" class="sf_admin_list">    
        <thead>
            <tr>
                <th>SKPD</th>
                <th>Nama</th>
                <th>Spesifikasi</th>
                <th>Hidden Spec</th>
                <th>Merek</th>
                <th>Rekening</th>
                <th>Satuan</th>
                <th>Harga</th>
                <th>Pajak</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $user=$sf_user->getNamaLengkap();
            $i = 1;
            foreach ($pager->getResults() as $usulanssh){                
                $survey = $usulanssh->getSurveyor();     
               if( $user == $survey)
               {   
                  $odd = fmod( ++$i, 2)        
                ?>
                <tr class="sf_admin_row_<?php echo $odd ?>">
                    <td>
                        <?php
                        echo '<b>(' . $usulanssh->getUnitId() . ') ' . UnitKerjaPeer::getStringUnitKerja($usulanssh->getUnitId()) . '</b><br/>';
                        echo 'Nomor Surat : <b>' . UsulanSPJMPeer::getStringNomorSurat($usulanssh->getIdSpjm()) . '</b><br/>';
                        echo 'File Rar Upload: <b><a href="' . UsulanDinasPeer::getStringFilepath($usulanssh->getIdSpjm()) . '">Download RAR</a></b><br/>';
                        echo 'File Excel Upload : <b><a href="' . UsulanSPJMPeer::getStringFilepath($usulanssh->getIdSpjm()) . '">Download Excel</a></b>';
                        ?>
                    </td>
                    <td><?php echo $usulanssh->getNama() ?></td>                   
                    <td><?php echo $usulanssh->getSpec() ?></td>
                    <td><?php echo $usulanssh->getHiddenSpec() ?></td>
                    <td style="text-align: center"><?php echo $usulanssh->getMerek() ?></td>
                    <td style="text-align: center"><?php echo $usulanssh->getRekening() ?></td>
                    <td style="text-align: center"><?php echo $usulanssh->getSatuan() ?></td>
                    <td style="text-align: right"><?php echo number_format($usulanssh->getHarga()) ?></td>
                    <td style="text-align: center"><?php echo $usulanssh->getPajak() ?></td>
                    <td style="text-align: center; font-weight: bold">
                        <?php
                        $namasurvey = $usulanssh->getSurveyor();
                        if ($usulanssh->getStatusHapus() == t) {
                            echo '<font style="color:red">Ditolak</font>';
                        } else {
                            if ($usulanssh->getStatusVerifikasi() == t) {
                                echo '<font style="color:green">Terverifikasi</font>';
                            } else {
                                echo '<font style="color:blue">Antrian</font><br/>';
                                if(!empty($namasurvey) && $usulanssh->getStatusConfirmasiPenyelia()==t && $usulanssh->getStatusSurvey()==t){
                                    echo '<font style="color:blue">Posisi di Tim Data</font>';
                                } elseif(!empty($namasurvey) && $usulanssh->getStatusConfirmasiPenyelia()==t){
                                    echo '<font style="color:blue">Posisi di Survey</font>';
                                } elseif($usulanssh->getStatusPending()==t){
                                    echo '<font style="color:blue">Posisi Komponen Pending</font>';
                                } elseif($usulanssh->getStatusConfirmasiPenyelia()==t && empty($namasurvey)){
                                    echo '<font style="color:blue">Posisi di Tim Data</font>';
                                } else{
                                    echo '<font style="color:blue">Posisi di Penyelia</font>';
                                }
                            }
                        }
                        echo '<br/>';
                        echo $usulanssh->getKomentarSurvey();
                        ?>
                    </td>
                    <td style="text-align: center; font-weight: bold">
                        <div class="btn-group">
                            <?php
                            echo link_to('<i class="fa fa-check"></i> Cek', 'usulan_ssh/usulanprosessurvey?id=' . $usulanssh->getIdUsulan() . '&key=' . md5('cek_konfirmasi_usulan'), array('class' => 'btn btn-default'));
                            ?>
                        </div>
                    </td>
                </tr>
            <?php    
            }        
        } ?>
        </tbody>
        <tfoot>
            <tr>
                <th colspan="11">
        <div class="float-right">
            <?php if ($pager->haveToPaginate()): ?>
                <?php echo link_to(image_tag(sfConfig::get('sf_admin_web_dir') . '/images/first.png', array('align' => 'absmiddle', 'alt' => __('First'), 'title' => __('First'))), 'usulan_ssh/usulansshsurvey?page=1') ?>
                <?php echo link_to(image_tag(sfConfig::get('sf_admin_web_dir') . '/images/previous.png', array('align' => 'absmiddle', 'alt' => __('Previous'), 'title' => __('Previous'))), 'usulan_ssh/usulansshsurvey?page=' . $pager->getPreviousPage()) ?>

                <?php foreach ($pager->getLinks() as $page): ?>
                    <?php echo link_to_unless($page == $pager->getPage(), $page, 'usulan_ssh/usulansshsurvey?page=' . $page) ?>
                <?php endforeach; ?>

                <?php echo link_to(image_tag(sfConfig::get('sf_admin_web_dir') . '/images/next.png', array('align' => 'absmiddle', 'alt' => __('Next'), 'title' => __('Next'))), 'usulan_ssh/usulansshsurvey?page=' . $pager->getNextPage()) ?>
                <?php echo link_to(image_tag(sfConfig::get('sf_admin_web_dir') . '/images/last.png', array('align' => 'absmiddle', 'alt' => __('Last'), 'title' => __('Last'))), 'usulan_ssh/usulansshsurvey?page=' . $pager->getLastPage()) ?>
            <?php endif; ?>
        </div>
        <?php echo format_number_choice('[0] no result|[1] 1 result|(1,+Inf] %1% results', array('%1%' => $pager->getNbResults()), $pager->getNbResults()) ?>
        </th>
        </tr>
        </tfoot>
    </table>
</div>