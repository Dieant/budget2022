<?php use_helper('Url', 'Javascript', 'Form', 'Object'); ?>
<?php
$i = 0;
?>
<tr class="ssh_lalu_<?php echo $id ?>" style="text-align: center; font-weight: bold">
    <th colspan="5">Nama Komponen</th>
    <th colspan="4">Harga</th>
    <th colspan="3">Pajak</th>
</tr>
<?php
foreach ($list as $value) {
    if ($value['komponen_non_pajak'] == 't') {
        $pajak = 0;
    } else {
        $pajak = 10;
    }
    ?>
    <tr class="ssh_lalu_<?php echo $id ?>" style="text-align: center">
        <td colspan="5"><?php echo $value['komponen_name'] ?></td>
        <td colspan="4"><?php echo number_format($value['komponen_harga'], 0, ',', '.') ?></td>
        <td colspan="3"><?php echo $pajak ?></td>
    </tr>
    <?php
}
?>
<tr class="ssh_lalu_<?php echo $id ?>">
    <td colspan="13">&nbsp;</td>
</tr>