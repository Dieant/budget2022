<?php use_helper('I18N', 'Date', 'Object', 'Javascript', 'Validation') ?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Antrian Usulan</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="#">Usulan SHS</a></li>
                  <li class="breadcrumb-item active">Antrian Usulan</li>
              </ol>
            </div>
        </div>
    </div>
</section>
<section class="content">
    <?php include_partial('usulan_ssh/list_messages'); ?>
    <div class="container-fluid">
        <div class="row">
          <div class="col-3">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">
                        <i class="fab fa-foursquare"></i> Ubah Data SPJM
                    </h3>
                </div>
                <div class="card-body">
                    <?php echo form_tag('usulan_ssh/updateSPJM', array('class' => 'form-horizontal')); ?>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                            <label>Nama Penyelia</label>
                            <span class="form-control"><?php echo $data_spjm->getPenyelia(); ?></span>
                        </div>
                      </div>
                      <!-- /.col -->
                      <div class="col-md-12">
                        <div class="form-group">
                            <label>Perangkat Daerah</label>
                            <span class="form-control"><?php echo UnitKerjaPeer::getStringUnitKerja($data_spjm->getSkpd()); ?></span>
                        </div>
                      </div>
                      <!-- /.col -->
                      <div class="col-md-12">
                        <div class="form-group">
                            <label>Tanggal SPJM</label>
                            <span class="form-control"><?php echo date("j F Y", strtotime($data_spjm->getTglSpjm())); ?></span>
                        </div>
                      </div>
                      <!-- /.col -->
                      <div class="col-md-12">
                        <div class="form-group">
                            <label>Tipe Usulan</label>
                            <span class="form-control"><?php echo $data_spjm->getTipeUsulan(); ?></span>
                        </div>
                      </div>
                      <!-- /.col -->
                      <div class="col-md-12">
                        <div class="form-group">
                            <label>Jenis Usulan</label>
                            <span class="form-control"><?php echo $data_spjm->getJenisUsulan(); ?></span>
                        </div>
                      </div>
                      <!-- /.col -->
                      <div class="col-md-12">
                        <div class="form-group">
                            <label>Jumlah Dukungan</label>
                            <input type="number" name="jumlah_dukungan" value="<?php echo $data_spjm->getJumlahDukungan() ?>" required class="form-control"/>
                        </div>
                      </div>
                      <!-- /.col -->
                      <div class="col-md-12">
                        <div class="form-group">
                            <label>Nomor Surat</label>
                            <?php echo input_tag('nomor surat', $data_spjm->getNomorSurat(), array('class' => 'form-control')); ?>
                        </div>
                      </div>
                      <!-- /.col -->
                      <div class="col-md-12">
                        <div class="form-group">
                            <label>Catatan Verifikator</label>
                            <?php echo textarea_tag('komentar_verifikator', '', array('class' => 'form-control')); ?>
                        </div>
                      </div>
                      <!-- /.col -->
                      <div class="col-md-12">
                        <div class="form-group">
                            <label class="tombol_filter">Tombol Filter</label><br/>
                            <button type="submit" name="commit" class="btn btn-outline-primary btn-sm">Simpan <i class="far fa-save"></i></button>
                            <button type="reset" name="reset" class="btn btn-outline-danger btn-sm">Reset <i class="fa fa-backspace"></i></button>
                        </div>
                        <!-- /.form-group -->
                      </div>
                      <!-- /.col -->  
                    </div>                    
                    <?php echo '</form>'; ?>
                </div>
            </div>
          </div>  
          <div class="col-9">
            <div class="card">
                <div class="card-body table-responsive">
                    <table class="table table-hover">
                        <thead class="head_peach">
                            <tr>
                                <th>Tipe</th>
                                <th>Nama</th>
                                <th>Spesifikasi</th>
                                <th>Hidden Spec</th>
                                <th>Merek</th>
                                <th>Keterangan</th>
                                <th>Kode Barang</th>
                                <th>Rekening</th>
                                <th>Satuan</th>
                                <th>Harga</th>
                                <th>Pajak</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($list_usulan as $usulan) {
                                ?>
                                <tr style="text-align: center">
                                    <td><?php echo $usulan->getTipeUsulan() ?></td>
                                    <td><?php echo $usulan->getNama() ?></td>
                                    <td><?php echo $usulan->getSpec() ?></td>
                                    <td><?php echo $usulan->getHiddenSpec() ?></td>
                                    <td><?php echo $usulan->getMerek() ?></td>
                                    <td><?php echo $usulan->getKeterangan() ?></td>
                                    <td><?php echo $usulan->getKodeBarang() ?></td>
                                    <td><?php echo $usulan->getRekening() ?></td>
                                    <td><?php echo $usulan->getSatuan() ?></td>
                                    <td style="text-align: right">
                                        <?php
                                        if ($usulan->getSatuan() == '%') {
                                            echo $usulan->getHarga();
                                        } else {
                                            echo number_format($usulan->getHarga());
                                        }
                                        ?>
                                    </td>
                                    <td><?php echo $usulan->getPajak() ?></td>
                                    <td style="font-weight: bold">
                                        <?php
                                        echo date("d-m-Y H:i", strtotime($usulan->getUpdatedAt())) . '<br/>';
                                        if ($usulan->getStatusHapus() == TRUE) {
                                            echo '<center><font style="color:red;font-weight:bold">Ditolak</font></center>';
                                        } else {
                                            if ($usulan->getStatusVerifikasi() == FALSE) {
                                                if ($usulan->getStatusConfirmasiPenyelia() == false) {
                                                    echo '<center><font style="color:blue;font-weight:bold">Menunggu Konfirmasi</font></center>';
                                                }
                                            } else {
                                                echo '<center><font style="color:green;font-weight:bold">Verifikasi</font></center>';
                                            }
                                        }
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                        if ($usulan->getStatusHapus() == FALSE) {
                                            if ($usulan->getStatusVerifikasi() == FALSE) {
                                                if ($usulan->getStatusConfirmasiPenyelia() == TRUE) {
                                                    echo link_to('<i class="fa fa-search"></i>', 'usulan_ssh/usulansshcek?id=' . $usulan->getIdUsulan() . '&key=' . md5('cek_ssh'), array('class' => 'btn btn-outline-primary btn-sm'));
                                                }
                                            }
                                        }
                                        ?>
                                    </td>
                                </tr>  
                                <?php
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
          </div>
        </div>
    </div>
</section>