<?php use_helper('Url', 'Javascript', 'Form', 'Object'); ?>
<?php
$i = 0;
?>
<tr class="mirip_ssh_<?php echo $id ?>" style="text-align: center; font-weight: bold">
    <th>Tipe</th>
    <th>Kode SSH</th>
    <th>Nama SSH</th>
    <th>Spesifikasi</th>
    <th>Hidden Spec</th>
    <th>Merek</th>
    <th>Rekening</th>
    <th>Satuan</th>
    <th>Harga</th>
    <th>Pajak</th>
    <th>Status</th>
    <th colspan="2">Usulan</th>
</tr>
<?php
foreach ($list_ssh_mirip as $ssh) {
    $con = Propel::getConnection();

    $query_di_komponen_rekening = "select count(*) as total "
            . "from " . sfConfig::get('app_default_schema') . ".komponen_rekening "
            . "where komponen_id ilike '" . $ssh['komponen_id'] . "' ";
    $stmt_di_komponen_rekening = $con->prepareStatement($query_di_komponen_rekening);
    $rs_di_komponen_rekening = $stmt_di_komponen_rekening->executeQuery();
    while ($rs_di_komponen_rekening->next()) {
        $total_di_komponen_rekening = $rs_di_komponen_rekening->getString('total');
    }
    ?>
    <tr class="mirip_ssh_<?php echo $id ?>" style="text-align: center">
        <td><?php echo $satukode['komponen_tipe'] ?></td>
        <td><?php echo $ssh['komponen_id'] ?></td>
        <td><?php echo $ssh['komponen_name'] ?></td>
        <td><?php echo $ssh['spec'] ?></td>
        <td><?php echo $ssh['hidden_spec'] ?></td>
        <td><?php echo $ssh['merk'] ?></td>
        <td>
            <td><?php 
            $query_rekening = "select rekening_code "
            . "from " . sfConfig::get('app_default_schema') . ".komponen_rekening "
            . "where komponen_id = '" . $satukode['komponen_id'] . "' ";
            $stmt_rekening = $con->prepareStatement($query_rekening);
            $rs_rekening = $stmt_rekening->executeQuery();
            while ($rs_rekening->next()) {
                echo $rekening = $rs_rekening->getString('rekening_code');
            } 
            ?>   
        </td>
        </td>
        <td><?php echo $ssh['satuan_name'] ?></td>
        <td style="text-align: right">
            <?php
            if ($ssh['satuan_name'] == '%') {
                echo $ssh['komponen_harga'];
            } else {
                echo number_format($ssh['komponen_harga']);
            }
            ?>
        </td>
        <td style="text-align: center">
            <?php
            if ($ssh['non_pajak'] == t) {
                echo '0';
            } else {
                echo '10';
            }
            ?>
        </td>
        <td style="font-weight: bold">
            <?php
            if ($total_di_komponen_rekening > 0) {
                echo 'Tersedia';
            } else {
                echo 'Tersembunyi';
            }
            ?>
        </td>
        <td colspan="2"><?php echo $ssh['usulan_skpd'] ?></td>
    </tr>  
    <?php
}
?>
<tr class="mirip_ssh_<?php echo $id ?>">
    <td colspan="13">&nbsp;</td>
</tr>  