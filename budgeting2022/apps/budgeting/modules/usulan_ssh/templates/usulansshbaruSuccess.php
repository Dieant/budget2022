<?php use_helper('I18N', 'Date', 'Object', 'Javascript', 'Validation') ?>
<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Tambah Usulan SHS</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Usulan SHS</a></li>
          <li class="breadcrumb-item active">Tambah Usulan</li>
        </ol>
      </div>
    </div>
    <div class="text-right">
        <?php echo link_to('Format File Upload ', sfConfig::get('app_path_pdf') . 'template/template_usulan_eBudgeting2022.xls', array('class' => 'btn btn-sm btn-outline-primary', 'target' => '_blank')); ?> 
    </div>
  </div>
</section>
<!-- Main content -->
<section class="content">
    <?php include_partial('usulan_ssh/list_messages'); ?>
    <div class="container-fluid">
        <div class="row">
          <div class="col-md-6">
            <div class="card">
                <div class="card-body table-responsive p-0">
                    <table class="table table-hover">
                        <thead class="head_peach">
                            <tr>
                                <th>Tanggal</th>
                                <th>Nomor Surat</th>
                                <th>Satuan Kerja</th>
                                <th>Jumlah Usulan</th>
                                <th>Token</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($list_token as $value) { ?>
                                <tr>
                                    <td style="text-align: center"><?php echo date("d-m-Y H:i", strtotime($value->getRequiredAt())); ?></td>
                                    <td style="text-align: center"><?php echo $value->getNomorSurat(); ?></td>
                                    <td style="text-align: center"><?php echo UnitKerjaPeer::getStringUnitKerja($value->getUnitId()); ?></td>
                                    <td style="text-align: center"><?php echo $value->getJumlahSsh(); ?></td>
                                    <td style="text-align: center; font-weight: bold"><?php echo $value->getToken(); ?></td>
                                </tr>
                            <?php }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">
                        <i class="fab fa-foursquare"></i> Form Tambah
                    </h3>
                </div>
                <div class="card-body">
                    <?php echo form_tag('usulan_ssh/saveusulan', array('multipart' => true, 'class' => 'form-horizontal')); ?>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                            <label>Nama Penyelia</label>
                            <input type="text" name="nama_penyelia" readonly class="form-control" value="<?php echo $sf_user->getNamaUser() ?>" />
                        </div>
                      </div>
                      <!-- /.col -->
                      <div class="col-md-6">
                        <div class="form-group">
                            <label>Tanggal</label>
                            <?php echo input_date_tag('tanggal_spjm', null, array('rich' => true, 'required' => true, 'class' => 'form-control')); ?>
                        </div>
                      </div>
                      <!-- /.col -->
                      <div class="col-md-6">
                        <div class="form-group">
                            <label>Perangkat Daerah</label>
                            <select name="skpd" class="form-control select2">                        
                                <?php
                                if ($sf_user->getCredentialMember() == 'peneliti') {
                                    foreach ($unit_kerja_handle as $value) {
                                        ?>
                                        <option value="<?php echo $value->getUnitId(); ?>"><?php echo $value->getUnitName() ?></option>
                                        <?php
                                    }
                                } else if ($sf_user->getCredentialMember() == 'admin' || $sf_user->getCredentialMember() == 'admin_super') {
                                    foreach ($unit_kerja as $value) {
                                        ?>
                                        <option value="<?php echo $value->getUnitId(); ?>"><?php echo $value->getUnitName() ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>       
                        </div>
                      </div>
                      <!-- /.col -->
                      <div class="col-md-6">
                        <div class="form-group">
                            <label>Kategori Usulan</label>
                            <select name="kategori_usulan" class="form-control select2">
                                <option value="SSH">SSH</option>
                                <option value="ESTIMASI">ESTIMASI</option>                        
                                <option value="SSHESTIMASI">SSH & ESTIMASI</option>
                            </select>
                        </div>
                      </div>
                      <!-- /.col -->
                      <div class="col-md-6">
                        <div class="form-group">
                            <label>Jenis Usulan</label>
                            <select name="jenis_usulan" class="form-control select2">
                                <option value="BARU">BARU</option>
                                <option value="PERUBAHAN HARGA">PERUBAHAN HARGA</option>
                            </select>
                        </div>
                      </div>
                      <!-- /.col -->
                      <?php if ($sf_user->getNamaUser() == 'admin') { ?>
                      <div class="col-md-6">
                        <div class="form-group">
                            <label>Nomor SPJM [Hanya Login Admin]</label>
                            <input type="text" name="spjm" required  class="form-control"/>
                            <input type="hidden" name="token"/>
                        </div>
                      </div>
                      <!-- /.col -->
                      <div class="col-md-6">
                        <div class="form-group">
                            <label>Jumlah SSH [Hanya Login Admin]</label>
                            <input type="number" name="jumlah_ssh" required  class="form-control"/>
                        </div>
                      </div>
                      <!-- /.col -->
                      <?php } else { ?>
                      <div class="col-md-6">
                        <div class="form-group">
                            <label>Token</label>
                            <input type="text" name="token" required  class="form-control"/>
                            <input type="hidden" name="spjm"/>
                            <input type="hidden" name="jumlah_ssh"/>
                        </div>
                      </div>
                      <!-- /.col -->
                      <?php } ?>
                      <div class="col-md-6">
                        <div class="form-group">
                            <label>Jumlah dukungan Penyedia berbeda</label>
                            <input type="number" name="jumlah_dukungan" value="0" required class="form-control"/>
                        </div>
                      </div>
                      <!-- /.col -->
                      <div class="col-md-6">
                        <div class="form-group">
                            <label>Upload Usulan Komponen: (Gunakan File Excel .xls hasil download)</label>
                            <?php echo input_file_tag('file', array('class' => 'form-control')) ?>
                        </div>
                      </div>
                      <!-- /.col -->
                      <div class="col-md-3">
                        <div class="form-group">
                            <label class="tombol_filter">Tombol Filter</label><br/>
                            <button type="submit" name="commit" class="btn btn-outline-primary btn-sm">Simpan <i class="far fa-save"></i></button>
                            <button type="reset" name="reset" class="btn btn-outline-danger btn-sm">Reset <i class="fa fa-backspace"></i></button>
                        </div>
                        <!-- /.form-group -->
                      </div>
                      <!-- /.col -->                      
                    </div>
                    <?php echo '</form>'; ?>
                </div>
            </div>
          </div>
        </div>
    </div>
</section>