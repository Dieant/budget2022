<?php use_helper('I18N', 'Date', 'Object', 'Javascript', 'Validation') ?>
<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Antrian Usulan Pending</h1>
      </div>
      <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Usulan SHS</a></li>
              <li class="breadcrumb-item active">Antrian Usulan Pending</li>
          </ol>
      </div>
    </div>
  </div>
</section>
<section class="content">
    <?php include_partial('usulan_ssh/list_messages'); ?>
    <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
                <div class="card-body table-responsive">
                    <table class="table table-hover">
                        <thead class="head_peach">
                            <tr>
                                <th>id SPJM</th>
                                <th colspan="3">Penyelia - Perangkat Daerah</th>
                                <th>Usulan</th>
                                <th colspan="2">Tanggal</th>
                                <th colspan="3">Dukungan</th>
                                <th>Nomor Surat</th>
                                <th>Status</th>
                                <th>Catatan Surveyor</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>     
                            <?php
                            $i = 1;
                            $con = Propel::getConnection();
                            foreach ($rs_usulanSPJM as $usulan_SPJM) {
                                $id_spjm = $usulan_SPJM->getIdSpjm();
                                $tgl_spjm = $usulan_SPJM->getTglSpjm();
                                $penyelia = $usulan_SPJM->getPenyelia();
                                $skpd = $usulan_SPJM->getSkpd();
                                $tipe_usulan = $usulan_SPJM->getTipeUsulan();
                                $jenis_usulan = $usulan_SPJM->getJenisUsulan();
                                $jumlah_dukungan = $usulan_SPJM->getJumlahDukungan();
                                $created_at = $usulan_SPJM->getCreatedAt();
                                $updated_at = $usulan_SPJM->getUpdatedAt();
                                $status_verifikasi = $usulan_SPJM->getStatusVerifikasi();
                                $filepath = $usulan_SPJM->getFilepath();
                                $odd = fmod($i++, 2);

                                $query = "select * from unit_kerja where unit_id = '$skpd'";
                                $stmt = $con->prepareStatement($query);
                                $t = $stmt->executeQuery();
                                while ($t->next()) {
                                    $nama_skpd = $t->getString('unit_name');
                                }

                                $query_total_confirm_penyelia = "select count(*) as total from ebudget.usulan_ssh "
                                . "where status_hapus = false and status_verifikasi = false and status_confirmasi_penyelia = true and shsd_id is null and status_survey = true "
                                . "and id_spjm = '$id_spjm' and status_pending = true";
                                $stmt_total_confirm = $con->prepareStatement($query_total_confirm_penyelia);
                                $total_confirm_q = $stmt_total_confirm->executeQuery();
                                while ($total_confirm_q->next()) {
                                    $total_confirm = $total_confirm_q->getString('total');
                                }
                                if ($total_confirm > 0) {
                                    ?>
                                    <div class="modal fade" id="myModal<?php echo $id_spjm ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="myModalLabel">Alasan Tolak Verifikasi Penyelia</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                      <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <?php echo form_tag('usulan_ssh/tolakVerifikasiPenyeliaDenganAlasanAllSpjm', array('class' => 'form-horizontal')); ?>
                                                <div class="modal-body">
                                                    <?php echo input_hidden_tag('id_spjm', $id_spjm); ?>
                                                    <div class="form-group">
                                                        Nomor Surat <?php echo $usulan_SPJM->getNomorSurat() ?>
                                                    </div>
                                                    <div class="form-group">
                                                        Jenis Penolakan 
                                                        <select name="jenis_alasan" class="js-example-basic-single select2">
                                                            <option value="">--Silahkan memilih tipe alasan penolakan--</option>
                                                            <option value="kode barang usulan belum tepat">Kode Barang belum tepat</option>
                                                            <option value="usulan berbeda dengan pendukung">Berbeda dengan pendukung</option>
                                                            <option value="pendukung belum lengkap">pendukung belum lengkap</option>
                                                            <option value="sudah muncul komponen dengan nama yang sama">Sudah muncul komponen dengan nama yang sama</option>
                                                            <option value="pendukung tidak ada">Pendukung tidak ada</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <input type="submit" name="submit" value="Simpan" class="btn btn-outline-primary btn-sm"/>
                                                    <input type="reset" name="reset" value="Reset" class="btn btn-outline-warning btn-sm"/>                
                                                </div>
                                                <?php echo '</form>'; ?>
                                            </div>
                                        </div>
                                    </div>
                                    <tr id="spjm_<?php echo $id_spjm ?>" style="text-align: center">
                                        <td><?php echo link_to_function(image_tag('b_plus.png', array('name' => 'img_' . $id_spjm, 'id' => 'img_' . $id_spjm, 'border' => 0)), 'showHideUsulan(' . $id_spjm . ')') . ' '; ?>SPJM-<?php echo $id_spjm ?></td>
                                        <td colspan="3"c><?php echo $penyelia . ' : ' . $nama_skpd ?></td>
                                        <td><?php echo $tipe_usulan . ' - ' . $jenis_usulan ?></td>
                                        <td colspan="2"><?php echo date("d-m-Y H:i", strtotime($tgl_spjm)); ?></td>
                                        <td colspan="3"><?php echo $jumlah_dukungan . ' dukungan' ?></td>
                                        <td style="text-align: center">
                                            <?php echo $usulan_SPJM->getNomorSurat(); ?>
                                        </td>
                                        <td style="font-weight: bold">
                                            <?php
                                            echo date("d-m-Y H:i", strtotime($updated_at)) . '<br/>';
                                            if ($status_verifikasi == false) {
                                                echo 'Antrian';
                                            } else {
                                                echo 'Verifikasi';
                                            }
                                            ?>
                                        </td>
                                        <td style="text-align: center">                                    
                                        </td>
                                        <td>
                                            <div class="btn-group">
                                                <?php
                                                echo link_to('<i class="fa fa-edit"></i> Edit', 'usulan_ssh/usulaneditSPJM?id=' . $id_spjm, array('class' => 'btn btn-outline-primary btn-sm'));
                                                if ($status_verifikasi == false) {
                                                    ?>
                                                    <button type="button" class="btn btn-outline-primary btn-sm dropdown-toggle" data-toggle="dropdown">
                                                        <span class="caret"></span>
                                                        <span class="sr-only">Toggle Dropdown</span>
                                                    </button>
                                                    <div class="dropdown-menu dropdown-menu-right" role="menu">
                                                        <button type="button" class="dropdown-item" data-toggle="modal" data-target="#myModal<?php echo $id_spjm ?>">
                                                            Kembalikan ke penyelia
                                                        </button>
                                                        <?php // echo link_to('<i class="fa fa-trash"></i> Hapus', 'usulan_ssh/hapusSPJM?id=' . $id_spjm . '&key=' . md5('hapus_spjm'), Array('confirm' => 'Apakah yakin menghapus usulan SPJM dari ' . $penyelia . ' untuk dinas ' . $nama_skpd . '?', 'class' => 'btn btn-default')); ?>
                                                        <?php // echo link_to('<i class="fa fa-trash"></i> Hapus', 'usulan_ssh/hapusSSH?id=' . $usulanssh->getIdUsulan() . '&key=' . md5('hapus_ssh'), Array('confirm' => 'Apakah yakin menghapus usulan SSH : ' . $usulanssh->getNama() . ' ' . $usulanssh->getSpec() . '?', 'class' => 'btn btn-default')); ?>
                                                    </div>
                                                <?php }
                                                ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
          </div>
        </div>
    </div>
</section>
<script>
    function showHideUsulan(id) {
        var row = $('#spjm_' + id);
        var img = $('#img_' + id);

        if (img) {
            var src = document.getElementById('img_' + id).getAttribute('src');
            var minus = src.indexOf('/<?php echo sfConfig::get('app_default_coding'); ?>/images/b_minus.png');
            if (minus != -1) {
                src = '/<?php echo sfConfig::get('app_default_coding'); ?>/images/b_plus.png';
            } else {
                src = '/<?php echo sfConfig::get('app_default_coding'); ?>/images/b_minus.png';
            }
            img.attr('src', src);
        }


        if (minus == -1) {
            var spjm_id = 'spjm_' + id;
            var usulan = $('.usulan_' + id);
            var n = usulan.length;

            if (n > 0) {
                for (var i = 0; i < usulan.length; i++) {
                    var pekerjaan = usulan[i];
                    pekerjaan.style.display = 'table-row';
                }
            } else {
                $('#indicator_' + id).show();
                $.ajax({
                    url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/usulan_ssh/getUsulanPending/id/" + id + ".html",
                    context: document.body
                }).done(function(msg) {
                    $('#spjm_' + id).after(msg);
                    $('#indicator_' + id).remove();
                });
            }
        } else {
            $('.usulan_' + id).remove();
            minus = -1;
        }
    }
</script>