<?php use_helper('I18N', 'Date', 'Object', 'Javascript', 'Validation') ?>
<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>List Usulan SHS</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Usulan SHS</a></li>
          <li class="breadcrumb-item active">List Usulan</li>
        </ol>
      </div>
    </div>
  </div>
</section>
<!-- Main content -->
<section class="content">
    <?php include_partial('usulan_ssh/list_messages'); ?>
    <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">
                        <i class="fas fa-filter"></i> Filter
                    </h3>
                </div>
                <div class="card-body">
                    <?php echo form_tag('usulan_ssh/usulandinaslist', array('method' => 'get', 'class' => 'form-horizontal')); ?>
                    <div class="row">
                      <div class="col-md-3">
                        <div class="form-group">
                            <label>Pilihan</label>
                            <?php
                                echo select_tag('filters[select]', options_for_select( 
                                    $arrOptions = array(
                                    1 => 'User Dinas'
                                    ), $filters['select'] ? $filters['select'] : '',array('include_custom' => '--Pilih--')), array('class' => 'form-control select2'));
                            ?>
                        </div>
                      </div>
                      <!-- /.col -->
                      <div class="col-md-3">
                        <div class="form-group">
                            <label>Action</label>
                            <?php echo input_tag('filters[komponen]', isset($filters['komponen']) ? $filters['komponen'] : null, array('class' => 'form-control')); ?>
                        </div>
                      </div>
                      <!-- /.col -->
                      <div class="col-md-2">
                        <div class="form-group">
                            <label class="tombol_filter">Tombol Filter</label><br/>
                            <button type="submit" name="filter" class="btn btn-outline-primary btn-sm">Filter <i class="fas fa-search"></i></button>
                            <?php
                                echo link_to('Reset <i class="fa fa-backspace"></i>', 'usulan_ssh/usulandinaslist?filter=filter', array('class' => 'btn btn-outline-danger btn-sm'));
                            ?>
                        </div>
                        <!-- /.form-group -->
                      </div>
                      <!-- /.col -->                      
                    </div>
                    <?php echo '</form>'; ?>
                </div>
            </div>
          </div>
        </div>
        <div class="row">
            <div class="col-md-12 stretch-card">
                <div class="card">
                    <?php if (!$pager->getNbResults()): ?>
                    <?php echo __('no result') ?>
                    <?php else: ?>
                        <?php include_partial("usulan_ssh/usulandinaslist", array('pager' => $pager)) ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>
