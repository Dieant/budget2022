<div class="card-body table-responsive p-0">
    <table class="table table-hover">
        <thead class="head_peach">
            <tr>
                <th>Perangkat Daerah</th>
                <th>User Dinas</th>
                <th>File Excel</th>
                <th>File Compress</th>
                <th>Nomor SPJM</th>
                <th>Waktu</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $i = 1;
            foreach ($pager->getResults() as $usulandinas): $odd = fmod( ++$i, 2)
                ?>
                <tr class="sf_admin_row_<?php echo $odd ?>">            
                    <td style="text-align: center">                                
                        <?php echo '<b>(' . $usulandinas->getSkpd() . ') ' . UnitKerjaPeer::getStringUnitKerja($usulandinas->getSkpd()) . '</b>'; ?>
                    </td>
                    <td style="text-align: center">
                        <?php echo '<b>' . $usulandinas->getPenyelia() . '</b>'; ?>
                    </td>
                    <td style="text-align: center">
                        <a target="_blank" href="<?php echo $usulandinas->getFilepath() ?>"><?php echo date("d-m-Y_H-i", strtotime($usulandinas->getCreatedAt())); ?></a>
                    </td>
                    <td style="text-align: center">
                        <a target="_blank" href="<?php echo $usulandinas->getFilepathRar() ?>"><?php echo date("d-m-Y_H-i", strtotime($usulandinas->getCreatedAt())); ?></a>
                    </td>
                    <td style="text-align: center">                
                        <?php echo $usulandinas->getNomorSurat(); ?>
                    </td>
                    <td style="text-align: center">
                        <?php echo 'Created : ' . date("d-m-Y H:i", strtotime($usulandinas->getCreatedAt())) . '<br/>'; ?>
                        <?php echo 'Updated : ' . date("d-m-Y H:i", strtotime($usulandinas->getUpdatedAt())); ?>
                    </td>
                    <td style="text-align: center; font-weight: bold">
                        <?php if ($usulandinas->getStatusVerifikasi() == false) { ?>
                            <?php echo form_tag('usulan_ssh/hapususulanolehpenyelia'); ?>
                            Alasan : <input type="text" name="alasan" required="true" />
                            <input type="hidden" name="id" value="<?php echo $usulandinas->getIdUsulan() ?>"/>
                            <input type="hidden" name="key" value="<?php echo md5('delete_usulan_dinas') ?>"/>
                            <input type="submit" name="submit" class="btn btn-outline-primary btn-sm" value="Tolak Usulan"/>
                            <?php echo '</form>'; ?>
                            <?php
                        } else {
                            echo 'Silahkan Proses Request Token Usulan';
                        }
                        ?>                
                    </td>

                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
<div class="card-footer clearfix">
    <ul class="pagination pagination-sm m-0 float-left">
        <?php
            echo format_number_choice('[0] no result|[1] 1 result|(1,+Inf] %1% results', array('%1%' => $pager->getNbResults()), $pager->getNbResults());
        ?>
    </ul>
    <ul class="pagination pagination-sm m-0 float-right">
        <?php 
        if ($pager->haveToPaginate()): 
            echo '<li class="page-item">'.link_to('Previous', 'usulan_ssh/ceklistusulandinas?page=' . $pager->getPreviousPage(), array('class' => 'page-link', 'align' => 'absmiddle', 'alt' => __('Previous'), 'title' => __('Previous'))).'</li>';

            foreach ($pager->getLinks() as $page):
                $activeclass = ($page == $pager->getPage()) ? 'active' : '';
                echo '<li class="page-item '.$activeclass.'">'.link_to_unless($page == $pager->getPage(), $page, "usulan_ssh/ceklistusulandinas?page={$page}", array('class' => 'page-link')).'</li>';
            endforeach;
            echo '<li class="page-item">'.link_to('Next', 'usulan_ssh/ceklistusulandinas?page=' . $pager->getNextPage(), array('class' => 'page-link', 'align' => 'absmiddle', 'alt' => __('Next'), 'title' => __('Next'))).'</li>'; 
            endif;
        ?>
    </ul>
</div>
