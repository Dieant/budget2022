<div class="card-body table-responsive p-0">
    <table class="table table-hover">
        <thead class="head_peach">
            <tr>
            <th>Usulan</th>
            <th>Nama</th>
            <th>Spesifikasi</th>
            <th>Hidden Spec</th>
            <th>Merek</th>
            <th>Rekening</th>
            <th>Satuan</th>
            <th>Harga</th>
            <th>Pajak</th>
            <th>Waktu</th>
            <th>Penjelasan</th>
            <th>Status/Komentar</th>
        </tr>
        </thead>
        <tbody>
            <?php
            $i = 1;
            foreach ($pager->getResults() as $usulanssh): $odd = fmod( ++$i, 2)
                ?>
                <tr>
                    <td>
                        <?php 
                        echo 'Oleh : <b>' . UsulanSPJMPeer::getStringPenyelia($usulanssh->getIdSpjm()) . '</b><br/>';
                        echo 'Dinas  : <b>(' . $usulanssh->getUnitId() . ') ' . UnitKerjaPeer::getStringUnitKerja($usulanssh->getUnitId()) . '</b><br/>'; 
                        echo 'Nomor Surat : <b>' . UsulanSPJMPeer::getStringNomorSurat($usulanssh->getIdSpjm()) . '</b><br/>';
                        echo 'File Rar Upload: <b><a href="' . UsulanDinasPeer::getStringFilepath($usulanssh->getIdSpjm()) . '">Download RAR</a></b><br/>';
                        echo 'File Excel Upload : <b><a href="' . UsulanSPJMPeer::getStringFilepath($usulanssh->getIdSpjm()) . '">Download Excel</a></b>';
                        ?>
                    </td>
                    <td><?php echo $usulanssh->getNama() ?></td>
                    <td><?php echo $usulanssh->getSpec() ?></td>
                    <td><?php echo $usulanssh->getHiddenSpec() ?></td>
                    <td style="text-align: center"><?php echo $usulanssh->getMerek() ?></td>
                    <td style="text-align: center"><?php echo $usulanssh->getRekening() ?></td>
                    <td style="text-align: center"><?php echo $usulanssh->getSatuan() ?></td>
                    <td style="text-align: right"><?php echo number_format($usulanssh->getHarga()) ?></td>
                    <td style="text-align: center"><?php echo $usulanssh->getPajak() ?></td>
                    <td>
                        <?php echo 'Created : ' . date("d-m-Y H:i", strtotime($usulanssh->getCreatedAt())) . '<br/>'; ?>
                        <?php echo 'Updated : ' . date("d-m-Y H:i", strtotime($usulanssh->getUpdatedAt())); ?>
                    </td>
                    <?php
                    $query = "select keterangan_dinas from " . sfConfig::get('app_default_schema') . ".usulan_dinas a, ebudget.usulan_spjm b
                            where a.nomor_surat=b.nomor_surat and b.id_spjm=".$usulanssh->getIdSpjm();
                    $con = Propel::getConnection();
                    $stmt = $con->prepareStatement($query);
                    $rs = $stmt->executeQuery();
                    $rs->next();
                    ?>
                    <td style="text-align: center"><?php echo $usulanssh->getKomentarSurvey() ?></td>
                    <td style="text-align: center; font-weight: bold">
                        <?php
                        $namasurvey = $usulanssh->getSurveyor();
                        if ($usulanssh->getStatusHapus() == t) {
                            echo '<font style="color:red">Ditolak</font>';
                        } else {
                            if ($usulanssh->getStatusVerifikasi() == t) {
                                echo '<font style="color:green">Terverifikasi <br/> Surveyor oleh '. $namasurvey .'</font>';
                            } else {
                                echo '<font style="color:blue">Antrian</font><br/>';
                                if(!empty($namasurvey) && $usulanssh->getStatusConfirmasiPenyelia()==t && $usulanssh->getStatusSurvey()==t){
                                    echo '<font style="color:blue">Posisi di Tim Data <br/> Surveyor oleh '. $namasurvey .'</font>';
                                } elseif(!empty($namasurvey) && $usulanssh->getStatusConfirmasiPenyelia()==t){
                                    echo '<font style="color:blue">Posisi di Survey oleh '. $namasurvey .'</font>';
                                } elseif($usulanssh->getStatusPending()==t){
                                    echo '<font style="color:blue">Posisi Komponen Pending</font>';
                                } elseif($usulanssh->getStatusConfirmasiPenyelia()==t && empty($namasurvey)){
                                    echo '<font style="color:blue">Posisi di Tim Data</font>';
                                } else{
                                    echo '<font style="color:blue">Posisi di Penyelia</font>';
                                }
                            }
                        }
                        echo '<br/>';
                        echo $usulanssh->getKomentarVerifikator();
                        ?>
                    </td>
                </tr>
                <?php endforeach; ?>
        </tbody>
    </table>
</div>
<div class="card-footer clearfix">
    <ul class="pagination pagination-sm m-0 float-left">
        <?php
            echo format_number_choice('[0] no result|[1] 1 result|(1,+Inf] %1% results', array('%1%' => $pager->getNbResults()), $pager->getNbResults());
        ?>
    </ul>
    <ul class="pagination pagination-sm m-0 float-right">
        <?php 
        if ($pager->haveToPaginate()): 
            echo '<li class="page-item">'.link_to('Previous', 'usulan_ssh/usulansshlist?page=' . $pager->getPreviousPage(), array('class' => 'page-link', 'align' => 'absmiddle', 'alt' => __('Previous'), 'title' => __('Previous'))).'</li>';

            foreach ($pager->getLinks() as $page):
                $activeclass = ($page == $pager->getPage()) ? 'active' : '';
                echo '<li class="page-item '.$activeclass.'">'.link_to_unless($page == $pager->getPage(), $page, "usulan_ssh/usulansshlist?page={$page}", array('class' => 'page-link')).'</li>';
            endforeach;
            echo '<li class="page-item">'.link_to('Next', 'usulan_ssh/usulansshlist?page=' . $pager->getNextPage(), array('class' => 'page-link', 'align' => 'absmiddle', 'alt' => __('Next'), 'title' => __('Next'))).'</li>'; 
            endif;
        ?>
    </ul>
</div>