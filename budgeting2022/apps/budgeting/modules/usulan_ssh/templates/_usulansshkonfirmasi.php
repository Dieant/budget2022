<div class="card-body table-responsive p-0">
    <table class="table table-hover">
        <thead class="head_peach">
            <tr>
                <th>Perangkat Daerah</th>
                <th>Nama</th>
                <th>Spesifikasi</th>
                <th>Hidden Spec</th>
                <th>Merek</th>
                <th>Rekening</th>
                <th>Satuan</th>
                <th>Harga</th>
                <th>Pajak</th>
                <th>Komentar Surveyor</th>
                <th>Komentar Verifikator</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $i = 1;
            foreach ($pager->getResults() as $usulanssh): $odd = fmod( ++$i, 2)
                ?>
                <tr>
                    <td>
                        <?php
                        echo '<b>(' . $usulanssh->getUnitId() . ') ' . UnitKerjaPeer::getStringUnitKerja($usulanssh->getUnitId()) . '</b><br/>';
                        echo 'Nomor Surat : <b>' . UsulanSPJMPeer::getStringNomorSurat($usulanssh->getIdSpjm()) . '</b>';
                        ?>
                    </td>
                    <td><?php echo $usulanssh->getNama() ?></td>
                    <td><?php echo $usulanssh->getSpec() ?></td>
                    <td><?php echo $usulanssh->getHiddenSpec() ?></td>
                    <td style="text-align: center"><?php echo $usulanssh->getMerek() ?></td>
                    <td style="text-align: center"><?php echo $usulanssh->getRekening() ?></td>
                    <td style="text-align: center"><?php echo $usulanssh->getSatuan() ?></td>
                    <td style="text-align: right"><?php echo number_format($usulanssh->getHarga()) ?></td>
                    <td style="text-align: center"><?php echo $usulanssh->getPajak() ?></td>
                    <td>
                        <?php echo $usulanssh->getKomentarSurvey(); ?>
                    </td>
                    <td>
                        <?php echo $usulanssh->getKomentarVerifikator(); ?>
                    </td>
                    <td style="text-align: center; font-weight: bold">
                        <?php
                        $namasurvey = $usulanssh->getSurveyor();
                        if ($usulanssh->getStatusHapus() == t) {
                            echo '<font style="color:red">Ditolak</font>';
                        } else {
                            if ($usulanssh->getStatusVerifikasi() == t) {
                                echo '<font style="color:green">Terverifikasi</font>';
                            } else {
                                echo '<font style="color:blue">Antrian</font><br/>';
                                if(!empty($namasurvey) && $usulanssh->getStatusConfirmasiPenyelia()==t && $usulanssh->getStatusSurvey()==t){
                                    echo '<font style="color:blue">Posisi di Tim Data</font>';
                                } elseif(!empty($namasurvey) && $usulanssh->getStatusConfirmasiPenyelia()==t){
                                    echo '<font style="color:blue">Posisi di Survey</font>';
                                } elseif($usulanssh->getStatusPending()==t){
                                    echo '<font style="color:blue">Posisi Komponen Pending</font>';
                                } elseif($usulanssh->getStatusConfirmasiPenyelia()==t && empty($namasurvey)){
                                    echo '<font style="color:blue">Posisi di Tim Data</font>';
                                } else{
                                    echo '<font style="color:blue">Posisi di Penyelia</font>';
                                }
                            }
                        }                  
                        ?>
                    </td>
                    <td style="text-align: center; font-weight: bold">
                        <div class="btn-group">
                            <?php
                            echo link_to('Cek <i class="fa fa-check"></i>', 'usulan_ssh/usulanproseskonfirmasi?id=' . $usulanssh->getIdUsulan() . '&key=' . md5('cek_konfirmasi_usulan'), array('class' => 'btn btn-outline-success btn-sm'));
                            ?>
                            <button type="button" class="btn btn-outline-success dropdown-toggle" data-toggle="dropdown">
                                <span class="caret"></span>
                                <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <div class="dropdown-menu dropdown-menu-right" role="menu">
                                <?php echo link_to('Hapus <i class="fa fa-trash"></i>', 'usulan_ssh/hapusSSH?id=' . $usulanssh->getIdUsulan() . '&key=' . md5('hapus_ssh'), Array('confirm' => 'Apakah yakin menghapus usulan SSH : ' . $usulanssh->getNama() . ' ' . $usulanssh->getSpec() . '?', 'class' => 'dropdown-item btn btn-outline-danger btn-sm')); ?>
                            </div>
                        </div>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
<div class="card-footer clearfix">
    <ul class="pagination pagination-sm m-0 float-left">
        <?php
            echo format_number_choice('[0] no result|[1] 1 result|(1,+Inf] %1% results', array('%1%' => $pager->getNbResults()), $pager->getNbResults());
        ?>
    </ul>
    <ul class="pagination pagination-sm m-0 float-right">
        <?php 
        if ($pager->haveToPaginate()): 
            echo '<li class="page-item">'.link_to('Previous', 'usulan_ssh/usulansshkonfirmasi?page=' . $pager->getPreviousPage(), array('class' => 'page-link', 'align' => 'absmiddle', 'alt' => __('Previous'), 'title' => __('Previous'))).'</li>';

            foreach ($pager->getLinks() as $page):
                $activeclass = ($page == $pager->getPage()) ? 'active' : '';
                echo '<li class="page-item '.$activeclass.'">'.link_to_unless($page == $pager->getPage(), $page, "usulan_ssh/usulansshkonfirmasi?page={$page}", array('class' => 'page-link')).'</li>';
            endforeach;
            echo '<li class="page-item">'.link_to('Next', 'usulan_ssh/usulansshkonfirmasi?page=' . $pager->getNextPage(), array('class' => 'page-link', 'align' => 'absmiddle', 'alt' => __('Next'), 'title' => __('Next'))).'</li>'; 
            endif;
        ?>
    </ul>
</div>