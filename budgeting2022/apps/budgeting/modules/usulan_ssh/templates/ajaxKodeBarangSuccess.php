<?php use_helper('I18N', 'Date') ?>
<?php use_stylesheet('/sf/sf_admin/css/main') ?>
<?php echo use_helper('Object', 'Javascript', 'Validation') ?>


<?php

$kode_keyword = '%' . $kode . '%';
$index = $index;
$options = array();
$x = new Criteria();
$x->add(KategoriShsdPeer::KATEGORI_SHSD_NAME, $kode_keyword, Criteria::ILIKE);
$x->addAnd(KategoriShsdPeer::KATEGORI_SHSD_ID, '%.%.%.%', Criteria::ILIKE);
$x->addAscendingOrderByColumn(KategoriShsdPeer::KATEGORI_SHSD_ID);
$x->setLimit(10);
$rs_kode = KategoriShsdPeer::doSelect($x);

foreach ($rs_kode as $kodebar) {
    $options[$kodebar->getKategoriShsdId()] = $kodebar->getKategoriShsdId();
    echo '<li onclick="set_item(\'' . $kodebar->getKategoriShsdId() . '\')">' . $kodebar->getKategoriShsdId() . ' - ' . $kodebar->getKategoriShsdName() . '</li>';
}
?>
