<?php use_helper('I18N', 'Date', 'Url', 'Javascript', 'Form', 'Object') ?>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myModalLabel">Alasan Tolak Verifikasi Penyelia</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php echo form_tag('usulan_ssh/tolakVerifikasiPenyeliaDenganAlasan', array('class' => 'form-horizontal')); ?>
            <div class="modal-body">
                <?php echo input_hidden_tag('id_usulan', $data_usulan_ssh->getIdUsulan()); ?>
                <div class="form-group">
                    <label class="col-xs-3 text-right">Usulan</label>
                    <div class="col-xs-9">
                        <?php echo $data_usulan_ssh->getNama() . ' ' . $data_usulan_ssh->getSpec() ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-xs-3 text-right">Jenis Penolakan</label>
                    <div class="col-xs-9">
                        <select name="jenis_alasan" class="form-control select2">
                            <option value="">--Silahkan memilih tipe alasan penolakan--</option>
                            <option value="sudah muncul komponen dengan nama yang sama">Sudah muncul komponen dengan
                                nama yang sama</option>
                            <option value="dukungan tidak sesuai">Dukungan tidak sesuai</option>
                            <option value="dukungan tidak ada">Dukungan tidak ada</option>
                            <option value="usulan tidak disertai gambar">Usulan tidak disertai gambar</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-xs-3 text-right">Alasan</label>
                    <div class="col-xs-9">
                        <?php echo textarea_tag('alasan', '', array('class' => 'form-control')); ?>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <label class="col-xs-3 text-right">&nbsp;</label>
                <div class="col-xs-9">
                    <input type="submit" name="submit" value="Simpan" class="btn btn-outline-primary btn-sm" />
                    <input type="reset" name="reset" value="Reset" class="btn btn-outline-warning btn-sm" />
                </div>
            </div>
            <?php echo '</form>'; ?>
        </div>
    </div>
</div>
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Verifikasi Usulan</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Usulan SHS</a></li>
                    <li class="breadcrumb-item active">Verifikasi Usulan</li>
                </ol>
            </div>
        </div>
    </div>
</section>
<!-- Main content -->
<section class="content">
    <?php include_partial('usulan_ssh/list_messages'); ?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="fab fa-buffer"></i> Cek Kesamaan Komponen
                        </h3>
                    </div>
                    <div class="card-body table-responsive">
                        <table class="table table-hover">
                            <tr id="usulan_<?php echo $data_usulan_ssh->getIdUsulan() ?>" style="width: 100%">
                                <td colspan="13">
                                    <?php echo link_to_function(image_tag('b_plus.png', array('name' => 'img_usulan_' . $data_usulan_ssh->getIdUsulan(), 'id' => 'img_usulan_' . $data_usulan_ssh->getIdUsulan(), 'border' => 0)), 'showHideUsulanMirip(' . $data_usulan_ssh->getIdUsulan() . ')') . ' '; ?><b>List
                                        Usulan Menyerupai nama
                                        "<?php echo $data_usulan_ssh->getNama() . ' ' . $data_usulan_ssh->getSpec() ?>"</b>
                                </td>
                            </tr>
                            <tr id="ssh_<?php echo $data_usulan_ssh->getIdUsulan() ?>" style="width: 100%">
                                <td colspan="13">
                                    <?php echo link_to_function(image_tag('b_plus.png', array('name' => 'img_ssh_' . $data_usulan_ssh->getIdUsulan(), 'id' => 'img_ssh_' . $data_usulan_ssh->getIdUsulan(), 'border' => 0)), 'showHideSSHMirip(' . $data_usulan_ssh->getIdUsulan() . ')') . ' '; ?><b>List
                                        SSH Menyerupai nama
                                        "<?php echo $data_usulan_ssh->getNama() . ' ' . $data_usulan_ssh->getSpec() ?>"</b>
                                </td>
                            </tr>
                            <tr id="kode_<?php echo $data_usulan_ssh->getIdUsulan() ?>" style="width: 100%">
                                <td colspan="13">
                                    <?php echo link_to_function(image_tag('b_plus.png', array('name' => 'img_kode_' . $data_usulan_ssh->getIdUsulan(), 'id' => 'img_kode_' . $data_usulan_ssh->getIdUsulan(), 'border' => 0)), 'showHideSSHSatuKodeBarang(' . $data_usulan_ssh->getIdUsulan() . ')') . ' '; ?><b>List
                                        SSH dalam kode barang "<?php echo $data_usulan_ssh->getKodeBarang() ?>"</b></td>
                            </tr>
                            <tr id="lalu_<?php echo $data_usulan_ssh->getIdUsulan() ?>" style="width: 100%">
                                <td colspan="13">
                                    <?php echo link_to_function(image_tag('b_plus.png', array('name' => 'img_lalu_' . $data_usulan_ssh->getIdUsulan(), 'id' => 'img_lalu_' . $data_usulan_ssh->getIdUsulan(), 'border' => 0)), 'showHideKomponenTahunLalu(' . $data_usulan_ssh->getIdUsulan() . ')') . ' '; ?><b>List
                                        Komponen Tahun Lalu
                                        "<?php echo $data_usulan_ssh->getNama() . ' ' . $data_usulan_ssh->getSpec() ?>"</b>
                                </td>
                            </tr>
                        </table>
                        <div class="card-body" id="indicator" style="display:none;" align="center">
                            <dt>&nbsp;</dt>
                            <dd><b>Mohon Tunggu</b><?php echo image_tag('loading.gif', array('align' => 'absmiddle')) ?>
                            </dd>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="fab fa-foursquare"></i> Form Verifikasi Usulan
                        </h3>
                        <div class="card-tools">
                            <div class="input-group input-group-sm">
                                <button type="button" class="btn btn-outline-danger btn-sm" data-toggle="modal"
                                    data-target="#myModal">
                                    Kembalikan ke penyelia
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <?php echo form_tag('usulan_ssh/savesshusulan', array('class' => 'form-horizontal')); ?>
                        <?php echo input_hidden_tag('id_usulan', $data_usulan_ssh->getIdUsulan()); ?>
                        <?php echo input_hidden_tag('id_spjm', $data_usulan_ssh->getIdSpjm()); ?>
                        <?php echo input_hidden_tag('id_user', $sf_user->getNamaUser()); ?>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Tipe Usulan</label>
                                    <?php echo input_tag('tipe', $data_usulan_ssh->getTipeUsulan(), array('readonly' => true, 'required' => true, 'class' => 'form-control')); ?>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Catatan Surveyor</label>
                                    <?php echo textarea_tag('komentar_survey', $data_usulan_ssh->getKomentarSurvey(), array('readonly' => 'true' ,'class' => 'form-control')) ?>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Kode Barang</label>
                                    <?php echo input_tag('kode_barang_a', '(' . $data_usulan_ssh->getKodeBarang() . ') ' . KategoriShsdPeer::getStringKategoriShsdName($data_usulan_ssh->getKodeBarang()), array('required' => true, 'class' => 'form-control', 'class' => 'form-control', 'readonly' => true)); ?>
                                    <select name="kode_barang" class="form-control select2">
                                        <option value="<?php echo $data_usulan_ssh->getKodeBarang() ?>" selected>
                                            <?php echo '(' . $data_usulan_ssh->getKodeBarang() . ') ' . KategoriShsdPeer::getStringKategoriShsdName($data_usulan_ssh->getKodeBarang()); ?>
                                        </option>
                                        <?php foreach ($list_kode_barang as $value) { ?>
                                        <option value="<?php echo $value['kategori_shsd_id'] ?>">
                                            <?php echo '(' . $value['kategori_shsd_id'] . ') ' . $value['kategori_shsd_name'] ?>
                                        </option>
                                        <?php }
                                ?>
                                    </select>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Hasil Cek Surveyor</label>
                                    <?php echo textarea_tag('komentar_survey', $data_usulan_ssh->getKomentarSurvey(), array('readonly' => true,'class' => 'form-control')) ?>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Kode Komponen</label>
                                    <?php echo input_tag('id_ssh', null, array('required' => true, 'class' => 'form-control')); ?>
                                    <font style="color: red">* Cek ID teratas dari Droplist Kode Barang diatas.</font>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Nama Sebelum</label>
                                    <?php echo input_tag('nama_sebelum', $data_usulan_ssh->getNamaSebelum(), array('readonly' => true, 'class' => 'form-control')); ?>
                                </div>
                            </div>
                            <!-- /.col -->
                            <!-- <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Tahap Usulan</label>
                                    <input type="text" name="tahap" required class="form-control" />
                                </div>
                            </div> -->

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Tahap Usulan</label>
                                    <select name="tahap" class="form-control select2">     
                                        <option value="">--Pilih--</option>                              
                                        <?php foreach ($list_tahap as $value) { ?>
                                        <option value="<?php echo $value->getTahapId() ?>">
                                            <?php echo $value->getTahapName() ?></option>
                                        <?php }
                                    ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Tahap Buku (0,1,2, dst)</label>
                                    <?php echo input_tag('tahap_buku', null, array('required' => true, 'class' => 'form-control')); ?>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Harga Sebelum</label>
                                    <?php echo input_tag('harga_sebelum', number_format($data_usulan_ssh->getHargaSebelum(), 0, ',', '.'), array('readonly' => true, 'class' => 'form-control')); ?>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Nama Komponen</label>
                                    <?php echo input_tag('nama_ssh', $data_usulan_ssh->getNama() . ' ' . $data_usulan_ssh->getSpec(), array('required' => true, 'class' => 'form-control')); ?>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Alasan Perubahan</label>
                                    <?php echo textarea_tag('alasan_perubahan', $data_usulan_ssh->getAlasanPerubahanHarga(), array('readonly' => true, 'class' => 'form-control')); ?>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Nama Dasar</label>
                                    <?php echo input_tag('nama', $data_usulan_ssh->getNama(), array('required' => true, 'class' => 'form-control')); ?>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Kode Komponen Sebelum</label>
                                    <?php echo input_tag('id_komponen_perubahan', '', array('class' => 'form-control')); ?>
                                    <font style="color: red">* Masukan Kode Komponen Sebelumnya, agar otomatis di lock
                                        untuk komponen yang lama.</font>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Spesifikasi</label>
                                    <?php echo input_tag('spec', $data_usulan_ssh->getSpec(), array('class' => 'form-control')); ?>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Perubahan harga?</label>
                                    <select name="is_perubahan_harga" class="form-control">
                                        <option value="<?php echo $data_usulan_ssh->getIsPerubahanHarga() ?>" selected>
                                            <?php
                                    if ($data_usulan_ssh->getIsPerubahanHarga() == 1) {
                                        echo 'Iya';
                                    } else {
                                        echo 'Tidak';
                                    }
                                    ?>
                                        </option>
                                        <option value="0">Tidak</option>
                                        <option value="1">Iya</option>
                                    </select>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Spesifikasi Tersembunyi</label>
                                    <?php echo input_tag('hidden_spec', $data_usulan_ssh->getHiddenSpec(), array('class' => 'form-control')); ?>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Harga Pendukung 1</label>
                                    <?php echo input_tag('harga_pendukung1', number_format($data_usulan_ssh->getHargaPendukung1(), 0, ',', '.'), array('class' => 'form-control')); ?>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Merek</label>
                                    <?php echo input_tag('merek', $data_usulan_ssh->getMerek(), array('class' => 'form-control')); ?>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Harga Pendukung 2</label>
                                    <?php echo input_tag('harga_pendukung2', number_format($data_usulan_ssh->getHargaPendukung2(), 0, ',', '.'), array('readonly' => true, 'class' => 'form-control')); ?>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Satuan</label>
                                    <select name="satuan" class="form-control select2">
                                        <option value="<?php echo $data_usulan_ssh->getSatuan() ?>" selected>
                                            <?php echo $data_usulan_ssh->getSatuan() ?></option>
                                        <?php foreach ($list_satuan as $value) { ?>
                                        <option value="<?php echo $value->getSatuanName() ?>">
                                            <?php echo $value->getSatuanName() ?></option>
                                        <?php }
                                ?>
                                    </select>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Harga Pendukung 3</label>
                                    <?php echo input_tag('harga_pendukung3', number_format($data_usulan_ssh->getHargaPendukung3(), 0, ',', '.'), array('readonly' => true, 'class' => 'form-control')); ?>
                                </div>
                            </div> 
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Harga Satuan</label>
                                    <?php 
                                echo input_tag('harga', $data_usulan_ssh->getHarga(), array('required' => true, 'class' => 'form-control')); 
                                echo input_tag('hargacek', number_format($data_usulan_ssh->getHarga(), 3, '.', ','), array('required' => true, 'class' => 'form-control', 'readonly' => 'readonly','style' => 'font-weight: bold')); 
                            ?>
                                </div>
                            </div>
                            <!-- /.col --> 
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Harga Survey 1</label>
                                    <?php echo input_tag('harga1', number_format($data_shsd->getHarga1(), 0, ',', '.'), array('class' => 'form-control')); ?>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Pajak</label>
                                    <select name="pajak" class="form-control select2">
                                        <option value="<?php echo $data_usulan_ssh->getPajak() ?>" selected>
                                            <?php echo $data_usulan_ssh->getPajak() ?></option>
                                        <option value="0">0</option>
                                        <option value="10">10</option>
                                    </select>
                                </div>
                            </div>
                            <!-- /.col --> 
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Harga Survey 2</label>
                                    <?php echo input_tag('harga2', number_format($data_shsd->getHarga2(), 0, ',', '.'), array('readonly' => true, 'class' => 'form-control')); ?>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Rekening</label>
                                    <?php echo input_tag('rekening_a', '(' . $data_usulan_ssh->getRekening() . ') ' . RekeningPeer::getStringRekeningName($data_usulan_ssh->getRekening()), array('required' => true, 'class' => 'form-control', 'class' => 'form-control', 'readonly' => true)); ?>
                                    <select id="rekening" name="rekening" class="form-control select2">
                                        <option value="<?php echo $data_usulan_ssh->getRekening() ?>" selected>
                                            <?php echo '(' . $data_usulan_ssh->getRekening() . ') ' . RekeningPeer::getStringRekeningName($data_usulan_ssh->getRekening()) ?>
                                        </option>
                                        <?php foreach ($list_rekening as $value) { ?>
                                        <option value="<?php echo $value->getRekeningCode() ?>">
                                            <?php echo '(' . $value->getRekeningCode() . ') ' . $value->getRekeningName() ?>
                                        </option>
                                        <?php }
                                ?>
                                    </select>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Harga Survey 3</label>
                                    <?php echo input_tag('harga3', number_format($data_shsd->getHarga3(), 0, ',', '.'), array('readonly' => true, 'class' => 'form-control')); ?>
                                </div>
                            </div>
                            <!-- /.col -->
                            <?php
                            //ILIKE kalau rekening mengandung 5.2
                            if (strpos($data_usulan_ssh->getRekening(), '5.2') !== FALSE) {
                            ?>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>Jenis Barang</label>
                                    <select name="komponen_tipe2" class="form-control select2">
                                        <option value="">BUKAN REKENING MODAL</option>
                                        <option value="KONTRUSKSI">KONTRUSKSI</option>
                                        <option value="NONKONSTRUKSI"
                                            <?php if ($data_usulan_ssh->getTipeUsulan() == 'SSH') echo 'selected' ?>>
                                            NONKONSTRUKSI</option>
                                        <option value="ATRIBUSI">ATRIBUSI</option>
                                        <option value="PERENCANAAN">PERENCANAAN</option>
                                        <option value="PENGAWASAN">PENGAWASAN</option>
                                    </select>
                                </div>
                            </div>
                            <!-- /.col -->
                            <?php } ?>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Usulan SKPD</label>
                                    <select name="skpd" class="form-control select2">
                                        <option value="<?php echo $data_usulan_ssh->getUnitId() ?>" selected>
                                            <?php echo UnitKerjaPeer::getStringUnitKerja($data_usulan_ssh->getUnitId()); ?>
                                        </option>
                                    </select>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Alasan Perbedaan Dinas</label>
                                    <?php echo textarea_tag('alasan_perbedaan_dinas', $data_usulan_ssh->getAlasanPerbedaanDinas(), array('class' => 'form-control', 'readonly' => true)); ?>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>Keterangan</label>
                                    <?php echo textarea_tag('keterangan', $data_usulan_ssh->getKeterangan(), array('class' => 'form-control')) ?>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Alasan Perbedaan Penyelia</label>
                                    <?php echo textarea_tag('alasan_perbedaan_penyelia', $data_usulan_ssh->getAlasanPerbedaanPenyelia(), array('class' => 'form-control', 'readonly' => true)); ?>
                                </div>
                            </div>
                            <!-- /.col -->

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Apakah komponen Iuran BPJS?</label>
                                    <select name="is_iuran_bpjs" class="form-control">
                                        <option value="tidak" selected>tidak</option>
                                        <option value="iya">iya</option>
                                    </select>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Perbedaan Harga Pendukung?</label>
                                    <select name="is_perbedaan_pendukung" class="form-control">
                                        <option value="<?php echo $data_usulan_ssh->getIsPerbedaanPendukung() ?>"
                                            selected>
                                            <?php
                                    if ($data_usulan_ssh->getIsPerbedaanPendukung() == 1) {
                                        echo 'Iya';
                                    } else {
                                        echo 'Tidak';
                                    }
                                    ?>
                                        </option>
                                        <option value="0">Tidak</option>
                                        <option value="1">Iya</option>
                                    </select>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Apakah komponen gaji yang terpotong BPJS?</label>
                                    <select name="is_potong_bpjs" class="form-control">
                                        <option value="tidak" selected>tidak</option>
                                        <option value="iya">iya</option>
                                    </select>
                                </div>
                            </div>
                            <!-- /.col --> 
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="tombol_filter">Tombol Filter</label><br />
                                    <button type="submit" name="commit" class="btn btn-outline-primary btn-sm">Simpan <i
                                            class="far fa-save"></i></button>
                                    <button type="reset" name="reset" class="btn btn-outline-danger btn-sm">Reset <i
                                            class="fa fa-backspace"></i></button>
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <?php echo '</form>'; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    function showHideUsulanMirip(id) {
        var row = $('#usulan_' + id);
        var img = $('#img_usulan_' + id);

        if (img) {
            var src = document.getElementById('img_usulan_' + id).getAttribute('src');
            var minus = src.indexOf('/<?php echo sfConfig::get('
                app_default_coding '); ?>/images/b_minus.png');
            if (minus !== -1) {
                src = '/<?php echo sfConfig::get('
                app_default_coding '); ?>/images/b_plus.png';
            } else {
                src = '/<?php echo sfConfig::get('
                app_default_coding '); ?>/images/b_minus.png';
            }
            img.attr('src', src);
        }


        if (minus === -1) {
            var usulan_id = 'usulan_' + id;
            var mirip = $('.mirip_' + id);
            var n = mirip.length;

            if (n > 0) {
                for (var i = 0; i < mirip.length; i++) {
                    var list_mirip = mirip[i];
                    list_mirip.style.display = 'table-row';
                }
            } else {
                $('#indicator').show();
                $.ajax({
                    url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/usulan_ssh/getUsulanMirip/id/" +
                        id + ".html",
                    context: document.body
                }).done(function (msg) {
                    $('#usulan_' + id).after(msg);
                    $('#indicator').remove();
                });
            }
        } else {
            $('.mirip_' + id).remove();
            minus = -1;
        }
    }

    function showHideSSHMirip(id) {
        var row = $('#ssh_' + id);
        var img = $('#img_ssh_' + id);

        if (img) {
            var src = document.getElementById('img_ssh_' + id).getAttribute('src');
            var minus = src.indexOf('/<?php echo sfConfig::get('
                app_default_coding '); ?>/images/b_minus.png');
            if (minus !== -1) {
                src = '/<?php echo sfConfig::get('
                app_default_coding '); ?>/images/b_plus.png';
            } else {
                src = '/<?php echo sfConfig::get('
                app_default_coding '); ?>/images/b_minus.png';
            }
            img.attr('src', src);
        }

        if (minus === -1) {
            var ssh_id = 'ssh_' + id;
            var mirip_ssh = $('.mirip_ssh_' + id);
            var n = mirip_ssh.length;

            if (n > 0) {
                for (var i = 0; i < mirip_ssh.length; i++) {
                    var list_mirip_ssh = mirip_ssh[i];
                    list_mirip_ssh.style.display = 'table-row';
                }
            } else {
                $('#indicator').show();
                $.ajax({
                    url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/usulan_ssh/getSSHMirip/id/" +
                        id + ".html",
                    context: document.body
                }).done(function (msg) {
                    $('#ssh_' + id).after(msg);
                    $('#indicator').remove();
                });
            }
        } else {
            $('.mirip_ssh_' + id).remove();
            minus = -1;
        }
    }

    function showHideSSHSatuKodeBarang(id) {
        var row = $('kode_' + id);
        var img = $('#img_kode_' + id);

        if (img) {
            var src = document.getElementById('img_kode_' + id).getAttribute('src');
            var minus = src.indexOf('/<?php echo sfConfig::get('
                app_default_coding '); ?>/images/b_minus.png');
            if (minus != -1) {
                src = '/<?php echo sfConfig::get('
                app_default_coding '); ?>/images/b_plus.png';
            } else {
                src = '/<?php echo sfConfig::get('
                app_default_coding '); ?>/images/b_minus.png';
            }
            img.attr('src', src);
        }


        if (minus == -1) {
            var kode_id = 'kode_' + id;
            var samakode = $('.samakode_' + id);
            var n = samakode.length;
            if (n > 0) {
                for (var i = 0; i < samakode.length; i++) {
                    var barang = samakode[i];
                    barang.style.display = 'table-row';
                }
            } else {
                $('#indicator').show();
                $.ajax({
                    url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/usulan_ssh/getSSHSatuKodeBarang/id/" +
                        id + ".html",
                    context: document.body
                }).done(function (msg) {
                    $('#kode_' + id).after(msg);
                    $('#indicator').remove();
                });
            }
        } else {
            $('.samakode_' + id).remove();
            minus = -1;
        }
    }

    function showHideKomponenTahunLalu(id) {
        var row = $('#lalu_' + id);
        var img = $('#img_lalu_' + id);

        if (img) {
            var src = document.getElementById('img_lalu_' + id).getAttribute('src');
            var minus = src.indexOf('/<?php echo sfConfig::get('
                app_default_coding '); ?>/images/b_minus.png');
            if (minus !== -1) {
                src = '/<?php echo sfConfig::get('
                app_default_coding '); ?>/images/b_plus.png';
            } else {
                src = '/<?php echo sfConfig::get('
                app_default_coding '); ?>/images/b_minus.png';
            }
            img.attr('src', src);
        }


        if (minus === -1) {
            var ssh_id = 'lalu_' + id;
            var ssh_lalu = $('.ssh_lalu_' + id);
            var n = ssh_lalu.length;

            if (n > 0) {
                for (var i = 0; i < ssh_lalu.length; i++) {
                    var list_ssh_lalu = ssh_lalu[i];
                    list_ssh_lalu.style.display = 'table-row';
                }
            } else {
                $('#indicator').show();
                $.ajax({
                    url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/usulan_ssh/getKomponenTahunLalu/id/" +
                        id + ".html",
                    context: document.body
                }).done(function (msg) {
                    $('#lalu_' + id).after(msg);
                    $('#indicator').remove();
                });
            }
        } else {
            $('.ssh_lalu_' + id).remove();
            minus = -1;
        }
    }
    $("#rekening").change(function () {
        var id = $(this).val();
        if (id.slice(0, 5) != '5.2.3') {
            $('#ajax-barang').css('display', 'none');
        } else {
            $('#ajax-barang').css('display', 'block');
        }
    });

    $("#harga").change(function () {
        var harga = $('#harga').val();
        harga = harga.replace(',', '.');
        var formatter = new Intl.NumberFormat(['en'], {
            minimumFractionDigits: 3,
        });

        $('#hargacek').val(formatter.format(harga));

    });
</script>
