<div class="card-body table-responsive p-0">
    <table class="table table-hover">
        <thead class="head_peach">
            <tr>
                <th>Perangkat Daerah</th>
                <th>User Dinas</th>
                <th>File Excel</th>
                <th>File Compress</th>
                <th>Nomor SPJM</th>
                <th>Waktu</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $i = 1;
            foreach ($pager->getResults() as $usulankomponenlist):
                $odd = fmod( ++$i, 2);
                ?>
                <tr>
                    <?php echo form_tag('usulan_ssh/saveperbaikanusulan', 'multipart=true'); ?>
                    <input type="hidden" name="id_usulan" value="<?php echo $usulankomponenlist->getIdUsulan() ?>" />
                    <td style="text-align: center">                                
                        <?php echo '<b>(' . $usulankomponenlist->getSkpd() . ') ' . UnitKerjaPeer::getStringUnitKerja($usulankomponenlist->getSkpd()) . '</b>'; ?>
                    </td>
                    <td style="text-align: center">
                        <?php echo '<b>' . $usulankomponenlist->getPenyelia() . '</b>'; ?>
                    </td>
                    <td style="text-align: center">
                        <?php if ($usulankomponenlist->getFilepath() <> '' && $usulankomponenlist->getFilepath() <> NULL) { ?>
                            <a target="_blank" href="<?php echo $usulankomponenlist->getFilepath() ?>"><?php echo date("d-m-Y_H-i", strtotime($usulankomponenlist->getCreatedAt())); ?></a>                
                            <?php
                        } else {
                            //echo input_file_tag('file');
                        }
                        ?>                
                    </td>
                    <td style="text-align: center">
                        <?php if ($usulankomponenlist->getFilepathRar() <> '' && $usulankomponenlist->getFilepathRar() <> NULL) { ?>
                            <a target="_blank" href="<?php echo $usulankomponenlist->getFilepathRar() ?>"><?php echo date("d-m-Y_H-i", strtotime($usulankomponenlist->getCreatedAt())); ?></a>
                            <?php
                            echo input_file_tag('file_spjm');
                        } else {
                            echo input_file_tag('file_spjm');
                        }
                        ?>                
                    </td>
                    <td style="text-align: center">
                        <?php echo $usulankomponenlist->getNomorSurat(); ?>
                    </td>
                    <td style="text-align: center">
                        <?php echo 'Created : ' . date("d-m-Y H:i", strtotime($usulankomponenlist->getCreatedAt())) . '<br/>'; ?>
                        <?php echo 'Updated : ' . date("d-m-Y H:i", strtotime($usulankomponenlist->getUpdatedAt())); ?>
                    </td>
                    <td style="text-align: center">
                        <?php
                        if ($usulankomponenlist->getStatusVerifikasi() == true) {
                            echo 'Terverifikasi Penyelia';
                        } else {
                            if ($usulankomponenlist->getStatusTolak() == true) {
                                echo 'Ditolak penyelia <br/>karena ' . $usulankomponenlist->getKomentarVerifikator();
                            } else {
                                echo 'Antrian';
                            }
                        }
                        ?>
                    </td>
                    <td>
                        <?php echo '<input type="submit" class="btn btn-outline-primary btn-sm" name="submit" value="Perbaikan Usulan"/>'; ?>
                    </td>
                    <?php echo '</form>'; ?>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
<div class="card-footer clearfix">
    <ul class="pagination pagination-sm m-0 float-left">
        <?php
            echo format_number_choice('[0] no result|[1] 1 result|(1,+Inf] %1% results', array('%1%' => $pager->getNbResults()), $pager->getNbResults());
        ?>
    </ul>
    <ul class="pagination pagination-sm m-0 float-right">
        <?php 
        if ($pager->haveToPaginate()): 
            echo '<li class="page-item">'.link_to('Previous', 'usulan_ssh/usulanperbaikanlist?page=' . $pager->getPreviousPage(), array('class' => 'page-link', 'align' => 'absmiddle', 'alt' => __('Previous'), 'title' => __('Previous'))).'</li>';

            foreach ($pager->getLinks() as $page):
                $activeclass = ($page == $pager->getPage()) ? 'active' : '';
                echo '<li class="page-item '.$activeclass.'">'.link_to_unless($page == $pager->getPage(), $page, "usulan_ssh/usulanperbaikanlist?page={$page}", array('class' => 'page-link')).'</li>';
            endforeach;
            echo '<li class="page-item">'.link_to('Next', 'usulan_ssh/usulanperbaikanlist?page=' . $pager->getNextPage(), array('class' => 'page-link', 'align' => 'absmiddle', 'alt' => __('Next'), 'title' => __('Next'))).'</li>'; 
            endif;
        ?>
    </ul>
</div>