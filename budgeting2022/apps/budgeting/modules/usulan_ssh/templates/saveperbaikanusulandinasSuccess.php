<?php use_helper('I18N', 'Date', 'Validation') ?>

<div id="sf_admin_container">
    <?php
    $array_salah = explode('|', $string_salah);
    ?>
    <h2>Terdapat kesalahan data Upload Usulan Komponen. Silahkan Cek Isian Data Excel Anda.</h2>
    <div class="eula" align="center">
        <ul>
            <?php
            foreach ($array_salah as $value) {
                if ($value <> '') {
                    echo '<li>' . $value . '</li>';
                }
            }
            ?>
        </ul><br/><br/>
        <button onclick="javascript:history.back(-1)">Kembali ke halaman sebelumnya</button>
    </div>


</div>