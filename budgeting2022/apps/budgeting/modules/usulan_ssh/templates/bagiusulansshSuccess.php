<?php use_helper('I18N', 'Date', 'Object', 'Javascript', 'Validation') ?>
<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Pembagian Survey Usulan</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Usulan SHS</a></li>
          <li class="breadcrumb-item active">Pembagian Survey Usulan</li>
        </ol>
      </div>
    </div>
  </div>
</section>
<section class="content">
    <?php include_partial('usulan_ssh/list_messages'); ?>
    <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
                <div class="card-body table-responsive">
                    <table class="table table-hover">
                        <thead class="head_peach">
                            <tr>
                                <th>ID SPJM</th>
                                <th colspan="2">Penyelia - Perangkat Daerah</th>
                                <th>Usulan</th>
                                <th colspan="2">Tanggal</th>
                                <th colspan="3">Dukungan</th>
                                <th>Nomor Surat</th>
                                <th colspan="2">Status</th>
                                <th colspan="2">Action</th>
                            </tr>
                        </thead>
                        <tbody>     
                            <?php
                            $i = 1;
                            $con = Propel::getConnection();
                            foreach ($rs_usulanSPJM as $usulan_SPJM) 
                            {
                                $id_spjm = $usulan_SPJM->getIdSpjm();
                                $tgl_spjm = $usulan_SPJM->getTglSpjm();
                                $penyelia = $usulan_SPJM->getPenyelia();
                                $skpd = $usulan_SPJM->getSkpd();
                                $tipe_usulan = $usulan_SPJM->getTipeUsulan();
                                $jenis_usulan = $usulan_SPJM->getJenisUsulan();
                                $jumlah_dukungan = $usulan_SPJM->getJumlahDukungan();
                                $created_at = $usulan_SPJM->getCreatedAt();
                                $updated_at = $usulan_SPJM->getUpdatedAt();
                                $status_verifikasi = $usulan_SPJM->getStatusVerifikasi();
                                $filepath = $usulan_SPJM->getFilepath();
                                $odd = fmod($i++, 2);

                                $query = "select * from unit_kerja where unit_id = '$skpd'";
                                $stmt = $con->prepareStatement($query);
                                $t = $stmt->executeQuery();
                                while ($t->next()) {
                                    $nama_skpd = $t->getString('unit_name');
                                }

                                $query_total_confirm_penyelia = "select count(*) as total from ebudget.usulan_ssh "
                                        . "where status_hapus = false and shsd_id is null and surveyor is null and status_verifikasi = FALSE and status_survey = false and status_confirmasi_penyelia = TRUE "
                                        . "and id_spjm = '$id_spjm'";
                                $stmt_total_confirm = $con->prepareStatement($query_total_confirm_penyelia);
                                $total_confirm_q = $stmt_total_confirm->executeQuery();
                                while ($total_confirm_q->next()) {
                                    $total_confirm = $total_confirm_q->getString('total');
                                }
                                if ($total_confirm > 0) {
                                    ?>
                                <tr class="sf_admin_row_1" id="spjm_<?php echo $id_spjm ?>" style="text-align: center">
                                    <td><?php echo link_to_function(image_tag('b_plus.png', array('name' => 'img_' . $id_spjm, 'id' => 'img_' . $id_spjm, 'border' => 0)), 'showHideUsulan(' . $id_spjm . ')') . ' '; ?>SPJM-<?php echo $id_spjm ?></td>
                                    <td colspan="2"c><?php echo $penyelia . ' : ' . $nama_skpd ?></td>
                                    <td><?php echo $tipe_usulan . ' - ' . $jenis_usulan ?></td>
                                    <td colspan="2"><?php echo date("d-m-Y H:i", strtotime($tgl_spjm)); ?></td>
                                    <td colspan="3"><?php echo $jumlah_dukungan . ' dukungan' ?></td>
                                    <td style="text-align: center">
                                        <?php echo $usulan_SPJM->getNomorSurat(); ?>
                                    </td>
                                    <td colspan="2" style="font-weight: bold">
                                        <?php
                                        echo date("d-m-Y H:i", strtotime($updated_at)) . '<br/>';
                                        if ($status_verifikasi == false) {
                                            echo 'Antrian';
                                        } else {
                                            echo 'Verifikasi';
                                        }
                                        ?>
                                    </td>
                                    <td colspan="2">
                                        <?php
                                        $c = new Criteria();
                                        $c->addAscendingOrderByColumn(SurveyorPeer::NAMA);
                                        $v = SurveyorPeer::doSelect($c);                                       
                                        ?>

                                        <!-- coba select manual -->
                                        <select name="nama" id="nama" class="js-example-basic-single form-control select2" onchange="setSurveyor('<?php echo $id_spjm ?>',this)">
                                            <option value="">--Pilih Surveyor--</option>
                                            <?php foreach($v as $surveyor) { ?>
                                            <option value="<?php echo $surveyor->getNama(); ?>" <?php ((isset($filters['nama'])&&$filters['nama']==$surveyor->getNama()) ? 'selected' : '') ?> ><?php echo $surveyor->getNama(); ?></option>
                                            <?php } ?>
                                        </select>
                                    </td>
                                </tr>
                                <?php
                                }
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
          </div>
        </div>
    </div>
</section>
<script>
    function showHideUsulan(id) {
        var row = $('#spjm_' + id);
        var img = $('#img_' + id);

        if (img) {
            var src = document.getElementById('img_' + id).getAttribute('src');
            var minus = src.indexOf('/<?php echo sfConfig::get('app_default_coding'); ?>/images/b_minus.png');
            if (minus != -1) {
                src = '/<?php echo sfConfig::get('app_default_coding'); ?>/images/b_plus.png';
            } else {
                src = '/<?php echo sfConfig::get('app_default_coding'); ?>/images/b_minus.png';
            }
            img.attr('src', src);
        }


        if (minus == -1) {
            var spjm_id = 'spjm_' + id;
            var usulan = $('.usulan_' + id);
            var n = usulan.length;

            if (n > 0) {
                for (var i = 0; i < usulan.length; i++) {
                    var pekerjaan = usulan[i];
                    pekerjaan.style.display = 'table-row';
                }
            } else {
                $('#indicator_' + id).show();
                $.ajax({
                    url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/usulan_ssh/getUsulanAll/id/" + id + ".html",
                    context: document.body
                }).done(function(msg) {
                    $('#spjm_' + id).after(msg);
                    $('#indicator_' + id).remove();
                });
            }
        } else {
            $('.usulan_' + id).remove();
            minus = -1;
        }
    }

    function setSurveyor(id,obj) {
        var id_usulan=id;
        var surveyor=obj.value;
        console.log('id='+id_usulan+' & surveyor='+surveyor);

        $.ajax({
            type:'GET',
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/usulan_ssh/setNamaSurveyor/id/" + id_usulan + "/surveyor/" + surveyor + ".html",
            context: document.body
        }).done(function (msg) {
            console.log(msg);
        });
        toastr.success("Pembagian Survey Telah Disimpan.","Success!");
        $(obj).parents('tr').detach();

        // $.ajax({
        //     url: "/<?php //echo sfConfig::get('app_default_coding'); ?>/index.php/usulan_ssh/setNamaSurveyor/id/" + id_usulan + "/surveyor/" + surveyor + ".html",
        //     type: "GET",
        //     dataType: 'json',
        //     success: function(hasil){
        //         console.log(hasil);
        //     }
        // })

        //alert(id+'-'+surveyor);
    }

</script>