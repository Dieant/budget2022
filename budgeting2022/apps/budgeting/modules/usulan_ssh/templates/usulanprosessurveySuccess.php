<?php use_helper('I18N', 'Date', 'Url', 'Javascript', 'Form', 'Object') ?>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myModalLabel">Alasan Tolak Verifikasi Survey</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php echo form_tag('usulan_ssh/tolakVerifikasiSurveyDenganAlasan', array('class' => 'form-horizontal')); ?>
            <?php echo input_hidden_tag('id_usulan', $data_usulan_ssh->getIdUsulan()); ?>
            <div class="modal-body">
                <div class="form-group">
                    <label class="col-xs-3 text-right">Usulan</label>
                    <div class="col-xs-9">
                        <?php echo $data_usulan_ssh->getNama() . ' ' . $data_usulan_ssh->getSpec() ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-xs-3 text-right">Jenis Penolakan</label>
                    <div class="col-xs-9">
                        <select name="jenis_alasan" class="form-control select2">
                            <option value="">--Silahkan memilih tipe alasan penolakan--</option>
                            <option value="sudah muncul komponen dengan nama yang sama">Sudah muncul komponen dengan nama yang sama</option>
                            <option value="dukungan tidak sesuai">Dukungan tidak sesuai</option>
                            <option value="dukungan tidak ada">Dukungan tidak ada</option>
                            <option value="usulan tidak disertai gambar">Usulan tidak disertai gambar</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-xs-3 text-right">Alasan</label>
                    <div class="col-xs-9">
                        <?php echo textarea_tag('alasan', '', array('class' => 'form-control')); ?>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <label class="col-xs-3 text-right">&nbsp;</label>
                <div class="col-xs-9">
                    <input type="submit" name="submit" value="Simpan" class="btn btn-outline-primary btn-sm" />
                    <input type="reset" name="reset" value="Reset" class="btn btn-outline-warning btn-sm" />
                </div>
            </div>
            <?php echo '</form>'; ?>
        </div>
    </div>
</div>
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Verifikasi Usulan SPTJM</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Survey SPTJM</a></li>
                    <li class="breadcrumb-item active">Verifikasi Survey</li>
                </ol>
            </div>
        </div>
    </div>
</section>
<!-- Main content -->
<section class="content">
    <?php include_partial('usulan_ssh/list_messages'); ?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="fab fa-buffer"></i> Cek Kesamaan Komponen
                        </h3>
                    </div>
                    <div class="card-body table-responsive">
                        <table class="table table-hover">
                            <tr id="usulan_<?php echo $data_usulan_ssh->getIdUsulan() ?>" style="width: 100%">
                                <td colspan="13">
                                    <?php echo link_to_function(image_tag('b_plus.png', array('name' => 'img_usulan_' . $data_usulan_ssh->getIdUsulan(), 'id' => 'img_usulan_' . $data_usulan_ssh->getIdUsulan(), 'border' => 0)), 'showHideUsulanMirip(' . $data_usulan_ssh->getIdUsulan() . ')') . ' '; ?><b>List
                                        Usulan Menyerupai nama
                                        "<?php echo $data_usulan_ssh->getNama() . ' ' . $data_usulan_ssh->getSpec() ?>"</b>
                                </td>
                            </tr>
                            <tr id="ssh_<?php echo $data_usulan_ssh->getIdUsulan() ?>" style="width: 100%">
                                <td colspan="13">
                                    <?php echo link_to_function(image_tag('b_plus.png', array('name' => 'img_ssh_' . $data_usulan_ssh->getIdUsulan(), 'id' => 'img_ssh_' . $data_usulan_ssh->getIdUsulan(), 'border' => 0)), 'showHideSSHMirip(' . $data_usulan_ssh->getIdUsulan() . ')') . ' '; ?><b>List
                                        SSH Menyerupai nama
                                        "<?php echo $data_usulan_ssh->getNama() . ' ' . $data_usulan_ssh->getSpec() ?>"</b>
                                </td>
                            </tr>
                        </table>
                        <div class="card-body" id="indicator" style="display:none;" align="center">
                            <dt>&nbsp;</dt>
                            <dd><b>Mohon Tunggu</b><?php echo image_tag('loading.gif', array('align' => 'absmiddle')) ?>
                            </dd>
                        </div>
                    </div>
                </div>
            </div>
            <?php if($rs_usulan_mirip==1&&$rs_ssh_mirip==1){ ?>
            <div class="alert alert-danger" role="alert">Mohon cek di Cek Kesamaan Komponen Usulan dan SSH</div>
            <?php } elseif($rs_usulan_mirip==1){ ?>
            <div class="alert alert-danger" role="alert">Mohon cek di Cek Kesamaan Komponen Usulan</div>
            <?php } elseif($rs_ssh_mirip==1){ ?>
            <div class="alert alert-danger" role="alert">Mohon cek di Cek Kesamaan Komponen SSH</div>
            <?php } ?>
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="fab fa-foursquare"></i> Form Verifikasi Usulan
                        </h3>
                        <div class="card-tools">
                            <div class="input-group input-group-sm">
                                <button type="button" class="btn btn-outline-danger btn-sm" data-toggle="modal"
                                    data-target="#myModal">
                                    Kembalikan ke penyelia
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <?php echo form_tag('usulan_ssh/savekonfirmasisurvey', array('multipart' => true,'class' => 'form-horizontal')); ?>
                        <?php echo input_hidden_tag('id_usulan', $data_usulan_ssh->getIdUsulan()); ?>
                        <?php echo input_hidden_tag('id_spjm', $data_usulan_ssh->getIdSpjm()); ?>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Kode Barang</label>
                                    <?php echo input_tag('kode_barang_a', '(' . $data_usulan_ssh->getKodeBarang() . ') ' . KategoriShsdPeer::getStringKategoriShsdName($data_usulan_ssh->getKodeBarang()), array('required' => true, 'class' => 'form-control', 'class' => 'form-control', 'readonly' => true)); ?>
                                    <select name="kode_barang" class="form-control select2">
                                        <option value="<?php echo $data_usulan_ssh->getKodeBarang() ?>" selected>
                                            <?php echo '(' . $data_usulan_ssh->getKodeBarang() . ') ' . KategoriShsdPeer::getStringKategoriShsdName($data_usulan_ssh->getKodeBarang()); ?>
                                        </option>
                                        <?php foreach ($list_kode_barang as $value) { ?>
                                        <option value="<?php echo $value['kategori_shsd_id'] ?>">
                                            <?php echo '(' . $value['kategori_shsd_id'] . ') ' . $value['kategori_shsd_name'] ?>
                                        </option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Rekening</label>
                                    <?php echo input_tag('rekening_a', '(' . $data_usulan_ssh->getRekening() . ') ' . RekeningPeer::getStringRekeningName($data_usulan_ssh->getRekening()), array('required' => true, 'class' => 'form-control', 'class' => 'form-control', 'readonly' => true)); ?>
                                    <select id="rekening" name="rekening" class="form-control select2">
                                        <option value="<?php echo $data_usulan_ssh->getRekening() ?>" selected>
                                            <?php echo '(' . $data_usulan_ssh->getRekening() . ') ' . RekeningPeer::getStringRekeningName($data_usulan_ssh->getRekening()) ?>
                                        </option>
                                        <?php foreach ($list_rekening as $value) { ?>
                                        <option value="<?php echo $value->getRekeningCode() ?>">
                                            <?php echo '(' . $value->getRekeningCode() . ') ' . $value->getRekeningName() ?>
                                        </option>
                                        <?php }
                                ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Nama Komponen</label>
                                    <?php echo input_tag('nama', $data_usulan_ssh->getNama(), array('readonly' => true, 'required' => true, 'class' => 'form-control')); ?>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Nama Sebelum</label>
                                    <?php echo input_tag('nama_sebelum', $data_usulan_ssh->getNamaSebelum(), array('readonly' => true, 'class' => 'form-control')); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Spesifikasi</label>
                                    <?php echo input_tag('spec', $data_usulan_ssh->getSpec(), array('readonly' => true, 'class' => 'form-control')); ?>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Harga Sebelum</label>
                                    <?php echo input_tag('harga_sebelum', $data_usulan_ssh->getHargaSebelum(), array('readonly' => true, 'class' => 'form-control')); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Spesifikasi Tersembunyi</label>
                                    <?php echo input_tag('hidden_spec', $data_usulan_ssh->getHiddenSpec(), array('readonly' => true, 'class' => 'form-control')); ?>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Alasan Perubahan</label>
                                    <?php
                                        if ($data_usulan_ssh->getNamaSebelum() <> '' && $data_usulan_ssh->getHargaSebelum() > 0) {
                                        echo textarea_tag('alasan_perubahan', $data_usulan_ssh->getAlasanPerubahanHarga(), array('required' => true, 'class' => 'form-control'));
                                    } else {
                                        echo textarea_tag('alasan_perubahan', $data_usulan_ssh->getAlasanPerubahanHarga(), array('readonly' => true, 'class' => 'form-control'));
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Merek</label>
                                    <?php echo input_tag('merek', $data_usulan_ssh->getMerek(), array('readonly' => true, 'class' => 'form-control')); ?>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>CV Pendukung 1</label>
                                    <?php echo input_tag('cv_pendukung1', $data_usulan_ssh->getCv1(), array('readonly' => true, 'class' => 'form-control')); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Satuan</label>
                                    <select name="satuan" class="form-control select2">
                                        <option value="<?php echo $data_usulan_ssh->getSatuan() ?>" selected>
                                            <?php echo $data_usulan_ssh->getSatuan() ?>
                                        </option>
                                        <?php foreach ($list_satuan as $value) { ?>
                                        <option value="<?php echo $value->getSatuanName() ?>">
                                            <?php echo $value->getSatuanName() ?>
                                        </option>
                                        <?php }
                                ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Harga Pendukung 1</label>
                                    <?php echo input_tag('harga_pendukung1', $data_usulan_ssh->getHargaPendukung1(), array('readonly' => true, 'class' => 'form-control')); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Harga Satuan</label>                  
                                    <?php echo input_tag('harga', $data_usulan_ssh->getHarga(), array('readonly' => true, 'required' => true, 'class' => 'form-control')); ?>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>CV Pendukung 2</label>
                                    <?php echo input_tag('cv_pendukung2', $data_usulan_ssh->getCv2(), array('readonly' => true, 'class' => 'form-control')); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Pajak</label>
                                    <select name="pajak" class="form-control select2">
                                        <option value="<?php echo $data_usulan_ssh->getPajak() ?>" selected>
                                            <?php echo $data_usulan_ssh->getPajak() ?></option>
                                        <option value="0">0</option>
                                        <option value="10">10</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Harga Pendukung 2</label>
                                    <?php echo input_tag('harga_pendukung2', $data_usulan_ssh->getHargaPendukung2(), array('readonly' => true, 'class' => 'form-control')); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <?php 
                            if (strpos($data_usulan_ssh->getRekening(), '5.2') !== FALSE) {
                            ?>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Jenis Barang</label>
                                    <select name="komponen_tipe2" class="form-control select2">
                                        <option value="">BUKAN REKENING MODAL</option>
                                        <option value="KONTRUSKSI">KONTRUSKSI</option>
                                        <option value="NONKONSTRUKSI">NONKONSTRUKSI</option>
                                        <option value="ATRIBUSI">ATRIBUSI</option>
                                        <option value="PERENCANAAN">PERENCANAAN</option>
                                        <option value="PENGAWASAN">PENGAWASAN</option>
                                    </select>
                                </div>
                            </div>
                            <?php }?>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>CV Pendukung 3</label>
                                    <?php echo input_tag('cv_pendukung3', $data_usulan_ssh->getCv3(), array('readonly' => true, 'class' => 'form-control')); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Usulan SKPD</label>
                                    <select name="skpd" class="form-control select2">
                                        <option value="<?php echo $data_usulan_ssh->getUnitId() ?>" selected>
                                            <?php echo UnitKerjaPeer::getStringUnitKerja($data_usulan_ssh->getUnitId()); ?>
                                        </option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Harga Pendukung 3</label>
                                    <?php echo input_tag('harga_pendukung3', $data_usulan_ssh->getHargaPendukung3(), array('readonly' => true, 'class' => 'form-control')); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Alasan Perbedaan</label>
                                    <?php
                                    if ($data_usulan_ssh->getHarga() == $data_usulan_ssh->getHargaPendukung1() || $data_usulan_ssh->getHarga() == $data_usulan_ssh->getHargaPendukung2() || $data_usulan_ssh->getHarga() == $data_usulan_ssh->getHargaPendukung3()) {
                                        echo textarea_tag('alasan_perbedaan', $data_usulan_ssh->getAlasanPerbedaanPenyelia(), array('class' => 'form-control', 'readonly' => true));
                                    } else {
                                        echo textarea_tag('alasan_perbedaan', $data_usulan_ssh->getAlasanPerbedaanPenyelia(), array('class' => 'form-control', 'required' => true));
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Keterangan</label> 
                                    <?php echo textarea_tag('keterangan', $data_usulan_ssh->getKeterangan(), array('readonly' => true,'class' => 'form-control')) ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>CV Pendukung 1</label>
                                    <?php echo input_tag('cv_survey1', null, array('required' => true, 'class' => 'form-control')); ?>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>harga Pendukung 1</label>
                                    <?php echo input_tag('harga_survey1', null, array('required' => true, 'class' => 'form-control')); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Kontak Penyedia/Alamat</label>
                                    <?php echo input_tag('alamat1', null, array('required' => true, 'class' => 'form-control')); ?>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Nama Penerima</label>
                                    <?php echo input_tag('penerima1', null, array('required' => true, 'class' => 'form-control')); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Tanggal Konfirmasi</label>
                                    <?php echo input_date_tag('tgl_konfirmasi1', null, array('rich' => true, 'required' => true, 'class' => 'form-control')); ?>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                <label>Data Pendukung : (Gunakan file xls, xlsx, zip, rar, jpg, png, atau pdf)</label>
                                    <?php echo input_file_tag('dukungan1', array('class' => 'form-control')) ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                <label>CV Pendukung 2</label> 
                                    <?php echo input_tag('cv_survey2', null, array( 'class' => 'form-control')); ?>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                <label>Harga Pendukung 2</label>
                                    <?php echo input_tag('harga_survey2', null, array('class' => 'form-control')); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                <label>Kontak Penyedia/Alamat</label>
                                    <?php echo input_tag('alamat2', null, array('class' => 'form-control')); ?>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                <label>Nama Penerima</label>
                                    <?php echo input_tag('penerima2', null, array('class' => 'form-control')); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                <label>Tanggal Konfirmasi</label>
                                    <?php echo input_date_tag('tgl_konfirmasi2', null, array('rich' => true, 'class' => 'form-control')); ?>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                <label>Data Pendukung : (Gunakan file xls, xlsx, zip, rar, jpg, png, atau pdf)</label>
                                    <?php echo input_file_tag('dukungan2', array('class' => 'form-control')) ?>
                                </div>
                            </div> 
                        </div>\
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                <label>CV Pendukung 3</label> 
                                    <?php echo input_tag('cv_survey3', null, array( 'class' => 'form-control')); ?>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                <label>Harga Pendukung 3</label> 
                                    <?php echo input_tag('harga_survey3', null, array( 'class' => 'form-control')); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                <label>Kontak Penyedia/Alamat</label> 
                                    <?php echo input_tag('alamat3', null, array( 'class' => 'form-control')); ?>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                <label>Nama Penerima</label>
                                    <?php echo input_tag('penerima3', null, array( 'class' => 'form-control')); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                <label>Tanggal Konfirmasi</label> 
                                    <?php echo input_date_tag('tgl_konfirmasi3', null, array('rich' => true,'class' => 'form-control')); ?>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                <label>Data Pendukung : (Gunakan file xls, xlsx, zip, rar, jpg, png, atau pdf)</label> 
                                    <?php echo input_file_tag('dukungan3', array('class' => 'form-control')) ?>
                                </div>
                            </div> 
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                <label>Deskripsi Barang </label> 
                                    <?php echo textarea_tag('keterangan_surveyor', $data_usulan_ssh->getKeterangan(), array('class' => 'form-control')) ?>
                                </div> 
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Catatan Surveyor</label>
                                    <?php echo input_tag('catatan_surveyor', null, array('required' => true, 'class' => 'form-control')); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <input type="hidden" name="unit_id" value="<?php echo $data_usulan_ssh->getUnitId();?>">
                            <input type="hidden" name="surveyor" value="<?php echo $sf_user->getNamaLengkap();?>">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="tombol_filter">Tombol Filter</label><br />
                                    <button type="submit" name="submit" class="btn btn-outline-primary btn-sm">Simpan <i class="far fa-save"></i></button>
                                    <button type="reset" name="reset" class="btn btn-outline-danger btn-sm">Reset <i class="fa fa-backspace"></i></button>
                                </div>
                                <!-- /.form-group -->
                            </div>
                        </div>
                        <?php echo '</form>'; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    function showHideUsulanMirip(id) {
        var row = $('#usulan_' + id);
        var img = $('#img_usulan_' + id);

        if (img) {
            var src = document.getElementById('img_usulan_' + id).getAttribute('src');
            var minus = src.indexOf('/<?php echo sfConfig::get('app_default_coding'); ?>/images/b_minus.png');
            if (minus !== -1) {
                src = '/<?php echo sfConfig::get('app_default_coding'); ?>/images/b_plus.png';
            } else {
                src = '/<?php echo sfConfig::get('app_default_coding'); ?>/images/b_minus.png';
            }
            img.attr('src', src);
        }


        if (minus === -1) {
            var usulan_id = 'usulan_' + id;
            var mirip = $('.mirip_' + id);
            var n = mirip.length;

            if (n > 0) {
                for (var i = 0; i < mirip.length; i++) {
                    var list_mirip = mirip[i];
                    list_mirip.style.display = 'table-row';
                }
            } else {
                $('#indicator').show();
                $.ajax({
                    url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/usulan_ssh/getUsulanMirip/id/" + id + ".html",
                    context: document.body
                }).done(function(msg) {
                    $('#usulan_' + id).after(msg);
                    $('#indicator').remove();
                });
            }
        } else {
            $('.mirip_' + id).remove();
            minus = -1;
        }
    }

    function showHideSSHMirip(id) {
        var row = $('#ssh_' + id);
        var img = $('#img_ssh_' + id);

        if (img) {
            var src = document.getElementById('img_ssh_' + id).getAttribute('src');
            var minus = src.indexOf('/<?php echo sfConfig::get('app_default_coding'); ?>/images/b_minus.png');
            if (minus !== -1) {
                src = '/<?php echo sfConfig::get('app_default_coding'); ?>/images/b_plus.png';
            } else {
                src = '/<?php echo sfConfig::get('app_default_coding'); ?>/images/b_minus.png';
            }
            img.attr('src', src);
        }


        if (minus === -1) {
            var ssh_id = 'ssh_' + id;
            var mirip_ssh = $('.mirip_ssh_' + id);
            var n = mirip_ssh.length;

            if (n > 0) {
                for (var i = 0; i < mirip_ssh.length; i++) {
                    var list_mirip_ssh = mirip_ssh[i];
                    list_mirip_ssh.style.display = 'table-row';
                }
            } else {
                $('#indicator').show();
                $.ajax({
                    url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/usulan_ssh/getSSHMirip/id/" + id + ".html",
                    context: document.body
                }).done(function(msg) {
                    $('#ssh_' + id).after(msg);
                    $('#indicator').remove();
                });
            }
        } else {
            $('.mirip_ssh_' + id).remove();
            minus = -1;
        }
    }

    function showHideKomponenTahunLalu(id) {
        var row = $('#lalu_' + id);
        var img = $('#img_lalu_' + id);

        if (img) {
            var src = document.getElementById('img_lalu_' + id).getAttribute('src');
            var minus = src.indexOf('/<?php echo sfConfig::get('app_default_coding'); ?>/images/b_minus.png');
            if (minus !== -1) {
                src = '/<?php echo sfConfig::get('app_default_coding'); ?>/images/b_plus.png';
            } else {
                src = '/<?php echo sfConfig::get('app_default_coding'); ?>/images/b_minus.png';
            }
            img.attr('src', src);
        }


        if (minus === -1) {
            var ssh_id = 'lalu_' + id;
            var ssh_lalu = $('.ssh_lalu_' + id);
            var n = ssh_lalu.length;

            if (n > 0) {
                for (var i = 0; i < ssh_lalu.length; i++) {
                    var list_ssh_lalu = ssh_lalu[i];
                    list_ssh_lalu.style.display = 'table-row';
                }
            } else {
                $('#indicator').show();
                $.ajax({
                    url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/usulan_ssh/getKomponenTahunLalu/id/" + id + ".html",
                    context: document.body
                }).done(function(msg) {
                    $('#lalu_' + id).after(msg);
                    $('#indicator').remove();
                });
            }
        } else {
            $('.ssh_lalu_' + id).remove();
            minus = -1;
        }
    }

     var rupiah = document.getElementById('harga_survey1');
        rupiah.addEventListener('keyup', function(e){
            // tambahkan 'Rp.' pada saat form di ketik
            // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
            rupiah.value = formatRupiah(this.value, 'Rp. ');
        });

    var rupiah2 = document.getElementById('harga_survey2');
        rupiah2.addEventListener('keyup', function(e){
            // tambahkan 'Rp.' pada saat form di ketik
            // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
            rupiah2.value = formatRupiah(this.value, 'Rp. ');
        });

    var rupiah3 = document.getElementById('harga_survey3');
        rupiah3.addEventListener('keyup', function(e){
            // tambahkan 'Rp.' pada saat form di ketik
            // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
            rupiah3.value = formatRupiah(this.value, 'Rp. ');
    });
 
        /* Fungsi formatRupiah */
        function formatRupiah(angka, prefix){
            var number_string = angka.replace(/[^,\d]/g, '').toString(),
            split           = number_string.split(','),
            sisa            = split[0].length % 3,
            rupiah          = split[0].substr(0, sisa),
            ribuan          = split[0].substr(sisa).match(/\d{3}/gi);
 
            // tambahkan titik jika yang di input sudah menjadi angka ribuan
            if(ribuan){
                separator = sisa ? '.' : '';
                rupiah += separator + ribuan.join('.');
            }
 
            rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
            return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
    }

</script>