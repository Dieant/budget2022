<?php use_helper('I18N', 'Date', 'Object', 'Javascript', 'Validation') ?>
<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Tambah Usulan SHS</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Usulan SHS</a></li>
          <li class="breadcrumb-item active">Tambah Usulan</li>
        </ol>
      </div>
    </div>
    <div class="text-right">
        <?php echo link_to('Tutorial File Upload ', sfConfig::get('app_path_uploads') . 'video/tutorial_ssh.mp4', array('class' => 'btn btn-outline-primary btn-sm', 'target' => '_blank')); ?>
        <?php echo link_to('Format File Upload ', sfConfig::get('app_path_pdf') . 'template/template_usulan_eBudgeting2022.xls', array('class' => 'btn btn-outline-primary btn-sm', 'target' => '_blank')); ?> 
        <?php echo link_to('Format Rekap Usulan ', sfConfig::get('app_path_pdf') . 'template/format_rekap_usulan.xlsx', array('class' => 'btn btn-outline-primary btn-sm', 'target' => '_blank')); ?> 
        <?php echo link_to('Format SPTJM Baru ', sfConfig::get('app_path_pdf') . 'format_sptjm_baru.pdf', array('class' => 'btn btn-outline-primary btn-sm ', 'target' => '_blank')); ?> 
    </div>
  </div>
</section>
<section class="content">
    <?php include_partial('usulan_ssh/list_messages'); ?>
    <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">
                        <i class="fab fa-foursquare"></i> Form Tambah
                    </h3>
                </div>
                <div class="card-body">
                    <?php echo form_tag('usulan_ssh/saveusulandinas', array('multipart' => true, 'class' => 'form-horizontal')); ?>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                            <label>ID User</label>
                            <?php echo input_tag('user_dinas', $sf_user->getNamaLogin(), array('readonly' => true, 'required' => true, 'class' => 'form-control')); ?>
                        </div>
                      </div>
                      <!-- /.col -->
                      <div class="col-md-6">
                        <div class="form-group">
                            <label>Tanggal</label>
                            <?php echo input_date_tag('tanggal_spjm', null, array('rich' => true, 'required' => true, 'class' => 'form-control')); ?>
                        </div>
                      </div>
                      <!-- /.col -->
                      <div class="col-md-6">
                        <div class="form-group">
                            <label>Perangkat Daerah</label>
                            <?php echo input_tag('skpd', $sf_user->getNamaUser(), array('readonly' => true, 'required' => true, 'class' => 'form-control')); ?>
                        </div>
                      </div>
                      <!-- /.col -->
                      <div class="col-md-6">
                        <div class="form-group">
                            <label>Jenis Usulan</label>
                            <select name="jenis_usulan" class="form-control">
                                <option value="BARU">BARU</option>
                                <option value="PERUBAHAN HARGA">PERUBAHAN HARGA</option>
                            </select> 
                        </div>
                      </div>
                      <!-- /.col -->
                      <div class="col-md-6">
                        <div class="form-group">
                            <label>Jumlah Komponen dalam SPJM</label>
                            <?php echo input_tag('jumlah_komponen',0, array('required' => true, 'class' => 'form-control')); ?>
                        </div>
                      </div>
                      <!-- /.col -->
                      <div class="col-md-6">
                        <div class="form-group">
                            <label>Jumlah dukungan Penyedia berbeda</label>
                            <?php echo input_tag('jumlah_dukungan',0, array('required' => true, 'class' => 'form-control')); ?>
                        </div>
                      </div>
                      <!-- /.col -->
                      <div class="col-md-6">
                        <div class="form-group">
                            <label>Nomor Surat</label>
                            <?php echo input_tag('nomor_surat','', array('required' => true, 'class' => 'form-control')); ?>
                        </div>
                      </div>
                      <!-- /.col -->
                      <div class="col-md-6">
                        <div class="form-group">
                            <label>Penjelasan Komponen</label>
                            <?php echo input_tag('keterangan_dinas','', array('required' => true, 'class' => 'form-control')); ?>
                        </div>
                      </div>
                      <!-- /.col -->
                      <div class="col-md-6">
                        <div class="form-group">
                            <label>Upload Usulan Komponen:<br/>(Gunakan File Excel .xls hasil download)</label>
                            <?php echo input_file_tag('file', array('class' => 'form-control')) ?>
                            <span style="color: red">* komponen ini akan melalui proses verifikasi oleh penyelia terlebih dahulu sebelum masuk dalam Aplikasi.</span>
                        </div>
                      </div>
                      <!-- /.col -->
                      <div class="col-md-6">
                        <div class="form-group">
                            <label>Upload Data Pendukung  dan Surat Pertanggung Jawaban Mutlak :<br/>(Gunakan File .rar atau .zip)</label>
                            <?php echo input_file_tag('file_spjm', array('class' => 'form-control')) ?>
                        </div>
                      </div>
                      <!-- /.col -->
                      <div class="col-md-12">
                        <div class="form-group">
                        <label>
                        Proses Upload Usulan Komponen akan berjalan, apabila mengupload <strong>file compress Data Pendukung Usulan dan Surat Pertanggung Jawaban Mutlak (dengan ekstensi .rar atau .zip)</strong> dan <strong>file Data Usulan Komponen (dengan.xls seperti hasil download)</strong>.<br/>Jangan Lupa Untuk Mengupload Data <strong>Surat Pertanggung Jawaban Mutlak Bersama dengan Data Pendukung.Size maksimal file compress data usulan adalah tidak terhingga.<br/>Proses Upload Usulan Komponen tidak akan berjalan, apabila total data pada excel Usulan Komponen tidak sama dengan isian Jumlah Komponen dalam SPJM.</strong>
                        </label>
                        </div>
                      </div>
                      <div class="col-md-2">
                        <div class="form-group">
                            <label class="tombol_filter">Tombol Filter</label><br/>
                            <button type="submit" name="commit" class="btn btn-outline-primary btn-sm">Simpan <i class="far fa-save"></i></button>
                            <button type="reset" name="reset" class="btn btn-outline-danger btn-sm">Reset <i class="fa fa-backspace"></i></button>
                        </div>
                        <!-- /.form-group -->
                      </div>
                      <!-- /.col -->                      
                    </div>
                    <?php echo '</form>'; ?>
                </div>
            </div>
          </div>
        </div>
    </div>
</section>