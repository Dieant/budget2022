<div class="card-body table-responsive p-0">
    <table class="table table-hover">
        <thead class="head_peach">
            <tr>
                <th>Penyelia</th>
                <th>Perangkat Daerah</th>
                <th>Jumlah SSH</th>
                <th>Request Date</th>
                <th>Nomor Surat</th>
                <th>Verifikator</th>
                <th>Token</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $i = 1;
            foreach ($pager->getResults() as $usulantoken): $odd = fmod(++$i, 2)
                ?>
                <tr>
                    <td style="text-align: center"><?php echo $usulantoken->getPenyelia() ?></td>
                    <td><?php echo '(' . $usulantoken->getUnitId() . ') ' . UnitKerjaPeer::getStringUnitKerja($usulantoken->getUnitId()); ?></td>
                    <td style="text-align: center"><?php echo $usulantoken->getJumlahSsh() ?></td>
                    <td style="text-align: center"><?php echo date("d-m-Y H:i", strtotime($usulantoken->getRequiredAt())) ?></td>
                    <td style="text-align: center"><?php echo $usulantoken->getNomorSurat(); ?></td>
                    <td style="text-align: center"><?php echo $usulantoken->getCreatedBy() ?></td>
                    <td style="text-align: center; font-weight: bold"><?php echo $usulantoken->getToken() ?></td>
                    <td style="text-align: center; font-weight: bold">
                        <?php
                        if ($usulantoken->getStatusUsed() == false) {
                            echo 'Belum Digunakan';
                        } else {
                            echo 'Telah Digunakan<br/>';
                            echo date("d-m-Y H:i", strtotime($usulantoken->getUsedAt()));
                        }
                        ?>
                    </td>
                    <td>
                        <?php echo link_to('Hapus <i class="fa fa-trash"></i>', 'usulan_ssh/hapusToken?token=' . $usulantoken->getToken(), Array('confirm' => 'Apakah yakin menghapus usulan Token : ' . $usulantoken->getToken() . '?', 'class' => 'btn btn-outline-danger btn-sm')); ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
<div class="card-footer clearfix">
    <ul class="pagination pagination-sm m-0 float-left">
        <?php
            echo format_number_choice('[0] no result|[1] 1 result|(1,+Inf] %1% results', array('%1%' => $pager->getNbResults()), $pager->getNbResults());
        ?>
    </ul>
    <ul class="pagination pagination-sm m-0 float-right">
        <?php 
        if ($pager->haveToPaginate()): 
            echo '<li class="page-item">'.link_to('Previous', 'usulan_ssh/usulantoken?page=' . $pager->getPreviousPage(), array('class' => 'page-link', 'align' => 'absmiddle', 'alt' => __('Previous'), 'title' => __('Previous'))).'</li>';

            foreach ($pager->getLinks() as $page):
                $activeclass = ($page == $pager->getPage()) ? 'active' : '';
                echo '<li class="page-item '.$activeclass.'">'.link_to_unless($page == $pager->getPage(), $page, "usulan_ssh/usulantoken?page={$page}", array('class' => 'page-link')).'</li>';
            endforeach;
            echo '<li class="page-item">'.link_to('Next', 'usulan_ssh/usulantoken?page=' . $pager->getNextPage(), array('class' => 'page-link', 'align' => 'absmiddle', 'alt' => __('Next'), 'title' => __('Next'))).'</li>'; 
            endif;
        ?>
    </ul>
</div>
