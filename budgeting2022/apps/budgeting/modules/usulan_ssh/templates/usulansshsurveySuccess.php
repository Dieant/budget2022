<?php use_helper('I18N', 'Date', 'Object', 'Javascript', 'Validation') ?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Data Survey SPTJM</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Survey SPTJM</a></li>
                    <li class="breadcrumb-item active">Data Survey SPTJM</li>
                </ol>
            </div>
        </div>
    </div>
</section>
<section class="content">
    <?php include_partial('usulan_ssh/list_messages'); ?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body table-responsive">
                        <table class="table table-hover">
                            <thead class="head_peach">
                                <tr>
                                    <th>SKPD</th>
                                    <th>Nama</th>
                                    <th>Spesifikasi</th>
                                    <th>Hidden Spec</th>
                                    <th>Merek</th>
                                    <th>Rekening</th>
                                    <th>Satuan</th>
                                    <th>Harga</th>
                                    <th>Pajak</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                            $user=$sf_user->getNamaLengkap();
                            // $user_arr = explode(",",$user);
                            $i = 1;
                            foreach ($rs_usulanSurvey as $usulanssh):
                                $survey = $usulanssh->getSurveyor();
                                if( $user == $survey)
                                {
                            ?>
                                <tr>
                                    <td>
                                        <?php
                                        echo '<b>(' . $usulanssh->getUnitId() . ') ' . UnitKerjaPeer::getStringUnitKerja($usulanssh->getUnitId()) . '</b><br/>';
                                        echo 'Nomor Surat : <b>' . UsulanSPJMPeer::getStringNomorSurat($usulanssh->getIdSpjm()) . '</b><br/>';
                                        echo 'File Rar Upload: <b><a href="' . UsulanDinasPeer::getStringFilepath($usulanssh->getIdSpjm()) . '">Download RAR</a></b><br/>';
                                        echo 'File Excel Upload : <b><a href="' . UsulanSPJMPeer::getStringFilepath($usulanssh->getIdSpjm()) . '">Download Excel</a></b>';
                                        ?>
                                    </td>
                                    <td><?php echo $usulanssh->getNama() ?></td>
                                    <td><?php echo $usulanssh->getSpec() ?></td>
                                    <td><?php echo $usulanssh->getHiddenSpec() ?></td>
                                    <td style="text-align: center"><?php echo $usulanssh->getMerek() ?></td>
                                    <td style="text-align: center"><?php echo $usulanssh->getRekening() ?></td>
                                    <td style="text-align: center"><?php echo $usulanssh->getSatuan() ?></td>
                                    <td style="text-align: right"><?php echo number_format($usulanssh->getHarga()) ?>
                                    </td>
                                    <td style="text-align: center"><?php echo $usulanssh->getPajak() ?></td>
                                    <td style="text-align: center; font-weight: bold" width="150">
                                        <?php
                                        $namasurvey = $usulanssh->getSurveyor();
                                        if ($usulanssh->getStatusHapus() == t) {
                                            echo '<font style="color:red">Ditolak</font>';
                                        } else {
                                            if ($usulanssh->getStatusVerifikasi() == t) {
                                                echo '<font style="color:green">Terverifikasi</font>';
                                            } else {
                                                echo '<font style="color:blue">Antrian</font><br/>';
                                                if(!empty($namasurvey) && $usulanssh->getStatusConfirmasiPenyelia()==t && $usulanssh->getStatusSurvey()==t){
                                                    echo '<font style="color:blue">Posisi di Tim Data</font>';
                                                } elseif(!empty($namasurvey) && $usulanssh->getStatusConfirmasiPenyelia()==t){
                                                    echo '<font style="color:blue">Posisi di Survey</font>';
                                                } elseif($usulanssh->getStatusPending()==t){
                                                    echo '<font style="color:blue">Posisi Komponen Pending</font>';
                                                } elseif($usulanssh->getStatusConfirmasiPenyelia()==t && empty($namasurvey)){
                                                    echo '<font style="color:blue">Posisi di Tim Data</font>';
                                                } else{
                                                    echo '<font style="color:blue">Posisi di Penyelia</font>';
                                                }
                                            }
                                        }
                                        echo '<br/>';
                                        echo $usulanssh->getKomentarSurvey();
                                        ?>
                                    </td>
                                    <td style="text-align: center; font-weight: bold">
                                        <div class="btn-group">
                                            <?php
                                            echo link_to('<i class="fa fa-edit"></i> Edit', 'usulan_ssh/usulanprosessurvey?id=' . $usulanssh->getIdUsulan() . '&key=' . md5('cek_konfirmasi_usulan'), array('class' => 'btn btn-outline-primary btn-sm'));
                                            ?>
                                        </div>
                                    </td>
                                </tr>
                                <?php
                                }
                            endforeach;  
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
</section>