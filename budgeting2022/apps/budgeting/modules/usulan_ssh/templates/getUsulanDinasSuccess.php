<?php use_helper('Url', 'Javascript', 'Form', 'Object'); ?>
<?php
$i = 0;
?>
<tr class="usulan_<?php echo $id ?>" style="text-align: center; font-weight: bold">
    <th>Usulan</th>
    <th>Nama</th>
    <th>Spesifikasi</th>
    <th>Hidden Spec</th>
    <th>Merek</th>
    <th>Rekening</th>
    <th>Satuan</th>
    <th>Harga</th>
    <th>Pajak</th>
    <th>Waktu</th>
    <th>Status/Komentar</th>
</tr>
<?php
    $i = 1;
    foreach ($rs_usulan as $usulanssh):
        $odd = fmod( ++$i, 2);
        ?>
        <tr class="usulan_<?php echo $id ?> sf_admin_row_<?php echo $odd ?>">
            <td>
                <?php 
                echo 'Oleh : <b>' . UsulanSPJMPeer::getStringPenyelia($usulanssh->getIdSpjm()) . '</b><br/>';
                echo 'Dinas  : <b>(' . $usulanssh->getUnitId() . ') ' . UnitKerjaPeer::getStringUnitKerja($usulanssh->getUnitId()) . '</b><br/>'; 
                echo 'Nomor Surat : <b>' . UsulanSPJMPeer::getStringNomorSurat($usulanssh->getIdSpjm()) . '</b><br/>';
                echo 'File Rar Upload: <b><a href="' . UsulanDinasPeer::getStringFilepath($usulanssh->getIdSpjm()) . '">Download RAR</a></b><br/>';
                echo 'File Excel Upload : <b><a href="' . UsulanSPJMPeer::getStringFilepath($usulanssh->getIdSpjm()) . '">Download Excel</a></b>';
                ?>
            </td>
            <td><?php echo $usulanssh->getNama() ?></td>
            <td><?php echo $usulanssh->getSpec() ?></td>
            <td><?php echo $usulanssh->getHiddenSpec() ?></td>
            <td style="text-align: center"><?php echo $usulanssh->getMerek() ?></td>
            <td style="text-align: center"><?php echo $usulanssh->getRekening() ?></td>
            <td style="text-align: center"><?php echo $usulanssh->getSatuan() ?></td>
            <td style="text-align: right"><?php echo number_format($usulanssh->getHarga()) ?></td>
            <td style="text-align: center"><?php echo $usulanssh->getPajak() ?></td>
            <td>
                <?php echo 'Created : ' . date("d-m-Y H:i", strtotime($usulanssh->getCreatedAt())) . '<br/>'; ?>
                <?php echo 'Updated : ' . date("d-m-Y H:i", strtotime($usulanssh->getUpdatedAt())); ?>
            </td>
            <td style="text-align: center; font-weight: bold">
                <?php
                $namasurvey = $usulanssh->getSurveyor();
                if ($usulanssh->getStatusHapus() == t) {
                    echo '<font style="color:red">Ditolak</font>';
                } else {
                    if ($usulanssh->getStatusVerifikasi() == t) {
                        echo '<font style="color:green">Terverifikasi</font>';
                    } else {
                        echo '<font style="color:blue">Antrian</font><br/>';
                        if(!empty($namasurvey) && $usulanssh->getStatusConfirmasiPenyelia()==t && $usulanssh->getStatusSurvey()==t){
                            echo '<font style="color:blue">Posisi di Tim Data</font>';
                        } elseif(!empty($namasurvey) && $usulanssh->getStatusConfirmasiPenyelia()==t){
                            echo '<font style="color:blue">Posisi di Survey</font>';
                        } elseif($usulanssh->getStatusPending()==t){
                            echo '<font style="color:blue">Posisi Komponen Pending</font>';
                        } elseif($usulanssh->getStatusConfirmasiPenyelia()==t && empty($namasurvey)){
                            echo '<font style="color:blue">Posisi di Tim Data</font>';
                        } else{
                            echo '<font style="color:blue">Posisi di Penyelia</font>';
                        }
                    }
                }
                echo '<br/>';
                echo $usulanssh->getKomentarVerifikator();
                ?>
            </td>
        </tr>
<?php endforeach; ?>
<tr class="usulan_<?php echo $id ?>">
    <td colspan="13">&nbsp;</td>
</tr>  