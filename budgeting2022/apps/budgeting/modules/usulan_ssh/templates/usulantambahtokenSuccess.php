<?php use_helper('I18N', 'Date', 'Object', 'Javascript', 'Validation') ?>
<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Tambah Token</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Usulan SHS</a></li>
          <li class="breadcrumb-item active">Tambah Token</li>
        </ol>
      </div>
    </div>
  </div>
</section>
<!-- Main content -->
<section class="content">
    <?php include_partial('usulan_ssh/list_messages'); ?>
    <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">
                        <i class="fab fa-foursquare"></i> Form Tambah
                    </h3>
                </div>
                <div class="card-body">
                    <?php echo form_tag('usulan_ssh/savetoken', array('class' => 'form-horizontal')); ?>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                            <label>Nama Penyelia</label>
                            <select name="penyelia" class="form-control select2">
                                <?php foreach ($list_penyelia as $value) { ?>
                                    <option value="<?php echo $value->getIdPenyelia() ?>"><?php echo $value->getUsername() ?></option>
                                <?php }
                                ?>
                            </select>   
                        </div>
                      </div>
                      <!-- /.col -->
                      <div class="col-md-6">
                        <div class="form-group">
                            <label>Perangkat Daerah</label>
                            <select name="skpd" class="form-control select2">
                                <?php foreach ($list_skpd as $value) { ?>
                                    <option value="<?php echo $value->getUnitId(); ?>"><?php echo $value->getUnitName() ?></option>
                                <?php }
                                ?>
                            </select> 
                        </div>
                      </div>
                      <!-- /.col -->
                      <div class="col-md-6">
                        <div class="form-group">
                            <label>Jumlah SSH</label>
                            <input type="number" name="jumlah" value="0" required class="form-control"/>
                        </div>
                      </div>
                      <!-- /.col -->
                      <div class="col-md-6">
                        <div class="form-group">
                            <label>Nomor Surat SPJM</label>
                            <input type="text" name="nomor_surat" required class="form-control"/>
                        </div>
                      </div>
                      <!-- /.col -->
                      <div class="col-md-6">
                        <div class="form-group">
                            <label>Verifikator</label>
                            <input type="text" name="verifikator" readonly value="<?php echo $sf_user->getNamaUser() ?>" class="form-control" />
                        </div>
                      </div>
                      <!-- /.col -->
                      <div class="col-md-2">
                        <div class="form-group">
                            <label class="tombol_filter">Tombol Filter</label><br/>
                            <button type="submit" name="commit" class="btn btn-outline-primary btn-sm">Simpan <i class="far fa-save"></i></button>
                            <button type="reset" name="reset" class="btn btn-outline-danger btn-sm">Reset <i class="fa fa-backspace"></i></button>
                        </div>
                        <!-- /.form-group -->
                      </div>
                      <!-- /.col -->                      
                    </div>
                    <?php echo '</form>'; ?>
                </div>
            </div>
          </div>
        </div>
    </div>
</section>
