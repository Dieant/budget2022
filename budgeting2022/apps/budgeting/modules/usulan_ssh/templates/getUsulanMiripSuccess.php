<?php use_helper('Url', 'Javascript', 'Form', 'Object'); ?>
<?php
$i = 0;
?>
<tr class="mirip_<?php echo $id ?>" style="text-align: center; font-weight: bold">
    <th>Tipe</th>
    <th>Kode Barang</th>
    <th>Nama</th>
    <th>Spesifikasi</th>
    <th>Hidden Spec</th>
    <th>Merek</th>
    <th>Rekening</th>
    <th>Satuan</th>
    <th>Harga</th>
    <th>Pajak</th>
    <th>Status</th>
    <th>Keterangan</th>
    <th>Action</th>
</tr>
<?php
foreach ($list_usulan_ssh_mirip as $usulan) {

    if ($status_hapus == false) {
        ?>
        <tr class="mirip_<?php echo $id ?>" style="text-align: center">
            <td><?php echo $usulan['tipe_usulan'] ?></td>
            <td><?php echo $usulan['kode_barang'] ?></td>
            <td><?php echo $usulan['nama'] ?></td>
            <td><?php echo $usulan['spec'] ?></td>
            <td><?php echo $usulan['hidden_spec'] ?></td>
            <td><?php echo $usulan['merek'] ?></td>
            <td><?php echo $usulan['rekening'] ?></td>
            <td><?php echo $usulan['satuan'] ?></td>
            <td style="text-align: right"><?php echo number_format($usulan['harga']) ?></td>
            <td><?php echo $usulan['pajak'] ?></td>
            <td style="font-weight: bold">
                <?php
                echo '(Edit terakhir : ' . date("d-m-Y H:i", strtotime($usulan['updated_at'])) . ')<br/>';
                if ($usulan['status_verifikasi'] == t && $usulan['status_hapus'] == f) {
                    echo '<font style="color:green">Terverifikasi</font>';
                } else if ($usulan['status_verifikasi'] == f && $usulan['status_hapus'] == f){
                    echo '<font style="color:blue">Antrian</font>';
                }else if ($usulan['status_hapus'] == t){
                    echo '<font style="color:red">Ditolak</font>';
                }
                ?>
            </td>
            <td><?php echo $usulan['keterangan'] ?></td>
            <td>ACTION</td>
        </tr>  
        <?php
    }
}
?>
<tr class="mirip_<?php echo $id ?>">
    <td colspan="13">&nbsp;</td>
</tr>  