<?php

/**
 * anggaran actions.
 *
 * @package    budgeting
 * @subpackage anggaran
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 2288 2006-10-02 15:22:13Z fabien $
 */
class anggaranActions extends autoanggaranActions {

    public function executeVerifikasiKegiatan() {
        $unit_id = $this->getRequestParameter('unit_id');
        $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
        $tahap = DinasMasterKegiatanPeer::getTahapKegiatan($unit_id, $kode_kegiatan);

        $c = new Criteria();
        $c->add(RincianBappekoPeer::UNIT_ID, $unit_id);
        $c->add(RincianBappekoPeer::KODE_KEGIATAN, $kode_kegiatan);
        $c->add(RincianBappekoPeer::TAHAP, $tahap);
        if ($rs_rincian_bappeko = RincianBappekoPeer::doSelectOne($c)) {
            $this->rs_rincian_bappeko = $rs_rincian_bappeko;
            if ($this->getRequest()->getMethod() == sfRequest::POST && $this->getRequestParameter('simpan')) {
                $buka = $this->getRequestParameter('buka');
                if ($buka == 'setuju') {
                    $c_kegiatan = new Criteria();
                    $c_kegiatan->add(DinasRincianPeer::UNIT_ID, $unit_id);
                    $c_kegiatan->add(DinasRincianPeer::KEGIATAN_CODE, $kode_kegiatan);
                    if ($rs_rincian = DinasRincianPeer::doSelectOne($c_kegiatan)) {
                        $rs_rincian->setRincianLevel(2);
                        $rs_rincian->save();
                    }
                    $rs_rincian_bappeko->setStatusBuka(TRUE);
                    $this->setFlash('berhasil', 'Kegiatan telah dibuka ke Dinas');
                } elseif ($buka == 'tolak') {
                    $c_kegiatan = new Criteria();
                    $c_kegiatan->add(DinasRincianPeer::UNIT_ID, $unit_id);
                    $c_kegiatan->add(DinasRincianPeer::KEGIATAN_CODE, $kode_kegiatan);
                    if ($rs_rincian = DinasRincianPeer::doSelectOne($c_kegiatan)) {
                        $rs_rincian->setRincianLevel(3);
                        $rs_rincian->save();
                    }
                    $catatan = $this->getRequestParameter('catatan');
                    $rs_rincian_bappeko->setCatatanBappeko($catatan);
                    $rs_rincian_bappeko->setStatusBuka(FALSE);
                    $this->setFlash('berhasil', 'Kegiatan tetap dikunci');
                }
                $rs_rincian_bappeko->save();
                $this->redirect('anggaran/listRevisi');
            }
        } else {
            $this->setFlash('gagal', 'Tidak ada jawaban dinas');
            $this->redirect('anggaran/listRevisi');
        }
    }

    public function executeEditHeader() {
        $unit_id = $this->getRequestParameter('unit_id');
        $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');

        $c = new Criteria();
        $c->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
        $c->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
        if ($dapat_kegiatan = DinasMasterKegiatanPeer::doSelectOne($c)) {
            $this->dapat_kegiatan = $dapat_kegiatan;

            if ($this->getRequest()->getMethod() == sfRequest::POST && $this->getRequestParameter('simpan')) {
                try {
                    $lokasi = $this->getRequestParameter('lokasi');
                    $kelompok_sasaran = $this->getRequestParameter('kelompok_sasaran');

                    $dapat_kegiatan->setLokasi($lokasi);
                    $dapat_kegiatan->setKelompokSasaran($kelompok_sasaran);
                    $dapat_kegiatan->save();

//update lokasi dan sasaran di RKA juga
                    $c = new Criteria();
                    $c->add(MasterKegiatanPeer::UNIT_ID, $unit_id);
                    $c->add(MasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                    if ($dapat_kegiatan2 = MasterKegiatanPeer::doSelectOne($c)) {
                        $dapat_kegiatan2->setLokasi($lokasi);
                        $dapat_kegiatan2->setKelompokSasaran($kelompok_sasaran);
                        $dapat_kegiatan2->save();
                    }

                    $this->setFlash('berhasil', 'Data telah tersimpan');
                    $this->redirect('anggaran/editRevisi?unit_id=' . $unit_id . '&kode_kegiatan=' . $kode_kegiatan);
                } catch (Exception $ex) {
                    $this->setFlash('gagal', 'Gagal tersimpan');
                    $this->redirect('anggaran/editRevisi?unit_id=' . $unit_id . '&kode_kegiatan=' . $kode_kegiatan);
                }
            }
        }
    }

     public function executePenandaCovid() {
        if ($this->getRequestParameter('unit_id') ) {
            $unit_id = $this->getRequestParameter('unit_id');
            $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');

            $c = new Criteria();
            $c->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
            $c->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
            $rs_kegiatan = DinasMasterKegiatanPeer::doSelectOne($c);
            $this->rs_kegiatan = $rs_kegiatan;

            $komponen_terambil = array();
            $c = new Criteria();
            $c->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
            $c->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
            $c->add(DinasRincianDetailPeer::STATUS_HAPUS, FALSE);
            $rs_budgeting = DinasRincianDetailPeer::doSelect($c);
            foreach ($rs_budgeting as $key => $budgeting) {
                $komponen = $budgeting->getKomponenId();
                $subtitle = $budgeting->getSubtitle();
                $komponen_terambil["$komponen"] = 1;
                $komponen_terambil["$subtitle"] = 1;
            }
            $this->komponen_terambil = $komponen_terambil;

            $query =
            "SELECT rd.unit_id,rd.kegiatan_code,rd.komponen_name ,rd.is_covid, rd.volume, rd.satuan, rd.detail_name, rd.detail_kegiatan,rd.keterangan_koefisien, rd.tahap,rd.komponen_harga, rd.nilai_anggaran, rd.volume_orang, rd.volume_orang as vol_orang, rd.detail_no,case when rd.is_covid = true then 'Covid-19' else '' end as sumber_dana
            from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail rd
            WHERE rd.status_hapus=false   
            AND rd.volume > 0              
            AND rd.unit_id = '$unit_id'
            AND rd.kegiatan_code = '$kode_kegiatan'           
            ORDER BY rd.komponen_name";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs_rinciandetail = $stmt->executeQuery();
            $rs_copy = $stmt->executeQuery();
            $this->rs_rinciandetail = $rs_copy;

            $this->unit_id = $unit_id;
            $this->kode_kegiatan = $kode_kegiatan;
        }
    }

 public function executeProsesUbahPenandaCovid() {
        $detail_no = $this->getRequestParameter('detail_no');
        $penanda = $this->getRequestParameter('sumber_dana');
        //$realisasi = $this->getRequestParameter('nilairealisasi');
        $unit_id = $this->getRequestParameter('unit_id');
        $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
      
        try {
            $con = Propel::getConnection();
            $con->begin();            
            foreach( $detail_no as $key => $no ) {             
               
                    if(!empty($penanda[$key])) {
                    $tahap=sfConfig::get('app_tahap_detail');
                    $detail=$unit_id.'.'.$kode_kegiatan.'.'.$penanda[$key]; 
                   // die($penanda[$key]." ".$key."".$tahap." ". $detail);    

                     $level = "select max(status_level) as status_level from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail                                     
                        WHERE kegiatan_code='$kode_kegiatan' and status_hapus=false";
                        $stmt1 = $con->prepareStatement($level);
                        $rs_cek = $stmt1->executeQuery();
                        if ($rs_cek->next()) {
                         $status_level = $rs_cek->getString('status_level');
                        }

                      $note = "select note_skpd from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail                                     
                        WHERE detail_kegiatan='$detail'";
                        $stmt2 = $con->prepareStatement($note);
                        $rs_cek2 = $stmt2->executeQuery();
                        if ($rs_cek2->next()) {
                         $note_skpd = $rs_cek2->getString('note_skpd');
                        }

                    $status = "select lock from " . sfConfig::get('app_default_schema') . ".dinas_rincian                                     
                        WHERE kegiatan_code='$kode_kegiatan' and unit_id='$unit_id'";
                        $stmt3 = $con->prepareStatement($status);
                        $rs_cek3 = $stmt3->executeQuery();
                        if ($rs_cek3->next()) {
                         $status_revisi = $rs_cek3->getString('lock');                        
                        }

                    $detail_name = "select detail_name from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail                                     
                        WHERE kegiatan_code='$kode_kegiatan' and status_hapus=false and detail_kegiatan='$detail'";
                        $stmt4 = $con->prepareStatement($detail_name);
                        $rs_cek4 = $stmt4->executeQuery();
                        if ($rs_cek4->next()) {
                         $detail_names = $rs_cek4->getString('detail_name');
                        }

                    $detail_name=$detail_names.' ';                        


                    if ($this->getRequestParameter('proses'))
                    {                       
                         
                        if($note_skpd <> '')
                        {
                            $note=$note_skpd;
                        }
                        else
                        {
                           //die($status_revisi);
                            $this->setFlash('gagal', 'Gagal Komponen belum diberi catatan PD');
                            return $this->redirect('anggaran/penandaCovid?unit_id=' . $unit_id . '&kode_kegiatan=' . $kode_kegiatan);
                        }                            

                        if($status_revisi == 'f') //false kegiatan dibuka *sedang revisi
                        {
                            $query = "UPDATE " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail
                            set is_covid = true , note_skpd= '$note' ,tahap=$tahap, detail_name='$detail_name',status_level=$status_level    WHERE detail_kegiatan='$detail'";
                            $stmt = $con->prepareStatement($query);
                            $stmt->executeQuery();

                            budgetLogger::log('Menandai Komponen Covid 19 pada kode sub kegiatan ' . $kode_kegiatan . ' unit_id : '.$unit_id.' untuk detail kegiatan '. $detail);
                        }
                        else if($status_revisi == 't' )
                        {
                            //die($status_revisi);
                            $this->setFlash('gagal', 'Gagal Kegiatan tidak sedang revisi');
                            return $this->redirect('anggaran/penandaCovid?unit_id=' . $unit_id . '&kode_kegiatan=' . $kode_kegiatan);
                        }


                    }    
                    else if ($this->getRequestParameter('hapus'))
                    {
                        if($note_skpd <> '')
                        {
                            $note=$note_skpd;
                        }
                        else
                        {
                           //die($status_revisi);
                            $this->setFlash('gagal', 'Gagal Komponen belum diberi catatan PD');
                            return $this->redirect('anggaran/penandaCovid?unit_id=' . $unit_id . '&kode_kegiatan=' . $kode_kegiatan);
                        }      

                        if($status_revisi == 'f') //false kegiatan dibuka *sedang revisi
                        {
                        $query = "UPDATE " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail
                        set is_covid = false, note_skpd= '$note',tahap=$tahap,detail_name='$detail_name',status_level=$status_level            
                        WHERE detail_kegiatan='$detail'";
                        $stmt = $con->prepareStatement($query);
                        $stmt->executeQuery();

                        budgetLogger::log('Menghapus Penanda Komponen Covid 19 pada kode sub kegiatan ' . $kode_kegiatan . ' unit_id : '.$unit_id.' untuk detail kegiatan '. $detail);
                        }
                        else if($status_revisi == 't')
                        {
                           $this->setFlash('gagal', 'Gagal Kegiatan tidak sedang revisi');
                            return $this->redirect('anggaran/penandaCovid?unit_id=' . $unit_id . '&kode_kegiatan=' . $kode_kegiatan);   
                        }

                    } 
                }
                else if ( $realisasi <> 0)
                {
                     $this->setFlash('gagal', 'Gagal Sudah Ada Realisasi');
                     return $this->redirect('anggaran/penandaCovid?unit_id=' . $unit_id . '&kode_kegiatan=' . $kode_kegiatan);

                }
            }
            //die($query);
        } catch(Exception $e) {
            $con->rollback();
            $this->setFlash('gagal', 'Gagal : ' . $e->getMessage());
            return $this->redirect('anggaran/penandaCovid?unit_id=' . $unit_id . '&kode_kegiatan=' . $kode_kegiatan);
        }

        $con->commit();

        $this->setFlash('berhasil', 'Berhasil Ubah Penanda Covid 19.');
        return $this->redirect('anggaran/penandaCovid?unit_id=' . $unit_id . '&kode_kegiatan=' . $kode_kegiatan);
    }
    
    public function executePenandaBtl() {
        if ($this->getRequestParameter('unit_id') ) {
            $unit_id = $this->getRequestParameter('unit_id');
            $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');

            $c = new Criteria();
            $c->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
            $c->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
            $rs_kegiatan = DinasMasterKegiatanPeer::doSelectOne($c);
            $this->rs_kegiatan = $rs_kegiatan;

            $komponen_terambil = array();
            $c = new Criteria();
            $c->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
            $c->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
            $c->add(DinasRincianDetailPeer::STATUS_HAPUS, FALSE);
            $rs_budgeting = DinasRincianDetailPeer::doSelect($c);
            foreach ($rs_budgeting as $key => $budgeting) {
                $komponen = $budgeting->getKomponenId();
                $subtitle = $budgeting->getSubtitle();
                $komponen_terambil["$komponen"] = 1;
                $komponen_terambil["$subtitle"] = 1;
            }
            $this->komponen_terambil = $komponen_terambil;

            // $query =
            // "SELECT rd.unit_id,rd.kegiatan_code,rd.is_btl,rd.komponen_name , rd.volume, rd.satuan, rd.detail_name, rd.detail_kegiatan,rd.keterangan_koefisien, rd.tahap,rd.komponen_harga, rd.nilai_anggaran, rd.volume_orang, r.volume_orang as vol_orang, rd.detail_no 
            // from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail rd," . sfConfig::get('app_default_schema') . ".rincian_detail r
            // WHERE rd.status_hapus=false
            // AND rd.unit_id=r.unit_id
            // AND rd.kegiatan_code=r.kegiatan_code
            // AND rd.detail_kegiatan = r.detail_kegiatan
            // AND (rd.is_btl is null or rd.is_btl <> true)
            // AND rd.note_skpd is not null
            // AND rd.unit_id = '$unit_id'
            // AND rd.kegiatan_code = '$kode_kegiatan'           
            // ORDER BY rd.komponen_name";
            //AND (rd.is_did is null or rd.is_did=false)
            $query =
             "SELECT rd.unit_id,rd.kegiatan_code,rd.komponen_name , rd.volume, rd.satuan, rd.detail_name, rd.detail_kegiatan,rd.keterangan_koefisien, rd.tahap,rd.komponen_harga, rd.nilai_anggaran, rd.volume_orang, rd.rekening_code,rd.volume_orang as vol_orang, rd.detail_no,case when rd.sumber_dana_id = 11 then 'APBD' when rd.sumber_dana_id = 1 then 'DAK'
                when rd.sumber_dana_id = 2 then 'DBHCHT' when rd.sumber_dana_id = 3 then 'DID' 
                when rd.sumber_dana_id = 4 then 'DBH' when rd.sumber_dana_id = 5 then 'Pajak Rokok'
                when rd.sumber_dana_id = 6 then 'BLUD' when rd.sumber_dana_id = 7 then 'Bantuan Keuangan Provinsi'
                when rd.sumber_dana_id = 8 then 'Kapitasi JKN' when rd.sumber_dana_id = 9 then 'BOS' 
                when rd.sumber_dana_id = 10 then 'DAU' end as sumber_dana
            from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail rd
            WHERE rd.status_hapus=false     
            AND rd.volume > 0                 
            AND rd.unit_id = '$unit_id'
            AND rd.kegiatan_code = '$kode_kegiatan'           
            ORDER BY rd.komponen_name";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs_rinciandetail = $stmt->executeQuery();
            $rs_copy = $stmt->executeQuery();
            $this->rs_rinciandetail = $rs_copy;

            $this->unit_id = $unit_id;
            $this->kode_kegiatan = $kode_kegiatan;
        }
    }

    public function executeProsesUbahPenandaBtl() {
        $detail_no = $this->getRequestParameter('detail_no');
        $penanda = $this->getRequestParameter('sumber_dana');
        //$realisasi = $this->getRequestParameter('nilairealisasi');
        $unit_id = $this->getRequestParameter('unit_id');
        $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
      
        try {
            $con = Propel::getConnection();
            $con->begin();            
            foreach( $detail_no as $key => $no ) {             
               
                if(!empty($penanda[$key])) {

                    $tahap=sfConfig::get('app_tahap_detail');
                    $detail=$unit_id.'.'.$kode_kegiatan.'.'.$no;     
                    //die($penanda[$key]." ".$no."".$tahap." ". $detail);                                

                    $query =
                    "UPDATE " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail
                    set sumber_dana_id = $penanda[$key]
                    WHERE detail_kegiatan='$detail'";
                    $stmt = $con->prepareStatement($query);
                    $stmt->executeQuery();

                    $queryrka = "UPDATE " . sfConfig::get('app_default_schema') . ".rincian_detail
                    set sumber_dana_id = $penanda[$key]
                    WHERE detail_kegiatan='$detail'";
                    $stmt1 = $con->prepareStatement($queryrka);
                    $stmt1->executeQuery();

                    $querybp = "UPDATE " . sfConfig::get('app_default_schema') . ".rincian_detail_bp
                    set sumber_dana_id = $penanda[$key]
                    WHERE detail_kegiatan='$detail'";
                    $stmt2 = $con->prepareStatement($querybp);
                    $stmt2->executeQuery();

                    historyUserLog::update_sumber_dana($unit_id, $kode_kegiatan, $detail, $penanda[$key]);
                    budgetLogger::log('Update Sumber dana id pada kode sub kegiatan ' . $kode_kegiatan . ' unit_id : '.$unit_id.' untuk detail kegiatan '. $detail.'dengan sumber dana id baru : '. $penanda[$key]);
                              
                            
                }
                else if ( $realisasi <> 0)
                {
                     $this->setFlash('gagal', 'Gagal Sudah Ada Realisasi');
                     return $this->redirect('anggaran/penandaBtl?unit_id=' . $unit_id . '&kode_kegiatan=' . $kode_kegiatan);

                }
            }
            //die($query);
        } catch(Exception $e) {
            $con->rollback();
            $this->setFlash('gagal', 'Gagal : ' . $e->getMessage());
            return $this->redirect('anggaran/penandaBtl?unit_id=' . $unit_id . '&kode_kegiatan=' . $kode_kegiatan);
        }

        $con->commit();

        $this->setFlash('berhasil', 'Berhasil Ubah Penanda Sumber Dana.');
        return $this->redirect('anggaran/penandaBtl?unit_id=' . $unit_id . '&kode_kegiatan=' . $kode_kegiatan);
    }

    public function executeKegiatanBtl() {
        $this->unit_id = $unit_id = $this->getRequestParameter('unit_id');
        $this->kode_kegiatan_asal = $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');

        $this->tahap = $tahap = DinasMasterKegiatanPeer::getTahapKegiatan($unit_id, $kode_kegiatan);

        $con = Propel::getConnection();
        $query = " select distinct unit_id, kode_kegiatan, nama_kegiatan, catatan "
                . " from " . sfConfig::get('app_default_schema') . ".kegiatan_btl "
                . " where unit_id='$unit_id' and tahap=$tahap ";
        $stmt = $con->prepareStatement($query);
        $this->rs_kegiatan = $stmt->executeQuery();
    }

    public function executeEditKegiatanBtl() {
        $this->unit_id = $unit_id = $this->getRequestParameter('unit_id');
        $this->tahap = $tahap = $this->getRequestParameter('tahap');
        $this->kode_kegiatan_asal = $kode_kegiatan_asal = $this->getRequestParameter('kode_kegiatan_asal');

        if ($this->getRequest()->getMethod() == sfRequest::POST && $this->getRequestParameter('simpan')) {
//simpan
            $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
            $nama_kegiatan = $this->getRequestParameter('nama_kegiatan');
            $catatan = $this->getRequestParameter('catatan');
            $kode_rekening = $this->getRequestParameter('kode_rekening');
            $nama_rekening = $this->getRequestParameter('nama_rekening');
            $semula = $this->getRequestParameter('semula');
            $menjadi = $this->getRequestParameter('menjadi');

            $total_semula = 0;
            $total_menjadi = 0;

            $con = Propel::getConnection();
            $con->begin();
            try {
                if ($this->getRequestParameter('kode_kegiatan_lama')) {
//update
                    $kode_kegiatan_lama = $this->getRequestParameter('kode_kegiatan_lama');
                    $query = " delete from " . sfConfig::get('app_default_schema') . ".kegiatan_btl "
                            . " where unit_id='$unit_id' and kode_kegiatan='$kode_kegiatan_lama' and tahap=$tahap ";
                    $stmt = $con->prepareStatement($query);
                    $stmt->executeQuery();
                }
//insert
                for ($i = 0; $i < count($kode_rekening); $i++) {
                    $total_semula += $semula[$i];
                    $total_menjadi += $menjadi[$i];
                    if ($kode_rekening[$i] <> '') {
                        $new_kegiatan = new KegiatanBtl();
                        $new_kegiatan->setUnitId($unit_id);
                        $new_kegiatan->setKodeKegiatan($kode_kegiatan);
                        $new_kegiatan->setNamaKegiatan($nama_kegiatan);
                        $new_kegiatan->setCatatan($catatan);
                        $new_kegiatan->setRekeningCode($kode_rekening[$i]);
                        $new_kegiatan->setRekeningName($nama_rekening[$i]);
                        $new_kegiatan->setSemula($semula[$i]);
                        $new_kegiatan->setMenjadi($menjadi[$i]);
                        $new_kegiatan->setTahap($tahap);
                        $new_kegiatan->save();
                    }
                }
                if ($total_semula <> $total_menjadi) {
                    $con->rollback();
                    $this->setFlash('gagal', 'Gagal dirubah karena nilai rekening tidak balance');
                } else {
                    $con->commit();
                    $this->setFlash('berhasil', 'Data telah tersimpan');
                }
            } catch (Exception $ex) {
                $con->rollback();
                $this->setFlash('gagal', 'Gagal dirubah');
            }
            $this->redirect('anggaran/kegiatanBtl?unit_id=' . $unit_id . '&kode_kegiatan=' . $kode_kegiatan_asal);
        } elseif ($this->getRequestParameter('kode_kegiatan')) {
//data untuk edit kegiatan
            $this->kode_kegiatan = $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');

            $c = new Criteria();
            $c->add(KegiatanBtlPeer::UNIT_ID, $unit_id);
            $c->add(KegiatanBtlPeer::KODE_KEGIATAN, $kode_kegiatan);
            $c->add(KegiatanBtlPeer::TAHAP, $tahap);
            $this->rs_rekening = KegiatanBtlPeer::doSelect($c);
            $rs = KegiatanBtlPeer::doSelectOne($c);
            $this->kode_kegiatan = $rs->getKodeKegiatan();
            $this->nama_kegiatan = $rs->getNamaKegiatan();
            $this->catatan = $rs->getCatatan();
        }
    }

    public function executeUbahPass() {
        if ($this->getRequest()->getMethod() == sfRequest::POST) {
            if ($this->getRequestParameter('pass_baru') == $this->getRequestParameter('ulang_pass_baru')) {
                $member = MasterUserV2Peer::retrieveByPK($this->getUser()->getNamaUser());

                $member->setUserPassword(md5($this->getRequestParameter('pass_baru')));
                $member->save();
                $this->setFlash('berhasil', 'Password berhasil terganti.');

                $this->habis_ganti = true;
            } else {
                $this->setFlash('gagal', 'Password tidak sama.');
            }
        }
    }

    public function executeGetHeader() {
        $this->tahap = $tahap = $this->getRequestParameter('tahap');
        if ($tahap == 'pakbp') {
            $c = new Criteria();
            $c->add(PakBukuPutihMasterKegiatanPeer::KODE_KEGIATAN, $this->getRequestParameter('kegiatan'));
            $c->add(PakBukuPutihMasterKegiatanPeer::UNIT_ID, $this->getRequestParameter('unit'));
            $master_kegiatan = PakBukuPutihMasterKegiatanPeer::doSelectOne($c);
        } elseif ($tahap == 'pakbb') {
            $c = new Criteria();
            $c->add(PakBukuBiruMasterKegiatanPeer::KODE_KEGIATAN, $this->getRequestParameter('kegiatan'));
            $c->add(PakBukuBiruMasterKegiatanPeer::UNIT_ID, $this->getRequestParameter('unit'));
            $master_kegiatan = PakBukuBiruMasterKegiatanPeer::doSelectOne($c);
        } elseif ($tahap == 'murni') {
            $c = new Criteria();
            $c->add(MurniMasterKegiatanPeer::KODE_KEGIATAN, $this->getRequestParameter('kegiatan'));
            $c->add(MurniMasterKegiatanPeer::UNIT_ID, $this->getRequestParameter('unit'));
            $master_kegiatan = MurniMasterKegiatanPeer::doSelectOne($c);
        } elseif ($tahap == 'murnibp') {
            $c = new Criteria();
            $c->add(MurniBukuPutihMasterKegiatanPeer::KODE_KEGIATAN, $this->getRequestParameter('kegiatan'));
            $c->add(MurniBukuPutihMasterKegiatanPeer::UNIT_ID, $this->getRequestParameter('unit'));
            $master_kegiatan = MurniBukuPutihMasterKegiatanPeer::doSelectOne($c);
        } elseif ($tahap == 'murnibb') {
            $c = new Criteria();
            $c->add(MurniBukuBiruMasterKegiatanPeer::KODE_KEGIATAN, $this->getRequestParameter('kegiatan'));
            $c->add(MurniBukuBiruMasterKegiatanPeer::UNIT_ID, $this->getRequestParameter('unit'));
            $master_kegiatan = MurniBukuBiruMasterKegiatanPeer::doSelectOne($c);
        } elseif ($tahap == 'revisi1') {
            $c = new Criteria();
            $c->add(Revisi1MasterKegiatanPeer::KODE_KEGIATAN, $this->getRequestParameter('kegiatan'));
            $c->add(Revisi1MasterKegiatanPeer::UNIT_ID, $this->getRequestParameter('unit'));
            $master_kegiatan = Revisi1MasterKegiatanPeer::doSelectOne($c);
        } elseif ($tahap == 'rkua') {
            $c = new Criteria();
            $c->add(RkuaMasterKegiatanPeer::KODE_KEGIATAN, $this->getRequestParameter('kegiatan'));
            $c->add(RkuaMasterKegiatanPeer::UNIT_ID, $this->getRequestParameter('unit'));
            $master_kegiatan = RkuaMasterKegiatanPeer::doSelectOne($c);
        } else {
            $c = new Criteria();
            $c->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $this->getRequestParameter('kegiatan'));
            $c->add(DinasMasterKegiatanPeer::UNIT_ID, $this->getRequestParameter('unit'));
            $master_kegiatan = DinasMasterKegiatanPeer::doSelectOne($c);
        }

        if ($master_kegiatan) {
            $this->master_kegiatan = $master_kegiatan;
        }
        $this->setLayout('kosong');
    }

    public function executeRequestlist() {
        $this->processSort();
        $this->processFilters();
        $this->filters = $this->getUser()->getAttributeHolder()->getAll('sf_admin/master_kegiatan/filters');

        $pagers = new sfPropelPager('LogRequestPenyelia', 20);
        $c = new Criteria();
        $this->addSortCriteria($c);
        $user_id = $this->getUser()->getNamaUser();
        $c1 = new Criteria();
        $c1->add(SchemaAksesV2Peer::USER_ID, $user_id);
        $c1->add(SchemaAksesV2Peer::SCHEMA_ID, 2);
        $level = SchemaAksesV2Peer::doSelectOne($c1);
        if ($level->getLevelId() == 16) {
            $c = new Criteria();
            $c->add(UserHandleV2Peer::USER_ID, $user_id);
            $c->add(UserHandleV2Peer::SCHEMA_ID, 2);
            $es = UserHandleV2Peer::doSelect($c);
            $unit_kerja = Array();
            foreach ($es as $x) {
                $unit_kerja[] = $x->getUnitId();
            }
        } else {
            $es = UnitKerjaPeer::doSelect(new Criteria);
            $unit_kerja = Array();
            foreach ($es as $x) {
                $unit_kerja[] = $x->getUnitId();
            }
        }
        $this->unit_kerja = $unit_kerja;
        $c->add(LogRequestPenyeliaPeer::STATUS, 4, Criteria::NOT_EQUAL);
        $c->add(LogRequestPenyeliaPeer::UNIT_ID, $unit_kerja[0]);
        for ($i = 1; $i < count($unit_kerja); $i++) {
            $c->addOr(LogRequestPenyeliaPeer::UNIT_ID, $unit_kerja[$i]);
        }
//$c->addDescendingOrderByColumn(LogRequestPenyeliaPeer::STATUS);
        $c->addDescendingOrderByColumn(LogRequestPenyeliaPeer::UPDATED_AT);
//$c->addAscendingOrderByColumn(LogRequestPenyeliaPeer::UNIT_ID);
        $this->addFiltersCriteriaRequestlist($c);

        $pagers->setCriteria($c);
        $pagers->setPage($this->getRequestParameter('page', 1));
        $pagers->init();

        $this->pager = $pagers;
    }

    protected function addFiltersCriteriaRequestlist($c) {
        if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
            $unit = $this->filters['unit_id'];
            $c->add(LogRequestPenyeliaPeer::UNIT_ID, $unit);
        }
        if (isset($this->filters['tipe']) && $this->filters['tipe'] !== '') {
            $c->add(LogRequestPenyeliaPeer::TIPE, $this->filters['tipe']);
        }
    }

//tiket #59
//11 Maret 2016 - proses ke penyelia 1
    public function executeProsesapprovetapd() {
        $tipeApproval = $this->getUser()->getNamaLogin();
        // $catatan = '';
        // $catatan_bpkpd = '';
        // $catatan_bagian_hukum = '';
        // $catatan_inspektorat = '';
        // $catatan_badan_kepegawaian = '';
        // $catatan_lppa = '';

        if($tipeApproval == 'anggaran') {
            $catatan = trim($this->getRequestParameter('catatan'));
            $catatan = str_replace("'", "''", $catatan);
            $catatan_bpkpd = trim($this->getRequestParameter('catatan_bpkpd'));
            $catatan_bpkpd = str_replace("'", "''", $catatan_bpkpd);
        } else if($tipeApproval == 'bagian_hukum') {
            $catatan_bagian_hukum = trim($this->getRequestParameter('catatan_bagian_hukum'));
            $catatan_bagian_hukum = str_replace("'", "''", $catatan_bagian_hukum);
        } else if($tipeApproval == 'inspektorat') {
            $catatan_inspektorat = trim($this->getRequestParameter('catatan_inspektorat'));
            $catatan_inspektorat = str_replace("'", "''", $catatan_inspektorat);
        } else if($tipeApproval == 'bkd' || $tipeApproval == 'bkpsdm') {
            $catatan_badan_kepegawaian = trim($this->getRequestParameter('catatan_badan_kepegawaian'));
            $catatan_badan_kepegawaian = str_replace("'", "''", $catatan_badan_kepegawaian);
        } else if($tipeApproval == 'lppa' || $tipeApproval == 'bapenda') {
            $catatan_lppa = trim($this->getRequestParameter('catatan_lppa'));
            $catatan_lppa = str_replace("'", "''", $catatan_lppa);
        } else if($tipeApproval == 'bagian_organisasi') {
            $catatan_bagian_organisasi = trim($this->getRequestParameter('catatan_bagian_organisasi'));
            $catatan_bagian_organisasi = str_replace("'", "''", $catatan_bagian_organisasi);
        }
        $unit_id = $this->getRequestParameter('unit_id');
        $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
        $pilihan = $this->getRequestParameter('pilihaction');
        // jawaban verifikasi
        $verifikasi = $this->getRequestParameter('jawaban_verifikasi');
        if(empty($verifikasi)) {
            if($tipeApproval == 'anggaran')
                $verifikasi = $catatan_bpkpd;
            else if($tipeApproval == 'bagian_hukum')
                $verifikasi = $catatan_bagian_hukum;
            else if($tipeApproval == 'inspektorat')
                $verifikasi = $catatan_inspektorat;
            else if($tipeApproval == 'bkd' || $tipeApproval == 'bkpsdm')
                $verifikasi = $catatan_badan_kepegawaian;
            else if($tipeApproval == 'lppa' || $tipeApproval == 'bapenda')
                $verifikasi = $catatan_lppa;
            else if($tipeApproval == 'bagian_organisasi')
                $verifikasi = $catatan_bagian_organisasi;
        }

        $status_bypass_validasi = FALSE;

        // biar gabisa lolos kalo catatannya kurang/tidak diisi
        if ($tipeApproval == 'anggaran' && strlen(strip_tags($catatan)) < 20) {
            $this->setFlash('gagal', 'Catatan pembahasan harus berisi minimal 20 karakter');
            return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
        } 

        // else if($tipeApproval == 'anggaran' && strlen(strip_tags($catatan_bpkpd)) < 15) {
        //     $this->setFlash('gagal', 'Catatan BPKPD harus berisi minimal 15 karakter');
        //     return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
        // } else if($tipeApproval == 'bagian_hukum' && strlen(strip_tags($catatan_bagian_hukum)) < 15) {
        //     $this->setFlash('gagal', 'Catatan Bagian Hukum harus berisi minimal 15 karakter');
        //     return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
        // } else if($tipeApproval == 'inspektorat' && strlen(strip_tags($catatan_inspektorat)) < 15) {
        //     $this->setFlash('gagal', 'Catatan Inspektorat harus berisi minimal 15 karakter');
        //     return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
        // } else if($tipeApproval == 'bkd' && strlen(strip_tags($catatan_badan_kepegawaian)) < 15) {
        //     $this->setFlash('gagal', 'Catatan BKD harus berisi minimal 15 karakter');
        //     return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
        // } else if($tipeApproval == 'lppa' && strlen(strip_tags($catatan_lppa)) < 15) {
        //     $this->setFlash('gagal', 'Catatan LP2A harus berisi minimal 15 karakter');
        //     return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
        // } else if($tipeApproval == 'lppa' && strlen(strip_tags($catatan_lppa)) < 15) {
        //     $this->setFlash('gagal', 'Catatan LP2A harus berisi minimal 15 karakter');
        //     return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
        // } else if($tipeApproval == 'bagian_organisasi' && strlen(strip_tags($catatan_bagian_organisasi)) < 15) {
        //     $this->setFlash('gagal', 'Catatan Bagian Organisasi harus berisi minimal 15 karakter');
        //     return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
        // }

        if(sfConfig::get('app_tahap_edit') == 'murni') {
            $status_bypass_validasi = TRUE;
        }
        // SEMENTARA SAJA, INGAT DICOMMENT KEMBALI!!!!!
        // DINAS PU, MAU GANTI REKENING Biaya Counter Mesin Fotocopy DAN Biaya Counter Mesin Fotocopy Warna DARI (5.2.2.20.06 Belanja Pemeliharaan Alat Kantor dan Rumah Tangga) KE (   5.2.2.10.01 Belanja Sewa Perlengkapan / Peralatan Kantor)
        if (in_array($kode_kegiatan, array(''))) {
            $status_bypass_validasi = TRUE;
        }
        // cuma simpan catatan
        if ($unit_id != '' && $kode_kegiatan != '' && $this->getRequestParameter('simpancatatan')) {
            $c_kegiatan = new Criteria();
            $c_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
            $c_kegiatan->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
            $kegiatan = DinasMasterKegiatanPeer::doSelectOne($c_kegiatan);
            if($tipeApproval == 'anggaran') {
                $kegiatan->setCatatanPembahasan($catatan);
                $kegiatan->setCatatanBpkpd($catatan_bpkpd);
            } else if($tipeApproval == 'bagian_hukum') {
                $kegiatan->setCatatanBagianHukum($catatan_bagian_hukum);
            } else if($tipeApproval == 'inspektorat') {
                $kegiatan->setCatatanInspektorat($catatan_inspektorat);
            } else if($tipeApproval == 'bkd' || $tipeApproval == 'bkpsdm') {
                $kegiatan->setCatatanBadanKepegawaian($catatan_badan_kepegawaian);
            } else if($tipeApproval == 'lppa' || $tipeApproval == 'bapenda') {
                $kegiatan->setCatatanLppa($catatan_lppa);
            } else if($tipeApproval == 'bagian_organisasi') {
                $kegiatan->setCatatanBagianOrganisasi($catatan_bagian_organisasi);
            }
            $sudah_rka = $kegiatan->getIsPernahRka();
            $sekarang = date('Y-m-d H:i:s');
            $kegiatan->setLastUpdateTime($sekarang);
            
            $kegiatan->save();
            if ($sudah_rka) {
                $c_kegiatan = new Criteria();
                $c_kegiatan->add(MasterKegiatanPeer::UNIT_ID, $unit_id);
                $c_kegiatan->add(MasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                $kegiatan = MasterKegiatanPeer::doSelectOne($c_kegiatan);
                if($tipeApproval == 'anggaran') {
                    $kegiatan->setCatatanPembahasan($catatan);
                    $kegiatan->setCatatanBpkpd($catatan_bpkpd);
                } else if($tipeApproval == 'bagian_hukum') {
                    $kegiatan->setCatatanBagianHukum($catatan_bagian_hukum);
                } else if($tipeApproval == 'inspektorat') {
                    $kegiatan->setCatatanInspektorat($catatan_inspektorat);
                } else if($tipeApproval == 'bkd' || $tipeApproval == 'bkpsdm') {
                    $kegiatan->setCatatanBadanKepegawaian($catatan_badan_kepegawaian);
                } else if($tipeApproval == 'lppa' || $tipeApproval == 'bapenda') {
                    $kegiatan->setCatatanLppa($catatan_lppa);
                } else if($tipeApproval == 'bagian_organisasi') {
                    $kegiatan->setCatatanBagianOrganisasi($catatan_bagian_organisasi);
                }
                $kegiatan->setLastUpdateTime($sekarang);
                $kegiatan->save();
            }
            $tahap = DinasMasterKegiatanPeer::getTahapKegiatan($unit_id, $kode_kegiatan);
            $query = "update " . sfConfig::get('app_default_schema') . ".deskripsi_resume_rapat_kegiatan set catatan_pembahasan='$catatan' where id_deskripsi_resume_rapat=
                (select max(id) from " . sfConfig::get('app_default_schema') . ".deskripsi_resume_rapat
                where tahap=$tahap and unit_id='$unit_id') and kegiatan_code='$kode_kegiatan'";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $stmt->executeQuery();
            $this->setFlash('berhasil', 'Telah berhasil menyimpan catatan');
            return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
        }
        // ada komponen 
        if ($unit_id != '' && $kode_kegiatan != '' && $pilihan != null) {
            $ada_error = FALSE;
            $error = array();
            // approve
            if ($this->getRequestParameter('proses')) {
                $c_kegiatan = new Criteria();
                $c_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                $c_kegiatan->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                $kegiatan = DinasMasterKegiatanPeer::doSelectOne($c_kegiatan);

                if (strlen(strip_tags($catatan)) >= 20 || (strlen(strip_tags($kegiatan->getCatatanPembahasan())) > 20 && strlen(strip_tags($catatan)) < 20) || ($tipeApproval != 'anggaran')) {
                    $con = Propel::getConnection();
                    $con->begin();

                    $c_kegiatan = new Criteria();
                    $c_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                    $c_kegiatan->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                    $c_kegiatan->add(DinasMasterKegiatanPeer::IS_PERNAH_RKA, TRUE);
                    if (DinasMasterKegiatanPeer::doSelectOne($c_kegiatan)) {
                        $tabel_prev = 'prev_';
                    }
//tambahan untuk yang sudah dihapus
                    $query = "select a.detail_no from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail a, " . sfConfig::get('app_default_schema') . "." . $tabel_prev . "rincian_detail b 
                    where a.status_hapus=true and b.status_hapus=false and 
                    a.unit_id=b.unit_id and a.kegiatan_code=b.kegiatan_code and a.detail_no=b.detail_no and 
                    a.unit_id='$unit_id' and a.kegiatan_code='$kode_kegiatan'";
                    $stmt = $con->prepareStatement($query);
                    $rs_dihapus = $stmt->executeQuery();
                    while ($rs_dihapus->next()) {
                        array_push($pilihan, $rs_dihapus->getString('detail_no'));
                    }

                    $rd = new DinasRincianDetail();

                    if(sfConfig::get('app_fasilitas_paguDinasBerdasarKegiatan') == 'buka'){
                        $status_pagu_rincian = $rd->getBatasPaguPerKegiatanforApprove($unit_id, $kode_kegiatan);
                        if ($status_pagu_rincian == '1') {
                            $this->setFlash('gagal', 'Jumlah rincian pada kegiatan ini tidak sama dengan jumlah pagu');
                            $con->rollback();
                            return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                        }
                    }

                    if($kegiatan->getTahap() != 'murni'){
                        if ($rd->cekKomponenTertinggal($unit_id, $kode_kegiatan, 4, $pilihan)) {
                            $this->setFlash('gagal', 'Masih ada komponen yang tertinggal');
                            $con->rollback();
                            return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                        }
                    }

                    if ($rd->cekPerBelanja($unit_id, $kode_kegiatan, 4, $pilihan) && $unit_id != '9999') {
                        $this->setFlash('gagal', 'Masih ada belanja yang belum balance');
                        $con->rollback();
                        return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                    }

                    // if($kegiatan->getTahap() == 'murni' && sfConfig::get('app_tahap_detail') != 'murni_bukuputih'){
                    //     $status_bypass_validasi = TRUE;

                    //     $arr_skpd_buka_cek_belanja_murni = array();

                    //     if ($rd->cekPerBelanjaMurni($unit_id, $kode_kegiatan, 4, $pilihan) && !in_array($kode_kegiatan, $arr_skpd_buka_cek_belanja_murni)) {
                    //         $this->setFlash('gagal', 'Masih ada belanja yang belum balance dari Murni Buku Biru (Ada Geser Nilai Belanja)');
                    //         $con->rollback();
                    //         return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                    //     }
                    // }

                    if($kegiatan->getTahap() == 'pak' && sfConfig::get('app_tahap_detail') != 'pak_bukuputih'){
                        // cekperbelanjaPak
                        $arr_skpd_buka_cek_belanja = array();
                        if ($rd->cekPerBelanjaPak($unit_id, $kode_kegiatan, 4, $pilihan) && !in_array($kode_kegiatan, $arr_skpd_buka_cek_belanja)) {
                           $this->setFlash('gagal', 'Masih ada belanja yang belum balance');
                           $con->rollback();
                           return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                        }
                    }

                    // ganti per kode kegiatan 
                    $arr_skpd_tutup = array('');
                    if ($kegiatan->getTahap() != "murni") {
                        if (sfConfig::get('app_fasilitas_cekRekeningRevisi') == 'buka' && !(in_array($kode_kegiatan, $arr_skpd_tutup))) {
                            if ($rek = $rd->cekPerRekening($unit_id, $kode_kegiatan, 4, $pilihan)) {
                                $pesan = '';
                                foreach ($rek as $kode => $isi) {
                                    $nilai = explode('|', $isi);
                                    $pesan += '- ' . $kode . ' semula=' . $nilai[0] . ' menjadi=' . $nilai[1] . ' -';
                                }
                                $this->setFlash('gagal', 'Ada pergeseran rekening : ' . $pesan . ' dari RKA');
                                $con->rollback();
                                return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                            }
                        }
                    } //tutup  if tahap != murni

                    // $maju = FALSE;
                    // $where_persetujuan = '';

                    // if($tipeApproval == 'anggaran') {
                    //     $where_persetujuan = 'is_penyelia_setuju=false or is_bappeko_setuju=false or is_bagian_hukum_setuju=false or is_inspektorat_setuju = false or is_badan_kepegawaian_setuju = false or is_lppa_setuju = false or is_bagian_organisasi_setuju = false';
                    // } else if($tipeApproval == 'bagian_hukum') {
                    //     $where_persetujuan = 'is_penyelia_setuju=false or is_bappeko_setuju=false or is_tapd_setuju=false or is_inspektorat_setuju = false or is_badan_kepegawaian_setuju = false or is_lppa_setuju = false or is_bagian_organisasi_setuju = false';
                    // } else if($tipeApproval == 'inspektorat') {
                    //     $where_persetujuan = 'is_penyelia_setuju=false or is_bappeko_setuju=false or is_bagian_hukum_setuju=false or is_tapd_setuju = false or is_badan_kepegawaian_setuju = false or is_lppa_setuju = false or is_bagian_organisasi_setuju = false';
                    // } else if($tipeApproval == 'bkd' || $tipeApproval == 'bkpsdm') {
                    //     $where_persetujuan = 'is_penyelia_setuju=false or is_bappeko_setuju=false or is_bagian_hukum_setuju=false or is_inspektorat_setuju = false or is_tapd_setuju = false or is_lppa_setuju = false or is_bagian_organisasi_setuju = false';
                    // } else if($tipeApproval == 'lppa') {
                    //     $where_persetujuan = 'is_penyelia_setuju=false or is_bappeko_setuju=false or is_bagian_hukum_setuju=false or is_inspektorat_setuju = false or is_badan_kepegawaian_setuju = false or is_tapd_setuju = false or is_bagian_organisasi_setuju = false';
                    // } else if($tipeApproval == 'bagian_organisasi') {
                    //     $where_persetujuan = 'is_penyelia_setuju=false or is_bappeko_setuju=false or is_bagian_hukum_setuju=false or is_inspektorat_setuju = false or is_badan_kepegawaian_setuju = false or is_tapd_setuju = false';
                    // } else {
                    //     $where_persetujuan = 'is_penyelia_setuju=false or is_bappeko_setuju=false or is_bagian_hukum_setuju=false or is_inspektorat_setuju = false or is_badan_kepegawaian_setuju = false or is_tapd_setuju = false or is_lppa_setuju = false or is_bagian_organisasi_setuju = false';
                    // }

                    // $query = "select count(*) as nilai from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail 
                    //           where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and status_hapus=false and
                    //           status_level=4 and ($where_persetujuan) and volume>0";

                    if($tipeApproval == 'anggaran') {
                        $where_persetujuan = 'is_penyelia_setuju=true and is_bappeko_setuju=true and is_bagian_hukum_setuju=true and is_inspektorat_setuju = true and is_badan_kepegawaian_setuju = true and is_lppa_setuju = true and is_bagian_organisasi_setuju = true';
                    } else if($tipeApproval == 'bagian_hukum') {
                        $where_persetujuan = 'is_penyelia_setuju=true and is_bappeko_setuju=true and is_tapd_setuju=true and is_inspektorat_setuju = true and is_badan_kepegawaian_setuju = true and is_lppa_setuju = true and is_bagian_organisasi_setuju = true';
                    } else if($tipeApproval == 'inspektorat') {
                        $where_persetujuan = 'is_penyelia_setuju=true and is_bappeko_setuju=true and is_bagian_hukum_setuju=true and is_tapd_setuju = true and is_badan_kepegawaian_setuju = true and is_lppa_setuju = true and is_bagian_organisasi_setuju = true';
                    } else if($tipeApproval == 'bkd' || $tipeApproval == 'bkpsdm') {
                        $where_persetujuan = 'is_penyelia_setuju=true and is_bappeko_setuju=true and is_bagian_hukum_setuju=true and is_inspektorat_setuju = true and is_tapd_setuju = true and is_lppa_setuju = true and is_bagian_organisasi_setuju = true';
                    } else if($tipeApproval == 'lppa' || $tipeApproval == 'bapenda') {
                        $where_persetujuan = 'is_penyelia_setuju=true and is_bappeko_setuju=true and is_bagian_hukum_setuju=true and is_inspektorat_setuju = true and is_badan_kepegawaian_setuju = true and is_tapd_setuju = true and is_bagian_organisasi_setuju = true';
                    } else if($tipeApproval == 'bagian_organisasi') {
                        $where_persetujuan = 'is_penyelia_setuju=true and is_bappeko_setuju=true and is_bagian_hukum_setuju=true and is_inspektorat_setuju = true and is_badan_kepegawaian_setuju = true and is_tapd_setuju = true and is_lppa_setuju = true';
                    } else {
                        $where_persetujuan = 'is_penyelia_setuju=true and is_bappeko_setuju=true and is_bagian_hukum_setuju=true and is_inspektorat_setuju = true and is_badan_kepegawaian_setuju = true and is_tapd_setuju = true and is_lppa_setuju = true and is_bagian_organisasi_setuju = true';
                    }

                    $query = "select count(*) as nilai from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail 
                              where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and status_hapus=false and
                              status_level=4 and $where_persetujuan and (volume>0 or volume=0)";

                    $stmt = $con->prepareStatement($query);
                    $rs_cek = $stmt->executeQuery();
                    if ($rs_cek->next()) {
                        $cek = $rs_cek->getString('nilai');
                        if ($cek > 0) {
                            // biar gabisa lolos kalo catatannya kurang/tidak diisi
                            // if ($tipeApproval != 'anggaran' && strlen(strip_tags($kegiatan->getCatatanPembahasan())) < 20) {
                            //     $this->setFlash('gagal', 'Catatan pembahasan belum diisi / masih kurang dari 20 karakter');
                            //     return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                            // }
                            // if($tipeApproval != 'anggaran' && strlen(strip_tags($kegiatan->getCatatanBpkpd())) < 15) {
                            //     $this->setFlash('gagal', 'Catatan BPKPD belum diisi / masih kurang dari 15 karakter');
                            //     return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                            // }
                            // if($tipeApproval != 'bagian_hukum' && strlen(strip_tags($kegiatan->getCatatanBagianHukum())) < 15) {
                            //     $this->setFlash('gagal', 'Catatan Bagian Hukum belum diisi / masih kurang dari 15 karakter');
                            //     return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                            // }
                            // if($tipeApproval != 'inspektorat' && strlen(strip_tags($kegiatan->getCatatanInspektorat())) < 15) {
                            //     $this->setFlash('gagal', 'Catatan Inspektorat belum diisi / masih kurang dari 15 karakter');
                            //     return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                            // }
                            // if($tipeApproval != 'bkd' && strlen(strip_tags($kegiatan->getCatatanBadanKepegawaian())) < 15) {
                            //     $this->setFlash('gagal', 'Catatan BKD belum diisi / masih kurang dari 15 karakter');
                            //     return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                            // }
                            // if($tipeApproval != 'lppa' && strlen(strip_tags($kegiatan->getCatatanLppa())) < 15) {
                            //     $this->setFlash('gagal', 'Catatan LP2A belum diisi / masih kurang dari 15 karakter');
                            //     return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                            // }
                            // if($tipeApproval != 'bagian_organisasi' && strlen(strip_tags($kegiatan->getCatatanBagianOrganisasi())) < 15) {
                            //     $this->setFlash('gagal', 'Catatan Bagian Organisasi belum diisi / masih kurang dari 15 karakter');
                            //     return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                            // }
                            // if(strlen(strip_tags($kegiatan->getCatatanBappeko())) < 15) {
                            //     $this->setFlash('gagal', 'Catatan Bappeko belum diisi / masih kurang dari 15 karakter');
                            //     return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                            // }
                            // if(strlen(strip_tags($kegiatan->getCatatanPenyelia())) < 15) {
                            //     $this->setFlash('gagal', 'Catatan Penyelia belum diisi / masih kurang dari 15 karakter');
                            //     return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                            // }

                            $maju = TRUE;
                            $cek_sisipan = $rd->cekSisipan($unit_id, $kode_kegiatan, 4);
                            $c_rd = new Criteria();
                            $c_rd->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
                            $c_rd->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
                            $c_rd->add(DinasRincianDetailPeer::STATUS_HAPUS, FALSE);
                            $c_rd->add(DinasRincianDetailPeer::STATUS_LEVEL, 4);
                            $c_rd->add(DinasRincianDetailPeer::IS_BAPPEKO_SETUJU, TRUE);
                            $c_rd->add(DinasRincianDetailPeer::IS_PENYELIA_SETUJU, TRUE);
                            $c_rd->add(DinasRincianDetailPeer::IS_TAPD_SETUJU, TRUE);
                            $c_rd->add(DinasRincianDetailPeer::IS_BAGIAN_HUKUM_SETUJU, TRUE);
                            $c_rd->add(DinasRincianDetailPeer::IS_INSPEKTORAT_SETUJU, TRUE);
                            $c_rd->add(DinasRincianDetailPeer::IS_BADAN_KEPEGAWAIAN_SETUJU, TRUE);
                            $c_rd->add(DinasRincianDetailPeer::IS_LPPA_SETUJU, TRUE);
                            $c_rd->add(DinasRincianDetailPeer::IS_BAGIAN_ORGANISASI_SETUJU, TRUE);
                            $rs_rd_pilih = DinasRincianDetailPeer::doSelect($c_rd);
                            foreach ($rs_rd_pilih as $pilih) {
                                $pilihan[] = $pilih->getDetailNo();
                            }
                        }
                    }
//UNTUK REVISI4
//                    $c_rd2 = new Criteria();
//                    $c_rd2->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
//                    $c_rd2->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
//                    $c_rd2->add(DinasRincianDetailPeer::STATUS_LEVEL, 4);
////                    $c_rd2->add(DinasRincianDetailPeer::IS_TAPD_SETUJU, FALSE);
//                    $c_rd2->add(DinasRincianDetailPeer::IS_BAPPEKO_SETUJU, TRUE);
//                    $c_rd2->add(DinasRincianDetailPeer::IS_PENYELIA_SETUJU, TRUE);
//                    $c_rd2->add(DinasRincianDetailPeer::DETAIL_NO, implode(',', $pilihan), Criteria::NOT_IN);
//                    if (DinasRincianDetailPeer::doCount($c_rd2) > 0) {
//                        $maju = TRUE;
//                        $cek_sisipan = $rd->cekSisipan($unit_id, $kode_kegiatan, 4);
//                    }

                    foreach ($pilihan as $detail_no) {
                        $c = new Criteria();
                        $c->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
                        $c->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
                        $c->add(DinasRincianDetailPeer::DETAIL_NO, $detail_no);
                        $rs_rd = DinasRincianDetailPeer::doSelectOne($c);
                        //$nilaiBaru = round($rs_rd->getKomponenHargaAwal() * $rs_rd->getVolume() * (100 + $rs_rd->getPajak()) / 100);
                        $nilaiBaru = $rs_rd->getNilaiAnggaran();
                        $totNilaiSwakelola = 0;
                        $totNilaiKontrak = 0;
                        $totNilaiRealisasi = 0;
                        $totVolumeRealisasi = 0;
                        $totNilaiAlokasi = 0;
                        $totNilaiHps = 0;
                        $ceklelangselesaitidakaturanpembayaran = 0;
                        $lelang = 0;
                        $getCekNilaiKontrak = 0;

                        // if (sfConfig::get('app_fasilitas_cekeProject') == 'buka') {
                        //     $totNilaiAlokasi = $rd->getCekNilaiAlokasiProject($unit_id, $kode_kegiatan, $detail_no);
                        //     if (sfConfig::get('app_fasilitas_cekServer') == 'buka') {
                        //         $lelang = $rd->getCekLelang($unit_id, $kode_kegiatan, $detail_no, $rs_rd->getNilaiAnggaran());
                        //         if (sfConfig::get('app_fasilitas_cekeDelivery') == 'buka') {
                        //             $totNilaiSwakelola = $rd->getCekNilaiSwakelolaDelivery2($unit_id, $kode_kegiatan, $detail_no);
                        //             $totNilaiKontrak = $rd->getCekNilaiKontrakDelivery2($unit_id, $kode_kegiatan, $detail_no);
                        //             $totNilaiRealisasi = $rd->getCekRealisasi($unit_id, $kode_kegiatan, $detail_no);
                        //             $totVolumeRealisasi = $rd->getCekVolumeRealisasi($unit_id, $kode_kegiatan, $detail_no);
                        //             $getCekNilaiKontrak = $rd->cekKontrak($unit_id . '.' . $kode_kegiatan . '.' . $detail_no);
                        //             $totNilaiHps = $rd->getCekNilaiHPSKomponen($unit_id, $kode_kegiatan, $detail_no);
                        //             $ceklelangselesaitidakaturanpembayaran = $rd->getCekLelangTidakAdaAturanPembayaran($unit_id, $kode_kegiatan, $detail_no);
                        //         }
                        //     }
                        // }
                        if ( (($nilaiBaru < $totNilaiKontrak) || ($nilaiBaru < $totNilaiSwakelola))  && !$status_bypass_validasi ) {
                            if ($totNilaiKontrak == 0) {
                                // array_push($error, 'Komponen ' . $rs_rd->getKomponenName() . ' (' . $rs_rd->getDetailName() . ') sudah terpakai di edelivery, sejumlah Rp.' . number_format($totNilaiSwakelola, 0, ',', '.'));
                                array_push($error, 'Komponen ' . $rs_rd->getKomponenName() . ' (' . $rs_rd->getDetailName() . ') sudah terpakai di edelivery, silahkan cek eDelivery terlebih dahulu');
                            } else if ($totNilaiSwakelola == 0) {
                                // array_push($error, 'Komponen ' . $rs_rd->getKomponenName() . ' (' . $rs_rd->getDetailName() . ') sudah terpakai di edelivery, sejumlah Rp.' . number_format($totNilaiKontrak, 0, ',', '.'));
                                array_push($error, 'Komponen ' . $rs_rd->getKomponenName() . ' (' . $rs_rd->getDetailName() . ') sudah terpakai di edelivery, silahkan cek eDelivery terlebih dahulu');
                            } else {
                                // array_push($error, 'Komponen ' . $rs_rd->getKomponenName() . ' (' . $rs_rd->getDetailName() . ') sudah terpakai di edelivery, sejumlah Rp.' . number_format($totNilaiKontrak, 0, ',', '.'));
                                array_push($error, 'Komponen ' . $rs_rd->getKomponenName() . ' (' . $rs_rd->getDetailName() . ') sudah terpakai di edelivery, silahkan cek eDelivery terlebih dahulu');
                            }
                            $ada_error = TRUE;

//                        $con->rollback();
//                        return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                        } else if ( ($nilaiBaru < $totNilaiHps)  && !$status_bypass_validasi ) {
                            $ada_error = TRUE;
                            array_push($error, 'Komponen ' . $rs_rd->getKomponenName() . ' (' . $rs_rd->getDetailName() . ') lebih kecil dari Nilai HPS Per Komponen , sejumlah Rp.' . number_format($totNilaiHps, 0, ',', '.'));
//                        $this->setFlash('gagal', 'Mohon maaf , lebih kecil dari Nilai HPS Per Komponen , sejumlah Rp.' . number_format($totNilaiHps, 0, ',', '.'));
//                        $con->rollback();
//                        return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                        } else if ( $ceklelangselesaitidakaturanpembayaran == 1  && !$status_bypass_validasi ) {
                            $ada_error = TRUE;
                            array_push($error, 'Proses Lelang untuk  komponen ' . $rs_rd->getKomponenName() . ' (' . $rs_rd->getDetailName() . ') telah selesai, namun Belum ada isian Aturan Pembayaran eDelivery. Silahkan mengisi Aturan Pembayaran terlebih dahulu ');
//                        $this->setFlash('gagal', 'Proses Lelang untuk  komponen ' . $rs_rd->getKomponenName() . ' (' . $rs_rd->getDetailName() . ') telah selesai, namun Belum ada isian Aturan Pembayaran eDelivery. Silahkan mengisi Aturan Pembayaran terlebih dahulu ');
//                        $con->rollback();
//                        return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                        } else if ($lelang > 0  && !$status_bypass_validasi) {
                            $ada_error = TRUE;
                            array_push($error, 'Komponen ' . $rs_rd->getKomponenName() . ' (' . $rs_rd->getDetailName() . ') Sedang dalam Proses Lelang');
//                        $this->setFlash('gagal', 'Sedang dalam Proses Lelang untuk  komponen ' . $rs_rd->getKomponenName() . ' (' . $rs_rd->getDetailName() . ')');
//                        $con->rollback();
//                        return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                        } else if ( $nilaiBaru < $totNilaiRealisasi  && !$status_bypass_validasi) {
                            $ada_error = TRUE;
                            // array_push($error, 'Komponen ' . $rs_rd->getKomponenName() . ' (' . $rs_rd->getDetailName() . ') sudah terpakai di edelivery, sejumlah Rp.' . number_format($totNilaiRealisasi, 0, ',', '.'));
                            array_push($error, 'Komponen ' . $rs_rd->getKomponenName() . ' (' . $rs_rd->getDetailName() . ') sudah terpakai di edelivery, silahkan cek eDelivery terlebih dahulu');
                        } else if ($rs_rd->getVolume() < $totVolumeRealisasi && $rs_rd->getStatusLelang() != 'lock'  && !$status_bypass_validasi) {
                            $ada_error = TRUE;
                            array_push($error, 'Komponen ' . $rs_rd->getKomponenName() . ' (' . $rs_rd->getDetailName() . ') sudah terpakai di edelivery, dengan volume sebesar' . number_format($totVolumeRealisasi, 0, ',', '.'));
                        } else if( ($getCekNilaiKontrak) > $nilaiBaru && in_array($rs_rd->getRekeningCode(), array('5.2.2.24.01', '5.2.1.02.02')) ) {
                            $ada_error = TRUE;
                            array_push($error, 'Mohon maaf, untuk komponen ini sudah ada kontrak senilai ' . number_format($getCekNilaiKontrak, 0, ',', '.') . ', silahkan cek di edelivery');
                        } else {
                            $c = new Criteria();
                            $c->add(RincianDetailPeer::UNIT_ID, $unit_id);
                            $c->addAnd(RincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
                            $c->addAnd(RincianDetailPeer::DETAIL_NO, $detail_no);
                            if ($rs_rka = RincianDetailPeer::doSelectOne($c)) {
                                $semula = $rs_rka->getNilaiAnggaran();
                                $volume_semula = $rs_rka->getVolume();
                                $satuan_semula = $rs_rka->getSatuan();
                            } else {
                                $semula = 0;
                                $volume_semula = 0;
                                $satuan_semula = '';
                            }
                            $menjadi = $rs_rd->getNilaiAnggaran();
                            $volume_menjadi = $rs_rd->getVolume();
                            $satuan_menjadi = $rs_rd->getSatuan();
                            if ($semula <> $menjadi) {
                                $tahap = DinasMasterKegiatanPeer::getTahapKegiatan($unit_id, $kode_kegiatan);
                                if($satuan_menjadi=='Paket' and $status_lelang == 'lock' and $volume_menjadi=='1')
                                    $nilai_psp=$menjadi-$semula;
                                $c_log = new Criteria();
                                $c_log->add(LogPerubahanRevisiPeer::UNIT_ID, $unit_id);
                                $c_log->addAnd(LogPerubahanRevisiPeer::KEGIATAN_CODE, $kode_kegiatan);
                                $c_log->addAnd(LogPerubahanRevisiPeer::DETAIL_NO, $detail_no);
                                $c_log->addAnd(LogPerubahanRevisiPeer::TAHAP, $tahap);

                                if ($log = LogPerubahanRevisiPeer::doSelectOne($c_log)) {
                                    $log->setNilaiAnggaranSemula($semula);
                                    $log->setNilaiAnggaranMenjadi($menjadi);
                                    $log->setVolumeSemula($volume_semula);
                                    $log->setVolumeMenjadi($volume_menjadi);
                                    $log->setSatuanSemula($satuan_semula);
                                    $log->setSatuanMenjadi($satuan_menjadi);
                                    $log->setNilaiPsp($nilai_psp);
                                    $log->setStatus(0);
                                    $log->save();
                                } else {
                                    $log = new LogPerubahanRevisi();
                                    $log->setUnitId($unit_id);
                                    $log->setKegiatanCode($kode_kegiatan);
                                    $log->setDetailNo($detail_no);
                                    $log->setTahap($tahap);
                                    $log->setNilaiAnggaranSemula($semula);
                                    $log->setNilaiAnggaranMenjadi($menjadi);
                                    $log->setVolumeSemula($volume_semula);
                                    $log->setVolumeMenjadi($volume_menjadi);
                                    $log->setSatuanSemula($satuan_semula);
                                    $log->setSatuanMenjadi($satuan_menjadi);
                                    $log->setNilaiPsp($nilai_psp);
                                    $log->setStatus(0);
                                    $log->save();
                                }
                            }
                            if ($maju) {
                                if ($rs_rd->getStatusLevelTolak() == $rs_rd->getStatusLevel()) {
                                    $rs_rd->setStatusLevelTolak(null);
                                }
                                if ($cek_sisipan) {
                                    $rs_rd->setStatusSisipan(false);
                                }
                                $rs_rd->setStatusLevel(5);
                            }
                            if($tipeApproval == 'anggaran')
                                $rs_rd->setIsTapdSetuju(TRUE);
                            else if($tipeApproval == 'bagian_hukum')
                                $rs_rd->setIsBagianHukumSetuju(TRUE);
                            else if($tipeApproval == 'inspektorat')
                                $rs_rd->setIsInspektoratSetuju(TRUE);
                            else if($tipeApproval == 'bkd' || $tipeApproval == 'bkpsdm')
                                $rs_rd->setIsBadanKepegawaianSetuju(TRUE);
                            else if($tipeApproval == 'lppa' || $tipeApproval == 'bapenda')
                                $rs_rd->setIsLppaSetuju(TRUE);
                            else if($tipeApproval == 'bagian_organisasi')
                                $rs_rd->setIsBagianOrganisasiSetuju(TRUE);
                            $rs_rd->save();
                        }
                    }

                    $kumpulan_detail_no = implode('|', $pilihan);
                    $log = new LogApproval();
                    $log->setUnitId($unit_id);
                    $log->setKegiatanCode($kode_kegiatan);
                    $log->setUserId($this->getUser()->getNamaLogin());
                    $log->setKumpulanDetailNo($kumpulan_detail_no);
                    $log->setTahap(LogApprovalPeer::getTahapKegiatan($unit_id, $kode_kegiatan));
                    $log->setWaktu(date('Y-m-d H:i:s'));
                    $log->setSebagai('Tim Anggaran ('.$tipeApproval.')');
                    $log->save();

                    $c_kegiatan = new Criteria();
                    $c_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                    $c_kegiatan->addAnd(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                    $kegiatan = DinasMasterKegiatanPeer::doSelectOne($c_kegiatan);
                    $kegiatan_sisipan = $kegiatan->getIsPernahRka();
                    if($tipeApproval == 'anggaran')
                        $kegiatan->setIsTapdSetuju(TRUE);
                    else if($tipeApproval == 'bagian_hukum')
                        $kegiatan->setIsBagianHukumSetuju(TRUE);
                    else if($tipeApproval == 'inspektorat')
                        $kegiatan->setIsInspektoratSetuju(TRUE);
                    else if($tipeApproval == 'bkd' || $tipeApproval == 'bkpsdm')
                        $kegiatan->setIsBadanKepegawaianSetuju(TRUE);
                    else if($tipeApproval == 'lppa' || $tipeApproval == 'bapenda')
                        $kegiatan->setIsLppaSetuju(TRUE);
                    else if($tipeApproval == 'bagian_organisasi')
                        $kegiatan->setIsBagianOrganisasiSetuju(TRUE);
                    if ($maju) {
                        $kegiatan->setStatusLevel(5);
                    }

                    if ($tipeApproval == 'anggaran') {
                        $kegiatan->setCatatanPembahasan($catatan);
                        $kegiatan->setVerifikasiBpkpd($verifikasi);
                    }

                    if ($tipeApproval == 'anggaran') {
                        $kegiatan->setCatatanBpkpd($catatan_bpkpd);
                        $kegiatan->setVerifikasiBpkpd($verifikasi);
                    }

                    if ($tipeApproval == 'bagian_hukum') {
                        $kegiatan->setCatatanBagianHukum($catatan_bagian_hukum);
                        $kegiatan->setVerifikasiBagianHukum($verifikasi);
                    }

                    if ($tipeApproval == 'inspektorat') {
                        $kegiatan->setCatatanInspektorat($catatan_inspektorat);
                        $kegiatan->setVerifikasiInspektorat($verifikasi);
                    }

                    if ($tipeApproval == 'bkd' || $tipeApproval == 'bkpsdm') {
                        $kegiatan->setCatatanBadanKepegawaian($catatan_badan_kepegawaian);
                        $kegiatan->setVerifikasiBadanKepegawaian($verifikasi);
                    }

                    if ($tipeApproval == 'lppa' || $tipeApproval == 'bapenda') {
                        $kegiatan->setCatatanLppa($catatan_lppa);
                        $kegiatan->setVerifikasiLppa($verifikasi);
                    }

                    if ($tipeApproval == 'bagian_organisasi') {
                        $kegiatan->setCatatanBagianOrganisasi($catatan_bagian_organisasi);
                        $kegiatan->setVerifikasiBagianOrganisasi($verifikasi);
                    }

                    $sekarang = date('Y-m-d H:i:s');
                    $kegiatan->setLastUpdateTime($sekarang);
                    $kegiatan->save();

                    // tutup dulu karena masih murni
                    if ($ada_error) {
                        $this->setFlash('gagal', implode(' --- ', $error));
                        $con->rollback();
                        return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                    } else {
//buat resume rapat
                        if (DeskripsiResumeRapatPeer::saveResumeRapat($unit_id, $kode_kegiatan)) {
                            if($tipeApproval == 'anggaran'){
                                $this->setFlash('berhasil', 'Telah berhasil diapprove BPKAD');
                            }
                            elseif($tipeApproval == 'bagian_hukum'){
                                $this->setFlash('berhasil', 'Telah berhasil diapprove Bagian Hukum');
                            }
                            elseif($tipeApproval == 'inspektorat'){
                                $this->setFlash('berhasil', 'Telah berhasil diapprove Inspektorat');
                            }
                            elseif($tipeApproval == 'bkd' || $tipeApproval == 'bkpsdm'){
                                $this->setFlash('berhasil', 'Telah berhasil diapprove BKPSDM');   
                            }
                            elseif($tipeApproval == 'lppa' || $tipeApproval == 'bapenda'){
                                if($tipeApproval == 'lppa'){
                                    $this->setFlash('berhasil', 'Telah berhasil diapprove LP2A');
                                }else if ($tipeApproval == 'bapenda') {
                                    $this->setFlash('berhasil', 'Telah berhasil diapprove Bapenda');
                                }
                            }
                            elseif($tipeApproval == 'bagian_organisasi'){
                                $this->setFlash('berhasil', 'Telah berhasil diapprove Bagian Organisasi');
                            }
                            $con->commit();
                            return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                        } else {
                            $this->setFlash('gagal', 'Terjadi kesalahan, gagal menyimpan berita acara.');
                            $con->rollback();
                            return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                        }
                    } //tutup else tidak ada error 


                } else {
                    $this->setFlash('gagal', 'Catatan pembahasan harus berisi minimal 20 karakter');
                    return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                }
            }

             // balik ke entri
            elseif ($this->getRequestParameter('balik')) {
                $c_kegiatan = new Criteria();
                $c_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                $c_kegiatan->addAnd(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                $c_kegiatan->addAnd(DinasMasterKegiatanPeer::IS_PERNAH_RKA, TRUE);
                if (DinasMasterKegiatanPeer::doSelectOne($c_kegiatan)) {
                    $tabel_prev = 'prev_';
                }
//tambahan untuk yang sudah dihapus
                $query = "select a.detail_no from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail a, " . sfConfig::get('app_default_schema') . "." . $tabel_prev . "rincian_detail b 
                    where a.status_hapus=true and b.status_hapus=false and 
                    a.unit_id=b.unit_id and a.kegiatan_code=b.kegiatan_code and a.detail_no=b.detail_no and 
                    a.unit_id='$unit_id' and a.kegiatan_code='$kode_kegiatan'";
                $con = Propel::getConnection();
                $stmt = $con->prepareStatement($query);
                $rs_dihapus = $stmt->executeQuery();
                while ($rs_dihapus->next()) {
                    array_push($pilihan, $rs_dihapus->getString('detail_no'));
                }

                foreach ($pilihan as $detail_no) {
                    $c = new Criteria();
                    $c->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
                    $c->addAnd(DinasRincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
                    $c->addAnd(DinasRincianDetailPeer::DETAIL_NO, $detail_no);
                    $rs_rd = DinasRincianDetailPeer::doSelectOne($c);
                    $rs_rd->setStatusLevel(0);
                    $rs_rd->setStatusLevelTolak(4);
                    $rs_rd->setIsTapdSetuju(FALSE);
                    $rs_rd->setIsBappekoSetuju(FALSE);
                    $rs_rd->setIsPenyeliaSetuju(FALSE);
                    $rs_rd->setIsBagianHukumSetuju(FALSE);
                    $rs_rd->setIsInspektoratSetuju(FALSE);
                    $rs_rd->setIsBadanKepegawaianSetuju(FALSE);
                    $rs_rd->setIsLppaSetuju(FALSE);
                    $rs_rd->setIsBagianOrganisasiSetuju(FALSE);
                    $rs_rd->save();

                    $tahap = DinasMasterKegiatanPeer::getTahapKegiatan($unit_id, $kode_kegiatan);
                    $c_log = new Criteria();
                    $c_log->add(LogPerubahanRevisiPeer::UNIT_ID, $unit_id);
                    $c_log->addAnd(LogPerubahanRevisiPeer::KEGIATAN_CODE, $kode_kegiatan);
                    $c_log->addAnd(LogPerubahanRevisiPeer::DETAIL_NO, $detail_no);
                    $c_log->addAnd(LogPerubahanRevisiPeer::TAHAP, $tahap);

                    if ($log = LogPerubahanRevisiPeer::doSelectOne($c_log)) {
                        $log->setStatus(-1);
                        $log->save();
                    }
                }

                $catatan_tolak = $this->getRequestParameter('catatan_tolak');
                $kumpulan_detail_no = implode('|', $pilihan);
                $log = new LogApproval();
                $log->setUnitId($unit_id);
                $log->setKegiatanCode($kode_kegiatan);
                $log->setUserId($this->getUser()->getNamaLogin());
                $log->setKumpulanDetailNo($kumpulan_detail_no);
                $log->setTahap(LogApprovalPeer::getTahapKegiatan($unit_id, $kode_kegiatan));
                $log->setWaktu(date('Y-m-d H:i:s'));
                $log->setSebagai('Tim Anggaran (BPKAD)|kembali');
                $log->setCatatan($catatan_tolak);
                $log->save();

                $c_kegiatan = new Criteria();
                $c_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                $c_kegiatan->addAnd(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                $kegiatan = DinasMasterKegiatanPeer::doSelectOne($c_kegiatan);

                if ($catatan && strlen(strip_tags($catatan)) > 5 && $tipeApproval == 'anggaran') {
                    $kegiatan->setCatatanPembahasan($catatan);
                }
// cek catatan bpkpd
                if ($catatan_bpkpd && strlen(strip_tags($catatan_bpkpd)) > 5 && $tipeApproval == 'anggaran') {
                    $kegiatan->setCatatanBpkpd($catatan_bpkpd);
                }

                if ($catatan_bagian_hukum && strlen(strip_tags($catatan_bagian_hukum)) > 5 && $tipeApproval == 'bagian_hukum') {
                    $kegiatan->setCatatanBagianHukum($catatan_bagian_hukum);
                }

                if ($catatan_inspektorat && strlen(strip_tags($catatan_inspektorat)) > 5 && $tipeApproval == 'inspektorat') {
                    $kegiatan->setCatatanInspektorat($catatan_inspektorat);
                }

                if ($catatan_badan_kepegawaian && strlen(strip_tags($catatan_badan_kepegawaian)) > 5 && ($tipeApproval == 'bkd' || $tipeApproval == 'bkpsdm')) {
                    $kegiatan->setCatatanBadanKepegawaian($catatan_badan_kepegawaian);
                }

                if ($catatan_lppa && strlen(strip_tags($catatan_lppa)) > 5 && ($tipeApproval == 'lppa' || $tipeApproval == 'bapenda')) {
                    $kegiatan->setCatatanLppa($catatan_lppa);
                }

                if ($catatan_bagian_organisasi && strlen(strip_tags($catatan_bagian_organisasi)) > 5 && $tipeApproval == 'bagian_organisasi') {
                    $kegiatan->setCatatanBagianOrganisasi($catatan_bagian_organisasi);
                }

                $sekarang = date('Y-m-d H:i:s');
                $kegiatan->setLastUpdateTime($sekarang);
                $kegiatan->setStatusLevel(0);
                $kegiatan->setIsTapdSetuju(FALSE);
                $kegiatan->setIsBappekoSetuju(FALSE);
                $kegiatan->setIsPenyeliaSetuju(FALSE);
                $kegiatan->setIsBagianHukumSetuju(FALSE);
                $kegiatan->setIsInspektoratSetuju(FALSE);
                $kegiatan->setIsBadanKepegawaianSetuju(FALSE);
                $kegiatan->setIsLppaSetuju(FALSE);
                $kegiatan->setIsBagianOrganisasiSetuju(FALSE);
                $kegiatan->setUbahF1Dinas(NULL);
                $kegiatan->setSisaLelangDinas(NULL);
                $kegiatan->setUbahF1Peneliti(NULL);
                $kegiatan->setSisaLelangPeneliti(NULL);
                // $kegiatan->setVerifikasiBpkpd('');
                // $kegiatan->setVerifikasiBappeko('');
                // $kegiatan->setVerifikasiPenyelia('');
                // $kegiatan->setVerifikasiBagianHukum('');
                // $kegiatan->setVerifikasiInspektorat('');
                // $kegiatan->setVerifikasiBadanKepegawaian('');
                // $kegiatan->setVerifikasiLppa('');
                $kegiatan->save();

                $cri = new Criteria();
                $cri->add(RincianBappekoPeer::UNIT_ID, $unit_id);
                $cri->addAnd(RincianBappekoPeer::KODE_KEGIATAN, $kode_kegiatan);
                $cri->addAnd(RincianBappekoPeer::TAHAP, DinasMasterKegiatanPeer::getTahapKegiatan($unit_id, $kode_kegiatan));
                $jawaban = RincianBappekoPeer::doSelectOne($cri);
                if($jawaban) {
                    $jawaban->setJawaban1Bappeko(NULL);
                    $jawaban->setJawaban2Bappeko(NULL);
                    $jawaban->save();
                }

                $this->setFlash('berhasil', 'Telah berhasil memproses ke posisi entri');
                return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
            }
        } else {
            // tidak ada komponen
            if ($this->getRequestParameter('proses') && !$this->getRequestParameter('ada_komponen')) {
//cuma isi catatan / F1 lalu di proses
                $con = Propel::getConnection();
                $c_kegiatan = new Criteria();
                $c_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                $c_kegiatan->addAnd(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                $kegiatan = DinasMasterKegiatanPeer::doSelectOne($c_kegiatan);
                if (strlen(strip_tags($kegiatan->getCatatan())) > 20) {
                    if ($kegiatan->getStatusLevel() >= 4 && !$this->getRequestParameter('ada_komponen')) {
                        
                        if ($tipeApproval == 'anggaran') {
                            $kegiatan->setCatatanPembahasan($catatan);
                            $kegiatan->setVerifikasiBpkpd($verifikasi);
                        }
// cek catatan bpkpd
                        if ($tipeApproval == 'anggaran') {
                            $kegiatan->setCatatanBpkpd($catatan_bpkpd);
                            $kegiatan->setVerifikasiBpkpd($verifikasi);
                        }
                        if ($tipeApproval == 'bagian_hukum') {
                            $kegiatan->setCatatanBagianHukum($catatan_bagian_hukum);
                            $kegiatan->setVerifikasiBagianHukum($verifikasi);
                        }

                        if ($tipeApproval == 'inspektorat') {
                            $kegiatan->setCatatanInspektorat($catatan_inspektorat);
                            $kegiatan->setVerifikasiInspektorat($verifikasi);
                        }

                        if ($tipeApproval == 'bkd' || $tipeApproval == 'bkpsdm') {
                            $kegiatan->setCatatanBadanKepegawaian($catatan_badan_kepegawaian);
                            $kegiatan->setVerifikasiBadanKepegawaian($verifikasi);
                        }

                        if ($tipeApproval == 'lppa' || $tipeApproval == 'bapenda') {
                            $kegiatan->setCatatanLppa($catatan_lppa);
                            $kegiatan->setVerifikasiLppa($verifikasi);
                        }

                        if ($tipeApproval == 'bagian_organisasi') {
                            $kegiatan->setCatatanBagianOrganisasi($catatan_bagian_organisasi);
                            $kegiatan->setVerifikasiBagianOrganisasi($verifikasi);
                        }

                        $sekarang = date('Y-m-d H:i:s');
                        $kegiatan->setLastUpdateTime($sekarang);
                        if (!$this->getRequestParameter('ada_komponen')) {
                            if($tipeApproval == 'anggaran')
                                $kegiatan->setIsTapdSetuju(TRUE);
                            else if($tipeApproval == 'bagian_hukum')
                                $kegiatan->setIsBagianHukumSetuju(TRUE);
                            else if($tipeApproval == 'inspektorat')
                                $kegiatan->setIsInspektoratSetuju(TRUE);
                            else if($tipeApproval == 'bkd' || $tipeApproval == 'bkpsdm')
                                $kegiatan->setIsBadanKepegawaianSetuju(TRUE);
                            else if($tipeApproval == 'lppa' || $tipeApproval == 'bapenda')
                                $kegiatan->setIsLppaSetuju(TRUE);
                            else if($tipeApproval == 'bagian_organisasi')
                                $kegiatan->setIsBagianOrganisasiSetuju(TRUE);
                            if ($kegiatan->getIsTapdSetuju() && $kegiatan->getIsBappekoSetuju() && $kegiatan->getIsPenyeliaSetuju() && $kegiatan->getIsBagianHukumSetuju() && $kegiatan->getIsInspektoratSetuju() && $kegiatan->getIsBadanKepegawaianSetuju() && $kegiatan->getIsLppaSetuju() && $kegiatan->getIsBagianOrganisasiSetuju() && $kegiatan->getStatusLevel() == 4) {
                                $kegiatan->setStatusLevel(5);
                            }
                        }
                        $sudah_rka = $kegiatan->getIsPernahRka();
                        $kegiatan->save();
                        if ($sudah_rka) {
                            $c_kegiatan = new Criteria();
                            $c_kegiatan->add(MasterKegiatanPeer::UNIT_ID, $unit_id);
                            $c_kegiatan->add(MasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                            $kegiatan = MasterKegiatanPeer::doSelectOne($c_kegiatan);
                            
                            if ($tipeApproval == 'anggaran') {
                                $kegiatan->setCatatanPembahasan($catatan);
                                $kegiatan->setVerifikasiBpkpd($verifikasi);
                            }
                            if ($tipeApproval == 'anggaran') {
                                $kegiatan->setCatatanBpkpd($catatan_bpkpd);
                                $kegiatan->setVerifikasiBpkpd($verifikasi);
                            }
                            if ($tipeApproval == 'bagian_hukum') {
                                $kegiatan->setCatatanBagianHukum($catatan_bagian_hukum);
                                $kegiatan->setVerifikasiBagianHukum($verifikasi);
                            }

                            if ($tipeApproval == 'inspektorat') {
                                $kegiatan->setCatatanInspektorat($catatan_inspektorat);
                                $kegiatan->setVerifikasiInspektorat($verifikasi);
                            }

                            if ($tipeApproval == 'bkd' || $tipeApproval == 'bkpsdm') {
                                $kegiatan->setCatatanBadanKepegawaian($catatan_badan_kepegawaian);
                                $kegiatan->setVerifikasiBadanKepegawaian($verifikasi);
                            }

                            if ($tipeApproval == 'lppa' || $tipeApproval == 'bapenda') {
                                $kegiatan->setCatatanLppa($catatan_lppa);
                                $kegiatan->setVerifikasiLppa($verifikasi);
                            }

                            if ($tipeApproval == 'bagian_organisasi') {
                                $kegiatan->setCatatanBagianOrganisasi($catatan_bagian_organisasi);
                                $kegiatan->setVerifikasiBagianOrganisasi($verifikasi);
                            }

                            $kegiatan->save();
                        }
//buat resume rapat
                        if (DeskripsiResumeRapatPeer::saveResumeRapat($unit_id, $kode_kegiatan)) {
                            $this->setFlash('berhasil', 'Telah berhasil menyetujui catatan');
                        } else {
                            $this->setFlash('gagal', 'Terjadi kesalahan, gagal menyimpan berita acara.');
                        }
                        return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                    } else {
                        $this->setFlash('gagal', 'Catatan belum berada di posisi tim anggaran');
                        return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                    }
                } else {
                    $this->setFlash('gagal', 'Catatan harus berisi minimal 20 karakter');
                    return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                }
            }
            if ($this->getRequestParameter('balikall')) {
                $c = new Criteria();
                $c->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
                $c->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
                $c->add(DinasRincianDetailPeer::STATUS_LEVEL, 4);
                $rs_rd = DinasRincianDetailPeer::doSelect($c);
                foreach ($rs_rd as $item) {
                    $item->setStatusLevel(0);
                    $item->setStatusLevelTolak(4);
                    $item->setIsTapdSetuju(FALSE);
                    $item->setIsBappekoSetuju(FALSE);
                    $item->setIsBagianHukumSetuju(FALSE);
                    $item->setIsInspektoratSetuju(FALSE);
                    $item->setIsBadanKepegawaianSetuju(FALSE);
                    $item->setIsLppaSetuju(FALSE);
                    $item->setIsBagianOrganisasiSetuju(FALSE);
                    $item->save();
                }
                
                $c_kegiatan = new Criteria();
                $c_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                $c_kegiatan->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                $kegiatan = DinasMasterKegiatanPeer::doSelectOne($c_kegiatan);

                if ($catatan && strlen(strip_tags($catatan)) > 5 && $tipeApproval == 'anggaran') {
                    $kegiatan->setCatatanPembahasan($catatan);
                }
                if ($catatan_bpkpd && strlen(strip_tags($catatan_bpkpd)) > 5 && $tipeApproval == 'anggaran') {
                    $kegiatan->setCatatanBpkpd($catatan_bpkpd);
                }
                if ($catatan_bagian_hukum && strlen(strip_tags($catatan_bagian_hukum)) > 5 && $tipeApproval == 'bagian_hukum') {
                    $kegiatan->setCatatanBagianHukum($catatan_bagian_hukum);
                }

                if ($catatan_inspektorat && strlen(strip_tags($catatan_inspektorat)) > 5 && $tipeApproval == 'inspektorat') {
                    $kegiatan->setCatatanInspektorat($catatan_inspektorat);
                }

                if ($catatan_badan_kepegawaian && strlen(strip_tags($catatan_badan_kepegawaian)) > 5 && ($tipeApproval == 'bkd' || $tipeApproval == 'bkpsdm')) {
                    $kegiatan->setCatatanBadanKepegawaian($catatan_badan_kepegawaian);
                }

                if ($catatan_lppa && strlen(strip_tags($catatan_lppa)) > 5 && ($tipeApproval == 'lppa' || $tipeApproval == 'bapenda')) {
                    $kegiatan->setCatatanLppa($catatan_lppa);
                }

                if ($catatan_bagian_organisasi && strlen(strip_tags($catatan_bagian_organisasi)) > 5 && $tipeApproval == 'bagian_organisasi') {
                    $kegiatan->setCatatanBagianOrganisasi($catatan_bagian_organisasi);
                }

                $kegiatan->setStatusLevel(0);
                $kegiatan->save();
                
                $this->setFlash('berhasil', 'Telah berhasil memproses semua komponen ke posisi entri');
                return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
            }
            // print lembar verifikasi usulan pergeseran
            if($this->getRequestParameter('print')) {
                $catatan_asisten = $this->getRequestParameter('catatan_asisten');
                //$catatan_save = '<p>'.$catatan_asisten.'</p>';
                $c_kegiatan = new Criteria();
                $c_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                $c_kegiatan->addAnd(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                $kegiatan = DinasMasterKegiatanPeer::doSelectOne($c_kegiatan);

                $kegiatan->setVerifikasiAsisten($catatan_asisten);
                $kegiatan->save();

                return $this->redirect("report/printLembarVerifikasiUsulanPergeseran?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
            }

            $this->setFlash('gagal', 'Mohon memberi tanda cek (v) pada komponen yang disetujui');
            return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
        }
    }

    public function executeProsesapprovebappeko() {
        $tipeApproval = $this->getUser()->getNamaLogin();
        $catatan = trim($this->getRequestParameter('catatan'));
        $catatan = str_replace("'", "''", $catatan);
        $unit_id = $this->getRequestParameter('unit_id');
        $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
        $pilihan = $this->getRequestParameter('pilihaction');
        // jawaban verifikasi
        $verifikasi = $this->getRequestParameter('jawaban_verifikasi');
        if(empty($verifikasi)) {
            $verifikasi = $catatan;
        }

        $status_bypass_validasi = FALSE;

        // biar gabisa lolos kalo catatannya kurang/tidak diisi
        // if (strlen(strip_tags($catatan)) < 15) {
        //     $this->setFlash('gagal', 'Catatan Bappeko harus berisi minimal 15 karakter');
        //     return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
        // }

        if(sfConfig::get('app_tahap_edit') == 'murni') {
            $status_bypass_validasi = TRUE;
        }
        // SEMENTARA SAJA, INGAT DICOMMENT KEMBALI!!!!!
        // DINAS PU, MAU GANTI REKENING Biaya Counter Mesin Fotocopy DAN Biaya Counter Mesin Fotocopy Warna DARI (5.2.2.20.06 Belanja Pemeliharaan Alat Kantor dan Rumah Tangga) KE (   5.2.2.10.01 Belanja Sewa Perlengkapan / Peralatan Kantor)
        if (in_array($kode_kegiatan, array(''))) {
            $status_bypass_validasi = TRUE;
        }
        if ($unit_id != '' && $kode_kegiatan != '' && $pilihan != null) {
            // ada komponen;
            $ada_error = FALSE;
            $error = array();
            if ($this->getRequestParameter('proses')) {
                $con = Propel::getConnection();
                $con->begin();

                $tahap_angka = DinasMasterKegiatanPeer::getTahapKegiatan($unit_id, $kode_kegiatan);

                // if (strlen(strip_tags($catatan)) == 0) {
                //     $this->setFlash('gagal', 'Belum mengisi catatan bappeko');
                //     $con->rollback();
                //     return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                // }

                $c_kegiatan = new Criteria();
                $c_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                $c_kegiatan->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                $c_kegiatan->add(DinasMasterKegiatanPeer::IS_PERNAH_RKA, TRUE);
                if (DinasMasterKegiatanPeer::doSelectOne($c_kegiatan)) {
                    $tabel_prev = 'prev_';
                }
//tambahan untuk yang sudah dihapus
                $query = "select a.detail_no from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail a, " . sfConfig::get('app_default_schema') . "." . $tabel_prev . "rincian_detail b 
                    where a.status_hapus=true and b.status_hapus=false and 
                    a.unit_id=b.unit_id and a.kegiatan_code=b.kegiatan_code and a.detail_no=b.detail_no and 
                    a.unit_id='$unit_id' and a.kegiatan_code='$kode_kegiatan'";
                $stmt = $con->prepareStatement($query);
                $rs_dihapus = $stmt->executeQuery();
                while ($rs_dihapus->next()) {
                    array_push($pilihan, $rs_dihapus->getString('detail_no'));
                }

                $rd = new DinasRincianDetail();

                if(sfConfig::get('app_fasilitas_paguDinasBerdasarKegiatan') == 'buka'){
                    $status_pagu_rincian = $rd->getBatasPaguPerKegiatanforApprove($unit_id, $kode_kegiatan);
                    if ($status_pagu_rincian == '1') {
                        $this->setFlash('gagal', 'Jumlah rincian pada kegiatan ini tidak sama dengan jumlah pagu');
                        $con->rollback();
                        return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                    }
                }
                
                
                if ($rd->cekKomponenTertinggal($unit_id, $kode_kegiatan, 4, $pilihan)) {
                   $this->setFlash('gagal', 'Masih ada komponen yang tertinggal');
                   $con->rollback();
                   return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                }

                if ($rd->cekPerBelanja($unit_id, $kode_kegiatan, 4, $pilihan) && $unit_id != '9999') {
                    $this->setFlash('gagal', 'Masih ada belanja yang belum balance');
                    $con->rollback();
                    return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                }

                // if($tahap_angka == 0 && sfConfig::get('app_tahap_detail') != 'murni_bukuputih'){
                //     $status_bypass_validasi = TRUE;

                //     $arr_skpd_buka_cek_belanja_murni = array();

                //     if ($rd->cekPerBelanjaMurni($unit_id, $kode_kegiatan, 4, $pilihan) && !in_array($kode_kegiatan, $arr_skpd_buka_cek_belanja_murni)) {
                //         $this->setFlash('gagal', 'Masih ada belanja yang belum balance dari Murni Buku Biru (Ada Geser Nilai Belanja)');
                //         $con->rollback();
                //         return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                //     }
                // }
                
                if($tahap_angka == 10 && sfConfig::get('app_tahap_detail') != 'pak_bukuputih'){
                    // cekperbelanjaPak
                    // ganti per kode kegiatan
                    $arr_skpd_buka_cek_belanja = array();
                    if ($rd->cekPerBelanjaPak($unit_id, $kode_kegiatan, 4, $pilihan) && !in_array($kode_kegiatan, $arr_skpd_buka_cek_belanja)) {
                       $this->setFlash('gagal', 'Masih ada belanja yang belum balance');
                       $con->rollback();
                       return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                    }
                }

                // ganti per kode kegiatan
                $arr_skpd_tutup = array('');
                if ($tahap_angka != 0) {
                    if (sfConfig::get('app_fasilitas_cekRekeningRevisi') == 'buka' && !(in_array($kode_kegiatan, $arr_skpd_tutup))) {
                        if ($rek = $rd->cekPerRekening($unit_id, $kode_kegiatan, 4, $pilihan)) {
                            $pesan = '';
                            foreach ($rek as $kode => $isi) {
                                $nilai = explode('|', $isi);
                                $pesan += '- ' . $kode . ' semula=' . $nilai[0] . ' menjadi=' . $nilai[1] . ' -';
                            }
                            $this->setFlash('gagal', 'Ada pergeseran rekening : ' . $pesan . ' dari RKA');
                            $con->rollback();
                            return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                        }
                    }
                } //tutup  if tahap != murni

                $maju = FALSE;
                $where_persetujuan = '';

                // if($tipeApproval == 'anggaran') {
                //     $where_persetujuan = 'is_penyelia_setuju=false or is_bappeko_setuju=false or is_bagian_hukum_setuju=false or is_inspektorat_setuju = false or is_badan_kepegawaian_setuju = false or is_lppa_setuju = false or is_bagian_organisasi_setuju = false';
                // } else if($tipeApproval == 'bagian_hukum') {
                //     $where_persetujuan = 'is_penyelia_setuju=false or is_bappeko_setuju=false or is_tapd_setuju=false or is_inspektorat_setuju = false or is_badan_kepegawaian_setuju = false or is_lppa_setuju = false or is_bagian_organisasi_setuju = false';
                // } else if($tipeApproval == 'inspektorat') {
                //     $where_persetujuan = 'is_penyelia_setuju=false or is_bappeko_setuju=false or is_bagian_hukum_setuju=false or is_tapd_setuju = false or is_badan_kepegawaian_setuju = false or is_lppa_setuju = false or is_bagian_organisasi_setuju = false';
                // } else if($tipeApproval == 'bkd' || $tipeApproval == 'bkpsdm') {
                //     $where_persetujuan = 'is_penyelia_setuju=false or is_bappeko_setuju=false or is_bagian_hukum_setuju=false or is_inspektorat_setuju = false or is_tapd_setuju = false or is_lppa_setuju = false or is_bagian_organisasi_setuju = false';
                // } else if($tipeApproval == 'lppa') {
                //     $where_persetujuan = 'is_penyelia_setuju=false or is_bappeko_setuju=false or is_bagian_hukum_setuju=false or is_inspektorat_setuju = false or is_badan_kepegawaian_setuju = false or is_tapd_setuju = false or is_bagian_organisasi_setuju = false';
                // } else if($tipeApproval == 'bagian_organisasi') {
                //     $where_persetujuan = 'is_penyelia_setuju=false or is_bappeko_setuju=false or is_bagian_hukum_setuju=false or is_inspektorat_setuju = false or is_badan_kepegawaian_setuju = false or is_tapd_setuju = false';
                // } else {
                //     // bappekonya yg nyetujuin
                //     $where_persetujuan = 'is_penyelia_setuju=false or is_bagian_hukum_setuju=false or is_inspektorat_setuju = false or is_badan_kepegawaian_setuju = false or is_tapd_setuju = false or is_lppa_setuju = false or is_bagian_organisasi_setuju = false';
                // }

                if($tipeApproval == 'anggaran') {
                    $where_persetujuan = 'is_penyelia_setuju=true and is_bappeko_setuju=true and is_bagian_hukum_setuju=true and is_inspektorat_setuju = true and is_badan_kepegawaian_setuju = true and is_lppa_setuju = true and is_bagian_organisasi_setuju = true';
                } else if($tipeApproval == 'bagian_hukum') {
                    $where_persetujuan = 'is_penyelia_setuju=true and is_bappeko_setuju=true and is_tapd_setuju=true and is_inspektorat_setuju = true and is_badan_kepegawaian_setuju = true and is_lppa_setuju = true and is_bagian_organisasi_setuju = true';
                } else if($tipeApproval == 'inspektorat') {
                    $where_persetujuan = 'is_penyelia_setuju=true and is_bappeko_setuju=true and is_bagian_hukum_setuju=true and is_tapd_setuju = true and is_badan_kepegawaian_setuju = true and is_lppa_setuju = true and is_bagian_organisasi_setuju = true';
                } else if($tipeApproval == 'bkd' || $tipeApproval == 'bkpsdm') {
                    $where_persetujuan = 'is_penyelia_setuju=true and is_bappeko_setuju=true and is_bagian_hukum_setuju=true and is_inspektorat_setuju = true and is_tapd_setuju = true and is_lppa_setuju = true and is_bagian_organisasi_setuju = true';
                } else if($tipeApproval == 'lppa' || $tipeApproval == 'bapenda') {
                    $where_persetujuan = 'is_penyelia_setuju=true and is_bappeko_setuju=true and is_bagian_hukum_setuju=true and is_inspektorat_setuju = true and is_badan_kepegawaian_setuju = true and is_tapd_setuju = true and is_bagian_organisasi_setuju = true';
                } else if($tipeApproval == 'bagian_organisasi') {
                    $where_persetujuan = 'is_penyelia_setuju=true and is_bappeko_setuju=true and is_bagian_hukum_setuju=true and is_inspektorat_setuju = true and is_badan_kepegawaian_setuju = true and is_tapd_setuju = true';
                } else {
                    // bappekonya yg nyetujuin
                    $where_persetujuan = 'is_penyelia_setuju=true and is_bagian_hukum_setuju=true and is_inspektorat_setuju = true and is_badan_kepegawaian_setuju = true and is_tapd_setuju = true and is_lppa_setuju = true and is_bagian_organisasi_setuju = true';
                }

                $c_kegiatan = new Criteria();
                $c_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                $c_kegiatan->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                $kegiatan = DinasMasterKegiatanPeer::doSelectOne($c_kegiatan);

                // $query = "select count(*) as nilai from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail 
                //           where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and status_hapus=false and
                //           status_level=4 and ($where_persetujuan) and volume>0";

                 $query = "select count(*) as nilai from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail 
                          where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and status_hapus=false and
                          status_level=4 and $where_persetujuan and (volume>0 or volume=0)";
                $stmt = $con->prepareStatement($query);
                $rs_cek = $stmt->executeQuery();
                if ($rs_cek->next()) {
                    $cek = $rs_cek->getString('nilai');
                    if ($cek > 0) {
                        // biar gabisa lolos kalo catatannya kurang/tidak diisi
                        // if (strlen(strip_tags($kegiatan->getCatatanPembahasan())) < 20) {
                        //     $this->setFlash('gagal', 'Catatan pembahasan belum diisi / masih kurang dari 20 karakter');
                        //     return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                        // }
                        // if(strlen(strip_tags($kegiatan->getCatatanBpkpd())) < 15) {
                        //     $this->setFlash('gagal', 'Catatan BPKPD belum diisi / masih kurang dari 15 karakter');
                        //     return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                        // }
                        // if(strlen(strip_tags($kegiatan->getCatatanBagianHukum())) < 15) {
                        //     $this->setFlash('gagal', 'Catatan Bagian Hukum belum diisi / masih kurang dari 15 karakter');
                        //     return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                        // }
                        // if(strlen(strip_tags($kegiatan->getCatatanInspektorat())) < 15) {
                        //     $this->setFlash('gagal', 'Catatan Inspektorat belum diisi / masih kurang dari 15 karakter');
                        //     return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                        // }
                        // if(strlen(strip_tags($kegiatan->getCatatanBadanKepegawaian())) < 15) {
                        //     $this->setFlash('gagal', 'Catatan BKD belum diisi / masih kurang dari 15 karakter');
                        //     return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                        // }
                        // if(strlen(strip_tags($kegiatan->getCatatanLppa())) < 15) {
                        //     $this->setFlash('gagal', 'Catatan LP2A belum diisi / masih kurang dari 15 karakter');
                        //     return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                        // }
                        // if(strlen(strip_tags($kegiatan->getCatatanBagianOrganisasi())) < 15) {
                        //     $this->setFlash('gagal', 'Catatan Bagian Organisasi belum diisi / masih kurang dari 15 karakter');
                        //     return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                        // }
                        // if(strlen(strip_tags($kegiatan->getCatatanPenyelia())) < 15) {
                        //     $this->setFlash('gagal', 'Catatan Penyelia belum diisi / masih kurang dari 15 karakter');
                        //     return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                        // }

                        $maju = TRUE;
                        $cek_sisipan = $rd->cekSisipan($unit_id, $kode_kegiatan, 4);
                        $c_rd = new Criteria();
                        $c_rd->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
                        $c_rd->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
                        $c_rd->add(DinasRincianDetailPeer::STATUS_HAPUS, FALSE);
                        $c_rd->add(DinasRincianDetailPeer::STATUS_LEVEL, 4);
                        $c_rd->add(DinasRincianDetailPeer::IS_BAPPEKO_SETUJU, TRUE);
                        $c_rd->add(DinasRincianDetailPeer::IS_PENYELIA_SETUJU, TRUE);
                        $c_rd->add(DinasRincianDetailPeer::IS_TAPD_SETUJU, TRUE);
                        $c_rd->add(DinasRincianDetailPeer::IS_BAGIAN_HUKUM_SETUJU, TRUE);
                        $c_rd->add(DinasRincianDetailPeer::IS_INSPEKTORAT_SETUJU, TRUE);
                        $c_rd->add(DinasRincianDetailPeer::IS_BADAN_KEPEGAWAIAN_SETUJU, TRUE);
                        $c_rd->add(DinasRincianDetailPeer::IS_LPPA_SETUJU, TRUE);
                        $c_rd->add(DinasRincianDetailPeer::IS_BAGIAN_ORGANISASI_SETUJU, TRUE);
                        $rs_rd_pilih = DinasRincianDetailPeer::doSelect($c_rd);
                        foreach ($rs_rd_pilih as $pilih) {
                            $pilihan[] = $pilih->getDetailNo();
                        }
                    }
                }
//UNTUK REVISI4
//                $c_rd2 = new Criteria();
//                $c_rd2->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
//                $c_rd2->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
//                $c_rd2->add(DinasRincianDetailPeer::STATUS_LEVEL, 4);
//                $c_rd2->add(DinasRincianDetailPeer::IS_TAPD_SETUJU, TRUE);
////                $c_rd2->add(DinasRincianDetailPeer::IS_BAPPEKO_SETUJU, FALSE);
//                $c_rd2->add(DinasRincianDetailPeer::IS_PENYELIA_SETUJU, TRUE);
//                $c_rd2->add(DinasRincianDetailPeer::DETAIL_NO, implode(',', $pilihan), Criteria::NOT_IN);
//                if (DinasRincianDetailPeer::doCount($c_rd2) > 0) {
//                    $maju = TRUE;
//                    $cek_sisipan = $rd->cekSisipan($unit_id, $kode_kegiatan, 4);
//                }
                foreach ($pilihan as $detail_no) {
                    $c = new Criteria();
                    $c->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
                    $c->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
                    $c->add(DinasRincianDetailPeer::DETAIL_NO, $detail_no);
                    $rs_rd = DinasRincianDetailPeer::doSelectOne($c);
                    //$nilaiBaru = round($rs_rd->getKomponenHargaAwal() * $rs_rd->getVolume() * (100 + $rs_rd->getPajak()) / 100);
                    $nilaiBaru = $rs_rd->getNilaiAnggaran();
                    $totNilaiSwakelola = 0;
                    $totNilaiKontrak = 0;
                    $totNilaiRealisasi = 0;
                    $totVolumeRealisasi = 0;
                    $totNilaiAlokasi = 0;
                    $totNilaiHps = 0;
                    $ceklelangselesaitidakaturanpembayaran = 0;
                    $lelang = 0;
                    $getCekNilaiKontrak = 0;

                    // if (sfConfig::get('app_fasilitas_cekeProject') == 'buka') {
                    //     $totNilaiAlokasi = $rd->getCekNilaiAlokasiProject($unit_id, $kode_kegiatan, $detail_no);
                    //     if (sfConfig::get('app_fasilitas_cekServer') == 'buka') {
                    //         $lelang = $rd->getCekLelang($unit_id, $kode_kegiatan, $detail_no, $rs_rd->getNilaiAnggaran());
                    //         if (sfConfig::get('app_fasilitas_cekeDelivery') == 'buka') {
                    //             $totNilaiSwakelola = $rd->getCekNilaiSwakelolaDelivery2($unit_id, $kode_kegiatan, $detail_no);
                    //             $totNilaiKontrak = $rd->getCekNilaiKontrakDelivery2($unit_id, $kode_kegiatan, $detail_no);
                    //             $totNilaiRealisasi = $rd->getCekRealisasi($unit_id, $kode_kegiatan, $detail_no);
                    //             $totVolumeRealisasi = $rd->getCekVolumeRealisasi($unit_id, $kode_kegiatan, $detail_no);
                    //             $getCekNilaiKontrak = $rd->cekKontrak($unit_id . '.' . $kode_kegiatan . '.' . $detail_no);
                    //             $totNilaiHps = $rd->getCekNilaiHPSKomponen($unit_id, $kode_kegiatan, $detail_no);
                    //             $ceklelangselesaitidakaturanpembayaran = $rd->getCekLelangTidakAdaAturanPembayaran($unit_id, $kode_kegiatan, $detail_no);
                    //         }
                    //     }
                    // }
                    if ((($nilaiBaru < $totNilaiKontrak) || ($nilaiBaru < $totNilaiSwakelola)) && !$status_bypass_validasi ) {
                        if ($totNilaiKontrak == 0) {
                            // array_push($error, 'Komponen ' . $rs_rd->getKomponenName() . ' (' . $rs_rd->getDetailName() . ') sudah terpakai di edelivery, sejumlah Rp.' . number_format($totNilaiSwakelola, 0, ',', '.'));
                            array_push($error, 'Komponen ' . $rs_rd->getKomponenName() . ' (' . $rs_rd->getDetailName() . ') sudah terpakai di edelivery, silahkan cek eDelivery terlebih dahulu');
                        } else if ($totNilaiSwakelola == 0) {
                            // array_push($error, 'Komponen ' . $rs_rd->getKomponenName() . ' (' . $rs_rd->getDetailName() . ') sudah terpakai di edelivery, sejumlah Rp.' . number_format($totNilaiKontrak, 0, ',', '.'));
                            array_push($error, 'Komponen ' . $rs_rd->getKomponenName() . ' (' . $rs_rd->getDetailName() . ') sudah terpakai di edelivery, silahkan cek eDelivery terlebih dahulu');
                        } else {
                            // array_push($error, 'Komponen ' . $rs_rd->getKomponenName() . ' (' . $rs_rd->getDetailName() . ') sudah terpakai di edelivery, sejumlah Rp.' . number_format($totNilaiKontrak, 0, ',', '.'));
                            array_push($error, 'Komponen ' . $rs_rd->getKomponenName() . ' (' . $rs_rd->getDetailName() . ') sudah terpakai di edelivery, silahkan cek eDelivery terlebih dahulu');
                        }
                        $ada_error = TRUE;

//                        $con->rollback();
//                        return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                    } else if ($nilaiBaru < $totNilaiHps  && !$status_bypass_validasi ) {
                        $ada_error = TRUE;
                        array_push($error, 'Komponen ' . $rs_rd->getKomponenName() . ' (' . $rs_rd->getDetailName() . ') lebih kecil dari Nilai HPS Per Komponen , sejumlah Rp.' . number_format($totNilaiHps, 0, ',', '.'));
//                        $this->setFlash('gagal', 'Mohon maaf , lebih kecil dari Nilai HPS Per Komponen , sejumlah Rp.' . number_format($totNilaiHps, 0, ',', '.'));
//                        $con->rollback();
//                        return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                    } else if ($ceklelangselesaitidakaturanpembayaran == 1  && !$status_bypass_validasi ) {
                        $ada_error = TRUE;
                        array_push($error, 'Proses Lelang untuk  komponen ' . $rs_rd->getKomponenName() . ' (' . $rs_rd->getDetailName() . ') telah selesai, namun Belum ada isian Aturan Pembayaran eDelivery. Silahkan mengisi Aturan Pembayaran terlebih dahulu ');
//                        $this->setFlash('gagal', 'Proses Lelang untuk  komponen ' . $rs_rd->getKomponenName() . ' (' . $rs_rd->getDetailName() . ') telah selesai, namun Belum ada isian Aturan Pembayaran eDelivery. Silahkan mengisi Aturan Pembayaran terlebih dahulu ');
//                        $con->rollback();
//                        return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                    } else if ($lelang > 0  && !$status_bypass_validasi) {
                        $ada_error = TRUE;
                        array_push($error, 'Komponen ' . $rs_rd->getKomponenName() . ' (' . $rs_rd->getDetailName() . ') Sedang dalam Proses Lelang');
//                        $this->setFlash('gagal', 'Sedang dalam Proses Lelang untuk  komponen ' . $rs_rd->getKomponenName() . ' (' . $rs_rd->getDetailName() . ')');
//                        $con->rollback();
//                        return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                    } else if ($nilaiBaru < $totNilaiRealisasi  && !$status_bypass_validasi) {
                        $ada_error = TRUE;
                        // array_push($error, 'Komponen ' . $rs_rd->getKomponenName() . ' (' . $rs_rd->getDetailName() . ') sudah terpakai di edelivery, sejumlah Rp.' . number_format($totNilaiRealisasi, 0, ',', '.'));
                        array_push($error, 'Komponen ' . $rs_rd->getKomponenName() . ' (' . $rs_rd->getDetailName() . ') sudah terpakai di edelivery, silahkan cek eDelivery terlebih dahulu');
                    } else if ( ($rs_rd->getVolume() < $totVolumeRealisasi) && ($rs_rd->getStatusLelang() != 'lock')  && !$status_bypass_validasi ) {
                        $ada_error = TRUE;
                        array_push($error, 'Komponen ' . $rs_rd->getKomponenName() . ' (' . $rs_rd->getDetailName() . ') sudah terpakai di edelivery, dengan volume sebesar' . number_format($totVolumeRealisasi, 0, ',', '.'));
                    } else if( ($getCekNilaiKontrak) > $nilaiBaru && in_array($rs_rd->getRekeningCode(), array('5.2.2.24.01', '5.2.1.02.02')) ) {
                        $ada_error = TRUE;
                        array_push($error, 'Mohon maaf, untuk komponen ini sudah ada kontrak senilai ' . number_format($getCekNilaiKontrak, 0, ',', '.') . ', silahkan cek di edelivery');
                    } else {
                        $c = new Criteria();
                        $c->add(RincianDetailPeer::UNIT_ID, $unit_id);
                        $c->addAnd(RincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
                        $c->addAnd(RincianDetailPeer::DETAIL_NO, $detail_no);
                        if ($rs_rka = RincianDetailPeer::doSelectOne($c)) {
                            $semula = $rs_rka->getNilaiAnggaran();
                            $volume_semula = $rs_rka->getVolume();
                            $satuan_semula = $rs_rka->getSatuan();
                        } else {
                            $semula = 0;
                            $volume_semula = 0;
                            $satuan_semula = '';
                        }
                        $menjadi = $rs_rd->getNilaiAnggaran();
                        $volume_menjadi = $rs_rd->getVolume();
                        $satuan_menjadi = $rs_rd->getSatuan();
                        if ($semula <> $menjadi) {
                            $tahap = DinasMasterKegiatanPeer::getTahapKegiatan($unit_id, $kode_kegiatan);
                            if($satuan_menjadi=='Paket' and $status_lelang == 'lock' and $volume_menjadi=='1')
                                    $nilai_psp=$menjadi-$semula;
                            $c_log = new Criteria();
                            $c_log->add(LogPerubahanRevisiPeer::UNIT_ID, $unit_id);
                            $c_log->addAnd(LogPerubahanRevisiPeer::KEGIATAN_CODE, $kode_kegiatan);
                            $c_log->addAnd(LogPerubahanRevisiPeer::DETAIL_NO, $detail_no);
                            $c_log->addAnd(LogPerubahanRevisiPeer::TAHAP, $tahap);

                            if ($log = LogPerubahanRevisiPeer::doSelectOne($c_log)) {
                                $log->setNilaiAnggaranSemula($semula);
                                $log->setNilaiAnggaranMenjadi($menjadi);
                                $log->setVolumeSemula($volume_semula);
                                $log->setVolumeMenjadi($volume_menjadi);
                                $log->setSatuanSemula($satuan_semula);
                                $log->setSatuanMenjadi($satuan_menjadi);
                                $log->setNilaiPsp($nilai_psp);
                                $log->setStatus(0);
                                $log->save();
                            } else {
                                $log = new LogPerubahanRevisi();
                                $log->setUnitId($unit_id);
                                $log->setKegiatanCode($kode_kegiatan);
                                $log->setDetailNo($detail_no);
                                $log->setTahap($tahap);
                                $log->setNilaiAnggaranSemula($semula);
                                $log->setNilaiAnggaranMenjadi($menjadi);
                                $log->setVolumeSemula($volume_semula);
                                $log->setVolumeMenjadi($volume_menjadi);
                                $log->setSatuanSemula($satuan_semula);
                                $log->setSatuanMenjadi($satuan_menjadi);
                                $log->setNilaiPsp($nilai_psp);
                                $log->setStatus(0);
                                $log->save();
                            }
                        }
                        if ($maju) {
                            if ($rs_rd->getStatusLevelTolak() == $rs_rd->getStatusLevel()) {
                                $rs_rd->setStatusLevelTolak(null);
                            }
                            if ($cek_sisipan) {
                                $rs_rd->setStatusSisipan(false);
                            }
                            $rs_rd->setStatusLevel(5);
                        }
                        $rs_rd->setIsBappekoSetuju(TRUE);
                        $rs_rd->save();
                    }
                }

                $kumpulan_detail_no = implode('|', $pilihan);
                $log = new LogApproval();
                $log->setUnitId($unit_id);
                $log->setTahap(LogApprovalPeer::getTahapKegiatan($unit_id, $kode_kegiatan));
                $log->setKegiatanCode($kode_kegiatan);
                $log->setUserId($this->getUser()->getNamaLogin());
                $log->setKumpulanDetailNo($kumpulan_detail_no);
                $log->setWaktu(date('Y-m-d H:i:s'));
                $log->setSebagai('Tim Anggaran (Bappeko)');
                $log->save();

                $c_kegiatan = new Criteria();
                $c_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                $c_kegiatan->addAnd(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                $kegiatan = DinasMasterKegiatanPeer::doSelectOne($c_kegiatan);
                $kegiatan->setIsBappekoSetuju(TRUE);
                if ($maju) {
                    $kegiatan->setStatusLevel(5);
                }
                if ($catatan && strlen(strip_tags($catatan)) > 0) {
                    $kegiatan->setCatatanBappeko($catatan);
                }
                $kegiatan->setVerifikasiBappeko($verifikasi);
                $kegiatan->save();
                
                // tutup dulu karena masih murni
                if ($ada_error) {
                    $this->setFlash('gagal', implode(' --- ', $error));
                    $con->rollback();
                    return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                } else {
                
                    $this->setFlash('berhasil', 'Telah berhasil diapprove Bappeko');
                    $con->commit();
                    return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                } //tutup else tidak ada error

            } elseif ($this->getRequestParameter('balik')) {
                $c_kegiatan = new Criteria();
                $c_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                $c_kegiatan->addAnd(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                $c_kegiatan->addAnd(DinasMasterKegiatanPeer::IS_PERNAH_RKA, TRUE);
                if (DinasMasterKegiatanPeer::doSelectOne($c_kegiatan)) {
                    $tabel_prev = 'prev_';
                }
//tambahan untuk yang sudah dihapus
                $query = "select a.detail_no from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail a, " . sfConfig::get('app_default_schema') . "." . $tabel_prev . "rincian_detail b 
                    where a.status_hapus=true and b.status_hapus=false and 
                    a.unit_id=b.unit_id and a.kegiatan_code=b.kegiatan_code and a.detail_no=b.detail_no and 
                    a.unit_id='$unit_id' and a.kegiatan_code='$kode_kegiatan'";
                $con = Propel::getConnection();
                $stmt = $con->prepareStatement($query);
                $rs_dihapus = $stmt->executeQuery();
                while ($rs_dihapus->next()) {
                    array_push($pilihan, $rs_dihapus->getString('detail_no'));
                }

                foreach ($pilihan as $detail_no) {
                    $c = new Criteria();
                    $c->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
                    $c->addAnd(DinasRincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
                    $c->addAnd(DinasRincianDetailPeer::DETAIL_NO, $detail_no);
                    $rs_rd = DinasRincianDetailPeer::doSelectOne($c);
                    $rs_rd->setStatusLevel(0);
                    $rs_rd->setStatusLevelTolak(4);
                    $rs_rd->setIsTapdSetuju(FALSE);
                    $rs_rd->setIsBappekoSetuju(FALSE);
                    $rs_rd->setIsPenyeliaSetuju(FALSE);
                    $rs_rd->setIsBagianHukumSetuju(FALSE);
                    $rs_rd->setIsInspektoratSetuju(FALSE);
                    $rs_rd->setIsBadanKepegawaianSetuju(FALSE);
                    $rs_rd->setIsLppaSetuju(FALSE);
                    $rs_rd->setIsBagianOrganisasiSetuju(FALSE);
                    $rs_rd->save();

                    $tahap = DinasMasterKegiatanPeer::getTahapKegiatan($unit_id, $kode_kegiatan);
                    $c_log = new Criteria();
                    $c_log->add(LogPerubahanRevisiPeer::UNIT_ID, $unit_id);
                    $c_log->addAnd(LogPerubahanRevisiPeer::KEGIATAN_CODE, $kode_kegiatan);
                    $c_log->addAnd(LogPerubahanRevisiPeer::DETAIL_NO, $detail_no);
                    $c_log->addAnd(LogPerubahanRevisiPeer::TAHAP, $tahap);

                    if ($log = LogPerubahanRevisiPeer::doSelectOne($c_log)) {
                        $log->setStatus(-1);
                        $log->save();
                    }
                }

                $catatan_tolak = $this->getRequestParameter('catatan_tolak');
                $kumpulan_detail_no = implode('|', $pilihan);
                $log = new LogApproval();
                $log->setUnitId($unit_id);
                $log->setKegiatanCode($kode_kegiatan);
                $log->setUserId($this->getUser()->getNamaLogin());
                $log->setKumpulanDetailNo($kumpulan_detail_no);
                $log->setTahap(LogApprovalPeer::getTahapKegiatan($unit_id, $kode_kegiatan));
                $log->setWaktu(date('Y-m-d H:i:s'));
                $log->setSebagai('Tim Anggaran (Bappeko)|kembali');
                $log->setCatatan($catatan_tolak);
                $log->save();

                $cri = new Criteria();
                $cri->add(RincianBappekoPeer::UNIT_ID, $unit_id);
                $cri->addAnd(RincianBappekoPeer::KODE_KEGIATAN, $kode_kegiatan);
                $cri->addAnd(RincianBappekoPeer::TAHAP, DinasMasterKegiatanPeer::getTahapKegiatan($unit_id, $kode_kegiatan));
                $jawaban = RincianBappekoPeer::doSelectOne($cri);
                $jawaban->setJawaban1Bappeko(NULL);
                $jawaban->setJawaban2Bappeko(NULL);
                $jawaban->save();

                // if ($catatan && strlen(strip_tags($catatan)) > 5) {
                $c_kegiatan = new Criteria();
                $c_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                $c_kegiatan->addAnd(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                $kegiatan = DinasMasterKegiatanPeer::doSelectOne($c_kegiatan);
                $kegiatan->setCatatanBappeko($catatan);
                $kegiatan->setStatusLevel(0);
                $kegiatan->setIsTapdSetuju(FALSE);
                $kegiatan->setIsBappekoSetuju(FALSE);
                $kegiatan->setIsPenyeliaSetuju(FALSE);
                $kegiatan->setIsBagianHukumSetuju(FALSE);
                $kegiatan->setIsInspektoratSetuju(FALSE);
                $kegiatan->setIsBadanKepegawaianSetuju(FALSE);
                $kegiatan->setIsLppaSetuju(FALSE);
                $kegiatan->setIsBagianOrganisasiSetuju(FALSE);
                $kegiatan->setUbahF1Dinas(NULL);
                $kegiatan->setSisaLelangDinas(NULL);
                $kegiatan->setUbahF1Peneliti(NULL);
                $kegiatan->setSisaLelangPeneliti(NULL);
                // $kegiatan->setVerifikasiBpkpd('');
                // $kegiatan->setVerifikasiBappeko('');
                // $kegiatan->setVerifikasiPenyelia('');
                // $kegiatan->setVerifikasiBagianHukum('');
                // $kegiatan->setVerifikasiInspektorat('');
                // $kegiatan->setVerifikasiBadanKepegawaian('');
                // $kegiatan->setVerifikasiLppa('');
                $kegiatan->save();
                // }

                $this->setFlash('berhasil', 'Telah berhasil memproses ke posisi entri');
                return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
            }
        } else {
            if ($this->getRequestParameter('proses')) {
                // F1 
                $con = Propel::getConnection();
                $c_kegiatan = new Criteria();
                $c_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                $c_kegiatan->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                $kegiatan = DinasMasterKegiatanPeer::doSelectOne($c_kegiatan);
                if (strlen(strip_tags($kegiatan->getCatatan())) > 20) {
                    if ($kegiatan->getStatusLevel() >= 4) {
                        $kegiatan->setCatatanBappeko($catatan);
                        if (!$this->getRequestParameter('ada_komponen')) {
                            $kegiatan->setIsBappekoSetuju(TRUE);
                            if ($kegiatan->getIsTapdSetuju() && $kegiatan->getIsBappekoSetuju() && $kegiatan->getIsPenyeliaSetuju() && $kegiatan->getIsBagianHukumSetuju() && $kegiatan->getIsInspektoratSetuju() && $kegiatan->getIsBadanKepegawaianSetuju() && $kegiatan->getIsLppaSetuju() && $kegiatan->getIsBagianOrganisasiSetuju()) {
                                $kegiatan->setStatusLevel(5);
                            }
                            $kegiatan->setVerifikasiBappeko($verifikasi);
                        }
                        $kegiatan->save();
                        $this->setFlash('berhasil', 'Telah berhasil mengubah catatan');
                        return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                    } else {
                        $this->setFlash('gagal', 'Catatan belum berada di posisi tim anggaran');
                        return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                    }
                } else {
                    $this->setFlash('gagal', 'Catatan harus berisi minimal 20 karakter');
                    return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                }
            }
            if ($this->getRequestParameter('simpancatatan') && $catatan && strlen(strip_tags($catatan)) > 5) {
//cuma isi catatan
                $c_kegiatan = new Criteria();
                $c_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                $c_kegiatan->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                $kegiatan = DinasMasterKegiatanPeer::doSelectOne($c_kegiatan);
                $kegiatan->setCatatanBappeko($catatan);
                $kegiatan->save();
                $this->setFlash('berhasil', 'Telah berhasil menyimpan catatan');
                return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
            } elseif ($this->getRequestParameter('balikall')) {
                $c = new Criteria();
                $c->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
                $c->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
                $c->add(DinasRincianDetailPeer::STATUS_LEVEL, 4);
                $rs_rd = DinasRincianDetailPeer::doSelect($c);
                foreach ($rs_rd as $item) {
                    $item->setStatusLevel(0);
                    $item->setStatusLevelTolak(4);
                    $item->setIsTapdSetuju(FALSE);
                    $item->setIsBappekoSetuju(FALSE);
                    $item->setIsBagianHukumSetuju(FALSE);
                    $item->setIsInspektoratSetuju(FALSE);
                    $item->setIsBadanKepegawaianSetuju(FALSE);
                    $item->setIsLppaSetuju(FALSE);
                    $item->setIsBagianOrganisasiSetuju(FALSE);
                    $item->save();
                }
                if ($catatan && strlen(strip_tags($catatan)) > 5) {
                    $c_kegiatan = new Criteria();
                    $c_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                    $c_kegiatan->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                    $kegiatan = DinasMasterKegiatanPeer::doSelectOne($c_kegiatan);
                    $kegiatan->setCatatanBappeko($catatan);
                    $kegiatan->setStatusLevel(0);
                    $kegiatan->save();
                }
                $this->setFlash('berhasil', 'Telah berhasil memproses semua komponen ke posisi entri');
                return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
            }
            $this->setFlash('gagal', 'Mohon memberi tanda cek (v) pada komponen yang disetujui');
            return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
        }
    }

    public function executeResumeRapat() {
        $unit_id = $this->unit_id = $this->getRequestParameter('unit_id');
        $kode_kegiatan = $this->kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
        $c = new Criteria();
        $c->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
        $c->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
        $master_kegiatan = DinasMasterKegiatanPeer::doSelectOne($c);
        $this->tahap = $master_kegiatan->getTahap();

        $tahap = DinasMasterKegiatanPeer::getTahapKegiatan($unit_id, $kode_kegiatan);
        $sub = "id = (select max(id) from " . sfConfig::get('app_default_schema') . ".deskripsi_resume_rapat where unit_id='$unit_id' and tahap=$tahap)";

        $c = new Criteria();
        $c->add(DeskripsiResumeRapatPeer::ID, $sub, Criteria::CUSTOM);
        if ($rs_deskripsi = DeskripsiResumeRapatPeer::doSelectOne($c)) {
            $this->deskripsi = $rs_deskripsi->getCatatan();
        }
    }

    public function executeResumeRapatMurni() {
        $unit_id = $this->unit_id = $this->getRequestParameter('unit_id');
        $kode_kegiatan = $this->kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
        $c = new Criteria();
        $c->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
        $c->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
        $master_kegiatan = DinasMasterKegiatanPeer::doSelectOne($c);
        $this->tahap = $master_kegiatan->getTahap();

        $c = new Criteria();
        $c->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
        $c->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan, Criteria::NOT_EQUAL);
        $c->addAscendingOrderByColumn(DinasMasterKegiatanPeer::KODE_KEGIATAN);
        $pilih_kegiatan = DinasMasterKegiatanPeer::doSelect($c);
        foreach ($pilih_kegiatan as $kegiatan) {
            $options[$kegiatan->getKodeKegiatan()] = $kegiatan->getKodeKegiatan() . ' - ' . $kegiatan->getNamaKegiatan();
        }
        $this->options = $options;
    }

    public function executeProsesResumeRapat() {
        $unit_id = $this->getRequestParameter('unit_id');
        $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
        $tanggal = $this->getRequestParameter('tanggal');
        $jam = $this->getRequestParameter('jam');
        if ($jam < 10)
            $jam = '0' . $jam;
        $menit = $this->getRequestParameter('menit');
        if ($menit < 10)
            $menit = '0' . $menit;
        $tempat = $this->getRequestParameter('tempat');
        $acara = $this->getRequestParameter('acara');
        $acara2 = $this->getRequestParameter('acara2');
        $pimpinan = $this->getRequestParameter('pimpinan');
        $catatan = $this->getRequestParameter('catatan');
        $con = Propel::getConnection();

//update tabel deskripsi_resume_rapat
        $tahap = DinasMasterKegiatanPeer::getTahapKegiatan($unit_id, $kode_kegiatan);
        $sub = "id = (select max(id) from " . sfConfig::get('app_default_schema') . ".deskripsi_resume_rapat where unit_id='$unit_id' and tahap=$tahap)";

        $c = new Criteria();
        $c->add(DeskripsiResumeRapatPeer::ID, $sub, Criteria::CUSTOM);
        $rs_deskripsi = null;
        if (!$rs_deskripsi = DeskripsiResumeRapatPeer::doSelectOne($c)) {
            $tahap = DinasMasterKegiatanPeer::getTahapKegiatan($unit_id, $kode_kegiatan);
            $query = " select max(id) as id from " . sfConfig::get('app_default_schema') . ".deskripsi_resume_rapat";
            $stmt = $con->prepareStatement($query);
            $rs_id = $stmt->executeQuery();
            if ($rs_id->next()) {
                $id_baru = $rs_id->getInt('id') + 1;
            } else {
                $id_baru = 1;
            }
            $token = md5(rand());
            $rs_deskripsi = new DeskripsiResumeRapat();
            $rs_deskripsi->setUnitId($unit_id);
            $rs_deskripsi->setTahap($tahap);
            $rs_deskripsi->setId($id_baru);
            $rs_deskripsi->setToken($token);
        }
        $rs_deskripsi->setTanggal($tanggal);
        $rs_deskripsi->setJam($jam);
        $rs_deskripsi->setMenit($menit);
        $rs_deskripsi->setAcara($acara);
        $rs_deskripsi->setAcara2($acara2);
        $rs_deskripsi->setTempat($tempat);
        $rs_deskripsi->setPimpinan($pimpinan);
        $rs_deskripsi->setCatatan($catatan);
        $rs_deskripsi->save();

        try {
            $con->begin();

//kegiatan dan rekening BTL
            $query = "select max(id) as id from " . sfConfig::get('app_default_schema') . ".deskripsi_resume_rapat where unit_id='$unit_id' and tahap=$tahap";
            $stmt = $con->prepareStatement($query);
            $rs_id_baru = $stmt->executeQuery();
            if ($rs_id_baru->next()) {
                $id = $rs_id_baru->getString('id');
            }

//cek kegiatan dan rekening sudah ada belum
            $query = "select kegiatan_code
                    from " . sfConfig::get('app_default_schema') . ".deskripsi_resume_rapat_kegiatan
                    where id_deskripsi_resume_rapat='$id' and kegiatan_code not in 
                    (select kode_kegiatan 
                    from " . sfConfig::get('app_default_schema') . ".dinas_master_kegiatan
                    where unit_id='$unit_id')";
            $stmt = $con->prepareStatement($query);
            $rs_kegiatan_btl = $stmt->executeQuery();
            while ($rs_kegiatan_btl->next()) {
//kalau ada delete di kegiatan dan rekening
                $kegiatan_dihapus = $rs_kegiatan_btl->getString('kegiatan_code');

                $query = "delete from " . sfConfig::get('app_default_schema') . ".deskripsi_resume_rapat_kegiatan
                    where id_deskripsi_resume_rapat='$id' and kegiatan_code='$kegiatan_dihapus'";
                $stmt = $con->prepareStatement($query);
                $stmt->executeQuery();

                $query = "delete from " . sfConfig::get('app_default_schema') . ".deskripsi_resume_rapat_rekening
                    where id_deskripsi_resume_rapat='$id' and kegiatan_code='$kegiatan_dihapus'";
                $stmt = $con->prepareStatement($query);
                $stmt->executeQuery();
            }

            $c = new Criteria();
            $c->add(KegiatanBtlPeer::UNIT_ID, $unit_id);
            $c->add(KegiatanBtlPeer::TAHAP, DinasMasterKegiatanPeer::getTahapKegiatan($unit_id, $kode_kegiatan));
            $c->addAscendingOrderByColumn(KegiatanBtlPeer::KODE_KEGIATAN);
            $rs_kegiatan_btl = KegiatanBtlPeer::doSelect($c);
            foreach ($rs_kegiatan_btl as $kegiatan) {
                $kode_kegiatan_btl = $kegiatan->getKodeKegiatan();
                $nama_kegiatan_btl = $kegiatan->getNamaKegiatan();
                $catatan_btl = $kegiatan->getCatatan();
                $rekening_code = $kegiatan->getRekeningCode();
                $rekening_name = $kegiatan->getRekeningName();
                $semula = $kegiatan->getSemula();
                $menjadi = $kegiatan->getMenjadi();

//insert kegiatan
                if ($kegiatan_prev != $kode_kegiatan_btl) {
                    $query = "insert into " . sfConfig::get('app_default_schema') . ".deskripsi_resume_rapat_kegiatan
                    values($id, '$kode_kegiatan_btl', '$nama_kegiatan_btl', '$catatan_btl')";
                    $stmt = $con->prepareStatement($query);
                    $stmt->executeQuery();
                }

//insert rekening
                $query = "insert into " . sfConfig::get('app_default_schema') . ".deskripsi_resume_rapat_rekening
                    values($id, '$kode_kegiatan_btl', '$rekening_code', '$rekening_name', $semula, $menjadi)";
                $stmt = $con->prepareStatement($query);
                $stmt->executeQuery();

                $kegiatan_prev = $kode_kegiatan_btl;
            }
            $con->commit();
        } catch (Exception $ex) {
            $con->rollback();
        }

        if ($this->getRequestParameter('print')) {

            if($tahap == 100){
                $this->redirect("report/printResumeRapatPak?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan&tanggal=$tanggal&jam=$jam&menit=$menit&tempat=$tempat&acara=$acara&acara2=$acara2&pimpinan=$pimpinan&catatan=$catatan");
            }

            $this->redirect("report/printResumeRapat?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan&tanggal=$tanggal&jam=$jam&menit=$menit&tempat=$tempat&acara=$acara&acara2=$acara2&pimpinan=$pimpinan&catatan=$catatan");
        }

        if ($this->getRequestParameter('excel')) {
            $c = new Criteria();
            $c->add(UnitKerjaPeer::UNIT_ID, $unit_id);
            $rs_unit_kerja = UnitKerjaPeer::doSelectOne($c);
            $unit_kerja = $rs_unit_kerja->getUnitName();

            $date = date_create($tanggal);
            $day = date('D', strtotime($tanggal));
            $dayList = array('Sun' => 'Minggu', 'Mon' => 'Senin', 'Tue' => 'Selasa', 'Wed' => 'Rabu', 'Thu' => 'Kamis', 'Fri' => 'Jumat', 'Sat' => 'Sabtu');
            $hari = $dayList[$day];
            $month = date('n', strtotime($tanggal));
            $monthList = array(1 => 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
            $bulan = $monthList[$month];
            $tanggalbaru = $hari . ', ' . date_format($date, 'd ') . $bulan . date_format($date, ' Y');
            try {
                $objPHPExcel = new sfPhpExcel();

                $objPHPExcel->createSheet(0);
                $objPHPExcel->setActiveSheetIndex(0);
                $objPHPExcel->getActiveSheet()->setTitle('Resume Rapat');
                $objPHPExcel->getActiveSheet()->setCellValue('A1', 'RESUME RAPAT');
                $objPHPExcel->getActiveSheet()->mergeCells('A1:H1');
                $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(14);
                $objPHPExcel->getActiveSheet()->setCellValue('A3', 'Hari/Tanggal');
                $objPHPExcel->getActiveSheet()->setCellValue('A4', 'Pukul');
                $objPHPExcel->getActiveSheet()->setCellValue('A5', 'Tempat');
                $objPHPExcel->getActiveSheet()->setCellValue('A6', 'Acara');
                $objPHPExcel->getActiveSheet()->setCellValue('A8', 'Pimpinan');
                $objPHPExcel->getActiveSheet()->setCellValue('A9', 'Nama SKPD');
                $objPHPExcel->getActiveSheet()->mergeCells('A3:B3');
                $objPHPExcel->getActiveSheet()->mergeCells('A4:B4');
                $objPHPExcel->getActiveSheet()->mergeCells('A5:B5');
                $objPHPExcel->getActiveSheet()->mergeCells('A6:B6');
                $objPHPExcel->getActiveSheet()->mergeCells('A7:B7');
                $objPHPExcel->getActiveSheet()->mergeCells('A8:B8');
                $objPHPExcel->getActiveSheet()->mergeCells('A9:B9');
                $objPHPExcel->getActiveSheet()->setCellValue('C3', ': ' . $tanggalbaru);
                $objPHPExcel->getActiveSheet()->setCellValueExplicit('C4', ": $jam.$menit WIB", PHPExcel_Cell_DataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->setCellValue('C5', ': ' . $tempat);
                $objPHPExcel->getActiveSheet()->setCellValue('C6', ': ' . $acara);
                $objPHPExcel->getActiveSheet()->setCellValue('C7', '  ' . $acara2);
                $objPHPExcel->getActiveSheet()->setCellValue('C8', ': ' . $pimpinan);
                $objPHPExcel->getActiveSheet()->setCellValue('C9', ': ' . $unit_kerja);
                $objPHPExcel->getActiveSheet()->mergeCells('C3:E3');
                $objPHPExcel->getActiveSheet()->mergeCells('C4:E4');
                $objPHPExcel->getActiveSheet()->mergeCells('C5:E5');
                $objPHPExcel->getActiveSheet()->mergeCells('C6:E6');
                $objPHPExcel->getActiveSheet()->mergeCells('C7:E7');
                $objPHPExcel->getActiveSheet()->mergeCells('C8:E8');
                $objPHPExcel->getActiveSheet()->mergeCells('C9:E9');
                $objPHPExcel->getActiveSheet()->getStyle('A3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                $objPHPExcel->getActiveSheet()->getStyle('A4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                $objPHPExcel->getActiveSheet()->getStyle('A5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                $objPHPExcel->getActiveSheet()->getStyle('A6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                $objPHPExcel->getActiveSheet()->getStyle('A7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                $objPHPExcel->getActiveSheet()->getStyle('A8')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                $objPHPExcel->getActiveSheet()->getStyle('A9')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                $objPHPExcel->getActiveSheet()->getStyle('C3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                $objPHPExcel->getActiveSheet()->getStyle('C4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                $objPHPExcel->getActiveSheet()->getStyle('C5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                $objPHPExcel->getActiveSheet()->getStyle('C6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                $objPHPExcel->getActiveSheet()->getStyle('C7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                $objPHPExcel->getActiveSheet()->getStyle('C8')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                $objPHPExcel->getActiveSheet()->getStyle('C9')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

                $kolom_ke = 11;

                $objPHPExcel->getActiveSheet()->setCellValue('A' . $kolom_ke, 'No');
                $objPHPExcel->getActiveSheet()->setCellValue('B' . $kolom_ke, 'Kode Kegiatan');
                $objPHPExcel->getActiveSheet()->setCellValue('C' . $kolom_ke, 'Nama Kegiatan');
                $objPHPExcel->getActiveSheet()->setCellValue('D' . $kolom_ke, 'Uraian Kode Rekening');
                $objPHPExcel->getActiveSheet()->setCellValue('E' . $kolom_ke, 'Semula');
                $objPHPExcel->getActiveSheet()->setCellValue('F' . $kolom_ke, 'Menjadi');
                $objPHPExcel->getActiveSheet()->setCellValue('G' . $kolom_ke, 'Selisih');
                $objPHPExcel->getActiveSheet()->setCellValue('H' . $kolom_ke, 'Alasan/Keterangan');
                $objPHPExcel->getActiveSheet()->getStyle('A' . $kolom_ke)->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->getStyle('B' . $kolom_ke)->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->getStyle('C' . $kolom_ke)->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->getStyle('D' . $kolom_ke)->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->getStyle('E' . $kolom_ke)->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->getStyle('F' . $kolom_ke)->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->getStyle('G' . $kolom_ke)->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->getStyle('H' . $kolom_ke)->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->getStyle('A' . $kolom_ke)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $objPHPExcel->getActiveSheet()->getStyle('B' . $kolom_ke)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $objPHPExcel->getActiveSheet()->getStyle('C' . $kolom_ke)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $objPHPExcel->getActiveSheet()->getStyle('D' . $kolom_ke)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $objPHPExcel->getActiveSheet()->getStyle('E' . $kolom_ke)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $objPHPExcel->getActiveSheet()->getStyle('F' . $kolom_ke)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $objPHPExcel->getActiveSheet()->getStyle('G' . $kolom_ke)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $objPHPExcel->getActiveSheet()->getStyle('H' . $kolom_ke)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

                $query = "( select a.kegiatan_code, d.nama_kegiatan, a.rekening_code, c.rekening_name, 
                        a.tot as sebelum, b.tot as setelah, (b.tot-a.tot) as selisih 
                        from (select distinct unit_id, kegiatan_code, (rekening_code), sum(nilai_anggaran) as tot 
                                from " . sfConfig::get('app_default_schema') . ".rincian_detail a
                                where unit_id = '$unit_id' and status_hapus = false and 
                                detail_no in (select distinct detail_no 
                                        from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail b
                                        where unit_id = '$unit_id' and a.kegiatan_code=b.kegiatan_code and status_hapus = false and 
                                        (status_level between 4 and 7) and is_tapd_setuju=true) 
                                group by unit_id, kegiatan_code, rekening_code 
                                order by rekening_code) a, 
                        (select distinct unit_id, kegiatan_code, (rekening_code), sum(nilai_anggaran) as tot 
                                from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail where unit_id = '$unit_id' and status_hapus = false and 
                                (status_level between 4 and 7) and is_tapd_setuju=true 
                                group by unit_id, kegiatan_code, rekening_code 
                                order by rekening_code) b, " . sfConfig::get('app_default_schema') . ".rekening c, " . sfConfig::get('app_default_schema') . ".master_kegiatan d 
                        where a.rekening_code = b.rekening_code and a.rekening_code = c.rekening_code and 
                        a.kegiatan_code=d.kode_kegiatan and a.unit_id=d.unit_id and a.kegiatan_code=b.kegiatan_code 
                        ) 
                        union all 
                        ( 
                        select c.kode_kegiatan, c.nama_kegiatan, a.rekening_code, b.rekening_name, 
                        0 as sebelum, sum(nilai_anggaran) as setelah, (sum(nilai_anggaran) - 0) as selisih 
                        from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail a, " . sfConfig::get('app_default_schema') . ".rekening b, " . sfConfig::get('app_default_schema') . ".master_kegiatan c 
                        where a.unit_id = '$unit_id' and status_hapus = false and b.rekening_code = a.rekening_code and 
                        a.unit_id=c.unit_id and a.kegiatan_code=c.kode_kegiatan and (a.status_level between 4 and 7) and a.is_tapd_setuju=true and 
                        a.kegiatan_code||'.'||a.rekening_code NOT IN ( select a.kegiatan_code||'.'||a.rekening_code
                                from (select distinct unit_id, kegiatan_code, (rekening_code), sum(nilai_anggaran) as tot 
                                        from " . sfConfig::get('app_default_schema') . ".rincian_detail a
                                        where unit_id = '$unit_id' and status_hapus = false and 
                                        detail_no in (select distinct detail_no 
                                                from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail b
                                                where unit_id = '$unit_id' and a.kegiatan_code=b.kegiatan_code and status_hapus = false and 
                                                (status_level between 4 and 7) and is_tapd_setuju=true) 
                                        group by unit_id, kegiatan_code, rekening_code 
                                        order by rekening_code) a, 
                                (select distinct unit_id, kegiatan_code, (rekening_code), sum(nilai_anggaran) as tot 
                                        from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail where unit_id = '$unit_id' and status_hapus = false and 
                                        (status_level between 4 and 7) and is_tapd_setuju=true 
                                        group by unit_id, kegiatan_code, rekening_code 
                                        order by rekening_code) b, " . sfConfig::get('app_default_schema') . ".rekening c, " . sfConfig::get('app_default_schema') . ".master_kegiatan d 
                                where a.rekening_code = b.rekening_code and a.rekening_code = c.rekening_code and 
                                a.kegiatan_code=d.kode_kegiatan and a.unit_id=d.unit_id and a.kegiatan_code=b.kegiatan_code 
                                ) 
                        group by c.kode_kegiatan, c.nama_kegiatan, a.rekening_code, b.rekening_name 
                        order by a.rekening_code 
                        ) order by kegiatan_code, rekening_code";

                $stmt = $con->prepareStatement($query);
                $rs = $stmt->executeQuery();
                $rek_keg = array();
                while ($rs->next()) {
                    array_push($rek_keg, '\'' . $rs->getString('kegiatan_code') . '-' . $rs->getString('rekening_code') . '\'');
                }
                $kumpulan_rek_keg = implode(', ', $rek_keg);
                if (strlen($kumpulan_rek_keg) < 1) {
                    $kumpulan_rek_keg = 'null';
                }

                $query = "(select  a.kegiatan_code, d.nama_kegiatan, a.rekening_code, c.rekening_name, 
                        a.tot as sebelum, b.tot as setelah, (b.tot-a.tot) as selisih 
                        from (select distinct unit_id, kegiatan_code, rekening_code, sum(nilai_anggaran) as tot 
                                from " . sfConfig::get('app_default_schema') . ".rincian_detail
                                where unit_id = '$unit_id' and status_hapus = false and 
                                kegiatan_code||'-'||rekening_code in ($kumpulan_rek_keg)
                                group by unit_id, kegiatan_code, rekening_code) a, 
                        (select distinct unit_id, kegiatan_code, rekening_code, sum(nilai_anggaran) as tot 
                                from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail 
                                where unit_id = '$unit_id' and status_hapus = false and 
                                kegiatan_code||'-'||rekening_code in ($kumpulan_rek_keg)
                                group by unit_id, kegiatan_code, rekening_code) b,
                        " . sfConfig::get('app_default_schema') . ".rekening c, " . sfConfig::get('app_default_schema') . ".master_kegiatan d
                        where a.rekening_code = b.rekening_code and a.rekening_code = c.rekening_code and 
                        a.kegiatan_code=d.kode_kegiatan and a.unit_id=d.unit_id and a.kegiatan_code=b.kegiatan_code
                        )
                        union all
                        (
                        select c.kode_kegiatan, c.nama_kegiatan, a.rekening_code, b.rekening_name, 
                        0 as sebelum, sum(nilai_anggaran) as setelah, (sum(nilai_anggaran) - 0) as selisih 
                        from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail a, " . sfConfig::get('app_default_schema') . ".rekening b, " . sfConfig::get('app_default_schema') . ".master_kegiatan c 
                        where a.unit_id = '$unit_id' and status_hapus = false and b.rekening_code = a.rekening_code and 
                        a.unit_id=c.unit_id and a.kegiatan_code=c.kode_kegiatan and 
                        a.kegiatan_code||'-'||a.rekening_code IN ($kumpulan_rek_keg) and
                        a.kegiatan_code||'-'||a.rekening_code NOT IN (select  a.kegiatan_code||'-'||a.rekening_code
                                                from (select distinct unit_id, kegiatan_code, rekening_code, sum(nilai_anggaran) as tot 
                                                        from " . sfConfig::get('app_default_schema') . ".rincian_detail
                                                        where unit_id = '$unit_id' and status_hapus = false and 
                                                        kegiatan_code||'-'||rekening_code in ($kumpulan_rek_keg)
                                                        group by unit_id, kegiatan_code, rekening_code) a, 
                                                (select distinct unit_id, kegiatan_code, rekening_code, sum(nilai_anggaran) as tot 
                                                        from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail 
                                                        where unit_id = '$unit_id' and status_hapus = false and 
                                                        kegiatan_code||'-'||rekening_code in ($kumpulan_rek_keg)
                                                        group by unit_id, kegiatan_code, rekening_code) b,
                                                " . sfConfig::get('app_default_schema') . ".rekening c, " . sfConfig::get('app_default_schema') . ".master_kegiatan d
                                                where a.rekening_code = b.rekening_code and a.rekening_code = c.rekening_code and 
                                                a.kegiatan_code=d.kode_kegiatan and a.unit_id=d.unit_id and a.kegiatan_code=b.kegiatan_code
                                                )
                        group by c.kode_kegiatan, c.nama_kegiatan, a.rekening_code, b.rekening_name 
                        ) order by kegiatan_code, rekening_code";

//echo $query; exit;
                $con = Propel::getConnection();
                $stmt = $con->prepareStatement($query);
                $rs = $stmt->executeQuery();
//pengambilan data sesuai dengan rekening yang diambil

                $posisi_terakhir = 0;
                while ($rs->next()) {
                    $kolom_ke++;
                    if ($prev_kode_kegiatan != $rs->getString('kegiatan_code')) {
                        $c = new Criteria();
                        $c->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                        $c->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $rs->getString('kegiatan_code'));
                        $rs_catatan = DinasMasterKegiatanPeer::doSelectOne($c);
                        $catatan_pembahasan = strip_tags(str_replace("<br />", "\n", $rs_catatan->getCatatanPembahasan()));
                        $objPHPExcel->getActiveSheet()->setCellValueExplicit('A' . $kolom_ke, ++$no, PHPExcel_Cell_DataType::TYPE_STRING);
                        $objPHPExcel->getActiveSheet()->setCellValue('B' . $kolom_ke, $rs->getString('kegiatan_code'));
                        $objPHPExcel->getActiveSheet()->setCellValue('C' . $kolom_ke, $rs->getString('nama_kegiatan'));
                        $objPHPExcel->getActiveSheet()->setCellValue('H' . $kolom_ke, $catatan_pembahasan);
                        if ($posisi_terakhir) {
                            $objPHPExcel->getActiveSheet()->mergeCells("A$posisi_terakhir:A" . ($kolom_ke - 1));
                            $objPHPExcel->getActiveSheet()->mergeCells("B$posisi_terakhir:B" . ($kolom_ke - 1));
                            $objPHPExcel->getActiveSheet()->mergeCells("C$posisi_terakhir:C" . ($kolom_ke - 1));
                            $objPHPExcel->getActiveSheet()->mergeCells("H$posisi_terakhir:H" . ($kolom_ke - 1));
                            $objPHPExcel->getActiveSheet()->getStyle('A' . $posisi_terakhir)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                            $objPHPExcel->getActiveSheet()->getStyle('B' . $posisi_terakhir)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                            $objPHPExcel->getActiveSheet()->getStyle('C' . $posisi_terakhir)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                            $objPHPExcel->getActiveSheet()->getStyle('H' . $posisi_terakhir)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                        }
                        $posisi_terakhir = $kolom_ke;
                    }

//$objPHPExcel->getActiveSheet()->setCellValue('D' . $kolom_ke, $rs->getString('rekening_code') . ' ' . $rs->getString('rekening_name'));
                    $objPHPExcel->getActiveSheet()->setCellValue('D' . $kolom_ke, $rs->getString('rekening_name'));
                    $objPHPExcel->getActiveSheet()->setCellValue('E' . $kolom_ke, number_format($rs->getString('sebelum'), 0, '.', ','));
                    $objPHPExcel->getActiveSheet()->getStyle('E' . $kolom_ke)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                    $objPHPExcel->getActiveSheet()->setCellValue('F' . $kolom_ke, number_format($rs->getString('setelah'), 0, '.', ','));
                    $objPHPExcel->getActiveSheet()->getStyle('F' . $kolom_ke)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                    $objPHPExcel->getActiveSheet()->setCellValue('G' . $kolom_ke, number_format($rs->getString('selisih'), 0, '.', ','));
                    $objPHPExcel->getActiveSheet()->getStyle('G' . $kolom_ke)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

                    $prev_kode_kegiatan = $rs->getString('kegiatan_code');
                }
                $query = "select kode_kegiatan as kegiatan_code, nama_kegiatan, 0 as rekening_code, 0 as rekening_name, 
                        0 as sebelum, 0 as setelah, 0 as selisih 
                        from " . sfConfig::get('app_default_schema') . ".dinas_master_kegiatan where unit_id='$unit_id' and 
                        (status_level between 4 and 7) and is_tapd_setuju=true and kode_kegiatan not in ($cekf1)";
                $stmt = $con->prepareStatement($query);
                $rs = $stmt->executeQuery();
                while ($rs->next()) {
                    $kolom_ke++;
                    if ($prev_kode_kegiatan != $rs->getString('kegiatan_code')) {
                        $c = new Criteria();
                        $c->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                        $c->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $rs->getString('kegiatan_code'));
                        $rs_catatan = DinasMasterKegiatanPeer::doSelectOne($c);
                        $catatan_pembahasan = strip_tags(str_replace("<br />", "\n", $rs_catatan->getCatatanPembahasan()));
                        $objPHPExcel->getActiveSheet()->setCellValueExplicit('A' . $kolom_ke, ++$no, PHPExcel_Cell_DataType::TYPE_STRING);
                        $objPHPExcel->getActiveSheet()->setCellValue('B' . $kolom_ke, $rs->getString('kegiatan_code'));
                        $objPHPExcel->getActiveSheet()->setCellValue('C' . $kolom_ke, $rs->getString('nama_kegiatan'));
                        $objPHPExcel->getActiveSheet()->setCellValue('H' . $kolom_ke, $catatan_pembahasan);
                        if ($posisi_terakhir) {
                            $objPHPExcel->getActiveSheet()->mergeCells("A$posisi_terakhir:A" . ($kolom_ke - 1));
                            $objPHPExcel->getActiveSheet()->mergeCells("B$posisi_terakhir:B" . ($kolom_ke - 1));
                            $objPHPExcel->getActiveSheet()->mergeCells("C$posisi_terakhir:C" . ($kolom_ke - 1));
                            $objPHPExcel->getActiveSheet()->mergeCells("H$posisi_terakhir:H" . ($kolom_ke - 1));
                            $objPHPExcel->getActiveSheet()->getStyle('A' . $posisi_terakhir)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                            $objPHPExcel->getActiveSheet()->getStyle('B' . $posisi_terakhir)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                            $objPHPExcel->getActiveSheet()->getStyle('C' . $posisi_terakhir)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                            $objPHPExcel->getActiveSheet()->getStyle('H' . $posisi_terakhir)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                        }
                        $posisi_terakhir = $kolom_ke;
                    }
                    $objPHPExcel->getActiveSheet()->getStyle('E' . $kolom_ke)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                    $objPHPExcel->getActiveSheet()->getStyle('F' . $kolom_ke)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                    $objPHPExcel->getActiveSheet()->getStyle('G' . $kolom_ke)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                    $prev_kode_kegiatan = $rs->getString('kegiatan_code');
                }
                $objPHPExcel->getActiveSheet()->mergeCells("A$posisi_terakhir:A" . ($kolom_ke));
                $objPHPExcel->getActiveSheet()->mergeCells("B$posisi_terakhir:B" . ($kolom_ke));
                $objPHPExcel->getActiveSheet()->mergeCells("C$posisi_terakhir:C" . ($kolom_ke));
                $objPHPExcel->getActiveSheet()->mergeCells("H$posisi_terakhir:H" . ($kolom_ke));
                $objPHPExcel->getActiveSheet()->getStyle('A' . $posisi_terakhir)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $objPHPExcel->getActiveSheet()->getStyle('B' . $posisi_terakhir)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $objPHPExcel->getActiveSheet()->getStyle('C' . $posisi_terakhir)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $objPHPExcel->getActiveSheet()->getStyle('H' . $posisi_terakhir)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $kolom_ke++;
                $objPHPExcel->getActiveSheet()->setCellValue('A' . ++$kolom_ke, $catatan);
                $objPHPExcel->getActiveSheet()->mergeCells("A$kolom_ke:H$kolom_ke");
                $kolom_ke++;
                $objPHPExcel->getActiveSheet()->setCellValue('C' . ++$kolom_ke, 'Asisten 1 :');
                $objPHPExcel->getActiveSheet()->setCellValue('E' . $kolom_ke, 'Bappeko :');
                $objPHPExcel->getActiveSheet()->mergeCells("E$kolom_ke:F$kolom_ke");
                $objPHPExcel->getActiveSheet()->getStyle("C$kolom_ke")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                $objPHPExcel->getActiveSheet()->getStyle("E$kolom_ke")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                $objPHPExcel->getActiveSheet()->setCellValue('C' . ++$kolom_ke, 'Asisten 2 :');
                $objPHPExcel->getActiveSheet()->setCellValue('E' . $kolom_ke, 'BPKPD :');
                $objPHPExcel->getActiveSheet()->mergeCells("E$kolom_ke:F$kolom_ke");
                $objPHPExcel->getActiveSheet()->getStyle("C$kolom_ke")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                $objPHPExcel->getActiveSheet()->getStyle("E$kolom_ke")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                $objPHPExcel->getActiveSheet()->setCellValue('C' . ++$kolom_ke, 'Asisten 3 :');
                $objPHPExcel->getActiveSheet()->setCellValue('E' . $kolom_ke, 'Bagian Administrasi Pembangunan :');
                $objPHPExcel->getActiveSheet()->mergeCells("E$kolom_ke:F$kolom_ke");
                $objPHPExcel->getActiveSheet()->getStyle("C$kolom_ke")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                $objPHPExcel->getActiveSheet()->getStyle("E$kolom_ke")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
//$objPHPExcel->getActiveSheet()->setCellValue('C' . ++$kolom_ke, 'Asisten 4 :');
                ++$kolom_ke;
                $objPHPExcel->getActiveSheet()->setCellValue('E' . $kolom_ke, $unit_kerja . ' :');
                $objPHPExcel->getActiveSheet()->mergeCells("E$kolom_ke:F$kolom_ke");
                $objPHPExcel->getActiveSheet()->getStyle("C$kolom_ke")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                $objPHPExcel->getActiveSheet()->getStyle("E$kolom_ke")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

                $objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
                header('Content-Type: application/vnd.ms-excel');
                header('Content-Disposition: attachment;filename=resume_rapat_' . date('d-m-Y') . '.xls');
                header('Cache-Control: max-age=0');
                $objWriter->save('php://output');
                exit();
            } catch (Exception $ex) {
                echo $ex->getMessage();
                exit;
            }
        }
    }

    public function executeProsesResumeRapatMurni() {
        $unit_id = $this->getRequestParameter('unit_id');
        $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');

        $arr_kegiatan = $this->getRequestParameter('arr_kegiatan');
        foreach ($arr_kegiatan as $value) {
            if ($kode_kegiatan != $value)
                $kode_kegiatan .= ',' . $value;
        }

        $tempat = $this->getRequestParameter('tempat');
        $acara = $this->getRequestParameter('acara');
        $acara2 = $this->getRequestParameter('acara2');

        $this->redirect("report/printResumeRapatMurni?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan&tempat=$tempat&acara=$acara&acara2=$acara2");
    }

    public function executeListRevisi() {

        $menu = $this->getRequestParameter('menu');
        $this->menu = $menu;
        $this->processSort();
        $this->processFilters();
        $this->filters = $this->getUser()->getAttributeHolder()->getAll('sf_admin/master_kegiatan/filters');

        // $this->pager = new sfPropelPager('DinasMasterKegiatan', 20);

        $user_id = $this->getUser()->getNamaUser();
        $c1 = new Criteria();
        $c1->add(SchemaAksesV2Peer::USER_ID, $user_id);
        $c1->add(SchemaAksesV2Peer::SCHEMA_ID, 2);
        $level = SchemaAksesV2Peer::doSelectOne($c1);
        if ($level->getLevelId() == 16) {
            $c = new Criteria();
            $c->add(UserHandleV2Peer::USER_ID, $user_id);
            $c->add(UserHandleV2Peer::SCHEMA_ID, 2);
            $es = UserHandleV2Peer::doSelect($c);
            $unit_kerja = Array();
            foreach ($es as $x) {
                $unit_kerja[] = $x->getUnitId();
            }
        } else {
            $es = UnitKerjaPeer::doSelect(new Criteria);
            $unit_kerja = Array();
            foreach ($es as $x) {
                $unit_kerja[] = $x->getUnitId();
            }
        }
        $this->unit_kerja = $unit_kerja;
        // $c = new Criteria();
        // $this->addSortCriteria($c);
        // $c->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_kerja[0], Criteria::ILIKE);
        // for ($i = 1; $i < count($unit_kerja); $i++) {
        //     $c->addOr(DinasMasterKegiatanPeer::UNIT_ID, $unit_kerja[$i], Criteria::ILIKE);
        // }

        // $unit_id = $this->getRequestParameter('unit_id');
        // if (isset($unit_id)) {
        //     $c->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
        // }
        // $c->addAscendingOrderByColumn(DinasMasterKegiatanPeer::KODE_KEGIATAN);
        // $this->addFiltersCriteria($c);

        // filter tahap

        if (isset($this->filters['tahap']) && $this->filters['tahap'] == 'pakbp') {
            $this->pager = new sfPropelPager('PakBukuPutihMasterKegiatan', 20);
            $c = new Criteria();
            // $this->addSortCriteria($c);
            // $es = UnitKerjaPeer::doSelect(new Criteria);
            // $this->satuan_kerja = $es;
            // $unit_kerja = Array();
            // foreach ($es as $x) {
            //     $unit_kerja[] = $x->getUnitId();
            // }

            $c->add(PakBukuPutihMasterKegiatanPeer::UNIT_ID, $unit_kerja[0], Criteria::ILIKE);
            for ($i = 1; $i < count($unit_kerja); $i++) {
                $c->addOr(PakBukuPutihMasterKegiatanPeer::UNIT_ID, $unit_kerja[$i], Criteria::ILIKE);
            }

            $unit_id = $this->getRequestParameter('unit_id');
            if (isset($unit_id)) {
                $c->add(PakBukuPutihMasterKegiatanPeer::UNIT_ID, $unit_id);
            }
            $c->addAscendingOrderByColumn(PakBukuPutihMasterKegiatanPeer::KODE_KEGIATAN);
        } elseif (isset($this->filters['tahap']) && $this->filters['tahap'] == 'pakbb') {
            $this->pager = new sfPropelPager('PakBukuBiruMasterKegiatan', 20);
            $c = new Criteria();
            // $this->addSortCriteria($c);
            // $es = UnitKerjaPeer::doSelect(new Criteria);
            // $this->satuan_kerja = $es;
            // $unit_kerja = Array();
            // foreach ($es as $x) {
            //     $unit_kerja[] = $x->getUnitId();
            // }

            $c->add(PakBukuBiruMasterKegiatanPeer::UNIT_ID, $unit_kerja[0], Criteria::ILIKE);
            for ($i = 1; $i < count($unit_kerja); $i++) {
                $c->addOr(PakBukuBiruMasterKegiatanPeer::UNIT_ID, $unit_kerja[$i], Criteria::ILIKE);
            }

            $unit_id = $this->getRequestParameter('unit_id');
            if (isset($unit_id)) {
                $c->add(PakBukuBiruMasterKegiatanPeer::UNIT_ID, $unit_id);
            }
            $c->addAscendingOrderByColumn(PakBukuBiruMasterKegiatanPeer::KODE_KEGIATAN);
        } elseif (isset($this->filters['tahap']) && $this->filters['tahap'] == 'murni') {
            $this->pager = new sfPropelPager('MurniMasterKegiatan', 20);
            $c = new Criteria();
            // $this->addSortCriteria($c);
            // $es = UnitKerjaPeer::doSelect(new Criteria);
            // $this->satuan_kerja = $es;
            // $unit_kerja = Array();
            // foreach ($es as $x) {
            //     $unit_kerja[] = $x->getUnitId();
            // }

            $c->add(MurniMasterKegiatanPeer::UNIT_ID, $unit_kerja[0], Criteria::ILIKE);
            for ($i = 1; $i < count($unit_kerja); $i++) {
                $c->addOr(MurniMasterKegiatanPeer::UNIT_ID, $unit_kerja[$i], Criteria::ILIKE);
            }

            $unit_id = $this->getRequestParameter('unit_id');
            if (isset($unit_id)) {
                $c->add(MurniMasterKegiatanPeer::UNIT_ID, $unit_id);
            }

            if($menu == 'Pendapatan'){
                $c->add(MurniMasterKegiatanPeer::IS_BTL,TRUE);
            }else{
                $c->add(MurniMasterKegiatanPeer::IS_BTL,FALSE);
            }

            if(isset($this->filters['id']) && $this->filters['id'] != ''){
                $c->add(MurniMasterKegiatanPeer::KODE_KEGIATAN, $this->filters['id'], Criteria::EQUAL);
            }
            
            $load_id = array();
            if(isset($this->filters['unit_id']) && $this->filters['unit_id'] != ''){
                $kode_id = new Criteria();
                $kode_id->add(MurniMasterKegiatanPeer::UNIT_ID, $this->filters['unit_id'], Criteria::EQUAL);
                if($menu == 'Pendapatan'){
                    $kode_id->add(MurniMasterKegiatanPeer::IS_BTL,TRUE);
                }else{
                    $kode_id->add(MurniMasterKegiatanPeer::IS_BTL,FALSE);
                }
                $kode_id->addAscendingOrderByColumn('CAST('.MurniMasterKegiatanPeer::KODE_KEGIATAN.' AS INT)');
                $load_id = MurniMasterKegiatanPeer::doSelect($kode_id);
            }
            $this->load_id = $load_id;

            $c->addAscendingOrderByColumn('CAST('.MurniMasterKegiatanPeer::KODE_KEGIATAN.' AS INT)');
        } elseif (isset($this->filters['tahap']) && $this->filters['tahap'] == 'murnibp') {
            $this->pager = new sfPropelPager('MurniBukuPutihMasterKegiatan', 20);
            $c = new Criteria();
            // $this->addSortCriteria($c);
            // $es = UnitKerjaPeer::doSelect(new Criteria);
            // $this->satuan_kerja = $es;
            // $unit_kerja = Array();
            // foreach ($es as $x) {
            //     $unit_kerja[] = $x->getUnitId();
            // }

            $c->add(MurniBukuPutihMasterKegiatanPeer::UNIT_ID, $unit_kerja[0], Criteria::ILIKE);
            for ($i = 1; $i < count($unit_kerja); $i++) {
                $c->addOr(MurniBukuPutihMasterKegiatanPeer::UNIT_ID, $unit_kerja[$i], Criteria::ILIKE);
            }

            $unit_id = $this->getRequestParameter('unit_id');
            if (isset($unit_id)) {
                $c->add(MurniBukuPutihMasterKegiatanPeer::UNIT_ID, $unit_id);
            }
            $c->addAscendingOrderByColumn(MurniBukuPutihMasterKegiatanPeer::KODE_KEGIATAN);
        } elseif (isset($this->filters['tahap']) && $this->filters['tahap'] == 'murnibb') {
            $this->pager = new sfPropelPager('MurniBukuBiruMasterKegiatan', 20);
            $c = new Criteria();
            // $this->addSortCriteria($c);
            // $es = UnitKerjaPeer::doSelect(new Criteria);
            // $this->satuan_kerja = $es;
            // $unit_kerja = Array();
            // foreach ($es as $x) {
            //     $unit_kerja[] = $x->getUnitId();
            // }

            $c->add(MurniBukuBiruMasterKegiatanPeer::UNIT_ID, $unit_kerja[0], Criteria::ILIKE);
            for ($i = 1; $i < count($unit_kerja); $i++) {
                $c->addOr(MurniBukuBiruMasterKegiatanPeer::UNIT_ID, $unit_kerja[$i], Criteria::ILIKE);
            }

            $unit_id = $this->getRequestParameter('unit_id');
            if (isset($unit_id)) {
                $c->add(MurniBukuBiruMasterKegiatanPeer::UNIT_ID, $unit_id);
            }
            $c->addAscendingOrderByColumn(MurniBukuBiruMasterKegiatanPeer::KODE_KEGIATAN);
        } elseif (isset($this->filters['tahap']) && $this->filters['tahap'] == 'murnibbpraevagub') {
            $this->pager = new sfPropelPager('MurniBukubiruPraevagubMasterKegiatan', 20);
            $c = new Criteria();
            // $this->addSortCriteria($c);
            // $es = UnitKerjaPeer::doSelect(new Criteria);
            // $this->satuan_kerja = $es;
            // $unit_kerja = Array();
            // foreach ($es as $x) {
            //     $unit_kerja[] = $x->getUnitId();
            // }

            $c->add(MurniBukubiruPraevagubMasterKegiatanPeer::UNIT_ID, $unit_kerja[0], Criteria::ILIKE);
            for ($i = 1; $i < count($unit_kerja); $i++) {
                $c->addOr(MurniBukubiruPraevagubMasterKegiatanPeer::UNIT_ID, $unit_kerja[$i], Criteria::ILIKE);
            }

            $unit_id = $this->getRequestParameter('unit_id');
            if (isset($unit_id)) {
                $c->add(MurniBukubiruPraevagubMasterKegiatanPeer::UNIT_ID, $unit_id);
            }
            $c->addAscendingOrderByColumn(MurniBukubiruPraevagubMasterKegiatanPeer::KODE_KEGIATAN);
        } elseif (isset($this->filters['tahap']) && $this->filters['tahap'] == 'revisi1') {
            $this->pager = new sfPropelPager('Revisi1MasterKegiatan', 20);
            $c = new Criteria();
            // $this->addSortCriteria($c);
            // $es = UnitKerjaPeer::doSelect(new Criteria);
            // $this->satuan_kerja = $es;
            // $unit_kerja = Array();
            // foreach ($es as $x) {
            //     $unit_kerja[] = $x->getUnitId();
            // }

            $c->add(Revisi1MasterKegiatanPeer::UNIT_ID, $unit_kerja[0], Criteria::ILIKE);
            for ($i = 1; $i < count($unit_kerja); $i++) {
                $c->addOr(Revisi1MasterKegiatanPeer::UNIT_ID, $unit_kerja[$i], Criteria::ILIKE);
            }

            $unit_id = $this->getRequestParameter('unit_id');
            if (isset($unit_id)) {
                $c->add(Revisi1MasterKegiatanPeer::UNIT_ID, $unit_id);
            }
            $c->addAscendingOrderByColumn(Revisi1MasterKegiatanPeer::KODE_KEGIATAN);
        } elseif (isset($this->filters['tahap']) && $this->filters['tahap'] == 'revisi1_1') {
            $this->pager = new sfPropelPager('Revisi1bMasterKegiatan', 20);
            $c = new Criteria();
            // $this->addSortCriteria($c);
            // $es = UnitKerjaPeer::doSelect(new Criteria);
            // $this->satuan_kerja = $es;
            // $unit_kerja = Array();
            // foreach ($es as $x) {
            //     $unit_kerja[] = $x->getUnitId();
            // }

            $c->add(Revisi1bMasterKegiatanPeer::UNIT_ID, $unit_kerja[0], Criteria::ILIKE);
            for ($i = 1; $i < count($unit_kerja); $i++) {
                $c->addOr(Revisi1bMasterKegiatanPeer::UNIT_ID, $unit_kerja[$i], Criteria::ILIKE);
            }

            $unit_id = $this->getRequestParameter('unit_id');
            if (isset($unit_id)) {
                $c->add(Revisi1bMasterKegiatanPeer::UNIT_ID, $unit_id);
            }
            $c->addAscendingOrderByColumn(Revisi1bMasterKegiatanPeer::KODE_KEGIATAN);
        } elseif (isset($this->filters['tahap']) && $this->filters['tahap'] == 'revisi2') {
            $this->pager = new sfPropelPager('Revisi2MasterKegiatan', 20);
            $c = new Criteria();
            // $this->addSortCriteria($c);
            // $es = UnitKerjaPeer::doSelect(new Criteria);
            // $this->satuan_kerja = $es;
            // $unit_kerja = Array();
            // foreach ($es as $x) {
            //     $unit_kerja[] = $x->getUnitId();
            // }

            $c->add(Revisi2MasterKegiatanPeer::UNIT_ID, $unit_kerja[0], Criteria::ILIKE);
            for ($i = 1; $i < count($unit_kerja); $i++) {
                $c->addOr(Revisi2MasterKegiatanPeer::UNIT_ID, $unit_kerja[$i], Criteria::ILIKE);
            }

            $unit_id = $this->getRequestParameter('unit_id');
            if (isset($unit_id)) {
                $c->add(Revisi2MasterKegiatanPeer::UNIT_ID, $unit_id);
            }
            $c->addAscendingOrderByColumn(Revisi2MasterKegiatanPeer::KODE_KEGIATAN);
        } elseif (isset($this->filters['tahap']) && $this->filters['tahap'] == 'revisi2_1') {
            $this->pager = new sfPropelPager('Revisi21MasterKegiatan', 20);
            $c = new Criteria();
            // $this->addSortCriteria($c);
            // $es = UnitKerjaPeer::doSelect(new Criteria);
            // $this->satuan_kerja = $es;
            // $unit_kerja = Array();
            // foreach ($es as $x) {
            //     $unit_kerja[] = $x->getUnitId();
            // }

            $c->add(Revisi21MasterKegiatanPeer::UNIT_ID, $unit_kerja[0], Criteria::ILIKE);
            for ($i = 1; $i < count($unit_kerja); $i++) {
                $c->addOr(Revisi21MasterKegiatanPeer::UNIT_ID, $unit_kerja[$i], Criteria::ILIKE);
            }

            $unit_id = $this->getRequestParameter('unit_id');
            if (isset($unit_id)) {
                $c->add(Revisi21MasterKegiatanPeer::UNIT_ID, $unit_id);
            }
            $c->addAscendingOrderByColumn(Revisi21MasterKegiatanPeer::KODE_KEGIATAN);
        } elseif (isset($this->filters['tahap']) && $this->filters['tahap'] == 'revisi2_2') {
            $this->pager = new sfPropelPager('Revisi22MasterKegiatan', 20);
            $c = new Criteria();
            // $this->addSortCriteria($c);
            // $es = UnitKerjaPeer::doSelect(new Criteria);
            // $this->satuan_kerja = $es;
            // $unit_kerja = Array();
            // foreach ($es as $x) {
            //     $unit_kerja[] = $x->getUnitId();
            // }

            $c->add(Revisi22MasterKegiatanPeer::UNIT_ID, $unit_kerja[0], Criteria::ILIKE);
            for ($i = 1; $i < count($unit_kerja); $i++) {
                $c->addOr(Revisi22MasterKegiatanPeer::UNIT_ID, $unit_kerja[$i], Criteria::ILIKE);
            }

            $unit_id = $this->getRequestParameter('unit_id');
            if (isset($unit_id)) {
                $c->add(Revisi22MasterKegiatanPeer::UNIT_ID, $unit_id);
            }
            $c->addAscendingOrderByColumn(Revisi22MasterKegiatanPeer::KODE_KEGIATAN);
        } 
        //revisi 3 start 21-08-2017 yogie =======================
        elseif (isset($this->filters['tahap']) && $this->filters['tahap'] == 'revisi3') {
//            echo "a";die();
            $this->pager = new sfPropelPager('Revisi3MasterKegiatan', 20);
            //print_r($this->pager);die();           
            $c = new Criteria();
            // $this->addSortCriteria($c);
            // $es = UnitKerjaPeer::doSelect(new Criteria);
            // $this->satuan_kerja = $es;
            // $unit_kerja = Array();
            // foreach ($es as $x) {
            //     $unit_kerja[] = $x->getUnitId();
            // }
            //print_r($unit_kerja);die();

            $c->add(Revisi3MasterKegiatanPeer::UNIT_ID, $unit_kerja[0], Criteria::ILIKE);
            for ($i = 1; $i < count($unit_kerja); $i++) {
                $c->addOr(Revisi3MasterKegiatanPeer::UNIT_ID, $unit_kerja[$i], Criteria::ILIKE);
            }

            $unit_id = $this->getRequestParameter('unit_id');
//            echo $this->getRequestParameter('unit_id');die();
            if (isset($unit_id)) {
//                echo $unit_id;die();
                $c->add(Revisi3MasterKegiatanPeer::UNIT_ID, $unit_id);
            }
//            echo $unit_id;die();
            $c->addAscendingOrderByColumn(Revisi3MasterKegiatanPeer::KODE_KEGIATAN);
        }
        elseif (isset($this->filters['tahap']) && $this->filters['tahap'] == 'revisi3_1') {
//            echo "a";die();
            $this->pager = new sfPropelPager('Revisi31MasterKegiatan', 20);
            //print_r($this->pager);die();           
            $c = new Criteria();
            // $this->addSortCriteria($c);
            // $es = UnitKerjaPeer::doSelect(new Criteria);
            // $this->satuan_kerja = $es;
            // $unit_kerja = Array();
            // foreach ($es as $x) {
            //     $unit_kerja[] = $x->getUnitId();
            // }
            //print_r($unit_kerja);die();

            $c->add(Revisi31MasterKegiatanPeer::UNIT_ID, $unit_kerja[0], Criteria::ILIKE);
            for ($i = 1; $i < count($unit_kerja); $i++) {
                $c->addOr(Revisi31MasterKegiatanPeer::UNIT_ID, $unit_kerja[$i], Criteria::ILIKE);
            }

            $unit_id = $this->getRequestParameter('unit_id');
//            echo $this->getRequestParameter('unit_id');die();
            if (isset($unit_id)) {
//                echo $unit_id;die();
                $c->add(Revisi31MasterKegiatanPeer::UNIT_ID, $unit_id);
            }
//            echo $unit_id;die();
            $c->addAscendingOrderByColumn(Revisi31MasterKegiatanPeer::KODE_KEGIATAN);
        }
        //revisi 3 end 21-08-2017 yogie =======================
        elseif (isset($this->filters['tahap']) && $this->filters['tahap'] == 'revisi4') {
            $this->pager = new sfPropelPager('Revisi4MasterKegiatan', 20);
            $c = new Criteria();
            // $this->addSortCriteria($c);
            // $es = UnitKerjaPeer::doSelect(new Criteria);
            // $this->satuan_kerja = $es;
            // $unit_kerja = Array();
            // foreach ($es as $x) {
            //     $unit_kerja[] = $x->getUnitId();
            // }

            $c->add(Revisi4MasterKegiatanPeer::UNIT_ID, $unit_kerja[0], Criteria::ILIKE);
            for ($i = 1; $i < count($unit_kerja); $i++) {
                $c->addOr(Revisi4MasterKegiatanPeer::UNIT_ID, $unit_kerja[$i], Criteria::ILIKE);
            }

            $unit_id = $this->getRequestParameter('unit_id');
            if (isset($unit_id)) {
                $c->add(Revisi4MasterKegiatanPeer::UNIT_ID, $unit_id);
            }
            $c->addAscendingOrderByColumn(Revisi4MasterKegiatanPeer::KODE_KEGIATAN);
        }
        elseif (isset($this->filters['tahap']) && $this->filters['tahap'] == 'revisi5') {
            $this->pager = new sfPropelPager('Revisi5MasterKegiatan', 20);
            $c = new Criteria();
            // $this->addSortCriteria($c);
            // $es = UnitKerjaPeer::doSelect(new Criteria);
            // $this->satuan_kerja = $es;
            // $unit_kerja = Array();
            // foreach ($es as $x) {
            //     $unit_kerja[] = $x->getUnitId();
            // }

            $c->add(Revisi5MasterKegiatanPeer::UNIT_ID, $unit_kerja[0], Criteria::ILIKE);
            for ($i = 1; $i < count($unit_kerja); $i++) {
                $c->addOr(Revisi5MasterKegiatanPeer::UNIT_ID, $unit_kerja[$i], Criteria::ILIKE);
            }

            $unit_id = $this->getRequestParameter('unit_id');
            if (isset($unit_id)) {
                $c->add(Revisi5MasterKegiatanPeer::UNIT_ID, $unit_id);
            }
            $c->addAscendingOrderByColumn(Revisi5MasterKegiatanPeer::KODE_KEGIATAN);
        }
        elseif (isset($this->filters['tahap']) && $this->filters['tahap'] == 'revisi6') {
            $this->pager = new sfPropelPager('Revisi6MasterKegiatan', 20);
            $c = new Criteria();
            // $this->addSortCriteria($c);
            // $es = UnitKerjaPeer::doSelect(new Criteria);
            // $this->satuan_kerja = $es;
            // $unit_kerja = Array();
            // foreach ($es as $x) {
            //     $unit_kerja[] = $x->getUnitId();
            // }

            $c->add(Revisi6MasterKegiatanPeer::UNIT_ID, $unit_kerja[0], Criteria::ILIKE);
            for ($i = 1; $i < count($unit_kerja); $i++) {
                $c->addOr(Revisi6MasterKegiatanPeer::UNIT_ID, $unit_kerja[$i], Criteria::ILIKE);
            }

            $unit_id = $this->getRequestParameter('unit_id');
            if (isset($unit_id)) {
                $c->add(Revisi6MasterKegiatanPeer::UNIT_ID, $unit_id);
            }
            $c->addAscendingOrderByColumn(Revisi6MasterKegiatanPeer::KODE_KEGIATAN);
        }
        elseif (isset($this->filters['tahap']) && $this->filters['tahap'] == 'revisi7') {
            $this->pager = new sfPropelPager('Revisi7MasterKegiatan', 20);
            $c = new Criteria();

            $c->add(Revisi7MasterKegiatanPeer::UNIT_ID, $unit_kerja[0], Criteria::ILIKE);
            for ($i = 1; $i < count($unit_kerja); $i++) {
                $c->addOr(Revisi7MasterKegiatanPeer::UNIT_ID, $unit_kerja[$i], Criteria::ILIKE);
            }

            $unit_id = $this->getRequestParameter('unit_id');
            if (isset($unit_id)) {
                $c->add(Revisi7MasterKegiatanPeer::UNIT_ID, $unit_id);
            }
            $c->addAscendingOrderByColumn(Revisi7MasterKegiatanPeer::KODE_KEGIATAN);
        }
        elseif (isset($this->filters['tahap']) && $this->filters['tahap'] == 'revisi8') {
            $this->pager = new sfPropelPager('Revisi8MasterKegiatan', 20);
            $c = new Criteria();

            $c->add(Revisi8MasterKegiatanPeer::UNIT_ID, $unit_kerja[0], Criteria::ILIKE);
            for ($i = 1; $i < count($unit_kerja); $i++) {
                $c->addOr(Revisi8MasterKegiatanPeer::UNIT_ID, $unit_kerja[$i], Criteria::ILIKE);
            }

            $unit_id = $this->getRequestParameter('unit_id');
            if (isset($unit_id)) {
                $c->add(Revisi8MasterKegiatanPeer::UNIT_ID, $unit_id);
            }
            $c->addAscendingOrderByColumn(Revisi8MasterKegiatanPeer::KODE_KEGIATAN);
        }
        elseif (isset($this->filters['tahap']) && $this->filters['tahap'] == 'revisi9') {
            $this->pager = new sfPropelPager('Revisi9MasterKegiatan', 20);
            $c = new Criteria();

            $c->add(Revisi9MasterKegiatanPeer::UNIT_ID, $unit_kerja[0], Criteria::ILIKE);
            for ($i = 1; $i < count($unit_kerja); $i++) {
                $c->addOr(Revisi9MasterKegiatanPeer::UNIT_ID, $unit_kerja[$i], Criteria::ILIKE);
            }

            $unit_id = $this->getRequestParameter('unit_id');
            if (isset($unit_id)) {
                $c->add(Revisi9MasterKegiatanPeer::UNIT_ID, $unit_id);
            }
            $c->addAscendingOrderByColumn(Revisi9MasterKegiatanPeer::KODE_KEGIATAN);
        }
        elseif (isset($this->filters['tahap']) && $this->filters['tahap'] == 'revisi10') {
            $this->pager = new sfPropelPager('Revisi10MasterKegiatan', 20);
            $c = new Criteria();

            $c->add(Revisi10MasterKegiatanPeer::UNIT_ID, $unit_kerja[0], Criteria::ILIKE);
            for ($i = 1; $i < count($unit_kerja); $i++) {
                $c->addOr(Revisi10MasterKegiatanPeer::UNIT_ID, $unit_kerja[$i], Criteria::ILIKE);
            }

            $unit_id = $this->getRequestParameter('unit_id');
            if (isset($unit_id)) {
                $c->add(Revisi10MasterKegiatanPeer::UNIT_ID, $unit_id);
            }
            $c->addAscendingOrderByColumn(Revisi10MasterKegiatanPeer::KODE_KEGIATAN);
        }
        elseif (isset($this->filters['tahap']) && $this->filters['tahap'] == 'rkua') {
            $this->pager = new sfPropelPager('RkuaMasterKegiatan', 20);
            $c = new Criteria();
            // $this->addSortCriteria($c);
            // $es = UnitKerjaPeer::doSelect(new Criteria);
            // $this->satuan_kerja = $es;
            // $unit_kerja = Array();
            // foreach ($es as $x) {
            //     $unit_kerja[] = $x->getUnitId();
            // }

            $c->add(RkuaMasterKegiatanPeer::UNIT_ID, $unit_kerja[0], Criteria::ILIKE);
            for ($i = 1; $i < count($unit_kerja); $i++) {
                $c->addOr(RkuaMasterKegiatanPeer::UNIT_ID, $unit_kerja[$i], Criteria::ILIKE);
            }

            $unit_id = $this->getRequestParameter('unit_id');
            if (isset($unit_id)) {
                $c->add(RkuaMasterKegiatanPeer::UNIT_ID, $unit_id);
            }
            $c->addAscendingOrderByColumn(RkuaMasterKegiatanPeer::KODE_KEGIATAN);
        } else {
            $this->pager = new sfPropelPager('DinasMasterKegiatan', 20);
            $c = new Criteria();
            // $this->addSortCriteria($c);
            // $es = UnitKerjaPeer::doSelect(new Criteria);
            // $this->satuan_kerja = $es;
            // $unit_kerja = Array();
            // foreach ($es as $x) {
            //     $unit_kerja[] = $x->getUnitId();
            // }

            $c->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_kerja[0], Criteria::ILIKE);
            for ($i = 1; $i < count($unit_kerja); $i++) {
                $c->addOr(DinasMasterKegiatanPeer::UNIT_ID, $unit_kerja[$i], Criteria::ILIKE);
            }

            $unit_id = $this->getRequestParameter('unit_id');
            if (isset($unit_id)) {
                $c->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
            }
            if ($menu == 'Pendapatan') {
                $c->add(DinasMasterKegiatanPeer::IS_BTL,TRUE);
            }
               else
            {
                    $c->add(DinasMasterKegiatanPeer::IS_BTL,FALSE);
            }

            if(isset($this->filters['id']) && $this->filters['id'] != ''){
                $c->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $this->filters['id'], Criteria::EQUAL);
            }
            
            $load_id = array();
            if(isset($this->filters['unit_id']) && $this->filters['unit_id'] != ''){
                $kode_id = new Criteria();
                $kode_id->add(DinasMasterKegiatanPeer::UNIT_ID, $this->filters['unit_id'], Criteria::EQUAL);
                if($menu == 'Pendapatan'){
                    $kode_id->add(DinasMasterKegiatanPeer::IS_BTL,TRUE);
                }else{
                    $kode_id->add(DinasMasterKegiatanPeer::IS_BTL,FALSE);
                }
                $kode_id->addAscendingOrderByColumn('CAST('.DinasMasterKegiatanPeer::KODE_KEGIATAN.' AS INT)');
                $load_id = DinasMasterKegiatanPeer::doSelect($kode_id);
            }
            $this->load_id = $load_id;

            $c->addAscendingOrderByColumn('CAST('.DinasMasterKegiatanPeer::KODE_KEGIATAN.' AS INT)');
        }
        $this->addFiltersCriteriaRevisi($c, isset($this->filters['tahap']) ? $this->filters['tahap'] : null);

        // end of filter tahap

        $this->pager->setCriteria($c);
        $this->pager->setPage($this->getRequestParameter('page', 1));
        $this->pager->init();
    }

        protected function addFiltersCriteriaRevisi($c, $tahap) {
        if ($tahap == 'pakbp') {
            if (isset($this->filters['kode_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(PakBukuPutihMasterKegiatanPeer::KODE_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(PakBukuPutihMasterKegiatanPeer::KODE_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['kode_kegiatan']) && $this->filters['kode_kegiatan'] !== '') {
                $kode_kegiatan = $this->filters['kode_kegiatan'];
                $arr = explode('.', $kode_kegiatan);
                $jum_array = count($arr);
                //if($jum_array<2)
                //{
                $cton1 = $c->getNewCriterion(PakBukuPutihMasterKegiatanPeer::KODE_URUSAN, $kode_kegiatan . '%', Criteria::LIKE);
                $cton2 = $c->getNewCriterion(PakBukuPutihMasterKegiatanPeer::KODE_PROGRAM2, $kode_kegiatan . '%', Criteria::LIKE);
                $cton1->addOr($cton2);
                //$c->add($cton1);
                //}
                if ($jum_array == 2) {
                    //$c->add(PakBukuPutihMasterKegiatanPeer::KODE_PROGRAM,$arr[1],Criteria::LIKE);
                    $cton3 = $c->getNewCriterion(PakBukuPutihMasterKegiatanPeer::KODE_PROGRAM, $arr[1] . '%', Criteria::LIKE);
                    $cton1->addAnd($cton3);
                }
                $c->add($cton1);
            }
            if (isset($this->filters['nama_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(PakBukuPutihMasterKegiatanPeer::NAMA_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(PakBukuPutihMasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
                $c->add(PakBukuPutihMasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
            }
            if (isset($this->filters['posisi_is_empty'])) {
                $criterion = $c->getNewCriterion(PakBukuPutihMasterKegiatanPeer::POSISI, '');
                $criterion->addOr($c->getNewCriterion(PakBukuPutihMasterKegiatanPeer::POSISI, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['posisi']) && $this->filters['posisi'] !== '') {
                //$query="select *
                //from ". sfConfig::get('app_default_schema') .".rincian r, ". sfConfig::get('app_default_schema') .".master_kegiatan mk
                //where r.rincian_level=".$this->filters['posisi']." and r.kegiatan_code=mk.kode_kegiatan";
                $crt1 = new Criteria();
                $crt1->add(DinasRincianPeer::RINCIAN_LEVEL, $this->filters['posisi']);
                $rincian = DinasRincianPeer::doSelect($crt1);
                foreach ($rincian as $r) {
                    if ($r->getKegiatanCode() != '') {
                        $c->add(PakBukuPutihMasterKegiatanPeer::KODE_KEGIATAN, $r->getKegiatanCode());
                    }
                }
            }
            if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
                $unit = $this->filters['unit_id'];
                $c->add(PakBukuPutihMasterKegiatanPeer::UNIT_ID, $unit);
            }
        } elseif ($tahap == 'pakbb') {
            if (isset($this->filters['kode_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(PakBukuBiruMasterKegiatanPeer::KODE_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(PakBukuBiruMasterKegiatanPeer::KODE_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['kode_kegiatan']) && $this->filters['kode_kegiatan'] !== '') {
                $kode_kegiatan = $this->filters['kode_kegiatan'];
                $arr = explode('.', $kode_kegiatan);
                $jum_array = count($arr);
                //if($jum_array<2)
                //{
                $cton1 = $c->getNewCriterion(PakBukuBiruMasterKegiatanPeer::KODE_URUSAN, $kode_kegiatan . '%', Criteria::LIKE);
                $cton2 = $c->getNewCriterion(PakBukuBiruMasterKegiatanPeer::KODE_PROGRAM2, $kode_kegiatan . '%', Criteria::LIKE);
                $cton1->addOr($cton2);
                //$c->add($cton1);
                //}
                if ($jum_array == 2) {
                    //$c->add(PakBukuBiruMasterKegiatanPeer::KODE_PROGRAM,$arr[1],Criteria::LIKE);
                    $cton3 = $c->getNewCriterion(PakBukuBiruMasterKegiatanPeer::KODE_PROGRAM, $arr[1] . '%', Criteria::LIKE);
                    $cton1->addAnd($cton3);
                }
                $c->add($cton1);
            }
            if (isset($this->filters['nama_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(PakBukuBiruMasterKegiatanPeer::NAMA_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(PakBukuBiruMasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
                $c->add(PakBukuBiruMasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
            }
            if (isset($this->filters['posisi_is_empty'])) {
                $criterion = $c->getNewCriterion(PakBukuBiruMasterKegiatanPeer::POSISI, '');
                $criterion->addOr($c->getNewCriterion(PakBukuBiruMasterKegiatanPeer::POSISI, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['posisi']) && $this->filters['posisi'] !== '') {
                //$query="select *
                //from ". sfConfig::get('app_default_schema') .".rincian r, ". sfConfig::get('app_default_schema') .".master_kegiatan mk
                //where r.rincian_level=".$this->filters['posisi']." and r.kegiatan_code=mk.kode_kegiatan";
                $crt1 = new Criteria();
                $crt1->add(DinasRincianPeer::RINCIAN_LEVEL, $this->filters['posisi']);
                $rincian = DinasRincianPeer::doSelect($crt1);
                foreach ($rincian as $r) {
                    if ($r->getKegiatanCode() != '') {
                        $c->add(PakBukuBiruMasterKegiatanPeer::KODE_KEGIATAN, $r->getKegiatanCode());
                    }
                }
            }
            if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
                $unit = $this->filters['unit_id'];
                $c->add(PakBukuBiruMasterKegiatanPeer::UNIT_ID, $unit);
            }
        } elseif ($tahap == 'murni') {
            if (isset($this->filters['kode_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(MurniMasterKegiatanPeer::KODE_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(MurniMasterKegiatanPeer::KODE_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['kode_kegiatan']) && $this->filters['kode_kegiatan'] !== '') {
                $kode_kegiatan = $this->filters['kode_kegiatan'];
                $arr = explode('.', $kode_kegiatan);
                $jum_array = count($arr);
                //if($jum_array<2)
                //{
                $cton1 = $c->getNewCriterion(MurniMasterKegiatanPeer::KODE_URUSAN, $kode_kegiatan . '%', Criteria::LIKE);
                $cton2 = $c->getNewCriterion(MurniMasterKegiatanPeer::KODE_PROGRAM2, $kode_kegiatan . '%', Criteria::LIKE);
                $cton1->addOr($cton2);
                //$c->add($cton1);
                //}
                if ($jum_array == 2) {
                    //$c->add(MurniMasterKegiatanPeer::KODE_PROGRAM,$arr[1],Criteria::LIKE);
                    $cton3 = $c->getNewCriterion(MurniMasterKegiatanPeer::KODE_PROGRAM, $arr[1] . '%', Criteria::LIKE);
                    $cton1->addAnd($cton3);
                }
                $c->add($cton1);
            }
            if (isset($this->filters['nama_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(MurniMasterKegiatanPeer::NAMA_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(MurniMasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
                $c->add(MurniMasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
            }
            if (isset($this->filters['posisi_is_empty'])) {
                $criterion = $c->getNewCriterion(MurniMasterKegiatanPeer::POSISI, '');
                $criterion->addOr($c->getNewCriterion(MurniMasterKegiatanPeer::POSISI, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['posisi']) && $this->filters['posisi'] !== '') {
                //$query="select *
                //from ". sfConfig::get('app_default_schema') .".rincian r, ". sfConfig::get('app_default_schema') .".master_kegiatan mk
                //where r.rincian_level=".$this->filters['posisi']." and r.kegiatan_code=mk.kode_kegiatan";
                $crt1 = new Criteria();
                $crt1->add(MurniRincianPeer::RINCIAN_LEVEL, $this->filters['posisi']);
                $rincian = MurniRincianPeer::doSelect($crt1);
                foreach ($rincian as $r) {
                    if ($r->getKegiatanCode() != '') {
                        $c->add(MurniMasterKegiatanPeer::KODE_KEGIATAN, $r->getKegiatanCode());
                    }
                }
            }
            if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
                $unit = $this->filters['unit_id'];
                $c->add(MurniMasterKegiatanPeer::UNIT_ID, $unit);
            }
        } elseif ($tahap == 'murnibp') {
            if (isset($this->filters['kode_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(MurniBukuPutihMasterKegiatanPeer::KODE_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(MurniBukuPutihMasterKegiatanPeer::KODE_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['kode_kegiatan']) && $this->filters['kode_kegiatan'] !== '') {
                $kode_kegiatan = $this->filters['kode_kegiatan'];
                $arr = explode('.', $kode_kegiatan);
                $jum_array = count($arr);
                //if($jum_array<2)
                //{
                $cton1 = $c->getNewCriterion(MurniBukuPutihMasterKegiatanPeer::KODE_URUSAN, $kode_kegiatan . '%', Criteria::LIKE);
                $cton2 = $c->getNewCriterion(MurniBukuPutihMasterKegiatanPeer::KODE_PROGRAM2, $kode_kegiatan . '%', Criteria::LIKE);
                $cton1->addOr($cton2);
                //$c->add($cton1);
                //}
                if ($jum_array == 2) {
                    //$c->add(MurniBukuPutihMasterKegiatanPeer::KODE_PROGRAM,$arr[1],Criteria::LIKE);
                    $cton3 = $c->getNewCriterion(MurniBukuPutihMasterKegiatanPeer::KODE_PROGRAM, $arr[1] . '%', Criteria::LIKE);
                    $cton1->addAnd($cton3);
                }
                $c->add($cton1);
            }
            if (isset($this->filters['nama_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(MurniBukuPutihMasterKegiatanPeer::NAMA_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(MurniBukuPutihMasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
                $c->add(MurniBukuPutihMasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
            }
            if (isset($this->filters['posisi_is_empty'])) {
                $criterion = $c->getNewCriterion(MurniBukuPutihMasterKegiatanPeer::POSISI, '');
                $criterion->addOr($c->getNewCriterion(MurniBukuPutihMasterKegiatanPeer::POSISI, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['posisi']) && $this->filters['posisi'] !== '') {
                //$query="select *
                //from ". sfConfig::get('app_default_schema') .".rincian r, ". sfConfig::get('app_default_schema') .".master_kegiatan mk
                //where r.rincian_level=".$this->filters['posisi']." and r.kegiatan_code=mk.kode_kegiatan";
                $crt1 = new Criteria();
                $crt1->add(MurniBukuPutihRincianPeer::RINCIAN_LEVEL, $this->filters['posisi']);
                $rincian = MurniBukuPutihRincianPeer::doSelect($crt1);
                foreach ($rincian as $r) {
                    if ($r->getKegiatanCode() != '') {
                        $c->add(MurniBukuPutihMasterKegiatanPeer::KODE_KEGIATAN, $r->getKegiatanCode());
                    }
                }
            }
            if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
                $unit = $this->filters['unit_id'];
                $c->add(MurniBukuPutihMasterKegiatanPeer::UNIT_ID, $unit);
            }
        } elseif ($tahap == 'murnibb') {
            if (isset($this->filters['kode_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(MurniBukuBiruMasterKegiatanPeer::KODE_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(MurniBukuBiruMasterKegiatanPeer::KODE_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['kode_kegiatan']) && $this->filters['kode_kegiatan'] !== '') {
                $kode_kegiatan = $this->filters['kode_kegiatan'];
                $arr = explode('.', $kode_kegiatan);
                $jum_array = count($arr);
                //if($jum_array<2)
                //{
                $cton1 = $c->getNewCriterion(MurniBukuBiruMasterKegiatanPeer::KODE_URUSAN, $kode_kegiatan . '%', Criteria::LIKE);
                $cton2 = $c->getNewCriterion(MurniBukuBiruMasterKegiatanPeer::KODE_PROGRAM2, $kode_kegiatan . '%', Criteria::LIKE);
                $cton1->addOr($cton2);
                //$c->add($cton1);
                //}
                if ($jum_array == 2) {
                    //$c->add(MurniBukuBiruMasterKegiatanPeer::KODE_PROGRAM,$arr[1],Criteria::LIKE);
                    $cton3 = $c->getNewCriterion(MurniBukuBiruMasterKegiatanPeer::KODE_PROGRAM, $arr[1] . '%', Criteria::LIKE);
                    $cton1->addAnd($cton3);
                }
                $c->add($cton1);
            }
            if (isset($this->filters['nama_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(MurniBukuBiruMasterKegiatanPeer::NAMA_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(MurniBukuBiruMasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
                $c->add(MurniBukuBiruMasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
            }
            if (isset($this->filters['posisi_is_empty'])) {
                $criterion = $c->getNewCriterion(MurniBukuBiruMasterKegiatanPeer::POSISI, '');
                $criterion->addOr($c->getNewCriterion(MurniBukuBiruMasterKegiatanPeer::POSISI, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['posisi']) && $this->filters['posisi'] !== '') {
                //$query="select *
                //from ". sfConfig::get('app_default_schema') .".rincian r, ". sfConfig::get('app_default_schema') .".master_kegiatan mk
                //where r.rincian_level=".$this->filters['posisi']." and r.kegiatan_code=mk.kode_kegiatan";
                $crt1 = new Criteria();
                $crt1->add(MurniBukuBiruRincianPeer::RINCIAN_LEVEL, $this->filters['posisi']);
                $rincian = MurniBukuBiruRincianPeer::doSelect($crt1);
                foreach ($rincian as $r) {
                    if ($r->getKegiatanCode() != '') {
                        $c->add(MurniBukuBiruMasterKegiatanPeer::KODE_KEGIATAN, $r->getKegiatanCode());
                    }
                }
            }
            if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
                $unit = $this->filters['unit_id'];
                $c->add(MurniBukuBiruMasterKegiatanPeer::UNIT_ID, $unit);
            }
        } elseif ($tahap == 'murnibbpraevagub') {
            if (isset($this->filters['kode_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(MurniBukubiruPraevagubMasterKegiatanPeer::KODE_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(MurniBukubiruPraevagubMasterKegiatanPeer::KODE_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['kode_kegiatan']) && $this->filters['kode_kegiatan'] !== '') {
                $kode_kegiatan = $this->filters['kode_kegiatan'];
                $arr = explode('.', $kode_kegiatan);
                $jum_array = count($arr);
                //if($jum_array<2)
                //{
                $cton1 = $c->getNewCriterion(MurniBukubiruPraevagubMasterKegiatanPeer::KODE_URUSAN, $kode_kegiatan . '%', Criteria::LIKE);
                $cton2 = $c->getNewCriterion(MurniBukubiruPraevagubMasterKegiatanPeer::KODE_PROGRAM2, $kode_kegiatan . '%', Criteria::LIKE);
                $cton1->addOr($cton2);
                //$c->add($cton1);
                //}
                if ($jum_array == 2) {
                    //$c->add(MurniBukubiruPraevagubMasterKegiatanPeer::KODE_PROGRAM,$arr[1],Criteria::LIKE);
                    $cton3 = $c->getNewCriterion(MurniBukubiruPraevagubMasterKegiatanPeer::KODE_PROGRAM, $arr[1] . '%', Criteria::LIKE);
                    $cton1->addAnd($cton3);
                }
                $c->add($cton1);
            }
            if (isset($this->filters['nama_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(MurniBukubiruPraevagubMasterKegiatanPeer::NAMA_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(MurniBukubiruPraevagubMasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
                $c->add(MurniBukubiruPraevagubMasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
            }
            if (isset($this->filters['posisi_is_empty'])) {
                $criterion = $c->getNewCriterion(MurniBukubiruPraevagubMasterKegiatanPeer::POSISI, '');
                $criterion->addOr($c->getNewCriterion(MurniBukubiruPraevagubMasterKegiatanPeer::POSISI, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['posisi']) && $this->filters['posisi'] !== '') {
                //$query="select *
                //from ". sfConfig::get('app_default_schema') .".rincian r, ". sfConfig::get('app_default_schema') .".master_kegiatan mk
                //where r.rincian_level=".$this->filters['posisi']." and r.kegiatan_code=mk.kode_kegiatan";
                $crt1 = new Criteria();
                $crt1->add(MurniBukubiruPraevagubRincianPeer::RINCIAN_LEVEL, $this->filters['posisi']);
                $rincian = MurniBukubiruPraevagubRincianPeer::doSelect($crt1);
                foreach ($rincian as $r) {
                    if ($r->getKegiatanCode() != '') {
                        $c->add(MurniBukubiruPraevagubMasterKegiatanPeer::KODE_KEGIATAN, $r->getKegiatanCode());
                    }
                }
            }
            if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
                $unit = $this->filters['unit_id'];
                $c->add(MurniBukubiruPraevagubMasterKegiatanPeer::UNIT_ID, $unit);
            }
        } elseif ($tahap == 'revisi1') {
            if (isset($this->filters['kode_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi1MasterKegiatanPeer::KODE_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(Revisi1MasterKegiatanPeer::KODE_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['kode_kegiatan']) && $this->filters['kode_kegiatan'] !== '') {
                $kode_kegiatan = $this->filters['kode_kegiatan'];
                $arr = explode('.', $kode_kegiatan);
                $jum_array = count($arr);
                //if($jum_array<2)
                //{
                $cton1 = $c->getNewCriterion(Revisi1MasterKegiatanPeer::KODE_URUSAN, $kode_kegiatan . '%', Criteria::LIKE);
                $cton2 = $c->getNewCriterion(Revisi1MasterKegiatanPeer::KODE_PROGRAM2, $kode_kegiatan . '%', Criteria::LIKE);
                $cton1->addOr($cton2);
                //$c->add($cton1);
                //}
                if ($jum_array == 2) {
                    //$c->add(Revisi1MasterKegiatanPeer::KODE_PROGRAM,$arr[1],Criteria::LIKE);
                    $cton3 = $c->getNewCriterion(Revisi1MasterKegiatanPeer::KODE_PROGRAM, $arr[1] . '%', Criteria::LIKE);
                    $cton1->addAnd($cton3);
                }
                $c->add($cton1);
            }
            if (isset($this->filters['nama_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi1MasterKegiatanPeer::NAMA_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(Revisi1MasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
                $c->add(Revisi1MasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
            }
            if (isset($this->filters['posisi_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi1MasterKegiatanPeer::POSISI, '');
                $criterion->addOr($c->getNewCriterion(Revisi1MasterKegiatanPeer::POSISI, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['posisi']) && $this->filters['posisi'] !== '') {
                //$query="select *
                //from ". sfConfig::get('app_default_schema') .".rincian r, ". sfConfig::get('app_default_schema') .".master_kegiatan mk
                //where r.rincian_level=".$this->filters['posisi']." and r.kegiatan_code=mk.kode_kegiatan";
                $crt1 = new Criteria();
                $crt1->add(Revisi1RincianPeer::RINCIAN_LEVEL, $this->filters['posisi']);
                $rincian = Revisi1RincianPeer::doSelect($crt1);
                foreach ($rincian as $r) {
                    if ($r->getKegiatanCode() != '') {
                        $c->add(Revisi1MasterKegiatanPeer::KODE_KEGIATAN, $r->getKegiatanCode());
                    }
                }
            }
            if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
                $unit = $this->filters['unit_id'];
                $c->add(Revisi1MasterKegiatanPeer::UNIT_ID, $unit);
            }
        } elseif ($tahap == 'revisi1_1') {
            if (isset($this->filters['kode_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi1bMasterKegiatanPeer::KODE_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(Revisi1bMasterKegiatanPeer::KODE_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['kode_kegiatan']) && $this->filters['kode_kegiatan'] !== '') {
                $kode_kegiatan = $this->filters['kode_kegiatan'];
                $arr = explode('.', $kode_kegiatan);
                $jum_array = count($arr);
                //if($jum_array<2)
                //{
                $cton1 = $c->getNewCriterion(Revisi1bMasterKegiatanPeer::KODE_URUSAN, $kode_kegiatan . '%', Criteria::LIKE);
                $cton2 = $c->getNewCriterion(Revisi1bMasterKegiatanPeer::KODE_PROGRAM2, $kode_kegiatan . '%', Criteria::LIKE);
                $cton1->addOr($cton2);
                //$c->add($cton1);
                //}
                if ($jum_array == 2) {
                    //$c->add(Revisi1MasterKegiatanPeer::KODE_PROGRAM,$arr[1],Criteria::LIKE);
                    $cton3 = $c->getNewCriterion(Revisi1bMasterKegiatanPeer::KODE_PROGRAM, $arr[1] . '%', Criteria::LIKE);
                    $cton1->addAnd($cton3);
                }
                $c->add($cton1);
            }
            if (isset($this->filters['nama_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi1bMasterKegiatanPeer::NAMA_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(Revisi1bMasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
                $c->add(Revisi1bMasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
            }
            if (isset($this->filters['posisi_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi1bMasterKegiatanPeer::POSISI, '');
                $criterion->addOr($c->getNewCriterion(Revisi1bMasterKegiatanPeer::POSISI, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['posisi']) && $this->filters['posisi'] !== '') {
                //$query="select *
                //from ". sfConfig::get('app_default_schema') .".rincian r, ". sfConfig::get('app_default_schema') .".master_kegiatan mk
                //where r.rincian_level=".$this->filters['posisi']." and r.kegiatan_code=mk.kode_kegiatan";
                $crt1 = new Criteria();
                $crt1->add(Revisi1bRincianPeer::RINCIAN_LEVEL, $this->filters['posisi']);
                $rincian = Revisi1bRincianPeer::doSelect($crt1);
                foreach ($rincian as $r) {
                    if ($r->getKegiatanCode() != '') {
                        $c->add(Revisi1bMasterKegiatanPeer::KODE_KEGIATAN, $r->getKegiatanCode());
                    }
                }
            }
            if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
                $unit = $this->filters['unit_id'];
                $c->add(Revisi1bMasterKegiatanPeer::UNIT_ID, $unit);
            }
        } elseif ($tahap == 'revisi2') {
            if (isset($this->filters['kode_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi2MasterKegiatanPeer::KODE_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(Revisi2MasterKegiatanPeer::KODE_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['kode_kegiatan']) && $this->filters['kode_kegiatan'] !== '') {
                $kode_kegiatan = $this->filters['kode_kegiatan'];
                $arr = explode('.', $kode_kegiatan);
                $jum_array = count($arr);
                //if($jum_array<2)
                //{
                $cton1 = $c->getNewCriterion(Revisi2MasterKegiatanPeer::KODE_URUSAN, $kode_kegiatan . '%', Criteria::LIKE);
                $cton2 = $c->getNewCriterion(Revisi2MasterKegiatanPeer::KODE_PROGRAM2, $kode_kegiatan . '%', Criteria::LIKE);
                $cton1->addOr($cton2);
                //$c->add($cton1);
                //}
                if ($jum_array == 2) {
                    //$c->add(Revisi2MasterKegiatanPeer::KODE_PROGRAM,$arr[1],Criteria::LIKE);
                    $cton3 = $c->getNewCriterion(Revisi2MasterKegiatanPeer::KODE_PROGRAM, $arr[1] . '%', Criteria::LIKE);
                    $cton1->addAnd($cton3);
                }
                $c->add($cton1);
            }
            if (isset($this->filters['nama_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi2MasterKegiatanPeer::NAMA_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(Revisi2MasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
                $c->add(Revisi2MasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
            }
            if (isset($this->filters['posisi_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi2MasterKegiatanPeer::POSISI, '');
                $criterion->addOr($c->getNewCriterion(Revisi2MasterKegiatanPeer::POSISI, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['posisi']) && $this->filters['posisi'] !== '') {
                //$query="select *
                //from ". sfConfig::get('app_default_schema') .".rincian r, ". sfConfig::get('app_default_schema') .".master_kegiatan mk
                //where r.rincian_level=".$this->filters['posisi']." and r.kegiatan_code=mk.kode_kegiatan";
                $crt1 = new Criteria();
                $crt1->add(Revisi2RincianPeer::RINCIAN_LEVEL, $this->filters['posisi']);
                $rincian = Revisi2RincianPeer::doSelect($crt1);
                foreach ($rincian as $r) {
                    if ($r->getKegiatanCode() != '') {
                        $c->add(Revisi2MasterKegiatanPeer::KODE_KEGIATAN, $r->getKegiatanCode());
                    }
                }
            }
            if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
                $unit = $this->filters['unit_id'];
                $c->add(Revisi2MasterKegiatanPeer::UNIT_ID, $unit);
            }
        } elseif ($tahap == 'revisi2_1') {
            if (isset($this->filters['kode_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi21MasterKegiatanPeer::KODE_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(Revisi21MasterKegiatanPeer::KODE_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['kode_kegiatan']) && $this->filters['kode_kegiatan'] !== '') {
                $kode_kegiatan = $this->filters['kode_kegiatan'];
                $arr = explode('.', $kode_kegiatan);
                $jum_array = count($arr);
                //if($jum_array<2)
                //{
                $cton1 = $c->getNewCriterion(Revisi21MasterKegiatanPeer::KODE_URUSAN, $kode_kegiatan . '%', Criteria::LIKE);
                $cton2 = $c->getNewCriterion(Revisi21MasterKegiatanPeer::KODE_PROGRAM2, $kode_kegiatan . '%', Criteria::LIKE);
                $cton1->addOr($cton2);
                //$c->add($cton1);
                //}
                if ($jum_array == 2) {
                    //$c->add(Revisi2MasterKegiatanPeer::KODE_PROGRAM,$arr[1],Criteria::LIKE);
                    $cton3 = $c->getNewCriterion(Revisi21MasterKegiatanPeer::KODE_PROGRAM, $arr[1] . '%', Criteria::LIKE);
                    $cton1->addAnd($cton3);
                }
                $c->add($cton1);
            }
            if (isset($this->filters['nama_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi21MasterKegiatanPeer::NAMA_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(Revisi21MasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
                $c->add(Revisi2MasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
            }
            if (isset($this->filters['posisi_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi21MasterKegiatanPeer::POSISI, '');
                $criterion->addOr($c->getNewCriterion(Revisi21MasterKegiatanPeer::POSISI, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['posisi']) && $this->filters['posisi'] !== '') {
                //$query="select *
                //from ". sfConfig::get('app_default_schema') .".rincian r, ". sfConfig::get('app_default_schema') .".master_kegiatan mk
                //where r.rincian_level=".$this->filters['posisi']." and r.kegiatan_code=mk.kode_kegiatan";
                $crt1 = new Criteria();
                $crt1->add(Revisi21RincianPeer::RINCIAN_LEVEL, $this->filters['posisi']);
                $rincian = Revisi21RincianPeer::doSelect($crt1);
                foreach ($rincian as $r) {
                    if ($r->getKegiatanCode() != '') {
                        $c->add(Revisi21MasterKegiatanPeer::KODE_KEGIATAN, $r->getKegiatanCode());
                    }
                }
            }
            if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
                $unit = $this->filters['unit_id'];
                $c->add(Revisi21MasterKegiatanPeer::UNIT_ID, $unit);
            }
        } elseif ($tahap == 'revisi2_2') {
            if (isset($this->filters['kode_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi22MasterKegiatanPeer::KODE_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(Revisi22MasterKegiatanPeer::KODE_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['kode_kegiatan']) && $this->filters['kode_kegiatan'] !== '') {
                $kode_kegiatan = $this->filters['kode_kegiatan'];
                $arr = explode('.', $kode_kegiatan);
                $jum_array = count($arr);
                //if($jum_array<2)
                //{
                $cton1 = $c->getNewCriterion(Revisi22MasterKegiatanPeer::KODE_URUSAN, $kode_kegiatan . '%', Criteria::LIKE);
                $cton2 = $c->getNewCriterion(Revisi22MasterKegiatanPeer::KODE_PROGRAM2, $kode_kegiatan . '%', Criteria::LIKE);
                $cton1->addOr($cton2);
                //$c->add($cton1);
                //}
                if ($jum_array == 2) {
                    //$c->add(Revisi2MasterKegiatanPeer::KODE_PROGRAM,$arr[1],Criteria::LIKE);
                    $cton3 = $c->getNewCriterion(Revisi22MasterKegiatanPeer::KODE_PROGRAM, $arr[1] . '%', Criteria::LIKE);
                    $cton1->addAnd($cton3);
                }
                $c->add($cton1);
            }
            if (isset($this->filters['nama_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi22MasterKegiatanPeer::NAMA_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(Revisi22MasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
                $c->add(Revisi2MasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
            }
            if (isset($this->filters['posisi_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi22MasterKegiatanPeer::POSISI, '');
                $criterion->addOr($c->getNewCriterion(Revisi22MasterKegiatanPeer::POSISI, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['posisi']) && $this->filters['posisi'] !== '') {
                //$query="select *
                //from ". sfConfig::get('app_default_schema') .".rincian r, ". sfConfig::get('app_default_schema') .".master_kegiatan mk
                //where r.rincian_level=".$this->filters['posisi']." and r.kegiatan_code=mk.kode_kegiatan";
                $crt1 = new Criteria();
                $crt1->add(Revisi22RincianPeer::RINCIAN_LEVEL, $this->filters['posisi']);
                $rincian = Revisi22RincianPeer::doSelect($crt1);
                foreach ($rincian as $r) {
                    if ($r->getKegiatanCode() != '') {
                        $c->add(Revisi22MasterKegiatanPeer::KODE_KEGIATAN, $r->getKegiatanCode());
                    }
                }
            }
            if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
                $unit = $this->filters['unit_id'];
                $c->add(Revisi22MasterKegiatanPeer::UNIT_ID, $unit);
            }
        }
        //tambahan rev 3
        elseif ($tahap == 'revisi3') {
            if (isset($this->filters['kode_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi3MasterKegiatanPeer::KODE_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(Revisi3MasterKegiatanPeer::KODE_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['kode_kegiatan']) && $this->filters['kode_kegiatan'] !== '') {
                $kode_kegiatan = $this->filters['kode_kegiatan'];
                $arr = explode('.', $kode_kegiatan);
                $jum_array = count($arr);
                //if($jum_array<2)
                //{
                $cton1 = $c->getNewCriterion(Revisi3MasterKegiatanPeer::KODE_URUSAN, $kode_kegiatan . '%', Criteria::LIKE);
                $cton2 = $c->getNewCriterion(Revisi3MasterKegiatanPeer::KODE_PROGRAM2, $kode_kegiatan . '%', Criteria::LIKE);
                $cton1->addOr($cton2);
                //$c->add($cton1);
                //}
                if ($jum_array == 2) {
                    //$c->add(Revisi2MasterKegiatanPeer::KODE_PROGRAM,$arr[1],Criteria::LIKE);
                    $cton3 = $c->getNewCriterion(Revisi3MasterKegiatanPeer::KODE_PROGRAM, $arr[1] . '%', Criteria::LIKE);
                    $cton1->addAnd($cton3);
                }
                $c->add($cton1);
            }
            if (isset($this->filters['nama_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi3MasterKegiatanPeer::NAMA_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(Revisi3MasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
                $c->add(Revisi3MasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
            }
            if (isset($this->filters['posisi_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi3MasterKegiatanPeer::POSISI, '');
                $criterion->addOr($c->getNewCriterion(Revisi3MasterKegiatanPeer::POSISI, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['posisi']) && $this->filters['posisi'] !== '') {
                //$query="select *
                //from ". sfConfig::get('app_default_schema') .".rincian r, ". sfConfig::get('app_default_schema') .".master_kegiatan mk
                //where r.rincian_level=".$this->filters['posisi']." and r.kegiatan_code=mk.kode_kegiatan";
                $crt1 = new Criteria();
                $crt1->add(Revisi3RincianPeer::RINCIAN_LEVEL, $this->filters['posisi']);
                $rincian = Revisi3RincianPeer::doSelect($crt1);
                foreach ($rincian as $r) {
                    if ($r->getKegiatanCode() != '') {
                        $c->add(Revisi3MasterKegiatanPeer::KODE_KEGIATAN, $r->getKegiatanCode());
                    }
                }
            }
            if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
                $unit = $this->filters['unit_id'];
                $c->add(Revisi3MasterKegiatanPeer::UNIT_ID, $unit);
            }
        }
        elseif ($tahap == 'revisi3_1') {
            if (isset($this->filters['kode_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi31MasterKegiatanPeer::KODE_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(Revisi31MasterKegiatanPeer::KODE_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['kode_kegiatan']) && $this->filters['kode_kegiatan'] !== '') {
                $kode_kegiatan = $this->filters['kode_kegiatan'];
                $arr = explode('.', $kode_kegiatan);
                $jum_array = count($arr);
                //if($jum_array<2)
                //{
                $cton1 = $c->getNewCriterion(Revisi31MasterKegiatanPeer::KODE_URUSAN, $kode_kegiatan . '%', Criteria::LIKE);
                $cton2 = $c->getNewCriterion(Revisi31MasterKegiatanPeer::KODE_PROGRAM2, $kode_kegiatan . '%', Criteria::LIKE);
                $cton1->addOr($cton2);
                //$c->add($cton1);
                //}
                if ($jum_array == 2) {
                    //$c->add(Revisi2MasterKegiatanPeer::KODE_PROGRAM,$arr[1],Criteria::LIKE);
                    $cton3 = $c->getNewCriterion(Revisi31MasterKegiatanPeer::KODE_PROGRAM, $arr[1] . '%', Criteria::LIKE);
                    $cton1->addAnd($cton3);
                }
                $c->add($cton1);
            }
            if (isset($this->filters['nama_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi31MasterKegiatanPeer::NAMA_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(Revisi31MasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
                $c->add(Revisi31MasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
            }
            if (isset($this->filters['posisi_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi31MasterKegiatanPeer::POSISI, '');
                $criterion->addOr($c->getNewCriterion(Revisi31MasterKegiatanPeer::POSISI, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['posisi']) && $this->filters['posisi'] !== '') {
                //$query="select *
                //from ". sfConfig::get('app_default_schema') .".rincian r, ". sfConfig::get('app_default_schema') .".master_kegiatan mk
                //where r.rincian_level=".$this->filters['posisi']." and r.kegiatan_code=mk.kode_kegiatan";
                $crt1 = new Criteria();
                $crt1->add(Revisi31RincianPeer::RINCIAN_LEVEL, $this->filters['posisi']);
                $rincian = Revisi31RincianPeer::doSelect($crt1);
                foreach ($rincian as $r) {
                    if ($r->getKegiatanCode() != '') {
                        $c->add(Revisi31MasterKegiatanPeer::KODE_KEGIATAN, $r->getKegiatanCode());
                    }
                }
            }
            if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
                $unit = $this->filters['unit_id'];
                $c->add(Revisi31MasterKegiatanPeer::UNIT_ID, $unit);
            }
        }
        elseif ($tahap == 'revisi4') {
            if (isset($this->filters['kode_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi3MasterKegiatanPeer::KODE_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(Revisi4MasterKegiatanPeer::KODE_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['kode_kegiatan']) && $this->filters['kode_kegiatan'] !== '') {
                $kode_kegiatan = $this->filters['kode_kegiatan'];
                $arr = explode('.', $kode_kegiatan);
                $jum_array = count($arr);
                //if($jum_array<2)
                //{
                $cton1 = $c->getNewCriterion(Revisi4MasterKegiatanPeer::KODE_URUSAN, $kode_kegiatan . '%', Criteria::LIKE);
                $cton2 = $c->getNewCriterion(Revisi4MasterKegiatanPeer::KODE_PROGRAM2, $kode_kegiatan . '%', Criteria::LIKE);
                $cton1->addOr($cton2);
                //$c->add($cton1);
                //}
                if ($jum_array == 2) {
                    //$c->add(Revisi2MasterKegiatanPeer::KODE_PROGRAM,$arr[1],Criteria::LIKE);
                    $cton3 = $c->getNewCriterion(Revisi4MasterKegiatanPeer::KODE_PROGRAM, $arr[1] . '%', Criteria::LIKE);
                    $cton1->addAnd($cton3);
                }
                $c->add($cton1);
            }
            if (isset($this->filters['nama_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi4MasterKegiatanPeer::NAMA_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(Revisi4MasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
                $c->add(Revisi4MasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
            }
            if (isset($this->filters['posisi_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi4MasterKegiatanPeer::POSISI, '');
                $criterion->addOr($c->getNewCriterion(Revisi4MasterKegiatanPeer::POSISI, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['posisi']) && $this->filters['posisi'] !== '') {
                //$query="select *
                //from ". sfConfig::get('app_default_schema') .".rincian r, ". sfConfig::get('app_default_schema') .".master_kegiatan mk
                //where r.rincian_level=".$this->filters['posisi']." and r.kegiatan_code=mk.kode_kegiatan";
                $crt1 = new Criteria();
                $crt1->add(Revisi4RincianPeer::RINCIAN_LEVEL, $this->filters['posisi']);
                $rincian = Revisi4RincianPeer::doSelect($crt1);
                foreach ($rincian as $r) {
                    if ($r->getKegiatanCode() != '') {
                        $c->add(Revisi4MasterKegiatanPeer::KODE_KEGIATAN, $r->getKegiatanCode());
                    }
                }
            }
            if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
                $unit = $this->filters['unit_id'];
                $c->add(Revisi4MasterKegiatanPeer::UNIT_ID, $unit);
            }
        }
        elseif ($tahap == 'revisi5') {
            if (isset($this->filters['kode_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi5MasterKegiatanPeer::KODE_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(Revisi5MasterKegiatanPeer::KODE_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['kode_kegiatan']) && $this->filters['kode_kegiatan'] !== '') {
                $kode_kegiatan = $this->filters['kode_kegiatan'];
                $arr = explode('.', $kode_kegiatan);
                $jum_array = count($arr);
                //if($jum_array<2)
                //{
                $cton1 = $c->getNewCriterion(Revisi5MasterKegiatanPeer::KODE_URUSAN, $kode_kegiatan . '%', Criteria::LIKE);
                $cton2 = $c->getNewCriterion(Revisi5MasterKegiatanPeer::KODE_PROGRAM2, $kode_kegiatan . '%', Criteria::LIKE);
                $cton1->addOr($cton2);
                //$c->add($cton1);
                //}
                if ($jum_array == 2) {
                    //$c->add(Revisi2MasterKegiatanPeer::KODE_PROGRAM,$arr[1],Criteria::LIKE);
                    $cton3 = $c->getNewCriterion(Revisi5MasterKegiatanPeer::KODE_PROGRAM, $arr[1] . '%', Criteria::LIKE);
                    $cton1->addAnd($cton3);
                }
                $c->add($cton1);
            }
            if (isset($this->filters['nama_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi5MasterKegiatanPeer::NAMA_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(Revisi5MasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
                $c->add(Revisi5MasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
            }
            if (isset($this->filters['posisi_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi5MasterKegiatanPeer::POSISI, '');
                $criterion->addOr($c->getNewCriterion(Revisi5MasterKegiatanPeer::POSISI, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['posisi']) && $this->filters['posisi'] !== '') {
                //$query="select *
                //from ". sfConfig::get('app_default_schema') .".rincian r, ". sfConfig::get('app_default_schema') .".master_kegiatan mk
                //where r.rincian_level=".$this->filters['posisi']." and r.kegiatan_code=mk.kode_kegiatan";
                $crt1 = new Criteria();
                $crt1->add(Revisi5RincianPeer::RINCIAN_LEVEL, $this->filters['posisi']);
                $rincian = Revisi5RincianPeer::doSelect($crt1);
                foreach ($rincian as $r) {
                    if ($r->getKegiatanCode() != '') {
                        $c->add(Revisi5MasterKegiatanPeer::KODE_KEGIATAN, $r->getKegiatanCode());
                    }
                }
            }
            if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
                $unit = $this->filters['unit_id'];
                $c->add(Revisi5MasterKegiatanPeer::UNIT_ID, $unit);
            }
        }
        elseif ($tahap == 'revisi6') {
            if (isset($this->filters['kode_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi6MasterKegiatanPeer::KODE_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(Revisi6MasterKegiatanPeer::KODE_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['kode_kegiatan']) && $this->filters['kode_kegiatan'] !== '') {
                $kode_kegiatan = $this->filters['kode_kegiatan'];
                $arr = explode('.', $kode_kegiatan);
                $jum_array = count($arr);
                
                $cton1 = $c->getNewCriterion(Revisi6MasterKegiatanPeer::KODE_URUSAN, $kode_kegiatan . '%', Criteria::LIKE);
                $cton2 = $c->getNewCriterion(Revisi6MasterKegiatanPeer::KODE_PROGRAM2, $kode_kegiatan . '%', Criteria::LIKE);
                $cton1->addOr($cton2);
                
                if ($jum_array == 2) {
                    $cton3 = $c->getNewCriterion(Revisi6MasterKegiatanPeer::KODE_PROGRAM, $arr[1] . '%', Criteria::LIKE);
                    $cton1->addAnd($cton3);
                }
                $c->add($cton1);
            }
            if (isset($this->filters['nama_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi6MasterKegiatanPeer::NAMA_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(Revisi6MasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
                $c->add(Revisi6MasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
            }
            if (isset($this->filters['posisi_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi6MasterKegiatanPeer::POSISI, '');
                $criterion->addOr($c->getNewCriterion(Revisi6MasterKegiatanPeer::POSISI, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['posisi']) && $this->filters['posisi'] !== '') {
                $crt1 = new Criteria();
                $crt1->add(Revisi6RincianPeer::RINCIAN_LEVEL, $this->filters['posisi']);
                $rincian = Revisi6RincianPeer::doSelect($crt1);
                foreach ($rincian as $r) {
                    if ($r->getKegiatanCode() != '') {
                        $c->add(Revisi6MasterKegiatanPeer::KODE_KEGIATAN, $r->getKegiatanCode());
                    }
                }
            }
            if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
                $unit = $this->filters['unit_id'];
                $c->add(Revisi6MasterKegiatanPeer::UNIT_ID, $unit);
            }
        }
        elseif ($tahap == 'revisi7') {
            if (isset($this->filters['kode_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi7MasterKegiatanPeer::KODE_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(Revisi7MasterKegiatanPeer::KODE_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['kode_kegiatan']) && $this->filters['kode_kegiatan'] !== '') {
                $kode_kegiatan = $this->filters['kode_kegiatan'];
                $arr = explode('.', $kode_kegiatan);
                $jum_array = count($arr);
                
                $cton1 = $c->getNewCriterion(Revisi7MasterKegiatanPeer::KODE_URUSAN, $kode_kegiatan . '%', Criteria::LIKE);
                $cton2 = $c->getNewCriterion(Revisi7MasterKegiatanPeer::KODE_PROGRAM2, $kode_kegiatan . '%', Criteria::LIKE);
                $cton1->addOr($cton2);
                
                if ($jum_array == 2) {
                    $cton3 = $c->getNewCriterion(Revisi7MasterKegiatanPeer::KODE_PROGRAM, $arr[1] . '%', Criteria::LIKE);
                    $cton1->addAnd($cton3);
                }
                $c->add($cton1);
            }
            if (isset($this->filters['nama_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi7MasterKegiatanPeer::NAMA_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(Revisi7MasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
                $c->add(Revisi7MasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
            }
            if (isset($this->filters['posisi_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi7MasterKegiatanPeer::POSISI, '');
                $criterion->addOr($c->getNewCriterion(Revisi7MasterKegiatanPeer::POSISI, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['posisi']) && $this->filters['posisi'] !== '') {
                $crt1 = new Criteria();
                $crt1->add(Revisi7RincianPeer::RINCIAN_LEVEL, $this->filters['posisi']);
                $rincian = Revisi7RincianPeer::doSelect($crt1);
                foreach ($rincian as $r) {
                    if ($r->getKegiatanCode() != '') {
                        $c->add(Revisi7MasterKegiatanPeer::KODE_KEGIATAN, $r->getKegiatanCode());
                    }
                }
            }
            if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
                $unit = $this->filters['unit_id'];
                $c->add(Revisi7MasterKegiatanPeer::UNIT_ID, $unit);
            }
        }
        elseif ($tahap == 'revisi8') {
            if (isset($this->filters['kode_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi8MasterKegiatanPeer::KODE_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(Revisi8MasterKegiatanPeer::KODE_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['kode_kegiatan']) && $this->filters['kode_kegiatan'] !== '') {
                $kode_kegiatan = $this->filters['kode_kegiatan'];
                $arr = explode('.', $kode_kegiatan);
                $jum_array = count($arr);
                
                $cton1 = $c->getNewCriterion(Revisi8MasterKegiatanPeer::KODE_URUSAN, $kode_kegiatan . '%', Criteria::LIKE);
                $cton2 = $c->getNewCriterion(Revisi8MasterKegiatanPeer::KODE_PROGRAM2, $kode_kegiatan . '%', Criteria::LIKE);
                $cton1->addOr($cton2);
                
                if ($jum_array == 2) {
                    $cton3 = $c->getNewCriterion(Revisi8MasterKegiatanPeer::KODE_PROGRAM, $arr[1] . '%', Criteria::LIKE);
                    $cton1->addAnd($cton3);
                }
                $c->add($cton1);
            }
            if (isset($this->filters['nama_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi8MasterKegiatanPeer::NAMA_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(Revisi8MasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
                $c->add(Revisi8MasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
            }
            if (isset($this->filters['posisi_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi8MasterKegiatanPeer::POSISI, '');
                $criterion->addOr($c->getNewCriterion(Revisi8MasterKegiatanPeer::POSISI, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['posisi']) && $this->filters['posisi'] !== '') {
                $crt1 = new Criteria();
                $crt1->add(Revisi8RincianPeer::RINCIAN_LEVEL, $this->filters['posisi']);
                $rincian = Revisi8RincianPeer::doSelect($crt1);
                foreach ($rincian as $r) {
                    if ($r->getKegiatanCode() != '') {
                        $c->add(Revisi8MasterKegiatanPeer::KODE_KEGIATAN, $r->getKegiatanCode());
                    }
                }
            }
            if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
                $unit = $this->filters['unit_id'];
                $c->add(Revisi8MasterKegiatanPeer::UNIT_ID, $unit);
            }
        }
        elseif ($tahap == 'revisi9') {
            if (isset($this->filters['kode_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi9MasterKegiatanPeer::KODE_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(Revisi9MasterKegiatanPeer::KODE_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['kode_kegiatan']) && $this->filters['kode_kegiatan'] !== '') {
                $kode_kegiatan = $this->filters['kode_kegiatan'];
                $arr = explode('.', $kode_kegiatan);
                $jum_array = count($arr);
                
                $cton1 = $c->getNewCriterion(Revisi9MasterKegiatanPeer::KODE_URUSAN, $kode_kegiatan . '%', Criteria::LIKE);
                $cton2 = $c->getNewCriterion(Revisi9MasterKegiatanPeer::KODE_PROGRAM2, $kode_kegiatan . '%', Criteria::LIKE);
                $cton1->addOr($cton2);
                
                if ($jum_array == 2) {
                    $cton3 = $c->getNewCriterion(Revisi9MasterKegiatanPeer::KODE_PROGRAM, $arr[1] . '%', Criteria::LIKE);
                    $cton1->addAnd($cton3);
                }
                $c->add($cton1);
            }
            if (isset($this->filters['nama_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi9MasterKegiatanPeer::NAMA_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(Revisi9MasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
                $c->add(Revisi9MasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
            }
            if (isset($this->filters['posisi_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi9MasterKegiatanPeer::POSISI, '');
                $criterion->addOr($c->getNewCriterion(Revisi9MasterKegiatanPeer::POSISI, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['posisi']) && $this->filters['posisi'] !== '') {
                $crt1 = new Criteria();
                $crt1->add(Revisi9RincianPeer::RINCIAN_LEVEL, $this->filters['posisi']);
                $rincian = Revisi9RincianPeer::doSelect($crt1);
                foreach ($rincian as $r) {
                    if ($r->getKegiatanCode() != '') {
                        $c->add(Revisi9MasterKegiatanPeer::KODE_KEGIATAN, $r->getKegiatanCode());
                    }
                }
            }
            if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
                $unit = $this->filters['unit_id'];
                $c->add(Revisi9MasterKegiatanPeer::UNIT_ID, $unit);
            }
        }
        elseif ($tahap == 'revisi10') {
            if (isset($this->filters['kode_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi10MasterKegiatanPeer::KODE_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(Revisi10MasterKegiatanPeer::KODE_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['kode_kegiatan']) && $this->filters['kode_kegiatan'] !== '') {
                $kode_kegiatan = $this->filters['kode_kegiatan'];
                $arr = explode('.', $kode_kegiatan);
                $jum_array = count($arr);
                
                $cton1 = $c->getNewCriterion(Revisi10MasterKegiatanPeer::KODE_URUSAN, $kode_kegiatan . '%', Criteria::LIKE);
                $cton2 = $c->getNewCriterion(Revisi10MasterKegiatanPeer::KODE_PROGRAM2, $kode_kegiatan . '%', Criteria::LIKE);
                $cton1->addOr($cton2);
                
                if ($jum_array == 2) {
                    $cton3 = $c->getNewCriterion(Revisi10MasterKegiatanPeer::KODE_PROGRAM, $arr[1] . '%', Criteria::LIKE);
                    $cton1->addAnd($cton3);
                }
                $c->add($cton1);
            }
            if (isset($this->filters['nama_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi10MasterKegiatanPeer::NAMA_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(Revisi10MasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
                $c->add(Revisi10MasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
            }
            if (isset($this->filters['posisi_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi10MasterKegiatanPeer::POSISI, '');
                $criterion->addOr($c->getNewCriterion(Revisi10MasterKegiatanPeer::POSISI, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['posisi']) && $this->filters['posisi'] !== '') {
                $crt1 = new Criteria();
                $crt1->add(Revisi10RincianPeer::RINCIAN_LEVEL, $this->filters['posisi']);
                $rincian = Revisi10RincianPeer::doSelect($crt1);
                foreach ($rincian as $r) {
                    if ($r->getKegiatanCode() != '') {
                        $c->add(Revisi10MasterKegiatanPeer::KODE_KEGIATAN, $r->getKegiatanCode());
                    }
                }
            }
            if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
                $unit = $this->filters['unit_id'];
                $c->add(Revisi10MasterKegiatanPeer::UNIT_ID, $unit);
            }
        }
        elseif ($tahap == 'rkua') {
            if (isset($this->filters['kode_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(RkuaMasterKegiatanPeer::KODE_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(RkuaMasterKegiatanPeer::KODE_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['kode_kegiatan']) && $this->filters['kode_kegiatan'] !== '') {
                $kode_kegiatan = $this->filters['kode_kegiatan'];
                $arr = explode('.', $kode_kegiatan);
                $jum_array = count($arr);
                //if($jum_array<2)
                //{
                $cton1 = $c->getNewCriterion(RkuaMasterKegiatanPeer::KODE_URUSAN, $kode_kegiatan . '%', Criteria::LIKE);
                $cton2 = $c->getNewCriterion(RkuaMasterKegiatanPeer::KODE_PROGRAM2, $kode_kegiatan . '%', Criteria::LIKE);
                $cton1->addOr($cton2);
                //$c->add($cton1);
                //}
                if ($jum_array == 2) {
                    //$c->add(PakBukuBiruMasterKegiatanPeer::KODE_PROGRAM,$arr[1],Criteria::LIKE);
                    $cton3 = $c->getNewCriterion(RkuaMasterKegiatanPeer::KODE_PROGRAM, $arr[1] . '%', Criteria::LIKE);
                    $cton1->addAnd($cton3);
                }
                $c->add($cton1);
            }
            if (isset($this->filters['nama_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(RkuaMasterKegiatanPeer::NAMA_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(RkuaMasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
                $c->add(RkuaMasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
            }
            if (isset($this->filters['posisi_is_empty'])) {
                $criterion = $c->getNewCriterion(RkuaMasterKegiatanPeer::POSISI, '');
                $criterion->addOr($c->getNewCriterion(RkuaMasterKegiatanPeer::POSISI, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['posisi']) && $this->filters['posisi'] !== '') {
                //$query="select *
                //from ". sfConfig::get('app_default_schema') .".rincian r, ". sfConfig::get('app_default_schema') .".master_kegiatan mk
                //where r.rincian_level=".$this->filters['posisi']." and r.kegiatan_code=mk.kode_kegiatan";
                $crt1 = new Criteria();
                $crt1->add(RkuaRincianPeer::RINCIAN_LEVEL, $this->filters['posisi']);
                $rincian = RkuaRincianPeer::doSelect($crt1);
                foreach ($rincian as $r) {
                    if ($r->getKegiatanCode() != '') {
                        $c->add(RkuaMasterKegiatanPeer::KODE_KEGIATAN, $r->getKegiatanCode());
                    }
                }
            }
            if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
                $unit = $this->filters['unit_id'];
                $c->add(RkuaMasterKegiatanPeer::UNIT_ID, $unit);
            }
        } else {
            if (isset($this->filters['kode_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(DinasMasterKegiatanPeer::KODE_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(DinasMasterKegiatanPeer::KODE_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['kode_kegiatan']) && $this->filters['kode_kegiatan'] !== '') {
                $kode_kegiatan = $this->filters['kode_kegiatan'];
                $arr = explode('.', $kode_kegiatan);
                $jum_array = count($arr);
                //if($jum_array<2)
                //{
                $cton1 = $c->getNewCriterion(DinasMasterKegiatanPeer::KODE_URUSAN, $kode_kegiatan . '%', Criteria::LIKE);
                $cton2 = $c->getNewCriterion(DinasMasterKegiatanPeer::KODE_PROGRAM2, $kode_kegiatan . '%', Criteria::LIKE);
                $cton1->addOr($cton2);
                //$c->add($cton1);
                //}
                if ($jum_array == 2) {
                    //$c->add(DinasMasterKegiatanPeer::KODE_PROGRAM,$arr[1],Criteria::LIKE);
                    $cton3 = $c->getNewCriterion(DinasMasterKegiatanPeer::KODE_PROGRAM, $arr[1] . '%', Criteria::LIKE);
                    $cton1->addAnd($cton3);
                }
                $c->add($cton1);
            }
            if (isset($this->filters['nama_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(DinasMasterKegiatanPeer::NAMA_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(DinasMasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
                $c->add(DinasMasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
            }
            if (isset($this->filters['posisi_is_empty'])) {
                $criterion = $c->getNewCriterion(DinasMasterKegiatanPeer::POSISI, '');
                $criterion->addOr($c->getNewCriterion(DinasMasterKegiatanPeer::POSISI, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['posisi']) && $this->filters['posisi'] !== '') {
                //$query="select *
                //from ". sfConfig::get('app_default_schema') .".rincian r, ". sfConfig::get('app_default_schema') .".master_kegiatan mk
                //where r.rincian_level=".$this->filters['posisi']." and r.kegiatan_code=mk.kode_kegiatan";
                $crt1 = new Criteria();
                $crt1->add(DinasRincianPeer::RINCIAN_LEVEL, $this->filters['posisi']);
                $rincian = DinasRincianPeer::doSelect($crt1);
                foreach ($rincian as $r) {
                    if ($r->getKegiatanCode() != '') {
                        $c->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $r->getKegiatanCode());
                    }
                }
            }
//        if (isset($this->filters['tahap_is_empty'])) {
//            $criterion = $c->getNewCriterion(DinasMasterKegiatanPeer::TAHAP, '');
//            $criterion->addOr($c->getNewCriterion(DinasMasterKegiatanPeer::TAHAP, null, Criteria::ISNULL));
//            $c->add($criterion);
//        } else if (isset($this->filters['tahap']) && $this->filters['tahap'] !== '') {
//            $c->add(DinasMasterKegiatanPeer::TAHAP, $this->filters['tahap'] . '%', Criteria::LIKE);
//        }
            if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
                $unit = $this->filters['unit_id'];
                $c->add(DinasMasterKegiatanPeer::UNIT_ID, $unit);
            }
        }
    }

    protected function addFiltersCriteria($c) {
        if (isset($this->filters['kode_kegiatan_is_empty'])) {
            $criterion = $c->getNewCriterion(DinasMasterKegiatanPeer::KODE_KEGIATAN, '');
            $criterion->addOr($c->getNewCriterion(DinasMasterKegiatanPeer::KODE_KEGIATAN, null, Criteria::ISNULL));
            $c->add($criterion);
        } else if (isset($this->filters['kode_kegiatan']) && $this->filters['kode_kegiatan'] !== '') {
            $kode_kegiatan = $this->filters['kode_kegiatan'];
            $arr = explode('.', $kode_kegiatan);
            $jum_array = count($arr);
//if($jum_array<2)
//{
            $cton1 = $c->getNewCriterion(DinasMasterKegiatanPeer::KODE_URUSAN, $kode_kegiatan . '%', Criteria::LIKE);
            $cton2 = $c->getNewCriterion(DinasMasterKegiatanPeer::KODE_PROGRAM2, $kode_kegiatan . '%', Criteria::LIKE);
            $cton1->addOr($cton2);
//$c->add($cton1);
//}
            if ($jum_array == 2) {
//$c->add(DinasMasterKegiatanPeer::KODE_PROGRAM,$arr[1],Criteria::LIKE);
                $cton3 = $c->getNewCriterion(DinasMasterKegiatanPeer::KODE_PROGRAM, $arr[1] . '%', Criteria::LIKE);
                $cton1->addAnd($cton3);
            }
            $c->add($cton1);
        }
        if (isset($this->filters['nama_kegiatan_is_empty'])) {
            $criterion = $c->getNewCriterion(DinasMasterKegiatanPeer::NAMA_KEGIATAN, '');
            $criterion->addOr($c->getNewCriterion(DinasMasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
            $c->add($criterion);
        } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
            $c->add(DinasMasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
        }
        if (isset($this->filters['posisi_is_empty'])) {
            $criterion = $c->getNewCriterion(DinasMasterKegiatanPeer::POSISI, '');
            $criterion->addOr($c->getNewCriterion(DinasMasterKegiatanPeer::POSISI, null, Criteria::ISNULL));
            $c->add($criterion);
        } else if (isset($this->filters['posisi']) && $this->filters['posisi'] !== '') {
//$query="select *
//from ". sfConfig::get('app_default_schema') .".rincian r, ". sfConfig::get('app_default_schema') .".master_kegiatan mk
//where r.rincian_level=".$this->filters['posisi']." and r.kegiatan_code=mk.kode_kegiatan";
            $crt1 = new Criteria();
            $crt1->add(DinasRincianPeer::RINCIAN_LEVEL, $this->filters['posisi']);
            $rincian = DinasRincianPeer::doSelect($crt1);
            foreach ($rincian as $r) {
                if ($r->getKegiatanCode() != '') {
                    $c->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $r->getKegiatanCode());
                }
            }
        }
        if (isset($this->filters['tahap_is_empty'])) {
            $criterion = $c->getNewCriterion(DinasMasterKegiatanPeer::TAHAP, '');
            $criterion->addOr($c->getNewCriterion(DinasMasterKegiatanPeer::TAHAP, null, Criteria::ISNULL));
            $c->add($criterion);
        } else if (isset($this->filters['tahap']) && $this->filters['tahap'] !== '') {
            $c->add(DinasMasterKegiatanPeer::TAHAP, $this->filters['tahap'] . '%', Criteria::LIKE);
        }
        if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
            $unit = $this->filters['unit_id'];
            $c->add(DinasMasterKegiatanPeer::UNIT_ID, $unit);
        }
    }

    public function executeEditRevisi() {
        if ($this->getRequestParameter('unit_id')) {
            $unit_id = $this->getRequestParameter('unit_id');
            $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
            $this->tahap = $this->getRequestParameter('tahap');

            $c = new Criteria();
            $c->add(DinasSubtitleIndikatorPeer::UNIT_ID, $unit_id);
            $c->add(DinasSubtitleIndikatorPeer::KEGIATAN_CODE, $kode_kegiatan);
            $c->add(DinasSubtitleIndikatorPeer::SUBTITLE, 'Penunjang Kinerja%', Criteria::NOT_ILIKE);
            $c->addAscendingOrderByColumn(DinasSubtitleIndikatorPeer::PRIORITAS);
            $c->addAscendingOrderByColumn(DinasSubtitleIndikatorPeer::SUBTITLE);
            $rs_subtitle = DinasSubtitleIndikatorPeer::doSelect($c);

            $this->rs_subtitle = $rs_subtitle;
            $this->rinciandetail = '';
            $this->rs_rinciandetail = '';

            if (sfConfig::get('app_fasilitas_warningRekening') == 'buka') {
//untuk warning
                $query = "select sum(nilai_anggaran) as tot
                              from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail
                              where status_hapus=FALSE and unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rekening_code like '5.2.1%' and status_hapus=false";
                $con = Propel::getConnection();
                $statement = $con->prepareStatement($query);
                $rs = $statement->executeQuery();
                while ($rs->next())
                    $total_menjadi_1 = $rs->getString('tot');

                $query = "select sum(nilai_anggaran) as tot
                              from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail
                              where status_hapus=FALSE and unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rekening_code like '5.2.2%' and status_hapus=false";
                $con = Propel::getConnection();
                $statement = $con->prepareStatement($query);
                $rs = $statement->executeQuery();
                while ($rs->next())
                    $total_menjadi_2 = $rs->getString('tot');

                $query = "select sum(nilai_anggaran) as tot
                              from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail
                              where status_hapus=FALSE and unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rekening_code like '5.2.3%' and status_hapus=false";
                $con = Propel::getConnection();
                $statement = $con->prepareStatement($query);
                $rs = $statement->executeQuery();
                while ($rs->next())
                    $total_menjadi_3 = $rs->getString('tot');

                if ($unit_id == '9999') {
                    $query = "select sum(nilai_anggaran) as tot
                              from " . sfConfig::get('app_default_schema') . ".titiknol_rincian_detail
                              where status_hapus=FALSE and unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rekening_code like '5.2.1%' and status_hapus=false";

                    $con = Propel::getConnection();
                    $statement = $con->prepareStatement($query);
                    $rs = $statement->executeQuery();
                    while ($rs->next())
                        $total_semula_1 = $rs->getString('tot');

                    $query = "select sum(nilai_anggaran) as tot
                              from " . sfConfig::get('app_default_schema') . ".titiknol_rincian_detail
                              where status_hapus=FALSE and unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rekening_code like '5.2.2%' and status_hapus=false";

                    $con = Propel::getConnection();
                    $statement = $con->prepareStatement($query);
                    $rs = $statement->executeQuery();
                    while ($rs->next())
                        $total_semula_2 = $rs->getString('tot');

                    $query = "select sum(nilai_anggaran) as tot
                              from " . sfConfig::get('app_default_schema') . ".titiknol_rincian_detail
                              where status_hapus=FALSE and unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rekening_code like '5.2.3%' and status_hapus=false";

                    $con = Propel::getConnection();
                    $statement = $con->prepareStatement($query);
                    $rs = $statement->executeQuery();
                    while ($rs->next())
                        $total_semula_3 = $rs->getString('tot');
                } else {
                    $query = "select sum(nilai_anggaran) as tot
                              from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail
                              where status_hapus=FALSE and unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rekening_code like '5.2.1%' and status_hapus=false";

                    $con = Propel::getConnection();
                    $statement = $con->prepareStatement($query);
                    $rs = $statement->executeQuery();
                    while ($rs->next())
                        $total_semula_1 = $rs->getString('tot');

                    $query = "select sum(nilai_anggaran) as tot
                              from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail
                              where status_hapus=FALSE and unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rekening_code like '5.2.2%' and status_hapus=false";

                    $con = Propel::getConnection();
                    $statement = $con->prepareStatement($query);
                    $rs = $statement->executeQuery();
                    while ($rs->next())
                        $total_semula_2 = $rs->getString('tot');

                    $query = "select sum(nilai_anggaran) as tot
                              from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail
                              where status_hapus=FALSE and unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rekening_code like '5.2.3%' and status_hapus=false";

                    $con = Propel::getConnection();
                    $statement = $con->prepareStatement($query);
                    $rs = $statement->executeQuery();
                    while ($rs->next())
                        $total_semula_3 = $rs->getString('tot');
                }



                $this->selisih_1 = number_format($total_menjadi_1 - $total_semula_1, 0, ',', '.');
                $this->selisih_2 = number_format($total_menjadi_2 - $total_semula_2, 0, ',', '.');
                $this->selisih_3 = number_format($total_menjadi_3 - $total_semula_3, 0, ',', '.');

                $this->total_menjadi_1 = number_format($total_menjadi_1, 0, ',', '.');
                $this->total_menjadi_2 = number_format($total_menjadi_2, 0, ',', '.');
                $this->total_menjadi_3 = number_format($total_menjadi_3, 0, ',', '.');

                $this->total_semula_1 = number_format($total_semula_1, 0, ',', '.');
                $this->total_semula_2 = number_format($total_semula_2, 0, ',', '.');
                $this->total_semula_3 = number_format($total_semula_3, 0, ',', '.');
//end of warning
            }
        } else {
            $this->forward404();
        }
    }

    public function executeGetPekerjaansRevisi() {
        if ($this->getRequestParameter('id')) {
            $sub_id = $this->getRequestParameter('id');
            $c = new Criteria();
            $c->add(DinasSubtitleIndikatorPeer::SUB_ID, $sub_id);
            $c->add(DinasSubtitleIndikatorPeer::PRIORITAS, 0, Criteria::NOT_EQUAL);
            $rs_subtitle = DinasSubtitleIndikatorPeer::doSelectOne($c);
            if ($rs_subtitle) {
                $unit_id = $rs_subtitle->getUnitId();
                $kegiatan_code = $rs_subtitle->getKegiatanCode();
                $subtitle = $rs_subtitle->getSubtitle();
                $nama_subtitle = trim($subtitle);
            }

            $c_rincianDetail = new Criteria();
            $c_rincianDetail->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
            $c_rincianDetail->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
            $c_rincianDetail->add(DinasRincianDetailPeer::SUBTITLE, $nama_subtitle, Criteria::ILIKE);
            $c_rincianDetail->add(DinasRincianDetailPeer::STATUS_HAPUS, false);
            $c_rincianDetail->add(DinasRincianDetailPeer::TAHUN, sfConfig::get('app_tahun_default'));
//$c_rincianDetail->addAscendingOrderByColumn(DinasRincianDetailPeer::SUB);
            $c_rincianDetail->addAscendingOrderByColumn(DinasRincianDetailPeer::KODE_SUB);
            $c_rincianDetail->addAscendingOrderByColumn(DinasRincianDetailPeer::REKENING_CODE);
            $c_rincianDetail->addDescendingOrderByColumn(DinasRincianDetailPeer::NILAI_ANGGARAN);

            $c_rincianDetail->addAscendingOrderByColumn(DinasRincianDetailPeer::LOKASI_KECAMATAN);
            $c_rincianDetail->addAscendingOrderByColumn(DinasRincianDetailPeer::LOKASI_KELURAHAN);
            $c_rincianDetail->addAscendingOrderByColumn(DinasRincianDetailPeer::KOMPONEN_NAME);
            $rs_rd = DinasRincianDetailPeer::doSelect($c_rincianDetail);
            $this->rs_rd = $rs_rd;

            $this->id = $sub_id;
            $this->rinciandetail = 'ada';
            $this->setLayout('kosong');
        }
    }

    public function executeBuatCatatan() {
        $unit_id = $this->unit_id = $this->getRequestParameter('unitid');
        $kode_kegiatan = $this->kode_kegiatan = $this->getRequestParameter('kodekegiatan');
        $detail_no = $this->detail_no = $this->getRequestParameter('detailno');
        $c = new Criteria();
        $c->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
        $c->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
        $c->add(DinasRincianDetailPeer::DETAIL_NO, $detail_no);
        $rs = DinasRincianDetailPeer::doSelectOne($c);
        if ($this->getUser()->hasCredential('bappeko')) {
            $this->catatan = $rs->getNoteBappeko();
        } elseif ($this->getUser()->getNamaUser() == 'anggaran') {
            $this->catatan = $rs->getNoteTapd();
        }
    }

    public function executeSimpanCatatan() {
        $unit_id = $this->getRequestParameter('unit_id');
        $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
        $detail_no = $this->getRequestParameter('detail_no');
        $catatan = $this->getRequestParameter('catatan');
        $catatan = str_replace("'", "''", $catatan);
        $c = new Criteria();
        $c->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
        $c->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
        $c->add(DinasRincianDetailPeer::DETAIL_NO, $detail_no);
        $rs = DinasRincianDetailPeer::doSelectOne($c);
        if ($this->getUser()->hasCredential('bappeko')) {
            $rs->setNoteBappeko($catatan);
        } elseif ($this->getUser()->getNamaUser() == 'anggaran') {
            $rs->setNoteTapd($catatan);
        }
        $rs->save();
        $this->setFlash('berhasil', 'Catatan Telah Disimpan.');
        return $this->redirect("anggaran/editRevisi?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
    }

    public function executeBuatCatatanBappeko() {
        $unit_id = $this->unit_id = $this->getRequestParameter('unitid');
        $kode_kegiatan = $this->kode_kegiatan = $this->getRequestParameter('kodekegiatan');
        $detail_no = $this->detail_no = $this->getRequestParameter('detailno');
        $c = new Criteria();
        $c->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
        $c->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
        $c->add(DinasRincianDetailPeer::DETAIL_NO, $detail_no);
        $rs = DinasRincianDetailPeer::doSelectOne($c);
        $this->catatan = $rs->getNoteBappeko();
    }

    public function executeSimpanCatatanBappeko() {
        $unit_id = $this->getRequestParameter('unit_id');
        $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
        $detail_no = $this->getRequestParameter('detail_no');
        $catatan = $this->getRequestParameter('catatan');
        $catatan = str_replace("'", "''", $catatan);
        $c = new Criteria();
        $c->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
        $c->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
        $c->add(DinasRincianDetailPeer::DETAIL_NO, $detail_no);
        $rs = DinasRincianDetailPeer::doSelectOne($c);
        $rs->setNoteTapd($catatan);
        $rs->save();
        $this->setFlash('berhasil', 'Catatan Telah Disimpan.');
        return $this->redirect("anggaran/editRevisi?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
    }

    public function executeKrka() {
        $this->processFilterskrka();
        $this->filters = $this->getUser()->getAttributeHolder()->getAll('sf_admin/krka/filters');

        $pagers = new sfPropelPager('RincianDetail', 50);
        $c = new Criteria();
//$c->addAscendingOrderByColumn(RekeningPeer::REKENING_CODE);
        $c->addAscendingOrderByColumn(RincianDetailPeer::UNIT_ID);
////schema public
        $c->addJoin(RincianDetailPeer::UNIT_ID, UnitKerjaPeer::UNIT_ID, Criteria::LEFT_JOIN);
        $c->add(RincianDetailPeer::UNIT_ID, '9999', Criteria::NOT_EQUAL);
        /* $c->addSelectColumn(RincianDetailPeer::UNIT_ID);
          $c->addSelectColumn(UnitKerjaPeer::UNIT_NAME);
          $c->addSelectColumn(RincianDetailPeer::KEGIATAN_CODE);
          $c->addSelectColumn(RincianDetailPeer::KOMPONEN_NAME);
          $c->addSelectColumn(RincianDetailPeer::KOMPONEN_ID);
          $c->addSelectColumn(RincianDetailPeer::KOMPONEN_HARGA_AWAL);
          $c->addSelectColumn(RincianDetailPeer::VOLUME);
          $c->addSelectColumn(RincianDetailPeer::PAJAK); */
        $this->addFiltersCriteriakrka($c);
        $dinas_array = $this->getUser()->getAttributeHolder()->getAll('dinas');


        $pagers->setCriteria($c);
        $pagers->setPage($this->getRequestParameter('page', 1));
        $pagers->init();

        $this->pager = $pagers;
    }

    public function executeKrkaRevisi() {
        $this->processFilterskrkaRevisi();
        $this->filters = $this->getUser()->getAttributeHolder()->getAll('sf_admin/krkaRevisi/filters');

        $pagers = new sfPropelPager('DinasRincianDetail', 50);
        $c = new Criteria();
        $c->addAscendingOrderByColumn(DinasRincianDetailPeer::UNIT_ID);
////schema public
        $c->addJoin(DinasRincianDetailPeer::UNIT_ID, UnitKerjaPeer::UNIT_ID, Criteria::LEFT_JOIN);
        $c->add(DinasRincianDetailPeer::UNIT_ID, '9999', Criteria::NOT_EQUAL);
        $this->addFiltersCriteriakrkaRevisi($c);
        $dinas_array = $this->getUser()->getAttributeHolder()->getAll('dinas');


        $pagers->setCriteria($c);
        $pagers->setPage($this->getRequestParameter('page', 1));
        $pagers->init();

        $this->pager = $pagers;
    }

    protected function processFilterskrka() {
        if ($this->getRequest()->hasParameter('filter')) {
            $filters = $this->getRequestParameter('filters');

            $this->getUser()->getAttributeHolder()->removeNamespace('sf_admin/krka/filters');
            $this->getUser()->getAttributeHolder()->add($filters, 'sf_admin/krka/filters');
//$searchOption = $this->getRequestParameter('search_option');
//$this->getUser()->setAttribute('searchOption', $searchOption);
//echo $searchOption;exit;
        }
    }

    protected function processFilterskrkaRevisi() {
        if ($this->getRequest()->hasParameter('filter')) {
            $filters = $this->getRequestParameter('filters');

            $this->getUser()->getAttributeHolder()->removeNamespace('sf_admin/krkaRevisi/filters');
            $this->getUser()->getAttributeHolder()->add($filters, 'sf_admin/krkaRevisi/filters');
        }
    }

    protected function addFiltersCriteriakrka($c) {
        if (isset($this->filters['select'])) {
            if ($this->filters['select'] == 1) {
                if (isset($this->filters['komponen_name_is_empty'])) {
                    $criterion = $c->getNewCriterion(RincianDetailPeer::KOMPONEN_NAME, '');
                    $criterion->addOr($c->getNewCriterion(RincianDetailPeer::KOMPONEN_NAME, null, Criteria::ISNULL));
                    $c->add($criterion);
                } else if (isset($this->filters['komponen']) && $this->filters['komponen'] !== '') {
                    $kata = '%' . $this->filters['komponen'] . '%';
                    $c->add(RincianDetailPeer::KOMPONEN_NAME, strtr($kata, '*', '%'), Criteria::ILIKE);
                    $c->add(RincianDetailPeer::STATUS_HAPUS, FALSE);
//$c->addJoin(RincianDetailPeer::UNIT_ID, UnitKerjaPeer::UNIT_ID);
                }
            } elseif ($this->filters['select'] == 2) {
                if (isset($this->filters['komponen_id_is_empty'])) {
                    $criterion = $c->getNewCriterion(RincianDetailPeer::KOMPONEN_ID, '');
                    $criterion->addOr($c->getNewCriterion(RincianDetailPeer::KOMPONEN_ID, null, Criteria::ISNULL));
//$criterion->addJoin($c->getNewCriterion())
                    $c->add($criterion);
                } else if (isset($this->filters['komponen']) && $this->filters['komponen'] !== '') {
                    $kata = '%' . $this->filters['komponen'] . '%';
//                                echo $kata;
                    $c->add(RincianDetailPeer::KOMPONEN_ID, strtr($kata, '*', '%'), Criteria::ILIKE);
                    $c->add(RincianDetailPeer::STATUS_HAPUS, FALSE);
//$c->addJoin(RincianDetailPeer::UNIT_ID, UnitKerjaPeer::UNIT_ID);
                }
            } elseif ($this->filters['select'] == 3) {
                if (isset($this->filters['rekening_code_is_empty'])) {
                    $criterion = $c->getNewCriterion(RincianDetailPeer::REKENING_CODE, '');
                    $criterion->addOr($c->getNewCriterion(RincianDetailPeer::REKENING_CODE, null, Criteria::ISNULL));
                    $c->add($criterion);
                } else if (isset($this->filters['komponen']) && $this->filters['komponen'] !== '') {
                    $kata = '%' . $this->filters['komponen'] . '%';
                    $c->add(RincianDetailPeer::REKENING_CODE, strtr($kata, '*', '%'), Criteria::ILIKE);
                    $c->add(RincianDetailPeer::STATUS_HAPUS, FALSE);
//$c->addJoin(RincianDetailPeer::UNIT_ID, UnitKerjaPeer::UNIT_ID);
                }
            } else {
                if (isset($this->filters['komponen_is_empty'])) {
                    echo 'is empty';
                    $criterion = $c->getNewCriterion(RincianDetailPeer::KOMPONEN_NAME, '');
                    $criterion->addOr($c->getNewCriterion(RincianDetailPeer::KOMPONEN_NAME, null, Criteria::ISNULL));
                    $c->add($criterion);
                } else if (isset($this->filters['komponen']) && $this->filters['komponen'] !== '') {
//				$kata = '%'.$this->filters['komponen'].'%';
//				$c->add(RincianDetailPeer::KOMPONEN_NAME, strtr($kata, '*', '%'), Criteria::ILIKE);
                    $c->add(RincianDetailPeer::STATUS_HAPUS, FALSE);
// $c->addJoin(RincianDetailPeer::UNIT_ID, UnitKerjaPeer::UNIT_ID);
                }
            }
        }
    }

    protected function addFiltersCriteriakrkaRevisi($c) {
        if (isset($this->filters['select'])) {
            if ($this->filters['select'] == 1) {
                if (isset($this->filters['komponen_name_is_empty'])) {
                    $criterion = $c->getNewCriterion(DinasRincianDetailPeer::KOMPONEN_NAME, '');
                    $criterion->addOr($c->getNewCriterion(DinasRincianDetailPeer::KOMPONEN_NAME, null, Criteria::ISNULL));
                    $c->add($criterion);
                } else if (isset($this->filters['komponen']) && $this->filters['komponen'] !== '') {
                    $kata = '%' . $this->filters['komponen'] . '%';
                    $c->add(DinasRincianDetailPeer::KOMPONEN_NAME, strtr($kata, '*', '%'), Criteria::ILIKE);
                    $c->add(DinasRincianDetailPeer::STATUS_HAPUS, FALSE);
                }
            } elseif ($this->filters['select'] == 2) {
                if (isset($this->filters['komponen_id_is_empty'])) {
                    $criterion = $c->getNewCriterion(DinasRincianDetailPeer::KOMPONEN_ID, '');
                    $criterion->addOr($c->getNewCriterion(DinasRincianDetailPeer::KOMPONEN_ID, null, Criteria::ISNULL));
                    $c->add($criterion);
                } else if (isset($this->filters['komponen']) && $this->filters['komponen'] !== '') {
                    $kata = '%' . $this->filters['komponen'] . '%';
                    $c->add(DinasRincianDetailPeer::KOMPONEN_ID, strtr($kata, '*', '%'), Criteria::ILIKE);
                    $c->add(DinasRincianDetailPeer::STATUS_HAPUS, FALSE);
                }
            } elseif ($this->filters['select'] == 3) {
                if (isset($this->filters['rekening_code_is_empty'])) {
                    $criterion = $c->getNewCriterion(DinasRincianDetailPeer::REKENING_CODE, '');
                    $criterion->addOr($c->getNewCriterion(DinasRincianDetailPeer::REKENING_CODE, null, Criteria::ISNULL));
                    $c->add($criterion);
                } else if (isset($this->filters['komponen']) && $this->filters['komponen'] !== '') {
                    $kata = '%' . $this->filters['komponen'] . '%';
                    $c->add(DinasRincianDetailPeer::REKENING_CODE, strtr($kata, '*', '%'), Criteria::ILIKE);
                    $c->add(DinasRincianDetailPeer::STATUS_HAPUS, FALSE);
                }
            } else {
                if (isset($this->filters['komponen_is_empty'])) {
                    echo 'is empty';
                    $criterion = $c->getNewCriterion(DinasRincianDetailPeer::KOMPONEN_NAME, '');
                    $criterion->addOr($c->getNewCriterion(DinasRincianDetailPeer::KOMPONEN_NAME, null, Criteria::ISNULL));
                    $c->add($criterion);
                } else if (isset($this->filters['komponen']) && $this->filters['komponen'] !== '') {
                    $c->add(DinasRincianDetailPeer::STATUS_HAPUS, FALSE);
                }
            }
        }
    }

    public function executeBuatCatatanKomponen() {
        $unit_id = $this->unit = $this->getRequestParameter('unit');
        $kode_kegiatan = $this->kegiatan = $this->getRequestParameter('kegiatan');
        $detail_no = $this->id = $this->getRequestParameter('id');
        $detail_kegiatan = $unit_id . '.' . $kode_kegiatan . '.' . $detail_no;
        
        $con = Propel::getConnection();
        $query = " select note_mondal"
                . " from " . sfConfig::get('app_default_schema') . ".buka_komponen_murni "
                . " where detail_kegiatan = '$detail_kegiatan' ";
        $stmt = $con->prepareStatement($query);
        $rs = $stmt->executeQuery();
        while ($rs->next()) $this->note_mondal = $rs->getString('note_mondal');
    }

    public function executeBukaKomponenMurni() {
        $unit_id = $this->getRequestParameter('unit');
        $kegiatan_code = $this->getRequestParameter('kegiatan');
        $detail_no = $this->getRequestParameter('id');
        $catatan = $this->getRequestParameter('catatan');
        $catatan = str_replace("'", "''", $catatan);

        $detail_kegiatan = $unit_id . '.' . $kegiatan_code . '.' . $detail_no;

        $con = Propel::getConnection();

        $query =
        "INSERT INTO " . sfConfig::get('app_default_schema') . ".buka_komponen_murni
        VALUES ('$detail_kegiatan', '$catatan')";
        $stmt = $con->prepareStatement($query);
        $stmt->executeQuery();
        $con->commit();

        $this->setFlash('berhasil', 'Berhasil buka komponen');
        return $this->redirect("anggaran/editRevisi?unit_id=$unit_id&kode_kegiatan=$kegiatan_code");
    }

    public function executeBukaKomponenMurniPerKegiatan() {
        $unit_id = $this->getRequestParameter('unit_id');
        $kegiatan_code = $this->getRequestParameter('kegiatan_code');

        $con = Propel::getConnection();

        $query =
        "INSERT INTO " . sfConfig::get('app_default_schema') . ".buka_komponen_murni
        (SELECT detail_kegiatan FROM " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail
        WHERE kegiatan_code = '$kegiatan_code'
        AND unit_id = '$unit_id'
        AND status_hapus = FALSE)";
        $stmt = $con->prepareStatement($query);
        $stmt->executeQuery();
        $con->commit();

        $this->setFlash('berhasil', "Berhasil buka komponen untuk kegiatan $kegiatan_code");
        return $this->redirect('anggaran/listRevisi?unit_id=' . $unit_id);
    }

    public function executeGetKodeKegiatanByPd(){
        $unit_id = $this->getRequestParameter('unit_id');
        $menu = $this->getRequestParameter('menu');
        $tahap = $this->getRequestParameter('tahap');

        if(strlen($unit_id) < 4){
            $unit_id = "0"."$unit_id";
        }

        // die($unit_id);
        if($tahap == 'murni'){
            $c = new Criteria();
            $c->add(MurniMasterKegiatanPeer::UNIT_ID, $unit_id, Criteria::EQUAL);
            if($menu == 'Pendapatan'){
                $c->add(MurniMasterKegiatanPeer::IS_BTL,TRUE);
            }else{
                $c->add(MurniMasterKegiatanPeer::IS_BTL,FALSE);
            }
            $c->addAscendingOrderByColumn('CAST('.MurniMasterKegiatanPeer::KODE_KEGIATAN.' AS INT)');
            $rs_user = MurniMasterKegiatanPeer::doSelect($c);
        }else{
            $c = new Criteria();
            $c->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id, Criteria::EQUAL);
            if($menu == 'Pendapatan'){
                $c->add(DinasMasterKegiatanPeer::IS_BTL,TRUE);
            }else{
                $c->add(DinasMasterKegiatanPeer::IS_BTL,FALSE);
            }
            $c->addAscendingOrderByColumn('CAST('.DinasMasterKegiatanPeer::KODE_KEGIATAN.' AS INT)');
            $rs_user = DinasMasterKegiatanPeer::doSelect($c);
        }

        $data['html'] = '<option value="">--Pilih Kode--</option>';
        foreach($rs_user as $key => $value) {
            $data['html'] .= '<option value="'.$value->getKodeKegiatan().'">'.$value->getKodeKegiatan().'</option>';
        }

        return $this->renderText(json_encode($data));
    }

}

?>
