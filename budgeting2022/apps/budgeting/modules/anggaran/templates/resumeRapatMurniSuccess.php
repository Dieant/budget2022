<?php use_helper('I18N', 'Date', 'Url', 'Javascript', 'Form', 'Object', 'Number', 'Validation') ?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>Daftar Isian Rincian Resume Rapat</h1>
</section>

<!-- Main content -->
<section class="content">
    <?php include_partial('anggaran/list_messages'); ?>
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Form Keterangan Resume Rapat</h3>
        </div>
        <div class="box-body">
            <?php echo form_tag('anggaran/prosesResumeRapatMurni', array('method' => 'post', 'class' => 'form-horizontal')) ?>
            <?php echo input_hidden_tag('unit_id', $unit_id) ?>
            <div class="form-group">
                <label class="col-sm-2 control-label">Kegiatan</label>
                <div class="col-sm-10">
                    <?php echo input_tag('kode_kegiatan', $kode_kegiatan, array('readonly'=>'true')) ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Tambah Kegiatan</label>
                <div class="col-sm-10">
                    <?php echo select_tag('arr_kegiatan', options_for_select($options, '', array('include_blank' => true)), array('id' => 'sub', 'class' => 'js-example-basic-multiple', 'style' => 'width:100%', 'multiple' => 'multiple')); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Tempat</label>
                <div class="col-sm-10">
                    <?php echo input_tag('tempat', 'Graha Sawunggaling Gedung Pemerintah Kota Surabaya Lantai 6', array('class' => 'form-control', 'placeholder' => 'Tempat rapat', 'required' => 'true')); ?>
                    <?php //echo input_tag('tempat', 'Ruang Sidang Sekretaris Daerah Kota Surabaya', array('class' => 'form-control', 'placeholder' => 'Tempat rapat', 'required' => 'true')); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Acara</label>
                <div class="col-sm-10">
                    <?php
                    $temp_tahap = '';
                    if ($tahap == 'murni') {
                        $temp_tahap = 'Murni';
                    } elseif ($tahap == 'revisi1') {
                        $temp_tahap = 'Revisi I';
                    } elseif ($tahap == 'revisi2') {
                        $temp_tahap = 'Revisi II';
                    } elseif ($tahap == 'revisi3') {
                        $temp_tahap = 'Revisi III';
                    } elseif ($tahap == 'revisi4') {
                        $temp_tahap = 'Revisi IV';
                    } elseif ($tahap == 'revisi5') {
                        $temp_tahap = 'Revisi V';
                    } else
                        $temp_tahap = 'PAK';
                    ?>
                    <?php echo input_tag('acara', 'Membahas RKA 2017', array('class' => 'form-control', 'placeholder' => 'Nama acara', 'required' => 'true')); ?>
                </div>
            </div>
            <!--<div class="form-group">
                <label class="col-sm-2 control-label">&nbsp;</label>
                <div class="col-sm-10">
                    <?php //echo input_tag('acara2', '(Berdasarkan surat no. )', array('class' => 'form-control', 'placeholder' => 'Nama acara')); ?>
                </div>
            </div>-->
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <?php echo submit_tag('Print Resume Rapat', 'name=print class=btn btn-flat btn-success'); ?>
                    <?php //echo submit_tag('Save Excel', 'name=excel class=btn btn-flat btn-success'); ?>
                    <?php echo reset_tag('Reset', 'class=btn btn-flat btn-warning'); ?>
                </div>
            </div>
            <?php echo '</form>'; ?>
        </div>
    </div>
</section>
<script>
    $(document).ready(function () {
        $(".js-example-basic-multiple").select2();
    });
</script>