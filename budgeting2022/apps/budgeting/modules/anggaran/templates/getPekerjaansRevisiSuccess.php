<?php use_helper('Url', 'Javascript', 'Form', 'Object'); ?>
<?php
$status = $sf_user->getAttribute('status', '', 'status_dinas');
$i = 0;
$kode_sub = '';
$temp_rekening = '';
foreach ($rs_rd as $rd):
    $est_fisik = FALSE;
    $c = new Criteria();
    $c->add(KomponenPeer::KOMPONEN_ID, $rd->getKomponenId());
    $c->add(KomponenPeer::IS_EST_FISIK, TRUE);
    if ($rs_est_fisik = KomponenPeer::doSelectOne($c))
        $est_fisik = TRUE;

    $odd = fmod($i++, 2);
    $unit_id = $rd->getUnitId();
    $kegiatan_code = $rd->getKegiatanCode();
    if ($kode_sub != $rd->getKodeSub()) 
    {
        $kode_sub = $rd->getKodeSub();
        $sub = $rd->getSub();
        $cekKodeSub = substr($kode_sub, 0, 4);
        if ($cekKodeSub == 'RKAM') 
        {//RKA Member
            $C_RKA = new Criteria();
            $C_RKA->add(DinasRkaMemberPeer::KODE_SUB, $kode_sub);
            $C_RKA->addAscendingOrderByColumn(DinasRkaMemberPeer::KODE_SUB);
            $rs_rkam = DinasRkaMemberPeer::doSelectOne($C_RKA);
            if ($rs_rkam) 
            {
            ?>
                <tr class="pekerjaans_<?php echo $id ?>" bgcolor="#e6e6e6">
                    <td colspan="7">
                        <div id="<?php echo 'header_' . $rs_rkam->getKodeSub() ?>"><b> .:. <?php echo $rs_rkam->getKomponenName() . ' ' . $rs_rkam->getDetailName(); ?></b></div>
                    </td>
                    <td align="right">
                        <?php
                        echo number_format($rd->getTotalKodeSub($kode_sub), 0, ',', '.');
                        ?>
                    </td>
                    <td colspan="3">&nbsp;</td>
                </tr>
            <?php
            } 
        } 
        else 
        { //else form SUB
            $c = new Criteria();
            $c->add(DinasRincianSubParameterPeer::KODE_SUB, $kode_sub);
            $rs_subparameter = DinasRincianSubParameterPeer::doSelectOne($c);
            if ($rs_subparameter) 
            {
                ?>
                <tr class="pekerjaans_<?php echo $id ?>" bgcolor="#e6e6e6">
                    <td colspan="7">
                    <b> :. <?php echo $rs_subparameter->getSubKegiatanName() . ' ' . $rs_subparameter->getDetailName(); ?></b></td>
                    <td align="right">
                        <?php echo number_format($rd->getTotalKodeSub($kode_sub), 0, ',', '.'); ?>
                    </td>
                    <td colspan="3">&nbsp;</td>
                </tr>
            <?php
            } 
            else 
            {
                $ada = 'tidak';
                $query = "select * from " . sfConfig::get('app_default_schema') . ".dinas_rincian_sub_parameter "
                . "where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and new_subtitle ilike '%$sub%'";
                $con = Propel::getConnection();
                $stmt = $con->prepareStatement($query);
                $t = $stmt->executeQuery();
                while ($t->next()) 
                {
                    if ($t->getString('kode_sub')) {
                        ?>
                        <tr class="pekerjaans_<?php echo $id ?>" bgcolor="#e6e6e6">
                            <td colspan="7"><b> :. <?php echo $t->getString('sub_kegiatan_name') . ' ' . $t->getString('detail_name'); ?></b></td>
                            <td align="right">
                                <?php
                                echo number_format($rd->getTotalSub($sub), 0, ',', '.');
                                $ada = 'ada';
                                ?> 
                            </td>
                            <td colspan="3">&nbsp;</td>
                        </tr>
                        <?php
                        }
                }

                if ($ada == 'tidak') 
                {
                    if ($kode_sub != '') 
                    {
                        $query = "select * from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail "
                                . "where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and sub = '$sub' and status_hapus=false";
                        $con = Propel::getConnection();
                        $stmt = $con->prepareStatement($query);
                        $t = $stmt->executeQuery();
                        while ($t->next()) 
                        {
                            if ($t->getString('kode_sub')) 
                            {
                            ?>
                                <tr class="pekerjaans_<?php echo $id ?>" bgcolor="#e6e6e6">
                                    <td colspan="7">
                                        <b> :. <?php echo $t->getString('komponen_name') . ' ' . $t->getString('detail_name'); ?></b>
                                    </td>
                                    <td align="right">
                                        <?php
                                        echo number_format($rd->getTotalSub($sub), 0, ',', '.');
                                        $ada = 'ada';
                                        ?>
                                    </td>
                                    <td colspan="3">&nbsp;</td>
                                </tr>
                            <?php
                            }
                        }
                    }
                }
            }
        }
    }

    $rekening_code = $rd->getRekeningCode();
    if ($temp_rekening != $rekening_code) 
    {
        $temp_rekening = $rekening_code;
        $c = new Criteria();
        $c->add(RekeningPeer::REKENING_CODE, $rekening_code);
        $rs_rekening = RekeningPeer::doSelectOne($c);
        if ($rs_rekening) {
            $rekening_name = $rs_rekening->getRekeningName();
            ?>
            <tr class="pekerjaans_<?php echo $id ?>" bgcolor="#e6e6e6">
                <td colspan="7"><i><?php echo $rekening_code . ' ' . $rekening_name ?></i> </td>
                <td colspan="3">&nbsp;</td>
            </tr>
            <?php
        }
    }
    ?>
    <tr class="pekerjaans_<?php echo $id ?>">
        <td>
            <?php
            $kegiatan = $rd->getKegiatanCode();
            $unit = $rd->getUnitId();
            $no = $rd->getDetailNo();
            $sub = $rd->getSubtitle();
            $benar_musrenbang = 0;
            if ($rd->getIsMusrenbang() == 'TRUE') {
                $benar_musrenbang = 1;
            }
            $benar_prioritas = 0;
            if ($rd->getPrioritasWali() == 'TRUE') {
                $benar_prioritas = 1;
            }
            $benar_output = 0;
            if ($rd->getIsOutput() == 'TRUE') {
                $benar_output = 1;
            }
            $benar_multiyears = 0;
            if ($rd->getThKeMultiyears() <> null && $rd->getThKeMultiyears() > 0) {
                $benar_multiyears = 1;
            }
            $benar_hibah = 0;
            if ($rd->getIsHibah() == 'TRUE') {
                $benar_hibah = 1;
            }
            ?>
        </td>
        <?php
        $cek = FALSE;
        if ($cek or ( $rd->getNotePeneliti() != '' and $rd->getNotePeneliti() != NULL ) or ( $rd->getNoteSkpd() != '' and $rd->getNoteSkpd() != NULL ) and $rd->getStatusHapus() == false) 
        {
        ?>
        <td style="background: #e8f3f1">
            <?php
            if ($benar_musrenbang == 1) {
                echo '&nbsp;<span class="badge badge-success">Musrenbang</span>';
            }
            if ($benar_output == 1) {
                echo '&nbsp;<span class="badge badge-info">Output</span>';
            }
            if ($benar_prioritas == 1) {
                echo '&nbsp;<span class="badge badge-warning">Prioritas</span>';
            }
            if ($benar_hibah == 1) {
                echo '&nbsp;<span class="badge badge-success">Hibah</span>';
            }
            if ($rd->getKecamatan() <> '') {
                echo '&nbsp;<span class="badge badge-warning">Jasmas</span>';
            }
            if ($benar_multiyears == 1) {
                echo '&nbsp;<span class="badge badge-primary">Multiyears Tahun ke ' . $rd->getThKeMultiyears() . '</span>';
            }
            if ($rd->getIsBlud() == 1) {
                echo '&nbsp;<span class="badge badge-info">BLUD</span>';
            }
            echo '<br/>';
            echo $rd->getKomponenName();
            if (sfConfig::get('app_fasilitas_keteranganKomponen') == 'buka') {
                echo ' ' . $rd->getDetailName() . '<br/>';
                if ($rd->getTipe2() == 'KONSTRUKSI' || $rd->getTipe() == 'FISIK' || $est_fisik) {
                    if ($rd->getLokasiKecamatan() <> '' && $rd->getLokasiKelurahan() <> '') {
                        echo '[' . $rd->getLokasiKelurahan() . ' - ' . $rd->getLokasiKecamatan() . ']';
                    }
                }
            }
            $query2 = "select tahap from " . sfConfig::get('app_default_schema') . ".komponen "
            . "where komponen_id='" . $rd->getKomponenId() . "'";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query2);
            $t = $stmt->executeQuery();
            while ($t->next()) {
                if ($t->getString('tahap') == DinasMasterKegiatanPeer::getTahapKegiatan($rd->getUnitId(), $rd->getKegiatanCode())) {
                    echo image_tag('/images/newanima.gif');
                }
            }
            if ($rd->getStatusLelang() == 'lock' || $rd->getStatusLelang() == 'unlock') {
                echo '<br/><span class="badge badge-danger">[Telah dilakukan Penggunaan Sisa Lelang, tidak dapat mengedit volume]</span>';
            }
            ?>
            <br>
            <?php
                $query =
                "SELECT COUNT(*) AS jumlah
                FROM " . sfConfig::get('app_default_schema') . ".buka_komponen_murni
                WHERE detail_kegiatan = '" . $rd->getDetailKegiatan() . "'";
                $con = Propel::getConnection();
                $stmt = $con->prepareStatement($query);
                $rs = $stmt->executeQuery();
                if($rs->next()) $jml = $rs->getString('jumlah');

                $query =
                "SELECT COUNT(*) AS jumlah
                FROM " . sfConfig::get('app_default_schema') . ".buka_komponen_murni_mondalev
                WHERE kegiatan_code = '" . $rd->getKegiatanCode() . "'";
                $con = Propel::getConnection();
                $stmt = $con->prepareStatement($query);
                $rs = $stmt->executeQuery();
                if($rs->next()) $jml_mondalev = $rs->getString('jumlah');

                if($jml <= 0 && $jml_mondalev > 0 && ($rd->getIsOutput() || $rd->getIsMusrenbang() || $rd->getPrioritasWali()))
                    echo link_to_function('<i class="fa fa-unlock"></i> Buka Komponen', 'execBukaKomp("' . $rd->getDetailNo() . '","' . $unit_id . '","' . $rd->getKegiatanCode() . '", "' . str_replace('.', '_', $kegiatan_code) . '_' . $rd->getDetailNo() . '")', array('class' => 'btn btn-outline-primary btn-sm', 'disable' => true));
            ?>
        </td>
        <td style="background: #e8f3f1" align="center"><?php echo $rd->getSatuan(); ?></td>
        <?php if( $rd->getAccres() > 1) 
        {
            $keterangan_koefisien= '('.$rd->getKeteranganKoefisien().') + '.$rd->getAccres().'%';
        }
        else
        {
            $keterangan_koefisien=$rd->getKeteranganKoefisien();
        }
        ?>
        <td style="background: #e8f3f1" align="center"><?php echo $keterangan_koefisien; ?></td>
        <td style="background: #e8f3f1" align="right">
            <?php
            if ($rd->getSatuan() == '%') {
                echo $rd->getKomponenHargaAwal();
            } elseif ($rd->getKomponenHargaAwal() != floor($rd->getKomponenHargaAwal())) {
                echo number_format($rd->getKomponenHargaAwal(), 2, ',', '.');
            } else {
                echo number_format($rd->getKomponenHargaAwal(), 0, ',', '.');
            }
            ?>
        </td>
        <td style="background: #e8f3f1" align="right">
            <?php
            $volume = $rd->getVolume();
            $harga = $rd->getKomponenHargaAwal();
            $hasil = $volume * $harga;
            echo number_format($hasil, 0, ',', '.');
            ?>
        </td>
        <td style="background: #e8f3f1" align="right">
            <?php echo $rd->getPajak() . '%'; ?>
        </td>
        <td style="background: #e8f3f1" align="right">
            <?php
            $volume = $rd->getVolume();
            $harga = $rd->getKomponenHargaAwal();
            $pajak = $rd->getPajak();
            $total = $rd->getNilaiAnggaran();
            echo number_format($total, 0, ',', '.');
            ?>
        </td>
        <td style="background: #e8f3f1" align="center">
            <?php
            $rekening = $rd->getRekeningCode();
            $rekening_code = substr($rekening, 0, 6);
            $c = new Criteria();
            $c->add(KelompokBelanjaPeer::BELANJA_CODE, $rekening_code);
            $rs_rekening = KelompokBelanjaPeer::doSelectOne($c);
            if ($rs_rekening) {
                echo $rs_rekening->getBelanjaName();
            }
            ?>
        </td>
        <?php if(sfConfig::get('app_tahap_edit') != 'murni'): ?>
        <td style="background: #e8f3f1">
            <?php
                echo 'Catatan SKPD:<br/>';
                $skpd_note = $rd->getNoteSkpd();
                echo textarea_tag('skpd', $skpd_note, 'readonly=readonly');
                if ($sf_user->hasCredential('bappeko')) {
                    $peneliti_note = $rd->getNoteBappeko();
                    echo '<br/>Catatan Bappeko:<br/>';
                } elseif ($sf_user->getNamaUser() == 'anggaran') {
                    $peneliti_note = $rd->getNoteTapd();
                    echo '<br/>Catatan BPKPD:<br/>';
                }
                echo textarea_tag('peneliti', $peneliti_note, 'readonly=readonly');
            ?>
        </td>
        <?php endif; ?>
    <?php 
    } 
    else 
    {
    ?>
        <td>
            <?php
            if ($benar_musrenbang == 1) {
                echo '&nbsp;<span class="badge badge-success">Musrenbang</span>';
            }
            if ($benar_output == 1) {
                echo '&nbsp;<span class="badge badge-info">Output</span>';
            }
            if ($benar_prioritas == 1) {
                echo '&nbsp;<span class="badge badge-warning">Prioritas</span>';
            }
            if ($benar_hibah == 1) {
                echo '&nbsp;<span class="badge badge-success">Hibah</span>';
            }
            if ($rd->getKecamatan() <> '') {
                echo '&nbsp;<span class="badge badge-warning">Jasmas</span>';
            }
            if ($benar_multiyears == 1) {
                echo '&nbsp;<span class="badge badge-primary">Multiyears Tahun ke ' . $rd->getThKeMultiyears() . '</span>';
            }
            if ($rd->getIsBlud() == 1) {
                echo '&nbsp;<span class="badge badge-info">BLUD</span>';
            }
            echo '<br/>';
            echo $rd->getKomponenName();
            if (sfConfig::get('app_fasilitas_keteranganKomponen') == 'buka') {
                echo ' ' . $rd->getDetailName() . '<br/>';
                if ($rd->getTipe2() == 'KONSTRUKSI' || $rd->getTipe() == 'FISIK' || $est_fisik) {
                    if ($rd->getLokasiKecamatan() <> '' && $rd->getLokasiKelurahan() <> '') {
                        echo '[' . $rd->getLokasiKelurahan() . ' - ' . $rd->getLokasiKecamatan() . ']';
                    }
                }
            }
            $query2 = "select tahap from " . sfConfig::get('app_default_schema') . ".komponen where komponen_id='" . $rd->getKomponenId() . "'";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query2);
            $t = $stmt->executeQuery();
            while ($t->next()) {
                if ($t->getString('tahap') == DinasMasterKegiatanPeer::getTahapKegiatan($rd->getUnitId(), $rd->getKegiatanCode())) {
                    echo image_tag('/images/newanima.gif');
                }
            }
            if ($rd->getStatusLelang() == 'lock' || $rd->getStatusLelang() == 'unlock') {
                echo '<br/><span class="badge badge-danger">[Telah dilakukan Penggunaan Sisa Lelang, tidak dapat mengedit volume]</span>';
            }
            ?>
            <br>
            <?php
                $query =
                "SELECT COUNT(*) AS jumlah
                FROM " . sfConfig::get('app_default_schema') . ".buka_komponen_murni
                WHERE detail_kegiatan = '" . $rd->getDetailKegiatan() . "'";
                $con = Propel::getConnection();
                $stmt = $con->prepareStatement($query);
                $rs = $stmt->executeQuery();
                if($rs->next()) $jml = $rs->getString('jumlah');

                $query =
                "SELECT COUNT(*) AS jumlah
                FROM " . sfConfig::get('app_default_schema') . ".buka_komponen_murni_mondalev
                WHERE kegiatan_code = '" . $rd->getKegiatanCode() . "'";
                $con = Propel::getConnection();
                $stmt = $con->prepareStatement($query);
                $rs = $stmt->executeQuery();
                if($rs->next()) $jml_mondalev = $rs->getString('jumlah');

                if($jml <= 0 && $jml_mondalev > 0 && ($rd->getIsOutput() || $rd->getIsMusrenbang() || $rd->getPrioritasWali()))
                    echo link_to_function('<i class="fa fa-unlock"></i> Buka Komponen', 'execBukaKomp("' . $rd->getDetailNo() . '","' . $unit_id . '","' . $rd->getKegiatanCode() . '", "' . str_replace('.', '_', $kegiatan_code) . '_' . $rd->getDetailNo() . '")', array('class' => 'btn btn-outline-primary btn-sm', 'disable' => true));
            ?>
        </td>
        <td align="center"><?php echo $rd->getSatuan() ?></td>
        <?php if( $rd->getAccres() > 1) 
        {
            $keterangan_koefisien= '('.$rd->getKeteranganKoefisien().') + '.$rd->getAccres().'%';
        }
        else
        {
            $keterangan_koefisien=$rd->getKeteranganKoefisien();
        }
        ?>
        <td align="center"><?php echo $keterangan_koefisien; ?></td>
        <td align="right">
            <?php
            if ($rd->getSatuan() == '%') {
                echo $rd->getKomponenHargaAwal();
            } elseif ($rd->getKomponenHargaAwal() != floor($rd->getKomponenHargaAwal())) {
                echo number_format($rd->getKomponenHargaAwal(), 2, ',', '.');
            } else {
                echo number_format($rd->getKomponenHargaAwal(), 0, ',', '.');
            }
            ?>
        </td>
        <td align="right">
            <?php
            $volume = $rd->getVolume();
            $harga = $rd->getKomponenHargaAwal();
            $hasil = $volume * $harga;
            echo number_format($hasil, 0, ',', '.');
            ?>
        </td>
        <td align="right">
            <?php echo $rd->getPajak() . '%'; ?>
        </td>
        <td align="right">
            <?php
            $volume = $rd->getVolume();
            $harga = $rd->getKomponenHargaAwal();
            $pajak = $rd->getPajak();
            $total = $rd->getNilaiAnggaran();
            echo number_format($total, 0, ',', '.');
            ?>
        </td>
        <td align="center">
            <?php
            $rekening = $rd->getRekeningCode();
            $rekening_code = substr($rekening, 0, 6);
            $c = new Criteria();
            $c->add(KelompokBelanjaPeer::BELANJA_CODE, $rekening_code);
            $rs_rekening = KelompokBelanjaPeer::doSelectOne($c);
            if ($rs_rekening) {
                echo $rs_rekening->getBelanjaName();
            }
            ?>
        </td>
        <?php if(sfConfig::get('app_tahap_edit') != 'murni'): ?>
        <td>
            <?php
            echo 'Catatan SKPD:<br/>';
            $skpd_note = $rd->getNoteSkpd();
            echo textarea_tag('skpd', $skpd_note, 'readonly=readonly');
            if ($sf_user->hasCredential('bappeko')) {
                $peneliti_note = $rd->getNoteBappeko();
                echo '<br/>Catatan Bappeko:<br/>';
            } elseif ($sf_user->getNamaUser() == 'anggaran') {
                $peneliti_note = $rd->getNoteTapd();
                echo '<br/>Catatan BPKPD:<br/>';
            }
            echo textarea_tag('peneliti', $peneliti_note, 'readonly=readonly');
            ?>
        </td>
        <?php endif; ?>
<?php } ?>
</tr>
<?php
endforeach;
?>
<script>
    function execBukaKomp(detailno, unitid, kodekegiatan, id) {
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/anggaran/buatCatatanKomponen/unit/" + unitid + "/kegiatan/" + kodekegiatan + "/id/" + detailno + ".html",
            context: document.body
        }).done(function (msg) {
            $('#note_mondal_' + id).html(msg);
        });
    }

    function execBuatNote(detailno, unitid, kodekegiatan, id) {
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/anggaran/buatCatatan/unitid/" + unitid + "/kodekegiatan/" + kodekegiatan + "/detailno/" + detailno + ".html",
            context: document.body
        }).done(function (msg) {
            $('#tempat_note_' + id).html(msg);
        });
    }
    
    

</script>