<?php
    if (isset($filters['tahap']) && $filters['tahap'] == 'pakbp') {
        $tabel_semula = 'revisi6_';
        $tabel_dpn = 'pak_bukuputih_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'pakbb') {
        $tabel_dpn = 'pak_bukubiru_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'murni') {
        $tabel_semula = 'murni_bukubiru_';
        $tabel_dpn = 'murni_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'murnibp') {
        $tabel_semula = 'murni_bukuputih_';
        $tabel_dpn = 'murni_bukuputih_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'murnibb') {
        $tabel_semula = 'murni_bukuputih_';
        $tabel_dpn = 'murni_bukubiru_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'murnibbpraevagub') {
        $tabel_dpn = 'murni_bukubiru_praevagub_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi1') {
        $tabel_semula = 'murni_';
        $tabel_dpn = 'revisi1_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi1_1') {
        $tabel_dpn = 'revisi1_1_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi2') {
        $tabel_semula = 'revisi1_';
        $tabel_dpn = 'revisi2_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi2_1') {
        $tabel_dpn = 'revisi2_1_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi2_2') {
        $tabel_dpn = 'revisi2_2_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi3') {
        $tabel_semula = 'revisi2_';
        $tabel_dpn = 'revisi3_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi3_1') {
        $tabel_dpn = 'revisi3_1_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi4') {
        $tabel_semula = 'revisi3_';
        $tabel_dpn = 'revisi4_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi5') {
        $tabel_semula = 'revisi4_';
        $tabel_dpn = 'revisi5_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi6') {
        $tabel_semula = 'revisi5_';
        $tabel_dpn = 'revisi6_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi7') {
        $tabel_dpn = 'revisi7_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi8') {
        $tabel_dpn = 'revisi8_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi9') {
        $tabel_dpn = 'revisi9_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi10') {
        $tabel_dpn = 'revisi10_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'rkua') {
        $tabel_dpn = 'rkua_';
    } else {
        $tabel_semula = '';
        $tabel_dpn = 'dinas_';
    }

    use_helper('Url', 'Javascript', 'Form', 'Object');
    $kode = $master_kegiatan->getKodeKegiatan();
    $unit = $master_kegiatan->getUnitId();

    $query = "SELECT * from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "rincian_detail 
    where ((note_peneliti is not Null and note_peneliti <> '') or (note_skpd is not Null and note_skpd <> '')) and unit_id = '$unit' and kegiatan_code = '$kode' and status_hapus = false";
    //diambil nilai terpakai
    $con = Propel::getConnection();
    $statement = $con->prepareStatement($query);
    $rs = $statement->executeQuery();
    $jml = $rs->getRecordCount();

    $query2 = "SELECT * from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "master_kegiatan 
    where ((catatan is not Null and trim(catatan) <> '') or (catatan_pembahasan is not Null and trim(catatan_pembahasan) <> '')) and unit_id = '$unit' and kode_kegiatan = '$kode' and tahap='". $filters['tahap'] ."'";
    //diambil nilai terpakai
    $statement2 = $con->prepareStatement($query2);
    $rs = $statement2->executeQuery();
    $jml2 = $rs->getRecordCount();


    if ($jml > 0 or $jml2 > 0) {
        ?>
        <td style="background: #e8f3f1"><?php echo $master_kegiatan->getKodekegiatan(); ?></td>
        <td style="background: #e8f3f1"><?php
            echo $master_kegiatan->getNamakegiatan();
            echo ' <font color=#FF0000>(' . $master_kegiatan->getUserId() . ')</font><br/>';
            echo 'Kode: [ '.$master_kegiatan->getKegiatanId().' ]';
            ?></td>
        <?php if (sfConfig::get('app_tahap_edit') == 'murni') { ?> 
            <td align="right" style="background: #e8f3f1">
                <?php echo number_format($master_kegiatan->getAlokasidana(), 0, ',', '.'); ?>
            </td>
        <?php } else if (sfConfig::get('app_tahap_edit') != 'murni') { ?>                
                <td align="right" style="background: #e8f3f1"><?php echo get_partial('nilaisemula', array('type' => 'list', 'master_kegiatan' => $master_kegiatan)) ?></td>
                <?php if (sfConfig::get('app_tahap_edit') != 'pak') { ?>
                <td align="right" style="background: #e8f3f1"><?php echo get_partial('nilaimenjadi', array('type' => 'list', 'master_kegiatan' => $master_kegiatan)) ?></td>
                 <?php } ?>                
        <?php } ?>
        <?php if (sfConfig::get('app_tahap_edit') == 'pak') { ?>
            <td style="background: #e8f3f1">
                <?php
                $c = new Criteria();
                $c->add(PaguPakPeer::UNIT_ID, $master_kegiatan->getUnitId());
                $c->add(PaguPakPeer::KEGIATAN_CODE, $master_kegiatan->getKodeKegiatan());
                $pagu_pak = PaguPakPeer::doSelectOne($c);
                echo number_format($pagu_pak->getPagu(), 0, ',', '.');
                ?>
            </td>
        <?php } ?>

        <td align="right" style="background: #e8f3f1"><?php echo get_partial('nilairincian_revisi', array('type' => 'list', 'master_kegiatan' => $master_kegiatan)) ?></td>
        <td align="right" style="background: #e8f3f1"><?php echo get_partial('selisih_revisi', array('type' => 'list', 'master_kegiatan' => $master_kegiatan)) ?></td>
        <?php if (sfConfig::get('app_tahap_edit') == 'pak' ||  sfConfig::get('app_tahap_edit') == 'murni') { ?>
       <td align="center" style="background: #e8f3f1;">
            <div id="<?php echo 'tempat_ajax2_' . str_replace('.', '_', $master_kegiatan->getKodeKegiatan()) ?>" align="right">
                <?php
                echo number_format($master_kegiatan->getTambahanPagu(), 0, ',', '.') . '<br>';
                ?>
            </div>
        </td>
         <?php } ?>

        <td align="center" style="background: #e8f3f1"><?php echo get_partial('posisi_revisi', array('type' => 'list', 'master_kegiatan' => $master_kegiatan)) ?></td>

    <?php } else {
        ?>
        <td><?php echo $master_kegiatan->getKodekegiatan() ?></td>
        <td><?php 
            echo $master_kegiatan->getNamakegiatan();
            echo ' <font color=#FF0000>(' . $master_kegiatan->getUserId() . ')</font><br/>';
            echo 'Kode: [ '.$master_kegiatan->getKegiatanId().' ]';
            ?></td>

        <?php if (sfConfig::get('app_tahap_edit') == 'murni') { ?>
            <td align="right" >
                <?php echo number_format($master_kegiatan->getAlokasidana(), 0, ',', '.'); ?>
            </td>
        <?php } else if (sfConfig::get('app_tahap_edit') != 'murni') { ?>                
           
            <td align="right"><?php echo get_partial('nilaisemula', array('type' => 'list', 'master_kegiatan' => $master_kegiatan, 'tahap' => $filters)) ?></td>
             <?php if (sfConfig::get('app_tahap_edit') != 'pak') { ?>
             <td align="right"><?php echo get_partial('nilaimenjadi', array('type' => 'list', 'master_kegiatan' => $master_kegiatan, 'tahap' => $filters)) ?></td>
              <?php } ?>
        <?php } ?>
        <?php if (sfConfig::get('app_tahap_edit') == 'pak') { ?>
            <td align="right">
                <?php
                $c = new Criteria();
                $c->add(PaguPakPeer::UNIT_ID, $master_kegiatan->getUnitId());
                $c->add(PaguPakPeer::KEGIATAN_CODE, $master_kegiatan->getKodeKegiatan());
                $pagu_pak = PaguPakPeer::doSelectOne($c);
                echo number_format($pagu_pak->getPagu(), 0, ',', '.');
    //            echo number_format($master_kegiatan->getAlokasidana(), 0, ',', '.');
                ?>
            </td>
        <?php } ?>
        
        <td align="right"><?php echo get_partial('nilairincian_revisi', array('type' => 'list', 'master_kegiatan' => $master_kegiatan, 'tahap' => $filters)) ?></td>
        <td align="right"><?php echo get_partial('selisih_revisi', array('type' => 'list', 'master_kegiatan' => $master_kegiatan, 'tahap' => $filters)) ?></td>
        <?php if (sfConfig::get('app_tahap_edit') == 'pak' ||  sfConfig::get('app_tahap_edit') == 'murni') { ?>
        <td align="right"><?php echo get_partial('tambahan_pagu', array('type' => 'list', 'master_kegiatan' => $master_kegiatan, 'tahap' => $filters)) ?></td>
         <?php } ?>
        <!-- <td  align="right">
            <div id="<?php echo 'tempat_ajax2_' . str_replace('.', '_', $master_kegiatan->getKodeKegiatan()) ?>" align="right">
                <?php
                echo number_format($master_kegiatan->getTambahanPagu(), 0, ',', '.') . '<br>';
                ?>
            </div>
        </td> -->
        <td align="center"><?php echo get_partial('posisi_revisi', array('type' => 'list', 'master_kegiatan' => $master_kegiatan)) ?></td>
<?php } ?>