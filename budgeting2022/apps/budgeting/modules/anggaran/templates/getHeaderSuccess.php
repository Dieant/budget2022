<?php
use_helper('Url', 'Javascript', 'Form', 'Object', 'Validation');
if ($tahap == 'pakbp') {
    $tabel_dpn = 'pak_bukuputih_';
} elseif ($tahap == 'pakbb') {
    $tabel_dpn = 'pak_bukubiru_';
} elseif ($tahap == 'murni') {
    $tabel_dpn = 'murni_';
} elseif ($tahap == 'murnibp') {
    $tabel_dpn = 'murni_bukuputih_';
} elseif ($tahap == 'murnibb') {
    $tabel_dpn = 'murni_bukubiru_';
} elseif ($tahap == 'revisi1') {
    $tabel_dpn = 'revisi1_';
} elseif ($tahap == 'revisi1_1') {
    $tabel_dpn = 'revisi1_1_';
} elseif ($tahap == 'revisi2') {
    $tabel_dpn = 'revisi2_';
} elseif ($tahap == 'revisi2_1') {
    $tabel_dpn = 'revisi2_1_';
} elseif ($tahap == 'revisi2_2') {
    $tabel_dpn = 'revisi2_2_';
} elseif ($tahap == 'revisi3') {
    $tabel_dpn = 'revisi3_';
} elseif ($tahap == 'revisi3_1') {
    $tabel_dpn = 'revisi3_1_';
} elseif ($tahap == 'revisi4') {
    $tabel_dpn = 'revisi4_';
} elseif ($tahap == 'revisi5') {
    $tabel_dpn = 'revisi5_';
} elseif ($tahap == 'revisi6') {
    $tabel_dpn = 'revisi6_';
} elseif ($tahap == 'revisi7') {
    $tabel_dpn = 'revisi7_';
} elseif ($tahap == 'revisi8') {
    $tabel_dpn = 'revisi8_';
} elseif ($tahap == 'revisi9') {
    $tabel_dpn = 'revisi9_';
} elseif ($tahap == 'revisi10') {
    $tabel_dpn = 'revisi10_';
} elseif ($tahap == 'rkua') {
    $tabel_dpn = 'rkua_';
} else {
    $tabel_dpn = 'dinas_';
}
?>
<div class="row" id="depan_<?php echo $sf_params->get('id') ?>">
  <div class="col">
    <div class="card">
      <div class="card-body">
        <?php
          $kode_kegiatan = $sf_params->get('kegiatan');
          $unit_id = $sf_params->get('unit');

          $kode_program22 = substr($master_kegiatan->getKodeProgram2(), 9, 2);
          if (substr($master_kegiatan->getKodeProgram2(), 0, 8) == 'X.XX') {
              $kode_urusan = substr($master_kegiatan->getKodeUrusan(), 0, 8);
          } else {
              $kode_urusan = substr($master_kegiatan->getKodeProgram2(), 0, 8);
          }
          $e = new Criteria();
          $e->add(UnitKerjaPeer::UNIT_ID, $master_kegiatan->getUnitId());
          $es = UnitKerjaPeer::doSelectOne($e);
          if ($es) {
              $kode_permen = $es->getKodePermen();
          }
          $kode = $kode_urusan . '.' . $kode_permen . '.' . $kode_program22 . '.' . $master_kegiatan->getKodeKegiatan();
          $temp_kode = explode('.', $kode);
          if (count($temp_kode) >= 5) {
              $kode_kegiatan_pakai = $temp_kode[0] . '.' . $temp_kode[1] . '.' . $temp_kode[3] . '.' . $temp_kode[4];
          } else {
              $kode_kegiatan_pakai = $kode;
          }
          $query = "select mk.kode_kegiatan, mk.kode_head_kegiatan, mh.nama_head_kegiatan, mk.kegiatan_id, mk.nama_kegiatan, mk.alokasi_dana, mk.kode_program2, uk.unit_name, uk.unit_id, mk.kode_urusan, mk.kode_bidang "
          . "from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "master_kegiatan mk, " . sfConfig::get('app_default_schema') . ".master_head_kegiatan mh, unit_kerja uk "
          . "where mk.kode_kegiatan = '" . $kode_kegiatan . "' and mk.unit_id = '" . $unit_id . "' and mh.unit_id = '" . $unit_id . "' "
          . "and mk.kode_head_kegiatan = mh.kode_head_kegiatan and uk.unit_id=mk.unit_id";
          $con = Propel::getConnection();
          $stmt = $con->prepareStatement($query);
          $rs = $stmt->executeQuery();
          $ranking = '';
          while ($rs->next()) 
          {
              $kode_head_kegiatan = $rs->getString('kode_head_kegiatan');
              $nama_head_kegiatan = $rs->getString('nama_head_kegiatan');
              $kegiatan_kode = $rs->getString('kegiatan_id');
              $nama_kegiatan = $rs->getString('nama_kegiatan');
              $alokasi = $rs->getString('alokasi_dana');
              $kode_unit = $rs->getString('unit_id');
              $organisasi = $rs->getString('unit_name');
              $program_p_13 = $rs->getString('kode_program2');
              $kode_urusan = $rs->getString('kode_urusan');
              $kode_bidang = $rs->getString('kode_bidang');
          }

          $query = "select distinct(rd.subtitle) "
          . "from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "rincian_detail rd "
          . "where rd.kegiatan_code = '" . $kode_kegiatan . "' and rd.unit_id = '" . $unit_id . "'";
          $con = Propel::getConnection();
          $stmt = $con->prepareStatement($query);
          $rs = $stmt->executeQuery();

          $kode_kegiatan_baru = substr($master_kegiatan->getKodeKegiatan(), 12, 4);
          $kode_tulis = $kode_urusan . '.' . $kode_permen . '.' . $kode_program22 . '.' . $kode_kegiatan_baru;
        ?>
            <div class="row" style="margin-bottom: 5px">
                <div class="col-md-3 text-right text-bold">Organisasi</div>
                <div class="col-md-9"><?php echo $kode_unit . ' ' . $organisasi ?></div>
                <div class="clearfix"></div>
            </div>
            <div class="row" style="margin-bottom: 5px">
                <div class="col-md-3 text-right text-bold">Urusan</div>
                <div class="col-md-9">
                    <?php
                    $urusan = '';
                    $u = new Criteria();
                    $u->add(MasterUrusanPeer::KODE_URUSAN, $kode_urusan);
                    $us = MasterUrusanPeer::doSelectOne($u);
                    if ($us) {
                        $nama_urusan = $us->getNamaUrusan();
                    }
                    $urusan = $kode_urusan . ' ' . $nama_urusan;
                    echo $urusan;
                    ?>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="row" style="margin-bottom: 5px">
                <div class="col-md-3 text-right text-bold">Bidang Urusan</div>
                <div class="col-md-9">
                    <?php
                    $bidang = '';
                    $u = new Criteria();
                    $u->add(MasterBidangPeer::KODE_BIDANG, $kode_bidang);
                    $us = MasterBidangPeer::doSelectOne($u);
                    if ($us) {
                        $nama_bidang = $us->getNamaBidang();
                    }
                    $bidang = $kode_bidang . ' ' . $nama_bidang;
                    echo $bidang;
                    ?>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="row" style="margin-bottom: 5px">
                <div class="col-md-3 text-right text-bold">Program RPJM / Program 13</div>
                <div class="col-md-9">
                    <?php
                    $program13 = '';
                    $query = "select * from " . sfConfig::get('app_default_schema') . ".master_program2 kp where kp.kode_program='" . $kode_program . "' and kp.kode_program2='" . $program_p_13 . "'"; 
                    $query = "select *
                    from " . sfConfig::get('app_default_schema') . ".master_program2 kp
                    where kp.kode_program2='" . $program_p_13 . "'";

                    $con = Propel::getConnection();
                    $stmt = $con->prepareStatement($query);
                    $rs1 = $stmt->executeQuery();
                    while ($rs1->next()) {
                        $program13 = $rs1->getString('kode_program2') . ' ' . $rs1->getString('nama_program2');
                    }
                    echo $program13;
                    ?>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="row" style="margin-bottom: 5px">
                <div class="col-md-3 text-right text-bold">Kegiatan</div>
                <div class="col-md-9"><?php echo $kode_head_kegiatan . ' ' . $nama_head_kegiatan ?></div>
                <div class="clearfix"></div>
            </div>
            <div class="row" style="margin-bottom: 5px">
                <div class="col-md-3 text-right text-bold">Sub Kegiatan</div>
                <div class="col-md-9"><?php echo $kegiatan_kode. ' '.$nama_kegiatan ?></div>
                <div class="clearfix"></div>
            </div>
            <?php
                        $misi_name = '';
                        $c = new Criteria();
                        $c->add(VisiPdPeer::UNIT_ID,$unit_id, Criteria::EQUAL);
                        $rs_visi = VisiPdPeer::doSelectOne($c);
                        if(!is_null($rs_visi)) $visi_pd = $rs_visi->getVisi();
              ?>
            <div class="row" style="margin-bottom: 5px">
                <div class="col-md-3 text-right text-bold">Visi RPJMD 2016-2022</div>
                <div class="col-md-9"><?php echo $visi_pd; ?></div>
                <div class="clearfix"></div>
            </div>            
            <div class="row" style="margin-bottom: 5px">
                <div class="col-md-3 text-right text-bold">Misi RPJMD 2016-2022</div>
                 <?php
                                $di = new Criteria();
                                $di->add(MisiPdPeer::UNIT_ID, $unit_id);
                                $di->add(MisiPdPeer::KODE_KEGIATAN, $kode_kegiatan);
                                $vi = MisiPdPeer::doSelect($di);
                                foreach ($vi as $value):
                                    $misi_pd = $value->getMisi();
                  ?>
                  <div class="col-md-9"><?php echo $misi_pd; ?></div>                               
                  <?php endforeach; ?>                
                <div class="clearfix"></div>
            </div>
            <div class="row" style="margin-bottom: 5px">
                <div class="col-md-3 text-right text-bold">Tujuan Kota</div>
                <div class="col-md-9">
                    <table class="table table-responsive">
                        <thead>
                            <tr class="text-center">
                                <th style="padding:5px">Narasi Tujuan Kota</th>
                                <th style="padding:5px">Indikator Tujuan Kota</th>
                                <th style="padding:5px">Target</th>
                            </tr>
                        </thead>
                        <tbody>
                          <?php 
                                        $c = new Criteria();
                                        $c->add(IndikatorTujuanKotaPeer::UNIT_ID,$unit_id, Criteria::EQUAL);
                                        $c->addAnd(IndikatorTujuanKotaPeer::KODE_KEGIATAN,$kode_kegiatan, Criteria::EQUAL);
                                        $rs_itk = IndikatorTujuanKotaPeer::doSelect($c);
                                        $ind_tujuan_kota = array();
                                        $tar_tujuan_kota = array();
                                        foreach ($rs_itk as $key => $value) {
                                            array_push($ind_tujuan_kota, $value->getIndikatorTujuan());
                                            array_push($tar_tujuan_kota, $value->getNilai());
                                        }
                                        $idx = count($ind_tujuan_kota);
                          ?>
                           <tr class="text-center">
                            <td style="padding:5px">
                            <?php
                                                // get tujuan pd
                                                $di = new Criteria();
                                                $di->add(TujuanKotaPeer::UNIT_ID, $unit_id);
                                                $di->add(TujuanKotaPeer::KODE_KEGIATAN, $kode_kegiatan);
                                                $vi = TujuanKotaPeer::doSelect($di);
                                                foreach ($vi as $value):
                                                        $tujuan_kota = $value->getTujuanKota();
                                                ?>
                                                <?php echo $tujuan_kota; ?><br/>
                            <?php endforeach; ?>
                            </td>
                            <td style="padding:5px"><?php 
                                                for($i=0; $i < $idx; $i++) {                                                
                                                    echo nl2br('-'.$ind_tujuan_kota[$i]."\n");
                                                } 
                                                ?>
                            </td>
                            <td style="padding:5px"><?php 
                                                for($i=0; $i < $idx; $i++) {                                                
                                                    echo nl2br($tar_tujuan_kota[$i]."\n");
                                                }
                                                ?>
                             </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="clearfix"></div>
            </div>

                        <div class="row" style="margin-bottom: 5px">
                <div class="col-md-3 text-right text-bold">Tujuan Perangkat Daerah</div>
                <div class="col-md-9">
                    <table class="table table-responsive">
                        <thead>
                            <tr class="text-center">
                                <th style="padding:5px">Narasi Perangkat Daerah</th>
                                <th style="padding:5px">Indikator Perangkat Daerah</th>
                                <th style="padding:5px">Target</th>
                            </tr>
                        </thead>
                        <tbody>
                          <?php 
                                        $c = new Criteria();
                                        $c->add(IndikatorTujuanPdPeer::UNIT_ID,$unit_id, Criteria::EQUAL);
                                        $c->addAnd(IndikatorTujuanPdPeer::KODE_KEGIATAN,$kode_kegiatan, Criteria::EQUAL);
                                        $rs_itp = IndikatorTujuanPdPeer::doSelect($c);
                                        $ind_tujuan_pd = array();
                                        $tar_tujuan_pd = array();
                                        foreach ($rs_itp as $key => $value) {
                                            array_push($ind_tujuan_pd, $value->getIndikatorTujuan());
                                            array_push($tar_tujuan_pd, $value->getNilai());
                                        }
                                        $idx = count($ind_tujuan_pd);
                          ?>
                           <tr class="text-center">
                            <td style="padding:5px">
                            <?php
                                                // get tujuan pd
                                                $di = new Criteria();
                                                $di->add(TujuanPdPeer::UNIT_ID, $unit_id);
                                                $di->add(TujuanPdPeer::KODE_KEGIATAN, $kode_kegiatan);
                                                $vi = TujuanPdPeer::doSelect($di);
                                                foreach ($vi as $value):
                                                        $tujuan_pd = $value->getTujuanPd();
                                                ?>
                                               <?php echo $tujuan_pd; ?><br/>
                                      <?php endforeach; ?>
                            </td>
                            <td style="padding:5px"><?php 
                                                for($i=0; $i < $idx; $i++) {                                                
                                                    echo nl2br('-'.$ind_tujuan_pd[$i]."\n");
                                                } 
                                                ?>
                            </td>
                            <td style="padding:5px"><?php 
                                                for($i=0; $i < $idx; $i++) {                                                
                                                    echo nl2br($tar_tujuan_pd[$i]."\n");
                                                }
                                                ?>
                             </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="clearfix"></div>
            </div>

            <div class="row" style="margin-bottom: 5px">
                <div class="col-md-3 text-right text-bold">Sasaran Kota</div>
                <div class="col-md-9">
                    <table class="table table-responsive">
                        <thead>
                            <tr class="text-center">
                                <th style="padding:5px">Narasi Sasaran Kota</th>
                                <th style="padding:5px">Indikator Sasaran Kota</th>
                                <th style="padding:5px">Target</th>
                            </tr>
                        </thead>
                        <tbody>
                          <?php 
                                            $c = new Criteria();
                                            $c->add(IndikatorSasaranKotaPeer::UNIT_ID,$unit_id, Criteria::EQUAL);
                                            $c->addAnd(IndikatorSasaranKotaPeer::KODE_KEGIATAN,$kode_kegiatan, Criteria::EQUAL);
                                            $rs_isk = IndikatorSasaranKotaPeer::doSelect($c);
                                            $c_isk = 0;
                                            $ind_sasaran = array();
                                            $tar_sasaran = array();
                                            foreach ($rs_isk as $key => $value) {
                                                $indikator_sasaran = $value->getIndikatorSasaran();
                                                array_push($ind_sasaran, $indikator_sasaran);
                                                $target_sasaran = $value->getNilai();
                                                array_push($tar_sasaran, $target_sasaran);

                                            }
                                            $idx = count($ind_sasaran);
                          ?>
                           <tr class="text-center">
                            <td style="padding:5px">
                            <?php
                                            $di = new Criteria();
                                            $di->add(SasaranKotaPeer::UNIT_ID, $unit_id);
                                            $di->add(SasaranKotaPeer::KODE_KEGIATAN, $kode_kegiatan);
                                            $vi = SasaranKotaPeer::doSelect($di);
                                            foreach ($vi as $value):
                                                    $nama_sasaran = $value->getSasaranKota();
                                            ?>
                                            <?php echo $nama_sasaran ?><br/>
                                            <?php endforeach; ?>
                            </td>
                            <td style="padding:5px"><?php 
                                               for($i=0; $i < $idx; $i++) {                                                
                                                echo nl2br('-'.$ind_sasaran[$i]."\n");
                                            }  
                                                ?>
                            </td>
                            <td style="padding:5px"><?php 
                                                for($i=0; $i < $idx; $i++) {                                                
                                                echo nl2br($tar_sasaran[$i]."\n");
                                                }
                                                ?>
                             </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="clearfix"></div>
            </div>

            <div class="row" style="margin-bottom: 5px">
                <div class="col-md-3 text-right text-bold">Sasaran Perangkat Daerah</div>
                <div class="col-md-9">
                    <table class="table table-responsive">
                        <thead>
                            <tr class="text-center">
                                <th style="padding:5px">Narasi Sasaran Perangkat Daerah</th>
                                <th style="padding:5px">IndiIndikator Sasaran Perangkat Daerah</th>
                                <th style="padding:5px">Target</th>
                            </tr>
                        </thead>
                        <tbody>
                          <?php 
                                        $c = new Criteria();
                                        $c->add(IndikatorSasaranPdPeer::UNIT_ID,$unit_id, Criteria::EQUAL);
                                        $c->addAnd(IndikatorSasaranPdPeer::KODE_KEGIATAN,$kode_kegiatan, Criteria::EQUAL);
                                        $rs_isp = IndikatorSasaranPdPeer::doSelect($c);
                                        $ind_sasaran_pd = array();
                                        $tar_sasaran_pd = array();
                                        foreach ($rs_isp as $key => $value) {
                                            // $indikator_sasaran = $value->getIndikatorSasaran();
                                            array_push($ind_sasaran_pd, $value->getIndikatorSasaran());
                                            // $target_sasaran = $value->getNilai();
                                            array_push($tar_sasaran_pd, $value->getNilai());

                                        }
                                        $idx = count($ind_sasaran_pd);
                            ?>
                           <tr class="text-center">
                            <td style="padding:5px">
                             <?php
                                            $di = new Criteria();
                                            $di->add(SasaranPdPeer::UNIT_ID, $unit_id);
                                            $di->add(SasaranPdPeer::KODE_KEGIATAN, $kode_kegiatan);
                                            $vi = SasaranPdPeer::doSelect($di);
                                            foreach ($vi as $value):
                                                    $nama_sasaran_pd = $value->getSasaranPd();
                                            ?>
                                            <?php echo $nama_sasaran_pd ?><br/>
                                            <?php endforeach; ?>
                            </td>
                            <td style="padding:5px"><?php 
                                               for($i=0; $i < $idx; $i++) {                                                
                                                echo nl2br('-'.$ind_sasaran_pd[$i]."\n");
                                            }
                                                ?>
                            </td>
                            <td style="padding:5px"><?php 
                                               for($i=0; $i < $idx; $i++) {                                                
                                                echo nl2br($tar_sasaran_pd[$i]."\n");
                                               }
                                                ?>
                             </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="clearfix"></div>
            </div>

            <div class="row" style="margin-bottom: 5px">
                <div class="col-md-3 text-right text-bold">Program RPJMD 2016-2022</div>
                <div class="col-md-9">
                    <table class="table table-responsive">
                        <thead>
                            <tr class="text-center">
                                <th style="padding:5px">Narasi Program</th>
                                <th style="padding:5px">Indikator Program</th>
                                <th style="padding:5px">Target</th>
                            </tr>
                        </thead>
                        <tbody>
                          <!-- get program pd -->
                        <?php
                        $query = "select nama_program2 from " . sfConfig::get('app_default_schema') . ".master_program2 where kode_program2 = '$kode_program2'";
                        $stmt = $con->prepareStatement($query);
                        $rs1 = $stmt->executeQuery();
                        while ($rs1->next()) {
                            $program = $rs1->getString('nama_program2');
                        }
                        ?>
                            <?php 
                                        $c = new Criteria();
                                        $c->add(IndikatorProgramKotaPeer::UNIT_ID,$unit_id, Criteria::EQUAL);
                                        $c->addAnd(IndikatorProgramKotaPeer::KODE_KEGIATAN,$kode_kegiatan, Criteria::EQUAL);
                                        $rs_ip = IndikatorProgramKotaPeer::doSelect($c);
                                        $ind_program_pd = array();
                                        $tar_program_pd = array();
                                        foreach ($rs_ip as $key => $value) {
                                            array_push($ind_program_pd, $value->getIndikatorProgram());
                                            array_push($tar_program_pd, $value->getNilai());
                                        }
                                        $idx = count($ind_program_pd);
                            ?>
                           <tr class="text-center">
                            <td style="padding:5px">                             
                                <?php echo $program13 ?><br/>
                                            
                            </td>
                            <td style="padding:5px"><?php 
                                              for($i=0; $i < $idx; $i++) {                                                
                                                    echo nl2br('-'.$ind_program_pd[$i]."\n");
                                                }
                                                ?>
                            </td>
                            <td style="padding:5px"><?php 
                                               for($i=0; $i < $idx; $i++) {                                                
                                                    echo nl2br($tar_program_pd[$i]."\n");
                                                }
                                                ?>
                             </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="clearfix"></div>
            </div>
 
            <div class="row" style="margin-bottom: 5px">
                <div class="col-md-3 text-right text-bold">Output Sub Kegiatan</div>
                <div class="col-md-9">
                    <table class="table table-responsive">
                        <thead>
                            <tr class="text-center">
                                <th style="padding:5px">Tolak Ukur Kinerja</th>
                                <th style="padding:5px">Target</th>
                                <th style="padding:5px">Satuan</th>
                                <th style="padding:5px">Kelompok Sasaran</th>
                                <th style="padding:5px">Lokasi</th>
                                <th style="padding:5px">Lokasi Pelaksana</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $di = new Criteria();
                            $di->add(OutputSubtitlePeer::UNIT_ID, $unit_id);
                            $di->add(OutputSubtitlePeer::KODE_KEGIATAN, $kode_kegiatan);
                            $vi = OutputSubtitlePeer::doSelect($di);
                            foreach ($vi as $value):
                                $kinerja = $value->getOutput();
                                $kinerja_output = explode("|", $kinerja);
                                $kelompok = $value->getKelompokSasaran();
                                $lokasi = $value->getLokasi();
                                $lokasi_pelaksana = $value->getLokasiPelaksana();
                                ?>
                                <tr class="text-center">
                                    <td style="padding:5px">
                                        <?php echo $kinerja_output[0]; ?>
                                    </td>
                                    <td style="padding:5px">
                                        <?php echo $kinerja_output[1]; ?>
                                    </td>
                                    <td style="padding:5px">
                                        <?php echo $kinerja_output[2]; ?>
                                    </td>
                                    <td style="padding:5px">
                                        <?php echo $kelompok; ?>
                                    </td>
                                    <td style="padding:5px">
                                        <?php echo $lokasi; ?>
                                    </td>
                                    <td style="padding:5px">
                                        <?php echo $lokasi_pelaksana; ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>  
                        </tbody>
                    </table>
                </div>
                <div class="clearfix"></div>
            </div>

               <div class="row" style="margin-bottom: 5px">
                <div class="col-md-3 text-right text-bold">Output Kegiatan</div>
                <div class="col-md-9">
                    <table class="table table-responsive">
                        <thead>
                            <tr class="text-center">
                                <th style="padding:5px">Tolak Ukur Kinerja</th>
                                <th style="padding:5px">Target</th>
                                <th style="padding:5px">Satuan</th>                                
                            </tr>
                        </thead>
                        <tbody>
                            <?php                            
                            $di = new Criteria();
                            $di->add(OutputKegiatanPeer::UNIT_ID, $unit_id);
                            $di->add(OutputKegiatanPeer::KODE_HEAD_KEGIATAN, $kode_head_kegiatan);
                            $di->add(OutputKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                            $vi = OutputKegiatanPeer::doSelect($di);
                            foreach ($vi as $value):
                                 $kinerja = $value->getOutput();
                                $kinerja_output = explode("|", $kinerja);                                           
                            ?>
                                <tr class="text-center">
                                    <td style="padding:5px">
                                        <?php echo $kinerja_output[0]; ?>
                                    </td>
                                    <td style="padding:5px">
                                        <?php echo $kinerja_output[1]; ?>
                                    </td>
                                    <td style="padding:5px">
                                        <?php echo $kinerja_output[2]; ?>
                                    </td>                                    
                                </tr>
                            <?php endforeach; ?>  
                        </tbody>
                    </table>
                </div>
                <div class="clearfix"></div>
            </div>

            <div class="row" style="margin-bottom: 5px">
                <div class="col-md-3 text-right text-bold">Pengampu</div>
                <div class="col-md-9">
                    <table class="table table-responsive">
                        <thead>
                            <tr class="text-center">
                                <th style="padding:5px">User</th>
                                <th style="padding:5px">Nama</th>
                                <th style="padding:5px">NIP</th>
                                <th style="padding:5px">Jabatan</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                $di = new Criteria();
                                $di->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                                $di->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                                $vi = DinasMasterKegiatanPeer::doSelect($di);
                                foreach ($vi as $value):
                                    $user_id_pptk = $value->getUserIdPptk();
                                    $user_id_kpa = $value->getUserIdKpa();                           

                                    $c = new Criteria();
                                    $c->add(MasterUserV2Peer::USER_ID, array($user_id_pptk,$user_id_kpa), Criteria::IN);
                                    $ci = MasterUserV2Peer::doSelect($c);
                                    foreach ($ci as $val):
                                        $user_id = $val->getUserId();
                                        $user_name = $val->getUserName();
                                        $nip = $val->getNip();
                                        $jabatan = $val->getJabatan();

                                    ?>
                                    <tr class="text-center">
                                        <td style="padding:5px">
                                            <?php echo $user_id; ?>
                                        </td>
                                        <td style="padding:5px">
                                            <?php echo $user_name; ?>
                                        </td>
                                        <td style="padding:5px">
                                        <?php echo $nip; ?>
                                        </td>
                                        <td style="padding:5px">
                                        <?php echo $jabatan; ?>
                                        </td>
                                    </tr>
                                <?php endforeach; endforeach; ?>  
                        </tbody>
                    </table>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="row" style="margin-bottom: 5px">
                <div class="col-md-3 text-right text-bold">Alokasi</div>
                <div class="col-md-9">
                    <?php
                    echo 'Rp. ' . number_format($alokasi, 0, ',', '.'); 
                    ?>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="row" style="margin-bottom: 5px">
                <div class="col-md-3 text-right text-bold">Total</div>
                <div class="col-md-9">
                    <?php
                    $query = "select sum(nilai_anggaran) as total "
                    . "from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "rincian_detail rd "
                    . "where rd.kegiatan_code = '" . $kode_kegiatan . "' and rd.unit_id = '" . $unit_id . "' "
                    . "and rd.status_hapus = false ";
                    $con = Propel::getConnection();
                    $stmt = $con->prepareStatement($query);
                    $rs = $stmt->executeQuery();
                    while ($rs->next()) {
                        $total = $rs->getFloat('total');
                    }
                    echo 'Rp. ' . number_format($total, 0, ',', '.');
                    ?>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>