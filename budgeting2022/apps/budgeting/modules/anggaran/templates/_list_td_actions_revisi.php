<td class='tombol_actions'>
    <?php 
        echo link_to('<i class="fa fa-edit"></i>', 'anggaran/editRevisi?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan(), array('alt' => __('edit'), 'title' => __('edit'), 'class' => 'btn btn-outline-primary btn-sm'));
        if (!(isset($filters['tahap']) && $filters['tahap'] <> '')) 
        {
                echo link_to('<i class="fa fa-list-ul"></i>',  'report/tampillaporanasli?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan(), array('alt' => __('laporan perbandingan asli-draft'), 'title' => __('laporan perbandingan asli-draft'), 'class' => 'btn btn-outline-info btn-sm'));
                echo link_to('<i class="fa fa-donate"></i>',  'anggaran/penandaBtl?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan(), array('alt' => __('Penanda Sumber Dana'), 'title' => __('Penanda Sumber Dana'), 'class' => 'btn btn-outline-secondary btn-sm')); 
                echo link_to('<i class="fa fa-exclamation-triangle"></i>', 'report/penandaPrioritas?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan(), array('alt' => __('Penanda Prioritas'), 'title' => __('Penanda Prioritas'), 'class' => 'btn btn-outline-warning btn-sm'));
                echo link_to('<i class="fa fa-hand-holding-usd"></i>', 'report/editRasionalisasi?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan(), array('alt' => __('Nilai Rasionalisasi'), 'title' => __('Nilai Rasionalisasi'), 'class' => 'btn btn-outline-info btn-sm'));   

                if(sfConfig::get('app_tahap_edit') == 'pak') {                   
                    echo link_to('<i class="fa fa-th-list"></i>', 'report/tampillaporanBukuputih?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan(), array('alt' => __('laporan perbandingan asli-bukuputih'), 'title' => __('laporan perbandingan pak bukuputih-bukubiru'), 'class' => 'btn btn-outline-secondary btn-sm'));
                }

                if(sfConfig::get('app_tahap_edit') == 'murni') 
                {              
                    echo link_to('<i class="fa fa-th-list"></i>', 'report/tampillaporanBukuputih?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan(), array('alt' => __('laporan perbandingan asli-bukuputih'), 'title' => __('laporan perbandingan murni bukuputih-bukubiru'), 'class' => 'btn btn-outline-secondary btn-sm'));
                    if($sf_user->hasCredential('bappeko')) {
                        // echo ' ';
                        // echo link_to('<i class="fa fa-th-list"></i>', 'report/tampillaporanBukuputih?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan(), array('alt' => __('laporan perbandingan asli-bukuputih'), 'title' => __('laporan perbandingan asli-bukuputih'), 'class' => 'btn btn-outline-primary btn-sm'));
                        // echo '  ';
                        // echo ' ';
                        // echo link_to(image_tag('/sf/sf_admin/images/open.png', array('alt' => __('Buka Komponen Output, Prioritas dan Musrenbang Per Kegiatan'), 'title' => __('Buka Komponen Output, Prioritas dan Musrenbang Per Kegiatan'), 'class' => 'btn btn-outline-primary btn-sm')), 'anggaran/bukaKomponenMurniPerKegiatan?unit_id=' . $master_kegiatan->getUnitId() . '&kegiatan_code=' . $master_kegiatan->getKodeKegiatan());
                        // echo '  ';
                        // echo ' ';
                        // echo link_to(image_tag('/sf/sf_admin/images/hurufo.png', array('alt' => __('Set Komponen Pendukung Output'), 'title' => __('Set Komponen Pendukung Output'), 'class' => 'btn btn-outline-primary btn-sm')), 'report/menuSetOutput?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan() . '&tahap=' . (isset($filters['tahap']) ? $filters['tahap'] : ''));
                        // echo '  ';
                        // echo ' ';
                        // echo link_to(image_tag('/sf/sf_admin/images/hurufp.png', array('alt' => __('Set Prioritas Komponen'), 'title' => __('Set Prioritas Komponen'), 'class' => 'btn btn-outline-primary btn-sm')), 'report/menuSetPrioritas?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan() . '&tahap=' . (isset($filters['tahap']) ? $filters['tahap'] : ''));
                        // echo '  ';
                    }
                    // echo ' ';
                    echo link_to('<i class="fa fa-chalkboard-teacher"></i>', 'report/menuViewDevplan?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan() . '&tahap=' . (isset($filters['tahap']) ? $filters['tahap'] : ''), array('alt' => __('Melihat Komponen Devplan'), 'title' => __('Melihat Komponen Devplan'), 'class' => 'btn btn-outline-primary btn-sm'));
                    // echo '  ';
                }
        } 
        else 
        {
            echo link_to('<i class="fa fa-th-list"></i>', 'report/tampillaporanaslisemua?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan() . '&tahap=' . $filters['tahap'], array('alt' => __('laporan perbandingan asli-draft'), 'title' => __('laporan perbandingan asli-draft'), 'class' => 'btn btn-outline-success btn-sm'));
        }
        $c_rincian = new Criteria();
        $c_rincian->add(DinasRincianPeer::UNIT_ID, $master_kegiatan->getUnitId());
        $c_rincian->add(DinasRincianPeer::KEGIATAN_CODE, $master_kegiatan->getKodeKegiatan());
        $c_rincian->add(DinasRincianPeer::RINCIAN_LEVEL, 2);
        $c_rincian->add(DinasRincianPeer::LOCK, FALSE);
        if(DinasRincianPeer::doSelectOne($c_rincian))
        {
            echo link_to('<i class="fa fa-shield-virus"></i>',  'anggaran/penandaCovid?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan(), array('alt' => __('Penanda Covid 19'), 'title' => __('Penanda Covid 19'), 'class' => 'btn btn-outline-info btn-sm'));            
        }
        if ($sf_user->hasCredential('bappeko') && DinasRincianPeer::doSelectOne($c_rincian))
        {
            $tahap = DinasMasterKegiatanPeer::getTahapKegiatan($master_kegiatan->getUnitId(), $master_kegiatan->getKodeKegiatan());
            $c = new Criteria();
            $c->add(RincianBappekoPeer::UNIT_ID, $master_kegiatan->getUnitId());
            $c->add(RincianBappekoPeer::KODE_KEGIATAN, $master_kegiatan->getKodeKegiatan());
            $c->add(RincianBappekoPeer::TAHAP, $tahap);
            if ($rs_rincian_bappeko = RincianBappekoPeer::doSelectOne($c))
            {
                $jawaban1 = $rs_rincian_bappeko->getJawaban1Dinas();
                $jawaban2 = $rs_rincian_bappeko->getJawaban2Dinas();
                $status_buka = $rs_rincian_bappeko->getStatusBuka();
                if (($jawaban1 || $jawaban2) && is_null($status_buka))
                { 
                    echo link_to(image_tag('/sf/sf_admin/images/unlock.png', array('alt' => __('verifikasi kegiatan'), 'title' => __('verifikasi kegiatan'), 'class' => 'btn btn-outline-primary btn-sm')), 'anggaran/verifikasiKegiatan?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan()); 
                }
            }
        }
    ?>
</td>