<?php use_helper('I18N', 'Date', 'Url', 'Javascript', 'Form', 'Object', 'Number', 'Validation') ?>
<?php ?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>Verifikasi Kegiatan</h1>
</section>

<!-- Main content -->
<section class="content">
    <?php include_partial('anggaran/list_messages'); ?>

    <div class="box box-primary box-solid">
        <div class="box-header with-border">
            <h3 class="box-title">Form Verifikasi Kegiatan</h3>
        </div>
        <div class="box-body">
            <div id="sf_admin_container" class="table-responsive">
                <?php echo form_tag('anggaran/verifikasiKegiatan') ?>
                <table cellspacing="0" class="sf_admin_list">
                    <thead>  
                        <tr>
                            <th style="width: 40%"><b>Nama</b></th>
                            <th style="width: 1%">&nbsp;</th>
                            <th style="width: 59%"><b>Isian</b></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="sf_admin_row_0" align='right'>
                            <td>Apakah anda akan merubah output kegiatan / output subtitle?</td>
                            <td align="center">:</td>
                            <td align="left">
                                <?php echo ($rs_rincian_bappeko->getJawaban1Dinas()) ? 'Ya' : 'Tidak'; ?>
                            </td>
                        </tr>
                        <tr class="sf_admin_row_1" align='right'>
                            <td>Apakah anda akan merubah judul subtitle?</td>
                            <td align="center">:</td>
                            <td align="left">
                                <?php echo ($rs_rincian_bappeko->getJawaban2Dinas()) ? 'Ya' : 'Tidak'; ?>
                            </td>
                        </tr>
                        <tr class="sf_admin_row_0" align='right'>
                            <td>Catatan Perangkat Daerah / Unit Kerja</td>
                            <td align="center">:</td>
                            <td align="left">
                                <?php echo $rs_rincian_bappeko->getCatatanDinas(); ?>
                            </td>
                        </tr>
                        <tr class="sf_admin_row_1" align='right'>
                            <td>Persetujuan</td>
                            <td align="center">:</td>
                            <td align="left">
                                <div class="row">
                                    <div class="col-xs-12"><input type="radio" name="buka" value="setuju"> Setuju</div>
                                    <div class="col-xs-12"><input type="radio" name="buka" value="tolak"> Tidak Setuju, karena 
                                    <br/>
                                    <input class="form form-control" type="text" name="catatan"></div>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr class="sf_admin_row_0" align='left' valign="top">
                            <td>&nbsp; </td>
                            <td>
                                <?php
                                echo input_hidden_tag('unit_id', $sf_params->get('unit_id'));
                                echo input_hidden_tag('kode_kegiatan', $sf_params->get('kode_kegiatan'));
                                ?>
                            </td>
                            <td>
                                <?php
                                echo submit_tag('Simpan', 'name=simpan') . ' ' . button_to('Kembali', '#', array('onClick' => "javascript:history.back()"));
                                ?>
                            </td>
                        </tr>
                    </tfoot>
                </table>
                <?php echo '</form>'; ?>
            </div>
        </div>
    </div>
</section>
