<?php use_helper('I18N', 'Date', 'Object', 'Javascript', 'Validation') ?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <?php
    if (sfConfig::get('app_tahap_edit') == 'murni') {
        $nama_sistem = 'Pra-RKA';
    } elseif (sfConfig::get('app_tahap_edit') == 'pak') {
        $nama_sistem = 'PAK';
    } elseif (sfConfig::get('app_tahap_edit') == 'penyesuaian') {
        $nama_sistem = 'Penyesuaian';
    } else {
        $nama_sistem = 'Revisi';
    }
    ?>
    <h1>Komponen <?php echo $nama_sistem; ?></h1>
</section>
<!-- Main content -->
<section class="content">
    <?php include_partial('anggaran/list_messages') ?>
    <!-- Default box -->
    <div class="box box-solid box-primary">
        <div class="box-header with-border">           
            <h3 class="box-title">Filters</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body">
            <?php echo form_tag('anggaran/krkaRevisi', array('class' => 'form-horizontal')); ?>
            <div class="form-group">
                <?php
                $arrOptions = array(
                    1 => 'Komponen Name',
                    2 => 'Kode Komponen',
                    3 => 'Rekening'
                );
                echo "<div class='col-xs-3'>";
                echo select_tag('filters[select]', options_for_select($arrOptions, @$filters['select'] ? $filters['select'] : '', array('include_blank' => true)), array('class' => 'form-control'));
                echo "</div>";
                echo "<div class='col-xs-9'>";
                echo input_tag('filters[komponen]', isset($filters['komponen']) ? $filters['komponen'] : null, array('class' => 'form-control'));
                echo "</div>";
                ?>
            </div>
            <div id="sf_admin_container">
                <ul class="sf_admin_actions">
                    <li><?php echo button_to(__('reset'), 'anggaran/krkaRevisi?filter=filter', 'class=sf_admin_action_reset_filter') ?></li>
                    <li><?php echo submit_tag(__('cari'), 'name=filter class=sf_admin_action_filter') ?></li>
                </ul>
            </div>
            <?php echo '</form>'; ?>
        </div>
    </div><!-- /.box-body -->

    <!-- Default box -->
    <div class="box box-solid box-primary">
        <div class="box-body">
            <?php if (!$pager->getNbResults()): ?>
                <?php echo __('no result') ?>
            <?php else: ?>
                <?php include_partial("anggaran/krkaRevisi", array('pager' => $pager)) ?>
            <?php endif; ?>
        </div>
    </div>
</section>