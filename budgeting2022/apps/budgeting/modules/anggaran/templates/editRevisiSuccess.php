<?php use_helper('I18N', 'Date', 'Url', 'Javascript', 'Form', 'Object') ?>
<?php
$kode_kegiatan = $sf_params->get('kode_kegiatan');
$unit_id = $sf_params->get('unit_id');
$sub_id = 1;
$cetak = FALSE;
$kegiatan_code = $sf_params->get('kode_kegiatan');

$status = 'OPEN';
$sf_user->removeCredential('status_dinas');
$sf_user->addCredential('status_dinas');
$sf_user->setAttribute('status', $status, 'status_dinas');
$c = new Criteria();
$c->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
$c->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
$rs_kegiatan = DinasMasterKegiatanPeer::doSelectOne($c);
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Ubah Sub Kegiatan - (<?php echo $rs_kegiatan->getKegiatanId(); ?>)</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="#">Lembar Kerja</a></li>
                  <li class="breadcrumb-item active">Ubah Sub-Kegiatan</li>
              </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<!-- Main content -->
<section class="content">
    <?php include_partial('anggaran/list_messages'); ?>
    <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">       
                <div class="card-body text-center" id="headerrka_<?php echo $sub_id ?>">
                    <div class="btn-group btn-group-justified" role="group">
                        <?php
                        echo link_to_function('<span>' . image_tag('down.png', array('name' => 'img_' . $sub_id, 'id' => 'img_' . $sub_id, 'border' => 0)) . '</span> Melihat Header Kegiatan', 'inKeterangan(' . $sub_id . ',"' . $kode_kegiatan . '","' . $unit_id . '")', array('class' => 'btn btn-outline-info btn-sm text-bold col-xs-3'));
                        if ($tahap != '') {
                            if($sf_user->getNamaUser() == 'bappeko'){
                                echo link_to('<i class="fas fa-print"></i> Format RKA Berdasarkan Subtitle', 'report/TemplateRKA?kode_kegiatan=' . $kode_kegiatan . '&unit_id=' . $unit_id . '&tahap=' . $tahap, array('class' => 'btn btn-outline-info btn-sm text-bold col-xs-4', 'target' => '_blank'));
                                echo link_to('<i class="fas fa-print"></i> Format RKA Berdasarkan Rekening', 'report/printrekening?kode_kegiatan=' . $kode_kegiatan . '&unit_id=' . $unit_id. '&tahap=' . $tahap, array('class' => 'btn btn-outline-info btn-sm text-bold col-xs-4', 'target' => '_blank'));
                                echo link_to('<i class="fas fa-print"></i> Format RKA Tanpa Komponen', 'report/printrekeningtanpakomponen?kode_kegiatan=' . $kode_kegiatan . '&unit_id=' . $unit_id. '&tahap=' . $tahap, array('class' => 'btn btn-outline-info btn-sm text-bold col-xs-4', 'target' => '_blank'));
                            }
                        } else {
                            if($sf_user->getNamaUser() == 'bappeko'){
                                echo link_to('<i class="fas fa-print"></i> Format RKA Berdasarkan Subtitle', 'report/TemplateRKApptk?kode_kegiatan=' . $kode_kegiatan . '&unit_id=' . $unit_id, array('class' => 'btn btn-outline-info btn-sm text-bold col-xs-4', 'target' => '_blank'));
                                echo link_to('<i class="fas fa-print"></i> Format RKA Berdasarkan Rekening', 'report/printrekeningpptk?kode_kegiatan=' . $kode_kegiatan . '&unit_id=' . $unit_id, array('class' => 'btn btn-outline-info btn-sm text-bold col-xs-4', 'target' => '_blank'));
                                echo link_to('<i class="fas fa-print"></i> Format RKA Tanpa Komponen', 'report/printrekeningtanpakomponen?kode_kegiatan=' . $kode_kegiatan . '&unit_id=' . $unit_id, array('class' => 'btn btn-outline-info btn-sm text-bold col-xs-4', 'target' => '_blank'));
                            }
                        }
                        ?>
                    </div>
                </div>
                <div class="card-body table-responsive p-0">
                    <?php if (!$rs_subtitle): ?>
                        <?php echo __('Tidak Ada Subtitle untuk SKPD dengan Kegiatan ini') ?>
                    <?php else: ?>
                        <table class="table table-hover">
                            <thead class="head_peach">
                                <tr>
                                    <th>Subtitle | Rekening</th>
                                    <th>Komponen</th>
                                    <th>Satuan</th>
                                    <th>Koefisien</th>
                                    <th>Harga</th>
                                    <th>Hasil</th>
                                    <th>PPN</th>
                                    <th>Total</th>
                                    <th>Belanja</th>
                                    <?php if(sfConfig::get('app_tahap_edit') != 'murni'): ?>
                                        <th>Catatan Tim Anggaran</th>
                                    <?php endif; ?>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 1;
                                foreach ($rs_subtitle as $subtitle_indikator):
                                    ?>
                                    <?php
                                    $unit_id = $subtitle_indikator->getUnitId();
                                    $kode_kegiatan = $subtitle_indikator->getKegiatanCode();
                                    $subtitle = $subtitle_indikator->getSubtitle();
                                    $sub_id = $subtitle_indikator->getSubId();
                                    $nama_subtitle = trim($subtitle);
                                    $odd = fmod($i++, 2);
                                                    //* untuk memberi warning jika komponen ada catatan penyelia dan SKPD
                                    $query = "SELECT * from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail 
                                    where ((note_peneliti is not Null and note_peneliti <> '') or (note_skpd is not Null and note_skpd <> '')) and unit_id = '$unit_id' and kegiatan_code = '$kode_kegiatan' and subtitle ilike '%$nama_subtitle%' and status_hapus = false ";
                                                    //diambil nilai terpakai
                                    $con = Propel::getConnection();
                                    $statement = $con->prepareStatement($query);
                                    $rs = $statement->executeQuery();
                                    $jml = $rs->getRecordCount();
                                    if ($jml > 0) 
                                    {
                                        ?>
                                        <tr id="subtitle_<?php print $sub_id ?>">
                                            <td style="background: #e8f3f1" colspan="7">
                                                <b><i>
                                                    <?php echo link_to_function(image_tag('b_plus.png', array('name' => 'img_' . $sub_id, 'id' => 'img_' . $sub_id, 'border' => 0)), 'showHideKegiatan(' . $sub_id . ')') . ' ' . $subtitle_indikator->getSubtitle(); ?>
                                                </i></b>
                                            </td>
                                            <td style="background: #e8f3f1" align="right">
                                                <?php
                                                    $query2 = "select sum(nilai_anggaran) as hasil_kali
                                                    from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail
                                                    where kegiatan_code ='$kode_kegiatan' and unit_id='$unit_id' and subtitle ilike '$nama_subtitle' and status_hapus=FALSE";
                                                    $con = Propel::getConnection();
                                                    $stmt = $con->prepareStatement($query2);
                                                    $t = $stmt->executeQuery();
                                                    while ($t->next()) {
                                                        echo number_format($t->getString('hasil_kali'), 0, ',', '.');
                                                    }
                                                ?>
                                            </td>
                                            <td style="background: #e8f3f1" colspan="2">&nbsp;</td>
                                        </tr>
                                    <?php 
                                    } else {
                                    ?>
                                        <tr id="subtitle_<?php print $sub_id ?>">
                                            <td colspan="7">
                                                <b><i>
                                                    <?php echo link_to_function(image_tag('b_plus.png', array('name' => 'img_' . $sub_id, 'id' => 'img_' . $sub_id, 'border' => 0)), 'showHideKegiatan(' . $sub_id . ')') . ' ' . $subtitle_indikator->getSubtitle(); ?>
                                                </i></b>
                                            </td>
                                            <td align="right">
                                                <?php
                                                $query2 = "select sum(nilai_anggaran) as hasil_kali
                                                from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail
                                                where kegiatan_code ='$kode_kegiatan' and unit_id='$unit_id' and subtitle ilike '$nama_subtitle' and status_hapus=FALSE";
                                                $con = Propel::getConnection();
                                                $stmt = $con->prepareStatement($query2);
                                                $t = $stmt->executeQuery();
                                                while ($t->next()) {
                                                    echo number_format($t->getString('hasil_kali'), 0, ',', '.');
                                                }
                                                ?>
                                            </td>
                                            <td colspan="3">&nbsp;</td>
                                        </tr> 
                                    <?php 
                                    } 
                                    ?>
                                        <tr id="indicator_<?php echo $sub_id ?>" style="display:none;" align="center">
                                            <td colspan="11">
                                                <dt>&nbsp;</dt><dd><b>Mohon Tunggu </b><?php echo image_tag('loading.gif', array('align' => 'absmiddle')) ?></dd>
                                            </td>
                                        </tr>
                                <?php endforeach; ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="7" align="right">Total Keseluruhan:</td>
                                    <td align="right">
                                        <?php
                                        $unit_id = $subtitle_indikator->getUnitId();
                                        $kode_kegiatan = $subtitle_indikator->getKegiatanCode();
                                        $subtitle = $subtitle_indikator->getSubtitle();
                                        $nama_subtitle = trim($subtitle);
                                        $query2 = "select sum(nilai_anggaran) as hasil_kali
                                        from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail
                                        where kegiatan_code ='$kode_kegiatan' and unit_id='$unit_id' and status_hapus=false";
                                        $con = Propel::getConnection();
                                        $stmt = $con->prepareStatement($query2);
                                        $t = $stmt->executeQuery();
                                        while ($t->next()) {
                                            echo "<b>".number_format($t->getString('hasil_kali'), 0, ',', '.')."</b>";
                                        }
                                        ?>
                                    </td>
                                    <td colspan="2">&nbsp;</td>
                                </tr>
                            </tfoot>
                        </table>
                    <?php endif; ?>
                </div>
            </div>
          </div>
        </div>
    </div>
</section><!-- /.content -->

<script>
    image1 = new Image();
    image1.src = '/<?php echo sfConfig::get('app_default_coding'); ?>/images/b_plus.png';

    image2 = new Image();
    image2.src = '/<?php echo sfConfig::get('app_default_coding'); ?>/images/b_minus.png';

    function showHideKegiatan(id) {
        var row = $('#subtitle_' + id);
        var img = $('#img_' + id);

        if (img) {
            var src = document.getElementById('img_' + id).getAttribute('src');
            var minus = src.indexOf('/<?php echo sfConfig::get('app_default_coding'); ?>/images/b_minus.png');
            if (minus !== -1) {
                src = '/<?php echo sfConfig::get('app_default_coding'); ?>/images/b_plus.png';
            } else {
                src = '/<?php echo sfConfig::get('app_default_coding'); ?>/images/b_minus.png';
            }
            img.attr('src', src);
        }

        if (minus === -1) {
            var kegiatan_id = 'subtitle_' + id;
            var pekerjaans = $('.pekerjaans_' + id);
            var n = pekerjaans.length;

            if (n > 0) {
                for (var i = 0; i < pekerjaans.length; i++) {
                    var pekerjaan = pekerjaans[i];
                    pekerjaan.style.display = 'table-row';
                }
            } else {
                $('#indicator_' + id).show();
                $.ajax({
                    url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/anggaran/getPekerjaansRevisi/id/" + id + ".html",
                    context: document.body
                }).done(function (msg) {
                    $('#indicator_' + id).remove();
                    $('#subtitle_' + id).after(msg);
                });
            }
        } else {
            $('.pekerjaans_' + id).remove();
            minus = -1;
        }
    }

    function showHideKegiatanMasalah(id, kegiatan, unit) {
        var row = $('#subtitle_' + id);
        var img = $('#img_' + id);

        if (img) {
            var src = document.getElementById('img_' + id).getAttribute('src');
            var minus = src.indexOf('/<?php echo sfConfig::get('app_default_coding'); ?>/images/b_minus.png');
            if (minus !== -1) {
                src = '/<?php echo sfConfig::get('app_default_coding'); ?>/images/b_plus.png';
            } else {
                src = '/<?php echo sfConfig::get('app_default_coding'); ?>/images/b_minus.png';
            }
            img.attr('src', src);
        }


        if (minus === -1) {
            var kegiatan_id = 'subtitle_' + id;
            var pekerjaans = $('.pekerjaans_' + id);
            var n = pekerjaans.length;

            if (n > 0) {
                for (var i = 0; i < pekerjaans.length; i++) {
                    var pekerjaan = pekerjaans[i];
                    pekerjaan.style.display = 'table-row';
                }
            } else {
                $('#indicator_' + id).show();
                $.ajax({
                    url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/anggaran/getPekerjaansMasalah/id/" + id + ".html",
                    context: document.body
                }).done(function (msg) {
                    $('#indicator_' + id).remove();
                    $('#subtitle_0').after(msg);
                });
            }
        } else {
            $('.pekerjaans_' + id).remove();
            minus = -1;
        }
    }

    function hapusHeaderKegiatan(id, kegiatan, unit, kodesub) {
        var a = confirm('Apakah anda yakin akan menghapus Header Sub Subtitle kegiatan ini??');
        if (a === true)
        {
            $('.pekerjaans_' + id).remove();
            $.ajax({
                url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/anggaran/hapusHeaderPekerjaans/id/" + id + "/kegiatan/" + kegiatan + "/unit/" + unit + "/no/" + kodesub + ".html"
            }).done();

        }
    }

    function editHeaderKegiatan(id, kegiatan, unit, kodesub) {
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/anggaran/ajaxEditHeader/act/editHeader/kodeSub/" + kodesub + "/unitId/" + unit + "/kegiatanCode/" + kegiatan + ".html",
            context: document.body
        }).done(function (msg) {
            $('#header_' + kodesub).before(msg);
            $('#header_' + kodesub).remove();
        });
    }

    function hapusKegiatan(id, kegiatan, unit, no) {
        var a = confirm('Apakah anda yakin akan menghapus kegiatan ini??');
        if (a === true) {
            $('.pekerjaans_' + id).remove();
            $.ajax({
                url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/anggaran/hapusPekerjaans/id/" + id + "/kegiatan/" + kegiatan + "/unit/" + unit + "/no/" + no + ".html"
            }).done();
        }
    }

    function editSubtitle(id) {
        var loading = $('#indicator_' + id);
        loading.show();
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/anggaran/getPekerjaans/id/" + id + ".html",
            context: document.body
        }).done(function (msg) {
            loading.remove();
        });

    }

    function inKeterangan(id, kegiatan, unit) {
        var row = $('headerrka_' + id);
        var img = $('#img_' + id);
        var pekerjaans = $('#depan_' + id);
        if (img) {
            var src = document.getElementById('img_' + id).getAttribute('src');
            var minus = src.indexOf('/<?php echo sfConfig::get('app_default_coding'); ?>/images/up.png');
            if (minus !== -1) {
                src = '/<?php echo sfConfig::get('app_default_coding'); ?>/images/down.png';
            } else {
                src = '/<?php echo sfConfig::get('app_default_coding'); ?>/images/up.png';
            }
            img.attr('src', src);
        }


        if (minus === -1) {
            var kegiatan_id = 'headerrka_' + id;
            var pekerjaans = $('#depan_' + id);
            var n = pekerjaans.length;

            if (n > 0) {
                for (var i = 0; i < pekerjaans.length; i++) {
                    var pekerjaan = pekerjaans[i];
                    pekerjaan.style.display = 'table-row';
                }
            } else {
                $('#indicator').show();
                $.ajax({
                    url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/anggaran/getHeader/id/" + id + "/kegiatan/" + kegiatan + "/unit/" + unit + ".html",
                    context: document.body
                }).done(function (msg) {
                    $('#indicator').remove();
                    $("#headerrka_" + id).after(msg);
                });
            }
        } else {
            $('#depan_' + id).remove();
        }
    }

    function hapusSubKegiatan(id, kegiatan, unit, kodesub) {
        var a = confirm('Apakah anda yakin akan menghapus SUB kegiatan ini??');
        if (a === true) {
            var kegiatan_id = 'subtitle_' + id;
            var pekerjaans = $('.pekerjaans_' + id);
            for (var i = 0; i < pekerjaans.length; i++) {
                var pekerjaan = pekerjaans[i];
                pekerjaan.style.display = 'none';
            }
            $.ajax({
                url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/anggaran/hapusSubPekerjaans/id/" + id + "/kegiatan/" + kegiatan + "/unit/" + unit + "/no/" + kodesub + ".html",
                context: document.body
            }).done();
        }
    }
</script>