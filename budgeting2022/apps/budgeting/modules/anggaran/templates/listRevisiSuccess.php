<?php use_helper('I18N', 'Date', 'Object', 'Javascript', 'Validation') ?>
<?php
    if (sfConfig::get('app_tahap_edit') == 'murni') {
        $nama_sistem = 'Belanja Daerah';
    } elseif (sfConfig::get('app_tahap_edit') == 'pak') {
        $nama_sistem = 'PAK';
    } elseif (sfConfig::get('app_tahap_edit') == 'penyesuaian') {
        $nama_sistem = 'Penyesuaian';
    }else {
        $nama_sistem = 'Revisi';
    }
?>
<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Sub Kegiatan - (<?php echo $nama_sistem ?>)</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Lembar Kerja</a></li>
          <li class="breadcrumb-item active"><?php echo $nama_sistem ?></li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>
<!-- Main content -->
<section class="content">
    <?php include_partial('anggaran/list_messages'); ?>
    <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">
                        <i class="fas fa-filter"></i> Filter
                    </h3>
                </div>
                <div class="card-body">
                    <?php echo form_tag('anggaran/listRevisi?menu='.$menu, array('method' => 'get', 'class' => 'form-horizontal','filters' => $filters)) ?>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                 <?php $nama = $sf_user->getNamaLogin(); ?>
                                <label>Perangkat Daerah</label>
                                <?php
                                    if (strpos($nama, 'b') === FALSE) {
                                         $e = new Criteria();
                                        $e->addAscendingOrderByColumn(UnitKerjaPeer::UNIT_NAME);
                                    }

                                    else
                                    {                                
                                    $e = new Criteria();                                   
                                    $b = new Criteria;
                                    $b->add(UserHandleV2Peer::USER_ID, $nama);
                                    $es = UserHandleV2Peer::doSelect($b);
                                    $satuan_kerja = $es;
                                    $unit_kerja = Array();
                                    foreach ($satuan_kerja as $x) {
                                        $unit_kerja[] = $x->getUnitId();
                                    }
                                    $e->add(UnitKerjaPeer::UNIT_ID, $unit_kerja[0], criteria::ILIKE);
                                    for ($i = 1; $i < count($unit_kerja); $i++) {
                                        $e->addOr(UnitKerjaPeer::UNIT_ID, $unit_kerja[$i], criteria::ILIKE);
                                    }
                                    $e->addAscendingOrderByColumn(UnitKerjaPeer::UNIT_NAME);
                                    }

                                    $v = UnitKerjaPeer::doSelect($e);
                                    echo select_tag('filters[unit_id]', objects_for_select($v, 'getUnitId', 'getUnitName', isset($filters['unit_id']) ? $filters['unit_id'] : null, array('include_custom' => '------Semua Satuan Kerja------')), Array('class' => 'js-example-basic-single form-control select2', 'onchange'=>'loadKodeKegiatan(this)'));
                                ?>
                            </div>
                        </div>

                        <?php if($sf_user->getNamaLogin() == 'anggaran') { ?>
                        <div class="col-md-1">
                            <div class="form-group">
                                <label>ID</label>
                                <select name="filters[id]" id="filters_id" class="form-control js-example-basic-single select2">
                                    <option value="">--Pilih Kode--</option>
                                    <?php
                                        if(!empty($load_id)){
                                            foreach($load_id as $key=>$value){
                                                $text = '';
                                                if(isset($filters['id']) && $filters['id']==$value->getKodeKegiatan()){
                                                    $text = 'selected';
                                                }
                                                echo '<option value="'.$value->getKodeKegiatan().'" '.$text.'>'.$value->getKodeKegiatan().'</option>';
                                            }
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <?php } ?>

                        <!-- /.col -->
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>Nama sub kegiatan</label>
                                <?php
                                    echo input_tag('filters[nama_kegiatan]', isset($filters['nama_kegiatan']) ? $filters['nama_kegiatan'] : null, array('class' => 'form-control'));
                                ?>
                            </div>
                        </div>
                        <!-- /.col -->
                        <?php if (sfConfig::get('app_tahap_edit') != 'murni') { ?>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Tahap</label>
                                <?php
                                    echo select_tag('filters[tahap]', options_for_select(
                                    array(
                                            // 'murnibp' => 'Murni (Buku Putih)',
                                            // 'murnibb' => 'Murni (Buku Biru)', 
                                            // 'murnipp' => 'Murni (Penyesuaian)',                                  
                                            'murni' => 'Murni (Final)',
                                            //  'revisi1' => 'Revisi 1',
                                            // 'revisi1_1' => 'Penyesuaian Komponen 1',
                                            //  'revisi2' => 'Revisi 2',
                                            // 'revisi2_1' => 'Penyesuaian Komponen 2',
                                            // 'revisi2_2' => 'Penyesuaian Komponen 3',
                                            //  'revisi3' => 'Revisi 3',
                                            // 'revisi3_1' => 'Penyesuaian Komponen 4',
                                            //  'revisi4' => 'Revisi 4',
                                            //  'revisi5' => 'Revisi 5',
                                            //  'revisi6' => 'Revisi 6',
                                            //  'pakbp'   => 'PAK Buku Putih'
                                        ),
                                        isset($filters['tahap']) ? $filters['tahap'] : '', array('include_custom' => '--Pilih Tahap--')), array('class' => 'js-example-basic-single form-control select2'));
                                ?>
                            </div>
                        </div>
                        <!-- /.col -->
                        <?php } ?>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="tombol_filter">Tombol Filter</label><br/>
                                <button type="submit" name="filter" class="btn btn-outline-primary btn-sm">Filter <i class="fas fa-search"></i></button>
                                <?php
                                    echo link_to('Reset <i class="fa fa-backspace"></i>', 'anggaran/listRevisi?filter=filter', array('class' => 'btn btn-outline-danger btn-sm'));
                                ?>
                            </div>
                            <!-- /.form-group -->
                        </div>
                        <!-- /.col -->                      
                    </div>
                    <?php echo '</form>'; ?>
                </div>
            </div>
          </div>
        </div>
        <div class="row">
            <div class="col-md-12 stretch-card">
                <div class="card">                 
                    <?php if (!$pager->getNbResults()): ?>
                    <?php echo __('no result') ?>
                    <?php else: ?>
                        <?php include_partial('anggaran/list_revisi', array('pager' => $pager, 'filters' => $filters)) ?>
                    <?php endif; ?>            
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    function loadKodeKegiatan(obj){
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/anggaran/getKodeKegiatanByPd/unit_id/" + $(obj).val() + "/menu/<?php echo $menu ?>/tahap/<?php echo isset($filters['tahap']) ? $filters['tahap'] : '' ?>.html",
            type: "GET",
            dataType: 'json',
            success: function(hasil){
                console.log(hasil);
                $('#filters_id').html(hasil.html);
            }
        });
        $('#filters_id').select2();
    }
</script>