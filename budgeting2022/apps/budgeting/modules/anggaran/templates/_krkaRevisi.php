<div class="card-body table-responsive p-0">
    <table class="table table-hover">
       <thead class="head_peach">
                <tr>
                    <th id="sf_admin_list_th_kompoen_id">
                        Perangkat Daerah
                    </th>
                    <th id="sf_admin_list_th_komponen_name">
                        Sub Kegiatan
                    </th>
                    <th id="sf_admin_list_th_kompoen_id">
                        Kode Komponen
                    </th>
                    <th id="sf_admin_list_th_komponen_name">
                        Nama Komponen
                    </th>
                    <th id="sf_admin_list_th_pajak">
                        Volume
                    </th>
                    <th id="sf_admin_list_th_pajak">
                        Koefisien
                    </th>
                    <th id="sf_admin_list_th_komponen_harga">
                        Harga
                    </th>
                    <th id="sf_admin_list_th_rekening">
                        Rekening
                    </th>  
                    <th id="sf_admin_list_th_pajak">
                        Pajak
                    </th>  
                    <th id="sf_admin_list_th_pajak">
                        Anggaran
                    </th>
                </tr>
        </thead>
        <tbody>
            <?php
            $i = 1;
            foreach ($pager->getResults() as $komponen): $odd = fmod(++$i, 2)
                ?>
                <tr>
                    <td><?php echo UnitKerjaPeer::getStringUnitKerja($komponen->getUnitId()) ?></td>
                    <td><?php echo $komponen->getKegiatanId() ?></td>
                    <td><?php echo $komponen->getKomponenId() ?></td>
                    <td><?php echo $komponen->getKomponenName() ?></td>
                    <td style="text-align: center"><?php echo $komponen->getVolume() ?></td>
                    <td style="text-align: center"><?php echo $komponen->getKeteranganKoefisien() ?></td>
                    <td style="text-align: right">
                        <?php
                        if ($komponen->getSatuan() == '%') {
                            echo $komponen->getKomponenHargaAwal();
                        } else {
                            echo number_format($komponen->getKomponenHargaAwal());
                        }
                        ?>
                    </td>
                    <td style="text-align: center"><?php echo $komponen->getRekeningCode() ?></td>
                    <td style="text-align: center"><?php echo $komponen->getPajak() ?></td>
                    <td style="text-align: right"><?php echo number_format($komponen->getNilaiAnggaran()) ?></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>    
</div>
<div class="card-footer clearfix">
    <ul class="pagination pagination-sm m-0 float-left">
        <?php
            echo format_number_choice('[0] no result|[1] 1 result|(1,+Inf] %1% results', array('%1%' => $pager->getNbResults()), $pager->getNbResults()) 
        ?>
    </ul>
    <ul class="pagination pagination-sm m-0 float-right">
        <?php 
        if ($pager->haveToPaginate()): 
            echo '<li class="page-item">'.link_to('Previous', 'anggaran/krkaRevisi?page=' . $pager->getPreviousPage(), array('class' => 'page-link', 'align' => 'absmiddle', 'alt' => __('Previous'), 'title' => __('Previous'))).'</li>';

            foreach ($pager->getLinks() as $page):
                $activeclass = ($page == $pager->getPage()) ? 'active' : '';
                echo '<li class="page-item '.$activeclass.'">'.link_to_unless($page == $pager->getPage(), $page, "anggaran/krkaRevisi?page={$page}", array('class' => 'page-link')).'</li>';
            endforeach;
            echo '<li class="page-item">'.link_to('Next', 'anggaran/krkaRevisi?page=' . $pager->getNextPage(), array('class' => 'page-link', 'align' => 'absmiddle', 'alt' => __('Next'), 'title' => __('Next'))).'</li>'; 
            endif;
        ?>
    </ul>
</div>