<?php use_helper('I18N', 'Date', 'Url', 'Javascript', 'Form', 'Object') ?>

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>Penanda Sumber Dana <?php echo $kode_kegiatan; ?></h1>
</section>

<!-- Main content -->
<section class="content">
    <?php include_partial('anggaran/list_messages'); ?>
    <!-- Default box -->

    <?php echo form_tag('anggaran/prosesUbahPenandaBtl'); ?>
    <div class="box box-primary box-solid">
        <div class="box-body">
            <div id="sf_admin_container">
                <div id="sf_admin_content" class="table-responsive">
                    <table cellspacing="0" class="sf_admin_list">
                        <thead>
                            <tr>
                                <th><b>Detail Kegiatan</b></th>
                                <th><b>Nama Komponen</b></th>
                                <th><b>Satuan</b></th>
                                <th><b>Keterangan Koefisien</b></th>
                                <th><b>Harga</b></th>
                                <th><b>Total</b></th>
                                <th><b>Sumber Dana</b></th>    
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $counter = 0;
                            while($rs_rinciandetail->next()) {
                                $c = new Criteria();
                                $c->addAscendingOrderByColumn(MasterSumberDanaPeer::SUMBER_DANA);
                                $v = MasterSumberDanaPeer::doSelect($c);    
                                $counter++;
                                $nama = $rs_rinciandetail->getString('komponen_name');
                                $style = '';
                                echo "<tr $style>";
                                echo "<td>".$rs_rinciandetail->getString('detail_kegiatan')."</td>";
                                echo "<td>".$rs_rinciandetail->getString('komponen_name')." ".$rs_rinciandetail->getString('detail_name');echo "<span class='label label-info'>".$rs_rinciandetail->getString('sumber_dana')."</span></td>";
                                echo "<td>".$rs_rinciandetail->getString('satuan')."</td>";
                                echo "<td>".$rs_rinciandetail->getString('keterangan_koefisien')."</td>";
                                echo "<td align='right'>".number_format($rs_rinciandetail->getString('komponen_harga'), 0, ',', '.')."</td>";
                                echo "<td align='right'>".number_format($rs_rinciandetail->getString('nilai_anggaran'), 0, ',', '.')."</td>";
                                echo "<td>";
                                    //echo select_tag('id', objects_for_select($V, 'getId', 'getSumberDana', isset($filters['id']) ? $filters['id'] : null, array('include_custom' => '------Sumber Dana------')), array('class' => 'form-control')); 
                                    echo select_tag('sumber_dana[]', objects_for_select($v, 'getId', 'getSumberDana', '', 'include_custom=---Pilih Sumber Dana---'), array('class' => 'js-example-basic-single', 'style' => 'width:100%'));
                                echo "</td>";
                                echo input_hidden_tag('detail_no[]', $rs_rinciandetail->getString('detail_no'));
                                echo input_hidden_tag('unit_id', $unit_id);
                                echo input_hidden_tag('kode_kegiatan', $kode_kegiatan);
                                echo "</tr>";
                            }
                            ?>
                            <?php if($counter <= 0): ?>
                                <tr><td colspan="16" align="center">Tidak ada komponen yang bisa ditampilkan</td></tr>
                            <?php endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <br/>
             <?php
            if($counter > 0)
                echo submit_tag('Proses Ubah Penanda Sumber Dana', array('name' => 'proses', 'class' => 'btn btn-success btn-flat')) . '&nbsp;';
            ?> 
        </div>
    </div>
    </form>

</section><!-- /.content -->

<script>
    $("#cekSemua").change(function () {
        $(".cek").prop('checked', $(this).prop("checked"));
    });
</script>