<?php use_helper('I18N', 'Date', 'Url', 'Javascript', 'Form', 'Object', 'Number', 'Validation') ?>
<?php ?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>Edit Header Kegiatan</h1>
</section>

<!-- Main content -->
<section class="content">
    <?php include_partial('anggaran/list_messages'); ?>

    <div class="box box-primary box-solid">
        <div class="box-header with-border">
            <h3 class="box-title">Form Edit Header Kegiatan</h3>
        </div>
        <div class="box-body">
            <div id="sf_admin_container" class="table-responsive">
                <?php echo form_tag('anggaran/editHeader') ?>
                <table cellspacing="0" class="sf_admin_list">
                    <thead>  
                        <tr>
                            <th style="width: 19%"><b>Nama</b></th>
                            <th style="width: 1%">&nbsp;</th>
                            <th style="width: 80%"><b>Isian</b></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="sf_admin_row_0" align='right'>
                            <td>Lokasi</td>
                            <td align="center">:</td>
                            <td align="left">
                                <input type="text" name="lokasi" class="form-control" placeholder="Lokasi" value="<?php echo $dapat_kegiatan->getLokasi(); ?>">
                            </td>
                        </tr>
                        <tr class="sf_admin_row_1" align='right'>
                            <td>Kelompok Sasaran</td>
                            <td align="center">:</td>
                            <td align="left">
                                <?php //$sasaran = str_replace('|', ', ', $dapat_kegiatan->getKelompokSasaran()); ?>
                                <input type="text" name="kelompok_sasaran" class="form-control" placeholder="Kelompok Sasaran" value="<?php echo $dapat_kegiatan->getKelompokSasaran(); ?>">
                            </td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr class="sf_admin_row_0" align='right' valign="top">
                            <td>&nbsp; </td>
                            <td>
                                <?php
                                echo input_hidden_tag('unit_id', $sf_params->get('unit_id'));
                                echo input_hidden_tag('kode_kegiatan', $sf_params->get('kode_kegiatan'));
                                ?>
                            </td>
                            <td>
                                <?php
                                echo submit_tag('simpan', 'name=simpan') . ' ' . button_to('kembali', '#', array('onClick' => "javascript:history.back()"));
                                ?>
                            </td>
                        </tr>
                    </tfoot>
                </table>
                <?php echo '</form>'; ?>
            </div>
        </div>
    </div>
</section>
