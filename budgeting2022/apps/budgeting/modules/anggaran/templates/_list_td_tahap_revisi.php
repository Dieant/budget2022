<?php
$con = Propel::getConnection();
$query = "select max(status_level) as max "
        . " from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail "
        . " where unit_id='" . $master_kegiatan->getUnitId() . "' and kegiatan_code='" . $master_kegiatan->getKodeKegiatan() . "'";
$stmt = $con->prepareStatement($query);
$rs_max = $stmt->executeQuery();
if ($rs_max->next()) {
    $max = $rs_max->getInt('max');
    if ($max == 0 || $max == null) {
        $max = $master_kegiatan->getStatusLevel();
    }
}
$sisipan = '';
if ($max < 7 && $master_kegiatan->getIsPernahRka()) {
    $sisipan = 'S';
}
?>
<td>
    <?php
    if (isset($filters['tahap']) && $filters['tahap'] == 'pakbp') {
        echo '<b><blink>PAK (Buku Putih)</blink></b>';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'pakbb') {
        echo '<b><blink>PAK (Buku Biru)</blink></b>';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'murni') {
        echo '<b><blink>Murni</blink></b>';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'murnibp') {
        echo '<b><blink>Murni (Buku Putih)</blink></b>';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'murnibb') {
        echo '<b><blink>Murni (Buku Biru)</blink></b>';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'murnibbpraevagub') {
        echo '<b><blink>Murni (Buku Biru Pra Evaluasi Gubernur)</blink></b>';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi1') {
        echo '<b><blink>Revisi 1</blink></b>';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi1_1') {
        echo '<b><blink>Penyesuaian Komponen 1</blink></b>';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi2') {
        echo '<b><blink>Revisi 2</blink></b>';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi2_1') {
        echo '<b><blink>Penyesuaian Komponen 2</blink></b>';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi2_2') {
        echo '<b><blink>Penyesuaian Komponen 3</blink></b>';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi3') {
        echo '<b><blink>Revisi 3</blink></b>';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi3_1') {
        echo '<b><blink>Penyesuaian Komponen 4</blink></b>';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi4') {
        echo '<b><blink>Revisi 4</blink></b>';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi5') {
        echo '<b><blink>Revisi 5</blink></b>';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi6') {
        echo '<b><blink>Revisi 6</blink></b>';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi7') {
        echo '<b><blink>Revisi 7</blink></b>';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi8') {
        echo '<b><blink>Revisi 8</blink></b>';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi9') {
        echo '<b><blink>Revisi 9</blink></b>';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi10') {
        echo '<b><blink>Revisi 10</blink></b>';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'rkua') {
        echo '<b><blink>RKUA</blink></b>';
    } else {
        //var_dump($master_kegiatan);exit;
        $tahap = historyTahap::cekTahapRevisi($master_kegiatan->getUnitId(), $master_kegiatan->getKodeKegiatan());
        if ($sf_user->getNamaUser() != 'peninjau' && $sf_user->getNamaUser() != 'wawali' && $sf_user->getNamaUser() != 'walikota' && $sf_user->getNamaUser() != 'sekda' && $sf_user->getNamaUser() != 'asisten' && $sf_user->getNamaUser() != 'parlemen2') {
            echo '<b><blink>' . strtoupper($tahap . $sisipan) . '</blink></b>';
        }
    }
    ?>
</td>