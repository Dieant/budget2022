<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="fas fa-align-left"></i> Laporan Per Dinas Per Kegiatan Per Belanja
                        </h3>
                    </div>
                    <div class="card-body"> 
                        <table class="table table-bordered">
                            <tr>
                                <th class="text-center" style="width: 10px"></th>
                                <th class="text-center" style="width: 10px"></th>
                                <th class="text-center">Dinas - Kegiatan - Belanja</th>
                                <th class="text-center">Nilai Usulan</th>
                                <th class="text-center">Nilai Disetujui</th>
                            </tr>
                            <?php
                            $i = 0;
                            if ($rs->first() <> '') {
                                $rs->first();
                                do {
                                    if (isset($dinas_ok[$i])):
                                        ?>
                                        <tr><td colspan="5">&nbsp;</td></tr>
                                        <tr>
                                            <td colspan="3" class="text-left text-bold"><?php echo $unit_id[$i] . ' - ' . $unit_name[$i] ?></td>
                                            <td class="text-right text-bold"><?php //echo $nilai_usulan_unit[$i] ?></td>
                                            <td class="text-right text-bold"><?php //echo $nilai_locked[$i] ?></td>
                                            <?php $nilai_locked_total = str_replace('.', '', $nilai_locked[$i]); ?>
                                        </tr>
                                        <?php
                                    endif;
                                    if (isset($kegiatan_ok[$i])):
                                        ?>
                                        <tr>
                                            <td style="width: 10px">&nbsp;</td>
                                            <td colspan="2" class="text-left text-bold"><?php echo $rs->getString('kegiatan_id'); ?>&nbsp;<?php echo $nama_kegiatan[$i] ?></td>
                                            <td class="text-right text-bold"><?php echo $nilai_draft3[$i] ?></td>
                                            <td class="text-right text-bold"><?php echo $nilai_locked3[$i] ?></td>
                                        </tr>
                                    <?php endif;
                                    ?>
                                    <tr>
                                        <td colspan="2">&nbsp;</td>
                                        <td class="text-left"><?php echo isset($subtitle[$i]) ? $subtitle[$i] : '' ?></td>
                                        <td class="text-right"><?php echo isset($nilai_draft4[$i]) ? $nilai_draft4[$i] : '' ?></td>
                                        <td class="text-right"><?php echo isset($nilai_locked4[$i]) ? $nilai_locked4[$i] : '' ?></td>
                                    </tr>
                                    <?php
                                    $i++;
                                } while ($rs->next());
                            }
                            ?>
                            <tr>
                                <td colspan="3" class="text-bold text-center bg-green-active">Total</td>
                                <td class="text-bold text-right bg-green-active"><?php echo $nilai_draft2 ?></td>
                                <td class="text-bold text-right bg-green-active"><?php echo $nilai_locked2 ?></td>
                            </tr>
                        </table>  
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- <div class="box box-info">        
    <div class="box-header with-border">
        
    </div>
    <div class="box-body">
             
    </div>
</div> -->