<div class="box box-info">        
    <div class="box-header with-border">
        Laporan Per Dinas Per Belanja
    </div>
    <div class="box-body">
        <table class="table table-bordered">
            <tr>
                <th style="width: 10px"></th>
                <th style="width: 10px"></th>
                <th class="text-center">Dinas - Belanja</th>
                <th class="text-center">Nilai Semula</th>
                <th class="text-center">Nilai Menjadi</th>
                <th class="text-center">Selisih</th>
            </tr>
            <?php
            // print_r($rs);die();
            // print_r($nilai_mj_draft);die();
            $pd_mjd = 0;
            $pd_sm = 0;
            $blj_mjd = 0;
            $tot_new_sm = 0;

            $i = 0;
            // if ($rs->first() <> '') {
            //     $rs->first();
            if ($rs_menjadi_br->first() <> '') {
                $rs_menjadi_br->first();
                do {
                    ?>
                    <?php if (isset($dinas_ok[$i])): ?>
                        <tr><td colspan="6">&nbsp;</td></tr>
                        <tr>
                            <td colspan="3" class="text-left text-bold"><?php echo $unit_id[$i] ?> &nbsp; <?php echo $unit_name[$i] ?></td>
                            <td class="text-right text-bold">
                            <?php //echo $nilai_draft[$i] 
                            $tot_new_sm += $nil_sm[$pd_sm];
                            echo number_format($nil_sm[$pd_sm], 0, ",", ".");
                            $pd_sm++;

                            ?></td>
                            <td class="text-right text-bold">
                            <?php 
                            //echo $nilai_mj_locked[$i] 
                            $hsl += $total_jd[$pd_mjd];
                            echo number_format($total_jd[$pd_mjd], 0, ",", ".");
                            $pd_mjd++;
                            ?>    
                            </td>

                            <td class="text-right text-bold">&nbsp;</td>
                            <?php
                            $nilai_locked_total = str_replace('.', '', $nilai_locked[$i]);
                            ?>
                        </tr>
                    <?php endif ?>
                    <tr>
                        <th style="width: 10px"></th>
                        <th style="width: 10px"></th>
                        <td class="text-left">
                            <?php echo isset($subtitle[$i]) ? $subtitle[$i] : '' ?>
                        </td>
                        <td class="text-right">
                            <?php 
                            // echo isset($nilai_draft4[$i]) ? $nilai_draft4[$i] : '' ?>
                            <?php 
                            echo number_format($nilai_sm_br[$i], 0, ",", "."); ?>
                                
                        </td>
                        <td class="text-right">
                        <?php 
                        //echo isset($nilai_mj_locked4[$i]) ? $nilai_mj_locked4[$i] : '' 
                        // echo $nilai_jd_br[$blj_mjd];
                        echo number_format($nilai_jd_br[$blj_mjd], 0, ",", ".");
                        //echo number_format($aaa[$blj_mjd], 0, ",", ".");
                        
                        ?>
                        </td>
                        <td class="text-right">
                            <?php
                            //$nilai = str_replace('.', '', ($nilai_draft4[$i]-$nilai_mj_locked4[$i]));
                            //$nilai = $nilai_draft4[$i]-$nilai_mj_locked4[$i];

                            $nilai_selisih = $nilai_sm_br[$i]-$nilai_jd_br[$blj_mjd];
                            //echo $nilai_selisih;
                            //$nilai = str_replace('.', '', $nilai_locked4[$i]);
                            //echo number_format(($nilai / $nilai_locked_total) * 100, 2, ",", ".");
                            echo number_format($nilai_selisih, 0, ",", ".");
                            ?>
                        </td>
                    </tr>
                    <?php
                    
                    $blj_mjd++;
                    $i++;
                // } while ($rs->next());
                } while ($rs_menjadi_br->next());
                
            }
            ?>
            <tr>
                <td colspan="3" class="text-center text-bold bg-green-active">Total</td>
                <td class="text-right text-bold bg-green-active"><?php //echo $nilai_draft2 
                echo number_format($tot_new_sm, 0, ",", ".");
                ?></td>
                <td class="text-right text-bold bg-green-active">
                <?php 
                //echo $nilai_mj_locked2 
                echo number_format($hsl, 0, ",", ".");
                
                ?>
                </td>
                <td class="text-right text-bold bg-green-active">
                <?php
                    $total_semula = str_replace('.', '', $nilai_draft2);
                    $total_menjadi = str_replace('.', '', $nilai_mj_locked2);
                    // $total_selisih = $total_menjadi-$total_semula;
                    $total_selisih = $hsl-$tot_new_sm;
                    echo number_format($total_selisih, 0, ",", ".");
                    //394....
                ?>
                </td>
            </tr>
        </table>       
    </div>
</div>