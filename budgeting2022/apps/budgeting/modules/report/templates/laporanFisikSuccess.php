<div class="box box-info">
    <div class="box-header with-border">
        Laporan Pengisian Komponen Fisik
    </div>
    <div class="box-body">
        <table class="table table-bordered">
            <tr>
                <th class="text-center">Kode SKPD</th>
                <th class="text-center">Nama SKPD</th>
                <th class="text-center">Kode Kegiatan</th>
                <th class="text-center">Nama Kegiatan</th>
                <th class="text-center">FISIK</th>
                <th class="text-center">Pengawasan</th>
                <th class="text-center">Perencanaan</th>
            </tr>
                    <?php
                        $pagu_fisik = 0;
                        $oper_pagu_fisik = 0;
                        $uk_awas = 0;
                        $oper_uk_awas = 0;
                        $uk_plan = 0;
                        $oper_uk_plan = 0;
                        $totalfisik = 0;
                        $totalawas = 0;
                        $totalplan = 0;
                    ?>


                    <?php

                    foreach ($b as $key => $valuea) {
                    ?>
                        <tr>
                            <td class="text-left" ><?php echo $valuea['unit_id'] ?></td>
                            <td class="text-left" ><?php echo $valuea['unit_name'] ?></td>                          
                            <td class="text-left" ><?php echo $valuea['kode_kegiatan'] ?>
                            </td>
                            <td class="text-left" ><?php echo $valuea['nama_kegiatan'] ?>
                            </td>
                            <td class="text-right" >
                                <?php 
                                    $oper_pagu_fisik = $valuea['komponen_fisik'];
                                    if ($oper_pagu_fisik) {
                                        $pagu_fisik = $oper_pagu_fisik;
                                    }
                                    else {
                                        $pagu_fisik = 0;   
                                    }
                                    echo number_format($pagu_fisik, 0, ',', '.');
                                    $totalfisik += $pagu_fisik;
                                    //$totalKua += $valuea['pagu_uk'];
                                    //echo number_format($valuea['pagu_uk'], 0, ',', '.');
                                ?>
                            </td>
                            <td class="text-right" >
                                <?php
                                    $oper_uk_awas = $valuea['pengawasan'];
                                    if ($oper_uk_awas) {
                                        $uk_awas = $oper_uk_awas;
                                    }
                                    else {
                                        $uk_awas = 0;   
                                    }
                                    echo number_format($uk_awas, 0, ',', '.');
                                    $totalawas += $uk_awas;
                                    // $totalRinc += $valuea['uk_terisi'];
                                    // echo number_format($valuea['uk_terisi'], 0, ',', '.');
                                ?>
                            </td>
                            <td class="text-right" >
                                <?php
                                    $oper_uk_plan = $valuea['perencana'];
                                    if ($oper_uk_plan) {
                                        $uk_plan = $oper_uk_plan;
                                    }
                                    else {
                                        $uk_plan = 0;   
                                    }
                                    echo number_format($uk_plan, 0, ',', '.');
                                    $totalplan += $uk_plan;
                                    // $totalRinc += $valuea['uk_terisi'];
                                    // echo number_format($valuea['uk_terisi'], 0, ',', '.');
                                ?>
                            </td>
                        </tr>                    
                    <?php
                    }
                    ?>
                        <tr>
                            <td colspan="4" class="text-center"><h5><b>TOTAL</b></h5></td> 
                            <td class="text-right" ><b><?php echo number_format($totalfisik, 0, ',', '.');?></b></td>
                            <td class="text-right" ><b><?php echo number_format($totalawas, 0, ',', '.');?><b></td>
                            <td class="text-right"><b><?php echo number_format($totalplan, 0, ',', '.');?></b></td>
                        </tr>
        </table>     
    </div>
</div>