<?php use_helper('I18N', 'Date', 'Url', 'Javascript', 'Form', 'Object') ?>

<!-- Content Header (Page header) -->
<section class="content-header">    
    <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Penanda Prioritas</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Lembar Kerja</a></li>
          <li class="breadcrumb-item active">Set Komponen Prioritas <?php echo $kode_kegiatan;?></li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <?php include_partial('entri/list_messages'); ?>
    <!-- Default box -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 stretch-card">
                <div class="card">
                <?php echo form_tag('report/prosesSetPrioritas'); ?>           
                    <div class="card-body table-responsive p-0">
                        <table class="table table-hover">
                            <thead class="head_peach">
                                <tr>
                                <th><b>Nama Komponen</b></th>
                                <th><b>Volume</b></th>
                                <th><b>Satuan</b></th>
                                <th><b>Pajak</b></th>
                                <th><b>Harga</b></th>
                                <th><b>Total</b></th>
                                <th><?php echo checkbox_tag('cekAll', 1, null, "id=cekSemua"); ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                                            <?php
                            $counter = 0;
                            while($rs_rinciandetail->next()) {
                                $counter++;
                                $nama = $rs_rinciandetail->getString('komponen_name');
                                $style = '';
                                $check = "name='pilihAction[]' class='cek'";
                                if($rs_rinciandetail->getBoolean('prioritas_wali'))
                                    $rek = 'checked';
                                else
                                    $rek = '';
                                echo "<tr $style>";

                                echo "<td>".$rs_rinciandetail->getString('komponen_name')." ".$rs_rinciandetail->getString('detail_name')."</td>";
                                echo "<td>".$rs_rinciandetail->getString('volume')."</td>";
                                echo "<td>".$rs_rinciandetail->getString('satuan')."</td>";
                                echo "<td>".$rs_rinciandetail->getString('pajak')."</td>";
                                echo "<td>";
                                if ($rs_rinciandetail->getString('satuan') == '%') {
                                    $len = strlen(substr(strrchr($rs_rinciandetail->getFloat('komponen_harga_awal'), "."), 1));
                                    echo number_format($rs_rinciandetail->getFloat('komponen_harga_awal'), $len, ',', '.');
                                } else {
                                    // khusus komponen Biaya Rekening Listrik, dll
                                    $sub_id = substr($rs_rinciandetail->getString('komponen_id'), 0, 11);
                                    $arr_komp = array('23.02.02.03.03.B');
                                    $arr_name = array('Tenaga Operasional','Tenaga Administrasi 3','Tenaga Programmer 3','Tenaga Administrasi 3','Petugas Keamanan','Tenaga Operator 3','Tenaga Satgas','Pembantu Paramedis');
                                    if(
                                        in_array($rs_rinciandetail->getString('komponen_name'), $arr_name) ||
                                        in_array($rs_rinciandetail->getString('komponen_id'), $arr_komp) ||
                                        // all komponen honorer
                                        $sub_id == '23.01.01.08'
                                    ) {
                                        echo number_format($rs_rinciandetail->getFloat('komponen_harga_awal'), 2, ',', '.');
                                    } else {
                                        echo number_format($rs_rinciandetail->getFloat('komponen_harga_awal'), 0, ',', '.');
                                    }
                                }
                                echo "</td>";
                                echo "<td>".number_format(round($rs_rinciandetail->getString('nilai_anggaran')), 0, ',', '.')."</td>";

                                // echo "<td align='left'>".select_tag('rekening_'.$rs_rinciandetail->getString('detail_no'), $arr_rekening[$rs_rinciandetail->getString('komponen_id')], array('style' => 'color:black', 'disabled' => $rek))."</td>";

                                echo "<td align='center'>"."<input type='checkbox' $check $rek value='".$rs_rinciandetail->getString('detail_no')."'>"."</td>";
                                echo input_hidden_tag('unit_id', $unit_id);
                                echo input_hidden_tag('kode_kegiatan', $kode_kegiatan);

                                echo "</tr>";
                            }
                            ?>
                            <?php if($counter <= 0): ?>
                                <tr><td colspan="16" align="center">Tidak ada komponen yang bisa ditampilkan</td></tr>
                            <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer">
                         <br/>
                        <?php
                        if($counter > 0)
                            echo submit_tag('Proses Set Prioritas', array('name' => 'proses', 'class' => 'btn btn-success btn-flat')) . '&nbsp;';
                        ?>
                    </div>
                </div>
                <?php echo '</form>'; ?>
            </div>
        </div>
    </div>  
</section><!-- /.content -->

<script>
    $("#cekSemua").change(function () {
        $(".cek").prop('checked', $(this).prop("checked"));
    });
</script>