<?php use_helper('Form', 'Object', 'Javascript') ?>
<?php
if (sfConfig::get('app_tahap_edit') == 'murni') {
    $nama_sistem = 'Pra-RKA';
} elseif (sfConfig::get('app_tahap_edit') == 'pak') {
    $nama_sistem = 'PAK';
} elseif (sfConfig::get('app_tahap_edit') == 'penyesuaian') {
    $nama_sistem = 'Penyesuaian';
} else {
    $nama_sistem = 'Revisi';
}
?>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="fas fa-align-left"></i> Laporan Posisi Kegiatan <?php echo $nama_sistem; ?>
                        </h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Perangkat Daerah</label>
                                    <?php
                                        $e = new Criteria();
                                        $e->add(UnitKerjaPeer::UNIT_ID, '9999', Criteria::NOT_IN);
                                        $e->addAscendingOrderByColumn(UnitKerjaPeer::UNIT_NAME);
                                        $unit_kerja = UnitKerjaPeer::doSelect($e);
                                        echo select_tag('unit_id', objects_for_select($unit_kerja, 'getUnitId', 'getUnitName', (isset($filter_unit_kerja) ? $filter_unit_kerja : NULL), array('include_custom' => '------Pilih Perangkat Daerah------')), array('id' => 'unit_id', 'class' => 'form-control js-example-basic-single'));
                                    ?>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Posisi</label>
                                    <?php
                                        echo select_tag('posisi', options_for_select(array('-1' => 'Entri', '1' => 'PPTK', '2' => 'KPA', '3' => 'PA I', '4' => 'Tim Anggaran', '5' => 'PA II', '6' => 'Penyelia II', '7' => 'RKA'), $filter_posisi, 'include_custom=------Pilih Posisi------'), Array('id' => 'posisi', 'class' => 'form-control js-example-basic-single', 'style' => 'width:100%'));
                                    ?>
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                    </div>
                    <div class="card-body table-responsive p-0">
                        <table class="table table-hover">
                            <thead class="head_peach">
                                <tr>
                                    <th class="text-center">Dinas</th>
                                    <th class="text-center">Kode Kegiatan</th>
                                    <th class="text-center">Nama Kegiatan</th>
                                    <th class="text-center">Posisi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($kegiatan as $value):
                                    if (isset($value['skpd_ok'])):
                                        ?>
                                <tr>
                                    <td colspan="4">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="4" class="text-left text-bold">
                                        <?php echo $value['unit_id'] . ' - ' . $value['unit_name'] ?></td>
                                </tr>
                                <?php endif; ?>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td class="text-left"><?php echo $value['kode_kegiatan'] ?></td>
                                    <td class="text-left"><?php echo $value['nama_kegiatan'] ?></td>
                                    <td class="text-left"><?php echo $value['posisi'] ?></td>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $(document).ready(function () {
        $(".js-example-basic-single").select2();
    });
    $("#unit_id").change(function () {
        $('#indicator').show();
        var id = $(this).val();
        var id2 = $("#posisi").val();
        if (id == '') {
            id = '0000';
        }
        if (id2 == '') {
            id2 = 'xxx';
        }
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/report/reportPosisiRevisi/unit_id/" + id + "/posisi/" + id2 + ".html",
            context: document.body
        }).done(function (msg) {
            $('#indicator').hide();
            $('#isi').html(msg);
        });

    });
    $("#posisi").change(function () {
        $('#indicator').show();
        var id = $("#unit_id").val();
        var id2 = $(this).val();
        if (id == '') {
            id = '0000';
        }
        if (id2 == '') {
            id2 = 'xxx';
        }
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/report/reportPosisiRevisi/unit_id/" + id + "/posisi/" + id2 + ".html",
            context: document.body
        }).done(function (msg) {
            $('#indicator').hide();
            $('#isi').html(msg);
        });
    });
</script>