<?php use_helper('Form', 'Object', 'Javascript') ?>
<?php
if (sfConfig::get('app_tahap_edit') == 'murni') {
    $nama_sistem = 'Pra-RKA';
} elseif (sfConfig::get('app_tahap_edit') == 'pak') {
    $nama_sistem = 'PAK';
} elseif (sfConfig::get('app_tahap_edit') == 'penyesuaian') {
    $nama_sistem = 'Penyesuaian';
} else {
    $nama_sistem = 'Revisi';
}
?>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="fas fa-align-left"></i> Laporan Approval 8 TAPD Kegiatan <?php echo $nama_sistem; ?>
                        </h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                <label>Perangkat Daerah</label>
                                    <?php
                                    $e = new Criteria();
                                    $e->add(UnitKerjaPeer::UNIT_ID, '9999', Criteria::NOT_IN);
                                    $e->addAscendingOrderByColumn(UnitKerjaPeer::UNIT_NAME);
                                    $unit_kerja = UnitKerjaPeer::doSelect($e);
                                    echo select_tag('unit_id', objects_for_select($unit_kerja, 'getUnitId', 'getUnitName', (isset($filter_unit_kerja) ? $filter_unit_kerja : NULL), array('include_custom' => '------Pilih Perangkat Daerah------')), array('id' => 'unit_id', 'class' => 'form-control js-example-basic-single'));
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body table-responsive p-0">
                        <table class="table table-hover">
                            <thead class="head_peach">
                                <tr> 
                                    <th class="text-center">Dinas</th>
                                    <th class="text-center">Kode Kegiatan</th>
                                    <th class="text-center">Nama Kegiatan</th>
                                    <th class="text-center">BPKAD</th>
                                    <th class="text-center">Bappeda</th>
                                    <th class="text-center">BPBJAP</th>
                                    <th class="text-center">Bag. Hukum</th>
                                    <th class="text-center">Inspektorat</th>
                                    <th class="text-center">BKPSDM</th>
                                    <!-- <th class="text-center">LP2A</th> -->
                                    <th class="text-center">Bapenda</th>
                                    <th class="text-center">Bag. Organisasi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $tempUnit = '';
                                foreach ($kegiatan as $value): ?>
                                    <?php if($tempUnit != $value['unit_id']): ?>
                                        <tr><td colspan="11">&nbsp;</td></tr>
                                        <tr>
                                            <td colspan="11" class="text-left text-bold"><?php echo $value['unit_id'] . ' - ' . $value['unit_name'] ?></td>
                                        </tr>
                                    <?php endif; ?>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td class="text-left" ><?php echo $value['kode_kegiatan']; ?></td>
                                        <td class="text-left" ><?php echo $value['nama_kegiatan']; ?></td>
                                        <td class="text-center"><?php echo $value['bpkpd'] ? '&#10004;' : '&nbsp;'; ?></td>
                                        <td class="text-center"><?php echo $value['bappeko'] ? '&#10004;' : '&nbsp;'; ?></td>
                                        <td class="text-center"><?php echo $value['adpemb'] ? '&#10004;' : '&nbsp;'; ?></td>
                                        <td class="text-center"><?php echo $value['bagian_hukum'] ? '&#10004;' : '&nbsp;'; ?></td>
                                        <td class="text-center"><?php echo $value['inspektorat'] ? '&#10004;' : '&nbsp;'; ?></td>
                                        <td class="text-center"><?php echo $value['bkd'] ? '&#10004;' : '&nbsp;'; ?></td>
                                        <td class="text-center"><?php echo $value['lp2a'] ? '&#10004;' : '&nbsp;'; ?></td>
                                        <td class="text-center"><?php echo $value['bagian_organisasi'] ? '&#10004;' : '&nbsp;'; ?></td>
                                    </tr>
                                    <?php
                                    $tempUnit = $value['unit_id'];
                                endforeach;
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $(document).ready(function () {
        $(".js-example-basic-single").select2();
    });
    $("#unit_id").change(function () {
        $('#indicator').show();
        var id = $(this).val();
        if (id == '') {
            id = '0000';
        }
        var tahap = "<?php echo $tahap; ?>";
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/report/reportApprovalTapd/unit_id/" + id + ".html",
            context: document.body
        }).done(function (msg) {
            $('#indicator').hide();
            $('#isi').html(msg);
        });

    });
</script>