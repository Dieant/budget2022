<?php use_helper('Form','Object','Javascript') ?>
<?php

echo select_tag('dinas', objects_for_select($unit_kerja,'getUnitId', 'getUnitName',
                        '',array('include_custom'=>'Pilih SKPD')));
?>
<div id="isiKeg">
<?php
//print_r($unit_idDinas.' '.$nama_unit);
    echo select_tag('kegiatan', options_for_select(array( 'null'=>'Pilih Jenis Tampilan','semua'=>'Tampilkan Semua', 'berubah'=>'Tampilkan yang berubah'), 'null'),
     Array('id'=>'combo_pilih','onChange'=>remote_function(Array('update'=>'isi', 'url'=>'report/reportTambahBeda',
    'with'=>"'b=reportTambahBeda&tampilan='+this.value+'&unitId='+dinas.value",
    'loading'=>"Element.show('indicator');Element.hide('isi');Element.hide('isi_dalam');",
    'complete'=>"Element.hide('indicator');Element.show('isi');".visual_effect('highlight', 'isi')))))
?></div>

<div id="isi"></div>
<?php if(isset ($rs)):?>
<table width="760"  border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td width="1" background="../images/grs_vert.gif"></td>
      <td width="730" height="20" align="center" background="../images/menu_bg.gif" class="font12"><font color="#FFFFFF"><strong class="font12"><span class="Font10BoldWhite">Laporan Revisi Per Dinas - Kegiatan - Rekening</span></strong></font>
  	  &nbsp;&nbsp;</td>
      <td width="30" align="right" background="../images/menu_bg.gif" class="Font10BoldWhite"><a href="index.php"><img src="../images/cross.gif" width="18" height="16" border="0" align="absmiddle"></a></td>
      <td width="1" background="../images/grs_vert.gif"></td>
    </tr>
    <tr>
      <td width="1" background="../images/grs_vert.gif"></td>
      <td colspan="2" align="center">
        <table width="760"  border="0" cellpadding="3" cellspacing="1" bgcolor="#333333">
          <tr>
		  <td  align="center" bgcolor="#9999FF"  width="17"><strong></strong></td>
            <td width="8"  align="center" bgcolor="#9999FF"><div align="center"></div></td>
            <td width="694" colspan="2" align="center" bgcolor="#9999FF"><strong>Unit - Kegiatan - Rekening</strong></td>
            <td width="241" align="center" bgcolor="#9999FF"><strong>Semula</strong></td>
            <td width="241" align="center" bgcolor="#9999FF"><strong>Menjadi</strong></td>
            <td width="241" align="center" bgcolor="#9999FF"><strong>Selisih</strong></td>
          </tr>
		  
		   {foreach from=$rs item=r}
		  
          {if $r.dinas_ok=='t'}
		  <tr><td colspan="7"  bgcolor="#FFFFFF"  >&nbsp;</td></tr>
         <!-- <tr>
            <td colspan="6" align="left" bgcolor="#FFFFFF" ><strong>{$r.unit_id} &nbsp; {$r.unit_name}</strong></td>
            <td align="right" bgcolor="#FFFFFF" class="Font8v"><strong>{$r.nilai_selisih}</strong></td>
          </tr>-->
          {/if}
		  {if $r.kegiatan_ok=='t' and  $r.sembunyi<>'t'}
          <tr>
            <td  align="center" bgcolor="#FFFFFF"  width="17"><strong></strong></td>
          <td colspan="5"  align="left" bgcolor="#FFFFFF" class="Font8v"><strong>{$r.kode_kegiatan}&nbsp;{$r.nama_kegiatan}</strong></td>
            <td align="right" bgcolor="#FFFFFF" class="Font8v"><strong>{$r.nilai_selisih3}</strong></td>
          </tr>
           
		  
		  {/if}
		  {if $r.sub_ok=='t' and $r.sembunyi<>'t'}
          <tr>
            <td  align="center" bgcolor="#FFFFFF"></td>
            <td  align="left" bgcolor="#FFFFFF" class="Font8v"></td>
            <td colspan="4"  align="left" bgcolor="#FFFFFF" class="Font8v"><em><strong>{$r.subtitle}</strong></em></td>
            <td align="right" bgcolor="#FFFFFF" class="Font8v">{$r.nilai_selisih5}</td>
          </tr>
		  {/if}
		{if $r.nilai_selisih4<>0 and $r.sembunyi<>'t'}
	       
          <tr>
            <td  align="center" bgcolor="#FFFFFF"  width="17"><strong></strong></td>
          <td  align="left" bgcolor="#FFFFFF" class="Font8v"></td>
            <td align="left" bgcolor="#FFFFFF" class="Font8v">&nbsp;</td>
            <td align="left" bgcolor="#FFFFFF" class="Font8v">{$r.rekening_code}&nbsp;{$r.rekening_name}</td>
            <td align="right" bgcolor="#FFFFFF" class="Font8v">{$r.nilai_draft4}</td>
            <td align="right" bgcolor="#FFFFFF" class="Font8v">{$r.nilai_locked4}</td>
            <td align="right" bgcolor="#FFFFFF" class="Font8v">{$r.nilai_selisih4}</td>
          </tr>
		{/if}
          {/foreach}
		  <tr>
            <td colspan="4" bgcolor="#FFFFFF" class="Font8v"><strong>Total</strong></td>
            <td align="right" bgcolor="#FFFFFF" class="Font8v"><strong>{$nilai_draft2}</strong></td>
            <td align="right" bgcolor="#FFFFFF" class="Font8v"><strong>{$nilai_locked2}</strong></td>
            <td align="right" bgcolor="#FFFFFF" class="Font8v"><strong>{$nilai_selisih2}</strong></td>
		  </tr>
        </table>
        <br>
      </td>
      <td width="1" background="../images/grs_vert.gif"></td>
    </tr>
    <tr>
      <td width="1" background="../images/grs_vert.gif"></td>
      <td height="1" colspan="2" background="../images/grs_hors.gif"></td>
      <td width="1" background="../images/grs_vert.gif"></td>
    </tr>
  </table>
<?php endif; ?>