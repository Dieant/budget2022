<?php use_helper('Form', 'Object', 'Javascript') ?>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="fas fa-align-left"></i> Laporan Pagu KUA PPAS & APBD
                        </h3>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered">
                            <tr>
                                <th class="text-center">Dinas</th>
                                <th class="text-center">Kode Sub Kegiatan</th>
                                <th class="text-center">Id Sub Kegiatan</th>
                                <th class="text-center">Nama Sub Kegiatan</th>
                                <!-- <th class="text-center">RAPBD (Buku Putih)</th> -->
                                <th class="text-center">Pagu</th>
                                <th class="text-center">Rincian (Terentri)</th>
                                <th class="text-center">Selisih (Pagu - Terentri)</th>
                                <!-- <th class="text-center">Selisih (RAPBD - APBD)</th> -->
                                <th class="text-center">Kunci</th>
                                <th class="text-center">Posisi</th>
                            </tr>
                            <?php
                            $tempUnit = '';
                            $anggaran = 0;
                            $tambahan = 0;
                            $pagu = 0;
                            $anggaran_kota = 0;
                            $tambahan_kota = 0;
                            $pagu_kota = 0;
                            foreach ($kegiatans as $value): ?>
                                <?php if($tempUnit != $value['unit_id']): ?>
                                    <?php if($pagu > 0): ?>
                                        <tr>
                                            <td colspan="3"></td>
                                        <!--  <td>Rp.<b><?php echo number_format($pagu, 2, ',', '.') ?></b></td> -->
                                            <td>Rp.<b><?php echo number_format($pagu + $tambahan, 2, ',', '.') ?></b></td>
                                            <td>Rp.<b><?php echo number_format($anggaran, 2, ',', '.') ?></b></td>
                                            <td>Rp.<b><?php echo number_format($pagu + $tambahan - $anggaran, 2, ',', '.') ?></b></td>
                                            <!-- <td>Rp.<b><?php echo number_format($pagu - $pagu + $tambahan, 2, ',', '.') ?></b></td> -->
                                        </tr>
                                        <?php
                                        $anggaran = 0;
                                        $tambahan = 0;
                                        $pagu = 0;
                                        ?>
                                    <?php endif; ?>
                                    <?php if(!empty($value['unit_id'])): ?>
                                        <tr><td colspan="8">&nbsp;</td></tr>
                                        <tr>
                                            <td colspan="8" class="text-left text-bold"><?php echo $value['unit_id'] . ' - ' . $value['unit_name'] ?></td>
                                        </tr>
                                    <?php endif; ?>
                                <?php endif; ?>

                                <?php
                                if($value['pagu'] + $value['tambahan_pagu'] != $value['nilai_anggaran'])
                                    $style = "background: pink;";
                                else
                                    $style = "";
                                ?>

                                <?php
                                if($value['status_level'] == 0)
                                    $level = 'Entri';
                                else if($value['status_level'] == 1)
                                    $level = 'PPTK';
                                else if($value['status_level'] == 2)
                                    $level = 'KPA';
                                else if($value['status_level'] == 3)
                                    $level = 'PA I';
                                else if($value['status_level'] == 4)
                                    $level = 'Tim Anggaran';
                                else if($value['status_level'] == 5)
                                    $level = 'PA II';
                                else if($value['status_level'] == 6)
                                    $level = 'Penyelia II';
                                else if($value['status_level'] == 7)
                                    $level = 'RKA';

                                if($value['kunci'] == 1 || $value['kunci'] == 2)
                                    $kunci = 'DINAS';
                                else
                                    $kunci = 'PENELITI';
                                ?>

                                <?php if(!empty($value['unit_id'])): ?>
                                    <tr style="<?php echo $style ?>">
                                        <td>&nbsp;</td>
                                        <td class="text-left" ><?php echo $value['kegiatan_id'] ?></td>
                                        <td class="text-left" ><?php echo $value['kode_kegiatan'] ?></td>
                                        <td class="text-left" ><?php echo $value['nama_kegiatan'] ?></td>
                                        <!-- <td class="text-left" >Rp.<?php echo number_format($value['pagu'], 0, ',', '.') ?></td> -->
                                        <td class="text-left" >Rp.<?php echo number_format($value['pagu'] + $value['tambahan_pagu'], 0, ',', '.') ?></td>
                                        <td class="text-left" >Rp.<?php echo number_format($value['nilai_anggaran'], 0, ',', '.') ?></td>
                                        <td class="text-left" >Rp.<?php echo number_format($value['pagu'] + $value['tambahan_pagu'] - $value['nilai_anggaran'], 0, ',', '.') ?></td>
                                        <!-- <td class="text-left" >Rp.<?php echo number_format($value['pagu'] - $value['pagu'] + $value['tambahan_pagu'], 0, ',', '.') ?></td> -->
                                        <td class="text-left" ><?php echo $kunci ?></td>
                                        <td class="text-left" ><?php echo $level ?></td>
                                    </tr>
                                    <?php
                                    $tempUnit = $value['unit_id'];
                                    $anggaran += $value['nilai_anggaran'];
                                    $tambahan += $value['tambahan_pagu'];
                                    $pagu += $value['pagu'];
                                    $anggaran_kota += $value['nilai_anggaran'];
                                    $tambahan_kota += $value['tambahan_pagu'];
                                    $pagu_kota += $value['pagu']; ?>
                                <?php endif; ?>
                            <?php endforeach; ?>
                            <!-- <tr><td colspan="8"><b>TOTAL RAPBD BUKU PUTIH = Rp.<?php echo number_format($pagu_kota, 2, ',', '.') ?></b></td></tr> -->
                            <tr><td colspan="8"><b>TOTAL Pagu = Rp.<?php echo number_format($pagu_kota + $tambahan_kota, 2, ',', '.') ?></b></td></tr>
                            <tr><td colspan="8"><b>TOTAL TERENTRI = Rp.<?php echo number_format($anggaran_kota, 2, ',', '.') ?></b></td></tr>
                            <tr><td colspan="8"><b>SELISIH (Terentri)= Rp.<?php echo number_format($pagu_kota + $tambahan_kota - $anggaran_kota, 2, ',', '.') ?></b></td></tr>
                            <!-- <tr><td colspan="8"><b>SELISIH (Pagu)= Rp.<?php echo number_format($pagu_kota - $pagu_kota + $tambahan_kota, 2, ',', '.') ?></b></td></tr> -->
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
                    


<!-- <div class="box box-info">
    <div class="box-header with-border">
        
    </div>
    <div class="box-body">
        
    </div>
</div> -->