<?php use_helper('I18N', 'Date', 'Object', 'Javascript', 'Validation') ?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Log Survey</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Survey SHS</a></li>
                    <li class="breadcrumb-item active">Log Survey</li>
                </ol>
            </div>
        </div>
    </div>
</section>
<!-- Main content -->
<section class="content">
    <?php include_partial('survey/messages'); ?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body table-responsive p-0">
                        <table class="table table-hover">
                            <thead class="head_peach">
                                <tr>
                                    <th>UserID</th>
                                    <th>Nama</th>
                                    <th>Waktu</th>
                                    <th>Deskripsi</th>
                                    <th>IP Address</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                        foreach ($pager->getResults() as $log):
                            $user_id = $log->getUsername();
                            $c = new Criteria();
                            $c->add(MasterUserV2Peer::USER_ID, $user_id);
                            $rs_nama = MasterUserV2Peer::doSelectOne($c);
                            $nama = $rs_nama->getUserName();

                            $unit_id = $log->getUnitId();
                            $c = new Criteria();
                            $c->add(UnitKerjaPeer::UNIT_ID, $unit_id);
                            $rs_unitkerja = UnitKerjaPeer::doSelectOne($c);
                            ?>
                                <tr>
                                    <td><?php echo $user_id ?></td>
                                    <td><?php echo $nama ?></td>
                                    <td><?php echo $log->getTimeAct() ?></td>
                                    <td><?php echo $log->getDescription() ?></td>
                                    <td><?php echo $log->getIp() ?></td>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer clearfix">
                        <ul class="pagination pagination-sm m-0 float-left">
                            <?php echo format_number_choice('[0] no result|[1] 1 result|(1,+Inf] %1% results', array('%1%' => $pager->getNbResults()), $pager->getNbResults()); ?>
                        </ul>
                        <ul class="pagination pagination-sm m-0 float-right">
                            <?php
                            if ($pager->haveToPaginate()):
                                echo '<li class="page-item">'.link_to('Previous', 'report/logSurvey?page=' . $pager->getPreviousPage(), array('class' => 'page-link', 'align' => 'absmiddle', 'alt' => __('Previous'), 'title' => __('Previous'))).'</li>';

                                foreach ($pager->getLinks() as $page):
                                        echo '<li class="page-item">'.link_to_unless($page == $pager->getPage(), $page, "report/logSurvey?page={$page}", array('class' => 'page-link')).'</li>';
                                endforeach;
                                echo '<li class="page-item">'.link_to('Next', 'report/logSurvey?page=' . $pager->getNextPage(), array('class' => 'page-link', 'align' => 'absmiddle', 'alt' => __('Next'), 'title' => __('Next'))).'</li>'; 
                                endif;
                            ?>
                        </ul>
                    </div> 
                </div>
            </div>
        </div>
    </div>
</section><!-- /.content -->