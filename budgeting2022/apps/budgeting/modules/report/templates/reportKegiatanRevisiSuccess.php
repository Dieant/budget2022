<?php use_helper('Form', 'Object', 'Javascript') ?>
<div class="box box-info">        
    <div class="box-header with-border">
        Pilih SKPD
    </div>
    <div class="box-body">
        <?php
        $e = new Criteria();
        $e->add(UnitKerjaPeer::UNIT_ID, '9999', Criteria::NOT_IN);
        $e->addAscendingOrderByColumn(UnitKerjaPeer::UNIT_NAME);
        $unit_kerja = UnitKerjaPeer::doSelect($e);
        echo select_tag('unit_id', objects_for_select($unit_kerja, 'getUnitId', 'getUnitName', (isset($filter_unit_kerja) ? $filter_unit_kerja : NULL), array('include_custom' => '------Pilih Dinas------')), array('id' => 'unit_id', 'class' => 'form-control'));
        ?>
    </div>
</div>
<div class="box box-info">        
    <div class="box-header with-border">
        Laporan Per Dinas
    </div>
    <div class="box-body">
        <table class="table table-bordered">
            <tr>
                <th class="text-center">Kode Dinas</th>
                <th class="text-center">Dinas</th>                
                <th class="text-center">Kode Kegiatan</th>
                <th class="text-center">Nama Kegiatan</th>                
            </tr>
            <?php            
            $i = 0;
            $tempSub ='';
            $tempSub1 =''; 
            $temps='';           
            $rs->first();            
            do {
                  if ($temps != $unit_id[$i].'.'.$kode_kegiatan[$i]){?>
                <tr>   
                    <td class="text-left"><?php echo $unit_id[$i] ?></td>
                    <td class="text-left"><?php echo $unit_name[$i] ?></td>
                    <td class="text-center"><?php echo $kode_kegiatan[$i] ?></td>
                    <td class="text-left"><?php echo $nama_kegiatan[$i] ?></td>                           
                </tr>
                 <?php }                                                                             
                    $tempSub  = $kode_kegiatan[$i];
                    $tempSub1 = $unit_id[$i];
                    $temps= $unit_id[$i].'.'.$kode_kegiatan[$i];             
                   ?>                               
                <?php $i++; ?>
                <?php
            } while ($rs->next());
            ?>
           
        </table>       
    </div>
</div>
<script>
    $("#unit_id").change(function () {
        $('#indicator').show();
        var id = $(this).val();
        if (id == '') {
            id = '0000';
        }
        var tahap = "<?php echo $tahap; ?>";
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/report/reportKegiatanRevisi/unit_id/" + id + ".html",
            context: document.body
        }).done(function (msg) {
            $('#indicator').hide();
            $('#isi').html(msg);
        });

    });
</script>