<?php use_helper('Form', 'Object', 'Javascript') ?>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="fas fa-align-left"></i> Laporan Pagu KUA PPAS & APBD
                        </h3>
                    </div>
                    <div class="card-body"> 
                        <table class="table table-bordered">
                            <tr>
                                <th class="text-center">Dinas</th>
                                <th class="text-center">Kode Kegiatan</th>
                                <th class="text-center">Nama Kegiatan</th>
                                <th class="text-center">Pagu KUA (Buku Putih)</th>
                                <th class="text-center">Tambahan Pagu</th>
                                <!-- <th class="text-center">Pagu RAPBD</th> -->
                                <!--  <th class="text-center">Pagu APBD (Buku Biru)</th> -->
                                <!-- <th class="text-center">Selisih (Pagu KUA - Pagu APBD)</th> -->
                                <th class="text-center">Pagu Rincian (Terentri)</th>
                                <th class="text-center">Selisih (Pagu KUA - Terentri)</th>
                                <th class="text-center">Posisi Approval</th>
                                
                            </tr>
                            <?php
                            $tempUnit = '';
                            $anggaran = 0;
                            $entri = 0;
                            $pagu = 0;
                            $pagu_pak = 0;
                            $pagu_tambahan = 0;
                            $entri_kota = 0;
                            $pak_kota = 0;
                            $pak_tambahan = 0;
                            $pagu_kota = 0;
                            foreach ($kegiatans as $value): ?>
                                <?php if($tempUnit != $value['unit_id']): ?>
                                    <?php if($pagu > 0): ?>
                                        <tr>
                                            <td colspan="3"></td>
                                            <td class="text-right"><b><?php echo number_format($pagu_pak, 0, ',', '.') ?></b></td>
                                            <td>Rp.<b><?php echo number_format($pagu_tambahan, 0, ',', '.') ?></b></td>
                                            <!-- <td>Rp.<b><?php echo number_format($pagu_pak, 0, ',', '.') ?></b></td> -->
                                            <!-- td>Rp.<b><?php echo number_format($pagu_pak + $pagu_tambahan, 0, ',', '.') ?></b></td> -->
                                            <!-- <td>Rp.<b><?php echo number_format($pagu_tambahan, 0, ',', '.') ?></b></td> -->
                                            <td class="text-right"><b><?php echo number_format($entri, 0, ',', '.') ?></b></td>
                                            <td class="text-right"><b><?php echo number_format($pagu_pak + $pagu_tambahan - $entri, 0, ',', '.') ?></b></td>
                                        </tr>
                                        <?php
                                        $anggaran = 0;
                                        $entri = 0;
                                        $pagu = 0;
                                        $pagu_pak = 0;
                                        $pagu_tambahan = 0;
                                        ?>
                                    <?php endif; ?>
                                    <?php if(!empty($value['unit_id'])): ?>
                                        <tr><td colspan="8">&nbsp;</td></tr>
                                        <tr>
                                            <td colspan="8" class="text-left text-bold"><?php echo $value['unit_id'] . ' - ' . $value['unit_name'] ?></td>
                                        </tr>
                                    <?php endif; ?>
                                <?php endif; ?>

                                <?php
                                if($value['anggaran_pagu'] + $value['tambahan_pagu'] != $value['entri'])
                                    $style = "background: pink;";
                                else
                                    $style = "";
                                ?>
                        

                                <?php if(!empty($value['unit_id'])): ?>
                                    <tr style="<?php echo $style ?>">
                                        <td>&nbsp;</td>
                                        <td class="text-left" ><?php echo $value['kode_kegiatan'] ?></td>
                                        <td class="text-left" ><?php echo $value['nama_kegiatan'] ?></td>
                                    <!--  <td class="text-left" >Rp.<?php echo number_format($value['anggaran'], 0, ',', '.') ?></td> -->
                                        <td class="text-right" ><?php echo number_format($value['anggaran_pagu'], 0, ',', '.') ?></td> 
                                    <!--  <td class="text-left" >Rp.<?php echo number_format($value['anggaran_pagu'] + $value['tambahan_pagu'], 0, ',', '.') ?></td> -->
                                        <!--<td class="text-left" >Rp.<?php echo number_format($value['tambahan_pagu'], 0, ',', '.') ?></td> -->
                                    <td class="text-left" >Rp.<?php echo number_format($value['tambahan_pagu'], 0, ',', '.') ?></td>
                                    <td class="text-right" ><?php echo number_format($value['entri'], 0, ',', '.') ?></td>
                                    <td class="text-right" ><?php echo number_format($value['anggaran_pagu'] + $value['tambahan_pagu'] - $value['entri'], 0, ',', '.') ?></td>
                                    <td class="text-left" ><?php echo $value['posisi'] ?></td>
                                        
                                    </tr>
                                    <?php
                                    $tempUnit = $value['unit_id'];
                                    $entri_kota += $value['entri'];
                                    $pagu_kota += $value['anggaran'];
                                    $pak_kota += $value['anggaran_pagu'];
                                    $pak_tambahan += $value['tambahan_pagu'];
                                    $pagu += $value['anggaran'];
                                    $pagu_pak += $value['anggaran_pagu'];
                                    $pagu_tambahan += $value['tambahan_pagu'];
                                    $entri += $value['entri'];
                                    ?>
                                <?php endif; ?>
                            <?php endforeach; ?>
                            <tr><td colspan="8"><b>TOTAL TERENTRI = Rp.<?php echo number_format($entri_kota, 0, ',', '.') ?></b></td></tr>
                            <tr><td colspan="8"><b>Pagu KUA (Buku Putih) = Rp.<?php echo number_format($pak_kota, 0, ',', '.') ?></b></td></tr>
                            <tr><td colspan="8"><b>Pagu Tambahan = Rp.<?php echo number_format($pak_tambahan, 0, ',', '.') ?></b></td></tr>
                            <!-- <tr><td colspan="8"><b>Pagu RAPBD = Rp.<?php echo number_format($pak_kota, 0, ',', '.') ?></b></td></tr> -->
                            <!-- <tr><td colspan="8"><b>Pagu APBD (Buku Biru) = Rp.<?php echo number_format($pak_kota + $pak_tambahan, 0, ',', '.') ?></b></td></tr> -->
                        <!--  <tr><td colspan="8"><b>SELISIH (Pagu APBD-KUA)  = Rp.<?php echo number_format($pak_tambahan, 0, ',', '.') ?></b></td></tr> -->
                            <tr><td colspan="8"><b>SELISIH Entrian (Pagu KUA - Entrian) = Rp.<?php echo number_format($pak_kota - $entri_kota, 0, ',', '.') ?></b></td></tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<!-- <div class="box box-info">
    <div class="box-header with-border">
        
    </div>
    <div class="box-body">
        
    </div>
</div> -->