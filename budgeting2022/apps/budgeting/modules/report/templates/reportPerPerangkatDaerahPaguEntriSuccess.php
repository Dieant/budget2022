<div class="box box-info">        
    <div class="box-header with-border">
        Laporan Per Dinas Pagu - Entri
    </div>
    <div class="box-body table-responsive">
        <table class="table table-bordered">
            <tr>
                <th class="text-center">Dinas</th>
                <th class="text-center">Pagu</th>
                <th class="text-center">Tambahan Pagu</th>
                <th class="text-center">Jumlah Kegiatan</th>
                <th class="text-center">Nilai Entrian</th>
                <th class="text-center">Pagu - Entrian</th>
                <th class="text-center">Persentase</th>
            </tr>
            <?php
            $i = 0;
            $rs->first();
            do {
                ?>
                <tr>
                    <td class="text-left"><?php echo $rs->getString('unit_id') . ' - ' . $rs->getString('unit_name') ?></td>
                    <td class="text-right"><?php echo $pagu1[$i] ?></td>
                    <td class="text-right"><?php echo $pagu2[$i] ?></td>
                    <td class="text-center"><?php echo $rs->getString('total_draft') ?></td>
                    <td class="text-right"><?php echo $nilai_usulan[$i] ?></td>
                    <td class="text-right"><?php echo $selisih[$i] ?></td>
                    <td class="text-right"><?php echo $persen_entrian[$i] ?></td>

                </tr>
                <?php $i++; ?>
                <?php
            } while ($rs->next());
            ?>
            <tr>
                <td class="text-center text-bold bg-green-active">Total</td>
                <td class="text-right text-bold bg-green-active"><?php echo $total1_pagu ?></td>
                <td class="text-right text-bold bg-green-active"><?php echo $total2_pagu ?></td>
                <td class="text-center text-bold bg-green-active"><?php echo $total_draft2 ?></td>
                <td class="text-right text-bold bg-green-active"><?php echo $nilai_draft2 ?></td>
                <td class="text-right text-bold bg-green-active"><?php echo $total_selisih ?></td>
                <td class="text-right text-bold bg-green-active"><?php $persen=($nilai_draft2/$total_pagu)*100; echo $tampil = number_format($persen, 3, ",", "."); echo' %' ?></td>
            </tr>
        </table>       
    </div>
</div>