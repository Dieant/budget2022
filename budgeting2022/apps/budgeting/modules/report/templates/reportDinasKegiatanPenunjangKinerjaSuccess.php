<div class="box box-info">        
    <div class="box-header with-border">
        Laporan Per Dinas Per Kegiatan Penunjang kerja
    </div>
    <div class="box-body">
        <table class="table table-bordered">
            <tr>
                <th class="text-center" style="width: 10px"></th>
                <th class="text-center">Kode Kegiatan</th>
                <th class="text-center">Kegiatan</th>
                <th class="text-center">Pagu</th>
                <th class="text-center">Nilai Usulan</th>
                <th class="text-center">Nilai Disetujui</th>
                <th class="text-center">Catatan</th>
            </tr>
            <?php
            $i = 0;
            if ($rs->first() <> '') {
                $rs->first();
                do {
                    ?>
                    <?php if (isset($dinas_ok[$i])): ?>
                        <tr><td colspan="7"    >&nbsp;</td></tr>
                        <tr>
                            <td colspan="3" align="left"  ><strong><?php echo $unit_id[$i] . ' - ' . $unit_name[$i] ?></strong></td>
                            <td align="right"  ><strong><?php echo $nilai_draft[$i] ?></strong></td>
                            <td align="right"  ><strong><?php echo $nilai_usulan_unit[$i] ?></strong></td>
                            <td align="right"  ><strong><?php echo $nilai_locked[$i] ?></strong></td>
                            <td align="right"  ><strong>&nbsp;</strong></td>
                        </tr>
                    <?php endif ?>
                    <tr>
                        <td  style="width: 10px"></td>
                        <td  align="left"  ><?php echo $rs->getString('kegiatan_id') ?></td>
                        <td align="left"  ><?php echo $rs->getString('nama_kegiatan') ?></td>
                        <td align="right"  ><?php echo $nilai_draft3[$i] ?></td>
                        <td align="right"  ><?php echo $nilai_usulan_kegiatan[$i] ?></td>
                        <td align="right"  ><?php echo $nilai_locked3[$i] ?></td>
                        <td align="justify"  ><?php echo $rs->getString('catatan') ?></td>
                    </tr>
                    <?php
                    $i++;
                } while ($rs->next());
            }
            ?>
            <tr>
                <td colspan="3"  ><strong>Total</strong></td>
                <td align="right"  ><strong><?php echo $nilai_draft2 ?></strong></td>
                <td align="right"  ><strong><?php echo $nilai_draft4 ?></strong></td>
                <td align="right"  ><strong><?php echo $nilai_locked2 ?></strong></td>
                <td align="right"  ><strong>&nbsp;</strong></td>
            </tr>
        </table>       
    </div>
</div>