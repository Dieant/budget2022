<div class="box box-info">
    <div class="box-header with-border">
        Laporan Rekening Alat Tulis Kantor Per Dinas
    </div>
    <div class="box-body">
        <table class="table table-bordered">
            <tr>
                <th class="text-center">SKPD</th>
                <th class="text-center">Pagu</th>
                <th class="text-center">Nilai</th>
                <th class="text-center">Selisih</th>
            </tr>
            <?php
            foreach ($dinas as $value):
                ?>
                <tr>
                    <td class="text-left" ><?php echo $value['unit_name'] ?></td>
                    <td class="text-right" ><?php echo number_format($value['pagu'], 0, ',', '.') ?></td>
                    <td class="text-right" ><?php echo number_format($value['nilai'], 0, ',', '.') ?></td>
                    <td class="text-right" ><?php echo number_format(($value['pagu'] - $value['nilai']), 0, ',', '.') ?></td>
                </tr>
            <?php endforeach; ?>
            <tr>
                <td class="text-center"><b> TOTAL </b></td>
                <td class="text-right"><b> <?php echo number_format($totalPagu, 0, ',', '.'); ?> </b></td>
                <td class="text-right"><b> <?php echo number_format($totalNilai, 0, ',', '.'); ?> </b></td>
                <td class="text-right"><b> <?php echo number_format($totalPagu - $totalNilai, 0, ',', '.'); ?> </b></td>
            </tr>
        </table>
    </div>
</div>