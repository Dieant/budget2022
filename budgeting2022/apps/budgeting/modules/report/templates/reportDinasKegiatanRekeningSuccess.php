<?php use_helper('Form', 'Object', 'Javascript') ?>
<div class="box box-info">        
    <div class="box-header with-border">
        Pilih SKPD
    </div>
    <div class="box-body">
        <?php
        $e = new Criteria();
        $e->add(UnitKerjaPeer::UNIT_ID, '9999', Criteria::NOT_IN);
        $e->addAscendingOrderByColumn(UnitKerjaPeer::UNIT_NAME);
        $unit_kerja = UnitKerjaPeer::doSelect($e);
        echo select_tag('unit_id', objects_for_select($unit_kerja, 'getUnitId', 'getUnitName', $filter_unit_kerja, array('include_custom' => '------Pilih Dinas------')), array('id' => 'unit_id', 'class' => 'form-control'));
        ?>
    </div>
</div>
<div class="box box-info">        
    <div class="box-header with-border">
        Laporan Per Dinas Per Kegiatan Per Rekening
    </div>
    <div class="box-body">
        <table class="table table-bordered">
            <tr>
                <th class="text-center">Dinas</th>
                <th class="text-center" width="50px">Kegiatan</th>
                <th class="text-center">Kode Rekening</th>
                <th class="text-center">Nama Rekening</th>
                <th class="text-center">Nilai</th>
            </tr>
            <?php
            foreach ($rekening as $value):
                if (isset($value['skpd_ok'])):
                    ?>
                    <tr><td colspan="5">&nbsp;</td></tr>
                    <tr>
                        <td colspan="5" class="text-left text-bold"><?php echo $value['unit_id'] . ' - ' . $value['unit_name'] ?></td>
                    </tr>
                <?php endif; ?>
                <tr>
                    <?php if (isset($value['kegiatan_ok'])): ?>
                        <td rowspan="<?php echo $value['span']; ?>">&nbsp;</td>
                        <td class="text-left" rowspan="<?php echo $value['span']; ?>" style="word-wrap: break-word"><?php echo $value['kode_kegiatan'] . ' - ' . $value['nama_kegiatan'] ?></td>
                    <?php endif; ?>
                    <td class="text-left" ><?php echo $value['rekening_code'] ?></td>
                    <td class="text-left" ><?php echo $value['rekening_name'] ?></td>
                    <td class="text-right" ><?php echo number_format($value['nilai'], 0, ",", ".") ?></td>
                </tr>
            <?php endforeach; ?>
        </table>       
    </div>
</div>
<script>
    $("#unit_id").change(function () {
        $('#indicator').show();
        var id = $(this).val();
        if (id == '') {
            id = '0000';
        }
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/report/reportDinasKegiatanRekening/unit_id/" + id + ".html",
            context: document.body
        }).done(function (msg) {
            $('#indicator').hide();
            $('#isi').html(msg);
        });

    });
</script>