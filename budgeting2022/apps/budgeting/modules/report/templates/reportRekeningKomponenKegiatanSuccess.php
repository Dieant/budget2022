<div class="box box-info">        
    <div class="box-header with-border">
        Laporan Per Rekening Per Komponen Per Kegiatan
    </div>
    <div class="box-body">
        <table class="table table-bordered">
            <tr>
                <th class="text-center">Komponen</th>
                <th class="text-center">SKPD-Kegiatan</th>
                <th class="text-center">Nilai Usulan</th>
                <th class="text-center">Nilai Disetujui</th>
            </tr>
            <?php
            $i = 0;
            $rs->first();
            do {
                ?>
                <?php if (isset($dinas_ok[$i])): ?>
                    <tr><td colspan="4"></td></tr>
                    <tr>
                        <td colspan="2" class="text-left text-bold"><?php echo $rekening_code[$i] . ' - ' . $rekening_name[$i] ?></td>
                        <td class="text-right text-bold"><?php echo $nilai_draft[$i] ?></td>
                        <td class="text-right text-bold"><?php echo $nilai_locked[$i] ?></td>
                    </tr>
                <?php endif ?>
                <tr>
                    <td class="text-left" colspan="2"><?php echo isset($komponen_name[$i]) ? $komponen_name[$i] : '' ?></td>
                    <td class="text-right"><?php echo isset($nilai_draft3[$i]) ? $nilai_draft3[$i] : '' ?></td>
                    <td class="text-right"><?php echo isset($nilai_locked3[$i]) ? $nilai_locked3[$i] : '' ?></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td colspan="3"><?php echo isset($units[$i]) ? $units[$i] : '' ?></td>
                </tr>
                <?php
                $i++;
            }while ($rs->next());
            ?>        
            <tr>                
                <td class="text-right text-bold bg-green-active" colspan="2">Total</td>
                <td class="text-right text-bold bg-green-active"><?php echo $nilai_draft2 ?></td>
                <td class="text-right text-bold bg-green-active"><?php echo $nilai_locked2 ?></td>
            </tr>            
        </table>       
    </div>
</div>