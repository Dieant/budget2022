<?php use_helper('Javascript', 'Number') ?>
<?php
//echo "Print RKA melalui SABK ".link_to('(http://sabk.surabaya.go.id)','http://sabk.surabaya.go.id', array('target'=>'_blank')).". Tekan tombol BACK untuk kembali";exit;
//echo use_stylesheet('/css/tampilan_print2.css');
$kode_kegiatan = $sf_params->get('kode_kegiatan');
$unit_id = $sf_params->get('unit_id');

//total_anggaran_sebelum
$query_tot_awal = "select sum(nilai_anggaran) as total 
	from " . sfConfig::get('app_default_schema') . ".prev_rincian_detail 
	where kegiatan_code = '" . $kode_kegiatan . "' and unit_id = '" . $unit_id . "' and status_hapus = false";
$con = Propel::getConnection();
$stmt_tot_awal = $con->prepareStatement($query_tot_awal);
$rs_tot_awal = $stmt_tot_awal->executeQuery();
while ($rs_tot_awal->next()) {
    $total_sebelum = $rs_tot_awal->getInt('total');
}
//total_anggaran_sebelum
//total_anggaran_setelah
$query_tot_awal = "select sum(nilai_anggaran) as total 
	from " . sfConfig::get('app_default_schema') . ".rincian_detail 
	where kegiatan_code = '" . $kode_kegiatan . "' and unit_id = '" . $unit_id . "' and status_hapus = false";
$con = Propel::getConnection();
$stmt_tot_awal = $con->prepareStatement($query_tot_awal);
$rs_tot_awal = $stmt_tot_awal->executeQuery();
while ($rs_tot_awal->next()) {
    $total_setelah = $rs_tot_awal->getInt('total');
}
//total_anggaran_setelah
//query ambil data sekarang
$query = "select mk.kode_kegiatan, mk.nama_kegiatan, mk.alokasi_dana, mk.kode_program,
	mk.kode_program2, mk.benefit, mk.impact, mk.kode_program2, mk.kode_misi, mk.kode_tujuan,
	mk.outcome, mk.target_outcome, mk.catatan, mk.ranking,uk.unit_name, mk.kode_urusan,
	uk.kode_permen,mk.output, mk.kelompok_sasaran
	from " . sfConfig::get('app_default_schema') . ".master_kegiatan mk,unit_kerja uk
	where mk.kode_kegiatan = '" . $kode_kegiatan . "' and mk.unit_id = '" . $unit_id . "'
	and uk.unit_id=mk.unit_id";
$con = Propel::getConnection();
$stmt = $con->prepareStatement($query);
$rs = $stmt->executeQuery();
$ranking = '';
while ($rs->next()) {
    $kegiatan_kode = $rs->getString('kode_kegiatan');
    $nama_kegiatan = $rs->getString('nama_kegiatan');
    $alokasi = $rs->getString('alokasi_dana');
    $organisasi = $rs->getString('unit_name');
    $benefit = $rs->getString('benefit');
    $impact = $rs->getString('impact');
    $kode_program = $rs->getString('kode_program');
    $program_p_13 = $rs->getString('kode_program2');
    //$urusan = $rs->getString('urusan');
    $kode_misi = $rs->getString('kode_misi');
    $kode_tujuan = '';
    if ($rs->getString('kode_tujuan')) {
        $kode_tujuan = $rs->getString('kode_tujuan');
    }

    $outcome = $rs->getString('outcome');
    $target_outcome = $rs->getString('target_outcome');
    $catatan = $rs->getString('catatan');
    $ranking = $rs->getInt('ranking');
    $kode_organisasi = $rs->getString('kode_urusan');
    $kode_program2 = $rs->getString('kode_program2');
    $kode_permen = $rs->getString('kode_permen');
    $output = $rs->getString('output');
    $kelompok_sasaran = $rs->getString('kelompok_sasaran');
    $kode_per = explode(".", $kode_organisasi);
}
//query ambil data sekarang
?>
<table style="width: 100%; border: 1" cellspacing="0">
    <tr>
        <td style="border: 1" colspan="11">
            <div style="border: 1">
                <center><span class="style1 font12"><strong><span class="style3" style="font-size: smaller">RINCIAN PENYESUAIAN RAPBD <?php echo sfConfig::get('app_tahun_default'); ?></span><br></strong></span></center>
            </div>        
            <div style="border: 1">
                <center><span class="font12 style1 style2"><span class="font12 style3 style1" style="font-size: smaller"><strong>SATUAN KERJA PERANGKAT DAERAH </strong></span></span></center>
            </div>
        </td>
        <td style="border: 1" rowspan="2" colspan="4">
            <div class="style3" style="font-size: smaller">
                <center><strong>Formulir<br>RKA-SKPD 2.2.1 </strong></center>
            </div>
        </td>
    </tr>   
    <tr>
        <td colspan="15" style="border: 1">
            <div style="font-size: smaller">
                <center><span class="style3">KOTA SURABAYA <br></span></center>
            </div>
            <div >
                <center><span class="style3" style="font-size: smaller">TAHUN ANGGARAN <?php echo sfConfig::get('app_tahun_default'); ?></span></center>
            </div>
        </td>
    </tr>
    <tr>
        <td colspan="15" style="border: 1">
            <table>
                <tr align="left" valign="top">
                    <td nowrap="nowrap" width="150">
                        <span class="style3" style="font-size: smaller">Urusan Pemerintahan </span>
                    </td>
                    <?php
                    $kode_urusan_parameter = $kode_per[0] . '.' . $kode_per[1];

                    $queryU = "select * 
                        from " . sfConfig::get('app_default_schema') . ".master_urusan mu 
                            where mu.kode_urusan='" . $kode_urusan_parameter . "'";
                    $con = Propel::getConnection();
                    $stmt = $con->prepareStatement($queryU);
                    $rsU = $stmt->executeQuery();
                    while ($rsU->next()) {
                        $nama_urusan = $rsU->getString('nama_urusan');
                    }
                    ?>
                    <td width="10"><span class="style3" style="font-size: smaller">:</span></td>
                    <td ><span class="style3" style="font-size: smaller"><?php echo $kode_urusan_parameter . ' ' . $nama_urusan ?></span></td>
                </tr>
                <?php
                $unitKerja = new Criteria();
                $unitKerja->add(UnitKerjaPeer::UNIT_ID, $unit_id);
                $uk = UnitKerjaPeer::doSelectOne($unitKerja);
                ?>
                <tr align="left" valign="top">
                    <td><span class="style3" style="font-size: smaller">Unit Kerja</span></td>
                    <td><span class="style3" style="font-size: smaller">:</span></td>
                    <td><span class="style3" style="font-size: smaller"><?php echo $unit_id . ' ' . $uk->getUnitName() ?></span></td>
                </tr>
                <tr align="left" valign="top">
                    <td><span class="style3" style="font-size: smaller">Kegiatan</span></td>
                    <td><span class="style3" style="font-size: smaller">:</span></td>
                    <td><span class="Font8v style3" style="font-size: smaller"><?php echo $kegiatan_kode . ' ' ?></span><span class="Font8v style3" style="font-size: smaller"><?php echo $nama_kegiatan ?></span></td>
                </tr>
                <?php
                $query = "select *
			from " . sfConfig::get('app_default_schema') . ".master_program2 ms
			where ms.kode_program2='" . $kode_program2 . "'";
                $con = Propel::getConnection();
                $stmt = $con->prepareStatement($query);
                $rs1 = $stmt->executeQuery();
                while ($rs1->next()) {
                    $nama_program2 = $rs1->getString('nama_program2');
                }
                ?>
                <tr align="left" valign="top">
                    <td><span class="style3" style="font-size: smaller">Program</span></td>
                    <td><span class="style3" style="font-size: smaller">:</span></td>
                    <td><span class="Font8v style3" style="font-size: smaller"><?php echo $kode_program2 . ' ' . $nama_program2 ?></span></td>
                </tr>
                <?php
                $d = new Criteria();
                $d->add(MasterProgram2Peer::KODE_PROGRAM, $kode_program);
                $d->add(MasterProgram2Peer::KODE_PROGRAM2, $program_p_13);
                $dx = MasterProgram2Peer::doSelectOne($d);
                if ($dx) {
                    $program2 = $dx->getNamaProgram2();
                } else {
                    $program2 = substr($kode_per[0] . '.' . $kode_per[1], 0, 4) . '.' . $kode_program;
                }
                ?>
                <tr align="left" valign="top">
                    <td><span class="style3" style="font-size: smaller">Program P13</span></td>
                    <td><span class="style3" style="font-size: smaller">:</span></td>
                    <td><span class="style3" style="font-size: smaller"><?php echo $program2 ?> </span></td>
                </tr>
                <tr>
                    <td><span class="style3" style="font-size: smaller">Sasaran Kegiatan</span></td>
                    <td><span class="style3" style="font-size: smaller">:</span></td>
                    <td><span class="style3" style="font-size: smaller"><?php echo $kelompok_sasaran ?></span></td>
                </tr>
                <tr class="sf_admin_row_0">
                    <td align='left' width='20%' valign='top'>Target</td>
                    <td width='1%' align='center' valign='top'>:</td>
                    <td valign='top'>
                        <table border="1" width="100%" cellspacing="0">
                            <tr align='center'>
                                <td align='center'>Subtitle</td>
                                <td align='center'>Semula</td>
                                <td align='center'>Menjadi</td>
                            </tr>
                            <?php
                            $c = new Criteria();
                            $c->add(SubtitleIndikatorPeer::KEGIATAN_CODE, $kode_kegiatan);
                            $c->add(SubtitleIndikatorPeer::UNIT_ID, $unit_id);
                            $c->addAscendingOrderByColumn(SubtitleIndikatorPeer::SUBTITLE);
                            $cs = SubtitleIndikatorPeer::doSelect($c);
                            $subtitle = '';
                            foreach ($cs as $css) {
                                if ($subtitle != $css->getSubtitle()) {
                                    $subtitle = $css->getSubtitle();
                                    ?>
                                    <tr>
                                        <td align='justify' style="padding: 2px"><?php echo $css->getSubtitle(); ?></td>
                                        <td align='justify' style="padding: 2px">
                                            <?php
                                            $pd = new Criteria();
                                            $pd->add(PrevSubtitleIndikatorPeer::KEGIATAN_CODE, $kode_kegiatan);
                                            $pd->add(PrevSubtitleIndikatorPeer::UNIT_ID, $unit_id);
                                            $pd->add(PrevSubtitleIndikatorPeer::SUBTITLE, $subtitle);
                                            $pds = PrevSubtitleIndikatorPeer::doSelectOne($pd);
                                            if ($pds) {
                                                echo $pds->getIndikator() . '&nbsp;' . $pds->getNilai() . '&nbsp;' . $pds->getSatuan();
                                            }
                                            ?>
                                        </td>
                                        <td align='justify' style="padding: 2px">
                                            <?php echo $css->getIndikator() . '&nbsp;' . $css->getNilai() . '&nbsp;' . $css->getSatuan() ?>
                                        </td>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>

                        </table>
                    </td>
                </tr>
                <tr>
                    <td align='left' width='20%' valign='top'>Nilai</td>
                    <td width='1%' align='center' valign='top'>:</td>
                    <td valign='top'>
                        <table border="1" width="100%" cellspacing="0">
                            <tr align='center'>
                                <td style="width: 40%">&nbsp;</td>
                                <td style="width: 30%; text-align: center">Semula</td>
                                <td style="width: 30%; text-align: center">Menjadi</td>
                            </tr>
                            <?php
//awal-comment
                            $query = " select nilai_sekarang.belanja_name, sum(hasil_kali2) as menjadi, sum(hasil_kali1) as semula
from
	( select k.belanja_name, sum(detail2.nilai_anggaran) as hasil_kali2
		from " . sfConfig::get('app_default_schema') . ".rincian_detail detail2," . sfConfig::get('app_default_schema') . ".rekening r, " . sfConfig::get('app_default_schema') . ".kelompok_belanja k
		where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and r.rekening_code=detail2.rekening_code and k.belanja_id=r.belanja_id and detail2.status_hapus=false
		group by k.belanja_name
	) as nilai_sekarang,
	( 
		select k.belanja_name, sum(detail1.nilai_anggaran) as hasil_kali1
		from " . sfConfig::get('app_default_schema') . ".prev_rincian_detail detail1," . sfConfig::get('app_default_schema') . ".rekening r, " . sfConfig::get('app_default_schema') . ".kelompok_belanja k
		where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and r.rekening_code=detail1.rekening_code and k.belanja_id=r.belanja_id and detail1.status_hapus=false
		group by k.belanja_name
	) as nilai_murni

where nilai_sekarang.belanja_name = nilai_murni.belanja_name
group by nilai_sekarang.belanja_name";

                            $query = "select nilai_murni.belanja_name as belanja_semula, nilai_sekarang.belanja_name as belanja_menjadi, sum(nilai_sekarang.hasil_kali2) as menjadi, sum(nilai_murni.hasil_kali1) as semula
from
	( select k.belanja_name, sum(detail2.nilai_anggaran) as hasil_kali2
	from " . sfConfig::get('app_default_schema') . ".rincian_detail detail2," . sfConfig::get('app_default_schema') . ".rekening r, " . sfConfig::get('app_default_schema') . ".kelompok_belanja k
        where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and r.rekening_code=detail2.rekening_code and k.belanja_id=r.belanja_id  and detail2.status_hapus=false
        group by k.belanja_name ) as nilai_sekarang left outer join
        ( select k.belanja_name, sum(detail1.nilai_anggaran) as hasil_kali1
	from " . sfConfig::get('app_default_schema') . ".prev_rincian_detail detail1," . sfConfig::get('app_default_schema') . ".rekening r, " . sfConfig::get('app_default_schema') . ".kelompok_belanja k
	where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and r.rekening_code=detail1.rekening_code and k.belanja_id=r.belanja_id  and detail1.status_hapus=false
        group by k.belanja_name ) as nilai_murni
 on nilai_sekarang.belanja_name= nilai_murni.belanja_name

group by nilai_murni.belanja_name, nilai_sekarang.belanja_name";
////awal-comment
//print_r($query);exit;
                            $con = Propel::getConnection();
                            $stmt = $con->prepareStatement($query);
                            $rs_rekening = $stmt->executeQuery();
                            $rekening_prev = 0;
                            $rekening_now = 0;
                            while ($rs_rekening->next()) {
                                if (!$rs_rekening->getString('belanja_semula')) {
                                    $nama_belanja = $rs_rekening->getString('belanja_menjadi');
                                } else {
                                    $nama_belanja = $rs_rekening->getString('belanja_semula');
                                }
                                ?>
                                <tr>
                                    <td><?php echo $nama_belanja; ?></td>
                                    <td align='right'><?php
                            echo number_format($rs_rekening->getString('semula'), 0, ',', '.');
                            $rekening_prev+=$rs_rekening->getString('semula');
                                ?></td>
                                    <td align='right'><?php
                                    echo number_format($rs_rekening->getString('menjadi'), 0, ',', '.');
                                    $rekening_now+=$rs_rekening->getString('menjadi');
                                ?></td>
                                </tr>
                                <?php
                            }
                            ?>
                            <tr>
                                <td>Total</td>
                                <td align='right'><?php echo number_format($rekening_prev, 0, ',', '.'); ?></td>
                                <td align='right'><?php echo number_format($rekening_now, 0, ',', '.'); ?></td>
                            </tr>
                        </table>
                    </td>
                </tr>                
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="15" style="border: 1">
            <div><span class="style3" style="font-size: smaller"><center>Rincian Anggaran Belanja Langsung</center></span></div>
            <div><span class="style3" style="font-size: smaller"><center>Menurut Program dan Per Kegiatan Satuan Kerja Perangkat Daerah</center></span></div>
        </td>
    </tr>
    <tr>
        <td colspan="7" align="center" bgcolor="#9999ff" style="border: 1"><span class="style3" style="font-size: smaller"><strong>Semula</strong></span></td>
        <td align="center" bgcolor="#9999ff" style="border: 1;"><span class="style3" style="font-size: smaller"><strong>|</strong></span></td>
        <td colspan="7" align="center" bgcolor="#9999ff" style="border: 1"><span class="style3" style="font-size: smaller"><strong>Menjadi</strong></span></td>
    </tr>
    <tr>
        <td align="center" bgcolor="#9999ff" style="border: 1"><span class="style3" style="font-size: smaller"><strong>Komponen</strong></span></td>
        <td align="center" bgcolor="#9999ff" style="border: 1"><span class="style3" style="font-size: smaller"><strong>Satuan</strong></span></td>
        <td align="center" bgcolor="#9999ff" style="border: 1"><span class="style3" style="font-size: smaller"><strong>Koefisien</strong></span></td>
        <td align="center" bgcolor="#9999ff" style="border: 1;"><span class="style3" style="font-size: smaller"><strong>Harga</strong></span></td>
        <td align="center" bgcolor="#9999ff" style="border: 1;"><span class="style3" style="font-size: smaller"><strong>Hasil</strong></span></td>
        <td align="center" bgcolor="#9999ff" style="border: 1;"><span class="style3" style="font-size: smaller"><strong>PPN</strong></span></td>
        <td align="center" bgcolor="#9999ff" style="border: 1;"><span class="style3" style="font-size: smaller"><strong>Total</strong></span></td>
        <td align="center" bgcolor="#9999ff" style="border: 1;"><span class="style3" style="font-size: smaller"><strong>|</strong></span></td>
        <td align="center" bgcolor="#9999ff" style="border: 1"><span class="style3" style="font-size: smaller"><strong>Komponen</strong></span></td>
        <td align="center" bgcolor="#9999ff" style="border: 1"><span class="style3" style="font-size: smaller"><strong>Satuan</strong></span></td>
        <td align="center" bgcolor="#9999ff" style="border: 1"><span class="style3" style="font-size: smaller"><strong>Koefisien</strong></span></td>
        <td align="center" bgcolor="#9999ff" style="border: 1;"><span class="style3" style="font-size: smaller"><strong>Harga</strong></span></td>
        <td align="center" bgcolor="#9999ff" style="border: 1;"><span class="style3" style="font-size: smaller"><strong>Hasil</strong></span></td>
        <td align="center" bgcolor="#9999ff" style="border: 1;"><span class="style3" style="font-size: smaller"><strong>PPN</strong></span></td>
        <td align="center" bgcolor="#9999ff" style="border: 1;"><span class="style3" style="font-size: smaller"><strong>Total</strong></span></td>
    </tr>
    <!--atasan-->
    <tbody>
        <?php
        $jumlah_harga_prev = 0;
        $jumlah_harga = 0;
        $query = "
(select detail2.subtitle as subtitle2, detail.subtitle as subtitle1
from " . sfConfig::get('app_default_schema') . ".subtitle_indikator detail2, " . sfConfig::get('app_default_schema') . ".prev_subtitle_indikator detail
where
detail2.unit_id='$unit_id' and detail2.kegiatan_code='$kode_kegiatan' and detail2.kegiatan_code=detail.kegiatan_code and detail2.unit_id=detail.unit_id
and detail2.subtitle=detail.subtitle)

union

(select '', detail.subtitle as subtitle1
from  " . sfConfig::get('app_default_schema') . ".subtitle_indikator detail
where
detail.unit_id='$unit_id' and detail.kegiatan_code='$kode_kegiatan' and detail.subtitle not in (select subtitle from " . sfConfig::get('app_default_schema') . ".subtitle_indikator detail2
where detail2.unit_id='$unit_id' and detail2.kegiatan_code='$kode_kegiatan'))

union

(select detail2.subtitle as subtitle2, '' as subtitle1
from  " . sfConfig::get('app_default_schema') . ".subtitle_indikator detail2
where
detail2.unit_id='$unit_id' and detail2.kegiatan_code='$kode_kegiatan' and detail2.subtitle not in (select subtitle from " . sfConfig::get('app_default_schema') . ".prev_subtitle_indikator detail
where detail.unit_id='$unit_id' and detail.kegiatan_code='$kode_kegiatan'))
order by subtitle2";
//print_r($query);exit;
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query);
        $rs = $stmt->executeQuery();
        while ($rs->next()) {
            $subtitle_prev = '';
            $subtitle = '';
            $subtitle_prev = $rs->getString('subtitle1');
            $subtitle = $rs->getString('subtitle2');

            $j = 0;
            $kosong = 'kanan';
            if ($subtitle_prev == $subtitle) {
                $kosong = 'tidakada';
                //$subtitle_prev = $subtitle;                                                                          
                $query2 = " (
select distinct detail2.rekening_code as rekening, detail.rekening_code as rekening_prev,r.rekening_name
from " . sfConfig::get('app_default_schema') . ".rincian_detail detail2 right outer join " . sfConfig::get('app_default_schema') . ".prev_rincian_detail detail
on detail2.unit_id=detail.unit_id and detail2.kegiatan_code=detail.kegiatan_code and detail2.subtitle=detail.subtitle
and detail2.rekening_code = detail.rekening_code, " . sfConfig::get('app_default_schema') . ".rekening r
where detail.unit_id='$unit_id' and detail.kegiatan_code='$kode_kegiatan' and detail.subtitle ilike '$subtitle' and detail.rekening_code=r.rekening_code 
order by detail2.rekening_code,detail.rekening_code
)
union
(
select distinct detail2.rekening_code as rekening, detail2.rekening_code as rekening_prev,r.rekening_name
from " . sfConfig::get('app_default_schema') . ".rincian_detail detail2 , " . sfConfig::get('app_default_schema') . ".rekening r
where  detail2.unit_id='$unit_id' and detail2.kegiatan_code='$kode_kegiatan' and detail2.subtitle ilike '$subtitle' and detail2.rekening_code=r.rekening_code and detail2.status_hapus=false and detail2.detail_no not in
(
 select detail_no
from " . sfConfig::get('app_default_schema') . ".prev_rincian_detail detail
where detail.unit_id='$unit_id' and detail.kegiatan_code='$kode_kegiatan' and detail.subtitle ilike '$subtitle'  and detail.status_hapus=false
)
order by detail2.rekening_code
)
union
(
select distinct detail2.rekening_code as rekening, detail2.rekening_code as rekening_prev,r.rekening_name
from " . sfConfig::get('app_default_schema') . ".rincian_detail detail2 , " . sfConfig::get('app_default_schema') . ".rekening r
where  detail2.unit_id='$unit_id' and detail2.kegiatan_code='$kode_kegiatan' and detail2.subtitle ilike '$subtitle' and detail2.rekening_code=r.rekening_code and detail2.status_hapus=false and (detail2.detail_no,detail2.rekening_code) not in
(
 select detail_no, rekening_code
from " . sfConfig::get('app_default_schema') . ".prev_rincian_detail detail
where detail.unit_id='$unit_id' and detail.kegiatan_code='$kode_kegiatan' and detail.subtitle ilike '$subtitle'  and detail.status_hapus=false
)
order by detail2.rekening_code
)";
//print_r($query2);exit;
                if ($subtitle == 'Pembangunan Jalan') {
                    //print_r($query2);exit;
                }
            } elseif ($rs->getString('subtitle2')) {
                $kosong = 'kiri';
                $query2 = "select
										distinct(detail.rekening_code), detail.subtitle, rekening.rekening_name, detail.rekening_code as rekening, '' as rekening_prev
										from
											" . sfConfig::get('app_default_schema') . ".rincian_detail detail," . sfConfig::get('app_default_schema') . ".rekening rekening
										where
										detail.kegiatan_code = '" . $kode_kegiatan . "' and detail.unit_id = '" . $unit_id . "' and detail.subtitle ilike '" . $subtitle . "'
										and detail.rekening_code=rekening.rekening_code and detail.status_hapus=false
                                                                                order by detail.rekening_code

										";
                //print_r($query2);exit;
            } else {
                $kosong = 'kanan';
                //$subtitle = $subtitle_prev;
                $query2 = "select
										distinct(detail.rekening_code), detail.subtitle, rekening.rekening_name, '' as rekening, detail.rekening_code as rekening_prev
										from
											" . sfConfig::get('app_default_schema') . ".prev_rincian_detail detail," . sfConfig::get('app_default_schema') . ".rekening rekening
										where
										detail.kegiatan_code = '" . $kode_kegiatan . "' and detail.unit_id = '" . $unit_id . "' and detail.subtitle ilike '" . $subtitle_prev . "'
										and detail.rekening_code=rekening.rekening_code and detail.status_hapus=false
                                                                                order by detail.rekening_code

										";
                //print_r($query2);exit;
            }

            $stmt2 = $con->prepareStatement($query2);
            $rs2 = $stmt2->executeQuery();
            if ($subtitle_prev <> '') {
                echo '<tr align="left" bgcolor="#ffffff"><td colspan="7"><span class="style3"><strong> :: ' . $subtitle_prev . '</strong></span></td>';
                echo '<td><strong><span>&nbsp;</span></strong></td>';
                echo '<td colspan="8"><span class="style3"><strong> :: ' . $subtitle . '</strong></span></td></tr>';
            } else {
                echo '<tr align="left" bgcolor="#ffffff"><td colspan="7"><span class="style3"><strong>&nbsp;</strong></span></td>';
                echo '<td><strong><span style="color:red;">!</span></strong></td>';
                echo '<td colspan="8"><span class="style3"><strong> :: ' . $subtitle . '</strong></span></td></tr>';
            }
            $total_semua = 0;
            $total_semua_n = 0;
            while ($rs2->next()) {
                if ($rs2->getString('rekening_prev') == '') {
                    $rekening_prev = '';
                }
                if ($rs2->getString('rekening_prev') != '') {
                    $rekening_prev = $rs2->getString('rekening_prev') . '&nbsp;' . $rs2->getString('rekening_name');
                }
                if ($rs2->getString('rekening_prev') != $rs2->getString('rekening')) {
                    $rekening_prev = $rs2->getString('rekening_prev');
                }
                $rekening = $rs2->getString('rekening') . '&nbsp;' . $rs2->getString('rekening_name');

                if ($kosong == 'tidakada') {
                    $rekening_prev = $rs2->getString('rekening_prev') . '&nbsp;' . $rs2->getString('rekening_name');
                    $rekening = $rs2->getString('rekening') . '&nbsp;' . $rs2->getString('rekening_name');
                    if ($rs2->getString('rekening')) {
                        $kode_rekening = $rs2->getString('rekening');
                    } else {
                        $kode_rekening = $rs2->getString('rekening_prev');
                    }
                    $query3 = "
												(
SELECT

	rekening.rekening_code,rekening.rekening_code as rekening_code2,
	rekening.rekening_name as nama_rekening2,
	detail2.komponen_name || ' ' || detail2.detail_name as detail_name22,
	detail2.komponen_harga_awal as detail_harga2,
	detail2.pajak as pajak2,
	detail2.komponen_id as komponen_id2,
	detail2.detail_name as detail_name2,
	detail2.komponen_name as komponen_name2,
	detail2.volume as volume2,
	detail2.subtitle as subtitle_name2,
	detail2.satuan as detail_satuan2,
	replace(detail2.keterangan_koefisien,'<*>','X') as keterangan_koefisien2,
	detail2.detail_no as detail_no2,
	detail2.volume * detail2.komponen_harga_awal as hasil2,
	(detail2.nilai_anggaran) as hasil_kali2,

	detail.komponen_name || ' ' || detail.detail_name as detail_name2,
	detail.komponen_harga_awal as detail_harga,
	detail.pajak as pajak,
	detail.komponen_id as komponen_id,
	detail.detail_name as detail_name,
	detail.komponen_name as komponen_name,
	detail.volume as volume,
	detail.subtitle as subtitle_name,
	detail.satuan as detail_satuan,
	replace(detail.keterangan_koefisien,'<*>','X') as keterangan_koefisien,
	detail.detail_no as detail_no,
	detail.volume * detail.komponen_harga_awal as hasil,
	(detail.nilai_anggaran) as hasil_kali,
	detail2.detail_name as detail_name_rd,
	detail2.sub as sub2,
	detail.sub,
        detail2.note_skpd as note_skpd

FROM
	" . sfConfig::get('app_default_schema') . ".rincian_detail detail2, " . sfConfig::get('app_default_schema') . ".rekening rekening, " . sfConfig::get('app_default_schema') . ".prev_rincian_detail detail

WHERE
	detail2.kegiatan_code = '$kode_kegiatan' and
	detail2.unit_id='$unit_id'

	and detail2.rekening_code='$kode_rekening'
	and detail2.subtitle ilike '$subtitle'
	and detail2.kegiatan_code=detail.kegiatan_code
	and detail2.unit_id=detail.unit_id
	and detail2.subtitle=detail.subtitle
	and detail2.rekening_code=detail.rekening_code
	and rekening.rekening_code=detail2.rekening_code
	and detail2.detail_no=detail.detail_no
        and detail.status_hapus<>true
        and detail2.status_hapus<>true
        

ORDER BY

	rekening.rekening_code,
	detail2.subtitle ,
	detail2.komponen_name
)
union
(
SELECT

	rekening.rekening_code,rekening.rekening_code as rekening_code2,
	rekening.rekening_name as nama_rekening2,
	detail2.komponen_name || ' ' || detail2.detail_name as detail_name22,
	detail2.komponen_harga_awal as detail_harga2,
	detail2.pajak as pajak2,
	detail2.komponen_id as komponen_id2,
	detail2.detail_name as detail_name2,
	detail2.komponen_name as komponen_name2,
	detail2.volume as volume2,
	detail2.subtitle as subtitle_name2,
	detail2.satuan as detail_satuan2,
	replace(detail2.keterangan_koefisien,'<*>','X') as keterangan_koefisien2,
	detail2.detail_no as detail_no2,
	detail2.volume * detail2.komponen_harga_awal as hasil2,
	(detail2.nilai_anggaran) as hasil_kali2,

	'' as detail_name,
	0 as detail_harga,
	0 as pajak,
	'' as komponen_id,
	'' as detail_name,
	'' as komponen_name,
	0 as volume,
	'' as subtitle_name,
	'' as detail_satuan,
	'' as keterangan_koefisien,
	0 as detail_no,
	0 as hasil,
	0 as hasil_kali,
	detail2.detail_name as detail_name_rd,
	detail2.sub as sub2,
	'' as sub,
        detail2.note_skpd as note_skpd


FROM
	" . sfConfig::get('app_default_schema') . ".rincian_detail detail2, " . sfConfig::get('app_default_schema') . ".rekening rekening, " . sfConfig::get('app_default_schema') . ".prev_rincian_detail detail

WHERE
	detail2.kegiatan_code = '$kode_kegiatan' and
	detail2.unit_id='$unit_id'

	and detail2.rekening_code='$kode_rekening'
	and detail2.subtitle ilike '$subtitle'
	and detail2.kegiatan_code=detail.kegiatan_code
	and detail2.unit_id=detail.unit_id
	and detail2.subtitle=detail.subtitle
	and detail2.rekening_code=detail.rekening_code
	and rekening.rekening_code=detail2.rekening_code
	and detail2.detail_no=detail.detail_no
        and detail.status_hapus=true
        and detail2.status_hapus<>true
        

ORDER BY

	rekening.rekening_code,
	detail2.subtitle ,
	detail2.komponen_name
)
union
(
SELECT

	rekening.rekening_code,rekening.rekening_code as rekening_code2,
	rekening.rekening_name as nama_rekening2,
	detail2.komponen_name || ' ' || detail2.detail_name as detail_name22,
	detail2.komponen_harga_awal as detail_harga2,
	detail2.pajak as pajak2,
	detail2.komponen_id as komponen_id2,
	detail2.detail_name as detail_name2,
	detail2.komponen_name as komponen_name2,
	detail2.volume as volume2,
	detail2.subtitle as subtitle_name2,
	detail2.satuan as detail_satuan2,
	replace(detail2.keterangan_koefisien,'<*>','X') as keterangan_koefisien2,
	detail2.detail_no as detail_no2,
	detail2.volume * detail2.komponen_harga_awal as hasil2,
	(detail2.nilai_anggaran) as hasil_kali2,

	'' as detail_name,
	0 as detail_harga,
	0 as pajak,
	'' as komponen_id,
	'' as detail_name,
	'' as komponen_name,
	0 as volume,
	'' as subtitle_name,
	'' as detail_satuan,
	'' as keterangan_koefisien,
	0 as detail_no,
	0 as hasil,
	0 as hasil_kali,
	detail2.detail_name as detail_name_rd,
	detail2.sub as sub2,
	'' as sub,
        detail2.note_skpd as note_skpd


FROM
	" . sfConfig::get('app_default_schema') . ".rincian_detail detail2, " . sfConfig::get('app_default_schema') . ".rekening rekening

WHERE
	detail2.kegiatan_code = '$kode_kegiatan' and
	detail2.unit_id='$unit_id'

        and detail2.status_hapus=false
	and detail2.rekening_code='$kode_rekening'
	and detail2.subtitle ilike '$subtitle'
	and rekening.rekening_code=detail2.rekening_code
	and (detail2.unit_id||detail2.kegiatan_code||detail2.detail_no) not in
	(
	select (detail.unit_id||detail.kegiatan_code||detail.detail_no) from " . sfConfig::get('app_default_schema') . ".prev_rincian_detail detail where detail.kegiatan_code = '$kode_kegiatan' and
	detail.unit_id='$unit_id' and detail.subtitle ilike '$subtitle'
	and detail.rekening_code='$kode_rekening'
	)

ORDER BY

	rekening.rekening_code,
	detail2.subtitle ,
	detail2.komponen_name)
union
(
SELECT

	rekening.rekening_code,rekening.rekening_code as rekening_code2,
	rekening.rekening_name as nama_rekening2,
	'' as detail_name22,
	0 as detail_harga2,
	0 as pajak2,
	'' as komponen_id2,
	'' as detail_name2,
	'' as komponen_name2,
	0 as volume2,
	'' as subtitle_name2,
	'' as detail_satuan2,
	'' as keterangan_koefisien2,
	0 as detail_no2,
	0 as hasil2,
	0 as hasil_kali2,

	detail.komponen_name || ' ' || detail.detail_name as detail_name2,
	detail.komponen_harga_awal as detail_harga,
	detail.pajak as pajak,
	detail.komponen_id as komponen_id,
	detail.detail_name as detail_name,
	detail.komponen_name as komponen_name,
	detail.volume as volume,
	detail.subtitle as subtitle_name,
	detail.satuan as detail_satuan,
	replace(detail.keterangan_koefisien,'<*>','X') as keterangan_koefisien,
	detail.detail_no as detail_no,
	detail.volume * detail.komponen_harga_awal as hasil,
	(detail.nilai_anggaran) as hasil_kali,
	'' as detail_name_rd,
	'' as sub2,
	detail.sub,
        '' as note_skpd


FROM
	" . sfConfig::get('app_default_schema') . ".prev_rincian_detail detail, " . sfConfig::get('app_default_schema') . ".rekening rekening

WHERE
	detail.kegiatan_code = '$kode_kegiatan' and
	detail.unit_id='$unit_id'

        and detail.status_hapus=false
	and detail.rekening_code='$kode_rekening'
	and detail.subtitle ilike '$subtitle'
	and rekening.rekening_code=detail.rekening_code
	and (detail.unit_id||detail.kegiatan_code||detail.detail_no) not in
	(
	select (detail2.unit_id||detail2.kegiatan_code||detail2.detail_no) from " . sfConfig::get('app_default_schema') . ".rincian_detail detail2 where detail2.kegiatan_code = '$kode_kegiatan' and
	detail2.unit_id='$unit_id' and detail2.subtitle ilike '$subtitle' and detail2.status_hapus<>true
	and detail2.rekening_code='$kode_rekening'
	)

ORDER BY

	rekening.rekening_code,
	detail.subtitle ,
	detail.komponen_name)";
//print_r($query3);exit;
                    //if($kode_rekening=='5.2.3.15.01') {print_r($query3);exit;}
                } elseif ($kosong == 'kiri') {
                    $rekening_prev = '';
                    $rekening = $rs2->getString('rekening') . '&nbsp;' . $rs2->getString('rekening_name');
                    $kode_rekening = $rs2->getString('rekening');

                    $query3 = "(SELECT

											rekening.rekening_code,rekening.rekening_code as rekening_code2,
											rekening.rekening_name as nama_rekening2,
											detail2.komponen_name || ' ' || detail2.detail_name as detail_name22,
											detail2.komponen_harga_awal as detail_harga2,
											detail2.pajak as pajak2,
											detail2.komponen_id as komponen_id2,
											detail2.detail_name as detail_name2,
											detail2.komponen_name as komponen_name2,
											detail2.volume as volume2,
											detail2.subtitle as subtitle_name2,
											detail2.satuan as detail_satuan2,
											replace(detail2.keterangan_koefisien,'<*>','X') as keterangan_koefisien2,
											detail2.detail_no as detail_no2,
											detail2.volume * detail2.komponen_harga_awal as hasil2,
											(detail2.nilai_anggaran) as hasil_kali2,
											detail2.detail_name as detail_name_rd,
											'' as detail_name2,
											0 as detail_harga,
											0 as pajak,
											'' as komponen_id,
											'' as detail_name,
											'' as komponen_name,
											0 as volume,
											'' as subtitle_name,
											'' as detail_satuan,
											'' as keterangan_koefisien,
											'' as detail_no,
											0 as hasil,
											0 as hasil_kali,
                                                                                        detail2.note_skpd as note_skpd


										FROM
											(" . sfConfig::get('app_default_schema') . ".rincian_detail detail2 inner join
											" . sfConfig::get('app_default_schema') . ".rekening rekening on rekening.rekening_code = detail2.rekening_code)
										WHERE
											detail2.kegiatan_code = '" . $kode_kegiatan . "' and
											detail2.unit_id='" . $unit_id . "'

											and detail2.rekening_code='" . $kode_rekening . "'
											and detail2.subtitle ilike '$subtitle'

                                                                                        and detail2.status_hapus=false         

										ORDER BY
											rekening.rekening_code,
											detail2.subtitle ,
											detail2.komponen_name)";
                } elseif ($kosong == 'kanan') {
                    $rekening_prev = $rs2->getString('rekening') . '&nbsp;' . $rs2->getString('rekening_name');
                    $rekening = '';
                    $kode_rekening = $rs2->getString('rekening_prev');
                    $query3 = "SELECT

											rekening.rekening_code,rekening.rekening_code as rekening_code2,
											rekening.rekening_name as nama_rekening2,
											'' as detail_name22,
											0 as detail_harga2,
											0 as pajak2,
											'' as komponen_id2,
											'' as detail_name2,
											'' as komponen_name2,
											0 as volume2,
											'' as subtitle_name2,
											'' as detail_satuan2,
											'' as keterangan_koefisien2,
											0 as detail_no2,
											0 as hasil2,
											0 as hasil_kali2,


											detail.komponen_name || ' ' || detail.detail_name as detail_name2,
											detail.komponen_harga_awal as detail_harga,
											detail.pajak as pajak,
											detail.komponen_id as komponen_id,
											detail.detail_name as detail_name,
											detail.komponen_name as komponen_name,
											detail.volume as volume,
											detail.subtitle as subtitle_name,
											detail.satuan as detail_satuan,
											replace(detail.keterangan_koefisien,'<*>','X') as keterangan_koefisien,
											detail.detail_no as detail_no,
											detail.volume * detail.komponen_harga_awal as hasil,
											(detail.nilai_anggaran) as hasil_kali,
											detail2.detail_name as detail_name_rd,
                                                                                        detail2.note_skpd as note_skpd


										FROM
											(" . sfConfig::get('app_default_schema') . ".rincian_detail detail2 inner join
											" . sfConfig::get('app_default_schema') . ".rekening rekening on rekening.rekening_code = detail2.rekening_code) left outer join
											" . sfConfig::get('app_default_schema') . ".prev_rincian_detail detail on detail2.unit_id=detail.unit_id and detail2.kegiatan_code=detail.kegiatan_code and detail2.detail_no=detail.detail_no
										WHERE
											detail.kegiatan_code = '" . $kode_kegiatan . "' and
											detail.unit_id='" . $unit_id . "'

											and detail.rekening_code='" . $kode_rekening . "'
											and detail.subtitle ilike '$subtitle_prev'

                                                                                        and detail2.status_hapus=false
                                                                                        and detail.status_hapus=false

										ORDER BY

											rekening.rekening_code,
											detail2.subtitle ,
											detail2.komponen_name";

                    $subtitle = $subtitle_prev;
                }

                if ($rs2->getString('rekening_prev')) {
                    ?>         
                    <?php
                    $ada_kosong = 0;
                    $query = "select count(*) as tot  
                                            from " . sfConfig::get('app_default_schema') . ".rincian_detail rd,  " . sfConfig::get('app_default_schema') . ".prev_rincian_detail prd 
                                                where rd.unit_id = '$unit_id' and rd.kegiatan_code = '$kode_kegiatan' and rd.rekening_code = '" . $rs2->getString('rekening') . "' and rd.status_hapus = true and prd.status_hapus = true 
                                            and rd.unit_id = prd.unit_id and rd.kegiatan_code = prd.kegiatan_code and rd.detail_no = prd.detail_no and prd.rekening_code = '" . $rs2->getString('rekening_prev') . "'";
                    $con = Propel::getConnection();
                    $stmt = $con->prepareStatement($query);
                    $rs1 = $stmt->executeQuery();
                    while ($rs1->next()) {
                        $ada_kosong = $rs1->getString('tot');
                    }
                    if ($ada_kosong == 0) {
                        ?>
                        <tr bgcolor="#ffffff" valign="top">
                            <td colspan="7" style="border: 1"><span class="style3" style="font-size: smaller"><strong><?php echo $rekening_prev ?></strong></span></td>
                            <td align="center" style="border: 1"><span class="style3" style="font-size: smaller">&nbsp;</span></td>
                            <td colspan="7" style="border: 1"><span class="style3" style="font-size: smaller"><strong><?php echo $rekening ?></strong></span></td>
                        </tr>
                    <?php }
                    ?>

                <?php } else if ($rs2->getString('rekening_prev') == '') { ?>
                    <?php
                    $ada_kosong = 0;
                    $query = "select count(*) as tot  
                                            from " . sfConfig::get('app_default_schema') . ".rincian_detail rd,  " . sfConfig::get('app_default_schema') . ".prev_rincian_detail prd 
                                                where rd.unit_id = '$unit_id' and rd.kegiatan_code = '$kode_kegiatan' and rd.rekening_code = '" . $rs2->getString('rekening') . "' and rd.status_hapus = true and prd.status_hapus = true 
                                            and rd.unit_id = prd.unit_id and rd.kegiatan_code = prd.kegiatan_code and rd.detail_no = prd.detail_no and prd.rekening_code = '" . $rs2->getString('rekening_prev') . "'";
                    $con = Propel::getConnection();
                    $stmt = $con->prepareStatement($query);
                    $rs1 = $stmt->executeQuery();
                    while ($rs1->next()) {
                        $ada_kosong = $rs1->getString('tot');
                    }
                    if ($ada_kosong == 0) {
                        ?>
                        <tr bgcolor="#ffffff" valign="top">
                            <td colspan="7" style="border: 1"><span class="style3" style="font-size: smaller">&nbsp;</span></td>
                            <td align="center"  style="border: 1"><span class="style3" style="font-size: smaller"><strong><span style="color:red;">!</span></strong></span></td>
                            <td colspan="7" style="border: 1"><span class="style3" style="font-size: smaller"><strong><?php echo $rekening ?></strong></span></td>
                        </tr>
                        <?php
                    }
                    ?>

                    <?php
                }

                //print_r($query3);exit;
                $stmt3 = $con->prepareStatement($query3);
                $rs3 = $stmt3->executeQuery();
                $total = 0;
                $total_n = 0;

                while ($rs3->next()) {
                    //prev_rincian_detail
                    if ($rs2->getString('rekening_prev')) {
                        $hasil = 0;
                        $detail_harga = 0;
                        $volume = 0;
                        $pajak = 0;
                        $detail_harga = $rs3->getString('detail_harga');
                        $volume = $rs3->getString('volume');
                        $pajak = $rs3->getString('pajak');
                        $subnya = '';
                        if ($rs3->getString('sub') and $rs3->getString('komponen_name'))
                            $subnya = '[' . $rs3->getString('sub') . '] ';

                        //tebal
                        if (($rs3->getString('komponen_name') != $rs3->getString('komponen_name2')) ||
                                ($rs3->getString('detail_name22') != $rs3->getString('detail_name2')) ||
                                ($rs3->getString('detail_satuan') != $rs3->getString('detail_satuan2')) ||
                                ($rs3->getString('keterangan_koefisien') != $rs3->getString('keterangan_koefisien2')) ||
                                ($rs3->getString('pajak') != $rs3->getString('pajak2')) ||
                                //irul 20maret 2014 - tambah banding harga
                                ($rs3->getString('detail_harga2') != $rs3->getString('detail_harga')) ||
                                //irul 20maret 2014 - tambah banding harga
                                ($rs3->getString('volume') != $rs3->getString('volume2'))) {
                            //print_r($query3);exit;
                            if ($sf_user->getNamaUser() != 'parlemen2') {
                                ?>
                                <tr bgcolor="#ffffff" valign="top">
                                    <td style="border: 1"><span class="style3" style="font-size: smaller"><strong><?php echo $subnya . $rs3->getString('komponen_name') . ' ' . $rs3->getString('detail_name') ?></strong></span></td>
                                    <td style="border: 1"><span class="style3" style="font-size: smaller"><strong><?php echo $rs3->getString('detail_satuan') ?></strong></span></td>
                                    <td style="border: 1"><span class="style3" style="font-size: smaller"><strong><?php echo $rs3->getString('keterangan_koefisien') . '=' . $rs3->getString('volume') . ' ' . $rs3->getString('detail_satuan') ?></strong></span></td>
                                    <td style="border: 1;text-align: right"><span class="style3" style="font-size: smaller"><strong><?php echo number_format($rs3->getString('detail_harga'), 0, ',', '.') ?></strong></span></td>
                                    <td style="border: 1;text-align: right"><span class="style3" style="font-size: smaller"><strong>
                                                <?php
                                                $hasil = $detail_harga * $volume;
                                                echo number_format($hasil, 0, ',', '.');
                                                ?>
                                            </strong></span></td>
                                    <td style="border: 1;text-align: right"><span class="style3" style="font-size: smaller"><strong><?php echo $rs3->getString('pajak') . '%'; ?></strong></span></td>
                                    <td style="border: 1;text-align: right"><span class="style3" style="font-size: smaller"><strong><?php echo number_format($rs3->getString('hasil_kali'), 0, ',', '.') ?></strong></span></td>
                                    <?php
                                    $total1 = $hasil - (($pajak / 100) * $hasil);
                                    $total = $total + $total1;
                                    $jumlah_harga_prev += $hasil;
                                    ?>
                                    <?php
                                }
                            } else {
                                if ($sf_user->getNamaUser() != 'parlemen2') {
                                    ?>
                                <tr bgcolor="#ffffff" valign="top">
                                    <td style="border: 1"><span class="style3" style="font-size: smaller"><?php echo $subnya . $rs3->getString('komponen_name') . ' ' . $rs3->getString('detail_name') ?></span></td>
                                    <td style="border: 1"><span class="style3" style="font-size: smaller"><?php echo $rs3->getString('detail_satuan') ?></span></td>
                                    <td style="border: 1"><span class="style3" style="font-size: smaller"><?php echo $rs3->getString('keterangan_koefisien') . '=' . $rs3->getString('volume') . ' ' . $rs3->getString('detail_satuan') ?></span></td>
                                    <td style="border: 1;text-align: right"><span class="style3" style="font-size: smaller"><?php echo number_format($rs3->getString('detail_harga'), 0, ',', '.') ?></span></td>
                                    <td style="border: 1;text-align: right"><span class="style3" style="font-size: smaller">
                                            <?php
                                            $hasil = $detail_harga * $volume;
                                            echo number_format($hasil, 0, ',', '.');
                                            ?>
                                        </span></td>
                                    <td style="border: 1;text-align: right"><span class="style3" style="font-size: smaller"><?php echo $rs3->getString('pajak') . '%'; ?></span></td>
                                    <td style="border: 1;text-align: right"><span class="style3" style="font-size: smaller"><?php echo number_format($rs3->getString('hasil_kali'), 0, ',', '.') ?></span></td>
                                    <?php
                                    $total1 = $hasil - (($pajak / 100) * $hasil);
                                    $total = $total + $total1;
                                    $jumlah_harga_prev += $hasil;
                                    ?>
                                    <?php
                                }
                            }

                            //tanda perbandingan
                            if (($rs3->getString('komponen_name') != $rs3->getString('komponen_name2')) ||
                                    ($rs3->getString('detail_name22') != $rs3->getString('detail_name2')) ||
                                    ($rs3->getString('detail_satuan') != $rs3->getString('detail_satuan2')) ||
                                    ($rs3->getString('keterangan_koefisien') != $rs3->getString('keterangan_koefisien2')) ||
                                    ($rs3->getString('pajak') != $rs3->getString('pajak2')) ||
                                    //irul 20maret 2014 - tambah banding harga
                                    ($rs3->getString('detail_harga2') != $rs3->getString('detail_harga')) ||
                                    //irul 20maret 2014 - tambah banding harga
                                    ($rs3->getString('volume') != $rs3->getString('volume2'))) {
                                if ($sf_user->getNamaUser() != 'parlemen2') {
                                    ?>

                                    <td align="center" style="border: 1;"><span class="style3" style="font-size: smaller"><strong><span style="color:red;">!</span></strong></span></td>
                                    <?php
                                }
                            } else {
                                if ($sf_user->getNamaUser() != 'parlemen2') {
                                    ?>
                                    <td align="center" style="border: 1;"><span class="style3" style="font-size: smaller">&nbsp;</span></td>
                                    <?php
                                }
                            }

                            $subnya2 = '';
                            $subnya = '';
                            if ($rs3->getString('sub2') and $rs3->getString('komponen_name2'))
                                $subnya2 = '[' . $rs3->getString('sub2') . '] ';
                            //rincian_detail
                            //tebal
                            if (($rs3->getString('komponen_name') != $rs3->getString('komponen_name2')) ||
                                    ($rs3->getString('detail_name22') != $rs3->getString('detail_name2')) ||
                                    ($rs3->getString('detail_satuan') != $rs3->getString('detail_satuan2')) ||
                                    ($rs3->getString('keterangan_koefisien') != $rs3->getString('keterangan_koefisien2')) ||
                                    ($rs3->getString('pajak') != $rs3->getString('pajak2')) ||
                                    //irul 20maret 2014 - tambah banding harga
                                    ($rs3->getString('detail_harga2') != $rs3->getString('detail_harga')) ||
                                    //irul 20maret 2014 - tambah banding harga
                                    ($rs3->getString('volume') != $rs3->getString('volume2'))) {
                                if ($sf_user->getNamaUser() != 'parlemen2') {
                                    ?>
                                    <td style="border: 1"><span class="style3" style="font-size: smaller"><strong><?php echo $subnya2 . $rs3->getString('komponen_name2') . ' ' . $rs3->getString('detail_name_rd') ?></strong></span></td>
                                    <td style="border: 1"><span class="style3" style="font-size: smaller"><strong><?php echo $rs3->getString('detail_satuan2') ?></strong></span></td>
                                    <td style="border: 1"><span class="style3" style="font-size: smaller"><strong><?php echo $rs3->getString('keterangan_koefisien2') . '=' . $rs3->getString('volume2') . ' ' . $rs3->getString('detail_satuan2') ?></strong></span></td>
                                    <td style="border: 1;text-align: right"><span class="style3" style="font-size: smaller"><strong><?php echo number_format($rs3->getString('detail_harga2'), 0, ',', '.') ?></strong></span></td>
                                    <td style="border: 1;text-align: right"><span class="style3" style="font-size: smaller"><strong>
                                                <?php
                                                $hasil = $rs3->getString('detail_harga2') * $rs3->getString('volume2');
                                                echo number_format($hasil, 0, ',', '.');
                                                ?>
                                            </strong></span></td>
                                    <td style="border: 1;text-align: right"><span class="style3" style="font-size: smaller"><strong><?php echo $rs3->getString('pajak2') . '%' ?></strong></span></td>
                                    <td style="border: 1;text-align: right"><span class="style3" style="font-size: smaller"><strong><?php echo number_format($rs3->getString('hasil_kali2'), 0, ',', '.') ?></strong></span></td>
                                    <?php
                                    $total1 = $hasil - (($rs3->getString('pajak2') / 100) * $hasil);
                                    $total_n += $total1;
                                    $jumlah_harga += $hasil;
                                    ?>
                                </tr>
                                <?php
                            }
                        } else if ($rs2->getString('rekening_prev')) {
                            if ($sf_user->getNamaUser() != 'parlemen2') {
                                ?>
                            <td style="border: 1"><span class="style3" style="font-size: smaller"><?php echo $subnya2 . $rs3->getString('komponen_name2') . ' ' . $rs3->getString('detail_name_rd') ?></span></td>
                            <td style="border: 1"><span class="style3" style="font-size: smaller"><?php echo $rs3->getString('detail_satuan2') ?></span></td>
                            <td style="border: 1"><span class="style3" style="font-size: smaller"><?php echo $rs3->getString('keterangan_koefisien2') . '=' . $rs3->getString('volume2') . ' ' . $rs3->getString('detail_satuan2') ?></span></td>
                            <td style="border: 1;text-align: right"><span class="style3" style="font-size: smaller"><?php echo number_format($rs3->getString('detail_harga2'), 0, ',', '.') ?></span></td>
                            <td style="border: 1;text-align: right"><span class="style3" style="font-size: smaller">
                                    <?php
                                    $hasil = $rs3->getString('detail_harga2') * $rs3->getString('volume2');
                                    echo number_format($hasil, 0, ',', '.');
                                    ?>
                                </span></td>
                            <td style="border: 1;text-align: right"><span class="style3" style="font-size: smaller"><?php echo $rs3->getString('pajak2') . '%' ?></span></td>
                            <td style="border: 1;text-align: right"><span class="style3" style="font-size: smaller"><?php echo number_format($rs3->getString('hasil_kali2'), 0, ',', '.') ?></span></td>
                            <?php
                            $total1 = $hasil - (($rs3->getString('pajak2') / 100) * $hasil);
                            $total_n += $total1;
                            $jumlah_harga += $hasil;
                            ?>
                        </tr>
                        <?php
                    }
                }
            } else if ($rs2->getString('rekening_prev') == '') {
                //irul 2april 2014 - kalo kiri gak ada subtitle
                ?>
                <tr bgcolor="#ffffff" valign="top">
                    <td align="center" style="border: 1" colspan="7"><span class="style3" style="font-size: smaller"><strong>&nbsp;</strong></span></td>
                    <td align="center" style="border: 1;"><span class="style3" style="font-size: smaller"><strong><span style="color:red;">!</span></strong></span></td>
                    <td align="center" style="border: 1"><span class="style3" style="font-size: smaller"><strong><?php echo $subnya2 . $rs3->getString('komponen_name2') . ' ' . $rs3->getString('detail_name_rd') ?></strong></span></td>
                    <td align="center" style="border: 1"><span class="style3" style="font-size: smaller"><strong><?php echo $rs3->getString('detail_satuan2') ?></strong></span></td>
                    <td align="center" style="border: 1"><span class="style3" style="font-size: smaller"><strong><?php echo $rs3->getString('keterangan_koefisien2') . '=' . $rs3->getString('volume2') . ' ' . $rs3->getString('detail_satuan2') ?></strong></span></td>
                    <td style="border: 1;text-align: right"><span class="style3" style="font-size: smaller"><strong><?php echo number_format($rs3->getString('detail_harga2'), 0, ',', '.') ?></strong></span></td>
                    <td style="border: 1;text-align: right"><span class="style3" style="font-size: smaller"><strong>
                                <?php
                                $hasil = $rs3->getString('detail_harga2') * $rs3->getString('volume2');
                                echo number_format($hasil, 0, ',', '.');
                                ?>
                            </strong></span></td>
                    <td style="border: 1;text-align: right"><span class="style3" style="font-size: smaller"><strong><?php echo $rs3->getString('pajak2') ?></strong></span></td>
                    <td style="border: 1;text-align: right"><span class="style3" style="font-size: smaller"><strong><?php echo number_format($rs3->getString('hasil_kali2'), 0, ',', '.'); ?></strong></span></td>
                    <?php
                    $total1 = $hasil - (($rs3->getString('pajak2') / 100) * $hasil);
                    $total_n += $total1;
                    $jumlah_harga += $hasil;
                    ?>
                </tr>
                <?php
                //irul 2april 2014 - kalo kiri gak ada subtitle
            }
        }
    }
    $total_semua = $total_semua + $total;
    $total_semua_n += $total_n;
    $query = "select sum(rd.nilai_anggaran) as nilai 
        from " . sfConfig::get('app_default_schema') . ".prev_rincian_detail rd 
        where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and subtitle='$subtitle' and rd.status_hapus=false";
    $con = Propel::getConnection();
    $stmt = $con->prepareStatement($query);
    $rs_harga_subtitle = $stmt->executeQuery();
    while ($rs_harga_subtitle->next()) {
        $total_subtitle_semula = $rs_harga_subtitle->getString('nilai');
    }

    $query = "select sum(rd.nilai_anggaran) as nilai 
        from " . sfConfig::get('app_default_schema') . ".rincian_detail rd 
        where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and subtitle='$subtitle' and rd.status_hapus=false";
    $con = Propel::getConnection();
    $stmt = $con->prepareStatement($query);
    $rs_harga_subtitle = $stmt->executeQuery();
    while ($rs_harga_subtitle->next()) {
        $total_subtitle_menjadi = $rs_harga_subtitle->getString('nilai');
    }
    ?>
    <tr bgcolor="#ffffff">
        <td colspan="6" class="Font8v style3" style="font-weight: bold" align="right">Total <?php echo $subtitle ?> : </td>
        <td class="Font8v style3" align="right" nowrap="nowrap"><?php echo number_format($total_subtitle_semula, 0, ',', '.') ?></td>
        <td></td>
        <td colspan="6" class="Font8v style3" style="font-weight: bold" align="right">Total <?php echo $subtitle ?> : </td>
        <td class="Font8v style3" align="right" nowrap="nowrap"><?php echo number_format($total_subtitle_menjadi, 0, ',', '.') ?></td>
    </tr>
    <?php
}
?>
<tr bgcolor="#ffffff">
    <td colspan="6" class="Font8v style3" style="font-weight: bold" align="right">Total : </td>
    <td class="Font8v style3" align="right" nowrap="nowrap"><?php echo number_format($total_sebelum, 0, ',', '.') ?></td>
    <td></td>
    <td colspan="6" class="Font8v style3" style="font-weight: bold" align="right">Total : </td>
    <td class="Font8v style3" align="right" nowrap="nowrap"><?php echo number_format($total_setelah, 0, ',', '.') ?></td>
</tr>
</tbody>
<!--bawahan-->
<?php
$c = new Criteria();
$c->add(UnitKerjaPeer::UNIT_ID, $unit_id);
$cs = UnitKerjaPeer::doSelectOne($c);
if ($cs) {
    $kepala = $cs->getKepalaPangkat();
    $nama = $cs->getKepalaNama();
    $nip = $cs->getKepalaNip();
    $unit_name = $cs->getUnitName();
}
?>
<tr>
    <td style="font-size: smaller;" colspan="9" rowspan="2">&nbsp;</td>
    <td style="font-size: smaller;" colspan="6" border="0">
        Surabaya,
    </td>
</tr>        
<tr>
    <td style="font-size: smaller;" colspan="6" border="0" align="center">            
        <br/>
        <?php
        if ($unit_id == '0700') {
            echo strtoupper('INSPEKTUR');
        } else {
            echo 'KEPALA ';
            echo strtoupper($unit_name);
        }
        ?>
        <br/><br/><br/><br/><br/><br/><br/>
        <font style="text-decoration: underline; font-weight: bold"><?php echo $nama ?></font><br/>
        <?php echo $kepala ?><br/>
        <?php echo 'NIP : ' . $nip ?>
        <br/><br/>
    </td>
</tr>
<tr>
    <td colspan="15" style="border: 1">Keterangan :</td>
</tr>
<tr>
    <td colspan="15" style="border: 1">Tanggal Pembahasan :</td>
</tr>    
<tr>
    <td colspan="15" style="border: 1">Catatan :</td>
</tr>    
<tr>
    <td colspan="15" style="border: 1">1.</td>
</tr>
<tr>
    <td colspan="15" style="border: 1">2.</td>
</tr>
<tr>
    <td colspan="15" style="border: 1">3.</td>
</tr>
<tr>
    <td colspan="15" style="border: 1">4.</td>
</tr>
<tr>
    <td colspan="15" style="border: 1">5.</td>
</tr>
<tr>
    <td colspan="15" style="border: 1" align="center">Tim Anggaran Pemerintahan Daerah</td>
</tr>
<tr>
    <td colspan="2" style="border: 1" align="center">No</td>
    <td colspan="5" style="border: 1" align="center">Nama</td>
    <td colspan="5" style="border: 1" align="center">NIP</td>
    <td colspan="3" style="border: 1" align="center">Jabatan</td>
</tr>
<?php for ($i = 1; $i <= 3; $i++) { ?>
    <tr>
        <td colspan="2" style="border: 1" align="center"><?php echo $i ?></td>
        <td colspan="5" style="border: 1"></td>
        <td colspan="5" style="border: 1"></td>
        <td colspan="3" style="border: 1"></td>
    </tr>        
    <?php
}
?>    
</table>