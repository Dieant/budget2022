<?php use_helper('Form', 'Object', 'Javascript') ?>
<?php
if (sfConfig::get('app_tahap_edit') == 'murni') {
    $nama_sistem = 'Pra-RKA';
} elseif (sfConfig::get('app_tahap_edit') == 'pak') {
    $nama_sistem = 'PAK';
} else {
    $nama_sistem = 'Revisi';
}
?>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">Pilih SKPD</div>
        </div>
        <div class="row">
            <div class="col-12">
                <?php
                    $e = new Criteria();
                    $e->add(UnitKerjaPeer::UNIT_ID, '9999', Criteria::NOT_IN);
                    $e->addAscendingOrderByColumn(UnitKerjaPeer::UNIT_NAME);
                    $unit_kerja = UnitKerjaPeer::doSelect($e);
                    echo select_tag('unit_id', objects_for_select($unit_kerja, 'getUnitId', 'getUnitName', (isset($filter_unit_kerja) ? $filter_unit_kerja : NULL)), array('id' => 'unit_id', 'class' => 'form-control'));
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="fas fa-align-left"></i> Perbandingan Data e-Budgeting dengan SABK
                        </h3>
                    </div>
                    <div class="card-body"> 
                        <table class="table table-bordered">
                            <tr>
                                <th class="text-center">Kode Urusan e-Budgeting</th>
                                <th class="text-center">Kode Urusan Devplan</th>
                                <th class="text-center">Kode Bidang e-Budgeting</th>
                                <th class="text-center">Kode Bidang Devplan</th>
                                <th class="text-center">Kode Program e-Budgeting</th>
                                <th class="text-center">Kode Program Devplan</th>
                                <th class="text-center">Kode Kegiatan e-Budgeting</th>
                                <th class="text-center">Kode Kegiatan Devplan</th>
                                <th class="text-center">Kode Sub Kegiatan e-Budgeting</th>
                                <th class="text-center">Kode Sub Kegiatan Devplan</th>
                            </tr>
                            <?php
                            $tempUnit = '';
                            ?>
                            <tr><td colspan="3">&nbsp;</td></tr>
                            <tr>
                                <td colspan="3" class="text-left text-bold"><?php echo $unit_id . ' - ' . $unit_name; ?></td>
                            </tr>
                            <?php
                            foreach ($perbedaans as $key => $value):
                                if($value['kode_urusan'] != $value['kode_urusan_devplan'] || $value['kode_bidang'] != $value['kode_bidang_devplan'] || $value['kode_program'] != $value['kode_program_devplan'] || $value['kode_kegiatan'] != $value['kode_kegiatan_devplan'] || $value['kode_sub_kegiatan'] != $value['kode_sub_kegiatan_devplan']) {
                                    $warna = 'pink';
                                    ?>
                                <tr style='background: pink;'>
                                    <td class='text-left' ><?php echo $value['kode_urusan']; ?></td>
                                    <td class='text-left' ><?php echo $value['kode_urusan_devplan']; ?></td>
                                    <td class='text-left' ><?php echo $value['kode_bidang']; ?></td>
                                    <td class='text-left' ><?php echo $value['kode_bidang_devplan']; ?></td>
                                    <td class='text-left' ><?php echo $value['kode_program']; ?></td>
                                    <td class='text-left' ><?php echo $value['kode_program_devplan']; ?></td>
                                    <td class='text-left' ><?php echo $value['kode_kegiatan']; ?></td>
                                    <td class='text-left' ><?php echo $value['kode_kegiatan_devplan']; ?></td>
                                    <td class='text-left' ><?php echo $value['kode_sub_kegiatan']; ?></td>
                                    <td class='text-left' ><?php echo $value['kode_sub_kegiatan_devplan']; ?></td>
                                </tr>
                                <?php
                                }
                                else
                                $warna = '';

                                $tempUnit = $value['unit_id'];
                            endforeach; ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- <div class="box box-info">
    <div class="box-header with-border">
        
    </div>
    <div class="box-body">
        
    </div>
</div>
<div class="box box-info">        
    <div class="box-header with-border">
        
    </div>
    <div class="box-body">
        
    </div>
</div> -->
<script>
    $("#unit_id").change(function () {
        $('#indicator').show();
        var id = $(this).val();
        if (id == '') {
            id = '0000';
        }
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/report/reportDataHeaderDevplan/unit_id/" + id + ".html",
            context: document.body
        }).done(function (msg) {
            $('#indicator').hide();
            $('#isi').html(msg);
        });

    });
</script>