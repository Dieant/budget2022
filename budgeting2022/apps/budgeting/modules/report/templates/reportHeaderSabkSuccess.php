<?php use_helper('Form', 'Object', 'Javascript') ?>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="fas fa-align-left"></i> Laporan Perbandingan Header eBudgeting dengan SABK
                        </h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Perangkat Daerah</label>
                                    <?php
                                    $e = new Criteria();
                                    $e->add(UnitKerjaPeer::UNIT_ID, '9999', Criteria::NOT_IN);
                                    $e->addAscendingOrderByColumn(UnitKerjaPeer::UNIT_NAME);
                                    $unit_kerja = UnitKerjaPeer::doSelect($e);
                                    echo select_tag('unit_id', objects_for_select($unit_kerja, 'getUnitId', 'getUnitName', (isset($filter_unit_kerja) ? $filter_unit_kerja : NULL)), array('id' => 'unit_id', 'class' => 'form-control js-example-basic-single'));
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body table-responsive p-0">
                        <table class="table table-hover">
                            <thead class="head_peach">
                                <tr>
                                    <th class="text-center">Dinas</th>
                                    <th class="text-center">Kode Permen</th>
                                    <th class="text-center">Kode Permen SABK</th>
                                    <th class="text-center">Kode Kegiatan</th>
                                    <th class="text-center">Nama Kegiatan</th>
                                    <th class="text-center">Nama Kegiatan SABK</th>
                                    <th class="text-center">Target Capaian Program</th>
                                    <th class="text-center">Target Capaian Program SABK</th>
                                    <th class="text-center">Target Input</th>
                                    <th class="text-center">Target Input SABK</th>
                                    <th class="text-center">Tolak Ukur Output</th>
                                    <th class="text-center">Tolak Ukur Output SABK</th> 
                                    <th class="text-center">Target Output</th>
                                    <th class="text-center">Target Output SABK</th>
                                    <th class="text-center">Tolak Ukur Outcome</th>
                                    <th class="text-center">Tolak Ukur Outcome SABK</th>
                                    <th class="text-center">Target Outcome</th>
                                    <th class="text-center">Target Outcome SABK</th>
                                    <th class="text-center">Kelompok Sasaran</th>
                                    <th class="text-center">Kelompok Sasaran SABK</th>
                                    <th class="text-center">Lokasi</th>
                                    <th class="text-center">Lokasi SABK</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    $tempUnit = '';
                                ?>
                                <tr>
                                    <td colspan="3">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="text-left text-bold"><?php echo $unit_id . ' - ' . $unit_name; ?></td>
                                </tr>
                                <?php
                                    foreach ($perbedaans as $value):
                                        if(  $value['kode_permen'] !=  $value['kode_permen_sabk'] || $value['nama_kegiatan'] != $value['nama_kegiatan_sabk'] || $value['target_capaian_program'] != $value['target_capaian_program_sabk'] || $value['target_input'] != $value['target_input_sabk'] || $value['tolak_ukur_output'] != $value['tolak_ukur_output_sabk'] || $value['target_output'] !=$value['target_output_sabk'] || $value['target_outcome'] != $value['target_outcome_sabk'] ||  $value['kelompok_sasaran'] != $value['kelompok_sasaran_sabk'] || $value['lokasi'] != $value['lokasi_sabk'] ) {
                                            $warna = 'pink';
                                            ?>
                                            <tr style='background: pink;'>
                                                <td>&nbsp;</td>
                                                <td class='text-left' ><?php echo $value['kode_permen']; ?></td>
                                                <td class='text-left' ><?php echo $value['kode_permen_sabk']; ?></td>
                                                <td class='text-left' ><?php echo $value['kode_kegiatan']; ?></td>
                                                <td class='text-left' ><?php echo $value['nama_kegiatan']; ?></td>
                                                <td class='text-left' ><?php echo $value['nama_kegiatan_sabk']; ?></td>
                                                <td class='text-left' ><?php echo $value['target_capaian_program']; ?></td>
                                                <td class='text-left' ><?php echo $value['target_capaian_program_sabk']; ?></td>
                                                <td class='text-left' ><?php echo $value['target_input']; ?></td>
                                                <td class='text-left' ><?php echo $value['target_input_sabk']; ?></td>
                                                <td class='text-left' ><?php echo $value['tolak_ukur_output']; ?></td>
                                                <td class='text-left' ><?php echo $value['tolak_ukur_output_sabk']; ?></td>
                                                <td class='text-left' ><?php echo $value['target_output']; ?></td>
                                                <td class='text-left' ><?php echo $value['target_output_sabk']; ?></td>
                                                <td class='text-left' ><?php echo $value['target_outcome']; ?></td>
                                                <td class='text-left' ><?php echo $value['target_outcome_sabk']; ?></td>
                                                <td class='text-left' ><?php echo $value['tolak_ukur_outcome']; ?></td>
                                                <td class='text-left' ><?php echo $value['tolak_ukur_outcome_sabk']; ?></td>
                                                <td class='text-left' ><?php echo $value['kelompok_sasaran']; ?></td>
                                                <td class='text-left' ><?php echo $value['kelompok_sasaran_sabk']; ?></td>
                                                <td class='text-left' ><?php echo $value['lokasi']; ?></td>
                                                <td class='text-left' ><?php echo $value['lokasi_sabk']; ?></td>            
                                            </tr>
                                            <?php
                                        }
                                        else
                                            $warna = '';
                                        $tempUnit = $value['unit_id'];
                                    endforeach; 
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $(document).ready(function () {
        $(".js-example-basic-single").select2();
    });
    $("#unit_id").change(function () {
        $('#indicator').show();
        var id = $(this).val();
        if (id == '') {
            id = '0000';
        }
        var tahap = "<?php echo $tahap; ?>";
        var access_token = "<?php echo $access_token; ?>";
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/report/reportHeaderSabk/unit_id/" + id + "/tahap/" + tahap + "/access_token/" + access_token + ".html",
            context: document.body
        }).done(function (msg) {
            $('#indicator').hide();
            $('#isi').html(msg);
        });
    });
</script>