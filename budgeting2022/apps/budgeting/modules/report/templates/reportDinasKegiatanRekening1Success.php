<?php use_helper('Form', 'Object', 'Javascript') ?>

<?php
echo select_tag('criteria', objects_for_select($unit_kerja, 'getUnitId', 'getUnitName', '', array('include_custom' => 'Pilih SKPD')), Array('id' => 'combo_pilih', 'onChange' => remote_function(Array('update' => 'isi', 'url' => 'report/reportDinasKegiatanRekening',
        'with' => "'b=rdsr2&where='+this.options[this.selectedIndex].value",
        'loading' => "Element.show('indicator');Element.hide('isi');Element.hide('isi_dalam');",
        'complete' => "Element.hide('indicator');Element.show('isi');" . visual_effect('highlight', 'isi')))));
?>
<br/><br/><br/>
<div id="isi"></div>
<?php if (isset($rs)): ?>
    <?php echo '<b>SKPD : ' . $nama_unit . ' (' . $unit_unit . ')</b>'; ?>
    <table cellspacing="0" class="sf_admin_list" width="100%">
        <tr>
            <td align="center" width="17"><strong></strong></td>
            <td ><div align="center"></div></td>
            <td align="center"><strong> Kegiatan - Rekening</strong></td>
            <td align="center"><strong>Nilai Semula </strong></td>
            <td align="center"><strong>Nilai Menjadi </strong></td>
        </tr>

        <?php
        $i = 0;
        $rs->first();
        do {
            ?>

        <?php if (isset($dinas_ok[$i])): ?>
                <tr bgcolor="black" ><td colspan="5" ><div class=""><hr size="5" color="grey"></div></td>
                </tr>
                <tr>
                    <td colspan="3" ><strong><?php echo $kode_kegiatan[$i] . ' ' . $nama_kegiatan[$i] ?></strong></td>
                    <td align="right"><strong><?php echo $nilai_draft[$i]; ?></strong></td>
                    <td align="right"><strong><?php echo $nilai_locked[$i]; ?></strong></td>
                </tr>
        <?php endif ?>
            <tr>
                <td ><strong></strong></td>
                <td  ></td>
                <td ><?php echo $kode_rekening[$i] . ' ' . $nama_rekening[$i]; ?></td>
                <td align="right"><?php echo $nilai_draft4[$i]; ?></td>
                <td align="right"><?php echo $nilai_draft4[$i]; ?></td>
            </tr>
        <?php $i++;
    }while ($rs->next());
    ?>
        <tr>
            <td colspan="3"><strong>Total</strong></td>
            <td align="right"><strong><?php echo $nilai_draft2; ?></strong></td>
            <td align="right"><strong><?php echo $nilai_locked2; ?></strong></td>
        </tr>
    </table>
<?php endif; ?>