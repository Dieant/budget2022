<?php use_helper('I18N', 'Date', 'Object', 'Javascript', 'Validation') ?>
<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Log Approval</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Master</a></li>
          <li class="breadcrumb-item active">Log Approval</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>
<!-- Main content -->
<section class="content">
    <?php include_partial('report/list_messages'); ?>
    <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">
                        <i class="fas fa-filter"></i> Filter
                    </h3>
                </div>
                <div class="card-body">
                    <?php echo form_tag('report/logApproval', array('method' => 'get', 'class' => 'form-horizontal')); ?>
                    <div class="row">
                      <div class="col-md-3">
                        <div class="form-group">
                            <label>Perangkat Daerah</label>
                            <?php
                                $c = new Criteria();
                                $c->addAscendingOrderByColumn(UnitKerjaPeer::UNIT_NAME);
                                $rs_unit = UnitKerjaPeer::doSelect($c);
                                echo select_tag('filters[unit_id]', objects_for_select($rs_unit, 'getUnitId', 'getUnitName', isset($filters['unit_id']) ? $filters['unit_id'] : '', array('include_custom' => '--Pilih Perangkat Daerah--')), array('id' => 'unit_id', 'class' => 'form-control select2'));
                            ?>
                        </div>
                      </div>
                      <!-- /.col -->
                      <div class="col-md-3">
                        <div class="form-group">
                            <label>Nama Sub-Kegiatan</label>
                            <?php
                            if (isset($filters['unit_id'])) {
                                $c = new Criteria();
                                $c->add(MasterKegiatanPeer::UNIT_ID, $filters['unit_id']);
                                $c->addAscendingOrderByColumn(MasterKegiatanPeer::KODE_KEGIATAN);
                                $rs = MasterKegiatanPeer::doSelect($c);
                                $arr_tampung = array();
                                foreach ($rs as $kegiatan) {
                                    $arr_tampung[$kegiatan->getKodeKegiatan()] = $kegiatan->getKodeKegiatan() . ' - ' . $kegiatan->getNamaKegiatan();
                                }
                            } else {
                                $arr_tampung = array();
                            }
                            echo select_tag('filters[kode_kegiatan]', options_for_select($arr_tampung, isset($filters['kode_kegiatan']) ? $filters['kode_kegiatan'] : '', array('include_custom' => '---Pilih Perangkat Daerah Dulu---')), Array('id' => 'kegiatan', 'class' => 'form-control select2'));
                            ?>
                        </div>
                      </div>
                      <!-- /.col -->
                      <div class="col-md-3">
                        <div class="form-group">
                            <label>Tahap</label>
                             <?php 
                                echo select_tag('filters[tahap]', options_for_select(array('murni' => 'Murni', 'revisi1' => 'Revisi 1', 'revisi2' => 'Revisi 2', 'revisi3' => 'Revisi 3', 'revisi4' => 'Revisi 4', 'revisi5' => 'Revisi 5'), isset($filters['tahap']) ? $filters['tahap'] : '', array('include_custom' => '---Pilih Tahap---')), array('id' => 'tahap', 'class' => 'form-control select2'));
                             ?>
                        </div>
                      </div>
                      <!-- /.col -->
                      <div class="col-md-3">
                        <div class="form-group">
                            <label>Jabatan</label>
                            <?php 
                                echo select_tag('filters[approve]', options_for_select(array('0' => 'PPTK', '1' => 'KPA', '2' => 'PA', '3' => 'Tim Anggaran', '4' => 'Penyelia', '5' => 'Admin'), isset($filters['approve']) ? $filters['approve'] : '', array('include_custom' => '---Pilih Jabatan Approve---')), array('id' => 'tahap', 'class' => 'form-control select2')); 
                            ?>
                        </div>
                      </div>
                      <!-- /.col -->
                      <div class="col-md-2">
                        <div class="form-group">
                            <label class="tombol_filter">Tombol Filter</label><br/>
                            <button type="submit" name="filter" class="btn btn-outline-primary btn-sm">Filter <i class="fas fa-search"></i></button>
                            <?php
                                echo link_to('Reset <i class="fa fa-backspace"></i>', 'report/logApproval?filter=filter', array('class' => 'btn btn-outline-danger btn-sm'));
                            ?>
                        </div>
                        <!-- /.form-group -->
                      </div>
                      <!-- /.col -->                      
                    </div>
                    <?php echo '</form>'; ?>
                </div>
            </div>
          </div>
        </div>
        <div class="row">
            <div class="col-md-12 stretch-card">
                <div class="card">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead class="head_peach">
                                <tr>
                                    <th><strong>Satuan Kerja</strong></th>
                                    <th><strong>Kegiatan</strong></th>
                                    <th><strong>NIP/UserID</strong></th>
                                    <th><strong>Nama</strong></th>
                                    <th><strong>Jabatan</strong></th>
                                    <th><strong>Waktu</strong></th>
                                    <th><strong>Approve Sebagai</strong></th>
                                    <th><strong>Tahap</strong></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($pager->getResults() as $log):
                                    $user_id = $log->getUserId();
                                    $c = new Criteria();
                                    $c->add(MasterUserV2Peer::USER_ID, $user_id);
                                    $rs_nama = MasterUserV2Peer::doSelectOne($c);
                                    $nama = $rs_nama->getUserName();
                                    $jabatan = $rs_nama->getJabatan();

                                    $unit_id = $log->getUnitId();
                                    $c = new Criteria();
                                    $c->add(UnitKerjaPeer::UNIT_ID, $unit_id);
                                    $rs_unitkerja = UnitKerjaPeer::doSelectOne($c);
                                    ?>
                                    <tr>
                                        <td><?php echo $rs_unitkerja->getUnitId() . ' - ' . $rs_unitkerja->getUnitName() ?></td>
                                        <td><?php echo $log->getKegiatanCode() ?></td>
                                        <td><?php echo $user_id ?></td>
                                        <td><?php echo $nama ?></td>
                                        <td><?php echo $jabatan ?></td>
                                        <td><?php echo $log->getWaktu() ?></td>
                                        <td><?php echo $log->getSebagai() ?></td>
                                        <td><?php echo $log->getTahap() ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer clearfix">
                        <ul class="pagination pagination-sm m-0 float-left">
                            <?php
                                echo format_number_choice('[0] no result|[1] 1 result|(1,+Inf] %1% results', array('%1%' => $pager->getNbResults()), $pager->getNbResults()) 
                            ?>
                        </ul>
                        <ul class="pagination pagination-sm m-0 float-right">
                            <?php 
                            if ($pager->haveToPaginate()): 
                                echo '<li class="page-item">'.link_to('Previous', 'report/logApproval?page=' . $pager->getPreviousPage(), array('class' => 'page-link', 'align' => 'absmiddle', 'alt' => __('Previous'), 'title' => __('Previous'))).'</li>';

                                foreach ($pager->getLinks() as $page):
                                        echo '<li class="page-item">'.link_to_unless($page == $pager->getPage(), $page, "report/logApproval?page={$page}", array('class' => 'page-link')).'</li>';
                                endforeach;
                                echo '<li class="page-item">'.link_to('Next', 'report/logApproval?page=' . $pager->getNextPage(), array('class' => 'page-link', 'align' => 'absmiddle', 'alt' => __('Next'), 'title' => __('Next'))).'</li>'; 
                                endif;
                            ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    $(document).ready(function () {
        $(".js-example-basic-single").select2();
    });
    $("#unit_id").change(function () {
        var id = $(this).val();
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/report/pilihKegiatanLog/b/" + id + ".html",
            context: document.body
        }).done(function (msg) {
            $('#kegiatan').html(msg);
        });
    });
</script>