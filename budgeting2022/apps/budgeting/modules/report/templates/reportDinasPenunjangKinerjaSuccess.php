<div class="box box-info">        
    <div class="box-header with-border">
        Laporan Per Dinas Penunjang Kerja
    </div>
    <div class="box-body">
        <table class="table table-bordered">
            <tr>
                <th class="text-center">Dinas</th>
                <th class="text-center">Pagu</th>
                <th class="text-center">Jumlah Usulan</th>
                <th class="text-center">Nilai Usulan</th>
                <th class="text-center">Jumlah Disetujui</th>
                <th class="text-center">Nilai Disetujui</th>
                <th class="text-center">Setuju - Pagu</th>
            </tr>
            <?php
            $i = 0;
            if ($rs->first() <> '') {
                $rs->first();
                do {
                    ?>
                    <tr>
                        <td class="text-left"><?php echo $rs->getString('unit_id').' - '.$rs->getString('unit_name') ?></td>
                        <td class="text-right"><?php echo $pagu[$i] ?></td>
                        <td align="center"><?php echo $rs->getString('total_draft') ?></td>
                        <td align="right"><?php echo $nilai_usulan[$i] ?></td>
                        <td class="text-center"><?php echo ($rs->getString('total_locked') < 0) ? '0' : $rs->getString('total_locked') ?></td>
                        <td align="right"><?php echo number_format(($rs->getString('nilai_locked') < 0) ? '0' : $rs->getString('nilai_locked'), 0, ",", ".") ?></td>
                        <td align="right"><?php echo ($rs->getString('nilai_locked') <= 0) ? '0' : $selisih[$i] ?></td>
                    </tr>
                    <?php
                    $i++;
                } while ($rs->next());
            }
            ?>
            <tr>
                <td class="text-center bg-green-active text-bold">Total</td>
                <td class="text-right bg-green-active text-bold"><?php echo $total_pagu ?></td>
                <td class="text-center bg-green-active text-bold"><?php echo $total_draft2 ?></td>
                <td class="text-right bg-green-active text-bold"><?php echo $nilai_draft2 ?></td>
                <td class="text-center bg-green-active text-bold"  ><?php echo ($total_locked2 < 0) ? '0' : $total_locked2 ?></td>
                <td class="text-right bg-green-active text-bold"><?php echo ($total_locked2 < 0) ? '0' : $nilai_locked2 ?></td>
                <td class="text-right bg-green-active text-bold"><?php echo ($total_locked2 < 0) ? '0' : $total_selisih ?></td>
            </tr>
        </table>       
    </div>
</div>