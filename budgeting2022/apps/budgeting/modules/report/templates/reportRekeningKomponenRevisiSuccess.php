<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="fas fa-align-left"></i> Laporan Per Rekening Per Komponen
                        </h3>
                    </div>
                    <div class="card-body">
                        <table class="table table-hover">
                            <thead class="head_peach">
                                <tr>
                                    <th class="text-center" style="width: 5px"></th>
                                    <th class="text-center">Komponen</th>
                                    <th class="text-center">Nilai Usulan</th>
                                    <th class="text-center">% Usulan</th>
                                    <th class="text-center">Nilai Disetujui</th>
                                    <th class="text-center">% Disetujui</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    $i = 0;
                                    $rs->first();
                                    do {
                                        if (isset($dinas_ok[$i])):
                                            ?>
                                            <tr>
                                                <td colspan="6">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td class="text-left text-bold" colspan="2"><?php echo $rekening_code[$i] . ' - ' . $rekening_name[$i] ?></td>
                                                <td class="text-right text-bold"><?php echo $nilai_draft[$i] ?></td>
                                                <td class="text-right text-bold"><?php echo $persen_draft[$i] . ' %' ?></td>
                                                <td class="text-right text-bold"><?php echo $nilai_locked[$i] ?></td>
                                                <td class="text-right text-bold"><?php echo $persen_locked[$i] . ' %' ?></td>
                                            </tr>
                                            <?php
                                        endif;
                                        ?>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td class="text-left"><?php echo isset($komponen_name[$i]) ? $komponen_name[$i] : '' ?></td>
                                            <td class="text-right"><?php echo isset($nilai_draft3[$i]) ? $nilai_draft3[$i] : '' ?></td>
                                            <td class="text-right"><?php echo isset($persen_draft3[$i]) ? $persen_draft3[$i] : '' ?> %</td>
                                            <td class="text-right"><?php echo isset($nilai_locked3[$i]) ? $nilai_locked3[$i] : '' ?></td>
                                            <td class="text-right"><?php echo isset($persen_locked3[$i]) ? $persen_locked3[$i] : '' ?> %</td>
                                        </tr>
                                        <?php
                                        $i++;
                                    } while ($rs->next())
                                ?>
                                <tr>
                                    <td colspan="2" class="text-bold text-center">Total</td>
                                    <td class="text-bold text-right"><?php echo $nilai_draft2 ?></td>
                                    <td class="text-bold text-right">100 %</td>
                                    <td class="text-bold text-right"><?php echo $nilai_locked2 ?></td>
                                    <td class="text-bold text-right">100 %</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>