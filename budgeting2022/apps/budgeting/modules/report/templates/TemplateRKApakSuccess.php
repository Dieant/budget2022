<?php use_helper('Javascript', 'Number') ?>
<?php
//echo "Print RKA melalui SABK ".link_to('(http://sabk.surabaya.go.id)','http://sabk.surabaya.go.id', array('target'=>'_blank')).". Tekan tombol BACK untuk kembali";exit;
echo use_stylesheet('/css/tampilan_print2.css');
echo javascript_tag("window.print();");
?>

<?php
$kode_kegiatan = $sf_params->get('kode_kegiatan');
$unit_id = $sf_params->get('unit_id');
$query = "select mk.kode_kegiatan, mk.nama_kegiatan, mk.alokasi_dana, mk.kode_program,
	mk.kode_program2, mk.benefit, mk.impact, mk.kode_program2, mk.kode_misi, mk.kode_tujuan,
	mk.outcome, mk.target_outcome, mk.catatan, mk.ranking,uk.unit_name, mk.kode_urusan,
	uk.kode_permen,mk.output, mk.kelompok_sasaran
	from " . sfConfig::get('app_default_schema') . ".dinas_master_kegiatan mk,unit_kerja uk
	where mk.kode_kegiatan = '" . $kode_kegiatan . "' and mk.unit_id = '" . $unit_id . "'
	and uk.unit_id=mk.unit_id";
$con = Propel::getConnection();
$stmt = $con->prepareStatement($query);
$rs = $stmt->executeQuery();
$ranking = '';
while ($rs->next()) {
    $kegiatan_kode = $rs->getString('kode_kegiatan');
    $nama_kegiatan = $rs->getString('nama_kegiatan');
    $alokasi = $rs->getString('alokasi_dana');
    $organisasi = $rs->getString('unit_name');
    $benefit = $rs->getString('benefit');
    $impact = $rs->getString('impact');
    $kode_program = $rs->getString('kode_program');
    $program_p_13 = $rs->getString('kode_program2');
    //$urusan = $rs->getString('urusan');
    $kode_misi = $rs->getString('kode_misi');
    $kode_tujuan = '';
    if ($rs->getString('kode_tujuan')) {
        $kode_tujuan = $rs->getString('kode_tujuan');
    }

    $outcome = $rs->getString('outcome');
    $target_outcome = $rs->getString('target_outcome');
    $catatan = $rs->getString('catatan');
    $ranking = $rs->getInt('ranking');
    $kode_organisasi = $rs->getString('kode_urusan');
    $kode_program2 = $rs->getString('kode_program2');
    $kode_permen = $rs->getString('kode_permen');
    $output = $rs->getString('output');
    $kelompok_sasaran = $rs->getString('kelompok_sasaran');
}
?>

<table style="empty-cells: show;" border="0" cellpadding="3" cellspacing="0" width="100%">
    <tbody>
        <tr>
            <td colspan="3" style="border: 1px solid rgb(0, 0, 0);">
                <div align="center"><span class="style1 font12"><strong><span class="style3" style="font-size: smaller">RENCANA KERJA DAN ANGGARAN</span><br></strong></span></div>
                <div align="center"><span class="font12 style1 style2"><span class="font12 style3 style1" style="font-size: smaller"><strong>SATUAN KERJA PERANGKAT DAERAH </strong></span></span></div>
            </td>
            <td rowspan="2" style="border: 1px solid rgb(0, 0, 0);"><div class="style3" align="center" style="font-size: smaller"><strong>Formulir<br>
                        RKA-SKPD 2.2.1 </strong></div></td>
        </tr>
        <tr>

            <td colspan="3" style="border: 1px solid rgb(0, 0, 0);"><div align="center" style="font-size: smaller"><span class="style3">KOTA SURABAYA <br>
                    </span></div>
                <div align="center"><span class="style3" style="font-size: smaller">TAHUN ANGGARAN <?php echo sfConfig::get('app_tahun_default'); ?></span></div></td>
        </tr>
        <tr>
            <td colspan="4" style="border: 1px solid rgb(0, 0, 0);">
                <table border="0" width="100%">
                    <?php
                    $kode_per = explode(".", $kode_organisasi);
                    ?>
                    <tbody><tr align="left" valign="top">
                            <td nowrap="nowrap" width="194">
                                <span class="style3" style="font-size: smaller">Urusan Pemerintahan </span>
                            </td>
                            <?php
                            $query = "select *
			from " . sfConfig::get('app_default_schema') . ".master_program2 ms
			where ms.kode_program2='" . $kode_program2 . "'";
                            $con = Propel::getConnection();
                            $stmt = $con->prepareStatement($query);
                            $rs1 = $stmt->executeQuery();
                            while ($rs1->next()) {
                                $nama_program2 = $rs1->getString('nama_program2');
                            }

                            $kode_program22 = substr($kode_program2, 5, 2);
                            if (substr($kode_program2, 0, 4) == 'X.XX') {
                                $kode_urusan = substr($kode_per[0] . '.' . $kode_per[1], 0, 4);
                            } else {
                                $kode_urusan = substr($kode_program2, 0, 4);
                            }

                            $kode_parameter_urusan = $kode_per[0] . '.' . $kode_per[1];

                            $queryU = "select *
                            from " . sfConfig::get('app_default_schema') . ".master_urusan mu
                            where mu.kode_urusan='" . $kode_parameter_urusan . "'";
                            $con = Propel::getConnection();
                            $stmt = $con->prepareStatement($queryU);
                            $rsU = $stmt->executeQuery();
                            while ($rsU->next()) {
                                $nama_urusan = $rsU->getString('nama_urusan');
                            }
                            ?>
                            <td width="10"><span class="style3" style="font-size: smaller">:</span></td>
                            <td ><span class="style3" style="font-size: smaller"><?php echo $kode_per[0] . '.' . $kode_per[1] . ' ' . $nama_urusan ?></span></td>
                        </tr>
                        <tr align="left" valign="top">
                            <td><span class="style3" style="font-size: smaller">Organisasi</span></td>
                            <td><span class="style3" style="font-size: smaller">:</span></td>
                            <td><span class="style3" style="font-size: smaller"><?php echo /* $kode_program2 */$kode_permen . ' ' . $organisasi ?></span></td>

                        </tr>

                        <tr align="left" valign="top">
                            <td><span class="style3" style="font-size: smaller">Kegiatan</span></td>
                            <td><span class="style3" style="font-size: smaller">:</span></td>
                            <td><span class="Font8v style3" style="font-size: smaller"><?php echo $kegiatan_kode . ' ' ?></span><span class="Font8v style3" style="font-size: smaller"><?php echo $nama_kegiatan ?></span></td>
                        </tr>
                        <?php
                        $misi_name = '';
                        $query = "select *
	from " . sfConfig::get('app_default_schema') . ".master_misi ms
	where ms.kode_misi='" . $kode_misi . "'";
                        $con = Propel::getConnection();
                        $stmt = $con->prepareStatement($query);
                        $rs1 = $stmt->executeQuery();
                        while ($rs1->next()) {
                            $misi_name = $rs1->getString('nama_misi');
                        }
                        ?>
                        <tr align="left" valign="top">
                            <td><span class="style3" style="font-size: smaller">Visi</span></td>
                            <td><span class="style3" style="font-size: smaller">:</span></td>
                            <td><span class="style3" style="font-size: smaller">Menuju Surabaya Lebih Baik Sebagai Kota Jasa dan Perdagangan yang Cerdas, Manusiawi, Bermartabat, dan Berwawasan Lingkungan</span></td>
                        </tr>
                        <tr align="left" valign="top">
                            <td><span class="style3" style="font-size: smaller">Misi</span></td>

                            <td><span class="style3" style="font-size: smaller">:</span></td>
                            <td ><span class="style3" style="font-size: smaller"><?php echo $misi_name ?> </span></td>
                        </tr>
                        <?php
                        $tujuan = '';
                        $query = "select nama_tujuan
			from " . sfConfig::get('app_default_schema') . ".master_tujuan mj
			where mj.kode_misi='" . $kode_misi . "' and mj.kode_tujuan='" . $kode_tujuan . "'";
                        $con = Propel::getConnection();
                        $stmt = $con->prepareStatement($query);
//                        echo $query;exit;
                        $rs1 = $stmt->executeQuery();
                        while ($rs1->next()) {
                            $tujuan = $rs1->getString('nama_tujuan');
                        }
                        ?>
                        <tr align="left" valign="top">
                            <td><span class="style3" style="font-size: smaller">Tujuan</span></td>
                            <td><span class="style3" style="font-size: smaller">:</span></td>

                            <td><span class="style3" style="font-size: smaller"> <?php echo $tujuan ?>  </span></td>
                        </tr>
                        <?php
                        $query = "select nama_program from " . sfConfig::get('app_default_schema') . ".master_program where kode_program ilike '$kode_program' and kode_tujuan ilike '%$kode_tujuan%'";
                        $stmt = $con->prepareStatement($query);
                        $rs1 = $stmt->executeQuery();
                        while ($rs1->next()) {
                            $program = $rs1->getString('nama_program');
                        }
                        ?>
                        <tr align="left" valign="top">
                            <td><span class="style3" style="font-size: smaller">Program RPJM </span></td>
                            <td><span class="style3" style="font-size: smaller">:</span></td>
                            <td><span class="style3" style="font-size: smaller"> <?php echo $program ?>  </span></td>
                        </tr>
                        <?php
                        $d = new Criteria();
                        //$d->add(MasterProgram2Peer::KODE_PROGRAM, $kode_program);
                        $d->add(MasterProgram2Peer::KODE_PROGRAM2, $program_p_13);
                        $dx = MasterProgram2Peer::doSelectOne($d);
                        if ($dx) {
                            $program2 = $dx->getNamaProgram2();
                        } else {
                            $program2 = substr($kode_per[0] . '.' . $kode_per[1], 0, 4) . '.' . $kode_program;
                        }
                        ?>
                        <tr align="left" valign="top">
                            <td><span class="style3" style="font-size: smaller">Program P13</span></td>
                            <td><span class="style3" style="font-size: smaller">:</span></td>
                            <td><span class="style3" style="font-size: smaller"><?php echo $program2 ?> </span></td>
                        </tr>
                        <?php
                        $query = "select rd.komponen_harga, rd.volume, rd.pajak, rd.subtitle
			from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail rd
			where rd.kegiatan_code = '" . $kode_kegiatan . "' and rd.unit_id = '" . $unit_id . " and rd.status_hapus=false'
			order by rd.subtitle";
                        $con = Propel::getConnection();
                        $stmt = $con->prepareStatement($query);
                        //$stmt->setInt(1, $this->getId());
                        //$stmt->setLimit($max);
                        $rs = $stmt->executeQuery();
                        //split("pembatas",string,);
                        $total = 0;
                        $harga = 0;
                        $harga_awal_kena_pajak = 0;
                        $subtitle_array = array();
                        $subtitle_array_harga = array();
                        $nama_subtitle = '';
                        $total_subtitle = 0;
                        $total_subtitle_harga = 0;
                        while ($rs->next()) {
                            if ($nama_subtitle == '') {
                                $nama_subtitle = $rs->getString('subtitle');
                                $subtitle_array[] = $nama_subtitle;
                            } else if ($nama_subtitle != $rs->getString('subtitle')) {
                                $nama_subtitle = $rs->getString('subtitle');
                                $subtitle_array[] = $nama_subtitle;
                                $subtitle_array_harga[] = $total_subtitle_harga;
                                $total_subtitle = 0;
                                $total_subtitle_harga = 0;
                            }
                            $volume = $rs->getString('volume');
                            $tunai = $rs->getString('komponen_harga');
                            $harga_awal = ($rs->getString('volume') * $rs->getString('komponen_harga'));
                            $total_subtitle = $harga_awal;
                            $harga_awal_kena_pajak = $harga_awal - (($rs->getString('pajak') / 100) * $harga_awal);
                            $total_subtitle = $total_subtitle - (($rs->getString('pajak') / 100) * $total_subtitle);
                            $total = $total + $harga_awal_kena_pajak;
                            $total_subtitle_harga = $total_subtitle_harga + $total_subtitle;
                        }
                        $subtitle_array_harga[] = $total_subtitle_harga;
                        ?>
                        <tr align="left" valign="top">
                            <?php
                            $query2 = "select sum(nilai_anggaran) as hasil
			from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail rd
			where rd.kegiatan_code = '" . $kode_kegiatan . "' and rd.unit_id = '" . $unit_id . "' and rd.status_hapus=false";
                            $con = Propel::getConnection();
                            $stmt5 = $con->prepareStatement($query2);
                            $total_rekening = $stmt5->executeQuery();
                            while ($total_rekening->next()) {
                                $total = $total_rekening->getString('hasil');
                            }
                            ?>
                            <td><span class="style3" style="font-size: smaller">Total Nilai</span></td>
                            <td><span class="style3" style="font-size: smaller">:</span></td>
                            <td><span class="style3" style="font-size: smaller">Rp.<?php echo number_format($total, 0, ',', '.') ?>,00</span></td>
                        </tr>
                        <tr valign="top">
                            <td><span class="style3" style="font-size: smaller">Output Kegiatan</span></td>
                            <td><span class="style3" style="font-size: smaller">:</span></td>
                            <td><?php
                                $output = str_replace("#", "|", $output);
                                $arr_output = explode("|", $output);
                                ?>
                                <table border="1"  align="left" width="70%" cellspacing="0">
                                    <tr align='center'>
                                        <td style="border: 1px solid"><span class="style5" style="font-size: smaller">Tolak Ukur Kinerja</span></td>
                                        <td style="border: 1px solid"><span class="style5" style="font-size: smaller">Target</span></td>
                                        <td style="border: 1px solid"><span class="style5" style="font-size: smaller">Satuan</span></td>
                                    </tr>
                                    <?php
                                    for ($i = 0; $i < count($arr_output); $i = $i + 3) {
                                        ?>
                                        <tr class="sf_admin_row_1" align='center'>
                                            <td style="border: 1px solid"><span class="style5" style="font-size: smaller"><?php echo $arr_output[$i] ?></span></td>
                                            <td style="border: 1px solid"><span class="style5" style="font-size: smaller"><?php echo $arr_output[$i + 1] ?></span></td>
                                            <td style="border: 1px solid"><span class="style5" style="font-size: smaller"><?php echo $arr_output[$i + 2] ?></span></td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </table>
                            </td>
                        </tr>

                        <tr valign="top">
                            <td><span class="style3" style="font-size: smaller">Output</span></td>
                            <td><span class="style3" style="font-size: smaller">:</span></td>
                            <td>

                                <table border="1" align="left" width="95%" cellspacing="0">

                                    <tr bgcolor="#ffffff" style="border: 1px solid">
                                        <td class="style3" width="223" style="border: 1px solid;"><div class="style5" align="center" style="font-size: smaller">Subtitle</div></td>
                                    <!--    <td width="218" style="border: 1px solid"><div class="style5" align="center" style="font-size: smaller">Output</div></td>-->
                                        <td width="97" style="border: 1px solid"><div class="style5" align="center" style="font-size: smaller">Target</div></td>
                                        <td width="102" style="border: 1px solid"><div class="style5" align="center" style="font-size: smaller">Satuan</div></td>
                                    </tr>
                                    <?php
                                    $query = "select distinct(rd.subtitle)
			from " . sfConfig::get('app_default_schema') . ".dinas_subtitle_indikator rd
			where rd.kegiatan_code = '" . $kode_kegiatan . "' and rd.unit_id = '" . $unit_id . "'";

                                    $con = Propel::getConnection();
                                    $stmt = $con->prepareStatement($query);
                                    $rs = $stmt->executeQuery();
                                    while ($rs->next()) {
                                        $subtitles = $rs->getString('subtitle');
                                        $d = new Criteria();
                                        //$d -> setOffset(10);
                                        $d->add(DinasSubtitleIndikatorPeer::UNIT_ID, $unit_id);
                                        $d->add(DinasSubtitleIndikatorPeer::KEGIATAN_CODE, $kode_kegiatan);
                                        $d->add(DinasSubtitleIndikatorPeer::SUBTITLE, $subtitles);
                                        $d->setDistinct(DinasSubtitleIndikatorPeer::SUBTITLE);
                                        $d->addAscendingOrderByColumn(DinasSubtitleIndikatorPeer::SUBTITLE);
                                        $v = DinasSubtitleIndikatorPeer::doSelect($d);
                                        $subtitle = '';
                                        foreach ($v as $vs) {

                                            $subtitle = $vs->getSubtitle();
                                            $output = $vs->getIndikator();
                                            $target = $vs->getNilai();
                                            $satuan = $vs->getSatuan();
                                            ?>
                                            <tr>
                                                <td style="border: 1px solid"><span class="style5" style="font-size: smaller"><?php echo $subtitle ?></span></td>
                                                <!--<td style="border: 1px solid"><span class="Font8v style5" style="font-size: smaller"><?php echo $output ?></span></td>-->
                                                <td style="border: 1px solid"><div class="style5" align="left" style="font-size: smaller"><div align="center" style="font-size: smaller"><span class="Font8v" style="font-size: smaller">
                                                                <?php echo $target ?></span></td>
                                                            <td style="border: 1px solid"><div class="style5" align="center" style="font-size: smaller"><span class="Font8v" style="font-size: smaller">
                                                                        <?php echo $satuan ?></span></td>
                                                            </tr>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                    </tbody>
                                                    </table>
                                                    </td>
                                                    </tr>

                                                    </tbody>
                                                    </table></td>

                                                    </tr>
                                                    <tr>
<!--                                                        <td><span class="style3" style="font-size: smaller">Kelompok Sasaran Kegiatan </span></td>
                                                        <td><span class="style3" style="font-size: smaller">:</span></td>
                                                        <td><span class="style3" style="font-size: smaller"><?php echo $kelompok_sasaran ?></span></td>-->
                                                        <td colspan="4" style="border: 1px solid rgb(0, 0, 0);" width="30%">
                                                            <span class="style3" style="font-size: smaller; width: 194px">Kelompok Sasaran Kegiatan</span>
                                                            <span class="style3" style="font-size: smaller; width: 10px">:</span>
                                                            <span class="style3" style="font-size: smaller;"><?php echo $kelompok_sasaran ?></span>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td colspan="4" style="border: 1px solid rgb(0, 0, 0);"><div align="center"><span class="style3" style="font-size: smaller">Rincian Anggaran Belanja Langsung</span></div>
                                                            <div align="center"><span class="style3" style="font-size: smaller">Menurut Program dan Per Kegiatan Satuan Kerja Perangkat Daerah </span></div></td>
                                                    </tr>
                                                    </tbody>
                                                    </table>
                                                    <table bgcolor="#ffffff" border="0" cellpadding="3" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td align="center" style="border: 1px solid rgb(0, 0, 0);">&nbsp;</td>
                                                            <td align="center" style="border: 1px solid rgb(0, 0, 0);"><span class="style3" style="font-size: smaller"><strong>Komponen</strong></span></td>
                                                            <td align="center" style="border: 1px solid rgb(0, 0, 0);"><span class="style3" style="font-size: smaller"><strong>Satuan</strong></span></td>
                                                            <td align="center" width="20%" style="border: 1px solid rgb(0, 0, 0);"><span class="style3" style="font-size: smaller"><strong>Koefisien</strong></span></td>
                                                            <td align="center" style="border: 1px solid rgb(0, 0, 0);"><span class="style3" style="font-size: smaller"><strong>Harga</strong></span></td>
                                                            <td align="center" style="border: 1px solid rgb(0, 0, 0);"><span class="style3" style="font-size: smaller"><strong>Hasil</strong></span></td>
                                                            <td align="center" style="border: 1px solid rgb(0, 0, 0);"><span class="style3" style="font-size: smaller"><strong>PPN</strong></span></td>
                                                            <td align="center" style="border: 1px solid rgb(0, 0, 0);"><span class="style3" style="font-size: smaller"><strong>Total</strong></span></td>
                                                            <td align="center" style="border: 1px solid rgb(0, 0, 0);"><span class="style3" style="font-size: smaller"><strong>Belanja</strong></span></td>
                                                        </tr>
                                                        <?php
                                                        $total = 0;
                                                        $totalrek = 0;
                                                        $totalsub = 0;
                                                        $pertama = TRUE;

                                                        $c = new Criteria();
                                                        $c->add(PrintRkaPakPeer::UNIT_ID, $unit_id);
                                                        $c->add(PrintRkaPakPeer::KEGIATAN_CODE, $kegiatan_kode);
                                                        $c->addDescendingOrderByColumn(PrintRkaPakPeer::ID);
                                                        $c->setLimit(1);
                                                        $rs = PrintRkaPakPeer::doSelectOne($c);
                                                        $id_print_rka = $rs->getId();
                                                        $token = $rs->getToken();

                                                        $c = new Criteria();
                                                        $c->add(PrintRkaPakDetailPeer::ID_PRINT_RKA_PAK, $id_print_rka);
                                                        $c->addAscendingOrderByColumn(PrintRkaPakDetailPeer::SUBTITLE);
                                                        $c->addAscendingOrderByColumn(PrintRkaPakDetailPeer::SUBSUBTITLE);
                                                        $c->addAscendingOrderByColumn(PrintRkaPakDetailPeer::REKENING_CODE);
                                                        $c->addAscendingOrderByColumn(PrintRkaPakDetailPeer::NAMA_KOMPONEN);
                                                        $items = PrintRkaPakDetailPeer::doSelect($c);

                                                        foreach ($items as $item) {
                                                            //subtitle
                                                            if ($subtitle != $item->getSubtitle()) {
                                                                if (!$pertama) {
                                                                    //total rekening sebelum
                                                                    echo '<tr bgcolor="#ffffff">';
                                                                    echo '<td colspan="7" class="Font8v style3" align="right" style="border: 1px solid rgb(0, 0, 0); font-size: smaller;">';
                                                                    echo 'Total ' . $rekening_name . ' :</td>';
                                                                    echo '<td class="Font8v style3" align="right" nowrap="nowrap" style="border: 1px solid rgb(0, 0, 0); font-size: smaller;">' . number_format(round($totalrek, 0), 0, ',', '.') . '</td>';
                                                                    echo '<td class="Font8v style3" align="right" style="border: 1px solid rgb(0, 0, 0); font-size: smaller;">&nbsp;</td>';
                                                                    echo '</tr>';
                                                                    $totalrek = 0;
                                                                    //total subtitle sebelum
                                                                    echo '<tr bgcolor="#ffffff">';
                                                                    echo '<td colspan="7" class="Font8v style3" align="right" style="border: 1px solid rgb(0, 0, 0); font-size: smaller;">';
                                                                    echo '<b>Total ' . $subtitle . '  :</td></b>';
                                                                    echo '<td class="Font8v style3" align="right" nowrap="nowrap" style="border: 1px solid rgb(0, 0, 0); font-size: smaller;"><b>' . number_format(round($totalsub, 0), 0, ',', '.') . '</b></td>';
                                                                    echo '<td class="Font8v style3" align="right" style="border: 1px solid rgb(0, 0, 0); font-size: smaller;">&nbsp;</td>';
                                                                    echo '</tr>';
                                                                    $totalsub = 0;
                                                                }
                                                                ?>
                                                                <tr align="left" bgcolor="white"><td colspan="9" style="border: 1px solid rgb(0, 0, 0);"><span class="style3" style="font-size: smaller"><strong><?php echo ':: ' . $item->getSubtitle() ?></strong></span></td></tr>
                                                                <?php
                                                                $query = "select sum(hasil*((pajak+100)/100)) as jml
                                                                        from " . sfConfig::get('app_default_schema') . ".print_rka_pak_detail
                                                                        where subtitle ilike '" . $item->getSubtitle() . "' and subsubtitle='" . $item->getSubsubtitle() . "' and id_print_rka_pak=$id_print_rka";
                                                                $stmt = $con->prepareStatement($query);
                                                                $rs = $stmt->executeQuery();
                                                                $rs->next();
                                                                $total_subsubtitle = $rs->getString('jml');

                                                                echo '<tr bgcolor="#ffffff">';
                                                                echo '<td colspan="7" class="Font8v style3" align="left" style="border: 1px solid rgb(0, 0, 0); font-size: smaller;">';
                                                                echo '<b><i>.:. ' . $item->getSubsubtitle();
                                                                '</td></i></b>';
                                                                echo '<td class="Font8v style3" align="right" nowrap="nowrap" style="border: 1px solid rgb(0, 0, 0); font-size: smaller;"><b>' . number_format(round($total_subsubtitle, 0), 0, ',', '.') . '</b></td>';
                                                                echo '<td class="Font8v style3" align="right" style="border: 1px solid rgb(0, 0, 0); font-size: smaller;">&nbsp;</td>';
                                                                echo '</tr>';

                                                                $c = new Criteria();
                                                                $c->add(RekeningPeer::REKENING_CODE, $item->getRekeningCode());
                                                                $rs = RekeningPeer::doSelectOne($c);
                                                                $rekening_name = $rs->getRekeningName();

                                                                $c = new Criteria();
                                                                $c->add(KelompokBelanjaPeer::BELANJA_CODE, substr($item->getRekeningCode(), 0, 5));
                                                                $belanja = KelompokBelanjaPeer::doSelectOne($c);
                                                                $nama_belanja = $belanja->getBelanjaName();
                                                                echo '<tr bgcolor="white"><td style="border: 1px solid rgb(0, 0, 0);"> :</td><td colspan="8" style="border: 1px solid rgb(0, 0, 0); font-size: smaller;">' . $item->getRekeningCode() . ' ' . $rekening_name . '</td></tr>';
                                                                $subsubtitle = $item->getSubsubtitle();
                                                                $rekening_code = $item->getRekeningCode();
                                                            }
                                                            $subtitle = $item->getSubtitle();

                                                            //subsubtitle
                                                            if ($subsubtitle != $item->getSubsubtitle()) {
                                                                //total rekening sebelum
                                                                if (!$pertama) {
                                                                    echo '<tr bgcolor="#ffffff">';
                                                                    echo '<td colspan="7" class="Font8v style3" align="right" style="border: 1px solid rgb(0, 0, 0); font-size: smaller;">';
                                                                    echo 'Total ' . $rekening_name . ' :</td>';
                                                                    echo '<td class="Font8v style3" align="right" nowrap="nowrap" style="border: 1px solid rgb(0, 0, 0); font-size: smaller;">' . number_format(round($totalrek, 0), 0, ',', '.') . '</td>';
                                                                    echo '<td class="Font8v style3" align="right" style="border: 1px solid rgb(0, 0, 0); font-size: smaller;">&nbsp;</td>';
                                                                    echo '</tr>';
                                                                    $totalrek = 0;
                                                                }
                                                                $query = "select sum(hasil*((pajak+100)/100)) as jml
                                                                        from " . sfConfig::get('app_default_schema') . ".print_rka_pak_detail
                                                                        where subtitle ilike '$subtitle' and subsubtitle='" . $item->getSubsubtitle() . "' and id_print_rka_pak=$id_print_rka";
                                                                $stmt = $con->prepareStatement($query);
                                                                $rs = $stmt->executeQuery();
                                                                $total_subsubtitle = $rs->getInt('jml');

                                                                echo '<tr bgcolor="#ffffff">';
                                                                echo '<td colspan="7" class="Font8v style3" align="left" style="border: 1px solid rgb(0, 0, 0); font-size: smaller;">';
                                                                echo '<b><i>.:. ' . $item->getSubsubtitle();
                                                                '</td></i></b>';
                                                                echo '<td class="Font8v style3" align="right" nowrap="nowrap" style="border: 1px solid rgb(0, 0, 0); font-size: smaller;"><b>' . number_format(round($total_subsubtitle, 0), 0, ',', '.') . '</b></td>';
                                                                echo '<td class="Font8v style3" align="right" style="border: 1px solid rgb(0, 0, 0); font-size: smaller;">&nbsp;</td>';
                                                                echo '</tr>';
                                                                $c = new Criteria();
                                                                $c->add(RekeningPeer::REKENING_CODE, $item->getRekeningCode());
                                                                $rs = RekeningPeer::doSelectOne($c);
                                                                $rekening_name = $rs->getRekeningName();

                                                                $c = new Criteria();
                                                                $c->add(KelompokBelanjaPeer::BELANJA_CODE, substr($item->getRekeningCode(), 0, 5));
                                                                $belanja = KelompokBelanjaPeer::doSelectOne($c);
                                                                $nama_belanja = $belanja->getBelanjaName();
                                                                echo '<tr bgcolor="white"><td style="border: 1px solid rgb(0, 0, 0);"> :</td><td colspan="8" style="border: 1px solid rgb(0, 0, 0); font-size: smaller;">' . $item->getRekeningCode() . ' ' . $rekening_name . '</td></tr>';
                                                                $rekening_code = $item->getRekeningCode();
                                                            }
                                                            $subsubtitle = $item->getSubsubtitle();

                                                            //rekening
                                                            if ($rekening_code != $item->getRekeningCode()) {
                                                                //total rekening sebelum
                                                                if (!$pertama) {
                                                                    echo '<tr bgcolor="#ffffff">';
                                                                    echo '<td colspan="7" class="Font8v style3" align="right" style="border: 1px solid rgb(0, 0, 0); font-size: smaller;">';
                                                                    echo 'Total ' . $rekening_name . ' :</td>';
                                                                    echo '<td class="Font8v style3" align="right" nowrap="nowrap" style="border: 1px solid rgb(0, 0, 0); font-size: smaller;">' . number_format(round($totalrek, 0), 0, ',', '.') . '</td>';
                                                                    echo '<td class="Font8v style3" align="right" style="border: 1px solid rgb(0, 0, 0); font-size: smaller;">&nbsp;</td>';
                                                                    echo '</tr>';
                                                                    $totalrek = 0;
                                                                }
                                                                $c = new Criteria();
                                                                $c->add(RekeningPeer::REKENING_CODE, $item->getRekeningCode());
                                                                $rs = RekeningPeer::doSelectOne($c);
                                                                $rekening_name = $rs->getRekeningName();

                                                                $c = new Criteria();
                                                                $c->add(KelompokBelanjaPeer::BELANJA_CODE, substr($item->getRekeningCode(), 0, 5));
                                                                $belanja = KelompokBelanjaPeer::doSelectOne($c);
                                                                $nama_belanja = $belanja->getBelanjaName();
                                                                echo '<tr bgcolor="white"><td style="border: 1px solid rgb(0, 0, 0);"> :</td><td colspan="8" style="border: 1px solid rgb(0, 0, 0); font-size: smaller;">' . $item->getRekeningCode() . ' ' . $rekening_name . '</td></tr>';
                                                            }
                                                            $rekening_code = $item->getRekeningCode();

                                                            //komponen
                                                            echo '<tr bgcolor="#ffffff" valign="top">';
                                                            echo '<td class="Font8v" align="left" style="border: 1px solid rgb(0, 0, 0);">&nbsp;</td>';

                                                            echo '<td class="Font8v" align="left" style="border: 1px solid rgb(0, 0, 0);"><span class="style3" style="font-size: smaller">' . $item->getNamaKomponen() . '</span></td>';
                                                            echo '<td class="Font8v style3" align="center" nowrap="nowrap" style="border: 1px solid rgb(0, 0, 0);"><span class="Font8v" style="font-size: smaller">' . $item->getSatuan() . '</span></td>';
                                                            echo '<td class="Font8v style3" align="center" style="border: 1px solid rgb(0, 0, 0);"><span class="Font8v" style="font-size: smaller">' . $item->getKoefisien() . '</span></td>';
                                                            if (number_format($item->getHarga(), 0, ',', '.') == '0') {
                                                                echo '<td class="Font8v style3" align="right" nowrap="nowrap" style="border: 1px solid rgb(0, 0, 0); font-size: smaller;">' . number_format($item->getHarga(), 4, ',', '.') . '</td>';
                                                            } else {
                                                                echo '<td class="Font8v style3" align="right" nowrap="nowrap" style="border: 1px solid rgb(0, 0, 0); font-size: smaller;">' . number_format($item->getHarga(), 0, ',', '.') . '</td>';
                                                            }
                                                            echo '<td class="Font8v style3" align="right" nowrap="nowrap" style="border: 1px solid rgb(0, 0, 0); font-size: smaller;">' . number_format($item->getHasil(), 0, ',', '.') . '</td>';
                                                            echo '<td class="Font8v style3" align="right" nowrap="nowrap" style="border: 1px solid rgb(0, 0, 0); font-size: smaller;">' . $item->getPajak() . '%</td>';
                                                            $hasil = $item->getHasil();
                                                            $total1 = $hasil + (($item->getPajak() / 100) * $hasil);
                                                            echo '<td class="Font8v style3" align="right" nowrap="nowrap" style="border: 1px solid rgb(0, 0, 0); font-size: smaller;">' . number_format(round($total1, 0), 0, ',', '.') . '</td>';

                                                            $l = strlen($nama_belanja);
                                                            echo '<td class="Font8v style3" align="center" style="border: 1px solid rgb(0, 0, 0); font-size: smaller;">' . substr(ucwords(strtolower($nama_belanja)), 8, $l) . '</td>';
                                                            echo '</tr>';
                                                            $total = $total + $total1;
                                                            $totalsub = $totalsub + $total1;
                                                            $totalrek = $totalrek + $total1;
                                                            $pertama = FALSE;
                                                        }
                                                        //total rekening sebelum
                                                        echo '<tr bgcolor="#ffffff">';
                                                        echo '<td colspan="7" class="Font8v style3" align="right" style="border: 1px solid rgb(0, 0, 0); font-size: smaller;">';
                                                        echo 'Total ' . $rekening_name . ' :</td>';
                                                        echo '<td class="Font8v style3" align="right" nowrap="nowrap" style="border: 1px solid rgb(0, 0, 0); font-size: smaller;">' . number_format(round($totalrek, 0), 0, ',', '.') . '</td>';
                                                        echo '<td class="Font8v style3" align="right" style="border: 1px solid rgb(0, 0, 0); font-size: smaller;">&nbsp;</td>';
                                                        echo '</tr>';
                                                        //total subtitle sebelum
                                                        echo '<tr bgcolor="#ffffff">';
                                                        echo '<td colspan="7" class="Font8v style3" align="right" style="border: 1px solid rgb(0, 0, 0); font-size: smaller;">';
                                                        echo '<b>Total ' . $subtitle . '  :</td></b>';
                                                        echo '<td class="Font8v style3" align="right" nowrap="nowrap" style="border: 1px solid rgb(0, 0, 0); font-size: smaller;"><b>' . number_format(round($totalsub, 0), 0, ',', '.') . '</b></td>';
                                                        echo '<td class="Font8v style3" align="right" style="border: 1px solid rgb(0, 0, 0); font-size: smaller;">&nbsp;</td>';
                                                        echo '</tr>';

                                                        echo '<tr bgcolor="#ffffff">';
                                                        echo '<td colspan="7" class="Font8v style3" align="right" style="border: 1px solid rgb(0, 0, 0); font-size: smaller;">';
                                                        echo '<b>Grand Total  :</b></td>';
                                                        echo '<td class="Font8v style3" align="right" nowrap="nowrap" style="border: 1px solid rgb(0, 0, 0); font-size: smaller;"><b>' . number_format(round($total, 0), 0, ',', '.') . '</b></td>';
                                                        echo '<td class="Font8v style3" align="right">&nbsp;</td>';
                                                        echo '</tr>';
                                                        ?>
                                                    </table>
                                                    <table border="0" width="100%">
                                                        <tbody><tr>
                                                                <?php
                                                                $c = new Criteria();
                                                                $c->add(UnitKerjaPeer::UNIT_ID, $unit_id);
                                                                $cs = UnitKerjaPeer::doSelectOne($c);
                                                                if ($cs) {
                                                                    $kepala = $cs->getKepalaPangkat();
                                                                    $nama = $cs->getKepalaNama();
                                                                    $nip = $cs->getKepalaNip();
                                                                    $unit_name = $cs->getUnitName();
                                                                }
                                                                ?>
                                                                <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0); font-size: smaller;" width="60%">
                                                                    <?php
                                                                        $url = sfConfig::get('app_path_default_sf') . 'index.php/cek_qr/cekPrintRka/id/' . $id_print_rka . '/token/' . $token . '.html';
                                                                        echo image_tag('https://chart.googleapis.com/chart?chs=300x300&cht=qr&chl=' . $url, array('width' => '120', 'heigth' => '120'));
                                                                    ?></td>
                                                                <td class="style4" style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0); font-size: smaller;" align="left" width="30%">Surabaya,
                                                                    <p align="center">
                                                                        <?php
                                                                        if ($unit_id == '0700') {
                                                                            echo strtoupper('INSPEKTUR');
                                                                        } else {
                                                                            ?>
                                                                            KEPALA <?php
                                                                            echo strtoupper($unit_name);
                                                                        }
                                                                        ?> </p>
                                                                    <br>
                                                                    <br>

                                                                    <br>
                                                                    <br>
                                                        <center><?php echo $nama ?><br>
                                                            <?php echo $kepala ?><br>
                                                            NIP : <?php echo $nip ?></center>  </td>
                                                        </tr>

                                                        <tr>
                                                            <td colspan="2">
                                                                <table border="0" cellspacing="0" width="100%" >
                                                                    <tbody><tr>
                                                                            <td colspan="5" style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0); font-size: smaller;">Keterangan : </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="5" style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0); font-size: smaller;">Tanggal Pembahasan : </td>


                                                                        </tr>

                                                                        <tr>
                                                                            <td colspan="5" style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0); font-size: smaller;">Catatan Hasil Pembahasan : </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="5" style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0); font-size: smaller;">1. </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="5" style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0); font-size: smaller;">2. </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="5" style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0); font-size: smaller;">3. </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="5" style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0); font-size: smaller;">4. </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="5" style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0); font-size: smaller;">5. </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="5" style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0);"><div class="style3" align="center" style="font-size: smaller;">
                                                                                    <div align="center" style="font-size: smaller;">Tim Anggaran Pemerintah Daerah </div>
                                                                                </div></td>

                                                                        </tr>
                                                                        <tr>
                                                                            <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0);" width="3%"><div align="center"><span class="style3" style="font-size: smaller;">No</span></div></td>
                                                                            <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0);" width="51%"><div align="center"><span class="style3" style="font-size: smaller;">Nama</span></div></td>
                                                                            <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0);" width="11%"><div align="center"><span class="style3" style="font-size: smaller;">NIP</span></div></td>
                                                                            <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0);" width="21%"><div align="center"><span class="style3" style="font-size: smaller;">Jabatan</span></div></td>
                                                                            <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0);" width="14%"><div align="center"><span class="style3" style="font-size: smaller;">Tanda Tangan </span></div></td>

                                                                        </tr>
                                                                        <tr>
                                                                            <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0); font-size: smaller;">1</td>
                                                                            <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0); font-size: smaller;">&nbsp;</td>
                                                                            <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0); font-size: smaller;">&nbsp;</td>
                                                                            <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0); font-size: smaller;">&nbsp;</td>
                                                                            <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0); font-size: smaller;">&nbsp;</td>
                                                                        </tr>

                                                                        <tr>
                                                                            <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0); font-size: smaller;">2</td>
                                                                            <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0); font-size: smaller;">&nbsp;</td>
                                                                            <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0); font-size: smaller;">&nbsp;</td>
                                                                            <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0); font-size: smaller;">&nbsp;</td>
                                                                            <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0); font-size: smaller;">&nbsp;</td>
                                                                        </tr>
                                                                        <tr>

                                                                            <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0); font-size: smaller;">3</td>
                                                                            <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0); font-size: smaller;">&nbsp;</td>
                                                                            <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0); font-size: smaller;">&nbsp;</td>
                                                                            <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0); font-size: smaller;">&nbsp;</td>
                                                                            <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0); font-size: smaller;">&nbsp;</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0); font-size: smaller;">4</td>

                                                                            <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0); font-size: smaller;">&nbsp;</td>
                                                                            <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0); font-size: smaller;">&nbsp;</td>
                                                                            <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0); font-size: smaller;">&nbsp;</td>
                                                                            <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0); font-size: smaller;">&nbsp;</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>&nbsp;</td>
                                                                            <td>&nbsp;</td>
                                                                            <td>&nbsp;</td>

                                                                            <td>&nbsp;</td>
                                                                            <td>&nbsp;</td>
                                                                        </tr>
                                                                    </tbody></table></td>
                                                        </tr>
                                                        </tbody></table>
