<div class="box box-info">        
    <div class="box-header with-border">
        Laporan Perbandingan Murni-Revisi1 Per Dinas Per Kegiatan Per Belanja
    </div>
    <div class="box-body">
        <table class="table table-bordered">
            <tr>
                <th class="text-center" style="width: 10px"></th>
                <th class="text-center" style="width: 10px"></th>
                <th class="text-center">Dinas - Kegiatan - Belanja</th>
                <th class="text-center">Nilai Murni</th>
                <th class="text-center">Nilai Revisi 1</th>
                <th class="text-center">Selisih</th>
            </tr>
            <?php
            foreach ($hasil as $value):
                if (isset($value['skpd_ok'])):
                    $temp = '';
                    if ($value['skpd_selisih'] != '0') $temp = 'style = "background-color:#fffcce"';
                    ?>
                    <tr><td colspan="5">&nbsp;</td></tr>
                    <tr>
                        <td colspan="3" class="text-left text-bold" <?php echo ($temp!='')? $temp : ''; ?>><?php echo $value['unit_id'] . ' - ' . $value['unit_name'] ?></td>
                        <td class="text-right text-bold" <?php echo ($temp!='')? $temp : ''; ?>><?php echo $value['skpd_murni'] ?></td>
                        <td class="text-right text-bold" <?php echo ($temp!='')? $temp : ''; ?>><?php echo $value['skpd_revisi'] ?></td>
                        <td class="text-right text-bold" <?php echo ($temp!='')? $temp : ''; ?>><?php echo $value['skpd_selisih'] ?></td>
                    </tr>
                <?php endif;
                if (isset($value['kegiatan_ok'])):
                    $temp = '';
                    if ($value['kegiatan_selisih'] != '0') $temp = 'style = "background-color:#fffcce"';
                    ?>
                    <tr>
                        <td style="width: 10px" <?php echo ($temp!='')? $temp : ''; ?>>&nbsp;</td>
                        <td colspan="2" class="text-left text-bold" <?php echo ($temp!='')? $temp : ''; ?>><?php echo $value['kode_kegiatan'] ?>&nbsp;<?php echo $value['nama_kegiatan'] ?></td>
                        <td class="text-right text-bold" <?php echo ($temp!='')? $temp : ''; ?>><?php echo $value['kegiatan_murni'] ?></td>
                        <td class="text-right text-bold" <?php echo ($temp!='')? $temp : ''; ?>><?php echo $value['kegiatan_revisi'] ?></td>
                        <td class="text-right text-bold" <?php echo ($temp!='')? $temp : ''; ?>><?php echo $value['kegiatan_selisih'] ?></td>
                    </tr>
                <?php endif;
                $temp = '';
                if ($value['selisih'] != '0') $temp = 'style = "background-color:#fffcce"';
                ?>
                <tr>
                    <td colspan="2" <?php echo ($temp!='')? $temp : ''; ?>>&nbsp;</td>
                    <td class="text-left" <?php echo ($temp!='')? $temp : ''; ?>><?php echo $value['belanja'] ?></td>
                    <td class="text-right" <?php echo ($temp!='')? $temp : ''; ?>><?php echo $value['murni'] ?></td>
                    <td class="text-right" <?php echo ($temp!='')? $temp : ''; ?>><?php echo $value['revisi'] ?></td>
                    <td class="text-right" <?php echo ($temp!='')? $temp : ''; ?>><?php echo $value['selisih'] ?></td>
                </tr>
            <?php endforeach; ?>
            <tr>
                <td colspan="3" class="text-bold text-center bg-green-active">Total</td>
                <td class="text-bold text-right bg-green-active"><?php echo $murni ?></td>
                <td class="text-bold text-right bg-green-active"><?php echo $revisi ?></td>
                <td class="text-bold text-right bg-green-active"></td>
            </tr>
        </table>       
    </div>
</div>