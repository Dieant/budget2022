<?php use_helper('Object', 'Javascript') ?>
<div class="box box-info">        
    <div class="box-header with-border">
        Pilih Rekening
    </div>
    <div class="box-body">
        <select class="form-control" name="rek" id="rek">
            <option value="">---Pilih Rekening ---</option>
            <?php foreach ($rek as $value) { ?>
                <option value="<?php echo $value->getRekeningCode() ?>"><?php echo $value->getRekeningCode() . ' - ' . $value->getRekeningName() ?></option>
            <?php }
            ?>            
        </select>
    </div>
</div>
<script>
    $("#rek").change(function() {
        var id = $(this).val();
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/report/tampilReport/b/rrdk/rek/" + id + ".html",
            context: document.body
        }).done(function(msg) {
            $('#isi_dalam').html(msg);
        });
    });
</script>