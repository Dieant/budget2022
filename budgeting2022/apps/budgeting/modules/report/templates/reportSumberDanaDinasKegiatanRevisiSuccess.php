<?php use_helper('Form', 'Object', 'Javascript') ?>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="fas fa-align-left"></i> Laporan Sumber Dana <?php echo $nama_sistem; ?>
                        </h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Pilih Sumber Dana</label>
                                    <?php
                                        $c = new Criteria();
                                        $c->addAscendingOrderByColumn(MasterSumberDanaPeer::SUMBER_DANA);
                                        $v = MasterSumberDanaPeer::doSelect($c);  
                                        echo select_tag('sumber_dana_id', objects_for_select($v, 'getId', 'getSumberDana', $filter_sumber_dana, 'include_custom=---Pilih Sumber Dana---'), array('class' => 'form-control js-example-basic-single', 'style' => 'width:100%'));
                                    ?>
                                </div>
                            </div>
                            <!-- /.col -->
                            <!-- <div class="col-md-3">
                                <div class="form-group">
                                    <label>Perangkat Daerah</label>
                                    <?php
                                        $e = new Criteria();
                                        $e->add(UnitKerjaPeer::UNIT_ID, '9999', Criteria::NOT_IN);
                                        $e->addAscendingOrderByColumn(UnitKerjaPeer::UNIT_NAME);
                                        $unit_kerja = UnitKerjaPeer::doSelect($e);
                                        echo select_tag('unit_id', objects_for_select($unit_kerja, 'getUnitId', 'getUnitName', (isset($filter_unit_kerja) ? $filter_unit_kerja : NULL), array('include_custom' => '------Pilih Perangkat Daerah------')), array('id' => 'unit_id', 'class' => 'form-control js-example-basic-single'));
                                    ?>
                                </div>
                            </div> -->
                            <!-- /.col -->
                        </div>
                    </div>
                    <div class="card-body table-responsive p-0">
                        <table class="table table-hover">
                            <thead class="head_peach">
                                <tr>
                                    <th class="text-center">Dinas</th>
                                    <th class="text-center">Kode</th>
                                    <th class="text-center">Sub Kegiatan</th>
                                    <th class="text-center">Detail Kegiatan</th>
                                    <th class="text-center">Nama Komponen</th>
                                    <th class="text-center">Nilai</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $pagu_kota = 0;
                                $temp_kegiatan = '';
                                $temp_pd = '';
                                $temp_nama = '';
                                $anggaran_keg = 0;
                                $anggaran_pd = 0;   
                                $numItems = count($rekening);
                                $i=0;
                                foreach ($rekening as $value):
                                //$temp_nama = $value['unit_name'];?>
                                                             
                                 <?php if ($temp_kegiatan != $value['kode_kegiatans']): ?> 
                                     <?php if($anggaran_keg > 0): ?>                            
                                            <!-- <tr>
                                                <td colspan="4" class="text-left text-bold" style="background-color:#e8e6e6">&nbsp;</td>
                                                <td colspan="1"  class="text-right text-bold" style="background-color:#e8e6e6">Anggaran Sub Kegiatan : </td>
                                                <td colspan="1" class="text-right text-bold" style="background-color:#e8e6e6">
                                                <?php echo number_format($anggaran_keg, 0, ",", ".") ?></td>
                                            </tr> -->
                                        <?php $anggaran_keg=0; 
                                        endif;
                                    endif;
                                 ?>
                                 <?php if ($temp_pd != $value['unit_ids']): ?> 
                                            <?php if($anggaran_pd > 0): ?>
                                                <tr>         
                                                    <td colspan="5" class="text-right text-bold" style="background-color:#dbd7d7">
                                                        <?php echo 'Anggaran '.$temp_nama.' : ' ?>
                                                    </td>                               
                                                    <td class="text-right text-bold" style="background-color:#dbd7d7">
                                                        <?php echo  'Rp ' . number_format($anggaran_pd, 0, ",", ".") ?>
                                                    </td>
                                                </tr> 
                                            <!-- <tr>
                                           
                                            <td colspan="6" class="text-left text-bold" style="background-color:#dbd7d7" >
                                               <?php //echo 'Anggaran '.$temp_nama.' : '.number_format($anggaran_pd, 0, ",", ".") ?></td>
                                            </tr> -->
                                            <?php 
                                            $anggaran_pd=0; 
                                            endif; 
                                        endif;?>
                                <?php                                  
                                    if (isset($value['skpd_ok'])):
                                        ?>
                                    <tr>
                                        <td colspan="6">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td colspan="6" class="text-left text-bold">
                                            <?php echo $value['unit_id'] . ' - ' . $value['unit_name'] ?></td>
                                            
                                    </tr>
                                    <?php endif; ?>

                                    <?php //if (isset($value['kegiatan_ok'])): ?>
                                        <?php
                                                // $query2 = "Select kegiatan_id from " . sfConfig::get('app_default_schema') . ".dinas_master_kegiatan where kode_kegiatan = '". $value['kode_kegiatan'] ."'";
                                                // $con = Propel::getConnection();
                                                // $stmt = $con->prepareStatement($query2);
                                                // $rs = $stmt->executeQuery();
                                                // while ($rs->next()) {
                                                //     $kegiatan_id = $rs->getString('kegiatan_id');
                                                // }
                                            ?>
                                        <!-- <tr>
                                            <td colspan="4" class="text-left text-bold">
                                            <?php //echo $kegiatan_id . ' - ' . $value['nama_kegiatan'] ?></td>
                                            <td colspan="2" class="text-left text-bold">
                                            </td>
                                        </tr> -->
                                    <?php //endif; ?>
                                    <tr>
  <!--                                       <?php //if (isset($value['kegiatan_ok'])): ?>
                                        <td rowspan="<?php echo $value['span']; ?>">&nbsp;</td>
                                        <?php
                                            $query2 = "Select kegiatan_id from " . sfConfig::get('app_default_schema') . ".dinas_master_kegiatan where kode_kegiatan = '". $value['kode_kegiatan'] ."'";
                                            $con = Propel::getConnection();
                                            $stmt = $con->prepareStatement($query2);
                                            $rs = $stmt->executeQuery();
                                            while ($rs->next()) {
                                                $kegiatan_id = $rs->getString('kegiatan_id');
                                            }
                                        ?>
                                        <td class="text-left" rowspan="<?php echo $value['span']; ?>"
                                            style="word-wrap: break-word">
                                            <?php //echo $kegiatan_id . ' - ' . $value['nama_kegiatan'] ?></td>
                                        <?php //endif; ?> -->
                                        <td class="text-left">&nbsp;</td>
                                        <td class="text-left"><?php echo $value['kode_kegiatans'] ?></td>
                                        <td class="text-left"><?php echo $value['nama_kegiatans'] ?></td>
                                        <td class="text-left"><?php echo $value['detail_kegiatan'] ?></td>
                                        <td class="text-left">
                                            <?php 
                                            echo 
                                            $value['komponen_name'].' '.$value['detail_name']. ' - <span class="badge badge-primary">'. $value['sumber_dana'] .'</span>';
                                            ?>
                                        </td>
                                        <td class="text-right">Rp <?php echo number_format($value['nilai'], 0, ",", ".") ?>
                                        </td>                                        
                                    </tr>                                
            
                                <?php
                                    $anggaran_keg  += $value['nilai'];
                                    $temp_kegiatan = $value['kode_kegiatans'];
                                    $anggaran_pd  += $value['nilai'];
                                    $temp_pd = $value['unit_ids']; 
                                    $temp_nama = $value['unit_names'];                                   
                                    $pagu_kota += $value['nilai'];?>

                                <?php
                                if(++$i === $numItems) {
                                   ?>
                                    <!-- <tr>  
                                            <td colspan="4" class="text-left text-bold" style="background-color:#e8e6e6">&nbsp;</td>
                                            <td colspan="1"  class="text-right text-bold" style="background-color:#e8e6e6">Anggaran Sub Kegiatan : </td>
                                            <td colspan="1" class="text-right text-bold" style="background-color:#e8e6e6">
                                               <?php //echo number_format($anggaran_keg, 0, ",", ".") ?></td>                                       -->
                                            <!-- <td colspan="6" class="text-left text-bold" style="background-color:#e8e6e6">
                                                <?php //echo 'Anggaran Sub Kegiatan - '.$temp_kegiatan.' : '.number_format($anggaran_keg, 0, ",", ".") ?>
                                            </td> -->
                                    <!-- </tr> -->
                                    <tr>         
                                        <td colspan="5" class="text-right text-bold" style="background-color:#dbd7d7">
                                            <?php echo 'Anggaran '.$temp_nama.' : ' ?>
                                        </td>                               
                                        <td class="text-right text-bold" style="background-color:#dbd7d7">
                                            <?php echo  'Rp ' . number_format($anggaran_pd, 0, ",", ".") ?>
                                        </td>
                                    </tr>
                                    <?php
                                }
                                endforeach; 
                                ?>
                                <tr>
                                    <td colspan="6" style="background-color:#2550db;color:white"><b>Total Anggaran = Rp.<?php echo number_format($pagu_kota, 2, ',', '.') ?></b></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $(document).ready(function () {
        $(".js-example-basic-single").select2();
    });
    $("#unit_id").change(function () {
        $('#indicator').show();
        var id = $(this).val();
        var id2 = $("#sumber_dana_id").val();
        if (id == '') {
            id = '0000';
        }
        if (id2 == '') {
            id2 = 'xxx';
        }
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/report/reportSumberDanaDinasKegiatanRevisi/unit_id/" + id + "/sumber_dana_id/" + id2 + ".html",
            context: document.body
        }).done(function (msg) {
            $('#indicator').hide();
            $('#isi').html(msg);
        });

    });
    $("#sumber_dana_id").change(function () {
        $('#indicator').show();
        var id = $(this).val();
        if (id == '') {
            id = '0000';
        }
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/report/reportSumberDanaDinasKegiatanRevisi/sumber_dana_id/" +
                id + ".html",
            context: document.body
        }).done(function (msg) {
            $('#indicator').hide();
            $('#isi').html(msg);
        });

    });
</script>