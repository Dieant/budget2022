<?php use_helper('Form', 'Object', 'Javascript') ?>
<div class="box box-info">
    <div class="box-header with-border">
        Laporan Perbandingan Tabel Komponen - Rincian Detail
    </div>
    <div class="box-body">
        <table class="table table-bordered">
            <tr>
                <th class="text-center">Komponen</th>
                <th class="text-center">Dinas</th>
                <th class="text-center">Kode Kegiatan</th>
                <th class="text-center">Keterangan Koefisien</th>
                <th class="text-center">Harga Komponen</th>
                <th class="text-center">Harga Terambil</th>
            </tr>
            <?php
            $tempKomponen = '';
            foreach ($kegiatans as $value): ?>
                <?php if($tempKomponen != $value['komponen_id']): ?>
                    <?php if(!empty($value['komponen_id'])): ?>
                        <tr><td colspan="6">&nbsp;</td></tr>
                        <tr>
                            <td colspan="6" class="text-left text-bold"><?php echo $value['komponen_id'] . ' - ' . $value['komponen_name'] ?></td>
                        </tr>
                    <?php endif; ?>
                <?php endif; ?>

                <?php if(!empty($value['komponen_id'])): ?>
                    <tr>
                        <td>&nbsp;</td>
                        <td class="text-left" ><?php echo $value['unit_name'] ?></td>
                        <td class="text-left" ><?php echo $value['kode_kegiatan'] ?></td>
                        <td class="text-left" ><?php echo $value['keterangan_koefisien'] ?></td>
                        <td class="text-left" >Rp.<?php echo number_format($value['harga_komponen'], 0, ',', '.') ?></td>
                        <td class="text-left" >Rp.<?php echo number_format($value['harga_rka'], 0, ',', '.') ?></td>
                    </tr>
                    <?php $tempKomponen = $value['komponen_id']; ?>
                <?php endif; ?>
            <?php endforeach; ?>
        </table>
    </div>
</div>