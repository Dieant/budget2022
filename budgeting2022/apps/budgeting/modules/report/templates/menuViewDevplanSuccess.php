<?php use_helper('I18N', 'Date', 'Url', 'Javascript', 'Form', 'Object') ?>

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>Daftar Komponen Kegiatan SKPD <?php echo $kode_kegiatan; ?> Di Devplan</h1>
</section>

<!-- Main content -->
<section class="content">
    <?php include_partial('entri/list_messages'); ?>
    <!-- Default box -->

    <div class="box box-info">
        <div class="box-body">
            <div id="sf_admin_container" style="position:relative;overflow:hidden;padding-bottom:56.25%;padding-top:30px;height:0;">
                <iframe style="position:absolute;width:100%;height:100%;top:0;left:0;" frameborder="0" width="100%" height="100%" src="https://devplan.surabaya.go.id/devplan2022/devplan_rincian.php?pd=<?php echo $unit_id; ?>&kode_kegiatan=<?php echo $kode_kegiatan;?>&dok=kua"></iframe>               
            </div>            
        </div>
    </div>

</section><!-- /.content -->