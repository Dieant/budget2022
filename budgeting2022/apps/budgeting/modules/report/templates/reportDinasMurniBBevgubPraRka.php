<div class="box box-info">        
    <div class="box-header with-border">
        Perbandingan Buku Biru - Pra RKA
    </div>
    <div class="box-body">
        <table class="table table-bordered">
            <tr>
                <th class="text-center">No</th>
                <th class="text-center">Dinas</th>
                <th class="text-center">Kegiatan</th>
                <th class="text-center">Pagu Buku Biru Pra EvaGub</th>
                <th class="text-center">Rincian / Usulan</th>
                <th class="text-center">Selisih</th>
                <th class="text-center">Nilai Disetujui</th>
                <th class="text-center">Rincian - Disetujui</th>
            </tr>
            <?php

            $tot_pag = 0;
            $tot_pagu_tmbhan = 0;
            $tot_pagu_final_pak = 0;
            $tot_rincian_nilai_usulan = 0;
            $tot_disetujui = 0;
            $setujupagu = 0;
            $i = 0;
            $p = 0;
            $q = 0;
            $z = 0;
            $no = 0;
            //$rs->first();
            $rs->first();
            do {
                ?>
                <tr>
                    <td class="text-left">
                        <?php 
                            echo $no++;
                        ?>
                    </td>
                    <td class="text-left">
                        <?php 
                            echo $rs->getString('unit_id') . ' - ' . $rs->getString('unit_name');
                        ?>
                    </td>
                    <td class="text-right">
                        <?php 
                            echo $rs->getString('kegiatan_code');
                            // $tot_pag += $rs_new_que_kua_ppas->getString('tot_anggaran');
                            // echo number_format($rs_new_que_kua_ppas->getString('tot_anggaran'), 0, ",", ".");
                        ?>
                    </td>
                    <td class="text-center">
                        <?php 
                            echo $rs->getString('pagu_bb');
                            // $tot_pagu_tmbhan += $pagu_tmbhan[$i]; 
                            // echo number_format($pagu_tmbhan[$i], 0, ",", ".");
                        ?>
                    </td>

                    <td class="text-right">
                        <?php 
                            echo $rs->getString('rincian_usulan');
                            // $tot_pagu_final_pak += $rs_new_que_kua_ppas->getString('tot_anggaran')+$pagu_tmbhan[$i];
                            // echo number_format($rs_new_que_kua_ppas->getString('tot_anggaran')+$pagu_tmbhan[$i], 0, ",", ".");
                        ?>
                    </td>

                    <td class="text-center">
                        <?php 
                            echo $rs->getString('selisih');
                            // $tot_rincian_nilai_usulan += $smw_rinc[$i]; 
                            // echo number_format($smw_rinc[$i], 0, ",", ".");
                        ?>
                    </td>
                    
                    <td class="text-right">
                        <?php 
                            echo $rs->getString('disetujui');
                        ?>
                        
                    </td>

                    <td class="text-right">
                        <?php 
                            echo $rs->getString('selisih2');
                        ?>
                    </td>

                </tr>

                <?php
            // } while ($rs->next());
            } while ($rs->next());
            ?>

            <tr>
                <td class="text-center text-bold bg-green-active">Total</td>
<!--                 <td class="text-right text-bold bg-green-active">
                    <?php 
                    // echo number_format($tot_pag, 0, ",", ".");
                    ?>
                </td>
                <td class="text-center text-bold bg-green-active">
                    <?php 
                        // echo number_format($tot_pagu_tmbhan, 0, ",", "."); 
                    ?>
                </td>
                <td class="text-right text-bold bg-green-active">
                <?php 
                    // echo number_format($tot_pagu_final_pak, 0, ",", "."); 
                ?>
                </td>
                <td class="text-center text-bold bg-green-active">
                    <?php 
                    // echo number_format($tot_rincian_nilai_usulan, 0, ",", ".");
                    ?>
                </td>
                
                <td class="text-right text-bold bg-green-active">
                <?php 
                // echo number_format($tot_disetujui, 0, ",", ".");
                
                ?>
                </td>

                <td class="text-right text-bold bg-green-active">
                <?php 
                // echo number_format($setujupagu, 0, ",", ".");
                ?>
                    
                </td> -->
            </tr>
        </table>       
    </div>
</div>