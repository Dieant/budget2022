<?php use_helper('I18N', 'Date', 'Url', 'Javascript', 'Form', 'Object') ?>

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>Set Komponen Pendukung Output <?php echo $kode_kegiatan; ?></h1>
</section>

<!-- Main content -->
<section class="content">
    <?php include_partial('entri/list_messages'); ?>
    <!-- Default box -->

    <?php echo form_tag('report/prosesSetOutput'); ?>
    <div class="box box-info">
        <div class="box-body">
            <div id="sf_admin_container">
                <p>
                    <b>OUTPUT KEGIATAN SAAT INI: </b>
                    <?php
                    $temp = explode('|', $rs_kegiatan->getOutput());
                    echo $temp[0] . ': ' . $temp[1] . ' ' . $temp[2];
                    ?>
                </p>
                <p>
                    <b>METODE PERHITUNGAN OUTPUT:</b>
                    <input type="radio" name="metode" id="sum" value="sum" <?php echo $rs_kegiatan->getMetodeCount() ? '' : 'checked'; ?> onchange='cekMetode(this)'> SUM
                    <input type="radio" name="metode" id="count" value="count" <?php echo $rs_kegiatan->getMetodeCount() ? 'checked' : ''; ?> onchange='cekMetode(this)'> COUNT
                </p>
                <div id="sf_admin_content" class="table-responsive">
                    <table cellspacing="0" class="sf_admin_list">
                        <thead>
                            <tr>
                                <th><b>Nama Komponen</b></th>
                                <!-- <th><b>Volume</b></th>
                                <th><b>Satuan</b></th> -->
                                <th><b>Vol 1</b></th>
                                <th><b>Vol 2</b></th>
                                <th><b>Vol 3</b></th>
                                <th><b>Vol 4</b></th>
                                <th><b>Pajak</b></th>
                                <th><b>Harga</b></th>
                                <th><b>Total</b></th>
                                <th><?php // echo checkbox_tag('cekAll', 1, null, "id=cekSemua"); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $counter = 0;
                            while($rs_rinciandetail->next()) {
                                $counter++;
                                $nama = $rs_rinciandetail->getString('komponen_name');
                                $style = '';
                                $check = "name='pilihAction[".$rs_rinciandetail->getString('detail_no')."]' class='cek' id='cek".($counter - 1)."'";
                                if($rs_rinciandetail->getBoolean('is_output')) {
                                    $rek = 'checked';
                                    $disabled = '';
                                    $className = "class='vol check'";
                                } else {
                                    $rek = '';
                                    $disabled = 'disabled';
                                    $className = "class='vol'";
                                }
                                if($rs_kegiatan->getMetodeCount()) {
                                    $disabled = 'disabled';
                                }
                                echo "<tr $style>";

                                echo "<td>".$rs_rinciandetail->getString('komponen_name')." ".$rs_rinciandetail->getString('detail_name')."</td>";
                                // echo "<td>".$rs_rinciandetail->getString('volume')."</td>";
                                // echo "<td>".$rs_rinciandetail->getString('satuan')."</td>";
                                $temp = explode(' X ', $rs_rinciandetail->getString('keterangan_koefisien'));
                                $vol1 = $temp[0];
                                $vol2 = $temp[1];
                                $vol3 = $temp[2];
                                $vol4 = $temp[3];
                                $vol1_tag = $rs_rinciandetail->getBoolean('vol1_tag') ? 'checked' : '';
                                $vol2_tag = $rs_rinciandetail->getBoolean('vol2_tag') ? 'checked' : '';
                                $vol3_tag = $rs_rinciandetail->getBoolean('vol3_tag') ? 'checked' : '';
                                $vol4_tag = $rs_rinciandetail->getBoolean('vol4_tag') ? 'checked' : '';
                                echo "<td>".($vol1 ? "<input type='checkbox' name='vol1[".$rs_rinciandetail->getString('detail_no')."]' id='vol1".$rs_rinciandetail->getString('detail_no')."' $className $disabled $vol1_tag value='$vol1'> $vol1" : "")."</td>";
                                echo "<td>".($vol2 ? "<input type='checkbox' name='vol2[".$rs_rinciandetail->getString('detail_no')."]' id='vol2".$rs_rinciandetail->getString('detail_no')."' $className $disabled $vol2_tag value='$vol2'> $vol2" : "")."</td>";
                                echo "<td>".($vol3 ? "<input type='checkbox' name='vol3[".$rs_rinciandetail->getString('detail_no')."]' id='vol3".$rs_rinciandetail->getString('detail_no')."' $className $disabled $vol3_tag value='$vol3'> $vol3" : "")."</td>";
                                echo "<td>".($vol4 ? "<input type='checkbox' name='vol4[".$rs_rinciandetail->getString('detail_no')."]' id='vol4".$rs_rinciandetail->getString('detail_no')."' $className $disabled $vol4_tag value='$vol4'> $vol4" : "")."</td>";
                                echo "<td>".$rs_rinciandetail->getString('pajak')."</td>";
                                echo "<td>";
                                if ($rs_rinciandetail->getString('satuan') == '%') {
                                    $len = strlen(substr(strrchr($rs_rinciandetail->getFloat('komponen_harga_awal'), "."), 1));
                                    echo number_format($rs_rinciandetail->getFloat('komponen_harga_awal'), $len, ',', '.');
                                } else {
                                    // khusus komponen Biaya Rekening Listrik, dll
                                    $sub_id = substr($rs_rinciandetail->getString('komponen_id'), 0, 11);
                                    $arr_komp = array('23.02.02.03.03.B');
                                    $arr_name = array('Tenaga Operasional','Tenaga Administrasi 3','Tenaga Programmer 3','Tenaga Administrasi 3','Petugas Keamanan','Tenaga Operator 3','Tenaga Satgas','Pembantu Paramedis');
                                    if(
                                        in_array($rs_rinciandetail->getString('komponen_name'), $arr_name) ||
                                        in_array($rs_rinciandetail->getString('komponen_id'), $arr_komp) ||
                                        // all komponen honorer
                                        $sub_id == '23.01.01.08'
                                    ) {
                                        echo number_format($rs_rinciandetail->getFloat('komponen_harga_awal'), 2, ',', '.');
                                    } else {
                                        echo number_format($rs_rinciandetail->getFloat('komponen_harga_awal'), 0, ',', '.');
                                    }
                                }
                                echo "</td>";
                                echo "<td>".number_format(round($rs_rinciandetail->getString('nilai_anggaran')), 0, ',', '.')."</td>";

                                // echo "<td align='left'>".select_tag('rekening_'.$rs_rinciandetail->getString('detail_no'), $arr_rekening[$rs_rinciandetail->getString('komponen_id')], array('style' => 'color:black', 'disabled' => $rek))."</td>";

                                echo "<td align='center'>"."<input type='checkbox' $check $rek value='".$rs_rinciandetail->getString('detail_no')."' onchange='cekBaris(this)'>"."</td>";
                                echo input_hidden_tag('unit_id', $unit_id);
                                echo input_hidden_tag('kode_kegiatan', $kode_kegiatan);

                                echo "</tr>";
                            }
                            ?>
                            <?php if($counter <= 0): ?>
                                <tr><td colspan="16" align="center">Tidak ada komponen yang bisa ditampilkan</td></tr>
                            <?php endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <br/>
            <?php
            if($counter > 0)
                echo submit_tag('Proses Set Pendukung Output', array('name' => 'proses', 'class' => 'btn btn-success btn-flat')) . '&nbsp;';
            ?>
        </div>
    </div>
    </form>

</section><!-- /.content -->

<script>
    $("#cekSemua").change(function () {
        $(".cek").prop('checked', $(this).prop("checked"));
    });

    function cekBaris(element) {
        var vol1 = document.getElementById("vol1" + element.value);
        var vol2 = document.getElementById("vol2" + element.value);
        var vol3 = document.getElementById("vol3" + element.value);
        var vol4 = document.getElementById("vol4" + element.value);
        var sum = document.getElementById("sum");

        if(element.checked && sum.checked) {
            if(vol1) vol1.disabled = false;
            if(vol2) vol2.disabled = false;
            if(vol3) vol3.disabled = false;
            if(vol4) vol4.disabled = false;
        } else {
            if(vol1) vol1.disabled = true;
            if(vol2) vol2.disabled = true;
            if(vol3) vol3.disabled = true;
            if(vol4) vol4.disabled = true;
        }

        if(element.checked) {
            if(vol1) vol1.className = 'vol check';
            if(vol2) vol2.className = 'vol check';
            if(vol3) vol3.className = 'vol check';
            if(vol4) vol4.className = 'vol check';
        } else {
            if(vol1) vol1.className = 'vol';
            if(vol2) vol2.className = 'vol';
            if(vol3) vol3.className = 'vol';
            if(vol4) vol4.className = 'vol';
        }
    }

    function cekMetode(element) {
        if(element.value == 'sum') {
            var vol = document.getElementsByClassName('check');
            for(i = 0; i < vol.length; i++) vol[i].disabled = false;
        } else {
            var vol = document.getElementsByClassName('vol');
            for(i = 0; i < vol.length; i++) vol[i].disabled = true;
        }
    }
</script>