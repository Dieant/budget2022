<div class="box box-info">        
    <div class="box-header with-border">
        Laporan Per Dinas Per Kegiatan
    </div>
    <div class="box-body">
        <table class="table table-bordered">
            <tr>
                <th></th>
                <th class="text-center">Kode Kegiatan</th>
                <th class="text-center">Nama Kegiatan</th>
                <th class="text-center">Pagu</th>
                <th class="text-center">Nilai Usulan</th>
                <th class="text-center">Nilai Disetujui</th>
            </tr>
            <?php
            // print_r($rs);die();
            $i = 0;
            $rs->first();
            do {
                ?>

                <?php if (isset($dinas_ok[$i])): 
                        
                ?>
                    <tr>
                        <td colspan="6">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="3" class="text-left text-bold"><?php echo '('.$unit_id[$i].') '.$unit_name[$i].' '.$operan[0]; ?></td>
                        <td class="text-right text-bold"><?php echo $nilai_draft[$i] ?></td>
                        <td class="text-right text-bold"><?php echo $nilai_usulan_unit[$i] ?></td>
                        <td class="text-right text-bold"><?php echo $nilai_locked[$i] ?></td>
                    </tr>
                <?php endif ?>
                <?php //echo $dinas_ok[11];die(); ?>
                <tr>
                    <td>&nbsp;</td>
                    <td class="text-left"><?php echo $rs->getString('kode_kegiatan') ?></td>
                    <td class="text-left"><?php echo $rs->getString('nama_kegiatan') ?></td>
                    <td class="text-right"><?php echo $nilai_draft3[$i] ?></td>
                    <td class="text-right"><?php echo $nilai_usulan_kegiatan[$i] ?></td>
                    <!-- <td class="text-right"><?php echo $nilai_locked3[$i]." - ".$dinas_ok[$i]." - ".$i;?></td> -->
                    <td class="text-right"><?php echo $nilai_locked3[$i];?></td>
                </tr>
                <?php $i++ ?>
                <?php
            }while ($rs->next());
            ?>
            <tr>
                <td colspan="3" class="text-center text-bold bg-green-active">Total</td>
                <td class="text-right text-bold bg-green-active"><?php echo $nilai_draft2 ?></td>
                <td class="text-right text-bold bg-green-active"><?php echo $nilai_draft4 ?></td>
                <td class="text-right text-bold bg-green-active"><?php echo $nilai_locked2 ?></td>
            </tr>
        </table>       
    </div>
</div>