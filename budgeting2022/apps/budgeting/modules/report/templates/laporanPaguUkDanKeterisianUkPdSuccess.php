<div class="box box-info">
    <div class="box-header with-border">
        Laporan Pengisian UK Dan Keterisian UK Per Dinas
    </div>
    <div class="box-body">
        <table class="table table-bordered">
            <tr>
                <th class="text-center">Kode SKPD</th>
                <th class="text-center">Nama SKPD</th>
                <th class="text-center">Pagu UK</th>
                <th class="text-center">UK Terisi</th>
                <th class="text-center">Selisih</th>
            </tr>
                    <?php
                        $pagu_uk = 0;
                        $oper_pagu_uk = 0;
                        $uk_terisi = 0;
                        $oper_uk_terisi = 0;
                        $selisii = 0;
                        $totalKua = 0;
                        $totalRinc = 0;
                        $totalSel = 0;
                    ?>


                    <?php

                    foreach ($b as $key => $valuea) {
                    ?>
                        <tr>
                            <td class="text-left" ><?php echo $valuea['unit_id'] ?></td>
                            <td class="text-left" ><?php echo $valuea['unit_name'] ?></td>                          

                            <td class="text-right" >
                                <?php 
                                    $oper_pagu_uk = $valuea['pagu_uk'];
                                    if ($oper_pagu_uk) {
                                        $pagu_uk = $oper_pagu_uk;
                                    }
                                    else {
                                        $pagu_uk = 0;   
                                    }
                                    echo number_format($pagu_uk, 0, ',', '.');
                                    $totalKua += $pagu_uk;
                                    //$totalKua += $valuea['pagu_uk'];
                                    //echo number_format($valuea['pagu_uk'], 0, ',', '.');
                                ?>
                            </td>
                            <td class="text-right" >
                                <?php
                                    $oper_uk_terisi = $valuea['uk_terisi'];
                                    if ($oper_uk_terisi) {
                                        $uk_terisi = $oper_uk_terisi;
                                    }
                                    else {
                                        $uk_terisi = 0;   
                                    }
                                    echo number_format($uk_terisi, 0, ',', '.');
                                    $totalRinc += $uk_terisi;
                                    // $totalRinc += $valuea['uk_terisi'];
                                    // echo number_format($valuea['uk_terisi'], 0, ',', '.');
                                ?>
                            </td>
                            <td class="text-right" >
                                <?php 
                                    //$totalSel += $valuea['selisih'];
                                    //echo number_format($valuea['selisih'], 0, ',', '.');
                                    $selisii = $pagu_uk - $uk_terisi;
                                    $totalSel += $selisii;
                                    echo number_format($selisii, 0, ',', '.');
                                ?>
                            </td>
                        </tr>                    
                    <?php
                    }
                    ?>
                        <tr>
                            <td colspan="2" class="text-center"><h5><b>TOTAL</b></h5></td> 
                            <td class="text-right" ><b><?php echo number_format($totalKua, 0, ',', '.');?></b></td>
                            <td class="text-right" ><b><?php echo number_format($totalRinc, 0, ',', '.');?><b></td>
                            <td class="text-right"><b><?php echo number_format($totalSel, 0, ',', '.');?></b></td>
                        </tr>

                        
        </table>     
    </div>
</div>