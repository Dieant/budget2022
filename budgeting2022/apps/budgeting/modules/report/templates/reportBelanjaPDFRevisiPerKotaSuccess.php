<style>
    table, th, td{
        border: 1px solid black;
        padding: 0px;
    }
    table {
        border-spacing: 0px;
    }
</style>
<table style="width: 100%;" cellspacing="0">
    <tr>
        <td style="text-align: center; font-weight: bold">
            <h4>Report e-Budgeting <?php echo sfConfig::get('app_tahun_default'); ?></h4>
        </td>
    </tr>
    <tr>
        <td style="text-align: center; font-weight: bold">
            <h2>Report Anggaran Per Belanja Kota</h2>
        </td>
    </tr>
    <tr>
        <td style="text-align: center; font-weight: bold">
            <h4>Waktu : <?php echo date('d-m-Y H:i:s') ?></h4>
        </td>
    </tr>
    <tr>
        <td>
            <div>
                <table>
                    <thead>
                        <tr>
                            <th style="text-align: center; font-weight: bold">Nama Belanja</th>
                            <th style="text-align: center; font-weight: bold">Semula</th>
                            <th style="text-align: center; font-weight: bold">% Usulan</th>
                            <th style="text-align: center; font-weight: bold">Menjadi</th> <!-- bener -->
                            <th style="text-align: center; font-weight: bold">% Disetujui</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = 0;
                        $rs->first();
                        do {
                            ?>
                            <tr>
                                <td colspan="5">&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="text-align: left"><?php echo isset($belanja_name[$i]) ? $belanja_name[$i] : '' ?></td>
                                <td style="text-align: right"><?php 
                                //echo isset($nilai_draft[$i]) ? $nilai_draft[$i] : '' ?>
                                <?php echo $lock_mjd[$i]; ?>    
                                </td>
                                <td style="text-align: right"><?php 
                                // echo isset($persen_draft[$i]) ? $persen_draft[$i] : '' 
                                echo isset($persen_draft_sml[$i]) ? $persen_draft_sml[$i] : '' 
                                ?> %</td>
                                <td style="text-align: right"><?php 
                                echo isset($nilai_locked[$i]) ? $nilai_locked[$i] : '' ?></td>
                                <td style="text-align: right"><?php 
                                echo isset($persen_locked[$i]) ? $persen_locked[$i] : '' ?> %</td>
                            </tr>
                            <?php
                            $i++;
                        } while ($rs->next())
                        ?>  
                        <tr>
                            <td colspan="5">&nbsp;</td>
                        </tr>    
                        <tr>
                            <td style="text-align: center;font-weight: bold">Total</td>
                            <td style="text-align: right;font-weight: bold"><?php echo $total_semula; ?></td>
                            <td style="text-align: right;font-weight: bold">100%</td>
                            <td style="text-align: right;font-weight: bold"><?php echo $nilai_locked2; ?></td>
                            <td style="text-align: right;font-weight: bold">100%</td>
                        </tr>
                    </tbody>
                </table>
            </div>            
        </td>
    </tr>
</table>