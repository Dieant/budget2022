<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="fas fa-align-left"></i> Laporan Per Program
                        </h3>
                    </div>
                    <div class="card-body"> 
                        <table class="table table-bordered">
                            <tr>
                                <th class="text-center">Kode Program</th>
                                <th class="text-center">Nama Program</th>
                                <th class="text-center">Nilai</th>
                                <th class="text-center">%</th>
                            </tr>
                            <?php
                            $i = 0;
                            $rs->first();
                            do {
                                ?>
                                <tr>
                                    <td class="text-center"><?php echo $rs->getString('kode_bidang') ?></td>
                                    <td class="text-left"><?php echo $rs->getString('nama_bidang') ?></td>
                                    <td class="text-right"><?php echo $nilai_draft3[$i] ?></td>
                                    <td class="text-right"><?php echo $persen_draft3[$i] ?>%</td>
                                </tr>
                                <?php
                                $i++;
                            } while ($rs->next());
                            ?>
                            <tr>
                                <td colspan="2" class="text-center text-bold bg-green-active">Total</td>
                                <td class="text-right text-bold bg-green-active"><?php echo $nilai_draft2; ?></td>
                                <td class="text-right text-bold bg-green-active">100%</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- <div class="box box-info">        
    <div class="box-header with-border">
        
    </div>
    <div class="box-body">
               
    </div>
</div> -->