<?php use_helper('Form', 'Object', 'Javascript') ?>
<?php
if (sfConfig::get('app_tahap_edit') == 'murni') {
    $nama_sistem = 'Pra-RKA';
} elseif (sfConfig::get('app_tahap_edit') == 'pak') {
    $nama_sistem = 'PAK';
} elseif (sfConfig::get('app_tahap_edit') == 'penyesuaian') {
    $nama_sistem = 'Penyesuaian';
} else {
    $nama_sistem = 'Revisi';
}
?>
<div class="box box-info">
    <div class="box-header with-border">
        Pilih SKPD
    </div>
    <?php if ($sf_user->getNamaUser()=='admin') {?>
    <div class="box-body">
        <?php
        $e = new Criteria();
        $e->add(UnitKerjaPeer::UNIT_ID, '9999', Criteria::NOT_IN);
        $e->addAscendingOrderByColumn(UnitKerjaPeer::UNIT_NAME);
        $unit_kerja = UnitKerjaPeer::doSelect($e);
        echo select_tag('unit_id', objects_for_select($unit_kerja, 'getUnitId', 'getUnitName', $filter_unit_kerja, array('include_custom' => '------Pilih Dinas------')), array('id' => 'unit_id', 'class' => 'form-control'));
        ?>
    </div>  
    <?php }
    else {?>
    <div class="box-body">
                    <?php
                    $e = new Criteria();
                    $nama = $sf_user->getNamaUser();
                    //print_r ($nama); exit;
                    $b = new Criteria;
                    $b->add(UserHandleV2Peer::USER_ID, $nama);
                    $es = UserHandleV2Peer::doSelect($b);
                    $satuan_kerja = $es;
                    $unit_kerja = Array();
                    foreach ($satuan_kerja as $x) {
                        $unit_kerja[] = $x->getUnitId();
                    }
                    $e->add(UnitKerjaPeer::UNIT_ID, $unit_kerja[0], criteria::ILIKE);
                    for ($i = 1; $i < count($unit_kerja); $i++) {
                        $e->addOr(UnitKerjaPeer::UNIT_ID, $unit_kerja[$i], criteria::ILIKE);
                    }
                    //$e->add(UnitKerjaPeer::UNIT_NAME, 'Latihan', criteria::NOT_EQUAL);
                    $e->addAscendingOrderByColumn(UnitKerjaPeer::UNIT_NAME);
                    $v = UnitKerjaPeer::doSelect($e);

                    echo select_tag('unit_id', objects_for_select($v, 'getUnitId', 'getUnitName',  $filter_unit_kerja, array('include_custom' => '------Pilih Dinas------')), array('id' => 'unit_id', 'class' => 'form-control'));
                    ?>
                </div>
    <?php }?>
    <div class="box-header with-border">
        Nama Komponen
    </div>
    <div class="box-body">
        <?php
        echo input_tag('komponen', $filter_komponen, array('id' => 'komponen', 'class' => 'form-control'));
        ?>
    </div>
    <div class="box-body">
        <?php echo link_to_function('Filter', 'filterKomponen()', array('class' => 'btn btn-flat btn-success')); ?>
    </div>
</div>
<div class="box box-info">
    <div class="box-header with-border">
        Laporan Komponen Serupa Non-Fisik Per Dinas
    </div>
    <div class="box-body  table-responsive">
        <table class="table table-bordered">
            <tr>
                <th class="text-center">Komponen</th>
                <th class="text-center">Rekening</th>
                <th class="text-center">Kegiatan</th>
                <th class="text-center">Subtitle</th>
                <th class="text-center">Sub Subtitle</th>
                <th class="text-center">Koefisien</th>
                <th class="text-center">Catatan</th>
            </tr>
            <?php
            foreach ($komponen as $value):
                if (isset($value['komponen_ok'])):
                    ?>
                    <tr><td colspan="7">&nbsp;</td></tr>
                    <tr>
                        <td colspan="7" class="text-left text-bold"><?php echo $value['komponen_id'] . ' - ' . $value['komponen_name'] ?></td>
                    </tr>
                <?php endif;
                if (isset($value['rekening_ok'])):
                    ?>
                    <tr>
                        <td>&nbsp;</td>
                        <td colspan="6" class="text-left text-bold"><?php echo $value['rekening_code'] . ' - ' . $value['rekening_name'] ?></td>
                    </tr>
                <?php endif; ?>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td class="text-left" ><?php echo $value['kode_kegiatan'] . ' - ' . $value['nama_kegiatan'] ?></td>
                    <td class="text-left" ><?php echo $value['subtitle'] ?></td>
                    <td class="text-left" ><?php echo $value['sub'] ?></td>
                    <td class="text-left" ><?php echo $value['keterangan_koefisien'] ?></td>
                    <td class="text-left" ><?php echo $value['note_skpd'] ?></td>
                </tr>
            <?php endforeach; ?>
        </table>       
    </div>
</div>
<script>
    $("#unit_id").change(function () {
        $('#indicator').show();
        var id = $(this).val();
        var id2 = $("#komponen").val();
        if (id == '') {
            id = '0000';
        }
        if (id2 == '') {
            id2 = '';
        }
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/report/reportKomponenSerupa/unit_id/" + id + "/komponen/" + id2 + ".html",
            context: document.body
        }).done(function (msg) {
            $('#indicator').hide();
            $('#isi').html(msg);
        });

    });

    function filterKomponen() {
        $('#indicator').show();
        var id = $("#unit_id").val();
        var id2 = $("#komponen").val();
        if (id == '') {
            id = '0000';
        }
        if (id2 == '') {
            id2 = '';
        }
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/report/reportKomponenSerupa/unit_id/" + id + "/komponen/" + id2 + ".html",
            context: document.body
        }).done(function (msg) {
            $('#indicator').hide();
            $('#isi').html(msg);
        });
    }
</script>