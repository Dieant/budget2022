<?php use_helper('Form', 'Object', 'Javascript') ?>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="fas fa-align-left"></i> Laporan Perbandingan Data eBudgeting dengan SABK
                        </h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Perangkat Daerah</label>
                                        <?php
                                        $e = new Criteria();
                                        $e->add(UnitKerjaPeer::UNIT_ID, '9999', Criteria::NOT_IN);
                                        $e->addAscendingOrderByColumn(UnitKerjaPeer::UNIT_NAME);
                                        $unit_kerja = UnitKerjaPeer::doSelect($e);
                                        echo select_tag('unit_id', objects_for_select($unit_kerja, 'getUnitId', 'getUnitName', (isset($filter_unit_kerja) ? $filter_unit_kerja : NULL)), array('id' => 'unit_id', 'class' => 'form-control js-example-basic-single'));
                                        ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body table-responsive p-0">
                        <table class="table table-hover">
                            <thead class="head_peach">
                                <tr>
                                    <th class="text-center">Dinas</th>
                                    <th class="text-center">Id Sub Kegiatan</th>
                                    <th class="text-center">Kode Sub Kegiatan</th>
                                    <th class="text-center">Kode Sub Kegiatan SABK</th>
                                    <th class="text-center">Nama Sub Kegiatan</th>
                                    <th class="text-center">Nama Sub Kegiatan SABK</th>
                                    <th class="text-center">Nama Komponen</th>
                                    <th class="text-center">Harga Komponen e-Budgeting</th>
                                    <th class="text-center">Harga Komponen SABK</th>
                                    <th class="text-center">Volume e-Budgeting</th>
                                    <th class="text-center">Volume SABK</th>
                                    <th class="text-center">Satuan</th>
                                    <th class="text-center">Detail e-Budgeting</th>
                                    <th class="text-center">Detail SABK</th>
                                    <th class="text-center">Nilai Anggaran e-Budgeting</th>
                                    <th class="text-center">Nilai Anggaran SABK</th>
                                    <th class="text-center">Rekening e-Budgeting</th>
                                    <th class="text-center">Rekening SABK</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    $tempUnit = '';
                                ?>
                                <tr>
                                    <td colspan="3">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="text-left text-bold"><?php echo $unit_id . ' - ' . $unit_name; ?></td>
                                </tr>
                                <?php
                                    foreach ($perbedaans as $value):
                                        if($value['kode_subkegiatan'] != $value['kode_subkegiatan_sabk'] || $value['nama_kegiatan'] != $value['namakeg_sabk'] || number_format($value['harga_budgeting'], 0, ',', '.') != number_format($value['harga_sabk'], 0, ',', '.') || $value['volume_budgeting'] != $value['volume_sabk'] || number_format($value['total_budgeting'], 0, ',', '.') != number_format($value['total_sabk'], 0, ',', '.') || $value['detail_budgeting'] != $value['detail_sabk'] || $value['rek_budgeting'] != $value['rek_sabk']) {
                                            $warna = 'pink';
                                            ?>
                                            <tr style='background: pink;'>
                                                <td>&nbsp;</td>
                                                <td class='text-left' ><?php echo $value['id_subkegiatan']; ?></td>
                                                <td class='text-left' ><?php echo ($value['tipe'] == 'BTL') ? $value['kode_subkegiatan_sabk'] : $value['kode_subkegiatan'] ?></td>
                                                <td class='text-left' ><?php echo $value['kode_subkegiatan_sabk']; ?></td>
                                                <td class='text-left' ><?php echo $value['nama_kegiatan']; ?></td>
                                                <td class='text-left' ><?php echo $value['namakeg_sabk']; ?></td>
                                                <td class='text-left' ><?php echo $value['komponen_name']; ?></td>
                                                <td class='text-right' ><?php echo $value['harga_budgeting']; ?></td>
                                                <td class='text-right' ><?php echo $value['harga_sabk']; ?></td>
                                                <td class='text-right' ><?php echo $value['volume_budgeting']; ?></td>
                                                <td class='text-right' ><?php echo $value['volume_sabk']; ?></td>
                                                <td class='text-right' ><?php echo $value['satuan_budgeting']; ?></td>
                                                <td class='text-right' ><?php echo $value['detail_budgeting']; ?></td>
                                                <td class='text-right' ><?php echo $value['detail_sabk']; ?></td>
                                                <td class='text-right' ><?php echo number_format($value['total_budgeting'], 0, ',', '.'); ?></td>
                                                <td class='text-right' ><?php echo number_format($value['total_sabk'], 0, ',', '.'); ?></td>
                                                <td class='text-right' ><?php echo $value['rek_budgeting']; ?></td>
                                                <td class='text-right' ><?php echo $value['rek_sabk']; ?></td>
                                            </tr>
                                            <?php
                                       }
                                        else
                                            $warna = '';
                                        $tempUnit = $value['unit_id'];
                                    endforeach;
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $("#unit_id").change(function () {
        $('#indicator').show();
        var id = $(this).val();
        if (id == '') {
            id = '0000';
        }
        var tahap = "<?php echo $tahap; ?>";
        var access_token = "<?php echo $access_token; ?>";
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/report/reportDataSabk/unit_id/" + id + "/tahap/" + tahap + "/access_token/" + access_token + ".html",
            context: document.body
        }).done(function (msg) {
            $('#indicator').hide();
            $('#isi').html(msg);
        });
    });
    $(document).ready(function () {
        $(".js-example-basic-single").select2();
    });
</script>