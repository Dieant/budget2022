<?php use_helper('Form', 'Object', 'Javascript') ?>
<div class="box box-info">
    <div class="box-header with-border">
        Laporan Pagu - Entry (Dana Kelurahan)
    </div>
    <div class="box-body">
        <table class="table table-bordered">
            <tr>
                <th class="text-center">Dinas</th>
                <th class="text-center">Kode Kegiatan</th>
                <th class="text-center">Nama Kegiatan</th>
                <th class="text-center">Rincian (Terentri)</th>
                <th class="text-center">UK (Terentri)</th>
                <th class="text-center">Selisih (Rincian - UK)</th>
                
            </tr>
            <?php
            $tempUnit = '';
            $anggaran = 0;
            $tambahan = 0;
            $pagu = 0;
            $anggaran_kota = 0;
            $tambahan_kota = 0;
            $pagu_kota = 0;
            foreach ($kegiatans as $value): ?>
                <?php if($tempUnit != $value['unit_id']): ?>
                    <?php if($pagu > 0): ?>
                        <tr>
                            <td colspan="3"></td>
                            <td>Rp.<b><?php echo number_format($pagu, 2, ',', '.') ?></b></td>
                            <td>Rp.<b><?php echo number_format($tambahan, 2, ',', '.') ?></b></td>
                            <td>Rp.<b><?php echo number_format($pagu - $tambahan, 2, ',', '.') ?></b></td>
                        </tr>
                        <?php
                        $anggaran = 0;
                        $tambahan = 0;
                        $pagu = 0;
                        ?>
                    <?php endif; ?>
                    <?php if(!empty($value['unit_id'])): ?>
                        <tr><td colspan="8">&nbsp;</td></tr>
                        <tr>
                            <td colspan="8" class="text-left text-bold"><?php echo $value['unit_id'] . ' - ' . $value['unit_name'] ?></td>
                        </tr>
                    <?php endif; ?>
                <?php endif; ?>

                <!-- <?php
                if($value['pagu'] + $value['tambahan_pagu'] != $value['nilai_anggaran'])
                    $style = "background: pink;";
                else
                    $style = "";
                ?>
 -->
               

                <?php if(!empty($value['unit_id'])): ?>
                    <tr style="<?php echo $style ?>">
                        <td>&nbsp;</td>
                        <td class="text-left" ><?php echo $value['kode_kegiatan'] ?></td>
                        <td class="text-left" ><?php echo $value['nama_kegiatan'] ?></td>
                        <td class="text-left" >Rp.<?php echo number_format($value['entri'], 0, ',', '.') ?></td>
                        <td class="text-left" >Rp.<?php echo number_format($value['uk'], 0, ',', '.') ?></td>
                        <td class="text-left" >Rp.<?php echo number_format($value['entri'] - $value['uk'], 0, ',', '.') ?></td>
                        
                    </tr>
                    <?php
                    $tempUnit = $value['unit_id'];
                    $tambahan += $value['uk'];
                    $pagu += $value['entri'];
                    $tambahan_kota += $value['uk'];
                    $pagu_kota += $value['entri']; ?>
                <?php endif; ?>
            <?php endforeach; ?>
            <tr><td colspan="8"><b>TOTAL TERENTRI = Rp.<?php echo number_format($pagu_kota, 2, ',', '.') ?></b></td></tr>
            <tr><td colspan="8"><b>UK (Terentri)= Rp.<?php echo number_format($tambahan_kota, 2, ',', '.') ?></b></td></tr>
            <tr><td colspan="8"><b>SELISIH = Rp.<?php echo number_format($pagu_kota - $tambahan_kota, 2, ',', '.') ?></b></td></tr>
        </table>
    </div>
</div>