<?php use_helper('Form', 'Object', 'Javascript') ?>
<div class="box box-info">        
    <div class="box-header with-border">
        Pilih SKPD
    </div>
    <div class="box-body">
        <?php
        echo select_tag('unit_id', objects_for_select($unit_kerja, 'getUnitId', 'getUnitName', $unit_kerja, 'include_custom=---Pilih SKPD---'), array('class' => 'form-control js-example-basic-single', 'style' => 'width:100%'));
        ?>
    </div>
    <div class="box-header with-border">
        Pilih Kegiatan
    </div>
    <div class="box-body">
        <?php
        echo select_tag('kegCode', options_for_select(array(), '', 'include_custom=---Pilih SKPD Dulu---'), Array('id' => 'keg', 'class' => 'form-control js-example-basic-single', 'style' => 'width:100%'));
        ?>
    </div>
</div>

<script>
    $("#unit_id").change(function() {
        var id = $(this).val();
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/report/pilihKegiatan/where/"+id+"/tahap/revisi1.html",
            context: document.body
        }).done(function(msg) {
            $('#keg').html(msg);
        });

    });
    $("#keg").change(function() {
        $('#indicator').show();
        var id = $(this).val();
        var idskpd = $("#unit_id").val();
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/report/tampilReport/b/<?php echo $report ?>/unit_id/"+idskpd+"/kegCode/"+id+".html",
            context: document.body
        }).done(function(msg) {
            $('#indicator').hide();
            $('#isi_dalam').html(msg);
        });

    });
</script>