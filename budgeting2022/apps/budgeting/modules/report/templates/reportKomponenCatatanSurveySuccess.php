<?php use_helper('Form', 'Object', 'Javascript') ?>
<?php
if (sfConfig::get('app_tahap_edit') == 'murni') {
    $nama_sistem = 'Pra-RKA';
} elseif (sfConfig::get('app_tahap_edit') == 'pak') {
    $nama_sistem = 'PAK';
} elseif (sfConfig::get('app_tahap_edit') == 'penyesuaian') {
    $nama_sistem = 'Penyesuaian';
} else {
    $nama_sistem = 'Revisi';
}
?>
<div class="box box-primary box-solid">
    <div class="box-header with-border">
        Laporan Komponen Catatan Survey
    </div>
    <div class="box-body  table-responsive">
        <table class="table table-bordered">
            <tr>
                <th class="text-center">Komponen ID</th>
                <th class="text-center">Nama Komponen</th>
                <th class="text-center">Satuan</th>
                <th class="text-center">Harga</th>
                <th class="text-center">Rekening</th>
                <th class="text-center">Nama Surveyor</th>
                <th class="text-center">Catatan</th>
            </tr>
            <?php
            foreach ($komponen as $value):
            ?>
                <tr>
                    <td class="text-left" ><?php echo $value['shsd_id'] ?></td>
                    <td class="text-left" ><?php echo $value['shsd_name'] ?></td>
                    <td class="text-left" ><?php echo $value['satuan'] ?></td>
                    <td class="text-left" ><?php echo $value['shsd_harga'] ?></td>
                    <td class="text-left" ><?php echo $value['rekening_code'] ?></td>
                    <td class="text-left" ><?php echo $value['surveyor'] ?></td>
                    <td class="text-left" ><?php echo $value['shsd_catatan'] ?></td>
                </tr>
            <?php endforeach; ?>
        </table>       
    </div>
</div>
