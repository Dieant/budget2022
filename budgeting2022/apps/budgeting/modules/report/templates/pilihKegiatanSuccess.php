<?php use_helper('Object','Javascript','I18N', 'Date'); ?>

<?php use_stylesheet('/sf/sf_admin/css/main') ?>
<?php echo use_helper('Object', 'Javascript', 'Validation') ?>
<?php 
 
        $options = array();
        $d = new Criteria();
    if($tahap=='revisi1')
    {
        $d->add(Revisi1MasterKegiatanPeer::UNIT_ID, $unit_idDinas);
        $rs = Revisi1MasterKegiatanPeer::doSelect($d);
//        echo select_tag('kegiatan', objects_for_select($kegiatan,'getKodeKegiatan', 'getKodeNamaKegiatan',
//                        '',array('include_custom'=>'Pilih Kegiatan')),
//                         Array('id'=>'combo_pilih','onChange'=>remote_function(Array('update'=>'isi', 'url'=>'report/reportSemulaMenjadiMurniRevisi1?unit_id='.$unit_idDinas.'&tahap='.$tahap,
//                        'with'=>"'b=rsmmr1&kegCode='+this.options[this.selectedIndex].value",
//                        'loading'=>"Element.show('indicator');Element.hide('isi');Element.hide('isi_dalam');",
//                        'complete'=>"Element.hide('indicator');Element.show('isi');".visual_effect('highlight', 'isi')))));
    }else if($tahap=='revisi2')
    {
        $d->add(Revisi2MasterKegiatanPeer::UNIT_ID, $unit_idDinas);
        $rs = Revisi2MasterKegiatanPeer::doSelect($d);
//        echo select_tag('kegiatan', objects_for_select($kegiatan,'getKodeKegiatan', 'getKodeNamaKegiatan',
//                        '',array('include_custom'=>'Pilih Kegiatan')),
//                         Array('id'=>'combo_pilih','onChange'=>remote_function(Array('update'=>'isi', 'url'=>'report/reportSemulaMenjadiRevisi1Revisi2?unit_id='.$unit_idDinas.'&tahap='.$tahap,
//                        'with'=>"'b=rsmmr1&kegCode='+this.options[this.selectedIndex].value",
//                        'loading'=>"Element.show('indicator');Element.hide('isi');Element.hide('isi_dalam');",
//                        'complete'=>"Element.hide('indicator');Element.show('isi');".visual_effect('highlight', 'isi')))));
    }else if($tahap=='revisi3')
    {
        $d->add(Revisi3MasterKegiatanPeer::UNIT_ID, $unit_idDinas);
        $rs = Revisi3MasterKegiatanPeer::doSelect($d);
//        echo select_tag('kegiatan', objects_for_select($kegiatan,'getKodeKegiatan', 'getKodeNamaKegiatan',
//                        '',array('include_custom'=>'Pilih Kegiatan')),
//                         Array('id'=>'combo_pilih','onChange'=>remote_function(Array('update'=>'isi', 'url'=>'report/reportSemulaMenjadiRevisi2Revisi3?unit_id='.$unit_idDinas.'&tahap='.$tahap,
//                        'with'=>"'b=rsmmr1&kegCode='+this.options[this.selectedIndex].value",
//                        'loading'=>"Element.show('indicator');Element.hide('isi');Element.hide('isi_dalam');",
//                        'complete'=>"Element.hide('indicator');Element.show('isi');".visual_effect('highlight', 'isi')))));
    }else if($tahap=='revisi4')
    {
        $d->add(Revisi4MasterKegiatanPeer::UNIT_ID, $unit_idDinas);
        $rs = Revisi4MasterKegiatanPeer::doSelect($d);
//        echo select_tag('kegiatan', objects_for_select($kegiatan,'getKodeKegiatan', 'getKodeNamaKegiatan',
//                        '',array('include_custom'=>'Pilih Kegiatan')),
//                         Array('id'=>'combo_pilih','onChange'=>remote_function(Array('update'=>'isi', 'url'=>'report/reportSemulaMenjadiRevisi3Revisi4?unit_id='.$unit_idDinas.'&tahap='.$tahap,
//                        'with'=>"'b=rsmmr1&kegCode='+this.options[this.selectedIndex].value",
//                        'loading'=>"Element.show('indicator');Element.hide('isi');Element.hide('isi_dalam');",
//                        'complete'=>"Element.hide('indicator');Element.show('isi');".visual_effect('highlight', 'isi')))));
    }else if($tahap=='revisi5')
    {
        $d->add(Revisi5MasterKegiatanPeer::UNIT_ID, $unit_idDinas);
        $rs = Revisi5MasterKegiatanPeer::doSelect($d);
//        echo select_tag('kegiatan', objects_for_select($kegiatan,'getKodeKegiatan', 'getKodeNamaKegiatan',
//                        '',array('include_custom'=>'Pilih Kegiatan')),
//                         Array('id'=>'combo_pilih','onChange'=>remote_function(Array('update'=>'isi', 'url'=>'report/reportSemulaMenjadiRevisi4Revisi5?unit_id='.$unit_idDinas.'&tahap='.$tahap,
//                        'with'=>"'b=rsmmr1&kegCode='+this.options[this.selectedIndex].value",
//                        'loading'=>"Element.show('indicator');Element.hide('isi');Element.hide('isi_dalam');",
//                        'complete'=>"Element.hide('indicator');Element.show('isi');".visual_effect('highlight', 'isi')))));
    }
    
    foreach ($rs as $keg) {
        $options[$keg->getKodeKegiatan()] = $keg->getNamaKegiatan();
    }

    echo options_for_select($options, '', array('include_blank' => true));
?>