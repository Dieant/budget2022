<?php use_helper('Form', 'Object', 'Javascript') ?>
<?php
if (sfConfig::get('app_tahap_edit') == 'murni') {
    $nama_sistem = 'Pra-RKA';
} elseif (sfConfig::get('app_tahap_edit') == 'pak') {
    $nama_sistem = 'PAK';
} elseif (sfConfig::get('app_tahap_edit') == 'penyesuaian') {
    $nama_sistem = 'Penyesuaian';
} else {
    $nama_sistem = 'Revisi';
}
?>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="fas fa-align-left"></i> Laporan Komponen COVID
                        </h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Pilih Prioritas</label>
                                    <?php
                                        $c = new Criteria();
                                        $c->add(MasterPrioritasPeer::ID, array(69,70,72,73), Criteria::IN);
                                        $c->addAscendingOrderByColumn(MasterPrioritasPeer::ID);
                                        $v = MasterPrioritasPeer::doSelect($c);  
                                        echo select_tag('prioritas_id', objects_for_select($v, 'getId', 'getPrioritas', $filter_prioritas, 'include_custom=---Pilih Prioritas---'), array('class' => 'form-control js-example-basic-single', 'style' => 'width:100%'));
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body table-responsive p-0">
                        <table class="table table-hover">
                            <thead class="head_peach">
                                <tr>
                                    <th class="text-center">Dinas</th>
                                    <th class="text-center">Kode</th>
                                    <th class="text-center">Sub Kegiatan</th>
                                    <th class="text-center">Detail Kegiatan</th>
                                    <th class="text-center">Nama Komponen</th>
                                    <!-- <th class="text-center">Keterangan</th> -->
                                    <th class="text-center">Nilai Anggaran</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $tempUnit = '';
                                $anggaran = 0; 
                                $pagu_kota = 0;    
                                foreach ($komponen as $value): ?>
                                <?php if($tempUnit != $value['unit_id']): ?>
                                <?php if($anggaran > 0): ?>
                                <tr style="background-color: #e8e6e6;">
                                    <td colspan="5"></td>
                                    <td class="text-right">Rp.<b><?php echo number_format($anggaran, 2, ',', '.') ?></b></td>
                                </tr>
                                <?php
                                            $anggaran = 0;                       
                                            ?>
                                <?php endif; ?>
                                <?php if(!empty($value['unit_id'])): ?>
                                <tr>
                                    <td colspan="6">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="6" class="text-left text-bold">
                                        <?php echo $value['unit_id'] . ' - ' . $value['unit_name'] ?></td>
                                </tr>
                                <?php endif; ?>
                                <?php endif; ?>
                                <?php if(!empty($value['unit_id'])): ?>
                                <tr style="<?php echo $style ?>">
                                    <!--  <td>&nbsp;</td> -->
                                    <td class="text-left"><?php //echo $value['unit_id'] ?></td>
                                    <td class="text-left"><?php echo $value['kegiatan_code'] ?></td>
                                    <td class="text-left">
                                        <?php //echo $value['kegiatan_id'].' - ['. $value['kegiatan_code'].'] '. $value['nama_kegiatan'] ?>
                                        <?php echo $value['nama_kegiatan'] ?> <span class="badge bg-warning"><?php echo $value['prioritas'] ?></span>
                                    </td>
                                    <td class="text-left"><?php echo $value['detail_kegiatan'] ?></td>
                                    <td class="text-left"><?php echo $value['komponen_name'] ?></td>
                                    <!-- <td class="text-left"><?php //echo $value['detail_name'] ?></td> -->
                                    <td class="text-right">
                                        <?php echo number_format($value['nilai_anggaran'], 0, ',', '.'); ?></td>

                                </tr>
                                <?php
                                $tempUnit = $value['unit_id'];                    
                                $anggaran += $value['nilai_anggaran'];
                                $pagu_kota += $value['nilai_anggaran'];
                                ?>
                                <?php endif; ?>
                                <?php endforeach; ?>

                                <?php if(!empty($komponen)) : ?>
                                <tr style="background-color: #e8e6e6;">
                                    <td colspan="5"></td>
                                    <td class="text-right">Rp.<b><?php echo number_format($anggaran, 2, ',', '.') ?></b></td>
                                </tr>
                                <?php endif; ?>
                                <tr>
                                    <td colspan="6"><b>TOTAL ALL SKPD =  Rp.<?php echo number_format($pagu_kota, 2, ',', '.') ?></b></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $(document).ready(function () {
        $(".js-example-basic-single").select2();
    });
    // $("#unit_id").change(function () {
    //     $('#indicator').show();
    //     var id = $(this).val();
    //     var id2 = $("#sumber_dana_id").val();
    //     if (id == '') {
    //         id = '0000';
    //     }
    //     if (id2 == '') {
    //         id2 = 'xxx';
    //     }
    //     $.ajax({
    //         url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/report/reportSumberDanaDinasKegiatanRevisi/unit_id/" + id + "/sumber_dana_id/" + id2 + ".html",
    //         context: document.body
    //     }).done(function (msg) {
    //         $('#indicator').hide();
    //         $('#isi').html(msg);
    //     });

    // });
    $("#prioritas_id").change(function () {
        $('#indicator').show();
        var id = $(this).val();
        if (id == '') {
            id = '0000';
        }
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/report/reportKomponenCovid/prioritas_id/" +
                id + ".html",
            context: document.body
        }).done(function (msg) {
            $('#indicator').hide();
            $('#isi').html(msg);
        });

    });
</script>