<?php use_helper('I18N', 'Date', 'Url', 'Javascript', 'Form', 'Object') ?>
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
        <div class="col-sm-6">
            <h1>Detail Kegiatan</h1>
        </div>
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Lembar Kerja</a></li>
            <li class="breadcrumb-item active">Detail Kegiatan</li>
            </ol>
        </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<!-- Main content -->
<section class="content">
    <?php include_partial('kegiatan/list_messages'); ?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 stretch-card">
                <div class="card">
                    <div class="card-body table-responsive p-0">
                        <table class="table table-hover">
                            <thead class="head_peach">
                                <tr>
                                    <th><b>Detail Kegiatan</b></th>
                                    <th><b>Nama Komponen</b></th>
                                    <th><b>Satuan</b></th>
                                    <th><b>Keterangan Koefisien</b></th>
                                    <th><b>Rekening</b></th>
                                    <th><b>Harga</b></th>
                                    <th><b>PPN</b></th>
                                    <th><b>Harga + PPN</b></th>
                                    <th><b>Total</b></th>
                                    <th><b>Realisasi</b></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $counter = 0;
                                $array_detail = array();
                                $query2 = "SELECT detail_kegiatan from " . sfConfig::get('app_default_schema') . "." ."rincian_detail where status_hapus=false and tahap ilike 'revisi7%' and kegiatan_code='".$kode_kegiatan."'";
                                $con = Propel::getConnection();
                                $statement2 = $con->prepareStatement($query2);
                                $rs = $statement2->executeQuery();
                                while ($rs->next()) 
                                    {
                                        $array_detail[] = $rs->getString('detail_kegiatan');
                                    }
                                while($rs_rinciandetail->next()) {
                                    if(in_array($rs_rinciandetail->getString('detail_kegiatan'), $array_detail))
                                    $color='pink';
                                    else
                                    $color='white';
                                    $counter++;
                                    $nama = $rs_rinciandetail->getString('komponen_name');
                                    $style = '';
                                    $harga_ppn=$rs_rinciandetail->getString('komponen_harga')*( $rs_rinciandetail->getString('pajak') + 100 ) / 100;
                                    echo "<tr $style>";
                                    echo "<td>".$rs_rinciandetail->getString('detail_kegiatan')."</td>";
                                    echo "<td>".$rs_rinciandetail->getString('komponen_name')." ".$rs_rinciandetail->getString('detail_name')."</td>";
                                    echo "<td>".$rs_rinciandetail->getString('satuan')."</td>";
                                    echo "<td>".$rs_rinciandetail->getString('keterangan_koefisien')."</td>";
                                    echo "<td>".$rs_rinciandetail->getString('rekening_code')."</td>";                             
                                    echo "<td align='right'>".number_format($rs_rinciandetail->getString('komponen_harga'), 0, ',', '.')."</td>";
                                    echo "<td align='right'>".$rs_rinciandetail->getString('pajak')."</td>";
                                    echo "<td align='right'>".number_format($harga_ppn, 0, ',', '.')."</td>";
                                    echo "<td align='right'>".number_format($rs_rinciandetail->getString('nilai_anggaran'), 0, ',', '.')."</td>";
                                    // itung realisasi
                                    $unit_id = $rs_rinciandetail->getString('unit_id');
                                    $kode_kegiatan =$rs_rinciandetail->getString('kegiatan_code');
                                    $detail_no = $rs_rinciandetail->getString('detail_no');
                                    $totNilaiAlokasi = 0;
                                    $totNilaiRealisasi = 0;
                                    $totNilaiKontrak = 0;
                                    $totNilaiSwakelola = 0;
                                    $nilairealisasi = 0;
                                    $rs_rinciandetails = new DinasRincianDetail;
                                    $totNilaiAlokasi = $rs_rinciandetails->getCekNilaiAlokasiProject($unit_id, $kode_kegiatan, $detail_no);
                                    $totNilaiRealisasi = $rs_rinciandetails->getCekRealisasi($unit_id, $kode_kegiatan, $detail_no);
                                    $totNilaiKontrak = $rs_rinciandetails->getCekNilaiKontrakDelivery2($unit_id, $kode_kegiatan, $detail_no);
                                    $totNilaiSwakelola = $rs_rinciandetails->getCekNilaiSwakelolaDelivery2($unit_id, $kode_kegiatan, $detail_no);
                                    if ($totNilaiRealisasi > 0) {
                                        $nilairealisasi = $totNilaiRealisasi;
                                    } else if ($totNilaiKontrak > 0) {
                                        $nilairealisasi = $totNilaiKontrak;
                                    } else if ($totNilaiSwakelola > 0) {
                                        $nilairealisasi = $totNilaiSwakelola;
                                    } else if ($totNilaiSwakelola > 0 && $totNilaiKontrak > 0) {
                                        $nilairealisasi = $totNilaiSwakelola;
                                    }
                                    echo "<td align='right'>".number_format($nilairealisasi, 0, ',', '.')."</td>";
                                }
                                ?>
                                <?php if($counter <= 0): ?>
                                    <tr><td colspan="16" align="center">Tidak ada komponen yang bisa ditampilkan</td></tr>
                                <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>