
<!-- <div id="sf_admin_container">
    <h1 width='100%' align="center">RESUME RAPAT</h1>
    <h1 width='100%' align="center">TAHUN ANGGARAN 2020</h1>

    <table style="empty-cells: show;" border="1" cellpadding="3" cellspacing="0" class="sf_admin_list" width="100%">
        <tr>
            <td>
                <table width='100%'>
                    <tr>
                        <th>Nama SKPD</th>
                        <th>Kode Kegiatan</th>
                        <th>Nama Kegiatan</th>
                        <th>Satuan</th>
                        <th>Uraian Kode Rekening</th>
                        <th>Semula</th>
                        <th>Menjadi</th>
                        <th>Selisih</th> 
                        <th>Alasan/Keterangan</th>
                    </tr>
                    <tbody>
                        <?php                        
                        while ($rs->next()) {
                        ?>
                        <tr>
                            
                            <td><?php echo $rs->getString('unit_id'); ?></td>
                            <td><?php echo $rs->getString('kode_kegiatan'); ?></td>
                            <td><?php echo $rs->getString('nama_kegiatan'); ?></td>
                            <td><?php echo $rs->getString('rekening_code'); ?></td>
                            <td><?php echo $rs->getString('rekening_name'); ?></td>
                            <td><?php echo $rs->getString('semula'); ?></td>
                            <td><?php echo $rs->getString('menjadi'); ?></td>
                            <td><?php echo $rs->getString('catatan_pembahasan'); ?></td>
                        </tr>
                        <?php
                        
                        }
                        ?>
                    </tbody>
                </table>
            </td>
        </tr>
    </table>
</td>
</tr>
</table>

</div>
 -->
<?php use_helper('Form', 'Object', 'Javascript') ?>
<?php
if (sfConfig::get('app_tahap_edit') == 'murni') {
    $nama_sistem = 'Pra-RKA';
} elseif (sfConfig::get('app_tahap_edit') == 'pak') {
    $nama_sistem = 'PAK';
} elseif (sfConfig::get('app_tahap_edit') == 'penyesuaian') {
    $nama_sistem = 'Penyesuaian';
} else {
    $nama_sistem = 'Revisi';
}
?>

<div class="box box-info">        
    <div class="box-header with-border">
        Laporan Resume Kegiatan <?php echo $nama_sistem; ?>
    </div>
    <div class="box-body">
        <table class="table table-bordered">
            <tr>
                <th class="text-center">Dinas</th>
                <th class="text-center">Kode Kegiatan</th>
                <th class="text-center">Nama Kegiatan</th>
                <th class="text-center">Uraian Kode Rekening</th>
            </tr>
            <?php
            foreach ($kegiatan as $value):
                    if (isset($value['skpd_ok'])): 
            ?>
                     <tr><td colspan="3">&nbsp;</td></tr>
                    <tr>
                        <td colspan="3" class="text-left text-bold"><?php echo $value['unit_id'] . ' - ' . $value['unit_name'] ?></td>
                    </tr> 
                    <?php endif; ?>
                <tr>
                    <td>&nbsp;</td>
                    <td class="text-left" ><?php echo $value['kode_kegiatan'] ?></td>
                    <td class="text-left" ><?php echo $value['nama_kegiatan'] ?></td>
                    <td class="text-left" ><?php echo $value['rekening_name'] ?></td>
                </tr>
            <?php endforeach; ?>
        </table>
    </div>
</div>