<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="fas fa-align-left"></i> Laporan Per Dinas Per Belanja
                        </h3>
                    </div>
                    <div class="card-body"> 
                        <table class="table table-bordered">
                            <tr>
                                <th style="width: 10px"></th>
                                <th style="width: 10px"></th>
                                <th class="text-center">Dinas - Belanja</th>
                                <th class="text-center">Nilai Usulan</th>
                                <th class="text-center">Nilai Disetujui</th>
                                <th class="text-center">Persentase</th>
                            </tr>
                            <?php
                            $i = 0;
                            if ($rs->first() <> '') {
                                $rs->first();
                                do {
                                    ?>
                                    <?php if (isset($dinas_ok[$i])): ?>
                                        <tr><td colspan="6">&nbsp;</td></tr>
                                        <tr>
                                            <td colspan="3" class="text-left text-bold"><?php echo $unit_id[$i] ?> &nbsp; <?php echo $unit_name[$i] ?></td>
                                            <td class="text-right text-bold"><?php echo $nilai_draft[$i] ?></td>
                                            <td class="text-right text-bold"><?php echo $nilai_locked[$i] ?></td>
                                            <td class="text-right text-bold">&nbsp;</td>
                                            <?php
                                            $nilai_locked_total = str_replace('.', '', $nilai_locked[$i]);
                                            ?>
                                        </tr>
                                    <?php endif ?>
                                    <tr>
                                        <th style="width: 10px"></th>
                                        <th style="width: 10px"></th>
                                        <td class="text-left"><?php echo isset($subtitle[$i]) ? $subtitle[$i] : '' ?></td>
                                        <td class="text-right"><?php echo isset($nilai_draft4[$i]) ? $nilai_draft4[$i] : '' ?></td>
                                        <td class="text-right"><?php echo isset($nilai_locked4[$i]) ? $nilai_locked4[$i] : '' ?></td>
                                        <td class="text-right">
                                            <?php
                                            $nilai = str_replace('.', '', $nilai_locked4[$i]);
                                            echo number_format(($nilai / $nilai_locked_total) * 100, 2, ",", ".");
                                            ?>
                                        </td>
                                    </tr>
                                    <?php
                                    $i++;
                                } while ($rs->next());
                            }
                            ?>
                            <tr>
                                <td colspan="3" class="text-center text-bold bg-green-active">Total</td>
                                <td class="text-right text-bold bg-green-active"><?php echo $nilai_draft2 ?></td>
                                <td class="text-right text-bold bg-green-active"><?php echo $nilai_locked2 ?></td>
                                <td class="text-right text-bold bg-green-active">&nbsp;</td>
                            </tr>
                        </table> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- <div class="box box-info">        
    <div class="box-header with-border">
        
    </div>
    <div class="box-body">
              
    </div>
</div> -->