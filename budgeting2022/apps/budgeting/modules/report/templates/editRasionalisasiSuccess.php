<?php use_helper('I18N', 'Date', 'Url', 'Javascript', 'Form', 'Object', 'Number', 'Validation') ?>
<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Edit Rasionalisasi</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Lembar Kerja</a></li>
          <li class="breadcrumb-item active">Edit Rasionalisasi</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>
<!-- Main content -->
<section class="content">
    <?php include_partial('report/list_messages') ?>
     <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 stretch-card">                
                <div class="card card-default">
                    <div class="card-body">
                        <div class="alert alert-info alert-dismissible"> 
                  <h5><i class="icon fas fa-ban"></i> Info!</h5>                  
                   Maksimal pagu anggaran rasionalisasi sebesar : <?php echo number_format($pagu_rasionalisasi, 0, ',', '.')."<br>Nilai yang telah di isi sebesar : ".number_format($tot_rasionalisasi, 0, ',', '.')."<br>Sisa yang belum di rasionalisasi kan sebesar : ".number_format($tot_rasionalisasi-$pagu_rasionalisasi, 0, ',', '.');?>
                </div>
                    </div>
                </div>               
            </div>
        </div>
     </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 stretch-card">
                <div class="card"> 
                    <div class="card-body table-responsive p-0">
                        <table class="table table-hover">
                            <thead class="head_peach">
                                <tr>
                                    <th><b>Detail Kegiatan</b></th>
                                    <th><b>Nama Komponen</b></th>
                                    <th><b>Satuan</b></th>
                                    <th><b>Keterangan Koefisien</b></th>
                                    <th><b>Harga</b></th>
                                    <th><b>Nilai Anggaran Semula (Revisi 6)</b></th>
                                    <?php $arr_unit_id = array('2600','2300','3000','0308','2800','2400','3300','0300','1800','0700');
                                    //$arr_unit_id=array('9999');
                                    if (!in_array($unit_id, $arr_unit_id)) 
                                        echo "<th><b>Nilai Realisasi</b></th>";?>
                                    <th><b>Nilai Anggaran Menjadi (PAK Buku Putih)</b></th>
                                    <th><b>Nilai Rasionalisasi</b></th> 
                                    <?php if ( ($sf_user->hasCredential('admin') || $sf_user->hasCredential('peneliti')) && $sf_user->getNamaLogin() !== 'tim_shs' ){?>                                 
                                    <th><b>Nilai Inputan Baru</b></th> 
                                    <th><b>Action</b></th> 
                                    <?php } ?>
                                </tr>
                            </thead>                             

                            <tbody>
                            <?php
                                $counter = 0;
                                while($rs_rinciandetail->next()) {
                                $detail_kegiatan=$rs_rinciandetail->getString('detail_kegiatan');

                                $query = "SELECT *
                                    from " . sfConfig::get('app_default_schema') . ".rincian_detail rd
                                WHERE rd.status_hapus=false                   
                                     AND rd.detail_kegiatan='$detail_kegiatan'        
                                ORDER BY rd.komponen_name";
                                $con = Propel::getConnection();
                                $stmt1 = $con->prepareStatement($query);                               
                                $rs_copy = $stmt1->executeQuery();                                
                                if ($rs_copy ->next()) {
                                 $nilai_anggaran_rka = $rs_copy->getString('nilai_anggaran');
                                }

                                    $counter++;
                                    $nama = $rs_rinciandetail->getString('komponen_name');
                                    $style = '';
                                    echo form_tag('report/prosesUbahRasionalisasi');           
                                    echo "<tr $style>";
                                    echo "<td>".$rs_rinciandetail->getString('detail_kegiatan')."</td>";
                                    echo "<td>".$rs_rinciandetail->getString('komponen_name')." ".$rs_rinciandetail->getString('detail_name')."</td>";
                                    echo "<td>".$rs_rinciandetail->getString('satuan')."</td>";
                                    echo "<td>".$rs_rinciandetail->getString('keterangan_koefisien')."</td>";
                                    echo "<td align='right'>".number_format($rs_rinciandetail->getString('komponen_harga'), 0, ',', '.')."</td>";
                                    // echo "<td align='right'>".number_format($rs_rinciandetail->getString('nilai_anggaran'), 0, ',', '.')."</td>";
                                    echo "<td align='right'>".number_format( $nilai_anggaran_rka, 0, ',', '.')."</td>";
                                    $anggaran_rasionalisasi= $rs_rinciandetail->getString('anggaran_rasionalisasi');
                                    // $anggaran_menjadi=$rs_rinciandetail->getString('nilai_anggaran')- $rs_rinciandetail->getString('anggaran_rasionalisasi');
                                    $anggaran_menjadi=$rs_rinciandetail->getString('nilai_anggaran');
                                    $unit_id = $rs_rinciandetail->getString('unit_id');
                                    $kode_kegiatan = $rs_rinciandetail->getString('kegiatan_code');
                                    $detail_no =  $rs_rinciandetail->getString('detail_no');
                                    $nilairealisasi=0;
                                    
                                    if(!in_array($unit_id, $arr_unit_id))
                                    {
                                      // itung realisasi                                       
                                        $totNilaiRealisasi = 0;
                                        $totNilaiKontrak = 0;
                                        $totNilaiSwakelola = 0;
                                        $nilairealisasi = 0;
                                         $rs1_rinciandetail = new DinasRincianDetail;                                        
                                        $totNilaiRealisasi = $rs1_rinciandetail->getCekRealisasi($unit_id, $kode_kegiatan, $detail_no);
                                        $totNilaiKontrak = $rs1_rinciandetail->getCekNilaiKontrakDelivery2($unit_id, $kode_kegiatan, $detail_no);
                                        $totNilaiSwakelola = $rs1_rinciandetail->getCekNilaiSwakelolaDelivery2($unit_id, $kode_kegiatan, $detail_no);
                                        if ($totNilaiRealisasi > 0) {
                                            $nilairealisasi = $totNilaiRealisasi;
                                        } else if ($totNilaiKontrak > 0) {
                                            $nilairealisasi = $totNilaiKontrak;
                                        } else if ($totNilaiSwakelola > 0) {
                                            $nilairealisasi = $totNilaiSwakelola;
                                        } else if ($totNilaiSwakelola > 0 && $totNilaiKontrak > 0) {
                                            $nilairealisasi = $totNilaiSwakelola;
                                        }
                                    }
                                    if(!in_array($unit_id, $arr_unit_id))
                                    {
                                    echo "<td align='right'>".number_format($nilairealisasi, 0, ',', '.')."</td>";
                                    }
                                    if(is_null($anggaran_rasionalisasi))
                                    {
                                          echo "<td align='right'>".number_format($rs_rinciandetail->getString('nilai_anggaran'), 0, ',', '.')."</td>";

                                     } 
                                    else if ($nilairealisasi > $anggaran_menjadi && (!in_array($unit_id, $arr_unit_id)))
                                    {
                                         echo "<td align='right'>".number_format( $anggaran_menjadi, 0, ',', '.')."(Kurang dari nilai realisasi sebesar : ".number_format( $nilairealisasi, 0, ',', '.')."</td>";
                                     }                                                                              
                                      else
                                    {
                                       
                                       echo "<td align='right'>".number_format( $anggaran_menjadi, 0, ',', '.')."</td>";
                                    }

                                     if(is_null($anggaran_rasionalisasi))
                                    {
                                          echo "<td align='right'> </td>";

                                    } 
                                    else if ($nilairealisasi > $anggaran_menjadi &&(!in_array($unit_id, $arr_unit_id)))
                                    {
                                        // echo "<td align='right'>".number_format($anggaran_rasionalisasi, 0, ',', '.')." (Perlu Penyesuaian) </td>"; 
                                    }                                                                             
                                      else
                                    {                                    
                                      
                                       echo "<td align='right'>".number_format($anggaran_rasionalisasi, 0, ',', '.')."</td>";                                    

                                    }
                                     if ( ($sf_user->hasCredential('admin') || $sf_user->hasCredential('peneliti')) && $sf_user->getNamaLogin() !== 'tim_shs') {
                                        $boleh = false;
                                        $c_user_handle = new Criteria();
                                        $c_user_handle->add(UserHandleV2Peer::USER_ID, $sf_user->getNamaLogin(), Criteria::EQUAL);
                                        $user_handles = UserHandleV2Peer::doSelect($c_user_handle);
                                        // $user_kecuali = array('adam.yulian','adhitiya');

                                        foreach($user_handles as $user_handle){
                                            if($user_handle->getStatusUser() != 'shs'){
                                                $boleh = true;
                                            }
                                        }

                                        if($boleh==true){
                                            echo "<td>".input_tag('nilai_rasionalisasi')."</td>";
                                            echo "<td>";
                                            echo input_hidden_tag('detail_no', $rs_rinciandetail->getString('detail_no'));                                        
                                            echo input_hidden_tag('unit_id', $unit_id);
                                            echo input_hidden_tag('kode_kegiatan', $kode_kegiatan);
                                            echo input_hidden_tag('nilai_anggaran', $rs_rinciandetail->getString('nilai_anggaran'));
                                            echo submit_tag('Simpan', array('name' => 'proses', 'class' => 'btn btn-block btn-info btn-sm'));
                                            echo "</td>";
                                        }
                                    }
                                    echo "</tr>";
                                    echo "</form>";
                                }
                                ?>
                                <?php if($counter <= 0): ?>
                                    <tr><td colspan="16" align="center">Tidak ada komponen yang bisa ditampilkan</td></tr>
                                <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $("#cekSemua").change(function () {
        $(".cek").prop('checked', $(this).prop("checked"));
    });


    // var rupiah3 = document.getElementById('nilai_rasionalisasi');
    //     rupiah3.addEventListener('keyup', function(e){
    //         // tambahkan 'Rp.' pada saat form di ketik
    //         // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
    //         rupiah3.value = formatRupiah(this.value, 'Rp. ');
    // });
 
        /* Fungsi formatRupiah */
        function formatRupiah(angka, prefix){
            var number_string = angka.replace(/[^,\d]/g, '').toString(),
            split           = number_string.split(','),
            sisa            = split[0].length % 3,
            rupiah          = split[0].substr(0, sisa),
            ribuan          = split[0].substr(sisa).match(/\d{3}/gi);
 
            // tambahkan titik jika yang di input sudah menjadi angka ribuan
            if(ribuan){
                separator = sisa ? '.' : '';
                rupiah += separator + ribuan.join('.');
            }
 
            rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
            return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
    }

</script>