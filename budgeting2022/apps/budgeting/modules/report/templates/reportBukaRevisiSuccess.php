<?php use_helper('Form', 'Object', 'Javascript') ?>
<?php
if (sfConfig::get('app_tahap_edit') == 'murni') {
    $nama_sistem = 'Pra-RKA';
} elseif (sfConfig::get('app_tahap_edit') == 'pak') {
    $nama_sistem = 'PAK';
} elseif (sfConfig::get('app_tahap_edit') == 'penyesuaian') {
    $nama_sistem = 'Penyesuaian';
} else {
    $nama_sistem = 'Revisi';
}
?>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="fas fa-align-left"></i> Laporan Buka Kegiatan <?php echo $nama_sistem; ?>
                        </h3>
                    </div>
                    <div class="card-body"> 
                        <table class="table table-bordered">
                            <tr>
                                <th class="text-center">Dinas</th>
                                <th class="text-center">Kode Kegiatan</th>
                                <th class="text-center">Nama Kegiatan</th>
                            </tr>
                            <?php
                            foreach ($kegiatan as $value):
                                    if (isset($value['skpd_ok'])): 
                            ?>
                                    <tr><td colspan="3">&nbsp;</td></tr>
                                    <tr>
                                        <td colspan="3" class="text-left text-bold"><?php echo $value['unit_id'] . ' - ' . $value['unit_name'] ?></td>
                                    </tr> 
                                    <?php endif; ?>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td class="text-left" ><?php echo $value['kegiatan_code'] ?></td>
                                    <td class="text-left" ><?php echo $value['nama_kegiatan'] ?></td>
                                </tr>
                            <?php endforeach; ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- <div class="box box-info">        
    <div class="box-header with-border">
        
    </div>
    <div class="box-body">
        
    </div>
</div> -->
<script>
    $("#unit_id").change(function () {
        $('#indicator').show();
        var id = $(this).val();
        if (id == '') {
            id = '0000';
        }
        if (id2 == '') {
            id2 = 'xxx';
        }
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/report/reportBukaRevisi/unit_id/" + id + ".html",
            context: document.body
        }).done(function (msg) {
            $('#indicator').hide();
            $('#isi').html(msg);
        });

    });
</script>