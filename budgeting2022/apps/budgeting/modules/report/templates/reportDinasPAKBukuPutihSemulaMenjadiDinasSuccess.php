<div class="box box-info">        
    <div class="box-header with-border">
        Laporan Per Dinas
    </div>
    <div class="box-body">
        <table class="table table-bordered">
            <tr>
                <th class="text-center">Dinas</th>
                <th class="text-center">Pagu</th>
                <!-- <th class="text-center">Jumlah Usulan</th> -->
                <th class="text-center">Penyesuaian Pagu</th>
                <th class="text-center">Pagu Final</th>
                <!-- <th class="text-center">Nilai Usulan</th> -->
                <!-- <th class="text-center">Jumlah Disetujui</th> -->
                <th class="text-center">Rincian Nilai Usulan</th>
                <th class="text-center">Nilai Disetujui</th>
                <th class="text-center">Setuju - Pagu</th>
            </tr>
            <?php
            // $rs_new_que->first();
            // do {
            //     echo $rs_new_que->getString('unit_id');die();
            // } while ($rs_new_que->next());
            $tot_pag = 0;
            $tot_pagu_tmbhan = 0;
            $tot_pagu_final_pak = 0;
            $tot_rincian_nilai_usulan = 0;
            $tot_disetujui = 0;
            $setujupagu = 0;
            $i = 0;
            $p = 0;
            $q = 0;
            $z = 0;
            //$rs->first();
            $rs_new_que_kua_ppas->first();
            do {
                ?>
                <tr>
                    <td class="text-left">
                        <?php 
                            echo $rs_new_que_kua_ppas->getString('unit_id') . ' - ' . $rs_new_que_kua_ppas->getString('unit_name') 
                        ?>
                    </td>
                    <td class="text-right">
                        <?php 
                            //echo $pagu[$i] 
                            $tot_pag += $rs_new_que_kua_ppas->getString('tot_anggaran');
                            echo number_format($rs_new_que_kua_ppas->getString('tot_anggaran'), 0, ",", ".");
                        ?>
                    </td>
                    <td class="text-center">
                        <?php 
                            $tot_pagu_tmbhan += $pagu_tmbhan[$i]; 
                            echo number_format($pagu_tmbhan[$i], 0, ",", ".");
                        ?>
                    </td>

                    <td class="text-right">
                        <?php 
                            //echo $nilai_usulan[$i]
                            $tot_pagu_final_pak += $rs_new_que_kua_ppas->getString('tot_anggaran')+$pagu_tmbhan[$i];
                            // echo $rs_new_que_kua_ppas->getString('tot_anggaran')+$pagu_tmbhan[$i];
                            echo number_format($rs_new_que_kua_ppas->getString('tot_anggaran')+$pagu_tmbhan[$i], 0, ",", ".");
                        ?>
                    </td>

                    <td class="text-center">
                        <?php 
                            //echo ($rs->getString('total_locked') < 0) ? '0' : $rs->getString('total_locked') 
                            $tot_rincian_nilai_usulan += $smw_rinc[$i]; 
                            echo number_format($smw_rinc[$i], 0, ",", ".");
                        ?>
                    </td>
                    
                    <td class="text-right">
                    <?php //echo number_format(($rs->getString('nilai_locked') < 0) ? '0' : $rs->getString('nilai_locked'), 0, ",", ".") ?>
                    <?php //echo $nilai_diset[$z];
                        //echo number_format($nilai_diset[$z], 0, ",", ".");
                        //echo $rs_new_que_kua_ppas->getString('total_alokasi_dana')+$rs_new_que_kua_ppas->getString('total_tambahan_pagu');
                        // echo $disetujui[$i];
                        $unit_id_skr = $rs_new_que_kua_ppas->getString('unit_id');
                        if ($unit_id_disetujui[$p] != $unit_id_skr) {
                            $nol[$p] = 0;
                            $tot_disetujui += $nol[$p];
                            echo number_format($nol[$p], 0, ",", ".").'<br>';
                            // echo $tot_disetujui;
                        }
                        else {
                            $tot_disetujui += $disetujui[$p]; 
                            echo number_format($disetujui[$p], 0, ",", ".")."<br>";
                            // echo $tot_disetujui;
                            // echo $unit_id_disetujui[$p]." = ".$unit_id_skr;
                            $p++;
                        }
                        // $tot_rincian_nilai_usulan += $disetujui[$p]; 
                        // echo $smw_rinc[$i];

                    ?>
                        
                    </td>

                    <td class="text-right">
                    <?php 
                        //echo ($rs->getString('nilai_locked') <= 0) ? '0' : $selisih[$i] 
                        //$nil_disetujui = str_replace('.', '', $nilai_diset[$z]);
                        //$nil_pagu = str_replace('.', '', $pagu[$i]);
                        //$sel_per_keg = $nil_disetujui - $nil_pagu;
                        //echo number_format($sel_per_keg, 0, ",", ".")
                        
                        $unit_id_skr_setuju = $rs_new_que_kua_ppas->getString('unit_id');
                        if ($unit_id_disetujui[$q] != $unit_id_skr_setuju) {
                            $nol[$q] = 0;
                            //$setujupagu += $nol[$q];
                            $pg_fin_pak = $rs_new_que_kua_ppas->getString('tot_anggaran')+$pagu_tmbhan[$i];

                            $set_pg_fin = $nol[$q]-$pg_fin_pak;
                            echo number_format($set_pg_fin, 0, ",", ".").'<br>';
                            //echo "aa";
                            $setujupagu += $set_pg_fin;
                        }
                        else {
                            // $setujupagu += $disetujui[$q]; 
                            $pg_fin_pak = $rs_new_que_kua_ppas->getString('tot_anggaran')+$pagu_tmbhan[$i];
                            
                            $set_pg_fin = $disetujui[$q]-$pg_fin_pak;
                            echo number_format($set_pg_fin, 0, ",", ".")."<br>";
                            //echo $disetujui[$q]-$pg_fin_pak."<br>";
                            // echo $tot_disetujui;
                            // echo $unit_id_disetujui[$p]." = ".$unit_id_skr;
                            $setujupagu += $set_pg_fin;
                            $q++;
                        }

                        //echo $disetujui[$q];
                    ?>
                    </td>

                </tr>
                <?php $z++; ?>
                <?php $i++; ?>
                <?php
            // } while ($rs->next());
            } while ($rs_new_que_kua_ppas->next());
            ?>
            <tr>
                <td class="text-center text-bold bg-green-active">Total</td>
                <td class="text-right text-bold bg-green-active">
                    <?php 
                    //echo $total_pagu;
                    echo number_format($tot_pag, 0, ",", ".");
                    ?>
                </td>
                <td class="text-center text-bold bg-green-active">
                    <?php 
                        //echo $total_draft2
                        echo number_format($tot_pagu_tmbhan, 0, ",", "."); 
                    ?>
                </td>
                <td class="text-right text-bold bg-green-active">
                <?php 
                    echo number_format($tot_pagu_final_pak, 0, ",", "."); 
                ?>
                </td>
                <td class="text-center text-bold bg-green-active">
                    <?php 
                    echo number_format($tot_rincian_nilai_usulan, 0, ",", ".");
                    ?>
                </td>
                
                <td class="text-right text-bold bg-green-active">
                <?php 
                echo number_format($tot_disetujui, 0, ",", ".");
                
                ?>
                </td>

                <td class="text-right text-bold bg-green-active">
                <?php 
                echo number_format($setujupagu, 0, ",", ".");
                //echo ($total_locked2 < 0) ? '0' : $total_selisih 
                // $total_sum - $total_pau
                // $total_total_sum = str_replace('.', '', $total_sum);
                // $total_total_pagu = str_replace('.', '', $total_pagu);
                // $total_sel = $total_total_sum - $total_total_pagu;
                // echo number_format($total_sel, 0, ",", ".")
                ?>
                    
                </td>
            </tr>
        </table>       
    </div>
</div>