<?php use_helper('Form', 'Object', 'Javascript') ?>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="fas fa-align-left"></i> Print Excel Resume Rapat Semua SKPD
                        </h3>
                    </div>
                    <div class="card-body">
                        <?php echo form_tag('report/prosesResumeRapatAll', array('method' => 'post', 'class' => 'form-horizontal', 'target' => '_blank', 'style' => 'margin-bottom: 10px')); ?>
                            <div class="row mb-2">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Perangkat Daerah</label>
                                        <?php echo select_tag('ambil_skpd', objects_for_select($list_skpd, 'getUnitId', 'getUnitName', '', 'include_custom=--Pilih Perangkat Daerah--'), array('class' => 'js-example-basic-multiple select2', 'style' => 'width:100%', 'multiple' => true)); ?>
                                    </div>
                                </div>                    
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <button type="submit" name="commit" class="btn btn-outline-primary btn-sm">Print File Excel Resume Rapat Per SKPD</button>
                                    </div>
                                </div>
                            </div>
                        <?php echo '</form>'; ?>
                        <?php echo link_to(image_tag('b_print.png') . ' Print File Excel Resume Rapat Semua SKPD', 'report/prosesResumeRapatAll', array('class' => 'btn btn-sm btn-block btn-outline-primary text-bold col-xs-4', 'target' => '_blank')); ?>
                        <?php echo link_to(image_tag('b_print.png') . ' Print File Excel Resume Rapat Revisi 6', 'report/prosesResumeRapatRev', array('class' => 'btn btn-sm btn-block btn-outline-primary text-bold col-xs-4', 'target' => '_blank')); ?> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    $(document).ready(function () {
        $(".js-example-basic-multiple").select2();
    });
</script>