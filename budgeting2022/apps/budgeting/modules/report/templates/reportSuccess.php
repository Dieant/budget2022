<?php use_helper('I18N', 'Date', 'Object', 'Javascript', 'Validation') ?>
<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Laporan</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Lembar Kerja</a></li>
          <li class="breadcrumb-item active">Laporan</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">
                        <i class="fas fa-filter"></i> Pilih
                    </h3>
                </div>
                <div class="card-body">
                    <?php include_partial('report/pilihReport') ?>
                    <div id="indicator" style="display:none;" align="center">
                        <dt>&nbsp;</dt>
                        <dd><b>Mohon Tunggu </b><br/>
                            <?php echo image_tag('loading.gif', array('align' => 'absmiddle')) ?></dd>
                    </div>
                </div>
            </div>
          </div>
        </div>
    </div>
    <div id="isi" class="table-responsive"></div>
    <div id="isi_dalam" class="table-responsive"></div>
</section>

<script>
    $("#combo_pilih").change(function () {
        $('#isi').html();
        $('#indicator').show();
        var id = $(this).val();
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/report/tampilReport/b/" + id + ".html",
            context: document.body
        }).done(function (msg) {
          $('#isi').html('');
          $('#isi_dalam').html('');

          $('#isi').html(msg);
          $('#indicator').hide();
        });
    });
</script>