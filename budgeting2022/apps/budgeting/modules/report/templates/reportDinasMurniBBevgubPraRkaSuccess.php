<div class="box box-info">        
    <div class="box-header with-border">
        Perbandingan Buku Biru - Buku Biru Pra EvaGub
    </div>
    <div class="box-body">
        <table class="table table-bordered">
            <tr>
                <th class="text-center">No.</th>
                <th class="text-center">Dinas</th>
                <th class="text-center">Kegiatan</th>
                <th class="text-center">Pagu Buku Biru Pra EvaGub</th>
                <th class="text-center">Rincian / Usulan (Buku Biru)</th>
                <th class="text-center">Selisih</th>
                <th class="text-center">Nilai Disetujui</th>
                <th class="text-center">Rincian - Disetujui</th>
            </tr>
            <?php

            $tot_pag_bb_pra = 0;
            $tot_rinc_usul = 0;
            $tot_selisih = 0;
            $tot_nilai_disetujui = 0;
            $tot_rinc_disetujui = 0;
            $no = 1;
            $rs->first();
            do {
                ?>
                <tr>
                    <td class="text-left">
                        <?php 
                            echo $no++.".";
                        ?>
                    </td>
                    <td class="text-left">
                        <?php 
                            echo $rs->getString('unit_id') . ' - ' . $rs->getString('unit_name');
                        ?>
                    </td>
                    <td class="text-right">
                        <?php 
                            echo $rs->getString('kegiatan_code');
                            // $tot_pag += $rs_new_que_kua_ppas->getString('tot_anggaran');
                            // echo number_format($rs_new_que_kua_ppas->getString('tot_anggaran'), 0, ",", ".");
                        ?>
                    </td>
                    <td class="text-center">
                        <?php 
                            // echo $rs->getString('pagu_bb');
                            echo number_format($rs->getString('pagu_bb'), 0, ",", ".");
                            $tot_pag_bb_pra += $rs->getString('pagu_bb'); 
                            // echo number_format($pagu_tmbhan[$i], 0, ",", ".");
                        ?>
                    </td>

                    <td class="text-right">
                        <?php 
                            // echo $rs->getString('rincian_usulan');
                            echo number_format($rs->getString('rincian_usulan'), 0, ",", ".");
                            $tot_rinc_usul += $rs->getString('rincian_usulan');
                            // echo number_format($rs_new_que_kua_ppas->getString('tot_anggaran')+$pagu_tmbhan[$i], 0, ",", ".");
                        ?>
                    </td>

                    <td class="text-center">
                        <?php 
                            // echo $rs->getString('selisih');
                            echo number_format($rs->getString('selisih'), 0, ",", ".");
                            $tot_selisih += $rs->getString('selisih'); 
                            // echo number_format($smw_rinc[$i], 0, ",", ".");
                        ?>
                    </td>
                    
                    <td class="text-right">
                        <?php 
                            // echo $rs->getString('disetujui');
                        echo number_format($rs->getString('disetujui'), 0, ",", ".");
                            $tot_nilai_disetujui += $rs->getString('disetujui');
                        ?>
                        
                    </td>

                    <td class="text-right">
                        <?php 
                            // echo $rs->getString('selisih2');
                            echo number_format($rs->getString('selisih2'), 0, ",", ".");
                            $tot_rinc_disetujui += $rs->getString('selisih2');
                        ?>
                    </td>

                </tr>

                <?php
            // } while ($rs->next());
            } while ($rs->next());
            ?>

            <tr>
                <td class="text-center text-bold bg-green-active" colspan="3">Total</td>
                <td class="text-right text-bold bg-green-active">
                    <?php 
                    echo number_format($tot_pag_bb_pra, 0, ",", ".");
                    ?>
                </td>
                <td class="text-center text-bold bg-green-active">
                    <?php 
                        echo number_format($tot_rinc_usul, 0, ",", "."); 
                    ?>
                </td>
                <td class="text-right text-bold bg-green-active">
                <?php 
                    echo number_format($tot_selisih, 0, ",", "."); 
                ?>
                </td>
                <td class="text-center text-bold bg-green-active">
                    <?php 
                    echo number_format($tot_nilai_disetujui, 0, ",", ".");
                    ?>
                </td>
                
                <td class="text-right text-bold bg-green-active">
                <?php 
                echo number_format($tot_rinc_disetujui, 0, ",", ".");
                
                ?>
                </td>


            </tr>
        </table>       
    </div>
</div>