<?php use_helper('Form', 'Object', 'Javascript') ?>
<?php
if (sfConfig::get('app_tahap_edit') == 'murni') {
    $nama_sistem = 'Pra-RKA';
} elseif (sfConfig::get('app_tahap_edit') == 'pak') {
    $nama_sistem = 'PAK';
} elseif (sfConfig::get('app_tahap_edit') == 'penyesuaian') {
    $nama_sistem = 'Penyesuaian';
} else {
    $nama_sistem = 'Revisi';
}
?>
<div class="box box-info">
    <div class="box-header with-border">
        Nama Komponen
    </div>
    <div class="box-body">
        <?php
        echo input_tag('komponen', $filter_komponen, array('id' => 'komponen', 'class' => 'form-control'));
        ?>
    </div>
    <div class="box-body">
        <?php echo link_to_function('Filter', 'filterKomponen()', array('class' => 'btn btn-flat btn-success')); ?>
    </div>
</div>
<div class="box box-info">
    <div class="box-header with-border">
        Laporan Perbandingan Komponen dengan Tahun Lalu
    </div>
    <div class="box-body  table-responsive">
        <table class="table table-bordered">
            <tr>
                <th colspan="5" class="text-center">SEKARANG</th>
                <th colspan="5" class="text-center">TAHUN LALU</th>
            </tr>
            <tr>
                <th class="text-center">Komponen</th>
                <th class="text-center">Tipe</th>
                <th class="text-center">Satuan</th>
                <th class="text-center">Pajak</th>
                <th class="text-center">Harga</th>
                <th class="text-center">Komponen</th>
                <th class="text-center">Tipe</th>
                <th class="text-center">Satuan</th>
                <th class="text-center">Pajak</th>
                <th class="text-center">Harga</th>
            </tr>
            <?php
            foreach ($komponen as $value):
                if ($value['komponen_non_pajak_1'] == 't') {
                    $pajak1 = 0;
                } elseif ($value['komponen_non_pajak_1'] == 'f') {
                    $pajak1 = 10;
                } else {
                    $pajak1 = '';
                }
                if ($value['komponen_non_pajak_2'] == 't') {
                    $pajak2 = 0;
                } elseif ($value['komponen_non_pajak_2'] == 'f') {
                    $pajak2 = 10;
                } else {
                    $pajak2 = '';
                }
                ?>
                <tr>
                    <td class="text-left" ><?php echo $value['komponen_id_1'] . ' - ' . $value['komponen_name_1'] ?></td>
                    <td class="text-left" ><?php echo $value['komponen_tipe_1'] ?></td>
                    <td class="text-left" ><?php echo $value['komponen_satuan_1'] ?></td>
                    <td class="text-left" ><?php echo $pajak1 ?></td>
                    <td class="text-right" ><?php echo number_format($value['komponen_harga_1'], 0, ',', '.') ?></td>
                    <td class="text-left" ><?php echo $value['komponen_id_2'] . ' - ' . $value['komponen_name_2'] ?></td>
                    <td class="text-left" ><?php echo $value['komponen_tipe_2'] ?></td>
                    <td class="text-left" ><?php echo $value['komponen_satuan_2'] ?></td>
                    <td class="text-left" ><?php echo $pajak2 ?></td>
                    <td class="text-right" ><?php echo number_format($value['komponen_harga_2'], 0, ',', '.') ?></td>
                </tr>
            <?php endforeach; ?>
        </table>       
    </div>
</div>
<script>
    function filterKomponen() {
        $('#indicator').show();
        var id = $("#komponen").val();
        if (id == '') {
            id = '';
        }
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/report/reportKomponenTahunLalu/komponen/" + id + ".html",
            context: document.body
        }).done(function (msg) {
            $('#indicator').hide();
            $('#isi').html(msg);
        });
    }
</script>