<?php use_helper('Form', 'Object', 'Javascript') ?>
<div class="box box-info">
    <div class="box-header with-border">
        Laporan Perbandingan Waktu Edit dengan Tarik ke eProject
    </div>
    <div class="box-body">
        <?php if ($kegiatan == null): ?>
            <div class="text-center">Semua kegiatan sudah tersinkron dengan eProject</div>
        <?php else: ?>
            <table class="table table-bordered">
                <tr>
                    <th class="text-center">Dinas</th>
                    <th class="text-center">Kode Kegiatan</th>
                    <th class="text-center">Nama Kegiatan</th>
                    <th class="text-center">Waktu Edit</th>
                    <th class="text-center">Waktu Tarik eProject</th>
                </tr>
                <?php
                foreach ($kegiatan as $value):
                    if (isset($value['skpd_ok'])):
                        ?>
                        <tr><td colspan="5">&nbsp;</td></tr>
                        <tr>
                            <td colspan="5" class="text-left text-bold"><?php echo $value['unit_id'] . ' - ' . $value['unit_name'] ?></td>
                        </tr>
                    <?php endif; ?>
                    <tr>
                        <td>&nbsp;</td>
                        <td class="text-left" ><?php echo $value['kode_kegiatan'] ?></td>
                        <td class="text-left" ><?php echo $value['nama_kegiatan'] ?></td>
                        <td class="text-left" ><?php echo $value['waktu_edit'] ?></td>
                        <td class="text-left" ><?php echo $value['waktu_tarik_eproject'] ?></td>
                    </tr>
                <?php endforeach; ?>
            </table>
        <?php endif; ?>
    </div>
</div>
<script>
    $("#unit_id").change(function () {
        $('#indicator').show();
        var id = $(this).val();
        var id2 = $("#posisi").val();
        if (id == '') {
            id = '0000';
        }
        if (id2 == '') {
            id2 = 'xxx';
        }
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/report/reportPosisiRevisi/unit_id/" + id + "/posisi/" + id2 + ".html",
            context: document.body
        }).done(function (msg) {
            $('#indicator').hide();
            $('#isi').html(msg);
        });

    });
    $("#posisi").change(function () {
        $('#indicator').show();
        var id = $("#unit_id").val();
        var id2 = $(this).val();
        if (id == '') {
            id = '0000';
        }
        if (id2 == '') {
            id2 = 'xxx';
        }
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/report/reportPosisiRevisi/unit_id/" + id + "/posisi/" + id2 + ".html",
            context: document.body
        }).done(function (msg) {
            $('#indicator').hide();
            $('#isi').html(msg);
        });
    });
</script>