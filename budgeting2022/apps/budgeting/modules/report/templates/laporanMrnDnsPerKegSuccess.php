<div class="box box-info">
    <div class="box-header with-border">
        Laporan Per Dinas Per Kegiatan Buku Putih
    </div>
    <div class="box-body">
        <table class="table table-bordered">
            <tr>
                <th class="text-center">Nama SKPD</th>
                <th class="text-center">Kode Kegiatan</th>
                <th class="text-center">Nama Kegiatan</th>
                <!-- <th class="text-center">Semula</th> -->
                <th class="text-center">Pagu</th>
                <th class="text-center">Pagu Tambahan</th>
                <th class="text-center">Pagu Final</th>
                <th class="text-center">Rincian</th>
                <th class="text-center">Nilai Disetujui</th>
                <th class="text-center">Selisih</th>
            </tr>



                    <?php
                    //$k = 0;
                    $totalSem = 0;
                    $totalSelisi = 0;
                    $totalKua = 0;
                    $totalRinc = 0;
                    $totaltambahan_pagu = 0;
                    $totalpagu_final = 0;
                    $totalRinc = 0;
                    $totalDisetujui = 0;
                    $selisi = 0;
                    foreach ($que2 as $key => $valuea) {
                    ?>
                        <tr>
                            <td class="text-left" ><?php echo $valuea['unit_name'] ?></td>                            
                            <td class="text-left" ><?php echo $valuea['kode_kegiatan'] ?></td>
                            <td class="text-left" ><?php echo $valuea['nama_kegiatan'] ?></td>

                            <td class="text-right" >
                                <?php 
                                    $totalKua += $valuea['alokasi_dana'];
                                    echo number_format($valuea['alokasi_dana'], 0, ',', '.');

                                    // $esh0 = 0;
                                ?>
                            </td>
                            <td class="text-right" >
                                <?php 
                                    $totaltambahan_pagu += $valuea['tambahan_pagu'];
                                    echo number_format($valuea['tambahan_pagu'], 0, ',', '.');

                                    // $esh0 = 0;
                                ?>
                            </td>
                            <td class="text-right" >
                                 <?php 
                                    // $totalPagu = $valuea['pagukua'] + $valuea['tambahan_pagu'];
                                    // echo number_format($totalPagu, 0, ',', '.');
                                    $totalpagu_final += $valuea['pagu_final'];
                                    echo number_format($valuea['pagu_final'], 0, ',', '.');


                                ?>                               
                            </td>
                            <td class="text-right" >
                                <?php 
                                    $totalRinc += $valuea['rincian'];
                                    echo number_format($valuea['rincian'], 0, ',', '.');
                                ?>
                            </td>
                            <td class="text-right" >
                                <?php 
                                    $totalDisetujui += $valuea['nilai_disetujui'];
                                    echo number_format($valuea['nilai_disetujui'], 0, ',', '.');
                                    // echo '-';
                                ?>
                            </td>
                            <td class="text-right" >
                                <?php 
                                    $totalSelisi += $selisi;
                                    $selisi = $valuea['pagu_final'] - $valuea['nilai_disetujui'];
                                    // if ($selisi == 0){
                                    //     echo 'aa';
                                    // }
                                    echo number_format($selisi, 0, ',', '.');
                                ?>
                            </td>
                        </tr>                    
                    <?php
                    //$k++;
                    }
                    ?>
                        <tr>

                            <td colspan="3" class="text-center"><h5><b>TOTAL</b></h5></td>
                            
                            <td class="text-right" ><b><?php echo number_format($totalKua, 0, ',', '.');?></b></td>
                            <td class="text-right" ><b><?php echo number_format($totaltambahan_pagu, 0, ',', '.');?></b>
                            </td>
                            <td class="text-right" ><b><?php echo number_format($totalpagu_final, 0, ',', '.');?></b>
                            </td>
                            <td class="text-right" >
                                <b><?php echo number_format($totalRinc, 0, ',', '.');?><b>
                            </td>
                            <td class="text-right" >
                                <b><?php echo number_format($totalDisetujui, 0, ',', '.');?><b>
                            </td>
                            <td class="text-right"><b><?php echo number_format($totalSelisi, 0, ',', '.');?></b></td>
                        </tr>

                        
        </table>     
    </div>
</div>