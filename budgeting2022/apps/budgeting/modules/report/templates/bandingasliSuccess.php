<?php use_helper('I18N', 'Date', 'Object', 'Javascript', 'Number', 'Validation') ?>
<?php
$co = new Criteria();
$co->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
$co->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
$master_keg = DinasMasterKegiatanPeer::doSelectOne($co);
$sesuatu = false;
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Perubahan Sub Kegiatan - (<?php echo $kode_kegiatan; ?>)</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Lembar Kerja</a></li>
                    <li class="breadcrumb-item active">Perubahan Sub-Kegiatan</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<!-- Main content -->
<section class="content">
    <?php include_partial('report/list_messages'); ?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <?php
                        if ($sf_user->hasCredential('dinas')) {
                            echo form_tag('entri/prosesentripptk');
                        } elseif ($sf_user->hasCredential('pptk')) {
                            $unit_tanpa_kpa = array('');
                            if(in_array($unit_id, $unit_tanpa_kpa)){
                                $c_hapus = new Criteria();
                                $c_hapus->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
                                $c_hapus->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
                                $c_hapus->add(DinasRincianDetailPeer::STATUS_HAPUS, FALSE);
                                $c_hapus->add(DinasRincianDetailPeer::STATUS_LEVEL_TOLAK, 4);
                                $c_hapus->add(DinasRincianDetailPeer::STATUS_LEVEL, 1);
                                if (DinasRincianDetailPeer::doSelectOne($c_hapus)){
                                    echo form_tag('entri/prosespptkanggaran');
                                }else{
                                    echo form_tag('entri/prosespptkpa');
                                }
                            }else{
                                echo form_tag('entri/prosespptkkpa');
                            }
                        } elseif ($sf_user->hasCredential('kpa')) {
                            $c_hapus = new Criteria();
                            $c_hapus->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
                            $c_hapus->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
                            $c_hapus->add(DinasRincianDetailPeer::STATUS_HAPUS, FALSE);
                            $c_hapus->add(DinasRincianDetailPeer::STATUS_LEVEL_TOLAK, 4);
                            $c_hapus->add(DinasRincianDetailPeer::STATUS_LEVEL, 2);
                            if (DinasRincianDetailPeer::doSelectOne($c_hapus))
                                echo form_tag('entri/proseskpaanggaran');
                            else
                                echo form_tag('entri/proseskpapa');
                        } elseif ($sf_user->hasCredential('pa')) {
                            $c_pa = new Criteria();
                            $c_pa->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
                            $c_pa->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
                            $c_pa->add(DinasRincianDetailPeer::STATUS_HAPUS, FALSE);
                            $c_pa->add(DinasRincianDetailPeer::STATUS_LEVEL, 5);
                            $c_kegiatan = new Criteria();
                            $c_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                            $c_kegiatan->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                            $c_kegiatan->add(DinasMasterKegiatanPeer::STATUS_LEVEL, 5);
                            if (DinasRincianDetailPeer::doSelectOne($c_pa) || DinasMasterKegiatanPeer::doSelectOne($c_kegiatan))
                                echo form_tag('entri/prosespapenyelia');
                            else
                                echo form_tag('entri/prosespaanggaran');
                        } elseif ($sf_user->hasCredential('peneliti')) {
                            echo form_tag('peneliti/prosesUbahStatusDinas');
                        } elseif ($sf_user->getNamaUser() == 'anggaran' || $sf_user->getNamaUser() == 'bagian_hukum' || $sf_user->getNamaUser() == 'inspektorat' || $sf_user->getNamaUser() == 'bkd' || $sf_user->getNamaUser() == 'bkpsdm' || $sf_user->getNamaUser() == 'lppa' || $sf_user->getNamaUser() == 'bapenda' || $sf_user->getNamaUser() == 'bagian_organisasi') {
                            echo form_tag('anggaran/prosesapprovetapd');
                        } elseif ($sf_user->hasCredential('bappeko')) {
                            echo form_tag('anggaran/prosesapprovebappeko');
                        } elseif ($sf_user->hasCredential('admin_super') || $sf_user->hasCredential('admin')) {
                            echo form_tag('admin/prosesStatusRevisi');
                        } else {
                            echo '<form>';
                        }
                        echo input_hidden_tag('unit_id', $unit_id);
                        echo input_hidden_tag('kode_kegiatan', $kode_kegiatan);
                    ?>
                    <div class="card-body text-center">
                        <div class="btn-group btn-group-justified" role="group">
                        <?php
                        $is_penyelia_bpkad = false;
                        $penyelia_bpkad = new Criteria();
                        $penyelia_bpkad->add(PenyeliaBpkadPeer::NAMA, $sf_user->getNamaLogin(), Criteria::EQUAL);
                        if ($rs_penyelia = PenyeliaBpkadPeer::doSelectOne($penyelia_bpkad)) {
                            $is_penyelia_bpkad = true;
                        }
                        // $penyelia_bpkad = array('winda','judi','tutik','febri','wike','lucky');
                        if ($is_penyelia_bpkad == true || $sf_user->getNamaUser() == 'bagian_hukum' || $sf_user->getNamaUser() == 'inspektorat' || $sf_user->getNamaUser() == 'bkd' || $sf_user->getNamaUser() == 'bkpsdm' || $sf_user->getNamaUser() == 'lppa' || $sf_user->getNamaUser() == 'bapenda' || $sf_user->getNamaUser() == 'bagian_organisasi' || $sf_user->hasCredential('bappeko') || $sf_user->getNamaUser() == 'bappeko' || $sf_user->getNamaUser() == 'bppk')
                        {
                            echo link_to('<i class="fas fa-print"></i> Print Berita Acara/Resume Rapat', 'anggaran/resumeRapat?kode_kegiatan=' . $kode_kegiatan . '&unit_id=' . $unit_id, array('class' => 'btn btn-outline-info btn-sm text-bold col-xs-3', 'target' => '_blank'));
                            echo link_to('<i class="fas fa-print"></i> Print Perbandingan Pergeseran', 'report/printBandingAsli?kode_kegiatan=' . $kode_kegiatan . '&unit_id=' . $unit_id . '&log=1', array('class' => 'btn btn-outline-info btn-sm text-bold col-xs-4', 'target' => '_blank'));
                            echo link_to('Perbandingan Semula-Menjadi', 'report/tampillaporanaslisemua?kode_kegiatan=' . $kode_kegiatan . '&unit_id=' . $unit_id, array('class' => 'btn btn-outline-info btn-sm text-bold col-xs-3'));
                            echo link_to('<i class="fas fa-print"></i> Print Lembar Verifikasi Usulan Pergeseran', 'report/printLembarVerifikasiUsulanPergeseran?kode_kegiatan=' . $kode_kegiatan . '&unit_id=' . $unit_id, array('class' => 'btn btn-outline-info btn-sm text-bold col-xs-4', 'target' => '_blank'));
                        }
                        elseif (($sf_user->hasCredential('kpa') || $sf_user->hasCredential('pa')) && !$status)
                        {
                            echo link_to('<i class="fas fa-print"></i> Print Perbandingan Pergeseran', 'report/printBandingAsli?kode_kegiatan=' . $kode_kegiatan . '&unit_id=' . $unit_id . '&log=1', array('class' => 'btn btn-outline-info btn-sm text-bold col-xs-4', 'target' => '_blank'));
                            echo link_to('Perbandingan Semula-Menjadi', 'report/tampillaporanaslisemua?kode_kegiatan=' . $kode_kegiatan . '&unit_id=' . $unit_id, array('class' => 'btn btn-outline-info btn-sm text-bold col-xs-4'));
                        }
                        elseif ($sf_user->hasCredential('dinas'))
                        {
                            if (sfConfig::get('app_tahap_edit') != 'murni')
                            {
                                echo link_to('<i class="fas fa-print"></i> Print Perbandingan Pergeseran', 'report/printBandingAsli?kode_kegiatan=' . $kode_kegiatan . '&unit_id=' . $unit_id . '&log=1', array('class' => 'btn btn-outline-info btn-sm text-bold col-xs-4', 'target' => '_blank'));
                                echo link_to('Perbandingan Semula-Menjadi', 'report/tampillaporanaslisemua?kode_kegiatan=' . $kode_kegiatan . '&unit_id=' . $unit_id, array('class' => 'btn btn-outline-info btn-sm text-bold col-xs-4'));
                            }
                        }
                        elseif ($sf_user->hasCredential('admin_super') || $sf_user->hasCredential('admin'))
                        {
                            echo link_to('<i class="fas fa-print"></i> Print Berita Acara/Resume Rapat', 'admin/resumeRapat?kode_kegiatan=' . $kode_kegiatan . '&unit_id=' . $unit_id, array('class' => 'btn btn-outline-info btn-sm text-bold col-xs-3', 'target' => '_blank'));
                            echo link_to('<i class="fas fa-print"></i> Print Perbandingan Pergeseran', 'report/printBandingAsli?kode_kegiatan=' . $kode_kegiatan . '&unit_id=' . $unit_id, array('class' => 'btn btn-outline-info btn-sm text-bold col-xs-3', 'target' => '_blank'));
                            echo link_to('Perbandingan Semula-Menjadi', 'report/tampillaporanaslisemua?kode_kegiatan=' . $kode_kegiatan . '&unit_id=' . $unit_id, array('class' => 'btn btn-outline-info btn-sm text-bold col-xs-3'));
                        }
                        else
                        {
                            echo link_to('Perbandingan Semula-Menjadi', 'report/tampillaporanaslisemua?kode_kegiatan=' . $kode_kegiatan . '&unit_id=' . $unit_id, array('class' => 'btn btn-outline-info btn-sm text-bold col-xs-6'));
                        }
                        ?>
                        </div>
                    </div>
                    <div class="card-body table-responsive p-0">
                        <table class="table table-borderless table-sm">
                            <tr>
                                <td align='right' width='20%'>Unit Kerja</td>
                                <td width='1%' align='center'>:</td>
                                <td align='left'><?php echo '(' . $unit_id . ')' . '&nbsp;' . $unit_kerja ?></td>
                            </tr>
                            <tr>
                                <td align='right' width='20%'>Kode Sub Kegiatan</td>
                                <td width='1%' align='center'>:</td>
                                <td><?php echo $master_keg->getKegiatanId(); ?></td>
                            </tr>
                            <tr>
                                <td align='right' width='20%'>Nama Sub Kegiatan</td>
                                <td width='1%' align='center'>:</td>
                                <td><?php echo $nama_kegiatan ?></td>
                            </tr>
                            <?php
                            $c = new Criteria();
                            $c->add(MasterUserV2Peer::USER_ID, $user_id_pptk);
                            if ($user_pptk = MasterUserV2Peer::doSelectOne($c))
                                $pptk = $user_pptk->getUserName() . ' (' . $user_pptk->getNip() . '-' . $user_pptk->getJabatan() . ')';

                            $c = new Criteria();
                            $c->add(MasterUserV2Peer::USER_ID, $user_id_kpa);
                            if ($user_kpa = MasterUserV2Peer::doSelectOne($c))
                                $kpa = $user_kpa->getUserName() . ' (' . $user_kpa->getNip() . '-' . $user_kpa->getJabatan() . ')';



                            $pd_dinkes = array('1700','0300','1800');
                            if(in_array($unit_id, $pd_dinkes)){
                                $unit_id_pa = '1700';
                            }else{
                                $unit_id_pa = $unit_id;
                            }
                            $c = new Criteria();
                            $c->addJoin(MasterUserV2Peer::USER_ID, SchemaAksesV2Peer::USER_ID);
                            $c->addJoin(MasterUserV2Peer::USER_ID, UserHandleV2Peer::USER_ID);
                            $c->add(MasterUserV2Peer::USER_ENABLE, TRUE);
                            $c->add(SchemaAksesV2Peer::LEVEL_ID, 15);
                            $c->add(UserHandleV2Peer::UNIT_ID, $unit_id_pa);
                            $c->addAscendingOrderByColumn(MasterUserV2Peer::USER_ID);
                            if ($user_pa = MasterUserV2Peer::doSelectOne($c))
                                $pa = $user_pa->getUserName() . ' (' . $user_pa->getNip() . '-' . $user_pa->getJabatan() . ')';
                            ?>
                            <tr>
                                <td align='right' width='20%'>PPTK</td>
                                <td width='1%' align='center'>:</td>
                                <td><?php echo $pptk ?></td>
                            </tr>
                            <tr>
                                <td align='right' width='20%'>KPA</td>
                                <td width='1%' align='center'>:</td>
                                <td><?php echo $kpa ?></td>
                            </tr>
                            <tr>
                                <td align='right' width='20%'>PA</td>
                                <td width='1%' align='center'>:</td>
                                <td><?php echo $pa ?></td>
                            </tr>
                            <tr>
                                <td align='right' width='20%'>Program</td>
                                <td width='1%' align='center'>:</td>
                                <td><?php echo $kode_program22 . '&nbsp;' . $nama_program22 ?></td>
                            </tr>
                            <tr>
                                <td align='right' width='20%' valign='top'>Target</td>
                                <td width='1%' align='center' valign='top'>:</td>
                                <td valign='top'>
                                    <table class="table table-striped" width="100%">
                                        <tr align='center'>
                                            <td><strong>Sub Kegiatan</strong></td>
                                            <td><strong>Output</strong></td>
                                        </tr>
                                        <?php
                                        $c = new Criteria();
                                        $c->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
                                        $c->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
                                        $c->addAscendingOrderByColumn(DinasRincianDetailPeer::SUBTITLE);
                                        $cs = DinasRincianDetailPeer::doSelect($c);
                                        $subtitle = '';
                                        foreach ($cs as $css)
                                        {
                                            if ($css->getSubtitle() != 'Penunjang Kinerja') {
                                                if ($subtitle != $css->getSubtitle()) {
                                                    $subtitle = $css->getSubtitle();
                                                    $d = new Criteria();
                                                    $d->add(DinasSubtitleIndikatorPeer::KEGIATAN_CODE, $kode_kegiatan);
                                                    $d->add(DinasSubtitleIndikatorPeer::UNIT_ID, $unit_id);
                                                    $d->add(DinasSubtitleIndikatorPeer::SUBTITLE, $subtitle);
                                                    $ds = DinasSubtitleIndikatorPeer::doSelectOne($d);
                                                    if ($ds)
                                                    {
                                        ?>
                                        <tr>
                                            <td><?php echo $ds->getSubtitle(); ?></td>
                                            <td>
                                                <?php
                                                $di = new Criteria();
                                                $di->add(OutputSubtitlePeer::UNIT_ID, $unit_id);
                                                $di->add(OutputSubtitlePeer::KODE_KEGIATAN, $kode_kegiatan);
                                                $vi = OutputSubtitlePeer::doSelect($di);
                                                foreach ($vi as $value):
                                                    $kinerja = $value->getOutput();
                                                    $kinerja_output = str_replace("|", " ", $kinerja);
                                                    echo $kinerja_output."<br/>";
                                                endforeach;
                                                ?>
                                            </td>
                                        </tr>
                                        <?php
                                                }
                                            }
                                        }
                                    }
                                    $c = new Criteria();
                                    $c->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
                                    $c->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
                                    $c->addAscendingOrderByColumn(DinasRincianDetailPeer::SUBTITLE);
                                    $cs = DinasRincianDetailPeer::doSelect($c);
                                    $subtitle = '';
                                    foreach ($cs as $css) {
                                        if ($css->getSubtitle() == 'Penunjang Kinerja') {
                                            if ($subtitle != $css->getSubtitle()) {
                                                $subtitle = $css->getSubtitle();
                                                ?>
                                        <tr>
                                            <td>
                                                <?php echo $css->getSubtitle(); ?>
                                            </td>
                                            <td>
                                                <?php echo $css->getKeteranganKoefisien(); ?>
                                            </td>
                                        </tr>
                                        <?php
                                            }
                                        }
                                    }
                                    ?>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align='right' width='20%' valign='top'>Nilai Belanja</td>
                                <td width='1%' align='center' valign='top'>:</td>
                                <td valign='top'>
                                    <table class="table table-striped" width="100%">
                                        <tr align='center'>
                                            <td width="40%">&nbsp;</td>
                                            <?php if ($tahap != 'murni'): ?>
                                            <td width="20%"><strong>Semula</strong></td>
                                            <?php endif; ?>
                                            <td width="20%"><strong>Menjadi</strong></td>
                                            <td width="20%"><strong>Selisih</strong></td>
                                        </tr>
                                        <?php
                                        $query = "select coalesce(nilai_murni.id,nilai_sekarang.id) as id, nilai_murni.belanja_name as belanja_semula, 
                                        nilai_sekarang.belanja_name as belanja_menjadi, nilai_sekarang.hasil_kali2 as menjadi, 
                                        nilai_murni.hasil_kali1 as semula 
                                        from ( select k.id, k.belanja_name, sum(detail2.nilai_anggaran) as hasil_kali2 
                                                from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail detail2," . sfConfig::get('app_default_schema') . ".rekening r, " . sfConfig::get('app_default_schema') . ".kelompok_belanja k 
                                                where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and r.rekening_code=detail2.rekening_code and 
                                                k.belanja_id=r.belanja_id and detail2.status_hapus=false and 
                                                (detail_no in (select drd.detail_no
                                                        from " . sfConfig::get('app_default_schema') . ".$tabel_semula rd 
                                                        left join " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail drd on rd.unit_id=drd.unit_id and rd.kegiatan_code=drd.kegiatan_code and 
                                                        rd.detail_no=drd.detail_no where rd.unit_id='$unit_id' and rd.kegiatan_code='$kode_kegiatan' and 
                                                        rd.status_hapus=false and drd.status_hapus=false and drd.status_level >= 0 and 
                                                        (drd.rekening_code<>rd.rekening_code or drd.subtitle<>rd.subtitle or drd.nilai_anggaran<>rd.nilai_anggaran or drd.is_covid<>rd.is_covid or
                                                        drd.komponen_name<>rd.komponen_name or drd.keterangan_koefisien<>rd.keterangan_koefisien or 
                                                        drd.detail_name<>rd.detail_name)) or 
                                                (detail_no not in (select detail_no
                                                        from " . sfConfig::get('app_default_schema') . ".$tabel_semula drd 
                                                        where drd.unit_id='$unit_id' and drd.kegiatan_code='$kode_kegiatan' and drd.status_hapus=false)
                                                        and detail2.nilai_anggaran>0 and detail2.status_level >= 0) 
                                                ) 
                                                group by k.id, k.belanja_name ) as nilai_sekarang 
                                        left outer join ( select k.id, k.belanja_name, sum(detail1.nilai_anggaran) as hasil_kali1 
                                                from " . sfConfig::get('app_default_schema') . ".$tabel_semula detail1," . sfConfig::get('app_default_schema') . ".rekening r, " . sfConfig::get('app_default_schema') . ".kelompok_belanja k 
                                                where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and r.rekening_code=detail1.rekening_code and 
                                                k.belanja_id=r.belanja_id and detail1.status_hapus=false and 
                                                detail1.detail_no in (select drd.detail_no as nilai 
                                                        from " . sfConfig::get('app_default_schema') . ".$tabel_semula rd 
                                                        left join " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail drd on rd.unit_id=drd.unit_id and rd.kegiatan_code=drd.kegiatan_code and 
                                                        rd.detail_no=drd.detail_no where rd.unit_id='$unit_id' and rd.kegiatan_code='$kode_kegiatan' and 
                                                        rd.status_hapus=false and drd.status_level >= 0 and (drd.rekening_code<>rd.rekening_code or drd.subtitle<>rd.subtitle or drd.nilai_anggaran<>rd.nilai_anggaran or drd.is_covid<>rd.is_covid or
                                                        drd.komponen_name<>rd.komponen_name or drd.keterangan_koefisien<>rd.keterangan_koefisien or 
                                                        drd.detail_name<>rd.detail_name or rd.status_hapus<>drd.status_hapus)) 
                                                        group by k.id, k.belanja_name ) as nilai_murni 
                                        on nilai_sekarang.belanja_name= nilai_murni.belanja_name 
                                        order by id";

                                        $con = Propel::getConnection();
                                        $stmt = $con->prepareStatement($query);
                                        $rs_rekening = $stmt->executeQuery();
                                        $rekening_prev = 0;
                                        $rekening_now = 0;
                                        $selisih = 0;
                                        $belanja = array();
                                        while ($rs_rekening->next())
                                        {
                                        if (!$rs_rekening->getString('belanja_semula')) {
                                            $nama_belanja = $rs_rekening->getString('belanja_menjadi');
                                            array_push($belanja, $rs_rekening->getInt('id'));
                                        } else {
                                            $nama_belanja = $rs_rekening->getString('belanja_semula');
                                            array_push($belanja, $rs_rekening->getInt('id'));
                                        }
                                        ?>
                                        <tr>
                                            <td><?php echo $nama_belanja; ?></td>
                                            <?php if ($tahap != 'murni'): ?>
                                            <td align='right'>
                                                <?php
                                                    echo number_format($rs_rekening->getString('semula'), 0, ',', '.');
                                                    $rekening_prev+=$rs_rekening->getString('semula');
                                                    ?>
                                            </td>
                                            <?php endif; ?>
                                            <td align='right'>
                                                <?php
                                                echo number_format($rs_rekening->getString('menjadi'), 0, ',', '.');
                                                $rekening_now+=$rs_rekening->getString('menjadi');
                                                ?>
                                            </td>
                                            <td align='right'>
                                                <?php
                                                echo number_format($rs_rekening->getInt('menjadi') - $rs_rekening->getInt('semula'), 0, ',', '.');
                                                $selisih+=$rs_rekening->getInt('menjadi') - $rs_rekening->getInt('semula');
                                                ?>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                        <tr>
                                            <td>Total</td>
                                            <?php if ($tahap != 'murni'): ?>
                                            <td align='right'><?php echo number_format($rekening_prev, 0, ',', '.'); ?>
                                            </td>
                                            <?php endif; ?>
                                            <td align='right'><?php echo number_format($rekening_now, 0, ',', '.'); ?>
                                            </td>
                                            <?php if($selisih >= 0) { ?>
                                            <td align='right'><?php echo number_format($selisih, 0, ',', '.'); ?></td>
                                            <?php } else { ?>
                                            <td align='right'>(<?php echo number_format(abs($selisih), 0, ',', '.'); ?>)
                                            </td>
                                            <?php } ?>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align='right' width='20%' valign='top'>Nilai Rekening</td>
                                <td width='1%' align='center' valign='top'>:</td>
                                <td valign='top'>
                                    <table class="table table-striped" width="100%">
                                        <tr align='center'>
                                            <td width="40%">&nbsp;</td>
                                            <?php if ($tahap != 'murni'): ?>
                                            <td width="20%"><strong>Semula</strong></td>
                                            <?php endif; ?>
                                            <td width="20%"><strong>Menjadi</strong></td>
                                            <td width="20%"><strong>Selisih</strong></td>
                                        </tr>
                                        <?php
                                        $query = " select 
                                            coalesce(nilai_murni.rekening_code,nilai_sekarang.rekening_code) as id, 
                                            nilai_murni.rekening_name as belanja_semula, 
                                            nilai_sekarang.rekening_name as belanja_menjadi, 
                                            sum(nilai_sekarang.hasil_kali2) as menjadi, 
                                            sum(nilai_murni.hasil_kali1) as semula 
                                        from ( select r.rekening_code, r.rekening_name, sum(detail2.nilai_anggaran) as hasil_kali2 
                                                from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail detail2," . sfConfig::get('app_default_schema') . ".rekening r 
                                                where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and r.rekening_code=detail2.rekening_code and 
                                                detail2.status_hapus=false and detail2.status_level >= 0 and 
                                                (detail_no in (select drd.detail_no as nilai 
                                                       from " . sfConfig::get('app_default_schema') . ".$tabel_semula rd
                                                       left join " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail drd 
                                                       on rd.unit_id=drd.unit_id and rd.kegiatan_code=drd.kegiatan_code and rd.detail_no=drd.detail_no
                                                       where rd.unit_id='$unit_id' and rd.kegiatan_code='$kode_kegiatan' and 
                                                       rd.status_hapus=false and drd.status_hapus=false and 
                                                       (drd.rekening_code<>rd.rekening_code or drd.subtitle<>rd.subtitle or drd.nilai_anggaran<>rd.nilai_anggaran or
                                                           drd.is_covid<>rd.is_covid or
                                                           drd.komponen_name<>rd.komponen_name or 
                                                       drd.keterangan_koefisien<>rd.keterangan_koefisien or drd.detail_name<>rd.detail_name))
                                                or detail_no not in (select detail_no from " . sfConfig::get('app_default_schema') . ".$tabel_semula where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan')
                                                ) 
                                                group by r.rekening_code, r.rekening_name ) as nilai_sekarang 
                                        full outer join (select r.rekening_code, r.rekening_name, sum(detail1.nilai_anggaran) as hasil_kali1 
                                                from " . sfConfig::get('app_default_schema') . ".$tabel_semula detail1," . sfConfig::get('app_default_schema') . ".rekening r 
                                                where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and r.rekening_code=detail1.rekening_code and 
                                                detail1.status_hapus=false and detail1.detail_no in (select drd.detail_no as nilai 
                                                       from " . sfConfig::get('app_default_schema') . ".$tabel_semula rd
                                                       left join " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail drd 
                                                       on rd.unit_id=drd.unit_id and rd.kegiatan_code=drd.kegiatan_code and rd.detail_no=drd.detail_no
                                                       where rd.unit_id='$unit_id' and rd.kegiatan_code='$kode_kegiatan' and 
                                                       rd.status_hapus=false and drd.status_level >= 0 and 
                                                       (drd.rekening_code<>rd.rekening_code or drd.subtitle<>rd.subtitle or drd.nilai_anggaran<>rd.nilai_anggaran or 
                                                           drd.is_covid<>rd.is_covid or drd.komponen_name<>rd.komponen_name or 
                                                       drd.keterangan_koefisien<>rd.keterangan_koefisien or drd.detail_name<>rd.detail_name or drd.status_hapus<>rd.status_hapus))
                                                group by r.rekening_code, r.rekening_name ) as nilai_murni 
                                        on nilai_sekarang.rekening_code=nilai_murni.rekening_code 
                                        group by nilai_murni.rekening_code, nilai_sekarang.rekening_code, nilai_murni.rekening_name, nilai_sekarang.rekening_name 
                                        having coalesce(sum(nilai_sekarang.hasil_kali2),0)>0 or coalesce(sum(nilai_murni.hasil_kali1),0)>0
                                        order by id";
                                        $con = Propel::getConnection();
                                        $stmt = $con->prepareStatement($query);
                                        $rs_rekening = $stmt->executeQuery();

                                        $rekening_prev = 0;
                                        $rekening_now = 0;
                                        $selisih = 0;
                                        $arr_catatan_rekening_1 = array();
                                        $arr_catatan_rekening_2 = array();
                                        $arr_catatan_rekening_3 = array();
                                        while ($rs_rekening->next())
                                        {
                                            if (!$rs_rekening->getString('belanja_semula')) {
                                                $nama_belanja = $rs_rekening->getString('belanja_menjadi');
                                            } else {
                                                $nama_belanja = $rs_rekening->getString('belanja_semula');
                                            }
                                            ?>
                                            <tr>
                                                <td><?php echo $nama_belanja; ?></td>
                                                <?php if ($tahap != 'murni'): ?>
                                                <td align='right'>
                                                    <?php
                                                        echo number_format($rs_rekening->getString('semula'), 0, ',', '.');
                                                        $rekening_prev+=$rs_rekening->getString('semula');
                                                        ?>
                                                </td>
                                                <?php endif; ?>
                                                <td align='right'>
                                                    <?php
                                                    echo number_format($rs_rekening->getString('menjadi'), 0, ',', '.');
                                                    $rekening_now+=$rs_rekening->getString('menjadi');
                                                    ?>
                                                </td>
                                                <td align='right'>
                                                    <?php
                                                    echo number_format($rs_rekening->getInt('menjadi') - $rs_rekening->getInt('semula'), 0, ',', '.');
                                                    $selisih+=$rs_rekening->getInt('menjadi') - $rs_rekening->getInt('semula');
                                                    if ($rs_rekening->getInt('menjadi') < $rs_rekening->getInt('semula')) {
                                                        array_push($arr_catatan_rekening_1, $nama_belanja);
                                                    } elseif ($rs_rekening->getInt('menjadi') > $rs_rekening->getInt('semula')) {
                                                        array_push($arr_catatan_rekening_2, $nama_belanja);
                                                    } elseif ($rs_rekening->getInt('menjadi') == $rs_rekening->getInt('semula')){
                                                        array_push($arr_catatan_rekening_3, $nama_belanja);
                                                    }
                                                    ?>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                        $catatan_rekening_1 = implode(', ', $arr_catatan_rekening_1);
                                        $catatan_rekening_2 = implode(', ', $arr_catatan_rekening_2);
                                        $catatan_rekening_3 = implode(', ', $arr_catatan_rekening_3);
                                        ?>
                                        <tr>
                                            <td>Total</td>
                                            <?php if ($tahap != 'murni'): ?>
                                            <td align='right'><?php echo number_format($rekening_prev, 0, ',', '.'); ?>
                                            </td>
                                            <?php endif; ?>
                                            <td align='right'><?php echo number_format($rekening_now, 0, ',', '.'); ?>
                                            </td>
                                            <?php if($selisih >= 0) { ?>
                                            <td align='right'><?php echo number_format($selisih, 0, ',', '.'); ?></td>
                                            <?php } else { ?>
                                            <td align='right'>(<?php echo number_format(abs($selisih), 0, ',', '.'); ?>)
                                            </td>
                                            <?php } ?>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="card-body table-responsive p-0">
                        <table class="table table-hover table-sm">
                            <thead class="head_peach">
                                <?php if ($tahap != 'murni'): ?>
                                <th colspan='7'><strong>SEMULA</strong></th>
                                <th><strong>|</strong></th>
                                <?php endif; ?>
                                <?php if(sfConfig::get('app_tahap_edit') != 'murni'): ?>
                                <th colspan='7'><strong>MENJADI</strong></th>
                                <?php else: ?>
                                <th colspan='6'><strong>MENJADI</strong></th>
                                <?php endif; ?>
                                <th colspan="4">&nbsp;</th>
                                <th>
                                    <?php 
                                        $boleh = false;
                                        $c_user_handle = new Criteria();
                                        $c_user_handle->add(UserHandleV2Peer::USER_ID, $sf_user->getNamaLogin(), Criteria::EQUAL);
                                        $user_handles = UserHandleV2Peer::doSelect($c_user_handle);
                                        // $user_kecuali = array('adam.yulian','adhitiya');

                                        foreach($user_handles as $user_handle){
                                            if($user_handle->getStatusUser() != 'shs'){
                                                $boleh = true;
                                            }
                                        }
                                        // $user_kecuali = array('tim_shs','adam.yulian','adhitiya');
                                        if($boleh==true || $sf_user->getNamaLogin()=='superadmin'){
                                            echo checkbox_tag('cekAll', 1, null, "id=cekSemua"); 
                                        }
                                    ?>
                                </th>
                            </thead>
                            <thead class="head_peach">
                                <?php if ($tahap != 'murni'): ?>
                                <th>Komponen</th>
                                <th>Satuan</th>
                                <th>Koefisien</th>
                                <th>Harga</th>
                                <th>Hasil</th>
                                <th>PPN</th>
                                <th>Total</th>
                                <th align='center'>|</th>
                                <?php endif; ?>
                                <th>Komponen</th>
                                <th>Satuan</th>
                                <th>Koefisien</th>
                                <th>Harga</th>
                                <th>Hasil</th>
                                <th>PPN</th>
                                <th>Total</th>
                                <th>Selisih</th>
                                <?php if(sfConfig::get('app_tahap_edit') != 'murni'): ?>
                                <th>Catatan</th>
                                <?php endif; ?>
                                <th>Posisi</th>
                                <th>Status Komponen</th>
                                <?php if ($sf_user->hasCredential('kpa') || $sf_user->hasCredential('peneliti') || $sf_user->getNamaUser() == 'anggaran' || $sf_user->hasCredential('bappeko') || $sf_user->getNamaUser() == 'bagian_hukum' || $sf_user->getNamaUser() == 'inspektorat' || $sf_user->getNamaUser() == 'bkd' || $sf_user->getNamaUser() == 'bkpsdm' || $sf_user->getNamaUser() == 'lppa' || $sf_user->getNamaUser() == 'bapenda' || $sf_user->getNamaUser() == 'bagian_organisasi' || $sf_user->hasCredential('pa') || $sf_user->hasCredential('pptk') || $sf_user->hasCredential('dinas') || $sf_user->hasCredential('admin_super') || $sf_user->hasCredential('admin')): ?>
                                <th>Action</th>
                                <?php endif; ?>
                            </thead>
                            <tbody>
                            <?php
                            $total_belanja_kiri = 0;
                            $total_belanja_kanan = 0;
                            foreach ($belanja as $id_belanja) 
                            {
                                $query = "select belanja_name, belanja_code from " . sfConfig::get('app_default_schema') . ".kelompok_belanja where id = $id_belanja";
                                $con = Propel::getConnection();
                                $stmt = $con->prepareStatement($query);
                                $rs = $stmt->executeQuery();
                                $rs->next();
                                $kode_belanja = $rs->getString('belanja_code');
                                $nama_belanja = $rs->getString('belanja_name');
                                ?>
                                <tr align="left">
                                    <?php if ($tahap != 'murni'): ?>
                                    <td colspan="7">
                                        <strong><?php echo '' . $kode_belanja . ' ' . $nama_belanja ?></strong>
                                    </td>
                                    <td>&nbsp;</td>
                                    <?php endif; ?>
                                    <td colspan="12">
                                        <strong><?php echo '' . $kode_belanja . ' ' . $nama_belanja ?></strong>
                                    </td>
                                </tr>
                                <?php
                                $jumlah_harga_prev = 0;
                                $jumlah_harga = 0;
                                $query = "(
                                select distinct detail.subtitle 
                                from " . sfConfig::get('app_default_schema') . ".dinas_subtitle_indikator detail, " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail rd, " . sfConfig::get('app_default_schema') . ".$tabel_semula rd1 
                                where detail.unit_id=rd.unit_id and detail.kegiatan_code=rd.kegiatan_code and detail.subtitle=rd.subtitle and 
                                detail.unit_id='$unit_id' and detail.kegiatan_code='$kode_kegiatan' and rd.rekening_code like '$kode_belanja%' and 
                                rd.kegiatan_code=rd1.kegiatan_code and rd.unit_id=rd1.unit_id and rd.detail_no=rd1.detail_no and 
                                rd.status_hapus=false and rd.status_level >= 0 and (rd.rekening_code<>rd1.rekening_code or rd.subtitle<>rd1.subtitle or 
                                rd.nilai_anggaran<>rd1.nilai_anggaran or 
                                rd.is_covid<>rd1.is_covid or rd.komponen_name<>rd1.komponen_name or 
                                rd.keterangan_koefisien<>rd1.keterangan_koefisien or rd.detail_name<>rd1.detail_name) 
                                ) 
                                union 
                                (
                                select distinct detail.subtitle 
                                from " . sfConfig::get('app_default_schema') . ".dinas_subtitle_indikator detail, " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail rd 
                                where detail.unit_id=rd.unit_id and detail.kegiatan_code=rd.kegiatan_code and detail.subtitle=rd.subtitle and 
                                detail.unit_id='$unit_id' and rd.status_level >= 0 and rd.status_hapus=false and detail.kegiatan_code='$kode_kegiatan' and rd.rekening_code like '$kode_belanja%' and 
                                rd.detail_no not in (select distinct detail_no 
                                from " . sfConfig::get('app_default_schema') . ".$tabel_semula 
                                where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan') 
                                ) 
                                union 
                                (
                                select distinct detail.subtitle 
                                from " . sfConfig::get('app_default_schema') . ".subtitle_indikator detail, " . sfConfig::get('app_default_schema') . ".$tabel_semula rd 
                                where detail.unit_id=rd.unit_id and detail.kegiatan_code=rd.kegiatan_code and detail.subtitle=rd.subtitle and 
                                detail.unit_id='$unit_id' and detail.kegiatan_code='$kode_kegiatan' and rd.status_hapus=false and rd.rekening_code like '$kode_belanja%' and 
                                rd.detail_no not in (select distinct detail_no 
                                from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail 
                                where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan') 
                                )order by subtitle";
                                $con = Propel::getConnection();
                                $stmt = $con->prepareStatement($query);
                                $rs = $stmt->executeQuery();
                                while ($rs->next())
                                {
                                    $subtitle_prev = '';
                                    $subtitle = '';
                                    $subtitle_prev = $rs->getString('subtitle');
                                    $subtitle = $rs->getString('subtitle');
                                    $j = 0;
                                    $query2 = "(
                                    select distinct rd.rekening_code as rekening, r.rekening_name
                                    from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail rd, " . sfConfig::get('app_default_schema') . ".$tabel_semula rd1, " . sfConfig::get('app_default_schema') . ".rekening r 
                                    where rd.unit_id='$unit_id' and rd.kegiatan_code='$kode_kegiatan' and rd.rekening_code like '$kode_belanja%' and 
                                    rd.kegiatan_code=rd1.kegiatan_code and rd.status_level>=0 and rd.unit_id=rd1.unit_id and 
                                    rd.subtitle ilike '$subtitle' and
                                    rd1.rekening_code=r.rekening_code and rd.rekening_code=r.rekening_code and
                                    rd.detail_no=rd1.detail_no and rd.status_hapus=false and 
                                    (rd.rekening_code<>rd1.rekening_code or rd.subtitle<>rd1.subtitle or rd.nilai_anggaran<>rd1.nilai_anggaran or rd.is_covid<>rd1.is_covid or 
                                    rd.komponen_name<>rd1.komponen_name or rd.keterangan_koefisien<>rd1.keterangan_koefisien or 
                                    rd.detail_name<>rd1.detail_name) 
                                    ) 
                                    union 
                                    (
                                    select distinct rd1.rekening_code as rekening, r.rekening_name
                                    from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail rd, " . sfConfig::get('app_default_schema') . ".$tabel_semula rd1, " . sfConfig::get('app_default_schema') . ".rekening r 
                                    where rd.unit_id='$unit_id' and rd.kegiatan_code='$kode_kegiatan' and rd1.rekening_code like '$kode_belanja%' and 
                                    rd.kegiatan_code=rd1.kegiatan_code and rd.detail_no=rd1.detail_no and rd.unit_id=rd1.unit_id and 
                                    rd.subtitle ilike '$subtitle' and
                                    rd1.rekening_code=r.rekening_code and rd.status_level>=0  and rd.status_hapus=false and 
                                    (rd.rekening_code<>rd1.rekening_code or rd.subtitle<>rd1.subtitle or rd.nilai_anggaran<>rd1.nilai_anggaran or rd.is_covid<>rd1.is_covid or 
                                    rd.komponen_name<>rd1.komponen_name or rd.keterangan_koefisien<>rd1.keterangan_koefisien or 
                                    rd.detail_name<>rd1.detail_name)
                                    )
                                    union 
                                    (
                                    select distinct rd.rekening_code as rekening, r.rekening_name
                                    from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail rd, " . sfConfig::get('app_default_schema') . ".$tabel_semula rd1, " . sfConfig::get('app_default_schema') . ".rekening r 
                                    where rd.unit_id='$unit_id' and rd.kegiatan_code='$kode_kegiatan' and rd.rekening_code like '$kode_belanja%' and 
                                    rd.kegiatan_code=rd1.kegiatan_code and rd.detail_no=rd1.detail_no and rd.unit_id=rd1.unit_id and 
                                    rd.subtitle ilike '$subtitle' and
                                    rd.rekening_code=r.rekening_code and rd.status_level>=0  and rd.status_hapus=false and 
                                    (rd.rekening_code<>rd1.rekening_code or rd.subtitle<>rd1.subtitle or rd.nilai_anggaran<>rd1.nilai_anggaran or rd.is_covid<>rd1.is_covid or 
                                    rd.komponen_name<>rd1.komponen_name or rd.keterangan_koefisien<>rd1.keterangan_koefisien or 
                                    rd.detail_name<>rd1.detail_name)
                                    )
                                    union 
                                    ( 
                                    select distinct rd.rekening_code as rekening, r.rekening_name 
                                    from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail rd, " . sfConfig::get('app_default_schema') . ".rekening r 
                                    where rd.unit_id='$unit_id' and rd.kegiatan_code='$kode_kegiatan' and rd.rekening_code like '$kode_belanja%' and 
                                    rd.rekening_code=r.rekening_code and rd.subtitle ilike '$subtitle' and rd.status_hapus=false and rd.nilai_anggaran>0 and 
                                    rd.detail_no not in (select distinct detail_no 
                                    from " . sfConfig::get('app_default_schema') . ".$tabel_semula 
                                    where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rekening_code like '$kode_belanja%' and 
                                    subtitle ilike '$subtitle') 
                                    )
                                    union 
                                    ( 
                                    select distinct rd.rekening_code as rekening, r.rekening_name 
                                    from " . sfConfig::get('app_default_schema') . ".$tabel_semula rd, " . sfConfig::get('app_default_schema') . ".rekening r 
                                    where rd.unit_id='$unit_id' and rd.kegiatan_code='$kode_kegiatan' and rd.rekening_code like '$kode_belanja%' and 
                                    rd.rekening_code=r.rekening_code and rd.subtitle ilike '$subtitle' and rd.status_hapus=false and rd.nilai_anggaran>0 and 
                                    rd.detail_no not in (select distinct detail_no 
                                    from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail 
                                    where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rekening_code like '$kode_belanja%' and 
                                    subtitle ilike '$subtitle') 
                                    ) order by rekening
                                    ";
                                    $stmt2 = $con->prepareStatement($query2);
                                    $rs2 = $stmt2->executeQuery();
                                    if ($tahap != 'murni') {
                                        echo '<tr align="left" bgcolor="#ffffff"><td colspan="7"><strong> :: ' . $subtitle_prev . '</strong></td>';
                                        echo '<td><strong>&nbsp;</strong></td>';
                                    }
                                    echo '<td colspan="12"><strong> :: ' . $subtitle . '</strong></td></tr>';
                                    $total_semua = 0;
                                    $total_semua_n = 0;
                                    while ($rs2->next())
                                    {
                                        $rekening_prev = $rs2->getString('rekening') . '&nbsp;' . $rs2->getString('rekening_name');
                                        $rekening = $rs2->getString('rekening') . '&nbsp;' . $rs2->getString('rekening_name');
                                        if ($rs2->getString('rekening')) {
                                            $kode_rekening = $rs2->getString('rekening');
                                        }
                                        $where_anggaran2 = " ";
                                        $c_kegiatan_anggaran = new Criteria();
                                        $c_kegiatan_anggaran->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                                        $c_kegiatan_anggaran->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                                        $c_kegiatan_anggaran->add(DinasMasterKegiatanPeer::IS_PERNAH_RKA, TRUE);
                                        if (DinasMasterKegiatanPeer::doSelectOne($c_kegiatan_anggaran)) {
                                            $where_anggaran2 = "or detail2.nilai_anggaran<>(select nilai_anggaran from " . sfConfig::get('app_default_schema') . ".rincian_detail where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and detail_no=detail2.detail_no) ";
                                        }
                                        //query panjang
                                        $query3 = "(
                                            SELECT

                                                    rekening.rekening_code,rekening.rekening_code as rekening_code2,
                                                    rekening.rekening_name as nama_rekening2,
                                                    detail2.komponen_name || ' ' || detail2.detail_name as detail_name22,
                                                    detail2.komponen_harga_awal as detail_harga2,
                                                    detail2.pajak as pajak2,
                                                    detail2.komponen_id as komponen_id2,
                                                    detail2.detail_name as detail_name2,
                                                    detail2.komponen_name as komponen_name2,
                                                    detail2.tahap as tahap2,
                                                    detail2.volume as volume2,
                                                    detail2.volume_orang as volumo2,
                                                    detail2.subtitle as subtitle_name2,
                                                    detail2.satuan as detail_satuan2,
                                                    replace(detail2.keterangan_koefisien,'<*>','X') as keterangan_koefisien2,
                                                    replace(detail2.note_koefisien,'<*>','X') as note_koefisien2,
                                                    detail2.detail_no as detail_no2,
                                                    detail2.volume * detail2.komponen_harga_awal as hasil2,
                                                    (detail2.nilai_anggaran) as hasil_kali2,

                                                    detail.komponen_name || ' ' || detail.detail_name as detail_name2,
                                                    detail.komponen_harga_awal as detail_harga,
                                                    detail.pajak as pajak,
                                                    detail.komponen_id as komponen_id,
                                                    detail.detail_name as detail_name,
                                                    detail.komponen_name as komponen_name,
                                                    detail.tahap as tahap,
                                                    detail.volume as volume,
                                                    detail.volume_orang as volumo,
                                                    detail.subtitle as subtitle_name,
                                                    detail.satuan as detail_satuan,
                                                    replace(detail.keterangan_koefisien,'<*>','X') as keterangan_koefisien,
                                                    replace(detail.note_koefisien,'<*>','X') as note_koefisien,
                                                    detail.detail_no as detail_no,
                                                    detail.volume * detail.komponen_harga_awal as hasil,
                                                    (detail.nilai_anggaran) as hasil_kali,
                                                    detail2.detail_name as detail_name_rd,
                                                    detail2.sub as sub2,
                                                    detail.sub,
                                                    detail2.note_skpd as note_skpd,
                                                    detail2.note_peneliti as note_peneliti,
                                                    detail2.note_tapd as note_tapd,
                                                    detail2.note_bappeko as note_bappeko,
                                                    detail2.status_level as status_level,
                                                    detail2.status_level_tolak,
                                                    detail2.status_sisipan,
                                                    detail2.status_komponen_berubah,
                                                    detail2.status_komponen_baru,
                                                    detail2.status_lelang,
                                                    detail2.is_hpsp,
                                                    detail2.is_tapd_setuju,
                                                    detail2.is_bappeko_setuju,
                                                    detail2.is_penyelia_setuju,
                                                    detail2.is_bagian_hukum_setuju,
                                                    detail2.is_inspektorat_setuju,
                                                    detail2.is_badan_kepegawaian_setuju,
                                                    detail2.is_lppa_setuju,
                                                    detail2.is_musrenbang,
                                                    detail2.is_output,
                                                    detail2.prioritas_wali,
                                                    detail2.is_potong_bpjs,
                                                    detail2.is_bagian_organisasi_setuju,
                                                    detail.sumber_dana_id,
                                                    detail.is_covid,
                                                    detail2.sumber_dana_id as sumber_dana_id2,
                                                    detail2.is_covid as is_covid2
                                            FROM
                                                    " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail detail2, " . sfConfig::get('app_default_schema') . ".rekening rekening, " . sfConfig::get('app_default_schema') . ".$tabel_semula detail

                                            WHERE
                                                    detail2.kegiatan_code = '$kode_kegiatan' and
                                                    detail2.unit_id='$unit_id'

                                                    and detail2.rekening_code='$kode_rekening'
                                                    and detail2.subtitle ilike '$subtitle'
                                                    and detail2.kegiatan_code=detail.kegiatan_code
                                                    and detail2.unit_id=detail.unit_id
                                                    and detail2.subtitle=detail.subtitle
                                                    and detail2.rekening_code=detail.rekening_code
                                                    and rekening.rekening_code=detail2.rekening_code
                                                    and detail2.detail_no=detail.detail_no
                                                    and detail.status_hapus<>true
                                                    and detail2.status_hapus<>true
                                                    and detail2.status_level >= 0
                                                    and (detail2.nilai_anggaran <> detail.nilai_anggaran
                                                    or detail2.rekening_code <> detail.rekening_code 
                                                    or detail2.subtitle<>detail.subtitle
                                                    or detail2.is_covid<>detail.is_covid
                                                    or detail2.komponen_name <> detail.komponen_name
                                                    or detail2.keterangan_koefisien <> detail.keterangan_koefisien
                                                    or detail2.detail_name <> detail.detail_name
                                                    $where_anggaran2 )


                                            ORDER BY

                                                    rekening.rekening_code,
                                                    detail2.subtitle ,
                                                    detail2.komponen_name
                                            )
                                            union
                                            (
                                            SELECT

                                                    rekening.rekening_code,rekening.rekening_code as rekening_code2,
                                                    rekening.rekening_name as nama_rekening2,
                                                    detail2.komponen_name || ' ' || detail2.detail_name as detail_name22,
                                                    detail2.komponen_harga_awal as detail_harga2,
                                                    detail2.pajak as pajak2,
                                                    detail2.komponen_id as komponen_id2,
                                                    detail2.detail_name as detail_name2,
                                                    detail2.komponen_name as komponen_name2,
                                                    detail2.tahap as tahap2,
                                                    detail2.volume as volume2,
                                                    detail2.volume_orang as volumo2,
                                                    detail2.subtitle as subtitle_name2,
                                                    detail2.satuan as detail_satuan2,
                                                    replace(detail2.keterangan_koefisien,'<*>','X') as keterangan_koefisien2,
                                                    replace(detail2.note_koefisien,'<*>','X') as note_koefisien2,
                                                    detail2.detail_no as detail_no2,
                                                    detail2.volume * detail2.komponen_harga_awal as hasil2,
                                                    (detail2.nilai_anggaran) as hasil_kali2,

                                                    '' as detail_name,
                                                    0 as detail_harga,
                                                    0 as pajak,
                                                    '' as komponen_id,
                                                    '' as detail_name,
                                                    '' as komponen_name,
                                                    '' as tahap,
                                                    0 as volume,
                                                    0 as volumo,
                                                    '' as subtitle_name,
                                                    '' as detail_satuan,
                                                    '' as keterangan_koefisien,
                                                    '' as note_koefisien,
                                                    0 as detail_no,
                                                    0 as hasil,
                                                    0 as hasil_kali,
                                                    detail2.detail_name as detail_name_rd,
                                                    detail2.sub as sub2,
                                                    '' as sub,
                                                    detail2.note_skpd as note_skpd,
                                                    detail2.note_peneliti as note_peneliti,
                                                    detail2.note_tapd as note_tapd,
                                                    detail2.note_bappeko as note_bappeko,
                                                    detail2.status_level as status_level,
                                                    detail2.status_level_tolak,
                                                    detail2.status_sisipan,
                                                    detail2.status_komponen_berubah,
                                                    detail2.status_komponen_baru,
                                                    detail2.status_lelang,
                                                    detail2.is_hpsp,
                                                    detail2.is_tapd_setuju,
                                                    detail2.is_bappeko_setuju,
                                                    detail2.is_penyelia_setuju,
                                                    detail2.is_bagian_hukum_setuju,
                                                    detail2.is_inspektorat_setuju,
                                                    detail2.is_badan_kepegawaian_setuju,
                                                    detail2.is_lppa_setuju,
                                                    detail2.is_musrenbang,
                                                    detail2.is_output,
                                                    detail2.prioritas_wali,
                                                    detail2.is_potong_bpjs,
                                                    detail2.is_bagian_organisasi_setuju,
                                                    11 as sumber_dana_id,
                                                    false as is_covid,
                                                    detail2.sumber_dana_id as sumber_dana_id2,
                                                    detail2.is_covid as is_covid2
                                            FROM
                                                    " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail detail2, " . sfConfig::get('app_default_schema') . ".rekening rekening, " . sfConfig::get('app_default_schema') . ".$tabel_semula detail

                                            WHERE
                                                    detail2.kegiatan_code = '$kode_kegiatan' and
                                                    detail2.unit_id='$unit_id'

                                                    and detail2.rekening_code='$kode_rekening'
                                                    and detail2.subtitle ilike '$subtitle'
                                                    and detail2.kegiatan_code=detail.kegiatan_code
                                                    and detail2.unit_id=detail.unit_id
                                                    and detail2.subtitle=detail.subtitle
                                                    and detail2.rekening_code=detail.rekening_code
                                                    and rekening.rekening_code=detail2.rekening_code
                                                    and detail2.detail_no=detail.detail_no
                                                    and detail.status_hapus=true
                                                    and detail2.status_hapus<>true
                                                    and detail2.status_level >= 0
                                                    and detail2.tahap='$tahap'
                                                    and (detail2.nilai_anggaran <> detail.nilai_anggaran
                                                    or detail2.rekening_code <> detail.rekening_code 
                                                    or detail2.subtitle<>detail.subtitle
                                                    or detail2.is_covid<>detail.is_covid
                                                    or detail2.komponen_name <> detail.komponen_name
                                                    or detail2.keterangan_koefisien <> detail.keterangan_koefisien
                                                    or detail2.detail_name <> detail.detail_name
                                                    $where_anggaran2 )


                                            ORDER BY

                                                    rekening.rekening_code,
                                                    detail2.subtitle ,
                                                    detail2.komponen_name
                                            )
                                            union
                                            (
                                            SELECT

                                                    rekening.rekening_code,rekening.rekening_code as rekening_code2,
                                                    rekening.rekening_name as nama_rekening2,
                                                    '' as detail_name22,
                                                    0 as detail_harga2,
                                                    0 as pajak2,
                                                    '' as komponen_id2,
                                                    '' as detail_name2,
                                                    '' as komponen_name2,
                                                    '' as tahap,
                                                    0 as volume2,
                                                    0 as volumo2,
                                                    '' as subtitle_name2,
                                                    '' as detail_satuan2,
                                                    '' as keterangan_koefisien2,
                                                    '' as note_koefisien2,
                                                    detail.detail_no as detail_no2,
                                                    0 as hasil2,
                                                    0 as hasil_kali2,

                                                    detail.komponen_name || ' ' || detail.detail_name as detail_name2,
                                                    detail.komponen_harga_awal as detail_harga,
                                                    detail.pajak as pajak,
                                                    detail.komponen_id as komponen_id,
                                                    detail.detail_name as detail_name,
                                                    detail.komponen_name as komponen_name,
                                                    detail.tahap as tahap,
                                                    detail.volume as volume,
                                                    detail.volume_orang as volumo,
                                                    detail.subtitle as subtitle_name,
                                                    detail.satuan as detail_satuan,
                                                    replace(detail.keterangan_koefisien,'<*>','X') as keterangan_koefisien,
                                                    replace(detail.note_koefisien,'<*>','X') as note_koefisien,
                                                    detail.detail_no as detail_no,
                                                    detail.volume * detail.komponen_harga_awal as hasil,
                                                    (detail.nilai_anggaran) as hasil_kali,
                                                    detail.detail_name as detail_name_rd,
                                                    '' as sub2,
                                                    detail.sub,
                                                    detail2.note_skpd as note_skpd,
                                                    detail2.note_peneliti as note_peneliti,
                                                    detail2.note_tapd as note_tapd,
                                                    detail2.note_bappeko as note_bappeko,
                                                    detail2.status_level as status_level,
                                                    detail2.status_level_tolak,
                                                    detail2.status_sisipan,
                                                    detail2.status_komponen_berubah,
                                                    detail2.status_komponen_baru,
                                                    detail2.status_lelang,
                                                    detail2.is_hpsp,
                                                    detail2.is_tapd_setuju,
                                                    detail2.is_bappeko_setuju,
                                                    detail2.is_penyelia_setuju,
                                                    detail2.is_bagian_hukum_setuju,
                                                    detail2.is_inspektorat_setuju,
                                                    detail2.is_badan_kepegawaian_setuju,
                                                    detail2.is_lppa_setuju,
                                                    detail2.is_musrenbang,
                                                    detail2.is_output,
                                                    detail2.prioritas_wali,
                                                    detail2.is_potong_bpjs,
                                                    detail2.is_bagian_organisasi_setuju,
                                                    11 as sumber_dana_id,
                                                    false as is_covid,
                                                    detail2.sumber_dana_id as sumber_dana_id2,
                                                    detail2.is_covid as is_covid2
                                            FROM
                                                    " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail detail2, " . sfConfig::get('app_default_schema') . ".rekening rekening, " . sfConfig::get('app_default_schema') . ".$tabel_semula detail

                                            WHERE
                                                    detail2.kegiatan_code = '$kode_kegiatan' and
                                                    detail2.unit_id='$unit_id'

                                                    and detail2.rekening_code='$kode_rekening'
                                                    and detail2.subtitle ilike '$subtitle'
                                                    and detail2.kegiatan_code=detail.kegiatan_code
                                                    and detail2.unit_id=detail.unit_id
                                                    and detail2.subtitle=detail.subtitle
                                                    and detail2.rekening_code=detail.rekening_code
                                                    and rekening.rekening_code=detail2.rekening_code
                                                    and detail2.detail_no=detail.detail_no
                                                    and detail.status_hapus<>true
                                                    and detail2.status_hapus=true
                                                    and detail2.status_level >= 0
                                                    and detail2.tahap=''
                                                    and (detail2.nilai_anggaran <> detail.nilai_anggaran
                                                    or detail2.rekening_code <> detail.rekening_code 
                                                    or detail2.subtitle<>detail.subtitle
                                                    or detail2.is_covid<>detail.is_covid
                                                    or detail2.komponen_name <> detail.komponen_name
                                                    or detail2.keterangan_koefisien <> detail.keterangan_koefisien
                                                    or detail2.detail_name <> detail.detail_name
                                                    $where_anggaran2 )


                                            ORDER BY

                                                    rekening.rekening_code,
                                                    detail2.subtitle ,
                                                    detail2.komponen_name
                                            )
                                            union
                                            (
                                            SELECT

                                                    rekening.rekening_code,rekening.rekening_code as rekening_code2,
                                                    rekening.rekening_name as nama_rekening2,
                                                    detail2.komponen_name || ' ' || detail2.detail_name as detail_name22,
                                                    detail2.komponen_harga_awal as detail_harga2,
                                                    detail2.pajak as pajak2,
                                                    detail2.komponen_id as komponen_id2,
                                                    detail2.detail_name as detail_name2,
                                                    detail2.komponen_name as komponen_name2,
                                                    detail2.tahap as tahap2,
                                                    detail2.volume as volume2,
                                                    detail2.volume_orang as volumo2,
                                                    detail2.subtitle as subtitle_name2,
                                                    detail2.satuan as detail_satuan2,
                                                    replace(detail2.keterangan_koefisien,'<*>','X') as keterangan_koefisien2,
                                                    replace(detail2.note_koefisien,'<*>','X') as note_koefisien2,
                                                    detail2.detail_no as detail_no2,
                                                    detail2.volume * detail2.komponen_harga_awal as hasil2,
                                                    (detail2.nilai_anggaran) as hasil_kali2,
                                                    '' as detail_name,
                                                    0 as detail_harga,
                                                    0 as pajak,
                                                    '' as komponen_id,
                                                    '' as detail_name,
                                                    '' as komponen_name,
                                                    '' as tahap,
                                                    0 as volume,
                                                    0 as volumo,
                                                    '' as subtitle_name,
                                                    '' as detail_satuan,
                                                    '' as keterangan_koefisien,
                                                    '' as note_koefisien,
                                                    0 as detail_no,
                                                    0 as hasil,
                                                    0 as hasil_kali,
                                                    detail2.detail_name as detail_name_rd,
                                                    detail2.sub as sub2,
                                                    '' as sub,
                                                    detail2.note_skpd as note_skpd,
                                                    detail2.note_peneliti as note_peneliti,
                                                    detail2.note_tapd as note_tapd,
                                                    detail2.note_bappeko as note_bappeko,
                                                    detail2.status_level as status_level,
                                                    detail2.status_level_tolak,
                                                    detail2.status_sisipan,
                                                    detail2.status_komponen_berubah,
                                                    detail2.status_komponen_baru,
                                                    detail2.status_lelang,
                                                    detail2.is_hpsp,
                                                    detail2.is_tapd_setuju,
                                                    detail2.is_bappeko_setuju,
                                                    detail2.is_penyelia_setuju,
                                                    detail2.is_bagian_hukum_setuju,
                                                    detail2.is_inspektorat_setuju,
                                                    detail2.is_badan_kepegawaian_setuju,
                                                    detail2.is_lppa_setuju,
                                                    detail2.is_musrenbang,
                                                    detail2.is_output,
                                                    detail2.prioritas_wali,
                                                    detail2.is_potong_bpjs,
                                                    detail2.is_bagian_organisasi_setuju,
                                                    11 as sumber_dana_id,
                                                    false as is_covid,
                                                    detail2.sumber_dana_id as sumber_dana_id2,
                                                    detail2.is_covid as is_covid2
                                            FROM
                                                    " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail detail2, " . sfConfig::get('app_default_schema') . ".rekening rekening

                                            WHERE
                                                    detail2.kegiatan_code = '$kode_kegiatan' and
                                                    detail2.unit_id='$unit_id'
                                                    
                                                    and ((detail2.nilai_anggaran > 0
                                                    and (detail2.unit_id||detail2.kegiatan_code||detail2.detail_no) not in
                                                    (
                                                    select (detail.unit_id||detail.kegiatan_code||detail.detail_no) from " . sfConfig::get('app_default_schema') . ".$tabel_semula detail 
                                                    where detail.kegiatan_code = '$kode_kegiatan' and detail.unit_id='$unit_id'
                                                    )) or (detail2.unit_id||detail2.kegiatan_code||detail2.detail_no) in
                                                    (
                                                    select (detail.unit_id||detail.kegiatan_code||detail.detail_no) from " . sfConfig::get('app_default_schema') . ".$tabel_semula detail 
                                                    where detail.kegiatan_code = '$kode_kegiatan' and detail.unit_id='$unit_id'
                                                    )) 
                                                    
                                                    and detail2.status_hapus=false
                                                    and detail2.rekening_code='$kode_rekening'
                                                    and detail2.subtitle ilike '$subtitle'
                                                    and rekening.rekening_code=detail2.rekening_code
                                                    and detail2.status_level >= 0
                                                    
                                                    and (detail2.unit_id||detail2.kegiatan_code||detail2.detail_no) not in
                                                    (
                                                    select (detail.unit_id||detail.kegiatan_code||detail.detail_no) from " . sfConfig::get('app_default_schema') . ".$tabel_semula detail where detail.kegiatan_code = '$kode_kegiatan' and
                                                    detail.unit_id='$unit_id' and detail.subtitle ilike '$subtitle'
                                                    and detail.rekening_code='$kode_rekening'
                                                    )

                                            ORDER BY

                                                    rekening.rekening_code,
                                                    detail2.subtitle ,
                                                    detail2.komponen_name)
                                            union
                                            (
                                            SELECT

                                                    rekening.rekening_code,rekening.rekening_code as rekening_code2,
                                                    rekening.rekening_name as nama_rekening2,
                                                    '' as detail_name22,
                                                    0 as detail_harga2,
                                                    0 as pajak2,
                                                    '' as komponen_id2,
                                                    '' as detail_name2,
                                                    '' as komponen_name2,
                                                    '' as tahap2,
                                                    0 as volume2,
                                                    0 as volumo2,
                                                    '' as subtitle_name2,
                                                    '' as detail_satuan2,
                                                    '' as keterangan_koefisien2,
                                                    '' as note_koefisien2,
                                                    0 as detail_no2,
                                                    0 as hasil2,
                                                    0 as hasil_kali2,

                                                    detail.komponen_name || ' ' || detail.detail_name as detail_name2,
                                                    detail.komponen_harga_awal as detail_harga,
                                                    detail.pajak as pajak,
                                                    detail.komponen_id as komponen_id,
                                                    detail.detail_name as detail_name,
                                                    detail.komponen_name as komponen_name,
                                                    detail.tahap as tahap,
                                                    detail.volume as volume,
                                                    detail.volume_orang as volumo,
                                                    detail.subtitle as subtitle_name,
                                                    detail.satuan as detail_satuan,
                                                    replace(detail.keterangan_koefisien,'<*>','X') as keterangan_koefisien,
                                                    replace(detail.note_koefisien,'<*>','X') as note_koefisien,
                                                    detail.detail_no as detail_no,
                                                    detail.volume * detail.komponen_harga_awal as hasil,
                                                    (detail.nilai_anggaran) as hasil_kali,
                                                    '' as detail_name_rd,
                                                    '' as sub2,
                                                    detail.sub,
                                                    '' as note_skpd,
                                                    '' as note_peneliti,
                                                    '' as note_tapd,
                                                    '' as note_bappeko,
                                                    null as status_level,
                                                    null,
                                                    false,
                                                    false,
                                                    false,
                                                    null,
                                                    null,
                                                    null,
                                                    null,
                                                    null,
                                                    null,
                                                    null,
                                                    null,
                                                    null,
                                                    null,
                                                    null,
                                                    null,
                                                    null,
                                                    null,                                                   
                                                    11 as sumber_dana_id,
                                                    false as is_covid,
                                                    11 as sumber_dana_id2,
                                                    false as is_covid2
                                            FROM
                                                    " . sfConfig::get('app_default_schema') . ".$tabel_semula detail, " . sfConfig::get('app_default_schema') . ".rekening rekening

                                            WHERE
                                                    detail.kegiatan_code = '$kode_kegiatan' and
                                                    detail.unit_id='$unit_id'

                                                    and detail.status_hapus=false
                                                    and detail.rekening_code='$kode_rekening'
                                                    and detail.subtitle ilike '$subtitle'
                                                    and rekening.rekening_code=detail.rekening_code
                                                    and (detail.unit_id||detail.kegiatan_code||detail.detail_no) not in
                                                    (
                                                    select (detail2.unit_id||detail2.kegiatan_code||detail2.detail_no) from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail detail2 where detail2.kegiatan_code = '$kode_kegiatan' and
                                                    detail2.unit_id='$unit_id' and detail2.subtitle ilike '$subtitle' and detail2.status_hapus<>true
                                                    and detail2.rekening_code='$kode_rekening'
                                                    )

                                            ORDER BY

                                                    rekening.rekening_code,
                                                    detail.subtitle ,
                                                    detail.komponen_name)                                            
                                            ORDER BY sub2 ASC, sub ASC, detail_name22 ASC";

                                        if ($rs2->getString('rekening'))
                                        {
                                            if (substr($rekening_prev, 0, 7) != $kode_belanja) {
                                                $query = " select belanja_code, belanja_name from " . sfConfig::get('app_default_schema') . ".kelompok_belanja "
                                                . " where belanja_code='" . substr($rekening_prev, 0, 7) . "'";
                                                $con = Propel::getConnection();
                                                $stmt = $con->prepareStatement($query);
                                                $rs1 = $stmt->executeQuery();
                                                if ($rs1->next()) {
                                                    echo '<tr bgcolor="#ffffff"><td colspan="17" align="left"><strong>::: ' . $rs1->getString('belanja_code') . ' ' . $rs1->getString('belanja_name') . '</strong></td>';
                                                }
                                            }
                                            $kode_belanja = substr($rekening_prev, 0, 7);
                                            $ada_kosong = 0;
                                            $query = "select count(*) as tot
                                            from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail rd,  " . sfConfig::get('app_default_schema') . ".$tabel_semula prd
                                            where rd.unit_id = '$unit_id' and rd.kegiatan_code = '$kode_kegiatan' and rd.rekening_code = '" . $rs2->getString('rekening') . "' and rd.status_hapus = false and prd.status_hapus = false
                                            and rd.unit_id = prd.unit_id and rd.kegiatan_code = prd.kegiatan_code and rd.detail_no = prd.detail_no and prd.rekening_code = '" . $rs2->getString('rekening') . "'
                                            and rd.subtitle = '$subtitle' and prd.subtitle = '$subtitle_prev'";
                                            $con = Propel::getConnection();
                                            $stmt = $con->prepareStatement($query);
                                            $rs1 = $stmt->executeQuery();
                                            while ($rs1->next()) {
                                                $ada_kosong_1 = $rs1->getString('tot');
                                            }
                                            $query = "select count(*) as tot
                                            from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail rd
                                            where rd.unit_id = '$unit_id' and rd.kegiatan_code = '$kode_kegiatan' and rd.rekening_code = '" . $rs2->getString('rekening') . "' and rd.status_hapus = false
                                            and rd.subtitle = '$subtitle' ";
                                            $con = Propel::getConnection();
                                            $stmt = $con->prepareStatement($query);
                                            $rs1 = $stmt->executeQuery();
                                            while ($rs1->next()) {
                                                $ada_kosong_2 = $rs1->getString('tot');
                                            }
                                            $query = "select count(*) as tot
                                            from " . sfConfig::get('app_default_schema') . ".$tabel_semula rd
                                            where rd.unit_id = '$unit_id' and rd.kegiatan_code = '$kode_kegiatan' and rd.rekening_code = '" . $rs2->getString('rekening') . "' and rd.status_hapus = false
                                            and rd.subtitle = '$subtitle' ";
                                            $con = Propel::getConnection();
                                            $stmt = $con->prepareStatement($query);
                                            $rs1 = $stmt->executeQuery();
                                            while ($rs1->next()) {
                                                $ada_kosong_3 = $rs1->getString('tot');
                                            }

                                            $ada_kosong = $ada_kosong_1 + $ada_kosong_2 + $ada_kosong_3;

                                            if ($ada_kosong > 0) {
                                                if ($tahap != 'murni') {
                                                    echo '<tr bgcolor="#e6e6e6"><td colspan="7" align="left"><strong>' . $rekening_prev . '</strong></td>';
                                                    echo '<td>&nbsp;</td>';
                                                }
                                                echo '<td bgcolor="#e6e6e6" colspan="12" align="left"><strong>' . $rekening . '</strong></td></tr>';
                                            }
                                        }
                                        else if ($rs2->getString('rekening') == '')
                                        {
                                            if (substr($rekening, 0, 7) != $kode_belanja) {
                                                $query = " select belanja_code, belanja_name from " . sfConfig::get('app_default_schema') . ".kelompok_belanja "
                                                . " where belanja_code='" . substr($rekening, 0, 7) . "'";
                                                $con = Propel::getConnection();
                                                $stmt = $con->prepareStatement($query);
                                                $rs1 = $stmt->executeQuery();
                                                if ($rs1->next()) {
                                                    echo '<tr bgcolor="#ffffff"><td colspan="17" align="left"><strong>::: ' . $rs1->getString('belanja_code') . ' ' . $rs1->getString('belanja_name') . '</strong></td>';
                                                }
                                            }
                                            $kode_belanja = substr($rekening, 0, 7);
                                            if ($tahap != 'murni') {
                                                echo '<tr bgcolor="#ffffff"><td colspan="7" align="left"><strong>&nbsp;</strong></td>';
                                                echo '<td><strong><span style="color:red;">!</span></strong></td>';
                                            }
                                            echo '<td bgcolor="#e6e6e6" colspan="12" align="left"><strong>' . $rekening . '</strong></td></tr>';
                                        }
                                        $stmt3 = $con->prepareStatement($query3);
                                        $rs3 = $stmt3->executeQuery();
                                        $total = 0;
                                        $total_n = 0;
                                        while ($rs3->next())
                                        {
                                            echo input_hidden_tag('ada_komponen', 1);
                                            if ($rs2->getString('rekening'))
                                            {
                                                if (($cek_sub != trim($rs3->getString('sub')) || $cek_sub2 != trim($rs3->getString('sub2'))) && (trim($rs3->getString('sub')) != '' || trim($rs3->getString('sub2')) != '') && (trim($rs3->getString('sub')) != '' || trim($rs3->getString('sub2')) != $cek_sub2)) {
                                                    if ($tahap != 'murni') {
                                                        echo '<tr bgcolor="#ffffff"><td colspan="7" align="left"><strong>.:. ' . trim($rs3->getString('sub')) . '</strong></td>';
                                                        echo '<td></td>';
                                                    }
                                                    echo '<td colspan="12" align="left"><strong>.:. ' . trim($rs3->getString('sub2')) . '</strong></td></tr>';
                                                }
                                                $cek_sub = trim($rs3->getString('sub'));
                                                $cek_sub2 = trim($rs3->getString('sub2'));
                                                $status_posisi = '';
                                                if ($rs3->getInt('detail_no2') != 0 && $rs3->getInt('status_level') == 0) {
                                                    $status_posisi = 'Entri';
                                                } elseif ($rs3->getString('status_level') == 1) {
                                                    $status_posisi = 'PPTK';
                                                } elseif ($rs3->getString('status_level') == 2) {
                                                    $status_posisi = 'KPA';
                                                } elseif ($rs3->getString('status_level') == 3) {
                                                    $status_posisi = 'PA I';
                                                } elseif ($rs3->getString('status_level') == 4) {
                                                    $penyetuju = array();
                                                    if($rs3->getBoolean('is_tapd_setuju'))
                                                        array_push($penyetuju, 'BPKAD');
                                                    if($rs3->getBoolean('is_bappeko_setuju'))
                                                        array_push($penyetuju, 'Bappeda');
                                                    if($rs3->getBoolean('is_penyelia_setuju'))
                                                        array_push($penyetuju, 'BPBJAP');
                                                    if($rs3->getBoolean('is_bagian_hukum_setuju'))
                                                        array_push($penyetuju, 'Bag. Hukum');
                                                    if($rs3->getBoolean('is_inspektorat_setuju'))
                                                        array_push($penyetuju, 'Inspektorat');
                                                    if($rs3->getBoolean('is_badan_kepegawaian_setuju'))
                                                        array_push($penyetuju, 'BKPSDM');
                                                    if($rs3->getBoolean('is_lppa_setuju'))
                                                        // array_push($penyetuju, 'Bag. LPPA');
                                                        array_push($penyetuju, 'Bapenda');
                                                    if($rs3->getBoolean('is_bagian_organisasi_setuju'))
                                                        array_push($penyetuju, 'Bag. Organisasi');

                                                    $strPenyetuju = implode(', ', $penyetuju);
                                                    $status_posisi = 'Tim Anggaran ('.$strPenyetuju.')';
                                                } elseif ($rs3->getString('status_level') == 5) {
                                                    $status_posisi = 'PA II';
                                                } elseif ($rs3->getString('status_level') == 6) {
                                                    $status_posisi = 'Penyelia II';
                                                } elseif ($rs3->getString('status_level') == 7) {
                                                    $status_posisi = 'RKA';
                                                } else
                                                $status_posisi = '';

                                                $status_komponen = '';
                                                $warna_status = '';

                                                if ($rs3->getBoolean('status_komponen_berubah')) {
                                                    $status_komponen = 'Penyesuaian Usulan';
                                                    $warna_status = 'blue';
                                                } elseif ($rs3->getBoolean('status_komponen_baru')) {
                                                    $status_komponen = 'Usulan Baru';
                                                    $warna_status = 'red';
                                                }

                                                $hasil = 0;
                                                $detail_harga = 0;
                                                $volume = 0;
                                                $pajak = 0;
                                                $detail_harga = $rs3->getString('detail_harga');
                                                $volume = $rs3->getString('volume');
                                                $pajak = $rs3->getString('pajak');
                                                $subnya = '';

                                                if ($tahap != 'murni')
                                                {
                                                    if (($rs3->getString('komponen_name') != $rs3->getString('komponen_name2')) ||
                                                        ($rs3->getString('detail_name22') != $rs3->getString('detail_name2')) ||
                                                        ($rs3->getString('detail_satuan') != $rs3->getString('detail_satuan2')) ||
                                                        ($rs3->getString('keterangan_koefisien') != $rs3->getString('keterangan_koefisien2')) ||
                                                        ($rs3->getString('pajak') != $rs3->getString('pajak2')) ||
                                                        ($rs3->getString('detail_harga2') != $rs3->getString('detail_harga')) ||
                                                        ($rs3->getString('volume') != $rs3->getString('volume2')))
                                                        {
                                                            if ($sf_user->getNamaUser() != 'parlemen2')
                                                            {
                                                                echo '<tr bgcolor="#ffffff" valign="top">';
                                                                echo '<td align="left"><span>' . $subnya . $rs3->getString('komponen_name') . ' ' . $rs3->getString('detail_name') . '&nbsp;<span class="badge badge-danger">' . $rs3->getString('tahap') . '</span></span>';
                                                                    if($rs3->getBoolean('is_output') == TRUE) {
                                                                        echo ' <span class="badge badge-info">Output</span>';
                                                                    }
                                                                    if($rs3->getBoolean('is_musrenbang') == TRUE) {
                                                                        echo ' <span class="badge badge-success">Musrenbang</span>';
                                                                    }
                                                                    if($rs3->getBoolean('prioritas_wali') == TRUE) {
                                                                        echo ' <span class="badge badge-danger">Prioritas</span>';
                                                                    }
                                                                    if($rs3->getBoolean('is_covid') == TRUE) {
                                                                        echo ' <span class="badge badge-danger">Covid-19</span>';
                                                                    }
                                                                    if($rs3->getString('sumber_dana_id') != 11) {
                                                                    $c = new Criteria();
                                                                    $c->add(MasterSumberDanaPeer::ID, $rs3->getString('sumber_dana_id'));
                                                                    $c->addAscendingOrderByColumn(MasterSumberDanaPeer::SUMBER_DANA);
                                                                    $v = MasterSumberDanaPeer::doSelectOne($c);
                                                                    echo ' <span class="badge badge-primary">'. $v->getSumberDana() .'</span>';
                                                                    }

                                                                echo '</td>';
                                                                echo '<td align="center" nowrap="nowrap"><span>' . $rs3->getString('detail_satuan') . '</span></td>';
                                                                echo '<td align="center"><span>' . $rs3->getString('keterangan_koefisien') .'</span></td>';
                                                                echo '<td align="right" nowrap="nowrap">';
                                                                if ($rs3->getString('detail_satuan') == '%') {
                                                                    $len = strlen(substr(strrchr($rs3->getString('detail_harga'), "."), 1));
                                                                    echo number_format($rs3->getFloat('detail_harga'), $len, ',', '.');
                                                                } else {
                                                                    echo number_format($rs3->getFloat('detail_harga'), 0, ',', '.');
                                                                }
                                                                echo '</td>';
                                                                $hasil = myTools::round_ganjil_genap($detail_harga * $volume);
                                                                echo '<td align="right" nowrap="nowrap">' . number_format($hasil, 0, ',', '.') . '</td>';
                                                                echo '<td align="right" nowrap="nowrap">' . $rs3->getString('pajak') . '%</td>';
                                                                $total1 = $hasil - (($pajak / 100) * $hasil);
                                                                echo '<td align="right" nowrap="nowrap">' . number_format($rs3->getString('hasil_kali'), 0, ',', '.') . '</td>';
                                                                $total = $total + $total1;
                                                                $jumlah_harga_prev += $hasil;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            if ($sf_user->getNamaUser() != 'parlemen2')
                                                            {
                                                                echo '<tr bgcolor="#ffffff" valign="top">';
                                                                echo '<td align="left"><span>' . $subnya . $rs3->getString('komponen_name') . ' ' . $rs3->getString('detail_name').'&nbsp;<span class="badge badge-danger">' . $rs3->getString('tahap') . '</span></span></td>';
                                                                 if($rs3->getString('sumber_dana_id') != 11) {
                                                                    $c = new Criteria();
                                                                    $c->add(MasterSumberDanaPeer::ID, $rs3->getString('sumber_dana_id'));
                                                                    $c->addAscendingOrderByColumn(MasterSumberDanaPeer::SUMBER_DANA);
                                                                    $v = MasterSumberDanaPeer::doSelectOne($c);
                                                                    echo ' <span class="badge badge-primary">'. $v->getSumberDana() .'</span>';
                                                                    }
                                                                echo '<td align="center" nowrap="nowrap"><span>' . $rs3->getString('detail_satuan') . '</span></td>';
                                                                echo '<td align="center"><span>' . $rs3->getString('keterangan_koefisien') .'</span></td>';
                                                                echo '<td align="right" nowrap="nowrap">';
                                                                if ($rs3->getString('detail_satuan') == '%') {
                                                                        $len = strlen(substr(strrchr($rs3->getString('detail_harga'), "."), 1));
                                                                        echo number_format($rs3->getFloat('detail_harga'), $len, ',', '.');
                                                                    } else {
                                                                        echo number_format($rs3->getFloat('detail_harga'), 0, ',', '.');
                                                                    }
                                                                    echo '</td>';
                                                                $hasil = myTools::round_ganjil_genap($detail_harga * $volume);
                                                                echo '<td align="right" nowrap="nowrap">' . number_format($hasil, 0, ',', '.') . '</td>';
                                                                echo '<td align="right" nowrap="nowrap">' . $rs3->getString('pajak') . '%</td>';
                                                                $total1 = $hasil - (($pajak / 100) * $hasil);
                                                                echo '<td align="right" nowrap="nowrap">' . number_format($rs3->getString('hasil_kali'), 0, ',', '.') . '</td>';
                                                                $total = $total + $total1;
                                                                $jumlah_harga_prev += $hasil;
                                                            }
                                                        }
                                                
                                                    $kiri = $rs3->getString('hasil_kali');
                                                    if (($rs3->getString('komponen_name') != $rs3->getString('komponen_name2')) ||
                                                        ($rs3->getString('detail_name22') != $rs3->getString('detail_name2')) ||
                                                        ($rs3->getString('detail_satuan') != $rs3->getString('detail_satuan2')) ||
                                                        ($rs3->getString('keterangan_koefisien') != $rs3->getString('keterangan_koefisien2')) ||
                                                        ($rs3->getString('pajak') != $rs3->getString('pajak2')) ||
                                                        ($rs3->getString('detail_harga2') != $rs3->getString('detail_harga')) ||
                                                        ($rs3->getString('volume') != $rs3->getString('volume2')))
                                                        {
                                                            if ($sf_user->getNamaUser() != 'parlemen2') {
                                                                echo '<td><strong><span style="color:red;">!</span></strong></td>';
                                                            }
                                                        } else {
                                                            if ($sf_user->getNamaUser() != 'parlemen2') {
                                                                echo '<td>&nbsp;</td>';
                                                            }
                                                        }
                                                }
                                                    $subnya2 = '';
                                                    $subnya = '';

                                                    if (($rs3->getString('komponen_name') != $rs3->getString('komponen_name2')) ||
                                                    ($rs3->getString('detail_name22') != $rs3->getString('detail_name2')) ||
                                                    ($rs3->getString('detail_satuan') != $rs3->getString('detail_satuan2')) ||
                                                    ($rs3->getString('keterangan_koefisien') != $rs3->getString('keterangan_koefisien2')) ||
                                                    ($rs3->getString('pajak') != $rs3->getString('pajak2')) ||
                                                    ($rs3->getString('detail_harga2') != $rs3->getString('detail_harga')) ||
                                                    ($rs3->getString('volume') != $rs3->getString('volume2')))
                                                    {
                                                        if ($sf_user->getNamaUser() != 'parlemen2')
                                                        {
                                                            $query_new = "select tahap from " . sfConfig::get('app_default_schema') . ".komponen "
                                                            . "where komponen_id='" . $rs3->getString('komponen_id') . "' or komponen_id='" . $rs3->getString('komponen_id2') . "'";
                                                            $stmt = $con->prepareStatement($query_new);
                                                            $t = $stmt->executeQuery();
                                                            while ($t->next()) {
                                                                if ($t->getString('tahap') == DinasMasterKegiatanPeer::getTahapKegiatan($unit_id, $kode_kegiatan) && sfConfig::get('app_tahap_edit') != 'murni') {
                                                                    $baru = image_tag('/images/newanima.gif');
                                                                } else {
                                                                    $baru = null;
                                                                }
                                                            }
                                                            if($rs3->getString('volume2') != 0)
                                                            {
                                                                echo '<td align="left"><span>' . $subnya2 . $rs3->getString('komponen_name2') . ' ' . $rs3->getString('detail_name_rd') . $baru;
                                                                if ($rs3->getString('detail_satuan2') == 'Paket' && $rs3->getString('volume2') == '1' && $rs3->getString('status_lelang') == 'lock') {
                                                                    echo ' <span class="badge badge-primary">PSP</span>';
                                                                }
                                                                if($rs3->getBoolean('is_hpsp') == TRUE) {
                                                                    echo ' <span class="badge badge-danger">HPSP</span>';
                                                                }
                                                                if($rs3->getBoolean('is_output') == TRUE) {
                                                                    echo ' <span class="badge badge-info">Output</span>';
                                                                }
                                                                if($rs3->getBoolean('is_musrenbang') == TRUE) {
                                                                    echo ' <span class="badge badge-success">Musrenbang</span>';
                                                                }
                                                                if($rs3->getBoolean('prioritas_wali') == TRUE) {
                                                                    echo ' <span class="badge badge-danger">Prioritas</span>';
                                                                }
                                                                if($rs3->getBoolean('is_covid2') == TRUE) {
                                                                    echo ' <span class="badge badge-danger">Covid-19</span>';
                                                                }
                                                                if($rs3->getString('sumber_dana_id2') != 11) {
                                                                    $c = new Criteria();
                                                                    $c->add(MasterSumberDanaPeer::ID, $rs3->getString('sumber_dana_id2'));
                                                                    $c->addAscendingOrderByColumn(MasterSumberDanaPeer::SUMBER_DANA);
                                                                    $v = MasterSumberDanaPeer::doSelectOne($c);
                                                                    echo ' <span class="badge badge-primary">'. $v->getSumberDana() .'</span>';
                                                                }
                                                            } else echo '<td align="left"><span></span></td>';
                                                            echo '</span></td>';
                                                            if($rs3->getString('volume2') != 0)
                                                                echo '<td align="center" nowrap="nowrap"><span>' . $rs3->getString('detail_satuan2') . '</span></td>';
                                                            else
                                                                echo '<td align="center" nowrap="nowrap"><span></span></td>';

                                                            if ($rs3->getString('is_potong_bpjs') == TRUE && $rs3->getString('volume2') != 0 && $rs3->getString('detail_satuan2') == 'Orang Bulan') {
                                                                echo '<td align="center"><span>' . $rs3->getString('keterangan_koefisien2') . '</span><br/><span> Volume Orang : ' . $rs3->getString('volumo2') . '</span></br><span>' . $rs3->getString('note_koefisien2') . '</span></td>';
                                                            }
                                                            else if ($rs3->getString('volume2') != 0) {
                                                                echo '<td align="center"><span>' . $rs3->getString('keterangan_koefisien2') . '</span><br/><span>' . $rs3->getString('note_koefisien2') . '</span></td>';
                                                            }
                                                            else
                                                                echo '<td align="center"><span></span><br/><span></span></td>';
                                                            if($rs3->getString('volume2') != 0) 
                                                            {
                                                                echo '<td align="right" nowrap="nowrap">';
                                                                if ($rs3->getString('detail_satuan2') == '%') 
                                                                {
                                                                    $len = strlen(substr(strrchr($rs3->getString('detail_harga2'), "."), 1));
                                                                    echo number_format($rs3->getFloat('detail_harga2'), $len, ',', '.');
                                                                } else {
                                                                    echo number_format($rs3->getFloat('detail_harga2'), 0, ',', '.');
                                                                }
                                                                echo '</td>';
                                                            } else
                                                                echo '<td align="right" nowrap="nowrap">0</td>'; 
                                                        }
                                                        $hasil = floor($rs3->getString('detail_harga2') * $rs3->getString('volume2'));
                                                        if($rs3->getString('volume2') != 0)
                                                            echo '<td align="right" nowrap="nowrap">' . number_format($hasil, 0, ',', '.') . '</td>';
                                                        else
                                                            echo '<td align="right" nowrap="nowrap">0</td>';
                                                        if($rs3->getString('volume2') != 0)
                                                            echo '<td align="right" nowrap="nowrap">' . $rs3->getString('pajak2') . '%</td>';
                                                        else
                                                            echo '<td align="right" nowrap="nowrap">0%</td>';
                                                        $total1 = $hasil - (($rs3->getString('pajak2') / 100) * $hasil);
                                                        $kanan = $rs3->getString('hasil_kali2');
                                                        $selisih = $kanan - $kiri;
                                                        echo '<td align="right" nowrap="nowrap">' . number_format($rs3->getString('hasil_kali2'), 0, ',', '.') . '</td>';
                                                        if($selisih >= 0)
                                                            echo '<td align="right" nowrap="nowrap">' . number_format($selisih, 0, ',', '.') . '</td>';
                                                        else
                                                            echo '<td align="right" nowrap="nowrap">(' . number_format(abs($selisih), 0, ',', '.') . ')</td>';
                                                        if(sfConfig::get('app_tahap_edit') != 'murni'):
                                                            echo '<td align="left">';
                                                            echo (strlen(trim($rs3->getString('note_skpd'))) > 0 ? '<strong>SKPD:</strong> ' . $rs3->getString('note_skpd') . '<br>' : '');
                                                            echo (strlen(trim($rs3->getString('note_peneliti'))) > 0 ? '<strong>Administrasi Pembangunan:</strong> ' . $rs3->getString('note_peneliti') . '<br>' : '');
                                                            echo (strlen(trim($rs3->getString('note_tapd'))) > 0 ? '<strong>BPKPD:</strong> ' . $rs3->getString('note_tapd') . '<br>' : '');
                                                            echo (strlen(trim($rs3->getString('note_bappeko'))) > 0 ? '<strong>Bappeko:</strong> ' . $rs3->getString('note_bappeko') . '<br>' : '');
                                                            echo '</td>';
                                                        endif;
                                                        echo '<td align="center">' . $status_posisi . '</td>';
                                                        echo '<td align="center"><p style="color:' . $warna_status . '">' . $status_komponen . '</p></td>';

                                                        if ($rs3->getInt('detail_no2') == 0 && $rs3->getInt('status_level') == 0) {
                                                            echo '<td align="center">';
                                                            echo image_tag('centang', 'title="komponen mengalami perubahan rekening/subtitle, silahkan centang komponen yang baru"');
                                                            echo '</td>';
                                                        } elseif ($sf_user->hasCredential('kpa')) {
                                                            echo '<td align="center">';
                                                            if ($rs3->getInt('status_level') == 2 && $rs3->getInt('detail_no2') != 0) {
                                                                echo checkbox_tag('pilihaction[]', $rs3->getInt('detail_no2'));
                                                            }
                                                            echo '</td>';
                                                        } elseif ($sf_user->getNamaUser() == 'anggaran') {
                                                            echo '<td align="center">';
                                                            if ($rs3->getInt('status_level') == 4 && !$rs3->getBoolean('is_tapd_setuju') && $rs3->getInt('detail_no2') != 0) {
                                                                // echo checkbox_tag('pilihaction[]', $rs3->getInt('detail_no2'));
                                                            }
                                                            echo '</td>';
                                                        } elseif ($sf_user->getNamaUser() == 'bagian_hukum') {
                                                            echo '<td align="center">';
                                                            if ($rs3->getInt('status_level') == 4 && !$rs3->getBoolean('is_bagian_hukum_setuju') && $rs3->getInt('detail_no2') != 0) {
                                                                echo checkbox_tag('pilihaction[]', $rs3->getInt('detail_no2'));
                                                            }
                                                            echo '</td>';
                                                        } elseif ($sf_user->getNamaUser() == 'inspektorat') {
                                                            echo '<td align="center">';
                                                            if ($rs3->getInt('status_level') == 4 && !$rs3->getBoolean('is_inspektorat_setuju') && $rs3->getInt('detail_no2') != 0) {
                                                                echo checkbox_tag('pilihaction[]', $rs3->getInt('detail_no2'));
                                                            }
                                                            echo '</td>';
                                                        } elseif ($sf_user->getNamaUser() == 'bkd' || $sf_user->getNamaUser() == 'bkpsdm') {
                                                            echo '<td align="center">';
                                                            if ($rs3->getInt('status_level') == 4 && !$rs3->getBoolean('is_badan_kepegawaian_setuju') && $rs3->getInt('detail_no2') != 0) {
                                                                echo checkbox_tag('pilihaction[]', $rs3->getInt('detail_no2'));
                                                            }
                                                            echo '</td>';
                                                        } elseif ($sf_user->getNamaUser() == 'lppa' || $sf_user->getNamaUser() == 'bapenda') {
                                                            echo '<td align="center">';
                                                            if ($rs3->getInt('status_level') == 4 && !$rs3->getBoolean('is_lppa_setuju') && $rs3->getInt('detail_no2') != 0) {
                                                                echo checkbox_tag('pilihaction[]', $rs3->getInt('detail_no2'));
                                                            }
                                                            echo '</td>';
                                                        } elseif ($sf_user->getNamaUser() == 'bagian_organisasi') {
                                                            echo '<td align="center">';
                                                            if ($rs3->getInt('status_level') == 4 && !$rs3->getBoolean('is_bagian_organisasi_setuju') && $rs3->getInt('detail_no2') != 0) {
                                                                echo checkbox_tag('pilihaction[]', $rs3->getInt('detail_no2'));
                                                            }
                                                            echo '</td>';
                                                        } elseif ($sf_user->hasCredential('bappeko')) {
                                                            echo '<td align="center">';
                                                            if ($rs3->getInt('status_level') == 4 && !$rs3->getBoolean('is_bappeko_setuju') && $rs3->getInt('detail_no2') != 0) {
                                                                echo checkbox_tag('pilihaction[]', $rs3->getInt('detail_no2'));
                                                            }
                                                            echo '</td>';
                                                        } elseif ($sf_user->hasCredential('peneliti')) {
                                                            echo '<td align="center">';
                                                            $penyelia_bpkad = new Criteria();
                                                            $penyelia_bpkad->add(PenyeliaBpkadPeer::NAMA, $sf_user->getNamaUser(), Criteria::EQUAL);
                                                            if ($rs_penyelia = PenyeliaBpkadPeer::doSelectOne($penyelia_bpkad)) {
                                                                if ($rs3->getInt('status_level') == 4 && !$rs3->getBoolean('is_tapd_setuju') && $rs3->getInt('detail_no2') != 0) {
                                                                    echo checkbox_tag('pilihaction[]', $rs3->getInt('detail_no2'));
                                                                }
                                                            }else{
                                                                if ($rs3->getInt('status_level') == 4 && !$rs3->getBoolean('is_penyelia_setuju') && $rs3->getInt('detail_no2') != 0) {
                                                                    echo checkbox_tag('pilihaction[]', $rs3->getInt('detail_no2'));
                                                                }
                                                            }
                                                            // if ($rs3->getInt('status_level') == 4 && $rs3->getInt('detail_no2') != 0) {
                                                            //     echo checkbox_tag('pilihaction[]', $rs3->getInt('detail_no2'));
                                                            // }
                                                            echo '</td>';
                                                        } elseif ($sf_user->hasCredential('pptk')) {
                                                            echo '<td align="center">';
                                                            if ($rs3->getInt('status_level') == 1 && $rs3->getInt('detail_no2') != 0) {
                                                                echo checkbox_tag('pilihaction[]', $rs3->getInt('detail_no2'));
                                                            }
                                                            echo '</td>';
                                                        } elseif ($sf_user->hasCredential('pa')) {
                                                            echo '<td align="center">';
                                                            if (($rs3->getInt('status_level') == 3 || $rs3->getInt('status_level') == 5) && ($rs3->getInt('detail_no2') != 0)) {
                                                                echo checkbox_tag('pilihaction[]', $rs3->getInt('detail_no2'));
                                                            }
                                                            echo '</td>';
                                                        } elseif ($sf_user->hasCredential('dinas')) {
                                                            echo '<td align="center">';
                                                            if ($rs3->getInt('status_level') == 0 && $rs3->getInt('detail_no2') != 0) {
                                                                echo checkbox_tag('pilihaction[]', $rs3->getInt('detail_no2'));
                                                            }
                                                            echo '</td>';
                                                        } elseif (($sf_user->hasCredential('admin_super') || $sf_user->hasCredential('admin')) && $sf_user->getNamaLogin() !== 'tim_shs') {
                                                            echo '<td align="center">';
                                                            if ($rs3->getInt('status_level') == 6)
                                                                echo checkbox_tag('pilihaction[]', $rs3->getInt('detail_no2'), FALSE, array('class' => 'chk_rka'));
                                                            else {
                                                                echo checkbox_tag('pilihaction[]', $rs3->getInt('detail_no2'));
                                                                $sesuatu = true;
                                                            }
                                                            echo '</td>';
                                                        }
                                                        $total_n += $total1;
                                                        echo '</tr>';
                                                        $jumlah_harga += $hasil;
                                                    }
                                            }
                                            else if ($rs2->getString('rekening'))
                                            {
                                                if ($sf_user->getNamaUser() != 'parlemen2') 
                                                {
                                                    if($rs3->getString('volume2') != 0) {
                                                        echo '<td align="left"><span>' . $subnya2 . $rs3->getString('komponen_name2') . ' ' . $rs3->getString('detail_name_rd');
                                                        if ($rs3->getString('detail_satuan2') == 'Paket' && $rs3->getString('volume2') == '1' && $rs3->getString('status_lelang') == 'lock') {
                                                            echo ' <span class="badge badge-primary">PSP</span>';
                                                        }
                                                        if ($rs3->getBoolean('is_hpsp') == TRUE) {
                                                            echo ' <span class="badge badge-danger">HPSP</span>';
                                                        }
                                                        if ($rs3->getBoolean('is_output') == TRUE) {
                                                            echo ' <span class="badge badge-info">Output</span>';
                                                        }
                                                        if ($rs3->getBoolean('is_musrenbang') == TRUE) {
                                                            echo ' <span class="badge badge-success">Musrenbang</span>';
                                                        }
                                                        if ($rs3->getBoolean('prioritas_wali') == TRUE) {
                                                            echo ' <span class="badge badge-danger">Prioritas</span>';
                                                        }
                                                        if($rs3->getBoolean('is_covid') == TRUE) {
                                                            echo ' <span class="badge badge-danger">Covid-19</span>';
                                                        }
                                                        if($rs3->getString('sumber_dana_id') != 11) {
                                                                    $c = new Criteria();
                                                                    $c->add(MasterSumberDanaPeer::ID, $rs3->getString('sumber_dana_id'));
                                                                    $c->addAscendingOrderByColumn(MasterSumberDanaPeer::SUMBER_DANA);
                                                                    $v = MasterSumberDanaPeer::doSelectOne($c);
                                                                    echo ' <span class="badge badge-primary">'. $v->getSumberDana() .'</span>';
                                                                }
                                                    } else echo '<td align="left"><span></span></td>';
                                                    echo '</span></td>';
                                                    if($rs3->getString('volume2') != 0)
                                                        echo '<td align="center" nowrap="nowrap"><span>' . $rs3->getString('detail_satuan2') . '</span></td>';
                                                    else
                                                        echo '<td align="center" nowrap="nowrap"><span></span></td>';
                                                    echo '<td align="center"><span>' . $rs3->getString('keterangan_koefisien2') . '</span></td>';

                                                    echo '<td align="right" nowrap="nowrap">';
                                                    if($rs3->getString('volume2') != 0) {
                                                        echo '<td align="right" nowrap="nowrap">';
                                                        if ($rs3->getString('detail_satuan2') == '%') {
                                                            $len = strlen(substr(strrchr($rs3->getString('detail_harga2'), "."), 1));
                                                            echo number_format($rs3->getFloat('detail_harga2'), $len, ',', '.');
                                                        } else {
                                                            echo number_format($rs3->getFloat('detail_harga2'), 0, ',', '.');
                                                        }
                                                        echo '</td>';
                                                    } else
                                                        echo '<td align="right" nowrap="nowrap">0</td>'; 
                                                }
                                                $hasil = myTools::round_ganjil_genap($rs3->getString('detail_harga2') * $rs3->getString('volume2'));
                                                echo '<td align="right" nowrap="nowrap">' . number_format($hasil, 0, ',', '.') . '</td>';
                                                if($rs3->getString('volume2') != 0)
                                                    echo '<td align="right" nowrap="nowrap">' . $rs3->getString('pajak2') . '%</td>';
                                                else
                                                    echo '<td align="right" nowrap="nowrap">0%</td>';
                                                $total1 = $hasil - (($rs3->getString('pajak2') / 100) * $hasil);
                                                $kanan = $rs3->getString('hasil_kali2');
                                                $selisih = $kanan - $kiri;
                                                if($rs3->getString('volume2') != 0)
                                                    echo '<td align="right" nowrap="nowrap">' . number_format($rs3->getString('hasil_kali2'), 0, ',', '.') . '</td>';
                                                else
                                                    echo '<td align="right" nowrap="nowrap">0</td>';
                                                if($selisih >= 0)
                                                    echo '<td align="right" nowrap="nowrap">' . number_format($selisih, 0, ',', '.') . '</td>';
                                                else
                                                    echo '<td align="right" nowrap="nowrap">(' . number_format(abs($selisih), 0, ',', '.') . ')</td>';
                                                if(sfConfig::get('app_tahap_edit') != 'murni'):
                                                    echo '<td align="left">';
                                                    echo (strlen(trim($rs3->getString('note_skpd'))) > 0 ? '<strong>SKPD:</strong> ' . $rs3->getString('note_skpd') . '<br>' : '');
                                                    echo (strlen(trim($rs3->getString('note_peneliti'))) > 0 ? '<strong>Administrasi Pembangunan:</strong> ' . $rs3->getString('note_peneliti') . '<br>' : '');
                                                    echo (strlen(trim($rs3->getString('note_tapd'))) > 0 ? '<strong>BPKPD:</strong> ' . $rs3->getString('note_tapd') . '<br>' : '');
                                                    echo (strlen(trim($rs3->getString('note_bappeko'))) > 0 ? '<strong>Bappeko:</strong> ' . $rs3->getString('note_bappeko') . '<br>' : '');
                                                    echo '</td>';
                                                endif;
                                                echo '<td align="center">' . $status_posisi . '</td>';
                                                echo '<td align="center"><p style="color:' . $warna_status . '">' . $status_komponen . '</p></td>';
                                                if ($rs3->getInt('detail_no2') == 0 && $rs3->getInt('status_level') == 0) {
                                                    echo '<td align="center">';
                                                    echo image_tag('centang', 'title="komponen mengalami perubahan rekening/subtitle, silahkan centang komponen yang baru"');
                                                    echo '</td>';
                                                } elseif ($sf_user->hasCredential('kpa')) {
                                                    echo '<td align="center">';
                                                    if ($rs3->getInt('status_level') == 2 && $rs3->getInt('detail_no2') != 0) {
                                                        echo checkbox_tag('pilihaction[]', $rs3->getInt('detail_no2'));
                                                    }
                                                    echo '</td>';
                                                } elseif ($sf_user->getNamaUser() == 'anggaran') {
                                                    echo '<td align="center">';
                                                    if ($rs3->getInt('status_level') == 4 && !$rs3->getBoolean('is_tapd_setuju') && $rs3->getInt('detail_no2') != 0) {
                                                        // echo checkbox_tag('pilihaction[]', $rs3->getInt('detail_no2'));
                                                    }
                                                    echo '</td>';
                                                } elseif ($sf_user->hasCredential('bappeko')) {
                                                    echo '<td align="center">';
                                                    if ($rs3->getInt('status_level') == 4 && !$rs3->getBoolean('is_bappeko_setuju') && $rs3->getInt('detail_no2') != 0) {
                                                        echo checkbox_tag('pilihaction[]', $rs3->getInt('detail_no2'));
                                                    }
                                                    echo '</td>';
                                                } elseif ($sf_user->getNamaUser() == 'bagian_hukum') {
                                                    echo '<td align="center">';
                                                    if ($rs3->getInt('status_level') == 4 && !$rs3->getBoolean('is_bagian_hukum_setuju') && $rs3->getInt('detail_no2') != 0) {
                                                        echo checkbox_tag('pilihaction[]', $rs3->getInt('detail_no2'));
                                                    }
                                                    echo '</td>';
                                                } elseif ($sf_user->getNamaUser() == 'inspektorat') {
                                                    echo '<td align="center">';
                                                    if ($rs3->getInt('status_level') == 4 && !$rs3->getBoolean('is_inspektorat_setuju') && $rs3->getInt('detail_no2') != 0) {
                                                        echo checkbox_tag('pilihaction[]', $rs3->getInt('detail_no2'));
                                                    }
                                                    echo '</td>';
                                                } elseif ($sf_user->getNamaUser() == 'bkd' || $sf_user->getNamaUser() == 'bkpsdm') {
                                                    echo '<td align="center">';
                                                    if ($rs3->getInt('status_level') == 4 && !$rs3->getBoolean('is_badan_kepegawaian_setuju') && $rs3->getInt('detail_no2') != 0) {
                                                        echo checkbox_tag('pilihaction[]', $rs3->getInt('detail_no2'));
                                                    }
                                                    echo '</td>';
                                                } elseif ($sf_user->getNamaUser() == 'lppa' || $sf_user->getNamaUser() == 'bapenda') {
                                                    echo '<td align="center">';
                                                    if ($rs3->getInt('status_level') == 4 && !$rs3->getBoolean('is_lppa_setuju') && $rs3->getInt('detail_no2') != 0) {
                                                        echo checkbox_tag('pilihaction[]', $rs3->getInt('detail_no2'));
                                                    }
                                                    echo '</td>';
                                                } elseif ($sf_user->getNamaUser() == 'bagian_organisasi') {
                                                    echo '<td align="center">';
                                                    if ($rs3->getInt('status_level') == 4 && !$rs3->getBoolean('is_bagian_organisasi_setuju') && $rs3->getInt('detail_no2') != 0) {
                                                        echo checkbox_tag('pilihaction[]', $rs3->getInt('detail_no2'));
                                                    }
                                                    echo '</td>';
                                                } elseif ($sf_user->hasCredential('peneliti')) {
                                                    echo '<td align="center">';
                                                    if ($rs3->getInt('status_level') == 4 && $rs3->getInt('detail_no2') != 0) {
                                                        echo checkbox_tag('pilihaction[]', $rs3->getInt('detail_no2'));
                                                    }
                                                    echo '</td>';
                                                } elseif ($sf_user->hasCredential('pptk')) {
                                                    echo '<td align="center">';
                                                    if ($rs3->getInt('status_level') == 1 && $rs3->getInt('detail_no2') != 0) {
                                                        echo checkbox_tag('pilihaction[]', $rs3->getInt('detail_no2'));
                                                    }
                                                    echo '</td>';
                                                } elseif ($sf_user->hasCredential('pa')) {
                                                    echo '<td align="center">';
                                                    if (($rs3->getInt('status_level') == 3 || $rs3->getInt('status_level') == 5) && ($rs3->getInt('detail_no2') != 0)) {
                                                        echo checkbox_tag('pilihaction[]', $rs3->getInt('detail_no2'));
                                                    }
                                                    echo '</td>';
                                                } elseif ($sf_user->hasCredential('dinas')) {
                                                    echo '<td align="center">';
                                                    if ($rs3->getInt('status_level') == 0 && $rs3->getInt('detail_no2') != 0) {
                                                        echo checkbox_tag('pilihaction[]', $rs3->getInt('detail_no2'));
                                                    }
                                                    echo '</td>';
                                                } elseif (($sf_user->hasCredential('admin_super') || $sf_user->hasCredential('admin')) && $sf_user->getNamaLogin() !== 'tim_shs') {
                                                    echo '<td align="center">';
                                                    if ($rs3->getInt('status_level') == 6)
                                                        echo checkbox_tag('pilihaction[]', $rs3->getInt('detail_no2'), FALSE, array('class' => 'chk_rka'));
                                                    else {
                                                        echo checkbox_tag('pilihaction[]', $rs3->getInt('detail_no2'));
                                                        $sesuatu = true;
                                                    }
                                                    echo '</td>';
                                                }
                                                $total_n += $total1;
                                                echo '</tr>';
                                                $jumlah_harga += $hasil;
                                            }
                                            else if ($rs2->getString('rekening') == '')
                                            {
                                                //kalo kiri gak ada subtitle
                                                echo '<tr bgcolor="#ffffff" valign="top">';
                                                echo '<td colspan="7">&nbsp;</td>';
                                                echo '<td><span style="color:red;">!</span></td>';
                                                if($rs3->getString('volume2') != 0) {
                                                    echo '<td align="left"><span>' . $subnya2 . $rs3->getString('komponen_name2') . ' ' . $rs3->getString('detail_name_rd');
                                                    if ($rs3->getString('detail_satuan2') == 'Paket' && $rs3->getString('volume2') == '1' && $rs3->getString('status_lelang') == 'lock') {
                                                        echo ' <span class="badge badge-primary">PSP</span>';
                                                    }
                                                    if ($rs3->getBoolean('is_hpsp') == TRUE) {
                                                        echo ' <span class="badge badge-danger">HPSP</span>';
                                                    }
                                                    if ($rs3->getBoolean('is_output') == TRUE) {
                                                        echo ' <span class="badge badge-info">Output</span>';
                                                    }
                                                    if ($rs3->getBoolean('is_musrenbang') == TRUE) {
                                                        echo ' <span class="badge badge-success">Musrenbang</span>';
                                                    }
                                                    if ($rs3->getBoolean('prioritas_wali') == TRUE) {
                                                        echo ' <span class="badge badge-danger">Prioritas</span>';
                                                    }
                                                    if($rs3->getBoolean('is_covid') == TRUE) {
                                                        echo ' <span class="badge badge-danger">Covid-19</span>';
                                                    }
                                                    if($rs3->getString('sumber_dana_id') != 11) {
                                                                    $c = new Criteria();
                                                                    $c->add(MasterSumberDanaPeer::ID, $rs3->getString('sumber_dana_id'));
                                                                    $c->addAscendingOrderByColumn(MasterSumberDanaPeer::SUMBER_DANA);
                                                                    $v = MasterSumberDanaPeer::doSelectOne($c);
                                                                    echo ' <span class="badge badge-primary">'. $v->getSumberDana() .'</span>';
                                                                }
                                                } else echo '<td align="left"><span></span></td>';
                                                echo '</span></td>';
                                                if($rs3->getString('volume2') != 0)
                                                    echo '<td align="center" nowrap="nowrap"><span>' . $rs3->getString('detail_satuan2') . '</span></td>';
                                                else
                                                    echo '<td align="center" nowrap="nowrap"><span></span></td>';
                                                echo '<td align="center"><span>' . $rs3->getString('keterangan_koefisien2') . '</span></td>';
                                                if($rs3->getString('volume2') != 0) {
                                                    echo '<td align="right" nowrap="nowrap">';
                                                    if ($rs3->getString('detail_satuan2') == '%') 
                                                    {
                                                        $len = strlen(substr(strrchr($rs3->getString('detail_harga2'), "."), 1));
                                                        echo number_format($rs3->getFloat('detail_harga2'), $len, ',', '.');
                                                    } else {
                                                        echo number_format($rs3->getFloat('detail_harga2'), 0, ',', '.');
                                                    }
                                                    echo '</td>';
                                                } else
                                                    echo '<td align="right" nowrap="nowrap">0</td>';
                                                $hasil = myTools::round_ganjil_genap($rs3->getString('detail_harga2') * $rs3->getString('volume2'));
                                                if($rs3->getString('volume2') != 0)
                                                echo '<td align="right" nowrap="nowrap">' . number_format($hasil, 0, ',', '.') . '</td>';
                                                else
                                                    echo '<td align="right" nowrap="nowrap">0</td>';
                                                if($rs3->getString('volume2') != 0)
                                                    echo '<td align="right" nowrap="nowrap">' . $rs3->getString('pajak2') . '%</td>';
                                                else
                                                    echo '<td align="right" nowrap="nowrap">0%</td>';
                                                $total1 = $hasil - (($rs3->getString('pajak2') / 100) * $hasil);
                                                $kanan = $rs3->getString('hasil_kali2');
                                                $selisih = $kanan - $kiri;
                                                echo '<td align="right" nowrap="nowrap">' . number_format($rs3->getString('hasil_kali2'), 0, ',', '.') . '</td>';
                                                if($selisih >= 0)
                                                    echo '<td align="right" nowrap="nowrap">' . number_format($selisih, 0, ',', '.') . '</td>';
                                                else
                                                    echo '<td align="right" nowrap="nowrap">(' . number_format(abs($selisih), 0, ',', '.') . ')</td>';
                                                if(sfConfig::get('app_tahap_edit') != 'murni'):
                                                    echo '<td align="left">';
                                                    echo (strlen(trim($rs3->getString('note_skpd'))) > 0 ? '<strong>SKPD:</strong> ' . $rs3->getString('note_skpd') . '<br>' : '');
                                                    echo (strlen(trim($rs3->getString('note_peneliti'))) > 0 ? '<strong>Administrasi Pembangunan:</strong> ' . $rs3->getString('note_peneliti') . '<br>' : '');
                                                    echo (strlen(trim($rs3->getString('note_tapd'))) > 0 ? '<strong>BPKPD:</strong> ' . $rs3->getString('note_tapd') . '<br>' : '');
                                                    echo (strlen(trim($rs3->getString('note_bappeko'))) > 0 ? '<strong>Bappeko:</strong> ' . $rs3->getString('note_bappeko') . '<br>' : '');
                                                    echo '</td>';
                                                endif;
                                                echo '<td align="center">' . $status_posisi . '</td>';
                                                echo '<td align="center"><p style="color:' . $warna_status . '">' . $status_komponen . '</p></td>';
                                                if ($rs3->getInt('detail_no2') == 0 && $rs3->getInt('status_level') == 0) {
                                                    echo '<td align="center">';
                                                    echo image_tag('centang', 'title="komponen mengalami perubahan rekening/subtitle, silahkan centang komponen yang baru"');
                                                    echo '</td>';
                                                } elseif ($sf_user->hasCredential('kpa')) {
                                                    echo '<td align="center">';
                                                    if ($rs3->getInt('status_level') == 2 && $rs3->getInt('detail_no2') != 0) {
                                                        echo checkbox_tag('pilihaction[]', $rs3->getInt('detail_no2'));
                                                    }
                                                    echo '</td>';
                                                } elseif ($sf_user->getNamaUser() == 'anggaran') {
                                                    echo '<td align="center">';
                                                    if ($rs3->getInt('status_level') == 4 && !$rs3->getBoolean('is_tapd_setuju') && $rs3->getInt('detail_no2') != 0) {
                                                        // echo checkbox_tag('pilihaction[]', $rs3->getInt('detail_no2'));
                                                    }
                                                    echo '</td>';
                                                } elseif ($sf_user->hasCredential('bappeko')) {
                                                    echo '<td align="center">';
                                                    if ($rs3->getInt('status_level') == 4 && !$rs3->getBoolean('is_bappeko_setuju') && $rs3->getInt('detail_no2') != 0) {
                                                        echo checkbox_tag('pilihaction[]', $rs3->getInt('detail_no2'));
                                                    }
                                                    echo '</td>';
                                                } elseif ($sf_user->getNamaUser() == 'bagian_hukum') {
                                                    echo '<td align="center">';
                                                    if ($rs3->getInt('status_level') == 4 && !$rs3->getBoolean('is_bagian_hukum_setuju') && $rs3->getInt('detail_no2') != 0) {
                                                        echo checkbox_tag('pilihaction[]', $rs3->getInt('detail_no2'));
                                                    }
                                                    echo '</td>';
                                                } elseif ($sf_user->getNamaUser() == 'inspektorat') {
                                                    echo '<td align="center">';
                                                    if ($rs3->getInt('status_level') == 4 && !$rs3->getBoolean('is_inspektorat_setuju') && $rs3->getInt('detail_no2') != 0) {
                                                        echo checkbox_tag('pilihaction[]', $rs3->getInt('detail_no2'));
                                                    }
                                                    echo '</td>';
                                                } elseif ($sf_user->getNamaUser() == 'bkd' || $sf_user->getNamaUser() == 'bkpsdm') {
                                                    echo '<td align="center">';
                                                    if ($rs3->getInt('status_level') == 4 && !$rs3->getBoolean('is_badan_kepegawaian_setuju') && $rs3->getInt('detail_no2') != 0) {
                                                        echo checkbox_tag('pilihaction[]', $rs3->getInt('detail_no2'));
                                                    }
                                                    echo '</td>';
                                                } elseif ($sf_user->getNamaUser() == 'lppa' || $sf_user->getNamaUser() == 'bapenda') {
                                                    echo '<td align="center">';
                                                    if ($rs3->getInt('status_level') == 4 && !$rs3->getBoolean('is_lppa_setuju') && $rs3->getInt('detail_no2') != 0) {
                                                        echo checkbox_tag('pilihaction[]', $rs3->getInt('detail_no2'));
                                                    }
                                                    echo '</td>';
                                                } elseif ($sf_user->getNamaUser() == 'bagian_organisasi') {
                                                    echo '<td align="center">';
                                                    if ($rs3->getInt('status_level') == 4 && !$rs3->getBoolean('is_bagian_organisasi_setuju') && $rs3->getInt('detail_no2') != 0) {
                                                        echo checkbox_tag('pilihaction[]', $rs3->getInt('detail_no2'));
                                                    }
                                                    echo '</td>';
                                                } elseif ($sf_user->hasCredential('peneliti')) {
                                                    echo '<td align="center">';
                                                    if ($rs3->getInt('status_level') == 4 && $rs3->getInt('detail_no2') != 0) {
                                                        echo checkbox_tag('pilihaction[]', $rs3->getInt('detail_no2'));
                                                    }
                                                    echo '</td>';
                                                } elseif ($sf_user->hasCredential('pptk')) {
                                                    echo '<td align="center">';
                                                    if ($rs3->getInt('status_level') == 1 && $rs3->getInt('detail_no2') != 0) {
                                                        echo checkbox_tag('pilihaction[]', $rs3->getInt('detail_no2'));
                                                    }
                                                    echo '</td>';
                                                } elseif ($sf_user->hasCredential('pa')) {
                                                    echo '<td align="center">';
                                                    if (($rs3->getInt('status_level') == 3 || $rs3->getInt('status_level') == 5) && ($rs3->getInt('detail_no2') != 0)) {
                                                        echo checkbox_tag('pilihaction[]', $rs3->getInt('detail_no2'));
                                                    }
                                                    echo '</td>';
                                                } elseif ($sf_user->hasCredential('dinas')) {
                                                    echo '<td align="center">';
                                                    if ($rs3->getInt('status_level') == 0 && $rs3->getInt('detail_no2') != 0) {
                                                        echo checkbox_tag('pilihaction[]', $rs3->getInt('detail_no2'));
                                                    }
                                                    echo '</td>';
                                                } elseif (($sf_user->hasCredential('admin_super') || $sf_user->hasCredential('admin')) && $sf_user->getNamaLogin() !== 'tim_shs') {
                                                    echo '<td align="center">';
                                                    if ($rs3->getInt('status_level') == 6)
                                                        echo checkbox_tag('pilihaction[]', $rs3->getInt('detail_no2'), FALSE, array('class' => 'chk_rka'));
                                                    else {
                                                        echo checkbox_tag('pilihaction[]', $rs3->getInt('detail_no2'));
                                                        $sesuatu = true;
                                                    }
                                                    echo '</td>';
                                                }
                                                $total_n += $total1;
                                                echo '</tr>';
                                                $jumlah_harga += $hasil;//kalo kiri gak ada subtitle
                                            }
                                        }
                                        $query = "select sum(rd.nilai_anggaran) as nilai
                                        from " . sfConfig::get('app_default_schema') . ".$tabel_semula rd where unit_id='$unit_id' and
                                        kegiatan_code='$kode_kegiatan' and rekening_code = '$kode_rekening' and
                                        subtitle = '$subtitle' and
                                        rd.status_hapus=false and
                                        detail_no in (select drd.detail_no
                                        from " . sfConfig::get('app_default_schema') . ".$tabel_semula rd
                                        left join " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail drd
                                        on rd.unit_id=drd.unit_id and rd.kegiatan_code=drd.kegiatan_code and rd.detail_no=drd.detail_no
                                        where rd.unit_id='$unit_id' and rd.kegiatan_code='$kode_kegiatan' and rd.status_hapus=false and
                                        rd.rekening_code = '$kode_rekening' and drd.status_level >= 0 and
                                        rd.subtitle = '$subtitle'and
                                        (drd.rekening_code<>rd.rekening_code or drd.subtitle<>rd.subtitle or drd.nilai_anggaran<>rd.nilai_anggaran or drd.is_covid<>rd.is_covid or
                                        drd.komponen_name<>rd.komponen_name or drd.keterangan_koefisien<>rd.keterangan_koefisien or
                                        drd.detail_name<>rd.detail_name or drd.status_hapus<>rd.status_hapus))";
                                        $con = Propel::getConnection();
                                        $stmt = $con->prepareStatement($query);
                                        $rs_harga_subtitle = $stmt->executeQuery();
                                        while ($rs_harga_subtitle->next()) {
                                            $total_subtitle_semula = $rs_harga_subtitle->getString('nilai');
                                        }

                                        $query = "select sum(rd.nilai_anggaran) as nilai
                                        from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail rd
                                        where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rekening_code = '$kode_rekening' and
                                        subtitle = '$subtitle' and
                                        rd.status_hapus=false and status_level >= 0 and
                                        (detail_no in (select drd.detail_no
                                        from " . sfConfig::get('app_default_schema') . ".$tabel_semula rd
                                        left join " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail drd
                                        on rd.unit_id=drd.unit_id and rd.kegiatan_code=drd.kegiatan_code and rd.detail_no=drd.detail_no
                                        where rd.unit_id='$unit_id' and rd.kegiatan_code='$kode_kegiatan' and rd.status_hapus=false and
                                        drd.status_hapus=false and rd.rekening_code = '$kode_rekening' and drd.status_level >= 0 and
                                        rd.subtitle = '$subtitle'and
                                        (drd.rekening_code<>rd.rekening_code or drd.subtitle<>rd.subtitle or drd.nilai_anggaran<>rd.nilai_anggaran or drd.is_covid<>rd.is_covid or
                                        drd.komponen_name<>rd.komponen_name or drd.keterangan_koefisien<>rd.keterangan_koefisien or
                                        drd.detail_name<>rd.detail_name)) or
                                        detail_no not in (select detail_no as nilai
                                        from " . sfConfig::get('app_default_schema') . ".$tabel_semula drd
                                        where drd.unit_id='$unit_id' and drd.kegiatan_code='$kode_kegiatan' and drd.status_hapus=false and
                                        rekening_code = '$kode_rekening' and
                                        subtitle = '$subtitle'))";
                                        $con = Propel::getConnection();
                                        $stmt = $con->prepareStatement($query);
                                        $rs_harga_subtitle = $stmt->executeQuery();
                                        while ($rs_harga_subtitle->next()) {
                                            $total_subtitle_menjadi = $rs_harga_subtitle->getString('nilai');
                                        }
                                        echo '<tr bgcolor="#ffffff">';
                                        if ($tahap != 'murni') {
                                            echo '<td colspan="6" align="right">';
                                            echo 'Total ' . $rekening_prev . ' :</td>';
                                            echo '<td align="right" nowrap="nowrap">' . number_format($total_subtitle_semula, 0, ',', '.') . '</td>';
                                            echo '<td> </td>';
                                        }
                                        echo '<td colspan="6" align="right">';
                                        echo 'Total ' . $rekening . ' :</td>';
                                        echo '<td align="right" nowrap="nowrap">' . number_format($total_subtitle_menjadi, 0, ',', '.') . '</td>';
                                        echo '<td colspan="5"></td>';
                                        echo '</tr>';
                                    }
                                    $total_semua = $total_semua + $total;
                                    $total_semua_n += $total_n;
                                }
                                $query = "select sum(rd.nilai_anggaran) as nilai from " . sfConfig::get('app_default_schema') . ".$tabel_semula rd
                                where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rekening_code like '$kode_belanja%' and rd.status_hapus=false "
                                . " and detail_no in (select drd.detail_no as nilai
                                from " . sfConfig::get('app_default_schema') . ".$tabel_semula rd
                                left join " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail drd on rd.unit_id=drd.unit_id and rd.kegiatan_code=drd.kegiatan_code and
                                rd.detail_no=drd.detail_no
                                where rd.unit_id='$unit_id' and rd.kegiatan_code='$kode_kegiatan' and rd.status_hapus=false and
                                drd.rekening_code like '$kode_belanja%' and drd.status_level >= 0 and
                                (drd.rekening_code<>rd.rekening_code or drd.subtitle<>rd.subtitle or drd.nilai_anggaran<>rd.nilai_anggaran or drd.is_covid<>rd.is_covid or drd.komponen_name<>rd.komponen_name or
                                drd.keterangan_koefisien<>rd.keterangan_koefisien or drd.detail_name<>rd.detail_name or rd.status_hapus<>drd.status_hapus))";
                                $con = Propel::getConnection();
                                $stmt = $con->prepareStatement($query);
                                $rs_harga_subtitle = $stmt->executeQuery();
                                while ($rs_harga_subtitle->next()) {
                                    $total_subtitle_semula = $rs_harga_subtitle->getString('nilai');
                                }
                                $query = "select sum(rd.nilai_anggaran) as nilai
                                from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail rd
                                where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rekening_code like '$kode_belanja%' and
                                rd.status_hapus=false and
                                (detail_no in (select drd.detail_no as nilai
                                from " . sfConfig::get('app_default_schema') . ".$tabel_semula rd
                                left join " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail drd on rd.unit_id=drd.unit_id and rd.kegiatan_code=drd.kegiatan_code and
                                rd.detail_no=drd.detail_no
                                where rd.unit_id='$unit_id' and rd.kegiatan_code='$kode_kegiatan' and rd.status_hapus=false and
                                drd.rekening_code like '$kode_belanja%' and drd.status_level >= 0 and drd.status_hapus=false and
                                (drd.rekening_code<>rd.rekening_code or drd.subtitle<>rd.subtitle or drd.nilai_anggaran<>rd.nilai_anggaran or drd.is_covid<>rd.is_covid or drd.komponen_name<>rd.komponen_name or
                                drd.keterangan_koefisien<>rd.keterangan_koefisien or drd.detail_name<>rd.detail_name)) and rd.status_level >= 0
                                or
                                detail_no not in (select detail_no as nilai
                                from " . sfConfig::get('app_default_schema') . ".$tabel_semula drd
                                where drd.unit_id='$unit_id' and drd.kegiatan_code='$kode_kegiatan' and drd.rekening_code like '$kode_belanja%' and
                                drd.status_hapus=false))";
                                $con = Propel::getConnection();
                                $stmt = $con->prepareStatement($query);
                                $rs_harga_subtitle = $stmt->executeQuery();
                                while ($rs_harga_subtitle->next()) {
                                    $total_subtitle_menjadi = $rs_harga_subtitle->getString('nilai');
                                }
                                echo '<tr bgcolor="#ffffff">';
                                if ($tahap != 'murni') {
                                    echo '<td colspan="6" align="right">';
                                    echo 'Total ' . $nama_belanja . ' :</td>';
                                    echo '<td align="right" nowrap="nowrap">' . number_format($total_subtitle_semula, 0, ',', '.') . '</td>';
                                    echo '<td> </td>';
                                }
                                echo '<td colspan="6" align="right">';
                                echo 'Total ' . $nama_belanja . ' :</td>';
                                echo '<td align="right" nowrap="nowrap">' . number_format($total_subtitle_menjadi, 0, ',', '.') . '</td>';
                                echo '<td colspan="5"></td>';
                                echo '</tr>';
                                $total_belanja_kiri += $total_subtitle_semula;
                                $total_belanja_kanan += $total_subtitle_menjadi;
                            }
                            echo '<tr bgcolor="#e6e6e6">';
                            if ($tahap != 'murni') {
                                echo '<td colspan="6" align="right"><strong>';
                                echo 'Grand Total :</strong></td>';
                                echo '<td align="right" nowrap="nowrap"><strong>' . number_format($total_belanja_kiri, 0, ',', '.') . '</strong></td>';
                                echo '<td> </td>';
                            }
                            echo '<td colspan="6" align="right"><strong>';
                            echo 'Grand Total :</strong></td>';
                            echo '<td align="right" nowrap="nowrap"><strong>' . number_format($total_belanja_kanan, 0, ',', '.') . '</strong></td>';
                            echo '<td colspan="5"></td>';
                            echo '</tr>';
                            ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="card-body">
                        <p>Telah mendapatkan persetujuan oleh:</p>
                        <?php
                        $c = new Criteria();
                        $c->add(LogApprovalPeer::UNIT_ID, $unit_id);
                        $c->add(LogApprovalPeer::KEGIATAN_CODE, $kode_kegiatan);
                        $c->add(LogApprovalPeer::TAHAP, $tahap);
                        $c->add(LogApprovalPeer::SEBAGAI, 'Entri', Criteria::NOT_EQUAL);
                        $c->add(LogApprovalPeer::SEBAGAI, 'Admin (RKA)', Criteria::NOT_EQUAL);
                        $c->addDescendingOrderByColumn(LogApprovalPeer::WAKTU);
                        $c->setDistinct();
                        $rs_log = LogApprovalPeer::doSelect($c);
                        $pertama = true;
                        foreach ($rs_log as $log):
                            $user_id = $log->getUserId();
                            $c = new Criteria();
                            $c->add(MasterUserV2Peer::USER_ID, $user_id);
                            if ($rs_nama = MasterUserV2Peer::doSelectOne($c)) {
                                $nama = $rs_nama->getUserName();
                                $jabatan = $rs_nama->getJabatan();
                                $arr_sebagai = explode('|', $log->getSebagai());
                                if ($arr_sebagai[1] == 'kembali') {
                                    echo $nama . ' (' . $jabatan . ') sebagai ' . $arr_sebagai[0] . ' telah mengembalikan ke Entri, dengan alasan: ' . $log->getCatatan() . '<br> ';
                                } else {
                                    if($log->getSebagai() == 'Entri')
                                        echo  $log->getUserId(). ' sebagai ' . $log->getSebagai() . '<br> ';
                                    else
                                        echo $nama . ' (' . $jabatan . ') sebagai ' . $log->getSebagai() . '<br> ';
                                }
                            }
                        endforeach;

                        $tahap_angka = DinasMasterKegiatanPeer::getTahapKegiatan($unit_id, $kode_kegiatan);
                        $c_rincian_bappeko = new Criteria();
                        $c_rincian_bappeko->add(RincianBappekoPeer::UNIT_ID, $unit_id);
                        $c_rincian_bappeko->add(RincianBappekoPeer::KODE_KEGIATAN, $kode_kegiatan);
                        $c_rincian_bappeko->add(RincianBappekoPeer::TAHAP, $tahap_angka);
                        if ($rs_rincian_bappeko = RincianBappekoPeer::doSelectOne($c_rincian_bappeko))
                        {
                            $status_buka_bappeko = FALSE;
                            if (!is_null($rs_rincian_bappeko->getJawaban1Dinas()) || !is_null($rs_rincian_bappeko->getJawaban2Dinas())) {
                                echo '<br><b>Perangkat Daerah / Unit Kerja</b> : ';
                                $alasan_dinas = strip_tags($rs_rincian_bappeko->getCatatanDinas());
                                if ($rs_rincian_bappeko->getJawaban1Dinas() && $rs_rincian_bappeko->getJawaban2Dinas()) {
                                    echo 'mengubah output dan subtitle, dengan alasan ' . $alasan_dinas;
                                    $status_buka_bappeko = TRUE;
                                } elseif ($rs_rincian_bappeko->getJawaban1Dinas()) {
                                    echo 'mengubah subtitle, dengan alasan ' . $alasan_dinas;
                                    $status_buka_bappeko = TRUE;
                                } elseif ($rs_rincian_bappeko->getJawaban2Dinas()) {
                                    echo 'mengubah subtitle, dengan alasan ' . $alasan_dinas;
                                    $status_buka_bappeko = TRUE;
                                } else {
                                    echo 'tidak mengubah output dan subtitle';
                                }
                            }
                            if (!is_null($rs_rincian_bappeko->getStatusBuka()) && $status_buka_bappeko) {
                                if ($rs_rincian_bappeko->getStatusBuka()) {
                                    echo '<br><b>Kegiatan telah mendapat persetujuan Bappeko</b>';
                                } else {
                                    echo '<br><b>Kegiatan tidak mendapat persetujuan Bappeko, karena ' . $rs_rincian_bappeko->getCatatanBappeko() . '</b>';
                                }
                            }
                            if (!is_null($rs_rincian_bappeko->getJawaban1Bappeko()) && !is_null($rs_rincian_bappeko->getJawaban2Bappeko())) {
                                echo '<br><b>Bappeko</b> : ';
                                if ($rs_rincian_bappeko->getJawaban1Bappeko() && $rs_rincian_bappeko->getJawaban2Bappeko()) {
                                    echo 'mengubah output dan subtitle';
                                } elseif ($rs_rincian_bappeko->getJawaban1Bappeko()) {
                                    echo 'mengubah subtitle';
                                } elseif ($rs_rincian_bappeko->getJawaban2Bappeko()) {
                                    echo 'mengubah subtitle';
                                } else {
                                    echo 'tidak mengubah output dan subtitle';
                                }
                            }
                        }
                        ?>
                        <br>
                        <?php
                        $posisi_kegiatan = '';
                        if ($level_kegiatan == 0) {
                            $posisi_kegiatan = 'Entri';
                        } elseif ($level_kegiatan == 1) {
                            $posisi_kegiatan = 'PPTK';
                        } elseif ($level_kegiatan == 2) {
                            $posisi_kegiatan = 'KPA';
                        } elseif ($level_kegiatan == 3) {
                            $posisi_kegiatan = 'PA I';
                        } elseif ($level_kegiatan == 4) {
                            $penyetuju = array();
                            if($kegiatan_is_tapd_setuju)
                                array_push($penyetuju, 'BPKAD');
                            if($kegiatan_is_bappeko_setuju)
                                array_push($penyetuju, 'Bappeda');
                            if($kegiatan_is_penyelia_setuju)
                                array_push($penyetuju, 'BPBJAP');
                            if($kegiatan_is_bagian_hukum_setuju)
                                array_push($penyetuju, 'Bag. Hukum');
                            if($kegiatan_is_inspektorat_setuju)
                                array_push($penyetuju, 'Inspektorat');
                            if($kegiatan_is_badan_kepegawaian_setuju)
                                array_push($penyetuju, 'BKPSDM');
                            if($kegiatan_is_lppa_setuju)
                                // array_push($penyetuju, 'Bag. LPPA');
                                array_push($penyetuju, 'Bapenda');
                            if($kegiatan_is_bagian_organisasi_setuju)
                                array_push($penyetuju, 'Bag. Organisasi');

                            $strPenyetuju = implode(', ', $penyetuju);
                            $posisi_kegiatan = 'Tim Anggaran ('.$strPenyetuju.')';
                        } elseif ($level_kegiatan == 5) {
                            $posisi_kegiatan = 'PA II';
                        } elseif ($level_kegiatan == 6) {
                            $posisi_kegiatan = 'Penyelia II';
                        } elseif ($level_kegiatan == 7) {
                            $posisi_kegiatan = 'RKA';
                        } else
                        $posisi_kegiatan = 'salah posisi';

                        $cek_max_level = TRUE;
                        $c_level = new Criteria();
                        $c_level->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
                        $c_level->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
                        $c_level->add(DinasRincianDetailPeer::STATUS_LEVEL, 4, Criteria::GREATER_THAN);
                        if (DinasRincianDetailPeer::doSelect($c_level))
                            $cek_max_level = FALSE;

                        if ($sf_user->hasCredential('dinas'))
                        {
                        ?>
                            <p>
                                Catatan Perangkat Daerah / Unit Kerja (Posisi=<?php echo $posisi_kegiatan ?>) :
                                <?php echo link_to_function('Generate', 'generateDinas()', array('class' => 'btn btn-xs btn-outline-info')); ?>
                            </p>
                            <p id="generate-dinas"></p>
                            <textarea id="catatan" name="catatan" placeholder="Isikan catatan" class="form-control"
                                rows="3"><?php echo $catatan; ?></textarea>
                            <?php use_javascript('/AdminLTE/plugins/tinymce/tinymce.min.js') ?>
                            <script>
                                tinymce.init({
                                    selector: 'textarea#catatan',
                                    menubar: 'edit view format',
                                    plugins: "paste",
                                    paste_as_text: true
                                });
                                function generateDinas() {
                                    $('#generate-dinas').html(
                                        'Pergeseran antar kode rekening dari semula: <?php echo $catatan_rekening_1 ?> dialihkan sebagian menjadi: <?php echo $catatan_rekening_2 ?>'
                                        );
                                }
                            </script>
                        <?php
                        }
                        else
                        {
                        ?>
                            <p>Catatan Perangkat Daerah / Unit Kerja (Posisi=<?php echo $posisi_kegiatan ?>) :</p>
                            <table class="table table-bordered">
                                <tr>
                                    <td><?php echo $catatan; ?></td>
                                </tr>
                            </table>
                        <?php
                        }
                        $is_penyelia_bpkad = false;
                        $penyelia_bpkad = new Criteria();
                        $penyelia_bpkad->add(PenyeliaBpkadPeer::NAMA, $sf_user->getNamaLogin(), Criteria::EQUAL);
                        if ($rs_penyelia = PenyeliaBpkadPeer::doSelectOne($penyelia_bpkad)) {
                            $is_penyelia_bpkad = true;
                        }

                        // $penyelia_bpkad = array('winda','judi','tutik','febri','wike','lucky');
                        if (($is_penyelia_bpkad == true || $sf_user->getNamaUser() == 'anggaran') && (sfConfig::get('app_fasilitas_editNoteTimAnggaranSetelahApprove') == 'buka' || $cek_max_level))
                        {
                        ?>
                            <p>
                                Catatan Pembahasan :
                                <?php echo link_to_function('Generate', 'generatePembahasan()', array('class' => 'btn btn-xs btn-outline-info')); ?>
                            </p>
                            <p id="generate-pembahasan"></p>
                            <textarea id="catatan_pembahasan" name="catatan" placeholder="Isikan catatan" class="form-control" rows="9">
                                <?php
                                    if(!is_null($catatan_ubah_f1_dinas) && !is_null($ubah_f1_dinas) && $ubah_f1_dinas) {
                                        echo $catatan_ubah_f1_dinas.' '.$catatan_pembahasan;
                                    } else if(!is_null($catatan_ubah_f1_dinas)){
                                        echo $catatan_pembahasan;
                                    } else {
                                        echo $catatan;
                                    }
                                ?>
                            </textarea>
                            <?php
                                if(empty($arr_catatan_rekening_3)){
                                    $template_catatan = '1. Pergeseran antar kode rekening dari <b>semula</b><br>';
                                    foreach($arr_catatan_rekening_1 as $catatan1){
                                        $template_catatan .= '- '.$catatan1.' <br>';
                                    }
                                    $template_catatan .= '<b>dialihkan menjadi</b><br>';
                                    foreach($arr_catatan_rekening_2 as $catatan2){
                                        $template_catatan .= '- '.$catatan2.' <br>';
                                    }
                                    $template_catatan .= '<b>untuk </b><br>';
                                }else{
                                    if(empty($arr_catatan_rekening_1) && empty($arr_catatan_rekening_2)){
                                        $template_catatan = '1. Penyesuaian <b>komponen</b> pada '.$catatan_rekening_3.' <b>untuk</b>';
                                    }else{
                                        $template_catatan = '1. Pergeseran antar kode rekening dari <b>semula</b><br>';
                                        foreach($arr_catatan_rekening_1 as $catatan1){
                                            $template_catatan .= '- '.$catatan1.' <br>';
                                        }
                                        $template_catatan .= '<b>dialihkan menjadi</b><br>';
                                        foreach($arr_catatan_rekening_2 as $catatan2){
                                            $template_catatan .= '- '.$catatan2.' <br>';
                                        }
                                        $template_catatan .= '<b>untuk </b><br><br>';
                                        $template_catatan .= '2. Penyesuaian <b>komponen</b> pada '.$catatan_rekening_3.' <b>untuk</b>';
                                    }
                                }
                            ?>
                            <?php use_javascript('/AdminLTE/plugins/tinymce/tinymce.min.js') ?>
                            <script>
                                tinymce.init({
                                    selector: 'textarea#catatan_pembahasan',
                                    menubar: 'edit view format',
                                    plugins: "paste",
                                    paste_as_text: true
                                });

                                function generatePembahasan() {
                                    // $('#generate-pembahasan').html(
                                    //     'Pergeseran antar kode rekening dari semula: <?php //echo $catatan_rekening_1 ?> dialihkan sebagian menjadi: <?php //echo $catatan_rekening_2 ?>'
                                    //     );
                                    $('#generate-pembahasan').html('<?php echo $template_catatan ?>');
                                }
                            </script>
                        <?php
                        }
                        else
                        {
                        ?>
                            <p>Catatan Pembahasan :</p>
                            <table class="table table-bordered">
                                <tr>
                                    <td>
                                        <?php
                                        if(!is_null($catatan_ubah_f1_dinas) && !is_null($ubah_f1_dinas) && $ubah_f1_dinas) {
                                            echo $catatan_ubah_f1_dinas.' '.$catatan_pembahasan;
                                        } else if(!is_null($catatan_ubah_f1_dinas)){
                                            echo $catatan_pembahasan;
                                        } else {
                                            echo $catatan;
                                        }
                                        ?>
                                    </td>
                                </tr>
                            </table>
                        <?php
                        }
                        if (!$sf_user->getNamaLogin() == 'sekdasby' && !$sf_user->hasCredential('dinas') && !($sf_user->hasCredential('admin_super') || $sf_user->hasCredential('admin'))) 
                        {
                            $user_id = $sf_user->getNamaLogin();
                            $c = new Criteria();
                            $c->add(MasterUserV2Peer::USER_ID, $user_id);
                            $rs_user = MasterUserV2Peer::doSelectOne($c);
                            $user_name = $rs_user->getUserName();
                            $user_jabatan = $rs_user->getJabatan();
                            $user_nip = $rs_user->getNip();
                            $c = new Criteria();
                            $c->add(SchemaAksesV2Peer::USER_ID, $user_id);
                            $rs_level = SchemaAksesV2Peer::doSelectOne($c);
                            $user_level = '';
                            $level_id = $rs_level->getLevelId();
                            if ($level_id == 12) {
                                $user_level = 'Tim Anggaran';
                            } else if ($level_id == 13) {
                                $user_level = 'PPTK';
                            } else if ($level_id == 14) {
                                $user_level = 'KPA/PPKM';
                            } else if ($level_id == 15) {
                                $user_level = 'PA';
                            } else if ($level_id == 16) {
                                $user_level = 'BAPPEKO';
                            }
                            ?>
                            <p>Dengan ini data telah disetujui oleh
                                <strong><?php echo $user_name . ' (' . $user_nip . ' - ' . $user_jabatan . ')' ?></strong>, selaku
                                <?php echo $user_level ?>
                            </p>
                        <?php 
                        } 
                        echo "<br/><div class='text-right'>";
                        if ($sf_user->hasCredential('dinas')) {
                            $c_pptk = new Criteria();
                            $c_pptk->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
                            $c_pptk->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
                            $c_pptk->add(DinasRincianDetailPeer::STATUS_LEVEL, 0);
                            $c_pptk->add(DinasRincianDetailPeer::STATUS_HAPUS, FALSE);
                            $c_pptk->add(DinasRincianDetailPeer::NOTE_SKPD, '', Criteria::NOT_EQUAL);
                            $c_kegiatan = new Criteria();
                            $c_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                            $c_kegiatan->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                            $c_kegiatan->add(DinasMasterKegiatanPeer::STATUS_LEVEL, 0);
                            $c_rincian = new Criteria();
                            $c_rincian->add(DinasRincianPeer::UNIT_ID, $unit_id);
                            $c_rincian->add(DinasRincianPeer::KEGIATAN_CODE, $kode_kegiatan);
                            $c_rincian->add(DinasRincianPeer::LOCK, FALSE);
                            if ((DinasRincianDetailPeer::doSelectOne($c_pptk) || DinasRincianDetailPeer::doSelectOne($c_kegiatan)) && DinasRincianPeer::doSelectOne($c_rincian))
                                echo submit_tag('Proses ke PPTK', array('name' => 'proses', 'class' => 'btn btn-outline-primary'));
                        } elseif ($sf_user->hasCredential('pptk')) {
                            $c_anggaran = new Criteria();
                            $c_anggaran->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
                            $c_anggaran->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
                            $c_anggaran->add(DinasRincianDetailPeer::STATUS_LEVEL, 1);
                            $c_anggaran->add(DinasRincianDetailPeer::STATUS_LEVEL_TOLAK, 4);
                            $c_anggaran->add(DinasRincianDetailPeer::STATUS_HAPUS, FALSE);
                            $c_rd = new Criteria();
                            $c_rd->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
                            $c_rd->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
                            $c_rd->add(DinasRincianDetailPeer::STATUS_LEVEL, 1);
                            $c_rd->add(DinasRincianDetailPeer::STATUS_HAPUS, FALSE);
                            $c_rd->add(DinasRincianDetailPeer::NOTE_SKPD, '', Criteria::NOT_EQUAL);
                            $c_kegiatan = new Criteria();
                            $c_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                            $c_kegiatan->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                            $c_kegiatan->add(DinasMasterKegiatanPeer::STATUS_LEVEL, 1);
                            if (DinasRincianDetailPeer::doSelectOne($c_rd) || DinasRincianDetailPeer::doSelectOne($c_kegiatan)) {
                                $unit_tanpa_kpa = array('');
                                if(in_array($unit_id, $unit_tanpa_kpa)){
                                    if(DinasRincianDetailPeer::doSelectOne($c_anggaran)){
                                        echo submit_tag('Proses ke Tim Anggaran', array('name' => 'proses', 'class' => 'btn btn-outline-primary')) . '&nbsp;';
                                    }else{
                                        echo submit_tag('Proses ke PA', array('name' => 'proses', 'class' => 'btn btn-outline-primary')) . '&nbsp;';
                                    }
                                }else{
                                    echo submit_tag('Proses ke KPA', array('name' => 'proses', 'class' => 'btn btn-outline-primary')) . '&nbsp;';
                                }
                                echo '<button type="button" class="btn btn-outline-danger" data-toggle="modal" data-target="#myModal">Kembalikan ke entri</button>';
                            }
                        } elseif ($sf_user->hasCredential('kpa')) {
                            $c_anggaran = new Criteria();
                            $c_anggaran->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
                            $c_anggaran->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
                            $c_anggaran->add(DinasRincianDetailPeer::STATUS_LEVEL, 2);
                            $c_anggaran->add(DinasRincianDetailPeer::STATUS_LEVEL_TOLAK, 4);
                            $c_anggaran->add(DinasRincianDetailPeer::STATUS_HAPUS, FALSE);
                            $c_pa = new Criteria();
                            $c_pa->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
                            $c_pa->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
                            $c_pa->add(DinasRincianDetailPeer::STATUS_LEVEL, 2);
                            $c_pa->add(DinasRincianDetailPeer::STATUS_HAPUS, FALSE);
                            $c_kegiatan = new Criteria();
                            $c_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                            $c_kegiatan->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                            $c_kegiatan->add(DinasMasterKegiatanPeer::STATUS_LEVEL, 2);
                            if (DinasRincianDetailPeer::doSelectOne($c_anggaran)) {
                                echo submit_tag('Proses ke Tim Anggaran', array('name' => 'proses', 'class' => 'btn btn-outline-primary')) . '&nbsp;';
                                echo '<button type="button" class="btn btn-outline-danger" data-toggle="modal" data-target="#myModal">Kembalikan ke entri</button>';
                            } elseif (DinasRincianDetailPeer::doSelectOne($c_pa) || DinasRincianDetailPeer::doSelectOne($c_kegiatan)) {
                                echo submit_tag('Proses ke PA', array('name' => 'proses', 'class' => 'btn btn-outline-primary')) . '&nbsp;';
                                echo '<button type="button" class="btn btn-outline-danger" data-toggle="modal" data-target="#myModal">Kembalikan ke entri</button>';
                            }
                        } elseif ($sf_user->hasCredential('pa')) {
                            $c_anggaran = new Criteria();
                            $c_anggaran->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
                            $c_anggaran->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
                            $c_anggaran->add(DinasRincianDetailPeer::STATUS_LEVEL, 3);
                            $c_anggaran->add(DinasRincianDetailPeer::STATUS_HAPUS, FALSE);
                            $c_kegiatan_anggaran = new Criteria();
                            $c_kegiatan_anggaran->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                            $c_kegiatan_anggaran->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                            $c_kegiatan_anggaran->add(DinasMasterKegiatanPeer::STATUS_LEVEL, 3);
                            $c_pa = new Criteria();
                            $c_pa->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
                            $c_pa->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
                            $c_pa->add(DinasRincianDetailPeer::STATUS_LEVEL, 5);
                            $c_pa->add(DinasRincianDetailPeer::STATUS_HAPUS, FALSE);
                            $c_kegiatan_pa = new Criteria();
                            $c_kegiatan_pa->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                            $c_kegiatan_pa->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                            $c_kegiatan_pa->add(DinasMasterKegiatanPeer::STATUS_LEVEL, 5);
                            if (DinasRincianDetailPeer::doSelectOne($c_pa) || DinasMasterKegiatanPeer::doSelectOne($c_kegiatan_pa)) {
                                echo submit_tag('Proses ke RKA', array('name' => 'proses', 'class' => 'btn btn-outline-primary')) . '&nbsp;';
                                echo '<button type="button" class="btn btn-outline-danger" data-toggle="modal" data-target="#myModal">Kembalikan ke entri</button>';
                            } elseif (DinasRincianDetailPeer::doSelectOne($c_anggaran) || DinasMasterKegiatanPeer::doSelectOne($c_kegiatan_anggaran)) {
                                echo submit_tag('Proses ke Tim Anggaran', array('name' => 'proses', 'class' => 'btn btn-outline-primary')) . '&nbsp;';
                                echo '<button type="button" class="btn btn-outline-danger" data-toggle="modal" data-target="#myModal">Kembalikan ke entri</button>';
                            }
                        } elseif ($sf_user->getNamaUser() == 'anggaran' || $sf_user->getNamaUser() == 'bagian_hukum' || $sf_user->getNamaUser() == 'inspektorat' || $sf_user->getNamaUser() == 'bkd' || $sf_user->getNamaUser() == 'bkpsdm' || $sf_user->getNamaUser() == 'lppa' || $sf_user->getNamaUser() == 'bapenda' || $sf_user->getNamaUser() == 'bagian_organisasi' || $sf_user->hasCredential('bappeko')) {

                            if($sf_user->getNamaUser() == 'anggaran'){
                                echo submit_tag('Simpan Catatan Pembahasan', array('name' => 'simpancatatan', 'class' => 'btn btn-outline-primary')) . '&nbsp;';
                            }

                            $c_rd = new Criteria();
                            $c_rd->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
                            $c_rd->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
                            $c_rd->add(DinasRincianDetailPeer::STATUS_LEVEL, 4);
                            $c_rd->add(DinasRincianDetailPeer::STATUS_HAPUS, FALSE);
                            $c_kegiatan_pa = new Criteria();
                            $c_kegiatan_pa->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                            $c_kegiatan_pa->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                            $c_kegiatan_pa->add(DinasMasterKegiatanPeer::STATUS_LEVEL, 4);
                            if (DinasRincianDetailPeer::doSelectOne($c_rd) || DinasMasterKegiatanPeer::doSelectOne($c_kegiatan_pa)) {
                                if($sf_user->getNamaUser() != 'anggaran'){
                                    echo submit_tag('Proses Approval Tim Anggaran', array('name' => 'proses', 'class' => 'btn btn-outline-primary')) . '&nbsp;';
                                    echo '<button type="button" class="btn btn-outline-danger" data-toggle="modal" data-target="#myModal">Kembalikan ke entri</button>';
                                }
                            }
                        } elseif ($sf_user->hasCredential('peneliti')) {
                            // $user_kecuali = array('adam.yulian','adhitiya');
                            $penyelia_bpkad = new Criteria();
                            $penyelia_bpkad->add(PenyeliaBpkadPeer::NAMA, $sf_user->getNamaUser(), Criteria::EQUAL);
                            if ($rs_penyelia = PenyeliaBpkadPeer::doSelectOne($penyelia_bpkad)) {
                                echo submit_tag('Simpan Catatan Pembahasan', array('name' => 'simpancatatan', 'class' => 'btn btn-outline-primary')) . '&nbsp;';
                            }

                            $boleh = false;
                            $c_user_handle = new Criteria();
                            $c_user_handle->add(UserHandleV2Peer::USER_ID, $sf_user->getNamaLogin(), Criteria::EQUAL);
                            $user_handles = UserHandleV2Peer::doSelect($c_user_handle);
                            // $user_kecuali = array('adam.yulian','adhitiya');

                            foreach($user_handles as $user_handle){
                                if($user_handle->getStatusUser() != 'shs'){
                                    $boleh = true;
                                }
                            }
                            if($boleh == true){
                                $c_pa = new Criteria();
                                $c_pa->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
                                $c_pa->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
                                $c_pa->add(DinasRincianDetailPeer::STATUS_LEVEL, 4);
                                $c_pa->add(DinasRincianDetailPeer::STATUS_HAPUS, FALSE);
                                $c_kegiatan = new Criteria();
                                $c_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                                $c_kegiatan->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                                $c_kegiatan->add(DinasMasterKegiatanPeer::STATUS_LEVEL, 4);
                                if (DinasRincianDetailPeer::doSelectOne($c_pa) || DinasMasterKegiatanPeer::doSelectOne($c_kegiatan))
                                    echo submit_tag('Proses Approval Penyelia', array('name' => 'proses_pa', 'class' => 'btn btn-outline-primary')) . '&nbsp;';
                                echo '<button type="button" class="btn btn-outline-danger" data-toggle="modal" data-target="#myModal">Kembalikan ke entri</button>';
                            }
                        } elseif (($sf_user->hasCredential('admin_super') || $sf_user->hasCredential('admin')) && $sf_user->getNamaLogin() !== 'tim_shs') {
                            echo 'Kembalikan ke Posisi&nbsp;';
                            echo select_tag('status_level', array('-1' => '', '0' => 'Entri', '1' => 'PPTK', '2' => 'KPA', '3' => 'PA I', '4' => 'Tim Anggaran', '5' => 'PA II', '6' => 'Penyelia II', '7' => 'Rka Sebelumnya'), array('class' => 'select2'));
                            echo submit_tag('Proses', array('name' => 'proses', 'class' => 'btn btn-outline-success')) . '&nbsp;';
                            echo '&nbsp;&nbsp;&nbsp;';
                            echo submit_tag('Simpan Catatan Pembahasan', array('name' => 'simpancatatan', 'class' => 'btn btn-outline-primary')) . '&nbsp;';
                            $c_pa = new Criteria();
                            $c_pa->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
                            $c_pa->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
                            $c_pa->add(DinasRincianDetailPeer::STATUS_LEVEL, 6);
                            $c_kegiatan = new Criteria();
                            $c_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                            $c_kegiatan->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                            $c_kegiatan->add(DinasMasterKegiatanPeer::STATUS_LEVEL, 6);
                            if ((DinasRincianDetailPeer::doSelectOne($c_pa) || DinasMasterKegiatanPeer::doSelectOne($c_kegiatan))) {
                                echo submit_tag('Proses ke RKA', array('name' => 'proses_rka', 'class' => 'btn btn-outline-primary')) . '&nbsp;';
                            }
                        }
                        echo "</div>";
                        ?>
                    </div>
                    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="myModalLabel">Alasan Tolak</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label class="col-xs-3 text-right">Alasan</label>
                                        <div class="col-xs-9">
                                            <?php echo textarea_tag('catatan_tolak', null, array('class' => 'form-control')) . '<br>'; ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <label class="col-xs-3 text-right">&nbsp;</label>
                                    <div class="col-xs-9">
                                        <?php echo submit_tag('Kembalikan ke entri', array('name' => 'balik', 'class' => 'btn btn-danger btn-flat')) . '&nbsp;'; ?>
                                    </div>
                                </div>
                                <?php echo '</form>' ?>
                            </div>
                        </div>
                    </div>
                    <?php echo '</form>' ?>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    $("#cekSemua").change(function () {
        $("input:checkbox").prop('checked', $(this).prop("checked"));
    });
    $("#cekSemuaRka").change(function () {
        $(".chk_rka").prop('checked', $(this).prop("checked"));
    });

    var temp_catatan = document.getElementById('catatan').value;

    function kirimCatatan() {
        var isi_catatan = document.querySelector('#catatan').value;
        var modalku = document.querySelector('#jawaban-catatan');

        modalku.value = isi_catatan;
    }
</script>