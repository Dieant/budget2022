<?php
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header("Content-Disposition: attachment; filename=\"".'Hasil Survey'."_".$tanggal_awal." s.d ".$tanggal_akhir.".xls\"");
header("Content-Transfer-Encoding: binary");
header("Pragma: no-cache");
header("Expires: 0");
?>

<div id="sf_admin_container">
    <h1 width='100%' align="center">DAFTAR STANDAR SATUAN HARGA</h1>
    <h1 width='100%' align="center">(SSH)</h1>

    <table style="empty-cells: show;" border="1" cellpadding="3" cellspacing="0" class="sf_admin_list" width="100%">
        <tr>
            <td>
                <table width='100%'>
                    <tr>
                        <td valign='top'>
                            <table width='100%' border='1'>
                                <tr>
                                    <th rowspan="2">No</th>
                                    <th rowspan="2">Nama Barang</th>
                                    <th rowspan="2">Spesifikasi</th>
                                    <th rowspan="2">Hidden Spec</th>
                                    <th rowspan="2">Satuan</th>
                                    <th rowspan="2">Harga 2018</th>
                                    <th rowspan="2">Harga 2019</th>
                                    <th rowspan="2">Harga Pakai</th>
                                    <th rowspan="2">Rekening</th>
                                    <th rowspan="2">PPN</th>
                                    <th colspan="5" style="background:#D8D8D8">1</th>
                                    <th colspan="5" style="background:#BBEAD4">2</th>
                                    <th colspan="5" style="background:#B3BFB8">3</th>
                                    <th rowspan="1">Tanggal Survey</th>
                                </tr>
                                <tr>
                                    <th style="background:#D8D8D8">Merk</th>
                                    <th style="background:#D8D8D8">Ket. Barang</th>
                                    <th style="background:#D8D8D8">Harga</th>
                                    <th style="background:#D8D8D8">Nama Toko</th>
                                    <th style="background:#D8D8D8">Surveyor</th>
                                    <th style="background:#BBEAD4">Merk</th>
                                    <th style="background:#BBEAD4">Ket. Barang</th>
                                    <th style="background:#BBEAD4">Harga</th>
                                    <th style="background:#BBEAD4">Nama Toko</th>
                                    <th style="background:#BBEAD4">Surveyor</th>
                                    <th style="background:#B3BFB8">Merk</th>
                                    <th style="background:#B3BFB8">Ket. Barang</th>
                                    <th style="background:#B3BFB8">Harga</th>
                                    <th style="background:#B3BFB8">Nama Toko</th>
                                    <th style="background:#B3BFB8">Surveyor</th>
                                    <th style="background:#D8D8D8">Tanggal Survey</th>
                                    <th style="background:#D8D8D8">Tanggal Dukungan 1</th>
                                    <th style="background:#D8D8D8">Tanggal Dukungan 2</th>
                                    <th style="background:#D8D8D8">Tanggal Dukungan 3</th>
                                    <th style="background:#D8D8D8">Status Verifikasi Admin Survey</th>
                                </tr>
                                
                    </tr>
                    <tbody>
                        <?php
                        if($surveyor == '')
                            $query="(select * from ebudget.shsd_survey where 
                            tanggal_survey BETWEEN '".$tanggal_awal."' AND '".$tanggal_akhir."')
                            union 
                            (select * from ebudget.shsd_survey where 
                            tgl_konfirm1 BETWEEN '".$tanggal_awal."' AND '".$tanggal_akhir."')
                             union 
                            (select * from ebudget.shsd_survey where 
                            tgl_konfirm2 BETWEEN '".$tanggal_awal."' AND '".$tanggal_akhir."')
                            union 
                            (select * from ebudget.shsd_survey where 
                            tgl_konfirm3 BETWEEN '".$tanggal_awal."' AND '".$tanggal_akhir."') order by surveyor
                        ";
                        else
                            $query="(select * from ebudget.shsd_survey where 
                            tanggal_survey BETWEEN '".$tanggal_awal."' AND '".$tanggal_akhir."' AND surveyor ILIKE '%".$surveyor."%')
                            union 
                            (select * from ebudget.shsd_survey where 
                            tgl_konfirm1 BETWEEN '".$tanggal_awal."' AND '".$tanggal_akhir."' AND surveyor ILIKE '%".$surveyor."%')
                             union 
                            (select * from ebudget.shsd_survey where 
                            tgl_konfirm2 BETWEEN '".$tanggal_awal."' AND '".$tanggal_akhir."' AND surveyor ILIKE '%".$surveyor."%')
                            union 
                            (select * from ebudget.shsd_survey where 
                            tgl_konfirm3 BETWEEN '".$tanggal_awal."' AND '".$tanggal_akhir."' AND surveyor ILIKE '%".$surveyor."%')
                        ";    

                        $con = Propel::getConnection();
                        $stmt = $con->prepareStatement($query);
                        $rs = $stmt->executeQuery();

                        while ($rs->next()) {
                            $jmlPendukung = 0;
                            $shsd_id = $rs->getString('shsd_id');
                            $name = $rs->getString('shsd_name');
                            $spec = $rs->getString('spec');
                            $h_spec = $rs->getString('hidden_spec');
                            $satuan = $rs->getString('satuan');
                            $harga_d = $rs->getString('shsd_harga_dasar');
                            $harga = $rs->getString('shsd_harga');
                            $harga_p = $rs->getString('shsd_harga_pakai') == 0 ? null : $rs->getString('shsd_harga_pakai');
                            $rek = $rs->getString('rekening_code');
                            $pajak = $rs->getString('non_pajak') == TRUE ? 0 : 10;
                            $merk1 = $rs->getString('merk1');
                            $ket1 = trim($rs->getString('keterangan1'));
                            $harga1 = $rs->getString('harga1') == 0 ? null : $rs->getString('harga1');
                            $toko1 = $rs->getString('toko1');
                            $svy1 = $rs->getString('surveyor');
                            $merk2 = $rs->getString('merk2');                            
                            $ket2 = trim($rs->getString('keterangan2'));
                            $harga2 = $rs->getString('harga2') == 0 ? null : $rs->getString('harga2');
                            $toko2 = $rs->getString('toko2');
                            $svy2 = $rs->getString('surveyor');
                            $merk3 = $rs->getString('merk3');                            
                            $ket3 = trim($rs->getString('keterangan3'));
                            $harga3 = $rs->getString('harga3') == 0 ? null : $rs->getString('harga3');
                            $toko3 = $rs->getString('toko3');
                            $svy3 = $rs->getString('surveyor');
                            $tanggal = substr($rs->getString('tanggal_survey'),0,10);
                            $tgl_konfirm1 = substr($rs->getString('tgl_konfirm1'),0,10);
                            $tgl_konfirm2 = substr($rs->getString('tgl_konfirm2'),0,10);
                            $tgl_konfirm3 = substr($rs->getString('tgl_konfirm3'),0,10);
                            $status_survey = $rs->getString('status_verifikasi');

                            if($rs->getString('file1') != '')
                                $jmlPendukung++;

                            if($rs->getString('file2') != '')
                                $jmlPendukung++;

                            if($rs->getString('file3') != '')
                                $jmlPendukung++;

                            if($jmlPendukung == 0) {
                                $warna = "#FF8C9D";
                            } else if($jmlPendukung == 1) {
                                $warna = "#FFF78C";
                            } else if($jmlPendukung == 2) {
                                $warna = "#C0FF8C";
                            }
                        ?>
                        <tr style="background: <?php echo $warna;?>;">
                            <td><?php echo $shsd_id; ?></td>
                            <td><?php echo $name; ?></td>
                            <td><?php echo $spec; ?></td>
                            <td><?php echo $h_spec; ?></td>
                            <td><?php echo $satuan; ?></td>
                            <td><?php echo $harga_d; ?></td>
                            <td><?php echo $harga; ?></td>
                            <td><?php echo $harga_p; ?></td>
                            <td><?php echo $rek; ?></td>
                            <td><?php echo $pajak; ?></td>
                            <td><?php echo $merk1; ?></td>
                            <td><?php echo $ket1; ?></td>
                            <td><?php echo $harga1; ?></td>
                            <td><?php echo $toko1; ?></td>
                            <td><?php echo $svy1; ?></td>
                            <td><?php echo $merk2; ?></td>
                            <td><?php echo $ket2; ?></td>
                            <td><?php echo $harga2; ?></td>
                            <td><?php echo $toko2; ?></td>
                            <td><?php echo $svy2; ?></td>
                            <td><?php echo $merk3; ?></td>
                            <td><?php echo $ket3; ?></td>
                            <td><?php echo $harga3; ?></td>
                            <td><?php echo $toko3; ?></td>
                            <td><?php echo $svy3; ?></td>
                            <td><?php echo $tanggal; ?></td>
                            <td><?php echo $tgl_konfirm1; ?></td>
                            <td><?php echo $tgl_konfirm2; ?></td>
                            <td><?php echo $tgl_konfirm3; ?></td>
                            <td><?php echo $status_survey; ?></td>

                        </tr>
                        <?php
                        }
                        ?>
                    </tbody>
                </table>
            </td>
        </tr>
    </table>
</td>
</tr>
</table>

</div>
