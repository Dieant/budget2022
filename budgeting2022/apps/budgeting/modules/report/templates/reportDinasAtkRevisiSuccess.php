<div class="box box-info">
    <div class="box-header with-border">
        Laporan Rekening Alat Tulis Kantor Per Dinas
    </div>
    <div class="box-body">
        <table class="table table-bordered">
            <tr>
                <th class="text-center">SKPD</th>
                <th class="text-center">Semula</th>
                <th class="text-center">Menjadi</th>
                <th class="text-center">Selisih</th>
            </tr>
            <?php
            foreach ($dinas as $value):
                ?>
                <tr>
                    <td class="text-left" ><?php echo $value['unit_name'] ?></td>
                    <td class="text-right" ><?php echo number_format($value['semula'], 0, ',', '.') ?></td>
                    <td class="text-right" ><?php echo number_format($value['menjadi'], 0, ',', '.') ?></td>
                    <td class="text-right" ><?php echo number_format(($value['semula'] - $value['menjadi']), 0, ',', '.') ?></td>
                </tr>
            <?php endforeach; ?>
            <tr>
                <td class="text-center"><b> TOTAL </b></td>
                <td class="text-right"><b> <?php echo number_format($totalSemula, 0, ',', '.'); ?> </b></td>
                <td class="text-right"><b> <?php echo number_format($totalMenjadi, 0, ',', '.'); ?> </b></td>
                <td class="text-right"><b> <?php echo number_format($totalSemula - $totalMenjadi, 0, ',', '.'); ?> </b></td>
            </tr>
        </table>
    </div>
</div>