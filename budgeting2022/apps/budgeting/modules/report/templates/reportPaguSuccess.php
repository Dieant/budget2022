<?php use_helper('Form', 'Object', 'Javascript') ?>
<div class="box box-primary box-solid">
    <div class="box-header with-border">
        Laporan Pagu Murni & Entry
    </div>
    <div class="box-body">
        <table class="table table-bordered">
            <tr>
                <th class="text-center">Dinas</th>
                <th class="text-center">Kode Kegiatan</th>
                <th class="text-center">Nama Kegiatan</th>
                <th class="text-center">Semula</th>
                <!-- <th class="text-center">Pagu KUA</th>
                <th class="text-center">Pagu APBD</th>-->
                <th class="text-center">Rincian (Terentri)</th>
                <!-- <th class="text-center">Selisih (Pagu KUA - Pagu APBD)</th> -->
                <th class="text-center">Selisih (Semula - Rincian (Terentri))</th>
                <!-- <th class="text-center">Kunci</th>
                <th class="text-center">Posisi</th> -->
            </tr>
            
            <?php
            $tempUnit = '';
            $anggaran = 0;
            $tambahan = 0;
            $pagu = 0;
            $rapbd= 0 ;
            $anggaran_kota = 0;
            $tambahan_kota = 0;
            $pagu_kota = 0;
            $pagumurni = 0;
            $nilai_anggaran = 0;

            foreach ($kegiatans as $value): 
                    if($tempUnit != $value['unit_id']): ?>
                    <?php if($pagu > 0): ?>
                        <tr> 
                            <td colspan="3"></td>
                            <td></td>
                            <!-- <td>Rp.<b><?php echo number_format($pagu, 2, ',', '.') ?></b></td>
                            <td>Rp.<b><?php echo number_format($nilai_anggaran, 2, ',', '.') ?></b></td>
                            <td>Rp.<b><?php echo number_format($value['pagu']+$value['tambahan_pagu'], 2, ',', '.') ?></b></td> -->
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            
                        </tr>
                        <?php
                        $nilai_anggaran = 0;
                        $tambahan = 0;
                        $pagu = 0;
                        $pagumurni = 0;
                        ?>
                    <?php endif; ?>
                    <?php if(!empty($value['unit_id'])): ?>
                        <tr><td colspan="8">&nbsp;</td></tr>
                        <tr>
                            <td colspan="8" class="text-left text-bold"><?php echo $value['unit_id'] . ' - ' . $value['unit_name'] ?></td>
                        </tr>
                    <?php endif; ?>
                <?php endif; ?>
                
                <?php
                
                

                $query = "select SUM(alokasi) as nilai
                    from " . sfConfig::get('app_default_schema') . ".pagu
                    where kode_kegiatan ='" . $value['kode_kegiatan'] . "' and unit_id ='" . $value['unit_id'] . "' ";
                    $con = Propel::getConnection();
                    $stmt = $con->prepareStatement($query);
                    $rs = $stmt->executeQuery();
                    while ($rs->next()) {
                        $pagumurni = $rs->getFloat('nilai');
                    }
                $query = "select SUM(nilai_anggaran) as nilai_anggaran
                    from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail
                    where kegiatan_code ='" . $value['kode_kegiatan'] . "' and unit_id ='" . $value['unit_id'] . "' and status_hapus=FALSE";
                    $con = Propel::getConnection();
                    $stmt = $con->prepareStatement($query);
                    $rs = $stmt->executeQuery();
                    while ($rs->next()) {
                        $nilai_anggaran = $rs->getFloat('nilai_anggaran');
                }
                $query = "select max(status_level) as status_level
                    from ".sfConfig::get('app_default_schema').".dinas_rincian_detail
                    where kegiatan_code ='" . $value['kode_kegiatan'] . "' and unit_id ='" . $value['unit_id'] . "' and status_hapus=FALSE";
                    $con = Propel::getConnection();
                    $stmt = $con->prepareStatement($query);
                    $rs = $stmt->executeQuery();
                    while ($rs->next()) {
                        $status_level = $rs->getString('status_level');
                }

                if($status_level == 0)
                    $level = 'Entri';
                else if($status_level == 1)
                    $level = 'PPTK';
                else if($status_level == 2)
                    $level = 'KPA';
                else if($status_level == 3)
                    $level = 'PA I';
                else if($status_level == 4)
                    $level = 'Tim Anggaran';
                else if($status_level == 5)
                    $level = 'PA II';
                else if($status_level == 6)
                    $level = 'Penyelia II';
                else if($status_level == 7)
                    $level = 'RKA';

                if($value['kunci'] == 1 || $value['kunci'] == 2)
                    $kunci = 'DINAS';
                else
                    $kunci = 'PENELITI'; 

                // if($value['pagu'] != $nilai_anggaran)
                //     $style = "background: pink;";
                // else
                //     $style = "";
                
                if(!empty($value['unit_id'])): 
                    $rapbd=$value['pagu']+$value['tambahan_pagu'];

                    if($rapbd != $nilai_anggaran)
                    $style = "background: pink;";
                    else
                    $style = "";
                    ?>

                    <tr style="<?php echo $style ?>">
                        <td>&nbsp;</td>
                        <td class="text-left" ><?php echo $value['kode_kegiatan'] ?></td>
                        <td class="text-left" ><?php echo $value['nama_kegiatan'] ?></td>
                        <td class="text-left" >Rp.<?php echo number_format($pagumurni, 0, ',', '.') ?></td>
                        <!-- <td class="text-left" >Rp.<?php echo number_format($value['pagu'], 0, ',', '.') ?></td>
                        <td class="text-left" >Rp.<?php echo number_format($rapbd, 0, ',', '.') ?></td> -->
                        <td class="text-left" >Rp.<?php echo number_format($nilai_anggaran, 0, ',', '.') ?></td>
                        <!-- <td class="text-left" >Rp.<?php echo number_format($value['tambahan_pagu'], 0, ',', '.') ?></td> -->
                        <td class="text-left" >Rp.<?php echo number_format($nilai_anggaran - $pagumurni, 0, ',', '.') ?></td>
                        <!-- <td class="text-left" ><?php echo $kunci ?></td>
                        <td class="text-left" ><?php echo $level ?></td> -->
                    </tr> 
                    <?php                    
                    $tempUnit = $value['unit_id'];
                    $tambahan += $value['tambahan_pagu'];
                    $pagu += $value['pagu'];
                    $anggaran_kota += $nilai_anggaran;
                    $pagumurni_kota += $pagumurni;
                    $pagu_kota += $value['pagu']; 
                    $pagu_rapbd += $rapbd;?>
                <?php endif; ?>
            <?php endforeach; ?>
            <tr><td colspan="8"><b>TOTAL MURNI = Rp.<?php echo number_format($pagumurni_kota, 2, ',', '.') ?></b></td></tr>
            <!-- <tr><td colspan="8"><b>TOTAL PAGU KUA = Rp.<?php echo number_format($pagu_kota, 2, ',', '.') ?></b></td></tr>
            <tr><td colspan="8"><b>TOTAL PAGU APBD = Rp.<?php echo number_format($pagu_rapbd, 2, ',', '.') ?></b></td></tr> -->
            <tr><td colspan="8"><b>TOTAL TERENTRI = Rp.<?php echo number_format($anggaran_kota, 2, ',', '.') ?></b></td></tr>
            <tr><td colspan="8"><b>SELISIH = Rp.<?php echo number_format($anggaran_kota - $pagumurni_kota, 2, ',', '.') ?></b></td></tr>
        </table>
    </div>
</div>