<?php use_helper('Form', 'Object', 'Javascript') ?>
<?php
if (sfConfig::get('app_tahap_edit') == 'murni') {
    $nama_sistem = 'Pra-RKA';
} elseif (sfConfig::get('app_tahap_edit') == 'pak') {
    $nama_sistem = 'PAK';
} elseif (sfConfig::get('app_tahap_edit') == 'penyesuaian') {
    $nama_sistem = 'Penyesuaian';
} else {
    $nama_sistem = 'Revisi';
}
?>
<div class="box box-primary box-solid">        
    <div class="box-header with-border">
        Pilih Kelompok Barang I
    </div>
    <div class="box-body">
        <?php
        echo select_tag('kelompok1', options_for_select($arrKelompok, $kelompok1 ? $kelompok1 : ''), array('class' => 'form-control'));
        ?>
    </div>
    <div class="box-header with-border">
        Pilih Kelompok Barang II
    </div>
    <div class="box-body">
        <?php
        echo select_tag('kelompok2', options_for_select($arrKelompok2, $kelompok2 ? $kelompok2 : '' , 'include_custom=---Pilih Kelompok Barang I Dulu---'), array('class' => 'form-control'));
        ?>
    </div>
</div>
<div class="box box-primary box-solid" id="body">        
    <div class="box-header with-border">
        Print Excel Survey SSH
    </div>
    <div class="box-body">
        Pilih Kelompok Barang 1 dan 2
    </div>
</div>
<script>
    $(document).ready(function () {
        $(".js-example-basic-single").select2();
        $(".js-example-basic-multiple").select2();
    });
    
    $("#kelompok1").change(function () {
        var id = $(this).val();
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/report/pilihKelompokSurvey/kelompok1/" + id + ".html",
            context: document.body
        }).done(function (msg) {
            $('#kelompok2').html(msg);
        });
    });
    $('#kelompok2').change(function () {
        var kelompok1 = $("#kelompok1").val();
        var kelompok2 = $("#kelompok2").val();
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/report/pilihKelompok2/kelompok1/" + kelompok1 + "/kelompok2/" + kelompok2 + ".html",
            context: document.body
        }).done(function (msg) {
            $('#body').html(msg);
        });
    });
    // $("#posisi").change(function () {
    //     $('#indicator').show();
    //     var id = $("#unit_id").val();
    //     var id2 = $(this).val();
    //     if (id == '') {
    //         id = '0000';
    //     }
    //     if (id2 == '') {
    //         id2 = 'xxx';
    //     }
    //     $.ajax({
    //         url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/report/reportPosisiRevisi/unit_id/" + id + "/posisi/" + id2 + ".html",
    //         context: document.body
    //     }).done(function (msg) {
    //         $('#indicator').hide();
    //         $('#isi').html(msg);
    //     });
    // });
</script>