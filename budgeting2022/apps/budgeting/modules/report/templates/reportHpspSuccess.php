<?php use_helper('Form', 'Object', 'Javascript') ?>
<?php
if (sfConfig::get('app_tahap_edit') == 'murni') {
    $nama_sistem = 'Pra-RKA';
} elseif (sfConfig::get('app_tahap_edit') == 'pak') {
    $nama_sistem = 'PAK';
} elseif (sfConfig::get('app_tahap_edit') == 'penyesuaian') {
    $nama_sistem = 'Penyesuaian';
} else {
    $nama_sistem = 'Revisi';
}
?>
<div class="box box-info">        
    <div class="box-header with-border">
        Pilih SKPD
    </div>
    <div class="box-body">
        <?php
        echo select_tag('filters[unit_id]', options_for_select($unit_kerja, @$filters['unit_id'] ? $filters['unit_id'] : ''), array('class' => 'form-control'));
        ?>
    </div>
    <div class="box-header with-border">
        Pilih Kegiatan
    </div>
    <div class="box-body">
        <?php
        echo select_tag('filters[kegiatan]', options_for_select($kegiatan, @$filters['kegiatan'] ? $filters['kegiatan'] : '' , 'include_custom=---Pilih SKPD Dulu---'), array('class' => 'form-control'));
        ?>
    </div>
    <div class="box-header with-border">
        Pilih Tahap
    </div>
    <div class="box-body">
        <select id="tahap" class="form-control">
            <option value="">------Pilih Tahap------</option>
            <option value="revisi1" <?php if ($tahap == 'revisi1') echo 'selected'; ?>>Revisi 1</option>
            <option value="revisi1_1" <?php if ($tahap == 'revisi1_1') echo 'selected'; ?>>Penyesuaian Komponen 1</option>
            <option value="revisi2" <?php if ($tahap == 'revisi2') echo 'selected'; ?>>Revisi 2</option>
            <option value="revisi2_1" <?php if ($tahap == 'revisi2_1') echo 'selected'; ?>>Penyesuaian Komponen 2</option>
            <option value="revisi2_1" <?php if ($tahap == 'revisi2_2') echo 'selected'; ?>>Penyesuaian Komponen 3</option>            
            <!-- <option value="revisi5" <?php if ($tahap == 'revisi5') echo 'selected'; ?>>Revisi 5</option> -->
        </select>
    </div>
</div>
<div class="box box-info">        
    <div class="box-header with-border">
        Laporan Komponen HPSP
    </div>
    <div class="box-body">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th class="text-center">Komponen PSP</th>
                    <th class="text-center">Nilai Anggaran</th>
                    <th class="text-center">Pemanfaatan</th>
                    <th class="text-center">Komponen Hasil PSP</th>
                    <th class="text-center">Volume</th>
                    <th class="text-center">Satuan</th>
                    <th class="text-center">Harga</th>
                    <th class="text-center">Pajak</th>
                    <th class="text-center">Nilai Anggaran</th>
                    <th class="text-center">Tahap</th>
                </tr>    
            </thead>
            <tbody id="body">
                
            </tbody>
        </table>
    </div>
</div>
<script>
    $(document).ready(function () {
        $(".js-example-basic-single").select2();
        $(".js-example-basic-multiple").select2();
    });
    
    $("#filters_unit_id").change(function () {
        var id = $(this).val();
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/report/pilihSkpd/unit_id/" + id + ".html",
            context: document.body
        }).done(function (msg) {
            $('#filters_kegiatan').html(msg);
        });
    });
    $('#filters_kegiatan').change(function () {
        var unit_id = $("#filters_unit_id").val();
        var kegiatan_code = $("#filters_kegiatan").val();
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/report/generateReportHpsp/unit_id/" + unit_id + "/kegiatan_code/" + kegiatan_code + ".html",
            context: document.body
        }).done(function (msg) {
            $('#body').html(msg);
        });
    });
    $('#tahap').change(function () {
        var unit_id = $("#filters_unit_id").val();
        var kegiatan_code = $("#filters_kegiatan").val();
        var tahap = $("#tahap").val();
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/report/generateReportHpsp/unit_id/" + unit_id + "/kegiatan_code/" + kegiatan_code + "/tahap/" + tahap + ".html",
            context: document.body
        }).done(function (msg) {
            $('#body').html(msg);
        });
    });
    // $("#posisi").change(function () {
    //     $('#indicator').show();
    //     var id = $("#unit_id").val();
    //     var id2 = $(this).val();
    //     if (id == '') {
    //         id = '0000';
    //     }
    //     if (id2 == '') {
    //         id2 = 'xxx';
    //     }
    //     $.ajax({
    //         url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/report/reportPosisiRevisi/unit_id/" + id + "/posisi/" + id2 + ".html",
    //         context: document.body
    //     }).done(function (msg) {
    //         $('#indicator').hide();
    //         $('#isi').html(msg);
    //     });
    // });
</script>