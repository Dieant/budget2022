<?php use_helper('Javascript', 'Number') ?>
<?php
if ($sf_params->get('print')) {
    echo use_stylesheet('/css/tampilan_print2.css');
    echo javascript_tag("window.print();");
}
?>
<div class="box box-info">        
    <div class="box-header with-border">
        Laporan Per Rekening Per Dinas
    </div>
    <div class="box-body">
        <table class="table table-bordered">
            <tr>
                <th class="text-center">Dinas</th>
                <th class="text-center">Nilai Usulan</th>
                <th class="text-center">% Usulan</th>
                <th class="text-center">Nilai Disetujui</th>
                <th class="text-center">% Disetujui</th>
            </tr>
            <?php
            $i = 0;
            $rs->first();
            $unit = '';
            do {
                if (isset($dinas_ok[$i]) == 't') {
                    ?>
                    <tr>
                        <td colspan="5"></td>
                    </tr>
                    <tr>
                        <td class="text-left text-bold"><?php echo $rs->getString('old_rekening_code') . ' ' . $rs->getString('old_rekening_name'); ?></td>
                        <td class="text-right text-bold"><?php echo $nilai_draft[$i]; ?></td>
                        <td class="text-right text-bold"><?php echo $persen_draft[$i]; ?>%</td>
                        <td class="text-right text-bold"><?php echo $nilai_locked[$i]; ?></td>
                        <td class="text-right text-bold"><?php echo $persen_locked[$i]; ?>%</td>
                    </tr>
                    <?php
                }
                if ($unit != $rs->getString('unit_id')) {
                    $unit = $rs->getString('unit_id');
                    $rekening_old = $rs->getString('old_rekening_code');
                    $sql = "select rekening_code as old_rekening_code, rekening_name as old_rekening_name,unit_id,unit_name ,sum((jumlah_draft*jumlah_non_lanjutan)) as total_draft3,  
                        sum((jumlah_setuju*jumlah_non_lanjutan)) as total_locked3, sum((nilai_draft*jumlah_non_lanjutan)) as nilai_draft3, sum((nilai_draft*jumlah_setuju*jumlah_non_lanjutan)) as nilai_locked3 
                        FROM " . sfConfig::get('app_default_schema') . ".v_kegiatan_rekening_prarka "
                            . "WHERE unit_id<>'9999' and unit_id='$unit' and rekening_code='$rekening_old' "
                            . "GROUP BY rekening_code,rekening_name,unit_id,unit_name ORDER BY rekening_code,rekening_name,unit_id,unit_name";
                    $con = Propel::getConnection();
                    $stmt = $con->prepareStatement($sql);
                    $jr = $stmt->executeQuery();
                    while ($jr->next()) {
                        $nilai_draft14 = $jr->getString('nilai_draft3');
                        $persen_draft14 = $nilai_draft14 * 100 / $sum_draft;
                        $nilai_locked14 = $jr->getString('nilai_locked3');
                        $persen_locked14 = $nilai_locked14 * 100 / $sum_locked;
                    }
                    ?>
                    <tr>
                        <td class="text-left"><?php echo $rs->getString('unit_id') . ' ' . $rs->getString('unit_name'); ?></td>
                        <td class="text-right"><?php echo number_format($nilai_draft14, 0, ",", "."); ?></td>
                        <td class="text-right"><?php echo number_format($persen_draft14, 2, ",", "."); ?>%</td>
                        <td class="text-right"><?php echo number_format($nilai_locked14, 0, ",", "."); ?></td>
                        <td class="text-right"><?php echo number_format($persen_locked14, 2, ",", "."); ?>%</td>
                    </tr> 
                <?php }
                ?>        
                <?php
                $i++;
            } while ($rs->next());
            ?>        
            <tr>                
                <td class="text-right text-bold bg-green-active">Total</td>
                <td class="text-right text-bold bg-green-active"><?php echo $nilai_draft2 ?></td>
                <td class="text-right text-bold bg-green-active">100 %</td>
                <td class="text-right text-bold bg-green-active"><?php echo $nilai_locked2 ?></td>
                <td class="text-right text-bold bg-green-active">100 %</td>
            </tr>
        </table>       
    </div>
</div>