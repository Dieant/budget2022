<?php use_helper('Object', 'Javascript') ?>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="fas fa-align-left"></i> Pilih Rekening
                        </h3>
                    </div>
                    <div class="card-body"> 
                        <select class="form-control" name="rek" id="rek">
                            <option value="">---Pilih Rekening ---</option>
                            <?php foreach ($rek as $value) { ?>
                                <option value="<?php echo $value->getRekeningCode() ?>"><?php echo $value->getRekeningCode() . ' - ' . $value->getRekeningName() ?></option>
                            <?php }
                            ?>            
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- <div class="box box-info">        
    <div class="box-header with-border">
        
    </div>
    <div class="box-body">
        
    </div>
</div> -->
<script>
    $("#rek").change(function() {
        var id = $(this).val();
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/report/tampilReport/b/rrkkpr/rek/" + id + ".html",
            context: document.body
        }).done(function(msg) {
            $('#isi_dalam').html('');
            $('#isi_dalam').html(msg);
        });
    });
</script>