<div class="box box-info">        
    <div class="box-header with-border">
        Laporan Per Dinas Per Rekening
    </div>
    <div class="box-body">
        <table class="table table-bordered">
            <tr>
                <th class="text-center" style="width: 10px"></th>
                <th class="text-center">Kode Rekening</th>
                <th class="text-center">Rekening</th>
                <th class="text-center">Nilai Usulan</th>
                <th class="text-center">Nilai Disetujui</th>
                <th class="text-center">Persentase</th>
            </tr>
            <?php
            $i = 0;
            if ($rs->first() <> '') {
                $rs->first();
                do {
                    if (isset($dinas_ok[$i])):
                        ?>
                        <tr><td colspan="6">&nbsp;</td></tr>
                        <tr>
                            <td colspan="3" class="text-left text-bold"><?php echo '('.$unit_id[$i] . ') ' . $unit_name[$i] ?></td>
                            <td class="text-right text-bold"><?php echo $nilai_usulan_unit[$i] ?></td>
                            <td class="text-right text-bold"><?php echo $nilai_locked[$i] ?></td>
                            <td class="text-right text-bold">&nbsp;</td>
                            <?php $nilai_locked_total = str_replace('.', '', $nilai_locked[$i]); ?>
                        </tr>
                    <?php endif ?>
                    <tr>
                        <td></td>
                        <td class="text-left"><?php echo $rs->getString('rekening_code') ?></td>
                        <td class="text-center"><?php echo $rs->getString('rekening_name') ?></td>
                        <td class="text-right"><?php echo $nilai_usulan_kegiatan[$i] ?></td>
                        <td class="text-right"><?php echo $nilai_locked3[$i] ?></td>
                        <td class="text-right">
                            <?php
                            $nilai = str_replace('.', '', $nilai_locked3[$i]);
                            echo number_format(($nilai / $nilai_locked_total) * 100, 2, ",", ".");
                            ?>
                        </td>
                    </tr>
                    <?php
                    $i++;
                } while ($rs->next());
            }
            ?>
            <tr>
                <td colspan="3" class="text-center text-bold bg-green-active">Total</td>
                <td class="text-right text-bold bg-green-active"><?php echo $nilai_draft4 ?></td>
                <td class="text-right text-bold bg-green-active"><?php echo $nilai_locked2 ?></td>
            </tr>
        </table>       
    </div>
</div>
<?php 
echo obse
?>