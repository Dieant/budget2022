<?php use_helper('Form', 'Object', 'Javascript') ?>
<?php
if (sfConfig::get('app_tahap_edit') == 'murni') {
    $nama_sistem = 'Pra-RKA';
} elseif (sfConfig::get('app_tahap_edit') == 'pak') {
    $nama_sistem = 'PAK';
} elseif (sfConfig::get('app_tahap_edit') == 'penyesuaian') {
    $nama_sistem = 'Penyesuaian';
} else {
    $nama_sistem = 'Revisi';
}
?>
<div class="box box-info">        
    <div class="box-header with-border">
        Pilih SKPD
    </div>
    <div class="box-body">
        <?php
        $e = new Criteria();
        $e->add(UnitKerjaPeer::UNIT_ID, '9999', Criteria::NOT_IN);
        $e->addAscendingOrderByColumn(UnitKerjaPeer::UNIT_NAME);
        $unit_kerja = UnitKerjaPeer::doSelect($e);
        echo select_tag('unit_id', objects_for_select($unit_kerja, 'getUnitId', 'getUnitName', (isset($filter_unit_kerja) ? $filter_unit_kerja : NULL), array('include_custom' => '------Pilih Dinas------')), array('id' => 'unit_id', 'class' => 'form-control'));
        ?>
    </div>
</div>
<div class="box box-info">        
    <div class="box-header with-border">
        Laporan Kegiatan yang Mengalami Perubahan F1
    </div>
    <div class="box-body">
        <table class="table table-bordered">
            <tr>
                <th class="text-center">Dinas</th>
                <th class="text-center">Kode Kegiatan</th>
                <th class="text-center">Nama Kegiatan</th>
            </tr>
            <?php
            $tempUnit = '';
            foreach ($kegiatans as $value):
                ?>
                <?php if($tempUnit != $value['unit_id']): ?>
                <tr><td colspan="3">&nbsp;</td></tr>
                <tr>
                    <td colspan="3" class="text-left text-bold"><?php echo $value['unit_id'] . ' - ' . $value['unit_name'] ?></td>
                </tr>
                <?php endif; ?>

                <tr>
                    <td>&nbsp;</td>
                    <td class="text-left" ><?php echo $value['kode_kegiatan'] ?></td>
                    <td class="text-left" ><?php echo $value['nama_kegiatan'] ?></td>
                </tr>
            <?php 
                $tempUnit = $value['unit_id'];
            endforeach; ?>
        </table>
    </div>
</div>
<script>
    $("#unit_id").change(function () {
        $('#indicator').show();
        var id = $(this).val();
        if (id == '') {
            id = '0000';
        }
        var tahap = "<?php echo $tahap; ?>";
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/report/reportPerubahanF1/unit_id/" + id + "/tahap/" + tahap + ".html",
            context: document.body
        }).done(function (msg) {
            $('#indicator').hide();
            $('#isi').html(msg);
        });

    });
</script>