<div class="box box-info">        
    <div class="box-header with-border">
        Laporan Per Program
    </div>
    <div class="box-body">
        <table class="table table-bordered">
            <tr>
                <th class="text-center">Kode Program</th>
                <th class="text-center">Nama Program</th>
                <th class="text-center">Nilai Usulan</th>
                <th class="text-center">% Usulan</th>
                <th class="text-center">Nilai Disetujui</th>
                <th class="text-center">% Disetujui</th>
            </tr>
            <?php
            $i = 0;
            $rs->first();
            do {
                ?>
                <tr>
                    <td class="text-center"><?php echo $rs->getString('kode_bidang') ?></td>
                    <td class="text-left"><?php echo $rs->getString('nama_bidang') ?></td>
                    <td class="text-right"><?php echo $nilai_draft3[$i] ?></td>
                    <td class="text-right"><?php echo $persen_draft3[$i] ?>%</td>
                    <td class="text-right"><?php echo $nilai_locked3[$i] ?></td>
                    <td class="text-right"><?php echo $persen_locked3[$i] ?>%</td>
                </tr>
                <?php
                $i++;
            } while ($rs->next());
            ?>
            <tr>
                <td colspan="2" class="text-center text-bold bg-green-active">Total</td>
                <td class="text-right text-bold bg-green-active"><?php echo $nilai_draft2; ?></td>
                <td class="text-right text-bold bg-green-active">100%</td>
                <td class="text-right text-bold bg-green-active"><?php echo $nilai_locked2; ?></td>
                <td class="text-right text-bold bg-green-active">100%</td>
            </tr>
        </table>       
    </div>
</div>