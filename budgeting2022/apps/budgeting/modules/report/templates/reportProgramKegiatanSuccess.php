<div class="box box-info">        
    <div class="box-header with-border">
        Laporan Per Program Per Kegiatan
    </div>
    <div class="box-body">
        <table class="table table-bordered">
            <tr>
                <th style="width: 10px" ></th>
                <th style="width: 10px" ></th>
                <th class="text-center">SKPD - Kegiatan</th>
                <th class="text-center">Nilai Usulan</th>
                <th class="text-center">% Usulan</th>
                <th class="text-center">Nilai Disetujui</th>
                <th class="text-center">% Disetujui</th>
            </tr>
            <?php
            $i = 0;
            $rs->first();
            do {
                ?>
                <?php if (isset($dinas_ok[$i])): ?>
                    <tr><td colspan="7">&nbsp;</td></tr>
                    <tr>
                        <td colspan="3" class="text-left text-bold"><?php echo $kode_program[$i] . ' ' . $nama_program[$i] ?></td>
                        <td class="text-right text-bold"><?php echo $nilai_draft[$i] ?></td>
                        <td class="text-right text-bold"><?php echo $persen_draft[$i] ?>%</td>
                        <td class="text-right text-bold"><?php echo $nilai_locked[$i] ?></td>
                        <td class="text-right text-bold"><?php echo $persen_locked[$i] ?>%</td>

                    </tr>
                <?php endif ?>
                <!-- <?php if (isset($kegiatan_ok[$i])): ?> -->
                    <tr>
                        <td></td>
                        <td colspan="2" class="text-left text-bold"><?php echo '('.$unit_id[$i] . ') ' . $unit_name[$i] ?></td>
                        <td class="text-right text-bold"><?php echo $nilai_draft3[$i] ?></td>
                        <td class="text-right text-bold"><?php echo $persen_draft3[$i] ?>%</td>
                        <td class="text-right text-bold"><?php echo $nilai_locked3[$i] ?></td>
                        <td class="text-right text-bold"><?php echo $persen_locked3[$i] ?>%</td>
                    </tr>
                <!-- <?php endif ?> -->
                <tr>
                    <td></td>
                    <td></td>
                    <td class="text-left"><?php echo $rs->getString('kode_kegiatan') . ' ' . $rs->getString('nama_kegiatan') ?></td>
                    <td class="text-right"><?php echo $nilai_draft4[$i] ?></td>
                    <td class="text-right"><?php echo $persen_draft4[$i] ?>%</td>
                    <td class="text-right"><?php echo $nilai_locked4[$i] ?></td>
                    <td class="text-right"><?php echo $persen_locked4[$i] ?>%</td>
                </tr>
                <?php
                $i++;
            } while ($rs->next());
            ?>
            <tr><td colspan="7">&nbsp;</td></tr>
            <tr>
                <td colspan="3" class="text-center text-bold bg-green-active">Total</td>
                <td class="text-right text-bold bg-green-active"><?php echo $nilai_draft2; ?></td>
                <td class="text-right text-bold bg-green-active">100%</td>
                <td class="text-right text-bold bg-green-active"><?php echo $nilai_locked2; ?></td>
                <td class="text-right text-bold bg-green-active">100%</td>
            </tr>
        </table>       
    </div>
</div>