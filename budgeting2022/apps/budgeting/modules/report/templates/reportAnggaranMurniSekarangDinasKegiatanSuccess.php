<?php use_stylesheet('/sf/sf_admin/css/main') ?>
<center><h3 style="color: black">Laporan Perbandingan Murni - Sekarang</h3></center><br/>
<table class="sf_admin_list">
    <tr>
        <th>#</th>
        <th>Kode SKPD</th>
        <th>Nama SKPD</th>
        <th>Kode Kegiatan</th>
        <th>Murni</th>
        <th>Sekarang</th>
        <th>Selisih</th>
    </tr>    
    <?php
    $query = "select Q.unit_id as kode_skpd, Q.unit_name as skpd, Q.kegiatan_code as kode_kegiatan, Q.jumlah as murni, W.jumlah as sekarang,  Q.jumlah - W.jumlah as selisih
from (select a.unit_id, b.unit_name, kegiatan_code, sum(nilai_anggaran) as jumlah
from " . sfConfig::get('app_default_schema') . ".prev_rincian_detail a, unit_kerja b 
where status_hapus = false and b.unit_id = a.unit_id and a.unit_id <> '9999' 
group by a.unit_id, b.unit_name, kegiatan_code order by a.unit_id, kegiatan_code) Q, 
(select a.unit_id, b.unit_name, kegiatan_code, sum(nilai_anggaran) as jumlah
from " . sfConfig::get('app_default_schema') . ".rincian_detail a, unit_kerja b 
where status_hapus = false and b.unit_id = a.unit_id and a.unit_id <> '9999' 
group by a.unit_id, b.unit_name, kegiatan_code order by a.unit_id, kegiatan_code) W
where Q.unit_id = W.unit_id and Q.kegiatan_code = W.kegiatan_code
group by Q.unit_id, Q.unit_name, Q.kegiatan_code, Q.jumlah, W.jumlah
having Q.jumlah <> W.jumlah";
    $con = Propel::getConnection();
    $stmt = $con->prepareStatement($query);
    $rs = $stmt->executeQuery();
    $i = 1;
    while ($rs->next()) {
        echo "<tr>";
        echo "<td>" . $i . "</td>";
        echo "<td style='text-align:center'>" . $rs->getString('kode_skpd') . "</td>";
        echo "<td style='text-align:center'>" . $rs->getString('skpd') . "</td>";
        echo "<td style='text-align:center'>" . $rs->getString('kode_kegiatan') . "</td>";
        echo "<td style='text-align:right'>" . number_format ( $rs->getString('murni') , 0 , "," ,"." ) . "</td>";
        echo "<td style='text-align:right'>" . number_format ( $rs->getString('sekarang') , 0 , "," ,"." ) . "</td>";
        echo "<td style='text-align:right'>" . number_format ( $rs->getString('selisih') , 0 , "," ,"." ) . "</td>";
        echo "</tr>";
        $i++;
    }
    ?>
</table>

