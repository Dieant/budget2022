<?php use_helper('Form', 'Object', 'Javascript') ?>
<?php
if (sfConfig::get('app_tahap_edit') == 'murni') {
    $nama_sistem = 'Pra-RKA';
} elseif (sfConfig::get('app_tahap_edit') == 'pak') {
    $nama_sistem = 'PAK';
} elseif (sfConfig::get('app_tahap_edit') == 'penyesuaian') {
    $nama_sistem = 'Penyesuaian';
} else {
    $nama_sistem = 'Revisi';
}
?>
<div class="box box-info">        
    <div class="box-header with-border">
        Pilih Lokasi
    </div>
    <div class="box-body">
        <?php
        echo select_tag('lokasi', objects_for_select($lokasi, 'getLokasi', 'getLokasi', (isset($filter_unit_kerja) ? $filter_unit_kerja : NULL), array('include_custom' => '------Pilih Lokasi------')), array('id' => 'lokasi', 'class' => 'form-control js-example-basic-single'));
        ?>
    </div>
</div>
<div class="box box-info">
    <div class="box-header with-border">
        Laporan Komponen Berdasarkan Lokasi
    </div>
    <div class="box-body">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th class="text-center">Nama OPD</th>
                    <th class="text-center">Kode Kegiatan</th>
                    <th class="text-center">Nama Kegiatan</th>
                    <th class="text-center">Nama Komponen</th>
                    <th class="text-center">Volume</th>
                    <th class="text-center">Satuan</th>
                    <th class="text-center">Pajak</th>
                    <th class="text-center">Harga</th>
                    <th class="text-center">Total Anggaran</th>
                    <th class="text-center">Tahun</th>
                </tr>    
            </thead>
            <tbody id="body">
                
            </tbody>
        </table>
    </div>
</div>
<script>
    $(document).ready(function () {
        $(".js-example-basic-single").select2();
        $(".js-example-basic-multiple").select2();
    });
    
    $("#lokasi").change(function () {
        var lokasi = $(this).val();
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/report/generateReportKomponenPerLokasi/lokasi/" + lokasi + ".html",
            context: document.body
        }).done(function (msg) {
            $('#body').html(msg);
        });

    });
</script>