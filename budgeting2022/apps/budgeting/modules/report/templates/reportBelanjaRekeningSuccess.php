<div class="box box-info">        
    <div class="box-header with-border">
        Laporan Per Belanja Per Rekening
    </div>
    <div class="box-body">
        <table class="table table-bordered">
            <tr>
                <th class="text-center">&nbsp;</th>
                <th class="text-center">Kode Rekening</th>
                <th class="text-center">Nama Rekening</th>
                <th class="text-center">Nilai Usulan</th>
                <th class="text-center">% Usulan</th>
                <th class="text-center">Nilai Disetujui</th>
                <th class="text-center">% Disetujui</th>
            </tr>
            <?php
            $i = 0;
            $rs->first();
            do {
                ?>
                <?php if (isset($dinas_ok[$i])):   ?>
                    <tr>
                        <td colspan="7">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="3" class="text-left text-bold" ><?php echo $belanja_id[$i] ?> &nbsp; <?php echo isset($belanja_name[$i]) ? $belanja_name[$i] : '' ?></td>
                        <td class="text-right text-bold"><?php echo isset($nilai_draft[$i]) ? $nilai_draft[$i] : '' ?></td>
                        <td class="text-right text-bold"><?php echo isset($persen_draft[$i]) ? $persen_draft[$i] : '' ?>%</td>
                        <td class="text-right text-bold"><?php echo isset($nilai_locked[$i]) ? $nilai_locked[$i] : '' ?></td>
                        <td class="text-right text-bold"><?php echo isset($persen_locked[$i]) ? $persen_locked[$i] : '' ?>%</td>
                    </tr>

                <?php endif; ?>
                <tr>
                    <td>&nbsp;</td>
                    <td class="text-center"><?php echo isset($rekening_code[$i]) ? $rekening_code[$i] : '' ?></td>
                    <td class="text-left"><?php echo isset($rekening_name[$i]) ? $rekening_name[$i] : '' ?></td>
                    <td class="text-right"><?php echo isset($nilai_draft3[$i]) ? $nilai_draft3[$i] : '' ?></td>
                    <td class="text-right"><?php echo isset($persen_draft3[$i]) ? $persen_draft3[$i] : '' ?>%</td>
                    <td class="text-right"><?php echo isset($nilai_locked3[$i]) ? $nilai_locked3[$i] : '' ?></td>
                    <td class="text-right"><?php echo isset($persen_locked3[$i]) ? $persen_locked3[$i] : '' ?>%</td>


                </tr>
                <?php
                $i++;
            }while ($rs->next())
            ?>
            <tr>
                <td colspan="3" class="text-center text-bold bg-green-active">Total</td>
                <td class="text-right text-bold bg-green-active"><?php echo $nilai_draft2 ?></td>
                <td class="text-right text-bold bg-green-active">100%</td>
                <td class="text-right text-bold bg-green-active"><?php echo $nilai_locked2 ?></td>
                <td class="text-right text-bold bg-green-active">100%</td>


            </tr>
        </table>       
    </div>
</div>