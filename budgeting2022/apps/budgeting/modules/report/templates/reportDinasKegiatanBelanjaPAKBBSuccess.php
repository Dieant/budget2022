<div class="box box-info">        
    <div class="box-header with-border">
        Laporan Per Dinas Per Kegiatan Per Belanja
    </div>
    <div class="box-body">
        <table class="table table-bordered">
            <tr>
                <th class="text-center" style="width: 10px"></th>
                <th class="text-center" style="width: 10px"></th>
                <th class="text-center">Dinas - Kegiatan - Belanja</th>
<!--                 <th class="text-center">Nilai Usulan</th>
                <th class="text-center">Nilai Disetujui</th> -->
                <th class="text-center">Nilai Semula</th>
                <th class="text-center">Nilai Menjadi</th>
                <th class="text-center">Selisih</th>
            </tr>
            <?php
            $tot_sel = 0;
            $i = 0;
            $z = 0;
            $zz = 0;
            $zzz = 0;
            if ($rs->first() <> '') {
                $rs->first();
                do {
                    if (isset($dinas_ok[$i])):
                        ?>
                        <!-- <tr><td colspan="5">&nbsp;</td></tr> -->
                        <tr><td colspan="6">&nbsp;</td></tr>
                        <tr>
                            <td colspan="3" class="text-left text-bold"><?php echo $unit_id[$i] . ' - ' . $unit_name[$i] ?></td>
                            <td class="text-right text-bold">
                                <?php //nilai per pd semula
                                    echo number_format($nilai_per_pd_semula[$z], 0, ",", ".");
                                    // echo $nilai_usulan_unit[$i];
                                ?>
                            </td>
                            <td class="text-right text-bold">
                                <?php //nilai per pd menjadi
                                    echo number_format($nilai_per_pd_menjadi[$z], 0, ",", "."); 
                                    //echo $nilai_locked[$i];

                                ?>
                            </td>
                            <td class="text-right text-bold">
                                <?php //nilai per pd menjadi
                                    $sel = $nilai_per_pd_semula[$z]-$nilai_per_pd_menjadi[$z];
                                    // $tot_sel += $sel;
                                    echo number_format($nilai_per_pd_semula[$z]-$nilai_per_pd_menjadi[$z], 0, ",", "."); 
                                    //echo $nilai_locked[$i];
                                    $z++;
                                ?>
                            </td>
                            <?php $nilai_locked_total = str_replace('.', '', $nilai_locked[$i]); ?>
                        </tr>
                        <?php
                    endif;
                    if (isset($kegiatan_ok[$i])):
                        ?>
                        <tr>
                            <td style="width: 10px">&nbsp;</td>
                            <td colspan="2" class="text-left text-bold"><?php echo $kode_kegiatan[$i] ?>&nbsp;<?php echo $nama_kegiatan[$i] ?></td>
                            <td class="text-right text-bold">
                            <?php //nilai per kegiatan semula
                                echo number_format($nilai_per_kegiatan_semula[$zz], 0, ",", ".");
                                //echo $nilai_draft3[$i]; 
                            ?>
                            </td>
                            <td class="text-right text-bold">
                            <?php //nilai per kegitan menjadi
                                echo number_format($nilai_per_kegiatan_menjadi[$zz], 0, ",", ".");
                                // echo $nilai_locked3[$i];
                            ?>
                            </td>
                            <td class="text-right text-bold">
                            <?php //nilai per kegitan menjadi
                                echo number_format($nilai_per_kegiatan_semula[$zz]-$nilai_per_kegiatan_menjadi[$zz], 0, ",", ".");
                                // echo $nilai_locked3[$i];
                                $zz++;
                            ?>
                            </td>
                        </tr>
                    <?php endif;
                    ?>
                    <tr>
                        <td colspan="2">&nbsp;</td>
                        <td class="text-left"><?php echo isset($subtitle[$i]) ? $subtitle[$i] : '' ?></td>
                        <td class="text-right">
                        <?php //nilai per belanja semula
                            echo number_format($nilai_per_belanja_semula[$zzz], 0, ",", ".");
                            //echo isset($nilai_draft4[$i]) ? $nilai_draft4[$i] : ''?>
                        </td>
                        <td class="text-right">
                        <?php //nilai per belanja menjadi 
                            echo number_format($nilai_per_belanja_menjadi[$zzz], 0, ",", "."); 
                            //echo isset($nilai_locked4[$i]) ? $nilai_locked4[$i] : '' 
                        ?>
                        </td>
                        <td class="text-right">
                        <?php //nilai per belanja menjadi 
                            echo number_format($nilai_per_belanja_semula[$zzz]-$nilai_per_belanja_menjadi[$zzz], 0, ",", "."); 
                            //echo isset($nilai_locked4[$i]) ? $nilai_locked4[$i] : '' 
                            $zzz++;
                        ?>
                        </td>
                    </tr>
                    <?php
                    $i++;
                } while ($rs->next());
            }
            ?>
            <tr>
                <td colspan="3" class="text-bold text-center bg-green-active">Total</td>
                <td class="text-bold text-right bg-green-active">
                <?php 
                    // echo $nilai_draft2 
                    echo number_format($tot_sem, 0, ",", ".");
                ?>
                </td>
                <td class="text-bold text-right bg-green-active">
                <?php 
                    echo number_format($tot_mjd, 0, ",", "."); 
                ?>
                </td>
                <td class="text-bold text-right bg-green-active">
                <?php 
                    echo number_format($tot_sem-$tot_mjd, 0, ",", "."); 
                ?>
                </td>
            </tr>
        </table>       
    </div>
</div>