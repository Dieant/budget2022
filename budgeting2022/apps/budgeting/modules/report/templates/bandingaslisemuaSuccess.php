<?php use_helper('I18N', 'Date', 'Object', 'Javascript', 'Number', 'Validation') ?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Perubahan Sub Kegiatan - (<?php echo $kode_kegiatan; ?>)</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Lembar Kerja</a></li>
                    <li class="breadcrumb-item active">Perubahan Sub-Kegiatan</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<!-- Main content -->
<!-- Main content -->
<section class="content">
    <?php include_partial('report/list_messages'); ?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                <div class="card-body table-responsive p-0">
                    <table class="table table-borderless table-sm">
                        <tr>
                            <td align='right' width='20%'>Unit Kerja</td>
                            <td width='1%' align='center'>:</td>
                            <td align='left'><?php echo '(' . $unit_id . ')' . '&nbsp;' . $unit_kerja ?></td>
                        </tr>
                        <tr>
                            <td align='right' width='20%'>Nama Sub Kegiatan</td>
                            <td width='1%' align='center'>:</td>
                            <td><?php echo $nama_kegiatan ?></td>
                        </tr>
                        <?php
                        $c = new Criteria();
                        $c->add(MasterUserV2Peer::USER_ID, $user_id_pptk);
                        if ($user_pptk = MasterUserV2Peer::doSelectOne($c))
                            $pptk = $user_pptk->getUserName() . ' (' . $user_pptk->getNip() . '-' . $user_pptk->getJabatan() . ')';

                        $c = new Criteria();
                        $c->add(MasterUserV2Peer::USER_ID, $user_id_kpa);
                        if ($user_kpa = MasterUserV2Peer::doSelectOne($c))
                            $kpa = $user_kpa->getUserName() . ' (' . $user_kpa->getNip() . '-' . $user_kpa->getJabatan() . ')';

                        $c = new Criteria();
                        $c->addJoin(MasterUserV2Peer::USER_ID, SchemaAksesV2Peer::USER_ID);
                        $c->addJoin(MasterUserV2Peer::USER_ID, UserHandleV2Peer::USER_ID);
                        $c->add(MasterUserV2Peer::USER_ENABLE, TRUE);
                        $c->add(SchemaAksesV2Peer::LEVEL_ID, 15);
                        $c->add(UserHandleV2Peer::UNIT_ID, $unit_id);
                        $c->addAscendingOrderByColumn(MasterUserV2Peer::USER_ID);
                        if ($user_pa = MasterUserV2Peer::doSelectOne($c))
                            $pa = $user_pa->getUserName() . ' (' . $user_pa->getNip() . '-' . $user_pa->getJabatan() . ')';
                        ?>
                        <tr>
                            <td align='right' width='20%'>PPTK</td>
                            <td width='1%' align='center'>:</td>
                            <td><?php echo $pptk ?></td>
                        </tr>
                        <tr>
                            <td align='right' width='20%'>KPA</td>
                            <td width='1%' align='center'>:</td>
                            <td><?php echo $kpa ?></td>
                        </tr>
                        <tr>
                            <td align='right' width='20%'>PA</td>
                            <td width='1%' align='center'>:</td>
                            <td><?php echo $pa ?></td>
                        </tr>
                        <?php if ($sf_user->getNamaUser() != 'masger') { ?>
                        <tr>
                            <td align='right' width='20%'>Program</td>
                            <td width='1%' align='center'>:</td>
                            <td><?php echo $kode_program22 . '&nbsp;' . $nama_program22 ?></td>
                        </tr>
                        <tr>
                            <td align='right' width='20%' valign='top'>Target</td>
                            <td width='1%' align='center' valign='top'>:</td>
                            <td valign='top'>
                                <table class="table table-striped" width="100%">
                                    <tr align='center'>
                                        <td><strong>Sub Kegiatan</strong></td>
                                        <td><strong>Output</strong></td>
                                    </tr>
                                    <?php
                                    $c = new Criteria();
                                    $c->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
                                    $c->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
                                    $c->addAscendingOrderByColumn(DinasRincianDetailPeer::SUBTITLE);
                                    $cs = DinasRincianDetailPeer::doSelect($c);
                                    $subtitle = '';
                                    foreach ($cs as $css)
                                    {
                                        if ($css->getSubtitle() != 'Penunjang Kinerja') {
                                            if ($subtitle != $css->getSubtitle()) {
                                                $subtitle = $css->getSubtitle();
                                                $d = new Criteria();
                                                $d->add(DinasSubtitleIndikatorPeer::KEGIATAN_CODE, $kode_kegiatan);
                                                $d->add(DinasSubtitleIndikatorPeer::UNIT_ID, $unit_id);
                                                $d->add(DinasSubtitleIndikatorPeer::SUBTITLE, $subtitle);
                                                $ds = DinasSubtitleIndikatorPeer::doSelectOne($d);
                                                if ($ds)
                                                {
                                    ?>
                                    <tr>
                                        <td><?php echo $ds->getSubtitle(); ?></td>
                                        <td>
                                            <?php
                                            $di = new Criteria();
                                            $di->add(OutputSubtitlePeer::UNIT_ID, $unit_id);
                                            $di->add(OutputSubtitlePeer::KODE_KEGIATAN, $kode_kegiatan);
                                            $vi = OutputSubtitlePeer::doSelect($di);
                                            foreach ($vi as $value):
                                                $kinerja = $value->getOutput();
                                                $kinerja_output = str_replace("|", " ", $kinerja);
                                                echo $kinerja_output."<br/>";
                                            endforeach;
                                            ?>
                                        </td>
                                    </tr>
                                    <?php
                                            }
                                        }
                                    }
                                }
                                $c = new Criteria();
                                $c->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
                                $c->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
                                $c->addAscendingOrderByColumn(DinasRincianDetailPeer::SUBTITLE);
                                $cs = DinasRincianDetailPeer::doSelect($c);
                                $subtitle = '';
                                foreach ($cs as $css) {
                                    if ($css->getSubtitle() == 'Penunjang Kinerja') {
                                        if ($subtitle != $css->getSubtitle()) {
                                            $subtitle = $css->getSubtitle();
                                            ?>
                                    <tr>
                                        <td>
                                            <?php echo $css->getSubtitle(); ?>
                                        </td>
                                        <td>
                                            <?php echo $css->getKeteranganKoefisien(); ?>
                                        </td>
                                    </tr>
                                    <?php
                                        }
                                    }
                                }
                                ?>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align='right' width='20%' valign='top'>Nilai Belanja</td>
                            <td width='1%' align='center' valign='top'>:</td>
                            <td valign='top'>
                                <table class="table table-striped" width="100%">
                                    <tr align='center'>
                                        <td width="40%">&nbsp;</td>
                                        <td width="20%"><strong>Semula</strong></td>
                                        <td width="20%"><strong>Menjadi</strong></td>
                                    </tr>
                                    <?php
                                    $query = " select nilai_sekarang.belanja_name, sum(hasil_kali2) as menjadi, sum(hasil_kali1) as semula
                                            from
                                                ( select k.belanja_name, sum(detail2.nilai_anggaran) as hasil_kali2
                                                    from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "rincian_detail detail2," . sfConfig::get('app_default_schema') . ".rekening r, " . sfConfig::get('app_default_schema') . ".kelompok_belanja k
                                                    where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and r.rekening_code=detail2.rekening_code and k.belanja_id=r.belanja_id and detail2.status_hapus=false
                                                    group by k.belanja_name
                                                ) as nilai_sekarang,
                                                ( 
                                                    select k.belanja_name, sum(detail1.nilai_anggaran) as hasil_kali1
                                                    from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn_semula . "rincian_detail detail1," . sfConfig::get('app_default_schema') . ".rekening r, " . sfConfig::get('app_default_schema') . ".kelompok_belanja k
                                                    where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and r.rekening_code=detail1.rekening_code and k.belanja_id=r.belanja_id and detail1.status_hapus=false
                                                    group by k.belanja_name
                                                ) as nilai_murni

                                            where nilai_sekarang.belanja_name = nilai_murni.belanja_name
                                            group by nilai_sekarang.belanja_name";

                                    $query = "select nilai_murni.belanja_name as belanja_semula, nilai_sekarang.belanja_name as belanja_menjadi, sum(nilai_sekarang.hasil_kali2) as menjadi, sum(nilai_murni.hasil_kali1) as semula
                                            from
                                                ( select k.belanja_name, sum(detail2.nilai_anggaran) as hasil_kali2
                                                from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "rincian_detail detail2," . sfConfig::get('app_default_schema') . ".rekening r, " . sfConfig::get('app_default_schema') . ".kelompok_belanja k
                                                    where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and r.rekening_code=detail2.rekening_code and k.belanja_id=r.belanja_id  and detail2.status_hapus=false
                                                    group by k.belanja_name ) as nilai_sekarang left outer join
                                                    ( select k.belanja_name, sum(detail1.nilai_anggaran) as hasil_kali1
                                                from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn_semula . "rincian_detail detail1," . sfConfig::get('app_default_schema') . ".rekening r, " . sfConfig::get('app_default_schema') . ".kelompok_belanja k
                                                where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and r.rekening_code=detail1.rekening_code and k.belanja_id=r.belanja_id  and detail1.status_hapus=false
                                                    group by k.belanja_name ) as nilai_murni
                                            on nilai_sekarang.belanja_name= nilai_murni.belanja_name

                                            group by nilai_murni.belanja_name, nilai_sekarang.belanja_name";
                                    $con = Propel::getConnection();
                                    $stmt = $con->prepareStatement($query);
                                    $rs_rekening = $stmt->executeQuery();
                                    $rekening_prev = 0;
                                    $rekening_now = 0;
                                    while ($rs_rekening->next()) {
                                        if (!$rs_rekening->getString('belanja_semula')) {
                                            $nama_belanja = $rs_rekening->getString('belanja_menjadi');
                                        } else {
                                            $nama_belanja = $rs_rekening->getString('belanja_semula');
                                        }
                                        ?>
                                        <tr>
                                            <td><?php echo $nama_belanja; ?></td>
                                            <td align='right'><?php
                                                echo number_format($rs_rekening->getString('semula'), 0, ',', '.');
                                                $rekening_prev+=$rs_rekening->getString('semula');
                                                ?></td>
                                            <td align='right'><?php
                                                echo number_format($rs_rekening->getString('menjadi'), 0, ',', '.');
                                                $rekening_now+=$rs_rekening->getString('menjadi');
                                                ?></td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                    <tr>
                                        <td>Total</td>
                                        <td align='right'><?php echo number_format($rekening_prev, 0, ',', '.'); ?></td>
                                        <td align='right'><?php echo number_format($rekening_now, 0, ',', '.'); ?></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <?php } ?>
                    </table>
                </div>
                <div class="card-body table-responsive p-0">
                        <table class="table table-hover table-sm">
                            <thead class="head_peach">
                                <th colspan='7'><strong>SEMULA</strong></th>
                                <th><strong>|</strong></th>
                                <th colspan='8'><strong>MENJADI</strong></th>
                                <th >&nbsp;</th>
                            </thead>
                            <thead class="head_peach">
                                <th>Komponen</th>
                                <th>Satuan</th>
                                <th>Koefisien</th>
                                <th>Harga</th>
                                <th>Hasil</th>
                                <th>PPN</th>
                                <th>Total</th>
                                <th align='center'>|</th>
                                <th>Komponen</th>
                                <th>Satuan</th>
                                <th>Koefisien</th>
                                <th>Harga</th>
                                <th>Hasil</th>
                                <th>PPN</th>
                                <th>Total</th>
                                <th>Selisih</th>
                                <th>Catatan</th>
                            </thead>
                            <tbody>
                                <?php
                                    $jumlah_harga_prev = 0;
                                    $jumlah_harga = 0;
                                    $query = "
                                        (select detail2.subtitle as subtitle2, detail.subtitle as subtitle1
                                        from " . sfConfig::get('app_default_schema') . ".dinas_subtitle_indikator detail2, " . sfConfig::get('app_default_schema') . ".subtitle_indikator detail
                                        where
                                        detail2.unit_id='$unit_id' and detail2.kegiatan_code='$kode_kegiatan' and detail2.kegiatan_code=detail.kegiatan_code and detail2.unit_id=detail.unit_id
                                        and detail2.subtitle=detail.subtitle)

                                        union

                                        (select '', detail.subtitle as subtitle1
                                        from  " . sfConfig::get('app_default_schema') . ".subtitle_indikator detail
                                        where
                                        detail.unit_id='$unit_id' and detail.kegiatan_code='$kode_kegiatan' and detail.subtitle not in (select subtitle from " . sfConfig::get('app_default_schema') . ".dinas_subtitle_indikator detail2
                                        where detail2.unit_id='$unit_id' and detail2.kegiatan_code='$kode_kegiatan'))

                                        union

                                        (select detail2.subtitle as subtitle2, '' as subtitle1
                                        from  " . sfConfig::get('app_default_schema') . ".dinas_subtitle_indikator detail2
                                        where
                                        detail2.unit_id='$unit_id' and detail2.kegiatan_code='$kode_kegiatan' and detail2.subtitle not in (select subtitle from " . sfConfig::get('app_default_schema') . ".subtitle_indikator detail
                                        where detail.unit_id='$unit_id' and detail.kegiatan_code='$kode_kegiatan'))
                                        order by subtitle2";
                                    $con = Propel::getConnection();
                                    $stmt = $con->prepareStatement($query);
                                    $rs = $stmt->executeQuery();
                                    while ($rs->next())
                                    {
                                        $subtitle_prev = '';
                                        $subtitle = '';
                                        $subtitle_prev = $rs->getString('subtitle1');
                                        $subtitle = $rs->getString('subtitle2');

                                        $j = 0;
                                        $kosong = 'kanan';
                                        if ($subtitle_prev == $subtitle) {
                                            $kosong = 'tidakada';
                                            $query2 = " (
                                                select distinct detail2.rekening_code as rekening, detail.rekening_code as rekening_prev,r.rekening_name
                                                from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "rincian_detail detail2 right outer join " . sfConfig::get('app_default_schema') . "." . $tabel_dpn_semula . "rincian_detail detail
                                                on detail2.unit_id=detail.unit_id and detail2.kegiatan_code=detail.kegiatan_code and detail2.detail_no=detail.detail_no
                                                , " . sfConfig::get('app_default_schema') . ".rekening r
                                                where detail.unit_id='$unit_id' and detail.kegiatan_code='$kode_kegiatan' and detail.subtitle ilike '$subtitle' and detail.rekening_code=r.rekening_code 
                                                order by detail2.rekening_code,detail.rekening_code
                                                )
                                                union
                                                (
                                                select distinct detail2.rekening_code as rekening, detail2.rekening_code as rekening_prev,r.rekening_name
                                                from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "rincian_detail detail2 , " . sfConfig::get('app_default_schema') . ".rekening r
                                                where  detail2.unit_id='$unit_id' and detail2.kegiatan_code='$kode_kegiatan' and detail2.subtitle ilike '$subtitle' and detail2.rekening_code=r.rekening_code and detail2.status_hapus=false and detail2.detail_no not in
                                                (
                                                 select detail_no
                                                from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn_semula . "rincian_detail detail
                                                where detail.unit_id='$unit_id' and detail.kegiatan_code='$kode_kegiatan' and detail.subtitle ilike '$subtitle'  and detail.status_hapus=false
                                                )
                                                order by detail2.rekening_code
                                                )
                                                union
                                                (
                                                select distinct detail2.rekening_code as rekening, detail2.rekening_code as rekening_prev,r.rekening_name
                                                from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "rincian_detail detail2 , " . sfConfig::get('app_default_schema') . ".rekening r
                                                where  detail2.unit_id='$unit_id' and detail2.kegiatan_code='$kode_kegiatan' and detail2.subtitle ilike '$subtitle' and detail2.rekening_code=r.rekening_code and detail2.status_hapus=false and (detail2.detail_no,detail2.rekening_code) not in
                                                (
                                                 select detail_no, rekening_code
                                                from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn_semula . "rincian_detail detail
                                                where detail.unit_id='$unit_id' and detail.kegiatan_code='$kode_kegiatan' and detail.subtitle ilike '$subtitle'  and detail.status_hapus=false
                                                )
                                                order by detail2.rekening_code
                                                )
                                                order by rekening";
                                        } elseif ($rs->getString('subtitle2')) {
                                            $kosong = 'kiri';
                                            $query2 = "select distinct(detail.rekening_code), detail.subtitle, rekening.rekening_name, detail.rekening_code as rekening, '' as rekening_prev
                                                        from
                                                            " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "rincian_detail detail," . sfConfig::get('app_default_schema') . ".rekening rekening
                                                        where
                                                        detail.kegiatan_code = '" . $kode_kegiatan . "' and detail.unit_id = '" . $unit_id . "' and detail.subtitle ilike '" . $subtitle . "'
                                                        and detail.rekening_code=rekening.rekening_code and detail.status_hapus=false order by detail.rekening_code"; 
                                        } else {
                                            $kosong = 'kanan';
                                            $query2 = "select distinct(detail.rekening_code), detail.subtitle, rekening.rekening_name, '' as rekening, detail.rekening_code as rekening_prev
                                                        from
                                                            " . sfConfig::get('app_default_schema') . "." . $tabel_dpn_semula . "rincian_detail detail," . sfConfig::get('app_default_schema') . ".rekening rekening
                                                        where
                                                        detail.kegiatan_code = '" . $kode_kegiatan . "' and detail.unit_id = '" . $unit_id . "' and detail.subtitle ilike '" . $subtitle_prev . "'
                                                        and detail.rekening_code=rekening.rekening_code and detail.status_hapus=false order by detail.rekening_code";
                                        }

                                        $stmt2 = $con->prepareStatement($query2);
                                        $rs2 = $stmt2->executeQuery();
                                        if ($subtitle_prev <> '') {
                                            echo '<tr align="left" bgcolor="#ffffff"><td colspan="7"><span class="style3"><strong> :: ' . $subtitle_prev . '</strong></span></td>';
                                            echo '<td><strong><span>&nbsp;</span></strong></td>';
                                            echo '<td colspan="9"><span class="style3"><strong> :: ' . $subtitle . '</strong></span></td></tr>';
                                        } else {
                                            echo '<tr align="left" bgcolor="#ffffff"><td colspan="7"><span class="style3"><strong>&nbsp;</strong></span></td>';
                                            echo '<td><strong><span style="color:red;">!</span></strong></td>';
                                            echo '<td colspan="9"><span class="style3"><strong> :: ' . $subtitle . '</strong></span></td></tr>';
                                        }
                                        $total_semua = 0;
                                        $total_semua_n = 0;
                                        while ($rs2->next()) 
                                        {
                                            if ($rs2->getString('rekening_prev') == '') {
                                                $rekening_prev = '';
                                            }
                                            if ($rs2->getString('rekening_prev') != '') {
                                                $rekening_prev = $rs2->getString('rekening_prev') . '&nbsp;' . $rs2->getString('rekening_name');
                                            }
                                            if ($rs2->getString('rekening_prev') != $rs2->getString('rekening')) {
                                                $rekening_prev = $rs2->getString('rekening_prev');
                                            }
                                            $rekening = $rs2->getString('rekening') . '&nbsp;' . $rs2->getString('rekening_name');

                                            if ($kosong == 'tidakada') {
                                                $rekening_prev = $rs2->getString('rekening_prev') . '&nbsp;' . $rs2->getString('rekening_name');
                                                $rekening = $rs2->getString('rekening') . '&nbsp;' . $rs2->getString('rekening_name');
                                                if ($rs2->getString('rekening')) {
                                                    $kode_rekening = $rs2->getString('rekening');
                                                } else {
                                                    $kode_rekening = $rs2->getString('rekening_prev');
                                                }
                                                $query3 = "
												(
                                                SELECT
                                                    rekening.rekening_code,rekening.rekening_code as rekening_code2,
                                                    rekening.rekening_name as nama_rekening2,
                                                    detail2.komponen_name || ' ' || detail2.detail_name as detail_name22,
                                                    detail2.komponen_harga_awal as detail_harga2,
                                                    detail2.pajak as pajak2,
                                                    detail2.komponen_id as komponen_id2,
                                                    detail2.detail_name as detail_name2,
                                                    detail2.komponen_name as komponen_name2,
                                                    detail2.volume as volume2,
                                                    detail2.subtitle as subtitle_name2,
                                                    detail2.satuan as detail_satuan2,
                                                    replace(detail2.keterangan_koefisien,'<*>','X') as keterangan_koefisien2,
                                                    detail2.detail_no as detail_no2,
                                                    detail2.volume * detail2.komponen_harga_awal as hasil2,
                                                    (detail2.nilai_anggaran) as hasil_kali2,

                                                    detail.komponen_name || ' ' || detail.detail_name as detail_name2,
                                                    detail.komponen_harga_awal as detail_harga,
                                                    detail.pajak as pajak,
                                                    detail.komponen_id as komponen_id,
                                                    detail.detail_name as detail_name,
                                                    detail.komponen_name as komponen_name,
                                                    detail.volume as volume,
                                                    detail.subtitle as subtitle_name,
                                                    detail.satuan as detail_satuan,
                                                    replace(detail.keterangan_koefisien,'<*>','X') as keterangan_koefisien,
                                                    detail.detail_no as detail_no,
                                                    detail.volume * detail.komponen_harga_awal as hasil,
                                                    (detail.nilai_anggaran) as hasil_kali,
                                                    detail2.detail_name as detail_name_rd,
                                                    detail2.sub as sub2,
                                                    detail.sub,
                                                        detail2.note_skpd as note_skpd,
                                                    detail2.status_lelang,
                                                    detail2.is_hpsp,
                                                    detail2.is_musrenbang,
                                                    detail2.is_output,
                                                    detail2.prioritas_wali

                                                FROM
                                                    " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "rincian_detail detail2, " . sfConfig::get('app_default_schema') . ".rekening rekening, " . sfConfig::get('app_default_schema') . "." . $tabel_dpn_semula . "rincian_detail detail

                                                WHERE
                                                    detail2.kegiatan_code = '$kode_kegiatan' and
                                                    detail2.unit_id='$unit_id'

                                                    and detail2.rekening_code='$kode_rekening'
                                                    and detail2.subtitle ilike '$subtitle'
                                                    and detail2.kegiatan_code=detail.kegiatan_code
                                                    and detail2.unit_id=detail.unit_id
                                                    and detail2.subtitle=detail.subtitle
                                                    and detail2.rekening_code=detail.rekening_code
                                                    and rekening.rekening_code=detail2.rekening_code
                                                    and detail2.detail_no=detail.detail_no
                                                        and detail.status_hapus<>true
                                                        and detail2.status_hapus<>true
                                                        

                                                ORDER BY

                                                    rekening.rekening_code,
                                                    detail2.subtitle ,
                                                    detail2.komponen_name
                                                )
                                                union
                                                (
                                                SELECT

                                                    rekening.rekening_code,rekening.rekening_code as rekening_code2,
                                                    rekening.rekening_name as nama_rekening2,
                                                    detail2.komponen_name || ' ' || detail2.detail_name as detail_name22,
                                                    detail2.komponen_harga_awal as detail_harga2,
                                                    detail2.pajak as pajak2,
                                                    detail2.komponen_id as komponen_id2,
                                                    detail2.detail_name as detail_name2,
                                                    detail2.komponen_name as komponen_name2,
                                                    detail2.volume as volume2,
                                                    detail2.subtitle as subtitle_name2,
                                                    detail2.satuan as detail_satuan2,
                                                    replace(detail2.keterangan_koefisien,'<*>','X') as keterangan_koefisien2,
                                                    detail2.detail_no as detail_no2,
                                                    detail2.volume * detail2.komponen_harga_awal as hasil2,
                                                    (detail2.nilai_anggaran) as hasil_kali2,

                                                    '' as detail_name,
                                                    0 as detail_harga,
                                                    0 as pajak,
                                                    '' as komponen_id,
                                                    '' as detail_name,
                                                    '' as komponen_name,
                                                    0 as volume,
                                                    '' as subtitle_name,
                                                    '' as detail_satuan,
                                                    '' as keterangan_koefisien,
                                                    0 as detail_no,
                                                    0 as hasil,
                                                    0 as hasil_kali,
                                                    detail2.detail_name as detail_name_rd,
                                                    detail2.sub as sub2,
                                                    '' as sub,
                                                        detail2.note_skpd as note_skpd,
                                                    detail2.status_lelang,
                                                    detail2.is_hpsp,
                                                    detail2.is_musrenbang,
                                                    detail2.is_output,
                                                    detail2.prioritas_wali

                                                FROM
                                                    " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "rincian_detail detail2, " . sfConfig::get('app_default_schema') . ".rekening rekening, " . sfConfig::get('app_default_schema') . "." . $tabel_dpn_semula . "rincian_detail detail

                                                WHERE
                                                    detail2.kegiatan_code = '$kode_kegiatan' and
                                                    detail2.unit_id='$unit_id'

                                                    and detail2.rekening_code='$kode_rekening'
                                                    and detail2.subtitle ilike '$subtitle'
                                                    and detail2.kegiatan_code=detail.kegiatan_code
                                                    and detail2.unit_id=detail.unit_id
                                                    and detail2.subtitle=detail.subtitle
                                                    and detail2.rekening_code=detail.rekening_code
                                                    and rekening.rekening_code=detail2.rekening_code
                                                    and detail2.detail_no=detail.detail_no
                                                        and detail.status_hapus=true
                                                        and detail2.status_hapus<>true
                                                        

                                                ORDER BY

                                                    rekening.rekening_code,
                                                    detail2.subtitle ,
                                                    detail2.komponen_name
                                                )
                                                union
                                                (
                                                SELECT

                                                    rekening.rekening_code,rekening.rekening_code as rekening_code2,
                                                    rekening.rekening_name as nama_rekening2,
                                                    detail2.komponen_name || ' ' || detail2.detail_name as detail_name22,
                                                    detail2.komponen_harga_awal as detail_harga2,
                                                    detail2.pajak as pajak2,
                                                    detail2.komponen_id as komponen_id2,
                                                    detail2.detail_name as detail_name2,
                                                    detail2.komponen_name as komponen_name2,
                                                    detail2.volume as volume2,
                                                    detail2.subtitle as subtitle_name2,
                                                    detail2.satuan as detail_satuan2,
                                                    replace(detail2.keterangan_koefisien,'<*>','X') as keterangan_koefisien2,
                                                    detail2.detail_no as detail_no2,
                                                    detail2.volume * detail2.komponen_harga_awal as hasil2,
                                                    (detail2.nilai_anggaran) as hasil_kali2,

                                                    '' as detail_name,
                                                    0 as detail_harga,
                                                    0 as pajak,
                                                    '' as komponen_id,
                                                    '' as detail_name,
                                                    '' as komponen_name,
                                                    0 as volume,
                                                    '' as subtitle_name,
                                                    '' as detail_satuan,
                                                    '' as keterangan_koefisien,
                                                    0 as detail_no,
                                                    0 as hasil,
                                                    0 as hasil_kali,
                                                    detail2.detail_name as detail_name_rd,
                                                    detail2.sub as sub2,
                                                    '' as sub,
                                                        detail2.note_skpd as note_skpd,
                                                    detail2.status_lelang,
                                                    detail2.is_hpsp,
                                                    detail2.is_musrenbang,
                                                    detail2.is_output,
                                                    detail2.prioritas_wali

                                                FROM
                                                    " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "rincian_detail detail2, " . sfConfig::get('app_default_schema') . ".rekening rekening

                                                WHERE
                                                    detail2.kegiatan_code = '$kode_kegiatan' and
                                                    detail2.unit_id='$unit_id'

                                                        and detail2.status_hapus=false
                                                    and detail2.rekening_code='$kode_rekening'
                                                    and detail2.subtitle ilike '$subtitle'
                                                    and rekening.rekening_code=detail2.rekening_code
                                                    and (detail2.unit_id||detail2.kegiatan_code||detail2.detail_no) not in
                                                    (
                                                    select (detail.unit_id||detail.kegiatan_code||detail.detail_no) from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn_semula . "rincian_detail detail where detail.kegiatan_code = '$kode_kegiatan' and
                                                    detail.unit_id='$unit_id' and detail.subtitle ilike '$subtitle'
                                                    and detail.rekening_code='$kode_rekening'
                                                    )

                                                ORDER BY

                                                    rekening.rekening_code,
                                                    detail2.subtitle ,
                                                    detail2.komponen_name)
                                                union
                                                (
                                                SELECT

                                                    rekening.rekening_code,rekening.rekening_code as rekening_code2,
                                                    rekening.rekening_name as nama_rekening2,
                                                    '' as detail_name22,
                                                    0 as detail_harga2,
                                                    0 as pajak2,
                                                    '' as komponen_id2,
                                                    '' as detail_name2,
                                                    '' as komponen_name2,
                                                    0 as volume2,
                                                    '' as subtitle_name2,
                                                    '' as detail_satuan2,
                                                    '' as keterangan_koefisien2,
                                                    0 as detail_no2,
                                                    0 as hasil2,
                                                    0 as hasil_kali2,

                                                    detail.komponen_name || ' ' || detail.detail_name as detail_name2,
                                                    detail.komponen_harga_awal as detail_harga,
                                                    detail.pajak as pajak,
                                                    detail.komponen_id as komponen_id,
                                                    detail.detail_name as detail_name,
                                                    detail.komponen_name as komponen_name,
                                                    detail.volume as volume,
                                                    detail.subtitle as subtitle_name,
                                                    detail.satuan as detail_satuan,
                                                    replace(detail.keterangan_koefisien,'<*>','X') as keterangan_koefisien,
                                                    detail.detail_no as detail_no,
                                                    detail.volume * detail.komponen_harga_awal as hasil,
                                                    (detail.nilai_anggaran) as hasil_kali,
                                                    '' as detail_name_rd,
                                                    '' as sub2,
                                                    detail.sub,
                                                        '' as note_skpd,
                                                    null,
                                                    null,
                                                    null,
                                                    null,
                                                    null

                                                FROM
                                                    " . sfConfig::get('app_default_schema') . "." . $tabel_dpn_semula . "rincian_detail detail, " . sfConfig::get('app_default_schema') . ".rekening rekening

                                                WHERE
                                                    detail.kegiatan_code = '$kode_kegiatan' and
                                                    detail.unit_id='$unit_id'

                                                        and detail.status_hapus=false
                                                    and detail.rekening_code='$kode_rekening'
                                                    and detail.subtitle ilike '$subtitle'
                                                    and rekening.rekening_code=detail.rekening_code
                                                    and (detail.unit_id||detail.kegiatan_code||detail.detail_no) not in
                                                    (
                                                    select (detail2.unit_id||detail2.kegiatan_code||detail2.detail_no) from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "rincian_detail detail2 where detail2.kegiatan_code = '$kode_kegiatan' and
                                                    detail2.unit_id='$unit_id' and detail2.subtitle ilike '$subtitle' and detail2.status_hapus<>true
                                                    and detail2.rekening_code='$kode_rekening'
                                                    )

                                                ORDER BY

                                                    rekening.rekening_code,
                                                    detail.subtitle ,
                                                    detail.komponen_name)
                                                ORDER BY sub2 ASC, sub DESC";
                                            } elseif ($kosong == 'kiri') {
                                                $rekening_prev = '';
                                                $rekening = $rs2->getString('rekening') . '&nbsp;' . $rs2->getString('rekening_name');
                                                $kode_rekening = $rs2->getString('rekening');
                                                $query3 = "SELECT
                                                            rekening.rekening_code,rekening.rekening_code as rekening_code2,
                                                            rekening.rekening_name as nama_rekening2,
                                                            detail2.komponen_name || ' ' || detail2.detail_name as detail_name22,
                                                            detail2.komponen_harga_awal as detail_harga2,
                                                            detail2.pajak as pajak2,
                                                            detail2.komponen_id as komponen_id2,
                                                            detail2.detail_name as detail_name2,
                                                            detail2.komponen_name as komponen_name2,
                                                            detail2.volume as volume2,
                                                            detail2.subtitle as subtitle_name2,
                                                            detail2.satuan as detail_satuan2,
                                                            replace(detail2.keterangan_koefisien,'<*>','X') as keterangan_koefisien2,
                                                            detail2.detail_no as detail_no2,
                                                            detail2.volume * detail2.komponen_harga_awal as hasil2,
                                                            (detail2.nilai_anggaran) as hasil_kali2,
                                                            detail2.detail_name as detail_name_rd,
                                                            '' as detail_name2,
                                                            0 as detail_harga,
                                                            0 as pajak,
                                                            '' as komponen_id,
                                                            '' as detail_name,
                                                            '' as komponen_name,
                                                            0 as volume,
                                                            '' as subtitle_name,
                                                            '' as detail_satuan,
                                                            '' as keterangan_koefisien,
                                                            '' as detail_no,
                                                            0 as hasil,
                                                            0 as hasil_kali,
                                                            '' as sub2,
                                                            detail2.sub,
                                                            detail2.note_skpd as note_skpd
                                                            FROM
                                                            (" . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "rincian_detail detail2 inner join
                                                            " . sfConfig::get('app_default_schema') . ".rekening rekening on rekening.rekening_code = detail2.rekening_code)
                                                        WHERE
                                                            detail2.kegiatan_code = '" . $kode_kegiatan . "' and
                                                            detail2.unit_id='" . $unit_id . "'

                                                            and detail2.rekening_code='" . $kode_rekening . "'
                                                            and detail2.subtitle ilike '$subtitle'
                                                            and detail2.status_hapus=false
                                                        ORDER BY
                                                            rekening.rekening_code,
                                                            detail2.subtitle ,
                                                            detail2.komponen_name,
                                                            sub DESC";
                                            } elseif ($kosong == 'kanan') {
                                                $rekening_prev = $rs2->getString('rekening') . '&nbsp;' . $rs2->getString('rekening_name');
                                                $rekening = '';
                                                $kode_rekening = $rs2->getString('rekening_prev');
                                                $query3 = "SELECT
                                                            rekening.rekening_code,rekening.rekening_code as rekening_code2,
                                                            rekening.rekening_name as nama_rekening2,
                                                            '' as detail_name22,
                                                            0 as detail_harga2,
                                                            0 as pajak2,
                                                            '' as komponen_id2,
                                                            '' as detail_name2,
                                                            '' as komponen_name2,
                                                            0 as volume2,
                                                            '' as subtitle_name2,
                                                            '' as detail_satuan2,
                                                            '' as keterangan_koefisien2,
                                                            0 as detail_no2,
                                                            0 as hasil2,
                                                            0 as hasil_kali2,
                                                            detail.komponen_name || ' ' || detail.detail_name as detail_name2,
                                                            detail.komponen_harga_awal as detail_harga,
                                                            detail.pajak as pajak,
                                                            detail.komponen_id as komponen_id,
                                                            detail.detail_name as detail_name,
                                                            detail.komponen_name as komponen_name,
                                                            detail.volume as volume,
                                                            detail.subtitle as subtitle_name,
                                                            detail.satuan as detail_satuan,
                                                            replace(detail.keterangan_koefisien,'<*>','X') as keterangan_koefisien,
                                                            detail.detail_no as detail_no,
                                                            detail.volume * detail.komponen_harga_awal as hasil,
                                                            (detail.nilai_anggaran) as hasil_kali,
                                                            detail2.detail_name as detail_name_rd,
                                                            '' as sub2,
                                                            detail.sub, 
                                                            detail2.note_skpd as note_skpd 
                                                        FROM
                                                            (" . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "rincian_detail detail2 inner join
                                                            " . sfConfig::get('app_default_schema') . ".rekening rekening on rekening.rekening_code = detail2.rekening_code) left outer join
                                                            " . sfConfig::get('app_default_schema') . "." . $tabel_dpn_semula . "rincian_detail detail on detail2.unit_id=detail.unit_id and detail2.kegiatan_code=detail.kegiatan_code and detail2.detail_no=detail.detail_no
                                                        WHERE
                                                            detail.kegiatan_code = '" . $kode_kegiatan . "' and
                                                            detail.unit_id='" . $unit_id . "'
                                                            and detail.rekening_code='" . $kode_rekening . "'
                                                            and detail.subtitle ilike '$subtitle_prev' 
                                                            and detail2.status_hapus=false
                                                            and detail.status_hapus=false
                                                        ORDER BY
                                                            rekening.rekening_code,
                                                            detail2.subtitle ,
                                                            detail2.komponen_name, 
                                                            sub2 ASC, sub DESC";
                                                $subtitle = $subtitle_prev;
                                            }

                                            if ($rs2->getString('rekening_prev')) {
                                                if (substr($rekening_prev, 0, 6) != $kode_belanja) {
                                                    $query = " select belanja_code, belanja_name from " . sfConfig::get('app_default_schema') . ".kelompok_belanja "
                                                            . " where belanja_code='" . substr($rekening_prev, 0, 5) . "'";
                                                    $con = Propel::getConnection();
                                                    $stmt = $con->prepareStatement($query);
                                                    $rs1 = $stmt->executeQuery();
                                                    if ($rs1->next()) {
                                                        echo '<tr bgcolor="#ffffff"><td colspan="18" align="left"><span class="style3"><strong>::: ' . $rs1->getString('belanja_code') . ' ' . $rs1->getString('belanja_name') . '</strong></span></td>';
                                                    }
                                                }
                                                $kode_belanja = substr($rekening_prev, 0, 6);

                                                $ada_kosong = 0;
                                                $query = "select count(*) as tot from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "rincian_detail rd,  " . sfConfig::get('app_default_schema') . "." . $tabel_dpn_semula . "rincian_detail prd 
                                                where rd.unit_id = '$unit_id' and rd.kegiatan_code = '$kode_kegiatan' and rd.rekening_code = '" . $rs2->getString('rekening') . "' and rd.status_hapus = false and prd.status_hapus = false and rd.unit_id = prd.unit_id and rd.kegiatan_code = prd.kegiatan_code and rd.detail_no = prd.detail_no and prd.rekening_code = '" . $rs2->getString('rekening_prev') . "' 
                                                and rd.subtitle = '$subtitle' and prd.subtitle = '$subtitle_prev'";
                                                $con = Propel::getConnection();
                                                $stmt = $con->prepareStatement($query);
                                                $rs1 = $stmt->executeQuery();
                                                while ($rs1->next()) {
                                                    $ada_kosong_1 = $rs1->getString('tot');
                                                }
                                                $query = "select count(*) as tot from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "rincian_detail rd 
                                                where rd.unit_id = '$unit_id' and rd.kegiatan_code = '$kode_kegiatan' and rd.rekening_code = '" . $rs2->getString('rekening') . "' and rd.status_hapus = false 
                                                    and rd.subtitle = '$subtitle' ";
                                                $con = Propel::getConnection();
                                                $stmt = $con->prepareStatement($query);
                                                $rs1 = $stmt->executeQuery();
                                                while ($rs1->next()) {
                                                    $ada_kosong_2 = $rs1->getString('tot');
                                                }
                                                $query = "select count(*) as tot from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn_semula . "rincian_detail rd 
                                                where rd.unit_id = '$unit_id' and rd.kegiatan_code = '$kode_kegiatan' and rd.rekening_code = '" . $rs2->getString('rekening') . "' and rd.status_hapus = false 
                                                    and rd.subtitle = '$subtitle' ";
                                                $con = Propel::getConnection();
                                                $stmt = $con->prepareStatement($query);
                                                $rs1 = $stmt->executeQuery();
                                                while ($rs1->next()) {
                                                    $ada_kosong_3 = $rs1->getString('tot');
                                                }
                                                $ada_kosong = $ada_kosong_1 + $ada_kosong_2 + $ada_kosong_3; 
                                                if ($ada_kosong > 0) {
                                                    echo '<tr bgcolor="#e6e6e6"><td colspan="7" align="left"><span class="style3"><strong>' . $rekening_prev . '</strong></span></td>'; 
                                                    echo '<td>&nbsp;</td>';
                                                    echo '<td colspan="9" align="left"><span class="style3"><strong>' . $rekening . '</strong></span></td></tr>';
                                                } 
                                            } else if ($rs2->getString('rekening_prev') == '') {
                                                if (substr($rekening, 0, 6) != $kode_belanja) {
                                                    $query = " select belanja_code, belanja_name from " . sfConfig::get('app_default_schema') . ".kelompok_belanja "
                                                            . " where belanja_code='" . substr($rekening, 0, 6) . "'";
                                                    $con = Propel::getConnection();
                                                    $stmt = $con->prepareStatement($query);
                                                    $rs1 = $stmt->executeQuery();
                                                    if ($rs1->next()) {
                                                        echo '<tr bgcolor="#ffffff"><td colspan="18" align="left"><span class="style3"><strong>::: ' . $rs1->getString('belanja_code') . ' ' . $rs1->getString('belanja_name') . '</strong></span></td>';
                                                    }
                                                }
                                                $kode_belanja = substr($rekening, 0, 6);
                                                echo '<tr bgcolor="#e6e6e6"><td colspan="7" align="left"><span class="style3"><strong>&nbsp;</strong></span></td>';
                                                echo '<td><strong><span style="color:red;">!</span></strong></td>';
                                                echo '<td colspan="9" align="left"><span class="style3"><strong>' . $rekening . '</strong></span></td></tr>';
                                            }
                                            $stmt3 = $con->prepareStatement($query3);
                                            $rs3 = $stmt3->executeQuery();
                                            $total = 0;
                                            $total_n = 0;

                                            while ($rs3->next()) 
                                            {
                                                //prev_rincian_detail
                                                if ($rs2->getString('rekening_prev')) 
                                                {
                                                    if (($cek_sub != $rs3->getString('sub') || $cek_sub2 != $rs3->getString('sub2')) && ($rs3->getString('sub') != '' || $rs3->getString('sub2') != '') && ($rs3->getString('sub') != '' || $rs3->getString('sub2') != $cek_sub2)) {
                                                        echo '<tr bgcolor="#ffffff"><td colspan="7" align="left"><span class="style3">' . $rs3->getString('sub') . '</span></td>';
                                                        echo '<td></td>';
                                                        echo '<td colspan="12" align="left"><span class="style3">' . $rs3->getString('sub2') . '</span></td></tr>';
                                                    }
                                                    $cek_sub = $rs3->getString('sub');
                                                    $cek_sub2 = $rs3->getString('sub2');
                                                    $hasil = 0;
                                                    $detail_harga = 0;
                                                    $volume = 0;
                                                    $pajak = 0;
                                                    $detail_harga = $rs3->getString('detail_harga');
                                                    $volume = $rs3->getString('volume');
                                                    $pajak = $rs3->getString('pajak');
                                                    $subnya = '';
                                                    if (($rs3->getString('komponen_name') != $rs3->getString('komponen_name2')) ||
                                                            ($rs3->getString('detail_name22') != $rs3->getString('detail_name2')) ||
                                                            ($rs3->getString('detail_satuan') != $rs3->getString('detail_satuan2')) ||
                                                            ($rs3->getString('keterangan_koefisien') != $rs3->getString('keterangan_koefisien2')) ||
                                                            ($rs3->getString('pajak') != $rs3->getString('pajak2')) ||
                                                            ($rs3->getString('detail_harga2') != $rs3->getString('detail_harga')) ||
                                                            ($rs3->getString('volume') != $rs3->getString('volume2')))
                                                            {
                                                                if ($sf_user->getNamaUser() != 'parlemen2') {
                                                                    echo '<tr bgcolor="#ffffff" valign="top">';
                                                                    echo '<td class="Font8v" align="left"><strong><span class="style3">' . $subnya . $rs3->getString('komponen_name') . ' ' . $rs3->getString('detail_name') . '</span></strong></td>';
                                                                    echo '<td align="center" nowrap="nowrap"><strong><span class="Font8v">' . $rs3->getString('detail_satuan') . '</span></strong></td>';
                                                                    echo '<td align="center"><strong><span class="Font8v">' . $rs3->getString('keterangan_koefisien') . '=' . $rs3->getString('volume') . ' ' . $rs3->getString('detail_satuan') . '</span></strong></td>';
                                                                    echo '<td align="right" nowrap="nowrap"><strong>';
                                                                    if ($rs3->getString('detail_satuan') == '%') {
                                                                        echo $rs3->getString('detail_harga');
                                                                    } else {
                                                                        echo number_format($rs3->getString('detail_harga'), 0, ',', '.');
                                                                    }
                                                                    echo '</strong></td>';
                                                                    $hasil = myTools::round_ganjil_genap($detail_harga * $volume);
                                                                    echo '<td align="right" nowrap="nowrap"><strong>' . number_format($hasil, 0, ',', '.') . '</strong></td>';
                                                                    echo '<td align="right" nowrap="nowrap"><strong>' . $rs3->getString('pajak') . '%</strong></td>';
                                                                    $total1 = $hasil + (($pajak / 100) * $hasil);
                                                                    echo '<td align="right" nowrap="nowrap"><strong>' . number_format($rs3->getString('hasil_kali'), 0, ',', '.') . '</strong></td>';
                                                                    $total1 = $rs3->getString('hasil_kali');
                                                                    $total = $total + $total1;
                                                                    $jumlah_harga_prev += $hasil;
                                                                }
                                                            } else {
                                                                if ($sf_user->getNamaUser() != 'parlemen2') {
                                                                    echo '<tr bgcolor="#ffffff" valign="top">';
                                                                    echo '<td class="Font8v" align="left"><span class="style3">' . $subnya . $rs3->getString('komponen_name') . ' ' . $rs3->getString('detail_name') . '</span></td>';
                                                                    echo '<td align="center" nowrap="nowrap"><span class="Font8v">' . $rs3->getString('detail_satuan') . '</span></td>';
                                                                    echo '<td align="center"><span class="Font8v">' . $rs3->getString('keterangan_koefisien') . '=' . $rs3->getString('volume') . ' ' . $rs3->getString('detail_satuan') . '</span></td>';
                                                                    echo '<td align="right" nowrap="nowrap">';
                                                                    if ($rs3->getString('detail_satuan') == '%') {
                                                                        echo $rs3->getString('detail_harga');
                                                                    } else {
                                                                        echo number_format($rs3->getString('detail_harga'), 0, ',', '.');
                                                                    }
                                                                    echo '</td>';
                                                                    // nilai hasil pada bagian semula yang tidak mengalami perubahan
                                                                    $hasil = myTools::round_ganjil_genap($detail_harga * $volume);
                                                                    //echo '<td align="right" nowrap="nowrap">' . $hasil . '</td>';
                                                                    echo '<td align="right" nowrap="nowrap">' . number_format($hasil, 0, ',', '.') . '</td>';
                                                                    echo '<td align="right" nowrap="nowrap">' . $rs3->getString('pajak') . '%</td>';
                                                                    $total1 = $hasil + (($pajak / 100) * $hasil);
                                                                    // nilai total pada bagian semula yang tidak mengalami perubahan
                                                                    echo '<td align="right" nowrap="nowrap">' . number_format($rs3->getString('hasil_kali'), 0, ',', '.') . '</td>';
                                                                    $total = $total + $total1;
                                                                    $jumlah_harga_prev += $hasil;
                                                                }
                                                            }
                                                    $kiri = $total1;
                                                    //tanda perbandingan
                                                    if (($rs3->getString('komponen_name') != $rs3->getString('komponen_name2')) ||
                                                            ($rs3->getString('detail_name22') != $rs3->getString('detail_name2')) ||
                                                            ($rs3->getString('detail_satuan') != $rs3->getString('detail_satuan2')) ||
                                                            ($rs3->getString('keterangan_koefisien') != $rs3->getString('keterangan_koefisien2')) ||
                                                            ($rs3->getString('pajak') != $rs3->getString('pajak2')) || 
                                                            ($rs3->getString('detail_harga2') != $rs3->getString('detail_harga')) ||
                                                            ($rs3->getString('volume') != $rs3->getString('volume2')))
                                                            {
                                                                echo '<td><strong><span style="color:red;">!</span></strong></td>';
                                                            } else {
                                                                echo '<td>&nbsp;</td>';
                                                            }
                                                    $subnya2 = '';
                                                    $subnya = '';
                                                    if (($rs3->getString('komponen_name') != $rs3->getString('komponen_name2')) ||
                                                            ($rs3->getString('detail_name22') != $rs3->getString('detail_name2')) ||
                                                            ($rs3->getString('detail_satuan') != $rs3->getString('detail_satuan2')) ||
                                                            ($rs3->getString('keterangan_koefisien') != $rs3->getString('keterangan_koefisien2')) ||
                                                            ($rs3->getString('pajak') != $rs3->getString('pajak2')) ||
                                                            ($rs3->getString('detail_harga2') != $rs3->getString('detail_harga')) ||
                                                            ($rs3->getString('volume') != $rs3->getString('volume2')))
                                                            {
                                                                if ($sf_user->getNamaUser() != 'parlemen2') {
                                                                    echo '<td class="Font8v" align="left"><strong><span class="style3">' . $subnya2 . $rs3->getString('komponen_name2') . ' ' . $rs3->getString('detail_name_rd'). '</span>';
                                                                        if ($rs3->getString('detail_satuan2') == 'Paket' && $rs3->getString('volume2') == '1' && $rs3->getString('status_lelang') == 'lock') {
                                                                            echo ' <span class="badge badge-primary">PSP</span>';
                                                                        }
                                                                        if($rs3->getBoolean('is_hpsp') == TRUE) {
                                                                            echo ' <span class="badge badge-danger">HPSP</span>';
                                                                        }
                                                                        if($rs3->getBoolean('is_output') == TRUE) {
                                                                            echo ' <span class="badge badge-info">Output</span>';
                                                                        }
                                                                        if($rs3->getBoolean('is_musrenbang') == TRUE) {
                                                                            echo ' <span class="badge badge-success">Musrenbang</span>';
                                                                        }
                                                                        if($rs3->getBoolean('prioritas_wali') == TRUE) {
                                                                            echo ' <span class="badge badge-danger">Prioritas</span>';
                                                                        }
                                                                    echo '</strong></td>';
                                                                    echo '<td align="center" nowrap="nowrap"><strong><span class="Font8v">' . $rs3->getString('detail_satuan2') . '</span></strong></td>';
                                                                    echo '<td align="center"><strong><span class="Font8v">' . $rs3->getString('keterangan_koefisien2') . '=' . $rs3->getString('volume2') . ' ' . $rs3->getString('detail_satuan2') . '</span></strong></td>';
                                                                    echo '<td align="right" nowrap="nowrap"><strong>';
                                                                        if ($rs3->getString('detail_satuan2') == '%') {
                                                                            echo $rs3->getString('detail_harga2');
                                                                        } else {
                                                                            echo number_format($rs3->getString('detail_harga2'), 0, ',', '.');
                                                                        }
                                                                    echo '</strong></td>';
                                                                    // nilai hasil pada bagian menjadi yang mengalami perubahan
                                                                    $hasil = myTools::round_ganjil_genap($rs3->getString('detail_harga2') * $rs3->getString('volume2'));
                                                                    echo '<td align="right" nowrap="nowrap"><strong>' . number_format($hasil, 0, ',', '.') . '</strong></td>';
                                                                    echo '<td align="right" nowrap="nowrap"><strong>' . $rs3->getString('pajak2') . '%</strong></td>';
                                                                    $total1 = $hasil + (($rs3->getString('pajak2') / 100) * $hasil);
                                                                    // nilai total pada bagian menjadi yang mengalami perubahan
                                                                    echo '<td align="right" nowrap="nowrap"><strong>' . number_format($rs3->getString('hasil_kali2'), 0, ',', '.') . '</strong></td>';
                                                                    $kanan = $rs3->getString('hasil_kali2');
                                                                    // $selisihnya = $total1 - $kiri;
                                                                    $selisihnya = $kanan - $kiri;
                                                                    echo '<td align="right" nowrap="nowrap"><strong>' . number_format($selisihnya, 0, ',', '.') . '</strong></td>';
                                                                    echo '<td align="left"><strong>' . $rs3->getString('note_skpd') . '</strong><br/>';
                                                                    echo '</td>';
                                                                    $total_n += $total1;
                                                                    echo '</tr>';
                                                                    $jumlah_harga += $hasil;
                                                                }
                                                            }
                                                            else if ($rs2->getString('rekening_prev')) 
                                                            {
                                                                if ($sf_user->getNamaUser() != 'parlemen2') {
                                                                    echo '<td class="Font8v" align="left"><span class="style3">' . $subnya2 . $rs3->getString('komponen_name2') . ' ' . $rs3->getString('detail_name_rd') . '</span>';
                                                                        if ($rs3->getString('detail_satuan2') == 'Paket' && $rs3->getString('volume2') == '1' && $rs3->getString('status_lelang') == 'lock') {
                                                                            echo ' <span class="badge badge-primary">PSP</span>';
                                                                        }
                                                                        if($rs3->getBoolean('is_hpsp') == TRUE) {
                                                                            echo ' <span class="badge badge-danger">HPSP</span>';
                                                                        }
                                                                        if($rs3->getBoolean('is_output') == TRUE) {
                                                                            echo ' <span class="badge badge-info">Output</span>';
                                                                        }
                                                                        if($rs3->getBoolean('is_musrenbang') == TRUE) {
                                                                            echo ' <span class="badge badge-success">Musrenbang</span>';
                                                                        }
                                                                        if($rs3->getBoolean('prioritas_wali') == TRUE) {
                                                                            echo ' <span class="badge badge-danger">Prioritas</span>';
                                                                        }
                                                                    echo '</td>';
                                                                    echo '<td align="center" nowrap="nowrap"><span class="Font8v">' . $rs3->getString('detail_satuan2') . '</span></td>';
                                                                    echo '<td align="center"><span class="Font8v">' . $rs3->getString('keterangan_koefisien2') . '=' . $rs3->getString('volume2') . ' ' . $rs3->getString('detail_satuan2') . '</span></td>';
                                                                    echo '<td align="right" nowrap="nowrap">';
                                                                        if ($rs3->getString('detail_satuan2') == '%') {
                                                                            echo $rs3->getString('detail_harga2');
                                                                        } else {
                                                                            echo number_format($rs3->getString('detail_harga2'), 0, ',', '.');
                                                                        }
                                                                    echo '</td>';
                                                                    // nilai hasil pada bagian menjadi yang tidak mengalami perubahan
                                                                    $hasil = myTools::round_ganjil_genap($rs3->getString('detail_harga2') * $rs3->getString('volume2'));
                                                                    echo '<td align="right" nowrap="nowrap">' . number_format($hasil, 0, ',', '.') . '</td>';
                                                                    echo '<td align="right" nowrap="nowrap">' . $rs3->getString('pajak2') . '%</td>';
                                                                    $total1 = $hasil + (($rs3->getString('pajak2') / 100) * $hasil);
                                                                    // nilai total pada bagian menjadi yang tidak mengalami perubahan
                                                                    echo '<td align="right" nowrap="nowrap">' . number_format($rs3->getString('hasil_kali2'), 0, ',', '.') . '</td>';
                                                                    $kanan = $rs3->getString('hasil_kali2');
                                                                    $selisihnya = $total1 - $kiri;
                                                                    echo '<td align="right" nowrap="nowrap"><strong>' . number_format($selisihnya, 0, ',', '.') . '</strong></td>';
                                                                    echo '<td align="left"><strong> - </strong><br/>';
                                                                    echo '</td>';
                                                                    $total_n += $total1;
                                                                    echo '</tr>';
                                                                    $jumlah_harga += $hasil;
                                                                }
                                                            }
                                                } 
                                                else if ($rs2->getString('rekening_prev') == '') 
                                                {
                                                    echo '<tr bgcolor="#ffffff" valign="top">';
                                                    echo '<td colspan="7">&nbsp;</td>';
                                                    echo '<td><strong><span style="color:red;">!</span></strong></td>';
                                                    echo '<td class="Font8v" align="left"><strong><span class="style3">' . $subnya2 . $rs3->getString('komponen_name2') . ' ' . $rs3->getString('detail_name_rd') . '</span></strong></td>';
                                                    echo '<td align="center" nowrap="nowrap"><strong><span class="Font8v">' . $rs3->getString('detail_satuan2') . '</span></strong></td>';
                                                    echo '<td align="center"><strong><span class="Font8v">' . $rs3->getString('keterangan_koefisien2') . '=' . $rs3->getString('volume2') . ' ' . $rs3->getString('detail_satuan2') . '</span></strong></td>';
                                                    echo '<td align="right" nowrap="nowrap"><strong>';
                                                        if ($rs3->getString('detail_satuan2') == '%') {
                                                            echo $rs3->getString('detail_harga2');
                                                        } else {
                                                            echo number_format($rs3->getString('detail_harga2'), 0, ',', '.');
                                                        }
                                                    echo '</strong></td>';
                                                    $hasil = myTools::round_ganjil_genap($rs3->getString('detail_harga2') * $rs3->getString('volume2'));
                                                    echo '<td align="right" nowrap="nowrap"><strong>' . number_format($hasil, 0, ',', '.') . '</strong></td>';
                                                    echo '<td align="right" nowrap="nowrap"><strong>' . $rs3->getString('pajak2') . '%</strong></td>';
                                                    $total1 = $hasil - (($rs3->getString('pajak2') / 100) * $hasil);
                                                    echo '<td align="right" nowrap="nowrap"><strong>' . number_format($rs3->getString('hasil_kali2'), 0, ',', '.') . '</strong></td>';
                                                    $kanan = $rs3->getString('hasil_kali2');
                                                    $selisihnya = $total1 - 0;
                                                    echo '<td align="right" nowrap="nowrap"><strong>' . number_format($selisihnya, 0, ',', '.') . '</strong></td>';
                                                    echo '<td align="left"><strong>' . $rs3->getString('note_skpd') . '</strong><br/>';
                                                    echo '</td>';
                                                    $total_n += $total1;
                                                    echo '</tr>';
                                                    $jumlah_harga += $hasil;
                                                    //kalo kiri gak ada subtitle
                                                }
                                            }
                                        }
                                        $total_semua = $total_semua + $total;
                                        $total_semua_n += $total_n;
                                        $query = "select sum(rd.nilai_anggaran) as nilai from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn_semula . "rincian_detail rd
												where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and subtitle='$subtitle' and rd.status_hapus=false";
                                        $con = Propel::getConnection();
                                        $stmt = $con->prepareStatement($query);
                                        $rs_harga_subtitle = $stmt->executeQuery();
                                        while ($rs_harga_subtitle->next()) {
                                            $total_subtitle_semula = $rs_harga_subtitle->getString('nilai');
                                        }

                                        $query = "select sum(rd.nilai_anggaran) as nilai from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "rincian_detail rd
												where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and subtitle='$subtitle' and rd.status_hapus=false";
                                        $con = Propel::getConnection();
                                        $stmt = $con->prepareStatement($query);
                                        $rs_harga_subtitle = $stmt->executeQuery();
                                        while ($rs_harga_subtitle->next()) {
                                            $total_subtitle_menjadi = $rs_harga_subtitle->getString('nilai');
                                        }
                                        echo '<tr bgcolor="#ffffff">';
                                        echo '<td colspan="6" align="right">';
                                        echo 'Total ' . $subtitle . ' :</td>';
                                        echo '<td align="right" nowrap="nowrap"><strong>' . number_format($total_subtitle_semula, 0, ',', '.') . '</strong></td>';
                                        echo '<td> </td>';
                                        echo '<td colspan="6" align="right">';
                                        echo 'Total ' . $subtitle . ' :</td>';
                                        echo '<td align="right" nowrap="nowrap"><strong>' . number_format($total_subtitle_menjadi, 0, ',', '.') . '</strong></td>';
                                        echo '<td colspan="2"></td>';
                                        echo '</tr>';
                                    }
                                    echo '<tr bgcolor="#e6e6e6">';
                                    echo '<td colspan="6" align="right">';
                                    echo '<b>Grand Total :</b></td>';
                                    echo '<td align="right" nowrap="nowrap">' . number_format($total_semula, 0, ',', '.') . '</td>';
                                    echo '<td> </td>';
                                    echo '<td colspan="6" align="right">';
                                    echo '<b>Grand Total :</b></td>';
                                    echo '<td align="right" nowrap="nowrap">' . number_format($total_sekarang, 0, ',', '.') . '</td>';
                                    echo '<td colspan="2"></td>';
                                    echo '</tr>';
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="card-body">
                        <p>Telah mendapatkan persetujuan oleh:</p>
                        <?php
                            $c = new Criteria();
                            $c->add(LogApprovalPeer::UNIT_ID, $unit_id);
                            $c->add(LogApprovalPeer::KEGIATAN_CODE, $kode_kegiatan);
                            $c->add(LogApprovalPeer::TAHAP, $tahap);
                            $c->add(LogApprovalPeer::SEBAGAI, 'Entri', Criteria::NOT_EQUAL);
                            $c->add(LogApprovalPeer::SEBAGAI, 'Admin (RKA)', Criteria::NOT_EQUAL);
                            $c->addDescendingOrderByColumn(LogApprovalPeer::WAKTU);
                            $c->setDistinct();
                            $rs_log = LogApprovalPeer::doSelect($c);
                            $pertama = true;
                            foreach ($rs_log as $log):
                                $user_id = $log->getUserId();
                                $c = new Criteria();
                                $c->add(MasterUserV2Peer::USER_ID, $user_id);
                                if ($rs_nama = MasterUserV2Peer::doSelectOne($c)) {
                                    $nama = $rs_nama->getUserName();
                                    $jabatan = $rs_nama->getJabatan();
                                    $arr_sebagai = explode('|', $log->getSebagai());
                                    if ($arr_sebagai[1] == 'kembali') {
                                        echo $nama . ' (' . $jabatan . ') sebagai ' . $arr_sebagai[0] . ' telah mengembalikan ke Entri, dengan alasan: ' . $log->getCatatan() . '<br> ';
                                    } else {
                                        if($log->getSebagai() == 'Entri')
                                            echo  $log->getUserId(). ' sebagai ' . $log->getSebagai() . '<br> ';
                                        else
                                            echo $nama . ' (' . $jabatan . ') sebagai ' . $log->getSebagai() . '<br> ';
                                    }
                                }
                            endforeach;
                            $tahap_angka = DinasMasterKegiatanPeer::getTahapKegiatan($unit_id, $kode_kegiatan);
                            $c_rincian_bappeko = new Criteria();
                            $c_rincian_bappeko->add(RincianBappekoPeer::UNIT_ID, $unit_id);
                            $c_rincian_bappeko->add(RincianBappekoPeer::KODE_KEGIATAN, $kode_kegiatan);
                            $c_rincian_bappeko->add(RincianBappekoPeer::TAHAP, $tahap_angka);
                            if ($rs_rincian_bappeko = RincianBappekoPeer::doSelectOne($c_rincian_bappeko))
                            {
                                $status_buka_bappeko = FALSE;
                                if (!is_null($rs_rincian_bappeko->getJawaban1Dinas()) || !is_null($rs_rincian_bappeko->getJawaban2Dinas())) {
                                    echo '<br><b>Perangkat Daerah / Unit Kerja</b> : ';
                                    $alasan_dinas = strip_tags($rs_rincian_bappeko->getCatatanDinas());
                                    if ($rs_rincian_bappeko->getJawaban1Dinas() && $rs_rincian_bappeko->getJawaban2Dinas()) {
                                        echo 'mengubah output dan subtitle, dengan alasan ' . $alasan_dinas;
                                        $status_buka_bappeko = TRUE;
                                    } elseif ($rs_rincian_bappeko->getJawaban1Dinas()) {
                                        echo 'mengubah subtitle, dengan alasan ' . $alasan_dinas;
                                        $status_buka_bappeko = TRUE;
                                    } elseif ($rs_rincian_bappeko->getJawaban2Dinas()) {
                                        echo 'mengubah subtitle, dengan alasan ' . $alasan_dinas;
                                        $status_buka_bappeko = TRUE;
                                    } else {
                                        echo 'tidak mengubah output dan subtitle';
                                    }
                                }
                                if (!is_null($rs_rincian_bappeko->getStatusBuka()) && $status_buka_bappeko) {
                                    if ($rs_rincian_bappeko->getStatusBuka()) {
                                        echo '<br><b>Kegiatan telah mendapat persetujuan Bappeko</b>';
                                    } else {
                                        echo '<br><b>Kegiatan tidak mendapat persetujuan Bappeko, karena ' . $rs_rincian_bappeko->getCatatanBappeko() . '</b>';
                                    }
                                }
                                if (!is_null($rs_rincian_bappeko->getJawaban1Bappeko()) && !is_null($rs_rincian_bappeko->getJawaban2Bappeko())) {
                                    echo '<br><b>Bappeko</b> : ';
                                    if ($rs_rincian_bappeko->getJawaban1Bappeko() && $rs_rincian_bappeko->getJawaban2Bappeko()) {
                                        echo 'mengubah output dan subtitle';
                                    } elseif ($rs_rincian_bappeko->getJawaban1Bappeko()) {
                                        echo 'mengubah subtitle';
                                    } elseif ($rs_rincian_bappeko->getJawaban2Bappeko()) {
                                        echo 'mengubah subtitle';
                                    } else {
                                        echo 'tidak mengubah output dan subtitle';
                                    }
                                }
                            }
                            echo "<br/>";
                            $posisi_kegiatan = '';
                            if ($level_kegiatan == 0) {
                                $posisi_kegiatan = 'Entri';
                            } elseif ($level_kegiatan == 1) {
                                $posisi_kegiatan = 'PPTK';
                            } elseif ($level_kegiatan == 2) {
                                $posisi_kegiatan = 'KPA';
                            } elseif ($level_kegiatan == 3) {
                                $posisi_kegiatan = 'PA I';
                            } elseif ($level_kegiatan == 4) {
                                $penyetuju = array();
                                if($kegiatan_is_tapd_setuju)
                                    array_push($penyetuju, 'BPKAD');
                                if($kegiatan_is_bappeko_setuju)
                                    array_push($penyetuju, 'Bappeda');
                                if($kegiatan_is_penyelia_setuju)
                                    array_push($penyetuju, 'BPBJAP');
                                if($kegiatan_is_bagian_hukum_setuju)
                                    array_push($penyetuju, 'Bag. Hukum');
                                if($kegiatan_is_inspektorat_setuju)
                                    array_push($penyetuju, 'Inspektorat');
                                if($kegiatan_is_badan_kepegawaian_setuju)
                                    array_push($penyetuju, 'BKPSDM');
                                if($kegiatan_is_lppa_setuju)
                                    // array_push($penyetuju, 'Bag. LPPA');
                                    array_push($penyetuju, 'Bapenda');

                                $strPenyetuju = implode(', ', $penyetuju);
                                $posisi_kegiatan = 'Tim Anggaran ('.$strPenyetuju.')';
                            } elseif ($level_kegiatan == 5) {
                                $posisi_kegiatan = 'PA II';
                            } elseif ($level_kegiatan == 6) {
                                $posisi_kegiatan = 'Penyelia II';
                            } elseif ($level_kegiatan == 7) {
                                $posisi_kegiatan = 'RKA';
                            } else
                                $posisi_kegiatan = 'salah posisi';
                                //untuk cek kalau sudah sampai PA 2, tidak bisa diedit lagi catatan di tim anggaran
                            $cek_max_level = TRUE;
                            $c_level = new Criteria();
                            $c_level->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
                            $c_level->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
                            $c_level->add(DinasRincianDetailPeer::STATUS_LEVEL, 4, Criteria::GREATER_THAN);
                            if (DinasRincianDetailPeer::doSelect($c_level))
                                $cek_max_level = FALSE;
                        ?>
                        <p>Catatan Perangkat Daerah / Unit Kerja (Posisi=<?php echo $posisi_kegiatan ?>) :</p>
                        <table class="table table-bordered">
                            <tr><td><?php echo $catatan; ?></td></tr>
                        </table>
                        <p>Catatan Pembahasan :</p>
                        <table class="table table-bordered">
                            <tr>
                                <td>
                                    <?php
                                    if(!is_null($catatan_ubah_f1_dinas) && !is_null($ubah_f1_dinas) && $ubah_f1_dinas) {
                                        echo trim($catatan_ubah_f1_dinas.' '.$catatan_pembahasan);
                                    } else {
                                        echo trim($catatan_pembahasan);
                                    }
                                    ?>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>