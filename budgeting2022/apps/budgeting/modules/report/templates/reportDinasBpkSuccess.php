<div class="box box-info">        
    <div class="box-header with-border">
        Laporan Per Dinas
    </div>
    <div class="box-body">
        <table class="table table-bordered">
            <tr>
                <!--
                <th class="text-center">Pagu</th>
                <th class="text-center">Jumlah Usulan</th>
                <th class="text-center">Nilai Usulan</th>
                <th class="text-center">Jumlah Disetujui</th>
                <th class="text-center">Setuju - Pagu</th>-->
                <th class="text-center">Dinas</th>
                <th class="text-center">Nilai</th>
            </tr>
            <?php
            $i = 0;
            $rs->first();
            do {
                ?>
                <tr>
                    <!--
                    <td class="text-right"><?php echo $pagu[$i] ?></td>
                    <td class="text-center"><?php echo $rs->getString('total_draft') ?></td>
                    <td class="text-right"><?php echo $nilai_usulan[$i] ?></td>
                    <td class="text-center"><?php echo ($rs->getString('total_locked') < 0) ? '0' : $rs->getString('total_locked') ?></td>
                    <td class="text-right"><?php echo ($rs->getString('nilai_locked') <= 0) ? '0' : $selisih[$i] ?></td>
                    -->
                    <td class="text-left"><?php echo $rs->getString('unit_id') . ' - ' . $rs->getString('unit_name') ?></td>
                    <td class="text-right"><?php echo number_format(($rs->getString('nilai_locked') < 0) ? '0' : $rs->getString('nilai_locked'), 0, ",", ".") ?></td>
                </tr>
                <?php $i++; ?>
                <?php
            } while ($rs->next());
            ?>
            <tr>
                <!--
                <td class="text-right text-bold bg-green-active"><?php echo $total_pagu ?></td>
                <td class="text-center text-bold bg-green-active"><?php echo $total_draft2 ?></td>
                <td class="text-right text-bold bg-green-active"><?php echo $nilai_draft2 ?></td>
                <td class="text-center text-bold bg-green-active"><?php echo ($total_locked2 < 0) ? '0' : $total_locked2 ?></td>
                <td class="text-right text-bold bg-green-active"><?php echo ($total_locked2 < 0) ? '0' : $total_selisih ?></td>
                -->
                <td class="text-center text-bold bg-green-active">Total</td>
                <td class="text-right text-bold bg-green-active"><?php echo ($total_locked2 < 0) ? '0' : $nilai_locked2 ?></td>
            </tr>
        </table>       
    </div>
</div>