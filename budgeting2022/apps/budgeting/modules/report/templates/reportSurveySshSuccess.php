<?php use_helper('Form', 'Object', 'Javascript') ?>
<div class="box box-info">
    <div class="box-header with-border">
        Pilih Kelompok Barang I
    </div>
    <div class="box-body">
        <?php
        echo select_tag('kelompok1', options_for_select($arrKelompok, $kelompok1 ? $kelompok1 : ''), array('class' => 'form-control'));
        ?>
    </div>

    <div class="box-header with-border">
        Pilih Kelompok Barang II
    </div>
    <div class="box-body">
        <?php
        echo select_tag('kelompok2', options_for_select($arrKelompok2, $kelompok2 ? $kelompok2 : '' , 'include_custom=---Pilih Kelompok Barang I Dulu---'), array('class' => 'form-control'));
        ?>
    </div>
</div>
<div class="box box-info" id="isiLink">        
    <div class="box-header with-border">
        Print Excel Survey SSH
    </div>
    <div class="box-body">
        Pilih Kelompok Barang 1 dan 2
    </div>
</div>

<script>
    $(document).ready(function () {
        $(".js-example-basic-single").select2();
        $(".js-example-basic-multiple").select2();
    });

    $("#kelompok1").change(function () {
        var id = $(this).val();
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/survey/pilihKelompok/kategori/" + id + ".html",
            context: document.body
        }).done(function (msg) {
            $('#kelompok2').html(msg);
        });

        if(id == '01') {
            loadLink(id, '');
        } else {
            removeLink();
        }
    });

    $("#kelompok2").change(function() {
        var kelompok1 = $("#kelompok1").val();
        var kelompok2 = $(this).val();
        loadLink(kelompok1, kelompok2);
    });

    function loadLink(kelompok1, kelompok2) {
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/report/pilihKelompok2/kelompok1/" + kelompok1 + "/kelompok2/" + kelompok2 + ".html",
            context: document.body
        }).done(function(msg) {
            if(kelompok2 === '' && kelompok1 != '01')
                removeLink();
            else
                $('#isiLink').html(msg);
        });
    }
    
    function removeLink() {
        $('#isiLink').html(
            "<div class='box-header with-border'>" +
                "Print Excel Survey SSH" +
            "</div>" +
            "<div class='box-body'>" +
                "Pilih Kelompok Barang 1 dan 2" +
            "</div>"
        );
    }
</script>