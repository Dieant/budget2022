<?php use_helper('Form', 'Object', 'Javascript') ?>
<?php
if (sfConfig::get('app_tahap_edit') == 'murni') {
    $nama_sistem = 'Pra-RKA';
} elseif (sfConfig::get('app_tahap_edit') == 'pak') {
    $nama_sistem = 'PAK';
} elseif (sfConfig::get('app_tahap_edit') == 'penyesuaian') {
    $nama_sistem = 'Penyesuaian';
} else {
    $nama_sistem = 'Revisi';
}
?>
<div class="box box-info">        
    <div class="box-header with-border">
        Pilih Tahap
    </div>
    <div class="box-body">
        <select id="tahap" class="form-control">
            <option value="">------Pilih Tahap------</option>
            <option value="murni" <?php if ($tahap == 'murni') echo 'selected'; ?>>Murni</option>
            <!-- <option value="revisi1" <?php if ($tahap == 'revisi1') echo 'selected'; ?>>Revisi 1</option>
            <option value="revisi2" <?php if ($tahap == 'revisi2') echo 'selected'; ?>>Revisi 2</option>
            <option value="revisi3" <?php if ($tahap == 'revisi3') echo 'selected'; ?>>Revisi 3</option> -->
            <!-- <option value="revisi4" <?php if ($tahap == 'revisi4') echo 'selected'; ?>>Revisi 4</option> -->
            <!-- <option value="revisi5" <?php if ($tahap == 'revisi5') echo 'selected'; ?>>Revisi 5</option> -->
            <option value="dinas" <?php if ($tahap == 'dinas') echo 'selected'; ?>>Pra RKA</option>
        </select>
    </div>
</div>
<div class="box box-info">
    <div class="box-header with-border">
        Laporan Catatan Pembahasan per Dinas
    </div>
    <div class="box-body">
        <table class="table table-bordered">
            <tr>
                <th class="text-center">No</th>
                <th class="text-center">Dinas</th>
                <th class="text-center">Kegiatan</th>
                <th class="text-center">Catatan Pembahasan</th>
            </tr><!--
            <?php //foreach ($dinas as $value): ?>
                        <tr>
                            <td><?php //echo ++$no;  ?></td>
                            <td class="text-left" ><?php //echo $value['unit_name']  ?></td>
                            <td class="text-left" ><?php //echo $value['kegiatan']  ?></td>
                            <td class="text-left" ><?php //echo $value['catatan']  ?></td>
                        </tr>
            <?php //endforeach; ?>-->
            <?php foreach ($dinas as $value): ?>
                <?php $pertama = TRUE; ?>
                <tr>
                    <td rowspan="<?php echo count($value['arr_kegiatan']) ?>"><?php echo ++$no; ?></td>
                    <td rowspan="<?php echo count($value['arr_kegiatan']) ?>" class="text-left" ><?php echo $value['unit_name'] ?></td>
                    <?php for ($j = 0; $j < count($value['arr_kegiatan']); $j++): ?>
                        <?php if (!$pertama) : $pertama = FALSE; ?><tr><?php endif; ?>
                        <td class="text-left" ><?php echo $value['arr_kegiatan'][$j] ?></td>
                        <td class="text-left" ><?php echo $value['arr_catatan'][$j] ?></td>
                    </tr>
                <?php endfor; ?>
                </tr>
            <?php endforeach; ?>
        </table>       
    </div>
</div>
<script>
    $("#tahap").change(function () {
        $('#indicator').show();
        var id = $(this).val();
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/report/reportCatatanPembahasan/tahap/" + id + ".html",
            context: document.body
        }).done(function (msg) {
            $('#indicator').hide();
            $('#isi').html(msg);
        });

    });
</script>