<section class="content">
    <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">
                        <i class="fas fa-align-left"></i> Laporan Per Belanja
                    </h3>
                    <div class="card-tools">
                        <div class="input-group input-group-sm">
                            <?php
                            if ($sf_flash->has('RBPR')) {
                                echo link_to('Export PDF', 'report/buatPDFReportBelanjaRevisi', array('class' => 'btn btn-outline-primary btn-sm text-bold'));
                            } else if($sf_flash->has('RBMBP')) {
                                echo link_to('Export PDF', 'report/buatPDFReportBelanja?tahap=murni_bukuputih&flash=RBMBP', array('class' => 'btn btn-outline-primary btn-sm text-bold'));
                            } else if($sf_flash->has('RBM')) {
                                echo link_to('Export PDF', 'report/buatPDFReportBelanja?tahap=murni&flash=RBMBP', array('class' => 'btn btn-outline-primary btn-sm text-bold'));
                            } else if($sf_flash->has('RBR1')) {
                                echo link_to('Export PDF', 'report/buatPDFReportBelanja?tahap=revisi1&flash=RBMBP', array('class' => 'btn btn-outline-primary btn-sm text-bold'));
                            } else {
                                 echo link_to('Export PDF', 'report/buatPDFReportBelanja', array('class' => 'btn btn-outline-primary btn-sm text-bold'));
                            }
                            ?>
                        </div>
                    </div>
                </div>
                <div class="card-body table-responsive p-0">
                    <table class="table table-hover text-nowrap">
                        <thead class="head_peach">
                            <tr>
                                <th class="text-center">Kode Belanja</th>
                                <th class="text-center">Nama Belanja</th>
                                <th class="text-center">Nilai Sebelum</th>
                                <th class="text-center">Nilai Usulan</th>
                                <th class="text-center">% Usulan</th>
                                <th class="text-center">Nilai Disetujui</th>
                                <th class="text-center">% Disetujui</th>
                            </tr>
                        </thead>
                        <?php
                        $tempBelanja = '';
                        $belanja='';
                        $i = 0;
                        $nilai_draft_total_sebelum = 0;
                        $nilai_draft_total = 0;
                        $nilai_locked_total = 0;
                        $rs->first();
                        do {   

                            if(strpos($belanja_code[$i], '5.2') !== FALSE){
                                $belanja = 'BELANJA MODAL';
                            }
                            elseif (strpos($belanja_code[$i], '5.3') !== FALSE) {
                                $belanja = 'BELANJA TIDAK TERDUGA';
                            }
                            else{
                                $belanja = 'BELANJA OPERASI'; 
                            }

                            // if(substr($belanja_code[$i],0,2) == '5.1'){
                            //     $belanja = 'BELANJA OPERASI';
                            // }elseif (substr($belanja_code[$i],0,2) == '5.2') {
                            //     $belanja = 'BELANJA MODAL';
                            // }else{
                            //     $belanja = 'BELANJA TIDAK TERDUGA';
                            // }
                                
                            // else
                            //      $belanja = 'BELANJA OPERASI';

                            if($tempBelanja != $belanja && substr($belanja_code[$i],0,2) == '5.'){
                            //if(!empty($belanja_code[$i])): 
                                $jenis='';
                                // if($belanja_code[$i] == '5.1.01' || $belanja_code[$i] == '5.1.02' || $belanja_code[$i] == '5.1.05' || $belanja_code[$i] == '5.1.06'){
                                if(strpos($belanja_code[$i] , '5.1') !== FALSE){
                                    $jenis='BELANJA OPERASI';
                                } 
                                else if(strpos($belanja_code[$i] , '5.2') !== FALSE)
                                {
                                    $jenis='BELANJA MODAL';
                                }
                                else if(strpos($belanja_code[$i] , '5.3') !== FALSE){
                                    $jenis='BELANJA TIDAK TERDUGA';
                                }
                                else  {
                                     $jenis='BELANJA TRANSFER';
                                }                         
                                ?>                      
                                    <tr>
                                        <td colspan="8" class="text-left text-bold"><?php echo $jenis; ?></td>
                                    </tr>
                            <?php } if (!empty($belanja_code[$i]) && substr($belanja_code[$i],0,2) == '5.') { ?>                           
                            <tr>
                                <td class="text-center"><?php echo isset($belanja_code[$i]) ? $belanja_code[$i] : '' ?></td>
                                <td class="text-left"> <?php echo isset($belanja_name[$i]) ? $belanja_name[$i] : '' ?></td>
                                <td class="text-right">
                                    <?php 
                                        $untuk_nilai_sebelum = 0;
                                        foreach($belanja_code_sebelum as $key=>$sebelum){
                                            if($sebelum == $belanja_code[$i]){
                                                $untuk_nilai_sebelum = isset($nilai_draft_sebelum[$key]) ? $nilai_draft_sebelum[$key] : 0;
                                                // $untuk_nilai_sebelum = $nilai_draft_sebelum[$key];
                                            }
                                        }
                                        echo $untuk_nilai_sebelum;
                                    ?>
                                </td>
                                <td class="text-right"><?php echo isset($nilai_draft[$i]) ? $nilai_draft[$i] : '' ?></td>
                                <td class="text-right"><?php echo isset($persen_draft[$i]) ? $persen_draft[$i] : '' ?>%</td>
                                <td class="text-right"><?php echo isset($nilai_locked[$i]) ? $nilai_locked[$i] : '' ?></td>
                                <td class="text-right"><?php echo isset($persen_locked[$i]) ? $persen_locked[$i] : '' ?>%</td>
                            </tr>
                           <!--  <tr>
                                    <td colspan="6">&nbsp;</td>
                            </tr> -->
                            <?php 
                                if(strpos($belanja_code[$i], '5.2') !== FALSE){
                                    $tempBelanja = 'BELANJA MODAL';
                                }
                                elseif (strpos($belanja_code[$i], '5.3') !== FALSE) {
                                    $tempBelanja = 'BELANJA TIDAK TERDUGA';
                                }
                                else{
                                    $tempBelanja = 'BELANJA OPERASI'; 
                                }           
                                // if(substr($belanja_code[$i],0,2) == '5.1'){
                                //     $belanja = 'BELANJA OPERASI';
                                // }elseif (substr($belanja_code[$i],0,2) == '5.2') {
                                //     $belanja = 'BELANJA MODAL';
                                // }else{
                                //     $belanja = 'BELANJA TIDAK TERDUGA';
                                // }
                                
                                $nilai_draft_total_sebelum += intval(str_replace(".","",$untuk_nilai_sebelum));
                                $nilai_draft_total += intval(str_replace(".","",$nilai_draft[$i]));
                                $nilai_locked_total += intval(str_replace(".","",$nilai_locked[$i]));


                            }     
                             
                            $i++;
                        } while ($rs->next())
                        ?> 
                        <tr>
                            <td colspan="2" class="text-center text-bold">Total BELANJA </td>
                            <td class="text-right text-bold"><?php echo number_format($nilai_draft_total_sebelum, 0, ",", "."); ?></td>
                            <td class="text-right text-bold"><?php echo number_format($nilai_draft_total, 0, ",", "."); ?></td>
                            <td class="text-right text-bold"></td>
                            <td class="text-right text-bold"><?php echo number_format($nilai_locked_total, 0, ",", "."); ?></td>
                            <td class="text-right text-bold"></td>
                        </tr>
                        <?php 
                        $tahap_angka = DinasMasterKegiatanPeer::ubahTahapAngka($tahap);
                        if ($tahap_angka > 3) {?>
                        <tr>                           
                            <td colspan="3" class="text-left text-bold">PERGESERAN Ex. BTL : 38.284.111.445</td>
                            <td colspan="1" class="text-left text-bold"> </td>
                            <td colspan="2" class="text-left text-bold"> </td>
                        </tr>
                        <tr>                           
                            <td colspan="3" class="text-left">Dinas Kesehatan : 22.005.249.652 </td>
                            <td colspan="1" class="text-left text-bold"> </td>
                            <td colspan="2" class="text-left text-bold"> </td>                          
                        </tr>
                        <tr>                           
                            <td colspan="3" class="text-left">RSUD DR. M. SOEWANDHIE : 8.990.290.290 </td>     
                            <td colspan="1" class="text-left text-bold"> </td>
                            <td colspan="2" class="text-left text-bold"> </td>                    
                        </tr>
                        <tr>                           
                         <td colspan="3" class="text-left">RSUD BHAKTI DHARMA HUSADA :  7.288.571.503 
                        </td> 
                         <td colspan="1" class="text-left text-bold"> </td>
                         <td colspan="2" class="text-left text-bold"> </td>                        
                        </tr>
                     <?php } ?>
                    </table>
                </div>
            </div>
          </div>
        </div>
    </div>
</section>
