<div class="box box-info">        
    <div class="box-header with-border">
        Laporan Semula Menjadi Belanja Kota
        <div class="box-tools pull-right">
            <?php
            if ($sf_flash->has('RBKT')) {
                // echo link_to('Export PDF', 'report/buatPDFReportBelanjaPerKotaRevisi', array('class' => 'btn btn-flat btn-warning text-bold'));
            } else {
                // echo link_to('Export PDF', 'report/buatPDFReportBelanja', array('class' => 'btn btn-flat btn-warning text-bold'));
            }
            ?>
        </div>
    </div>
    <div class="box-body">
        <table class="table table-bordered">
            <tr>
                <th class="text-center">Kode Belanja</th>
                <th class="text-center">Nama Belanja</th>
                <th class="text-center">Semula</th>
                <th class="text-center">% Usulan</th>
                <th class="text-center">Menjadi</th>
                <th class="text-center">% Disetujui</th>
            </tr>
            <?php
            // print_r($lckd_mjd);die();
            // $rsmjd->first();
            // do {
            //     echo 
            // } 
            // while ($rsmjd->next());
            ?>
            <?php
            $total_mjd_locked = 0;
            $i = 0;
            $rs->first();
            do {
                ?>
                <tr>
                    <td colspan="6">&nbsp;</td>
                </tr>
                <tr>
                    <td class="text-center text-bold">
                        <?php 
                            echo isset($belanja_id[$i]) ? $belanja_id[$i] : '' ?>
                    </td>
                    <td class="text-left text-bold"> <?php echo isset($belanja_name[$i]) ? $belanja_name[$i] : '' ?></td>
                    <td class="text-right">
                    <?php 
                    // echo isset($nilai_locked[$i]) ? $nilai_locked[$i] : ''
                    echo $blj_sml[$i]; 
                    ?>
                    </td>

                    <td class="text-right"><?php echo isset($persen_locked[$i]) ? $persen_locked[$i] : '' ?>%</td>
                    <td class="text-right">
                    <?php 
                    //echo isset($nilai_locked[$i]) ? $nilai_locked[$i] : '' 
                        $total_mjd_locked += str_replace('.', '', $blj_mnjd[$i]);
                        echo $blj_mnjd[$i];
                    ?>
                        
                    </td>
                    
                    <td class="text-right">
                    <?php 
                    //echo isset($persen_locked[$i]) ? $persen_locked[$i] : '' 
                    //echo $persen_locked_mjd[$i];
                    echo $persen_locked_mjd[$i];
                    ?>%
                    </td>

                </tr>
                <?php
                $i++;
            } while ($rs->next())
            ?>      
            <tr>
                <td colspan="6">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2" class="text-center text-bold">Total</td>
                <td class="text-right text-bold">
                    <?php 
                        // echo $nilai_locked2; 
                        echo $tot_blj_semula_fix;
                    ?>
                </td>
                <td class="text-right text-bold">100%</td>
                <td class="text-right text-bold">
                <?php 
                    echo number_format($total_mjd_locked, 0, ",", ".");
                    // echo $tot_blj_menjadi_fix; 
                ?>
                </td>
                <td class="text-right text-bold">100%</td>
            </tr>
        </table>       
    </div>
</div>



