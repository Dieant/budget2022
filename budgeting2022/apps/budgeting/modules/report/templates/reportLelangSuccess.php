<?php use_helper('Form', 'Object', 'Javascript') ?>
<?php
if (sfConfig::get('app_tahap_edit') == 'murni') {
    $nama_sistem = 'Pra-RKA';
} elseif (sfConfig::get('app_tahap_edit') == 'pak') {
    $nama_sistem = 'PAK';
} elseif (sfConfig::get('app_tahap_edit') == 'penyesuaian') {
    $nama_sistem = 'Penyesuaian';
} else {
    $nama_sistem = 'Revisi';
}
?>
<div class="box box-info">
    <div class="box-header with-border">
        Pilih Tahap
    </div>
    <div class="box-body">
        <?php
        echo select_tag('tahap', options_for_select(array('' => 'RKA', 'dinas' => 'Revisi', 'murni' => 'Murni', 'revisi1' => 'Revisi 1', 'revisi2' => 'Revisi 2'), $filter_tahap, 'include_custom=------Pilih Posisi------'), Array('id' => 'tahap', 'class' => 'form-control js-example-basic-single', 'style' => 'width:100%'));
        ?>
    </div>
</div>
<div class="box box-info">        
    <div class="box-header with-border">
        Laporan Posisi Kegiatan <?php echo $nama_sistem; ?>
    </div>
    <div class="box-body table-responsive">
        <table class="table table-bordered">
            <tr>
                <th class="text-center">Dinas</th>
                <th class="text-center">Kode Kegiatan</th>
                <th class="text-center">Nama Kegiatan</th>
                <th class="text-center">Subtitle</th>
                <th class="text-center">Rekening</th>
                <th class="text-center">Nama Komponen</th>
                <th class="text-center">Koefisien</th>
                <th class="text-center">Harga Komponen</th>
                <th class="text-center">Nilai</th>
                <th class="text-center">Catatan</th>
            </tr>
            <?php foreach ($komponen as $value): ?>
                <tr>
                    <td class="text-left" ><?php echo $value['unit_name'] ?></td>
                    <td class="text-left" ><?php echo $value['kode_kegiatan'] ?></td>
                    <td class="text-left" ><?php echo $value['nama_kegiatan'] ?></td>
                    <td class="text-left" ><?php echo $value['subtitle'] ?></td>
                    <td class="text-left" ><?php echo $value['rekening_code'] ?></td>
                    <td class="text-left" ><?php echo $value['komponen_name'] . (($value['detail_name'] != '') ? ' ' . $value['detail_name'] : '') ?></td>
                    <td class="text-left" ><?php echo $value['keterangan_koefisien'] ?></td>
                    <td class="text-right" ><?php echo number_format($value['komponen_harga'], 0, ',', '.') ?></td>
                    <td class="text-right" ><?php echo number_format($value['nilai_anggaran'], 0, ',', '.') ?></td>
                    <td class="text-left" ><?php echo $value['note_skpd'] ?></td>
                </tr>
            <?php endforeach; ?>
        </table>
    </div>
</div>
<script>
    $("#tahap").change(function () {
        $('#indicator').show();
        var id = $(this).val();
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/report/reportLelang/tahap/" + id + ".html",
            context: document.body
        }).done(function (msg) {
            $('#indicator').hide();
            $('#isi').html(msg);
        });
    });
</script>