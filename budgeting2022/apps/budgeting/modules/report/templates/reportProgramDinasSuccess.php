<div class="box box-info">        
    <div class="box-header with-border">
        Laporan Per Program Per Dinas
    </div>
    <div class="box-body">
        <table class="table table-bordered">
            <tr>
                <th colspan="2" class="text-center">Unit Satuan Kerja</th>
                <th class="text-center">Nilai Usulan</th>
                <th class="text-center">% Usulan</th>
                <th class="text-center">Nilai Disetujui</th>
                <th class="text-center">% Disetujui</th>
            </tr>
            <?php
            $i = 0;
            $rs->first();
            do {
                ?>

                <?php if (isset($dinas_ok[$i])): ?>
                    <tr>
                        <td colspan="6">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="2" class="text-bold text-left"><?php echo $kode_program[$i] ?>&nbsp;&nbsp;<?php echo $nama_program[$i] ?></td>
                        <td class="text-right text-bold"><?php echo $nilai_draft[$i] ?></td>
                        <td class="text-right text-bold"><?php echo $persen_draft[$i] ?>%</td>
                        <td class="text-right text-bold"><?php echo $nilai_locked[$i] ?></td>
                        <td class="text-right text-bold"><?php echo $persen_locked[$i] ?>%</td>

                    </tr>
                <?php endif; ?>
                <tr>
                    <td>&nbsp;</td>
                    <td class="text-left"><?php echo '(' . $rs->getString('unit_id') . ') ' . $rs->getString('unit_name') ?></td>
                    <td class="text-right"><?php echo number_format($rs->getString('nilai_draft3'), 0, '', '.') ?></td>
                    <td class="text-right"><?php echo $persen_draft3[$i] ?>%</td>
                    <td class="text-right"><?php echo number_format($rs->getString('nilai_locked3'), 0, '', '.') ?></td>
                    <td class="text-right"><?php echo $persen_locked3[$i] ?>%</td>

                </tr>

                <?php
                $i++;
            }while ($rs->next())
            ?>
            <tr>
                <td colspan="2" class="text-center text-bold bg-green-active">Total</td>
                <td class="text-right text-bold bg-green-active"><?php echo $nilai_draft2; ?></td>
                <td class="text-right text-bold bg-green-active">100%</td>
                <td class="text-right text-bold bg-green-active"><?php echo $nilai_locked2; ?></td>
                <td class="text-right text-bold bg-green-active">100%</td>

            </tr>
        </table>       
    </div>
</div>