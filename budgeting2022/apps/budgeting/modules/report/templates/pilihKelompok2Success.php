<?php use_helper('Object','Javascript','I18N', 'Date'); ?>

<div class="box-header with-border">
    Print Excel Survey SSH
</div>
<div class="box-body">
    <?php echo link_to(image_tag('b_print.png') . ' Print File Excel Survey SSH', 'report/excelReportSurveySsh?kelompok1=' . $kelompok1 . '&kelompok2=' . $kelompok2, array('class' => 'btn btn-flat btn-block btn-default text-bold col-xs-4', 'target' => '_blank')); ?>
</div>