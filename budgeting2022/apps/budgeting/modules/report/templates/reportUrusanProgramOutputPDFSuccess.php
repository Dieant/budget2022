<style>
    table, th, td{
        border: 1px solid black;
        padding: 0px;
    }
    table {
        border-spacing: 0px;
    }
</style>
<table style="width: 100%;" cellspacing="0">
    <tr>
        <td style="text-align: center; font-weight: bold">
            <h4>Report e-Budgeting <?php echo sfConfig::get('app_tahun_default'); ?></h4>
        </td>
    </tr>
    <tr>
        <td style="text-align: center; font-weight: bold">
            <h2>Report Per Urusan Per Program Output</h2>
        </td>
    </tr>
    <tr>
        <td style="text-align: center; font-weight: bold">
            <h4>Waktu : <?php echo date('d-m-Y H:i:s') ?></h4>
        </td>
    </tr>
    <tr>
        <td>
            <div>
                <table>
                    <thead>
                        <tr>
                            <th style="text-align: center; font-weight: bold">OPD</th>
                            <th style="text-align: center; font-weight: bold">Kode</th>
                            <th colspan="3" style="text-align: center; font-weight: bold">Urusan-Program-Kegiatan</th>
                            <th style="text-align: center; font-weight: bold">Output Semula</th>

                            <th colspan="6" style="text-align: center; font-weight: bold">Output Menjadi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($kegiatan as $value):
                            if (isset($value['urusan_ok'])) {
                                ?>
                                <tr>
                                    <td style="padding: 2px"> </td>
                                    <td style="text-align: left; font-weight: bold"><?php echo $value['kode_urusan'] ?></td>
                                    <td colspan="5" style="text-align: left; font-weight: bold"><?php echo $value['nama_urusan'] ?></td>
                                </tr>
                                <?php
                            }
                            if (isset($value['program_ok'])) {
                                ?>
                                <tr>
                                    <td style="padding: 2px"> </td>
                                    <td style="text-align: left; font-weight: bold"><?php echo $value['kode_program'] ?></td>
                                    <td style="padding: 2px"> </td>
                                    <td colspan="3" style="text-align: left; font-weight: bold"><?php echo $value['nama_program'] ?></td>
                                </tr>
                            <?php } ?>
                            <tr
                                <?php if($value['output'] != $value['output_jadi']){ ?>
                                style="background-color: pink"
                                <?php } ?>
                            >
                                <td style="text-align: left;" ><?php echo $value['unit_name'] ?></td>
                                <td style="text-align: left;" ><?php echo $value['kode_kegiatan'] ?></td>
                                <td style="padding: 2px">&nbsp;</td>
                                <td style="padding: 2px">&nbsp;</td>
                                <td style="text-align: left;" ><?php echo $value['nama_kegiatan'] ?></td>
                                <td style="text-align: left;" ><?php echo $value['output'] ?></td>

                                <td style="text-align: left;" ><?php echo $value['output_jadi'] ?></td>

                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>            
        </td>
    </tr>
</table>