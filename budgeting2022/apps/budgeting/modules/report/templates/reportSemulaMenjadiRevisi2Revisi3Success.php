<?php use_helper('Form', 'Object', 'Javascript') ?>

<?php if (isset($rs)): ?>
    <div class="box box-info">       
        <div class="box-header with-border">
            Laporan Perbandingan Revisi 2 - Revisi 3 Per Dinas Per Kegiatan Per Belanja
        </div>
        <div class="box-body">
            <?php echo '<b>SKPD : ' . $nama_unit . ' (' . $unit_unit . ')</b>'; ?>
            <table class="table table-bordered">
                <tr class="sf_admin_row_1">
                    <td colspan="2" align="center"><strong>Revisi 2</strong></td>
                    <td align="center"><strong>Selisih</strong></td>
                    <td colspan="2" align="center"><strong>Revisi 3</strong></td>
                </tr>

                <tr class="sf_admin_row_1">
                  <!--<td align="center" width="17"><strong></strong></td>
                  <td ><div align="center"></div></td>-->
                    <td align="center"><strong> Unit - Kegiatan - Rekening</strong></td>
                    <td align="center"><strong> Nilai </strong></td>
                    <td align="center"><strong> </strong></td>
                    <td align="center"><strong> Unit - Kegiatan - Rekening</strong></td>
                    <td align="center"><strong> Nilai </strong></td>
                </tr>

                <?php $i = 0;
                $rs->first();
                do { ?>
        <?php if ($dinas_ok[$i] == 't'): ?>
                        <tr><td colspan="7"  bgcolor="#FFFFFF"  >&nbsp;</td></tr>
                        <tr>
                            <td align="left" bgcolor="#FFFFFF" ><strong><?php echo $unit_id[$i] . ' ' . $unit_name[$i]; ?></strong></td>
                            <td align="right" bgcolor="#FFFFFF" class="Font8v"><strong><?php echo $total_unit_semula[$i] ?></strong></td>
                            <td align="right" bgcolor="#FFFFFF" class="Font8v"><strong><?php echo $total_unit_ubah[$i] ?></strong></td>
                            <td align="left" bgcolor="#FFFFFF" ><strong><?php echo $unit_id[$i] . ' ' . $unit_name[$i]; ?></strong></td>
                            <td align="right" bgcolor="#FFFFFF" class="Font8v"><strong><?php echo $total_unit_menjadi[$i] ?></strong></td>

                        </tr>
        <?php endif; ?>
        <?php if ($kegiatan_ok[$i] == 't'): ?>
                        <tr bgcolor="black" ><td colspan="6" ><div class=""><hr size="5" color="grey"></div></td>
                        </tr>
                        <tr>
                            <td ><strong><?php echo $kode_kegiatan[$i] . ' ' . $nama_kegiatan[$i] ?></strong></td>
                            <td align="right"><strong><?php echo $total_keg_semula[$i]; ?></strong></td>
                            <td align="right"><strong><?php echo $total_keg_ubah[$i]; ?></strong></td>
                            <td ><strong><?php echo $kode_kegiatan[$i] . ' ' . $nama_kegiatan[$i] ?></strong></td>
                            <td align="right"><strong><?php echo $total_keg_menjadi[$i]; ?></strong></td>

                        </tr>
        <?php endif; ?>
        <?php if ($sub_ok[$i] == 't'): ?>

                               <!--<tr><td colspan="6"  >&nbsp;</td></tr>-->

                        <tr>
                          <!--<td ><strong></strong></td>-->
                            <td colspan="2"><strong><?php echo ':: ' . $rs->getString('subtitle'); ?></strong></td>
                            <td align="right" ><strong><?php echo $nilai_ubah5[$i]; ?></strong></td>
                            <td colspan="2"><strong><?php echo ':: ' . $rs->getString('subtitle'); ?></strong></td>
                        </tr>
        <?php endif; ?>
        <?php if ($nilai_selisih[$i] <> 0 || $nilai_ubah4 <> 0): ?>
                        <tr>
                           <!--<td ><strong></strong></td>
                         <td  ></td>-->
                            <td ><?php echo $rs->getString('rekening_code') . ' ' . $rs->getString('rekening_name'); ?></td>
                            <td align="right"><?php echo $nilai_draft4[$i]; ?></td>
                            <td align="right"><?php echo $nilai_ubah4[$i]; ?></td>
                            <td ><?php echo $rs->getString('rekening_code') . ' ' . $rs->getString('rekening_name'); ?></td>
                            <td align="right"><?php echo $nilai_locked4[$i]; ?></td>

                        </tr>
        <?php endif; ?>
        <?php $i++;
    }while ($rs->next()); ?>
              <!--	  <tr>
                  <td colspan="3"><strong>Total</strong></td>
                  <td align="right"><strong><?php echo $nilai_draft2; ?></strong></td>
                  <td align="right"><strong><?php echo $nilai_locked2; ?></strong></td>
                </tr>-->
            </table>
        </div>
    </div>
<?php endif; ?>
