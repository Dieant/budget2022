<table cellspacing="0" class="sf_admin_list" width="100%">
  <tr>
	<td align="center" bgcolor="#9999FF" width="40%"><strong>Kode Jasmas </strong></td>
	<td align="center" bgcolor="#9999FF"><strong>Nama SKPD </strong></td>
	<td align="center" bgcolor="#9999FF"><strong>Lokasi </strong></td>
	<td align="center" bgcolor="#9999FF"><strong>Total </strong></td>
  </tr>
  <?php $i=0; $rs->first(); ?>
  <?php do{ 
  		$kode_jasmas = $rs->getString('kode_jasmas'); ?>
  <tr>
	<td align="left" bgcolor="#FFFFFF" class="Font8v">(<?php echo $rs->getString('kode_jasmas') ?>) <?php echo $rs->getString('nama') ?></td>
	<?php
		$query = "select u.unit_name,d.detail_name,d.sub,d.kode_sub,sum(d.komponen_harga_awal*d.volume*(100+d.pajak)/100) as harga 
		from unit_kerja u, ". sfConfig::get('app_default_schema') .".rincian_detail d 
		where u.unit_id=d.unit_id and d.kecamatan='$kode_jasmas' and volume>0 
		group by u.unit_name,d.detail_name,d.sub,d.kode_sub,d.pajak 
		order by u.unit_name,d.detail_name, d.kode_sub ";
		$con=Propel::getConnection();
		$stmt=$con->prepareStatement($query);
		$rs_jasmas=$stmt->executeQuery();
		$lokasi = '';
		$nama_skpd = '';
		$i=0;
		while($rs_jasmas->next())
		{
			if($nama_skpd!=$rs_jasmas->getString('unit_name'))
			{
				$nama_skpd = $rs_jasmas->getString('unit_name');
				if($i==0)
				{
	?>
	<td align="center" bgcolor="#FFFFFF" class="Font8v"><?php echo $nama_skpd ?></td>
	<td align="center" bgcolor="#FFFFFF" class="Font8v"></td>
	<td align="center" bgcolor="#FFFFFF" class="Font8v"></td>
</tr>
			
	<?php
				}
				else
				{
	?>
		<tr>
		<td align="left" bgcolor="#FFFFFF" class="Font8v"></td>
		<td align="center" bgcolor="#FFFFFF" class="Font8v"><?php echo $nama_skpd ?></td>
		<td align="center" bgcolor="#FFFFFF" class="Font8v"></td>
		<td align="right" bgcolor="#FFFFFF" class="Font8v"></td>
		</tr>
	<?php
				}
			}
			
			if($rs_jasmas->getString('kode_sub')=='')
			{
	?>
   <tr>
	<td align="left" bgcolor="#FFFFFF" class="Font8v"></td>
	<td align="left" bgcolor="#FFFFFF" class="Font8v"></td>
	<td align="center" bgcolor="#FFFFFF" class="Font8v"><?php echo $rs_jasmas->getString('detail_name') ?></td>
	<td align="right" bgcolor="#FFFFFF" class="Font8v"><?php echo number_format($rs_jasmas->getString('harga'),0,',','.')  ?></td>
  </tr>
	<?php		
			}
			else
			{
				$kode_sub = $rs_jasmas->getString('kode_sub');
				$query = "select detail_name from ". sfConfig::get('app_default_schema') .".rincian_sub_parameter where kode_sub='$kode_sub'";
				$con=Propel::getConnection();
				$stmt=$con->prepareStatement($query);
				$rs_sub=$stmt->executeQuery();
				while($rs_sub->next())
				{
					$detail_name = $rs_sub->getString('detail_name');
				}
	?>
   <tr>
	<td align="left" bgcolor="#FFFFFF" class="Font8v"></td>
	<td align="left" bgcolor="#FFFFFF" class="Font8v"></td>
	<td align="center" bgcolor="#FFFFFF" class="Font8v"><?php echo $detail_name ?></td>
	<td align="right" bgcolor="#FFFFFF" class="Font8v"><?php echo number_format($rs_jasmas->getString('harga'),0,',','.')  ?></td>
  </tr>
	<?php		
			}
			$i+=1;	
		}
		
		$query = "select sum(d.komponen_harga_awal*d.volume*(100+d.pajak)/100) as harga
		from ". sfConfig::get('app_default_schema') .".rincian_detail d 
		where d.kecamatan='$kode_jasmas' and volume>0 ";
		$con=Propel::getConnection();
		$stmt=$con->prepareStatement($query);
		$rs_total=$stmt->executeQuery();
		while($rs_total->next())
		{
	?>
		<tr>
			<td align="left" bgcolor="#FFFFFF" class="Font8v" colspan="2">&nbsp; </td>
			<td align="center" bgcolor="#FFFFFF" class="Font8v"><strong>Total Nilai : </strong></td>
			<td align="right" bgcolor="#FFFFFF" class="Font8v"><?php echo number_format($rs_total->getString('harga'),0,',','.')  ?></td>
		</tr>
	<?php
		}
	?>
		
 
  <?php $i++; ?>
  <?php }while($rs->next()) ?>
  <?php
  
  $query = "select sum(d.komponen_harga_awal*d.volume*(100+d.pajak)/100) as harga
		from ". sfConfig::get('app_default_schema') .".rincian_detail d 
		where d.kecamatan ilike 'J%' and volume>0 ";
		$con=Propel::getConnection();
		$stmt=$con->prepareStatement($query);
		$rs_total=$stmt->executeQuery();
		while($rs_total->next())
		{
	?>
		<tr>
			<td align="left" bgcolor="#FFFFFF" class="Font8v" colspan="2">&nbsp; </td>
			<td align="center" bgcolor="#FFFFFF" class="Font8v"><strong>Total Keseluruhan : </strong></td>
			<td align="right" bgcolor="#FFFFFF" class="Font8v"><?php echo number_format($rs_total->getString('harga'),0,',','.')  ?></td>
		</tr>
	<?php
		}
	?>
 
</table>
