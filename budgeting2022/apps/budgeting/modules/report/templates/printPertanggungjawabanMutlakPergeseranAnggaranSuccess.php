<?php
//echo "Print RKA melalui SABK ".link_to('(http://sabk.surabaya.go.id)','http://sabk.surabaya.go.id', array('target'=>'_blank')).". Tekan tombol BACK untuk kembali";exit;
// auto-generated by sfPropelAdmin
// date: 2008/05/05 10:33:37
?>
<?php use_helper('I18N', 'Date', 'Object', 'Javascript', 'Number') ?>
<?php use_helper('Javascript', 'Number') ?>
<?php
echo use_stylesheet('/css/tampilan_print2.css');
echo javascript_tag("window.print();");

function getDataKegiatan($rs) {
    $kegiatans = array();

    $terakhir = '';
    while ($rs->next()) {
        if (count($kegiatans) == 0) {
            $data = array(
                'kode' => $rs->getString('kegiatan_code'),
                'nama' => $rs->getString('kegiatan_name')
            );
            array_push($kegiatans, $data);
            $terakhir = $data['kode'];
        } else {
            $data = array(
                'kode' => $rs->getString('kegiatan_code'),
                'nama' => $rs->getString('kegiatan_name')
            );
            if ($terakhir != $data['kode']) {
                array_push($kegiatans, $data);
                $terakhir = $data['kode'];
            }
        }
    }

    return $kegiatans;
}

function getDataRekening($kegiatan_kode, $rs) {
    $rekenings = array();

    while ($rs->next()) {
        if ($rs->getString('kegiatan_code') == $kegiatan_kode) {
            $data = array(
                'rekening_name' => $rs->getString('rekening_name'),
                'rekening_code' => $rs->getString('rekening_code'),
                'semula' => $rs->getString('semula'),
                'menjadi' => $rs->getString('menjadi'),
                'selisih' => $rs->getString('selisih')
            );
            array_push($rekenings, $data);
        }
    }
    return $rekenings;
}
?>
<style>
    table{
        border-collapse: collapse;
    }
    .garis {
        border: 1px solid black;
        padding: 5px;
    }
</style>
<?php
$temp_tahap = '';
if ($tahap == 'murni') {
    $temp_tahap = 'MURNI';
} elseif ($tahap == 'revisi1') {
    $temp_tahap = 'Revisi I';
} elseif ($tahap == 'revisi2') {
    $temp_tahap = 'Revisi II';
} elseif ($tahap == 'revisi3') {
    $temp_tahap = 'Revisi III';
} elseif ($tahap == 'revisi4') {
    $temp_tahap = 'Revisi IV';
} elseif ($tahap == 'revisi5') {
    $temp_tahap = 'Revisi V';
} elseif ($tahap == 'pak') {
    $temp_tahap = 'PAK';
} else {
    $temp_tahap = 'Revisi';
}
 // die($unit_id);
?>

<?php

    $pd_bdh_soewandhi = array('0300','1800');

    $pd_bdh_soewandhi_true = false;
    if(in_array($unit_id, $pd_bdh_soewandhi)){
        $pd_bdh_soewandhi_true = true;
    }

?>

<div id="sf_admin_container">
    <h2 width='100%' align="center" > LEMBAR PERTANGGUNGJAWABAN MUTLAK PERGESERAN ANGGARAN TAHUN <?php echo sfConfig::get('app_tahun_default');?> </h2>
    <h2 width='100%' align="center" > <?php echo strtoupper($unit_kerja); ?> </h2>
    <h5>Berdasarkan Peraturan Menteri Dalam Negeri Nomor 77 Tahun 2020 tentang Pedoman Teknis Pengelolaan Keuangan Daerah dan Ketentuan Pasal 6 Ayat (6) Peraturan Walikota Surabaya Nomor 7 Tahun 2022 tentang Tata Cara Pergeseran Anggaran, selanjutnya saya akan bertanggungjawab penuh terhadap usulan dan pelaksanaan kegiatan serta segala akibat yang terjadi karena pergeseran anggaran sebagaimana berikut :</h5>
    

    <table style="empty-cells: show;" border="0" cellpadding="3" cellspacing="0" width="100%">
        <td valign='top'>
            <table width='100%' style="font-size: 0.8em" class="garis">
                <thead>
                    <th class="garis" bgcolor="#d0d0e1">No</th>
                    <th class="garis" colspan='1' bgcolor="#d0d0e1">Kode Kegiatan</th>
                    <th class="garis" colspan='1' bgcolor="#d0d0e1">Nama Kegiatan</th>
                    <th class="garis" colspan='1' bgcolor="#d0d0e1">Kode Rekening</th>
                    <th class="garis" colspan='1' bgcolor="#d0d0e1">Uraian Kode Rekening</th>
                    <th class="garis" colspan='1' bgcolor="#d0d0e1">Semula</th>
                    <th class="garis" colspan='1' bgcolor="#d0d0e1">Menjadi</th>
                    <th class="garis" colspan='1' bgcolor="#d0d0e1">Selisih</th>
                </thead>
                <?php
               
                if($tahap_angka == -1)
                {
                $query = "select max(id) as id from " . sfConfig::get('app_default_schema') . ".deskripsi_resume_rapat where unit_id='$unit_id'";
                    
                }
                else
                {
                 $query = "select max(id) as id from " . sfConfig::get('app_default_schema') . ".deskripsi_resume_rapat where unit_id='$unit_id' and tahap=$tahap_angka";
                }
                
                $con = Propel::getConnection();
                $stmt = $con->prepareStatement($query);
                $rs = $stmt->executeQuery();
                if($rs->next()){
                    $id_max = $rs->getString('id');
                }

                $query = "select b.kegiatan_code, b.kegiatan_name, rekening_code, rekening_name, semula, menjadi, 
                    (menjadi-semula) as selisih 
                    from " . sfConfig::get('app_default_schema') . ".deskripsi_resume_rapat_rekening a, " . sfConfig::get('app_default_schema') . ".deskripsi_resume_rapat_kegiatan b
                    where a.id_deskripsi_resume_rapat=$id_max and 
                    a.id_deskripsi_resume_rapat=b.id_deskripsi_resume_rapat and a.kegiatan_code=b.kegiatan_code 
                    order by kegiatan_code, rekening_code";

                $con = Propel::getConnection();
                $stmt = $con->prepareStatement($query);
                $rs = $stmt->executeQuery();
                //pengambilan data sesuai dengan rekening yang diambil

                $kegiatans = getDataKegiatan($rs);

                $ke = 1;
                foreach ($kegiatans as $kegiatan) {                    
                    $rekenings = getDataRekening($kegiatan['kode'], $stmt->executeQuery());
                    $span = count($rekenings);
                    $t = 0;
                    $is_f1 = false;
                    $semula = 0;
                    $menjadi = 0;
                    if($tahap != null)
                    {
                        $query1 =  "select catatan_pembahasan from " . sfConfig::get('app_default_schema') .".".$tahap."_master_kegiatan WHERE kode_kegiatan = '" . $kegiatan['kode'] . "'";
                    }
                    else
                    {
                        $query1 =  "select catatan_pembahasan from " . sfConfig::get('app_default_schema') .".dinas_master_kegiatan WHERE kode_kegiatan = '" . $kegiatan['kode'] . "'";
                    }
                    $con = Propel::getConnection();
                    $stmt2 = $con->prepareStatement($query1);
                    $rs2 = $stmt2->executeQuery();
                    if($rs2->next()){
                        $catatan = $rs2->getString('catatan_pembahasan');
                    }
                    
                    if($catatan != '') {
                    foreach ($rekenings as $rekening) {
                        if ($rekening['rekening_name'] == '0' && $rekening['rekening_code'] == '0') {
                            $is_f1 = true;
                        }
                        ?>

                        <tr>
                            <?php
                            if ($t == 0) {
                                ?>
                                <td class="garis" rowspan='<?php echo $span ?>'><?php echo ++$no; ?></td>
                                <td class="garis" rowspan='<?php echo $span ?>' class="text-bold text-left" ><?php echo $kegiatan['kode']; ?></td>
                                <td class="garis" rowspan='<?php echo $span ?>' class="text-bold text-left"><?php echo $kegiatan['nama']; ?><br/>Alasan/Catatan Pergeseran Anggaran: <br/> <?php echo $catatan; ?></td>

                                <?php
                            }
                            ?>
                            <td class="garis"  class="text-bold text-left"><?php if (!$is_f1) echo $rekening['rekening_code'] ?></td>
                            <td class="garis"  class="text-bold text-left"><?php if (!$is_f1) echo $rekening['rekening_name'] ?></td>
                            <td class="garis"  class="text-right" style="text-align:right"><?php if (!$is_f1) echo number_format($rekening['semula'], 0, '.', ',') ?></td>
                            <td class="garis"  class="text-right" style="text-align:right"><?php if (!$is_f1) echo number_format($rekening['menjadi'], 0, '.', ',') ?></td>
                            <td class="garis"  class="text-right" style="text-align:right"><?php if (!$is_f1) echo number_format($rekening['selisih'], 0, '.', ',') ?></td>

                            <?php
                            $t = 1;
                            $semula += $rekening['semula'];
                            $menjadi += $rekening['menjadi'];
                            ?>
                        </tr>
                    <?php } ?>
                    <tr>
                        <td colspan="5" class="garis" class="text-right" style="text-align:right; font-weight: bold;">Total: </td>
                        <td class="garis" class="text-right" style="text-align:right; font-weight: bold;"><?php echo number_format($semula, 0, '.', ','); ?></td>
                        <td class="garis" class="text-right" style="text-align:right; font-weight: bold;"><?php echo number_format($menjadi, 0, '.', ','); ?></td>
                        <td class="garis" class="text-right" style="text-align:right; font-weight: bold;"><?php echo number_format($menjadi - $semula, 0, '.', ','); ?></td>
                    </tr>
                <?php }} ?>
            </table>
        <tr>
            <td colspan="20"><p></p></td>
        </tr>
    </table>
    <br/>
    <table width="100%">
        <tr>
            <td width="50%" align="center"></td>
            <td width="50%" align="center">Surabaya, <?php echo $tanggal; ?></td>
        </tr>
        <tr>
            <td width="50%" align="center"></td>
            <td width="50%" align="center">&nbsp;</td>
        </tr>
        <tr>
            <td width="50%" align="center"></td>
            <td width="50%" align="center">
                <?php 
                    if($pd_bdh_soewandhi_true == true) {
                        echo "PENGGUNA ANGGARAN";
                    }else{
                        echo "PENGGUNA ANGGARAN / KEPALA SKPD";
                    }
                ?>
                
            </td>
        </tr>
        <tr>
            <td align="center" width="50%" rowspan="5"></td>
        </tr>
        <tr>
            <td align="center" width="50%">
                <?php 
                    if($pd_bdh_soewandhi_true != true) {
                        echo "PENGUSUL PERGESERAN ANGGARAN";
                    }
                ?>
            </td>
        </tr>
        <tr>
            <td height="140px"></td>
        </tr>  
        <tr>
            <!--<td align="center" width="50%"><?php //echo strtoupper($nama_kpa);                 ?></td>-->
            <td align="center" width="50%"><?php echo $nama_pa; ?></td>
        </tr>
        <tr>
            <!--<td align="center" width="50%"><?php //echo 'NIP :' . strtoupper($nip_kpa);                 ?></td>-->
            <td align="center" width="50%"><?php echo 'NIP :' . strtoupper(substr($nip_pa, 0, 18)); ?></td>
        </tr>
    </table>

</div>
