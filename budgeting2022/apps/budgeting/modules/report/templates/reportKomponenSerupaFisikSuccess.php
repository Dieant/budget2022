<?php use_helper('Form', 'Object', 'Javascript') ?>
<?php
if (sfConfig::get('app_tahap_edit') == 'murni') {
    $nama_sistem = 'Pra-RKA';
} elseif (sfConfig::get('app_tahap_edit') == 'pak') {
    $nama_sistem = 'PAK';
} elseif (sfConfig::get('app_tahap_edit') == 'penyesuaian') {
    $nama_sistem = 'Penyesuaian';
} else {
    $nama_sistem = 'Revisi';
}
?>
<div class="box box-info">
    <div class="box-header with-border">
        Nama Komponen
    </div>
    <div class="box-body">
        <?php
        echo input_tag('komponen', $filter_komponen, array('id' => 'komponen', 'class' => 'form-control'));
        ?>
    </div>
    <div class="box-body">
        <?php echo link_to_function('Filter', 'filterKomponen()', array('class' => 'btn btn-flat btn-success')); ?>
    </div>
</div>
<div class="box box-info">
    <div class="box-header with-border">
        Laporan Komponen Serupa Fisik
    </div>
    <div class="box-body  table-responsive">
        <table class="table table-bordered">
            <tr>
                <th class="text-center">Komponen</th>
                <th class="text-center">Dinas</th>
                <th class="text-center">Kegiatan</th>
                <th class="text-center">Subtitle</th>
                <th class="text-center">Sub Subtitle</th>
                <th class="text-center">Keterangan</th>
                <th class="text-center">Rekening</th>
                <th class="text-center">Koefisien</th>
                <th class="text-center">Catatan</th>
            </tr>
            <?php
            foreach ($komponen as $value):
                if (isset($value['komponen_ok'])):
                    ?>
                    <tr><td colspan="10">&nbsp;</td></tr>
                    <tr>
                        <td colspan="10" class="text-left text-bold"><?php echo $value['komponen_id'] . ' - ' . $value['komponen_name'] ?></td>
                    </tr>
                <?php endif;?>
                <tr>
                    <td>&nbsp;</td>
                    <td class="text-left" ><?php echo $value['unit_id'] . ' - ' . $value['unit_name'] ?></td>
                    <td class="text-left" ><?php echo $value['kode_kegiatan'] . ' - ' . $value['nama_kegiatan'] ?></td>
                    <td class="text-left" ><?php echo $value['subtitle'] ?></td>
                    <td class="text-left" ><?php echo $value['sub'] ?></td>
                    <td class="text-left" ><?php echo $value['detail_name'] ?></td>
                    <td class="text-left" ><?php echo $value['rekening_code'] . ' - ' . $value['rekening_name'] ?></td>
                    <td class="text-left" ><?php echo $value['keterangan_koefisien'] ?></td>
                    <td class="text-left" ><?php echo $value['note_skpd'] ?></td>
                </tr>
            <?php endforeach; ?>
        </table>       
    </div>
</div>
<script>

    function filterKomponen() {
        $('#indicator').show();
        var id = $("#komponen").val();
        if (id == '') {
            id = '';
        }
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/report/reportKomponenSerupaFisik/komponen/" + id + ".html",
            context: document.body
        }).done(function (msg) {
            $('#indicator').hide();
            $('#isi').html(msg);
        });
    }
</script>