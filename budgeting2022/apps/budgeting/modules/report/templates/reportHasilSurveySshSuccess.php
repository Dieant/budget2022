<?php use_helper('Form', 'Object', 'Javascript') ?>
<?php
if (sfConfig::get('app_tahap_edit') == 'murni') {
    $nama_sistem = 'Pra-RKA';
} elseif (sfConfig::get('app_tahap_edit') == 'pak') {
    $nama_sistem = 'PAK';
} elseif (sfConfig::get('app_tahap_edit') == 'penyesuaian') {
    $nama_sistem = 'Penyesuaian';
} else {
    $nama_sistem = 'Revisi';
}
?>
<div class="box box-primary box-solid">        
    <div class="box-header with-border">
        List Hasil Survey SSH
    </div>
    <div class="box-body">
            <div class="box-body">
                <?php echo form_tag('report/printReportHasilSurveySsh', array('method' => 'get', 'class' => 'form-horizontal')) ?>
                <div class="form-group">
                    <div class="col-xs-8">
                        <div class="form-group">
                            <label class="col-xs-4 control-label">Surveyor :</label>
                            <div class="col-xs-3">
                                <?php
                                $c = new Criteria();
                                $c->addAscendingOrderByColumn(SurveyorPeer::NAMA);
                                $v = SurveyorPeer::doSelect($c);
                                echo select_tag('surveyor', objects_for_select($v, 'getNama', 'getNama', isset($surveyor) ? 
                                    $surveyor :'', array('include_custom' => '---Pilih Surveyor---')), array('class' => 'form-control js-example-basic-single'));
                                ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-xs-4 control-label">Tanggal Awal :</label>
                            <div class="col-xs-3">
                                <input type="date" name="tanggal_awal" class="form-control">  
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-xs-4 control-label">Tanggal Akhir :</label>
                            <div class="col-xs-3">
                                <input type="date" name="tanggal_akhir" class="form-control">  
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-xs-4 control-label"></label>
                            <div class="col-xs-3">
                                <input type="submit" name="submit" value="Download Hasil Survey SSH" class="btn btn-flat btn-block btn-default text-bold col-xs-4"/>
                            </div>
                        </div>

                    </div>
                </div>
                <?php echo '</form>'; ?>
            </div>
        </div>
</div>
