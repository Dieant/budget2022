<?php use_helper('Form', 'Object', 'Javascript') ?>

<div class="box box-info">        
    <div class="box-header with-border">
        Pilih SKPD
        <div class="box-tools pull-right">
            <?php if($unit_kerja != NULL && $unit_kerja != '0000'): ?>
            <!-- <?php echo link_to('Export PDF', 'report/buatPDFReportUrusanProgramOutput', array('class' => 'btn btn-flat btn-warning text-bold')); ?> -->
            <!-- <?php
            echo form_tag('report/buatPDFReportUrusanProgramOutput');
            echo input_hidden_tag('unit_id', $unit_kerja);
            echo submit_tag('Export PDF', array('name' => 'export', 'class' => 'btn btn-success btn-flat'));
            echo '</form>'
            ?>

            <?php endif; ?> -->
        </div>
    </div>
    <div class="box-body">
        <?php
        $e = new Criteria();
        $e->add(UnitKerjaPeer::UNIT_ID, '9999', Criteria::NOT_IN);
        $e->addAscendingOrderByColumn(UnitKerjaPeer::UNIT_NAME);
        $unit_kerja = UnitKerjaPeer::doSelect($e);
        echo select_tag('unit_id', objects_for_select($unit_kerja, 'getUnitId', 'getUnitName', (isset($filter_unit_kerja) ? $filter_unit_kerja : NULL), array('include_custom' => '------Pilih Dinas------')), array('id' => 'unit_id', 'class' => 'form-control'));
        ?>
    </div>
</div>


<div class="box box-info">

    <div class="box-header with-border">
        Laporan Urusan Dinas Program Output
        <div class="box-tools pull-right">
            <?php echo link_to('Download PDF Perubahan Output', sfConfig::get('app_path_pdf') . $nama_file . ".pdf", array('class' => 'btn btn-flat btn-warning text-bold')); ?>
        </div>
    </div>
    <div class="box-body">
        <table class="table table-bordered">
            <!-- <tr>
                <th colspan="6" class="text-center">SEMULA</th>
                <th colspan="6" class="text-center">MENJADI</th>
            </tr> -->
            <tr>
                <th class="text-center">OPD</th>
                <th class="text-center">Kode</th>
                <th colspan="3" class="text-center">Urusan-Program-Kegiatan</th>
                <th class="text-center">Output Semula</th>
                
                <!-- tabel sisi menjadi -->
                <!-- <th class="text-center">Kode</th> -->
                <!-- <th colspan="3" class="text-center">Urusan-Program-Kegiatan</th> -->
                <th colspan="6" class="text-center">Output Menjadi</th>
                <!-- <th class="text-center">Organisasi</th> -->
            </tr>
            <?php
            foreach ($kegiatan as $value):
                if (isset($value['urusan_ok'])) {
                    ?>
                    <tr>
                        <td style="padding: 2px"> </td>
                        <td class="text-left text-bold"><?php echo $value['kode_urusan'] ?></td>
                        <td colspan="5" class="text-left text-bold"><?php echo $value['nama_urusan'] ?></td>
                        <!-- tabel sisi menjadi -->
                        <!-- <td class="text-left text-bold"><?php echo $value['kode_urusan'] ?></td>
                        <td colspan="5" class="text-left text-bold"><?php echo $value['nama_urusan'] ?></td> -->
                    </tr>
                    <?php
                }
                if (isset($value['program_ok'])) {
                    ?>
                    <tr>
                        <td style="padding: 2px"> </td>
                        <td class="text-left text-bold"><?php echo $value['kode_program'] ?></td>
                        <td style="padding: 2px"> </td>
                        <td colspan="3" class="text-left text-bold"><?php echo $value['nama_program'] ?></td>
                        <!-- tabel sisi menjadi -->
                        <!-- <td class="text-left text-bold"><?php echo $value['kode_program'] ?></td>
                        <td style="padding: 2px">&nbsp;</td>
                        <td colspan="4" class="text-left text-bold"><?php echo $value['nama_program'] ?></td> -->
                    </tr>
                <?php } ?>
                <tr
                    <?php 
                    if($value['output'] != $value['output_jadi']){

                    ?> 
                    style="background-color: pink"
                    <?php
                    }
                    ?>
                >
                    <td class="text-left" ><?php echo $value['unit_name'] ?></td>
                    <td class="text-left" ><?php echo $value['kode_kegiatan'] ?></td>
                    <td style="padding: 2px">&nbsp;</td>
                    <td style="padding: 2px">&nbsp;</td>
                    <td class="text-left" ><?php echo $value['nama_kegiatan'] ?></td>
                    <td class="text-left" ><?php echo $value['output'] ?></td>
                    <!-- tabel sisi menjadi -->
                    <!-- <td class="text-left" ><?php echo $value['kode_kegiatan'] ?></td>
                    <td class="text-left" ><?php echo $value['nama_kegiatan'] ?></td> -->
                    <td class="text-left" ><?php echo $value['output_jadi'] ?></td>
                    <!-- <td class="text-left" ><?php echo $value['unit_name'] ?></td> -->

                </tr>
            <?php endforeach; ?>
        </table>
    </div>
</div>

<script>
    $("#unit_id").change(function () {
        $('#indicator').show();
        var id = $(this).val();
        if (id == '') {
            id = '0000';
        }
        var tahap = "<?php echo $tahap; ?>";
        var nama_file = "<?php echo $nama_file; ?>";
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/report/reportUrusanProgramOutput/unit_id/" + id + "/tahap/" + tahap + "/nama_file/" + nama_file + ".html",
            context: document.body
        }).done(function (msg) {
            $('#indicator').hide();
            $('#isi').html(msg);
        });
    });
</script>