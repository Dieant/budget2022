<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="fas fa-align-left"></i> Laporan Urusan Dinas Program Alokasi
                        </h3>
                    </div>
                    <div class="card-body"> 
                        <table class="table table-bordered">
                            <tr>
                                <th class="text-center">Kode</th>
                                <th colspan="7" class="text-center">Urusan-Perangkat Daerah Langsung</th>
                                <th colspan="2" class="text-center">Output</th>
                                <th class="text-center">Alokasi</th>
                            </tr>
                            <?php
                            foreach ($kegiatan as $value):
                                if (isset($value['urusan1_ok'])) {
                                    ?>
                                    <tr>
                                        <td class="text-left text-bold"><?php echo $value['kode_urusan1'] ?></td>
                                        <td colspan="10" class="text-left text-bold"><?php echo $value['nama_urusan1'] ?></td>
                                    </tr>
                                    <?php
                                }
                                if (isset($value['urusan2_ok'])) {
                                    ?>
                                    <tr>
                                        <td class="text-left text-bold"><?php echo $value['kode_urusan2'] ?></td>
                                        <td style="padding: 2px">&nbsp;</td>
                                        <td colspan="9" class="text-left text-bold"><?php echo $value['nama_urusan2'] ?></td>
                                    </tr>
                                    <?php
                                }
                                if (isset($value['urusan3_ok'])) {
                                    ?>
                                    <tr>
                                        <td class="text-left text-bold"><?php echo $value['kode_urusan3'] ?></td>
                                        <td style="padding: 2px">&nbsp;</td>
                                        <td style="padding: 2px">&nbsp;</td>
                                        <td colspan="8" class="text-left text-bold"><?php echo $value['nama_urusan3'] ?></td>
                                    </tr>
                                    <?php
                                }
                                if (isset($value['urusan_ok'])) {
                                    ?>
                                    <tr>
                                        <td class="text-left text-bold"><?php echo $value['kode_urusan'] ?></td>
                                        <td style="padding: 2px">&nbsp;</td>
                                        <td style="padding: 2px">&nbsp;</td>
                                        <td style="padding: 2px">&nbsp;</td>
                                        <td colspan="6" class="text-left text-bold"><?php echo $value['nama_urusan'] ?></td>
                                        <td class="text-right text-bold"><?php echo number_format($value['nilai_urusan'], 0, ',', '.') ?></td>
                                    </tr>
                                    <?php
                                }
                                if (isset($value['dinas_ok'])) {
                                    ?>
                                    <tr>
                                        <td style="padding: 2px">&nbsp;</td>
                                        <td style="padding: 2px">&nbsp;</td>
                                        <td style="padding: 2px">&nbsp;</td>
                                        <td style="padding: 2px">&nbsp;</td>
                                        <td style="padding: 2px">&nbsp;</td>
                                        <td colspan="5" class="text-left text-bold"><?php echo $value['unit_name'] ?></td>
                                        <td class="text-right text-bold"><?php echo number_format($value['nilai_dinas'], 0, ',', '.') ?></td>
                                    </tr>
                                    <?php
                                }
                                if (isset($value['program_ok'])) {
                                    ?>
                                    <tr>
                                        <td class="text-left text-bold"><?php echo $value['kode_program'] ?></td>
                                        <td style="padding: 2px">&nbsp;</td>
                                        <td style="padding: 2px">&nbsp;</td>
                                        <td style="padding: 2px">&nbsp;</td>
                                        <td style="padding: 2px">&nbsp;</td>
                                        <td style="padding: 2px">&nbsp;</td>
                                        <td colspan="4" class="text-left text-bold"><?php echo $value['nama_program'] ?></td>
                                        <td class="text-right text-bold"><?php echo number_format($value['nilai_program'], 0, ',', '.') ?></td>
                                    </tr>
                                <?php } ?>
                                <tr>
                                    <td class="text-left" ><?php echo $value['kegiatan_id'] ?></td>
                                    <td style="padding: 2px">&nbsp;</td>
                                    <td style="padding: 2px">&nbsp;</td>
                                    <td style="padding: 2px">&nbsp;</td>
                                    <td style="padding: 2px">&nbsp;</td>
                                    <td style="padding: 2px">&nbsp;</td>
                                    <td style="padding: 2px">&nbsp;</td>
                                    <td class="text-left" ><?php echo $value['nama_kegiatan'] ." (".$value['user_id'].")" ?></td>
                                    <td class="text-left" ><?php echo $value['output1'] ?></td>
                                    <td class="text-left" ><?php echo $value['output2'] ?></td>
                                    <td class="text-right" ><?php echo number_format($value['alokasi'], 0, ',', '.') ?></td>
                                </tr>
                            <?php endforeach; ?>
                                <tr>
                                    <td class="text-center" colspan="10"><b> GRAND TOTAL </b></td>
                                    <td class="text-right"><b> <?php echo number_format($grandTotal, 0, ',', '.'); ?> </b></td>
                                </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- <div class="box box-info">
    <div class="box-header with-border">
        
    </div>
    <div class="box-body">
        
    </div>
</div> -->