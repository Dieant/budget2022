<?php
echo input_hidden_tag('unit_id', $unit_id);
echo input_hidden_tag('kode_kegiatan', $kode_kegiatan);
?>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label class="col-sm-3 control-label">Alasan/Catatan Pergeseran Anggaran</label>
                    <div class="col-sm-9">
                        <?php echo input_tag('catatan_tolak', null, array('class' => 'form-control')) . '<br>'; ?>
                    </div>
                </div>
                <?php echo submit_tag('Kembalikan ke entri', array('name' => 'balik', 'class' => 'btn btn-success btn-flat')) . '&nbsp;'; ?>
            </div>
        </div>
    </div>
</div>