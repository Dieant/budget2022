<?php use_helper('Form','Object','Javascript') ?>

<?php

echo select_tag('criteria', objects_for_select($unit_kerja,'getUnitId', 'getUnitName',
                        '',array('include_custom'=>'Pilih SKPD')),
     Array('id'=>'combo_pilih','onChange'=>remote_function(Array('update'=>'isi', 'url'=>'report/reportSemulaMenjadi2',
    'with'=>"'b=rsm2&where='+this.options[this.selectedIndex].value",
    'loading'=>"Element.show('indicator');Element.hide('isi');Element.hide('isi_dalam');",
    'complete'=>"Element.hide('indicator');Element.show('isi');".visual_effect('highlight', 'isi')))));
?>
<br/><br/><br/>
<div id="isi"></div>
<?php //echo  '<b>SKPD : '.$unit_name.' ('.$unit_id.')</b>';?>
<?php if(isset ($rs)):?>
        <table cellspacing="0" class="sf_admin_list" width="100%">
	
          <tr>
            <td align="center" width="17"><strong></strong></td>
            <td ><div align="center"></div></td>
            <td align="center"><strong> Unit - Kegiatan - Rekening</strong></td>
            <td align="center"><strong> Buku Putih </strong></td>
            <td align="center"><strong> RKA </strong></td>
            <td align="center"><strong> Selisih </strong></td>
          </tr>
          
	  <?php $i=0; $rs->first(); do{      ?>
          <?php if ($dinas_ok[$i]=='t'): ?>
            <tr><td colspan="7"  bgcolor="#FFFFFF"  >&nbsp;</td></tr>
          <tr>
            <td colspan="3" align="left" bgcolor="#FFFFFF" ><strong><?php echo $unit_id[$i].' '.$unit_name[$i];?></strong></td>
            <td align="right" bgcolor="#FFFFFF" class="Font8v"><strong><?php echo $total_unit_semula[$i] ?></strong></td>
            <td align="right" bgcolor="#FFFFFF" class="Font8v"><strong><?php echo $total_unit_menjadi[$i] ?></strong></td>
            <td align="right" bgcolor="#FFFFFF" class="Font8v"><strong><?php echo $total_unit_ubah[$i] ?></strong></td>
          </tr>
          <?php endif; ?>
          <?php if($kegiatan_ok[$i]=='t'): ?>
		  <tr bgcolor="black" ><td colspan="6" ><div class=""><hr size="5" color="grey"></div></td>
		  </tr>
          <tr>
            <td colspan="3" ><strong><?php echo $kode_kegiatan[$i].' '.$nama_kegiatan[$i]?></strong></td>
            <td align="right"><strong><?php echo $total_keg_semula[$i];?></strong></td>
            <td align="right"><strong><?php echo $total_keg_menjadi[$i];?></strong></td>
            <td align="right"><strong><?php echo $total_keg_ubah[$i];?></strong></td>
          </tr>
          <?php endif; ?>
          <?php if($sub_ok[$i]=='t'): ?>

		   <tr><td colspan="6"  >&nbsp;</td></tr>

          <tr>
            <td ><strong></strong></td>
            <td colspan="4"  ><strong><?php echo $rs->getString('subtitle') ;?></strong></td>
            <td align="right" ><strong><?php echo $nilai_ubah5[$i]; ?></strong></td>
          </tr>
       	<?php endif; ?>
          <?php if ($nilai_selisih[$i] <> 0 || $nilai_ubah4 <> 0): ?>
         <tr>
            <td ><strong></strong></td>
          <td  ></td>
            <td ><?php echo $rs->getString('rekening_code').' '.$rs->getString('rekening_name');?></td>
            <td align="right"><?php echo $nilai_draft4[$i];?></td>
            <td align="right"><?php echo $nilai_locked4[$i];?></td>
            <td align="right"><?php echo $nilai_ubah4[$i];?></td>
          </tr>
          <?php endif; ?>
         <?php $i++; }while($rs->next()); ?>
	<!--	  <tr>
            <td colspan="3"><strong>Total</strong></td>
            <td align="right"><strong><?php echo $nilai_draft2;?></strong></td>
            <td align="right"><strong><?php echo $nilai_locked2;?></strong></td>
          </tr>-->
        </table>
<?php  endif; ?>
