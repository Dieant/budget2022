<div class="box box-info">
    <div class="box-header with-border">
<!--        Laporan Urusan Dinas Alokasi-->
    </div>
    <div class="box-body">
        <table class="table table-bordered">
            <tr>
                <th colspan="6" class="text-center">Urusan-Perangkat Daerah Penanggung Jawab</th>
<!--                <th colspan="2" class="text-center">Plafon Anggaran Sementara</th>-->
                <th colspan="2" class="text-center">Plafon Anggaran Semula</th>
                <th colspan="2" class="text-center">Plafon Anggaran Menjadi</th>
<!--
                <th colspan="2" class="text-center">Plafon Anggaran Menjadi</th>-->
            </tr>

            <?php
            foreach ($dinas as $value):
                if (isset($value['urusan1_ok'])) 
                {  
                    ?>
                    <tr>
<!--                        angka 1-->
                        <td class="text-left text-bold">
                            <?php 
                                echo $value['kode_urusan1'];
                            ?>
                        </td>
<!--                        Urusan Pemerintahan-->
                        <td colspan="5" class="text-left text-bold">
                            <?php echo $value['nama_urusan1']; 
                            ?>
                        </td>
                        <td colspan="2"></td>
                        <td></td>
                    </tr>
                    <?php 
                }
                if (isset($value['urusan2_ok'])) {
                    ?>
                    <tr><!-- 1.1 -->
                        <td class="text-left text-bold">
                            <?php 
                                echo $value['kode_urusan2'];
                            ?>
                        </td>
                        <td style="padding: 2px">&nbsp;</td>
                        <td colspan="4" class="text-left text-bold">
                            <?php 
                                echo $value['nama_urusan2'];
                            ?>
                        </td>
                        <td colspan="2"></td>
                        <td></td>
                    </tr>
                    <?php
                }
                if (isset($value['urusan3_ok'])) {
                    ?>
                    <tr> <!-- 1.1.1 -->
                        <td class="text-left text-bold">
                            <?php 
                            echo $value['kode_urusan3'];
                            ?>
                        </td>
                        <td style="padding: 2px">&nbsp;</td>
                        <td style="padding: 2px">&nbsp;</td>
                        <td colspan="3" class="text-left text-bold">
                            <?php 
                                echo $value['nama_urusan3'];
                            ?>
                        </td>
                        <td colspan="2"></td>
                        <td></td>
                    </tr>
                    <?php
                }
                if (isset($value['urusan_ok'])) {
                    ?>
                    <tr> <!-- 1.1.1.01 -->
                        <td class="text-left text-bold"><?php echo $value['kode_urusan']; ?></td>
                        <td style="padding: 2px"></td>
                        <td style="padding: 2px">&nbsp;</td>
                        <td style="padding: 2px">&nbsp;</td>
                        <td colspan="2" class="text-left text-bold"><?php echo $value['nama_urusan']; ?></td>
                        <td colspan="2" class="text-right text-bold"><?php echo number_format($value['nilai_urusan'], 0, ',', '.'); ?></td>
                        <td class="text-right text-bold"><?php echo number_format($value['nilai_urusan_jd'], 0, ',', '.'); ?></td>
                    </tr>
                <?php } ?>
                <tr>
                    <td style="padding: 2px">&nbsp;</td>
                    <td style="padding: 2px">&nbsp;</td>
                    <td style="padding: 2px">&nbsp;</td>
                    <td style="padding: 2px">&nbsp;</td>
                    <td style="padding: 2px">&nbsp;</td>
                    <td class="text-left" >
                        <?php 
                            echo $value['unit_name'];
                        ?>
                    </td>
                    <td colspan="2" class="text-right" ><?php echo number_format($value['nilai'], 0, ',', '.');?></td>
                    <td class="text-right" ><?php echo number_format($value['pernilaijadi'], 0, ',', '.'); ?></td>
                </tr>
            <?php endforeach; ?>
                <tr>
                    <td class="text-center" colspan="6"><b> GRAND TOTAL </b></td>
                    <td colspan="2" class="text-right"><b> <?php echo number_format($grandTotal, 0, ',', '.'); ?> </b></td>
                    <td class="text-right"><b> <?php echo number_format($grandTotaljadi, 0, ',', '.'); ?> </b></td>
                </tr>
        </table>
    </div>
</div>