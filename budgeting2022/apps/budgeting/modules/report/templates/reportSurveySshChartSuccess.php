<?php use_helper('I18N', 'Date', 'Object', 'Javascript', 'Validation') ?>

<style>
    .tbl_surveyor thead{
        font-weight: bold;
        text-align: center;
        /* vertical-align: middle; */
    }
    .tbl_surveyor thead tr td{
        vertical-align: middle;
    }
    #value_perolehan{
        text-align: right;
    }
    .tbl_surveyor tbody td:not(:first-child){
        text-align: center;
    }
</style>

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>Chart Pencapaian Survey</h1>
</section>

<!-- Main content -->
<section class="content" id="perolehan_total_survey_shs">
    <div class="row">
        <div class="col-sm">
            <table class="table table-bordered tbl_perolehan_total_survey_shs" style="width: 50%;">
                <thead style="background: #ffab73; color: black;">
                    <tr>
                        <td colspan="3" style="text-align: center;"><b>PEROLEHAN TOTAL SURVEY SHS 2022</b></td>
                    </tr>
                    <tr>
                        <td style="text-align: center; vertical-align: middle;"><b>NAMA DATA</b></td>
                        <td style="width: 100px; text-align: center; vertical-align: middle;"><b>JUMLAH SHS</b></td>
                        <td style="width: 100px; text-align: center; vertical-align: middle;"><b>(%)</b></td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>SURVEY SHS</td>
                        <td id="value_perolehan"><?= $perolehan_total['survey_shs'] ?></td>
                        <td id="value_perolehan">100</td>
                    </tr>
                    <tr>
                        <td>SHS SUDAH SURVEY</td>
                        <td id="value_perolehan"><?= $perolehan_total['sudah_survey'] ?></td>
                        <td id="value_perolehan"><?= round(($perolehan_total['sudah_survey'] / $perolehan_total['survey_shs']) * 100,2) ?></td>
                    </tr>
                    <tr>
                        <td>SHS BELUM SURVEY</td>
                        <td id="value_perolehan"><?= $perolehan_total['belum_survey'] ?></td>
                        <td id="value_perolehan"><?= round(($perolehan_total['belum_survey'] / $perolehan_total['survey_shs']) * 100,2) ?></td>
                    </tr>
                    <tr>
                        <td>SHS PROSES VERIF</td>
                        <td id="value_perolehan"><?= $perolehan_total['proses_verif'] ?></td>
                        <td id="value_perolehan"><?= round(($perolehan_total['proses_verif'] / $perolehan_total['survey_shs']) * 100,2) ?></td>
                    </tr>
                    <tr>
                        <td>SHS BELUM PROSES VERIF</td>
                        <td id="value_perolehan"><?= $perolehan_total['belum_verif'] ?></td>
                        <td id="value_perolehan"><?= round(($perolehan_total['belum_verif'] / $perolehan_total['survey_shs']) * 100,2) ?></td>
                    </tr>
                    <tr>
                        <td>SHS FINAL VERIF</td>
                        <td id="value_perolehan"><?= $perolehan_total['final_verif'] ?></td>
                        <td id="value_perolehan"><?= round(($perolehan_total['final_verif'] / $perolehan_total['survey_shs']) * 100,2) ?></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</section>

<section class="content mt-4" id="surveyor">
    <div class="row">
        <div class="col-sm">
            <table class="table table-bordered tbl_surveyor">
                <thead style="background: #ffab73; color: black;">
                    <tr>
                        <td rowspan="3">SURVEYOR</td>
                        <td colspan="2" rowspan="2">DATA SURVEY SURVEYOR</td>
                        <td colspan="6">SUDAH SURVEY</td>
                        <td colspan="2" rowspan="2">PROSES VERIF</td>
                        <td colspan="2" rowspan="2">FINAL VERIF</td>
                        <td colspan="2" rowspan="2">TERSURVEY SURVEYOR</td>
                        <td colspan="2" rowspan="2">BELUM SURVEY</td>
                    </tr>
                    <tr>
                        <td colspan="2">SURVEY 1</td>
                        <td colspan="2">SURVEY 2</td>
                        <td colspan="2">SURVEY 3</td>
                    </tr>
                    <tr>
                        <td>Total</td>
                        <td>(%)</td>
                        <td>Total</td>
                        <td>(%)</td>
                        <td>Total</td>
                        <td>(%)</td>
                        <td>Total</td>
                        <td>(%)</td>
                        <td>Total</td>
                        <td>(%)</td>
                        <td>Total</td>
                        <td>(%)</td>
                        <td>Total</td>
                        <td>(%)</td>
                        <td>Total</td>
                        <td>(%)</td>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $all_data_survey = 0;
                        $all_survey1 = 0;
                        $all_survey2 = 0;
                        $all_survey3 = 0;
                        $all_tersurvey = 0;
                        $all_belum_tersurvey = 0;
                        $all_proses_verif = 0;
                        $all_final_verif = 0;
                    ?>
                    <?php if(!empty($detail_per_surveyor)) {
                        foreach($detail_per_surveyor as $detail_surveyor) {  
                            
                        $tersurvey_surveyor = $detail_surveyor['data_survey1'] + $detail_surveyor['data_survey2'] + $detail_surveyor['data_survey3'] + $detail_surveyor['data_proses_verif'] + $detail_surveyor['data_final_verif'];
                    ?>
                    <tr>
                        <td><?php echo $detail_surveyor['surveyor'] ?></td>
                        <td><?php echo $detail_surveyor['data_surveyor'] ?></td>
                        <td></td>
                        <td><?php echo $detail_surveyor['data_survey1'] ?></td>
                        <td></td>
                        <td><?php echo $detail_surveyor['data_survey2'] ?></td>
                        <td></td>
                        <td><?php echo $detail_surveyor['data_survey3'] ?></td>
                        <td></td>
                        <td><?php echo $detail_surveyor['data_proses_verif'] ?></td>
                        <td></td>
                        <td><?php echo $detail_surveyor['data_final_verif'] ?></td>
                        <td></td>
                        <td><?php echo $tersurvey_surveyor ?></td>
                        <td></td>
                        <td><?php echo $detail_surveyor['data_belum_survey'] ?></td>
                        <td></td>
                    </tr>
                    <?php 
                        $all_data_survey += $detail_surveyor['data_surveyor'];
                        $all_survey1 += $detail_surveyor['data_survey1'];
                        $all_survey2 += $detail_surveyor['data_survey2'];
                        $all_survey3 += $detail_surveyor['data_survey3'];
                        // $all_tersurvey += $detail_surveyor['data_sudah_survey'];
                        $all_tersurvey += $tersurvey_surveyor;
                        $all_belum_tersurvey += $detail_surveyor['data_belum_survey'];
                        $all_proses_verif += $detail_surveyor['data_proses_verif'];
                        $all_final_verif += $detail_surveyor['data_final_verif'];
                        } 
                    } 
                    ?>
                    <tr>
                        <td style="text-align: center;">ALL TOTAL</td>
                        <td><?php echo $all_data_survey ?></td>
                        <td></td>
                        <td><?php echo $all_survey1 ?></td>
                        <td></td>
                        <td><?php echo $all_survey2 ?></td>
                        <td></td>
                        <td><?php echo $all_survey3 ?></td>
                        <td></td>
                        <td><?php echo $all_proses_verif ?></td>
                        <td></td>
                        <td><?php echo $all_final_verif ?></td>
                        <td></td>
                        <td><?php echo $all_tersurvey ?></td>
                        <td></td>
                        <td><?php echo $all_belum_tersurvey ?></td>
                        <td></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</section>

<section class="content mt-4" style="display: none;">
    <div class="box box-info">        
        <div class="box-body">
            <canvas id="myChart" width="400" height="400"></canvas>
        </div>
    </div>
</section>

<!---chartJS--->
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.4/dist/Chart.bundle.js"></script>
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.4/dist/Chart.bundle.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.4/dist/Chart.js"></script>
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.4/dist/Chart.js"></script>
<?php use_stylesheet('https://cdn.jsdelivr.net/npm/chart.js@2.9.4/dist/Chart.css'); ?>
<?php use_stylesheet('https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.min.css'); ?>
<script>
    var ctx = document.getElementById('myChart').getContext('2d');
    var myChart = new Chart(ctx, {
       type: 'bar',
       data: {
           datasets: [{
               label: 'Data Survey',
               data: [<?php
                echo $arrData1s;
                ?>],
               backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 0.2)'
                ],
                borderWidth: 1,
               order: 2
           }, {
               label: 'Data Pencapaian Survey',
               data: [<?php
                echo $arrData2s;
            ?>],
               type: 'line',
               // this dataset is drawn on top
               order: 1
           }],
           labels: [ 
           <?php
                echo $arrNamas;
            ?>]
       },
       options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });
   
</script>
