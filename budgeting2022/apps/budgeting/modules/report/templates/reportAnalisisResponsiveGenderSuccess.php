<div class="box box-info">        
    <div class="box-header with-border">
        Laporan Analisis Responsive Gender
    </div>
    <div class="box-body">
        <table class="table table-bordered">
            <tr>
                <th class="text-center">SKPD</th>
                <th class="text-center">Kegiatan</th>
                <th class="text-center">Subtitle</th>
                <th class="text-center">Analisa Situasi</th>
                <th class="text-center">Nilai Usulan</th>
            </tr>
            <?php
            $i = 0;
            $rs->first();
            do {
                if (isset($dinas_ok[$i])):
                    ?>
                    <tr>
                        <td colspan="5">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="text-left text-bold" colspan="3"><?php echo $unit_id[$i] . ' - ' . $unit_name[$i] ?></td>
                        <td class="text-right">&nbsp;</td>
                        <td class="text-right text-bold"><?php echo $nilai_draft[$i] ?></td>
                    </tr>
                    <?php
                endif;
                if (isset($kegiatan_ok[$i])):
                    ?>
                    <tr>
                        <td >&nbsp;</td>
                        <td colspan="2" class="text-bold text-left"><strong><?php echo $kode_kegiatan[$i] . ' - ' . $nama_kegiatan[$i] ?></strong></td>
                        <td >&nbsp;</td>
                        <td class="text-right text-bold"><?php echo $nilai_draft3[$i] ?></td>
                    </tr>
                <?php endif;
                ?>
                <tr>
                    <td colspan="2">&nbsp;</td>
                    <td class="text-left"><?php echo isset($subtitle[$i]) ? $subtitle[$i] : '' ?></td>
                    <td class="text-center"><?php echo isset($catatan[$i]) ? $catatan[$i] : '' ?></td>
                    <td class="text-bold text-right"><?php echo isset($nilai_draft4[$i]) ? $nilai_draft4[$i] : '' ?></td>
                </tr>
                <?php
                $i++;
            } while ($rs->next())
            ?>      
            <tr>
                <td colspan="3" class="text-bold text-center">Total</td>
                <td colspan="2" class="text-bold text-right"><?php echo $nilai_draft2 ?></td>
            </tr>
        </table>       
    </div>
</div>