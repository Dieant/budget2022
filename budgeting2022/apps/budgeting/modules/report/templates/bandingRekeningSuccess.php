<?php
use_helper('Form', 'Object', 'Javascript');
use_stylesheet('/sf/sf_admin/css/main');
//echo javascript_tag("window.print();");




/*
  echo select_tag('criteria', objects_for_select($unit_kerja,'getUnitId', 'getUnitName',
  '',array('include_custom'=>'Pilih SKPD')),
  Array('id'=>'combo_pilih','onChange'=>remote_function(Array('update'=>'isi', 'url'=>'report/reportSemulaMenjadi',
  'with'=>"'b=rsm&where='+this.options[this.selectedIndex].value",
  'loading'=>"Element.show('indicator');Element.hide('isi');Element.hide('isi_dalam');",
  'complete'=>"Element.hide('indicator');Element.show('isi');".visual_effect('highlight', 'isi')))));
 */
?>
<!--<br/><br/><br/>
<div id="isi"></div>-->
<?php //echo  '<b>SKPD : '.$unit_name.' ('.$unit_id.')</b>'; ?>
<div id="sf_admin_container" class="table-responsive">
    <h1 width='760'>Rincian RAPBD Pembahasan</h1>
    <?php //var_dump($rs) ?>
    <table style="empty-cells: show;" border="0" cellpadding="3" cellspacing="0" class="sf_admin_list" width="760">
        <tr class="sf_admin_row_0">
            <td align='right' width='20%'>Urusan Pemerintahan</td>
            <td width='1%' align='center'>:</td>
            <td align='left'><?php echo '(' . $kode_urusan . ')' . '&nbsp;' . $nama_urusan ?></td>
        </tr>
        <tr class="sf_admin_row_0">
            <td align='right' width='20%'>Unit Kerja</td>
            <td width='1%' align='center'>:</td>
            <td align='left'><?php echo '(' . $unit_id_header . ')' . '&nbsp;' . $unit_kerja ?></td>
        </tr>
        <tr class="sf_admin_row_0">
            <td align='right' width='20%'>Kode Kegiatan</td>
            <td width='1%' align='center'>:</td>
            <td><?php echo $kode_kegiatan2 ?></td>
        </tr>
        <tr class="sf_admin_row_0">
            <td align='right' width='20%'>Nama Kegiatan</td>
            <td width='1%' align='center'>:</td>
            <td><?php echo $nama_kegiatan_utama ?></td>
        </tr>
        <tr class="sf_admin_row_0">
            <td align='right' width='20%'>Program</td>
            <td width='1%' align='center'>:</td>
            <td><?php echo $kode_program1 . '&nbsp;' . $nama_program ?></td>
        </tr>
        <tr class="sf_admin_row_0">
            <td align='right' width='20%'>Program P13</td>
            <td width='1%' align='center'>:</td>
            <td><?php echo $kode_program22 . '&nbsp;' . $nama_program22 ?></td>
        </tr>
        <tr class="sf_admin_row_0">
            <td align='right' width='20%'>Nama Sasaran</td>
            <td width='1%' align='center'>:</td>
            <td><?php echo $kode_sasaran . '&nbsp;' . $nama_sasaran ?></td>
        </tr>
        <tr class="sf_admin_row_0">
            <td align='right' width='20%' valign='top'>Target</td>
            <td width='1%' align='center' valign='top'>:</td>
            <td valign='top'>
                <table border="1" width="100%">
                    <tr align='center'>
                        <td><strong>Sub Kegiatan</strong></td>
                        <td><strong>Semula</strong></td>
                        <td><strong>Menjadi</strong></td>
                    </tr>
                    <?php
                    $c = new Criteria();
                    $c->add(RincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan_header);
                    $c->add(RincianDetailPeer::UNIT_ID, $unit_id_header);
                    $c->addAscendingOrderByColumn(RincianDetailPeer::SUBTITLE);
                    $cs = RincianDetailPeer::doSelect($c);
                    $subtitle = '';
                    foreach ($cs as $css) {
                        if ($css->getSubtitle() != '  Biaya Administrasi Umum') {
                            if ($subtitle != $css->getSubtitle()) {
                                $subtitle = $css->getSubtitle();
                                $d = new Criteria();
                                $d->add(SubtitleIndikatorPeer::KEGIATAN_CODE, $kode_kegiatan_header);
                                $d->add(SubtitleIndikatorPeer::UNIT_ID, $unit_id_header);
                                $d->add(SubtitleIndikatorPeer::SUBTITLE, $subtitle);
                                $ds = SubtitleIndikatorPeer::doSelectOne($d);
                                if ($ds) {
                                    ?>
                                    <tr>
                                        <td><?php echo $ds->getSubtitle(); ?></td>
                                        <td>
                                            <?php
                                            $pd = new Criteria();
                                            $pd->add(PrevSubtitleIndikatorPeer::KEGIATAN_CODE, $kode_kegiatan_header);
                                            $pd->add(PrevSubtitleIndikatorPeer::UNIT_ID, $unit_id_header);
                                            $pd->add(PrevSubtitleIndikatorPeer::SUBTITLE, $subtitle);
                                            $pds = PrevSubtitleIndikatorPeer::doSelectOne($pd);
                                            if ($pds) {
                                                echo $pds->getIndikator() . '&nbsp;' . $pds->getNilai() . '&nbsp;' . $pds->getSatuan();
                                            }
                                            ?>
                                        </td>
                                        <td><?php echo $ds->getIndikator() . '&nbsp;' . $ds->getNilai() . '&nbsp;' . $ds->getSatuan() ?></td>
                                    </tr>
                                    <?php
                                }
                            }
                        }
                    }
                    ?>

                </table>
            </td>
        </tr>
        <tr>
            <td align='right' width='20%' valign='top'>Nilai</td>
            <td width='1%' align='center' valign='top'>:</td>
            <td valign='top'>
                <table border="1" width="100%">
                    <tr align='center'>
                        <td>&nbsp;</td>
                        <td><strong>Semula</strong></td>
                        <td><strong>Menjadi</strong></td>
                    </tr>
                    <?php
                    $query = "
										select nilai_sekarang.belanja_name, sum(hasil_kali2) as menjadi, sum(hasil_kali1) as semula
	from

		(
			select k.belanja_name, sum(round(detail2.volume * detail2.komponen_harga_awal * (100+detail2.pajak)/100)) as hasil_kali2
			from " . sfConfig::get('app_default_schema') . ".rincian_detail detail2," . sfConfig::get('app_default_schema') . ".rekening r, " . sfConfig::get('app_default_schema') . ".kelompok_belanja k
			where unit_id='$unit_id_header' and kegiatan_code='$kode_kegiatan_header' and r.rekening_code=detail2.rekening_code and k.belanja_id=r.belanja_id and detail2.status_hapus=false
			group by k.belanja_name
		) as nilai_sekarang,
		(
			select k.belanja_name, sum(round(detail1.volume * detail1.komponen_harga_awal * (100+detail1.pajak)/100)) as hasil_kali1
			from " . sfConfig::get('app_default_schema') . ".prev_rincian_detail detail1," . sfConfig::get('app_default_schema') . ".rekening r, " . sfConfig::get('app_default_schema') . ".kelompok_belanja k
			where unit_id='$unit_id_header' and kegiatan_code='$kode_kegiatan_header' and r.rekening_code=detail1.rekening_code and k.belanja_id=r.belanja_id and detail1.status_hapus=false
			group by k.belanja_name
		) as nilai_murni

	where nilai_sekarang.belanja_name = nilai_murni.belanja_name
	group by nilai_sekarang.belanja_name";

                    $query = "
								select nilai_murni.belanja_name as belanja_semula, nilai_sekarang.belanja_name as belanja_menjadi, sum(nilai_sekarang.hasil_kali2) as menjadi, sum(nilai_murni.hasil_kali1) as semula
	from

		( select k.belanja_name, sum(round(detail2.volume * detail2.komponen_harga_awal * (100+detail2.pajak)/100)) as hasil_kali2
		from " . sfConfig::get('app_default_schema') . ".rincian_detail detail2," . sfConfig::get('app_default_schema') . ".rekening r, " . sfConfig::get('app_default_schema') . ".kelompok_belanja k
		where unit_id='$unit_id_header' and kegiatan_code='$kode_kegiatan_header' and r.rekening_code=detail2.rekening_code and k.belanja_id=r.belanja_id  and detail2.status_hapus=false
		group by k.belanja_name ) as nilai_sekarang left outer join

	( select k.belanja_name, sum(round(detail1.volume * detail1.komponen_harga_awal * (100+detail1.pajak)/100)) as hasil_kali1
		from " . sfConfig::get('app_default_schema') . ".prev_rincian_detail detail1," . sfConfig::get('app_default_schema') . ".rekening r, " . sfConfig::get('app_default_schema') . ".kelompok_belanja k
		where unit_id='$unit_id_header' and kegiatan_code='$kode_kegiatan_header' and r.rekening_code=detail1.rekening_code and k.belanja_id=r.belanja_id  and detail1.status_hapus=false
		group by k.belanja_name ) as nilai_murni
	 on nilai_sekarang.belanja_name= nilai_murni.belanja_name

	group by nilai_murni.belanja_name, nilai_sekarang.belanja_name";
//print_r($query);exit;
                    $con = Propel::getConnection();
                    $stmt = $con->prepareStatement($query);
                    $rs_rekening = $stmt->executeQuery();
                    $rekening_prev = 0;
                    $rekening_now = 0;
                    while ($rs_rekening->next()) {
                        if (!$rs_rekening->getString('belanja_semula')) {
                            $nama_belanja = $rs_rekening->getString('belanja_menjadi');
                        } else {
                            $nama_belanja = $rs_rekening->getString('belanja_semula');
                        }
                        ?>
                        <tr>
                            <td><?php echo $nama_belanja; ?></td>
                            <td align='right'><?php echo number_format($rs_rekening->getString('semula'), 0, ',', '.');
                    $rekening_prev+=$rs_rekening->getString('semula');
                        ?></td>
                            <td align='right'><?php echo number_format($rs_rekening->getString('menjadi'), 0, ',', '.');
                            $rekening_now+=$rs_rekening->getString('menjadi');
                            ?></td>
                        </tr>
                        <?php
                    }
                    ?>
                    <tr>
                        <td>Total</td>
                        <td align='right'><?php echo number_format($rekening_prev, 0, ',', '.'); ?></td>
                        <td align='right'><?php echo number_format($rekening_now, 0, ',', '.'); ?></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan='3'>
                <table width='100%'>
                    <tr>
                        <td valign='top'>
                            <!-- batas header -->		

                            <table style="empty-cells: show;" border="0" cellpadding="3" cellspacing="0" class="sf_admin_list" width="100%">
                                <tr class="sf_admin_row_1">
                                    <td colspan="2" align="center"><strong>Alokasi RAPBD Semula</strong></td>
                                    <td align="center"><strong>|</strong></td>
                                    <td colspan="2" align="center"><strong>RAPBD Pembahasan</strong></td>
                                </tr>

                                <tr class="sf_admin_row_1">
                                  <!--<td align="center" width="17"><strong></strong></td>
                                  <td ><div align="center"></div></td>-->
                                    <td align="center"><strong> Subtitle - Rekening</strong></td>
                                    <td align="center"><strong> Nilai </strong></td>
                                    <td align="center"><strong>|</strong></td>
                                    <td align="center"><strong> Subtitle - Rekening</strong></td>
                                    <td align="center"><strong> Nilai </strong></td>
                                </tr>

                                <?php
                                $i = 0;
                                $rs->first();
                                do {
                                    ?>
    <?php if ($dinas_ok[$i] == 't'): ?>
                 <!--<tr class="sf_admin_row_0"><td colspan="7"  bgcolor="#FFFFFF"  >&nbsp;</td></tr>

                 <tr>
                  <td align="left" bgcolor="#FFFFFF" ><strong><?php echo $unit_id[$i] . ' ' . $unit_name[$i]; ?></strong></td>
                  <td align="right" bgcolor="#FFFFFF" class="Font8v"><strong><?php echo $total_unit_semula[$i] ?></strong></td>
                  <td align="right" bgcolor="#FFFFFF" class="Font8v"><strong><?php echo $total_unit_ubah[$i] ?></strong></td>
                  <td align="left" bgcolor="#FFFFFF" ><strong><?php echo $unit_id[$i] . ' ' . $unit_name[$i]; ?></strong></td>
                  <td align="right" bgcolor="#FFFFFF" class="Font8v"><strong><?php echo $total_unit_menjadi[$i] ?></strong></td>
                  
                </tr>
                                        -->
    <?php endif; ?>
    <?php if ($kegiatan_ok[$i] == 't'): ?>
                                                <!--<tr bgcolor="black" ><td colspan="6" ><div class=""><hr size="5" color="grey"></div></td>
                                                </tr>-->
                                        <tr>
                                            <td ><strong><?php echo $kode_kegiatan[$i] . ' ' . $nama_kegiatan[$i] ?></strong></td>

                                            <td align="right"><strong><?php //echo $total_keg_semula[$i];  ?></strong></td>
                                            <!--<td align="right"><strong><?php //echo $total_keg_ubah[$i];  ?></strong></td>-->
                                            <td>&nbsp;</td>
                                            <td ><strong><?php echo $kode_kegiatan[$i] . ' ' . $nama_kegiatan[$i] ?></strong></td>
                                            <td align="right"><strong><?php //echo $total_keg_menjadi[$i]; ?></strong></td>

                                        </tr>
    <?php endif; ?>
    <?php if ($sub_ok[$i] == 't'): ?>

                                           <!--<tr><td colspan="6"  >&nbsp;</td></tr>-->

                                        <tr class="sf_admin_row_0">
                                          <!--<td ><strong></strong></td>-->
                                            <td colspan="2"><strong><?php echo ':: ' . $rs->getString('subtitle'); ?></strong></td>
                                            <!--<td align="right" ><strong><?php echo $nilai_ubah5[$i]; ?></strong></td>-->
                                            <td>&nbsp;</td>
                                            <td colspan="2"><strong><?php echo ':: ' . $rs->getString('subtitle'); ?></strong></td>
                                        </tr>
    <?php endif; ?>
    <?php if ($nilai_selisih[$i] <> 0 || $nilai_ubah4 <> 0): ?>
                                        <tr class="sf_admin_row_0">
                                           <!--<td ><strong></strong></td>
                                         <td  ></td>-->
                                            <td ><?php echo $rs->getString('rekening_code') . ' ' . $rs->getString('rekening_name'); ?></td>
                                            <td align="right"><?php echo $nilai_draft4[$i]; ?></td>
                                            <!--<td align="right"><?php echo $nilai_ubah4[$i]; ?></td>-->
                                            <td align="center"><strong><?php if ($nilai_ubah4[$i] != 0) echo '<font color="red">!</font>' ?></strong></td>
                                            <td ><?php echo $rs->getString('rekening_code') . ' ' . $rs->getString('rekening_name'); ?></td>
                                            <td align="right"><?php echo $nilai_locked4[$i]; ?></td>

                                        </tr>
    <?php endif; ?>
    <?php // var_dump($nilai_draft4);echo '<br>';  ?>
    <?php $i++;
}while ($rs->next());
?>
                                <tr>
                                    <td ><strong>Total</strong></td>
                                    <td align="right"><strong><?php echo $nilai_draft2; ?></strong></td>
                                    <td ><strong>&nbsp;</strong></td>
                                    <td ><strong>Total</strong></td>
                                    <td align="right"><strong><?php echo $nilai_locked2; ?></strong></td>
                                </tr>
                            </table>

                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table> 
