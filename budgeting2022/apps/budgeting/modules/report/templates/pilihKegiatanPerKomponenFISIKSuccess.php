<?php use_helper('Object','Javascript'); ?>

<?php 
 
    if($tahap=='revisi1')
    {
        echo select_tag('kegiatan', objects_for_select($kegiatan,'getKodeKegiatan', 'getKodeNamaKegiatan',
                        '',array('include_custom'=>'Pilih Kegiatan')),
                         Array('id'=>'combo_pilih','onChange'=>remote_function(Array('update'=>'isi', 'url'=>'report/reportSemulaMenjadiMurniRevisi1PerKomponenFISIK?unit_id='.$unit_idDinas.'&tahap='.$tahap,
                        'with'=>"'b=rsmmr1pkFISIK&kegCode='+this.options[this.selectedIndex].value",
                        'loading'=>"Element.show('indicator');Element.hide('isi');Element.hide('isi_dalam');",
                        'complete'=>"Element.hide('indicator');Element.show('isi');".visual_effect('highlight', 'isi')))));
    }else if($tahap=='revisi2')
    {
        echo select_tag('kegiatan', objects_for_select($kegiatan,'getKodeKegiatan', 'getKodeNamaKegiatan',
                        '',array('include_custom'=>'Pilih Kegiatan')),
                         Array('id'=>'combo_pilih','onChange'=>remote_function(Array('update'=>'isi', 'url'=>'report/reportSemulaMenjadiRevisi1Revisi2PerKomponenFISIK?unit_id='.$unit_idDinas.'&tahap='.$tahap,
                        'with'=>"'b=rsmr1r2pkFISIK&kegCode='+this.options[this.selectedIndex].value",
                        'loading'=>"Element.show('indicator');Element.hide('isi');Element.hide('isi_dalam');",
                        'complete'=>"Element.hide('indicator');Element.show('isi');".visual_effect('highlight', 'isi')))));
    }else if($tahap=='revisi3')
    {
        echo select_tag('kegiatan', objects_for_select($kegiatan,'getKodeKegiatan', 'getKodeNamaKegiatan',
                        '',array('include_custom'=>'Pilih Kegiatan')),
                         Array('id'=>'combo_pilih','onChange'=>remote_function(Array('update'=>'isi', 'url'=>'report/reportSemulaMenjadiRevisi2Revisi3?unit_id='.$unit_idDinas.'&tahap='.$tahap,
                        'with'=>"'b=rsmmr1&kegCode='+this.options[this.selectedIndex].value",
                        'loading'=>"Element.show('indicator');Element.hide('isi');Element.hide('isi_dalam');",
                        'complete'=>"Element.hide('indicator');Element.show('isi');".visual_effect('highlight', 'isi')))));
    }else if($tahap=='revisi4')
    {
        echo select_tag('kegiatan', objects_for_select($kegiatan,'getKodeKegiatan', 'getKodeNamaKegiatan',
                        '',array('include_custom'=>'Pilih Kegiatan')),
                         Array('id'=>'combo_pilih','onChange'=>remote_function(Array('update'=>'isi', 'url'=>'report/reportSemulaMenjadiRevisi3Revisi4?unit_id='.$unit_idDinas.'&tahap='.$tahap,
                        'with'=>"'b=rsmmr1&kegCode='+this.options[this.selectedIndex].value",
                        'loading'=>"Element.show('indicator');Element.hide('isi');Element.hide('isi_dalam');",
                        'complete'=>"Element.hide('indicator');Element.show('isi');".visual_effect('highlight', 'isi')))));
    }else if($tahap=='revisi5')
    {
        echo select_tag('kegiatan', objects_for_select($kegiatan,'getKodeKegiatan', 'getKodeNamaKegiatan',
                        '',array('include_custom'=>'Pilih Kegiatan')),
                         Array('id'=>'combo_pilih','onChange'=>remote_function(Array('update'=>'isi', 'url'=>'report/reportSemulaMenjadiRevisi4Revisi5?unit_id='.$unit_idDinas.'&tahap='.$tahap,
                        'with'=>"'b=rsmmr1&kegCode='+this.options[this.selectedIndex].value",
                        'loading'=>"Element.show('indicator');Element.hide('isi');Element.hide('isi_dalam');",
                        'complete'=>"Element.hide('indicator');Element.show('isi');".visual_effect('highlight', 'isi')))));
    }
?>