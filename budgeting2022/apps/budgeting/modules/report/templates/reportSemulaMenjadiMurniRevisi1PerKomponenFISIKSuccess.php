<?php use_helper('Form', 'Object', 'Javascript') ?>


<div id="isiDinas">
    <?php
    echo select_tag('criteria', objects_for_select($unit_kerja, 'getUnitId', 'getUnitName', $unit_idDinas, array('include_custom' => 'Pilih SKPD')), Array('id' => 'combo_pilih', 'onChange' => remote_function(Array('update' => 'isiKeg', 'url' => 'report/pilihKegiatanPerKomponenFISIK?tahap=revisi1',
            'with' => "'b=rsmmr1pkFISIK&where='+this.options[this.selectedIndex].value",
            'loading' => "Element.show('indicator');Element.hide('isi');Element.hide('isi_dalam');",
            'complete' => "Element.hide('indicator');Element.show('isi');" . visual_effect('highlight', 'isi')))));
    ?>
</div>

<div id="isiKeg">
    <?php
    echo select_tag('kegiatan', objects_for_select($kegiatan, 'getKodeKegiatan', 'getKodeNamaKegiatan', '', array('include_custom' => 'Pilih Kegiatan')), Array('id' => 'combo_pilih', 'onChange' => remote_function(Array('update' => 'isi', 'url' => 'report/reportSemulaMenjadiMurniRevisi1PerKomponenFISIK?unit_id=' . $unit_idDinas . '&tahap=' . $tahap,
            'with' => "'b=rsmmr1pkFISIK&kegCode='+this.options[this.selectedIndex].value",
            'loading' => "Element.show('indicator');Element.hide('isi');Element.hide('isi_dalam');",
            'complete' => "Element.hide('indicator');Element.show('isi');" . visual_effect('highlight', 'isi')))));
    ?></div>
<br/><br/><br/>
<div id="isi"></div>

<?php if (isset($kode_kegiatan)): ?>
    <h1 width='760'>Perubahan Detail Rincian Murni - Revisi 1 FISIK</h1>
    <table cellspacing="0" class="sf_admin_list">        
    <tr id="headerrka_<?php echo $sub_id ?>">
        <td style="padding: 2px 2px 2px 2px"><b>
                <?php
                    echo link_to('<b>File Excel</b>', 'report/ExcelMurniRevisi1FISIK?kode_kegiatan=' . $kode_kegiatan . '&unit_id=' . $unit_id);
                    ?>
            </b>
        </td>
    </tr>
    </table>
    <table style="empty-cells: show;" border="0" cellpadding="3" cellspacing="0" class="sf_admin_list" width="760">

        <tr class="sf_admin_row_0">
            <td align='right' width='20%'>Unit Kerja</td>
            <td width='1%' align='center'>:</td>
            <td align='left'><?php echo '(' . $unit_id . ')' . '&nbsp;' . $unit_kerja ?></td>
        </tr>
        <tr class="sf_admin_row_0">
            <td align='right' width='20%'>Kode Kegiatan</td>
            <td width='1%' align='center'>:</td>
            <td><?php echo $kode_kegiatan2 ?></td>
        </tr>
        <tr class="sf_admin_row_0">
            <td align='right' width='20%'>Nama Kegiatan</td>
            <td width='1%' align='center'>:</td>
            <td><?php echo $nama_kegiatan ?></td>
        </tr>
        <tr>
            <td align='right' width='20%' valign='top'>Nilai</td>
            <td width='1%' align='center' valign='top'>:</td>
            <td valign='top'>
                <table border="1" width="100%">
                    <tr align='center'>
                        <td>&nbsp;</td>
                        <td><strong>Murni</strong></td>
                        <td><strong>Revisi 1</strong></td>
                    </tr>
                    <?php
                    $query = "select nilai_murni.belanja_name as belanja_semula, nilai_sekarang.belanja_name as belanja_menjadi, sum(nilai_sekarang.hasil_kali2) as menjadi, sum(nilai_murni.hasil_kali1) as semula
from
	( select k.belanja_name, sum(detail2.nilai_anggaran) as hasil_kali2
	from " . sfConfig::get('app_default_schema') . ".revisi1_rincian_detail detail2," . sfConfig::get('app_default_schema') . ".rekening r, " . sfConfig::get('app_default_schema') . ".kelompok_belanja k
        where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and r.rekening_code=detail2.rekening_code and k.belanja_id=r.belanja_id  and detail2.status_hapus=false and detail2.tipe = 'FISIK'
        group by k.belanja_name ) as nilai_sekarang left outer join
        ( select k.belanja_name, sum(detail1.nilai_anggaran) as hasil_kali1
	from " . sfConfig::get('app_default_schema') . ".murni_rincian_detail detail1," . sfConfig::get('app_default_schema') . ".rekening r, " . sfConfig::get('app_default_schema') . ".kelompok_belanja k
	where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and r.rekening_code=detail1.rekening_code and k.belanja_id=r.belanja_id  and detail1.status_hapus=false and detail1.tipe = 'FISIK'
        group by k.belanja_name ) as nilai_murni
 on nilai_sekarang.belanja_name= nilai_murni.belanja_name

group by nilai_murni.belanja_name, nilai_sekarang.belanja_name";
////awal-comment
//print_r($query);exit;
                    $con = Propel::getConnection();
                    $stmt = $con->prepareStatement($query);
                    $rs_rekening = $stmt->executeQuery();
                    $rekening_prev = 0;
                    $rekening_now = 0;
                    while ($rs_rekening->next()) {
                        if (!$rs_rekening->getString('belanja_semula')) {
                            $nama_belanja = $rs_rekening->getString('belanja_menjadi');
                        } else {
                            $nama_belanja = $rs_rekening->getString('belanja_semula');
                        }
                        ?>
                        <tr>
                            <td><?php echo $nama_belanja; ?></td>
                            <td align='right'><?php
                                echo number_format($rs_rekening->getString('semula'), 0, ',', '.');
                                $rekening_prev+=$rs_rekening->getString('semula');
                                ?></td>
                            <td align='right'><?php
                                echo number_format($rs_rekening->getString('menjadi'), 0, ',', '.');
                                $rekening_now+=$rs_rekening->getString('menjadi');
                                ?></td>
                        </tr>
                        <?php
                    }
                    ?>
                    <tr>
                        <td>Total</td>
                        <td align='right'><?php echo number_format($rekening_prev, 0, ',', '.'); ?></td>
                        <td align='right'><?php echo number_format($rekening_now, 0, ',', '.'); ?></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan='3'>
                <table width='100%'>
                    <tr>
                        <td valign='top'>
                            <table width='100%'>
                                <tr class="sf_admin_row_1" align='center'>
                                <thead>
                                <th colspan='7'><strong>MURNI</strong></th>
                                <th><strong>|</strong></th>
                                <th colspan='7'><strong>REVISI 1</strong></th>
                                </thead>
                                <thead>
                                <th>Komponen</th>
                                <th>Satuan</th>
                                <th>Koefisien</th>
                                <th>Harga</th>
                                <th>Hasil</th>
                                <th>PPN</th>
                                <th>Total</th>
                                <th align='center'>|</th>
                                <th>Komponen</th>
                                <th>Satuan</th>
                                <th>Koefisien</th>
                                <th>Harga</th>
                                <th>Hasil</th>
                                <th>PPN</th>
                                <th>Total</th>
                                </thead>
                    </tr>
                    <tbody>
                        <?php
                        $jumlah_harga_prev = 0;
                        $jumlah_harga = 0;
                        $query = "
(select detail2.subtitle as subtitle2, detail.subtitle as subtitle1
from " . sfConfig::get('app_default_schema') . ".revisi1_subtitle_indikator detail2, " . sfConfig::get('app_default_schema') . ".murni_subtitle_indikator detail
where
detail2.unit_id='$unit_id' and detail2.kegiatan_code='$kode_kegiatan' and detail2.kegiatan_code=detail.kegiatan_code and detail2.unit_id=detail.unit_id
and detail2.subtitle=detail.subtitle)

union

(select '', detail.subtitle as subtitle1
from  " . sfConfig::get('app_default_schema') . ".revisi1_subtitle_indikator detail
where
detail.unit_id='$unit_id' and detail.kegiatan_code='$kode_kegiatan' and detail.subtitle not in (select subtitle from " . sfConfig::get('app_default_schema') . ".revisi1_subtitle_indikator detail2
where detail2.unit_id='$unit_id' and detail2.kegiatan_code='$kode_kegiatan'))

union

(select detail2.subtitle as subtitle2, '' as subtitle1
from  " . sfConfig::get('app_default_schema') . ".revisi1_subtitle_indikator detail2
where
detail2.unit_id='$unit_id' and detail2.kegiatan_code='$kode_kegiatan' and detail2.subtitle not in (select subtitle from " . sfConfig::get('app_default_schema') . ".murni_subtitle_indikator detail
where detail.unit_id='$unit_id' and detail.kegiatan_code='$kode_kegiatan'))
order by subtitle2";
//print_r($query);exit;
                        $con = Propel::getConnection();
                        $stmt = $con->prepareStatement($query);
                        $rs = $stmt->executeQuery();
                        while ($rs->next()) {
                            $subtitle_prev = '';
                            $subtitle = '';
                            $subtitle_prev = $rs->getString('subtitle1');
                            $subtitle = $rs->getString('subtitle2');

                            $j = 0;
                            $kosong = 'kanan';
                            if ($subtitle_prev == $subtitle) {
                                $kosong = 'tidakada';

                                $query2 = " (
select distinct detail2.rekening_code as rekening, detail.rekening_code as rekening_prev,r.rekening_name
from " . sfConfig::get('app_default_schema') . ".revisi1_rincian_detail detail2 right outer join " . sfConfig::get('app_default_schema') . ".murni_rincian_detail detail
on detail2.unit_id=detail.unit_id and detail2.kegiatan_code=detail.kegiatan_code and detail2.subtitle=detail.subtitle
and detail2.rekening_code = detail.rekening_code, " . sfConfig::get('app_default_schema') . ".rekening r
where detail.unit_id='$unit_id' and detail.kegiatan_code='$kode_kegiatan' and detail.subtitle ilike '$subtitle' and detail.rekening_code=r.rekening_code and detail.tipe = 'FISIK' and detail2.tipe = 'FISIK'
order by detail2.rekening_code,detail.rekening_code
)
union
(
select distinct detail2.rekening_code as rekening, detail2.rekening_code as rekening_prev,r.rekening_name
from " . sfConfig::get('app_default_schema') . ".revisi1_rincian_detail detail2 , " . sfConfig::get('app_default_schema') . ".rekening r
where  detail2.unit_id='$unit_id' and detail2.kegiatan_code='$kode_kegiatan' and detail2.subtitle ilike '$subtitle' and detail2.rekening_code=r.rekening_code and detail2.status_hapus=false and detail2.tipe = 'FISIK' and detail2.detail_no not in
(
 select detail_no
from " . sfConfig::get('app_default_schema') . ".murni_rincian_detail detail
where detail.unit_id='$unit_id' and detail.kegiatan_code='$kode_kegiatan' and detail.subtitle ilike '$subtitle'  and detail.status_hapus=false and detail.tipe = 'FISIK'
)
order by detail2.rekening_code
)";

//print_r($query2);exit;


                                if ($subtitle == 'Pembangunan Jalan') {
                                    //print_r($query2);exit;
                                }
                            } elseif ($rs->getString('subtitle2')) {
                                $kosong = 'kiri';
                                $query2 = "select
										distinct(detail.rekening_code), detail.subtitle, rekening.rekening_name, detail.rekening_code as rekening, '' as rekening_prev
										from
											" . sfConfig::get('app_default_schema') . ".revisi1_rincian_detail detail," . sfConfig::get('app_default_schema') . ".rekening rekening
										where
										detail.kegiatan_code = '" . $kode_kegiatan . "' and detail.unit_id = '" . $unit_id . "' and detail.subtitle ilike '" . $subtitle . "'
										and detail.rekening_code=rekening.rekening_code and detail.tipe = 'FISIK'

										";
                                //print_r($query2);exit;
                            } else {
                                $kosong = 'kanan';
                                //$subtitle = $subtitle_prev;
                                $query2 = "select
										distinct(detail.rekening_code), detail.subtitle, rekening.rekening_name, '' as rekening, detail.rekening_code as rekening_prev
										from
											" . sfConfig::get('app_default_schema') . ".murni_rincian_detail detail," . sfConfig::get('app_default_schema') . ".rekening rekening
										where
										detail.kegiatan_code = '" . $kode_kegiatan . "' and detail.unit_id = '" . $unit_id . "' and detail.subtitle ilike '" . $subtitle_prev . "'
										and detail.rekening_code=rekening.rekening_code and detail.status_hapus=false  and detail.tipe = 'FISIK'

										";
                                //print_r($query2);exit;
                            }

                            $stmt2 = $con->prepareStatement($query2);
                            $rs2 = $stmt2->executeQuery();
                            ?>
                            <tr align="left" bgcolor="#ffffff"><td colspan="7"><span class="style3"><strong>
                                            <?php echo ':: ' . $subtitle_prev ?>
                                        </strong></span></td>
                                <td>&nbsp;</td>
                                <td colspan="7"><span class="style3"><strong>
                                            <?php echo ':: ' . $subtitle ?>
                                        </strong></span></td></tr>
                            <?php
                            $total_semua = 0;
                            $total_semua_n = 0;
                            while ($rs2->next()) {
                                if ($rs2->getString('rekening_prev') != '') {
                                    $rekening_prev = $rs2->getString('rekening_prev') . '&nbsp;' . $rs2->getString('rekening_name');
                                }
                                if ($rs2->getString('rekening_prev') != $rs2->getString('rekening')) {
                                    $rekening_prev = $rs2->getString('rekening_prev');
                                }
                                $rekening = $rs2->getString('rekening') . '&nbsp;' . $rs2->getString('rekening_name');

                                if ($kosong == 'tidakada') {
                                    $rekening_prev = $rs2->getString('rekening_prev') . '&nbsp;' . $rs2->getString('rekening_name');
                                    $rekening = $rs2->getString('rekening') . '&nbsp;' . $rs2->getString('rekening_name');
                                    if ($rs2->getString('rekening')) {
                                        $kode_rekening = $rs2->getString('rekening');
                                    } else {
                                        $kode_rekening = $rs2->getString('rekening_prev');
                                    }
                                    $query3 = "
												(
SELECT

	rekening.rekening_code,rekening.rekening_code as rekening_code2,
	rekening.rekening_name as nama_rekening2,
	detail2.komponen_name || ' ' || detail2.detail_name as detail_name22,
	detail2.komponen_harga_awal as detail_harga2,
	detail2.pajak as pajak2,
	detail2.komponen_id as komponen_id2,
	detail2.detail_name as detail_name2,
	detail2.komponen_name as komponen_name2,
	detail2.volume as volume2,
	detail2.subtitle as subtitle_name2,
	detail2.satuan as detail_satuan2,
	replace(detail2.keterangan_koefisien,'<*>','X') as keterangan_koefisien2,
	detail2.detail_no as detail_no2,
	detail2.volume * detail2.komponen_harga_awal as hasil2,
	detail2.nilai_anggaran as hasil_kali2,

	detail.komponen_name || ' ' || detail.detail_name as detail_name2,
	detail.komponen_harga_awal as detail_harga,
	detail.pajak as pajak,
	detail.komponen_id as komponen_id,
	detail.detail_name as detail_name,
	detail.komponen_name as komponen_name,
	detail.volume as volume,
	detail.subtitle as subtitle_name,
	detail.satuan as detail_satuan,
	replace(detail.keterangan_koefisien,'<*>','X') as keterangan_koefisien,
	detail.detail_no as detail_no,
	detail.volume * detail.komponen_harga_awal as hasil,
	detail.nilai_anggaran as hasil_kali,
	detail2.detail_name as detail_name_rd,
	detail2.sub as sub2,
	detail.sub,
        detail2.tipe as tipe2,
        detail.tipe as tipe


FROM
	" . sfConfig::get('app_default_schema') . ".revisi1_rincian_detail detail2, " . sfConfig::get('app_default_schema') . ".rekening rekening, " . sfConfig::get('app_default_schema') . ".murni_rincian_detail detail

WHERE
	detail2.kegiatan_code = '$kode_kegiatan' and
	detail2.unit_id='$unit_id'

	and detail2.rekening_code='$kode_rekening'
	and detail2.subtitle ilike '$subtitle'
	and detail2.kegiatan_code=detail.kegiatan_code
	and detail2.unit_id=detail.unit_id
	and detail2.subtitle=detail.subtitle
	and detail2.rekening_code=detail.rekening_code
	and rekening.rekening_code=detail2.rekening_code
	and detail2.detail_no=detail.detail_no
        and detail2.status_hapus<>true
        and detail2.tipe = 'FISIK'
        and detail.tipe = 'FISIK'
        

ORDER BY

	rekening.rekening_code,
	detail2.subtitle ,
	detail2.komponen_name
)
union
(
SELECT

	rekening.rekening_code,rekening.rekening_code as rekening_code2,
	rekening.rekening_name as nama_rekening2,
	detail2.komponen_name || ' ' || detail2.detail_name as detail_name22,
	detail2.komponen_harga_awal as detail_harga2,
	detail2.pajak as pajak2,
	detail2.komponen_id as komponen_id2,
	detail2.detail_name as detail_name2,
	detail2.komponen_name as komponen_name2,
	detail2.volume as volume2,
	detail2.subtitle as subtitle_name2,
	detail2.satuan as detail_satuan2,
	replace(detail2.keterangan_koefisien,'<*>','X') as keterangan_koefisien2,
	detail2.detail_no as detail_no2,
	detail2.volume * detail2.komponen_harga_awal as hasil2,
	detail2.nilai_anggaran as hasil_kali2,

	'' as detail_name,
	0 as detail_harga,
	0 as pajak,
	'' as komponen_id,
	'' as detail_name,
	'' as komponen_name,
	0 as volume,
	'' as subtitle_name,
	'' as detail_satuan,
	'' as keterangan_koefisien,
	0 as detail_no,
	0 as hasil,
	0 as hasil_kali,
	detail2.detail_name as detail_name_rd,
	detail2.sub as sub2,
	'' as sub,        
        detail2.tipe as tipe2,
        '' as tipe


FROM
	" . sfConfig::get('app_default_schema') . ".revisi1_rincian_detail detail2, " . sfConfig::get('app_default_schema') . ".rekening rekening

WHERE
	detail2.kegiatan_code = '$kode_kegiatan' and
	detail2.unit_id='$unit_id'        
        and detail2.tipe = 'FISIK'   
        and detail2.status_hapus=false
	and detail2.rekening_code='$kode_rekening'
	and detail2.subtitle ilike '$subtitle'
	and rekening.rekening_code=detail2.rekening_code
	and (detail2.unit_id||detail2.kegiatan_code||detail2.detail_no) not in
	(
	select (detail.unit_id||detail.kegiatan_code||detail.detail_no) from " . sfConfig::get('app_default_schema') . ".murni_rincian_detail detail where detail.kegiatan_code = '$kode_kegiatan' and
	detail.unit_id='$unit_id' and detail.subtitle ilike '$subtitle'
	and detail.rekening_code='$kode_rekening' and detail.tipe = 'FISIK'
	)

ORDER BY

	rekening.rekening_code,
	detail2.subtitle ,
	detail2.komponen_name)
union
(
SELECT

	rekening.rekening_code,rekening.rekening_code as rekening_code2,
	rekening.rekening_name as nama_rekening2,
	'' as detail_name22,
	0 as detail_harga2,
	0 as pajak2,
	'' as komponen_id2,
	'' as detail_name2,
	'' as komponen_name2,
	0 as volume2,
	'' as subtitle_name2,
	'' as detail_satuan2,
	'' as keterangan_koefisien2,
	0 as detail_no2,
	0 as hasil2,
	0 as hasil_kali2,

	detail.komponen_name || ' ' || detail.detail_name as detail_name2,
	detail.komponen_harga_awal as detail_harga,
	detail.pajak as pajak,
	detail.komponen_id as komponen_id,
	detail.detail_name as detail_name,
	detail.komponen_name as komponen_name,
	detail.volume as volume,
	detail.subtitle as subtitle_name,
	detail.satuan as detail_satuan,
	replace(detail.keterangan_koefisien,'<*>','X') as keterangan_koefisien,
	detail.detail_no as detail_no,
	detail.volume * detail.komponen_harga_awal as hasil,
	detail.nilai_anggaran as hasil_kali,
	'' as detail_name_rd,
	'' as sub2,
	detail.sub,        
        '' as tipe2,
        detail.tipe as tipe


FROM
	" . sfConfig::get('app_default_schema') . ".murni_rincian_detail detail, " . sfConfig::get('app_default_schema') . ".rekening rekening

WHERE
	detail.kegiatan_code = '$kode_kegiatan' and
	detail.unit_id='$unit_id'
        and detail.tipe = 'FISIK'    
        and detail.status_hapus=false
	and detail.rekening_code='$kode_rekening'
	and detail.subtitle ilike '$subtitle'
	and rekening.rekening_code=detail.rekening_code
	and (detail.unit_id||detail.kegiatan_code||detail.detail_no) not in
	(
	select (detail2.unit_id||detail2.kegiatan_code||detail2.detail_no) from " . sfConfig::get('app_default_schema') . ".revisi1_rincian_detail detail2 where detail2.kegiatan_code = '$kode_kegiatan' and
	detail2.unit_id='$unit_id' and detail2.subtitle ilike '$subtitle' and detail2.status_hapus<>true
	and detail2.rekening_code='$kode_rekening' and detail2.tipe = 'FISIK'
	)

ORDER BY

	rekening.rekening_code,
	detail.subtitle ,
	detail.komponen_name)";
//print_r($query3);exit;
                                    //if($kode_rekening=='5.2.3.15.01') {print_r($query3);exit;}
                                } elseif ($kosong == 'kiri') {
                                    $rekening_prev = '';
                                    $rekening = $rs2->getString('rekening') . '&nbsp;' . $rs2->getString('rekening_name');
                                    $kode_rekening = $rs2->getString('rekening');

                                    $query3 = "SELECT

											rekening.rekening_code,rekening.rekening_code as rekening_code2,
											rekening.rekening_name as nama_rekening2,
											detail2.komponen_name || ' ' || detail2.detail_name as detail_name22,
											detail2.komponen_harga_awal as detail_harga2,
											detail2.pajak as pajak2,
											detail2.komponen_id as komponen_id2,
											detail2.detail_name as detail_name2,
											detail2.komponen_name as komponen_name2,
											detail2.volume as volume2,
											detail2.subtitle as subtitle_name2,
											detail2.satuan as detail_satuan2,
											replace(detail2.keterangan_koefisien,'<*>','X') as keterangan_koefisien2,
											detail2.detail_no as detail_no2,
											detail2.volume * detail2.komponen_harga_awal as hasil2,
											detail2.nilai_anggaran as hasil_kali2,
											detail2.detail_name as detail_name_rd,
											'' as detail_name2,
											0 as detail_harga,
											0 as pajak,
											'' as komponen_id,
											'' as detail_name,
											'' as komponen_name,
											0 as volume,
											detail.subtitle as subtitle_name,
											'' as detail_satuan,
											'' as keterangan_koefisien,
											detail.detail_no as detail_no,
											0 as hasil,
											0 as hasil_kali,                                                                                        
                                                                                        detail2.tipe as tipe2,
                                                                                        '' as tipe


										FROM
											(" . sfConfig::get('app_default_schema') . ".revisi1_rincian_detail detail2 inner join
											" . sfConfig::get('app_default_schema') . ".rekening rekening on rekening.rekening_code = detail2.rekening_code) right outer join
											" . sfConfig::get('app_default_schema') . ".murni_rincian_detail detail on detail2.unit_id=detail.unit_id and detail2.kegiatan_code=detail.kegiatan_code and detail2.detail_no=detail.detail_no
										WHERE
											detail2.kegiatan_code = '" . $kode_kegiatan . "' and
											detail2.unit_id='" . $unit_id . "'

											and detail2.rekening_code='" . $kode_rekening . "'
											and detail2.subtitle ilike '$subtitle'

                                                                                        and detail2.status_hapus=false
                                                                                        and detail.status_hapus=false
                                                                                        
                                                                                        and detail2.tipe = 'FISIK'
                                                                                        and detail.tipe = 'FISIK'

										ORDER BY

											rekening.rekening_code,
											detail2.subtitle ,
											detail2.komponen_name";
                                } elseif ($kosong == 'kanan') {
                                    $rekening_prev = $rs2->getString('rekening') . '&nbsp;' . $rs2->getString('rekening_name');
                                    $rekening = '';
                                    $kode_rekening = $rs2->getString('rekening_prev');
                                    $query3 = "SELECT

											rekening.rekening_code,rekening.rekening_code as rekening_code2,
											rekening.rekening_name as nama_rekening2,
											'' as detail_name22,
											0 as detail_harga2,
											0 as pajak2,
											'' as komponen_id2,
											'' as detail_name2,
											'' as komponen_name2,
											0 as volume2,
											'' as subtitle_name2,
											'' as detail_satuan2,
											'' as keterangan_koefisien2,
											0 as detail_no2,
											0 as hasil2,
											0 as hasil_kali2,


											detail.komponen_name || ' ' || detail.detail_name as detail_name2,
											detail.komponen_harga_awal as detail_harga,
											detail.pajak as pajak,
											detail.komponen_id as komponen_id,
											detail.detail_name as detail_name,
											detail.komponen_name as komponen_name,
											detail.volume as volume,
											detail.subtitle as subtitle_name,
											detail.satuan as detail_satuan,
											replace(detail.keterangan_koefisien,'<*>','X') as keterangan_koefisien,
											detail.detail_no as detail_no,
											detail.volume * detail.komponen_harga_awal as hasil,
											detail.nilai_anggaran as hasil_kali,
											detail2.detail_name as detail_name_rd,                                                                                                                                                                            
                                                                                        '' as tipe2,
                                                                                        detail.tipe as tipe


										FROM
											(" . sfConfig::get('app_default_schema') . ".revisi1_rincian_detail detail2 inner join
											" . sfConfig::get('app_default_schema') . ".rekening rekening on rekening.rekening_code = detail2.rekening_code) left outer join
											" . sfConfig::get('app_default_schema') . ".murni_rincian_detail detail on detail2.unit_id=detail.unit_id and detail2.kegiatan_code=detail.kegiatan_code and detail2.detail_no=detail.detail_no
										WHERE
											detail.kegiatan_code = '" . $kode_kegiatan . "' and
											detail.unit_id='" . $unit_id . "'

											and detail.rekening_code='" . $kode_rekening . "'
											and detail.subtitle ilike '$subtitle_prev'

                                                                                        and detail2.status_hapus=false
                                                                                        and detail.status_hapus=false
                                                                                        
                                                                                        and detail2.tipe = 'FISIK'
                                                                                        and detail.tipe = 'FISIK'

										ORDER BY

											rekening.rekening_code,
											detail2.subtitle ,
											detail2.komponen_name";

                                    $subtitle = $subtitle_prev;
                                }

                                if ($rs2->getString('rekening_prev')) {

                                    echo '<tr bgcolor="#ffffff"><td colspan="7" class="Font8v style3" align="left"><span class="style3"><strong>' . $rekening_prev . '</strong></span></td>';
                                    //echo '<td class="Font8v style3" align="right"><span class="style3"><strong>.$hasil_kali.</strong></span></td>';
                                    echo '<td>&nbsp;</td>';
                                    echo '<td colspan="7" class="Font8v style3" align="left"><span class="style3"><strong>' . $rekening . '</strong></span></td></tr>';

                                    $rekening_code = $rs2->getString('rekening');
                                }


                                //print_r($query3);exit;
                                $stmt3 = $con->prepareStatement($query3);
                                $rs3 = $stmt3->executeQuery();
                                $total = 0;
                                $total_n = 0;

                                while ($rs3->next()) {
                                    //prev_rincian_detail
                                    if ($rs2->getString('rekening_prev')) {
                                        $hasil = 0;
                                        $detail_harga = 0;
                                        $volume = 0;
                                        $pajak = 0;
                                        $detail_harga = $rs3->getString('detail_harga');
                                        $volume = $rs3->getString('volume');
                                        $pajak = $rs3->getString('pajak');
                                        $subnya = '';
                                        if ($rs3->getString('sub') and $rs3->getString('komponen_name'))
                                            $subnya = '[' . $rs3->getString('sub') . '] ';

                                        //tebal
                                        if ((($rs3->getString('komponen_name') != $rs3->getString('komponen_name2')) ||
                                                ($rs3->getString('detail_name22') != $rs3->getString('detail_name2')) ||
                                                ($rs3->getString('detail_satuan') != $rs3->getString('detail_satuan2')) ||
                                                ($rs3->getString('keterangan_koefisien') != $rs3->getString('keterangan_koefisien2')) ||
                                                ($rs3->getString('pajak') != $rs3->getString('pajak2')) ||
                                                ($rs3->getString('detail_harga') != $rs3->getString('detail_harga2')) ||
                                                ($rs3->getString('volume') != $rs3->getString('volume2')))) {
                                            //print_r($query3);exit;
                                            if ($sf_user->getNamaUser() != 'parlemen2') {
                                                echo '<tr bgcolor="#ffffff" valign="top">';
                                                echo '<td class="Font8v" align="left"><strong><span class="style3">' . $subnya . $rs3->getString('komponen_name') . ' ' . $rs3->getString('detail_name') . '</span></strong></td>';
                                                echo '<td class="Font8v style3" align="center" nowrap="nowrap"><strong><span class="Font8v">' . $rs3->getString('detail_satuan') . '</span></strong></td>';
                                                echo '<td class="Font8v style3" align="center"><strong><span class="Font8v">' . $rs3->getString('volume') . ' ' . $rs3->getString('detail_satuan') . '</span></strong></td>';
                                                echo '<td class="Font8v style3" align="right" nowrap="nowrap"><strong>' . number_format($rs3->getString('detail_harga'), 0, ',', '.') . '</strong></td>';
                                                $hasil = $detail_harga * $volume;
                                                echo '<td class="Font8v style3" align="right" nowrap="nowrap"><strong>' . number_format($hasil, 0, ',', '.') . '</strong></td>';
                                                echo '<td class="Font8v style3" align="right" nowrap="nowrap"><strong>' . $rs3->getString('pajak') . '%</strong></td>';
                                                $total1 = $hasil - (($pajak / 100) * $hasil);
                                                echo '<td class="Font8v style3" align="right" nowrap="nowrap"><strong>' . number_format($rs3->getString('hasil_kali'), 0, ',', '.') . '</strong></td>';
                                                $total = $total + $total1;
                                                $jumlah_harga_prev += $hasil;
                                            }
                                        } else {
                                            if ($sf_user->getNamaUser() != 'parlemen2') {
                                                echo '<tr bgcolor="#ffffff" valign="top">';
                                                echo '<td class="Font8v" align="left"><span class="style3">' . $subnya . $rs3->getString('komponen_name') . ' ' . $rs3->getString('detail_name') . '</span></td>';
                                                echo '<td class="Font8v style3" align="center" nowrap="nowrap"><span class="Font8v">' . $rs3->getString('detail_satuan') . '</span></td>';
                                                echo '<td class="Font8v style3" align="center"><span class="Font8v">' . $rs3->getString('volume') . ' ' . $rs3->getString('detail_satuan') . '</span></td>';
                                                echo '<td class="Font8v style3" align="right" nowrap="nowrap">' . number_format($rs3->getString('detail_harga'), 0, ',', '.') . '</td>';
                                                $hasil = $detail_harga * $volume;
                                                echo '<td class="Font8v style3" align="right" nowrap="nowrap">' . number_format($hasil, 0, ',', '.') . '</td>';
                                                echo '<td class="Font8v style3" align="right" nowrap="nowrap">' . $rs3->getString('pajak') . '%</td>';
                                                $total1 = $hasil - (($pajak / 100) * $hasil);
                                                echo '<td class="Font8v style3" align="right" nowrap="nowrap">' . number_format($rs3->getString('hasil_kali'), 0, ',', '.') . '</td>';
                                                $total = $total + $total1;
                                                $jumlah_harga_prev += $hasil;
                                            }
                                        }

                                        //tanda perbandingan
                                        if ((($rs3->getString('komponen_name') != $rs3->getString('komponen_name2')) ||
                                                ($rs3->getString('detail_name22') != $rs3->getString('detail_name2')) ||
                                                ($rs3->getString('detail_satuan') != $rs3->getString('detail_satuan2')) ||
                                                ($rs3->getString('keterangan_koefisien') != $rs3->getString('keterangan_koefisien2')) ||
                                                ($rs3->getString('pajak') != $rs3->getString('pajak2')) ||
                                                ($rs3->getString('detail_harga') != $rs3->getString('detail_harga2')) ||
                                                ($rs3->getString('volume') != $rs3->getString('volume2')))) {
                                            if ($sf_user->getNamaUser() != 'parlemen2') {
                                                echo '<td><strong><span style="color:red;">!</span></strong></td>';
                                            }
                                        } else {
                                            if ($sf_user->getNamaUser() != 'parlemen2') {
                                                echo '<td>&nbsp;</td>';
                                            }
                                        }

                                        $subnya2 = '';
                                        $subnya = '';
                                        if ($rs3->getString('sub2') and $rs3->getString('komponen_name2'))
                                            $subnya2 = '[' . $rs3->getString('sub2') . '] ';
                                        //rincian_detail
                                        //tebal
                                        if ((($rs3->getString('komponen_name') != $rs3->getString('komponen_name2')) ||
                                                ($rs3->getString('detail_name22') != $rs3->getString('detail_name2')) ||
                                                ($rs3->getString('detail_satuan') != $rs3->getString('detail_satuan2')) ||
                                                ($rs3->getString('keterangan_koefisien') != $rs3->getString('keterangan_koefisien2')) ||
                                                ($rs3->getString('pajak') != $rs3->getString('pajak2')) ||
                                                ($rs3->getString('detail_harga') != $rs3->getString('detail_harga2')) ||
                                                ($rs3->getString('volume') != $rs3->getString('volume2')))) {
                                            if ($sf_user->getNamaUser() != 'parlemen2') {
                                                echo '<td class="Font8v" align="left"><strong><span class="style3">' . $subnya2 . $rs3->getString('komponen_name2') . ' ' . $rs3->getString('detail_name_rd') . '</span></strong></td>';
                                                echo '<td class="Font8v style3" align="center" nowrap="nowrap"><strong><span class="Font8v">' . $rs3->getString('detail_satuan2') . '</span></strong></td>';
                                                echo '<td class="Font8v style3" align="center"><strong><span class="Font8v">' . $rs3->getString('volume2') . ' ' . $rs3->getString('detail_satuan2') . '</span></strong></td>';
                                                echo '<td class="Font8v style3" align="right" nowrap="nowrap"><strong>' . number_format($rs3->getString('detail_harga2'), 0, ',', '.') . '</strong></td>';
                                                $hasil = $rs3->getString('detail_harga2') * $rs3->getString('volume2');
                                                echo '<td class="Font8v style3" align="right" nowrap="nowrap"><strong>' . number_format($hasil, 0, ',', '.') . '</strong></td>';
                                                echo '<td class="Font8v style3" align="right" nowrap="nowrap"><strong>' . $rs3->getString('pajak2') . '%</strong></td>';
                                                $total1 = $hasil - (($rs3->getString('pajak2') / 100) * $hasil);
                                                echo '<td class="Font8v style3" align="right" nowrap="nowrap"><strong>' . number_format($rs3->getString('hasil_kali2'), 0, ',', '.') . '</strong></td>';
                                                $total_n += $total1;
                                                echo '</tr>';
                                                $jumlah_harga += $hasil;
                                            }
                                        } else {
                                            if ($sf_user->getNamaUser() != 'parlemen2') {
                                                echo '<td class="Font8v" align="left"><span class="style3">' . $subnya2 . $rs3->getString('komponen_name2') . ' ' . $rs3->getString('detail_name_rd') . '</span></td>';
                                                echo '<td class="Font8v style3" align="center" nowrap="nowrap"><span class="Font8v">' . $rs3->getString('detail_satuan2') . '</span></td>';
                                                echo '<td class="Font8v style3" align="center"><span class="Font8v">' . $rs3->getString('volume2') . ' ' . $rs3->getString('detail_satuan2') . '</span></td>';
                                                echo '<td class="Font8v style3" align="right" nowrap="nowrap">' . number_format($rs3->getString('detail_harga2'), 0, ',', '.') . '</td>';
                                                $hasil = $rs3->getString('detail_harga2') * $rs3->getString('volume2');
                                                echo '<td class="Font8v style3" align="right" nowrap="nowrap">' . number_format($hasil, 0, ',', '.') . '</td>';
                                                echo '<td class="Font8v style3" align="right" nowrap="nowrap">' . $rs3->getString('pajak2') . '%</td>';
                                                $total1 = $hasil - (($rs3->getString('pajak2') / 100) * $hasil);
                                                echo '<td class="Font8v style3" align="right" nowrap="nowrap">' . number_format($rs3->getString('hasil_kali2'), 0, ',', '.') . '</td>';
                                                $total_n += $total1;
                                                echo '</tr>';
                                                $jumlah_harga += $hasil;
                                            }
                                        }
                                    }
                                }
                            }
                            $total_semua = $total_semua + $total;
                            $total_semua_n += $total_n;
                            $query = "select sum(nilai_anggaran) as nilai from " . sfConfig::get('app_default_schema') . ".murni_rincian_detail rd
												where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and subtitle='$subtitle' and rd.status_hapus=false and rd.tipe = 'FISIK'";
                            $con = Propel::getConnection();
                            $stmt = $con->prepareStatement($query);
                            $rs_harga_subtitle = $stmt->executeQuery();
                            while ($rs_harga_subtitle->next()) {
                                $total_subtitle_semula = $rs_harga_subtitle->getString('nilai');
                            }

                            $query = "select sum(nilai_anggaran) as nilai from " . sfConfig::get('app_default_schema') . ".revisi1_rincian_detail rd
												where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and subtitle='$subtitle' and rd.status_hapus=false and rd.tipe = 'FISIK'";
//										print_r($query);exit;
                            $con = Propel::getConnection();
                            $stmt = $con->prepareStatement($query);
                            $rs_harga_subtitle = $stmt->executeQuery();
                            while ($rs_harga_subtitle->next()) {
                                $total_subtitle_menjadi = $rs_harga_subtitle->getString('nilai');
                            }
                            echo '<tr bgcolor="#ffffff">';
                            echo '<td colspan="6" class="Font8v style3" align="right">';
                            echo 'Total ' . $subtitle . ' :</td>';
                            echo '<td class="Font8v style3" align="right" nowrap="nowrap">' . number_format($total_subtitle_semula, 0, ',', '.') . '</td>';
                            echo '<td> </td>';
                            echo '<td colspan="6" class="Font8v style3" align="right">';
                            echo 'Total ' . $subtitle . ' :</td>';
                            echo '<td class="Font8v style3" align="right" nowrap="nowrap">' . number_format($total_subtitle_menjadi, 0, ',', '.') . '</td>';
                            echo '</tr>';
                        }
                        echo '<tr bgcolor="#ffffff">';
                        echo '<td colspan="6" class="Font8v style3" align="right">';
                        echo 'Total  :</td>';
                        echo '<td class="Font8v style3" align="right" nowrap="nowrap">' . number_format($total_semula, 0, ',', '.') . '</td>';
                        echo '<td> </td>';
                        echo '<td colspan="6" class="Font8v style3" align="right">';
                        echo 'Total  :</td>';
                        echo '<td class="Font8v style3" align="right" nowrap="nowrap">' . number_format($total_sekarang, 0, ',', '.') . '</td>';
                        echo '</tr>';
                        ?>
                    </tbody>
                </table>
            </td>
        </tr>
    </table>
    </td>
    </tr>
    </table>

<?php endif; ?>
