<div class="box box-info">        
    <div class="box-header with-border">
        Laporan Per Dinas Per Belanja
    </div>
    <div class="box-body">
        <table class="table table-bordered">
            <tr>
                <th style="width: 10px"></th>
                <th style="width: 10px"></th>
                <th class="text-center">Dinas - Belanja</th>
                <!--
                <th class="text-center">Nilai Usulan</th>
                <th class="text-center">Persentase</th>
                -->
                <th class="text-center">Nilai</th>
            </tr>
            <?php
            $i = 0;
            if ($rs->first() <> '') {
                $rs->first();
                do {
                    ?>
                    <?php if (isset($dinas_ok[$i])): ?>
                        <tr><td colspan="6">&nbsp;</td></tr>
                        <tr>
                            <td colspan="3" class="text-left text-bold"><?php echo $unit_id[$i] ?> &nbsp; <?php echo $unit_name[$i] ?></td>

            <!--<td class="text-right text-bold"><?php echo $nilai_draft[$i] ?></td>
            <td class="text-right text-bold">&nbsp;</td>
                            <?php
                            $nilai_locked_total = str_replace('.', '', $nilai_locked[$i]);
                            ?>
                            -->
                            <td class="text-right text-bold"><?php echo $nilai_locked[$i] ?></td>
                        </tr>
                    <?php endif ?>
                    <tr>
                        <th style="width: 10px"></th>
                        <th style="width: 10px"></th>
                        <td class="text-left"><?php echo isset($subtitle[$i]) ? $subtitle[$i] : '' ?></td>
                        <!--<td class="text-right"><?php echo isset($nilai_draft4[$i]) ? $nilai_draft4[$i] : '' ?></td>
                        
                        <td class="text-right">
                            <?php
                            $nilai = str_replace('.', '', $nilai_locked4[$i]);
                            echo number_format(($nilai / $nilai_locked_total) * 100, 2, ",", ".");
                            ?>
                        </td>-->
                        <td class="text-right"><?php echo isset($nilai_locked4[$i]) ? $nilai_locked4[$i] : '' ?></td>
                    </tr>
                    <?php
                    $i++;
                } while ($rs->next());
            }
            ?>
            <tr>
                <td colspan="3" class="text-center text-bold bg-green-active">Total</td>
                <!--<td class="text-right text-bold bg-green-active"><?php echo $nilai_draft2 ?></td>
                <td class="text-right text-bold bg-green-active">&nbsp;</td>-->
                <td class="text-right text-bold bg-green-active"><?php echo $nilai_locked2 ?></td>
            </tr>
        </table>       
    </div>
</div>