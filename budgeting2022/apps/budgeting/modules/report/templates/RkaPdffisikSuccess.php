<?php use_helper('Javascript', 'Number') ?>
<?php
//echo "Print RKA melalui SABK ".link_to('(http://sabk.surabaya.go.id)','http://sabk.surabaya.go.id', array('target'=>'_blank')).". Tekan tombol BACK untuk kembali";exit;
//echo use_stylesheet('/css/tampilan_print2.css');
?>

<?php
$kode_kegiatan = $sf_params->get('kode_kegiatan');
$unit_id = $sf_params->get('unit_id');
$query = "select mk.kode_kegiatan, mk.nama_kegiatan, mk.alokasi_dana, mk.kode_program,
	mk.kode_program2, mk.benefit, mk.impact, mk.kode_program2, mk.kode_misi, mk.kode_tujuan,
	mk.outcome, mk.target_outcome, mk.catatan, mk.ranking,uk.unit_name, mk.kode_urusan,
	uk.kode_permen,mk.output, mk.kelompok_sasaran
	from " . sfConfig::get('app_default_schema') . ".master_kegiatan mk,unit_kerja uk
	where mk.kode_kegiatan = '" . $kode_kegiatan . "' and mk.unit_id = '" . $unit_id . "'
	and uk.unit_id=mk.unit_id";
$con = Propel::getConnection();
$stmt = $con->prepareStatement($query);
$rs = $stmt->executeQuery();
$ranking = '';
while ($rs->next()) {
    $kegiatan_kode = $rs->getString('kode_kegiatan');
    $nama_kegiatan = $rs->getString('nama_kegiatan');
    $alokasi = $rs->getString('alokasi_dana');
    $organisasi = $rs->getString('unit_name');
    $benefit = $rs->getString('benefit');
    $impact = $rs->getString('impact');
    $kode_program = $rs->getString('kode_program');
    $program_p_13 = $rs->getString('kode_program2');
    //$urusan = $rs->getString('urusan');
    $kode_misi = $rs->getString('kode_misi');
    $kode_tujuan = '';
    if ($rs->getString('kode_tujuan')) {
        $kode_tujuan = $rs->getString('kode_tujuan');
    }

    $outcome = $rs->getString('outcome');
    $target_outcome = $rs->getString('target_outcome');
    $catatan = $rs->getString('catatan');
    $ranking = $rs->getInt('ranking');
    $kode_organisasi = $rs->getString('kode_urusan');
    $kode_program2 = $rs->getString('kode_program2');
    $kode_permen = $rs->getString('kode_permen');
    $output = $rs->getString('output');
    $kelompok_sasaran = $rs->getString('kelompok_sasaran');
    $kode_per = explode(".", $kode_organisasi);
}
?>
<table style="width: 100%; border: 1" cellspacing="0">
    <tr>
        <td style="border: 1" colspan="6">
            <div style="border: 1">
                <center><span class="style1 font12"><strong><span class="style3" style="font-size: smaller">RENCANA KERJA DAN ANGGARAN</span><br></strong></span></center>
            </div>        
            <div style="border: 1">
                <center><span class="font12 style1 style2"><span class="font12 style3 style1" style="font-size: smaller"><strong>SATUAN KERJA PERANGKAT DAERAH </strong></span></span></center>
            </div>
        </td>
        <td style="border: 1" rowspan="2" colspan="3">
            <div class="style3" style="font-size: smaller">
                <center><strong>Formulir<br>RKA-SKPD 2.2.1 </strong></center>
            </div>
        </td>
    </tr>   
    <tr>
        <td colspan="6" style="border: 1">
            <div style="font-size: smaller">
                <center><span class="style3">KOTA SURABAYA <br></span></center>
            </div>
            <div >
                <center><span class="style3" style="font-size: smaller">TAHUN ANGGARAN <?php echo sfConfig::get('app_tahun_default'); ?></span></center>
            </div>
        </td>
    </tr>
    <tr>
        <td colspan="9" style="border: 1">
            <table>
                <tr align="left" valign="top">
                    <td nowrap="nowrap" width="150">
                        <span class="style3" style="font-size: smaller">Urusan Pemerintahan </span>
                    </td>
                    <?php
                    $query = "select * "
                            . "from " . sfConfig::get('app_default_schema') . ".master_program2 ms "
                            . "where ms.kode_program2='" . $kode_program2 . "'";
                    $con = Propel::getConnection();
                    $stmt = $con->prepareStatement($query);
                    $rs1 = $stmt->executeQuery();
                    while ($rs1->next()) {
                        $nama_program2 = $rs1->getString('nama_program2');
                    }

                    $kode_urusan_parameter = $kode_per[0] . '.' . $kode_per[1];
                    
                    $queryU="select * "
                            . "from ". sfConfig::get('app_default_schema') .".master_urusan mu "
                            . "where mu.kode_urusan='".$kode_urusan_parameter."'";
                            $con = Propel::getConnection();
                            $stmt = $con->prepareStatement($queryU);
                            $rsU = $stmt->executeQuery();
                             while ($rsU->next()) {
                                $nama_urusan = $rsU->getString('nama_urusan'); 
                            }
                            
                    ?>
                    <td width="10"><span class="style3" style="font-size: smaller">:</span></td>
                    <td ><span class="style3" style="font-size: smaller"><?php echo $kode_per[0] . '.' . $kode_per[1].' '.$nama_urusan ?></span></td>
                </tr>
                <tr align="left" valign="top">
                    <td><span class="style3" style="font-size: smaller">Organisasi</span></td>
                    <td><span class="style3" style="font-size: smaller">:</span></td>
                    <td><span class="style3" style="font-size: smaller"><?php echo $kode_permen . ' ' . $organisasi ?></span></td>
                </tr>
                <tr align="left" valign="top">
                    <td><span class="style3" style="font-size: smaller">Kegiatan</span></td>
                    <td><span class="style3" style="font-size: smaller">:</span></td>
                    <td><span class="Font8v style3" style="font-size: smaller"><?php echo $kegiatan_kode . ' ' ?></span><span class="Font8v style3" style="font-size: smaller"><?php echo $nama_kegiatan ?></span></td>
                </tr>
                <?php
                $misi_name = '';
                $query = "select * "
                        . "from " . sfConfig::get('app_default_schema') . ".master_misi ms "
                        . "where ms.kode_misi='" . $kode_misi . "'";
                $con = Propel::getConnection();
                $stmt = $con->prepareStatement($query);
                $rs1 = $stmt->executeQuery();
                while ($rs1->next()) {
                    $misi_name = $rs1->getString('nama_misi');
                }
                $visi_name = '';
                $query2 = "select * "
                        . "from " . sfConfig::get('app_default_schema') . ".master_visi mv limit 1";
                $stmt2 = $con->prepareStatement($query2);
                $rs2 = $stmt2->executeQuery();
                while ($rs2->next()) {
                    $visi_name = $rs2->getString('nama_visi');
                }
                ?>
                
                <tr align="left" valign="top">
                    <td><span class="style3" style="font-size: smaller">Visi</span></td>
                    <td><span class="style3" style="font-size: smaller">:</span></td>
                    <td><span class="style3" style="font-size: smaller"><?php echo $visi_name ?></span></td>
                </tr>
                <tr align="left" valign="top">
                    <td><span class="style3" style="font-size: smaller">Misi</span></td>

                    <td><span class="style3" style="font-size: smaller">:</span></td>
                    <td ><span class="style3" style="font-size: smaller"><?php echo $misi_name ?> </span></td>
                </tr>
                <?php
                $tujuan = '';
                $query = "select nama_tujuan "
                        . "from " . sfConfig::get('app_default_schema') . ".master_tujuan mj "
                        . "where mj.kode_misi='" . $kode_misi . "' and mj.kode_tujuan='" . $kode_tujuan . "'";
                $con = Propel::getConnection();
                $stmt = $con->prepareStatement($query);
//                        echo $query;exit;
                $rs1 = $stmt->executeQuery();
                while ($rs1->next()) {
                    $tujuan = $rs1->getString('nama_tujuan');
                }
                ?>
                <tr align="left" valign="top">
                    <td><span class="style3" style="font-size: smaller">Tujuan</span></td>
                    <td><span class="style3" style="font-size: smaller">:</span></td>

                    <td><span class="style3" style="font-size: smaller"> <?php echo $tujuan ?>  </span></td>
                </tr>
                <?php
                $query = "select nama_program "
                        . "from " . sfConfig::get('app_default_schema') . ".master_program "
                        . "where kode_program ilike '$kode_program' and kode_tujuan ilike '%$kode_tujuan%'";
                $stmt = $con->prepareStatement($query);
                $rs1 = $stmt->executeQuery();
                while ($rs1->next()) {
                    $program = $rs1->getString('nama_program');
                }
                ?>
                <tr align="left" valign="top">
                    <td><span class="style3" style="font-size: smaller">Program RPJM</span></td>
                    <td><span class="style3" style="font-size: smaller">:</span></td>
                    <td><span class="style3" style="font-size: smaller"> <?php echo $program ?>  </span></td>
                </tr>
                <?php
                $d = new Criteria();
                $d->add(MasterProgram2Peer::KODE_PROGRAM, $kode_program);
                $d->add(MasterProgram2Peer::KODE_PROGRAM2, $program_p_13);
                $dx = MasterProgram2Peer::doSelectOne($d);
                if ($dx) {
                    $program2 = $dx->getNamaProgram2();
                } else {
                    $program2 = substr($kode_per[0] . '.' . $kode_per[1], 0, 4) . '.' . $kode_program;
                }
                ?>
                <tr align="left" valign="top">
                    <td><span class="style3" style="font-size: smaller">Program P13</span></td>
                    <td><span class="style3" style="font-size: smaller">:</span></td>
                    <td><span class="style3" style="font-size: smaller"><?php echo $program2 ?> </span></td>
                </tr>
                <?php
                $query = "select rd.komponen_harga, rd.volume, rd.pajak, rd.subtitle
			from " . sfConfig::get('app_default_schema') . ".rincian_detail rd
			where rd.kegiatan_code = '" . $kode_kegiatan . "' and rd.unit_id = '" . $unit_id . " and rd.status_hapus=false' 
                            and rd.tipe = 'FISIK' 
                            order by rd.subtitle";
                $con = Propel::getConnection();
                $stmt = $con->prepareStatement($query);
                //$stmt->setInt(1, $this->getId());
                //$stmt->setLimit($max);
                $rs = $stmt->executeQuery();
                //split("pembatas",string,);
                $total = 0;
                $harga = 0;
                $harga_awal_kena_pajak = 0;
                $subtitle_array = array();
                $subtitle_array_harga = array();
                $nama_subtitle = '';
                $total_subtitle = 0;
                $total_subtitle_harga = 0;
                while ($rs->next()) {
                    if ($nama_subtitle == '') {
                        $nama_subtitle = $rs->getString('subtitle');
                        $subtitle_array[] = $nama_subtitle;
                    } else if ($nama_subtitle != $rs->getString('subtitle')) {
                        $nama_subtitle = $rs->getString('subtitle');
                        $subtitle_array[] = $nama_subtitle;
                        $subtitle_array_harga[] = $total_subtitle_harga;
                        $total_subtitle = 0;
                        $total_subtitle_harga = 0;
                    }
                    $volume = $rs->getString('volume');
                    $tunai = $rs->getString('komponen_harga');
                    $harga_awal = ($rs->getString('volume') * $rs->getString('komponen_harga'));
                    $total_subtitle = $harga_awal;
                    $harga_awal_kena_pajak = $harga_awal - (($rs->getString('pajak') / 100) * $harga_awal);
                    $total_subtitle = $total_subtitle - (($rs->getString('pajak') / 100) * $total_subtitle);
                    $total = $total + $harga_awal_kena_pajak;
                    $total_subtitle_harga = $total_subtitle_harga + $total_subtitle;
                }
                $subtitle_array_harga[] = $total_subtitle_harga;
                ?>
                <tr align="left" valign="top">
                    <?php
                    $query2 = "select sum(nilai_anggaran) as hasil
			from " . sfConfig::get('app_default_schema') . ".rincian_detail rd
			where rd.kegiatan_code = '" . $kode_kegiatan . "' and rd.unit_id = '" . $unit_id . "' and rd.status_hapus=false ";
                    $con = Propel::getConnection();
                    $stmt5 = $con->prepareStatement($query2);
                    $total_rekening = $stmt5->executeQuery();
                    while ($total_rekening->next()) {
                        $total = $total_rekening->getString('hasil');
                    }
                    ?>
                    <td><span class="style3" style="font-size: smaller">Total Nilai</span></td>
                    <td><span class="style3" style="font-size: smaller">:</span></td>
                    <td><span class="style3" style="font-size: smaller">Rp.<?php echo number_format($total, 0, ',', '.') ?>,00</span></td>
                </tr>
                <tr valign="top">
                    <td><span class="style3" style="font-size: smaller">Output Kegiatan</span></td>
                    <td><span class="style3" style="font-size: smaller">:</span></td>
                    <td>
                        <?php
                        $output = str_replace("#", "|", $output);
                        $arr_output = explode("|", $output);
                        ?>
                        <table border="1" width="100%" cellspacing="0">
                            <tr>
                                <td style="border: 1px solid; width: 80%"><span class="style5" style="font-size: smaller"><center>Tolak Ukur Kinerja</center></span></td>
                                <td style="border: 1px solid; width: 10%"><span class="style5" style="font-size: smaller"><center>Target</center></span></td>
                                <td style="border: 1px solid; width: 10%"><span class="style5" style="font-size: smaller"><center>Satuan</center></span></td>
                            </tr>
                            <?php
                            for ($i = 0; $i < count($arr_output); $i = $i + 3) {
                                ?>
                                <tr class="sf_admin_row_1" >
                                    <td style="border: 1px solid"><span class="style5" style="font-size: smaller"><?php echo $arr_output[$i] ?></span></td>
                                    <td style="border: 1px solid"><span class="style5" style="font-size: smaller"><center><?php echo $arr_output[$i + 1] ?></center></span></td>
                                    <td style="border: 1px solid"><span class="style5" style="font-size: smaller"><center><?php echo $arr_output[$i + 2] ?></center></span></td>
                                </tr>
                                <?php
                            }
                            ?>
                        </table>
                    </td>
                </tr>
                <tr valign="top">
                    <td><span class="style3" style="font-size: smaller">Output</span></td>
                    <td><span class="style3" style="font-size: smaller">:</span></td>
                    <td>
                        <table border="1" width="100%" cellspacing="0">
                            <tr>
                                <td style="border: 1px solid; width: 80%"><span class="style5" style="font-size: smaller"><center>Subtitle</center></span></td>
                                <td style="border: 1px solid; width: 10%"><span class="style5" style="font-size: smaller"><center>Target</center></span></td>
                                <td style="border: 1px solid; width: 10%"><span class="style5" style="font-size: smaller"><center>Satuan</center></span></td>
                            </tr>
                            <?php
                            $query = "select distinct(rd.subtitle) "
                                    . "from " . sfConfig::get('app_default_schema') . ".subtitle_indikator rd "
                                    . "where rd.kegiatan_code = '" . $kode_kegiatan . "' and rd.unit_id = '" . $unit_id . "'";
                            $con = Propel::getConnection();
                            $stmt = $con->prepareStatement($query);
                            $rs = $stmt->executeQuery();
                            while ($rs->next()) {
                                $subtitles = $rs->getString('subtitle');
                                $d = new Criteria();
                                //$d -> setOffset(10);
                                $d->add(SubtitleIndikatorPeer::UNIT_ID, $unit_id);
                                $d->add(SubtitleIndikatorPeer::KEGIATAN_CODE, $kode_kegiatan);
                                $d->add(SubtitleIndikatorPeer::SUBTITLE, $subtitles);
                                $d->setDistinct(SubtitleIndikatorPeer::SUBTITLE);
                                $d->addAscendingOrderByColumn(SubtitleIndikatorPeer::SUBTITLE);
                                $v = SubtitleIndikatorPeer::doSelect($d);
                                $subtitle = '';
                                foreach ($v as $vs) {
                                    $subtitle = $vs->getSubtitle();
                                    $output = $vs->getIndikator();
                                    $target = $vs->getNilai();
                                    $satuan = $vs->getSatuan();
                                    ?>
                                    <tr>
                                        <td style="border: 1px solid"><span class="style5" style = "font-size: smaller"><?php echo $subtitle ?></span></td>
                                        <td style="border: 1px solid"><span class="style5" style="font-size: smaller"><center><?php echo $target ?></center></span></td>
                                        <td style="border: 1px solid"><span class="style5" style="font-size: smaller"><center><?php echo $satuan ?></center></span></td>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="9" style="border: 1" width="100%">
            <span class="style3" style="font-size: smaller; width: 150">Kelompok Sasaran Kegiatan</span>
            <span class="style3" style="font-size: smaller; width: 10">:</span>
            <span class="style3" style="font-size: smaller;"><?php echo $kelompok_sasaran ?></span>
        </td>
    </tr>
    <tr>
        <td colspan="9" style="border: 1">
            <div><span class="style3" style="font-size: smaller"><center>Rincian Anggaran Belanja Langsung FISIK</center></span></div>
            <div><span class="style3" style="font-size: smaller"><center>Menurut Program dan Per Kegiatan Satuan Kerja Perangkat Daerah</center></span></div>
        </td>
    </tr>
    <tr>
        <td align="center" style="border: 1">&nbsp;</td>
        <td align="center" style="border: 1"><span class="style3" style="font-size: smaller"><strong>Komponen</strong></span></td>
        <td align="center" style="border: 1"><span class="style3" style="font-size: smaller"><strong>Satuan</strong></span></td>
        <td align="center" style="border: 1"><span class="style3" style="font-size: smaller"><strong>Koefisien</strong></span></td>
        <td align="center" style="border: 1;"><span class="style3" style="font-size: smaller"><strong>Harga</strong></span></td>
        <td align="center" style="border: 1;"><span class="style3" style="font-size: smaller"><strong>Hasil</strong></span></td>
        <td align="center" style="border: 1;"><span class="style3" style="font-size: smaller"><strong>PPN</strong></span></td>
        <td align="center" style="border: 1;"><span class="style3" style="font-size: smaller"><strong>Total</strong></span></td>
        <td align="center" style="border: 1;"><span class="style3" style="font-size: smaller"><strong>Belanja</strong></span></td>
    </tr>
    <?php
    $query = "select distinct(rd.subtitle) "
            . "from " . sfConfig::get('app_default_schema') . ".rincian_detail rd "
            . "where rd.kegiatan_code = '" . $kode_kegiatan . "' and rd.unit_id = '" . $unit_id . "' and rd.status_hapus=false "
            . "and rd.tipe='FISIK' "
            . "order by rd.subtitle";
    $con = Propel::getConnection();
    $stmt = $con->prepareStatement($query);
    $rs = $stmt->executeQuery();
    $subtitle = '';
    $pertama = 0;
    while ($rs->next()) {
        if (trim($subtitle) != trim($rs->getString('subtitle'))) {
            if ($pertama != 0) {
                $query2 = "select sum(nilai_anggaran) as hasil "
                        . "from " . sfConfig::get('app_default_schema') . ".rincian_detail rd "
                        . "where rd.kegiatan_code = '" . $kode_kegiatan . "' and rd.unit_id = '" . $unit_id . "' and rd.subtitle = '" . $subtitle . "' "
                        . "and rd.tipe = 'FISIK' and rd.status_hapus=false";
                //print_r($query2);
                //$con = Propel::getConnection();
                $stmt5 = $con->prepareStatement($query2);
                $total_rekening = $stmt5->executeQuery();
                while ($total_rekening->next()) {
                    echo '<tr bgcolor="#ffffff">';
                    echo '<td colspan="7" class="Font8v style3" align="right" style="border: 1; font-size: smaller;">';
                    echo '<b>Total ' . $subtitle . '</td></b>';
                    echo '<td class="Font8v style3" align="right" nowrap="nowrap" style="border: 1; font-size: smaller;"><b>' . number_format(round($total_rekening->getString('hasil'), 0), 0, ',', '.') . '</b></td>';
                    echo '<td class="Font8v style3" align="right" style="border: 1; font-size: smaller;">&nbsp;</td>';
                    echo '</tr>';
                }
            }
            $subtitle = $rs->getString('subtitle');
            $pertama+=1;
            ?>
            <tr align="left" bgcolor="white"><td colspan="9" style="border: 1;"><span class="style3" style="font-size: smaller"><strong>
                            <?php echo ':: ' . $subtitle ?>
                        </strong></span></td></tr>
            <?php
        }
        if (trim($subtitle) == trim($rs->getString('subtitle'))) {
            $j = 0;
            $subtitle = $rs->getString('subtitle');

            $query2 = "select distinct(rd.rekening_code), r.rekening_name, rd.kode_sub "
                    . "from " . sfConfig::get('app_default_schema') . ".rincian_detail rd, " . sfConfig::get('app_default_schema') . ".rekening r "
                    . "where rd.volume>0 and rd.status_hapus=false and rd.kegiatan_code = '" . $kode_kegiatan . "' and rd.unit_id = '" . $unit_id . "' and rd.subtitle = '" . $subtitle . "' "
                    . "and rd.rekening_code=r.rekening_code "
                    . "and rd.tipe= 'FISIK' "
                    . "order by rd.kode_sub,rd.rekening_code";
            //print_r($query2);
            //$con = Propel::getConnection();
            $stmt2 = $con->prepareStatement($query2);
            $rs2 = $stmt2->executeQuery();

            $total_semua = 0;
            $sub = '';
//bisma
            while ($rs2->next()) {
                if ($rs2->getString('rekening_code')) {
                    if ($rs2->getString('kode_sub')) {
                        if ($kode_sub != $rs2->getString('kode_sub')) {
                            $kode_sub = $rs2->getString('kode_sub');
                            $cekKodeSub = substr($kode_sub, 0, 4);

                            if ($cekKodeSub == 'RKAM') {
                                $query2 = "select rkam.komponen_name,rkam.detail_name, sum(rd.nilai_anggaran) as hasil "
                                        . "from " . sfConfig::get('app_default_schema') . ".rincian_detail rd, " . sfConfig::get('app_default_schema') . ".rka_member rkam "
                                        . "where rd.kegiatan_code = '" . $kode_kegiatan . "' and rd.unit_id = '" . $unit_id . "' and rd.kode_sub = '" . $kode_sub . "' 
                                            and rd.kode_sub=rkam.kode_sub and rd.status_hapus=false 
                                            and rd.tipe = 'FISIK'
                                            group by rkam.komponen_name,rkam.detail_name,rd.kode_sub 
                                            order by rd.kode_sub";
                                // print_r($query2);
                                $con = Propel::getConnection();
                                $stmt5 = $con->prepareStatement($query2);
                                $total_rkam = $stmt5->executeQuery();
                                while ($total_rkam->next()) {
                                    echo '<tr bgcolor="#ffffff">';
                                    echo '<td colspan="7" class="Font8v style3" align="left" style="border: 1; font-size: smaller;">';
                                    //total subtitle
                                    echo '<b><i>.:. ' . $total_rkam->getString('komponen_name') . ' ' . $total_rkam->getString('detail_name');
                                    '</td></i></b>';
                                    echo '<td class="Font8v style3" align="right" nowrap="nowrap" style="border: 1; font-size: smaller;"><b>' . number_format(round($total_rkam->getString('hasil'), 0), 0, ',', '.') . '</b></td>';
                                    echo '<td class="Font8v style3" align="right" style="border: 1; font-size: smaller;">&nbsp;</td>';
                                    echo '</tr>';
                                }
                            } elseif ($cekKodeSub == 'RSUB') {
                                //if($kode_sub!=$rs2->getString('kode_sub'))
                                {
                                    //echo $kode_sub.'aaa';
                                    $kode_sub = $rs2->getString('kode_sub');
                                    //$subId = $rs2->getString('from_sub_kegiatan');
                                    $s = new Criteria();
                                    //$s->add(RincianSubParameterPeer::FROM_SUB_KEGIATAN, $subId);
                                    $s->add(RincianSubParameterPeer::KEGIATAN_CODE, $kode_kegiatan);
                                    $s->add(RincianSubParameterPeer::KODE_SUB, $kode_sub);
                                    $s->add(RincianSubParameterPeer::UNIT_ID, $unit_id);
                                    //$s->add(RincianSubParameterPeer::NEW_SUBTITLE, $sub);
                                    //$s->setDistinct();
                                    $sub_pilih = RincianSubParameterPeer::doSelectOne($s);
                                    if ($sub_pilih) {
                                        $sub_name = $sub_pilih->getSubKegiatanName();
                                        $keterangan = $sub_pilih->getKeterangan();
                                        $detail_name = $sub_pilih->getDetailName();
                                    }


                                    //if($cekKodeSub=='RSUB')
                                    {
                                        ?>

                                        <tr align="left" bgcolor="white"><td colspan="9" style="border: 1; font-size: smaller;"><b>
                                                    &nbsp;<i><?php echo $sub_name . ' ' . $detail_name . '<table width=100%>{' . $keterangan . '}' ?></i>
                                                </b></td></tr>
                                        <?php
                                    }
                                }
                            }
                        }


                        $query3 = "select distinct(rd.rekening_code),rd.komponen_name, rd.satuan, rd.keterangan_koefisien, rd.komponen_harga_awal, rd.komponen_harga, rd.volume, rd.pajak, kb.belanja_name, rd.detail_no, rd.detail_name, rd.sub,rd.kode_sub,rd.tipe "
                                . "from " . sfConfig::get('app_default_schema') . ".rincian_detail rd, " . sfConfig::get('app_default_schema') . ".rekening r, " . sfConfig::get('app_default_schema') . ".kelompok_belanja kb "
                                . "where rd.volume>0 and rd.status_hapus=false and rd.kegiatan_code = '" . $kode_kegiatan . "' and rd.unit_id = '" . $unit_id . "' and rd.subtitle = '" . $subtitle . "' and rd.rekening_code='" . $rs2->getString('rekening_code') . "' "
                                . "and rd.rekening_code=r.rekening_code and r.belanja_id = kb.belanja_id and  rd.kode_sub = '" . $kode_sub . "' "
                                . "and rd.tipe = 'FISIK' "
                                . "order by rd.kode_sub,rd.rekening_code";

                        // print_r($query3);
                    } else {
                        $query3 = "select distinct(rd.rekening_code),rd.komponen_name, rd.satuan, rd.keterangan_koefisien, rd.komponen_harga_awal, rd.komponen_harga, rd.volume, rd.pajak, kb.belanja_name, rd.detail_no, rd.detail_name, rd.sub,rd.kode_sub,rd.tipe "
                                . "from " . sfConfig::get('app_default_schema') . ".rincian_detail rd, " . sfConfig::get('app_default_schema') . ".rekening r, " . sfConfig::get('app_default_schema') . ".kelompok_belanja kb "
                                . "where rd.volume>0 and rd.status_hapus=false and rd.kegiatan_code = '" . $kode_kegiatan . "' and rd.unit_id = '" . $unit_id . "' and rd.subtitle = '" . $subtitle . "' and rd.rekening_code='" . $rs2->getString('rekening_code') . "' 
                                    and rd.rekening_code=r.rekening_code and r.belanja_id = kb.belanja_id and rd.kode_sub = '' 
                                    and rd.tipe = 'FISIK' 
                                    order by rd.kode_sub,rd.rekening_code";
                    }


                    $rekening = $rs2->getString('rekening_code');
                    echo '<tr bgcolor="white"><td style="border: 1;"> :</td><td colspan="8" style="border: 1; font-size: smaller;">' . $rekening . ' ' . $rs2->getString('rekening_name') . '</td></tr>';

                    //$con = Propel::getConnection();
                    $stmt3 = $con->prepareStatement($query3);
                    $rs3 = $stmt3->executeQuery();
                    $total = 0;
                    while ($rs3->next()) {
                        echo '<tr bgcolor="#ffffff" valign="top">';
                        //$ubah_subtitle = str_ireplace("/", "-", $subtitle);
                        echo '<td class="Font8v" align="left" style="border: 1;">&nbsp;</td>';

                        if (!$rs3->getString('sub')) {
                            $tipe = '{' . $rs3->getString('tipe') . '}';
                        } else {
                            $tipe = '';
                        }

                        echo '<td class="Font8v" align="left" style="border: 1;"><span class="style3" style="font-size: smaller">' . $rs3->getString('komponen_name') . ' ' . $rs3->getString('detail_name') . '</span></td>';
                        echo '<td class="Font8v style3" align="center" nowrap="nowrap" style="border: 1;"><span class="Font8v" style="font-size: smaller">' . $rs3->getString('satuan') . '</span></td>';
                        echo '<td class="Font8v style3" align="center" style="border: 1;"><span class="Font8v" style="font-size: smaller">' . $rs3->getString('keterangan_koefisien') . '</span></td>';
                        if (number_format($rs3->getString('komponen_harga_awal'), 0, ',', '.') == '0') {
                            echo '<td class="Font8v style3" align="right" nowrap="nowrap" style="border: 1; font-size: smaller;">' . number_format($rs3->getString('komponen_harga_awal'), 3, ',', '.') . '</td>';
                        } else {
                            echo '<td class="Font8v style3" align="right" nowrap="nowrap" style="border: 1; font-size: smaller;">' . number_format($rs3->getString('komponen_harga_awal'), 0, ',', '.') . '</td>';
                        }

                        $hasil = $rs3->getString('komponen_harga_awal') * $rs3->getString('volume');
                        echo '<td class="Font8v style3" align="right" nowrap="nowrap" style="border: 1; font-size: smaller;">' . number_format($hasil, 0, ',', '.') . '</td>';
                        echo '<td class="Font8v style3" align="right" nowrap="nowrap" style="border: 1; font-size: smaller;">' . $rs3->getString('pajak') . '%</td>';
                        $total1 = $hasil + (($rs3->getString('pajak') / 100) * $hasil);
                        echo '<td class="Font8v style3" align="right" nowrap="nowrap" style="border: 1; font-size: smaller;">' . number_format(round($total1, 0), 0, ',', '.') . '</td>';
                        $l = strlen($rs3->getString('belanja_name'));
                        echo '<td class="Font8v style3" align="center" style="border: 1; font-size: smaller;">' . substr(ucwords(strtolower($rs3->getString('belanja_name'))), 8, $l) . '</td>';
                        echo '</tr>';
                        $total = $total + $total1;
                    }
                }
                $total_semua = $total_semua + $total;
                echo '<tr bgcolor="#ffffff">';
                echo '<td colspan="7" class="Font8v style3" align="right" style="border: 1; font-size: smaller;">';
                echo 'Total ' . $rs2->getString('rekening_name') . '</td>';
                echo '<td class="Font8v style3" align="right" nowrap="nowrap" style="border: 1; font-size: smaller;">' . number_format(round($total, 0), 0, ',', '.') . '</td>';
                echo '<td class="Font8v style3" align="right" style="border: 1; font-size: smaller;">&nbsp;</td>';
                echo '</tr>';
            }
        }
    }
    $query2 = "select sum(nilai_anggaran) as hasil "
            . "from " . sfConfig::get('app_default_schema') . ".rincian_detail "
            . "where kegiatan_code = '" . $kode_kegiatan . "' and unit_id = '" . $unit_id . "' and subtitle = '" . $subtitle . "' and status_hapus=false "
            . "and tipe = 'FISIK' ";
    //print_r($query2);
    $con = Propel::getConnection();
    $stmt5 = $con->prepareStatement($query2);
    $total_rekening = $stmt5->executeQuery();
    while ($total_rekening->next()) {
        echo '<tr bgcolor="#ffffff">';
        echo '<td colspan="7" class="Font8v style3" align="right" style="border: 1; font-size: smaller;">';
        //total subtitle
        echo '<b>Total ' . $subtitle . '</td></b>';
        echo '<td class="Font8v style3" align="right" nowrap="nowrap" style="border: 1; font-size: smaller;"><b>' . number_format(round($total_rekening->getString('hasil'), 0), 0, ',', '.') . '</b></td>';
        echo '<td class="Font8v style3" align="right" style="border: 1; font-size: smaller;">&nbsp;</td>';
        echo '</tr>';
    }
    $query2 = "select sum(nilai_anggaran) as hasil "
            . "from " . sfConfig::get('app_default_schema') . ".rincian_detail rd "
            . "where rd.kegiatan_code = '" . $kode_kegiatan . "' and rd.unit_id = '" . $unit_id . "' and rd.status_hapus=false "
            . "and rd.tipe = 'FISIK' ";
    $con = Propel::getConnection();
    $stmt5 = $con->prepareStatement($query2);
    $total_rekening = $stmt5->executeQuery();
    while ($total_rekening->next()) {
        echo '<tr bgcolor="#ffffff">';
        echo '<td colspan="7" class="Font8v style3" align="right" style="border: 1; font-size: smaller;">';
        echo '<b>Grand Total</b></td>';
        echo '<td class="Font8v style3" align="right" nowrap="nowrap" style="border: 1; font-size: smaller;"><b>' . number_format(round($total_rekening->getString('hasil'), 0), 0, ',', '.') . '</b></td>';
        echo '<td class="Font8v style3" align="right">&nbsp;</td>';
        echo '</tr>';
    }
    ?>
    <?php
    $c = new Criteria();
    $c->add(UnitKerjaPeer::UNIT_ID, $unit_id);
    $cs = UnitKerjaPeer::doSelectOne($c);
    if ($cs) {
        $kepala = $cs->getKepalaPangkat();
        $nama = $cs->getKepalaNama();
        $nip = $cs->getKepalaNip();
        $unit_name = $cs->getUnitName();
    }
    ?>
    <tr>
        <td style="font-size: smaller;" colspan="5" rowspan="2">&nbsp;</td>
        <td style="font-size: smaller;" colspan="4" border="">
            Surabaya,<br/>
        </td>
    </tr>        
    <tr>
        <td style="font-size: smaller;" colspan="4" border="0" align="center">            
            <br/>
            <?php
            if ($unit_id == '0700') {
                echo strtoupper('INSPEKTUR');
            } else {
                echo 'KEPALA ';
                echo strtoupper($unit_name);
            }
            ?>
            <br/><br/><br/><br/><br/><br/><br/>
            <font style="text-decoration: underline; font-weight: bold"><?php echo $nama ?></font><br/>
            <?php echo $kepala ?><br/>
            <?php echo 'NIP : ' . $nip ?>
            <br/><br/>
        </td>
    </tr>
    <tr>
        <td colspan="9" style="border: 1">Keterangan :</td>
    </tr>
    <tr>
        <td colspan="9" style="border: 1">Tanggal Pembahasan :</td>
    </tr>    
    <tr>
        <td colspan="9" style="border: 1">Catatan :</td>
    </tr>    
    <tr>
        <td colspan="9" style="border: 1">1.</td>
    </tr>
    <tr>
        <td colspan="9" style="border: 1">2.</td>
    </tr>
    <tr>
        <td colspan="9" style="border: 1">3.</td>
    </tr>
    <tr>
        <td colspan="9" style="border: 1">4.</td>
    </tr>
    <tr>
        <td colspan="9" style="border: 1">5.</td>
    </tr>
    <tr>
        <td colspan="9" style="border: 1" align="center">Tim Anggaran Pemerintahan Daerah</td>
    </tr>
    <tr>
        <td colspan="1" style="border: 1" align="center">No</td>
        <td colspan="3" style="border: 1" align="center">Nama</td>
        <td colspan="3" style="border: 1" align="center">NIP</td>
        <td colspan="2" style="border: 1" align="center">Jabatan</td>
    </tr>
    <tr>
        <td colspan="1" style="border: 1" align="center">1</td>
        <td colspan="3" style="border: 1"></td>
        <td colspan="3" style="border: 1"></td>
        <td colspan="2" style="border: 1"></td>
    </tr>    
    <tr>
        <td colspan="1" style="border: 1" align="center">2</td>
        <td colspan="3" style="border: 1"></td>
        <td colspan="3" style="border: 1"></td>
        <td colspan="2" style="border: 1"></td>
    </tr>    
    <tr>
        <td colspan="1" style="border: 1" align="center">3</td>
        <td colspan="3" style="border: 1"></td>
        <td colspan="3" style="border: 1"></td>
        <td colspan="2" style="border: 1"></td>
    </tr>    
    <tr>
        <td colspan="1" style="border: 1" align="center">4</td>
        <td colspan="3" style="border: 1"></td>
        <td colspan="3" style="border: 1"></td>
        <td colspan="2" style="border: 1"></td>
    </tr>    
</table>
