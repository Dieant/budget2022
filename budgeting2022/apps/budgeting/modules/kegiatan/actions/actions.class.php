<?php

/**
 * kegiatan actions.
 *
 * @package    budgeting
 * @subpackage dinas
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 2288 2006-10-02 15:22:13Z fabien $
 */
class kegiatanActions extends autokegiatanActions {

    public function executeLihatVerifikasiKegiatan() {
        $this->unit_id = $unit_id = $this->getRequestParameter('unit_id');
        $this->kode_kegiatan = $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
        $tahap = DinasMasterKegiatanPeer::getTahapKegiatan($unit_id, $kode_kegiatan);
        $c = new Criteria();
        $c->add(RincianBappekoPeer::UNIT_ID, $unit_id);
        $c->add(RincianBappekoPeer::KODE_KEGIATAN, $kode_kegiatan);
        $c->add(RincianBappekoPeer::TAHAP, $tahap);
        if ($rs_rincian_bappeko = RincianBappekoPeer::doSelectOne($c)) {
            if ($this->getRequest()->getMethod() == sfRequest::POST) {
                if ($this->getRequestParameter('ubah')) {
                    $status = "";
                    $c_rincian = new Criteria();
                    $c_rincian->add(DinasRincianPeer::UNIT_ID, $unit_id);
                    $c_rincian->add(DinasRincianPeer::KEGIATAN_CODE, $kode_kegiatan);
                    $rs_rincian = DinasRincianPeer::doSelectOne($c_rincian);
                    if ($rs_rincian_bappeko->getStatusBuka()) {
                        $rs_rincian_bappeko->setStatusBuka(FALSE);
                        $rs_rincian->setRincianLevel(3);
                        $status = "tidak setuju";
                    } else {
                        $rs_rincian_bappeko->setStatusBuka(TRUE);
                        $rs_rincian->setRincianLevel(2);
                        $status = "setuju";
                    }
                    $rs_rincian_bappeko->save();
                    $rs_rincian->save();
                    $this->setFlash('berhasil', 'Status persetujan bappeko telah dirubah menjadi ' . $status);
                    $this->redirect("kegiatan/lihatVerifikasiKegiatan?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                } elseif ($this->getRequestParameter('hapus')) {
                    $rs_rincian_bappeko->delete();

                    $c_rincian = new Criteria();
                    $c_rincian->add(DinasRincianPeer::UNIT_ID, $unit_id);
                    $c_rincian->add(DinasRincianPeer::KEGIATAN_CODE, $kode_kegiatan);
                    $rs_rincian = DinasRincianPeer::doSelectOne($c_rincian);
                    $rs_rincian->setRincianLevel(3);
                    $rs_rincian->save();

                    $this->setFlash('berhasil', 'Verifikasi telah dihapus');
                    $this->redirect("kegiatan/listRevisi");
                }
                $this->setFlash('gagal', 'Gagal');
                $this->redirect("kegiatan/lihatVerifikasiKegiatan?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
            } else {
                $cri = new Criteria();
                $cri->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                $cri->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);

                // tampilkan jawaban terkait ubah f1 dan sisa lelang
                $jawaban = DinasMasterKegiatanPeer::doSelectOne($cri);
                if ($jawaban) {
                    $this->jawabankegiatan = $jawaban;
                }

                $this->rs_rincian_bappeko = $rs_rincian_bappeko;
            }
        } else {
            $this->redirect("kegiatan/listRevisi");
        }
    }

     public function executeUbahVolRasionalisasi() {
        $this->detail_no = $this->getRequestParameter('detail_no');
        $this->unit_id = $this->getRequestParameter('unit_id');       
        $this->kegiatan_code = $this->getRequestParameter('kegiatan_code');

        $totVolumeRealisasi=0;
        $rs_rinciandetails = new DinasRincianDetail;
        $totVolumeRealisasi =  $rs_rinciandetails->getCekVolumeRealisasi($unit_id, $kegiatan_code, $detail_no);

        if($this->getRequestParameter('volume_rasionalisasi') >= $totVolumeRealisasi )
        {
        $query1 = "select *
                from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail r
                where unit_id='" .  $this->getRequestParameter('unit_id') . "' and kegiatan_code='" .  $this->getRequestParameter('kegiatan_code') . "' and detail_no ='" .  $this->getRequestParameter('detail_no') . "'";
            //print_r($query);exit;
            $con = Propel::getConnection();
            $stmt1 = $con->prepareStatement($query1);
            $rs1 = $stmt1->executeQuery();
            $rs1->next();
            $vol_awal = $rs1->getString('volume_rasionalisasi');
        
            $sql = "update " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail "
                    . "set volume_rasionalisasi='" . $this->getRequestParameter('volume_rasionalisasi')
                    . "' where unit_id='" . $this->getRequestParameter('unit_id')
                    . "' and kegiatan_code='" . $this->getRequestParameter('kegiatan_code')
                    . "' and detail_no= " . $this->getRequestParameter('detail_no');       
             $con = Propel::getConnection();
            $stmt = $con->prepareStatement($sql);        
            $stmt->executeQuery();
            budgetLogger::log('Update Volume Rasionalisasi dari volume rasionalisasi awal = '.$vol_awal.' menjadi '. $this->getRequestParameter('volume_rasionalisasi').' unit id =' . $this->act = $this->getRequestParameter('unit_id') . ' kode kegiatan=' . $this->act = $this->getRequestParameter('kegiatan_code') .
            ' dan detail no=' . $this->act = $this->getRequestParameter('detail_no'));       
            $this->setFlash('berhasil', 'berhasil mengubah nilai volume rasionalisasi di e-Budgeting');        
        }
        else
        {
             $this->setFlash('gagal', 'volume rasionalisasi di e-Budgeting harus melebihi volume realisasi e-delivery sebesar : '.$totVolumeRealisasi);    
        }

        return $this->redirect('kegiatan/editRevisi?unit_id=' .$this->getRequestParameter('unit_id') . '&kode_kegiatan=' . $this->getRequestParameter('kegiatan_code'));
        
    }

    public function executeUbahVolumeOrang() {
        if ($this->getRequestParameter('unit_id') ) {
            $unit_id = $this->getRequestParameter('unit_id');
            $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');

            $c = new Criteria();
            $c->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
            $c->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
            $rs_kegiatan = DinasMasterKegiatanPeer::doSelectOne($c);
            $this->rs_kegiatan = $rs_kegiatan;

            $komponen_terambil = array();
            $c = new Criteria();
            $c->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
            $c->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
            $c->add(DinasRincianDetailPeer::STATUS_HAPUS, FALSE);
            $rs_budgeting = DinasRincianDetailPeer::doSelect($c);
            foreach ($rs_budgeting as $key => $budgeting) {
                $komponen = $budgeting->getKomponenId();
                $subtitle = $budgeting->getSubtitle();
                $komponen_terambil["$komponen"] = 1;
                $komponen_terambil["$subtitle"] = 1;
            }
            $this->komponen_terambil = $komponen_terambil;
           
            $query ="(SELECT rd.komponen_name , rd.volume, rd.satuan, rd.detail_name, rd.detail_kegiatan,rd.keterangan_koefisien, rd.komponen_harga, rd.nilai_anggaran, rd.volume_orang, r.volume_orang as vol_orang, rd.detail_no 
            from ebudget.dinas_rincian_detail rd,ebudget.rincian_detail r
            WHERE rd.status_hapus=false
            AND rd.unit_id=r.unit_id
            AND rd.kegiatan_code=r.kegiatan_code
            AND rd.detail_kegiatan = r.detail_kegiatan
            AND rd.unit_id = '$unit_id'
            AND rd.kegiatan_code = '$kode_kegiatan'       
            AND (rd.satuan ILIKE '%Orang Bulan%' OR rd.satuan ILIKE '%Orang/Bulan%'))            
            UNION
            (SELECT rd.komponen_name , rd.volume, rd.satuan, rd.detail_name, rd.detail_kegiatan,rd.keterangan_koefisien, rd.komponen_harga, rd.nilai_anggaran ,rd.volume_orang,0 as vol_orang, rd.detail_no 
            from ebudget.dinas_rincian_detail rd
            WHERE rd.status_hapus=false            
            AND rd.unit_id = '$unit_id'
            AND rd.kegiatan_code = '$kode_kegiatan'          
            AND (rd.satuan ILIKE '%Orang Bulan%' OR rd.satuan ILIKE '%Orang/Bulan%')
            and rd.detail_kegiatan not in (select detail_kegiatan from ebudget.rincian_detail
            where status_hapus=false and unit_id= '$unit_id' and kegiatan_code = '$kode_kegiatan')
            )";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs_rinciandetail = $stmt->executeQuery();
            $rs_copy = $stmt->executeQuery();
            $this->rs_rinciandetail = $rs_copy;

            $this->unit_id = $unit_id;
            $this->kode_kegiatan = $kode_kegiatan;
        }
    }

    public function executeSearchKomponenRka() {
        $kegiatan_code = $this->getRequestParameter('kegiatan');
        $unit_id = $this->getRequestParameter('unit');

        $this->filters = $this->getRequestParameter('filters');
        if (isset($this->filters['nama_komponen']) && $this->filters['nama_komponen'] !== '') {
            $cari = $this->filters['nama_komponen'];
        }
        $this->cari = $cari;               
          $queryku = "select * from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail
                        where komponen_name ilike '%" . $cari . "%' and kegiatan_code='$kegiatan_code' and unit_id='$unit_id' and status_hapus=false order by detail_kegiatan";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($queryku);
            $rcsb = $stmt->executeQuery();         
            $this->rs_rd = $rcsb;
    }

    public function executeProsesUbahVolumeOrang() {
        $detail_no = $this->getRequestParameter('detail_no');
        $volume_orang = $this->getRequestParameter('volume_orang');
        $unit_id = $this->getRequestParameter('unit_id');
        $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
        
        try {
            $con = Propel::getConnection();
            $con->begin();
            
            foreach( $detail_no as $key => $no ) {
                //if(!empty($volume_orang[$key])) 
                if($volume_orang[$key] != '')
                {
                    $detail=$unit_id.'.'.$kode_kegiatan.'.'.$no;
                    
                    $vol_sebelum="SELECT volume_orang  from " . sfConfig::get('app_default_schema') . ".rincian_detail where detail_kegiatan='".$detail."'";
                    $st = $con->prepareStatement($vol_sebelum);
                    $rs1=$st->executeQuery();
                    while($rs1->next())
                    {
                        $cek = "SELECT count(*) as hasil from  " . sfConfig::get('app_default_schema') . ".log_volume_orang where 
                        detail_kegiatan='".$detail."' and tahap='".sfConfig::get('app_tahap_detail')."'";                   
                        $stmt3=  $con->prepareStatement($cek);
                        $rs=$stmt3->executeQuery();
                        while($rs->next())
                        { 
                            $nilai=$rs->getString('hasil');
                            if($nilai==0)
                            {
                               $insertvol ="INSERT INTO " . sfConfig::get('app_default_schema') . ".log_volume_orang(
                                detail_kegiatan, volume_semula, volume_menjadi, tahap, waktu_edit)
                                VALUES ('" . $detail. "',".$rs1->getString('volume_orang').", $volume_orang[$key],'".sfConfig::get('app_tahap_detail')."','".date('Y-m-d h:i:sa')."')";
                                $stmt2 = $con->prepareStatement($insertvol);
                                $stmt2->executeQuery();
                            }
                            else
                            {
                                $updatevol =
                                "UPDATE " . sfConfig::get('app_default_schema') . ".log_volume_orang
                                SET volume_menjadi=$volume_orang[$key],tahap='".sfConfig::get('app_tahap_detail')."',waktu_edit='".date('Y-m-d h:i:sa')."' where detail_kegiatan='".$detail."' and tahap='".sfConfig::get('app_tahap_detail')."'";
                                $stmt2 = $con->prepareStatement($updatevol);
                                $stmt2->executeQuery();
                            }

                        } 

                        $query =
                        "UPDATE " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail
                        set volume_orang = $volume_orang[$key]
                        WHERE unit_id = '$unit_id'
                        AND kegiatan_code = '$kode_kegiatan'
                        AND detail_no = $no";
                        $stmt = $con->prepareStatement($query);
                        $stmt->executeQuery();

                        $queryrka =
                        "UPDATE " . sfConfig::get('app_default_schema') . ".rincian_detail
                        set volume_orang = $volume_orang[$key]
                        WHERE unit_id = '$unit_id'
                        AND kegiatan_code = '$kode_kegiatan'
                        AND detail_no = $no";
                        $stmt1 = $con->prepareStatement($queryrka);
                        $stmt1->executeQuery();

                        $queryrbp =
                        "UPDATE " . sfConfig::get('app_default_schema') . ".rincian_detail_bp
                        set volume_orang = $volume_orang[$key]
                        WHERE unit_id = '$unit_id'
                        AND kegiatan_code = '$kode_kegiatan'
                        AND detail_no = $no";
                        $stmt2 = $con->prepareStatement($queryrbp);
                        $stmt2->executeQuery();
                    }


                     $query =
                        "UPDATE " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail
                        set volume_orang = $volume_orang[$key]
                        WHERE unit_id = '$unit_id'
                        AND kegiatan_code = '$kode_kegiatan'
                        AND detail_no = $no";
                        $stmt = $con->prepareStatement($query);
                        $stmt->executeQuery();                             
    
                }
            }
            //die($query);
        } catch(Exception $e) {
            $con->rollback();
            $this->setFlash('gagal', 'Gagal : ' . $e->getMessage());
            return $this->redirect('kegiatan/ubahVolumeOrang?unit_id=' . $unit_id . '&kode_kegiatan=' . $kode_kegiatan);
        }

        $con->commit();

        $this->setFlash('berhasil', 'Berhasil Ubah Volume Orang.');
        return $this->redirect('kegiatan/ubahVolumeOrang?unit_id=' . $unit_id . '&kode_kegiatan=' . $kode_kegiatan);
    }

    public function executePenandaCovid() {
        if ($this->getRequestParameter('unit_id') ) {
            $unit_id = $this->getRequestParameter('unit_id');
            $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');

            $c = new Criteria();
            $c->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
            $c->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
            $rs_kegiatan = DinasMasterKegiatanPeer::doSelectOne($c);
            $this->rs_kegiatan = $rs_kegiatan;

            $komponen_terambil = array();
            $c = new Criteria();
            $c->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
            $c->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
            $c->add(DinasRincianDetailPeer::STATUS_HAPUS, FALSE);
            $rs_budgeting = DinasRincianDetailPeer::doSelect($c);
            foreach ($rs_budgeting as $key => $budgeting) {
                $komponen = $budgeting->getKomponenId();
                $subtitle = $budgeting->getSubtitle();
                $komponen_terambil["$komponen"] = 1;
                $komponen_terambil["$subtitle"] = 1;
            }
            $this->komponen_terambil = $komponen_terambil;

            $query =
            "SELECT rd.unit_id,rd.kegiatan_code,rd.komponen_name ,rd.is_covid, rd.volume, rd.satuan, rd.detail_name, rd.detail_kegiatan,rd.keterangan_koefisien, rd.tahap,rd.komponen_harga, rd.nilai_anggaran, rd.volume_orang, rd.volume_orang as vol_orang, rd.detail_no,case when rd.is_covid = true then 'Covid-19' else '' end as sumber_dana
            from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail rd
            WHERE rd.status_hapus=false   
            AND rd.volume > 0              
            AND rd.unit_id = '$unit_id'
            AND rd.kegiatan_code = '$kode_kegiatan'           
            ORDER BY rd.komponen_name";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs_rinciandetail = $stmt->executeQuery();
            $rs_copy = $stmt->executeQuery();
            $this->rs_rinciandetail = $rs_copy;

            $this->unit_id = $unit_id;
            $this->kode_kegiatan = $kode_kegiatan;
        }
    }

      public function executeProsesUbahPenandaCovid() {
        $detail_no = $this->getRequestParameter('detail_no');
        $penanda = $this->getRequestParameter('sumber_dana');
        //$realisasi = $this->getRequestParameter('nilairealisasi');
        $unit_id = $this->getRequestParameter('unit_id');
        $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
      
        try {
            $con = Propel::getConnection();
            $con->begin();            
            foreach( $detail_no as $key => $no ) {             
               
                    if(!empty($penanda[$key])) {
                    $tahap=sfConfig::get('app_tahap_detail');
                    $detail=$unit_id.'.'.$kode_kegiatan.'.'.$penanda[$key]; 
                   // die($penanda[$key]." ".$key."".$tahap." ". $detail);    

                    if ($this->getRequestParameter('proses'))
                    {                     

                        $query = "UPDATE " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail
                        set is_covid = true               
                        WHERE detail_kegiatan='$detail'";
                        $stmt = $con->prepareStatement($query);
                        $stmt->executeQuery();

                        $queryrka = "UPDATE " . sfConfig::get('app_default_schema') . ".rincian_detail
                        set is_covid = true
                        WHERE detail_kegiatan='$detail'";
                        $stmt1 = $con->prepareStatement($queryrka);
                        $stmt1->executeQuery();

                        $querybp = "UPDATE " . sfConfig::get('app_default_schema') . ".rincian_detail_bp
                        set is_covid = true 
                        WHERE detail_kegiatan='$detail'";
                        $stmt2 = $con->prepareStatement($querybp);
                        $stmt2->executeQuery();

                        budgetLogger::log('Menandai Komponen Covid 19 pada kode sub kegiatan ' . $kode_kegiatan . ' unit_id : '.$unit_id.' untuk detail kegiatan '. $detail);

                    }    
                    else if ($this->getRequestParameter('hapus'))
                    {
                        
                        $query = "UPDATE " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail
                        set is_covid = false             
                        WHERE detail_kegiatan='$detail'";
                        $stmt = $con->prepareStatement($query);
                        $stmt->executeQuery();

                        $queryrka = "UPDATE " . sfConfig::get('app_default_schema') . ".rincian_detail
                        set is_covid = false                
                        WHERE detail_kegiatan='$detail'";
                        $stmt = $con->prepareStatement($queryrka);
                        $stmt->executeQuery();

                        $querybp = "UPDATE " . sfConfig::get('app_default_schema') . ".rincian_detail_bp
                        set is_covid = false             
                        WHERE detail_kegiatan='$detail'";
                        $stmt = $con->prepareStatement($querybp);
                        $stmt->executeQuery();

                        budgetLogger::log('Menghapus Penanda Komponen Covid 19 pada kode sub kegiatan ' . $kode_kegiatan . ' unit_id : '.$unit_id.' untuk detail kegiatan '. $detail);

                    }              
                }
                else if ( $realisasi <> 0)
                {
                     $this->setFlash('gagal', 'Gagal Sudah Ada Realisasi');
                     return $this->redirect('kegiatan/penandaCOvid?unit_id=' . $unit_id . '&kode_kegiatan=' . $kode_kegiatan);

                }
            }
            //die($query);
        } catch(Exception $e) {
            $con->rollback();
            $this->setFlash('gagal', 'Gagal : ' . $e->getMessage());
            return $this->redirect('kegiatan/penandaCovid?unit_id=' . $unit_id . '&kode_kegiatan=' . $kode_kegiatan);
        }

        $con->commit();

        $this->setFlash('berhasil', 'Berhasil Ubah Penanda Covid 19.');
        return $this->redirect('kegiatan/penandaCovid?unit_id=' . $unit_id . '&kode_kegiatan=' . $kode_kegiatan);
    }

    public function executePenandaBtl() {
        if ($this->getRequestParameter('unit_id') ) {
            $unit_id = $this->getRequestParameter('unit_id');
            $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');

            $c = new Criteria();
            $c->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
            $c->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
            $rs_kegiatan = DinasMasterKegiatanPeer::doSelectOne($c);
            $this->rs_kegiatan = $rs_kegiatan;

            $komponen_terambil = array();
            $c = new Criteria();
            $c->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
            $c->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
            $c->add(DinasRincianDetailPeer::STATUS_HAPUS, FALSE);
            $rs_budgeting = DinasRincianDetailPeer::doSelect($c);
            foreach ($rs_budgeting as $key => $budgeting) {
                $komponen = $budgeting->getKomponenId();
                $subtitle = $budgeting->getSubtitle();
                $komponen_terambil["$komponen"] = 1;
                $komponen_terambil["$subtitle"] = 1;
            }
            $this->komponen_terambil = $komponen_terambil;

            $query =
            "SELECT rd.unit_id,rd.kegiatan_code,rd.komponen_name , rd.volume, rd.satuan, rd.detail_name, rd.detail_kegiatan,rd.keterangan_koefisien, rd.tahap,rd.komponen_harga, rd.nilai_anggaran, rd.volume_orang, rd.rekening_code,rd.volume_orang as vol_orang, rd.detail_no,case when rd.sumber_dana_id = 11 then 'APBD' when rd.sumber_dana_id = 1 then 'DAK'
                when rd.sumber_dana_id = 2 then 'DBHCHT' when rd.sumber_dana_id = 3 then 'DID' 
                when rd.sumber_dana_id = 4 then 'DBH' when rd.sumber_dana_id = 5 then 'Pajak Rokok'
                when rd.sumber_dana_id = 6 then 'BLUD' when rd.sumber_dana_id = 7 then 'Bantuan Keuangan Provinsi'
                when rd.sumber_dana_id = 8 then 'Kapitasi JKN' when rd.sumber_dana_id = 9 then 'BOS' 
                when rd.sumber_dana_id = 10 then 'DAU' end as sumber_dana
            from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail rd
            WHERE rd.status_hapus=false   
            AND rd.volume > 0              
            AND rd.unit_id = '$unit_id'
            AND rd.kegiatan_code = '$kode_kegiatan'           
            ORDER BY rd.komponen_name";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs_rinciandetail = $stmt->executeQuery();
            $rs_copy = $stmt->executeQuery();
            $this->rs_rinciandetail = $rs_copy;

            $this->unit_id = $unit_id;
            $this->kode_kegiatan = $kode_kegiatan;
        }
    }

     public function executeProsesUbahPenandaBtl() {
        $detail_no = $this->getRequestParameter('detail_no');
        $penanda = $this->getRequestParameter('sumber_dana');
        //$realisasi = $this->getRequestParameter('nilairealisasi');
        $unit_id = $this->getRequestParameter('unit_id');
        $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
      
        try {
            $con = Propel::getConnection();
            $con->begin();            
            foreach( $detail_no as $key => $no ) {             
               
                if(!empty($penanda[$key])) {

                    $tahap=sfConfig::get('app_tahap_detail');
                    $detail=$unit_id.'.'.$kode_kegiatan.'.'.$no;     
                    //die($penanda[$key]." ".$no."".$tahap." ". $detail);                                

                    $query = "UPDATE " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail
                    set sumber_dana_id = $penanda[$key]
                    WHERE detail_kegiatan='$detail'";
                    $stmt = $con->prepareStatement($query);
                    $stmt->executeQuery();

                    $queryrka = "UPDATE " . sfConfig::get('app_default_schema') . ".rincian_detail
                    set sumber_dana_id = $penanda[$key]
                    WHERE detail_kegiatan='$detail'";
                    $stmt1 = $con->prepareStatement($queryrka);
                    $stmt1->executeQuery();

                    $querybp = "UPDATE " . sfConfig::get('app_default_schema') . ".rincian_detail_bp
                    set sumber_dana_id = $penanda[$key]
                    WHERE detail_kegiatan='$detail'";
                    $stmt2 = $con->prepareStatement($querybp);
                    $stmt2->executeQuery();
                              
                    historyUserLog::update_sumber_dana($unit_id, $kode_kegiatan, $detail, $penanda[$key]);
                    budgetLogger::log('Update Sumber dana id pada kode sub kegiatan ' . $kode_kegiatan . ' unit_id : '.$unit_id.' untuk detail kegiatan '. $detail.'dengan sumber dana id baru : '. $penanda[$key]);
                            
                }
                else if ( $realisasi <> 0)
                {
                     $this->setFlash('gagal', 'Gagal Sudah Ada Realisasi');
                     return $this->redirect('kegiatan/penandaBtl?unit_id=' . $unit_id . '&kode_kegiatan=' . $kode_kegiatan);

                }
            }
            //die($query);
        } catch(Exception $e) {
            $con->rollback();
            $this->setFlash('gagal', 'Gagal : ' . $e->getMessage());
            return $this->redirect('kegiatan/penandaBtl?unit_id=' . $unit_id . '&kode_kegiatan=' . $kode_kegiatan);
        }

        $con->commit();

        $this->setFlash('berhasil', 'Berhasil Ubah Penanda Sumber Dana.');
        return $this->redirect('kegiatan/penandaBtl?unit_id=' . $unit_id . '&kode_kegiatan=' . $kode_kegiatan);
    }
     

    public function executeProsesBukaRek() {
        $unit_id = $this->getRequestParameter('unit_id');
        if (!$unit_id) {
            $this->setFlash('gagal', 'Pilih SKPD terlebih dahulu');
            return $this->redirect("kegiatan/listRevisi");
        }
        // $query =
        // "SELECT *
        // FROM " . sfConfig::get('app_default_schema') . ".buka_rekening_komponen
        // WHERE unit_id = '$unit_id'";
        // $con = Propel::getConnection();
        // $stmt = $con->prepareStatement($query);
        // $rs = $stmt->executeQuery();
        // if($rs->next()) $jmlpd = $rs->getString('unit_id');
        // if($jmlpd => 1)
        // {
        //     $this->setFlash('gagal', 'Buka Rekening Bangunan Unit ID '. $unit_id . ' Sudah Pernah dibuka');
        //     return $this->redirect("kegiatan/listRevisi"); 
        // }
        $new_rek = new BukaRekeningBangunan();
        $new_rek->setUnitId($unit_id);
        $new_rek->save();

        $this->setFlash('berhasil', 'Request Buka Rekening Bangunan Unit ID '. $unit_id . ' Berhasil');
        return $this->redirect("kegiatan/listRevisi");
    }

    public function executeBatalRevisiKomponen() {
        $unit_id = $this->getRequestParameter('unit_id');
        $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
        $detail_no = $this->getRequestParameter('detail_no');

        $con = Propel::getConnection();
        $con->begin();

        $tahap = DinasMasterKegiatanPeer::getTahapKegiatan($unit_id, $kode_kegiatan);

        if($tahap == 10){
            $c->add(PakBukuPutihRincianDetailPeer::UNIT_ID, $unit_id);
            $c->add(PakBukuPutihRincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
            $c->add(PakBukuPutihRincianDetailPeer::DETAIL_NO, $detail_no);
            $rs_drd = PakBukuPutihRincianDetailPeer::doSelectOne($c);
        }
        else{
            $c->add(RincianDetailPeer::UNIT_ID, $unit_id);
            $c->add(RincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
            $c->add(RincianDetailPeer::DETAIL_NO, $detail_no);
            $rs_drd = RincianDetailPeer::doSelectOne($c);
        }

        if ($rs_drd) {
            $c = new Criteria();
            $c->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
            $c->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
            $c->add(DinasRincianDetailPeer::DETAIL_NO, $detail_no);
            if ($rs_rd = DinasRincianDetailPeer::doSelectOne($c)) {

                //cek untuk mengisi status_komponen_baru
                $status_komponen_berubah = FALSE;
                $status_komponen_baru = FALSE;
                $tahap_cek = DinasMasterKegiatanPeer::getTahapKegiatan($unit_id, $kode_kegiatan);
                $c_pembanding_kegiatan = new Criteria();
                $c_pembanding_kegiatan->add(PembandingKegiatanPeer::UNIT_ID, $unit_id);
                $c_pembanding_kegiatan->add(PembandingKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                $c_pembanding_kegiatan->add(PembandingKegiatanPeer::TAHAP, $tahap_cek);
                $c_pembanding_kegiatan->addDescendingOrderByColumn(PembandingKegiatanPeer::ID);
                if ($rs_pembanding_kegiatan = PembandingKegiatanPeer::doSelectOne($c_pembanding_kegiatan)) {
                    $id_pembanding = $rs_pembanding_kegiatan->getId();
                    $c_pembanding_komponen = new Criteria();
                    $c_pembanding_komponen->add(PembandingKomponenPeer::ID_PEMBANDING_KEGIATAN, $id_pembanding);
                    $c_pembanding_komponen->add(PembandingKomponenPeer::DETAIL_NO, $detail_no);
                    if ($rs_pembanding_komponen = PembandingKomponenPeer::doSelectOne($c_pembanding_komponen)) {
                        $pembanding_rekening_code = $rs_pembanding_komponen->getRekeningCode();
                        $pembanding_komponen_name = $rs_pembanding_komponen->getKomponenName();
                        $pembanding_satuan = $rs_pembanding_komponen->getSatuan();
                        $pembanding_subtitle = $rs_pembanding_komponen->getSubtitle();
                        $pembanding_detail_name = $rs_pembanding_komponen->getDetailName();
                        $pembanding_volume = $rs_pembanding_komponen->getVolume();
                        $pembanding_keterangan_koefisien = $rs_pembanding_komponen->getKeteranganKoefisien();

                        if ($rs_drd->getSubtitle() <> $pembanding_subtitle || $rs_drd->getDetailName() <> $pembanding_detail_name ||
                                $rs_drd->getVolume() <> $pembanding_volume || $rs_drd->getKeteranganKoefisien() <> $pembanding_keterangan_koefisien ||
                                $rs_drd->getRekeningCode() <> $pembanding_rekening_code || $rs_drd->getKomponenName() <> $pembanding_komponen_name ||
                                $rs_drd->getSatuan() <> $pembanding_satuan) {
                            $level_tolak = $rs_drd->getStatusLevelTolak();
                            if ($level_tolak >= 4) {
                                $status_komponen_berubah = TRUE;
                            } else {
                                $status_komponen_baru = TRUE;
                            }
                        }
                    } else {
                        $status_komponen_baru = TRUE;
                    }
                }
                //cek untuk mengisi status_komponen_baru

                $rd_function = new DinasRincianDetail();
                //cek pagu
                $status_pagu_rincian = 0;
                $array_buka_pagu_dinas_khusus = array('');
                $array_buka_pagu_kegiatan_khusus = array('');
                if (in_array($unit_id, $array_buka_pagu_dinas_khusus)) {
                    $status_pagu_rincian = $rd_function->getBatasPaguPerDinasforKembalikan($unit_id, $kode_kegiatan, $detail_no);
                } else if (in_array($unit_id, $array_buka_pagu_kegiatan_khusus)) {
                    $status_pagu_rincian = $rd_function->getBatasPaguPerKegiatanforKembalikan($unit_id, $kode_kegiatan, $detail_no);
                } else {
                    if (sfConfig::get('app_fasilitas_batasPaguDinas') == 'buka') {
                        if (sfConfig::get('app_fasilitas_paguDinasBerdasarDinas') == 'buka') {
                            $status_pagu_rincian = $rd_function->getBatasPaguPerDinasforKembalikan($unit_id, $kode_kegiatan, $detail_no);
                        } else if (sfConfig::get('app_fasilitas_paguDinasBerdasarKegiatan') == 'buka') {
                            $status_pagu_rincian = $rd_function->getBatasPaguPerKegiatanforKembalikan($unit_id, $kode_kegiatan, $detail_no);
                        }
                    } else if (sfConfig::get('app_fasilitas_batasPaguDinas') == 'tutup') {
                        $status_pagu_rincian = 0;
                    }
                }
                if ($status_pagu_rincian == '1') {
                    $con->rollback();
                    $this->setFlash('gagal', 'Komponen tidak berhasil dikembalikan karena nilai total RKA Melebihi total Pagu.');
                    return $this->redirect("kegiatan/editRevisi?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                }
                //cek realisasi dkk
                $volume = $rs_drd->getVolume();
                $pajak = $rs_drd->getPajak();
                $harga = $rs_drd->getKomponenHargaAwal();

                //$nilaiBaru = round($harga * $volume * (100 + $pajak) / 100);
                $nilaiBaru = $rs_drd->getNilaiAnggaran();
                if (sfConfig::get('app_fasilitas_cekeProject') == 'buka') {
                    if (sfConfig::get('app_fasilitas_cekServer') == 'buka') {
                        $lelang = $rd_function->getCekLelang($unit_id, $kode_kegiatan, $detail_no, $rs_drd->getNilaiAnggaran());
                        if (sfConfig::get('app_fasilitas_cekeDelivery') == 'buka') {
                            $totNilaiSwakelola = $rd_function->getCekNilaiSwakelolaDelivery2($unit_id, $kode_kegiatan, $detail_no);
                            $totNilaiKontrak = $rd_function->getCekNilaiKontrakDelivery2($unit_id, $kode_kegiatan, $detail_no);
                            $totNilaiRealisasi = $rd_function->getCekRealisasi($unit_id, $kode_kegiatan, $detail_no);
                            $totVolumeRealisasi = $rd_function->getCekVolumeRealisasi($unit_id, $kode_kegiatan, $detail_no);

                            $totNilaiHps = $rd_function->getCekNilaiHPSKomponen($unit_id, $kode_kegiatan, $detail_no);
                            $ceklelangselesaitidakaturanpembayaran = $rd_function->getCekLelangTidakAdaAturanPembayaran($unit_id, $kode_kegiatan, $detail_no);
                            $totNilaiKontrakTidakAdaAturanPembayaran = $rd_function->getCekNilaiDeliveryBelumAdaAturanPembayaran2($unit_id, $kode_kegiatan, $detail_no);
                        }
                    }
                }
                if (($nilaiBaru < $totNilaiRealisasi) || ($nilaiBaru < $totNilaiSwakelola)) {
                    if ($totNilaiKontrak == 0) {
                        $this->setFlash('gagal', 'Mohon maaf , untuk komponen ini sudah terpakai di edelivery, sejumlah Rp.' . number_format($totNilaiSwakelola, 0, ',', '.'));
                    } else if ($totNilaiSwakelola == 0) {
                        $this->setFlash('gagal', 'Mohon maaf , untuk komponen ini sudah terpakai di edelivery, sejumlah Rp.' . number_format($totNilaiRealisasi, 0, ',', '.'));
                    } else {
                        $this->setFlash('gagal', 'Mohon maaf , untuk komponen ini sudah terpakai di edelivery, sejumlah Rp.' . number_format($totNilaiRealisasi, 0, ',', '.'));
                    }
                    $con->rollback();
                    //return $this->redirect("kegiatan/editRevisi?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                } else if ($nilaiBaru < $totNilaiHps) {
                    $this->setFlash('gagal', 'Mohon maaf , lebih kecil dari Nilai HPS Per Komponen , sejumlah Rp.' . number_format($totNilaiHps, 0, ',', '.'));
                    $con->rollback();
                    //return $this->redirect("kegiatan/editRevisi?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                } else if ($ceklelangselesaitidakaturanpembayaran == 1) {
                    $this->setFlash('gagal', 'Proses Lelang untuk komponen ini telah selesai, namun Belum ada isian Aturan Pembayaran eDelivery. Silahkan mengisi Aturan Pembayaran terlebih dahulu ');
                    $con->rollback();
                    //return $this->redirect("kegiatan/editRevisi?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                } else if ($lelang > 0) {
                    $this->setFlash('gagal', 'Sedang dalam Proses Lelang untuk komponen ini');
                    $con->rollback();
                    //return $this->redirect("kegiatan/editRevisi?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                } else if ($totNilaiKontrakTidakAdaAturanPembayaran == 1) {
                    $this->setFlash('gagal', 'Komponen ini belum ada isian aturan pembayaran di eDelivery. Silahkan mengisi Aturan Pembayaran terlebih dahulu.');
                    $con->rollback();
                    //return $this->redirect("kegiatan/editRevisi?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                } else if ($nilaiBaru < $totNilaiRealisasi) {
                    $this->setFlash('gagal', 'Mohon maaf , untuk komponen ini sudah terpakai di edelivery, sejumlah Rp.' . number_format($totNilaiRealisasi, 0, ',', '.'));
                    $con->rollback();
                    //return $this->redirect("kegiatan/editRevisi?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                } else if ($volume < $totVolumeRealisasi && $rs_rd->getStatusLelang() != 'lock') {
                    $this->setFlash('gagal', 'Mohon maaf , untuk komponen ini melebihi volume realisasi di edelivery, sejumlah : ' . number_format($totVolumeRealisasi, 0, ',', '.'));
                    $con->rollback();
                    //return $this->redirect("kegiatan/editRevisi?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                } else {
                    $rs_rd->setTipe($rs_drd->getTipe());
                    $rs_rd->setRekeningCode($rs_drd->getRekeningCode());
                    $rs_rd->setKomponenId($rs_drd->getKomponenId());
                    $rs_rd->setDetailName($rs_drd->getDetailName());
                    $rs_rd->setVolume($rs_drd->getVolume());
                    $rs_rd->setKeteranganKoefisien($rs_drd->getKeteranganKoefisien());
                    $rs_rd->setSubtitle($rs_drd->getSubtitle());
                    $rs_rd->setKomponenHarga($rs_drd->getKomponenHarga());
                    $rs_rd->setKomponenHargaAwal($rs_drd->getKomponenHargaAwal());
                    $rs_rd->setKomponenName($rs_drd->getKomponenName());
                    $rs_rd->setSatuan($rs_drd->getSatuan());
                    $rs_rd->setPajak($rs_drd->getPajak());
                    $rs_rd->setFromSubKegiatan($rs_drd->getFromSubKegiatan());
                    $rs_rd->setSub($rs_drd->getSub());
                    $rs_rd->setKodeSub($rs_drd->getKodeSub());
                    $rs_rd->setLastUpdateUser($rs_drd->getLastUpdateUser());
                    $rs_rd->setLastUpdateTime($rs_drd->getLastUpdateTime());
                    $rs_rd->setLastUpdateIp($rs_drd->getLastUpdateIp());
                    $rs_rd->setTahap($rs_drd->getTahap());
                    $rs_rd->setTahapEdit($rs_drd->getTahapEdit());
                    $rs_rd->setTahapNew($rs_drd->getTahapNew());
                    $rs_rd->setStatusLelang($rs_drd->getStatusLelang());
                    $rs_rd->setNomorLelang($rs_drd->getNomorLelang());
                    $rs_rd->setKoefisienSemula($rs_drd->getKoefisienSemula());
                    $rs_rd->setVolumeSemula($rs_drd->getVolumeSemula());
                    $rs_rd->setHargaSemula($rs_drd->getHargaSemula());
                    $rs_rd->setTotalSemula($rs_drd->getTotalSemula());
                    $rs_rd->setLockSubtitle($rs_drd->getLockSubtitle());
                    $rs_rd->setStatusHapus($rs_drd->getStatusHapus());
                    $rs_rd->setTahun($rs_drd->getTahun());
                    $rs_rd->setKodeLokasi($rs_drd->getKodeLokasi());
                    $rs_rd->setKecamatan($rs_drd->getKecamatan());
                    $rs_rd->setRekeningCodeAsli($rs_drd->getRekeningCodeAsli());
                    $rs_rd->setNilaiAnggaran($rs_drd->getNilaiAnggaran());
                    $rs_rd->setNoteSkpd($rs_drd->getNoteSkpd());
                    $rs_rd->setNotePeneliti($rs_drd->getNotePeneliti());
                    $rs_rd->setIsBlud($rs_drd->getIsBlud());
                    $rs_rd->setOb($rs_drd->getOb());
                    $rs_rd->setObFromId($rs_drd->getObFromId());
                    $rs_rd->setIsPerKomponen($rs_drd->getIsPerKomponen());
                    $rs_rd->setKegiatanCodeAsal($rs_drd->getKegiatanCodeAsal());
                    $rs_rd->setThKeMultiyears($rs_drd->getThKeMultiyears());
                    $rs_rd->setLokasiKecamatan($rs_drd->getLokasiKecamatan());
                    $rs_rd->setLokasiKelurahan($rs_drd->getLokasiKelurahan());
                    $rs_rd->setHargaSebelumSisaLelang($rs_drd->getHargaSebelumSisaLelang());
                    $rs_rd->setIsMusrenbang($rs_drd->getIsMusrenbang());
                    $rs_rd->setSubIdAsal($rs_drd->getSubIdAsal());
                    $rs_rd->setSubtitleAsal($rs_drd->getSubtitleAsal());
                    $rs_rd->setKodeSubAsal($rs_drd->getKodeSubAsal());
                    $rs_rd->setSubAsal($rs_drd->getSubAsal());
                    $rs_rd->setLastEditTime($rs_drd->getLastEditTime());
                    $rs_rd->setIsPotongBpjs($rs_drd->getIsPotongBpjs());
                    $rs_rd->setIsIuranBpjs($rs_drd->getIsIuranBpjs());
                    $rs_rd->setStatusOb($rs_drd->getStatusOb());
                    $rs_rd->setObParent($rs_drd->getObParent());
                    $rs_rd->setObAlokasiBaru($rs_drd->getObAlokasiBaru());
                    $rs_rd->setIsHibah($rs_drd->getIsHibah());
                    $rs_rd->setStatusLevel(0);
                    $rs_rd->setStatusSisipan(false);
                    $rs_rd->setStatusLevelTolak(0);
                    $rs_rd->setIsTapdSetuju(false);
                    $rs_rd->setIsBappekoSetuju(false);
                    $rs_rd->setIsBagianHukumSetuju(false);
                    $rs_rd->setIsInspektoratSetuju(false);
                    $rs_rd->setIsBadanKepegawaianSetuju(false);
                    $rs_rd->setIsLppaSetuju(false);
                    $rs_rd->setIsBagianOrganisasiSetuju(false);
                    $rs_rd->setAkrualCode($rs_drd->getAkrualCode());
                    $rs_rd->setTipe2($rs_drd->getTipe2());
                    $rs_rd->setIsPenyeliaSetuju(false);
                    $rs_rd->setStatusKomponenBerubah($status_komponen_berubah);
                    $rs_rd->setStatusKomponenBaru($status_komponen_baru);
                    $rs_rd->save();
                    $con->commit();
                    $this->setFlash('berhasil', 'Komponen telah dikembalikan ke semula');
                }
            }
        } else {
            $nilaiBaru = 0;
            $volume = 0;
            if (sfConfig::get('app_fasilitas_cekeProject') == 'buka') {
                if (sfConfig::get('app_fasilitas_cekServer') == 'buka') {
                    $lelang = $rd_function->getCekLelang($unit_id, $kode_kegiatan, $detail_no, 0);
                    if (sfConfig::get('app_fasilitas_cekeDelivery') == 'buka') {
                        $totNilaiSwakelola = $rd_function->getCekNilaiSwakelolaDelivery2($unit_id, $kode_kegiatan, $detail_no);
                        $totNilaiKontrak = $rd_function->getCekNilaiKontrakDelivery2($unit_id, $kode_kegiatan, $detail_no);
                        $totNilaiRealisasi = $rd_function->getCekRealisasi($unit_id, $kode_kegiatan, $detail_no);
                        $totVolumeRealisasi = $rd_function->getCekVolumeRealisasi($unit_id, $kode_kegiatan, $detail_no);

                        $totNilaiHps = $rd_function->getCekNilaiHPSKomponen($unit_id, $kode_kegiatan, $detail_no);
                        $ceklelangselesaitidakaturanpembayaran = $rd_function->getCekLelangTidakAdaAturanPembayaran($unit_id, $kode_kegiatan, $detail_no);
                        $totNilaiKontrakTidakAdaAturanPembayaran = $rd_function->getCekNilaiDeliveryBelumAdaAturanPembayaran2($unit_id, $kode_kegiatan, $detail_no);
                    }
                }
            }
            if (($nilaiBaru < $totNilaiRealisasi) || ($nilaiBaru < $totNilaiSwakelola)) {
                if ($totNilaiKontrak == 0) {
                    $this->setFlash('gagal', 'Mohon maaf , untuk komponen ini sudah terpakai di edelivery, sejumlah Rp.' . number_format($totNilaiSwakelola, 0, ',', '.'));
                } else if ($totNilaiSwakelola == 0) {
                    $this->setFlash('gagal', 'Mohon maaf , untuk komponen ini sudah terpakai di edelivery, sejumlah Rp.' . number_format($totNilaiRealisasi, 0, ',', '.'));
                } else {
                    $this->setFlash('gagal', 'Mohon maaf , untuk komponen ini sudah terpakai di edelivery, sejumlah Rp.' . number_format($totNilaiRealisasi, 0, ',', '.'));
                }
                $con->rollback();
                //return $this->redirect("kegiatan/editRevisi?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
            } else if ($nilaiBaru < $totNilaiHps) {
                $this->setFlash('gagal', 'Mohon maaf , lebih kecil dari Nilai HPS Per Komponen , sejumlah Rp.' . number_format($totNilaiHps, 0, ',', '.'));
                $con->rollback();
                //return $this->redirect("kegiatan/editRevisi?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
            } else if ($ceklelangselesaitidakaturanpembayaran == 1) {
                $this->setFlash('gagal', 'Proses Lelang untuk komponen ini telah selesai, namun Belum ada isian Aturan Pembayaran eDelivery. Silahkan mengisi Aturan Pembayaran terlebih dahulu ');
                $con->rollback();
                //return $this->redirect("kegiatan/editRevisi?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
            } else if ($lelang > 0) {
                $this->setFlash('gagal', 'Sedang dalam Proses Lelang untuk komponen ini');
                $con->rollback();
                //return $this->redirect("kegiatan/editRevisi?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
            } else if ($totNilaiKontrakTidakAdaAturanPembayaran == 1) {
                $this->setFlash('gagal', 'Komponen ini belum ada isian aturan pembayaran di eDelivery. Silahkan mengisi Aturan Pembayaran terlebih dahulu.');
                $con->rollback();
                //return $this->redirect("kegiatan/editRevisi?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
            } else if ($nilaiBaru < $totNilaiRealisasi) {
                $this->setFlash('gagal', 'Mohon maaf , untuk komponen ini sudah terpakai di edelivery, sejumlah Rp.' . number_format($totNilaiRealisasi, 0, ',', '.'));
                $con->rollback();
                //return $this->redirect("kegiatan/editRevisi?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
            } else if ($volume < $totVolumeRealisasi && $rs_rd->getStatusLelang() != 'lock') {
                $this->setFlash('gagal', 'Mohon maaf , untuk komponen ini melebihi volume realisasi di edelivery, sejumlah : ' . number_format($totVolumeRealisasi, 0, ',', '.'));
                $con->rollback();
                //return $this->redirect("kegiatan/editRevisi?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
            } else {
                $c = new Criteria();
                $c->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
                $c->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
                $c->add(DinasRincianDetailPeer::DETAIL_NO, $detail_no);
                if ($rs_rd = DinasRincianDetailPeer::doSelectOne($c)) {
                    $rs_rd->setStatusHapus(TRUE);
                    $rs_rd->save();
                }
                $con->commit();
                $this->setFlash('berhasil', 'Komponen telah dikembalikan ke semula');
            }
        }
        return $this->redirect("kegiatan/editRevisi?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
    }

    public function executeSetTolak() {
        $this->id = $this->getRequestParameter('id');
        $this->request = $this->getRequestParameter('request');
    }

    public function executeSetTolakRequest() {
        $id = $this->getRequestParameter('id');
        $request = $this->getRequestParameter('request');
        $catatan = $this->getRequestParameter('catatan');
        if($request == 'peneliti')
            $query = "update " . sfConfig::get('app_default_schema') . ".log_request_penyelia set status=2, catatan_tolak='$catatan', updated_at='" . date('Y-m-d H:i:s') . "' where id='$id'";
        else
            $query = "update " . sfConfig::get('app_default_schema') . ".log_request_dinas set status=2, catatan_tolak='$catatan', updated_at='" . date('Y-m-d H:i:s') . "' where id='$id'";

        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query);
        $stmt->executeQuery();
        $this->setFlash('berhasil', 'Request Telah Ditolak ');
        if($request == 'peneliti')
            return $this->redirect('kegiatan/requestlist');
        else
            return $this->redirect('kegiatan/requestDinasList');
    }

    public function executeTerimaRequest() {
        $id = $this->getRequestParameter('id');

        //khusus terima request buka
        $c = new Criteria();
        $c->add(LogRequestPenyeliaPeer::ID, $id);
        $log = LogRequestPenyeliaPeer::doSelectOne($c);
        if ($log->getTipe() == 0) {
            $unit_id = $log->getUnitId();
            $arr_kode_kegiatan = explode('|', $log->getKegiatanCode());
            foreach ($arr_kode_kegiatan as $kode_kegiatan) {
                $sql = "update " . sfConfig::get('app_default_schema') . ".dinas_rincian 
                set lock=FALSE, rincian_confirmed=FALSE, rincian_selesai=FALSE
                where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and tahun='" . sfConfig::get('app_tahun_default') . "'";
                $con = Propel::getConnection();
                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();
            }
        }

        $query = "update " . sfConfig::get('app_default_schema') . ".log_request_penyelia set status=1, updated_at='" . date('Y-m-d H:i:s') . "' where id='$id'";
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query);
        $stmt->executeQuery();
        $this->setFlash('berhasil', 'Request Telah Diterima ');
        return $this->redirect('kegiatan/requestlist');
    }

    public function executeTerimaRequestDinas() {
        $id = $this->getRequestParameter('id');

        //khusus terima request buka
        // $c = new Criteria();
        // $c->add(LogRequestDinasPeer::ID, $id);
        // $log = LogRequestDinasPeer::doSelectOne($c);
        // if ($log->getTipe() == 0) {
        //     $unit_id = $log->getUnitId();
        //     $arr_kode_kegiatan = explode('|', $log->getKegiatanCode());
        //     foreach ($arr_kode_kegiatan as $kode_kegiatan) {
        //         $sql = "update " . sfConfig::get('app_default_schema') . ".dinas_rincian 
        //         set lock=FALSE, rincian_confirmed=FALSE, rincian_selesai=FALSE, rincian_level=3 
        //         where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and tahun='" . sfConfig::get('app_tahun_default') . "'";
        //         $con = Propel::getConnection();
        //         $stmt = $con->prepareStatement($sql);
        //         $stmt->executeQuery();
        //     }
        // }

        $query = "update " . sfConfig::get('app_default_schema') . ".log_request_dinas set status=1, updated_at='" . date('Y-m-d H:i:s') . "' where id='$id'";
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query);
        $stmt->executeQuery();
        $this->setFlash('berhasil', 'Request Telah Diterima ');
        return $this->redirect('kegiatan/RequestDinasList');
    }

    public function executeHapusRequest() {
        $id = $this->getRequestParameter('id');
        $query = "update " . sfConfig::get('app_default_schema') . ".log_request_penyelia set status=4, updated_at='" . date('Y-m-d H:i:s') . "' where id='$id'";
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query);
        $stmt->executeQuery();
        $this->setFlash('berhasil', 'Request Telah Dihapus');
        return $this->redirect('kegiatan/requestlist');
    }

    public function executeHapusRequestDinas() {
        $id = $this->getRequestParameter('id');
        $query = "update " . sfConfig::get('app_default_schema') . ".log_request_dinas set status=4, updated_at='" . date('Y-m-d H:i:s') . "' where id='$id'";
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query);
        $stmt->executeQuery();
        $this->setFlash('berhasil', 'Request Telah Dihapus');
        return $this->redirect('kegiatan/RequestDinasList');
    }

    public function executeRequestlist() {
        $this->processSort();
        $this->processFilters();
        $this->filters = $this->getUser()->getAttributeHolder()->getAll('sf_admin/master_kegiatan/filters');
        $is_dinas = false;
        $pagers = new sfPropelPager('LogRequestPenyelia', 20);
        $c = new Criteria();
        $this->addSortCriteria($c);
        $c->addAscendingOrderByColumn(LogRequestPenyeliaPeer::STATUS);
        $c->addDescendingOrderByColumn(LogRequestPenyeliaPeer::UPDATED_AT);
        $c->add(LogRequestPenyeliaPeer::STATUS, 4, Criteria::NOT_EQUAL);
        $this->addFiltersCriteriaRequestlist($c, $is_dinas);

        $pagers->setCriteria($c);
        $pagers->setPage($this->getRequestParameter('page', 1));
        $pagers->init();

        $this->pager = $pagers;
    }

    public function executeRequestDinasList() {
        $this->processSort();
        $this->processFilters();
        $this->filters = $this->getUser()->getAttributeHolder()->getAll('sf_admin/master_kegiatan/filters');
        $is_dinas = true;
        $pagers = new sfPropelPager('LogRequestDinas', 20);
        $c = new Criteria();
        $this->addSortCriteria($c);
        $c->addAscendingOrderByColumn(LogRequestDinasPeer::STATUS);
        $c->addDescendingOrderByColumn(LogRequestDinasPeer::UPDATED_AT);
        $c->add(LogRequestDinasPeer::STATUS, 4, Criteria::NOT_EQUAL);
        $this->addFiltersCriteriaRequestlist($c, $is_dinas);

        $pagers->setCriteria($c);
        $pagers->setPage($this->getRequestParameter('page', 1));
        $pagers->init();

        $this->pager = $pagers;
    }

    protected function addFiltersCriteriaRequestlist($c, $is_dinas) {
        if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
            $unit = $this->filters['unit_id'];
            if($is_dinas)
                $c->add(LogRequestDinasPeer::UNIT_ID, $unit);
            else
                $c->add(LogRequestPenyeliaPeer::UNIT_ID, $unit);
        }
        if (isset($this->filters['tipe']) && $this->filters['tipe'] !== '') {
            if($is_dinas)
                $c->add(LogRequestDinasPeer::TIPE, $this->filters['tipe']);
            else
                $c->add(LogRequestPenyeliaPeer::TIPE, $this->filters['tipe']);
        }
    }

    public function executeKonversilist() {
        $this->processSort();
        $this->processFilters();
        $this->filters = $this->getUser()->getAttributeHolder()->getAll('sf_admin/master_kegiatan/filters');

        $pagers = new sfPropelPager('KonversiSubtitle', 20);
        $c = new Criteria();
        $this->addSortCriteria($c);
        $c->add(KonversiSubtitlePeer::UNIT_ID_LAMA, '9999', Criteria::NOT_EQUAL);
        //$c->add(KonversiSubtitlePeer::MASUK, TRUE, Criteria::NOT_EQUAL);
        //$c->addOr(KonversiSubtitlePeer::MASUK, NULL);
        $c->addAscendingOrderByColumn(KonversiSubtitlePeer::MASUK);
        $c->addAscendingOrderByColumn(KonversiSubtitlePeer::UNIT_ID_LAMA);
        $c->addAscendingOrderByColumn(KonversiSubtitlePeer::KEGIATAN_CODE_LAMA);
        $this->addFiltersCriteriaKonversilist($c);

        $pagers->setCriteria($c);
        $pagers->setPage($this->getRequestParameter('page', 1));
        $pagers->init();

        $this->pager = $pagers;
    }

    protected function addFiltersCriteriaKonversilist($c) {
        if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
            $unit = $this->filters['unit_id'];
            $c->add(KonversiSubtitlePeer::UNIT_ID_LAMA, $unit);
        }
        if (isset($this->filters['kegiatan']) && $this->filters['kegiatan'] !== '') {
            $c->add(KonversiSubtitlePeer::KEGIATAN_CODE_LAMA, $this->filters['kegiatan']);
        }
    }

    public function executeBuatKonversiEdit() {
        $c = new Criteria();
        $c->add(UnitKerjaPeer::UNIT_ID, '9999', Criteria::NOT_EQUAL);
        $c->addAscendingOrderByColumn(UnitKerjaPeer::UNIT_NAME);
        $this->unit_kerja = UnitKerjaPeer::doSelect($c);
    }

    public function executeBuatKonversi() {
        $unit_id_lama = $this->getRequestParameter('unit_id_lama');
        $kode_kegiatan_lama = $this->getRequestParameter('kode_kegiatan_lama');
        $subtitle_lama = $this->getRequestParameter('subtitle_lama');
        $unit_id_baru = $this->getRequestParameter('unit_id_baru');
        $kode_kegiatan_baru = $this->getRequestParameter('kode_kegiatan_baru');
        $subtitle_baru = $this->getRequestParameter('subtitle_baru');

        foreach ($subtitle_lama as $value) {
            $new_upload = new KonversiSubtitle();
            $new_upload->setUnitIdLama($unit_id_lama);
            $new_upload->setKegiatanCodeLama($kode_kegiatan_lama);
            $new_upload->setSubtitleLama($value);
            $new_upload->setUnitIdBaru($unit_id_baru);
            $new_upload->setKegiatanCodeBaru($kode_kegiatan_baru);
            $new_upload->setSubtitleBaru($subtitle_baru);
            $new_upload->setMasuk(FALSE);
            $new_upload->save();
        }

        $this->setFlash('berhasil', 'Telah berhasil upload ');
        return $this->redirect('kegiatan/buatKonversiEdit');
    }

    public function executeHapusKonversi() {
        $unit_id = $this->getRequestParameter('unit_id');
        $kegiatan_code = $this->getRequestParameter('kegiatan_code');
        $subtitle = $this->getRequestParameter('subtitle');

        $con = Propel::getConnection();
        $query = "delete from ebudget.konversi_subtitle
            where unit_id_lama='$unit_id' and kegiatan_code_lama='$kegiatan_code' and subtitle_lama='$subtitle'";
        $stmt = $con->prepareStatement($query);
        $stmt->executeQuery();
        $this->setFlash('berhasil', 'Konversi Telah Dihapus');
        return $this->redirect('kegiatan/konversilist');
    }

    public function executePilihKegiatan() {
        $unit_id = $this->getRequestParameter('id');
        $c = new Criteria();
        $c->add(RkuaMasterKegiatanPeer::UNIT_ID, $unit_id);
        $c->addAscendingOrderByColumn(RkuaMasterKegiatanPeer::KODE_KEGIATAN);
        $this->master_kegiatan = RkuaMasterKegiatanPeer::doSelect($c);
    }

    public function executePilihKegiatan2() {
        $con = Propel::getConnection();
        $unit_id = $this->getRequestParameter('id');
        $query = "select distinct kegiatan_code, nama_kegiatan from ebudget.baru
            where unit_id='$unit_id'";
        $stmt = $con->prepareStatement($query);
        $rs = $stmt->executeQuery();
        $this->master_kegiatan = $rs;
    }

    public function executePilihSubtitle() {
        $con = Propel::getConnection();
        $unit_id = $this->getRequestParameter('unit_id');
        $kegiatan_code = $this->getRequestParameter('kegiatan_code');
//        $c = new Criteria();
//        $c->add(DinasSubtitleIndikatorPeer::UNIT_ID, $unit_id);
//        $c->add(DinasSubtitleIndikatorPeer::KEGIATAN_CODE, $kegiatan_code);
//        $c->addAscendingOrderByColumn(DinasSubtitleIndikatorPeer::SUBTITLE);
//        $this->subtitle = DinasSubtitleIndikatorPeer::doSelect($c);
        $subtitle = array();
        $query = "select subtitle from ebudget.rkua_subtitle_indikator
            where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and subtitle not in (
            select subtitle_lama from ebudget.konversi_subtitle
            where unit_id_lama='$unit_id' and kegiatan_code_lama='$kegiatan_code')";
        $stmt = $con->prepareStatement($query);
        $rs = $stmt->executeQuery();
        $this->subtitle = $rs;
    }

    public function executePilihSubtitle2() {
        $con = Propel::getConnection();
        $unit_id = $this->getRequestParameter('unit_id');
        $kegiatan_code = $this->getRequestParameter('kegiatan_code');
        $subtitle = array();
        $query = "select subtitle from ebudget.baru
            where unit_id='$unit_id' and kegiatan_code='$kegiatan_code'";
        $stmt = $con->prepareStatement($query);
        $rs = $stmt->executeQuery();
        $this->subtitle = $rs;
    }

    public function executeListRevisi() {
        $menu = $this->getRequestParameter('menu');
        //echo $menu;
        $this->menu = $menu;
        $this->processSort();
        $this->processFilters();
        $this->filters = $this->getUser()->getAttributeHolder()->getAll('sf_admin/master_kegiatan/filters');
        //print_r($this->filters);die();        

        if ($this->getRequestParameter('kunci') == 'kunci') {
            $this->prosesKunciRevisi();
        }
        if ($this->getRequestParameter('buka') == 'buka kunci') {
            $this->prosesBukakunciRevisi();
        }

        if (isset($this->filters['tahap']) && $this->filters['tahap'] == 'pakbp') {
            $this->pager = new sfPropelPager('PakBukuPutihMasterKegiatan', 20);
            $c = new Criteria();
            $this->addSortCriteria($c);
            $es = UnitKerjaPeer::doSelect(new Criteria);
            $this->satuan_kerja = $es;
            $unit_kerja = Array();
            foreach ($es as $x) {
                $unit_kerja[] = $x->getUnitId();
            }

            $c->add(PakBukuPutihMasterKegiatanPeer::UNIT_ID, $unit_kerja[0], Criteria::ILIKE);
            for ($i = 1; $i < count($unit_kerja); $i++) {
                $c->addOr(PakBukuPutihMasterKegiatanPeer::UNIT_ID, $unit_kerja[$i], Criteria::ILIKE);
            }

            $unit_id = $this->getRequestParameter('unit_id');
            if (isset($unit_id)) {
                $c->add(PakBukuPutihMasterKegiatanPeer::UNIT_ID, $unit_id);
            }
            if ($menu == 'Pendapatan') {
                $c->add(PakBukuPutihMasterKegiatanPeer::IS_BTL,TRUE);
            }else{
                $c->add(PakBukuPutihMasterKegiatanPeer::IS_BTL,FALSE);
            }

            if(isset($this->filters['id']) && $this->filters['id'] != ''){
                $c->add(PakBukuPutihMasterKegiatanPeer::KODE_KEGIATAN, $this->filters['id'], Criteria::EQUAL);
            }
            
            if(isset($this->filters['user_id']) && $this->filters['user_id'] != ''){
                $c->add(PakBukuPutihMasterKegiatanPeer::USER_ID, $this->filters['user_id'], Criteria::EQUAL);
            }

            $load_id = array();
            if(isset($this->filters['unit_id']) && $this->filters['unit_id'] != ''){
                $kode_id = new Criteria();
                $kode_id->add(PakBukuPutihMasterKegiatanPeer::UNIT_ID, $this->filters['unit_id'], Criteria::EQUAL);
                if($menu == 'Pendapatan'){
                    $kode_id->add(PakBukuPutihMasterKegiatanPeer::IS_BTL,TRUE);
                }else{
                    $kode_id->add(PakBukuPutihMasterKegiatanPeer::IS_BTL,FALSE);
                }
                $kode_id->addAscendingOrderByColumn(PakBukuPutihMasterKegiatanPeer::KODE_KEGIATAN);
                $load_id = PakBukuPutihMasterKegiatanPeer::doSelect($kode_id);
            }
            $this->load_id = $load_id;
            
            $load_user_id = array();
            if(isset($this->filters['unit_id']) && $this->filters['unit_id'] != ''){
                $kode_user_id = new Criteria();
                $kode_user_id->add(PakBukuPutihMasterKegiatanPeer::UNIT_ID, $this->filters['unit_id'], Criteria::EQUAL);
                if($menu == 'Pendapatan'){
                    $kode_user_id->add(PakBukuPutihMasterKegiatanPeer::IS_BTL,TRUE);
                }else{
                    $kode_user_id->add(PakBukuPutihMasterKegiatanPeer::IS_BTL,FALSE);
                }
                $kode_user_id->add(PakBukuPutihMasterKegiatanPeer::USER_ID, NULL, Criteria::ISNOTNULL);
                $kode_user_id->setDistinct(PakBukuPutihMasterKegiatanPeer::USER_ID);
                $kode_user_id->addAscendingOrderByColumn(PakBukuPutihMasterKegiatanPeer::USER_ID);
                $load_user_id = PakBukuPutihMasterKegiatanPeer::doSelect($kode_user_id);
            }
            $this->load_user_id = $load_user_id;

            $c->addAscendingOrderByColumn(PakBukuPutihMasterKegiatanPeer::KODE_KEGIATAN);
        } elseif (isset($this->filters['tahap']) && $this->filters['tahap'] == 'pakbb') {
            $this->pager = new sfPropelPager('PakBukuBiruMasterKegiatan', 20);
            $c = new Criteria();
            $this->addSortCriteria($c);
            $es = UnitKerjaPeer::doSelect(new Criteria);
            $this->satuan_kerja = $es;
            $unit_kerja = Array();
            foreach ($es as $x) {
                $unit_kerja[] = $x->getUnitId();
            }

            $c->add(PakBukuBiruMasterKegiatanPeer::UNIT_ID, $unit_kerja[0], Criteria::ILIKE);
            for ($i = 1; $i < count($unit_kerja); $i++) {
                $c->addOr(PakBukuBiruMasterKegiatanPeer::UNIT_ID, $unit_kerja[$i], Criteria::ILIKE);
            }

            $unit_id = $this->getRequestParameter('unit_id');
            if (isset($unit_id)) {
                $c->add(PakBukuBiruMasterKegiatanPeer::UNIT_ID, $unit_id);
            }
            if ($menu == 'Pendapatan') {
                $c->add(PakBukuBiruMasterKegiatanPeer::IS_BTL,TRUE);
            }else{
                $c->add(PakBukuBiruMasterKegiatanPeer::IS_BTL,FALSE);
            }

            if(isset($this->filters['id']) && $this->filters['id'] != ''){
                $c->add(PakBukuBiruMasterKegiatanPeer::KODE_KEGIATAN, $this->filters['id'], Criteria::EQUAL);
            }
            
            if(isset($this->filters['user_id']) && $this->filters['user_id'] != ''){
                $c->add(PakBukuBiruMasterKegiatanPeer::USER_ID, $this->filters['user_id'], Criteria::EQUAL);
            }

            $load_id = array();
            if(isset($this->filters['unit_id']) && $this->filters['unit_id'] != ''){
                $kode_id = new Criteria();
                $kode_id->add(PakBukuBiruMasterKegiatanPeer::UNIT_ID, $this->filters['unit_id'], Criteria::EQUAL);
                if($menu == 'Pendapatan'){
                    $kode_id->add(PakBukuBiruMasterKegiatanPeer::IS_BTL,TRUE);
                }else{
                    $kode_id->add(PakBukuBiruMasterKegiatanPeer::IS_BTL,FALSE);
                }
                $kode_id->addAscendingOrderByColumn(PakBukuBiruMasterKegiatanPeer::KODE_KEGIATAN);
                $load_id = PakBukuBiruMasterKegiatanPeer::doSelect($kode_id);
            }
            $this->load_id = $load_id;
            
            $load_user_id = array();
            if(isset($this->filters['unit_id']) && $this->filters['unit_id'] != ''){
                $kode_user_id = new Criteria();
                $kode_user_id->add(PakBukuBiruMasterKegiatanPeer::UNIT_ID, $this->filters['unit_id'], Criteria::EQUAL);
                if($menu == 'Pendapatan'){
                    $kode_user_id->add(PakBukuBiruMasterKegiatanPeer::IS_BTL,TRUE);
                }else{
                    $kode_user_id->add(PakBukuBiruMasterKegiatanPeer::IS_BTL,FALSE);
                }
                $kode_user_id->add(PakBukuBiruMasterKegiatanPeer::USER_ID, NULL, Criteria::ISNOTNULL);
                $kode_user_id->setDistinct(PakBukuBiruMasterKegiatanPeer::USER_ID);
                $kode_user_id->addAscendingOrderByColumn(PakBukuBiruMasterKegiatanPeer::USER_ID);
                $load_user_id = PakBukuBiruMasterKegiatanPeer::doSelect($kode_user_id);
            }
            $this->load_user_id = $load_user_id;

            $c->addAscendingOrderByColumn(PakBukuBiruMasterKegiatanPeer::KODE_KEGIATAN);
        } elseif (isset($this->filters['tahap']) && $this->filters['tahap'] == 'murni') {
            $this->pager = new sfPropelPager('MurniMasterKegiatan', 20);
            $c = new Criteria();
            $this->addSortCriteria($c);
            $es = UnitKerjaPeer::doSelect(new Criteria);
            $this->satuan_kerja = $es;
            $unit_kerja = Array();
            foreach ($es as $x) {
                $unit_kerja[] = $x->getUnitId();
            }

            $c->add(MurniMasterKegiatanPeer::UNIT_ID, $unit_kerja[0], Criteria::ILIKE);
            for ($i = 1; $i < count($unit_kerja); $i++) {
                $c->addOr(MurniMasterKegiatanPeer::UNIT_ID, $unit_kerja[$i], Criteria::ILIKE);
            }

            $unit_id = $this->getRequestParameter('unit_id');
            if (isset($unit_id)) {
                $c->add(MurniMasterKegiatanPeer::UNIT_ID, $unit_id);
            }
            if ($menu == 'Pendapatan') {
                $c->add(MurniMasterKegiatanPeer::IS_BTL,TRUE);
            }else{
                $c->add(MurniMasterKegiatanPeer::IS_BTL,FALSE);
            }

            if(isset($this->filters['id']) && $this->filters['id'] != ''){
                $c->add(MurniMasterKegiatanPeer::KODE_KEGIATAN, $this->filters['id'], Criteria::EQUAL);
            }
            
            if(isset($this->filters['user_id']) && $this->filters['user_id'] != ''){
                $c->add(MurniMasterKegiatanPeer::USER_ID, $this->filters['user_id'], Criteria::EQUAL);
            }

            $load_id = array();
            if(isset($this->filters['unit_id']) && $this->filters['unit_id'] != ''){
                $kode_id = new Criteria();
                $kode_id->add(MurniMasterKegiatanPeer::UNIT_ID, $this->filters['unit_id'], Criteria::EQUAL);
                if($menu == 'Pendapatan'){
                    $kode_id->add(MurniMasterKegiatanPeer::IS_BTL,TRUE);
                }else{
                    $kode_id->add(MurniMasterKegiatanPeer::IS_BTL,FALSE);
                }
                $kode_id->addAscendingOrderByColumn('CAST('.MurniMasterKegiatanPeer::KODE_KEGIATAN.' AS INT)');
                $load_id = MurniMasterKegiatanPeer::doSelect($kode_id);
            }
            $this->load_id = $load_id;
            
            $load_user_id = array();
            if(isset($this->filters['unit_id']) && $this->filters['unit_id'] != ''){
                $kode_user_id = new Criteria();
                $kode_user_id->add(MurniMasterKegiatanPeer::UNIT_ID, $this->filters['unit_id'], Criteria::EQUAL);
                if($menu == 'Pendapatan'){
                    $kode_user_id->add(MurniMasterKegiatanPeer::IS_BTL,TRUE);
                }else{
                    $kode_user_id->add(MurniMasterKegiatanPeer::IS_BTL,FALSE);
                }
                $kode_user_id->add(MurniMasterKegiatanPeer::USER_ID, NULL, Criteria::ISNOTNULL);
                $kode_user_id->setDistinct(MurniMasterKegiatanPeer::USER_ID);
                $kode_user_id->addAscendingOrderByColumn(MurniMasterKegiatanPeer::USER_ID);
                $load_user_id = MurniMasterKegiatanPeer::doSelect($kode_user_id);
            }
            $this->load_user_id = $load_user_id;

            $c->addAscendingOrderByColumn('CAST('.MurniMasterKegiatanPeer::KODE_KEGIATAN.' AS INT)');
        } elseif (isset($this->filters['tahap']) && $this->filters['tahap'] == 'murnibp') {
            $this->pager = new sfPropelPager('MurniBukuPutihMasterKegiatan', 20);
            $c = new Criteria();
            $this->addSortCriteria($c);
            $es = UnitKerjaPeer::doSelect(new Criteria);
            $this->satuan_kerja = $es;
            $unit_kerja = Array();
            foreach ($es as $x) {
                $unit_kerja[] = $x->getUnitId();
            }

            $c->add(MurniBukuPutihMasterKegiatanPeer::UNIT_ID, $unit_kerja[0], Criteria::ILIKE);
            for ($i = 1; $i < count($unit_kerja); $i++) {
                $c->addOr(MurniBukuPutihMasterKegiatanPeer::UNIT_ID, $unit_kerja[$i], Criteria::ILIKE);
            }

            $unit_id = $this->getRequestParameter('unit_id');
            if (isset($unit_id)) {
                $c->add(MurniBukuPutihMasterKegiatanPeer::UNIT_ID, $unit_id);
            }
            if ($menu == 'Pendapatan') {
                $c->add(MurniBukuPutihMasterKegiatanPeer::IS_BTL,TRUE);
            }else{
                $c->add(MurniBukuPutihMasterKegiatanPeer::IS_BTL,FALSE);
            }

            if(isset($this->filters['id']) && $this->filters['id'] != ''){
                $c->add(MurniBukuPutihMasterKegiatanPeer::KODE_KEGIATAN, $this->filters['id'], Criteria::EQUAL);
            }
            
            if(isset($this->filters['user_id']) && $this->filters['user_id'] != ''){
                $c->add(MurniBukuPutihMasterKegiatanPeer::USER_ID, $this->filters['user_id'], Criteria::EQUAL);
            }

            $load_id = array();
            if(isset($this->filters['unit_id']) && $this->filters['unit_id'] != ''){
                $kode_id = new Criteria();
                $kode_id->add(MurniBukuPutihMasterKegiatanPeer::UNIT_ID, $this->filters['unit_id'], Criteria::EQUAL);
                if($menu == 'Pendapatan'){
                    $kode_id->add(MurniBukuPutihMasterKegiatanPeer::IS_BTL,TRUE);
                }else{
                    $kode_id->add(MurniBukuPutihMasterKegiatanPeer::IS_BTL,FALSE);
                }
                $kode_id->addAscendingOrderByColumn(MurniBukuPutihMasterKegiatanPeer::KODE_KEGIATAN);
                $load_id = MurniBukuPutihMasterKegiatanPeer::doSelect($kode_id);
            }
            $this->load_id = $load_id;
            
            $load_user_id = array();
            if(isset($this->filters['unit_id']) && $this->filters['unit_id'] != ''){
                $kode_user_id = new Criteria();
                $kode_user_id->add(MurniBukuPutihMasterKegiatanPeer::UNIT_ID, $this->filters['unit_id'], Criteria::EQUAL);
                if($menu == 'Pendapatan'){
                    $kode_user_id->add(MurniBukuPutihMasterKegiatanPeer::IS_BTL,TRUE);
                }else{
                    $kode_user_id->add(MurniBukuPutihMasterKegiatanPeer::IS_BTL,FALSE);
                }
                $kode_user_id->add(MurniBukuPutihMasterKegiatanPeer::USER_ID, NULL, Criteria::ISNOTNULL);
                $kode_user_id->setDistinct(MurniBukuPutihMasterKegiatanPeer::USER_ID);
                $kode_user_id->addAscendingOrderByColumn(MurniBukuPutihMasterKegiatanPeer::USER_ID);
                $load_user_id = MurniBukuPutihMasterKegiatanPeer::doSelect($kode_user_id);
            }
            $this->load_user_id = $load_user_id;

            $c->addAscendingOrderByColumn(MurniBukuPutihMasterKegiatanPeer::KODE_KEGIATAN);
        } elseif (isset($this->filters['tahap']) && $this->filters['tahap'] == 'murnibb') {
            $this->pager = new sfPropelPager('MurniBukuBiruMasterKegiatan', 20);
            $c = new Criteria();
            $this->addSortCriteria($c);
            $es = UnitKerjaPeer::doSelect(new Criteria);
            $this->satuan_kerja = $es;
            $unit_kerja = Array();
            foreach ($es as $x) {
                $unit_kerja[] = $x->getUnitId();
            }

            $c->add(MurniBukuBiruMasterKegiatanPeer::UNIT_ID, $unit_kerja[0], Criteria::ILIKE);
            for ($i = 1; $i < count($unit_kerja); $i++) {
                $c->addOr(MurniBukuBiruMasterKegiatanPeer::UNIT_ID, $unit_kerja[$i], Criteria::ILIKE);
            }

            $unit_id = $this->getRequestParameter('unit_id');
            if (isset($unit_id)) {
                $c->add(MurniBukuBiruMasterKegiatanPeer::UNIT_ID, $unit_id);
            }
            // if ($menu == 'Pendapatan') {
            //     $c->add(MurniBukuBiruMasterKegiatanPeer::IS_BTL,TRUE);
            // }else{
            //     $c->add(MurniBukuBiruMasterKegiatanPeer::IS_BTL,FALSE);
            // }

            if(isset($this->filters['id']) && $this->filters['id'] != ''){
                $c->add(MurniBukuBiruMasterKegiatanPeer::KODE_KEGIATAN, $this->filters['id'], Criteria::EQUAL);
            }
            
            if(isset($this->filters['user_id']) && $this->filters['user_id'] != ''){
                $c->add(MurniBukuBiruMasterKegiatanPeer::USER_ID, $this->filters['user_id'], Criteria::EQUAL);
            }

            $load_id = array();
            if(isset($this->filters['unit_id']) && $this->filters['unit_id'] != ''){
                $kode_id = new Criteria();
                $kode_id->add(MurniBukuBiruMasterKegiatanPeer::UNIT_ID, $this->filters['unit_id'], Criteria::EQUAL);
                // if($menu == 'Pendapatan'){
                //     $kode_id->add(MurniBukuBiruMasterKegiatanPeer::IS_BTL,TRUE);
                // }else{
                //     $kode_id->add(MurniBukuBiruMasterKegiatanPeer::IS_BTL,FALSE);
                // }
                $kode_id->addAscendingOrderByColumn(MurniBukuBiruMasterKegiatanPeer::KODE_KEGIATAN);
                $load_id = MurniBukuBiruMasterKegiatanPeer::doSelect($kode_id);
            }
            $this->load_id = $load_id;
            
            $load_user_id = array();
            if(isset($this->filters['unit_id']) && $this->filters['unit_id'] != ''){
                $kode_user_id = new Criteria();
                $kode_user_id->add(MurniBukuBiruMasterKegiatanPeer::UNIT_ID, $this->filters['unit_id'], Criteria::EQUAL);
                // if($menu == 'Pendapatan'){
                //     $kode_user_id->add(MurniBukuBiruMasterKegiatanPeer::IS_BTL,TRUE);
                // }else{
                //     $kode_user_id->add(MurniBukuBiruMasterKegiatanPeer::IS_BTL,FALSE);
                // }
                $kode_user_id->add(MurniBukuBiruMasterKegiatanPeer::USER_ID, NULL, Criteria::ISNOTNULL);
                $kode_user_id->setDistinct(MurniBukuBiruMasterKegiatanPeer::USER_ID);
                $kode_user_id->addAscendingOrderByColumn(MurniBukuBiruMasterKegiatanPeer::USER_ID);
                $load_user_id = MurniBukuBiruMasterKegiatanPeer::doSelect($kode_user_id);
            }
            $this->load_user_id = $load_user_id;

            $c->addAscendingOrderByColumn(MurniBukuBiruMasterKegiatanPeer::KODE_KEGIATAN);
        } elseif (isset($this->filters['tahap']) && $this->filters['tahap'] == 'murnibbpraevagub') {
            $this->pager = new sfPropelPager('MurniBukubiruPraevagubMasterKegiatan', 20);
            $c = new Criteria();
            $this->addSortCriteria($c);
            $es = UnitKerjaPeer::doSelect(new Criteria);
            $this->satuan_kerja = $es;
            $unit_kerja = Array();
            foreach ($es as $x) {
                $unit_kerja[] = $x->getUnitId();
            }

            $c->add(MurniBukubiruPraevagubMasterKegiatanPeer::UNIT_ID, $unit_kerja[0], Criteria::ILIKE);
            for ($i = 1; $i < count($unit_kerja); $i++) {
                $c->addOr(MurniBukubiruPraevagubMasterKegiatanPeer::UNIT_ID, $unit_kerja[$i], Criteria::ILIKE);
            }

            $unit_id = $this->getRequestParameter('unit_id');
            if (isset($unit_id)) {
                $c->add(MurniBukubiruPraevagubMasterKegiatanPeer::UNIT_ID, $unit_id);
            }
            if ($menu == 'Pendapatan') {
                $c->add(MurniBukubiruPraevagubMasterKegiatanPeer::IS_BTL,TRUE);
            }else{
                $c->add(MurniBukubiruPraevagubMasterKegiatanPeer::IS_BTL,FALSE);
            }

            if(isset($this->filters['id']) && $this->filters['id'] != ''){
                $c->add(MurniBukubiruPraevagubMasterKegiatanPeer::KODE_KEGIATAN, $this->filters['id'], Criteria::EQUAL);
            }
            
            if(isset($this->filters['user_id']) && $this->filters['user_id'] != ''){
                $c->add(MurniBukubiruPraevagubMasterKegiatanPeer::USER_ID, $this->filters['user_id'], Criteria::EQUAL);
            }

            $load_id = array();
            if(isset($this->filters['unit_id']) && $this->filters['unit_id'] != ''){
                $kode_id = new Criteria();
                $kode_id->add(MurniBukubiruPraevagubMasterKegiatanPeer::UNIT_ID, $this->filters['unit_id'], Criteria::EQUAL);
                if($menu == 'Pendapatan'){
                    $kode_id->add(MurniBukubiruPraevagubMasterKegiatanPeer::IS_BTL,TRUE);
                }else{
                    $kode_id->add(MurniBukubiruPraevagubMasterKegiatanPeer::IS_BTL,FALSE);
                }
                $kode_id->addAscendingOrderByColumn(MurniBukubiruPraevagubMasterKegiatanPeer::KODE_KEGIATAN);
                $load_id = MurniBukubiruPraevagubMasterKegiatanPeer::doSelect($kode_id);
            }
            $this->load_id = $load_id;
            
            $load_user_id = array();
            if(isset($this->filters['unit_id']) && $this->filters['unit_id'] != ''){
                $kode_user_id = new Criteria();
                $kode_user_id->add(MurniBukubiruPraevagubMasterKegiatanPeer::UNIT_ID, $this->filters['unit_id'], Criteria::EQUAL);
                if($menu == 'Pendapatan'){
                    $kode_user_id->add(MurniBukubiruPraevagubMasterKegiatanPeer::IS_BTL,TRUE);
                }else{
                    $kode_user_id->add(MurniBukubiruPraevagubMasterKegiatanPeer::IS_BTL,FALSE);
                }
                $kode_user_id->add(MurniBukubiruPraevagubMasterKegiatanPeer::USER_ID, NULL, Criteria::ISNOTNULL);
                $kode_user_id->setDistinct(MurniBukubiruPraevagubMasterKegiatanPeer::USER_ID);
                $kode_user_id->addAscendingOrderByColumn(MurniBukubiruPraevagubMasterKegiatanPeer::USER_ID);
                $load_user_id = MurniBukubiruPraevagubMasterKegiatanPeer::doSelect($kode_user_id);
            }
            $this->load_user_id = $load_user_id;

            $c->addAscendingOrderByColumn(MurniBukubiruPraevagubMasterKegiatanPeer::KODE_KEGIATAN);
        } elseif (isset($this->filters['tahap']) && $this->filters['tahap'] == 'revisi1') {
            $this->pager = new sfPropelPager('Revisi1MasterKegiatan', 20);
            $c = new Criteria();
            $this->addSortCriteria($c);
            $es = UnitKerjaPeer::doSelect(new Criteria);
            $this->satuan_kerja = $es;
            $unit_kerja = Array();
            foreach ($es as $x) {
                $unit_kerja[] = $x->getUnitId();
            }

            $c->add(Revisi1MasterKegiatanPeer::UNIT_ID, $unit_kerja[0], Criteria::ILIKE);
            for ($i = 1; $i < count($unit_kerja); $i++) {
                $c->addOr(Revisi1MasterKegiatanPeer::UNIT_ID, $unit_kerja[$i], Criteria::ILIKE);
            }

            $unit_id = $this->getRequestParameter('unit_id');
            if (isset($unit_id)) {
                $c->add(Revisi1MasterKegiatanPeer::UNIT_ID, $unit_id);
            }

            if ($menu == 'Pendapatan') {
                $c->add(Revisi1MasterKegiatanPeer::IS_BTL,TRUE);
            }else{
                $c->add(Revisi1MasterKegiatanPeer::IS_BTL,FALSE);
            }

            if(isset($this->filters['id']) && $this->filters['id'] != ''){
                $c->add(Revisi1MasterKegiatanPeer::KODE_KEGIATAN, $this->filters['id'], Criteria::EQUAL);
            }
            
            if(isset($this->filters['user_id']) && $this->filters['user_id'] != ''){
                $c->add(Revisi1MasterKegiatanPeer::USER_ID, $this->filters['user_id'], Criteria::EQUAL);
            }

            $load_id = array();
            if(isset($this->filters['unit_id']) && $this->filters['unit_id'] != ''){
                $kode_id = new Criteria();
                $kode_id->add(Revisi1MasterKegiatanPeer::UNIT_ID, $this->filters['unit_id'], Criteria::EQUAL);
                if($menu == 'Pendapatan'){
                    $kode_id->add(Revisi1MasterKegiatanPeer::IS_BTL,TRUE);
                }else{
                    $kode_id->add(Revisi1MasterKegiatanPeer::IS_BTL,FALSE);
                }
                $kode_id->addAscendingOrderByColumn(Revisi1MasterKegiatanPeer::KODE_KEGIATAN);
                $load_id = Revisi1MasterKegiatanPeer::doSelect($kode_id);
            }
            $this->load_id = $load_id;
            
            $load_user_id = array();
            if(isset($this->filters['unit_id']) && $this->filters['unit_id'] != ''){
                $kode_user_id = new Criteria();
                $kode_user_id->add(Revisi1MasterKegiatanPeer::UNIT_ID, $this->filters['unit_id'], Criteria::EQUAL);
                if($menu == 'Pendapatan'){
                    $kode_user_id->add(Revisi1MasterKegiatanPeer::IS_BTL,TRUE);
                }else{
                    $kode_user_id->add(Revisi1MasterKegiatanPeer::IS_BTL,FALSE);
                }
                $kode_user_id->add(Revisi1MasterKegiatanPeer::USER_ID, NULL, Criteria::ISNOTNULL);
                $kode_user_id->setDistinct(Revisi1MasterKegiatanPeer::USER_ID);
                $kode_user_id->addAscendingOrderByColumn(Revisi1MasterKegiatanPeer::USER_ID);
                $load_user_id = Revisi1MasterKegiatanPeer::doSelect($kode_user_id);
            }
            $this->load_user_id = $load_user_id;

            $c->addAscendingOrderByColumn(Revisi1MasterKegiatanPeer::KODE_KEGIATAN);
        } elseif (isset($this->filters['tahap']) && $this->filters['tahap'] == 'revisi1_1') {
            $this->pager = new sfPropelPager('Revisi1bMasterKegiatan', 20);
            $c = new Criteria();
            $this->addSortCriteria($c);
            $es = UnitKerjaPeer::doSelect(new Criteria);
            $this->satuan_kerja = $es;
            $unit_kerja = Array();
            foreach ($es as $x) {
                $unit_kerja[] = $x->getUnitId();
            }

            $c->add(Revisi1bMasterKegiatanPeer::UNIT_ID, $unit_kerja[0], Criteria::ILIKE);
            for ($i = 1; $i < count($unit_kerja); $i++) {
                $c->addOr(Revisi1bMasterKegiatanPeer::UNIT_ID, $unit_kerja[$i], Criteria::ILIKE);
            }

            $unit_id = $this->getRequestParameter('unit_id');
            if (isset($unit_id)) {
                $c->add(Revisi1bMasterKegiatanPeer::UNIT_ID, $unit_id);
            }

            if ($menu == 'Pendapatan') {
                $c->add(Revisi1bMasterKegiatanPeer::IS_BTL,TRUE);
            }else{
                $c->add(Revisi1bMasterKegiatanPeer::IS_BTL,FALSE);
            }

            if(isset($this->filters['id']) && $this->filters['id'] != ''){
                $c->add(Revisi1bMasterKegiatanPeer::KODE_KEGIATAN, $this->filters['id'], Criteria::EQUAL);
            }
            
            if(isset($this->filters['user_id']) && $this->filters['user_id'] != ''){
                $c->add(Revisi1bMasterKegiatanPeer::USER_ID, $this->filters['user_id'], Criteria::EQUAL);
            }

            $load_id = array();
            if(isset($this->filters['unit_id']) && $this->filters['unit_id'] != ''){
                $kode_id = new Criteria();
                $kode_id->add(Revisi1bMasterKegiatanPeer::UNIT_ID, $this->filters['unit_id'], Criteria::EQUAL);
                if($menu == 'Pendapatan'){
                    $kode_id->add(Revisi1bMasterKegiatanPeer::IS_BTL,TRUE);
                }else{
                    $kode_id->add(Revisi1bMasterKegiatanPeer::IS_BTL,FALSE);
                }
                $kode_id->addAscendingOrderByColumn(Revisi1bMasterKegiatanPeer::KODE_KEGIATAN);
                $load_id = Revisi1bMasterKegiatanPeer::doSelect($kode_id);
            }
            $this->load_id = $load_id;
            
            $load_user_id = array();
            if(isset($this->filters['unit_id']) && $this->filters['unit_id'] != ''){
                $kode_user_id = new Criteria();
                $kode_user_id->add(Revisi1bMasterKegiatanPeer::UNIT_ID, $this->filters['unit_id'], Criteria::EQUAL);
                if($menu == 'Pendapatan'){
                    $kode_user_id->add(Revisi1bMasterKegiatanPeer::IS_BTL,TRUE);
                }else{
                    $kode_user_id->add(Revisi1bMasterKegiatanPeer::IS_BTL,FALSE);
                }
                $kode_user_id->add(Revisi1bMasterKegiatanPeer::USER_ID, NULL, Criteria::ISNOTNULL);
                $kode_user_id->setDistinct(Revisi1bMasterKegiatanPeer::USER_ID);
                $kode_user_id->addAscendingOrderByColumn(Revisi1bMasterKegiatanPeer::USER_ID);
                $load_user_id = Revisi1bMasterKegiatanPeer::doSelect($kode_user_id);
            }
            $this->load_user_id = $load_user_id;

            $c->addAscendingOrderByColumn(Revisi1bMasterKegiatanPeer::KODE_KEGIATAN);
        } elseif (isset($this->filters['tahap']) && $this->filters['tahap'] == 'revisi2') {
            $this->pager = new sfPropelPager('Revisi2MasterKegiatan', 20);
            $c = new Criteria();
            $this->addSortCriteria($c);
            $es = UnitKerjaPeer::doSelect(new Criteria);
            $this->satuan_kerja = $es;
            $unit_kerja = Array();
            foreach ($es as $x) {
                $unit_kerja[] = $x->getUnitId();
            }

            $c->add(Revisi2MasterKegiatanPeer::UNIT_ID, $unit_kerja[0], Criteria::ILIKE);
            for ($i = 1; $i < count($unit_kerja); $i++) {
                $c->addOr(Revisi2MasterKegiatanPeer::UNIT_ID, $unit_kerja[$i], Criteria::ILIKE);
            }

            $unit_id = $this->getRequestParameter('unit_id');
            if (isset($unit_id)) {
                $c->add(Revisi2MasterKegiatanPeer::UNIT_ID, $unit_id);
            }

            if ($menu == 'Pendapatan') {
                $c->add(Revisi2MasterKegiatanPeer::IS_BTL,TRUE);
            }else{
                $c->add(Revisi2MasterKegiatanPeer::IS_BTL,FALSE);
            }

            if(isset($this->filters['id']) && $this->filters['id'] != ''){
                $c->add(Revisi2MasterKegiatanPeer::KODE_KEGIATAN, $this->filters['id'], Criteria::EQUAL);
            }
            
            if(isset($this->filters['user_id']) && $this->filters['user_id'] != ''){
                $c->add(Revisi2MasterKegiatanPeer::USER_ID, $this->filters['user_id'], Criteria::EQUAL);
            }

            $load_id = array();
            if(isset($this->filters['unit_id']) && $this->filters['unit_id'] != ''){
                $kode_id = new Criteria();
                $kode_id->add(Revisi2MasterKegiatanPeer::UNIT_ID, $this->filters['unit_id'], Criteria::EQUAL);
                if($menu == 'Pendapatan'){
                    $kode_id->add(Revisi2MasterKegiatanPeer::IS_BTL,TRUE);
                }else{
                    $kode_id->add(Revisi2MasterKegiatanPeer::IS_BTL,FALSE);
                }
                $kode_id->addAscendingOrderByColumn(Revisi2MasterKegiatanPeer::KODE_KEGIATAN);
                $load_id = Revisi2MasterKegiatanPeer::doSelect($kode_id);
            }
            $this->load_id = $load_id;
            
            $load_user_id = array();
            if(isset($this->filters['unit_id']) && $this->filters['unit_id'] != ''){
                $kode_user_id = new Criteria();
                $kode_user_id->add(Revisi2MasterKegiatanPeer::UNIT_ID, $this->filters['unit_id'], Criteria::EQUAL);
                if($menu == 'Pendapatan'){
                    $kode_user_id->add(Revisi2MasterKegiatanPeer::IS_BTL,TRUE);
                }else{
                    $kode_user_id->add(Revisi2MasterKegiatanPeer::IS_BTL,FALSE);
                }
                $kode_user_id->add(Revisi2MasterKegiatanPeer::USER_ID, NULL, Criteria::ISNOTNULL);
                $kode_user_id->setDistinct(Revisi2MasterKegiatanPeer::USER_ID);
                $kode_user_id->addAscendingOrderByColumn(Revisi2MasterKegiatanPeer::USER_ID);
                $load_user_id = Revisi2MasterKegiatanPeer::doSelect($kode_user_id);
            }
            $this->load_user_id = $load_user_id;

            $c->addAscendingOrderByColumn(Revisi2MasterKegiatanPeer::KODE_KEGIATAN);
        } 
        elseif (isset($this->filters['tahap']) && $this->filters['tahap'] == 'revisi2_1') {
            $this->pager = new sfPropelPager('Revisi21MasterKegiatan', 20);
            $c = new Criteria();
            $this->addSortCriteria($c);
            $es = UnitKerjaPeer::doSelect(new Criteria);
            $this->satuan_kerja = $es;
            $unit_kerja = Array();
            foreach ($es as $x) {
                $unit_kerja[] = $x->getUnitId();
            }

            $c->add(Revisi21MasterKegiatanPeer::UNIT_ID, $unit_kerja[0], Criteria::ILIKE);
            for ($i = 1; $i < count($unit_kerja); $i++) {
                $c->addOr(Revisi21MasterKegiatanPeer::UNIT_ID, $unit_kerja[$i], Criteria::ILIKE);
            }

            $unit_id = $this->getRequestParameter('unit_id');
            if (isset($unit_id)) {
                $c->add(Revisi21MasterKegiatanPeer::UNIT_ID, $unit_id);
            }

            if ($menu == 'Pendapatan') {
                $c->add(Revisi21MasterKegiatanPeer::IS_BTL,TRUE);
            }else{
                $c->add(Revisi21MasterKegiatanPeer::IS_BTL,FALSE);
            }

            if(isset($this->filters['id']) && $this->filters['id'] != ''){
                $c->add(Revisi21MasterKegiatanPeer::KODE_KEGIATAN, $this->filters['id'], Criteria::EQUAL);
            }
            
            if(isset($this->filters['user_id']) && $this->filters['user_id'] != ''){
                $c->add(Revisi21MasterKegiatanPeer::USER_ID, $this->filters['user_id'], Criteria::EQUAL);
            }

            $load_id = array();
            if(isset($this->filters['unit_id']) && $this->filters['unit_id'] != ''){
                $kode_id = new Criteria();
                $kode_id->add(Revisi21MasterKegiatanPeer::UNIT_ID, $this->filters['unit_id'], Criteria::EQUAL);
                if($menu == 'Pendapatan'){
                    $kode_id->add(Revisi21MasterKegiatanPeer::IS_BTL,TRUE);
                }else{
                    $kode_id->add(Revisi21MasterKegiatanPeer::IS_BTL,FALSE);
                }
                $kode_id->addAscendingOrderByColumn(Revisi21MasterKegiatanPeer::KODE_KEGIATAN);
                $load_id = Revisi21MasterKegiatanPeer::doSelect($kode_id);
            }
            $this->load_id = $load_id;
            
            $load_user_id = array();
            if(isset($this->filters['unit_id']) && $this->filters['unit_id'] != ''){
                $kode_user_id = new Criteria();
                $kode_user_id->add(Revisi21MasterKegiatanPeer::UNIT_ID, $this->filters['unit_id'], Criteria::EQUAL);
                if($menu == 'Pendapatan'){
                    $kode_user_id->add(Revisi21MasterKegiatanPeer::IS_BTL,TRUE);
                }else{
                    $kode_user_id->add(Revisi21MasterKegiatanPeer::IS_BTL,FALSE);
                }
                $kode_user_id->add(Revisi21MasterKegiatanPeer::USER_ID, NULL, Criteria::ISNOTNULL);
                $kode_user_id->setDistinct(Revisi21MasterKegiatanPeer::USER_ID);
                $kode_user_id->addAscendingOrderByColumn(Revisi21MasterKegiatanPeer::USER_ID);
                $load_user_id = Revisi21MasterKegiatanPeer::doSelect($kode_user_id);
            }
            $this->load_user_id = $load_user_id;

            $c->addAscendingOrderByColumn(Revisi21MasterKegiatanPeer::KODE_KEGIATAN);
        }  elseif (isset($this->filters['tahap']) && $this->filters['tahap'] == 'revisi2_2') {
            $this->pager = new sfPropelPager('Revisi22MasterKegiatan', 20);
            $c = new Criteria();
            $this->addSortCriteria($c);
            $es = UnitKerjaPeer::doSelect(new Criteria);
            $this->satuan_kerja = $es;
            $unit_kerja = Array();
            foreach ($es as $x) {
                $unit_kerja[] = $x->getUnitId();
            }

            $c->add(Revisi22MasterKegiatanPeer::UNIT_ID, $unit_kerja[0], Criteria::ILIKE);
            for ($i = 1; $i < count($unit_kerja); $i++) {
                $c->addOr(Revisi22MasterKegiatanPeer::UNIT_ID, $unit_kerja[$i], Criteria::ILIKE);
            }

            $unit_id = $this->getRequestParameter('unit_id');
            if (isset($unit_id)) {
                $c->add(Revisi22MasterKegiatanPeer::UNIT_ID, $unit_id);
            }

            if ($menu == 'Pendapatan') {
                $c->add(Revisi22MasterKegiatanPeer::IS_BTL,TRUE);
            }else{
                $c->add(Revisi22MasterKegiatanPeer::IS_BTL,FALSE);
            }

            if(isset($this->filters['id']) && $this->filters['id'] != ''){
                $c->add(Revisi22MasterKegiatanPeer::KODE_KEGIATAN, $this->filters['id'], Criteria::EQUAL);
            }
            
            if(isset($this->filters['user_id']) && $this->filters['user_id'] != ''){
                $c->add(Revisi22MasterKegiatanPeer::USER_ID, $this->filters['user_id'], Criteria::EQUAL);
            }

            $load_id = array();
            if(isset($this->filters['unit_id']) && $this->filters['unit_id'] != ''){
                $kode_id = new Criteria();
                $kode_id->add(Revisi22MasterKegiatanPeer::UNIT_ID, $this->filters['unit_id'], Criteria::EQUAL);
                if($menu == 'Pendapatan'){
                    $kode_id->add(Revisi22MasterKegiatanPeer::IS_BTL,TRUE);
                }else{
                    $kode_id->add(Revisi22MasterKegiatanPeer::IS_BTL,FALSE);
                }
                $kode_id->addAscendingOrderByColumn(Revisi22MasterKegiatanPeer::KODE_KEGIATAN);
                $load_id = Revisi22MasterKegiatanPeer::doSelect($kode_id);
            }
            $this->load_id = $load_id;
            
            $load_user_id = array();
            if(isset($this->filters['unit_id']) && $this->filters['unit_id'] != ''){
                $kode_user_id = new Criteria();
                $kode_user_id->add(Revisi22MasterKegiatanPeer::UNIT_ID, $this->filters['unit_id'], Criteria::EQUAL);
                if($menu == 'Pendapatan'){
                    $kode_user_id->add(Revisi22MasterKegiatanPeer::IS_BTL,TRUE);
                }else{
                    $kode_user_id->add(Revisi22MasterKegiatanPeer::IS_BTL,FALSE);
                }
                $kode_user_id->add(Revisi22MasterKegiatanPeer::USER_ID, NULL, Criteria::ISNOTNULL);
                $kode_user_id->setDistinct(Revisi22MasterKegiatanPeer::USER_ID);
                $kode_user_id->addAscendingOrderByColumn(Revisi22MasterKegiatanPeer::USER_ID);
                $load_user_id = Revisi22MasterKegiatanPeer::doSelect($kode_user_id);
            }
            $this->load_user_id = $load_user_id;

            $c->addAscendingOrderByColumn(Revisi22MasterKegiatanPeer::KODE_KEGIATAN);
        } 
        //revisi 3 start 21-08-2017 yogie =======================
        elseif (isset($this->filters['tahap']) && $this->filters['tahap'] == 'revisi3') {
//            echo "a";die();
            $this->pager = new sfPropelPager('Revisi3MasterKegiatan', 20);
            //print_r($this->pager);die();           
            $c = new Criteria();
            $this->addSortCriteria($c);
            $es = UnitKerjaPeer::doSelect(new Criteria);
            $this->satuan_kerja = $es;
            $unit_kerja = Array();
            foreach ($es as $x) {
                $unit_kerja[] = $x->getUnitId();
            }
            //print_r($unit_kerja);die();

            $c->add(Revisi3MasterKegiatanPeer::UNIT_ID, $unit_kerja[0], Criteria::ILIKE);
            for ($i = 1; $i < count($unit_kerja); $i++) {
                $c->addOr(Revisi3MasterKegiatanPeer::UNIT_ID, $unit_kerja[$i], Criteria::ILIKE);
            }

            $unit_id = $this->getRequestParameter('unit_id');
//            echo $this->getRequestParameter('unit_id');die();
            if (isset($unit_id)) {
//                echo $unit_id;die();
                $c->add(Revisi3MasterKegiatanPeer::UNIT_ID, $unit_id);
            }
//            echo $unit_id;die();

            if ($menu == 'Pendapatan') {
                $c->add(Revisi3MasterKegiatanPeer::IS_BTL,TRUE);
            }else{
                $c->add(Revisi3MasterKegiatanPeer::IS_BTL,FALSE);
            }

            if(isset($this->filters['id']) && $this->filters['id'] != ''){
                $c->add(Revisi3MasterKegiatanPeer::KODE_KEGIATAN, $this->filters['id'], Criteria::EQUAL);
            }
            
            if(isset($this->filters['user_id']) && $this->filters['user_id'] != ''){
                $c->add(Revisi3MasterKegiatanPeer::USER_ID, $this->filters['user_id'], Criteria::EQUAL);
            }

            $load_id = array();
            if(isset($this->filters['unit_id']) && $this->filters['unit_id'] != ''){
                $kode_id = new Criteria();
                $kode_id->add(Revisi3MasterKegiatanPeer::UNIT_ID, $this->filters['unit_id'], Criteria::EQUAL);
                if($menu == 'Pendapatan'){
                    $kode_id->add(Revisi3MasterKegiatanPeer::IS_BTL,TRUE);
                }else{
                    $kode_id->add(Revisi3MasterKegiatanPeer::IS_BTL,FALSE);
                }
                $kode_id->addAscendingOrderByColumn(Revisi3MasterKegiatanPeer::KODE_KEGIATAN);
                $load_id = Revisi3MasterKegiatanPeer::doSelect($kode_id);
            }
            $this->load_id = $load_id;
            
            $load_user_id = array();
            if(isset($this->filters['unit_id']) && $this->filters['unit_id'] != ''){
                $kode_user_id = new Criteria();
                $kode_user_id->add(Revisi3MasterKegiatanPeer::UNIT_ID, $this->filters['unit_id'], Criteria::EQUAL);
                if($menu == 'Pendapatan'){
                    $kode_user_id->add(Revisi3MasterKegiatanPeer::IS_BTL,TRUE);
                }else{
                    $kode_user_id->add(Revisi3MasterKegiatanPeer::IS_BTL,FALSE);
                }
                $kode_user_id->add(Revisi3MasterKegiatanPeer::USER_ID, NULL, Criteria::ISNOTNULL);
                $kode_user_id->setDistinct(Revisi3MasterKegiatanPeer::USER_ID);
                $kode_user_id->addAscendingOrderByColumn(Revisi3MasterKegiatanPeer::USER_ID);
                $load_user_id = Revisi3MasterKegiatanPeer::doSelect($kode_user_id);
            }
            $this->load_user_id = $load_user_id;

            $c->addAscendingOrderByColumn(Revisi3MasterKegiatanPeer::KODE_KEGIATAN);
        }
        elseif (isset($this->filters['tahap']) && $this->filters['tahap'] == 'revisi3_1') {
//            echo "a";die();
            $this->pager = new sfPropelPager('Revisi31MasterKegiatan', 20);
            //print_r($this->pager);die();           
            $c = new Criteria();
            $this->addSortCriteria($c);
            $es = UnitKerjaPeer::doSelect(new Criteria);
            $this->satuan_kerja = $es;
            $unit_kerja = Array();
            foreach ($es as $x) {
                $unit_kerja[] = $x->getUnitId();
            }
            //print_r($unit_kerja);die();

            $c->add(Revisi31MasterKegiatanPeer::UNIT_ID, $unit_kerja[0], Criteria::ILIKE);
            for ($i = 1; $i < count($unit_kerja); $i++) {
                $c->addOr(Revisi31MasterKegiatanPeer::UNIT_ID, $unit_kerja[$i], Criteria::ILIKE);
            }

            $unit_id = $this->getRequestParameter('unit_id');
//            echo $this->getRequestParameter('unit_id');die();
            if (isset($unit_id)) {
//                echo $unit_id;die();
                $c->add(Revisi31MasterKegiatanPeer::UNIT_ID, $unit_id);
            }
//            echo $unit_id;die();

            if ($menu == 'Pendapatan') {
                $c->add(Revisi31MasterKegiatanPeer::IS_BTL,TRUE);
            }else{
                $c->add(Revisi31MasterKegiatanPeer::IS_BTL,FALSE);
            }

            if(isset($this->filters['id']) && $this->filters['id'] != ''){
                $c->add(Revisi31MasterKegiatanPeer::KODE_KEGIATAN, $this->filters['id'], Criteria::EQUAL);
            }
            
            if(isset($this->filters['user_id']) && $this->filters['user_id'] != ''){
                $c->add(Revisi31MasterKegiatanPeer::USER_ID, $this->filters['user_id'], Criteria::EQUAL);
            }

            $load_id = array();
            if(isset($this->filters['unit_id']) && $this->filters['unit_id'] != ''){
                $kode_id = new Criteria();
                $kode_id->add(Revisi31MasterKegiatanPeer::UNIT_ID, $this->filters['unit_id'], Criteria::EQUAL);
                if($menu == 'Pendapatan'){
                    $kode_id->add(Revisi31MasterKegiatanPeer::IS_BTL,TRUE);
                }else{
                    $kode_id->add(Revisi31MasterKegiatanPeer::IS_BTL,FALSE);
                }
                $kode_id->addAscendingOrderByColumn(Revisi31MasterKegiatanPeer::KODE_KEGIATAN);
                $load_id = Revisi31MasterKegiatanPeer::doSelect($kode_id);
            }
            $this->load_id = $load_id;
            
            $load_user_id = array();
            if(isset($this->filters['unit_id']) && $this->filters['unit_id'] != ''){
                $kode_user_id = new Criteria();
                $kode_user_id->add(Revisi31MasterKegiatanPeer::UNIT_ID, $this->filters['unit_id'], Criteria::EQUAL);
                if($menu == 'Pendapatan'){
                    $kode_user_id->add(Revisi31MasterKegiatanPeer::IS_BTL,TRUE);
                }else{
                    $kode_user_id->add(Revisi31MasterKegiatanPeer::IS_BTL,FALSE);
                }
                $kode_user_id->add(Revisi31MasterKegiatanPeer::USER_ID, NULL, Criteria::ISNOTNULL);
                $kode_user_id->setDistinct(Revisi31MasterKegiatanPeer::USER_ID);
                $kode_user_id->addAscendingOrderByColumn(Revisi31MasterKegiatanPeer::USER_ID);
                $load_user_id = Revisi31MasterKegiatanPeer::doSelect($kode_user_id);
            }
            $this->load_user_id = $load_user_id;

            $c->addAscendingOrderByColumn(Revisi31MasterKegiatanPeer::KODE_KEGIATAN);
        }
        //revisi 3 end 21-08-2017 yogie =======================
        elseif (isset($this->filters['tahap']) && $this->filters['tahap'] == 'revisi4') {
            $this->pager = new sfPropelPager('Revisi4MasterKegiatan', 20);
            $c = new Criteria();
            $this->addSortCriteria($c);
            $es = UnitKerjaPeer::doSelect(new Criteria);
            $this->satuan_kerja = $es;
            $unit_kerja = Array();
            foreach ($es as $x) {
                $unit_kerja[] = $x->getUnitId();
            }

            $c->add(Revisi4MasterKegiatanPeer::UNIT_ID, $unit_kerja[0], Criteria::ILIKE);
            for ($i = 1; $i < count($unit_kerja); $i++) {
                $c->addOr(Revisi4MasterKegiatanPeer::UNIT_ID, $unit_kerja[$i], Criteria::ILIKE);
            }

            $unit_id = $this->getRequestParameter('unit_id');
            if (isset($unit_id)) {
                $c->add(Revisi4MasterKegiatanPeer::UNIT_ID, $unit_id);
            }

            if ($menu == 'Pendapatan') {
                $c->add(Revisi4MasterKegiatanPeer::IS_BTL,TRUE);
            }else{
                $c->add(Revisi4MasterKegiatanPeer::IS_BTL,FALSE);
            }

            if(isset($this->filters['id']) && $this->filters['id'] != ''){
                $c->add(Revisi4MasterKegiatanPeer::KODE_KEGIATAN, $this->filters['id'], Criteria::EQUAL);
            }
            
            if(isset($this->filters['user_id']) && $this->filters['user_id'] != ''){
                $c->add(Revisi4MasterKegiatanPeer::USER_ID, $this->filters['user_id'], Criteria::EQUAL);
            }

            $load_id = array();
            if(isset($this->filters['unit_id']) && $this->filters['unit_id'] != ''){
                $kode_id = new Criteria();
                $kode_id->add(Revisi4MasterKegiatanPeer::UNIT_ID, $this->filters['unit_id'], Criteria::EQUAL);
                if($menu == 'Pendapatan'){
                    $kode_id->add(Revisi4MasterKegiatanPeer::IS_BTL,TRUE);
                }else{
                    $kode_id->add(Revisi4MasterKegiatanPeer::IS_BTL,FALSE);
                }
                $kode_id->addAscendingOrderByColumn(Revisi4MasterKegiatanPeer::KODE_KEGIATAN);
                $load_id = Revisi4MasterKegiatanPeer::doSelect($kode_id);
            }
            $this->load_id = $load_id;
            
            $load_user_id = array();
            if(isset($this->filters['unit_id']) && $this->filters['unit_id'] != ''){
                $kode_user_id = new Criteria();
                $kode_user_id->add(Revisi4MasterKegiatanPeer::UNIT_ID, $this->filters['unit_id'], Criteria::EQUAL);
                if($menu == 'Pendapatan'){
                    $kode_user_id->add(Revisi4MasterKegiatanPeer::IS_BTL,TRUE);
                }else{
                    $kode_user_id->add(Revisi4MasterKegiatanPeer::IS_BTL,FALSE);
                }
                $kode_user_id->add(Revisi4MasterKegiatanPeer::USER_ID, NULL, Criteria::ISNOTNULL);
                $kode_user_id->setDistinct(Revisi4MasterKegiatanPeer::USER_ID);
                $kode_user_id->addAscendingOrderByColumn(Revisi4MasterKegiatanPeer::USER_ID);
                $load_user_id = Revisi4MasterKegiatanPeer::doSelect($kode_user_id);
            }
            $this->load_user_id = $load_user_id;

            $c->addAscendingOrderByColumn(Revisi4MasterKegiatanPeer::KODE_KEGIATAN);
        }
        elseif (isset($this->filters['tahap']) && $this->filters['tahap'] == 'revisi5') {
            $this->pager = new sfPropelPager('Revisi5MasterKegiatan', 20);
            $c = new Criteria();
            $this->addSortCriteria($c);
            $es = UnitKerjaPeer::doSelect(new Criteria);
            $this->satuan_kerja = $es;
            $unit_kerja = Array();
            foreach ($es as $x) {
                $unit_kerja[] = $x->getUnitId();
            }

            $c->add(Revisi5MasterKegiatanPeer::UNIT_ID, $unit_kerja[0], Criteria::ILIKE);
            for ($i = 1; $i < count($unit_kerja); $i++) {
                $c->addOr(Revisi5MasterKegiatanPeer::UNIT_ID, $unit_kerja[$i], Criteria::ILIKE);
            }

            $unit_id = $this->getRequestParameter('unit_id');
            if (isset($unit_id)) {
                $c->add(Revisi5MasterKegiatanPeer::UNIT_ID, $unit_id);
            }

            if ($menu == 'Pendapatan') {
                $c->add(Revisi5MasterKegiatanPeer::IS_BTL,TRUE);
            }else{
                $c->add(Revisi5MasterKegiatanPeer::IS_BTL,FALSE);
            }

            if(isset($this->filters['id']) && $this->filters['id'] != ''){
                $c->add(Revisi5MasterKegiatanPeer::KODE_KEGIATAN, $this->filters['id'], Criteria::EQUAL);
            }
            
            if(isset($this->filters['user_id']) && $this->filters['user_id'] != ''){
                $c->add(Revisi5MasterKegiatanPeer::USER_ID, $this->filters['user_id'], Criteria::EQUAL);
            }

            $load_id = array();
            if(isset($this->filters['unit_id']) && $this->filters['unit_id'] != ''){
                $kode_id = new Criteria();
                $kode_id->add(Revisi5MasterKegiatanPeer::UNIT_ID, $this->filters['unit_id'], Criteria::EQUAL);
                if($menu == 'Pendapatan'){
                    $kode_id->add(Revisi5MasterKegiatanPeer::IS_BTL,TRUE);
                }else{
                    $kode_id->add(Revisi5MasterKegiatanPeer::IS_BTL,FALSE);
                }
                $kode_id->addAscendingOrderByColumn(Revisi5MasterKegiatanPeer::KODE_KEGIATAN);
                $load_id = Revisi5MasterKegiatanPeer::doSelect($kode_id);
            }
            $this->load_id = $load_id;
            
            $load_user_id = array();
            if(isset($this->filters['unit_id']) && $this->filters['unit_id'] != ''){
                $kode_user_id = new Criteria();
                $kode_user_id->add(Revisi5MasterKegiatanPeer::UNIT_ID, $this->filters['unit_id'], Criteria::EQUAL);
                if($menu == 'Pendapatan'){
                    $kode_user_id->add(Revisi5MasterKegiatanPeer::IS_BTL,TRUE);
                }else{
                    $kode_user_id->add(Revisi5MasterKegiatanPeer::IS_BTL,FALSE);
                }
                $kode_user_id->add(Revisi5MasterKegiatanPeer::USER_ID, NULL, Criteria::ISNOTNULL);
                $kode_user_id->setDistinct(Revisi5MasterKegiatanPeer::USER_ID);
                $kode_user_id->addAscendingOrderByColumn(Revisi5MasterKegiatanPeer::USER_ID);
                $load_user_id = Revisi5MasterKegiatanPeer::doSelect($kode_user_id);
            }
            $this->load_user_id = $load_user_id;

            $c->addAscendingOrderByColumn(Revisi5MasterKegiatanPeer::KODE_KEGIATAN);
        }
        elseif (isset($this->filters['tahap']) && $this->filters['tahap'] == 'revisi6') {
            $this->pager = new sfPropelPager('Revisi6MasterKegiatan', 20);
            $c = new Criteria();
            $this->addSortCriteria($c);
            $es = UnitKerjaPeer::doSelect(new Criteria);
            $this->satuan_kerja = $es;
            $unit_kerja = Array();
            foreach ($es as $x) {
                $unit_kerja[] = $x->getUnitId();
            }

            $c->add(Revisi6MasterKegiatanPeer::UNIT_ID, $unit_kerja[0], Criteria::ILIKE);
            for ($i = 1; $i < count($unit_kerja); $i++) {
                $c->addOr(Revisi6MasterKegiatanPeer::UNIT_ID, $unit_kerja[$i], Criteria::ILIKE);
            }

            $unit_id = $this->getRequestParameter('unit_id');
            if (isset($unit_id)) {
                $c->add(Revisi6MasterKegiatanPeer::UNIT_ID, $unit_id);
            }

            if ($menu == 'Pendapatan') {
                $c->add(Revisi6MasterKegiatanPeer::IS_BTL,TRUE);
            }else{
                $c->add(Revisi6MasterKegiatanPeer::IS_BTL,FALSE);
            }

            if(isset($this->filters['id']) && $this->filters['id'] != ''){
                $c->add(Revisi6MasterKegiatanPeer::KODE_KEGIATAN, $this->filters['id'], Criteria::EQUAL);
            }
            
            if(isset($this->filters['user_id']) && $this->filters['user_id'] != ''){
                $c->add(Revisi6MasterKegiatanPeer::USER_ID, $this->filters['user_id'], Criteria::EQUAL);
            }

            $load_id = array();
            if(isset($this->filters['unit_id']) && $this->filters['unit_id'] != ''){
                $kode_id = new Criteria();
                $kode_id->add(Revisi6MasterKegiatanPeer::UNIT_ID, $this->filters['unit_id'], Criteria::EQUAL);
                if($menu == 'Pendapatan'){
                    $kode_id->add(Revisi6MasterKegiatanPeer::IS_BTL,TRUE);
                }else{
                    $kode_id->add(Revisi6MasterKegiatanPeer::IS_BTL,FALSE);
                }
                $kode_id->addAscendingOrderByColumn(Revisi6MasterKegiatanPeer::KODE_KEGIATAN);
                $load_id = Revisi6MasterKegiatanPeer::doSelect($kode_id);
            }
            $this->load_id = $load_id;
            
            $load_user_id = array();
            if(isset($this->filters['unit_id']) && $this->filters['unit_id'] != ''){
                $kode_user_id = new Criteria();
                $kode_user_id->add(Revisi6MasterKegiatanPeer::UNIT_ID, $this->filters['unit_id'], Criteria::EQUAL);
                if($menu == 'Pendapatan'){
                    $kode_user_id->add(Revisi6MasterKegiatanPeer::IS_BTL,TRUE);
                }else{
                    $kode_user_id->add(Revisi6MasterKegiatanPeer::IS_BTL,FALSE);
                }
                $kode_user_id->add(Revisi6MasterKegiatanPeer::USER_ID, NULL, Criteria::ISNOTNULL);
                $kode_user_id->setDistinct(Revisi6MasterKegiatanPeer::USER_ID);
                $kode_user_id->addAscendingOrderByColumn(Revisi6MasterKegiatanPeer::USER_ID);
                $load_user_id = Revisi6MasterKegiatanPeer::doSelect($kode_user_id);
            }
            $this->load_user_id = $load_user_id;

            $c->addAscendingOrderByColumn(Revisi6MasterKegiatanPeer::KODE_KEGIATAN);
        }
        elseif (isset($this->filters['tahap']) && $this->filters['tahap'] == 'revisi7') {
            $this->pager = new sfPropelPager('Revisi7MasterKegiatan', 20);
            $c = new Criteria();
            $this->addSortCriteria($c);
            $es = UnitKerjaPeer::doSelect(new Criteria);
            $this->satuan_kerja = $es;
            $unit_kerja = Array();
            foreach ($es as $x) {
                $unit_kerja[] = $x->getUnitId();
            }

            $c->add(Revisi7MasterKegiatanPeer::UNIT_ID, $unit_kerja[0], Criteria::ILIKE);
            for ($i = 1; $i < count($unit_kerja); $i++) {
                $c->addOr(Revisi7MasterKegiatanPeer::UNIT_ID, $unit_kerja[$i], Criteria::ILIKE);
            }
            
            $unit_id = $this->getRequestParameter('unit_id');
            if (isset($unit_id)) {
                $c->add(Revisi7MasterKegiatanPeer::UNIT_ID, $unit_id);
            }

            if ($menu == 'Pendapatan') {
                $c->add(Revisi7MasterKegiatanPeer::IS_BTL,TRUE);
            }else{
                $c->add(Revisi7MasterKegiatanPeer::IS_BTL,FALSE);
            }

            if(isset($this->filters['id']) && $this->filters['id'] != ''){
                $c->add(Revisi7MasterKegiatanPeer::KODE_KEGIATAN, $this->filters['id'], Criteria::EQUAL);
            }
            
            if(isset($this->filters['user_id']) && $this->filters['user_id'] != ''){
                $c->add(Revisi7MasterKegiatanPeer::USER_ID, $this->filters['user_id'], Criteria::EQUAL);
            }

            $load_id = array();
            if(isset($this->filters['unit_id']) && $this->filters['unit_id'] != ''){
                $kode_id = new Criteria();
                $kode_id->add(Revisi7MasterKegiatanPeer::UNIT_ID, $this->filters['unit_id'], Criteria::EQUAL);
                if($menu == 'Pendapatan'){
                    $kode_id->add(Revisi7MasterKegiatanPeer::IS_BTL,TRUE);
                }else{
                    $kode_id->add(Revisi7MasterKegiatanPeer::IS_BTL,FALSE);
                }
                $kode_id->addAscendingOrderByColumn(Revisi7MasterKegiatanPeer::KODE_KEGIATAN);
                $load_id = Revisi7MasterKegiatanPeer::doSelect($kode_id);
            }
            $this->load_id = $load_id;
            
            $load_user_id = array();
            if(isset($this->filters['unit_id']) && $this->filters['unit_id'] != ''){
                $kode_user_id = new Criteria();
                $kode_user_id->add(Revisi7MasterKegiatanPeer::UNIT_ID, $this->filters['unit_id'], Criteria::EQUAL);
                if($menu == 'Pendapatan'){
                    $kode_user_id->add(Revisi7MasterKegiatanPeer::IS_BTL,TRUE);
                }else{
                    $kode_user_id->add(Revisi7MasterKegiatanPeer::IS_BTL,FALSE);
                }
                $kode_user_id->add(Revisi7MasterKegiatanPeer::USER_ID, NULL, Criteria::ISNOTNULL);
                $kode_user_id->setDistinct(Revisi7MasterKegiatanPeer::USER_ID);
                $kode_user_id->addAscendingOrderByColumn(Revisi7MasterKegiatanPeer::USER_ID);
                $load_user_id = Revisi7MasterKegiatanPeer::doSelect($kode_user_id);
            }
            $this->load_user_id = $load_user_id;

            $c->addAscendingOrderByColumn(Revisi7MasterKegiatanPeer::KODE_KEGIATAN);
        }
        elseif (isset($this->filters['tahap']) && $this->filters['tahap'] == 'revisi8') {
            $this->pager = new sfPropelPager('Revisi8MasterKegiatan', 20);
            $c = new Criteria();
            $this->addSortCriteria($c);
            $es = UnitKerjaPeer::doSelect(new Criteria);
            $this->satuan_kerja = $es;
            $unit_kerja = Array();
            foreach ($es as $x) {
                $unit_kerja[] = $x->getUnitId();
            }

            $c->add(Revisi8MasterKegiatanPeer::UNIT_ID, $unit_kerja[0], Criteria::ILIKE);
            for ($i = 1; $i < count($unit_kerja); $i++) {
                $c->addOr(Revisi8MasterKegiatanPeer::UNIT_ID, $unit_kerja[$i], Criteria::ILIKE);
            }
            
            $unit_id = $this->getRequestParameter('unit_id');
            if (isset($unit_id)) {
                $c->add(Revisi8MasterKegiatanPeer::UNIT_ID, $unit_id);
            }

            if ($menu == 'Pendapatan') {
                $c->add(Revisi8MasterKegiatanPeer::IS_BTL,TRUE);
            }else{
                $c->add(Revisi8MasterKegiatanPeer::IS_BTL,FALSE);
            }

            if(isset($this->filters['id']) && $this->filters['id'] != ''){
                $c->add(Revisi8MasterKegiatanPeer::KODE_KEGIATAN, $this->filters['id'], Criteria::EQUAL);
            }
            
            if(isset($this->filters['user_id']) && $this->filters['user_id'] != ''){
                $c->add(Revisi8MasterKegiatanPeer::USER_ID, $this->filters['user_id'], Criteria::EQUAL);
            }

            $load_id = array();
            if(isset($this->filters['unit_id']) && $this->filters['unit_id'] != ''){
                $kode_id = new Criteria();
                $kode_id->add(Revisi8MasterKegiatanPeer::UNIT_ID, $this->filters['unit_id'], Criteria::EQUAL);
                if($menu == 'Pendapatan'){
                    $kode_id->add(Revisi8MasterKegiatanPeer::IS_BTL,TRUE);
                }else{
                    $kode_id->add(Revisi8MasterKegiatanPeer::IS_BTL,FALSE);
                }
                $kode_id->addAscendingOrderByColumn(Revisi8MasterKegiatanPeer::KODE_KEGIATAN);
                $load_id = Revisi8MasterKegiatanPeer::doSelect($kode_id);
            }
            $this->load_id = $load_id;
            
            $load_user_id = array();
            if(isset($this->filters['unit_id']) && $this->filters['unit_id'] != ''){
                $kode_user_id = new Criteria();
                $kode_user_id->add(Revisi8MasterKegiatanPeer::UNIT_ID, $this->filters['unit_id'], Criteria::EQUAL);
                if($menu == 'Pendapatan'){
                    $kode_user_id->add(Revisi8MasterKegiatanPeer::IS_BTL,TRUE);
                }else{
                    $kode_user_id->add(Revisi8MasterKegiatanPeer::IS_BTL,FALSE);
                }
                $kode_user_id->add(Revisi8MasterKegiatanPeer::USER_ID, NULL, Criteria::ISNOTNULL);
                $kode_user_id->setDistinct(Revisi8MasterKegiatanPeer::USER_ID);
                $kode_user_id->addAscendingOrderByColumn(Revisi8MasterKegiatanPeer::USER_ID);
                $load_user_id = Revisi8MasterKegiatanPeer::doSelect($kode_user_id);
            }
            $this->load_user_id = $load_user_id;

            $c->addAscendingOrderByColumn(Revisi8MasterKegiatanPeer::KODE_KEGIATAN);
        }
        elseif (isset($this->filters['tahap']) && $this->filters['tahap'] == 'revisi9') {
            $this->pager = new sfPropelPager('Revisi9MasterKegiatan', 20);
            $c = new Criteria();
            $this->addSortCriteria($c);
            $es = UnitKerjaPeer::doSelect(new Criteria);
            $this->satuan_kerja = $es;
            $unit_kerja = Array();
            foreach ($es as $x) {
                $unit_kerja[] = $x->getUnitId();
            }

            $c->add(Revisi9MasterKegiatanPeer::UNIT_ID, $unit_kerja[0], Criteria::ILIKE);
            for ($i = 1; $i < count($unit_kerja); $i++) {
                $c->addOr(Revisi9MasterKegiatanPeer::UNIT_ID, $unit_kerja[$i], Criteria::ILIKE);
            }
            
            $unit_id = $this->getRequestParameter('unit_id');
            if (isset($unit_id)) {
                $c->add(Revisi9MasterKegiatanPeer::UNIT_ID, $unit_id);
            }

            if ($menu == 'Pendapatan') {
                $c->add(Revisi9MasterKegiatanPeer::IS_BTL,TRUE);
            }else{
                $c->add(Revisi9MasterKegiatanPeer::IS_BTL,FALSE);
            }

            if(isset($this->filters['id']) && $this->filters['id'] != ''){
                $c->add(Revisi9MasterKegiatanPeer::KODE_KEGIATAN, $this->filters['id'], Criteria::EQUAL);
            }
            
            if(isset($this->filters['user_id']) && $this->filters['user_id'] != ''){
                $c->add(Revisi9MasterKegiatanPeer::USER_ID, $this->filters['user_id'], Criteria::EQUAL);
            }

            $load_id = array();
            if(isset($this->filters['unit_id']) && $this->filters['unit_id'] != ''){
                $kode_id = new Criteria();
                $kode_id->add(Revisi9MasterKegiatanPeer::UNIT_ID, $this->filters['unit_id'], Criteria::EQUAL);
                if($menu == 'Pendapatan'){
                    $kode_id->add(Revisi9MasterKegiatanPeer::IS_BTL,TRUE);
                }else{
                    $kode_id->add(Revisi9MasterKegiatanPeer::IS_BTL,FALSE);
                }
                $kode_id->addAscendingOrderByColumn(Revisi9MasterKegiatanPeer::KODE_KEGIATAN);
                $load_id = Revisi9MasterKegiatanPeer::doSelect($kode_id);
            }
            $this->load_id = $load_id;
            
            $load_user_id = array();
            if(isset($this->filters['unit_id']) && $this->filters['unit_id'] != ''){
                $kode_user_id = new Criteria();
                $kode_user_id->add(Revisi9MasterKegiatanPeer::UNIT_ID, $this->filters['unit_id'], Criteria::EQUAL);
                if($menu == 'Pendapatan'){
                    $kode_user_id->add(Revisi9MasterKegiatanPeer::IS_BTL,TRUE);
                }else{
                    $kode_user_id->add(Revisi9MasterKegiatanPeer::IS_BTL,FALSE);
                }
                $kode_user_id->add(Revisi9MasterKegiatanPeer::USER_ID, NULL, Criteria::ISNOTNULL);
                $kode_user_id->setDistinct(Revisi9MasterKegiatanPeer::USER_ID);
                $kode_user_id->addAscendingOrderByColumn(Revisi9MasterKegiatanPeer::USER_ID);
                $load_user_id = Revisi9MasterKegiatanPeer::doSelect($kode_user_id);
            }
            $this->load_user_id = $load_user_id;

            $c->addAscendingOrderByColumn(Revisi9MasterKegiatanPeer::KODE_KEGIATAN);
        }
        elseif (isset($this->filters['tahap']) && $this->filters['tahap'] == 'revisi10') {
            $this->pager = new sfPropelPager('Revisi10MasterKegiatan', 20);
            $c = new Criteria();
            $this->addSortCriteria($c);
            $es = UnitKerjaPeer::doSelect(new Criteria);
            $this->satuan_kerja = $es;
            $unit_kerja = Array();
            foreach ($es as $x) {
                $unit_kerja[] = $x->getUnitId();
            }

            $c->add(Revisi10MasterKegiatanPeer::UNIT_ID, $unit_kerja[0], Criteria::ILIKE);
            for ($i = 1; $i < count($unit_kerja); $i++) {
                $c->addOr(Revisi10MasterKegiatanPeer::UNIT_ID, $unit_kerja[$i], Criteria::ILIKE);
            }
            
            $unit_id = $this->getRequestParameter('unit_id');
            if (isset($unit_id)) {
                $c->add(Revisi10MasterKegiatanPeer::UNIT_ID, $unit_id);
            }

            if ($menu == 'Pendapatan') {
                $c->add(Revisi10MasterKegiatanPeer::IS_BTL,TRUE);
            }else{
                $c->add(Revisi10MasterKegiatanPeer::IS_BTL,FALSE);
            }

            if(isset($this->filters['id']) && $this->filters['id'] != ''){
                $c->add(Revisi10MasterKegiatanPeer::KODE_KEGIATAN, $this->filters['id'], Criteria::EQUAL);
            }
            
            if(isset($this->filters['user_id']) && $this->filters['user_id'] != ''){
                $c->add(Revisi10MasterKegiatanPeer::USER_ID, $this->filters['user_id'], Criteria::EQUAL);
            }

            $load_id = array();
            if(isset($this->filters['unit_id']) && $this->filters['unit_id'] != ''){
                $kode_id = new Criteria();
                $kode_id->add(Revisi10MasterKegiatanPeer::UNIT_ID, $this->filters['unit_id'], Criteria::EQUAL);
                if($menu == 'Pendapatan'){
                    $kode_id->add(Revisi10MasterKegiatanPeer::IS_BTL,TRUE);
                }else{
                    $kode_id->add(Revisi10MasterKegiatanPeer::IS_BTL,FALSE);
                }
                $kode_id->addAscendingOrderByColumn(Revisi10MasterKegiatanPeer::KODE_KEGIATAN);
                $load_id = Revisi10MasterKegiatanPeer::doSelect($kode_id);
            }
            $this->load_id = $load_id;
            
            $load_user_id = array();
            if(isset($this->filters['unit_id']) && $this->filters['unit_id'] != ''){
                $kode_user_id = new Criteria();
                $kode_user_id->add(Revisi10MasterKegiatanPeer::UNIT_ID, $this->filters['unit_id'], Criteria::EQUAL);
                if($menu == 'Pendapatan'){
                    $kode_user_id->add(Revisi10MasterKegiatanPeer::IS_BTL,TRUE);
                }else{
                    $kode_user_id->add(Revisi10MasterKegiatanPeer::IS_BTL,FALSE);
                }
                $kode_user_id->add(Revisi10MasterKegiatanPeer::USER_ID, NULL, Criteria::ISNOTNULL);
                $kode_user_id->setDistinct(Revisi10MasterKegiatanPeer::USER_ID);
                $kode_user_id->addAscendingOrderByColumn(Revisi10MasterKegiatanPeer::USER_ID);
                $load_user_id = Revisi10MasterKegiatanPeer::doSelect($kode_user_id);
            }
            $this->load_user_id = $load_user_id;

            $c->addAscendingOrderByColumn(Revisi10MasterKegiatanPeer::KODE_KEGIATAN);
        }
        elseif (isset($this->filters['tahap']) && $this->filters['tahap'] == 'rkua') {
            $this->pager = new sfPropelPager('RkuaMasterKegiatan', 20);
            $c = new Criteria();
            $this->addSortCriteria($c);
            $es = UnitKerjaPeer::doSelect(new Criteria);
            $this->satuan_kerja = $es;
            $unit_kerja = Array();
            foreach ($es as $x) {
                $unit_kerja[] = $x->getUnitId();
            }

            $c->add(RkuaMasterKegiatanPeer::UNIT_ID, $unit_kerja[0], Criteria::ILIKE);
            for ($i = 1; $i < count($unit_kerja); $i++) {
                $c->addOr(RkuaMasterKegiatanPeer::UNIT_ID, $unit_kerja[$i], Criteria::ILIKE);
            }

            $unit_id = $this->getRequestParameter('unit_id');
            if (isset($unit_id)) {
                $c->add(RkuaMasterKegiatanPeer::UNIT_ID, $unit_id);
            }

            if ($menu == 'Pendapatan') {
                $c->add(RkuaMasterKegiatanPeer::IS_BTL,TRUE);
            }else{
                $c->add(RkuaMasterKegiatanPeer::IS_BTL,FALSE);
            }

            if(isset($this->filters['id']) && $this->filters['id'] != ''){
                $c->add(RkuaMasterKegiatanPeer::KODE_KEGIATAN, $this->filters['id'], Criteria::EQUAL);
            }
            
            if(isset($this->filters['user_id']) && $this->filters['user_id'] != ''){
                $c->add(RkuaMasterKegiatanPeer::USER_ID, $this->filters['user_id'], Criteria::EQUAL);
            }

            $load_id = array();
            if(isset($this->filters['unit_id']) && $this->filters['unit_id'] != ''){
                $kode_id = new Criteria();
                $kode_id->add(RkuaMasterKegiatanPeer::UNIT_ID, $this->filters['unit_id'], Criteria::EQUAL);
                if($menu == 'Pendapatan'){
                    $kode_id->add(RkuaMasterKegiatanPeer::IS_BTL,TRUE);
                }else{
                    $kode_id->add(RkuaMasterKegiatanPeer::IS_BTL,FALSE);
                }
                $kode_id->addAscendingOrderByColumn(RkuaMasterKegiatanPeer::KODE_KEGIATAN);
                $load_id = RkuaMasterKegiatanPeer::doSelect($kode_id);
            }
            $this->load_id = $load_id;
            
            $load_user_id = array();
            if(isset($this->filters['unit_id']) && $this->filters['unit_id'] != ''){
                $kode_user_id = new Criteria();
                $kode_user_id->add(RkuaMasterKegiatanPeer::UNIT_ID, $this->filters['unit_id'], Criteria::EQUAL);
                if($menu == 'Pendapatan'){
                    $kode_user_id->add(RkuaMasterKegiatanPeer::IS_BTL,TRUE);
                }else{
                    $kode_user_id->add(RkuaMasterKegiatanPeer::IS_BTL,FALSE);
                }
                $kode_user_id->add(RkuaMasterKegiatanPeer::USER_ID, NULL, Criteria::ISNOTNULL);
                $kode_user_id->setDistinct(RkuaMasterKegiatanPeer::USER_ID);
                $kode_user_id->addAscendingOrderByColumn(RkuaMasterKegiatanPeer::USER_ID);
                $load_user_id = RkuaMasterKegiatanPeer::doSelect($kode_user_id);
            }
            $this->load_user_id = $load_user_id;

            $c->addAscendingOrderByColumn('CAST('.RkuaMasterKegiatanPeer::KODE_KEGIATAN.' AS INT)');
        } else {
            $this->pager = new sfPropelPager('DinasMasterKegiatan', 20);
            $c = new Criteria();
            $this->addSortCriteria($c);
            $es = UnitKerjaPeer::doSelect(new Criteria);
            $this->satuan_kerja = $es;
            $unit_kerja = Array();
            foreach ($es as $x) {
                $unit_kerja[] = $x->getUnitId();
            }

            $c->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_kerja[0], Criteria::ILIKE);
            for ($i = 1; $i < count($unit_kerja); $i++) {
                $c->addOr(DinasMasterKegiatanPeer::UNIT_ID, $unit_kerja[$i], Criteria::ILIKE);
            }

            $unit_id = $this->getRequestParameter('unit_id');
            if (isset($unit_id)) {
                $c->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
            }
            if ($menu == 'Pendapatan') {
                $c->add(DinasMasterKegiatanPeer::IS_BTL,TRUE);
            }
           else
           {
                $c->add(DinasMasterKegiatanPeer::IS_BTL,FALSE);
           }

            if(isset($this->filters['id']) && $this->filters['id'] != ''){
                $c->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $this->filters['id'], Criteria::EQUAL);
            }
            
            if(isset($this->filters['user_id']) && $this->filters['user_id'] != ''){
                $c->add(DinasMasterKegiatanPeer::USER_ID, $this->filters['user_id'], Criteria::EQUAL);
            }
            
            
            $load_id = array();
            if(isset($this->filters['unit_id']) && $this->filters['unit_id'] != ''){
                $kode_id = new Criteria();
                $kode_id->add(DinasMasterKegiatanPeer::UNIT_ID, $this->filters['unit_id'], Criteria::EQUAL);
                if($menu == 'Pendapatan'){
                    $kode_id->add(DinasMasterKegiatanPeer::IS_BTL,TRUE);
                }else{
                    $kode_id->add(DinasMasterKegiatanPeer::IS_BTL,FALSE);
                }
                $kode_id->addAscendingOrderByColumn('CAST('.DinasMasterKegiatanPeer::KODE_KEGIATAN.' AS INT)');
                $load_id = DinasMasterKegiatanPeer::doSelect($kode_id);
            }
            $this->load_id = $load_id;
            
            $load_user_id = array();
            if(isset($this->filters['unit_id']) && $this->filters['unit_id'] != ''){
                $kode_user_id = new Criteria();
                $kode_user_id->add(DinasMasterKegiatanPeer::UNIT_ID, $this->filters['unit_id'], Criteria::EQUAL);
                if($menu == 'Pendapatan'){
                    $kode_user_id->add(DinasMasterKegiatanPeer::IS_BTL,TRUE);
                    $btl = " is_btl=true ";
                }else{
                    $kode_user_id->add(DinasMasterKegiatanPeer::IS_BTL,FALSE);
                    $btl = " is_btl=false ";
                }
                $kode_user_id->add(DinasMasterKegiatanPeer::USER_ID, NULL, Criteria::ISNOTNULL);
                $kode_user_id->setDistinct(DinasMasterKegiatanPeer::USER_ID);
                $kode_user_id->addAscendingOrderByColumn(DinasMasterKegiatanPeer::USER_ID);
                $load_user_id = DinasMasterKegiatanPeer::doSelect($kode_user_id);
            }
            $this->load_user_id = $load_user_id;

            $c->addAscendingOrderByColumn('CAST('.DinasMasterKegiatanPeer::KODE_KEGIATAN.' AS INT)');
        }

        $this->addFiltersCriteriaRevisi($c, isset($this->filters['tahap']) ? $this->filters['tahap'] : null, $menu);

        $this->pager->setCriteria($c);
        $this->pager->setPage($this->getRequestParameter('page', 1));
        $this->pager->init();
    }
    //yogie
    public function executeEditRevisi() {
        if ($this->getRequestParameter('unit_id')) {
            $unit_id = $this->getRequestParameter('unit_id');
            $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
            $this->tahap = $tahap = $this->getRequestParameter('tahap');

            if ($tahap == 'pakbp') {
                $c = new Criteria();
                $c->add(PakBukuPutihSubtitleIndikatorPeer::UNIT_ID, $unit_id);
                $c->add(PakBukuPutihSubtitleIndikatorPeer::KEGIATAN_CODE, $kode_kegiatan);
                $c->addAscendingOrderByColumn(PakBukuPutihSubtitleIndikatorPeer::PRIORITAS);
                $c->addAscendingOrderByColumn(PakBukuPutihSubtitleIndikatorPeer::SUBTITLE);
                $rs_subtitle = PakBukuPutihSubtitleIndikatorPeer::doSelect($c);
                $tabel_dpn = 'pak_bukuputih_';
            } elseif ($tahap == 'pakbb') {
                $c = new Criteria();
                $c->add(PakBukuBiruSubtitleIndikatorPeer::UNIT_ID, $unit_id);
                $c->add(PakBukuBiruSubtitleIndikatorPeer::KEGIATAN_CODE, $kode_kegiatan);
                $c->addAscendingOrderByColumn(PakBukuBiruSubtitleIndikatorPeer::PRIORITAS);
                $c->addAscendingOrderByColumn(PakBukuBiruSubtitleIndikatorPeer::SUBTITLE);
                $rs_subtitle = PakBukuBiruSubtitleIndikatorPeer::doSelect($c);
                $tabel_dpn = 'pak_bukubiru_';
            } elseif ($tahap == 'murni') {
                $c = new Criteria();
                $c->add(MurniSubtitleIndikatorPeer::UNIT_ID, $unit_id);
                $c->add(MurniSubtitleIndikatorPeer::KEGIATAN_CODE, $kode_kegiatan);
                $c->add(MurniSubtitleIndikatorPeer::SUBTITLE, 'Penunjang Kinerja%', Criteria::NOT_ILIKE);
                $c->addAscendingOrderByColumn(MurniSubtitleIndikatorPeer::PRIORITAS);
                $c->addAscendingOrderByColumn(MurniSubtitleIndikatorPeer::SUBTITLE);
                $rs_subtitle = MurniSubtitleIndikatorPeer::doSelect($c);
                $tabel_dpn = 'murni_';
            } elseif ($tahap == 'murnibp') {
                $c = new Criteria();
                $c->add(MurniBukuPutihSubtitleIndikatorPeer::UNIT_ID, $unit_id);
                $c->add(MurniBukuPutihSubtitleIndikatorPeer::KEGIATAN_CODE, $kode_kegiatan);
                $c->addAscendingOrderByColumn(MurniBukuPutihSubtitleIndikatorPeer::PRIORITAS);
                $c->addAscendingOrderByColumn(MurniBukuPutihSubtitleIndikatorPeer::SUBTITLE);
                $rs_subtitle = MurniBukuPutihSubtitleIndikatorPeer::doSelect($c);
                $tabel_dpn = 'murni_bukuputih_';
            } elseif ($tahap == 'murnibb') {
                $c = new Criteria();
                $c->add(MurniBukuBiruSubtitleIndikatorPeer::UNIT_ID, $unit_id);
                $c->add(MurniBukuBiruSubtitleIndikatorPeer::KEGIATAN_CODE, $kode_kegiatan);
                $c->addAscendingOrderByColumn(MurniBukuBiruSubtitleIndikatorPeer::PRIORITAS);
                $c->addAscendingOrderByColumn(MurniBukuBiruSubtitleIndikatorPeer::SUBTITLE);
                $rs_subtitle = MurniBukuBiruSubtitleIndikatorPeer::doSelect($c);
                $tabel_dpn = 'murni_bukubiru_';
            } elseif ($tahap == 'revisi1') {
                $c = new Criteria();
                $c->add(Revisi1SubtitleIndikatorPeer::UNIT_ID, $unit_id);
                $c->add(Revisi1SubtitleIndikatorPeer::KEGIATAN_CODE, $kode_kegiatan);
                $c->add(Revisi1SubtitleIndikatorPeer::SUBTITLE, 'Penunjang Kinerja%', Criteria::NOT_ILIKE);
                $c->addAscendingOrderByColumn(Revisi1SubtitleIndikatorPeer::PRIORITAS);
                $c->addAscendingOrderByColumn(Revisi1SubtitleIndikatorPeer::SUBTITLE);
                $rs_subtitle = Revisi1SubtitleIndikatorPeer::doSelect($c);
                $tabel_dpn = 'revisi1_';
            } elseif ($tahap == 'revisi1_1') {
                $c = new Criteria();
                $c->add(Revisi1bSubtitleIndikatorPeer::UNIT_ID, $unit_id);
                $c->add(Revisi1bSubtitleIndikatorPeer::KEGIATAN_CODE, $kode_kegiatan);
                $c->addAscendingOrderByColumn(Revisi1bSubtitleIndikatorPeer::PRIORITAS);
                $c->addAscendingOrderByColumn(Revisi1bSubtitleIndikatorPeer::SUBTITLE);
                $rs_subtitle = Revisi1bSubtitleIndikatorPeer::doSelect($c);
                $tabel_dpn = 'revisi1_1_';
            } elseif ($tahap == 'revisi2') {
                $c = new Criteria();
                $c->add(Revisi2SubtitleIndikatorPeer::UNIT_ID, $unit_id);
                $c->add(Revisi2SubtitleIndikatorPeer::KEGIATAN_CODE, $kode_kegiatan);
                $c->add(Revisi2SubtitleIndikatorPeer::SUBTITLE, 'Penunjang Kinerja%', Criteria::NOT_ILIKE);
                $c->addAscendingOrderByColumn(Revisi2SubtitleIndikatorPeer::PRIORITAS);
                $c->addAscendingOrderByColumn(Revisi2SubtitleIndikatorPeer::SUBTITLE);
                $rs_subtitle = Revisi2SubtitleIndikatorPeer::doSelect($c);
                $tabel_dpn = 'revisi2_';
            } elseif ($tahap == 'revisi2_1') { 
                $c = new Criteria();
                $c->add(Revisi21SubtitleIndikatorPeer::UNIT_ID, $unit_id);
                $c->add(Revisi21SubtitleIndikatorPeer::KEGIATAN_CODE, $kode_kegiatan);
                $c->addAscendingOrderByColumn(Revisi21SubtitleIndikatorPeer::PRIORITAS);
                $c->addAscendingOrderByColumn(Revisi21SubtitleIndikatorPeer::SUBTITLE);
                $rs_subtitle = Revisi21SubtitleIndikatorPeer::doSelect($c);
                $tabel_dpn = 'revisi2_1_';
            }  elseif ($tahap == 'revisi2_2') {
                $c = new Criteria();
                $c->add(Revisi22SubtitleIndikatorPeer::UNIT_ID, $unit_id);
                $c->add(Revisi22SubtitleIndikatorPeer::KEGIATAN_CODE, $kode_kegiatan);
                $c->addAscendingOrderByColumn(Revisi22SubtitleIndikatorPeer::PRIORITAS);
                $c->addAscendingOrderByColumn(Revisi22SubtitleIndikatorPeer::SUBTITLE);
                $rs_subtitle = Revisi22SubtitleIndikatorPeer::doSelect($c);
                $tabel_dpn = 'revisi2_2_';
            } elseif ($tahap == 'revisi3') {
                $c = new Criteria();
                $c->add(Revisi3SubtitleIndikatorPeer::UNIT_ID, $unit_id);
                $c->add(Revisi3SubtitleIndikatorPeer::KEGIATAN_CODE, $kode_kegiatan);
                $c->add(Revisi3SubtitleIndikatorPeer::SUBTITLE, 'Penunjang Kinerja%', Criteria::NOT_ILIKE);
                $c->addAscendingOrderByColumn(Revisi3SubtitleIndikatorPeer::PRIORITAS);
                $c->addAscendingOrderByColumn(Revisi3SubtitleIndikatorPeer::SUBTITLE);
                $rs_subtitle = Revisi3SubtitleIndikatorPeer::doSelect($c);
                $tabel_dpn = 'revisi3_';
            } elseif ($tahap == 'revisi3_1') {
                $c = new Criteria();
                $c->add(Revisi31SubtitleIndikatorPeer::UNIT_ID, $unit_id);
                $c->add(Revisi31SubtitleIndikatorPeer::KEGIATAN_CODE, $kode_kegiatan);
                $c->addAscendingOrderByColumn(Revisi31SubtitleIndikatorPeer::PRIORITAS);
                $c->addAscendingOrderByColumn(Revisi31SubtitleIndikatorPeer::SUBTITLE);
                $rs_subtitle = Revisi31SubtitleIndikatorPeer::doSelect($c);
                $tabel_dpn = 'revisi3_1_';
            } elseif ($tahap == 'revisi4') {
                $c = new Criteria();
                $c->add(Revisi4SubtitleIndikatorPeer::UNIT_ID, $unit_id);
                $c->add(Revisi4SubtitleIndikatorPeer::KEGIATAN_CODE, $kode_kegiatan);
                $c->addAscendingOrderByColumn(Revisi4SubtitleIndikatorPeer::PRIORITAS);
                $c->addAscendingOrderByColumn(Revisi4SubtitleIndikatorPeer::SUBTITLE);
                $rs_subtitle = Revisi4SubtitleIndikatorPeer::doSelect($c);
                $tabel_dpn = 'revisi4_';
            } elseif ($tahap == 'revisi5') {
                $c = new Criteria();
                $c->add(Revisi5SubtitleIndikatorPeer::UNIT_ID, $unit_id);
                $c->add(Revisi5SubtitleIndikatorPeer::KEGIATAN_CODE, $kode_kegiatan);
                $c->addAscendingOrderByColumn(Revisi5SubtitleIndikatorPeer::PRIORITAS);
                $c->addAscendingOrderByColumn(Revisi5SubtitleIndikatorPeer::SUBTITLE);
                $rs_subtitle = Revisi5SubtitleIndikatorPeer::doSelect($c);
                $tabel_dpn = 'revisi5_';
            } elseif ($tahap == 'revisi6') {
                $c = new Criteria();
                $c->add(Revisi6SubtitleIndikatorPeer::UNIT_ID, $unit_id);
                $c->add(Revisi6SubtitleIndikatorPeer::KEGIATAN_CODE, $kode_kegiatan);
                $c->addAscendingOrderByColumn(Revisi6SubtitleIndikatorPeer::PRIORITAS);
                $c->addAscendingOrderByColumn(Revisi6SubtitleIndikatorPeer::SUBTITLE);
                $rs_subtitle = Revisi6SubtitleIndikatorPeer::doSelect($c);
                $tabel_dpn = 'revisi6_';
            } elseif ($tahap == 'revisi7') {
                $c = new Criteria();
                $c->add(Revisi7SubtitleIndikatorPeer::UNIT_ID, $unit_id);
                $c->add(Revisi7SubtitleIndikatorPeer::KEGIATAN_CODE, $kode_kegiatan);
                $c->addAscendingOrderByColumn(Revisi7SubtitleIndikatorPeer::PRIORITAS);
                $c->addAscendingOrderByColumn(Revisi7SubtitleIndikatorPeer::SUBTITLE);
                $rs_subtitle = Revisi7SubtitleIndikatorPeer::doSelect($c);
                $tabel_dpn = 'revisi7_';
            } elseif ($tahap == 'revisi8') {
                $c = new Criteria();
                $c->add(Revisi8SubtitleIndikatorPeer::UNIT_ID, $unit_id);
                $c->add(Revisi8SubtitleIndikatorPeer::KEGIATAN_CODE, $kode_kegiatan);
                $c->addAscendingOrderByColumn(Revisi8SubtitleIndikatorPeer::PRIORITAS);
                $c->addAscendingOrderByColumn(Revisi8SubtitleIndikatorPeer::SUBTITLE);
                $rs_subtitle = Revisi8SubtitleIndikatorPeer::doSelect($c);
                $tabel_dpn = 'revisi8_';
            } elseif ($tahap == 'revisi9') {
                $c = new Criteria();
                $c->add(Revisi9SubtitleIndikatorPeer::UNIT_ID, $unit_id);
                $c->add(Revisi9SubtitleIndikatorPeer::KEGIATAN_CODE, $kode_kegiatan);
                $c->addAscendingOrderByColumn(Revisi9SubtitleIndikatorPeer::PRIORITAS);
                $c->addAscendingOrderByColumn(Revisi9SubtitleIndikatorPeer::SUBTITLE);
                $rs_subtitle = Revisi9SubtitleIndikatorPeer::doSelect($c);
                $tabel_dpn = 'revisi9_';
            } elseif ($tahap == 'revisi10') {
                $c = new Criteria();
                $c->add(Revisi10SubtitleIndikatorPeer::UNIT_ID, $unit_id);
                $c->add(Revisi10SubtitleIndikatorPeer::KEGIATAN_CODE, $kode_kegiatan);
                $c->addAscendingOrderByColumn(Revisi10SubtitleIndikatorPeer::PRIORITAS);
                $c->addAscendingOrderByColumn(Revisi10SubtitleIndikatorPeer::SUBTITLE);
                $rs_subtitle = Revisi10SubtitleIndikatorPeer::doSelect($c);
                $tabel_dpn = 'revisi10_';
            } elseif ($tahap == 'rkua') {
                $c = new Criteria();
                $c->add(RkuaSubtitleIndikatorPeer::UNIT_ID, $unit_id);
                $c->add(RkuaSubtitleIndikatorPeer::KEGIATAN_CODE, $kode_kegiatan);
                $c->addAscendingOrderByColumn(RkuaSubtitleIndikatorPeer::PRIORITAS);
                $c->addAscendingOrderByColumn(RkuaSubtitleIndikatorPeer::SUBTITLE);
                $rs_subtitle = RkuaSubtitleIndikatorPeer::doSelect($c);
                $tabel_dpn = 'rkua_';
            } else {
                $c = new Criteria();
                $c->add(DinasSubtitleIndikatorPeer::UNIT_ID, $unit_id);
                $c->add(DinasSubtitleIndikatorPeer::KEGIATAN_CODE, $kode_kegiatan);
                $c->add(DinasSubtitleIndikatorPeer::SUBTITLE, 'Penunjang Kinerja%', Criteria::NOT_ILIKE);
                $c->addAscendingOrderByColumn(DinasSubtitleIndikatorPeer::PRIORITAS);
                $c->addAscendingOrderByColumn(DinasSubtitleIndikatorPeer::SUBTITLE);
                $rs_subtitle = DinasSubtitleIndikatorPeer::doSelect($c);
                $tabel_dpn = 'dinas_';
            }

            $this->rs_subtitle = $rs_subtitle;
            $this->rinciandetail = '';
            $this->rs_rinciandetail = '';

            if (sfConfig::get('app_fasilitas_warningRekening') == 'buka') {
//untuk warning
                $query = "select sum(nilai_anggaran) as tot
                              from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "rincian_detail
                              where status_hapus=FALSE and unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rekening_code like '5.2.1%' and status_hapus=false";
                $con = Propel::getConnection();
                $statement = $con->prepareStatement($query);
                $rs = $statement->executeQuery();
                while ($rs->next())
                    $total_menjadi_1 = $rs->getString('tot');

                $query = "select sum(nilai_anggaran) as tot
                              from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "rincian_detail
                              where status_hapus=FALSE and unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rekening_code like '5.2.2%' and status_hapus=false";
                $con = Propel::getConnection();
                $statement = $con->prepareStatement($query);
                $rs = $statement->executeQuery();
                while ($rs->next())
                    $total_menjadi_2 = $rs->getString('tot');

                $query = "select sum(nilai_anggaran) as tot
                              from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "rincian_detail
                              where status_hapus=FALSE and unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rekening_code like '5.2.3%' and status_hapus=false";
                $con = Propel::getConnection();
                $statement = $con->prepareStatement($query);
                $rs = $statement->executeQuery();
                while ($rs->next())
                    $total_menjadi_3 = $rs->getString('tot');

                if ($unit_id == '9999') {
                    $query = "select sum(nilai_anggaran) as tot
                              from " . sfConfig::get('app_default_schema') . ".titiknol_rincian_detail
                              where status_hapus=FALSE and unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rekening_code like '5.2.1%' and status_hapus=false";

                    $con = Propel::getConnection();
                    $statement = $con->prepareStatement($query);
                    $rs = $statement->executeQuery();
                    while ($rs->next())
                        $total_semula_1 = $rs->getString('tot');

                    $query = "select sum(nilai_anggaran) as tot
                              from " . sfConfig::get('app_default_schema') . ".titiknol_rincian_detail
                              where status_hapus=FALSE and unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rekening_code like '5.2.2%' and status_hapus=false";

                    $con = Propel::getConnection();
                    $statement = $con->prepareStatement($query);
                    $rs = $statement->executeQuery();
                    while ($rs->next())
                        $total_semula_2 = $rs->getString('tot');

                    $query = "select sum(nilai_anggaran) as tot
                              from " . sfConfig::get('app_default_schema') . ".titiknol_rincian_detail
                              where status_hapus=FALSE and unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rekening_code like '5.2.3%' and status_hapus=false";

                    $con = Propel::getConnection();
                    $statement = $con->prepareStatement($query);
                    $rs = $statement->executeQuery();
                    while ($rs->next())
                        $total_semula_3 = $rs->getString('tot');
                } else {
                    $query = "select sum(nilai_anggaran) as tot
                              from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "rincian_detail
                              where status_hapus=FALSE and unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rekening_code like '5.2.1%' and status_hapus=false";

                    $con = Propel::getConnection();
                    $statement = $con->prepareStatement($query);
                    $rs = $statement->executeQuery();
                    while ($rs->next())
                        $total_semula_1 = $rs->getString('tot');

                    $query = "select sum(nilai_anggaran) as tot
                              from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "rincian_detail
                              where status_hapus=FALSE and unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rekening_code like '5.2.2%' and status_hapus=false";

                    $con = Propel::getConnection();
                    $statement = $con->prepareStatement($query);
                    $rs = $statement->executeQuery();
                    while ($rs->next())
                        $total_semula_2 = $rs->getString('tot');

                    $query = "select sum(nilai_anggaran) as tot
                              from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "rincian_detail
                              where status_hapus=FALSE and unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rekening_code like '5.2.3%' and status_hapus=false";

                    $con = Propel::getConnection();
                    $statement = $con->prepareStatement($query);
                    $rs = $statement->executeQuery();
                    while ($rs->next())
                        $total_semula_3 = $rs->getString('tot');
                }



                $this->selisih_1 = number_format($total_menjadi_1 - $total_semula_1, 0, ',', '.');
                $this->selisih_2 = number_format($total_menjadi_2 - $total_semula_2, 0, ',', '.');
                $this->selisih_3 = number_format($total_menjadi_3 - $total_semula_3, 0, ',', '.');

                $this->total_menjadi_1 = number_format($total_menjadi_1, 0, ',', '.');
                $this->total_menjadi_2 = number_format($total_menjadi_2, 0, ',', '.');
                $this->total_menjadi_3 = number_format($total_menjadi_3, 0, ',', '.');

                $this->total_semula_1 = number_format($total_semula_1, 0, ',', '.');
                $this->total_semula_2 = number_format($total_semula_2, 0, ',', '.');
                $this->total_semula_3 = number_format($total_semula_3, 0, ',', '.');
//end of warning
            }
        } else {
            $this->forward404();
        }
    }

    public function executeGetPekerjaansRevisi() {
        if ($this->getRequestParameter('id')) {
            $sub_id = $this->getRequestParameter('id');
            $this->tahap = $tahap = $this->getRequestParameter('tahap');

            if ($tahap == 'pakbp') {
                $c = new Criteria();
                $c->add(PakBukuPutihSubtitleIndikatorPeer::SUB_ID, $sub_id);
                $c->add(PakBukuPutihSubtitleIndikatorPeer::PRIORITAS, 0, Criteria::NOT_EQUAL);
                $rs_subtitle = PakBukuPutihSubtitleIndikatorPeer::doSelectOne($c);
                if ($rs_subtitle) {
                    $unit_id = $rs_subtitle->getUnitId();
                    $kegiatan_code = $rs_subtitle->getKegiatanCode();
                    $subtitle = $rs_subtitle->getSubtitle();
                    $nama_subtitle = trim($subtitle);
                }

                $c_rincianDetail = new Criteria();
                $c_rincianDetail->add(PakBukuPutihRincianDetailPeer::UNIT_ID, $unit_id);
                $c_rincianDetail->add(PakBukuPutihRincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
                $c_rincianDetail->add(PakBukuPutihRincianDetailPeer::SUBTITLE, $nama_subtitle, Criteria::ILIKE);
                $c_rincianDetail->add(PakBukuPutihRincianDetailPeer::STATUS_HAPUS, false);
                $c_rincianDetail->add(PakBukuPutihRincianDetailPeer::TAHUN, sfConfig::get('app_tahun_default'));
//$c_rincianDetail->addAscendingOrderByColumn(PakBukuPutihRincianDetailPeer::SUB);
                $c_rincianDetail->addAscendingOrderByColumn(PakBukuPutihRincianDetailPeer::KODE_SUB);
                $c_rincianDetail->addAscendingOrderByColumn(PakBukuPutihRincianDetailPeer::REKENING_CODE);
                $c_rincianDetail->addDescendingOrderByColumn(PakBukuPutihRincianDetailPeer::NILAI_ANGGARAN);

                $c_rincianDetail->addAscendingOrderByColumn(PakBukuPutihRincianDetailPeer::LOKASI_KECAMATAN);
                $c_rincianDetail->addAscendingOrderByColumn(PakBukuPutihRincianDetailPeer::LOKASI_KELURAHAN);
                $c_rincianDetail->addAscendingOrderByColumn(PakBukuPutihRincianDetailPeer::KOMPONEN_NAME);
                $rs_rd = PakBukuPutihRincianDetailPeer::doSelect($c_rincianDetail);
            } elseif ($tahap == 'pakbb') {
                $c = new Criteria();
                $c->add(PakBukuBiruSubtitleIndikatorPeer::SUB_ID, $sub_id);
                $c->add(PakBukuBiruSubtitleIndikatorPeer::PRIORITAS, 0, Criteria::NOT_EQUAL);
                $rs_subtitle = PakBukuBiruSubtitleIndikatorPeer::doSelectOne($c);
                if ($rs_subtitle) {
                    $unit_id = $rs_subtitle->getUnitId();
                    $kegiatan_code = $rs_subtitle->getKegiatanCode();
                    $subtitle = $rs_subtitle->getSubtitle();
                    $nama_subtitle = trim($subtitle);
                }

                $c_rincianDetail = new Criteria();
                $c_rincianDetail->add(PakBukuBiruRincianDetailPeer::UNIT_ID, $unit_id);
                $c_rincianDetail->add(PakBukuBiruRincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
                $c_rincianDetail->add(PakBukuBiruRincianDetailPeer::SUBTITLE, $nama_subtitle, Criteria::ILIKE);
                $c_rincianDetail->add(PakBukuBiruRincianDetailPeer::STATUS_HAPUS, false);
                $c_rincianDetail->add(PakBukuBiruRincianDetailPeer::TAHUN, sfConfig::get('app_tahun_default'));
//$c_rincianDetail->addAscendingOrderByColumn(PakBukuBiruRincianDetailPeer::SUB);
                $c_rincianDetail->addAscendingOrderByColumn(PakBukuBiruRincianDetailPeer::KODE_SUB);
                $c_rincianDetail->addAscendingOrderByColumn(PakBukuBiruRincianDetailPeer::REKENING_CODE);
                $c_rincianDetail->addDescendingOrderByColumn(PakBukuBiruRincianDetailPeer::NILAI_ANGGARAN);

                $c_rincianDetail->addAscendingOrderByColumn(PakBukuBiruRincianDetailPeer::LOKASI_KECAMATAN);
                $c_rincianDetail->addAscendingOrderByColumn(PakBukuBiruRincianDetailPeer::LOKASI_KELURAHAN);
                $c_rincianDetail->addAscendingOrderByColumn(PakBukuBiruRincianDetailPeer::KOMPONEN_NAME);
                $rs_rd = PakBukuBiruRincianDetailPeer::doSelect($c_rincianDetail);
            } elseif ($tahap == 'murni') {
                $c = new Criteria();
                $c->add(MurniSubtitleIndikatorPeer::SUB_ID, $sub_id);
                $c->add(MurniSubtitleIndikatorPeer::PRIORITAS, 0, Criteria::NOT_EQUAL);
                $rs_subtitle = MurniSubtitleIndikatorPeer::doSelectOne($c);
                if ($rs_subtitle) {
                    $unit_id = $rs_subtitle->getUnitId();
                    $kegiatan_code = $rs_subtitle->getKegiatanCode();
                    $subtitle = $rs_subtitle->getSubtitle();
                    $nama_subtitle = trim($subtitle);
                }

                $c_rincianDetail = new Criteria();
                $c_rincianDetail->add(MurniRincianDetailPeer::UNIT_ID, $unit_id);
                $c_rincianDetail->add(MurniRincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
                $c_rincianDetail->add(MurniRincianDetailPeer::SUBTITLE, $nama_subtitle, Criteria::ILIKE);
                $c_rincianDetail->add(MurniRincianDetailPeer::STATUS_HAPUS, false);
                $c_rincianDetail->add(MurniRincianDetailPeer::TAHUN, sfConfig::get('app_tahun_default'));
//$c_rincianDetail->addAscendingOrderByColumn(MurniRincianDetailPeer::SUB);
                $c_rincianDetail->addAscendingOrderByColumn(MurniRincianDetailPeer::KODE_SUB);
                $c_rincianDetail->addAscendingOrderByColumn(MurniRincianDetailPeer::REKENING_CODE);
                $c_rincianDetail->addDescendingOrderByColumn(MurniRincianDetailPeer::NILAI_ANGGARAN);

                $c_rincianDetail->addAscendingOrderByColumn(MurniRincianDetailPeer::LOKASI_KECAMATAN);
                $c_rincianDetail->addAscendingOrderByColumn(MurniRincianDetailPeer::LOKASI_KELURAHAN);
                $c_rincianDetail->addAscendingOrderByColumn(MurniRincianDetailPeer::KOMPONEN_NAME);
                $rs_rd = MurniRincianDetailPeer::doSelect($c_rincianDetail);
            } elseif ($tahap == 'murnibp') {
                $c = new Criteria();
                $c->add(MurniBukuPutihSubtitleIndikatorPeer::SUB_ID, $sub_id);
                $c->add(MurniBukuPutihSubtitleIndikatorPeer::PRIORITAS, 0, Criteria::NOT_EQUAL);
                $rs_subtitle = MurniBukuPutihSubtitleIndikatorPeer::doSelectOne($c);
                if ($rs_subtitle) {
                    $unit_id = $rs_subtitle->getUnitId();
                    $kegiatan_code = $rs_subtitle->getKegiatanCode();
                    $subtitle = $rs_subtitle->getSubtitle();
                    $nama_subtitle = trim($subtitle);
                }

                $c_rincianDetail = new Criteria();
                $c_rincianDetail->add(MurniBukuPutihRincianDetailPeer::UNIT_ID, $unit_id);
                $c_rincianDetail->add(MurniBukuPutihRincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
                $c_rincianDetail->add(MurniBukuPutihRincianDetailPeer::SUBTITLE, $nama_subtitle, Criteria::ILIKE);
                $c_rincianDetail->add(MurniBukuPutihRincianDetailPeer::STATUS_HAPUS, false);
                $c_rincianDetail->add(MurniBukuPutihRincianDetailPeer::TAHUN, sfConfig::get('app_tahun_default'));
//$c_rincianDetail->addAscendingOrderByColumn(MurniBukuPutihRincianDetailPeer::SUB);
                $c_rincianDetail->addAscendingOrderByColumn(MurniBukuPutihRincianDetailPeer::KODE_SUB);
                $c_rincianDetail->addAscendingOrderByColumn(MurniBukuPutihRincianDetailPeer::REKENING_CODE);
                $c_rincianDetail->addDescendingOrderByColumn(MurniBukuPutihRincianDetailPeer::NILAI_ANGGARAN);

                $c_rincianDetail->addAscendingOrderByColumn(MurniBukuPutihRincianDetailPeer::LOKASI_KECAMATAN);
                $c_rincianDetail->addAscendingOrderByColumn(MurniBukuPutihRincianDetailPeer::LOKASI_KELURAHAN);
                $c_rincianDetail->addAscendingOrderByColumn(MurniBukuPutihRincianDetailPeer::KOMPONEN_NAME);
                $rs_rd = MurniBukuPutihRincianDetailPeer::doSelect($c_rincianDetail);
            } elseif ($tahap == 'murnibb') {
                $c = new Criteria();
                $c->add(MurniBukuBiruSubtitleIndikatorPeer::SUB_ID, $sub_id);
                $c->add(MurniBukuBiruSubtitleIndikatorPeer::PRIORITAS, 0, Criteria::NOT_EQUAL);
                $rs_subtitle = MurniBukuBiruSubtitleIndikatorPeer::doSelectOne($c);
                if ($rs_subtitle) {
                    $unit_id = $rs_subtitle->getUnitId();
                    $kegiatan_code = $rs_subtitle->getKegiatanCode();
                    $subtitle = $rs_subtitle->getSubtitle();
                    $nama_subtitle = trim($subtitle);
                }

                $c_rincianDetail = new Criteria();
                $c_rincianDetail->add(MurniBukuBiruRincianDetailPeer::UNIT_ID, $unit_id);
                $c_rincianDetail->add(MurniBukuBiruRincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
                $c_rincianDetail->add(MurniBukuBiruRincianDetailPeer::SUBTITLE, $nama_subtitle, Criteria::ILIKE);
                $c_rincianDetail->add(MurniBukuBiruRincianDetailPeer::STATUS_HAPUS, false);
                $c_rincianDetail->add(MurniBukuBiruRincianDetailPeer::TAHUN, sfConfig::get('app_tahun_default'));
//$c_rincianDetail->addAscendingOrderByColumn(MurniBukuBiruRincianDetailPeer::SUB);
                $c_rincianDetail->addAscendingOrderByColumn(MurniBukuBiruRincianDetailPeer::KODE_SUB);
                $c_rincianDetail->addAscendingOrderByColumn(MurniBukuBiruRincianDetailPeer::REKENING_CODE);
                $c_rincianDetail->addDescendingOrderByColumn(MurniBukuBiruRincianDetailPeer::NILAI_ANGGARAN);

                $c_rincianDetail->addAscendingOrderByColumn(MurniBukuBiruRincianDetailPeer::LOKASI_KECAMATAN);
                $c_rincianDetail->addAscendingOrderByColumn(MurniBukuBiruRincianDetailPeer::LOKASI_KELURAHAN);
                $c_rincianDetail->addAscendingOrderByColumn(MurniBukuBiruRincianDetailPeer::KOMPONEN_NAME);
                $rs_rd = MurniBukuBiruRincianDetailPeer::doSelect($c_rincianDetail);
            } elseif ($tahap == 'revisi1') {
                $c = new Criteria();
                $c->add(Revisi1SubtitleIndikatorPeer::SUB_ID, $sub_id);
                $c->add(Revisi1SubtitleIndikatorPeer::PRIORITAS, 0, Criteria::NOT_EQUAL);
                $rs_subtitle = Revisi1SubtitleIndikatorPeer::doSelectOne($c);
                if ($rs_subtitle) {
                    $unit_id = $rs_subtitle->getUnitId();
                    $kegiatan_code = $rs_subtitle->getKegiatanCode();
                    $subtitle = $rs_subtitle->getSubtitle();
                    $nama_subtitle = trim($subtitle);
                }

                $c_rincianDetail = new Criteria();
                $c_rincianDetail->add(Revisi1RincianDetailPeer::UNIT_ID, $unit_id);
                $c_rincianDetail->add(Revisi1RincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
                $c_rincianDetail->add(Revisi1RincianDetailPeer::SUBTITLE, $nama_subtitle, Criteria::ILIKE);
                $c_rincianDetail->add(Revisi1RincianDetailPeer::STATUS_HAPUS, false);
                $c_rincianDetail->add(Revisi1RincianDetailPeer::TAHUN, sfConfig::get('app_tahun_default'));
//$c_rincianDetail->addAscendingOrderByColumn(Revisi1RincianDetailPeer::SUB);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi1RincianDetailPeer::KODE_SUB);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi1RincianDetailPeer::REKENING_CODE);
                $c_rincianDetail->addDescendingOrderByColumn(Revisi1RincianDetailPeer::NILAI_ANGGARAN);

                $c_rincianDetail->addAscendingOrderByColumn(Revisi1RincianDetailPeer::LOKASI_KECAMATAN);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi1RincianDetailPeer::LOKASI_KELURAHAN);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi1RincianDetailPeer::KOMPONEN_NAME);
                $rs_rd = Revisi1RincianDetailPeer::doSelect($c_rincianDetail);
            } elseif ($tahap == 'revisi1_1') {
                $c = new Criteria();
                $c->add(Revisi1bSubtitleIndikatorPeer::SUB_ID, $sub_id);
                $c->add(Revisi1bSubtitleIndikatorPeer::PRIORITAS, 0, Criteria::NOT_EQUAL);
                $rs_subtitle = Revisi1bSubtitleIndikatorPeer::doSelectOne($c);
                if ($rs_subtitle) {
                    $unit_id = $rs_subtitle->getUnitId();
                    $kegiatan_code = $rs_subtitle->getKegiatanCode();
                    $subtitle = $rs_subtitle->getSubtitle();
                    $nama_subtitle = trim($subtitle);
                }

                $c_rincianDetail = new Criteria();
                $c_rincianDetail->add(Revisi1bRincianDetailPeer::UNIT_ID, $unit_id);
                $c_rincianDetail->add(Revisi1bRincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
                $c_rincianDetail->add(Revisi1bRincianDetailPeer::SUBTITLE, $nama_subtitle, Criteria::ILIKE);
                $c_rincianDetail->add(Revisi1bRincianDetailPeer::STATUS_HAPUS, false);
                $c_rincianDetail->add(Revisi1bRincianDetailPeer::TAHUN, sfConfig::get('app_tahun_default'));
//$c_rincianDetail->addAscendingOrderByColumn(Revisi1RincianDetailPeer::SUB);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi1bRincianDetailPeer::KODE_SUB);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi1bRincianDetailPeer::REKENING_CODE);
                $c_rincianDetail->addDescendingOrderByColumn(Revisi1bRincianDetailPeer::NILAI_ANGGARAN);

                $c_rincianDetail->addAscendingOrderByColumn(Revisi1bRincianDetailPeer::LOKASI_KECAMATAN);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi1bRincianDetailPeer::LOKASI_KELURAHAN);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi1bRincianDetailPeer::KOMPONEN_NAME);
                $rs_rd = Revisi1bRincianDetailPeer::doSelect($c_rincianDetail);
            } elseif ($tahap == 'revisi2') {
                $c = new Criteria();
                $c->add(Revisi2SubtitleIndikatorPeer::SUB_ID, $sub_id);
                $c->add(Revisi2SubtitleIndikatorPeer::PRIORITAS, 0, Criteria::NOT_EQUAL);
                $rs_subtitle = Revisi2SubtitleIndikatorPeer::doSelectOne($c);
                if ($rs_subtitle) {
                    $unit_id = $rs_subtitle->getUnitId();
                    $kegiatan_code = $rs_subtitle->getKegiatanCode();
                    $subtitle = $rs_subtitle->getSubtitle();
                    $nama_subtitle = trim($subtitle);
                }

                $c_rincianDetail = new Criteria();
                $c_rincianDetail->add(Revisi2RincianDetailPeer::UNIT_ID, $unit_id);
                $c_rincianDetail->add(Revisi2RincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
                $c_rincianDetail->add(Revisi2RincianDetailPeer::SUBTITLE, $nama_subtitle, Criteria::ILIKE);
                $c_rincianDetail->add(Revisi2RincianDetailPeer::STATUS_HAPUS, false);
                $c_rincianDetail->add(Revisi2RincianDetailPeer::TAHUN, sfConfig::get('app_tahun_default'));
//$c_rincianDetail->addAscendingOrderByColumn(Revisi2RincianDetailPeer::SUB);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi2RincianDetailPeer::KODE_SUB);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi2RincianDetailPeer::REKENING_CODE);
                $c_rincianDetail->addDescendingOrderByColumn(Revisi2RincianDetailPeer::NILAI_ANGGARAN);

                $c_rincianDetail->addAscendingOrderByColumn(Revisi2RincianDetailPeer::LOKASI_KECAMATAN);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi2RincianDetailPeer::LOKASI_KELURAHAN);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi2RincianDetailPeer::KOMPONEN_NAME);
                $rs_rd = Revisi2RincianDetailPeer::doSelect($c_rincianDetail);
            }  elseif ($tahap == 'revisi2_1') {
                $c = new Criteria();
                $c->add(Revisi21SubtitleIndikatorPeer::SUB_ID, $sub_id);
                $c->add(Revisi21SubtitleIndikatorPeer::PRIORITAS, 0, Criteria::NOT_EQUAL);
                $rs_subtitle = Revisi21SubtitleIndikatorPeer::doSelectOne($c);
                if ($rs_subtitle) {
                    $unit_id = $rs_subtitle->getUnitId();
                    $kegiatan_code = $rs_subtitle->getKegiatanCode();
                    $subtitle = $rs_subtitle->getSubtitle();
                    $nama_subtitle = trim($subtitle);
                }

                $c_rincianDetail = new Criteria();
                $c_rincianDetail->add(Revisi21RincianDetailPeer::UNIT_ID, $unit_id);
                $c_rincianDetail->add(Revisi21RincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
                $c_rincianDetail->add(Revisi21RincianDetailPeer::SUBTITLE, $nama_subtitle, Criteria::ILIKE);
                $c_rincianDetail->add(Revisi21RincianDetailPeer::STATUS_HAPUS, false);
                $c_rincianDetail->add(Revisi21RincianDetailPeer::TAHUN, sfConfig::get('app_tahun_default'));
//$c_rincianDetail->addAscendingOrderByColumn(Revisi21RincianDetailPeer::SUB);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi21RincianDetailPeer::KODE_SUB);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi21RincianDetailPeer::REKENING_CODE);
                $c_rincianDetail->addDescendingOrderByColumn(Revisi21RincianDetailPeer::NILAI_ANGGARAN);

                $c_rincianDetail->addAscendingOrderByColumn(Revisi21RincianDetailPeer::LOKASI_KECAMATAN);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi21RincianDetailPeer::LOKASI_KELURAHAN);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi21RincianDetailPeer::KOMPONEN_NAME);
                $rs_rd = Revisi21RincianDetailPeer::doSelect($c_rincianDetail);
            } elseif ($tahap == 'revisi2_2') {
                $c = new Criteria();
                $c->add(Revisi22SubtitleIndikatorPeer::SUB_ID, $sub_id);
                $c->add(Revisi22SubtitleIndikatorPeer::PRIORITAS, 0, Criteria::NOT_EQUAL);
                $rs_subtitle = Revisi22SubtitleIndikatorPeer::doSelectOne($c);
                if ($rs_subtitle) {
                    $unit_id = $rs_subtitle->getUnitId();
                    $kegiatan_code = $rs_subtitle->getKegiatanCode();
                    $subtitle = $rs_subtitle->getSubtitle();
                    $nama_subtitle = trim($subtitle);
                }

                $c_rincianDetail = new Criteria();
                $c_rincianDetail->add(Revisi22RincianDetailPeer::UNIT_ID, $unit_id);
                $c_rincianDetail->add(Revisi22RincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
                $c_rincianDetail->add(Revisi22RincianDetailPeer::SUBTITLE, $nama_subtitle, Criteria::ILIKE);
                $c_rincianDetail->add(Revisi22RincianDetailPeer::STATUS_HAPUS, false);
                $c_rincianDetail->add(Revisi22RincianDetailPeer::TAHUN, sfConfig::get('app_tahun_default'));
//$c_rincianDetail->addAscendingOrderByColumn(Revisi22RincianDetailPeer::SUB);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi22RincianDetailPeer::KODE_SUB);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi22RincianDetailPeer::REKENING_CODE);
                $c_rincianDetail->addDescendingOrderByColumn(Revisi22RincianDetailPeer::NILAI_ANGGARAN);

                $c_rincianDetail->addAscendingOrderByColumn(Revisi22RincianDetailPeer::LOKASI_KECAMATAN);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi22RincianDetailPeer::LOKASI_KELURAHAN);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi22RincianDetailPeer::KOMPONEN_NAME);
                $rs_rd = Revisi22RincianDetailPeer::doSelect($c_rincianDetail);
            }  elseif ($tahap == 'revisi3') {
                $c = new Criteria();
                $c->add(Revisi3SubtitleIndikatorPeer::SUB_ID, $sub_id);
                $c->add(Revisi3SubtitleIndikatorPeer::PRIORITAS, 0, Criteria::NOT_EQUAL);
                $rs_subtitle = Revisi3SubtitleIndikatorPeer::doSelectOne($c);
                if ($rs_subtitle) {
                    $unit_id = $rs_subtitle->getUnitId();
                    $kegiatan_code = $rs_subtitle->getKegiatanCode();
                    $subtitle = $rs_subtitle->getSubtitle();
                    $nama_subtitle = trim($subtitle);
                }

                $c_rincianDetail = new Criteria();
                $c_rincianDetail->add(Revisi3RincianDetailPeer::UNIT_ID, $unit_id);
                $c_rincianDetail->add(Revisi3RincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
                $c_rincianDetail->add(Revisi3RincianDetailPeer::SUBTITLE, $nama_subtitle, Criteria::ILIKE);
                $c_rincianDetail->add(Revisi3RincianDetailPeer::STATUS_HAPUS, false);
                $c_rincianDetail->add(Revisi3RincianDetailPeer::TAHUN, sfConfig::get('app_tahun_default'));
//$c_rincianDetail->addAscendingOrderByColumn(Revisi3RincianDetailPeer::SUB);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi3RincianDetailPeer::KODE_SUB);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi3RincianDetailPeer::REKENING_CODE);
                $c_rincianDetail->addDescendingOrderByColumn(Revisi3RincianDetailPeer::NILAI_ANGGARAN);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi3RincianDetailPeer::LOKASI_KECAMATAN);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi3RincianDetailPeer::LOKASI_KELURAHAN);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi3RincianDetailPeer::KOMPONEN_NAME);
                $rs_rd = Revisi3RincianDetailPeer::doSelect($c_rincianDetail);
            }  elseif ($tahap == 'revisi3_1') {
                $c = new Criteria();
                $c->add(Revisi31SubtitleIndikatorPeer::SUB_ID, $sub_id);
                $c->add(Revisi31SubtitleIndikatorPeer::PRIORITAS, 0, Criteria::NOT_EQUAL);
                $rs_subtitle = Revisi31SubtitleIndikatorPeer::doSelectOne($c);
                if ($rs_subtitle) {
                    $unit_id = $rs_subtitle->getUnitId();
                    $kegiatan_code = $rs_subtitle->getKegiatanCode();
                    $subtitle = $rs_subtitle->getSubtitle();
                    $nama_subtitle = trim($subtitle);
                }

                $c_rincianDetail = new Criteria();
                $c_rincianDetail->add(Revisi31RincianDetailPeer::UNIT_ID, $unit_id);
                $c_rincianDetail->add(Revisi31RincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
                $c_rincianDetail->add(Revisi31RincianDetailPeer::SUBTITLE, $nama_subtitle, Criteria::ILIKE);
                $c_rincianDetail->add(Revisi31RincianDetailPeer::STATUS_HAPUS, false);
                $c_rincianDetail->add(Revisi31RincianDetailPeer::TAHUN, sfConfig::get('app_tahun_default'));
//$c_rincianDetail->addAscendingOrderByColumn(Revisi3RincianDetailPeer::SUB);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi31RincianDetailPeer::KODE_SUB);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi31RincianDetailPeer::REKENING_CODE);
                $c_rincianDetail->addDescendingOrderByColumn(Revisi31RincianDetailPeer::NILAI_ANGGARAN);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi31RincianDetailPeer::LOKASI_KECAMATAN);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi31RincianDetailPeer::LOKASI_KELURAHAN);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi31RincianDetailPeer::KOMPONEN_NAME);
                $rs_rd = Revisi31RincianDetailPeer::doSelect($c_rincianDetail);
            } elseif ($tahap == 'revisi4') {
                $c = new Criteria();
                $c->add(Revisi4SubtitleIndikatorPeer::SUB_ID, $sub_id);
                $c->add(Revisi4SubtitleIndikatorPeer::PRIORITAS, 0, Criteria::NOT_EQUAL);
                $rs_subtitle = Revisi4SubtitleIndikatorPeer::doSelectOne($c);
                if ($rs_subtitle) {
                    $unit_id = $rs_subtitle->getUnitId();
                    $kegiatan_code = $rs_subtitle->getKegiatanCode();
                    $subtitle = $rs_subtitle->getSubtitle();
                    $nama_subtitle = trim($subtitle);
                }

                $c_rincianDetail = new Criteria();
                $c_rincianDetail->add(Revisi4RincianDetailPeer::UNIT_ID, $unit_id);
                $c_rincianDetail->add(Revisi4RincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
                $c_rincianDetail->add(Revisi4RincianDetailPeer::SUBTITLE, $nama_subtitle, Criteria::ILIKE);
                $c_rincianDetail->add(Revisi4RincianDetailPeer::STATUS_HAPUS, false);
                $c_rincianDetail->add(Revisi4RincianDetailPeer::TAHUN, sfConfig::get('app_tahun_default'));
//$c_rincianDetail->addAscendingOrderByColumn(Revisi4RincianDetailPeer::SUB);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi4RincianDetailPeer::KODE_SUB);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi4RincianDetailPeer::REKENING_CODE);
                $c_rincianDetail->addDescendingOrderByColumn(Revisi4RincianDetailPeer::NILAI_ANGGARAN);

                $c_rincianDetail->addAscendingOrderByColumn(Revisi4RincianDetailPeer::LOKASI_KECAMATAN);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi4RincianDetailPeer::LOKASI_KELURAHAN);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi4RincianDetailPeer::KOMPONEN_NAME);
                $rs_rd = Revisi4RincianDetailPeer::doSelect($c_rincianDetail);
            } elseif ($tahap == 'revisi5') {
                $c = new Criteria();
                $c->add(Revisi5SubtitleIndikatorPeer::SUB_ID, $sub_id);
                $c->add(Revisi5SubtitleIndikatorPeer::PRIORITAS, 0, Criteria::NOT_EQUAL);
                $rs_subtitle = Revisi5SubtitleIndikatorPeer::doSelectOne($c);
                if ($rs_subtitle) {
                    $unit_id = $rs_subtitle->getUnitId();
                    $kegiatan_code = $rs_subtitle->getKegiatanCode();
                    $subtitle = $rs_subtitle->getSubtitle();
                    $nama_subtitle = trim($subtitle);
                }

                $c_rincianDetail = new Criteria();
                $c_rincianDetail->add(Revisi5RincianDetailPeer::UNIT_ID, $unit_id);
                $c_rincianDetail->add(Revisi5RincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
                $c_rincianDetail->add(Revisi5RincianDetailPeer::SUBTITLE, $nama_subtitle, Criteria::ILIKE);
                $c_rincianDetail->add(Revisi5RincianDetailPeer::STATUS_HAPUS, false);
                $c_rincianDetail->add(Revisi5RincianDetailPeer::TAHUN, sfConfig::get('app_tahun_default'));
//$c_rincianDetail->addAscendingOrderByColumn(Revisi5RincianDetailPeer::SUB);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi5RincianDetailPeer::KODE_SUB);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi5RincianDetailPeer::REKENING_CODE);
                $c_rincianDetail->addDescendingOrderByColumn(Revisi5RincianDetailPeer::NILAI_ANGGARAN);

                $c_rincianDetail->addAscendingOrderByColumn(Revisi5RincianDetailPeer::LOKASI_KECAMATAN);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi5RincianDetailPeer::LOKASI_KELURAHAN);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi5RincianDetailPeer::KOMPONEN_NAME);
                $rs_rd = Revisi5RincianDetailPeer::doSelect($c_rincianDetail);
            } elseif ($tahap == 'revisi6') {
                $c = new Criteria();
                $c->add(Revisi6SubtitleIndikatorPeer::SUB_ID, $sub_id);
                $c->add(Revisi6SubtitleIndikatorPeer::PRIORITAS, 0, Criteria::NOT_EQUAL);
                $rs_subtitle = Revisi6SubtitleIndikatorPeer::doSelectOne($c);
                if ($rs_subtitle) {
                    $unit_id = $rs_subtitle->getUnitId();
                    $kegiatan_code = $rs_subtitle->getKegiatanCode();
                    $subtitle = $rs_subtitle->getSubtitle();
                    $nama_subtitle = trim($subtitle);
                }

                $c_rincianDetail = new Criteria();
                $c_rincianDetail->add(Revisi6RincianDetailPeer::UNIT_ID, $unit_id);
                $c_rincianDetail->add(Revisi6RincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
                $c_rincianDetail->add(Revisi6RincianDetailPeer::SUBTITLE, $nama_subtitle, Criteria::ILIKE);
                $c_rincianDetail->add(Revisi6RincianDetailPeer::STATUS_HAPUS, false);
                $c_rincianDetail->add(Revisi6RincianDetailPeer::TAHUN, sfConfig::get('app_tahun_default'));
//$c_rincianDetail->addAscendingOrderByColumn(Revisi6RincianDetailPeer::SUB);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi6RincianDetailPeer::KODE_SUB);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi6RincianDetailPeer::REKENING_CODE);
                $c_rincianDetail->addDescendingOrderByColumn(Revisi6RincianDetailPeer::NILAI_ANGGARAN);

                $c_rincianDetail->addAscendingOrderByColumn(Revisi6RincianDetailPeer::LOKASI_KECAMATAN);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi6RincianDetailPeer::LOKASI_KELURAHAN);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi6RincianDetailPeer::KOMPONEN_NAME);
                $rs_rd = Revisi6RincianDetailPeer::doSelect($c_rincianDetail);
            } elseif ($tahap == 'revisi7') {
                $c = new Criteria();
                $c->add(Revisi7SubtitleIndikatorPeer::SUB_ID, $sub_id);
                $c->add(Revisi7SubtitleIndikatorPeer::PRIORITAS, 0, Criteria::NOT_EQUAL);
                $rs_subtitle = Revisi7SubtitleIndikatorPeer::doSelectOne($c);
                if ($rs_subtitle) {
                    $unit_id = $rs_subtitle->getUnitId();
                    $kegiatan_code = $rs_subtitle->getKegiatanCode();
                    $subtitle = $rs_subtitle->getSubtitle();
                    $nama_subtitle = trim($subtitle);
                }

                $c_rincianDetail = new Criteria();
                $c_rincianDetail->add(Revisi7RincianDetailPeer::UNIT_ID, $unit_id);
                $c_rincianDetail->add(Revisi7RincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
                $c_rincianDetail->add(Revisi7RincianDetailPeer::SUBTITLE, $nama_subtitle, Criteria::ILIKE);
                $c_rincianDetail->add(Revisi7RincianDetailPeer::STATUS_HAPUS, false);
                $c_rincianDetail->add(Revisi7RincianDetailPeer::TAHUN, sfConfig::get('app_tahun_default'));
//$c_rincianDetail->addAscendingOrderByColumn(Revisi6RincianDetailPeer::SUB);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi7RincianDetailPeer::KODE_SUB);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi7RincianDetailPeer::REKENING_CODE);
                $c_rincianDetail->addDescendingOrderByColumn(Revisi7RincianDetailPeer::NILAI_ANGGARAN);

                $c_rincianDetail->addAscendingOrderByColumn(Revisi7RincianDetailPeer::LOKASI_KECAMATAN);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi7RincianDetailPeer::LOKASI_KELURAHAN);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi7RincianDetailPeer::KOMPONEN_NAME);
                $rs_rd = Revisi7RincianDetailPeer::doSelect($c_rincianDetail);
            } elseif ($tahap == 'revisi8') {
                $c = new Criteria();
                $c->add(Revisi8SubtitleIndikatorPeer::SUB_ID, $sub_id);
                $c->add(Revisi8SubtitleIndikatorPeer::PRIORITAS, 0, Criteria::NOT_EQUAL);
                $rs_subtitle = Revisi8SubtitleIndikatorPeer::doSelectOne($c);
                if ($rs_subtitle) {
                    $unit_id = $rs_subtitle->getUnitId();
                    $kegiatan_code = $rs_subtitle->getKegiatanCode();
                    $subtitle = $rs_subtitle->getSubtitle();
                    $nama_subtitle = trim($subtitle);
                }

                $c_rincianDetail = new Criteria();
                $c_rincianDetail->add(Revisi8RincianDetailPeer::UNIT_ID, $unit_id);
                $c_rincianDetail->add(Revisi8RincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
                $c_rincianDetail->add(Revisi8RincianDetailPeer::SUBTITLE, $nama_subtitle, Criteria::ILIKE);
                $c_rincianDetail->add(Revisi8RincianDetailPeer::STATUS_HAPUS, false);
                $c_rincianDetail->add(Revisi8RincianDetailPeer::TAHUN, sfConfig::get('app_tahun_default'));
//$c_rincianDetail->addAscendingOrderByColumn(Revisi6RincianDetailPeer::SUB);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi8RincianDetailPeer::KODE_SUB);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi8RincianDetailPeer::REKENING_CODE);
                $c_rincianDetail->addDescendingOrderByColumn(Revisi8RincianDetailPeer::NILAI_ANGGARAN);

                $c_rincianDetail->addAscendingOrderByColumn(Revisi8RincianDetailPeer::LOKASI_KECAMATAN);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi8RincianDetailPeer::LOKASI_KELURAHAN);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi8RincianDetailPeer::KOMPONEN_NAME);
                $rs_rd = Revisi8RincianDetailPeer::doSelect($c_rincianDetail);
            } elseif ($tahap == 'revisi9') {
                $c = new Criteria();
                $c->add(Revisi9SubtitleIndikatorPeer::SUB_ID, $sub_id);
                $c->add(Revisi9SubtitleIndikatorPeer::PRIORITAS, 0, Criteria::NOT_EQUAL);
                $rs_subtitle = Revisi9SubtitleIndikatorPeer::doSelectOne($c);
                if ($rs_subtitle) {
                    $unit_id = $rs_subtitle->getUnitId();
                    $kegiatan_code = $rs_subtitle->getKegiatanCode();
                    $subtitle = $rs_subtitle->getSubtitle();
                    $nama_subtitle = trim($subtitle);
                }

                $c_rincianDetail = new Criteria();
                $c_rincianDetail->add(Revisi9RincianDetailPeer::UNIT_ID, $unit_id);
                $c_rincianDetail->add(Revisi9RincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
                $c_rincianDetail->add(Revisi9RincianDetailPeer::SUBTITLE, $nama_subtitle, Criteria::ILIKE);
                $c_rincianDetail->add(Revisi9RincianDetailPeer::STATUS_HAPUS, false);
                $c_rincianDetail->add(Revisi9RincianDetailPeer::TAHUN, sfConfig::get('app_tahun_default'));
//$c_rincianDetail->addAscendingOrderByColumn(Revisi6RincianDetailPeer::SUB);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi9RincianDetailPeer::KODE_SUB);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi9RincianDetailPeer::REKENING_CODE);
                $c_rincianDetail->addDescendingOrderByColumn(Revisi9RincianDetailPeer::NILAI_ANGGARAN);

                $c_rincianDetail->addAscendingOrderByColumn(Revisi9RincianDetailPeer::LOKASI_KECAMATAN);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi9RincianDetailPeer::LOKASI_KELURAHAN);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi9RincianDetailPeer::KOMPONEN_NAME);
                $rs_rd = Revisi9RincianDetailPeer::doSelect($c_rincianDetail);
            } elseif ($tahap == 'revisi10') {
                $c = new Criteria();
                $c->add(Revisi10SubtitleIndikatorPeer::SUB_ID, $sub_id);
                $c->add(Revisi10SubtitleIndikatorPeer::PRIORITAS, 0, Criteria::NOT_EQUAL);
                $rs_subtitle = Revisi10SubtitleIndikatorPeer::doSelectOne($c);
                if ($rs_subtitle) {
                    $unit_id = $rs_subtitle->getUnitId();
                    $kegiatan_code = $rs_subtitle->getKegiatanCode();
                    $subtitle = $rs_subtitle->getSubtitle();
                    $nama_subtitle = trim($subtitle);
                }

                $c_rincianDetail = new Criteria();
                $c_rincianDetail->add(Revisi10RincianDetailPeer::UNIT_ID, $unit_id);
                $c_rincianDetail->add(Revisi10RincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
                $c_rincianDetail->add(Revisi10RincianDetailPeer::SUBTITLE, $nama_subtitle, Criteria::ILIKE);
                $c_rincianDetail->add(Revisi10RincianDetailPeer::STATUS_HAPUS, false);
                $c_rincianDetail->add(Revisi10RincianDetailPeer::TAHUN, sfConfig::get('app_tahun_default'));
//$c_rincianDetail->addAscendingOrderByColumn(Revisi6RincianDetailPeer::SUB);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi10RincianDetailPeer::KODE_SUB);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi10RincianDetailPeer::REKENING_CODE);
                $c_rincianDetail->addDescendingOrderByColumn(Revisi10RincianDetailPeer::NILAI_ANGGARAN);

                $c_rincianDetail->addAscendingOrderByColumn(Revisi10RincianDetailPeer::LOKASI_KECAMATAN);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi10RincianDetailPeer::LOKASI_KELURAHAN);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi10RincianDetailPeer::KOMPONEN_NAME);
                $rs_rd = Revisi10RincianDetailPeer::doSelect($c_rincianDetail);
            } elseif ($tahap == 'rkua') {
                $c = new Criteria();
                $c->add(RkuaSubtitleIndikatorPeer::SUB_ID, $sub_id);
                $c->add(RkuaSubtitleIndikatorPeer::PRIORITAS, 0, Criteria::NOT_EQUAL);
                $rs_subtitle = RkuaSubtitleIndikatorPeer::doSelectOne($c);
                if ($rs_subtitle) {
                    $unit_id = $rs_subtitle->getUnitId();
                    $kegiatan_code = $rs_subtitle->getKegiatanCode();
                    $subtitle = $rs_subtitle->getSubtitle();
                    $nama_subtitle = trim($subtitle);
                }

                $c_rincianDetail = new Criteria();
                $c_rincianDetail->add(RkuaRincianDetailPeer::UNIT_ID, $unit_id);
                $c_rincianDetail->add(RkuaRincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
                $c_rincianDetail->add(RkuaRincianDetailPeer::SUBTITLE, $nama_subtitle, Criteria::ILIKE);
                $c_rincianDetail->add(RkuaRincianDetailPeer::STATUS_HAPUS, false);
                $c_rincianDetail->add(RkuaRincianDetailPeer::TAHUN, sfConfig::get('app_tahun_default'));
//$c_rincianDetail->addAscendingOrderByColumn(RkuaRincianDetailPeer::SUB);
                $c_rincianDetail->addAscendingOrderByColumn(RkuaRincianDetailPeer::KODE_SUB);
                $c_rincianDetail->addAscendingOrderByColumn(RkuaRincianDetailPeer::REKENING_CODE);
                $c_rincianDetail->addDescendingOrderByColumn(RkuaRincianDetailPeer::NILAI_ANGGARAN);

                $c_rincianDetail->addAscendingOrderByColumn(RkuaRincianDetailPeer::LOKASI_KECAMATAN);
                $c_rincianDetail->addAscendingOrderByColumn(RkuaRincianDetailPeer::LOKASI_KELURAHAN);
                $c_rincianDetail->addAscendingOrderByColumn(RkuaRincianDetailPeer::KOMPONEN_NAME);
                $rs_rd = RkuaRincianDetailPeer::doSelect($c_rincianDetail);
            } else {
                $c = new Criteria();
                $c->add(DinasSubtitleIndikatorPeer::SUB_ID, $sub_id);
                //$c->add(DinasSubtitleIndikatorPeer::PRIORITAS, 0, Criteria::NOT_EQUAL);
                $rs_subtitle = DinasSubtitleIndikatorPeer::doSelectOne($c);
                if ($rs_subtitle) {
                    $unit_id = $rs_subtitle->getUnitId();
                    $kegiatan_code = $rs_subtitle->getKegiatanCode();
                    $subtitle = $rs_subtitle->getSubtitle();
                    $nama_subtitle = trim($subtitle);
                }

                $c_rincianDetail = new Criteria();
                $c_rincianDetail->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
                $c_rincianDetail->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
                $c_rincianDetail->add(DinasRincianDetailPeer::SUBTITLE, $nama_subtitle, Criteria::ILIKE);
                $c_rincianDetail->add(DinasRincianDetailPeer::STATUS_HAPUS, false);
                $c_rincianDetail->add(DinasRincianDetailPeer::TAHUN, sfConfig::get('app_tahun_default'));
//$c_rincianDetail->addAscendingOrderByColumn(DinasRincianDetailPeer::SUB);
                $c_rincianDetail->addAscendingOrderByColumn(DinasRincianDetailPeer::KODE_SUB);
                $c_rincianDetail->addAscendingOrderByColumn(DinasRincianDetailPeer::REKENING_CODE);
                $c_rincianDetail->addAscendingOrderByColumn(DinasRincianDetailPeer::KOMPONEN_NAME);
                $c_rincianDetail->addDescendingOrderByColumn(DinasRincianDetailPeer::NILAI_ANGGARAN);

                $c_rincianDetail->addAscendingOrderByColumn(DinasRincianDetailPeer::LOKASI_KECAMATAN);
                $c_rincianDetail->addAscendingOrderByColumn(DinasRincianDetailPeer::LOKASI_KELURAHAN);
                $rs_rd = DinasRincianDetailPeer::doSelect($c_rincianDetail);
            }
            $query = "select kode_rka from history_pekerjaan_v2
                where (jalan, komponen) in
                (select jalan, komponen from history_pekerjaan_v2
                where jalan<>'' and status_hapus=false and tahun='" . sfConfig::get('app_tahun_default') . "' and substring(kode_rka for 3)<>'XXX' and substring(kode_rka for 4)<>'9999'
                group by jalan, komponen
                having count(*)>1) and jalan<>'' and status_hapus=false and tahun='" . sfConfig::get('app_tahun_default') . "' and substring(kode_rka for 3)<>'XXX' and substring(kode_rka for 4)<>'9999'
                order by jalan";
            $congis = Propel::getConnection('gis');
            $stmt = $congis->prepareStatement($query);
            $rs = $stmt->executeQuery();
            $arr_kode_rka = array();
            while ($rs->next()) {
                $arr_kode_rka[] = "'" . $rs->getString('kode_rka') . "'";
            }
            if (!$arr_kode_rka) {
                $kode_rka = "''";
            } else {
                $kode_rka = implode(',', $arr_kode_rka);
            }
            $query = "(select rd.detail_no
                    from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail rd
                    where (rd.unit_id,rd.komponen_id,rd.rekening_code) in
                            (select unit_id, komponen_id, rekening_code
                            from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail
                            where unit_id='$unit_id' and status_hapus=false and tipe<>'FISIK' and tipe<>'EST' and 
                            (rekening_code like '5.2.2.%' or rekening_code like '5.2.3.%')
                            group by unit_id, komponen_id, rekening_code
                            having count(*)>1) and 
                    status_hapus=false and nilai_anggaran>0 and unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and subtitle='$subtitle'
                    )union(
                    select rd.detail_no
                    from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail rd
                    where (rd.komponen_id) in
                            (select komponen_id
                            from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail
                            where status_hapus=false and nilai_anggaran>0 and unit_id||'.'||kegiatan_code||'.'||detail_no in ($kode_rka)
                            group by komponen_id
                            having count(*)>1) and rd.nilai_anggaran>0 and rd.status_hapus=false
                    and rd.unit_id||'.'||rd.kegiatan_code||'.'||rd.detail_no in ($kode_rka) and
                    unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and subtitle='$subtitle'
                    )";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs = $stmt->executeQuery();
            $komponen_serupa = array();
            while ($rs->next()) {
                $komponen_serupa[] = $rs->getString('detail_no');
            }

            $this->komponen_serupa = $komponen_serupa;
            $this->rs_rd = $rs_rd;

            $this->id = $sub_id;
            $this->rinciandetail = 'ada';
            $this->setLayout('kosong');
        }
    }

    public function executeTarikrevisi() {
        if ($this->getRequestParameter('tarik') == md5('dinas')) {
            $unit_id = $this->getRequestParameter('unit_id');
            $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');

            //------------------------------------------------------------------------------------------------------------------
            //irul tambahan
            set_time_limit(0);
            //irul tambahan
            $con = Propel::getConnection();
            $con->begin();
            try {
                $tahap = DinasMasterKegiatanPeer::getTahapKegiatan($unit_id, $kode_kegiatan);
                // backup dahulu di next, mungkin batal kembali
                $sql = "delete from " . sfConfig::get('app_default_schema') . ".next_rincian_detail where unit_id='" . $unit_id . "' and kegiatan_code='$kode_kegiatan' and tahun='" . sfConfig::get('app_tahun_default') . "'";
                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();
                $sql = "delete from " . sfConfig::get('app_default_schema') . ".next_master_kegiatan where unit_id='" . $unit_id . "' and kode_kegiatan='$kode_kegiatan' and tahun='" . sfConfig::get('app_tahun_default') . "'";
                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();
                $sql = "delete from " . sfConfig::get('app_default_schema') . ".next_rincian where unit_id='" . $unit_id . "' and kegiatan_code='$kode_kegiatan' and tahun='" . sfConfig::get('app_tahun_default') . "'";
                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();
                $sql = "delete from " . sfConfig::get('app_default_schema') . ".next_rincian_sub_parameter where unit_id='" . $unit_id . "' and kegiatan_code='$kode_kegiatan' and tahun='" . sfConfig::get('app_tahun_default') . "'";
                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();
                $sql = "delete from " . sfConfig::get('app_default_schema') . ".next_rka_member where unit_id='" . $unit_id . "' and kegiatan_code='$kode_kegiatan' and tahun='" . sfConfig::get('app_tahun_default') . "'";
                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();
                $sql = "delete from " . sfConfig::get('app_default_schema') . ".next_subtitle_indikator where unit_id='" . $unit_id . "' and kegiatan_code='$kode_kegiatan' and tahun='" . sfConfig::get('app_tahun_default') . "'";
                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();
                $sql = "delete from " . sfConfig::get('app_default_schema') . ".next_log_perubahan_revisi where unit_id='" . $unit_id . "' and kegiatan_code='$kode_kegiatan' and tahap='" . $tahap . "'";
                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();

                $sql = "insert into " . sfConfig::get('app_default_schema') . ".next_rincian_detail "
                        . "select * from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail where unit_id='" . $unit_id . "' and kegiatan_code='$kode_kegiatan' and tahun='" . sfConfig::get('app_tahun_default') . "' order by detail_no";
                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();
                $sql = "insert into " . sfConfig::get('app_default_schema') . ".next_master_kegiatan "
                        . "select * from " . sfConfig::get('app_default_schema') . ".dinas_master_kegiatan where unit_id='" . $unit_id . "' and kode_kegiatan='$kode_kegiatan' and tahun='" . sfConfig::get('app_tahun_default') . "'";
                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();
                $sql = "insert into " . sfConfig::get('app_default_schema') . ".next_rincian "
                        . "select * from " . sfConfig::get('app_default_schema') . ".dinas_rincian where unit_id='" . $unit_id . "' and kegiatan_code='$kode_kegiatan' and tahun='" . sfConfig::get('app_tahun_default') . "'";
                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();
                $sql = "insert into " . sfConfig::get('app_default_schema') . ".next_rincian_sub_parameter "
                        . "select * from " . sfConfig::get('app_default_schema') . ".dinas_rincian_sub_parameter where unit_id='" . $unit_id . "' and kegiatan_code='$kode_kegiatan' and tahun='" . sfConfig::get('app_tahun_default') . "'";
                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();
                $sql = "insert into " . sfConfig::get('app_default_schema') . ".next_rka_member "
                        . "select * from " . sfConfig::get('app_default_schema') . ".dinas_rka_member where unit_id='" . $unit_id . "' and kegiatan_code='$kode_kegiatan' and tahun='" . sfConfig::get('app_tahun_default') . "'";
                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();
                $sql = "insert into " . sfConfig::get('app_default_schema') . ".next_subtitle_indikator "
                        . "select * from " . sfConfig::get('app_default_schema') . ".dinas_subtitle_indikator where unit_id='" . $unit_id . "' and kegiatan_code='$kode_kegiatan' and tahun='" . sfConfig::get('app_tahun_default') . "'";
                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();
                $sql = "insert into " . sfConfig::get('app_default_schema') . ".next_log_perubahan_revisi "
                        . "select * from " . sfConfig::get('app_default_schema') . ".log_perubahan_revisi where unit_id='" . $unit_id . "' and kegiatan_code='$kode_kegiatan' and tahap='" . $tahap . "'";
                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();

                //$tahaprevisi = $tahap + 1;
                $sql = "delete from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail where unit_id='" . $unit_id . "' and kegiatan_code='$kode_kegiatan' and tahun='" . sfConfig::get('app_tahun_default') . "'";
                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();
                $sql = "delete from " . sfConfig::get('app_default_schema') . ".dinas_master_kegiatan where unit_id='" . $unit_id . "' and kode_kegiatan='$kode_kegiatan' and tahun='" . sfConfig::get('app_tahun_default') . "'";
                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();
                $sql = "delete from " . sfConfig::get('app_default_schema') . ".dinas_rincian where unit_id='" . $unit_id . "' and kegiatan_code='$kode_kegiatan' and tahun='" . sfConfig::get('app_tahun_default') . "'";
                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();
                $sql = "delete from " . sfConfig::get('app_default_schema') . ".dinas_rincian_sub_parameter where unit_id='" . $unit_id . "' and kegiatan_code='$kode_kegiatan' and tahun='" . sfConfig::get('app_tahun_default') . "'";
                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();
                $sql = "delete from " . sfConfig::get('app_default_schema') . ".dinas_rka_member where unit_id='" . $unit_id . "' and kegiatan_code='$kode_kegiatan' and tahun='" . sfConfig::get('app_tahun_default') . "'";
                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();
                $sql = "delete from " . sfConfig::get('app_default_schema') . ".dinas_subtitle_indikator where unit_id='" . $unit_id . "' and kegiatan_code='$kode_kegiatan' and tahun='" . sfConfig::get('app_tahun_default') . "'";
                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();
                $sql = "delete from " . sfConfig::get('app_default_schema') . ".log_perubahan_revisi where unit_id='" . $unit_id . "' and kegiatan_code='$kode_kegiatan' and tahap='" . $tahap . "'";
                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();

                $sql = "insert into " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail "
                        . "select * from " . sfConfig::get('app_default_schema') . ".rincian_detail where unit_id='" . $unit_id . "' and kegiatan_code='$kode_kegiatan' and tahun='" . sfConfig::get('app_tahun_default') . "'";
                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();
                $sql = "insert into " . sfConfig::get('app_default_schema') . ".dinas_master_kegiatan "
                        . "select * from " . sfConfig::get('app_default_schema') . ".master_kegiatan where unit_id='" . $unit_id . "' and kode_kegiatan='$kode_kegiatan' and tahun='" . sfConfig::get('app_tahun_default') . "'";
                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();
                $sql = "insert into " . sfConfig::get('app_default_schema') . ".dinas_rincian "
                        . "select * from " . sfConfig::get('app_default_schema') . ".rincian where unit_id='" . $unit_id . "' and kegiatan_code='$kode_kegiatan' and tahun='" . sfConfig::get('app_tahun_default') . "'";
                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();
                $sql = "insert into " . sfConfig::get('app_default_schema') . ".dinas_rincian_sub_parameter "
                        . "select * from " . sfConfig::get('app_default_schema') . ".rincian_sub_parameter where unit_id='" . $unit_id . "' and kegiatan_code='$kode_kegiatan' and tahun='" . sfConfig::get('app_tahun_default') . "'";
                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();
                $sql = "insert into " . sfConfig::get('app_default_schema') . ".dinas_rka_member "
                        . "select * from " . sfConfig::get('app_default_schema') . ".rka_member where unit_id='" . $unit_id . "' and kegiatan_code='$kode_kegiatan' and tahun='" . sfConfig::get('app_tahun_default') . "'";
                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();
                $sql = "insert into " . sfConfig::get('app_default_schema') . ".dinas_subtitle_indikator "
                        . "select * from " . sfConfig::get('app_default_schema') . ".subtitle_indikator where unit_id='" . $unit_id . "' and kegiatan_code='$kode_kegiatan' and tahun='" . sfConfig::get('app_tahun_default') . "'";
                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();

                $sql = "update " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail "
                        . "set status_level=0, is_penyelia_setuju=false, is_bappeko_setuju=false, is_tapd_setuju=false, is_bagian_hukum_setuju = false, is_inspektorat_setuju = false, is_badan_kepegawaian_setuju = false, is_lppa_setuju = false, is_bagian_organisasi_setuju = false where unit_id='" . $unit_id . "' and kegiatan_code='$kode_kegiatan' and tahun='" . sfConfig::get('app_tahun_default') . "'";
                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();
                $sql = "update " . sfConfig::get('app_default_schema') . ".dinas_master_kegiatan "
                        . "set status_level=0, is_penyelia_setuju=false, is_bappeko_setuju=false, is_tapd_setuju=false, is_bagian_hukum_setuju = false, is_inspektorat_setuju = false, is_badan_kepegawaian_setuju = false, is_lppa_setuju = false, is_bagian_organisasi_setuju = false where unit_id='" . $unit_id . "' and kode_kegiatan='$kode_kegiatan' and tahun='" . sfConfig::get('app_tahun_default') . "'";
                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();

                $con->commit();
                $this->setFlash('berhasil', 'Kegiatan ' . $kode_kegiatan . ' Telah Berhasil disiapkan Untuk Dapat ditarik oleh e - Revisi');
            } catch (Exception $e) {
                $con->rollback();
                $this->setFlash('gagal', 'Kegiatan GAGAL disiapkan Untuk Dapat ditarik oleh e - Revisi karena ' . $e->getMessage());
            }
            return $this->redirect('kegiatan/listRevisi?unit_id=' . $unit_id);
        }
    }

    public function executePilihKelurahan() {
        $this->id_kecamatan = $this->getRequestParameter('b');
    }

    //tiket #58
    //23 Februari 2016 - kembalikan ke waiting list
    public function executeKembalikanWaitingEdit() {
        $this->unit_id = $this->getRequestParameter('unitid');
        $this->kode_kegiatan = $this->getRequestParameter('kodekegiatan');
        $this->detail_no = $this->getRequestParameter('detailno');
    }

    //tiket #58
    //23 Februari 2016 - kembalikan ke waiting list
    public function executeKembalikanWaiting() {
        $sekarang = date('Y-m-d H:i:s');
        $detail_no = $this->getRequestParameter('detail_no');
        $unit_id = $this->getRequestParameter('unit_id');
        $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
        $keterangan = trim($this->getRequestParameter('keterangan'));
        if (is_null($detail_no) || is_null($unit_id) || is_null($kode_kegiatan) || is_null($keterangan) || ($this->getRequest()->hasParameter('key') != md5('kembali_waiting'))) {
            $this->setFlash('gagal', 'Gagal karena parameter kurang');
            return $this->redirect("kegiatan/edit?unit_id=" . $unit_id . "&kode_kegiatan=" . $kode_kegiatan);
        } else if (strlen($keterangan) < 20) {
            $this->setFlash('gagal', 'Keterangan minimal berisi 20 karakter');
            return $this->redirect("kegiatan/edit?unit_id=" . $unit_id . "&kode_kegiatan=" . $kode_kegiatan);
        } else {
            //ambil rincian detail yang dikembalikan
            $c_rincian_detail = new Criteria();
            $c_rincian_detail->add(RincianDetailPeer::UNIT_ID, $unit_id);
            $c_rincian_detail->add(RincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
            $c_rincian_detail->add(RincianDetailPeer::DETAIL_NO, $detail_no);
            $c_rincian_detail->add(RincianDetailPeer::STATUS_HAPUS, FALSE);
            $rd = RincianDetailPeer::doSelectOne($c_rincian_detail);
            $kode_rka = $rd->getUnitId() . '.' . $rd->getKegiatanCode() . '.' . $rd->getDetailNo();
            $komponen_name = $rd->getKomponenName();
            $detail_name = $rd->getDetailName();
            //ambil waitinglist pu
            $c_waiting = new Criteria();
            $c_waiting->add(WaitingListPUPeer::KODE_RKA, $kode_rka);
            $waiting = WaitingListPUPeer::doSelectOne($c_waiting);
            $id_waiting = $waiting->getIdWaiting();
            $kode_rka_waiting = $waiting->getUnitId() . '.' . $waiting->getKegiatanCode() . '.' . $waiting->getIdWaiting();
            $prioritas = $waiting->getPrioritas();

            //ambil geojsonlokasi rev1
            $c_cari_geojson = new Criteria();
            $c_cari_geojson->add(GeojsonlokasiRev1Peer::DETAIL_NO, $detail_no);
            $c_cari_geojson->addAnd(GeojsonlokasiRev1Peer::KEGIATAN_CODE, $kode_kegiatan);
            $c_cari_geojson->addAnd(GeojsonlokasiRev1Peer::UNIT_ID, $unit_id);
            $c_cari_geojson->addAnd(GeojsonlokasiRev1Peer::STATUS_HAPUS, FALSE);
            $dapat_geojson = GeojsonlokasiRev1Peer::doSelect($c_cari_geojson);

            //ambil geojsonlokasi waiting
            $c_hapus_geojson = new Criteria();
            $c_hapus_geojson->add(GeojsonlokasiWaitinglistPeer::ID_WAITING, $id_waiting);
            $c_hapus_geojson->addAnd(GeojsonlokasiWaitinglistPeer::KEGIATAN_CODE, $kode_kegiatan);
            $c_hapus_geojson->addAnd(GeojsonlokasiWaitinglistPeer::UNIT_ID, 'XXX' . $unit_id);
            $c_hapus_geojson->addAnd(GeojsonlokasiWaitinglistPeer::STATUS_HAPUS, FALSE);
            $hapus_geojson = GeojsonlokasiWaitinglistPeer::doSelect($c_hapus_geojson);

            //ambil nama skpd
            $c_unit = new Criteria();
            $c_unit->add(UnitKerjaPeer::UNIT_ID, $unit_id);
            $data_unit_kerja = UnitKerjaPeer::doSelectOne($c_unit);

            //cek lelang+eproject+edelivery
            if (sfConfig::get('app_fasilitas_cekeDelivery') == 'buka' && sfConfig::get('app_fasilitas_cekeProject') == 'buka') {
                $nilaiTerpakai = 0;
                $totNilaiSwakelola = 0;
                $totNilaiKontrak = 0;
                $totNilaiAlokasi = 0;
                $totNilaiHps = 0;
                $nilai = 0;
                $ceklelangselesaitidakaturanpembayaran = 0;
                $lelang = 0;
                $rd2 = new RincianDetail();
                $totNilaiSwakelola = $rd2->getCekNilaiSwakelolaDelivery2($unit_id, $kode_kegiatan, $detail_no);
                $totNilaiKontrak = $rd2->getCekNilaiKontrakDelivery2($unit_id, $kode_kegiatan, $detail_no);
                $totNilaiAlokasi = $rd2->getCekNilaiAlokasiProject($unit_id, $kode_kegiatan, $detail_no);
                if ($totNilaiSwakelola > 0) {
                    $totNilaiSwakelola = $totNilaiSwakelola;
                    //$totNilaiSwakelola = $totNilaiSwakelola + 10;
                }
                if ($totNilaiKontrak > 0) {
                    $totNilaiKontrak = $totNilaiKontrak;
                    //$totNilaiKontrak = $totNilaiKontrak + 10;
                }
                if (sfConfig::get('app_fasilitas_cekServer') == 'buka') {
                    $totNilaiHps = $rd2->getCekNilaiHPSKomponen($unit_id, $kode_kegiatan, $detail_no);
                    $lelang = $rd2->getCekLelang($unit_id, $kode_kegiatan, $detail_no, $nilai);
                    $ceklelangselesaitidakaturanpembayaran = $rd2->getCekLelangTidakAdaAturanPembayaran($unit_id, $kode_kegiatan, $detail_no);
                }
                if (0 < $totNilaiKontrak || 0 < $totNilaiSwakelola) {
                    if ($totNilaiKontrak == 0) {
                        $this->setFlash('gagal', 'Mohon maaf , untuk komponen  ini sudah terpakai di edelivery, sejumlah Swakelola Rp.' . number_format($totNilaiSwakelola, 0, ',', '.'));
                        return $this->redirect("kegiatan/edit?unit_id=" . $unit_id . "&kode_kegiatan=" . $kode_kegiatan);
                    } else if ($totNilaiSwakelola == 0) {
                        $this->setFlash('gagal', 'Mohon maaf , untuk komponen  ini sudah terpakai di edelivery, sejumlah Kontrak Rp.' . number_format($totNilaiKontrak, 0, ',', '.'));
                        return $this->redirect("kegiatan/edit?unit_id=" . $unit_id . "&kode_kegiatan=" . $kode_kegiatan);
                    }
                } else if (0 < $totNilaiHps) {
                    $this->setFlash('gagal', 'Mohon maaf , nilai HPS sebesar Rp.' . number_format($totNilaiHps, 0, ',', '.'));
                    return $this->redirect("kegiatan/edit?unit_id=" . $unit_id . "&kode_kegiatan=" . $kode_kegiatan);
                } else if ($ceklelangselesaitidakaturanpembayaran == 1) {
                    $this->setFlash('gagal', 'Proses Lelang untuk komponen ini telah selesai, namun belum ada Aturan Pembayaran di eDelivery. Silahkan mengisi Aturan Pembayaran terlebih dahulu.');
                    return $this->redirect("kegiatan/edit?unit_id=" . $unit_id . "&kode_kegiatan=" . $kode_kegiatan);
                } else if ($lelang > 0) {
                    $this->setFlash('gagal', 'Sedang Proses Lelang untuk komponen ini');
                    return $this->redirect("kegiatan/edit?unit_id=" . $unit_id . "&kode_kegiatan=" . $kode_kegiatan);
                }
            }

            //begin transaction
            $con = Propel::getConnection();
            $con->begin();
            try {
                //delete lokasi waiting lama
                $c_hapus_lokasi = new Criteria();
                $c_hapus_lokasi->add(HistoryPekerjaanV2Peer::KODE_RKA, $kode_rka_waiting);
                $lokasi_hapus = HistoryPekerjaanV2Peer::doSelect($c_hapus_lokasi);
                foreach ($lokasi_hapus as $lokasi_value) {
                    $lokasi_value->setStatusHapus(true);
                    $lokasi_value->save();
                }

                //delete geojsonlokasi waiting
                foreach ($hapus_geojson as $value_geojson) {
                    $value_geojson->setStatusHapus(true);
                    $value_geojson->save();
                }

                //ambil lokasi rincian detail di history pekerjaan V2
                $c_buat_lokasi = new Criteria();
                $c_buat_lokasi->add(HistoryPekerjaanV2Peer::KODE_RKA, $kode_rka);
                $c_buat_lokasi->addAnd(HistoryPekerjaanV2Peer::STATUS_HAPUS, FALSE);
                $lokasi_lama = HistoryPekerjaanV2Peer::doSelect($c_buat_lokasi);

                foreach ($lokasi_lama as $dapat_lokasi_lama) {
                    $jalan_fix = '';
                    $gang_fix = '';
                    $nomor_fix = '';
                    $rw_fix = '';
                    $rt_fix = '';
                    $keterangan_fix = '';
                    $tempat_fix = '';
                    $jalan_lama = $dapat_lokasi_lama->getJalan();
                    $gang_lama = $dapat_lokasi_lama->getGang();
                    $nomor_lama = $dapat_lokasi_lama->getNomor();
                    $rw_lama = $dapat_lokasi_lama->getRw();
                    $rt_lama = $dapat_lokasi_lama->getRt();
                    $keterangan_lama = $dapat_lokasi_lama->getKeterangan();
                    $tempat_lama = $dapat_lokasi_lama->getTempat();
                    if ($jalan_lama <> '') {
                        $jalan_fix = 'JL. ' . strtoupper($jalan_lama) . ' ';
                    }
                    if ($tempat_lama <> '') {
                        $tempat_fix = '(' . strtoupper($tempat_lama) . ') ';
                    }
                    if ($gang_lama <> '') {
                        $gang_fix = $gang_lama . ' ';
                    }
                    if ($nomor_lama <> '') {
                        $nomor_fix = 'NO ' . strtoupper($nomor_lama) . ' ';
                    }
                    if ($rw_lama <> '') {
                        $rw_fix = 'RW ' . strtoupper($rw_lama) . ' ';
                    }
                    if ($rt_lama <> '') {
                        $rt_fix = 'RT ' . strtoupper($rt_lama) . ' ';
                    }
                    if ($keterangan_lama <> '') {
                        $keterangan_fix = '' . strtoupper($keterangan_lama) . ' ';
                    }
                    $lokasi_baru = $tempat_fix . '' . $jalan_fix . '' . $gang_fix . '' . $nomor_fix . '' . $rw_fix . '' . $rt_fix . '' . $keterangan_fix;

                    //insert lokasi waiting
                    $komponen_lokasi_fix = $rd->getKomponenName() . ' ' . $rd->getDetailName();
                    $kecamatan_lokasi_fix = $rd->getLokasiKecamatan();
                    $kelurahan_lokasi_fix = $rd->getLokasiKelurahan();
                    $lokasi_per_titik_fix = $lokasi_baru;

                    $c_insert_gis = new HistoryPekerjaanV2();
                    $c_insert_gis->setTahun(sfConfig::get('app_tahun_default'));
                    $c_insert_gis->setKodeRka($kode_rka_waiting);
                    $c_insert_gis->setStatusHapus(FALSE);
                    $c_insert_gis->setJalan(strtoupper($jalan_lama));
                    $c_insert_gis->setGang(strtoupper($gang_lama));
                    $c_insert_gis->setNomor(strtoupper($nomor_lama));
                    $c_insert_gis->setRw(strtoupper($rw_lama));
                    $c_insert_gis->setRt(strtoupper($rt_lama));
                    $c_insert_gis->setKeterangan(strtoupper($keterangan_lama));
                    $c_insert_gis->setTempat(strtoupper($tempat_lama));
                    $c_insert_gis->setKomponen($komponen_lokasi_fix);
                    $c_insert_gis->setKecamatan($kecamatan_lokasi_fix);
                    $c_insert_gis->setKelurahan($kelurahan_lokasi_fix);
                    $c_insert_gis->setLokasi($lokasi_per_titik_fix);
                    $c_insert_gis->save();
                    $dapat_lokasi_lama->setStatusHapus(true);
                    $dapat_lokasi_lama->save();
                }

                foreach ($dapat_geojson as $value_geojson) {

                    //insert geojsonlokasi waiting
                    $geojson_baru = new GeojsonlokasiWaitinglist();
                    $geojson_baru->setUnitId('XXX' . $unit_id);
                    $geojson_baru->setUnitName($data_unit_kerja->getUnitName());
                    $geojson_baru->setKegiatanCode($kode_kegiatan);
                    $geojson_baru->setIdWaiting($id_waiting);
                    $geojson_baru->setSatuan($rd->getSatuan());
                    $geojson_baru->setVolume($rd->getVolume());
                    $geojson_baru->setNilaiAnggaran($rd->getNilaiAnggaran());
                    $geojson_baru->setTahun(sfConfig::get('app_tahun_default'));
                    $geojson_baru->setMlokasi($value_geojson->getMlokasi());
                    $geojson_baru->setIdKelompok($value_geojson->getIdKelompok());
                    $geojson_baru->setGeojson($value_geojson->getGeojson());
                    $geojson_baru->setKeterangan($value_geojson->getKeterangan());
                    $geojson_baru->setNmuser($value_geojson->getNmuser());
                    $geojson_baru->setLevel($value_geojson->getLevel());
                    $geojson_baru->setKomponenName($rd->getKomponenName() . ' ' . $rd->getDetailName());
                    $geojson_baru->setStatusHapus(FALSE);
                    $geojson_baru->setKeteranganAlamat($value_geojson->getKeteranganAlamat());
                    $geojson_baru->setLastCreateTime($sekarang);
                    $geojson_baru->setLastEditTime($sekarang);
                    $geojson_baru->setKoordinat($value_geojson->getKoordinat());
                    $geojson_baru->setLokasiKe($value_geojson->getLokasiKe());
                    $geojson_baru->save();

                    //delete geojsonlokasi rev1
                    $value_geojson->setStatusHapus(true);
                    $value_geojson->save();
                }

                //update prioritas
                $total_aktif = 0;
                $query = "select max(prioritas) as total "
                        . "from " . sfConfig::get('app_default_schema') . ".waitinglist_pu "
                        . "where status_hapus = false and status_waiting = 0 "
                        . "and unit_id = 'XXX$unit_id' and kegiatan_code = '" . $kode_kegiatan . "'";
                $stmt = $con->prepareStatement($query);
                $rs = $stmt->executeQuery();
                while ($rs->next()) {
                    $total_aktif = $rs->getString('total');
                }
                for ($index = $prioritas; $index <= $total_aktif; $index++) {
                    $index_tambah_satu = $index + 1;
                    $c_prioritas = new Criteria();
                    $c_prioritas->add(WaitingListPUPeer::UNIT_ID, 'XXX' . $unit_id);
                    $c_prioritas->addAnd(WaitingListPUPeer::KEGIATAN_CODE, $kode_kegiatan);
                    $c_prioritas->addAnd(WaitingListPUPeer::PRIORITAS, $index);
                    $c_prioritas->addAnd(WaitingListPUPeer::STATUS_HAPUS, FALSE);
                    $c_prioritas->addAnd(WaitingListPUPeer::STATUS_WAITING, 0);
                    if ($waiting_prio = WaitingListPUPeer::doSelectOne($c_prioritas)) {
                        $waiting_prio->setPrioritas($index_tambah_satu);
                        $waiting_prio->save();
                    }
                }

                //update waitinglist pu
                $waiting->setSubtitle($rd->getSubtitle());
                $waiting->setKomponenId($rd->getKomponenId());
                $waiting->setKomponenName($rd->getKomponenName());
                $waiting->setKomponenRekening($rd->getRekeningCode());
                $waiting->setKomponenLokasi('(' . $lokasi_baru . ')');
                $waiting->setKomponenHargaAwal($rd->getKomponenHargaAwal());
                $waiting->setPajak($rd->getPajak());
                $waiting->setKomponenSatuan($rd->getSatuan());
                $waiting->setKoefisien($rd->getKeteranganKoefisien());
                $waiting->setVolume($rd->getVolume());
                $waiting->setTahunInput(sfConfig::get('app_tahun_default'));
                $waiting->setUpdatedAt($sekarang);
                $waiting->setKecamatan($rd->getLokasiKecamatan());
                $waiting->setKelurahan($rd->getLokasiKelurahan());
                $waiting->setIsMusrenbang($rd->getIsMusrenbang());
                $waiting->setNilaiAnggaran($rd->getNilaiAnggaran());
                $waiting->setKeterangan($keterangan);
                $waiting->setStatusHapus(false);
                $waiting->setStatusWaiting(0);
                $waiting->setKodeRka(null);
                $waiting->save();

                //update rincian detail
                $rd->setStatusHapus(true);
                $rd->save();

                budgetLogger::log('Mengembalikan komponen ' . $komponen_name . ' ' . $detail_name . ' ke waitinglist');
                $con->commit();
                $this->setFlash('berhasil', 'Telah berhasil memproses ' . $komponen_name . ' ' . $detail_name . ' kembali ke Waiting List');
                return $this->redirect("kegiatan/edit?unit_id=" . $unit_id . "&kode_kegiatan=" . $kode_kegiatan);
            } catch (Exception $ex) {
                $con->rollback();
                $this->setFlash('gagal', 'Gagal Karena ' . $ex->getMessage());
                return $this->redirect("kegiatan/edit?unit_id=" . $unit_id . "&kode_kegiatan=" . $kode_kegiatan);
            }
        }
    }

    //tiket #58
    //23 Februari 2016 - kembalikan ke waiting list
    public function executeKembalikanWaitingEditRevisi() {
        $this->unit_id = $this->getRequestParameter('unitid');
        $this->kode_kegiatan = $this->getRequestParameter('kodekegiatan');
        $this->detail_no = $this->getRequestParameter('detailno');
    }

    //tiket #58
    //23 Februari 2016 - kembalikan ke waiting list
    public function executeKembalikanWaitingRevisi() {
        $sekarang = date('Y-m-d H:i:s');
        $detail_no = $this->getRequestParameter('detail_no');
        $unit_id = $this->getRequestParameter('unit_id');
        $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
        $keterangan = trim($this->getRequestParameter('keterangan'));
        if (is_null($detail_no) || is_null($unit_id) || is_null($kode_kegiatan) || is_null($keterangan) || ($this->getRequest()->hasParameter('key') != md5('kembali_waiting'))) {
            $this->setFlash('gagal', 'Gagal karena parameter kurang');
            return $this->redirect("kegiatan/editRevisi?unit_id=" . $unit_id . "&kode_kegiatan=" . $kode_kegiatan);
        } else if (strlen($keterangan) < 20) {
            $this->setFlash('gagal', 'Keterangan minimal berisi 20 karakter');
            return $this->redirect("kegiatan/editRevisi?unit_id=" . $unit_id . "&kode_kegiatan=" . $kode_kegiatan);
        } else {
            //ambil rincian detail yang dikembalikan
            $c_rincian_detail = new Criteria();
            $c_rincian_detail->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
            $c_rincian_detail->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
            $c_rincian_detail->add(DinasRincianDetailPeer::DETAIL_NO, $detail_no);
            $c_rincian_detail->add(DinasRincianDetailPeer::STATUS_HAPUS, FALSE);
            $rd = DinasRincianDetailPeer::doSelectOne($c_rincian_detail);
            $kode_rka = $rd->getUnitId() . '.' . $rd->getKegiatanCode() . '.' . $rd->getDetailNo();
            $komponen_name = $rd->getKomponenName();
            $detail_name = $rd->getDetailName();
            //ambil waitinglist pu
            $c_waiting = new Criteria();
            $c_waiting->add(WaitingListPUPeer::KODE_RKA, $kode_rka);
            $waiting = WaitingListPUPeer::doSelectOne($c_waiting);
            $id_waiting = $waiting->getIdWaiting();
            $kode_rka_waiting = $waiting->getUnitId() . '.' . $waiting->getKegiatanCode() . '.' . $waiting->getIdWaiting();
            $prioritas = $waiting->getPrioritas();

            //ambil geojsonlokasi rev1
            $c_cari_geojson = new Criteria();
            $c_cari_geojson->add(GeojsonlokasiRev1Peer::DETAIL_NO, $detail_no);
            $c_cari_geojson->addAnd(GeojsonlokasiRev1Peer::KEGIATAN_CODE, $kode_kegiatan);
            $c_cari_geojson->addAnd(GeojsonlokasiRev1Peer::UNIT_ID, $unit_id);
            $c_cari_geojson->addAnd(GeojsonlokasiRev1Peer::STATUS_HAPUS, FALSE);
            $dapat_geojson = GeojsonlokasiRev1Peer::doSelect($c_cari_geojson);

            //ambil geojsonlokasi waiting
            $c_hapus_geojson = new Criteria();
            $c_hapus_geojson->add(GeojsonlokasiWaitinglistPeer::ID_WAITING, $id_waiting);
            $c_hapus_geojson->addAnd(GeojsonlokasiWaitinglistPeer::KEGIATAN_CODE, $kode_kegiatan);
            $c_hapus_geojson->addAnd(GeojsonlokasiWaitinglistPeer::UNIT_ID, 'XXX' . $unit_id);
            $c_hapus_geojson->addAnd(GeojsonlokasiWaitinglistPeer::STATUS_HAPUS, FALSE);
            $hapus_geojson = GeojsonlokasiWaitinglistPeer::doSelect($c_hapus_geojson);

            //ambil nama skpd
            $c_unit = new Criteria();
            $c_unit->add(UnitKerjaPeer::UNIT_ID, $unit_id);
            $data_unit_kerja = UnitKerjaPeer::doSelectOne($c_unit);

            //cek lelang+eproject+edelivery
            if (sfConfig::get('app_fasilitas_cekeDelivery') == 'buka' && sfConfig::get('app_fasilitas_cekeProject') == 'buka') {
                $nilaiTerpakai = 0;
                $totNilaiSwakelola = 0;
                $totNilaiKontrak = 0;
                $totNilaiRealisasi = 0;
                $totVolumeRealisasi = 0;
                $totNilaiAlokasi = 0;
                $totNilaiHps = 0;
                $ceklelangselesaitidakaturanpembayaran = 0;
                $lelang = 0;
                $rd2 = new DinasRincianDetail();
                $totNilaiSwakelola = $rd2->getCekNilaiSwakelolaDelivery2($unit_id, $kode_kegiatan, $detail_no);
                $totNilaiKontrak = $rd2->getCekNilaiKontrakDelivery2($unit_id, $kode_kegiatan, $detail_no);
                $totNilaiRealisasi = $rd2->getCekRealisasi($unit_id, $kode_kegiatan, $detail_no);
                $totVolumeRealisasi = $rd2->getCekVolumeRealisasi($unit_id, $kode_kegiatan, $detail_no);

                $totNilaiAlokasi = $rd2->getCekNilaiAlokasiProject($unit_id, $kode_kegiatan, $detail_no);
                if ($totNilaiSwakelola > 0) {
                    $totNilaiSwakelola = $totNilaiSwakelola;
                    //$totNilaiSwakelola = $totNilaiSwakelola + 10;
                }
                if ($totNilaiKontrak > 0) {
                    $totNilaiKontrak = $totNilaiKontrak;
                    //$totNilaiKontrak = $totNilaiKontrak + 10;
                }
                if (sfConfig::get('app_fasilitas_cekServer') == 'buka') {

                    $totNilaiHps = $rd2->getCekNilaiHPSKomponen($unit_id, $kode_kegiatan, $detail_no);
                    $lelang = $rd2->getCekLelang($unit_id, $kode_kegiatan, $detail_no, 0);
                    $ceklelangselesaitidakaturanpembayaran = $rd2->getCekLelangTidakAdaAturanPembayaran($unit_id, $kode_kegiatan, $detail_no);
                }

                if (0 < $totNilaiKontrak || 0 < $totNilaiSwakelola) {
                    if ($totNilaiKontrak == 0) {
                        $this->setFlash('gagal', 'Mohon maaf , untuk komponen  ini sudah terpakai di edelivery, sejumlah Swakelola Rp.' . number_format($totNilaiSwakelola, 0, ',', '.'));
                        return $this->redirect("kegiatan/editRevisi?unit_id=" . $unit_id . "&kode_kegiatan=" . $kode_kegiatan);
                    } else if ($totNilaiSwakelola == 0) {
                        $this->setFlash('gagal', 'Mohon maaf , untuk komponen  ini sudah terpakai di edelivery, sejumlah Kontrak Rp.' . number_format($totNilaiKontrak, 0, ',', '.'));
                        return $this->redirect("kegiatan/editRevisi?unit_id=" . $unit_id . "&kode_kegiatan=" . $kode_kegiatan);
                    }
                } else if (0 < $totNilaiHps) {
                    $this->setFlash('gagal', 'Mohon maaf , nilai HPS sebesar Rp.' . number_format($totNilaiHps, 0, ',', '.'));
                    return $this->redirect("kegiatan/editRevisi?unit_id=" . $unit_id . "&kode_kegiatan=" . $kode_kegiatan);
                } else if ($ceklelangselesaitidakaturanpembayaran == 1) {
                    $this->setFlash('gagal', 'Proses Lelang untuk komponen ini telah selesai, namun belum ada Aturan Pembayaran di eDelivery. Silahkan mengisi Aturan Pembayaran terlebih dahulu.');
                    return $this->redirect("kegiatan/editRevisi?unit_id=" . $unit_id . "&kode_kegiatan=" . $kode_kegiatan);
                } else if ($lelang > 0) {
                    $this->setFlash('gagal', 'Sedang Proses Lelang untuk komponen ini');
                    return $this->redirect("kegiatan/editRevisi?unit_id=" . $unit_id . "&kode_kegiatan=" . $kode_kegiatan);
                } else if (0 < $totNilaiRealisasi) {
                    $this->setFlash('gagal', 'Mohon maaf , untuk komponen  ini sudah terpakai di edelivery, sejumlah Rp.' . number_format($totNilaiRealisasi, 0, ',', '.'));
                    return $this->redirect("kegiatan/editRevisi?unit_id=" . $unit_id . "&kode_kegiatan=" . $kode_kegiatan);
                } else if (0 < $totVolumeRealisasi) {
                    $this->setFlash('gagal', 'Mohon maaf , untuk komponen  ini sudah terpakai di edelivery, dengan volume' . number_format($totVolumeRealisasi, 0, ',', '.'));
                    return $this->redirect("kegiatan/editRevisi?unit_id=" . $unit_id . "&kode_kegiatan=" . $kode_kegiatan);
                }
            }

            //begin transaction
            $con = Propel::getConnection();
            $con->begin();
            try {
                //delete lokasi waiting lama
                $c_hapus_lokasi = new Criteria();
                $c_hapus_lokasi->add(HistoryPekerjaanV2Peer::KODE_RKA, $kode_rka_waiting);
                $lokasi_hapus = HistoryPekerjaanV2Peer::doSelect($c_hapus_lokasi);
                foreach ($lokasi_hapus as $lokasi_value) {
                    
                }

                //delete geojsonlokasi waiting
                foreach ($hapus_geojson as $value_geojson) {
                    $value_geojson->setStatusHapus(true);
                    $value_geojson->save();
                }

                //ambil lokasi rincian detail di history pekerjaan V2
                $c_buat_lokasi = new Criteria();
                $c_buat_lokasi->add(HistoryPekerjaanV2Peer::KODE_RKA, $kode_rka);
                $c_buat_lokasi->addAnd(HistoryPekerjaanV2Peer::STATUS_HAPUS, FALSE);
                $lokasi_lama = HistoryPekerjaanV2Peer::doSelect($c_buat_lokasi);

                foreach ($lokasi_lama as $dapat_lokasi_lama) {
                    $jalan_fix = '';
                    $gang_fix = '';
                    $nomor_fix = '';
                    $rw_fix = '';
                    $rt_fix = '';
                    $keterangan_fix = '';
                    $tempat_fix = '';
                    $jalan_lama = $dapat_lokasi_lama->getJalan();
                    $gang_lama = $dapat_lokasi_lama->getGang();
                    $nomor_lama = $dapat_lokasi_lama->getNomor();
                    $rw_lama = $dapat_lokasi_lama->getRw();
                    $rt_lama = $dapat_lokasi_lama->getRt();
                    $keterangan_lama = $dapat_lokasi_lama->getKeterangan();
                    $tempat_lama = $dapat_lokasi_lama->getTempat();
                    if ($jalan_lama <> '') {
                        $jalan_fix = 'JL. ' . strtoupper($jalan_lama) . ' ';
                    }
                    if ($tempat_lama <> '') {
                        $tempat_fix = '(' . strtoupper($tempat_lama) . ') ';
                    }
                    if ($gang_lama <> '') {
                        $gang_fix = $gang_lama . ' ';
                    }
                    if ($nomor_lama <> '') {
                        $nomor_fix = 'NO ' . strtoupper($nomor_lama) . ' ';
                    }
                    if ($rw_lama <> '') {
                        $rw_fix = 'RW ' . strtoupper($rw_lama) . ' ';
                    }
                    if ($rt_lama <> '') {
                        $rt_fix = 'RT ' . strtoupper($rt_lama) . ' ';
                    }
                    if ($keterangan_lama <> '') {
                        $keterangan_fix = '' . strtoupper($keterangan_lama) . ' ';
                    }
                    $lokasi_baru = $tempat_fix . '' . $jalan_fix . '' . $gang_fix . '' . $nomor_fix . '' . $rw_fix . '' . $rt_fix . '' . $keterangan_fix;

                    //insert lokasi waiting
                    $komponen_lokasi_fix = $rd->getKomponenName() . ' ' . $rd->getDetailName();
                    $kecamatan_lokasi_fix = $rd->getLokasiKecamatan();
                    $kelurahan_lokasi_fix = $rd->getLokasiKelurahan();
                    $lokasi_per_titik_fix = $lokasi_baru;

                    $c_insert_gis = new HistoryPekerjaanV2();
                    $c_insert_gis->setTahun(sfConfig::get('app_tahun_default'));
                    $c_insert_gis->setKodeRka($kode_rka_waiting);
                    $c_insert_gis->setStatusHapus(FALSE);
                    $c_insert_gis->setJalan(strtoupper($jalan_lama));
                    $c_insert_gis->setGang(strtoupper($gang_lama));
                    $c_insert_gis->setNomor(strtoupper($nomor_lama));
                    $c_insert_gis->setRw(strtoupper($rw_lama));
                    $c_insert_gis->setRt(strtoupper($rt_lama));
                    $c_insert_gis->setKeterangan(strtoupper($keterangan_lama));
                    $c_insert_gis->setTempat(strtoupper($tempat_lama));
                    $c_insert_gis->setKomponen($komponen_lokasi_fix);
                    $c_insert_gis->setKecamatan($kecamatan_lokasi_fix);
                    $c_insert_gis->setKelurahan($kelurahan_lokasi_fix);
                    $c_insert_gis->setLokasi($lokasi_per_titik_fix);
                    $c_insert_gis->save();
                    $dapat_lokasi_lama->setStatusHapus(true);
                    $dapat_lokasi_lama->save();
                }

                foreach ($dapat_geojson as $value_geojson) {

                    //insert geojsonlokasi waiting
                    $geojson_baru = new GeojsonlokasiWaitinglist();
                    $geojson_baru->setUnitId('XXX' . $unit_id);
                    $geojson_baru->setUnitName($data_unit_kerja->getUnitName());
                    $geojson_baru->setKegiatanCode($kode_kegiatan);
                    $geojson_baru->setIdWaiting($id_waiting);
                    $geojson_baru->setSatuan($rd->getSatuan());
                    $geojson_baru->setVolume($rd->getVolume());
                    $geojson_baru->setNilaiAnggaran($rd->getNilaiAnggaran());
                    $geojson_baru->setTahun(sfConfig::get('app_tahun_default'));
                    $geojson_baru->setMlokasi($value_geojson->getMlokasi());
                    $geojson_baru->setIdKelompok($value_geojson->getIdKelompok());
                    $geojson_baru->setGeojson($value_geojson->getGeojson());
                    $geojson_baru->setKeterangan($value_geojson->getKeterangan());
                    $geojson_baru->setNmuser($value_geojson->getNmuser());
                    $geojson_baru->setLevel($value_geojson->getLevel());
                    $geojson_baru->setKomponenName($rd->getKomponenName() . ' ' . $rd->getDetailName());
                    $geojson_baru->setStatusHapus(FALSE);
                    $geojson_baru->setKeteranganAlamat($value_geojson->getKeteranganAlamat());
                    $geojson_baru->setLastCreateTime($sekarang);
                    $geojson_baru->setLastEditTime($sekarang);
                    $geojson_baru->setKoordinat($value_geojson->getKoordinat());
                    $geojson_baru->setLokasiKe($value_geojson->getLokasiKe());
                    $geojson_baru->save();

                    //delete geojsonlokasi rev1
                    $value_geojson->setStatusHapus(true);
                    $value_geojson->save();
                }

                //update prioritas
                $total_aktif = 0;
                $query = "select max(prioritas) as total "
                        . "from " . sfConfig::get('app_default_schema') . ".waitinglist_pu "
                        . "where status_hapus = false and status_waiting = 0 "
                        . "and unit_id = 'XXX$unit_id' and kegiatan_code = '" . $kode_kegiatan . "'";
                $stmt = $con->prepareStatement($query);
                $rs = $stmt->executeQuery();
                while ($rs->next()) {
                    $total_aktif = $rs->getString('total');
                }
                for ($index = $prioritas; $index <= $total_aktif; $index++) {
                    $index_tambah_satu = $index + 1;
                    $c_prioritas = new Criteria();
                    $c_prioritas->add(WaitingListPUPeer::UNIT_ID, 'XXX' . $unit_id);
                    $c_prioritas->addAnd(WaitingListPUPeer::KEGIATAN_CODE, $kode_kegiatan);
                    $c_prioritas->addAnd(WaitingListPUPeer::PRIORITAS, $index);
                    $c_prioritas->addAnd(WaitingListPUPeer::STATUS_HAPUS, FALSE);
                    $c_prioritas->addAnd(WaitingListPUPeer::STATUS_WAITING, 0);
                    if ($waiting_prio = WaitingListPUPeer::doSelectOne($c_prioritas)) {
                        $waiting_prio->setPrioritas($index_tambah_satu);
                        $waiting_prio->save();
                    }
                }

                //update waitinglist pu
                $waiting->setSubtitle($rd->getSubtitle());
                $waiting->setKomponenId($rd->getKomponenId());
                $waiting->setKomponenName($rd->getKomponenName());
                $waiting->setKomponenRekening($rd->getRekeningCode());
                $waiting->setKomponenLokasi('(' . $lokasi_baru . ')');
                $waiting->setKomponenHargaAwal($rd->getKomponenHargaAwal());
                $waiting->setPajak($rd->getPajak());
                $waiting->setKomponenSatuan($rd->getSatuan());
                $waiting->setKoefisien($rd->getKeteranganKoefisien());
                $waiting->setVolume($rd->getVolume());
                $waiting->setTahunInput(sfConfig::get('app_tahun_default'));
                $waiting->setUpdatedAt($sekarang);
                $waiting->setKecamatan($rd->getLokasiKecamatan());
                $waiting->setKelurahan($rd->getLokasiKelurahan());
                $waiting->setIsMusrenbang($rd->getIsMusrenbang());
                $waiting->setNilaiAnggaran($rd->getNilaiAnggaran());
                $waiting->setKeterangan($keterangan);
                $waiting->setStatusHapus(false);
                $waiting->setStatusWaiting(0);
                $waiting->setKodeRka(null);
                $waiting->save();

                //update rincian detail
                $rd->setStatusHapus(true);
                $rd->save();

                budgetLogger::log('Mengembalikan komponen ' . $komponen_name . ' ' . $detail_name . ' ke waitinglist');
                $con->commit();
                $this->setFlash('berhasil', 'Telah berhasil memproses ' . $komponen_name . ' ' . $detail_name . ' kembali ke Waiting List');
                return $this->redirect("kegiatan/editRevisi?unit_id=" . $unit_id . "&kode_kegiatan=" . $kode_kegiatan);
            } catch (Exception $ex) {
                $con->rollback();
                $this->setFlash('gagal', 'Gagal Karena ' . $ex->getMessage());
                return $this->redirect("kegiatan/editRevisi?unit_id=" . $unit_id . "&kode_kegiatan=" . $kode_kegiatan);
            }
        }
    }

    //irul 29sept2015 - hapusLokasi
    public function executeHapusLokasi() {
        $unit_id = $this->getRequestParameter('unit');
        $kegiatan_code = $this->getRequestParameter('kegiatan');
        $detail_no = $this->getRequestParameter('no');

        $kode_rka = $unit_id . '.' . $kegiatan_code . '.' . $detail_no;

        $con = Propel::getConnection();
        $con->begin();
        try {
            $sekarang = date('Y-m-d H:i:s');
            $sql = "update " . sfConfig::get('app_default_gis') . ".geojsonlokasi_rev1 "
                    . "set status_hapus = TRUE, last_edit_time = '$sekarang' "
                    . "where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and detail_no = $detail_no ";
            $stmt = $con->prepareStatement($sql);
            $stmt->executeQuery();

            $con->commit();
            $this->setFlash('berhasil', 'Berhasil menghapus Lokasi untuk kode ' . $kode_rka);
            budgetLogger::log('menghapus GIS untuk kode RKA  ' . $kode_rka);

            historyUserLog::hapus_lokasi($unit_id, $kegiatan_code, $detail_no);
        } catch (Exception $exc) {

            $con->rollback();
            $this->setFlash('gagal', 'Hapus lokasi gagal karena ' . $exc->getMessage());
        }
        return $this->redirect('kegiatan/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
    }

    //irul 29sept2015 - hapusLokasi


    public function executeBukaSubtitle() {
        $subtitle = str_replace('=', '/', $this->getRequestParameter('subtitle'));
        $unit_id = $this->getRequestParameter('unit_id');
        $kegiatan_code = $this->getRequestParameter('kegiatan_code');
        $sub_id = $this->getRequestParameter('sub_id');

        $sql = "update " . sfConfig::get('app_default_schema') . ".rincian_detail set lock_subtitle='' where subtitle='$subtitle' and unit_id='$unit_id' and kegiatan_code='$kegiatan_code'";
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($sql);
        $stmt->executeQuery();

        $sql2 = "update " . sfConfig::get('app_default_schema') . ".subtitle_indikator set lock_subtitle=false where subtitle='$subtitle' and unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and sub_id='$sub_id'";
        $stmt2 = $con->prepareStatement($sql2);
        $stmt2->executeQuery();

        $this->setFlash('berhasil', 'Subtitle ' . $subtitle . ' telah dibuka');
        //$this->forward('/peneliti','edit?unit_id='.$unit_id.'&kode_kegiatan='.$kegiatan_code);
        return $this->redirect('kegiatan/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
    }

    public function executeKuncidp() {
        if ($this->getRequestParameter('unit_id')) {
            $unit_id = $this->getRequestParameter('unit_id');
            $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');

            $sql = "update " . sfConfig::get('app_default_schema') . ".rincian 
                set lock=TRUE,rincian_confirmed=TRUE, rincian_selesai=TRUE 
                where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and tahun='" . sfConfig::get('app_tahun_default') . "'";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($sql);
            $stmt->executeQuery();

            $sql =
            "DELETE FROM " . sfConfig::get('app_default_schema') . ".buka_komponen_murni
            WHERE detail_kegiatan ILIKE '%$kode_kegiatan%'";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($sql);
            $stmt->executeQuery();
            //var_dump($stmt);
            //awal-comment
            //$this->forward('kegiatan', 'list');
            //awal-comment
            //irul 26 feb 2014
            $this->setFlash('berhasil', 'Berhasil menutup kegiatan ' . $kode_kegiatan);
            budgetLogger::log('Mengunci RKA dinas ' . $unit_id . ' Kegiatan ' . $kode_kegiatan);
            return $this->redirect('kegiatan/list?unit_id=' . $unit_id);
            //irul 26 feb 2014
        }
    }

    public function executeBukadp() {
        if ($this->getRequestParameter('unit_id')) {
            $unit_id = $this->getRequestParameter('unit_id');
            $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');

            $sql = "update " . sfConfig::get('app_default_schema') . ".rincian 
                set lock=FALSE, rincian_confirmed=FALSE, rincian_selesai=FALSE  
                where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and tahun='" . sfConfig::get('app_tahun_default') . "'";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($sql);
            $stmt->executeQuery();
            //awal-comment
            //$this->forward('kegiatan', 'list');
            //awal-comment
            //irul 26 feb 2014
            $this->setFlash('berhasil', 'Berhasil membuka kegiatan ' . $kode_kegiatan);
            budgetLogger::log('Membuka RKA dinas ' . $unit_id . ' Kegiatan ' . $kode_kegiatan);
            return $this->redirect('kegiatan/list?unit_id=' . $unit_id);
            //irul 26 feb 2014
        }
    }

    public function executeKuncidpRevisi() {
        if ($this->getRequestParameter('unit_id')) {
            $unit_id = $this->getRequestParameter('unit_id');
            $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');

            $sql = "update " . sfConfig::get('app_default_schema') . ".dinas_rincian 
                set lock=TRUE,rincian_confirmed=TRUE, rincian_selesai=TRUE, rincian_level=3 
                where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and tahun='" . sfConfig::get('app_tahun_default') . "'";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($sql);
            $stmt->executeQuery();

            $sql =
            "DELETE FROM " . sfConfig::get('app_default_schema') . ".buka_komponen_murni
            WHERE detail_kegiatan ILIKE '%$kode_kegiatan%'";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($sql);
            $stmt->executeQuery();
            $this->setFlash('berhasil', 'Berhasil menutup kegiatan ' . $kode_kegiatan);
            budgetLogger::log('Mengunci Revisi dinas ' . $unit_id . ' Kegiatan ' . $kode_kegiatan);
            return $this->redirect('kegiatan/listRevisi?unit_id=' . $unit_id);
            //irul 26 feb 2014
        }
    }

    public function executeBukadpRevisi() {
        if ($this->getRequestParameter('unit_id')) {
            $unit_id = $this->getRequestParameter('unit_id');
            $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');

            $sql = "update " . sfConfig::get('app_default_schema') . ".dinas_rincian 
                set lock=FALSE, rincian_confirmed=FALSE, rincian_selesai=FALSE  
                where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and tahun='" . sfConfig::get('app_tahun_default') . "'";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($sql);
            $stmt->executeQuery();
            $this->setFlash('berhasil', 'Berhasil membuka kegiatan ' . $kode_kegiatan);
            budgetLogger::log('Membuka Revisi dinas ' . $unit_id . ' Kegiatan ' . $kode_kegiatan);
            return $this->redirect('kegiatan/listRevisi?unit_id=' . $unit_id);
            //irul 26 feb 2014
        }
    }

    public function executePrintBanding() {
        $unit_id = $this->getRequestParameter('unit_id');
        $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
        $this->kode_kegiatan = $kode_kegiatan;
        $c = new Criteria();
        $c->add(MasterKegiatanPeer::UNIT_ID, $unit_id);
        $c->add(MasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
        $master_kegiatan = MasterKegiatanPeer::doSelectOne($c);
        if ($master_kegiatan) {
            $this->kode_program22 = substr($master_kegiatan->getKodeProgram2(), 5, 2);
            if (substr($master_kegiatan->getKodeProgram2(), 0, 4) == 'X.XX') {
                $this->kode_urusan = substr($master_kegiatan->getKodeUrusan(), 0, 4);
            } else {
                $this->kode_urusan = substr($master_kegiatan->getKodeProgram2(), 0, 4);
            }
            //print_r($kode_program22);
            //exit;
            $e = new Criteria();
            $e->add(UnitKerjaPeer::UNIT_ID, $master_kegiatan->getUnitId());
            $es = UnitKerjaPeer::doSelectOne($e);
            if ($es) {
                $this->kode_permen = $es->getKodePermen();
            }


            $this->kode = $this->kode_urusan . '.' . $this->kode_permen . '.' . $this->kode_program22 . '.' . $master_kegiatan->getKodeKegiatan();
            $this->kode_kegiatan2 = $this->kode_urusan . '.' . $this->kode_program . '.' . $this->kode_program22 . '.' . $master_kegiatan->getKodeKegiatan();
            $this->nama_kegiatan = $master_kegiatan->getNamaKegiatan();
            $u = new Criteria();
            $u->add(MasterUrusanPeer::KODE_URUSAN, $this->kode_urusan);
            $us = MasterUrusanPeer::doSelectOne($u);
            if ($us) {
                $this->nama_urusan = $us->getNamaUrusan();
            }

            $this->kode_program = $master_kegiatan->getKodeProgram();
            $this->unit_id = $unit_id;
            $u = new Criteria();
            $u->add(UnitKerjaPeer::UNIT_ID, $unit_id);
            $us = UnitKerjaPeer::doSelectOne($u);
            if ($us) {
                $this->unit_kerja = $us->getUnitName();
            }

            $query = "select *
				from " . sfConfig::get('app_default_schema') . ".master_program kp
				where kp.kode_program='" . $this->kode_program . "' and kp.kode_tujuan ilike '" . $master_kegiatan->getKodeTujuan() . "'";
            //print_r($query);exit;
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs1 = $stmt->executeQuery();
            while ($rs1->next()) {
                $this->kode_program1 = $rs1->getString('kode_program');
                $this->nama_program = $rs1->getString('nama_program');
            }

            $query = "select *
				from " . sfConfig::get('app_default_schema') . ".master_program2 kp2
				where kp2.kode_program='" . $this->kode_program . "' and kp2.kode_program2 ilike '" . $this->kode_program22 . "'";
            //print_r($query);exit;
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs1 = $stmt->executeQuery();
            while ($rs1->next()) {
                $this->nama_program22 = $rs1->getString('nama_program2');
            }

            $query = "select *
				from " . sfConfig::get('app_default_schema') . ".master_sasaran kp2
				where kp2.kode_sasaran='" . $master_kegiatan->getKodeSasaran() . "'";
            //print_r($query);exit;
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs1 = $stmt->executeQuery();
            while ($rs1->next()) {
                $this->nama_sasaran = $rs1->getString('nama_sasaran');
                $this->kode_sasaran = $rs1->getString('kode_sasaran');
            }

            $query = "select sum(round(rd.volume * rd.komponen_harga_awal * (100 + rd.pajak) / 100)) as nilai from " . sfConfig::get('app_default_schema') . ".rincian_detail_bp rd
						where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and status_hapus=FALSE";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs1 = $stmt->executeQuery();
            while ($rs1->next()) {
                $this->total_semula = $rs1->getString('nilai');
            }

            $query = "select sum(round(rd.volume * rd.komponen_harga_awal * (100 + rd.pajak) / 100)) as nilai from " . sfConfig::get('app_default_schema') . ".rincian_detail rd
						where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and status_hapus=FALSE";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs1 = $stmt->executeQuery();
            while ($rs1->next()) {
                $this->total_sekarang = $rs1->getString('nilai');
            }

            $this->setLayout('kosong');
            $this->getResponse()->addStylesheet('tampilan_print2', '', array('media' => 'print'));
        }
    }

    public function executeLihatSsh() { //fungsi mbulet sesuai mas punya mas bayu. *maafkan*
        $id = $this->getRequestParameter('komponen_id');
        $c = new Criteria();
        $c->add(KomponenPeer::KOMPONEN_ID, $id);
        $ssh = $this->cs = KomponenPeer::doSelectOne($c);

        $shsd_id = $ssh->getShsdId();
        $c_spek = new Criteria();
        $c_spek->add(ShsdPeer::SHSD_ID, $shsd_id);
        $this->spek = SHsdPeer::doSelectOne($c_spek);
        $this->setLayout('popup');
    }

    public function executeLihatSAB() {
        //$coded='';
        $coded = $this->getRequestParameter('coded', '');
        $komponen_id = $this->getRequestParameter('komponen_id');
        //print_r(substr(trim($komponen_id),0,2));exit;
        if (substr(trim($komponen_id), 0, 2) <= 28) {
            return $this->redirect('kegiatan/lihatsabfisik?id=' . $komponen_id . '&coded=' . $coded);
        } elseif (substr($komponen_id, 0, 2) < 30) {
            return $this->redirect('kegiatan/lihatsublist?id=' . $komponen_id . '&coded=' . $coded);
        } elseif (substr($komponen_id, 0, 2) >= 30) {

            return $this->redirect('kegiatan/lihatsabom?id=' . $komponen_id . '&coded=' . $coded);
        }
    }

    public function executeLihatsabfisik() {
        $coded = $this->getRequestParameter('coded', '');
        $this->komponen_id = $this->getRequestParameter('komponen_id');
    }

    public function executeLihatsublist() {
        $coded = $this->getRequestParameter('coded', '');
        $sub_id = $this->getRequestParameter('id');
        $input = array();
        $c = new Criteria();
        $c->add(SubKegiatanPeer::SUB_KEGIATAN_ID, $sub_id);
        //$c -> add (SubKegiatanPeer::STATUS, 'Close') ;
        $cs = SubKegiatanPeer::doSelectOne($c);
        if ($cs) {
            $this->sub_kegiatan_id = $cs->getSubKegiatanId();
            $this->sub_kegiatan_name = $cs->getSubKegiatanName();
            $this->param = $cs->getParam();
            $this->satuan = $cs->getSatuan();
        }

        $d = new Criteria();
        $d->add(KomponenMemberPeer::KOMPONEN_ID, $sub_id);
        $d->addAscendingOrderByColumn(KomponenMemberPeer::SUBTITLE);
        $d->addAscendingOrderByColumn(KomponenMemberPeer::TIPE);
        $ds = KomponenMemberPeer::doSelect($d);
        $this->komponen_penyusun = $ds;
        $total = 0;
        foreach ($ds as $x) {
            $total = $total + $x->getMemberTotal();
        }
        $this->total_penyusun = $total;
        $sql = "select 
		sum(m.volume*m.komponen_harga_awal*(100+m.pajak)/100) as nilai_satuan
		from 
		" . sfConfig::get('app_default_schema') . ".sub_kegiatan_member m
		where 
		m.sub_kegiatan_id = '" . $sub_id . "'";
        //print_r($sql);exit;
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($sql);
        $rs = $stmt->executeQuery();
        while ($rs->next()) {
            $nilai_satuan = $rs->getString('nilai_satuan');
        }
        $this->nilai_satuan = $nilai_satuan;

        $query = "
		SELECT 
		rekening.rekening_code,
		detail.detail_name as detail_name,
		detail.komponen_name,
		detail.komponen_name || ' ' || detail.detail_name as detail_name2,
		detail.komponen_harga_awal as detail_harga,
		detail.pajak,
		detail.komponen_id,
		detail.subtitle ,
		detail_no,koefisien,param,
		detail.satuan as detail_satuan,
		replace(detail.keterangan_koefisien,'<*>','X') as keterangan_koefisien,
		detail.volume * detail.komponen_harga_awal as hasil,
		(detail.volume * detail.komponen_harga_awal  * (100+detail.pajak)/100) as hasil_kali,
		
		
		(SELECT '2' FROM " . sfConfig::get('app_default_schema') . ".komponen where komponen.komponen_id=detail.komponen_id AND komponen.komponen_tipe='EST')
		as x,
		(SELECT 'bahaya' FROM " . sfConfig::get('app_default_schema') . ".komponen where komponen.komponen_id=detail.komponen_id AND komponen_show=FALSE)
		as bahaya,
		substring(kb.belanja_name,8) as belanja_name
		
		FROM 
		" . sfConfig::get('app_default_schema') . ".rekening rekening ,
		" . sfConfig::get('app_default_schema') . ".sub_kegiatan_member detail ,
		" . sfConfig::get('app_default_schema') . ".kelompok_belanja kb
		
		WHERE 
		rekening.rekening_code = detail.rekening_code and
		detail.sub_kegiatan_id = '" . $sub_id . "' and 
		kb.belanja_id=rekening.belanja_id 
		
		ORDER BY 
		
		belanja_urutan,
		komponen_name
		";
        //echo $query;
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query);
        $rs = $stmt->executeQuery();
        $this->r = $rs;
        //$this->input = Array();
        $keterangan_koefisien = array();
        $detail_harga = array();
        $hasil_kali = array();
        $hasil = array();
        $detail_name = array();
        $detail_satuan = array();
        $pajak = array();
        $belanja_name = array();
        $this->keterangan_koefisien = Array();
        $this->detail_harga = Array();
        $this->hasil_kali = Array();
        $this->hasil = Array();
        $this->detail_name = array();
        $this->detail_satuan = array();
        $this->pajak = array();
        $this->belanja_name = array();
        $i = 0;
        while ($rs->next()) {
            $pars = explode("|", $rs->getString('param'));

            for ($j = 0; $j < count($pars); $j++) {
                if ($j == 0)
                    $inputs = $pars[$j];
                else
                    $inputs = $inputs . ',<BR>' . $pars[$j];
            }
            //echo $r['param'];
            $input[$i] = $inputs;
            //print_r($inputs);exit;

            $detail_name[$i] = $rs->getString('detail_name2');
            $detail_satuan[$i] = $rs->getString('detail_satuan');
            $pajak[$i] = $rs->getString('pajak');
            $belanja_name[$i] = $rs->getString('belanja_name');

            $keterangan_koefisien[$i] = $rs->getString('keterangan_koefisien');
            $nilai_satuan+=$rs->getString('hasil_kali');

            $detail_harga[$i] = number_format($rs->getString('detail_harga'), 0, ",", ".");
            $hasil_kali[$i] = number_format($rs->getString('hasil_kali'), 0, ",", ".");

            $hasil[$i] = number_format($rs->getString('hasil'), 0, ",", ".");
            $i = $i + 1;
        }
        $this->banyak = $i;
        //$this->nilai_satuan = $nilai_satuan;
        $this->input = $input;
        $this->keterangan_koefisien = $keterangan_koefisien;
        $this->detail_harga = $detail_harga;
        $this->hasil_kali = $hasil_kali;
        $this->hasil = $hasil;
        $this->detail_name = $detail_name;
        $this->detail_satuan = $detail_satuan;
        $this->pajak = $pajak;
        $this->belanja_name = $belanja_name;
        return sfView::SUCCESS;
    }

    public function executeLihatsabom() {
        $coded = $this->getRequestParameter('coded', '');
        return sfView::SUCCESS;
    }

    public function executeSimpanSubKegiatan() {
        $sub_kegiatan_id = $this->getRequestParameter('sub_kegiatan_id');
        $kodesub = $this->getRequestParameter('kodesub');
        $kegiatan_code = $this->getRequestParameter('kegiatan_code');
        $unit_id = $this->getRequestParameter('unit_id');

        if ($this->getRequestParameter('lokasipeta') == 'lokasi') {
            $user = $this->getUser();
            $user->removeCredential('lokasi');
            $user->addCredential('lokasi');
            $user->setAttribute('nama', $this->getRequestParameter('keterangan'), 'lokasi');
            $user->setAttribute('id', $sub_kegiatan_id, 'lokasi');
            $user->setAttribute('kegiatan', $kegiatan_code, 'lokasi');
            $user->setAttribute('unit', $unit_id, 'lokasi');
            $user->setAttribute('subtitle', $this->getRequestParameter('subtitle'), 'lokasi');
            $user->setAttribute('tipe', $this->getRequestParameter('tipe'), 'lokasi');
            $user->setAttribute('barusub', 'barusub', 'lokasi');

            return $this->forward('lokasi', 'list');
        }
        if ($this->getRequestParameter('simpansub') == 'simpan') {
            $sql = "select max(kode_sub) as kode_sub from " . sfConfig::get('app_default_schema') . ".rincian_sub_parameter where kode_sub ilike 'RSUB%'";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($sql);
            $rs = $stmt->executeQuery();
            while ($rs->next()) {
                $kodesub = $rs->getString('kode_sub');
            }
            $kode = substr($kodesub, 4, 5);
            $kode+=1;
            if ($kode < 10) {
                $kodesub = 'RSUB0000' . $kode;
            } elseif ($kode < 100) {
                $kodesub = 'RSUB000' . $kode;
            } elseif ($kode < 1000) {
                $kodesub = 'RSUB00' . $kode;
            } elseif ($kode < 10000) {
                $kodesub = 'RSUB0' . $kode;
            } elseif ($kode < 100000) {
                $kodesub = 'RSUB' . $kode;
            }

            if ($this->getRequestParameter('keterangan')) {
                $m_lokasi_ada = 'tidak ada';
                $lokasi = $this->getRequestParameter('keterangan');
                $query = "select (1) as ada from v_lokasi where nama ilike '$lokasi'";
                $con = Propel::getConnection();
                $stmt = $con->prepareStatement($query);
                $rs_cek = $stmt->executeQuery();
                while ($rs_cek->next()) {
                    if ($rs_cek->getString('ada')) {
                        $m_lokasi_ada = 'ada';
                    }
                }
            }
            //print_r($m_lokasi_ada);exit;
            if ($m_lokasi_ada == 'tidak ada') {
                $this->setFlash('error_lokasi', 'Lokasi yang dimasukkan tidak ada dalam G.I.S');
                return $this->redirect("kegiatan/pilihSubKegiatan?id=$sub_kegiatan_id&kode_kegiatan=$kegiatan_code&unit_id=$unit_id&sub=Pilih");
            }

            if ($this->getRequestParameter('subtitle')) {
                $kode_sub = $this->getRequestParameter('subtitle');
                $c = new Criteria();
                $c->add(SubtitleIndikatorPeer::UNIT_ID, $unit_id);
                $c->add(SubtitleIndikatorPeer::KEGIATAN_CODE, $kegiatan_code);
                $c->add(SubtitleIndikatorPeer::SUB_ID, $kode_sub);
                $rs_subtitle = SubtitleIndikatorPeer::doSelectOne($c);
                if ($rs_subtitle) {
                    $subtitle = $rs_subtitle->getSubtitle();
                }
            }

            $keter = $this->getRequestParameter('keterangan1') . ' ' . $this->getRequestParameter('keterangan');
            $detail_name = str_replace("'", " ", $keter);
            //$subtitle = $this->getRequestParameter('subtitle');
            $query = "SELECT * from " . sfConfig::get('app_default_schema') . ".sub_kegiatan where sub_kegiatan_id='" . $sub_kegiatan_id . "'";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs = $stmt->executeQuery();

            while ($rs->next()) {
                $sub_kegiatan_name = $rs->getString('sub_kegiatan_name');
                $subtitle2 = $sub_kegiatan_name . " " . $keter;
                $param = $rs->getString('param');
                $satuan = $rs->getString('satuan');
                $pembagi = $rs->getString('pembagi');
                $new_subtitle = $sub_kegiatan_name . " " . $keter . " " . $subtitle;
                if (($sub_kegiatan_id != '') && ($new_subtitle != '')) {
                    $query = "select kode_sub from " . sfConfig::get('app_default_schema') . ".rincian_sub_parameter where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and subtitle='$subtitle' and from_sub_kegiatan='$sub_kegiatan_id' and detail_name='$detail_name'";
                    $con = Propel::getConnection();
                    $stmt = $con->prepareStatement($query);
                    $rs_cek = $stmt->executeQuery();
                    while ($rs_cek->next()) {
                        if ($rs_cek->getString('kode_sub')) {
                            $kodesub = $rs_cek->getString('kode_sub');
                            $this->setFlash('error_lokasi', 'ASB Non Fisik dengan Subtitle dengan Kegiatan Sudah Ada. Mohon Diperhatikan Kembali');
                            return $this->redirect("kegiatan/pilihSubKegiatan?id=$sub_kegiatan_id&kode_kegiatan=$kegiatan_code&unit_id=$unit_id&sub=Pilih");
                        }
                    }
                }
            }
            $arrparamsub = explode("|", $param);
            $arr_pembagi = explode("|", $pembagi);
            $keterangan = '';
            $arr_input = '';
            $jumlah = count($arrparamsub);
            //$j=0;
            for ($i = 0; $i < $jumlah; $i++) {
                //print_r(str_replace(' ','',$arrparamsub[$i]).'pppppp<br>');
                //echo $this->getRequestParameter('param_'.str_replace(' ','',$arrparamsub[$i]));

                if ($this->getRequestParameter('param_' . $i) != '') {
                    if ($i == 0) {

                        $arr_input = $this->getRequestParameter('param_' . $i);
                        //print_r($arr_input).'<br>';
                    }
                    if ($i != 0) {
                        $arr_input = $arr_input . '|' . $this->getRequestParameter('param_' . $i);
                    }
                }
            }
            //print_r($arr_input);exit;
            $arrsatuansub = explode("|", $satuan);
            //$arr_input=implode("|",$inp);
            $inp = array();
            $inp = explode("|", $arr_input);
            $arr_pembagi = explode("|", $pembagi);
            $pembagi = 1;
            $ket_pembagi = '';
            //$pembagi='';
            for ($j = 0; $j < count($arrparamsub); $j++) {
                if ($j >= 0) {
                    if (isset($inp[$j])) {
                        if ($inp[$j] != 0)
                            $keterangan = $keterangan . '<tr><td class="Font8v">' . $arrparamsub[$j] . '</td><td class="Font8v">' . $inp[$j] . ' ' . $arrsatuansub[$j] . '</td></tr>';
                    }else {
                        if ((isset($arrparamsub[$j])) && (isset($arrsatuansub[$j])) && (isset($inp[$j]))) {
                            $keterangan = '<table><tr><td class="Font8v">' . $arrparamsub[$j] . '</td><td class="Font8v">' . $inp[$j] . ' ' . $arrsatuansub[$j] . '</td></tr>';
                        }
                    }
                }
                if (isset($arr_pembagi[$j])) {
                    if ($arr_pembagi[$j] == 't') {
                        if ($inp[$j] and $inp[$j] != 0)
                            $pembagi = $pembagi * $inp[$j];
                        $ket_pembagi = $ket_pembagi . "," . $arrparamsub[$j];
                    }
                }
            }
            $keterangan = $keterangan . '</table>';
            $ket_pembagi = substr($ket_pembagi, 1);

            $query = "INSERT INTO " . sfConfig::get('app_default_schema') . ".rincian_sub_parameter(unit_id,kegiatan_code,from_sub_kegiatan,sub_kegiatan_name,subtitle,detail_name,new_subtitle,param,keterangan,ket_pembagi,pembagi,kode_sub) VALUES(
	'" . $unit_id . "','" . $kegiatan_code . "','" . $sub_kegiatan_id . "','" . $sub_kegiatan_name . "','" . $subtitle . "','" . $keter . "', '" . $new_subtitle . "','" . $arr_input . "','" . $keterangan . "','" . $ket_pembagi . "'," . $pembagi . ",'" . $kodesub . "')";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            //echo $query.'<BR>';
            $stmt->executeQuery();

            $query = "SELECT *, satuan as satuan_asli from " . sfConfig::get('app_default_schema') . ".sub_kegiatan_member where sub_kegiatan_id='" . $sub_kegiatan_id . "'";

            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs = $stmt->executeQuery();
            //$volume=1;
            while ($rs->next()) {
                if ($rs->getString('param') == '') {
                    $keterangan_koefisien = $rs->getString('keterangan_koefisien');
                    $volume = $rs->getString('volume');
                } else {
                    $arrparam = array();
                    $arrparam = explode("|", $rs->getString('param'));

                    $volume = $rs->getString('volume');

                    $keterangan_koefisien = '';
                    if ($rs->getString('keterangan_koefisien')) {
                        $keterangan_koefisien = $rs->getString('keterangan_koefisien');
                    }
                    for ($j = 0; $j < count($arrparam); $j++) {

                        for ($i = 0; $i < count($arrparamsub); $i++) {

                            if (str_replace(' ', '', $arrparam[$j]) == str_replace(' ', '', $arrparamsub[$i])) {

                                $volume = $volume * $inp[$i];

                                if ($keterangan_koefisien != '') {

                                    $keterangan_koefisien = $keterangan_koefisien . ' X ' . $inp[$i] . ' ' . $arrsatuansub[$i] . '(Fix)';
                                } else {

                                    $keterangan_koefisien = $inp[$i] . ' ' . $arrsatuansub[$i] . '(Fix)';
                                }
                            }
                        }
                    }
                }


                if (stristr($keterangan_koefisien, "'")) {
                    $keterangan_koefisien = addslashes($keterangan_koefisien);
                    $detail_name = addslashes($rs->getString('detail_name'));
                }
                $query1 = "SELECT max(detail_no) AS maxno FROM " . sfConfig::get('app_default_schema') . ".rincian_detail 
				 WHERE kegiatan_code='" . $kegiatan_code . "'  and unit_id='" . $unit_id . "'";


                $con = Propel::getConnection();
                $stmt1 = $con->prepareStatement($query1);
                $rs1 = $stmt1->executeQuery();
                //$maxno=0;
                //echo $query1;exit; 
                while ($rs1->next()) {
                    $maxno = $rs1->getInt('maxno');
                    if (!$maxno) {
                        $maxno = 1;
                    } else {
                        $maxno = $maxno + 1;
                    }
                }
                $pajakawal = $rs->getInt('pajak');

                if (!$pajakawal) {
                    $pajakawal = 0;
                }

                $rekening_code = $rs->getString('rekening_code');
                $komponen_id = $rs->getString('komponen_id');
                $detail_name = $rs->getString('detail_name');
                $komponen_harga_awal = $rs->getString('komponen_harga_awal');
                $komp = new Criteria();
                $komp->add(KomponenPeer::KOMPONEN_ID, $komponen_id);
                $terpilih_komponen = KomponenPeer::doSelectOne($komp);
                if ($terpilih_komponen) {
                    $komponen_name = $terpilih_komponen->getKomponenName();
                }

                if ($this->getRequestParameter('subtitle')) {
                    $kode_sub = $this->getRequestParameter('subtitle');
                    $c = new Criteria();
                    $c->add(SubtitleIndikatorPeer::UNIT_ID, $unit_id);
                    $c->add(SubtitleIndikatorPeer::KEGIATAN_CODE, $kegiatan_code);
                    $c->add(SubtitleIndikatorPeer::SUB_ID, $kode_sub);
                    $rs_subtitle = SubtitleIndikatorPeer::doSelectOne($c);
                    if ($rs_subtitle) {
                        $subtitle = $rs_subtitle->getSubtitle();
                    }
                }

                $query2 = "
		INSERT INTO " . sfConfig::get('app_default_schema') . ".rincian_detail 
		(kegiatan_code,tipe,unit_id,detail_no,rekening_code,komponen_id,detail_name,volume,keterangan_koefisien,subtitle,komponen_harga,pajak,komponen_harga_awal,komponen_name,satuan,from_sub_kegiatan,sub,kode_sub) 
		VALUES 
		('" . $kegiatan_code . "','SUB','" . $unit_id . "'," . $maxno . ",'" . $rekening_code . "','" . $komponen_id . "','" . $detail_name . "'," . $volume . ",'" . $keterangan_koefisien . "','" . $subtitle . "'," . $komponen_harga_awal * (($pajakawal + 100) / 100) . "," . $pajakawal . "," . $komponen_harga_awal . ",'" . $komponen_name . "','" . $rs->getString('satuan_asli') . "','" . $sub_kegiatan_id . "','" . $new_subtitle . "','" . $kodesub . "')
		";

                if (($volume > 0) and ( $keterangan_koefisien != '')) {
                    $con = Propel::getConnection();
                    $stmt = $con->prepareStatement($query2);
                    $stmt->executeQuery();
                }
            }
            $this->setFlash('berhasil', 'Sub Kegiatan Baru Telah Tersimpan');
            return $this->redirect("kegiatan/edit?unit_id=" . $unit_id . "&kode_kegiatan=" . $kegiatan_code);
        }
    }

    public function executePilihSubKegiatan() {
        $sql = "select max(kode_sub) as kode_sub from " . sfConfig::get('app_default_schema') . ".rincian_detail where kode_sub ilike 'RSUB' and sub<>''";
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($sql);
        $rs = $stmt->executeQuery();
        while ($rs->next()) {
            $kodesub = $rs->getString('kode_sub');
        }
        $kode = substr($kodesub, 4, 5);
        $kode+=1;
        if ($kode < 10) {
            $kodesub = 'RSUB0000' . $kode;
        } elseif ($kode < 100) {
            $kodesub = 'RSUB000' . $kode;
        } elseif ($kode < 1000) {
            $kodesub = 'RSUB00' . $kode;
        } elseif ($kode < 10000) {
            $kodesub = 'RSUB0' . $kode;
        } elseif ($kode < 100000) {
            $kodesub = 'RSUB' . $kode;
        }
        $this->kodesub = $kodesub;
        return sfView::SUCCESS;
    }

    public function executeHapusSubPekerjaans() {
        if ($this->getRequestParameter('id')) {

            $kode_sub = $this->getRequestParameter('no');
            $unit_id = $this->getRequestParameter('unit');
            $kegiatan_code = $this->getRequestParameter('kegiatan');

            $tahap = '';
            $c = new Criteria();
            $c->add(MasterKegiatanPeer::UNIT_ID, $unit_id);
            $c->add(MasterKegiatanPeer::KODE_KEGIATAN, $kegiatan_code);
            $rs_kegiatan = MasterKegiatanPeer::doSelectOne($c);
            if ($rs_kegiatan) {
                $tahap = $rs_kegiatan->getTahap();
            }
            /*
              if($tahap=='')
              {
              $query = "delete from ". sfConfig::get('app_default_schema') .".rincian_detail
              where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and kode_sub='$kode_sub'";
              // print_r($query);exit;
              $con=Propel::getConnection(RincianDetailPeer::DATABASE_NAME);
              $statement=$con->prepareStatement($query);
              $statement->executeQuery();


              $query = "delete from ". sfConfig::get('app_default_schema') .".rincian_sub_parameter
              where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and kode_sub='$kode_sub'";
              // print_r($query);exit;
              $con=Propel::getConnection(RincianSubParameterPeer::DATABASE_NAME);
              $statement=$con->prepareStatement($query);
              $statement->executeQuery();
              }
              else if($tahap!='')
              {
              $query = "update ". sfConfig::get('app_default_schema') .".rincian_detail set volume=0,keterangan_koefisien='0'
              where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and kode_sub='$kode_sub'";
              $con=Propel::getConnection(RincianDetailPeer::DATABASE_NAME);
              $statement=$con->prepareStatement($query);
              $statement->executeQuery();

              $query = "update ". sfConfig::get('app_default_schema') .".rincian_detail set sub='',kode_sub=''
              where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and kode_sub='$kode_sub'";
              // print_r($query);exit;
              $con=Propel::getConnection(RincianDetailPeer::DATABASE_NAME);
              $statement=$con->prepareStatement($query);
              $statement->executeQuery();

              $query = "delete from ". sfConfig::get('app_default_schema') .".rincian_sub_parameter
              where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and kode_sub='$kode_sub'";
              // print_r($query);exit;
              $con=Propel::getConnection(RincianSubParameterPeer::DATABASE_NAME);
              $statement=$con->prepareStatement($query);
              $statement->executeQuery();

              $query = "delete from ". sfConfig::get('app_default_schema') .".rincian_sub_parameter
              where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and kode_sub='$kode_sub'";
              // print_r($query);exit;
              $con=Propel::getConnection(RincianSubParameterPeer::DATABASE_NAME);
              $statement=$con->prepareStatement($query);
              $statement->executeQuery();
              }
             */


            $c = new Criteria();
            $c->add(RincianDetailPeer::UNIT_ID, $unit_id);
            $c->add(RincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
            $c->add(RincianDetailPeer::KODE_SUB, $kode_sub);
            $jumlah = RincianDetailPeer::doCount($c);
            $volume = 0;
            $keterangan_koefisien = '0';
            $sub = '';
            $kode_sub = '';
            $kode_jasmas = '';
            $nilaiBaru = 0;
            if ($jumlah == 1) {
                $rincian_detail = RincianDetailPeer::doSelectOne($c);
                if ($rincian_detail) {
                    $detail_no = $rincian_detail->getDetailNo();
                    $pajak = $rincian_detail->getPajak();
                    $harga = $rincian_detail->getKomponenHargaAwal();
                    $nilaiBaru = round($harga * $volume * (100 + $pajak) / 100);
                    $nilaiBaru = $rincian_detail->getNilaiAnggaran();
                    //sfContext::getInstance()->getLogger()->debug('{eProject} rincian detail ketemu, nilai baru = '.$nilaiBaru);
                    $rincian_detail->setKeteranganKoefisien($keterangan_koefisien);
                    //$rincian_detail->setVolume($volume);
                    $rincian_detail->setDetailName($detail_name);
                    $rincian_detail->setSubtitle($subtitle);
                    $rincian_detail->setSub($sub);
                    $rincian_detail->setKodeSub($kode_sub);
                    $rincian_detail->setKecamatan($kode_jasmas);

                    $rincian_detail->setStatusHapus(TRUE);
                    //djieb: start edit from here
                    //eproject
                    $nilaiTerpakai = 0; //default 0
                    $query = "SELECT
                                          s.kode,k.kode,dk.kode_detail_kegiatan,dk.id_budgeting, sum(dp.ALOKASI) as jml
                                        from
                                          " . sfConfig::get('app_default_eproject') . ".detail_pekerjaan dp,
                                          " . sfConfig::get('app_default_eproject') . ".pekerjaan p,
                                          " . sfConfig::get('app_default_eproject') . ".detail_kegiatan dk,
                                          " . sfConfig::get('app_default_eproject') . ".kegiatan k,
                                          " . sfConfig::get('app_default_eproject') . ".skpd s
                                        where
                                          ((p.STATE<>0 and p.STATE IS NOT NULL) or p.pernah_realisasi=1 or pernah_konfirmasi=1) and
                                          dp.pekerjaan_id=p.id  and p.kegiatan_id=k.id and s.kode='$unit_id' and k.kode='$kegiatan_code' and dk.id_budgeting=$detail_no
                                          and k.skpd_id=s.id and dp.detail_kegiatan_id=dk.id
                                        group by s.kode,k.kode,dk.kode_detail_kegiatan,dk.id_budgeting";
                    //print_r($query);exit;
                    //diambil nilai terpakai
                    $con = Propel::getConnection(RincianDetailPeer::DATABASE_NAME);
                    $statement = $con->prepareStatement($query);
                    //$rs_eprojects=$statement->executeQuery();
                    //$this->rs_eproject = $rs_eproject;
                    /* while($rs_eprojects->next())
                      {
                      $nilaiTerpakai = $rs_eprojects->getString('jml');
                      $jumlahRows = $rs_eprojects->getRow();
                      } */

                    if ($nilaiBaru < $nilaiTerpakai) {

                        $this->setFlash('gagal', 'Mohon maaf , untuk Rincian Kegiatan ini sudah terpakai di pekerjaan, sejumlah ' . number_format($nilaiTerpakai, 0, ',', '.'));
                        return $this->redirect('kegiatan/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
                    }

                    budgetLogger::log('Menghapus Sub Kegiatan dari unit id :' . $unit_id . ' dengan kode :' . $kegiatan_code . ' dan kode sub :' . $this->getRequestParameter('no') . ' dan detail no (' . $detail_no . '); komponen_id:' . $rincian_detail->getKomponenId() . '; komponen_name:' . $rincian_detail->getKomponenName());
                    $rincian_detail->save();
                    $this->setFlash('berhasil', 'Perubahan Telah Berhasil Dilakukan');
                    return $this->redirect('kegiatan/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
                } else {
                    sfContext::getInstance()->getLogger()->debug{'{eProject} rincian detail tidak ketemu, nilai baru = ' . $nilaiBaru};
                    $this->setFlash('gagal', 'Mohon maaf, rincian yang dicari tidak ditemukan dalam database');
                    return $this->redirect('kegiatan/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
                }
            } elseif ($jumlah > 1) {
                $rincian_detail = RincianDetailPeer::doSelect($c);

                $gagal = 'salah';
                foreach ($rincian_detail as $rincian_details) {
                    $detail_no = $rincian_details->getDetailNo();
                    $query = "update " . sfConfig::get('app_default_schema') . ".rincian_detail set status_hapus=true,   tahap_edit='" . sfConfig::get('app_tahap_edit') . "' where
						unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and detail_no=$detail_no";
                    //echo $query;
                    $con = Propel::getConnection(RincianSubParameterPeer::DATABASE_NAME);
                    $statement = $con->prepareStatement($query);
                    budgetLogger::log('Menghapus Sub Kegiatan dari unit id :' . $unit_id . ' dengan kode :' . $kegiatan_code . ' dan kode sub :' . $this->getRequestParameter('no') . ' dan detail no (' . $detail_no . '); komponen_id:' . $rincian_details->getKomponenId() . '; komponen_name:' . $rincian_details->getKomponenName());
                    $statement->executeQuery();

                    //cek paketan
                    $d = new Criteria();
                    $d->add(RincianDetailPeer::UNIT_ID, $unit_id);
                    $d->add(RincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
                    $d->add(RincianDetailPeer::DETAIL_NO, $detail_no);
                    $cari_rincian_detail = RincianDetailPeer::doSelectOne($d);
                    if ($cari_rincian_detail) {

                        $host = 'pengendalian.surabaya2excellence.or.id/eproject2009';
                        $port = 8080;
                        $body = '';
                        $headers = '';
                        $tahun = sfConfig::get('app_sinkronisasi_tahun', date('Y'));


                        $tahun = substr($tahun, 2, 2); //ambil 2 digit terakhir;
                        $kode_detail_kegiatan = sprintf("%s.%s.%s.%s", $unit_id, $kegiatan_code, '09', $detail_no);
                        $url = 'http://pengendalian.surabaya2excellence.or.id/eproject2010/sinkronisasi/ubahDetailKegiatan.shtml?kode_detail_kegiatan=' . $kode_detail_kegiatan;
                        sfContext::getInstance()->getLogger()->debug('{eProject}akan mengambil data dari eproject ' . $kode_detail_kegiatan);
                        $cek_eproject = HttpHelper::httpGet($host, $port, $url, $body, $headers); //hasilnya bisa true bisa false
                        //hasil xml dari eproject masuk ke variabel body
                        $nilaiTerpakai = 0;
                        $httpOK = false;
                        if ($cek_eproject && ( strpos($body, '<?xml') !== false ) && ( strpos($body, 'nilai="') !== false )) {
                            $doc = new DomDocument("1.0");
                            $doc->loadXml($body);
                            $detail_kegiatan = $doc->documentElement; //root xml
                            $nilaiTerpakai = $detail_kegiatan->getAttribute('nilai');
                            $httpOK = true;
                        }
                        sfContext::getInstance()->getLogger()->debug('{eProject} rincian terpakai ketemu, nilai terpakai = ' . $nilaiTerpakai);
                        if ($nilaiBaru < $nilaiTerpakai) {
                            $gagal = 'benar';
                        }
                    }
                }

                if ($gagal == 'benar') {
                    $this->setFlash('gagal', 'Mohon maaf , untuk Rincian ASB Non Fisik Kegiatan ini sudah terpakai di pekerjaan');
                    return $this->redirect('kegiatan/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
                } elseif ($gagal == 'salah') {
//echo $keterangan_koefisien;
//print_r($rincian_detail);
                    /*
                      $rincian_detail->setKeteranganKoefisien($keterangan_koefisien);
                      $rincian_detail->setVolume($volume);
                      $rincian_detail->setDetailName($detail_name);
                      $rincian_detail->setSubtitle($subtitle);
                      $rincian_detail->setSub($sub);
                      $rincian_detail->setKodeSub($kode_sub);
                      $rincian_detail->setKecamatan($kode_jasmas);
                      $rincian_detail->save();
                     */
                    /*
                      $query="update ". sfConfig::get('app_default_schema') .".rincian_detail set volume=0, keterangan_koefisien='0 ' || satuan where
                      unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and detail_no=$detail_no";
                      echo $query;
                      $con=Propel::getConnection(RincianSubParameterPeer::DATABASE_NAME);
                      $statement=$con->prepareStatement($query);
                      $statement->executeQuery();
                     */
                    //$this->setFlash('berhasil', 'Perubahan Telah Berhasil Dilakukan');
                    //return $this->redirect('kegiatan/edit?unit_id='.$unit_id.'&kode_kegiatan='.$kegiatan_code);
                }
            } else {
                sfContext::getInstance()->getLogger()->debug{'{eProject} rincian detail tidak ketemu, nilai baru = ' . $nilaiBaru};
                $this->setFlash('gagal', 'Mohon maaf, rincian yang dicari tidak ditemukan dalam database');
                return $this->redirect('kegiatan/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
            }

            $sub_id = $this->getRequestParameter('id');
            $c = new Criteria();
            $c->add(SubtitleIndikatorPeer::SUB_ID, $sub_id);
            $rs_subtitle = SubtitleIndikatorPeer::doSelectOne($c);
            if ($rs_subtitle) {
                $unit_id = $rs_subtitle->getUnitId();
                $kegiatan_code = $rs_subtitle->getKegiatanCode();
                $subtitle = $rs_subtitle->getSubtitle();
                $nama_subtitle = trim($subtitle);
            }
            /*
              $query = "select *
              from ". sfConfig::get('app_default_schema') .".rincian_detail
              where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and subtitle ilike '$nama_subtitle' and volume>0 order by sub,rekening_code,komponen_name";
             */

            $query = "select *
					  from " . sfConfig::get('app_default_schema') . ".rincian_detail
					  where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and subtitle ilike '$nama_subtitle' and status_hapus=false order by sub,rekening_code,komponen_name";
            $con = Propel::getConnection(RincianDetailPeer::DATABASE_NAME);
            $statement = $con->prepareStatement($query);
            $rs_rinciandetail = $statement->executeQuery();
            $this->rs_rinciandetail = $rs_rinciandetail;
            $this->id = $sub_id;
            $this->rinciandetail = 'ada';
            $this->setLayout('kosong');
        }
    }

    public function executeHapusPekerjaans() {
        if ($this->getRequestParameter('id')) {
            $detail_no = $this->getRequestParameter('no');
            $unit_id = $this->getRequestParameter('unit');
            $kegiatan_code = $this->getRequestParameter('kegiatan');
            $tahap = '';
            $c = new Criteria();
            $c->add(MasterKegiatanPeer::UNIT_ID, $unit_id);
            $c->add(MasterKegiatanPeer::KODE_KEGIATAN, $kegiatan_code);
            $rs_kegiatan = MasterKegiatanPeer::doSelectOne($c);
            if ($rs_kegiatan) {
                $tahap = $rs_kegiatan->getTahap();
            }
            //$volume=0;
            //$keterangan_koefisien='0';
            //$sub='';
            //$kode_sub='';
            $kode_jasmas = '';

            $sekarang = date('Y-m-d H:i:s');

            $ada_waitinglist = 0;
            $rd_cari_waitinglist = new Criteria();
            $rd_cari_waitinglist->add(WaitingListPUPeer::KODE_RKA, $unit_id . '.' . $kegiatan_code . '.' . $detail_no, Criteria::EQUAL);
            $rd_cari_waitinglist->addAnd(WaitingListPUPeer::STATUS_WAITING, 1);
            $rd_cari_waitinglist->addAnd(WaitingListPUPeer::STATUS_HAPUS, FALSE);
            $ada_waitinglist = WaitingListPUPeer::doCount($rd_cari_waitinglist);
            if ($ada_waitinglist > 0) {
                $this->setFlash('gagal', 'Mohon maaf, rincian RKA tidak dapat dihapus karena komponen waiting list. Silahkan mengenolkan saja.');
                return $this->redirect('kegiatan/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
            }

            $c = new Criteria();
            $c->add(RincianDetailPeer::UNIT_ID, $unit_id);
            $c->add(RincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
            $c->add(RincianDetailPeer::DETAIL_NO, $detail_no);
            $rincian_detail = RincianDetailPeer::doSelectOne($c);
            if ($rincian_detail) {

                $totNilaiSwakelola = 0;
                $totNilaiKontrak = 0;
                $totNilaiAlokasi = 0;

                $ceklelangselesaitidakaturanpembayaran = 0;
                $lelang = 0;
                $totNilaiHps = 0;

                if (sfConfig::get('app_fasilitas_cekeProject') == 'buka') {
                    $totNilaiAlokasi = $rincian_detail->getCekNilaiAlokasiProject($unit_id, $kegiatan_code, $detail_no);
                    if (sfConfig::get('app_fasilitas_cekServer') == 'buka') {
                        $lelang = $rincian_detail->getCekLelang($unit_id, $kegiatan_code, $detail_no, 0);
                        if (sfConfig::get('app_fasilitas_cekeDelivery') == 'buka') {
                            $totNilaiSwakelola = $rincian_detail->getCekNilaiSwakelolaDelivery2($unit_id, $kegiatan_code, $detail_no);
                            $totNilaiKontrak = $rincian_detail->getCekNilaiKontrakDelivery2($unit_id, $kegiatan_code, $detail_no);

                            $totNilaiHps = $rincian_detail->getCekNilaiHPSKomponen($unit_id, $kegiatan_code, $detail_no);
                            $ceklelangselesaitidakaturanpembayaran = $rincian_detail->getCekLelangTidakAdaAturanPembayaran($unit_id, $kegiatan_code, $detail_no);
                        }
                    }
                }

                //irul 27 februari 2014 - ngecek hapus komponen
                if ($totNilaiAlokasi == 0 && $totNilaiKontrak == 0 && $totNilaiSwakelola == 0 && $lelang == 0 && $ceklelangselesaitidakaturanpembayaran == 0) {

                    $rincian_detail->setStatusHapus('TRUE');
                    $rincian_detail->setLastEditTime($sekarang);
                    $rincian_detail->setTahap($tahap);

                    $this->setFlash('berhasil', 'Komponen ' . $rincian_detail->getKomponenName() . ' berhasil dihapus ');
                    budgetLogger::log('Menghapus komponen dari unit id :' . $unit_id . ' dengan kode :' . $kegiatan_code . '; detail_no :' . $detail_no . '; komponen_id:' . $rincian_detail->getKomponenId() . '; komponen_name:' . $rincian_detail->getKomponenName());

                    //irul 10 september 2014 - status hapus data gmap diset true
                    $con = Propel::getConnection();
                    $query = "update " . sfConfig::get('app_default_gis') . ".geojsonlokasi_rev1 "
                            . "set status_hapus = true, last_edit_time = '$sekarang' "
                            . "where unit_id='" . $rincian_detail->getUnitId() . "' and kegiatan_code='" . $rincian_detail->getKegiatanCode() . "' and detail_no ='" . $rincian_detail->getDetailNo() . "' and tahun = '" . sfConfig::get('app_tahun_default') . "'";
                    $stmt = $con->prepareStatement($query);
                    $stmt->executeQuery();
                    //irul 10 september 2014 - status hapus data gmap diset true

                    $rincian_detail->save();

                    $kode_rka_fix = $unit_id . '.' . $kegiatan_code . '.' . $detail_no;

                    $c_update_history = new Criteria();
                    $c_update_history->add(HistoryPekerjaanV2Peer::KODE_RKA, $kode_rka_fix);
                    $dapat_history = HistoryPekerjaanV2Peer::doSelectOne($c_update_history);
                    if ($dapat_history) {
                        $dapat_history->setStatusHapus(TRUE);
                        $dapat_history->save();
                    }
                } else {
                    if ($totNilaiKontrak > 0 || $totNilaiSwakelola > 0) {
                        if ($totNilaiKontrak == 0) {
                            $this->setFlash('gagal', 'Mohon maaf, ' . $rincian_detail->getKomponenName() . ' ini sudah terpakai di eDelivery sebesar Rp ' . number_format($totNilaiSwakelola, 0, ',', '.'));
                        } else if ($totNilaiSwakelola == 0) {
                            $this->setFlash('gagal', 'Mohon maaf, ' . $rincian_detail->getKomponenName() . ' ini sudah terpakai di eDelivery sebesar Rp ' . number_format($totNilaiKontrak, 0, ',', '.'));
                        } else {
                            $this->setFlash('gagal', 'Mohon maaf, ' . $rincian_detail->getKomponenName() . ' ini sudah terpakai di eDelivery sebesar Rp ' . number_format($totNilaiSwakelola, 0, ',', '.'));
                        }
                    } else if ($lelang > 0) {
                        $this->setFlash('gagal', 'Mohon maaf, Komponen sedang dalam proses lelang');
                    } else if ($ceklelangselesaitidakaturanpembayaran == 1) {
                        $this->setFlash('gagal', 'Proses lelang selesai + Belum ada Aturan Pembayaran. Silahkan mengisi Aturan Pembayaran terlebih dahulu.');
                    }
                }
                //irul 27 februari 2014 - ngecek hapus komponen
            }
        } else {
            //sfContext::getInstance()->getLogger()->debug{'{eProject} rincian detail tidak ketemu, nilai baru = '.$nilaiBaru};
            $this->setFlash('gagal', 'Mohon maaf, rincian yang dicari tidak ditemukan dalam database');
        }
        return $this->redirect('kegiatan/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
    }

    public function executeHapusPekerjaansRevisi() {
        if ($this->getRequestParameter('id')) {
            $detail_no = $this->getRequestParameter('no');
            $unit_id = $this->getRequestParameter('unit');
            $kegiatan_code = $this->getRequestParameter('kegiatan');
            $tahap = '';
            $c = new Criteria();
            $c->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
            $c->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kegiatan_code);
            $rs_kegiatan = DinasMasterKegiatanPeer::doSelectOne($c);
            if ($rs_kegiatan) {
                $tahap = $rs_kegiatan->getTahap();
            }
            //$volume=0;
            //$keterangan_koefisien='0';
            //$sub='';
            //$kode_sub='';
            $kode_jasmas = '';

            $sekarang = date('Y-m-d H:i:s');

            $ada_waitinglist = 0;
            $rd_cari_waitinglist = new Criteria();
            $rd_cari_waitinglist->add(WaitingListPUPeer::KODE_RKA, $unit_id . '.' . $kegiatan_code . '.' . $detail_no, Criteria::EQUAL);
            $rd_cari_waitinglist->addAnd(WaitingListPUPeer::STATUS_WAITING, 1);
            $rd_cari_waitinglist->addAnd(WaitingListPUPeer::STATUS_HAPUS, FALSE);
            $ada_waitinglist = WaitingListPUPeer::doCount($rd_cari_waitinglist);
            if ($ada_waitinglist > 0) {
                $this->setFlash('gagal', 'Mohon maaf, rincian RKA tidak dapat dihapus karena komponen waiting list. Silahkan mengenolkan saja.');
                return $this->redirect('kegiatan/editRevisi?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
            }

            $c = new Criteria();
            $c->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
            $c->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
            $c->add(DinasRincianDetailPeer::DETAIL_NO, $detail_no);
            $rincian_detail = DinasRincianDetailPeer::doSelectOne($c);
            if ($rincian_detail) {

                $totNilaiSwakelola = 0;
                $totNilaiKontrak = 0;
                $totNilaiRealisasi = 0;
                $totVolumeRealisasi = 0;
                $totNilaiAlokasi = 0;

                $ceklelangselesaitidakaturanpembayaran = 0;
                $lelang = 0;
                $totNilaiHps = 0;

                if (sfConfig::get('app_fasilitas_cekeProject') == 'buka') {
                    $totNilaiAlokasi = $rincian_detail->getCekNilaiAlokasiProject($unit_id, $kegiatan_code, $detail_no);
                    if (sfConfig::get('app_fasilitas_cekServer') == 'buka') {
                        $lelang = $rincian_detail->getCekLelang($unit_id, $kegiatan_code, $detail_no, 0);
                        if (sfConfig::get('app_fasilitas_cekeDelivery') == 'buka') {
                            $totNilaiSwakelola = $rincian_detail->getCekNilaiSwakelolaDelivery2($unit_id, $kegiatan_code, $detail_no);
                            $totNilaiKontrak = $rincian_detail->getCekNilaiKontrakDelivery2($unit_id, $kegiatan_code, $detail_no);
                            $totNilaiRealisasi = $rincian_detail->getCekRealisasi($unit_id, $kegiatan_code, $detail_no);
                            $totVolumeRealisasi = $rincian_detail->getCekVolumeRealisasi($unit_id, $kegiatan_code, $detail_no);

                            $totNilaiHps = $rincian_detail->getCekNilaiHPSKomponen($unit_id, $kegiatan_code, $detail_no);
                            $ceklelangselesaitidakaturanpembayaran = $rincian_detail->getCekLelangTidakAdaAturanPembayaran($unit_id, $kegiatan_code, $detail_no);
                        }
                    }
                }

                //irul 27 februari 2014 - ngecek hapus komponen
                if ($totNilaiAlokasi == 0 && $totNilaiKontrak == 0 && $totNilaiSwakelola == 0 && $totNilaiRealisasi == 0 && $totVolumeRealisasi == 0 && $lelang == 0 && $ceklelangselesaitidakaturanpembayaran == 0) {

                    $rincian_detail->setStatusHapus('TRUE');
                    $rincian_detail->setLastEditTime($sekarang);
                    $rincian_detail->setTahap($tahap);

                    $this->setFlash('berhasil', 'Komponen ' . $rincian_detail->getKomponenName() . ' berhasil dihapus ');
                    budgetLogger::log('Menghapus komponen dari unit id :' . $unit_id . ' dengan kode :' . $kegiatan_code . '; detail_no :' . $detail_no . '; komponen_id:' . $rincian_detail->getKomponenId() . '; komponen_name:' . $rincian_detail->getKomponenName());

                    //irul 10 september 2014 - status hapus data gmap diset true
                    $con = Propel::getConnection();
                    $query = "update " . sfConfig::get('app_default_gis') . ".geojsonlokasi_rev1 "
                            . "set status_hapus = true, last_edit_time = '$sekarang' "
                            . "where unit_id='" . $rincian_detail->getUnitId() . "' and kegiatan_code='" . $rincian_detail->getKegiatanCode() . "' and detail_no ='" . $rincian_detail->getDetailNo() . "' and tahun = '" . sfConfig::get('app_tahun_default') . "'";
                    $stmt = $con->prepareStatement($query);
                    $stmt->executeQuery();
                    //irul 10 september 2014 - status hapus data gmap diset true

                    $rincian_detail->save();

                    $kode_rka_fix = $unit_id . '.' . $kegiatan_code . '.' . $detail_no;

                    $c_update_history = new Criteria();
                    $c_update_history->add(HistoryPekerjaanV2Peer::KODE_RKA, $kode_rka_fix);
                    $dapat_history = HistoryPekerjaanV2Peer::doSelectOne($c_update_history);
                    if ($dapat_history) {
                        $dapat_history->setStatusHapus(TRUE);
                        $dapat_history->save();
                    }
                } else {
                    if ($totNilaiKontrak > 0 || $totNilaiSwakelola > 0) {
                        if ($totNilaiKontrak == 0) {
                            $this->setFlash('gagal', 'Mohon maaf, ' . $rincian_detail->getKomponenName() . ' ini sudah terpakai di eDelivery sebesar Rp ' . number_format($totNilaiSwakelola, 0, ',', '.'));
                        } else if ($totNilaiSwakelola == 0) {
                            $this->setFlash('gagal', 'Mohon maaf, ' . $rincian_detail->getKomponenName() . ' ini sudah terpakai di eDelivery sebesar Rp ' . number_format($totNilaiKontrak, 0, ',', '.'));
                        } else {
                            $this->setFlash('gagal', 'Mohon maaf, ' . $rincian_detail->getKomponenName() . ' ini sudah terpakai di eDelivery sebesar Rp ' . number_format($totNilaiSwakelola, 0, ',', '.'));
                        }
                    } else if ($lelang > 0) {
                        $this->setFlash('gagal', 'Mohon maaf, Komponen sedang dalam proses lelang');
                    } else if ($ceklelangselesaitidakaturanpembayaran == 1) {
                        $this->setFlash('gagal', 'Proses lelang selesai + Belum ada Aturan Pembayaran. Silahkan mengisi Aturan Pembayaran terlebih dahulu.');
                    } else if ($totNilaiRealisasi > 0) {
                        $this->setFlash('gagal', 'Mohon maaf, ' . $rincian_detail->getKomponenName() . ' ini sudah terpakai di eDelivery sebesar Rp ' . number_format($totNilaiRealisasi, 0, ',', '.'));
                    } else if ($totVolumeRealisasi > 0) {
                        $this->setFlash('gagal', 'Mohon maaf, ' . $rincian_detail->getKomponenName() . ' ini sudah terpakai di eDelivery dengan volume ' . number_format($totVolumeRealisasi, 0, ',', '.'));
                    }
                }
                //irul 27 februari 2014 - ngecek hapus komponen
            }
        } else {
            //sfContext::getInstance()->getLogger()->debug{'{eProject} rincian detail tidak ketemu, nilai baru = '.$nilaiBaru};
            $this->setFlash('gagal', 'Mohon maaf, rincian yang dicari tidak ditemukan dalam database');
        }
        return $this->redirect('kegiatan/editRevisi?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
    }

    public function executeBaruKegiatan() {
        if ($this->getRequestParameter('cari') == 'cari') {
            //print_r($this->getRequestParameter('lokasi'));exit;
            $user = $this->getUser();
            $user->removeCredential('lokasi');
            $user->addCredential('lokasi');
            $user->setAttribute('nama', $this->getRequestParameter('lokasi'), 'lokasi');
            $user->setAttribute('id', $this->getRequestParameter('id'), 'lokasi');
            $user->setAttribute('kegiatan', $this->getRequestParameter('kegiatan'), 'lokasi');
            $user->setAttribute('unit', $this->getRequestParameter('unit'), 'lokasi');
            $user->setAttribute('subtitle', $this->getRequestParameter('subtitle'), 'lokasi');
            $user->setAttribute('sub', $this->getRequestParameter('sub'), 'lokasi');
            $user->setAttribute('pajak', $this->getRequestParameter('pajak'), 'lokasi');
            $user->setAttribute('rekening', $this->getRequestParameter('rekening'), 'lokasi');
            $user->setAttribute('tipe', $this->getRequestParameter('tipe'), 'lokasi');
            $user->setAttribute('baru', 'baru', 'lokasi');

            return $this->forward('lokasi', 'list');
        }
        if ($this->getRequestParameter('simpan') == 'simpan') {
            $unit_id = $this->getRequestParameter('unit');
            $kegiatan_code = $this->getRequestParameter('kegiatan');
            $subtitle = $this->getRequestParameter('subtitle');
            $sub = $this->getRequestParameter('sub');
            $tipe = $this->getRequestParameter('tipe');
            $rekening = $this->getRequestParameter('rekening');
            $pajak = $this->getRequestParameter('pajak');
            $komponen_id = $this->getRequestParameter('id');
            $akrual_code = $this->getRequestParameter('akrual_code');

            $c = new Criteria();
            $c->add(KomponenPeer::KOMPONEN_ID, $komponen_id);
            if ($rs_est_fisik = KomponenPeer::doSelectOne($c)) {
                $est_fisik = $rs_est_fisik->getIsEstFisik();
                $tipe2 = $rs_est_fisik->getKomponenTipe2();
            }

            $lokasi_baru = '';
            $lokasi_array = array();

            if (!$this->getRequestParameter('subtitle')) {
                $this->setFlash('gagal', 'Subtitle Belum Dipilih');
                return $this->redirect("kegiatan/buatbaru?kegiatan=$kegiatan_code&rekening=$rekening&pajak=$pajak&unit=$unit_id&tipe=$tipe&komponen=$komponen_id&baru=" . md5('terbaru') . "&commit=Pilih");
            }
#51 - GMAP simpan                   
//            if ($tipe2 == 'KONSTRUKSI' || $tipe == 'FISIK' && !$this->getRequestParameter('lokasi')) {
//                $this->setFlash('gagal', 'Lokasi Belum Dipilih');
//                return $this->redirect("kegiatan/buatbaru?kegiatan=$kegiatan_code&rekening=$rekening&pajak=$pajak&unit=$unit_id&tipe=$tipe&komponen=$komponen_id&baru=" . md5('terbaru') . "&commit=Pilih");
//            }
#51 - GMAP simpan                   
#51 - GMAP simpan
            if (($tipe2 == 'KONSTRUKSI' || $tipe == 'FISIK' || $est_fisik) && !$this->getRequestParameter('lokasi_jalan')) {
                $this->setFlash('gagal', 'Lokasi Belum Dipilih');
                return $this->redirect("kegiatan/buatbaru?kegiatan=$kegiatan_code&rekening=$rekening&pajak=$pajak&unit=$unit_id&tipe=$tipe&komponen=$komponen_id&baru=" . md5('terbaru') . "&commit=Pilih");
            }
            if ($tipe2 == 'KONSTRUKSI' || $tipe == 'FISIK' || $est_fisik) {
                $lokasi_jalan = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_jalan')));
                $tipe_gang = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('tipe_gang')));
                $lokasi_gang = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_gang')));
                $lokasi_nomor = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_nomor')));
                $lokasi_rw = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_rw')));
                $lokasi_rt = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_rt')));
                $lokasi_keterangan = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_keterangan')));
                $lokasi_tempat = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_tempat')));

                $total_array_lokasi = count($lokasi_jalan);

                $keisi = 0;
                for ($i = 0; $i < $total_array_lokasi; $i++) {
                    $jalan_fix = '';
                    $gang_fix = '';
                    $nomor_fix = '';
                    $rw_fix = '';
                    $rt_fix = '';
                    $keterangan_fix = '';
                    $tempat_fix = '';
                    $tipe_gang_fix = '';
                    if (trim($lokasi_jalan[$i]) <> '' || trim($lokasi_tempat[$i]) <> '') {

                        if (trim($lokasi_jalan[$i]) <> '') {
                            $jalan_fix = 'JL. ' . strtoupper(trim($lokasi_jalan[$i])) . ' ';
                        }

                        if (trim($lokasi_tempat[$i]) <> '') {
                            $tempat_fix = '(' . strtoupper(trim($lokasi_tempat[$i])) . ') ';
                        }

                        if (trim($tipe_gang[$i]) <> '') {
                            $tipe_gang_fix = strtoupper(trim($tipe_gang[$i])) . '. ';
                        } else {
                            $tipe_gang_fix = 'GG. ';
                        }

                        if (trim($lokasi_gang[$i]) <> '') {
                            $gang_fix = $tipe_gang_fix . '' . strtoupper(trim($lokasi_gang[$i])) . ' ';
                        }

                        if (trim($lokasi_nomor[$i]) <> '') {
                            $nomor_fix = 'NO ' . strtoupper(trim($lokasi_nomor[$i])) . ' ';
                        }

                        if (trim($lokasi_rw[$i]) <> '') {
                            $rw_fix = 'RW ' . strtoupper(trim($lokasi_rw[$i])) . ' ';
                        }

                        if (trim($lokasi_rt[$i]) <> '') {
                            $rt_fix = 'RT ' . strtoupper(trim($lokasi_rt[$i])) . ' ';
                        }

                        if (trim($lokasi_keterangan[$i]) <> '') {
                            $keterangan_fix = '' . strtoupper(trim($lokasi_keterangan[$i])) . ' ';
                        }

                        if ($keisi == 0) {
                            $lokasi_baru = $tempat_fix . '' . $jalan_fix . '' . $gang_fix . '' . $nomor_fix . '' . $rw_fix . '' . $rt_fix . '' . $keterangan_fix;
                            $keisi++;
                        } else {
                            $lokasi_baru = $lokasi_baru . ', ' . $tempat_fix . '' . $jalan_fix . '' . $gang_fix . '' . $nomor_fix . '' . $rw_fix . '' . $rt_fix . '' . $keterangan_fix;
                            $keisi++;
                        }
                    }
                }
                if ($keisi == 0) {
                    $this->setFlash('gagal', 'Lokasi Belum Dipilih');
                    return $this->redirect("kegiatan/buatbaru?kegiatan=$kegiatan_code&rekening=$rekening&pajak=$pajak&unit=$unit_id&tipe=$tipe&komponen=$komponen_id&baru=" . md5('terbaru') . "&commit=Pilih");
                }
            }
#51 - GMAP simpan             
            if ($akrual_code) {
                $akrual_code_baru = $akrual_code . '|01';
                $no_akrual_code = DinasRincianDetailPeer::AmbilUrutanAkrual($akrual_code_baru);
                $akrual_code_baru = $akrual_code_baru . $no_akrual_code;
            }
            $c = new Criteria();
            $c->add(KomponenPeer::KOMPONEN_ID, $komponen_id);
            $rs_komponen = KomponenPeer::doSelectOne($c);
            if ($rs_komponen) {
                $komponen_harga = $rs_komponen->getKomponenHarga();
                $komponen_name = $rs_komponen->getKomponenName();
                $satuan = $rs_komponen->getSatuan();
            }
            $detail_no = 0;
            $query = "select max(detail_no) as nilai from " . sfConfig::get('app_default_schema') . ".rincian_detail where unit_id='$unit_id' and kegiatan_code='$kegiatan_code'";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs_max = $stmt->executeQuery();
            while ($rs_max->next()) {
                $detail_no = $rs_max->getString('nilai');
            }
            $detail_no+=1;
            $detail_name = '';
            $ada_fisik = false;
            if ($tipe == 'FISIK' || $est_fisik) {
#51 - GMAP simpan                                                
//                $detail_name = $this->getRequestParameter('lokasi');
                $detail_name = $lokasi_baru;
                $ada_fisik = true;
#51 - GMAP simpan                                                                
            } else if ($tipe != 'FISIK' && !$est_fisik) {
                $detail_name = $this->getRequestParameter('keterangan');
            }

            $kode_sub = '';
            $sub = '';
            $kode_jasmas = '';
//            if ($this->getRequestParameter('lokasi')) {
//                $kode_lokasi = '';
//                $detail_name = $this->getRequestParameter('lokasi');
//                $kode_jasmas = $this->getRequestParameter('jasmas');
//                $c = new Criteria();
//                $c->add(VLokasiPeer::NAMA, $detail_name);
//                $rs_lokasi = VLokasiPeer::doSelectOne($c);
//                if ($rs_lokasi) {
//                    $kode_lokasi = $rs_lokasi->getKode();
//                }
//
//                if ($kode_lokasi == '') {
//                    $this->setFlash('gagal', 'Lokasi Belum Dipilih');
//                    return $this->redirect("kegiatan/buatbaru?kegiatan=$kegiatan_code&rekening=$rekening&pajak=$pajak&unit=$unit_id&tipe=$tipe&komponen=$komponen_id&baru=" . md5('terbaru') . "&commit=Pilih");
//                }
//            }

            if ($this->getRequestParameter('keterangan')) {
                $detail_name = $this->getRequestParameter('keterangan');
            }
            if ($this->getRequestParameter('sub')) {
                $kode_sub = $this->getRequestParameter('sub');
                $d = new Criteria();
                $d->add(RincianSubParameterPeer::KODE_SUB, $kode_sub);
                $rs_rinciansubparameter = RincianSubParameterPeer::doSelectOne($d);
                if ($rs_rinciansubparameter) {
                    $sub = $rs_rinciansubparameter->getNewSubtitle();
                    $kodesub = $rs_rinciansubparameter->getKodeSub();
                }
            }
            $kode_subtitle = $this->getRequestParameter('subtitle');

            $c = new Criteria();
            $c->add(SubtitleIndikatorPeer::SUB_ID, $kode_subtitle);
            $rs_subtitle = SubtitleIndikatorPeer::doSelectOne($c);
            if ($rs_subtitle) {
                $subtitle = $rs_subtitle->getSubtitle();
            }

            $volume = 0;
            $keterangan_koefisien = '';

            if ($this->getRequestParameter('vol1') || $this->getRequestParameter('vol2') || $this->getRequestParameter('vol3') || $this->getRequestParameter('vol4')) {
                if ($this->getRequestParameter('vol2') == '') {
                    $vol2 = 1;
                    $volume = $this->getRequestParameter('vol1') * $vol2;
                    $keterangan_koefisien = $this->getRequestParameter('vol1') . ' ' . $this->getRequestParameter('volume1');
                } else if (!$this->getRequestParameter('vol2') == '') {
                    $volume = $this->getRequestParameter('vol1') * $this->getRequestParameter('vol2');
                    $keterangan_koefisien = $this->getRequestParameter('vol1') . ' ' . $this->getRequestParameter('volume1') . ' X ' . $this->getRequestParameter('vol2') . ' ' . $this->getRequestParameter('volume2');
                }
                if ($this->getRequestParameter('vol3') == '') {
                    $vol3 = 1;
                    $volume = $volume * $vol3;
                } else if (!$this->getRequestParameter('vol3') == '') {
                    $volume = $this->getRequestParameter('vol1') * $this->getRequestParameter('vol2') * $this->getRequestParameter('vol3');
                    $keterangan_koefisien = $this->getRequestParameter('vol1') . ' ' . $this->getRequestParameter('volume1') . ' X ' . $this->getRequestParameter('vol2') . ' ' . $this->getRequestParameter('volume2') . ' X ' . $this->getRequestParameter('vol3') . ' ' . $this->getRequestParameter('volume3');
                }
                if ($this->getRequestParameter('vol4') == '') {
                    $vol4 = 1;
                    $volume = $volume * $vol4;
                } else if (!$this->getRequestParameter('vol4') == '') {
                    $volume = $this->getRequestParameter('vol1') * $this->getRequestParameter('vol2') * $this->getRequestParameter('vol3') * $this->getRequestParameter('vol4');
                    $keterangan_koefisien = $this->getRequestParameter('vol1') . ' ' . $this->getRequestParameter('volume1') . ' X ' . $this->getRequestParameter('vol2') . ' ' . $this->getRequestParameter('volume2') . ' X ' . $this->getRequestParameter('vol3') . ' ' . $this->getRequestParameter('volume3') . ' X ' . $this->getRequestParameter('vol4') . ' ' . $this->getRequestParameter('volume4');
                }
            }

            if ($this->getRequestParameter('status') == 'pending') {
                $detail_no = 0;
                $query = "select max(detail_no) as nilai from " . sfConfig::get('app_default_schema') . ".rincian_detail_masalah where unit_id='$unit_id' and kegiatan_code='$kegiatan_code'";
                $con = Propel::getConnection();
                $stmt = $con->prepareStatement($query);
                $rs_max = $stmt->executeQuery();
                while ($rs_max->next()) {
                    $detail_no = $rs_max->getString('nilai');
                }
                $detail_no+=1;
                $query = "insert into " . sfConfig::get('app_default_schema') . ".rincian_detail_masalah (kegiatan_code, tipe, detail_no, rekening_code, komponen_id, detail_name, volume, keterangan_koefisien, subtitle, komponen_harga, komponen_harga_awal, komponen_name, satuan, pajak, unit_id, kode_sub, sub, kecamatan, last_update_user, last_update_time,tahun) "
                        . "values ('" . $kegiatan_code . "', '" . $tipe . "', " . $detail_no . ", '" . $rekening . "', '" . $komponen_id . "', '" . $detail_name . "', " . $volume . ", '" . $keterangan_koefisien . "', '" . $subtitle . "', " . $komponen_harga . ", " . $komponen_harga . ",'" . $komponen_name . "', '" . $satuan . "', " . $pajak . ",'" . $unit_id . "','" . $kodesub . "', '" . $sub . "', '" . $kode_jasmas . "', '" . $dinas . "', now(),'" . sfConfig::get('app_tahun_default') . "')";

                $con = Propel::getConnection();
                $stmt = $con->prepareStatement($query);
                $stmt->executeQuery();
                $this->setFlash('berhasil', 'Penyimpanan Telah Berhasil, Tetapi Tidak Masuk Ke RKA, Harap Hubungi Penyelia Anda');
                return $this->redirect('kegiatan/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
            } else {
                $query = "insert into " . sfConfig::get('app_default_schema') . ".rincian_detail "
                        . "(kegiatan_code, tipe, detail_no, rekening_code, komponen_id, detail_name, volume, keterangan_koefisien, subtitle, komponen_harga, komponen_harga_awal, komponen_name, satuan, pajak, unit_id, kode_sub, sub, kecamatan, last_update_user, last_update_time,tahun,akrual_code,tipe2) "
                        . "values ('" . $kegiatan_code . "', '" . $tipe . "', " . $detail_no . ", '" . $rekening . "', '" . $komponen_id . "', '" . $detail_name . "', " . $volume . ", '" . $keterangan_koefisien . "', '" . $subtitle . "', " . $komponen_harga . ", " . $komponen_harga . ",'" . $komponen_name . "', '" . $satuan . "', " . $pajak . ",'" . $unit_id . "','" . $kodesub . "', '" . $sub . "', '" . $kode_jasmas . "', '" . $dinas . "', now(),'" . sfConfig::get('app_tahun_default') . "', '$akrual_code', '$tipe2')";

                $con = Propel::getConnection();
                $stmt = $con->prepareStatement($query);
                $stmt->executeQuery();

                if ($tipe2 == 'KONSTRUKSI' || $tipe == 'FISIK' || $est_fisik) {
                    $rd_cari = new Criteria();
                    $rd_cari->add(RincianDetailPeer::UNIT_ID, $unit_id, Criteria::EQUAL);
                    $rd_cari->add(RincianDetailPeer::KEGIATAN_CODE, $kegiatan_code, Criteria::EQUAL);
                    $rd_cari->add(RincianDetailPeer::DETAIL_NO, $detail_no, Criteria::EQUAL);
                    $rd_dapat = RincianDetailPeer::doSelectOne($rd_cari);

                    $lokasi_jalan = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_jalan')));
                    $lokasi_gang = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_gang')));
                    $tipe_gang = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('tipe_gang')));
                    $lokasi_nomor = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_nomor')));
                    $lokasi_rw = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_rw')));
                    $lokasi_rt = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_rt')));
                    $lokasi_keterangan = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_keterangan')));
                    $lokasi_tempat = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_tempat')));

                    $total_array_lokasi = count($lokasi_jalan);

                    for ($i = 0; $i < $total_array_lokasi; $i++) {
                        $jalan_fix = '';
                        $gang_fix = '';
                        $tipe_gang_fix = '';
                        $nomor_fix = '';
                        $rw_fix = '';
                        $rt_fix = '';
                        $keterangan_fix = '';
                        $tempat_fix = '';
                        if (trim($lokasi_jalan[$i]) <> '' || trim($lokasi_tempat[$i]) <> '') {

                            if (trim($lokasi_jalan[$i]) <> '') {
                                $jalan_fix = 'JL. ' . strtoupper(trim($lokasi_jalan[$i])) . ' ';
                            }

                            if (trim($lokasi_tempat[$i]) <> '') {
                                $tempat_fix = '(' . strtoupper(trim($lokasi_tempat[$i])) . ') ';
                            }

                            if (trim($tipe_gang[$i]) <> '') {
                                $tipe_gang_fix = strtoupper(trim($tipe_gang[$i])) . '. ';
                            } else {
                                $tipe_gang_fix = 'GG. ';
                            }

                            if (trim($lokasi_gang[$i]) <> '') {
                                $gang_fix = $tipe_gang_fix . '' . strtoupper(trim($lokasi_gang[$i])) . ' ';
                            }

                            if (trim($lokasi_nomor[$i]) <> '') {
                                $nomor_fix = 'NO ' . strtoupper(trim($lokasi_nomor[$i])) . ' ';
                            }

                            if (trim($lokasi_rw[$i]) <> '') {
                                $rw_fix = 'RW ' . strtoupper(trim($lokasi_rw[$i])) . ' ';
                            }

                            if (trim($lokasi_rt[$i]) <> '') {
                                $rt_fix = 'RT ' . strtoupper(trim($lokasi_rt[$i])) . ' ';
                            }

                            if (trim($lokasi_keterangan[$i]) <> '') {
                                $keterangan_fix = '' . strtoupper(trim($lokasi_keterangan[$i])) . ' ';
                            }


                            $lokasi_baru = $tempat_fix . '' . $jalan_fix . '' . $gang_fix . '' . $nomor_fix . '' . $rw_fix . '' . $rt_fix . '' . $keterangan_fix;

                            $rka_lokasi = $unit_id . '.' . $kegiatan_code . '.' . $detail_no;
                            $komponen_lokasi = $rd_dapat->getKomponenName() . ' ' . $rd_dapat->getDetailName();
                            $kecamatan_lokasi = $rd_dapat->getLokasiKecamatan();
                            $kelurahan_lokasi = $rd_dapat->getLokasiKelurahan();
                            $lokasi_per_titik = $lokasi_baru;

                            if ($unit_id <> '9999') {
                                $c_insert_gis = new HistoryPekerjaanV2();
                                $c_insert_gis->setTahun(sfConfig::get('app_tahun_default'));
                                $c_insert_gis->setKodeRka($rka_lokasi);
                                $c_insert_gis->setStatusHapus(FALSE);
                                $c_insert_gis->setJalan(strtoupper(trim($lokasi_jalan[$i])));
                                $c_insert_gis->setGang(strtoupper($gang_fix));
                                $c_insert_gis->setNomor(strtoupper(trim($lokasi_nomor[$i])));
                                $c_insert_gis->setRw(strtoupper(trim($lokasi_rw[$i])));
                                $c_insert_gis->setRt(strtoupper(trim($lokasi_rt[$i])));
                                $c_insert_gis->setKeterangan(strtoupper(trim($lokasi_keterangan[$i])));
                                $c_insert_gis->setTempat(strtoupper(trim($lokasi_tempat[$i])));
                                $c_insert_gis->setKomponen($komponen_lokasi);
                                $c_insert_gis->setKecamatan($kecamatan_lokasi);
                                $c_insert_gis->setKelurahan($kelurahan_lokasi);
                                $c_insert_gis->setLokasi($lokasi_per_titik);
                                $c_insert_gis->save();
                            }
                        }
                    }
                }
                $this->setFlash('berhasil', 'Komponen Telah Berhasil Disimpan');
            }
            return $this->redirect('kegiatan/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
        }
    }

    public function executeBuatbaru() {
        if ($this->getRequestParameter('baru') == md5('terbaru')) {
            $kode_kegiatan = $this->getRequestParameter('kegiatan');
            $unit_id = $this->getRequestParameter('unit');
            $pajak = $this->getRequestParameter('pajak');
            $komponen_id = $this->getRequestParameter('komponen');
            //print_r($komponen_id);exit;
            $tipe = $this->getRequestParameter('tipe');
            $kode_rekening = $this->getRequestParameter('rekening');

            if ($kode_rekening == '0') {
                $this->setFlash('gagal', 'Kode rekening belum dipilih');
                $cari = $this->getRequestParameter('cari');
                $this->redirect("kegiatan/carikomponen?filters[nama_komponen]=$cari&kegiatan=$kode_kegiatan&unit=$unit_id&filter=cari");
            }

            $komponen_id = trim($komponen_id);
            $c = new Criteria();
            $c->add(KomponenPeer::KOMPONEN_ID, $komponen_id, Criteria::ILIKE);
            $rs_komponen = KomponenPeer::doSelectOne($c);
            if ($rs_komponen) {
                $this->rs_komponen = $rs_komponen;
            }

            $d = new Criteria();
            $d->add(SubtitleIndikatorPeer::UNIT_ID, $unit_id);
            $d->add(SubtitleIndikatorPeer::KEGIATAN_CODE, $kode_kegiatan);
            $d->addAscendingOrderByColumn(SubtitleIndikatorPeer::SUBTITLE);
            $rs_subtitleindikator = SubtitleIndikatorPeer::doSelect($d);
            $this->rs_subtitleindikator = $rs_subtitleindikator;

            $e = new Criteria();
            $e->addAscendingOrderByColumn(SatuanPeer::SATUAN_NAME);
            $rs_satuan = SatuanPeer::doSelect($e);
            $this->rs_satuan = $rs_satuan;

            $f = new Criteria();
            $f->addAscendingOrderByColumn(JasmasPeer::NAMA);
            $rs_jasmas = JasmasPeer::doSelect($f);
            $this->rs_jasmas = $rs_jasmas;

            $c = new Criteria();
            $c->add(RekeningPeer::REKENING_CODE, $kode_rekening);
            if ($rs_rekening = RekeningPeer::doSelectOne($c)) {
                $akrual_rek = $rs_rekening->getAkrualKonstruksi();
            }
            $sub = "char_length(akrual_code)>10";
            $c = new Criteria();
            $c->add(AkrualPeer::AKRUAL_CODE, $sub, Criteria::CUSTOM);
            if ($akrual_rek) {
                $c->addAnd(AkrualPeer::AKRUAL_CODE, $akrual_rek . '%', Criteria::ILIKE);
            } elseif ($rs_komponen->getKomponenTipe2() == 'ATRIBUSI' || $rs_komponen->getKomponenTipe2() == 'PERENCANAAN' || $rs_komponen->getKomponenTipe2() == 'PENGAWASAN') {
                $c->addAnd(AkrualPeer::AKRUAL_CODE, '1.3.1%', Criteria::ILIKE);
                $c->addOr(AkrualPeer::AKRUAL_CODE, '1.3.3%', Criteria::ILIKE);
                $c->addOr(AkrualPeer::AKRUAL_CODE, '1.3.4%', Criteria::ILIKE);
                $c->addOr(AkrualPeer::AKRUAL_CODE, '1.3.5%', Criteria::ILIKE);
                $c->addOr(AkrualPeer::AKRUAL_CODE, '1.5.3%', Criteria::ILIKE);
            }
            $c->addAscendingOrderByColumn(AkrualPeer::AKRUAL_CODE);
            $rs_akrualcode = AkrualPeer::doSelect($c);
            $this->rs_akrualcode = $rs_akrualcode;
        }
    }

    public function executeCarikomponen() {
        $kegiatan_code = $this->getRequestParameter('kegiatan');
        $unit_id = $this->getRequestParameter('unit');
        $status = 'OPEN';
        $c = new Criteria();
        $c->add(RincianPeer::KEGIATAN_CODE, $kegiatan_code);
        $c->add(RincianPeer::UNIT_ID, $unit_id);
        $v = RincianPeer::doSelectOne($c);
        if ($v) {
            if ($v->getLock() == TRUE) {
                $status = 'CLOSE';
            }
        }
        if ($status == 'OPEN') {
            $this->filters = $this->getRequestParameter('filters');
            if (isset($this->filters['nama_komponen']) && $this->filters['nama_komponen'] !== '') {
                $cari = $this->filters['nama_komponen'];
            }
            $this->cari = $cari;
        } else {
            $this->setFlash('berhasil', 'Anda Tidak Berhak Memasuki Halaman Sebelumnya');
            $this->redirect("kegiatan/edit?kode_kegiatan=$kegiatan_code&unit_id=$unit_id");
        }
    }

    public function executePilihsubx() {
        $x = new Criteria();
        $x->addSelectColumn(RincianDetailPeer::SUBTITLE);
        $x->add(RincianDetailPeer::KEGIATAN_CODE, $this->getRequestParameter('kegiatan_code'));
        $x->add(RincianDetailPeer::UNIT_ID, $this->getRequestParameter('unit_id'));
        if ($this->getRequestParameter('b') != '') {
            $s = new Criteria();
            $s->add(RincianDetailPeer::KEGIATAN_CODE, $this->getRequestParameter('kegiatan_code'));
            $s->add(RincianDetailPeer::UNIT_ID, $this->getRequestParameter('unit_id'));
            $s->setDistinct();
            $r = RincianDetailPeer::doselect($s);
            $kodekegiatan = $this->getRequestParameter('kegiatan_code');
            $unitid = $this->getRequestParameter('unit_id');
            $kode_sub = $this->getRequestParameter('b');

            $c = new Criteria();
            $c->add(SubtitleIndikatorPeer::SUB_ID, $kode_sub);
            $rs_subtitle = SubtitleIndikatorPeer::doSelectOne($c);
            if ($rs_subtitle) {
                $subtitle = $rs_subtitle->getSubtitle();
            }

            $queryku = "select distinct kode_sub,sub from " . sfConfig::get('app_default_schema') . ".rincian_detail where subtitle ilike '" . $subtitle . "%' and kegiatan_code='$kodekegiatan' and unit_id='$unitid'";



            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($queryku);
            $rcsb = $stmt->executeQuery();

            $arr_tampung = array();

            $this->isi_baru = array();
            while ($rcsb->next()) {
                if ($rcsb->getString('sub') != '')
                    $arr_tampung[$rcsb->getString('kode_sub')] = $rcsb->getString('sub');
            }
            $this->isi_baru = $arr_tampung;
        }
    }

    public function executePilihsubxrevisi() {
        $x = new Criteria();
        $x->addSelectColumn(RincianDetailPeer::SUBTITLE);
        $x->add(RincianDetailPeer::KEGIATAN_CODE, $this->getRequestParameter('kegiatan_code'));
        $x->add(RincianDetailPeer::UNIT_ID, $this->getRequestParameter('unit_id'));
        if ($this->getRequestParameter('b') != '') {
            $s = new Criteria();
            $s->add(RincianDetailPeer::KEGIATAN_CODE, $this->getRequestParameter('kegiatan_code'));
            $s->add(RincianDetailPeer::UNIT_ID, $this->getRequestParameter('unit_id'));
            $s->setDistinct();
            $r = RincianDetailPeer::doselect($s);
            $kodekegiatan = $this->getRequestParameter('kegiatan_code');
            $unitid = $this->getRequestParameter('unit_id');
            $kode_sub = $this->getRequestParameter('b');

            $c = new Criteria();
            $c->add(SubtitleIndikatorPeer::SUB_ID, $kode_sub);
            $rs_subtitle = SubtitleIndikatorPeer::doSelectOne($c);
            if ($rs_subtitle) {
                $subtitle = $rs_subtitle->getSubtitle();
            }

            $queryku = "select distinct kode_sub,sub from " . sfConfig::get('app_default_schema') . ".rincian_detail where subtitle ilike '" . $subtitle . "%' and kegiatan_code='$kodekegiatan' and unit_id='$unitid'";



            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($queryku);
            $rcsb = $stmt->executeQuery();

            $arr_tampung = array();

            $this->isi_baru = array();
            while ($rcsb->next()) {
                if ($rcsb->getString('sub') != '')
                    $arr_tampung[$rcsb->getString('kode_sub')] = $rcsb->getString('sub');
            }
            $this->isi_baru = $arr_tampung;
        }
    }

    public function executeEditKegiatan() {
        //print_r($this->getRequest());exit;
        if ($this->getRequestParameter('edit') == md5('ubah')) {
            $unit_id = $this->getRequestParameter('unit');
            $kegiatan_code = $this->getRequestParameter('kegiatan');
            $detail_no = $this->getRequestParameter('id');

            $c = new Criteria();
            $c->add(RincianDetailPeer::UNIT_ID, $unit_id);
            $c->add(RincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
            $c->add(RincianDetailPeer::DETAIL_NO, $detail_no);
            $rs_rinciandetail = RincianDetailPeer::doSelectOne($c);
            if ($rs_rinciandetail) {
                $this->rs_rinciandetail = $rs_rinciandetail;

                $c = new Criteria();
                $c->add(KomponenPeer::KOMPONEN_ID, $rs_rinciandetail->getKomponenId());
                if ($rs_est_fisik = KomponenPeer::doSelectOne($c))
                    $this->est_fisik = $rs_est_fisik->getIsEstFisik();
            }

            $d = new Criteria();
            $d->add(SubtitleIndikatorPeer::UNIT_ID, $unit_id);
            $d->add(SubtitleIndikatorPeer::KEGIATAN_CODE, $kegiatan_code);
            $d->addAscendingOrderByColumn(SubtitleIndikatorPeer::SUBTITLE);
            $rs_subtitleindikator = SubtitleIndikatorPeer::doSelect($d);
            $this->rs_subtitleindikator = $rs_subtitleindikator;

            $e = new Criteria();
            $e->addAscendingOrderByColumn(SatuanPeer::SATUAN_NAME);
            $rs_satuan = SatuanPeer::doSelect($e);
            $this->rs_satuan = $rs_satuan;

            $f = new Criteria();
            $f->addAscendingOrderByColumn(JasmasPeer::NAMA);
            $rs_jasmas = JasmasPeer::doSelect($f);
            $this->rs_jasmas = $rs_jasmas;
        }
        if ($this->getRequestParameter('cari') == 'cari') {
            //print_r($this->getRequestParameter('lokasi'));exit;
            $user = $this->getUser();
            $user->removeCredential('lokasi');
            $user->addCredential('lokasi');
            $user->setAttribute('nama', $this->getRequestParameter('lokasi'), 'lokasi');
            $user->setAttribute('kegiatan', $this->getRequestParameter('kegiatan'), 'lokasi');
            $user->setAttribute('unit', $this->getRequestParameter('unit'), 'lokasi');
            $user->setAttribute('id', $this->getRequestParameter('id'), 'lokasi');
            $user->setAttribute('ubah', 'ubah', 'lokasi');

            return $this->forward('lokasi', 'list');
        }
        if ($this->getRequestParameter('simpan') == 'simpan') {
            $unit_id = $this->getRequestParameter('unit');
            $kegiatan_code = $this->getRequestParameter('kegiatan');
            $detail_no = $this->getRequestParameter('id');

            $c_rincian_detail = new Criteria();
            $c_rincian_detail->add(RincianDetailPeer::UNIT_ID, $unit_id);
            $c_rincian_detail->add(RincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
            $c_rincian_detail->add(RincianDetailPeer::DETAIL_NO, $detail_no);
            $data_rincian_detail = RincianDetailPeer::doSelectOne($c_rincian_detail);

//            ambil untuk mbalikin ke WaitingList
            $data_lama_volume = $data_rincian_detail->getVolume();
            $data_lama_komponen_harga_awal = $data_rincian_detail->getKomponenHargaAwal();
            $data_lama_pajak = $data_rincian_detail->getPajak();
            $data_lama_keterangan_koefisien = $data_rincian_detail->getKeteranganKoefisien();
            $data_lama_satuan = $data_rincian_detail->getSatuan();
//            ambil untuk mbalikin ke WaitingList


            if (!$this->getRequestParameter('subtitle')) {
                $this->setFlash('gagal', 'Subtitle Belum Dipilih');
                return $this->redirect("kegiatan/editKegiatan?id=$detail_no&unit=$unit_id&kegiatan=$kegiatan_code&edit=" . md5('ubah'));
            }


            $detail_name = '';
            $kode_sub = '';
            $sub = '';
            $kode_jasmas = '';
            if ($this->getRequestParameter('lokasi')) {
                $kode_lokasi = '';
                $detail_name = $this->getRequestParameter('lokasi');
                $kode_jasmas = $this->getRequestParameter('jasmas');
                $c = new Criteria();
                $c->add(VLokasiPeer::NAMA, $detail_name);
                $rs_lokasi = VLokasiPeer::doSelectOne($c);
                if ($rs_lokasi) {
                    $kode_lokasi = $rs_lokasi->getKode();
                }
            }
            if ($this->getRequestParameter('keterangan')) {
                $detail_name = $this->getRequestParameter('keterangan');
            }
            if ($this->getRequestParameter('sub')) {
                $kode_sub = $this->getRequestParameter('sub');
                //print_r($kode_sub);exit;
                $d = new Criteria();
                $d->add(RincianSubParameterPeer::KODE_SUB, $kode_sub);
                $rs_rinciansubparameter = RincianSubParameterPeer::doSelectOne($d);
                if ($rs_rinciansubparameter) {
                    $sub = $rs_rinciansubparameter->getNewSubtitle();
                }
            }
            $kode_subtitle = $this->getRequestParameter('subtitle');

            $c = new Criteria();
            $c->add(SubtitleIndikatorPeer::SUB_ID, $kode_subtitle);
            $rs_subtitle = SubtitleIndikatorPeer::doSelectOne($c);
            if ($rs_subtitle) {
                $subtitle = $rs_subtitle->getSubtitle();
            }

            $volume = 0;
            $keterangan_koefisien = '';

            if ($this->getRequestParameter('vol1') || $this->getRequestParameter('vol2') || $this->getRequestParameter('vol3') || $this->getRequestParameter('vol4')) {
                if ($this->getRequestParameter('vol2') == '') {
                    $vol2 = 1;
                    $volume = $this->getRequestParameter('vol1') * $vol2;
                    $keterangan_koefisien = $this->getRequestParameter('vol1') . ' ' . $this->getRequestParameter('volume1');
                } else if (!$this->getRequestParameter('vol2') == '') {
                    $volume = $this->getRequestParameter('vol1') * $this->getRequestParameter('vol2');
                    $keterangan_koefisien = $this->getRequestParameter('vol1') . ' ' . $this->getRequestParameter('volume1') . ' X ' . $this->getRequestParameter('vol2') . ' ' . $this->getRequestParameter('volume2');
                }
                if ($this->getRequestParameter('vol3') == '') {
                    $vol3 = 1;
                    $volume = $volume * $vol3;
                } else if (!$this->getRequestParameter('vol3') == '') {
                    $volume = $this->getRequestParameter('vol1') * $this->getRequestParameter('vol2') * $this->getRequestParameter('vol3');
                    $keterangan_koefisien = $this->getRequestParameter('vol1') . ' ' . $this->getRequestParameter('volume1') . ' X ' . $this->getRequestParameter('vol2') . ' ' . $this->getRequestParameter('volume2') . ' X ' . $this->getRequestParameter('vol3') . ' ' . $this->getRequestParameter('volume3');
                }
                if ($this->getRequestParameter('vol4') == '') {
                    $vol4 = 1;
                    $volume = $volume * $vol4;
                } else if (!$this->getRequestParameter('vol4') == '') {
                    $volume = $this->getRequestParameter('vol1') * $this->getRequestParameter('vol2') * $this->getRequestParameter('vol3') * $this->getRequestParameter('vol4');
                    $keterangan_koefisien = $this->getRequestParameter('vol1') . ' ' . $this->getRequestParameter('volume1') . ' X ' . $this->getRequestParameter('vol2') . ' ' . $this->getRequestParameter('volume2') . ' X ' . $this->getRequestParameter('vol3') . ' ' . $this->getRequestParameter('volume3') . ' X ' . $this->getRequestParameter('vol4') . ' ' . $this->getRequestParameter('volume4');
                }
            }

            $sekarang = date('Y-m-d H:i:s');
            $tahap = '';
            $c = new Criteria();
            $c->add(MasterKegiatanPeer::UNIT_ID, $unit_id);
            $c->add(MasterKegiatanPeer::KODE_KEGIATAN, $kegiatan_code);
            $rs_kegiatan = MasterKegiatanPeer::doSelectOne($c);
            if ($rs_kegiatan) {
                $tahap = $rs_kegiatan->getTahap();
            }

            if ($this->getRequestParameter('status') == 'pending') {
                $query = "update " . sfConfig::get('app_default_schema') . ".rincian_detail set keterangan_koefisien='$keterangan_koefisien', volume=$volume, detail_name='$detail_name', subtitle='$subtitle', sub='$sub',kode_sub='$kode_sub',kecamatan='$kode_jasmas' where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and detail_no=$detail_no";

                $con = Propel::getConnection();
                $stmt = $con->prepareStatement($query);
                $stmt->executeQuery();

                $query = "select max(detail_no) as urut from " . sfConfig::get('app_default_schema') . ".rincian_detail_masalah where unit_id='$unit_id' and kegiatan_code='$kegiatan_code'";

                $con = Propel::getConnection();
                $stmt = $con->prepareStatement($query);
                $rs = $stmt->executeQuery();
                while ($rs->next()) {
                    $detail = $rs->getString('urut');
                }
                $detail+=1;

                $query = "insert into " . sfConfig::get('app_default_schema') . ".rincian_detail_masalah select * from " . sfConfig::get('app_default_schema') . ".rincian_detail where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and detail_no = $detail_no";
                $con = Propel::getConnection();
                $stmt = $con->prepareStatement($query);
                $stmt->executeQuery();

                $query = "update " . sfConfig::get('app_default_schema') . ".rincian_detail_masalah set detail_no=$detail where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and detail_no=$detail_no";

                $con = Propel::getConnection();
                $stmt = $con->prepareStatement($query);
                $stmt->executeQuery();

                $query = "update " . sfConfig::get('app_default_schema') . ".rincian_detail set keterangan_koefisien='0', volume=0, detail_name='$detail_name', subtitle='$subtitle', sub='$sub',kode_sub='$kode_sub' where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and detail_no=$detail_no";

                $con = Propel::getConnection();
                $stmt = $con->prepareStatement($query);
                $stmt->executeQuery();
                $this->setFlash('berhasil', 'Perubahan Telah Berhasil Dilakukan, Tetapi Menjadi Tidak Masuk ke RKA, Harap Hubungi Penyelia Anda');
            } else {
                $c = new Criteria();
                $c->add(RincianDetailPeer::UNIT_ID, $unit_id);
                $c->add(RincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
                $c->add(RincianDetailPeer::DETAIL_NO, $detail_no);
                $rincian_detail = RincianDetailPeer::doSelectOne($c);

                $c = new Criteria();
                $c->add(KomponenPeer::KOMPONEN_ID, $rincian_detail->getKomponenId());
                if ($rs_est_fisik = KomponenPeer::doSelectOne($c))
                    $est_fisik = $rs_est_fisik->getIsEstFisik();
                $tipe2 = $data_rincian_detail->getTipe2();

                $pajak = $rincian_detail->getPajak();
                $harga = $rincian_detail->getKomponenHargaAwal();
                //$nilaiBaru = round($harga * $volume * (100 + $pajak) / 100);
                $nilaiBaru = $rincian_detail->getNilaiAnggaran();
                //irul 6 feb 2014 - tempat saving
                $totNilaiSwakelola = 0;
                $totNilaiKontrak = 0;
                $totNilaiAlokasi = 0;
                $totNilaiHps = 0;
                $lelang = 0;
                $ceklelangselesaitidakaturanpembayaran = 0;

                $rd = new RincianDetail();
                if (sfConfig::get('app_fasilitas_cekeProject') == 'buka') {
                    $totNilaiAlokasi = $rd->getCekNilaiAlokasiProject($unit_id, $kegiatan_code, $detail_no);
                    if (sfConfig::get('app_fasilitas_cekServer') == 'buka') {
                        $lelang = $rd->getCekLelang($unit_id, $kegiatan_code, $detail_no, $rincian_detail->getNilaiAnggaran());
                        if (sfConfig::get('app_fasilitas_cekeDelivery') == 'buka') {
                            $totNilaiSwakelola = $rd->getCekNilaiSwakelolaDelivery2($unit_id, $kegiatan_code, $detail_no);
                            $totNilaiKontrak = $rd->getCekNilaiKontrakDelivery2($unit_id, $kegiatan_code, $detail_no);

                            $totNilaiHps = $rd->getCekNilaiHPSKomponen($unit_id, $kegiatan_code, $detail_no);
                            $ceklelangselesaitidakaturanpembayaran = $rd->getCekLelangTidakAdaAturanPembayaran($unit_id, $kegiatan_code, $detail_no);
                        }
                    }
                }

                //irul 6 feb 2014 - cek nilai kontrak, swakelola, alokasi
                if (($nilaiBaru < $totNilaiRealisasi) || ($nilaiBaru < $totNilaiSwakelola)) {
                    if ($totNilaiKontrak == 0) {
                        $this->setFlash('gagal', 'Mohon maaf , untuk komponen  ini sudah terpakai di edelivery, sejumlah Rp.' . number_format($totNilaiSwakelola, 0, ',', '.'));
                    } else if ($totNilaiSwakelola == 0) {
                        $this->setFlash('gagal', 'Mohon maaf , untuk komponen  ini sudah terpakai di edelivery, sejumlah Rp.' . number_format($totNilaiRealisasi, 0, ',', '.'));
                    } else {
                        $this->setFlash('gagal', 'Mohon maaf , untuk komponen  ini sudah terpakai di edelivery, sejumlah Rp.' . number_format($totNilaiRealisasi, 0, ',', '.'));
                    }
                    return $this->redirect('kegiatan/editKegiatan?id=' . $detail_no . '&unit=' . $unit_id . '&kegiatan=' . $kegiatan_code . '&edit=' . md5('ubah'));
                } else if ($nilaiBaru < $totNilaiHps) {
                    $this->setFlash('gagal', 'Mohon maaf , Nilai HPS, sejumlah Rp.' . number_format($totNilaiHps, 0, ',', '.'));
                    return $this->redirect('kegiatan/editKegiatan?id=' . $detail_no . '&unit=' . $unit_id . '&kegiatan=' . $kegiatan_code . '&edit=' . md5('ubah'));
                } else if ($lelang > 0) {
                    $this->setFlash('gagal', 'Sedang dalam Proses Lelang untuk komponen ini');
                    return $this->redirect('kegiatan/editKegiatan?id=' . $detail_no . '&unit=' . $unit_id . '&kegiatan=' . $kegiatan_code . '&edit=' . md5('ubah'));
                } else if ($ceklelangselesaitidakaturanpembayaran == 1) {
                    $this->setFlash('gagal', 'Proses Lelang untuk komponen ini telah selesai, namun Belum ada isian Aturan Pembayaran eDelivery. Silahkan mengisi Aturan Pembayaran terlebih dahulu ');
                    return $this->redirect('kegiatan/editKegiatan?id=' . $detail_no . '&unit=' . $unit_id . '&kegiatan=' . $kegiatan_code . '&edit=' . md5('ubah'));
                } else {
                    //irul 24 feb 2014 tarik Komponen otomatis
                    if ($nilaiBaru < $totNilaiAlokasi) {
                        $I = new Criteria();
                        $I->add(RincianDetailBpPeer::UNIT_ID, $unit_id);
                        $I->add(RincianDetailBpPeer::KEGIATAN_CODE, $kegiatan_code);
                        $I->add(RincianDetailBpPeer::DETAIL_NO, $detail_no);
                        $rd_bp = RincianDetailBpPeer::doSelectOne($I);
                        if ($rd_bp) {
                            try {
                                $rd_bp->setKeteranganKoefisien($keterangan_koefisien);
                                $rd_bp->setVolume($volume);
                                $rd_bp->setDetailName($detail_name);
                                $rd_bp->setSubtitle($subtitle);
                                $rd_bp->setSub($sub);
                                $rd_bp->setKodeSub($kode_sub);
                                $rd_bp->setNotePeneliti($notepenyelia);
                                $rd_bp->setKecamatan($kode_jasmas);
                                $rd_bp->setIsPerKomponen('true');
                                $rd_bp->setTahap($tahap);
                                $rd_bp->save();

                                budgetLogger::log('Mengupdate volume eProject untuk komponen menjadi ' . $volume . ' dari unit id :' . $unit_id . ' dengan kode :' . $kegiatan_code . '; detail_no :' . $detail_no . '; komponen_id:' . $rincian_detail->getKomponenId() . '; komponen_name:' . $rincian_detail->getKomponenName());
                            } catch (Exception $ex) {
                                $this->setFlash('gagal', 'Gagal karena' . $ex);
                            }
                        }
                    }
                    //irul 24 feb 2014 tarik Komponen otomatis

                    sfContext::getInstance()->getLogger()->debug('{eProject} rincian detail ketemu, nilai baru = ' . $nilaiBaru);
                    $rincian_detail->setKeteranganKoefisien($keterangan_koefisien);
                    $rincian_detail->setVolume($volume);
                    $rincian_detail->setDetailName($detail_name);
                    $rincian_detail->setSubtitle($subtitle);
                    $rincian_detail->setSub($sub);
                    $rincian_detail->setKodeSub($kode_sub);
                    $rincian_detail->setKecamatan($kode_jasmas);
                    $rincian_detail->setLastEditTime($sekarang);
                    $rincian_detail->setTahap($tahap);

                    $rincian_detail->setNotePeneliti($notepenyelia);
                    if ($rincian_detail->getNoteSkpd() == '') {
                        $rincian_detail->setNoteSkpd($notepenyelia);
                    }

                    budgetLogger::log('Mengubah komponen dari unit id :' . $unit_id . ' dengan kode :' . $kegiatan_code . '; detail_no :' . $detail_no . '; komponen_id:' . $rincian_detail->getKomponenId() . '; komponen_name:' . $rincian_detail->getKomponenName());
                    $rincian_detail->save();
                    $this->setFlash('berhasil', 'Perubahan Telah Berhasil Dilakukan');
                }
            }
            //irul 10 desember 2014 - edit data gmap
            $c2 = new Criteria();
            $c2->add(RincianDetailPeer::UNIT_ID, $unit_id);
            $c2->add(RincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
            $c2->add(RincianDetailPeer::DETAIL_NO, $detail_no);
            $rincian_detail2 = RincianDetailPeer::doSelectOne($c2);
            if ($rincian_detail2) {
                $query_cek_gmap = "select count(*) as jumlah 
                        from " . sfConfig::get('app_default_gis') . ".geojsonlokasi_rev1 
                            where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and detail_no = $detail_no";
                $con = Propel::getConnection();
                $stmt = $con->prepareStatement($query_cek_gmap);
                $rs = $stmt->executeQuery();
                while ($rs->next()) {
                    $jumlah = $rs->getString('jumlah');
                }
                if ($jumlah > 0) {
                    $query = "update " . sfConfig::get('app_default_gis') . ".geojsonlokasi_rev1 "
                            . "set last_edit_time = '$sekarang', satuan = '" . $rincian_detail2->getSatuan() . "', volume = " . $rincian_detail2->getVolume() . ", nilai_anggaran = " . $rincian_detail2->getNilaiAnggaran() . ", komponen_name = '" . $rincian_detail2->getKomponenName() . "'  "
                            . "where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and detail_no = $detail_no and tahun = '" . sfConfig::get('app_tahun_default') . "'";
                    $stmt = $con->prepareStatement($query);
                    $stmt->executeQuery();
                }
            }
            //irul 10 desember 2014 - edit data gmap

            if ($tipe2 == 'KONSTRUKSI' || $data_rincian_detail->getTipe() == 'FISIK' || $est_fisik) {
                $rd_cari = new Criteria();
                $rd_cari->add(RincianDetailPeer::UNIT_ID, $unit_id, Criteria::EQUAL);
                $rd_cari->add(RincianDetailPeer::KEGIATAN_CODE, $kegiatan_code, Criteria::EQUAL);
                $rd_cari->add(RincianDetailPeer::DETAIL_NO, $detail_no, Criteria::EQUAL);
                $rd_dapat = RincianDetailPeer::doSelectOne($rd_cari);

                //                                kalo waitinglist dinolin
                $ada_waitinglist = 0;
                $rd_cari_waitinglist = new Criteria();
                $rd_cari_waitinglist->add(WaitingListPUPeer::KODE_RKA, $unit_id . '.' . $kegiatan_code . '.' . $detail_no, Criteria::EQUAL);
                $rd_cari_waitinglist->addAnd(WaitingListPUPeer::STATUS_WAITING, 1);
                $rd_cari_waitinglist->addAnd(WaitingListPUPeer::STATUS_HAPUS, FALSE);

                $ada_waitinglist = WaitingListPUPeer::doCount($rd_cari_waitinglist);

                if ($ada_waitinglist > 0 && ($unit_id == '2600' || $unit_id == '2300') && $rd_dapat->getNilaiAnggaran() == 0) {
                    $rd_dapat_waitinglist = WaitingListPUPeer::doSelectOne($rd_cari_waitinglist);

                    $total_aktif = 0;
                    $query = "select count(*) as total "
                            . "from " . sfConfig::get('app_default_schema') . ".waitinglist_pu "
                            . "where status_hapus = false and status_waiting = 0 "
                            . "and unit_id = 'XXX$unit_id' and kegiatan_code = '" . $kegiatan_code . "'";
                    $stmt = $con->prepareStatement($query);
                    $rs = $stmt->executeQuery();
                    while ($rs->next()) {
                        $total_aktif = $rs->getString('total');
                    }
                    $total_aktif++;

                    $balik_waitinglist = new WaitingListPU();
                    $balik_waitinglist->setUnitId('XXX' . $unit_id);
                    $balik_waitinglist->setKegiatanCode($kegiatan_code);
                    $balik_waitinglist->setSubtitle($rd_dapat->getSubtitle());
                    $balik_waitinglist->setKomponenId($rd_dapat->getKomponenId());
                    $balik_waitinglist->setKomponenName($rd_dapat->getKomponenName());
                    $balik_waitinglist->setKomponenLokasi($rd_dapat->getDetailName());
                    $balik_waitinglist->setKomponenHargaAwal($data_lama_komponen_harga_awal);
                    $balik_waitinglist->setPajak($data_lama_pajak);
                    $balik_waitinglist->setKomponenSatuan($data_lama_satuan);
                    $balik_waitinglist->setKomponenRekening($rd_dapat->getRekeningCode());
                    $balik_waitinglist->setKoefisien($data_lama_keterangan_koefisien);
                    $balik_waitinglist->setVolume($data_lama_volume);
                    $balik_waitinglist->setTahunInput($rd_dapat->getTahun());
                    $balik_waitinglist->setCreatedAt($sekarang);
                    $balik_waitinglist->setUpdatedAt($sekarang);
                    $balik_waitinglist->setStatusHapus(FALSE);
                    $balik_waitinglist->setStatusWaiting(0);
                    $balik_waitinglist->setKodeJasmas($rd_dapat->getKecamatan());
                    $balik_waitinglist->setKecamatan($rd_dapat->getLokasiKecamatan());
                    $balik_waitinglist->setKelurahan($rd_dapat->getLokasiKelurahan());
                    $balik_waitinglist->setIsMusrenbang($rd_dapat->getIsMusrenbang());
                    $balik_waitinglist->setPrioritas($total_aktif);
                    $balik_waitinglist->setNilaiEe($rd_dapat_waitinglist->getNilaiEe());
                    $balik_waitinglist->setKeterangan($rd_dapat_waitinglist->getKeterangan());
                    $balik_waitinglist->save();

                    $id_baru = $balik_waitinglist->getIdWaiting();

                    $c_cari_history_lama = new Criteria();
                    $c_cari_history_lama->add(HistoryPekerjaanV2Peer::KODE_RKA, $unit_id . '.' . $kegiatan_code . '.' . $detail_no);
                    $c_cari_history_lama->addAnd(HistoryPekerjaanV2Peer::TAHUN, sfConfig::get('app_tahun_default'));
                    $dapat_history_lama = HistoryPekerjaanV2Peer::doSelect($c_cari_history_lama);
                    if ($dapat_history_lama) {
                        foreach ($dapat_history_lama as $value_history_lama) {
                            $c_insert_gis = new HistoryPekerjaanV2();
                            $c_insert_gis->setTahun(sfConfig::get('app_tahun_default'));
                            $c_insert_gis->setKodeRka('XXX' . $unit_id . '.' . $kegiatan_code . '.' . $id_baru);
                            $c_insert_gis->setStatusHapus(FALSE);
                            $c_insert_gis->setJalan($value_history_lama->getJalan());
                            $c_insert_gis->setGang($value_history_lama->getGang());
                            $c_insert_gis->setNomor($value_history_lama->getNomor());
                            $c_insert_gis->setRw($value_history_lama->getRw());
                            $c_insert_gis->setRt($value_history_lama->getRt());
                            $c_insert_gis->setKeterangan($value_history_lama->getKeterangan());
                            $c_insert_gis->setTempat($value_history_lama->getTempat());
                            $c_insert_gis->setKomponen($value_history_lama->getKomponen());
                            $c_insert_gis->setKecamatan($value_history_lama->getKecamatan());
                            $c_insert_gis->setKelurahan($value_history_lama->getKelurahan());
                            $c_insert_gis->setLokasi($value_history_lama->getLokasi());
                            $c_insert_gis->save();
                        }
                    }
                }
//                                kalo waitinglist dinolin
            }

            return $this->redirect('kegiatan/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
        }
    }

    public function executeEditKegiatanRevisi() {
        //print_r($this->getRequest());exit;
        if ($this->getRequestParameter('edit') == md5('ubah')) {
            $unit_id = $this->getRequestParameter('unit');
            $kegiatan_code = $this->getRequestParameter('kegiatan');
            $detail_no = $this->getRequestParameter('id');

            $c = new Criteria();
            $c->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
            $c->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
            $c->add(DinasRincianDetailPeer::DETAIL_NO, $detail_no);
            $rs_rinciandetail = DinasRincianDetailPeer::doSelectOne($c);
            if ($rs_rinciandetail) {
                $this->rs_rinciandetail = $rs_rinciandetail;

                $c = new Criteria();
                $c->add(KomponenPeer::KOMPONEN_ID, $rs_rinciandetail->getKomponenId());
                if ($rs_est_fisik = KomponenPeer::doSelectOne($c))
                    $this->est_fisik = $rs_est_fisik->getIsEstFisik();
            }

            $d = new Criteria();
            $d->add(DinasSubtitleIndikatorPeer::UNIT_ID, $unit_id);
            $d->add(DinasSubtitleIndikatorPeer::KEGIATAN_CODE, $kegiatan_code);
            $d->addAscendingOrderByColumn(DinasSubtitleIndikatorPeer::SUBTITLE);
            $rs_subtitleindikator = DinasSubtitleIndikatorPeer::doSelect($d);
            $this->rs_subtitleindikator = $rs_subtitleindikator;

            $e = new Criteria();
            $e->addAscendingOrderByColumn(SatuanPeer::SATUAN_NAME);
            $rs_satuan = SatuanPeer::doSelect($e);
            $this->rs_satuan = $rs_satuan;

            $f = new Criteria();
            $f->addAscendingOrderByColumn(JasmasPeer::NAMA);
            $rs_jasmas = JasmasPeer::doSelect($f);
            $this->rs_jasmas = $rs_jasmas;
        }
        if ($this->getRequestParameter('cari') == 'cari') {
            //print_r($this->getRequestParameter('lokasi'));exit;
            $user = $this->getUser();
            $user->removeCredential('lokasi');
            $user->addCredential('lokasi');
            $user->setAttribute('nama', $this->getRequestParameter('lokasi'), 'lokasi');
            $user->setAttribute('kegiatan', $this->getRequestParameter('kegiatan'), 'lokasi');
            $user->setAttribute('unit', $this->getRequestParameter('unit'), 'lokasi');
            $user->setAttribute('id', $this->getRequestParameter('id'), 'lokasi');
            $user->setAttribute('ubah', 'ubah', 'lokasi');

            return $this->forward('lokasi', 'list');
        }
        if ($this->getRequestParameter('simpan') == 'simpan') {
            $unit_id = $this->getRequestParameter('unit');
            $kegiatan_code = $this->getRequestParameter('kegiatan');
            $detail_no = $this->getRequestParameter('id');

            $c_rincian_detail = new Criteria();
            $c_rincian_detail->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
            $c_rincian_detail->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
            $c_rincian_detail->add(DinasRincianDetailPeer::DETAIL_NO, $detail_no);
            $data_rincian_detail = DinasRincianDetailPeer::doSelectOne($c_rincian_detail);

//            ambil untuk mbalikin ke WaitingList
            $data_lama_volume = $data_rincian_detail->getVolume();
            $data_lama_komponen_harga_awal = $data_rincian_detail->getKomponenHargaAwal();
            $data_lama_pajak = $data_rincian_detail->getPajak();
            $data_lama_keterangan_koefisien = $data_rincian_detail->getKeteranganKoefisien();
            $data_lama_satuan = $data_rincian_detail->getSatuan();
//            ambil untuk mbalikin ke WaitingList


            if (!$this->getRequestParameter('subtitle')) {
                $this->setFlash('gagal', 'Subtitle Belum Dipilih');
                return $this->redirect("kegiatan/editKegiatanRevisi?id=$detail_no&unit=$unit_id&kegiatan=$kegiatan_code&edit=" . md5('ubah'));
            }

            $dau = 'false';
            if (is_null($this->getRequestParameter('dau'))) {
                $dau = 'false';
            } else if ($this->getRequestParameter('dau') == 1) {
                $dau = 'true';
            }


            $detail_name = '';
            $kode_sub = '';
            $sub = '';
            $kode_jasmas = '';
            if ($this->getRequestParameter('lokasi')) {
                $kode_lokasi = '';
                $detail_name = $this->getRequestParameter('lokasi');
                $kode_jasmas = $this->getRequestParameter('jasmas');
                $c = new Criteria();
                $c->add(VLokasiPeer::NAMA, $detail_name);
                $rs_lokasi = VLokasiPeer::doSelectOne($c);
                if ($rs_lokasi) {
                    $kode_lokasi = $rs_lokasi->getKode();
                }
            }
            if ($this->getRequestParameter('keterangan')) {
                $detail_name = $this->getRequestParameter('keterangan');
            }
            if ($this->getRequestParameter('sub')) {
                $kode_sub = $this->getRequestParameter('sub');
                //print_r($kode_sub);exit;
                $d = new Criteria();
                $d->add(DinasRincianSubParameterPeer::KODE_SUB, $kode_sub);
                $rs_rinciansubparameter = DinasRincianSubParameterPeer::doSelectOne($d);
                if ($rs_rinciansubparameter) {
                    $sub = $rs_rinciansubparameter->getNewSubtitle();
                }
            }
            $kode_subtitle = $this->getRequestParameter('subtitle');

            $c = new Criteria();
            $c->add(DinasSubtitleIndikatorPeer::SUB_ID, $kode_subtitle);
            $rs_subtitle = DinasSubtitleIndikatorPeer::doSelectOne($c);
            if ($rs_subtitle) {
                $subtitle = $rs_subtitle->getSubtitle();
            }
            $notepenyelia = '';
            $notepenyelia = $this->getRequestParameter('catatan');

            $volume = 0;
            $keterangan_koefisien = '';

            if ($this->getRequestParameter('vol1') || $this->getRequestParameter('vol2') || $this->getRequestParameter('vol3') || $this->getRequestParameter('vol4')) {
                if ($this->getRequestParameter('vol2') == '') {
                    $vol2 = 1;
                    $volume = $this->getRequestParameter('vol1') * $vol2;
                    $keterangan_koefisien = $this->getRequestParameter('vol1') . ' ' . $this->getRequestParameter('volume1');
                } else if (!$this->getRequestParameter('vol2') == '') {
                    $volume = $this->getRequestParameter('vol1') * $this->getRequestParameter('vol2');
                    $keterangan_koefisien = $this->getRequestParameter('vol1') . ' ' . $this->getRequestParameter('volume1') . ' X ' . $this->getRequestParameter('vol2') . ' ' . $this->getRequestParameter('volume2');
                }
                if ($this->getRequestParameter('vol3') == '') {
                    $vol3 = 1;
                    $volume = $volume * $vol3;
                } else if (!$this->getRequestParameter('vol3') == '') {
                    $volume = $this->getRequestParameter('vol1') * $this->getRequestParameter('vol2') * $this->getRequestParameter('vol3');
                    $keterangan_koefisien = $this->getRequestParameter('vol1') . ' ' . $this->getRequestParameter('volume1') . ' X ' . $this->getRequestParameter('vol2') . ' ' . $this->getRequestParameter('volume2') . ' X ' . $this->getRequestParameter('vol3') . ' ' . $this->getRequestParameter('volume3');
                }
                if ($this->getRequestParameter('vol4') == '') {
                    $vol4 = 1;
                    $volume = $volume * $vol4;
                } else if (!$this->getRequestParameter('vol4') == '') {
                    $volume = $this->getRequestParameter('vol1') * $this->getRequestParameter('vol2') * $this->getRequestParameter('vol3') * $this->getRequestParameter('vol4');
                    $keterangan_koefisien = $this->getRequestParameter('vol1') . ' ' . $this->getRequestParameter('volume1') . ' X ' . $this->getRequestParameter('vol2') . ' ' . $this->getRequestParameter('volume2') . ' X ' . $this->getRequestParameter('vol3') . ' ' . $this->getRequestParameter('volume3') . ' X ' . $this->getRequestParameter('vol4') . ' ' . $this->getRequestParameter('volume4');
                }
            }

            $sekarang = date('Y-m-d H:i:s');
            $tahap = '';
            $c = new Criteria();
            $c->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
            $c->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kegiatan_code);
            $rs_kegiatan = DinasMasterKegiatanPeer::doSelectOne($c);
            if ($rs_kegiatan) {
                $tahap = $rs_kegiatan->getTahap();
            }

            if ($this->getRequestParameter('status') == 'pending') {
                $query = "update " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail set keterangan_koefisien='$keterangan_koefisien', volume=$volume, detail_name='$detail_name', subtitle='$subtitle', sub='$sub',kode_sub='$kode_sub',kecamatan='$kode_jasmas' where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and detail_no=$detail_no";

                $con = Propel::getConnection();
                $stmt = $con->prepareStatement($query);
                $stmt->executeQuery();

                $query = "select max(detail_no) as urut from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail_masalah where unit_id='$unit_id' and kegiatan_code='$kegiatan_code'";

                $con = Propel::getConnection();
                $stmt = $con->prepareStatement($query);
                $rs = $stmt->executeQuery();
                while ($rs->next()) {
                    $detail = $rs->getString('urut');
                }
                $detail+=1;

                $query = "insert into " . sfConfig::get('app_default_schema') . ".rincian_detail_masalah select * from " . sfConfig::get('app_default_schema') . ".rincian_detail where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and detail_no = $detail_no";
                $con = Propel::getConnection();
                $stmt = $con->prepareStatement($query);
                $stmt->executeQuery();

                $query = "update " . sfConfig::get('app_default_schema') . ".rincian_detail_masalah set detail_no=$detail where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and detail_no=$detail_no";

                $con = Propel::getConnection();
                $stmt = $con->prepareStatement($query);
                $stmt->executeQuery();

                $query = "update " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail set keterangan_koefisien='0', volume=0, detail_name='$detail_name', subtitle='$subtitle', sub='$sub',kode_sub='$kode_sub' where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and detail_no=$detail_no";

                $con = Propel::getConnection();
                $stmt = $con->prepareStatement($query);
                $stmt->executeQuery();
                $this->setFlash('berhasil', 'Perubahan Telah Berhasil Dilakukan, Tetapi Menjadi Tidak Masuk ke RKA, Harap Hubungi Penyelia Anda');
            } else {             
                $c = new Criteria();
                $c->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
                $c->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
                $c->add(DinasRincianDetailPeer::DETAIL_NO, $detail_no);
                $rincian_detail = DinasRincianDetailPeer::doSelectOne($c);

                $c = new Criteria();
                $c->add(KomponenPeer::KOMPONEN_ID, $rincian_detail->getKomponenId());
                if ($rs_est_fisik = KomponenPeer::doSelectOne($c))
                    $est_fisik = $rs_est_fisik->getIsEstFisik();
                $tipe2 = $data_rincian_detail->getTipe2();

                $pajak = $rincian_detail->getPajak();
                $harga = $rincian_detail->getKomponenHargaAwal();
                //$nilaiBaru = round($harga * $volume * (100 + $pajak) / 100);
                $nilaiBaru = $rincian_detail->getNilaiAnggaran();
                //irul 6 feb 2014 - tempat saving
                $totNilaiSwakelola = 0;
                $totNilaiKontrak = 0;
                $totNilaiAlokasi = 0;
                $totNilaiHps = 0;
                $lelang = 0;
                $ceklelangselesaitidakaturanpembayaran = 0;

                $rd = new DinasRincianDetail();
                if (sfConfig::get('app_fasilitas_cekeProject') == 'buka') {
                    $totNilaiAlokasi = $rd->getCekNilaiAlokasiProject($unit_id, $kegiatan_code, $detail_no);
                    if (sfConfig::get('app_fasilitas_cekServer') == 'buka') {
                        $lelang = $rd->getCekLelang($unit_id, $kegiatan_code, $detail_no, $rincian_detail->getNilaiAnggaran());
                        if (sfConfig::get('app_fasilitas_cekeDelivery') == 'buka') {
                            $totNilaiSwakelola = $rd->getCekNilaiSwakelolaDelivery2($unit_id, $kegiatan_code, $detail_no);
                            $totNilaiKontrak = $rd->getCekNilaiKontrakDelivery2($unit_id, $kegiatan_code, $detail_no);

                            $totNilaiHps = $rd->getCekNilaiHPSKomponen($unit_id, $kegiatan_code, $detail_no);
                            $ceklelangselesaitidakaturanpembayaran = $rd->getCekLelangTidakAdaAturanPembayaran($unit_id, $kegiatan_code, $detail_no);
                        }
                    }
                }

                // if($tahap != 'murni'){

                //irul 6 feb 2014 - cek nilai kontrak, swakelola, alokasi
                // karena masih murni jadi validasi delivery nya di tutup
                if (($nilaiBaru < $totNilaiRealisasi) || ($nilaiBaru < $totNilaiSwakelola)) {
                    if ($totNilaiKontrak == 0) {
                        $this->setFlash('gagal', 'Mohon maaf , untuk komponen  ini sudah terpakai di edelivery, sejumlah Rp.' . number_format($totNilaiSwakelola, 0, ',', '.'));
                    } else if ($totNilaiSwakelola == 0) {
                        $this->setFlash('gagal', 'Mohon maaf , untuk komponen  ini sudah terpakai di edelivery, sejumlah Rp.' . number_format($totNilaiRealisasi, 0, ',', '.'));
                    } else {
                        $this->setFlash('gagal', 'Mohon maaf , untuk komponen  ini sudah terpakai di edelivery, sejumlah Rp.' . number_format($totNilaiRealisasi, 0, ',', '.'));
                    }
                    return $this->redirect('kegiatan/editKegiatanRevisi?id=' . $detail_no . '&unit=' . $unit_id . '&kegiatan=' . $kegiatan_code . '&edit=' . md5('ubah'));
                } else if ($nilaiBaru < $totNilaiHps) {
                    $this->setFlash('gagal', 'Mohon maaf , Nilai HPS, sejumlah Rp.' . number_format($totNilaiHps, 0, ',', '.'));
                    return $this->redirect('kegiatan/editKegiatanRevisi?id=' . $detail_no . '&unit=' . $unit_id . '&kegiatan=' . $kegiatan_code . '&edit=' . md5('ubah'));
                } else if ($lelang > 0) {
                    $this->setFlash('gagal', 'Sedang dalam Proses Lelang untuk komponen ini');
                    return $this->redirect('kegiatan/editKegiatanRevisi?id=' . $detail_no . '&unit=' . $unit_id . '&kegiatan=' . $kegiatan_code . '&edit=' . md5('ubah'));
                } else if ($ceklelangselesaitidakaturanpembayaran == 1) {
                    $this->setFlash('gagal', 'Proses Lelang untuk komponen ini telah selesai, namun Belum ada isian Aturan Pembayaran eDelivery. Silahkan mengisi Aturan Pembayaran terlebih dahulu ');
                    return $this->redirect('kegiatan/editKegiatanRevisi?id=' . $detail_no . '&unit=' . $unit_id . '&kegiatan=' . $kegiatan_code . '&edit=' . md5('ubah'));
                } else {
                    //irul 24 feb 2014 tarik Komponen otomatis
//                    if ($nilaiBaru < $totNilaiAlokasi) {
//                        $I = new Criteria();
//                        $I->add(RincianDetailBpPeer::UNIT_ID, $unit_id);
//                        $I->add(RincianDetailBpPeer::KEGIATAN_CODE, $kegiatan_code);
//                        $I->add(RincianDetailBpPeer::DETAIL_NO, $detail_no);
//                        $rd_bp = RincianDetailBpPeer::doSelectOne($I);
//                        if ($rd_bp) {
//                            try {
//                                $rd_bp->setKeteranganKoefisien($keterangan_koefisien);
//                                $rd_bp->setVolume($volume);
//                                $rd_bp->setDetailName($detail_name);
//                                $rd_bp->setSubtitle($subtitle);
//                                $rd_bp->setSub($sub);
//                                $rd_bp->setKodeSub($kode_sub);
//                                $rd_bp->setNotePeneliti($notepenyelia);
//                                $rd_bp->setKecamatan($kode_jasmas);
//                                $rd_bp->setIsPerKomponen('true');
//                                $rd_bp->setTahap($tahap);
//                                $rd_bp->save();
//
//                                budgetLogger::log('Mengupdate volume eProject untuk komponen menjadi ' . $volume . ' dari unit id :' . $unit_id . ' dengan kode :' . $kegiatan_code . '; detail_no :' . $detail_no . '; komponen_id:' . $rincian_detail->getKomponenId() . '; komponen_name:' . $rincian_detail->getKomponenName());
//                            } catch (Exception $ex) {
//                                $this->setFlash('gagal', 'Gagal karena' . $ex);
//                            }
//                        }
//                    }
                    //irul 24 feb 2014 tarik Komponen otomatis

                    sfContext::getInstance()->getLogger()->debug('{eProject} rincian detail ketemu, nilai baru = ' . $nilaiBaru);
                    $rincian_detail->setKeteranganKoefisien($keterangan_koefisien);
                    $rincian_detail->setVolume($volume);
                    $rincian_detail->setDetailName($detail_name);
                    $rincian_detail->setSubtitle($subtitle);
                    // $rincian_detail->setSub($sub);
                    $rincian_detail->setKodeSub($kode_sub);
                    $rincian_detail->setKecamatan($kode_jasmas);
                    $rincian_detail->setLastEditTime($sekarang);
                    $rincian_detail->setTahap($tahap);
                    $rincian_detail->setIsDau($dau);

                    $rincian_detail->setNotePeneliti($notepenyelia);
                    if ($rincian_detail->getNoteSkpd() == '') {
                        $rincian_detail->setNoteSkpd($notepenyelia);
                    }

                    budgetLogger::log('Mengubah komponen dari unit id :' . $unit_id . ' dengan kode :' . $kegiatan_code . '; detail_no :' . $detail_no . '; komponen_id:' . $rincian_detail->getKomponenId() . '; komponen_name:' . $rincian_detail->getKomponenName());
                    $rincian_detail->save();
                    $this->setFlash('berhasil', 'Perubahan Telah Berhasil Dilakukan');
                } //tutup else validasi delivery
            // } //tutup if tahap != murni
            }
            //irul 10 desember 2014 - edit data gmap
            $c2 = new Criteria();
            $c2->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
            $c2->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
            $c2->add(DinasRincianDetailPeer::DETAIL_NO, $detail_no);
            $rincian_detail2 = DinasRincianDetailPeer::doSelectOne($c2);
            if ($rincian_detail2) {
                $query_cek_gmap = "select count(*) as jumlah 
                        from " . sfConfig::get('app_default_gis') . ".geojsonlokasi_rev1 
                            where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and detail_no = $detail_no";
                $con = Propel::getConnection();
                $stmt = $con->prepareStatement($query_cek_gmap);
                $rs = $stmt->executeQuery();
                while ($rs->next()) {
                    $jumlah = $rs->getString('jumlah');
                }
                if ($jumlah > 0) {
                    $query = "update " . sfConfig::get('app_default_gis') . ".geojsonlokasi_rev1 "
                            . "set last_edit_time = '$sekarang', satuan = '" . $rincian_detail2->getSatuan() . "', volume = " . $rincian_detail2->getVolume() . ", nilai_anggaran = " . $rincian_detail2->getNilaiAnggaran() . ", komponen_name = '" . $rincian_detail2->getKomponenName() . "'  "
                            . "where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and detail_no = $detail_no and tahun = '" . sfConfig::get('app_tahun_default') . "'";
                    $stmt = $con->prepareStatement($query);
                    $stmt->executeQuery();
                }
            }
            //irul 10 desember 2014 - edit data gmap

            if ($tipe2 == 'KONSTRUKSI' || $data_rincian_detail->getTipe() == 'FISIK' || $est_fisik) {
                $rd_cari = new Criteria();
                $rd_cari->add(DinasRincianDetailPeer::UNIT_ID, $unit_id, Criteria::EQUAL);
                $rd_cari->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kegiatan_code, Criteria::EQUAL);
                $rd_cari->add(DinasRincianDetailPeer::DETAIL_NO, $detail_no, Criteria::EQUAL);
                $rd_dapat = DinasRincianDetailPeer::doSelectOne($rd_cari);

                //                                kalo waitinglist dinolin
                $ada_waitinglist = 0;
                $rd_cari_waitinglist = new Criteria();
                $rd_cari_waitinglist->add(WaitingListPUPeer::KODE_RKA, $unit_id . '.' . $kegiatan_code . '.' . $detail_no, Criteria::EQUAL);
                $rd_cari_waitinglist->addAnd(WaitingListPUPeer::STATUS_WAITING, 1);
                $rd_cari_waitinglist->addAnd(WaitingListPUPeer::STATUS_HAPUS, FALSE);

                $ada_waitinglist = WaitingListPUPeer::doCount($rd_cari_waitinglist);

                if ($ada_waitinglist > 0 && ($unit_id == '2600' || $unit_id == '2300') && $rd_dapat->getNilaiAnggaran() == 0) {
                    $rd_dapat_waitinglist = WaitingListPUPeer::doSelectOne($rd_cari_waitinglist);

                    $total_aktif = 0;
                    $query = "select count(*) as total "
                            . "from " . sfConfig::get('app_default_schema') . ".waitinglist_pu "
                            . "where status_hapus = false and status_waiting = 0 "
                            . "and unit_id = 'XXX$unit_id' and kegiatan_code = '" . $kegiatan_code . "'";
                    $stmt = $con->prepareStatement($query);
                    $rs = $stmt->executeQuery();
                    while ($rs->next()) {
                        $total_aktif = $rs->getString('total');
                    }
                    $total_aktif++;

                    $balik_waitinglist = new WaitingListPU();
                    $balik_waitinglist->setUnitId('XXX' . $unit_id);
                    $balik_waitinglist->setKegiatanCode($kegiatan_code);
                    $balik_waitinglist->setSubtitle($rd_dapat->getSubtitle());
                    $balik_waitinglist->setKomponenId($rd_dapat->getKomponenId());
                    $balik_waitinglist->setKomponenName($rd_dapat->getKomponenName());
                    $balik_waitinglist->setKomponenLokasi($rd_dapat->getDetailName());
                    $balik_waitinglist->setKomponenHargaAwal($data_lama_komponen_harga_awal);
                    $balik_waitinglist->setPajak($data_lama_pajak);
                    $balik_waitinglist->setKomponenSatuan($data_lama_satuan);
                    $balik_waitinglist->setKomponenRekening($rd_dapat->getRekeningCode());
                    $balik_waitinglist->setKoefisien($data_lama_keterangan_koefisien);
                    $balik_waitinglist->setVolume($data_lama_volume);
                    $balik_waitinglist->setTahunInput($rd_dapat->getTahun());
                    $balik_waitinglist->setCreatedAt($sekarang);
                    $balik_waitinglist->setUpdatedAt($sekarang);
                    $balik_waitinglist->setStatusHapus(FALSE);
                    $balik_waitinglist->setStatusWaiting(0);
                    $balik_waitinglist->setKodeJasmas($rd_dapat->getKecamatan());
                    $balik_waitinglist->setKecamatan($rd_dapat->getLokasiKecamatan());
                    $balik_waitinglist->setKelurahan($rd_dapat->getLokasiKelurahan());
                    $balik_waitinglist->setIsMusrenbang($rd_dapat->getIsMusrenbang());
                    $balik_waitinglist->setPrioritas($total_aktif);
                    $balik_waitinglist->setNilaiEe($rd_dapat_waitinglist->getNilaiEe());
                    $balik_waitinglist->setKeterangan($rd_dapat_waitinglist->getKeterangan());
                    $balik_waitinglist->save();

                    $id_baru = $balik_waitinglist->getIdWaiting();

                    $c_cari_history_lama = new Criteria();
                    $c_cari_history_lama->add(HistoryPekerjaanV2Peer::KODE_RKA, $unit_id . '.' . $kegiatan_code . '.' . $detail_no);
                    $c_cari_history_lama->addAnd(HistoryPekerjaanV2Peer::TAHUN, sfConfig::get('app_tahun_default'));
                    $dapat_history_lama = HistoryPekerjaanV2Peer::doSelect($c_cari_history_lama);
                    if ($dapat_history_lama) {
                        foreach ($dapat_history_lama as $value_history_lama) {
                            $c_insert_gis = new HistoryPekerjaanV2();
                            $c_insert_gis->setTahun(sfConfig::get('app_tahun_default'));
                            $c_insert_gis->setKodeRka('XXX' . $unit_id . '.' . $kegiatan_code . '.' . $id_baru);
                            $c_insert_gis->setStatusHapus(FALSE);
                            $c_insert_gis->setJalan($value_history_lama->getJalan());
                            $c_insert_gis->setGang($value_history_lama->getGang());
                            $c_insert_gis->setNomor($value_history_lama->getNomor());
                            $c_insert_gis->setRw($value_history_lama->getRw());
                            $c_insert_gis->setRt($value_history_lama->getRt());
                            $c_insert_gis->setKeterangan($value_history_lama->getKeterangan());
                            $c_insert_gis->setTempat($value_history_lama->getTempat());
                            $c_insert_gis->setKomponen($value_history_lama->getKomponen());
                            $c_insert_gis->setKecamatan($value_history_lama->getKecamatan());
                            $c_insert_gis->setKelurahan($value_history_lama->getKelurahan());
                            $c_insert_gis->setLokasi($value_history_lama->getLokasi());
                            $c_insert_gis->save();
                        }
                    }
                }
//                                kalo waitinglist dinolin
            }

            return $this->redirect('kegiatan/editRevisi?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
        }
    }

    public function executeGetPekerjaans() {
        if ($this->getRequestParameter('id')) {
            $sub_id = $this->getRequestParameter('id');
            $c = new Criteria();
            $c->add(SubtitleIndikatorPeer::SUB_ID, $sub_id);
            $rs_subtitle = SubtitleIndikatorPeer::doSelectOne($c);
            if ($rs_subtitle) {
                $unit_id = $rs_subtitle->getUnitId();
                $kegiatan_code = $rs_subtitle->getKegiatanCode();
                $subtitle = $rs_subtitle->getSubtitle();
                $nama_subtitle = trim($subtitle);
            }

            $query = "select *
                                  from " . sfConfig::get('app_default_schema') . ".rincian_detail
				  where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and subtitle ilike '$nama_subtitle' and status_hapus=false order by sub,rekening_code,komponen_name";

            $con = Propel::getConnection(RincianDetailPeer::DATABASE_NAME);
            $statement = $con->prepareStatement($query);
            $rs_rinciandetail = $statement->executeQuery();
            $this->rs_rinciandetail = $rs_rinciandetail;

            $c_rincianDetail = new Criteria();
            $c_rincianDetail->add(RincianDetailPeer::UNIT_ID, $unit_id);
            $c_rincianDetail->add(RincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
            $c_rincianDetail->add(RincianDetailPeer::SUBTITLE, $nama_subtitle, Criteria::ILIKE);
            $c_rincianDetail->add(RincianDetailPeer::STATUS_HAPUS, false);
            $c_rincianDetail->add(RincianDetailPeer::TAHUN, sfConfig::get('app_tahun_default'));
            $c_rincianDetail->addAscendingOrderByColumn(RincianDetailPeer::KODE_SUB);
            $c_rincianDetail->addAscendingOrderByColumn(RincianDetailPeer::REKENING_CODE);
            $c_rincianDetail->addDescendingOrderByColumn(RincianDetailPeer::NILAI_ANGGARAN);
            $c_rincianDetail->addAscendingOrderByColumn(RincianDetailPeer::KOMPONEN_NAME);
            $rs_rd = RincianDetailPeer::doSelect($c_rincianDetail);
            $this->rs_rd = $rs_rd;

            $this->id = $sub_id;
            $this->rinciandetail = 'ada';
            $this->setLayout('kosong');
        }
    }

    public function executeGetPekerjaansMasalah() {
        if ($this->getRequestParameter('id') == '0') {
            $sub_id = $this->getRequestParameter('id');
            $kegiatan_code = $this->getRequestParameter('kegiatan');
            $unit_id = $this->getRequestParameter('unit');

            $query = "select * 
					  from " . sfConfig::get('app_default_schema') . ".rincian_detail_masalah
					  where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' order by sub,kode_sub,rekening_code,komponen_name";
            //print($query);exit;
            $con = Propel::getConnection();
            $statement = $con->prepareStatement($query);
            $rs_rinciandetail = $statement->executeQuery();

            $this->rs_rinciandetail = $rs_rinciandetail;
            $this->id = $sub_id;
            $this->rinciandetail = 'ada';
            $this->setLayout('kosong');
        }
    }

    public function executeEdit() {
        if ($this->getRequestParameter('unit_id')) {
            $unit_id = $this->getRequestParameter('unit_id');
            $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');

            $c = new Criteria();
            $c->add(SubtitleIndikatorPeer::UNIT_ID, $unit_id);
            $c->add(SubtitleIndikatorPeer::KEGIATAN_CODE, $kode_kegiatan);
            $c->add(SubtitleIndikatorPeer::SUBTITLE, 'Penunjang Kinerja%', Criteria::NOT_ILIKE);
            $c->addAscendingOrderByColumn(SubtitleIndikatorPeer::SUBTITLE);
            $rs_subtitle = SubtitleIndikatorPeer::doSelect($c);

            $this->rs_subtitle = $rs_subtitle;
            $this->rinciandetail = '';
            $this->rs_rinciandetail = '';

            //untuk warning
            $query = "select sum(nilai_anggaran) as tot
					  from " . sfConfig::get('app_default_schema') . ".rincian_detail
					  where status_hapus=FALSE and unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rekening_code like '5.2.1%' and status_hapus=false";
            $con = Propel::getConnection();
            $statement = $con->prepareStatement($query);
            $rs = $statement->executeQuery();
            while ($rs->next())
                $total_menjadi_1 = $rs->getString('tot');

            $query = "select sum(nilai_anggaran) as tot
					  from " . sfConfig::get('app_default_schema') . ".rincian_detail
					  where status_hapus=FALSE and unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rekening_code like '5.2.2%' and status_hapus=false";
            $con = Propel::getConnection();
            $statement = $con->prepareStatement($query);
            $rs = $statement->executeQuery();
            while ($rs->next())
                $total_menjadi_2 = $rs->getString('tot');

            $query = "select sum(nilai_anggaran) as tot
					  from " . sfConfig::get('app_default_schema') . ".rincian_detail
					  where status_hapus=FALSE and unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rekening_code like '5.2.3%' and status_hapus=false";
            $con = Propel::getConnection();
            $statement = $con->prepareStatement($query);
            $rs = $statement->executeQuery();
            while ($rs->next())
                $total_menjadi_3 = $rs->getString('tot');

            $query = "select sum(nilai_anggaran) as tot
                              from " . sfConfig::get('app_default_schema') . ".prev_rincian_detail
                              where status_hapus=FALSE and unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rekening_code like '5.2.1%' and status_hapus=false";

            $con = Propel::getConnection();
            $statement = $con->prepareStatement($query);
            $rs = $statement->executeQuery();
            while ($rs->next())
                $total_semula_1 = $rs->getString('tot');

            $query = "select sum(nilai_anggaran) as tot
                              from " . sfConfig::get('app_default_schema') . ".prev_rincian_detail
                              where status_hapus=FALSE and unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rekening_code like '5.2.2%' and status_hapus=false";

            $con = Propel::getConnection();
            $statement = $con->prepareStatement($query);
            $rs = $statement->executeQuery();
            while ($rs->next())
                $total_semula_2 = $rs->getString('tot');

            $query = "select sum(nilai_anggaran) as tot
                              from " . sfConfig::get('app_default_schema') . ".prev_rincian_detail
                              where status_hapus=FALSE and unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rekening_code like '5.2.3%' and status_hapus=false";

            $con = Propel::getConnection();
            $statement = $con->prepareStatement($query);
            $rs = $statement->executeQuery();
            while ($rs->next())
                $total_semula_3 = $rs->getString('tot');


            $this->selisih_1 = number_format($total_menjadi_1 - $total_semula_1, 0, ',', '.');
            $this->selisih_2 = number_format($total_menjadi_2 - $total_semula_2, 0, ',', '.');
            $this->selisih_3 = number_format($total_menjadi_3 - $total_semula_3, 0, ',', '.');

            $this->total_menjadi_1 = number_format($total_menjadi_1, 0, ',', '.');
            $this->total_menjadi_2 = number_format($total_menjadi_2, 0, ',', '.');
            $this->total_menjadi_3 = number_format($total_menjadi_3, 0, ',', '.');

            $this->total_semula_1 = number_format($total_semula_1, 0, ',', '.');
            $this->total_semula_2 = number_format($total_semula_2, 0, ',', '.');
            $this->total_semula_3 = number_format($total_semula_3, 0, ',', '.');
        }
    }

    public function executeTarikmurni() {
        if ($this->getRequestParameter('unit_id')) {
            $unit_id = $this->getRequestParameter('unit_id');
            $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');

            $con = Propel::getConnection();
            $con->begin();
            try {
                $sql = "delete from " . sfConfig::get('app_default_schema') . ".murni_master_kegiatan where unit_id='" . $unit_id . "' and kode_kegiatan='$kode_kegiatan' and tahun='" . sfConfig::get('app_tahun_default') . "'";
                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();

                $sql = "delete from " . sfConfig::get('app_default_schema') . ".murni_subtitle_indikator where unit_id='" . $unit_id . "' and kegiatan_code='$kode_kegiatan' and tahun='" . sfConfig::get('app_tahun_default') . "'";
                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();

                $sql = "delete from " . sfConfig::get('app_default_schema') . ".murni_rincian_detail where unit_id='" . $unit_id . "' and kegiatan_code='$kode_kegiatan' and tahun='" . sfConfig::get('app_tahun_default') . "'";
                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();

                $sql = "delete from " . sfConfig::get('app_default_schema') . ".murni_rincian where unit_id='" . $unit_id . "' and kegiatan_code='$kode_kegiatan' and tahun='" . sfConfig::get('app_tahun_default') . "'";
                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();

                $sql = "insert into " . sfConfig::get('app_default_schema') . ".murni_master_kegiatan select * from " . sfConfig::get('app_default_schema') . ".master_kegiatan where unit_id='" . $unit_id . "' and kode_kegiatan='$kode_kegiatan' and tahun='" . sfConfig::get('app_tahun_default') . "'";
                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();

                $sql = "insert into " . sfConfig::get('app_default_schema') . ".murni_subtitle_indikator select * from " . sfConfig::get('app_default_schema') . ".subtitle_indikator where unit_id='" . $unit_id . "' and kegiatan_code='$kode_kegiatan' and tahun='" . sfConfig::get('app_tahun_default') . "'";
                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();

                $sql = "insert into " . sfConfig::get('app_default_schema') . ".murni_rincian_detail select * from " . sfConfig::get('app_default_schema') . ".rincian_detail where unit_id='" . $unit_id . "' and kegiatan_code='$kode_kegiatan' and tahun='" . sfConfig::get('app_tahun_default') . "'";
                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();

                $sql = "insert into " . sfConfig::get('app_default_schema') . ".murni_rincian select * from " . sfConfig::get('app_default_schema') . ".rincian where unit_id='" . $unit_id . "' and kegiatan_code='$kode_kegiatan' and tahun='" . sfConfig::get('app_tahun_default') . "'";
                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();

                $this->setFlash('berhasil', 'Kegiatan Telah Berhasil ditarik');

                $con->commit();
                $this->setFlash('berhasil', 'Kegiatan Telah Berhasil disiapkan Untuk Dapat ditarik oleh e - Project');
            } catch (Exception $e) {
                $con->rollback();
                $this->setFlash('gagal', 'Kegiatan GAGAL ditarik ke murni karena ' . $e->getMessage());
            }
            $this->forward('kegiatan', 'list');
        }
    }

    public function executeTarikrka() {
        if ($this->getRequestParameter('tarik') == md5('project')) {
            $unit_id = $this->getRequestParameter('unit_id');
            $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
            $tahap = $this->filters['tahap'];
            $tahap2 = $this->getRequestParameter('tahap');
            if(empty($tahap) && sfConfig::get('app_tahap_edit') != 'murni' && sfConfig::get('app_tahap_edit') != 'pak') {
                if(!empty($tahap2)) $tahap = $tahap2;
                else {
                    $this->setFlash('gagal', 'Mohon pilih tahap terlebih dahulu. Kesalahan pemilihan tahap dapat mengakibatkan lolosnya validasi realisasi di e-Delivery');
                    return $this->redirect('kegiatan/list?filters[unit_id]=' . $unit_id . '&filter=cari');
                }
            } else if(sfConfig::get('app_tahap_edit') == 'murni') {
                $tahap = 'murni';
            } else if(sfConfig::get('app_tahap_edit') == 'pak') {
                $tahap = 'pak';
            }
            
            set_time_limit(0);
            $con = Propel::getConnection();
            $con->begin();
            try {
                $sql = "delete from " . sfConfig::get('app_default_schema') . ".master_kegiatan_bp where unit_id='" . $unit_id . "' and kode_kegiatan='$kode_kegiatan' and tahun='" . sfConfig::get('app_tahun_default') . "'";
                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();

                $sql = "delete from " . sfConfig::get('app_default_schema') . ".subtitle_indikator_bp where unit_id='" . $unit_id . "' and kegiatan_id='$kode_kegiatan' and tahun='" . sfConfig::get('app_tahun_default') . "'";
                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();

                $sql = "delete from " . sfConfig::get('app_default_schema') . ".rincian_detail_bp where unit_id='" . $unit_id . "' and kegiatan_code='$kode_kegiatan' and tahun='" . sfConfig::get('app_tahun_default') . "'";
                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();

                $sql = "delete from " . sfConfig::get('app_default_schema') . ".rincian_bp where unit_id='" . $unit_id . "' and kegiatan_code='$kode_kegiatan' and tahun='" . sfConfig::get('app_tahun_default') . "'";
                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();

                $sql = "delete from " . sfConfig::get('app_default_schema') . ".rincian_sub_parameter_bp where unit_id='" . $unit_id . "' and kegiatan_code='$kode_kegiatan' and tahun='" . sfConfig::get('app_tahun_default') . "'";
                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();

                $sql = "delete from " . sfConfig::get('app_default_schema') . ".rka_member_bp where unit_id='" . $unit_id . "' and kegiatan_code='$kode_kegiatan' and tahun='" . sfConfig::get('app_tahun_default') . "'";
                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();

                //irul 28 jan 2014 -- isi kolom last_update_time saat penarikan e-project
                $sql = "update " . sfConfig::get('app_default_schema') . ".rincian set last_update_time = '" . date('Y-m-d H:i:s') . "' where unit_id='" . $unit_id . "' and kegiatan_code='$kode_kegiatan' and tahun='" . sfConfig::get('app_tahun_default') . "'";
                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();

                $sql = "update " . sfConfig::get('app_default_schema') . ".dinas_rincian set last_update_time = '" . date('Y-m-d H:i:s') . "' where unit_id='" . $unit_id . "' and kegiatan_code='$kode_kegiatan' and tahun='" . sfConfig::get('app_tahun_default') . "'";
                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();

                //irul 28 jan 2014 -- isi kolom last_update_time saat penarikan e-project
                //irul 24jun 2014 -- isi kolom TAHAP pada rincian_detail
//                $mk = new Criteria();
//                $mk->add(MasterKegiatanPeer::UNIT_ID, $unit_id);
//                $mk->add(MasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
//                $mk->add(MasterKegiatanPeer::TAHUN, sfConfig::get('app_tahun_default'));
//                $maskeg = MasterKegiatanPeer::doSelectOne($mk);
//                if ($maskeg) {
//                    $sql = "update " . sfConfig::get('app_default_schema') . ".rincian_detail "
//                            . "set tahap = '" . $maskeg->getTahap() . "' where unit_id='" . $unit_id . "' and kegiatan_code='$kode_kegiatan' and tahun='" . sfConfig::get('app_tahun_default') . "' and note_skpd <> ''";
//                    $stmt = $con->prepareStatement($sql);
//                    $stmt->executeQuery();
//                }
                //irul 24jun 2014 -- isi kolom TAHAP pada rincian_detail
                $sql = "insert into " . sfConfig::get('app_default_schema') . ".master_kegiatan_bp select * from " . sfConfig::get('app_default_schema') . ".master_kegiatan where unit_id='" . $unit_id . "' and kode_kegiatan='$kode_kegiatan' and tahun='" . sfConfig::get('app_tahun_default') . "'";
                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();

                $sql = "insert into " . sfConfig::get('app_default_schema') . ".subtitle_indikator_bp select * from " . sfConfig::get('app_default_schema') . ".subtitle_indikator where unit_id='" . $unit_id . "' and kegiatan_id='$kode_kegiatan' and tahun='" . sfConfig::get('app_tahun_default') . "'";
                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();

                $sql = "insert into " . sfConfig::get('app_default_schema') . ".rka_member_bp select * from " . sfConfig::get('app_default_schema') . ".rka_member where unit_id='" . $unit_id . "' and kegiatan_code='$kode_kegiatan' and tahun='" . sfConfig::get('app_tahun_default') . "'";
                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();

                $sql = "insert into " . sfConfig::get('app_default_schema') . ".rincian_bp select * from " . sfConfig::get('app_default_schema') . ".rincian where unit_id='" . $unit_id . "' and kegiatan_code='$kode_kegiatan' and tahun='" . sfConfig::get('app_tahun_default') . "'";
                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();

                $sql = "insert into " . sfConfig::get('app_default_schema') . ".rincian_sub_parameter_bp select * from " . sfConfig::get('app_default_schema') . ".rincian_sub_parameter where unit_id='" . $unit_id . "' and kegiatan_code='$kode_kegiatan' and tahun='" . sfConfig::get('app_tahun_default') . "'";
                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();

                try {
                    $sql = "insert into " . sfConfig::get('app_default_schema') . ".rincian_detail_bp "
                            . "select * from " . sfConfig::get('app_default_schema') . ".rincian_detail where unit_id='" . $unit_id . "' and kegiatan_code='$kode_kegiatan' and tahun='" . sfConfig::get('app_tahun_default') . "'";
                    $stmt = $con->prepareStatement($sql);
                    $stmt->executeQuery();
                } catch (Exception $exc) {
                    $con->rollback();
                    // echo $exc->getMessage();
                    // exit();
                    $this->setFlash('gagal', 'Kegiatan GAGAL disiapkan Untuk Dapat ditarik oleh e - Project karena : ' . $exc->getMessage());
                    return $this->redirect('kegiatan/list?unit_id=' . $unit_id);
                    // die;
                }

                $tahap_angka = DinasMasterKegiatanPeer::ubahTahapAngka($tahap);
                $sql =
                "UPDATE " . sfConfig::get('app_default_schema') . ".log_perubahan_revisi
                SET status = 1
                WHERE unit_id = '$unit_id'
                AND kegiatan_code = '$kode_kegiatan'
                AND tahap = $tahap_angka
                AND status = 0";
                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();

                $con->commit();
                $this->setFlash('berhasil', 'Kegiatan ' . $kode_kegiatan . ' Telah Berhasil disiapkan Untuk Dapat ditarik oleh e - Project');
                budgetLogger::log('Menarik RKA dinas ' . $unit_id . ' kegiatan : ' . $kode_kegiatan . ' ke e - Project');
                return $this->redirect('kegiatan/list?unit_id=' . $unit_id);
            } catch (Exception $e) {
                $con->rollback();
                // echo $e->getMessage();exit();
                // $this->setFlash('gagal', 'Kegiatan GAGAL disiapkan Untuk Dapat ditarik oleh e - Project karena ' . $e->getMessage());
                $this->setFlash('gagal', 'Kegiatan GAGAL disiapkan Untuk Dapat ditarik oleh e - Project karena - ' . $e->getMessage());
                return $this->redirect('kegiatan/list?unit_id=' . $unit_id);
            }
            // return $this->redirect('kegiatan/list?unit_id=' . $unit_id);
        }
    }

    public function executeBukarka() {
        if ($this->getRequestParameter('buka') == md5('dinas')) {
            $unit_id = $this->getRequestParameter('unit_id');
            $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');

            $sql = "update " . sfConfig::get('app_default_schema') . ".rincian "
                    . "set rincian_level='2' "
                    . "where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and tahun='" . sfConfig::get('app_tahun_default') . "'";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($sql);
            $stmt->executeQuery();

            $this->setFlash('berhasil', 'Kegiatan ' . $kode_kegiatan . ' Telah Berhasil Dibuka');


            //historyUserLog::buka_kegiatan_ke_dinas($unit_id, $kode_kegiatan);
            budgetLogger::log('Membuka RKA Kegiatan ' . $kode_kegiatan . ' ke Posisi Dinas');

            return $this->redirect('kegiatan/list?unit_id=' . $unit_id);
        }
    }

    public function executeKuncirka() {
        if ($this->getRequestParameter('kunci') == md5('dinas')) {
            $unit_id = $this->getRequestParameter('unit_id');
            $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');

            $sql = "update " . sfConfig::get('app_default_schema') . ".rincian "
                    . "set rincian_level='3' "
                    . "where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and tahun='" . sfConfig::get('app_tahun_default') . "'";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($sql);
            $stmt->executeQuery();

            $this->setFlash('berhasil', 'Kegiatan ' . $kode_kegiatan . ' Telah Berhasil Dikunci');

            //historyUserLog::tutup_kegiatan_ke_penyelia($unit_id, $kode_kegiatan);
            budgetLogger::log('Mengunci RKA Kegiatan ' . $kode_kegiatan . ' ke Posisi Peneliti');

            return $this->redirect('kegiatan/list?unit_id=' . $unit_id);
        }
    }

    public function executeBukarevisi() {
        if ($this->getRequestParameter('buka') == md5('dinas')) {
            $unit_id = $this->getRequestParameter('unit_id');
            $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');

            $dinas = false;
            // kunci ke bappeko untuk isi kuisioner
            // $c = new Criteria();
            // $c->add(RincianBappekoPeer::UNIT_ID, $unit_id);
            // $c->add(RincianBappekoPeer::KODE_KEGIATAN, $kode_kegiatan);
            // $rincian_b = RincianBappekoPeer::doSelectOne($c);

            // if ($rincian_b) {
            //     $sql = "update " . sfConfig::get('app_default_schema') . ".dinas_rincian "
            //             . "set rincian_level='2' "
            //             . "where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and tahun='" . sfConfig::get('app_tahun_default') . "'";
            //     $dinas = true;
            // } else {
            //     $sql = "update " . sfConfig::get('app_default_schema') . ".dinas_rincian "
            //             . "set rincian_level='1' "
            //             . "where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and tahun='" . sfConfig::get('app_tahun_default') . "'";
            // }

            $sql = "update " . sfConfig::get('app_default_schema') . ".dinas_rincian "
                    . "set rincian_level='2' "
                    . "where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and tahun='" . sfConfig::get('app_tahun_default') . "'";
            $dinas = true;
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($sql);
            $stmt->executeQuery();

            $sql =
            "DELETE FROM " . sfConfig::get('app_default_schema') . ".buka_komponen_murni
            WHERE detail_kegiatan ILIKE '%$kode_kegiatan%'";
            $dinas = true;
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($sql);
            $stmt->executeQuery();

            if ($dinas) {
                $this->setFlash('berhasil', 'Kegiatan ' . $kode_kegiatan . ' Telah Berhasil Dibuka posisi Dinas');
                budgetLogger::log('Membuka Revisi Dinas ' . $unit_id . ' Kegiatan ' . $kode_kegiatan . ' ke Posisi Dinas');
            } else {
                $this->setFlash('berhasil', 'Kegiatan ' . $kode_kegiatan . ' Telah Berhasil Dibuka posisi Bappeko');
                budgetLogger::log('Membuka Revisi Dinas ' . $unit_id . ' Kegiatan ' . $kode_kegiatan . ' ke Posisi Bappeko');
            }

            //historyUserLog::buka_kegiatan_ke_dinas($unit_id, $kode_kegiatan);

            return $this->redirect('kegiatan/listRevisi?unit_id=' . $unit_id);
        }
    }

    public function executeKuncirevisi() {
        if ($this->getRequestParameter('kunci') == md5('dinas')) {
            $unit_id = $this->getRequestParameter('unit_id');
            $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');

            $sql = "update " . sfConfig::get('app_default_schema') . ".dinas_rincian "
                    . "set rincian_level='3' "
                    . "where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and tahun='" . sfConfig::get('app_tahun_default') . "'";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($sql);
            $stmt->executeQuery();

            $this->setFlash('berhasil', 'Kegiatan ' . $kode_kegiatan . ' Telah Berhasil Dikunci posisi Peneliti');

            //historyUserLog::tutup_kegiatan_ke_penyelia($unit_id, $kode_kegiatan);
            budgetLogger::log('Mengunci Revisi Dinas ' . $unit_id . ' Kegiatan ' . $kode_kegiatan . ' ke Posisi Peneliti');

            return $this->redirect('kegiatan/listRevisi?unit_id=' . $unit_id);
        }
    }

    public function executeBukajk() {
        if ($this->getRequestParameter('buka') == md5('dinas')) {
            $unit_id = $this->getRequestParameter('unit_id');
            $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');

            $sql = "update " . sfConfig::get('app_default_schema') . ".dinas_rincian "
                    . "set rincian_level='1' "
                    . "where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and tahun='" . sfConfig::get('app_tahun_default') . "'";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($sql);
            $stmt->executeQuery();

            $this->setFlash('berhasil', 'Kegiatan ' . $kode_kegiatan . ' Telah Berhasil Dibuka JK');

            //historyUserLog::tutup_kegiatan_ke_penyelia($unit_id, $kode_kegiatan);
            budgetLogger::log('Membuka Rekening JK Dinas ' . $unit_id . ' Kegiatan ' . $kode_kegiatan);

            return $this->redirect('kegiatan/listRevisi?unit_id=' . $unit_id);
        }
    }

    public function executeKuncijk() {
        if ($this->getRequestParameter('kunci') == md5('dinas')) {
            $unit_id = $this->getRequestParameter('unit_id');
            $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');

            $sql = "update " . sfConfig::get('app_default_schema') . ".dinas_rincian "
                    . "set rincian_level='3' "
                    . "where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and tahun='" . sfConfig::get('app_tahun_default') . "'";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($sql);
            $stmt->executeQuery();

            $this->setFlash('berhasil', 'Kegiatan ' . $kode_kegiatan . ' Telah Berhasil Dikunci JK');

            //historyUserLog::tutup_kegiatan_ke_penyelia($unit_id, $kode_kegiatan);
            budgetLogger::log('Mengunci Rekening JK Dinas ' . $unit_id . ' Kegiatan ' . $kode_kegiatan);

            return $this->redirect('kegiatan/listRevisi?unit_id=' . $unit_id);
        }
    }

    public function executeList() {

        $this->processSort();

        $this->processFilters();

        $this->filters = $this->getUser()->getAttributeHolder()->getAll('sf_admin/master_kegiatan/filters');

        if ($this->getRequestParameter('kunci') == 'kunci') {
            $this->prosesKunci();
        }

        if ($this->getRequestParameter('kunciall') == 'kunci semua') {
            $this->prosesKunciSemua();
        }

        if ($this->getRequestParameter('bukaall') == 'buka semua') {
            $this->prosesBukaSemua();
        }

        if ($this->getRequestParameter('buka') == 'buka kunci') {
            $this->prosesBukakunci();
        }
// awal-comment		if($this->getRequestParameter('tarikrkaperdinas')=='tarik ke e-Project')
//		{
//			$this->prosesTarikrkaperdinas();
// awal-comment		}
        //irul 27 jan 2014
        if ($this->getRequestParameter('tarikrkaperdinas') == 'tarik e-Project per dinas') {
            $this->prosesTarikrkaperdinas();
        }
        if ($this->getRequestParameter('tarikrkasemuadinas') == 'tarik e-Project semua dinas') {
            $this->prosesTarikrkasemuadinas();
        }
        //irul 27 jan 2014
        //irul 11 feb 2014
        if ($this->getRequestParameter('kuncikomponen') == 'kunci komponen') {
            $this->prosesKunciKomponenPerDinas();
        }
        if ($this->getRequestParameter('bukakomponen') == 'buka komponen') {
            $this->prosesBukaKomponenPerDinas();
        }
        //irul 11 feb 2014
        // pager
        $this->pager = new sfPropelPager('MasterKegiatan', 20);
        $c = new Criteria();
        $this->addSortCriteria($c);
        $c->addAscendingOrderByColumn(MasterKegiatanPeer::KODE_KEGIATAN);
        $unit_id = $this->getRequestParameter('unit_id');
        if (isset($unit_id)) {
            $c->add(MasterKegiatanPeer::UNIT_ID, $unit_id);
            $c->addAscendingOrderByColumn(MasterKegiatanPeer::KEGIATAN_ID);
        }
        $this->addFiltersCriteria($c);

        if (isset($this->filters['unit_id'])) {
            $namaPenyelia = '';
            $penyelia = new Criteria();
            $penyelia->addAsColumn('userId', UserHandleV2Peer::USER_ID);
            $penyelia->add(UserHandleV2Peer::UNIT_ID, $this->filters['unit_id']);
            $rs_handle = UserHandleV2Peer::doSelect($penyelia);
            foreach ($rs_handle as $v) {
                $nama_peneliti = $v->getUserId();
                if ($nama_peneliti) {
                    $b = new Criteria();
                    $b->add(SchemaAksesV2Peer::USER_ID, $nama_peneliti);
                    $bs = SchemaAksesV2Peer::doSelectOne($b);
                    if ($bs) {
                        if (($bs->getLevelId() == '2')) {
                            $b = new Criteria();
                            $b->add(MasterPenyeliaPeer::USERNAME, $nama_peneliti);
                            $bss = MasterPenyeliaPeer::doSelectOne($b);
                            if ($bss) {
                                $namaPenyelia = $bss->getUsername();
                            }
                        }
                    }
                }
            }
            $this->namaPenyelia = $namaPenyelia;
        }


        $this->pager->setCriteria($c);
        $this->pager->setPage($this->getRequestParameter('page', 1));
        $this->pager->init();
    }

    protected function prosesKunciSemua() {

        if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
            $unit = $this->filters['unit_id'];
            $unit_id = $unit;
            $sql = "update " . sfConfig::get('app_default_schema') . ".rincian 
                set lock=TRUE, rincian_confirmed = TRUE, rincian_selesai = TRUE 
                where unit_id='$unit_id' and tahun='" . sfConfig::get('app_tahun_default') . "'";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($sql);
            $stmt->executeQuery();

            $sql =
            "DELETE FROM " . sfConfig::get('app_default_schema') . ".buka_komponen_murni
            WHERE detail_kegiatan ILIKE '$unit_id%'";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($sql);
            $stmt->executeQuery();
        } else {
            $sql = "update " . sfConfig::get('app_default_schema') . ".rincian 
                set lock=TRUE, rincian_confirmed = TRUE, rincian_selesai = TRUE 
                where unit_id='$unit_id' and tahun='" . sfConfig::get('app_tahun_default') . "'";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($sql);
            $stmt->executeQuery();

            $sql =
            "DELETE FROM " . sfConfig::get('app_default_schema') . ".buka_komponen_murni
            WHERE detail_kegiatan ILIKE '$unit_id%'";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($sql);
            $stmt->executeQuery();
        }
    }

    protected function prosesBukaSemua() {

        if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
            $unit = $this->filters['unit_id'];
            $unit_id = $unit;
            $sql = "update " . sfConfig::get('app_default_schema') . ".rincian 
                set lock=FALSE, rincian_confirmed = FALSE, rincian_selesai = FALSE 
                where unit_id='$unit_id' and tahun='" . sfConfig::get('app_tahun_default') . "' ";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($sql);
            $stmt->executeQuery();
        } else {
            $sql = "update " . sfConfig::get('app_default_schema') . ".rincian 
                set lock=FALSE, rincian_confirmed = FALSE, rincian_selesai = FALSE 
                where unit_id='$unit_id' and tahun='" . sfConfig::get('app_tahun_default') . "' ";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($sql);
            $stmt->executeQuery();
        }
    }

    protected function prosesKunci() {

        if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
            $unit = $this->filters['unit_id'];
            $unit_id = $unit;

            $sql = "update " . sfConfig::get('app_default_schema') . ".rincian "
                    . "set rincian_level='3' "
                    . "where unit_id='$unit_id' and tahun='" . sfConfig::get('app_tahun_default') . "'";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($sql);
            $stmt->executeQuery();

            $c_cari_kegiatan = new Criteria();
            $c_cari_kegiatan->add(MasterKegiatanPeer::UNIT_ID, $unit_id);
            $dapat_kegiatan = MasterKegiatanPeer::doSelect($c_cari_kegiatan);
            if ($dapat_kegiatan) {
                foreach ($dapat_kegiatan as $value) {
                    historyUserLog::tutup_kegiatan_ke_penyelia($unit_id, $value->getKodeKegiatan());
                }
            }
            $this->setFlash('berhasil', 'Kegiatan  pada SKPD' . $unit_id . ' berhasil dikunci pada posisi Penyelia');
            return $this->redirect('kegiatan/list?unit_id=' . $unit_id);
        }
    }

    protected function prosesBukakunci() {
        if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
            $unit = $this->filters['unit_id'];
            $unit_id = $unit;

            $sql = "update " . sfConfig::get('app_default_schema') . ".rincian "
                    . "set rincian_level=2 "
                    . "where unit_id='$unit' and tahun='" . sfConfig::get('app_tahun_default') . "'";
            //print_r($sql);exit;
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($sql);
            $stmt->executeQuery();

            $sql =
            "DELETE FROM " . sfConfig::get('app_default_schema') . ".buka_komponen_murni
            WHERE detail_kegiatan ILIKE '$unit%'";
            $dinas = true;
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($sql);
            $stmt->executeQuery();

            $c_cari_kegiatan = new Criteria();
            $c_cari_kegiatan->add(MasterKegiatanPeer::UNIT_ID, $unit_id);
            $dapat_kegiatan = MasterKegiatanPeer::doSelect($c_cari_kegiatan);
            if ($dapat_kegiatan) {
                foreach ($dapat_kegiatan as $value) {
                    historyUserLog::buka_kegiatan_ke_dinas($unit_id, $value->getKodeKegiatan());
                }
            }
            $this->setFlash('berhasil', 'Kegiatan pada SKPD' . $unit_id . ' berhasil dibuka pada posisi Dinas');
            return $this->redirect('kegiatan/list?unit_id=' . $unit_id);
        }
    }

    protected function prosesKunciRevisi() {
        if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
            $unit = $this->filters['unit_id'];
            $unit_id = $unit;

            $sql = "update " . sfConfig::get('app_default_schema') . ".dinas_rincian "
                    . "set rincian_level='3' "
                    . "where unit_id='$unit_id' and tahun='" . sfConfig::get('app_tahun_default') . "'";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($sql);
            $stmt->executeQuery();

            $c_cari_kegiatan = new Criteria();
            $c_cari_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
            $dapat_kegiatan = DinasMasterKegiatanPeer::doSelect($c_cari_kegiatan);
            if ($dapat_kegiatan) {
                foreach ($dapat_kegiatan as $value) {
                    historyUserLog::tutup_kegiatan_ke_penyelia($unit_id, $value->getKodeKegiatan());
                }
            }
            $this->setFlash('berhasil', 'Kegiatan  pada SKPD' . $unit_id . ' berhasil dikunci pada posisi Penyelia');
            return $this->redirect('kegiatan/listRevisi?unit_id=' . $unit_id);
        }
    }

    protected function prosesBukakunciRevisi() {
        if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
            $unit = $this->filters['unit_id'];
            $unit_id = $unit;

            $c_cari_rincian = new Criteria();
            $c_cari_rincian->add(DinasRincianPeer::UNIT_ID, $unit_id);
            $rs_rincian = DinasRincianPeer::doSelect($c_cari_rincian);
            foreach ($rs_rincian as $rincian) {
                $kode_kegiatan = $rincian->getKegiatanCode();

                // untuk cek posisi kunci bappeko dengan isi kuisioner
                // $c = new Criteria();
                // $c->add(RincianBappekoPeer::UNIT_ID, $unit_id);
                // $c->add(RincianBappekoPeer::KODE_KEGIATAN, $kode_kegiatan);
                // if ($rincian_b = RincianBappekoPeer::doSelectOne($c)) {
                //     $sql = "update " . sfConfig::get('app_default_schema') . ".dinas_rincian "
                //             . "set rincian_level=2 where unit_id='$unit' and kegiatan_code='$kode_kegiatan' and tahun='" . sfConfig::get('app_tahun_default') . "'";
                // } else {
                //     $sql = "update " . sfConfig::get('app_default_schema') . ".dinas_rincian "
                //             . "set rincian_level=1 where unit_id='$unit' and kegiatan_code='$kode_kegiatan' and tahun='" . sfConfig::get('app_tahun_default') . "'";
                // }
                //print_r($sql);exit;
                $sql = "update " . sfConfig::get('app_default_schema') . ".dinas_rincian "
                        . "set rincian_level=2 where unit_id='$unit' and kegiatan_code='$kode_kegiatan' and tahun='" . sfConfig::get('app_tahun_default') . "'";
                $con = Propel::getConnection();
                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();

                historyUserLog::buka_kegiatan_ke_dinas($unit_id, $kode_kegiatan);
            }
//            $c_cari_kegiatan = new Criteria();
//            $c_cari_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
//            $dapat_kegiatan = DinasMasterKegiatanPeer::doSelect($c_cari_kegiatan);
//            if ($dapat_kegiatan) {
//                foreach ($dapat_kegiatan as $value) {
//                    historyUserLog::buka_kegiatan_ke_dinas($unit_id, $value->getKodeKegiatan());
//                }
//            }

            $this->setFlash('berhasil', 'Kegiatan pada SKPD' . $unit_id . ' berhasil dibuka pada posisi Dinas');
            return $this->redirect('kegiatan/listRevisi?unit_id=' . $unit_id);
        }
    }

    protected function prosesKunciKomponenPerDinas() {

        if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
            $unit = $this->filters['unit_id'];
            $unit_id = $unit;

            $sql = "update " . sfConfig::get('app_default_schema') . ".rincian_detail  "
                    . "set lock_subtitle = 'LOCK' "
                    . "where unit_id='$unit_id' and tahun='" . sfConfig::get('app_tahun_default') . "' "
                    . "and status_hapus = false ";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($sql);
            $stmt->executeQuery();

            $c_cari_kegiatan = new Criteria();
            $c_cari_kegiatan->add(MasterKegiatanPeer::UNIT_ID, $unit_id);
            $dapat_kegiatan = MasterKegiatanPeer::doSelectOne($c_cari_kegiatan);
            if ($dapat_kegiatan) {
                foreach ($dapat_kegiatan as $value) {
                    historyUserLog::lock_komponen_kegiatan($unit_id, $value->getKodeKegiatan());
                }
            }
            $this->setFlash('berhasil', 'Komponen  pada SKPD ' . $unit_id . ' berhasil dikunci');
            return $this->redirect('kegiatan/list?unit_id=' . $unit_id);
        }
    }

    protected function prosesBukaKomponenPerDinas() {
        if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
            $unit = $this->filters['unit_id'];
            $unit_id = $unit;

            $sql = "update " . sfConfig::get('app_default_schema') . ".rincian_detail  "
                    . "set lock_subtitle = '' "
                    . "where unit_id='$unit_id' and tahun='" . sfConfig::get('app_tahun_default') . "' "
                    . "and rekening_code <> '5.2.1.04.01' and status_hapus = false ";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($sql);
            $stmt->executeQuery();

            $c_cari_kegiatan = new Criteria();
            $c_cari_kegiatan->add(MasterKegiatanPeer::UNIT_ID, $unit_id);
            $dapat_kegiatan = MasterKegiatanPeer::doSelectOne($c_cari_kegiatan);
            if ($dapat_kegiatan) {
                foreach ($dapat_kegiatan as $value) {
                    historyUserLog::unlock_komponen_kegiatan($unit_id, $value->getKodeKegiatan());
                }
            }
            $this->setFlash('berhasil', 'Komponen  pada SKPD ' . $unit_id . ' berhasil dibuka');
            return $this->redirect('kegiatan/list?unit_id=' . $unit_id);
        }
    }

    protected function prosesTarikrkaperdinas() {
        if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {

            $unit_id = $this->filters['unit_id'];
            $tahap = $this->filters['tahap'];
            if(empty($tahap) && sfConfig::get('app_tahap_edit') != 'murni' && sfConfig::get('app_tahap_edit') != 'pak') {
                $this->setFlash('gagal', 'Mohon pilih tahap terlebih dahulu. Kesalahan pemilihan tahap dapat mengakibatkan lolosnya validasi realisasi di e-Delivery');
                return $this->redirect('kegiatan/list?filters[unit_id]=' . $unit_id . '&filter=cari');
            } else if(sfConfig::get('app_tahap_edit') == 'murni') {
                $tahap = 'murni';
            } else if(sfConfig::get('app_tahap_edit') == 'pak') {
                $tahap = 'pak';
            }
            //$kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
            //print_r($unit_id);exit;
            //------------------------------------------------------------------------------------------------------------------

            $con = Propel::getConnection();
            $con->begin();
            try {
                $sql = "delete from " . sfConfig::get('app_default_schema') . ".master_kegiatan_bp where unit_id='" . $unit_id . "' and tahun='" . sfConfig::get('app_tahun_default') . "'";
                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();

                $sql = "delete from " . sfConfig::get('app_default_schema') . ".subtitle_indikator_bp where unit_id='" . $unit_id . "' and tahun='" . sfConfig::get('app_tahun_default') . "'";
                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();

                $sql = "delete from " . sfConfig::get('app_default_schema') . ".rincian_detail_bp where unit_id='" . $unit_id . "' and tahun='" . sfConfig::get('app_tahun_default') . "'";
                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();

                $sql = "delete from " . sfConfig::get('app_default_schema') . ".rincian_bp where unit_id='" . $unit_id . "' and tahun='" . sfConfig::get('app_tahun_default') . "'";
                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();

                $sql = "delete from " . sfConfig::get('app_default_schema') . ".rincian_sub_parameter_bp where unit_id='" . $unit_id . "' and tahun='" . sfConfig::get('app_tahun_default') . "'";
                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();

                $sql = "delete from " . sfConfig::get('app_default_schema') . ".rka_member_bp where unit_id='" . $unit_id . "' and tahun='" . sfConfig::get('app_tahun_default') . "'";
                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();

                //irul 28 jan 2014 -- isi kolom last_update_time saat penarikan e-project
                $sql = "update " . sfConfig::get('app_default_schema') . ".rincian set last_update_time = '" . date('Y-m-d H:i:s') . "' where unit_id='" . $unit_id . "' and tahun='" . sfConfig::get('app_tahun_default') . "'";
                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();

                $sql = "update " . sfConfig::get('app_default_schema') . ".dinas_rincian set last_update_time = '" . date('Y-m-d H:i:s') . "' where unit_id='" . $unit_id . "' and tahun='" . sfConfig::get('app_tahun_default') . "'";
                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();

                //irul 28 jan 2014 -- isi kolom last_update_time saat penarikan e-project
                //irul 24jun 2014 -- isi kolom TAHAP pada rincian_detail
//                $mk = new Criteria();
//                $mk->add(MasterKegiatanPeer::UNIT_ID, $unit_id);
//                $mk->add(MasterKegiatanPeer::TAHUN, sfConfig::get('app_tahun_default'));
//                $maskeg = MasterKegiatanPeer::doSelectOne($mk);
//                if ($maskeg) {
//                    $sql = "update " . sfConfig::get('app_default_schema') . ".rincian_detail "
//                            . "set tahap = '" . $maskeg->getTahap() . "' "
//                            . "where unit_id='" . $unit_id . "' and tahun='" . sfConfig::get('app_tahun_default') . "' and note_skpd <> '' ";
//                    $stmt = $con->prepareStatement($sql);
//                    $stmt->executeQuery();
//                }
                //irul 24jun 2014 -- isi kolom TAHAP pada rincian_detail
                $sql = "insert into " . sfConfig::get('app_default_schema') . ".master_kegiatan_bp select * from " . sfConfig::get('app_default_schema') . ".master_kegiatan where unit_id='" . $unit_id . "' and tahun='" . sfConfig::get('app_tahun_default') . "'";
                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();

                $sql = "insert into " . sfConfig::get('app_default_schema') . ".subtitle_indikator_bp select * from " . sfConfig::get('app_default_schema') . ".subtitle_indikator where unit_id='" . $unit_id . "' and tahun='" . sfConfig::get('app_tahun_default') . "'";
                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();

                $sql = "insert into " . sfConfig::get('app_default_schema') . ".rincian_bp select * from " . sfConfig::get('app_default_schema') . ".rincian where unit_id='" . $unit_id . "' and tahun='" . sfConfig::get('app_tahun_default') . "'";
                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();

                $sql = "insert into " . sfConfig::get('app_default_schema') . ".rincian_sub_parameter_bp select * from " . sfConfig::get('app_default_schema') . ".rincian_sub_parameter where unit_id='" . $unit_id . "' and tahun='" . sfConfig::get('app_tahun_default') . "'";
                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();

                $sql = "insert into " . sfConfig::get('app_default_schema') . ".rka_member_bp select * from " . sfConfig::get('app_default_schema') . ".rka_member where unit_id='" . $unit_id . "' and tahun='" . sfConfig::get('app_tahun_default') . "'";
                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();

                try {
                    $sql = "insert into " . sfConfig::get('app_default_schema') . ".rincian_detail_bp "
                            . "select * from " . sfConfig::get('app_default_schema') . ".rincian_detail "
                            . "where unit_id='" . $unit_id . "' and tahun='" . sfConfig::get('app_tahun_default') . "'";
                    $stmt = $con->prepareStatement($sql);
                    $stmt->executeQuery();
                } catch (Exception $exc) {
                    $con->rollback();
                    echo $exc->getMessage();
                    exit();
                }

                $tahap_angka = DinasMasterKegiatanPeer::ubahTahapAngka($tahap);
                $sql =
                "UPDATE " . sfConfig::get('app_default_schema') . ".log_perubahan_revisi
                SET status = 1
                WHERE unit_id = '$unit_id'
                AND tahap = $tahap_angka
                AND status = 0";
                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();

                $con->commit();
                $this->setFlash('berhasil', 'Kegiatan pada SKPD ' . $unit_id . ' Telah Berhasil disiapkan Untuk Dapat ditarik oleh e - Project');
                //$this->forward('kegiatan','list');
            } catch (Exception $e) {
                $con->rollback();
                $this->setFlash('gagal', 'Kegiatan GAGAL disiapkan Untuk Dapat ditarik oleh e - Project karena ' . $e->getMessage());
                //$this->forward('kegiatan','list');
            }
            return $this->redirect('kegiatan/list?filters[unit_id]=' . $unit_id . '&filter=cari');
        } else {
            echo 'DOR';
            exit();
        }
    }

    protected function prosesTarikrkasemuadinas() {
        // if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {

        //     $unit_id = $this->filters['unit_id'];
            //$kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
            //print_r($unit_id);exit;
            //------------------------------------------------------------------------------------------------------------------

            $tahap = $this->filters['tahap'];
            if(empty($tahap) && sfConfig::get('app_tahap_edit') != 'murni' && sfConfig::get('app_tahap_edit') != 'pak') {
                $this->setFlash('gagal', 'Mohon pilih tahap terlebih dahulu. Kesalahan pemilihan tahap dapat mengakibatkan lolosnya validasi realisasi di e-Delivery');
                return $this->redirect('kegiatan/list?filters[unit_id]=' . $unit_id . '&filter=cari');
            } else if(sfConfig::get('app_tahap_edit') == 'murni') {
                $tahap = 'murni';
            } else if(sfConfig::get('app_tahap_edit') == 'pak') {
                $tahap = 'pak';
            }
            
            $con = Propel::getConnection();
            $con->begin();
            try {
                $sql = "delete from " . sfConfig::get('app_default_schema') . ".master_kegiatan_bp where tahun='" . sfConfig::get('app_tahun_default') . "'";
                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();

                $sql = "delete from " . sfConfig::get('app_default_schema') . ".subtitle_indikator_bp where tahun='" . sfConfig::get('app_tahun_default') . "'";
                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();

                $sql = "delete from " . sfConfig::get('app_default_schema') . ".rincian_detail_bp where tahun='" . sfConfig::get('app_tahun_default') . "'";
                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();

                $sql = "delete from " . sfConfig::get('app_default_schema') . ".rincian_bp where tahun='" . sfConfig::get('app_tahun_default') . "'";
                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();

                $sql = "delete from " . sfConfig::get('app_default_schema') . ".rincian_sub_parameter_bp where tahun='" . sfConfig::get('app_tahun_default') . "'";
                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();

                $sql = "delete from " . sfConfig::get('app_default_schema') . ".rka_member_bp where tahun='" . sfConfig::get('app_tahun_default') . "'";
                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();

                //irul 28 jan 2014 -- isi kolom last_update_time saat penarikan e-project
                $sql = "update " . sfConfig::get('app_default_schema') . ".rincian set last_update_time = '" . date('Y-m-d H:i:s') . "' where tahun='" . sfConfig::get('app_tahun_default') . "'";
                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();

                $sql = "update " . sfConfig::get('app_default_schema') . ".dinas_rincian set last_update_time = '" . date('Y-m-d H:i:s') . "' where tahun='" . sfConfig::get('app_tahun_default') . "'";
                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();

                //irul 28 jan 2014 -- isi kolom last_update_time saat penarikan e-project
                //irul 24jun 2014 -- isi kolom TAHAP pada rincian_detail
//                $mk = new Criteria();
//                $mk->add(MasterKegiatanPeer::UNIT_ID, $unit_id);
//                $mk->add(MasterKegiatanPeer::TAHUN, sfConfig::get('app_tahun_default'));
//                $maskeg = MasterKegiatanPeer::doSelectOne($mk);
//                if ($maskeg) {
//                    $sql = "update " . sfConfig::get('app_default_schema') . ".rincian_detail "
//                            . "set tahap = '" . $maskeg->getTahap() . "' "
//                            . "where unit_id='" . $unit_id . "' and tahun='" . sfConfig::get('app_tahun_default') . "' and note_skpd <> '' ";
//                    $stmt = $con->prepareStatement($sql);
//                    $stmt->executeQuery();
//                }
                //irul 24jun 2014 -- isi kolom TAHAP pada rincian_detail
                $sql = "insert into " . sfConfig::get('app_default_schema') . ".master_kegiatan_bp select * from " . sfConfig::get('app_default_schema') . ".master_kegiatan where tahun='" . sfConfig::get('app_tahun_default') . "'";
                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();

                $sql = "insert into " . sfConfig::get('app_default_schema') . ".subtitle_indikator_bp select * from " . sfConfig::get('app_default_schema') . ".subtitle_indikator where tahun='" . sfConfig::get('app_tahun_default') . "'";
                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();

                $sql = "insert into " . sfConfig::get('app_default_schema') . ".rincian_bp select * from " . sfConfig::get('app_default_schema') . ".rincian where tahun='" . sfConfig::get('app_tahun_default') . "'";
                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();

                $sql = "insert into " . sfConfig::get('app_default_schema') . ".rincian_sub_parameter_bp select * from " . sfConfig::get('app_default_schema') . ".rincian_sub_parameter where tahun='" . sfConfig::get('app_tahun_default') . "'";
                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();

                $sql = "insert into " . sfConfig::get('app_default_schema') . ".rka_member_bp select * from " . sfConfig::get('app_default_schema') . ".rka_member where tahun='" . sfConfig::get('app_tahun_default') . "'";
                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();

                try {
                    $sql = "insert into " . sfConfig::get('app_default_schema') . ".rincian_detail_bp "
                            . "select * from " . sfConfig::get('app_default_schema') . ".rincian_detail "
                            . "where tahun='" . sfConfig::get('app_tahun_default') . "'";
                    $stmt = $con->prepareStatement($sql);
                    $stmt->executeQuery();
                } catch (Exception $exc) {
                    $con->rollback();
                    echo $exc->getMessage();
                    exit();
                }

                $tahap_angka = DinasMasterKegiatanPeer::ubahTahapAngka($tahap);
                $sql =
                "UPDATE " . sfConfig::get('app_default_schema') . ".log_perubahan_revisi
                SET status = 1
                WHERE tahap = $tahap_angka
                AND status = 0";
                $stmt = $con->prepareStatement($sql);
                $stmt->executeQuery();

                $con->commit();
                $this->setFlash('berhasil', 'Kegiatan pada semua SKPD Telah Berhasil disiapkan Untuk Dapat ditarik oleh e - Project');
                //$this->forward('kegiatan','list');
            } catch (Exception $e) {
                $con->rollback();
                $this->setFlash('gagal', 'Kegiatan GAGAL disiapkan Untuk Dapat ditarik oleh e - Project karena ' . $e->getMessage());
                //$this->forward('kegiatan','list');
            }
            return $this->redirect('kegiatan/list?filter=filter');
        // } else {
        //     echo 'DOR';
        //     exit();
        // }
    }

    protected function addFiltersCriteria($c) {
        //print_r($this->getRequestParameter('unit_name'));exit;

        if (isset($this->filters['kode_kegiatan_is_empty'])) {
            $criterion = $c->getNewCriterion(MasterKegiatanPeer::KODE_KEGIATAN, '');
            $criterion->addOr($c->getNewCriterion(MasterKegiatanPeer::KODE_KEGIATAN, null, Criteria::ISNULL));
            $c->add($criterion);
        } else if (isset($this->filters['kode_kegiatan']) && $this->filters['kode_kegiatan'] !== '') {
            $kode_kegiatan = $this->filters['kode_kegiatan'];
            $arr = explode('.', $kode_kegiatan);
            $jum_array = count($arr);
            //if($jum_array<2)
            //{
            $cton1 = $c->getNewCriterion(MasterKegiatanPeer::KODE_URUSAN, $kode_kegiatan . '%', Criteria::LIKE);
            $cton2 = $c->getNewCriterion(MasterKegiatanPeer::KODE_PROGRAM2, $kode_kegiatan . '%', Criteria::LIKE);
            $cton1->addOr($cton2);
            //$c->add($cton1);
            //}
            if ($jum_array == 2) {
                //$c->add(MasterKegiatanPeer::KODE_PROGRAM,$arr[1],Criteria::LIKE);
                $cton3 = $c->getNewCriterion(MasterKegiatanPeer::KODE_PROGRAM, $arr[1] . '%', Criteria::LIKE);
                $cton1->addAnd($cton3);
            }
            $c->add($cton1);
        }
        if (isset($this->filters['nama_kegiatan_is_empty'])) {
            $criterion = $c->getNewCriterion(MasterKegiatanPeer::NAMA_KEGIATAN, '');
            $criterion->addOr($c->getNewCriterion(MasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
            $c->add($criterion);
        } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
            $c->add(MasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
        }
        if (isset($this->filters['posisi_is_empty'])) {
            $criterion = $c->getNewCriterion(MasterKegiatanPeer::POSISI, '');
            $criterion->addOr($c->getNewCriterion(MasterKegiatanPeer::POSISI, null, Criteria::ISNULL));
            $c->add($criterion);
        } else if (isset($this->filters['posisi']) && $this->filters['posisi'] !== '') {
            //$query="select *
            //from ". sfConfig::get('app_default_schema') .".rincian r, ". sfConfig::get('app_default_schema') .".master_kegiatan mk
            //where r.rincian_level=".$this->filters['posisi']." and r.kegiatan_code=mk.kode_kegiatan";
            $crt1 = new Criteria();
            $crt1->add(RincianPeer::RINCIAN_LEVEL, $this->filters['posisi']);
            $rincian = RincianPeer::doSelect($crt1);
            foreach ($rincian as $r) {
                if ($r->getKegiatanCode() != '') {
                    $c->add(MasterKegiatanPeer::KODE_KEGIATAN, $r->getKegiatanCode());
                }
            }
        }
        if (isset($this->filters['tahap_is_empty'])) {
            $criterion = $c->getNewCriterion(MasterKegiatanPeer::TAHAP, '');
            $criterion->addOr($c->getNewCriterion(MasterKegiatanPeer::TAHAP, null, Criteria::ISNULL));
            $c->add($criterion);
        } else if (isset($this->filters['tahap']) && $this->filters['tahap'] !== '') {
            $c->add(MasterKegiatanPeer::TAHAP, $this->filters['tahap'] . '%', Criteria::LIKE);
        }
        if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
            $unit = $this->filters['unit_id'];
            $c->add(MasterKegiatanPeer::UNIT_ID, $unit);
        }
    }

    //untuk nambah revisi juga nambah fungsi disini
    protected function addFiltersCriteriaRevisi($c, $tahap, $menu)  {
       //die($menu);
         $menu = $this->getRequestParameter('menu');
        if ($tahap == 'pakbp') {
            //print_r($this->getRequestParameter('unit_name'));exit;
            if (isset($this->filters['kode_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(PakBukuPutihMasterKegiatanPeer::KODE_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(PakBukuPutihMasterKegiatanPeer::KODE_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['kode_kegiatan']) && $this->filters['kode_kegiatan'] !== '') {
                $kode_kegiatan = $this->filters['kode_kegiatan'];
                $arr = explode('.', $kode_kegiatan);
                $jum_array = count($arr);
                //if($jum_array<2)
                //{
                $cton1 = $c->getNewCriterion(PakBukuPutihMasterKegiatanPeer::KODE_URUSAN, $kode_kegiatan . '%', Criteria::LIKE);
                $cton2 = $c->getNewCriterion(PakBukuPutihMasterKegiatanPeer::KODE_PROGRAM2, $kode_kegiatan . '%', Criteria::LIKE);
                $cton1->addOr($cton2);
                //$c->add($cton1);
                //}
                if ($jum_array == 2) {
                    //$c->add(PakBukuPutihMasterKegiatanPeer::KODE_PROGRAM,$arr[1],Criteria::LIKE);
                    $cton3 = $c->getNewCriterion(PakBukuPutihMasterKegiatanPeer::KODE_PROGRAM, $arr[1] . '%', Criteria::LIKE);
                    $cton1->addAnd($cton3);
                }
                $c->add($cton1);
            }
            if (isset($this->filters['nama_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(PakBukuPutihMasterKegiatanPeer::NAMA_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(PakBukuPutihMasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
                $c->add(PakBukuPutihMasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
            }
            if (isset($this->filters['posisi_is_empty'])) {
                $criterion = $c->getNewCriterion(PakBukuPutihMasterKegiatanPeer::POSISI, '');
                $criterion->addOr($c->getNewCriterion(PakBukuPutihMasterKegiatanPeer::POSISI, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['posisi']) && $this->filters['posisi'] !== '') {
                //$query="select *
                //from ". sfConfig::get('app_default_schema') .".rincian r, ". sfConfig::get('app_default_schema') .".master_kegiatan mk
                //where r.rincian_level=".$this->filters['posisi']." and r.kegiatan_code=mk.kode_kegiatan";
                $crt1 = new Criteria();
                $crt1->add(PakBukuPutihRincianPeer::RINCIAN_LEVEL, $this->filters['posisi']);
                $rincian = PakBukuPutihRincianPeer::doSelect($crt1);
                foreach ($rincian as $r) {
                    if ($r->getKegiatanCode() != '') {
                        $c->add(PakBukuPutihMasterKegiatanPeer::KODE_KEGIATAN, $r->getKegiatanCode());
                    }
                }
            }
            if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
                $unit = $this->filters['unit_id'];
                $c->add(PakBukuPutihMasterKegiatanPeer::UNIT_ID, $unit);
            }
        } elseif ($tahap == 'pakbb') {
            //print_r($this->getRequestParameter('unit_name'));exit;
            if (isset($this->filters['kode_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(PakBukuBiruMasterKegiatanPeer::KODE_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(PakBukuBiruMasterKegiatanPeer::KODE_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['kode_kegiatan']) && $this->filters['kode_kegiatan'] !== '') {
                $kode_kegiatan = $this->filters['kode_kegiatan'];
                $arr = explode('.', $kode_kegiatan);
                $jum_array = count($arr);
                //if($jum_array<2)
                //{
                $cton1 = $c->getNewCriterion(PakBukuBiruMasterKegiatanPeer::KODE_URUSAN, $kode_kegiatan . '%', Criteria::LIKE);
                $cton2 = $c->getNewCriterion(PakBukuBiruMasterKegiatanPeer::KODE_PROGRAM2, $kode_kegiatan . '%', Criteria::LIKE);
                $cton1->addOr($cton2);
                //$c->add($cton1);
                //}
                if ($jum_array == 2) {
                    //$c->add(PakBukuBiruMasterKegiatanPeer::KODE_PROGRAM,$arr[1],Criteria::LIKE);
                    $cton3 = $c->getNewCriterion(PakBukuBiruMasterKegiatanPeer::KODE_PROGRAM, $arr[1] . '%', Criteria::LIKE);
                    $cton1->addAnd($cton3);
                }
                $c->add($cton1);
            }
            if (isset($this->filters['nama_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(PakBukuBiruMasterKegiatanPeer::NAMA_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(PakBukuBiruMasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
                $c->add(PakBukuBiruMasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
            }
            if (isset($this->filters['posisi_is_empty'])) {
                $criterion = $c->getNewCriterion(PakBukuBiruMasterKegiatanPeer::POSISI, '');
                $criterion->addOr($c->getNewCriterion(PakBukuBiruMasterKegiatanPeer::POSISI, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['posisi']) && $this->filters['posisi'] !== '') {
                //$query="select *
                //from ". sfConfig::get('app_default_schema') .".rincian r, ". sfConfig::get('app_default_schema') .".master_kegiatan mk
                //where r.rincian_level=".$this->filters['posisi']." and r.kegiatan_code=mk.kode_kegiatan";
                $crt1 = new Criteria();
                $crt1->add(PakBukuBiruRincianPeer::RINCIAN_LEVEL, $this->filters['posisi']);
                $rincian = PakBukuBiruRincianPeer::doSelect($crt1);
                foreach ($rincian as $r) {
                    if ($r->getKegiatanCode() != '') {
                        $c->add(PakBukuBiruMasterKegiatanPeer::KODE_KEGIATAN, $r->getKegiatanCode());
                    }
                }
            }
            if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
                $unit = $this->filters['unit_id'];
                $c->add(PakBukuBiruMasterKegiatanPeer::UNIT_ID, $unit);
            }
        } elseif ($tahap == 'murni') {
            //print_r($this->getRequestParameter('unit_name'));exit;
            if (isset($this->filters['kode_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(MurniMasterKegiatanPeer::KODE_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(MurniMasterKegiatanPeer::KODE_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['kode_kegiatan']) && $this->filters['kode_kegiatan'] !== '') {
                $kode_kegiatan = $this->filters['kode_kegiatan'];
                $arr = explode('.', $kode_kegiatan);
                $jum_array = count($arr);
                //if($jum_array<2)
                //{
                $cton1 = $c->getNewCriterion(MurniMasterKegiatanPeer::KODE_URUSAN, $kode_kegiatan . '%', Criteria::LIKE);
                $cton2 = $c->getNewCriterion(MurniMasterKegiatanPeer::KODE_PROGRAM2, $kode_kegiatan . '%', Criteria::LIKE);
                $cton1->addOr($cton2);
                //$c->add($cton1);
                //}
                if ($jum_array == 2) {
                    //$c->add(MurniMasterKegiatanPeer::KODE_PROGRAM,$arr[1],Criteria::LIKE);
                    $cton3 = $c->getNewCriterion(MurniMasterKegiatanPeer::KODE_PROGRAM, $arr[1] . '%', Criteria::LIKE);
                    $cton1->addAnd($cton3);
                }
                $c->add($cton1);
            }
            if (isset($this->filters['nama_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(MurniMasterKegiatanPeer::NAMA_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(MurniMasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
                $c->add(MurniMasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
            }
            if (isset($this->filters['posisi_is_empty'])) {
                $criterion = $c->getNewCriterion(MurniMasterKegiatanPeer::POSISI, '');
                $criterion->addOr($c->getNewCriterion(MurniMasterKegiatanPeer::POSISI, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['posisi']) && $this->filters['posisi'] !== '') {
                //$query="select *
                //from ". sfConfig::get('app_default_schema') .".rincian r, ". sfConfig::get('app_default_schema') .".master_kegiatan mk
                //where r.rincian_level=".$this->filters['posisi']." and r.kegiatan_code=mk.kode_kegiatan";
                $crt1 = new Criteria();
                $crt1->add(MurniRincianPeer::RINCIAN_LEVEL, $this->filters['posisi']);
                $rincian = MurniRincianPeer::doSelect($crt1);
                foreach ($rincian as $r) {
                    if ($r->getKegiatanCode() != '') {
                        $c->add(MurniMasterKegiatanPeer::KODE_KEGIATAN, $r->getKegiatanCode());
                    }
                }
            }
            if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
                $unit = $this->filters['unit_id'];
                $c->add(MurniMasterKegiatanPeer::UNIT_ID, $unit);
            }
        } elseif ($tahap == 'murnibp') {
            //print_r($this->getRequestParameter('unit_name'));exit;
            if (isset($this->filters['kode_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(MurniBukuPutihMasterKegiatanPeer::KODE_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(MurniBukuPutihMasterKegiatanPeer::KODE_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['kode_kegiatan']) && $this->filters['kode_kegiatan'] !== '') {
                $kode_kegiatan = $this->filters['kode_kegiatan'];
                $arr = explode('.', $kode_kegiatan);
                $jum_array = count($arr);
                //if($jum_array<2)
                //{
                $cton1 = $c->getNewCriterion(MurniBukuPutihMasterKegiatanPeer::KODE_URUSAN, $kode_kegiatan . '%', Criteria::LIKE);
                $cton2 = $c->getNewCriterion(MurniBukuPutihMasterKegiatanPeer::KODE_PROGRAM2, $kode_kegiatan . '%', Criteria::LIKE);
                $cton1->addOr($cton2);
                //$c->add($cton1);
                //}
                if ($jum_array == 2) {
                    //$c->add(MurniBukuPutihMasterKegiatanPeer::KODE_PROGRAM,$arr[1],Criteria::LIKE);
                    $cton3 = $c->getNewCriterion(MurniBukuPutihMasterKegiatanPeer::KODE_PROGRAM, $arr[1] . '%', Criteria::LIKE);
                    $cton1->addAnd($cton3);
                }
                $c->add($cton1);
            }
            if (isset($this->filters['nama_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(MurniBukuPutihMasterKegiatanPeer::NAMA_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(MurniBukuPutihMasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
                $c->add(MurniBukuPutihMasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
            }
            if (isset($this->filters['posisi_is_empty'])) {
                $criterion = $c->getNewCriterion(MurniBukuPutihMasterKegiatanPeer::POSISI, '');
                $criterion->addOr($c->getNewCriterion(MurniBukuPutihMasterKegiatanPeer::POSISI, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['posisi']) && $this->filters['posisi'] !== '') {
                //$query="select *
                //from ". sfConfig::get('app_default_schema') .".rincian r, ". sfConfig::get('app_default_schema') .".master_kegiatan mk
                //where r.rincian_level=".$this->filters['posisi']." and r.kegiatan_code=mk.kode_kegiatan";
                $crt1 = new Criteria();
                $crt1->add(MurniBukuPutihRincianPeer::RINCIAN_LEVEL, $this->filters['posisi']);
                $rincian = MurniBukuPutihRincianPeer::doSelect($crt1);
                foreach ($rincian as $r) {
                    if ($r->getKegiatanCode() != '') {
                        $c->add(MurniBukuPutihMasterKegiatanPeer::KODE_KEGIATAN, $r->getKegiatanCode());
                    }
                }
            }
            if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
                $unit = $this->filters['unit_id'];
                $c->add(MurniBukuPutihMasterKegiatanPeer::UNIT_ID, $unit);
            }
        } elseif ($tahap == 'murnibb') {
            //print_r($this->getRequestParameter('unit_name'));exit;
            if (isset($this->filters['kode_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(MurniBukuBiruMasterKegiatanPeer::KODE_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(MurniBukuBiruMasterKegiatanPeer::KODE_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['kode_kegiatan']) && $this->filters['kode_kegiatan'] !== '') {
                $kode_kegiatan = $this->filters['kode_kegiatan'];
                $arr = explode('.', $kode_kegiatan);
                $jum_array = count($arr);
                //if($jum_array<2)
                //{
                $cton1 = $c->getNewCriterion(MurniBukuBiruMasterKegiatanPeer::KODE_URUSAN, $kode_kegiatan . '%', Criteria::LIKE);
                $cton2 = $c->getNewCriterion(MurniBukuBiruMasterKegiatanPeer::KODE_PROGRAM2, $kode_kegiatan . '%', Criteria::LIKE);
                $cton1->addOr($cton2);
                //$c->add($cton1);
                //}
                if ($jum_array == 2) {
                    //$c->add(MurniBukuBiruMasterKegiatanPeer::KODE_PROGRAM,$arr[1],Criteria::LIKE);
                    $cton3 = $c->getNewCriterion(MurniBukuBiruMasterKegiatanPeer::KODE_PROGRAM, $arr[1] . '%', Criteria::LIKE);
                    $cton1->addAnd($cton3);
                }
                $c->add($cton1);
            }
            if (isset($this->filters['nama_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(MurniBukuBiruMasterKegiatanPeer::NAMA_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(MurniBukuBiruMasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
                $c->add(MurniBukuBiruMasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
            }
            if (isset($this->filters['posisi_is_empty'])) {
                $criterion = $c->getNewCriterion(MurniBukuBiruMasterKegiatanPeer::POSISI, '');
                $criterion->addOr($c->getNewCriterion(MurniBukuBiruMasterKegiatanPeer::POSISI, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['posisi']) && $this->filters['posisi'] !== '') {
                //$query="select *
                //from ". sfConfig::get('app_default_schema') .".rincian r, ". sfConfig::get('app_default_schema') .".master_kegiatan mk
                //where r.rincian_level=".$this->filters['posisi']." and r.kegiatan_code=mk.kode_kegiatan";
                $crt1 = new Criteria();
                $crt1->add(MurniBukuBiruRincianPeer::RINCIAN_LEVEL, $this->filters['posisi']);
                $rincian = MurniBukuBiruRincianPeer::doSelect($crt1);
                foreach ($rincian as $r) {
                    if ($r->getKegiatanCode() != '') {
                        $c->add(MurniBukuBiruMasterKegiatanPeer::KODE_KEGIATAN, $r->getKegiatanCode());
                    }
                }
            }
            if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
                $unit = $this->filters['unit_id'];
                $c->add(MurniBukuBiruMasterKegiatanPeer::UNIT_ID, $unit);
            }
        } elseif ($tahap == 'murnibbpraevagub') {
            //print_r($this->getRequestParameter('unit_name'));exit;
            if (isset($this->filters['kode_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(MurniBukubiruPraevagubMasterKegiatanPeer::KODE_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(MurniBukubiruPraevagubMasterKegiatanPeer::KODE_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['kode_kegiatan']) && $this->filters['kode_kegiatan'] !== '') {
                $kode_kegiatan = $this->filters['kode_kegiatan'];
                $arr = explode('.', $kode_kegiatan);
                $jum_array = count($arr);
                //if($jum_array<2)
                //{
                $cton1 = $c->getNewCriterion(MurniBukubiruPraevagubMasterKegiatanPeer::KODE_URUSAN, $kode_kegiatan . '%', Criteria::LIKE);
                $cton2 = $c->getNewCriterion(MurniBukubiruPraevagubMasterKegiatanPeer::KODE_PROGRAM2, $kode_kegiatan . '%', Criteria::LIKE);
                $cton1->addOr($cton2);
                //$c->add($cton1);
                //}
                if ($jum_array == 2) {
                    //$c->add(MurniBukubiruPraevagubMasterKegiatanPeer::KODE_PROGRAM,$arr[1],Criteria::LIKE);
                    $cton3 = $c->getNewCriterion(MurniBukubiruPraevagubMasterKegiatanPeer::KODE_PROGRAM, $arr[1] . '%', Criteria::LIKE);
                    $cton1->addAnd($cton3);
                }
                $c->add($cton1);
            }
            if (isset($this->filters['nama_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(MurniBukubiruPraevagubMasterKegiatanPeer::NAMA_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(MurniBukubiruPraevagubMasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
                $c->add(MurniBukubiruPraevagubMasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
            }
            if (isset($this->filters['posisi_is_empty'])) {
                $criterion = $c->getNewCriterion(MurniBukubiruPraevagubMasterKegiatanPeer::POSISI, '');
                $criterion->addOr($c->getNewCriterion(MurniBukubiruPraevagubMasterKegiatanPeer::POSISI, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['posisi']) && $this->filters['posisi'] !== '') {
                //$query="select *
                //from ". sfConfig::get('app_default_schema') .".rincian r, ". sfConfig::get('app_default_schema') .".master_kegiatan mk
                //where r.rincian_level=".$this->filters['posisi']." and r.kegiatan_code=mk.kode_kegiatan";
                $crt1 = new Criteria();
                $crt1->add(MurniBukubiruPraevagubRincianPeer::RINCIAN_LEVEL, $this->filters['posisi']);
                $rincian = MurniBukubiruPraevagubRincianPeer::doSelect($crt1);
                foreach ($rincian as $r) {
                    if ($r->getKegiatanCode() != '') {
                        $c->add(MurniBukubiruPraevagubMasterKegiatanPeer::KODE_KEGIATAN, $r->getKegiatanCode());
                    }
                }
            }
            if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
                $unit = $this->filters['unit_id'];
                $c->add(MurniBukubiruPraevagubMasterKegiatanPeer::UNIT_ID, $unit);
            }
        } elseif ($tahap == 'revisi1') {
            //print_r($this->getRequestParameter('unit_name'));exit;
            if (isset($this->filters['kode_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi1MasterKegiatanPeer::KODE_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(Revisi1MasterKegiatanPeer::KODE_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['kode_kegiatan']) && $this->filters['kode_kegiatan'] !== '') {
                $kode_kegiatan = $this->filters['kode_kegiatan'];
                $arr = explode('.', $kode_kegiatan);
                $jum_array = count($arr);
                //if($jum_array<2)
                //{
                $cton1 = $c->getNewCriterion(Revisi1MasterKegiatanPeer::KODE_URUSAN, $kode_kegiatan . '%', Criteria::LIKE);
                $cton2 = $c->getNewCriterion(Revisi1MasterKegiatanPeer::KODE_PROGRAM2, $kode_kegiatan . '%', Criteria::LIKE);
                $cton1->addOr($cton2);
                //$c->add($cton1);
                //}
                if ($jum_array == 2) {
                    //$c->add(Revisi1MasterKegiatanPeer::KODE_PROGRAM,$arr[1],Criteria::LIKE);
                    $cton3 = $c->getNewCriterion(Revisi1MasterKegiatanPeer::KODE_PROGRAM, $arr[1] . '%', Criteria::LIKE);
                    $cton1->addAnd($cton3);
                }
                $c->add($cton1);
            }
            if (isset($this->filters['nama_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi1MasterKegiatanPeer::NAMA_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(Revisi1MasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
                $c->add(Revisi1MasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
            }
            if (isset($this->filters['posisi_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi1MasterKegiatanPeer::POSISI, '');
                $criterion->addOr($c->getNewCriterion(Revisi1MasterKegiatanPeer::POSISI, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['posisi']) && $this->filters['posisi'] !== '') {
                //$query="select *
                //from ". sfConfig::get('app_default_schema') .".rincian r, ". sfConfig::get('app_default_schema') .".master_kegiatan mk
                //where r.rincian_level=".$this->filters['posisi']." and r.kegiatan_code=mk.kode_kegiatan";
                $crt1 = new Criteria();
                $crt1->add(Revisi1RincianPeer::RINCIAN_LEVEL, $this->filters['posisi']);
                $rincian = Revisi1RincianPeer::doSelect($crt1);
                foreach ($rincian as $r) {
                    if ($r->getKegiatanCode() != '') {
                        $c->add(Revisi1MasterKegiatanPeer::KODE_KEGIATAN, $r->getKegiatanCode());
                    }
                }
            }
            if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
                $unit = $this->filters['unit_id'];
                $c->add(Revisi1MasterKegiatanPeer::UNIT_ID, $unit);
            }
        } elseif ($tahap == 'revisi1_1') {
            //print_r($this->getRequestParameter('unit_name'));exit;
            if (isset($this->filters['kode_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi1bMasterKegiatanPeer::KODE_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(Revisi1bMasterKegiatanPeer::KODE_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['kode_kegiatan']) && $this->filters['kode_kegiatan'] !== '') {
                $kode_kegiatan = $this->filters['kode_kegiatan'];
                $arr = explode('.', $kode_kegiatan);
                $jum_array = count($arr);
                //if($jum_array<2)
                //{
                $cton1 = $c->getNewCriterion(Revisi1bMasterKegiatanPeer::KODE_URUSAN, $kode_kegiatan . '%', Criteria::LIKE);
                $cton2 = $c->getNewCriterion(Revisi1bMasterKegiatanPeer::KODE_PROGRAM2, $kode_kegiatan . '%', Criteria::LIKE);
                $cton1->addOr($cton2);
                //$c->add($cton1);
                //}
                if ($jum_array == 2) {
                    //$c->add(Revisi1MasterKegiatanPeer::KODE_PROGRAM,$arr[1],Criteria::LIKE);
                    $cton3 = $c->getNewCriterion(Revisi1bMasterKegiatanPeer::KODE_PROGRAM, $arr[1] . '%', Criteria::LIKE);
                    $cton1->addAnd($cton3);
                }
                $c->add($cton1);
            }
            if (isset($this->filters['nama_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi1bMasterKegiatanPeer::NAMA_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(Revisi1bMasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
                $c->add(Revisi1bMasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
            }
            if (isset($this->filters['posisi_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi1bMasterKegiatanPeer::POSISI, '');
                $criterion->addOr($c->getNewCriterion(Revisi1bMasterKegiatanPeer::POSISI, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['posisi']) && $this->filters['posisi'] !== '') {
                //$query="select *
                //from ". sfConfig::get('app_default_schema') .".rincian r, ". sfConfig::get('app_default_schema') .".master_kegiatan mk
                //where r.rincian_level=".$this->filters['posisi']." and r.kegiatan_code=mk.kode_kegiatan";
                $crt1 = new Criteria();
                $crt1->add(Revisi1bRincianPeer::RINCIAN_LEVEL, $this->filters['posisi']);
                $rincian = Revisi1bRincianPeer::doSelect($crt1);
                foreach ($rincian as $r) {
                    if ($r->getKegiatanCode() != '') {
                        $c->add(Revisi1bMasterKegiatanPeer::KODE_KEGIATAN, $r->getKegiatanCode());
                    }
                }
            }
            if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
                $unit = $this->filters['unit_id'];
                $c->add(Revisi1bMasterKegiatanPeer::UNIT_ID, $unit);
            }
        } 
        elseif ($tahap == 'revisi2') 
            {
            //print_r($this->getRequestParameter('unit_name'));exit;
            if (isset($this->filters['kode_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi2MasterKegiatanPeer::KODE_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(Revisi2MasterKegiatanPeer::KODE_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['kode_kegiatan']) && $this->filters['kode_kegiatan'] !== '') {
                $kode_kegiatan = $this->filters['kode_kegiatan'];
                $arr = explode('.', $kode_kegiatan);
                $jum_array = count($arr);
                //if($jum_array<2)
                //{
                $cton1 = $c->getNewCriterion(Revisi2MasterKegiatanPeer::KODE_URUSAN, $kode_kegiatan . '%', Criteria::LIKE);
                $cton2 = $c->getNewCriterion(Revisi2MasterKegiatanPeer::KODE_PROGRAM2, $kode_kegiatan . '%', Criteria::LIKE);
                $cton1->addOr($cton2);
                //$c->add($cton1);
                //}
                if ($jum_array == 2) {
                    //$c->add(Revisi2MasterKegiatanPeer::KODE_PROGRAM,$arr[1],Criteria::LIKE);
                    $cton3 = $c->getNewCriterion(Revisi2MasterKegiatanPeer::KODE_PROGRAM, $arr[1] . '%', Criteria::LIKE);
                    $cton1->addAnd($cton3);
                }
                $c->add($cton1);
            }
            if (isset($this->filters['nama_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi2MasterKegiatanPeer::NAMA_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(Revisi2MasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
                $c->add(Revisi2MasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
            }
            if (isset($this->filters['posisi_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi2MasterKegiatanPeer::POSISI, '');
                $criterion->addOr($c->getNewCriterion(Revisi2MasterKegiatanPeer::POSISI, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['posisi']) && $this->filters['posisi'] !== '') {
                //$query="select *
                //from ". sfConfig::get('app_default_schema') .".rincian r, ". sfConfig::get('app_default_schema') .".master_kegiatan mk
                //where r.rincian_level=".$this->filters['posisi']." and r.kegiatan_code=mk.kode_kegiatan";
                $crt1 = new Criteria();
                $crt1->add(Revisi2RincianPeer::RINCIAN_LEVEL, $this->filters['posisi']);
                $rincian = Revisi2RincianPeer::doSelect($crt1);
                foreach ($rincian as $r) {
                    if ($r->getKegiatanCode() != '') {
                        $c->add(Revisi2MasterKegiatanPeer::KODE_KEGIATAN, $r->getKegiatanCode());
                    }
                }
            }
            if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
                $unit = $this->filters['unit_id'];
                $c->add(Revisi2MasterKegiatanPeer::UNIT_ID, $unit);
            }
        } 
        elseif ($tahap == 'revisi2_1') 
            {
            //print_r($this->getRequestParameter('unit_name'));exit;
            if (isset($this->filters['kode_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi21MasterKegiatanPeer::KODE_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(Revisi21MasterKegiatanPeer::KODE_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['kode_kegiatan']) && $this->filters['kode_kegiatan'] !== '') {
                $kode_kegiatan = $this->filters['kode_kegiatan'];
                $arr = explode('.', $kode_kegiatan);
                $jum_array = count($arr);
                //if($jum_array<2)
                //{
                $cton1 = $c->getNewCriterion(Revisi21MasterKegiatanPeer::KODE_URUSAN, $kode_kegiatan . '%', Criteria::LIKE);
                $cton2 = $c->getNewCriterion(Revisi21MasterKegiatanPeer::KODE_PROGRAM2, $kode_kegiatan . '%', Criteria::LIKE);
                $cton1->addOr($cton2);
                //$c->add($cton1);
                //}
                if ($jum_array == 2) {
                    //$c->add(Revisi2MasterKegiatanPeer::KODE_PROGRAM,$arr[1],Criteria::LIKE);
                    $cton3 = $c->getNewCriterion(Revisi21MasterKegiatanPeer::KODE_PROGRAM, $arr[1] . '%', Criteria::LIKE);
                    $cton1->addAnd($cton3);
                }
                $c->add($cton1);
            }
            if (isset($this->filters['nama_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi21MasterKegiatanPeer::NAMA_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(Revisi21MasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
                $c->add(Revisi21MasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
            }
            if (isset($this->filters['posisi_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi21MasterKegiatanPeer::POSISI, '');
                $criterion->addOr($c->getNewCriterion(Revisi21MasterKegiatanPeer::POSISI, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['posisi']) && $this->filters['posisi'] !== '') {
                //$query="select *
                //from ". sfConfig::get('app_default_schema') .".rincian r, ". sfConfig::get('app_default_schema') .".master_kegiatan mk
                //where r.rincian_level=".$this->filters['posisi']." and r.kegiatan_code=mk.kode_kegiatan";
                $crt1 = new Criteria();
                $crt1->add(Revisi21RincianPeer::RINCIAN_LEVEL, $this->filters['posisi']);
                $rincian = Revisi21RincianPeer::doSelect($crt1);
                foreach ($rincian as $r) {
                    if ($r->getKegiatanCode() != '') {
                        $c->add(Revisi21MasterKegiatanPeer::KODE_KEGIATAN, $r->getKegiatanCode());
                    }
                }
            }
            if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
                $unit = $this->filters['unit_id'];
                $c->add(Revisi21MasterKegiatanPeer::UNIT_ID, $unit);
            }
        } elseif ($tahap == 'revisi2_2') 
            {
            //print_r($this->getRequestParameter('unit_name'));exit;
            if (isset($this->filters['kode_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi22MasterKegiatanPeer::KODE_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(Revisi22MasterKegiatanPeer::KODE_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['kode_kegiatan']) && $this->filters['kode_kegiatan'] !== '') {
                $kode_kegiatan = $this->filters['kode_kegiatan'];
                $arr = explode('.', $kode_kegiatan);
                $jum_array = count($arr);
                //if($jum_array<2)
                //{
                $cton1 = $c->getNewCriterion(Revisi22MasterKegiatanPeer::KODE_URUSAN, $kode_kegiatan . '%', Criteria::LIKE);
                $cton2 = $c->getNewCriterion(Revisi22MasterKegiatanPeer::KODE_PROGRAM2, $kode_kegiatan . '%', Criteria::LIKE);
                $cton1->addOr($cton2);
                //$c->add($cton1);
                //}
                if ($jum_array == 2) {
                    //$c->add(Revisi2MasterKegiatanPeer::KODE_PROGRAM,$arr[1],Criteria::LIKE);
                    $cton3 = $c->getNewCriterion(Revisi22MasterKegiatanPeer::KODE_PROGRAM, $arr[1] . '%', Criteria::LIKE);
                    $cton1->addAnd($cton3);
                }
                $c->add($cton1);
            }
            if (isset($this->filters['nama_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi22MasterKegiatanPeer::NAMA_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(Revisi22MasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
                $c->add(Revisi22MasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
            }
            if (isset($this->filters['posisi_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi22MasterKegiatanPeer::POSISI, '');
                $criterion->addOr($c->getNewCriterion(Revisi22MasterKegiatanPeer::POSISI, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['posisi']) && $this->filters['posisi'] !== '') {
                //$query="select *
                //from ". sfConfig::get('app_default_schema') .".rincian r, ". sfConfig::get('app_default_schema') .".master_kegiatan mk
                //where r.rincian_level=".$this->filters['posisi']." and r.kegiatan_code=mk.kode_kegiatan";
                $crt1 = new Criteria();
                $crt1->add(Revisi22RincianPeer::RINCIAN_LEVEL, $this->filters['posisi']);
                $rincian = Revisi22RincianPeer::doSelect($crt1);
                foreach ($rincian as $r) {
                    if ($r->getKegiatanCode() != '') {
                        $c->add(Revisi22MasterKegiatanPeer::KODE_KEGIATAN, $r->getKegiatanCode());
                    }
                }
            }
            if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
                $unit = $this->filters['unit_id'];
                $c->add(Revisi22MasterKegiatanPeer::UNIT_ID, $unit);
            }
        } 
//========== yogie 24-08-2017
        elseif ($tahap == 'revisi3') 
            {
            //print_r($this->getRequestParameter('unit_name'));exit;
            if (isset($this->filters['kode_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi3MasterKegiatanPeer::KODE_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(Revisi3MasterKegiatanPeer::KODE_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['kode_kegiatan']) && $this->filters['kode_kegiatan'] !== '') {
                $kode_kegiatan = $this->filters['kode_kegiatan'];
                $arr = explode('.', $kode_kegiatan);
                $jum_array = count($arr);
                //if($jum_array<2)
                //{
                $cton1 = $c->getNewCriterion(Revisi3MasterKegiatanPeer::KODE_URUSAN, $kode_kegiatan . '%', Criteria::LIKE);
                $cton2 = $c->getNewCriterion(Revisi3MasterKegiatanPeer::KODE_PROGRAM2, $kode_kegiatan . '%', Criteria::LIKE);
                $cton1->addOr($cton2);
                //$c->add($cton1);
                //}
                if ($jum_array == 2) {
                    //$c->add(Revisi3MasterKegiatanPeer::KODE_PROGRAM,$arr[1],Criteria::LIKE);
                    $cton3 = $c->getNewCriterion(Revisi3MasterKegiatanPeer::KODE_PROGRAM, $arr[1] . '%', Criteria::LIKE);
                    $cton1->addAnd($cton3);
                }
                $c->add($cton1);
            }
            if (isset($this->filters['nama_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi3MasterKegiatanPeer::NAMA_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(Revisi3MasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
                $c->add(Revisi3MasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
            }
            if (isset($this->filters['posisi_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi3MasterKegiatanPeer::POSISI, '');
                $criterion->addOr($c->getNewCriterion(Revisi3MasterKegiatanPeer::POSISI, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['posisi']) && $this->filters['posisi'] !== '') {
                //$query="select *
                //from ". sfConfig::get('app_default_schema') .".rincian r, ". sfConfig::get('app_default_schema') .".master_kegiatan mk
                //where r.rincian_level=".$this->filters['posisi']." and r.kegiatan_code=mk.kode_kegiatan";
                $crt1 = new Criteria();
                $crt1->add(Revisi3RincianPeer::RINCIAN_LEVEL, $this->filters['posisi']);
                $rincian = Revisi3RincianPeer::doSelect($crt1);
                foreach ($rincian as $r) {
                    if ($r->getKegiatanCode() != '') {
                        $c->add(Revisi3MasterKegiatanPeer::KODE_KEGIATAN, $r->getKegiatanCode());
                    }
                }
            }
            if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
                $unit = $this->filters['unit_id'];
                $c->add(Revisi3MasterKegiatanPeer::UNIT_ID, $unit);
            }
        } elseif ($tahap == 'revisi3_1') 
            {
            //print_r($this->getRequestParameter('unit_name'));exit;
            if (isset($this->filters['kode_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi31MasterKegiatanPeer::KODE_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(Revisi31MasterKegiatanPeer::KODE_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['kode_kegiatan']) && $this->filters['kode_kegiatan'] !== '') {
                $kode_kegiatan = $this->filters['kode_kegiatan'];
                $arr = explode('.', $kode_kegiatan);
                $jum_array = count($arr);
                //if($jum_array<2)
                //{
                $cton1 = $c->getNewCriterion(Revisi31MasterKegiatanPeer::KODE_URUSAN, $kode_kegiatan . '%', Criteria::LIKE);
                $cton2 = $c->getNewCriterion(Revisi31MasterKegiatanPeer::KODE_PROGRAM2, $kode_kegiatan . '%', Criteria::LIKE);
                $cton1->addOr($cton2);
                //$c->add($cton1);
                //}
                if ($jum_array == 2) {
                    //$c->add(Revisi3MasterKegiatanPeer::KODE_PROGRAM,$arr[1],Criteria::LIKE);
                    $cton3 = $c->getNewCriterion(Revisi31MasterKegiatanPeer::KODE_PROGRAM, $arr[1] . '%', Criteria::LIKE);
                    $cton1->addAnd($cton3);
                }
                $c->add($cton1);
            }
            if (isset($this->filters['nama_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi31MasterKegiatanPeer::NAMA_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(Revisi31MasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
                $c->add(Revisi31MasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
            }
            if (isset($this->filters['posisi_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi31MasterKegiatanPeer::POSISI, '');
                $criterion->addOr($c->getNewCriterion(Revisi31MasterKegiatanPeer::POSISI, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['posisi']) && $this->filters['posisi'] !== '') {
                //$query="select *
                //from ". sfConfig::get('app_default_schema') .".rincian r, ". sfConfig::get('app_default_schema') .".master_kegiatan mk
                //where r.rincian_level=".$this->filters['posisi']." and r.kegiatan_code=mk.kode_kegiatan";
                $crt1 = new Criteria();
                $crt1->add(Revisi31RincianPeer::RINCIAN_LEVEL, $this->filters['posisi']);
                $rincian = Revisi31RincianPeer::doSelect($crt1);
                foreach ($rincian as $r) {
                    if ($r->getKegiatanCode() != '') {
                        $c->add(Revisi31MasterKegiatanPeer::KODE_KEGIATAN, $r->getKegiatanCode());
                    }
                }
            }
            if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
                $unit = $this->filters['unit_id'];
                $c->add(Revisi31MasterKegiatanPeer::UNIT_ID, $unit);
            }
        } elseif ($tahap == 'revisi4') {
            //print_r($this->getRequestParameter('unit_name'));exit;
            if (isset($this->filters['kode_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi4MasterKegiatanPeer::KODE_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(Revisi4MasterKegiatanPeer::KODE_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['kode_kegiatan']) && $this->filters['kode_kegiatan'] !== '') {
                $kode_kegiatan = $this->filters['kode_kegiatan'];
                $arr = explode('.', $kode_kegiatan);
                $jum_array = count($arr);

                $cton1 = $c->getNewCriterion(Revisi4MasterKegiatanPeer::KODE_URUSAN, $kode_kegiatan . '%', Criteria::LIKE);
                $cton2 = $c->getNewCriterion(Revisi4MasterKegiatanPeer::KODE_PROGRAM2, $kode_kegiatan . '%', Criteria::LIKE);
                $cton1->addOr($cton2);

                if ($jum_array == 2) {
                    $cton3 = $c->getNewCriterion(Revisi4MasterKegiatanPeer::KODE_PROGRAM, $arr[1] . '%', Criteria::LIKE);
                    $cton1->addAnd($cton3);
                }
                $c->add($cton1);
            }
            if (isset($this->filters['nama_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi4MasterKegiatanPeer::NAMA_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(Revisi4MasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
                $c->add(Revisi4MasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
            }
            if (isset($this->filters['posisi_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi4MasterKegiatanPeer::POSISI, '');
                $criterion->addOr($c->getNewCriterion(Revisi4MasterKegiatanPeer::POSISI, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['posisi']) && $this->filters['posisi'] !== '') {
                $crt1 = new Criteria();
                $crt1->add(Revisi4RincianPeer::RINCIAN_LEVEL, $this->filters['posisi']);
                $rincian = Revisi4RincianPeer::doSelect($crt1);
                foreach ($rincian as $r) {
                    if ($r->getKegiatanCode() != '') {
                        $c->add(Revisi4MasterKegiatanPeer::KODE_KEGIATAN, $r->getKegiatanCode());
                    }
                }
            }
            if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
                $unit = $this->filters['unit_id'];
                $c->add(Revisi4MasterKegiatanPeer::UNIT_ID, $unit);
            }
        } elseif ($tahap == 'revisi5') {
            //print_r($this->getRequestParameter('unit_name'));exit;
            if (isset($this->filters['kode_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi5MasterKegiatanPeer::KODE_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(Revisi5MasterKegiatanPeer::KODE_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['kode_kegiatan']) && $this->filters['kode_kegiatan'] !== '') {
                $kode_kegiatan = $this->filters['kode_kegiatan'];
                $arr = explode('.', $kode_kegiatan);
                $jum_array = count($arr);
                
                $cton1 = $c->getNewCriterion(Revisi5MasterKegiatanPeer::KODE_URUSAN, $kode_kegiatan . '%', Criteria::LIKE);
                $cton2 = $c->getNewCriterion(Revisi5MasterKegiatanPeer::KODE_PROGRAM2, $kode_kegiatan . '%', Criteria::LIKE);
                $cton1->addOr($cton2);

                if ($jum_array == 2) {
                    $cton3 = $c->getNewCriterion(Revisi5MasterKegiatanPeer::KODE_PROGRAM, $arr[1] . '%', Criteria::LIKE);
                    $cton1->addAnd($cton3);
                }
                $c->add($cton1);
            }
            if (isset($this->filters['nama_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi5MasterKegiatanPeer::NAMA_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(Revisi5MasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
                $c->add(Revisi5MasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
            }
            if (isset($this->filters['posisi_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi5MasterKegiatanPeer::POSISI, '');
                $criterion->addOr($c->getNewCriterion(Revisi5MasterKegiatanPeer::POSISI, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['posisi']) && $this->filters['posisi'] !== '') {
                $crt1 = new Criteria();
                $crt1->add(Revisi5RincianPeer::RINCIAN_LEVEL, $this->filters['posisi']);
                $rincian = Revisi5RincianPeer::doSelect($crt1);
                foreach ($rincian as $r) {
                    if ($r->getKegiatanCode() != '') {
                        $c->add(Revisi5MasterKegiatanPeer::KODE_KEGIATAN, $r->getKegiatanCode());
                    }
                }
            }
            if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
                $unit = $this->filters['unit_id'];
                $c->add(Revisi5MasterKegiatanPeer::UNIT_ID, $unit);
            }
        } elseif ($tahap == 'revisi6') {
            //print_r($this->getRequestParameter('unit_name'));exit;
            if (isset($this->filters['kode_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi6MasterKegiatanPeer::KODE_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(Revisi6MasterKegiatanPeer::KODE_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['kode_kegiatan']) && $this->filters['kode_kegiatan'] !== '') {
                $kode_kegiatan = $this->filters['kode_kegiatan'];
                $arr = explode('.', $kode_kegiatan);
                $jum_array = count($arr);
                
                $cton1 = $c->getNewCriterion(Revisi6MasterKegiatanPeer::KODE_URUSAN, $kode_kegiatan . '%', Criteria::LIKE);
                $cton2 = $c->getNewCriterion(Revisi6MasterKegiatanPeer::KODE_PROGRAM2, $kode_kegiatan . '%', Criteria::LIKE);
                $cton1->addOr($cton2);

                if ($jum_array == 2) {
                    $cton3 = $c->getNewCriterion(Revisi6MasterKegiatanPeer::KODE_PROGRAM, $arr[1] . '%', Criteria::LIKE);
                    $cton1->addAnd($cton3);
                }
                $c->add($cton1);
            }
            if (isset($this->filters['nama_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi6MasterKegiatanPeer::NAMA_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(Revisi6MasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
                $c->add(Revisi6MasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
            }
            if (isset($this->filters['posisi_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi6MasterKegiatanPeer::POSISI, '');
                $criterion->addOr($c->getNewCriterion(Revisi6MasterKegiatanPeer::POSISI, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['posisi']) && $this->filters['posisi'] !== '') {
                $crt1 = new Criteria();
                $crt1->add(Revisi6RincianPeer::RINCIAN_LEVEL, $this->filters['posisi']);
                $rincian = Revisi6RincianPeer::doSelect($crt1);
                foreach ($rincian as $r) {
                    if ($r->getKegiatanCode() != '') {
                        $c->add(Revisi6MasterKegiatanPeer::KODE_KEGIATAN, $r->getKegiatanCode());
                    }
                }
            }
            if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
                $unit = $this->filters['unit_id'];
                $c->add(Revisi6MasterKegiatanPeer::UNIT_ID, $unit);
            }
        } elseif ($tahap == 'revisi7') {
            //print_r($this->getRequestParameter('unit_name'));exit;
            if (isset($this->filters['kode_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi7MasterKegiatanPeer::KODE_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(Revisi7MasterKegiatanPeer::KODE_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['kode_kegiatan']) && $this->filters['kode_kegiatan'] !== '') {
                $kode_kegiatan = $this->filters['kode_kegiatan'];
                $arr = explode('.', $kode_kegiatan);
                $jum_array = count($arr);
                
                $cton1 = $c->getNewCriterion(Revisi7MasterKegiatanPeer::KODE_URUSAN, $kode_kegiatan . '%', Criteria::LIKE);
                $cton2 = $c->getNewCriterion(Revisi7MasterKegiatanPeer::KODE_PROGRAM2, $kode_kegiatan . '%', Criteria::LIKE);
                $cton1->addOr($cton2);

                if ($jum_array == 2) {
                    $cton3 = $c->getNewCriterion(Revisi7MasterKegiatanPeer::KODE_PROGRAM, $arr[1] . '%', Criteria::LIKE);
                    $cton1->addAnd($cton3);
                }
                $c->add($cton1);
            }
            if (isset($this->filters['nama_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi7MasterKegiatanPeer::NAMA_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(Revisi7MasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
                $c->add(Revisi7MasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
            }
            if (isset($this->filters['posisi_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi7MasterKegiatanPeer::POSISI, '');
                $criterion->addOr($c->getNewCriterion(Revisi7MasterKegiatanPeer::POSISI, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['posisi']) && $this->filters['posisi'] !== '') {
                $crt1 = new Criteria();
                $crt1->add(Revisi7RincianPeer::RINCIAN_LEVEL, $this->filters['posisi']);
                $rincian = Revisi7RincianPeer::doSelect($crt1);
                foreach ($rincian as $r) {
                    if ($r->getKegiatanCode() != '') {
                        $c->add(Revisi7MasterKegiatanPeer::KODE_KEGIATAN, $r->getKegiatanCode());
                    }
                }
            }
            if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
                $unit = $this->filters['unit_id'];
                $c->add(Revisi7MasterKegiatanPeer::UNIT_ID, $unit);
            }
        } elseif ($tahap == 'revisi8') {
            //print_r($this->getRequestParameter('unit_name'));exit;
            if (isset($this->filters['kode_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi8MasterKegiatanPeer::KODE_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(Revisi8MasterKegiatanPeer::KODE_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['kode_kegiatan']) && $this->filters['kode_kegiatan'] !== '') {
                $kode_kegiatan = $this->filters['kode_kegiatan'];
                $arr = explode('.', $kode_kegiatan);
                $jum_array = count($arr);
                
                $cton1 = $c->getNewCriterion(Revisi8MasterKegiatanPeer::KODE_URUSAN, $kode_kegiatan . '%', Criteria::LIKE);
                $cton2 = $c->getNewCriterion(Revisi8MasterKegiatanPeer::KODE_PROGRAM2, $kode_kegiatan . '%', Criteria::LIKE);
                $cton1->addOr($cton2);

                if ($jum_array == 2) {
                    $cton3 = $c->getNewCriterion(Revisi8MasterKegiatanPeer::KODE_PROGRAM, $arr[1] . '%', Criteria::LIKE);
                    $cton1->addAnd($cton3);
                }
                $c->add($cton1);
            }
            if (isset($this->filters['nama_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi8MasterKegiatanPeer::NAMA_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(Revisi8MasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
                $c->add(Revisi8MasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
            }
            if (isset($this->filters['posisi_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi8MasterKegiatanPeer::POSISI, '');
                $criterion->addOr($c->getNewCriterion(Revisi8MasterKegiatanPeer::POSISI, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['posisi']) && $this->filters['posisi'] !== '') {
                $crt1 = new Criteria();
                $crt1->add(Revisi8RincianPeer::RINCIAN_LEVEL, $this->filters['posisi']);
                $rincian = Revisi8RincianPeer::doSelect($crt1);
                foreach ($rincian as $r) {
                    if ($r->getKegiatanCode() != '') {
                        $c->add(Revisi8MasterKegiatanPeer::KODE_KEGIATAN, $r->getKegiatanCode());
                    }
                }
            }
            if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
                $unit = $this->filters['unit_id'];
                $c->add(Revisi8MasterKegiatanPeer::UNIT_ID, $unit);
            }
        } elseif ($tahap == 'revisi9') {
            //print_r($this->getRequestParameter('unit_name'));exit;
            if (isset($this->filters['kode_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi9MasterKegiatanPeer::KODE_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(Revisi9MasterKegiatanPeer::KODE_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['kode_kegiatan']) && $this->filters['kode_kegiatan'] !== '') {
                $kode_kegiatan = $this->filters['kode_kegiatan'];
                $arr = explode('.', $kode_kegiatan);
                $jum_array = count($arr);
                
                $cton1 = $c->getNewCriterion(Revisi9MasterKegiatanPeer::KODE_URUSAN, $kode_kegiatan . '%', Criteria::LIKE);
                $cton2 = $c->getNewCriterion(Revisi9MasterKegiatanPeer::KODE_PROGRAM2, $kode_kegiatan . '%', Criteria::LIKE);
                $cton1->addOr($cton2);

                if ($jum_array == 2) {
                    $cton3 = $c->getNewCriterion(Revisi9MasterKegiatanPeer::KODE_PROGRAM, $arr[1] . '%', Criteria::LIKE);
                    $cton1->addAnd($cton3);
                }
                $c->add($cton1);
            }
            if (isset($this->filters['nama_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi9MasterKegiatanPeer::NAMA_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(Revisi9MasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
                $c->add(Revisi9MasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
            }
            if (isset($this->filters['posisi_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi9MasterKegiatanPeer::POSISI, '');
                $criterion->addOr($c->getNewCriterion(Revisi9MasterKegiatanPeer::POSISI, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['posisi']) && $this->filters['posisi'] !== '') {
                $crt1 = new Criteria();
                $crt1->add(Revisi9RincianPeer::RINCIAN_LEVEL, $this->filters['posisi']);
                $rincian = Revisi9RincianPeer::doSelect($crt1);
                foreach ($rincian as $r) {
                    if ($r->getKegiatanCode() != '') {
                        $c->add(Revisi9MasterKegiatanPeer::KODE_KEGIATAN, $r->getKegiatanCode());
                    }
                }
            }
            if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
                $unit = $this->filters['unit_id'];
                $c->add(Revisi9MasterKegiatanPeer::UNIT_ID, $unit);
            }
        } elseif ($tahap == 'revisi10') {
            //print_r($this->getRequestParameter('unit_name'));exit;
            if (isset($this->filters['kode_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi10MasterKegiatanPeer::KODE_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(Revisi10MasterKegiatanPeer::KODE_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['kode_kegiatan']) && $this->filters['kode_kegiatan'] !== '') {
                $kode_kegiatan = $this->filters['kode_kegiatan'];
                $arr = explode('.', $kode_kegiatan);
                $jum_array = count($arr);
                
                $cton1 = $c->getNewCriterion(Revisi10MasterKegiatanPeer::KODE_URUSAN, $kode_kegiatan . '%', Criteria::LIKE);
                $cton2 = $c->getNewCriterion(Revisi10MasterKegiatanPeer::KODE_PROGRAM2, $kode_kegiatan . '%', Criteria::LIKE);
                $cton1->addOr($cton2);

                if ($jum_array == 2) {
                    $cton3 = $c->getNewCriterion(Revisi10MasterKegiatanPeer::KODE_PROGRAM, $arr[1] . '%', Criteria::LIKE);
                    $cton1->addAnd($cton3);
                }
                $c->add($cton1);
            }
            if (isset($this->filters['nama_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi10MasterKegiatanPeer::NAMA_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(Revisi10MasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
                $c->add(Revisi10MasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
            }
            if (isset($this->filters['posisi_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi10MasterKegiatanPeer::POSISI, '');
                $criterion->addOr($c->getNewCriterion(Revisi10MasterKegiatanPeer::POSISI, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['posisi']) && $this->filters['posisi'] !== '') {
                $crt1 = new Criteria();
                $crt1->add(Revisi10RincianPeer::RINCIAN_LEVEL, $this->filters['posisi']);
                $rincian = Revisi10RincianPeer::doSelect($crt1);
                foreach ($rincian as $r) {
                    if ($r->getKegiatanCode() != '') {
                        $c->add(Revisi10MasterKegiatanPeer::KODE_KEGIATAN, $r->getKegiatanCode());
                    }
                }
            }
            if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
                $unit = $this->filters['unit_id'];
                $c->add(Revisi10MasterKegiatanPeer::UNIT_ID, $unit);
            }
        }
        elseif ($tahap == 'rkua') {
            //print_r($this->getRequestParameter('unit_name'));exit;
            if (isset($this->filters['kode_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(RkuaMasterKegiatanPeer::KODE_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(RkuaMasterKegiatanPeer::KODE_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['kode_kegiatan']) && $this->filters['kode_kegiatan'] !== '') {
                $kode_kegiatan = $this->filters['kode_kegiatan'];
                $arr = explode('.', $kode_kegiatan);
                $jum_array = count($arr);
                //if($jum_array<2)
                //{
                $cton1 = $c->getNewCriterion(RkuaMasterKegiatanPeer::KODE_URUSAN, $kode_kegiatan . '%', Criteria::LIKE);
                $cton2 = $c->getNewCriterion(RkuaMasterKegiatanPeer::KODE_PROGRAM2, $kode_kegiatan . '%', Criteria::LIKE);
                $cton1->addOr($cton2);
                //$c->add($cton1);
                //}
                if ($jum_array == 2) {
                    //$c->add(PakBukuBiruMasterKegiatanPeer::KODE_PROGRAM,$arr[1],Criteria::LIKE);
                    $cton3 = $c->getNewCriterion(RkuaMasterKegiatanPeer::KODE_PROGRAM, $arr[1] . '%', Criteria::LIKE);
                    $cton1->addAnd($cton3);
                }
                $c->add($cton1);
            }
            if (isset($this->filters['nama_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(RkuaMasterKegiatanPeer::NAMA_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(RkuaMasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
                $c->add(RkuaMasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
            }
            if (isset($this->filters['posisi_is_empty'])) {
                $criterion = $c->getNewCriterion(RkuaMasterKegiatanPeer::POSISI, '');
                $criterion->addOr($c->getNewCriterion(RkuaMasterKegiatanPeer::POSISI, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['posisi']) && $this->filters['posisi'] !== '') {
                //$query="select *
                //from ". sfConfig::get('app_default_schema') .".rincian r, ". sfConfig::get('app_default_schema') .".master_kegiatan mk
                //where r.rincian_level=".$this->filters['posisi']." and r.kegiatan_code=mk.kode_kegiatan";
                $crt1 = new Criteria();
                $crt1->add(RkuaRincianPeer::RINCIAN_LEVEL, $this->filters['posisi']);
                $rincian = RkuaRincianPeer::doSelect($crt1);
                foreach ($rincian as $r) {
                    if ($r->getKegiatanCode() != '') {
                        $c->add(RkuaMasterKegiatanPeer::KODE_KEGIATAN, $r->getKegiatanCode());
                    }
                }
            }
            if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
                $unit = $this->filters['unit_id'];
                $c->add(RkuaMasterKegiatanPeer::UNIT_ID, $unit);
            }
        } else {
            //print_r($this->getRequestParameter('unit_name'));exit;
            if (isset($this->filters['kode_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(DinasMasterKegiatanPeer::KODE_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(DinasMasterKegiatanPeer::KODE_KEGIATAN, null, Criteria::ISNULL));
                if ($menu == 'Pendapatan') {
                $c->add(DinasMasterKegiatanPeer::IS_BTL,TRUE);
                }
                $c->add($criterion);
            } else if (isset($this->filters['kode_kegiatan']) && $this->filters['kode_kegiatan'] !== '') {
                $kode_kegiatan = $this->filters['kode_kegiatan'];
                $arr = explode('.', $kode_kegiatan);
                $jum_array = count($arr);
                //if($jum_array<2)
                //{
                $cton1 = $c->getNewCriterion(DinasMasterKegiatanPeer::KODE_URUSAN, $kode_kegiatan . '%', Criteria::LIKE);
                $cton2 = $c->getNewCriterion(DinasMasterKegiatanPeer::KODE_PROGRAM2, $kode_kegiatan . '%', Criteria::LIKE);
                $cton1->addOr($cton2);
                //$c->add($cton1);
                //}
                if ($jum_array == 2) {
                    //$c->add(DinasMasterKegiatanPeer::KODE_PROGRAM,$arr[1],Criteria::LIKE);
                    $cton3 = $c->getNewCriterion(DinasMasterKegiatanPeer::KODE_PROGRAM, $arr[1] . '%', Criteria::LIKE);
                    $cton1->addAnd($cton3);
                }
                if ($menu == 'Pendapatan') {
                $c->add(DinasMasterKegiatanPeer::IS_BTL,TRUE);
                }
                $c->add($cton1);
            }
            if (isset($this->filters['nama_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(DinasMasterKegiatanPeer::NAMA_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(DinasMasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
                if ($menu == 'Pendapatan') {
                $c->add(DinasMasterKegiatanPeer::IS_BTL,TRUE);
                }
                $c->add($criterion);
            } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
                
                if(preg_match('~[0-9]+~', $this->filters['nama_kegiatan']))
                {
                     $c->add(DinasMasterKegiatanPeer::USER_ID,'%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
                }  
                else
                {
                    $c->add(DinasMasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
                }    
                if ($menu == 'Pendapatan') {
                $c->add(DinasMasterKegiatanPeer::IS_BTL,TRUE);
                }
            }
            if (isset($this->filters['posisi_is_empty'])) {
                $criterion = $c->getNewCriterion(DinasMasterKegiatanPeer::POSISI, '');
                $criterion->addOr($c->getNewCriterion(DinasMasterKegiatanPeer::POSISI, null, Criteria::ISNULL));
                if ($menu == 'Pendapatan') {
                $c->add(DinasMasterKegiatanPeer::IS_BTL,TRUE);
                }
                $c->add($criterion);
            } else if (isset($this->filters['posisi']) && $this->filters['posisi'] !== '') {
                //$query="select *
                //from ". sfConfig::get('app_default_schema') .".rincian r, ". sfConfig::get('app_default_schema') .".master_kegiatan mk
                //where r.rincian_level=".$this->filters['posisi']." and r.kegiatan_code=mk.kode_kegiatan";
                $crt1 = new Criteria();
                $crt1->add(DinasRincianPeer::RINCIAN_LEVEL, $this->filters['posisi']);
                $rincian = DinasRincianPeer::doSelect($crt1);
                foreach ($rincian as $r) {
                    if ($r->getKegiatanCode() != '') {
                        $c->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $r->getKegiatanCode());
                    }
                }
                if ($menu == 'Pendapatan') {
                $c->add(DinasMasterKegiatanPeer::IS_BTL,TRUE);
                }
            }
//        if (isset($this->filters['tahap_is_empty'])) {
//            $criterion = $c->getNewCriterion(DinasMasterKegiatanPeer::TAHAP, '');
//            $criterion->addOr($c->getNewCriterion(DinasMasterKegiatanPeer::TAHAP, null, Criteria::ISNULL));
//            $c->add($criterion);
//        } else if (isset($this->filters['tahap']) && $this->filters['tahap'] !== '') {
//            $c->add(DinasMasterKegiatanPeer::TAHAP, $this->filters['tahap'] . '%', Criteria::LIKE);
//        }
            if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
                $unit = $this->filters['unit_id'];
                $c->add(DinasMasterKegiatanPeer::UNIT_ID, $unit);
               // die('menu adalah '.$menu);
               // $c->add(DinasMasterKegiatanPeer::IS_BTL,TRUE);
                if ($menu == 'Pendapatan') {
                $c->add(DinasMasterKegiatanPeer::IS_BTL,TRUE);
                }
            }
        }
    }

    public function executeTemplateRKA() {
        //$this->setTemplate('templateRKA');
        $this->setLayout('kosong');
        $this->getResponse()->addStylesheet('tampilan_print2', '', array('media' => 'print'));
    }

    public function executePrintrekening() {
        $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
        $unit_id = $this->getRequestParameter('unit_id');
        $this->kode_kegiatan = $kode_kegiatan;
        $this->unit_id = $unit_id;
        $this->setLayout('kosong');
    }

    public function executeGetHeader() {
        $this->tahap = $tahap = $this->getRequestParameter('tahap');
        if ($tahap == 'pakbp') {
            $c = new Criteria();
            $c->add(PakBukuPutihMasterKegiatanPeer::KODE_KEGIATAN, $this->getRequestParameter('kegiatan'));
            $c->add(PakBukuPutihMasterKegiatanPeer::UNIT_ID, $this->getRequestParameter('unit'));
            $master_kegiatan = PakBukuPutihMasterKegiatanPeer::doSelectOne($c);
        } elseif ($tahap == 'pakbb') {
            $c = new Criteria();
            $c->add(PakBukuBiruMasterKegiatanPeer::KODE_KEGIATAN, $this->getRequestParameter('kegiatan'));
            $c->add(PakBukuBiruMasterKegiatanPeer::UNIT_ID, $this->getRequestParameter('unit'));
            $master_kegiatan = PakBukuBiruMasterKegiatanPeer::doSelectOne($c);
        } elseif ($tahap == 'murni') {
            $c = new Criteria();
            $c->add(MurniMasterKegiatanPeer::KODE_KEGIATAN, $this->getRequestParameter('kegiatan'));
            $c->add(MurniMasterKegiatanPeer::UNIT_ID, $this->getRequestParameter('unit'));
            $master_kegiatan = MurniMasterKegiatanPeer::doSelectOne($c);
        } elseif ($tahap == 'murnibp') {
            $c = new Criteria();
            $c->add(MurniBukuPutihMasterKegiatanPeer::KODE_KEGIATAN, $this->getRequestParameter('kegiatan'));
            $c->add(MurniBukuPutihMasterKegiatanPeer::UNIT_ID, $this->getRequestParameter('unit'));
            $master_kegiatan = MurniBukuPutihMasterKegiatanPeer::doSelectOne($c);
        } elseif ($tahap == 'murnibb') {
            $c = new Criteria();
            $c->add(MurniBukuBiruMasterKegiatanPeer::KODE_KEGIATAN, $this->getRequestParameter('kegiatan'));
            $c->add(MurniBukuBiruMasterKegiatanPeer::UNIT_ID, $this->getRequestParameter('unit'));
            $master_kegiatan = MurniBukuBiruMasterKegiatanPeer::doSelectOne($c);
        } elseif ($tahap == 'revisi1') {
            $c = new Criteria();
            $c->add(Revisi1MasterKegiatanPeer::KODE_KEGIATAN, $this->getRequestParameter('kegiatan'));
            $c->add(Revisi1MasterKegiatanPeer::UNIT_ID, $this->getRequestParameter('unit'));
            $master_kegiatan = Revisi1MasterKegiatanPeer::doSelectOne($c);
        } elseif ($tahap == 'revisi1_1') {
            $c = new Criteria();
            $c->add(Revisi1bMasterKegiatanPeer::KODE_KEGIATAN, $this->getRequestParameter('kegiatan'));
            $c->add(Revisi1bMasterKegiatanPeer::UNIT_ID, $this->getRequestParameter('unit'));
            $master_kegiatan = Revisi1bMasterKegiatanPeer::doSelectOne($c);
        } elseif ($tahap == 'revisi2') {
            $c = new Criteria();
            $c->add(Revisi2MasterKegiatanPeer::KODE_KEGIATAN, $this->getRequestParameter('kegiatan'));
            $c->add(Revisi2MasterKegiatanPeer::UNIT_ID, $this->getRequestParameter('unit'));
            $master_kegiatan = Revisi2MasterKegiatanPeer::doSelectOne($c);
        } elseif ($tahap == 'revisi3') {
            $c = new Criteria();
            $c->add(Revisi3MasterKegiatanPeer::KODE_KEGIATAN, $this->getRequestParameter('kegiatan'));
            $c->add(Revisi3MasterKegiatanPeer::UNIT_ID, $this->getRequestParameter('unit'));
            $master_kegiatan = Revisi3MasterKegiatanPeer::doSelectOne($c);
        } elseif ($tahap == 'revisi3_1') {
            $c = new Criteria();
            $c->add(Revisi31MasterKegiatanPeer::KODE_KEGIATAN, $this->getRequestParameter('kegiatan'));
            $c->add(Revisi31MasterKegiatanPeer::UNIT_ID, $this->getRequestParameter('unit'));
            $master_kegiatan = Revisi31MasterKegiatanPeer::doSelectOne($c);
        } elseif ($tahap == 'revisi4') {
            $c = new Criteria();
            $c->add(Revisi4MasterKegiatanPeer::KODE_KEGIATAN, $this->getRequestParameter('kegiatan'));
            $c->add(Revisi4MasterKegiatanPeer::UNIT_ID, $this->getRequestParameter('unit'));
            $master_kegiatan = Revisi4MasterKegiatanPeer::doSelectOne($c);
        } elseif ($tahap == 'revisi5') {
            $c = new Criteria();
            $c->add(Revisi5MasterKegiatanPeer::KODE_KEGIATAN, $this->getRequestParameter('kegiatan'));
            $c->add(Revisi5MasterKegiatanPeer::UNIT_ID, $this->getRequestParameter('unit'));
            $master_kegiatan = Revisi5MasterKegiatanPeer::doSelectOne($c);
        } elseif ($tahap == 'revisi6') {
            $c = new Criteria();
            $c->add(Revisi6MasterKegiatanPeer::KODE_KEGIATAN, $this->getRequestParameter('kegiatan'));
            $c->add(Revisi6MasterKegiatanPeer::UNIT_ID, $this->getRequestParameter('unit'));
            $master_kegiatan = Revisi6MasterKegiatanPeer::doSelectOne($c);
        } elseif ($tahap == 'revisi7') {
            $c = new Criteria();
            $c->add(Revisi7MasterKegiatanPeer::KODE_KEGIATAN, $this->getRequestParameter('kegiatan'));
            $c->add(Revisi7MasterKegiatanPeer::UNIT_ID, $this->getRequestParameter('unit'));
            $master_kegiatan = Revisi7MasterKegiatanPeer::doSelectOne($c);
        } elseif ($tahap == 'revisi8') {
            $c = new Criteria();
            $c->add(Revisi8MasterKegiatanPeer::KODE_KEGIATAN, $this->getRequestParameter('kegiatan'));
            $c->add(Revisi8MasterKegiatanPeer::UNIT_ID, $this->getRequestParameter('unit'));
            $master_kegiatan = Revisi8MasterKegiatanPeer::doSelectOne($c);
        } elseif ($tahap == 'revisi9') {
            $c = new Criteria();
            $c->add(Revisi9MasterKegiatanPeer::KODE_KEGIATAN, $this->getRequestParameter('kegiatan'));
            $c->add(Revisi9MasterKegiatanPeer::UNIT_ID, $this->getRequestParameter('unit'));
            $master_kegiatan = Revisi9MasterKegiatanPeer::doSelectOne($c);
        } elseif ($tahap == 'revisi10') {
            $c = new Criteria();
            $c->add(Revisi10MasterKegiatanPeer::KODE_KEGIATAN, $this->getRequestParameter('kegiatan'));
            $c->add(Revisi10MasterKegiatanPeer::UNIT_ID, $this->getRequestParameter('unit'));
            $master_kegiatan = Revisi10MasterKegiatanPeer::doSelectOne($c);
        } elseif ($tahap == 'rkua') {
            $c = new Criteria();
            $c->add(RkuaMasterKegiatanPeer::KODE_KEGIATAN, $this->getRequestParameter('kegiatan'));
            $c->add(RkuaMasterKegiatanPeer::UNIT_ID, $this->getRequestParameter('unit'));
            $master_kegiatan = RkuaMasterKegiatanPeer::doSelectOne($c);
        } else {
            $c = new Criteria();
            $c->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $this->getRequestParameter('kegiatan'));
            $c->add(DinasMasterKegiatanPeer::UNIT_ID, $this->getRequestParameter('unit'));
            $master_kegiatan = DinasMasterKegiatanPeer::doSelectOne($c);
        }

        if ($master_kegiatan) {
            $this->master_kegiatan = $master_kegiatan;
        }
        $this->setLayout('kosong');
    }

    public function executeSimulasi() {
//            $sub_id = $this->getRequestParameter('sub_id');
//            $this->sub_id = $sub_id;

        if ($this->getRequestParameter('sub_id')) {

            $sub_id = $this->getRequestParameter('sub_id');
            $c = new Criteria();
            $c->add(BappekoSubtitleIndikatorPeer::SUB_ID, $sub_id);
//                        echo 'masuk';exit;
            $rs_subtitle = BappekoSubtitleIndikatorPeer::doSelectOne($c);


            if ($rs_subtitle) {
                $unit_id = $rs_subtitle->getUnitId();
                $kegiatan_code = $rs_subtitle->getKegiatanCode();
                $subtitle = $rs_subtitle->getSubtitle();
                $nama_subtitle = trim($subtitle);
//                                echo $unit_id;
            }
            $this->nama_subtitle = $nama_subtitle;
            $query = "select *  from " . sfConfig::get('app_default_schema') . ".bappeko_rincian_detail
				  where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and subtitle ilike '%$nama_subtitle%' and status_hapus=false order by sub,rekening_code,komponen_name";

            $con = Propel::getConnection(BappekoRincianDetailPeer::DATABASE_NAME);
            $statement = $con->prepareStatement($query);
            $rs_rinciandetail = $statement->executeQuery();
            $this->rs_rinciandetail = $rs_rinciandetail;
//                        var_dump($query);
            $c_rincianDetail = new Criteria();
            $c_rincianDetail->add(BappekoRincianDetailPeer::UNIT_ID, $unit_id);
            $c_rincianDetail->add(BappekoRincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
            $c_rincianDetail->add(BappekoRincianDetailPeer::SUBTITLE, $nama_subtitle, Criteria::ILIKE);
            $c_rincianDetail->add(BappekoRincianDetailPeer::STATUS_HAPUS, false);
            $c_rincianDetail->add(BappekoRincianDetailPeer::TAHUN, sfConfig::get('app_tahun_default'));
            $c_rincianDetail->addAscendingOrderByColumn(BappekoRincianDetailPeer::KODE_SUB);
            $c_rincianDetail->addAscendingOrderByColumn(BappekoRincianDetailPeer::REKENING_CODE);
            $c_rincianDetail->addAscendingOrderByColumn(BappekoRincianDetailPeer::KOMPONEN_NAME);
            $rs_rd = BappekoRincianDetailPeer::doSelect($c_rincianDetail);
//                        var_dump($rs_rd);
            $this->rs_rd = $rs_rd;

            $this->id = $sub_id;
            $this->rinciandetail = 'ada';
//			$this->setLayout('kosong');
            $this->setLayout('layoutPop');
        }
    }

    public function executeBTL() {
//           $file = fopen('http://budget.localhost/budget2014sm/peneliti/tarik_urusan.php','r');
//           $content = fread($file,"5000");
        $content = file_get_contents('http://budget.localhost/budget2014sm/peneliti/tarik_urusan.php');
//           $content = readfile('http://budget.localhost/budget2014sm/peneliti/tarik_urusan.php');
        $header = (explode("\n", $content, 2));
        $dataArr = $header[1];

        echo $dataArr;
    }

    //irul 11 feb 2014 - kunci semua komponen
    protected function prosesKunciSemuaKomponen() {
        $con = Propel::getConnection();
        $con->begin();
        try {
            $query1 = "update " . sfConfig::get('app_default_schema') . ".rincian_detail "
                    . "set lock_subtitle='LOCK' "
                    . "where tahun ='" . sfConfig::get('app_tahun_default') . "' and rekening_code <> '5.2.1.04.01'";
            $stmt = $con->prepareStatement($query1);
            $stmt->executeQuery();
            $con->commit();
            $this->setFlash('berhasil', 'Sukses menutup semua komponen kegiatan');
        } catch (Exception $e) {
            $con->rollback();
            $this->setFlash('gagal', 'Gagal menutup komponen karena ' . $e->getMessage());
        }
    }

    //irul 11 feb 2014 - kunci semua komponen    
    //irul 8oktober 2014 - search komponen
    public function executeSearchKomponen() {
        if ($this->getRequest()->getMethod() == sfRequest::POST) {
            $this->habis_cari = true;
            $this->unit_id = $this->getRequestParameter('unit_id');
            $this->kegiatan_code = $this->getRequestParameter('kegiatan_code');
            $this->subtitle = $this->getRequestParameter('subtitle');
            $this->cari_lokasi = $this->getRequestParameter('cari_lokasi');
            $this->cari_komponen = $this->getRequestParameter('cari_komponen');
            $this->tipe_fisik = $this->getRequestParameter('tipe_fisik');
        }
    }

    //irul 8oktober 2014 - search komponen


    public function executeTambahanPagu() {
        $this->unit_id = $this->getRequestParameter('unit_id');
        $this->kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
        $this->tambahanPagu = $this->getRequestParameter('tambahPagu');
    }

    public function executeTambahPaguDetail() {
        $tambahPagu = $this->getRequestParameter('tambahanPagu');
        $unit_id = $this->getRequestParameter('unit_id');
        $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
        $query = "update " . sfConfig::get('app_default_schema') . ".master_kegiatan set tambahan_pagu=$tambahPagu where unit_id='$unit_id' and kode_kegiatan='$kode_kegiatan'";
        //print_r($query);exit;
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query);
        $stmt->executeQuery();
        $this->setFlash('berhasil', 'Sukses memberikan tambahan pagu untuk kegiatan ' . $kode_kegiatan);
        budgetLogger::log('Memberikan pagu tambahan untuk dinas ' . $unit_id . ' kegiatan ' . $kode_kegiatan . ' sebesar ' . $tambahPagu);
        return $this->redirect('kegiatan/list?unit_id=' . $unit_id . '&kode_kegiatan=' . $kode_kegiatan);
    }

    public function executeTambahanPaguRevisi() {
        $this->unit_id = $this->getRequestParameter('unit_id');
        $this->kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
        $this->tambahanPagu = $this->getRequestParameter('tambahPagu');
    }

    public function executeTambahPaguDetailRevisi() {
        $tambahPagu = $this->getRequestParameter('tambahanPagu');
        $unit_id = $this->getRequestParameter('unit_id');
        $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');

        //cek pagu dinas
        $c = new Criteria();
        $c->add(UnitKerjaPeer::UNIT_ID, $unit_id);
        $rs_pagu = UnitKerjaPeer::doSelectOne($c);
        $pagu_dinas = $rs_pagu->getPagu();

//        PAKAI TABEL DINAS MASTER KEGIATAN SAJA
//        $c = new Criteria();
//        $c->add(PaguPakPeer::UNIT_ID, $unit_id);
//        $c->add(PaguPakPeer::KEGIATAN_CODE, $kode_kegiatan);
//        $rs_pagu_pak = PaguPakPeer::doSelectOne($c);
//        $pagu_pak = $rs_pagu_pak->getPagu();

        $c = new Criteria();
        $c->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
        $c->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
        $rs_pagu_awal = DinasMasterKegiatanPeer::doSelectOne($c);
        $pagu_awal = $rs_pagu_awal->getTambahanPagu();
//        if (sfConfig::get('app_tahap_edit') != 'pak') {
        $query = "select sum(alokasi_dana) as pagu, sum(tambahan_pagu) as tambahan from " . sfConfig::get('app_default_schema') . ".dinas_master_kegiatan where unit_id='$unit_id'";
        //print_r($query);exit;
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query);
        $rs_total_awal = $stmt->executeQuery();
        $rs_total_awal->next();
        $total_awal = $rs_total_awal->getInt('pagu') + $rs_total_awal->getInt('tambahan');
//ditutup sementara
//            if (($pagu_awal < $tambahPagu) && ($total_awal - $pagu_awal) + $tambahPagu > $pagu_dinas) {
//                $selisih = $pagu_dinas - $total_awal;
//                $this->setFlash('gagal', 'Gagal karena melebihi pagu dinas. Sisa pagu sebesar ' . $selisih);
//                return $this->redirect('kegiatan/listRevisi?unit_id=' . $unit_id . '&kode_kegiatan=' . $kode_kegiatan);
//            }
//        } else {
//
//PAKAI TABEL DINAS MASTER KEGIATAN SAJA
//            $query = "select sum(alokasi_dana) as pagu, sum(tambahan_pagu) as tambahan from " . sfConfig::get('app_default_schema') . ".dinas_master_kegiatan where unit_id='$unit_id'";
//            print_r($query);exit;
//            $con = Propel::getConnection();
//            $stmt = $con->prepareStatement($query);
//            $rs_total_awal = $stmt->executeQuery();
//            $rs_total_awal->next();
//            $total_awal = $pagu_pak + $rs_total_awal->getInt('tambahan');
//
//            if (($pagu_awal < $tambahPagu) && ($total_awal - $pagu_awal) + $tambahPagu > $pagu_pak) {
//                $selisih = $pagu_pak - $total_awal;
//                $this->setFlash('gagal', 'Gagal karena melebihi pagu dinas. Sisa pagu sebesar ' . $selisih);
//                return $this->redirect('kegiatan/listRevisi?unit_id=' . $unit_id . '&kode_kegiatan=' . $kode_kegiatan);
//            }
//        }
        $query = "update " . sfConfig::get('app_default_schema') . ".dinas_master_kegiatan set tambahan_pagu=$tambahPagu where unit_id='$unit_id' and kode_kegiatan='$kode_kegiatan'";
        //print_r($query);exit;
        $stmt = $con->prepareStatement($query);
        $stmt->executeQuery();
        $this->setFlash('berhasil', 'Sukses memberikan tambahan pagu untuk kegiatan ' . $kode_kegiatan);
        budgetLogger::log('Memberikan pagu tambahan (Revisi) untuk dinas ' . $unit_id . ' kegiatan ' . $kode_kegiatan . ' sebesar ' . $tambahPagu);
        return $this->redirect('kegiatan/listRevisi?unit_id=' . $unit_id . '&kode_kegiatan=' . $kode_kegiatan);
    }

    public function executeAjaxEditHeader() {
        $kode_sub = $this->getRequestParameter('kodeSub');
        $unit_id = $this->getRequestParameter('unitId');
        $kode_kegiatan = $this->getRequestParameter('kegiatanCode');

        if ($this->getRequestParameter('act') == 'editHeader') {
            $c_rkam = new Criteria();
            $c_rkam->add(RkaMemberPeer::KODE_SUB, $kode_sub);
            $c_rkam->addAnd(RkaMemberPeer::UNIT_ID, $unit_id);
            $c_rkam->addAnd(RkaMemberPeer::KEGIATAN_CODE, $kode_kegiatan);
            $rkam = RkaMemberPeer::doSelectOne($c_rkam);

            $tamp = $rkam->getKomponenName() . ' ' . $rkam->getDetailName();
            $this->rkam = $rkam->getKodeSub();
            $this->nama = $tamp;
            $this->kodeSub = $kode_sub;
            $this->unit_id = $unit_id;
            $this->kegiatan_code = $kode_kegiatan;
        }
        if ($this->getRequestParameter('act') == 'simpan') {
            $header = $this->getRequestParameter('editHeader_' . $kode_sub);

            $con = Propel::getConnection();
            $con->begin();
            try {
                $sql2 = "update " . sfConfig::get('app_default_schema') . ".rka_member "
                        . "set komponen_name='$header', detail_name='' "
                        . "where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and kode_sub='$kode_sub'";
                $stmt = $con->prepareStatement($sql2);
                $stmt->executeQuery();


                $sql3 = "update  " . sfConfig::get('app_default_schema') . ".rincian_detail "
                        . "set sub='$header' where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and kode_sub='$kode_sub'";
                $stmt2 = $con->prepareStatement($sql3);
                $stmt2->executeQuery();

                $con->commit();
                $this->setFlash('berhasil', "Nama Header sudah berhasil di ubah");
                return $this->redirect('kegiatan/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kode_kegiatan);
            } catch (Exception $e) {
                echo $e;
                $con->rollback();
            }
        }
    }

    public function executeAjaxEditHeaderRevisi() {
        $kode_sub = $this->getRequestParameter('kodeSub');
        $unit_id = $this->getRequestParameter('unitId');
        $kode_kegiatan = $this->getRequestParameter('kegiatanCode');

        if ($this->getRequestParameter('act') == 'editHeader') {
            $c_rkam = new Criteria();
            $c_rkam->add(DinasRkaMemberPeer::KODE_SUB, $kode_sub);
            $c_rkam->addAnd(DinasRkaMemberPeer::UNIT_ID, $unit_id);
            $c_rkam->addAnd(DinasRkaMemberPeer::KEGIATAN_CODE, $kode_kegiatan);
            $rkam = DinasRkaMemberPeer::doSelectOne($c_rkam);

            $tamp = $rkam->getKomponenName() . ' ' . $rkam->getDetailName();
            $this->rkam = $rkam->getKodeSub();
            $this->nama = $tamp;
            $this->kodeSub = $kode_sub;
            $this->unit_id = $unit_id;
            $this->kegiatan_code = $kode_kegiatan;
        }
        if ($this->getRequestParameter('act') == 'simpan') {
            $header = $this->getRequestParameter('editHeader_' . $kode_sub);

            $con = Propel::getConnection();
            $con->begin();
            try {
                $sql2 = "update " . sfConfig::get('app_default_schema') . ".dinas_rka_member "
                        . "set komponen_name='$header', detail_name='' "
                        . "where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and kode_sub='$kode_sub'";
                $stmt = $con->prepareStatement($sql2);
                $stmt->executeQuery();


                $sql3 = "update  " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail "
                        . "set sub='$header' where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and kode_sub='$kode_sub'";
                $stmt2 = $con->prepareStatement($sql3);
                $stmt2->executeQuery();

                $con->commit();
                $this->setFlash('berhasil', "Nama Header sudah berhasil di ubah");
                return $this->redirect('kegiatan/editRevisi?unit_id=' . $unit_id . '&kode_kegiatan=' . $kode_kegiatan);
            } catch (Exception $e) {
                echo $e;
                $con->rollback();
            }
        }
    }

    public function executeHapusHeaderPekerjaans() {
        if ($this->getRequestParameter('id')) {

            $kode_sub = $this->getRequestParameter('no');
            $unit_id = $this->getRequestParameter('unit');
            $kegiatan_code = $this->getRequestParameter('kegiatan');

            $con = Propel::getConnection();
            $con->begin();
            try {
                $sql3 = "update  " . sfConfig::get('app_default_schema') . ".rincian_detail set sub='', kode_sub='' where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and kode_sub='$kode_sub'";
                //print_r($sql3);exit;
                $stmt2 = $con->prepareStatement($sql3);
                $stmt2->executeQuery();
                budgetLogger::log('Menghapus Header dari Unit=' . $unit_id . ' dan kegiatan=' . $kegiatan_code . ' dan kode_sub=' . $kode_sub);
                $con->commit();
                $this->setFlash('berhasil', "Header Sudah Berhasil Dihapus");
                return $this->redirect('kegiatan/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code . '.html');
            } catch (Exception $e) {
                echo $e;
                $con->rollback();
            }
        }
    }

    public function executeLockkomponen() {
        $this->unit_id = $unit_id = $this->getRequestParameter('unit_id');
        $this->kegiatan = $kegiatan_code = $this->getRequestParameter('kegiatan_code');
        $this->no = $detail_no = $this->getRequestParameter('detail_no');
        $this->nilai_gembok = $nilai_gembok = $this->getRequestParameter('nilai_gembok');

        $con = Propel::getConnection();

        $con->begin();
        try {
            if ($nilai_gembok == 0) {
                $queryUpdate = "update " . sfConfig::get('app_default_schema') . ".rincian_detail set lock_subtitle='LOCK' where unit_id = '$unit_id' and kegiatan_code = '$kegiatan_code' and detail_no = '$detail_no'";
            } else {
                $queryUpdate = "update " . sfConfig::get('app_default_schema') . ".rincian_detail set lock_subtitle=' ' where unit_id = '$unit_id' and kegiatan_code = '$kegiatan_code' and detail_no = '$detail_no' and rekening_code <> '5.2.1.04.01' ";
            }
            $stmt = $con->prepareStatement($queryUpdate);
            $stmt->executeQuery();

            budgetLogger::log('Membuka/menutup komponen dari Unit=' . $unit_id . ' dan kegiatan=' . $kegiatan_code . ' dan detail_no=' . $detail_no);
            $con->commit();

            if ($nilai_gembok == 0) {
                historyUserLog::lock_per_komponen($unit_id, $kegiatan_code, $detail_no);
            } else {
                historyUserLog::unlock_per_komponen($unit_id, $kegiatan_code, $detail_no);
            }
        } catch (Exception $e) {
            echo $e;
            $con->rollback();
        }
    }

    public function executeLockhargadasar() {
        $sql = "update " . sfConfig::get('app_default_schema') . ".rincian_detail "
                . "set status_lelang='" . $this->getRequestParameter('act')
                . "' where unit_id='" . $this->getRequestParameter('unit_id')
                . "' and kegiatan_code='" . $this->getRequestParameter('kegiatan_code')
                . "' and detail_no= " . $this->getRequestParameter('detail_no');

        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($sql);
        budgetLogger::log('Mengunci harga dasar untuk lelang pada unit id =' . $this->act = $this->getRequestParameter('unit_id') . ' kode kegiatan=' . $this->act = $this->getRequestParameter('kegiatan_code') .
                ' dan detail no=' . $this->act = $this->getRequestParameter('detail_no'));
        $stmt->executeQuery();

        $this->no = $this->getRequestParameter('detail_no');
        $this->unit_id = $this->getRequestParameter('unit_id');
        $this->id = $this->getRequestParameter('id');
        $this->kegiatan = $this->getRequestParameter('kegiatan_code');

        $this->ajax_id = $this->getRequestParameter('ajax_id');
        $this->act = $this->getRequestParameter('act');
    }

    public function executeLockhargadasarrevisi() {
        $sql = "update " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail "
                . "set status_lelang='" . $this->getRequestParameter('act')
                . "' where unit_id='" . $this->getRequestParameter('unit_id')
                . "' and kegiatan_code='" . $this->getRequestParameter('kegiatan_code')
                . "' and detail_no= " . $this->getRequestParameter('detail_no');

        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($sql);
        budgetLogger::log('Mengunci harga dasar (eRevisi) untuk lelang pada unit id =' . $this->act = $this->getRequestParameter('unit_id') . ' kode kegiatan=' . $this->act = $this->getRequestParameter('kegiatan_code') .
                ' dan detail no=' . $this->act = $this->getRequestParameter('detail_no'));
        $stmt->executeQuery();

        $this->no = $this->getRequestParameter('detail_no');
        $this->unit_id = $this->getRequestParameter('unit_id');
        $this->id = $this->getRequestParameter('id');
        $this->kegiatan = $this->getRequestParameter('kegiatan_code');

        $this->ajax_id = $this->getRequestParameter('ajax_id');
        $this->act = $this->getRequestParameter('act');
    }

    public function executeGantiRekening() {
        $unit_id = $this->getRequestParameter('unit_id');
        $kegiatan_code = $this->getRequestParameter('kegiatan_code');
        $detail_no = $this->getRequestParameter('detail_no');
        $rekening_code = $this->getRequestParameter('rek_' . $detail_no);

        $totNilaiSwakelola = 0;
        $totNilaiKontrak = 0;
        $totNilaiAlokasi = 0;

        if (sfConfig::get('app_tahap_edit') <> 'murni') {
            $rd = new RincianDetail();
            $totNilaiSwakelola = $rd->getCekNilaiSwakelolaDelivery2($unit_id, $kegiatan_code, $detail_no);
            $totNilaiKontrak = $rd->getCekNilaiKontrakDelivery2($unit_id, $kegiatan_code, $detail_no);
            $totNilaiAlokasi = $rd->getCekNilaiAlokasiProject($unit_id, $kegiatan_code, $detail_no);
        }

        if ($rekening_code && $totNilaiAlokasi == 0 && $totNilaiKontrak == 0 && $totNilaiSwakelola == 0) {
            $con = Propel::getConnection();

            $con->begin();
            try {
                $queryUpdate = "update " . sfConfig::get('app_default_schema') . ".rincian_detail "
                        . "set rekening_code = '$rekening_code', ob = true  "
                        . "where unit_id = '$unit_id' and kegiatan_code = '$kegiatan_code' and detail_no = '$detail_no'";
                $stmt = $con->prepareStatement($queryUpdate);
                $stmt->executeQuery();

                budgetLogger::log('Merubah kode rekening menjadi ' . $rekening_code . ' dari Unit=' . $unit_id . ' dan kegiatan=' . $kegiatan_code . ' dan detail_no=' . $detail_no);
                $con->commit();
            } catch (Exception $e) {
                echo $e;
                $con->rollback();
            }
        }
    }

    public function executeGantiRekeningRevisi() {
        $unit_id = $this->getRequestParameter('unit_id');
        $kegiatan_code = $this->getRequestParameter('kegiatan_code');
        $detail_no = $this->getRequestParameter('detail_no');
        $rekening_code = $this->getRequestParameter('rek_' . $detail_no);

        $totNilaiRealisasi = 0;
        $totNilaiKontrak = 0;
        $totNilaiSwakelola = 0;
        $nilairealisasi = 0;

        if (sfConfig::get('app_tahap_edit') <> 'murni') {
                    // itung realisasi                                       
                                        
                                         $rs1_rinciandetail = new DinasRincianDetail;
                                        
                                        $totNilaiRealisasi = $rs1_rinciandetail->getCekRealisasi($unit_id, $kegiatan_code, $detail_no);
                                        $totNilaiKontrak = $rs1_rinciandetail->getCekNilaiKontrakDelivery2($unit_id, $kegiatan_code, $detail_no);
                                        $totNilaiSwakelola = $rs1_rinciandetail->getCekNilaiSwakelolaDelivery2($unit_id, $kegiatan_code, $detail_no);
                                        if ($totNilaiRealisasi > 0) {
                                            $nilairealisasi = $totNilaiRealisasi;
                                        } else if ($totNilaiKontrak > 0) {
                                            $nilairealisasi = $totNilaiKontrak;
                                        } else if ($totNilaiSwakelola > 0) {
                                            $nilairealisasi = $totNilaiSwakelola;
                                        } else if ($totNilaiSwakelola > 0 && $totNilaiKontrak > 0) {
                                            $nilairealisasi = $totNilaiSwakelola;
                                        }
        if ($rekening_code && $totNilaiAlokasi == 0 && $totNilaiKontrak == 0 && $totNilaiSwakelola == 0 && $totNilaiRealisasi > 0) {
        $con = Propel::getConnection();
        $con->begin();
            try {
                $queryUpdate = "update " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail "
                        . "set rekening_code = '$rekening_code', ob = true  "
                        . "where unit_id = '$unit_id' and kegiatan_code = '$kegiatan_code' and detail_no = '$detail_no'";
                $stmt = $con->prepareStatement($queryUpdate);
                $stmt->executeQuery();

                $con->commit();
            } catch (Exception $e) {
                echo $e;
                $con->rollback();
            }
        } else {
            $con = Propel::getConnection();
            $con->begin();
            try {
                $queryUpdate = "update " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail "
                        . "set rekening_code = '$rekening_code'"
                        . "where unit_id = '$unit_id' and kegiatan_code = '$kegiatan_code' and detail_no = '$detail_no'";
                $stmt = $con->prepareStatement($queryUpdate);
                $stmt->executeQuery();

                $con->commit();
            } catch (Exception $e) {
                echo $e;
                $con->rollback();
            }
        }
    }
    else
    {
        //jika murni
        $con = Propel::getConnection();
            $con->begin();
            try {
                $queryUpdate = "update " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail "
                        . "set rekening_code = '$rekening_code'"
                        . "where unit_id = '$unit_id' and kegiatan_code = '$kegiatan_code' and detail_no = '$detail_no'";
                $stmt = $con->prepareStatement($queryUpdate);
                $stmt->executeQuery();

                $con->commit();
            } catch (Exception $e) {
                echo $e;
                $con->rollback();
            }
    }
}

    public function executeBukaKomponenAll() {
        $unit_id = $this->getRequestParameter('unit_id');
        $keg_code = $this->getRequestParameter('kode_kegiatan');

        $con = Propel::getConnection();
        $con->begin();
        try {
            $query1 = "update " . sfConfig::get('app_default_schema') . ".rincian_detail "
                    . "set lock_subtitle=' ' "
                    . "where status_hapus = false and kegiatan_code = '$keg_code' and unit_id = '$unit_id' and tahun ='" . sfConfig::get('app_tahun_default') . "' and rekening_code <> '5.2.1.04.01'";
            $stmt = $con->prepareStatement($query1);
            $stmt->executeQuery();
            $con->commit();
            $this->setFlash('berhasil', 'Sukses membuka komponen kegiatan ' . $keg_code);

            historyUserLog::unlock_komponen_kegiatan($unit_id, $keg_code);
        } catch (Exception $e) {
            $con->rollback();
            $this->setFlash('gagal', 'Gagal membuka komponen karena ' . $e->getMessage());
        }
        budgetLogger::log('Membuka komponen pada unit ' . $unit_id . ' dan kegiatan ' . $keg_code);
        return $this->redirect('kegiatan/list?unit_id=' . $unit_id);
    }

    public function executeTutupKomponenAll() {
        $unit_id = $this->getRequestParameter('unit_id');
        $keg_code = $this->getRequestParameter('kode_kegiatan');

        $con = Propel::getConnection();
        $con->begin();
        try {
            $query1 = "update " . sfConfig::get('app_default_schema') . ".rincian_detail "
                    . "set lock_subtitle='LOCK' "
                    . "where  status_hapus = false and kegiatan_code = '$keg_code' and unit_id = '$unit_id' and tahun ='" . sfConfig::get('app_tahun_default') . "' and rekening_code <> '5.2.1.04.01'";
            $stmt = $con->prepareStatement($query1);
            $stmt->executeQuery();
            $con->commit();
            $this->setFlash('berhasil', 'Sukses menutup komponen kegiatan ' . $keg_code);

            historyUserLog::lock_komponen_kegiatan($unit_id, $keg_code);
        } catch (Exception $e) {
            $con->rollback();
            $this->setFlash('gagal', 'Gagal membuka komponen karena ' . $e->getMessage());
        }
        budgetLogger::log('Menutup komponen pada unit ' . $unit_id . ' dan kegiatan ' . $keg_code);
        return $this->redirect('kegiatan/list?unit_id=' . $unit_id);
    }

    public function executeBukaKomponenMurni() {
        $unit_id = $this->getRequestParameter('unit');
        $kegiatan_code = $this->getRequestParameter('kegiatan');
        $detail_no = $this->getRequestParameter('id');

        $detail_kegiatan = $unit_id . '.' . $kegiatan_code . '.' . $detail_no;

        $con = Propel::getConnection();

        $query =
        "INSERT INTO " . sfConfig::get('app_default_schema') . ".buka_komponen_murni
        VALUES ('$detail_kegiatan')";
        $stmt = $con->prepareStatement($query);
        $stmt->executeQuery();
       

        $query1 =
        "DELETE FROM " . sfConfig::get('app_default_schema') . ".kunci_komponen_murni
        where detail_kegiatan='$detail_kegiatan'";
        $stmt1 = $con->prepareStatement($query1);
        $stmt1->executeQuery();
        $con->commit();

        $this->setFlash('berhasil', 'Berhasil buka komponen');
        return $this->redirect("kegiatan/editRevisi?unit_id=$unit_id&kode_kegiatan=$kegiatan_code");
    }

    public function executeKunciKomponenMurni() {
        $unit_id = $this->getRequestParameter('unit');
        $kegiatan_code = $this->getRequestParameter('kegiatan');
        $detail_no = $this->getRequestParameter('id');

        $detail_kegiatan = $unit_id . '.' . $kegiatan_code . '.' . $detail_no;

        $con = Propel::getConnection();

        $query =
        "INSERT INTO " . sfConfig::get('app_default_schema') . ".kunci_komponen_murni
        VALUES ('$detail_kegiatan')";
        $stmt = $con->prepareStatement($query);
        $stmt->executeQuery();
       
        $query1 =
        "DELETE FROM " . sfConfig::get('app_default_schema') . ".buka_komponen_murni
        where detail_kegiatan='$detail_kegiatan'";
        $stmt1 = $con->prepareStatement($query1);
        $stmt1->executeQuery();
        $con->commit();

        $this->setFlash('berhasil', 'Berhasil kunci komponen');
        return $this->redirect("kegiatan/editRevisi?unit_id=$unit_id&kode_kegiatan=$kegiatan_code");
    }

    public function executeBukaKomponenMurniPerKegiatan() {
        $unit_id = $this->getRequestParameter('unit_id');
        $kegiatan_code = $this->getRequestParameter('kegiatan_code');

        $con = Propel::getConnection();

        $query =
        "INSERT INTO " . sfConfig::get('app_default_schema') . ".buka_komponen_murni
        (SELECT detail_kegiatan FROM " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail
        WHERE kegiatan_code = '$kegiatan_code'
        AND unit_id = '$unit_id'
        AND status_hapus = FALSE)";
        $stmt = $con->prepareStatement($query);
        $stmt->executeQuery();
        $con->commit();

        $this->setFlash('berhasil', "Berhasil buka komponen untuk kegiatan $kegiatan_code");
        return $this->redirect('kegiatan/listRevisi?unit_id=' . $unit_id);
    }

    public function executeBatalRevisi() {
        $unit_id = $this->getRequestParameter('unit_id');
        $kegiatan_code = $this->getRequestParameter('kegiatan_code');
        $tahap = sfConfig::get('app_tahap_edit');
        $tahap_angka = DinasMasterKegiatanPeer::getTahapKegiatan($unit_id, $kegiatan_code);
        $tahap_detail = DinasMasterKegiatanPeer::getTahapDetail($unit_id, $kegiatan_code);

        $query =
        "SELECT max(id) AS id
        FROM " . sfConfig::get('app_default_schema') . ".deskripsi_resume_rapat
        WHERE unit_id = '$unit_id' AND tahap = $tahap_angka";
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query);
        $rs = $stmt->executeQuery();
        if($rs->next()) {
            $id_max = $rs->getString('id');
        }

        $con = Propel::getConnection();
        $con->begin();
        try {
            $query =
            "DELETE FROM " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail
            WHERE unit_id = '$unit_id'
            AND kegiatan_code = '$kegiatan_code'";
            $stmt = $con->prepareStatement($query);
            $stmt->executeQuery();

            $query =
            "INSERT INTO " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail
            (SELECT *
            FROM " . sfConfig::get('app_default_schema') . ".rincian_detail
            WHERE unit_id = '$unit_id'
            AND kegiatan_code = '$kegiatan_code')";
            $stmt = $con->prepareStatement($query);
            $stmt->executeQuery();

            $query =
            "UPDATE " . sfConfig::get('app_default_schema') . ".dinas_master_kegiatan
            SET
            status_level=0,
            is_pernah_rka=false,
            is_bappeko_setuju=false,
            is_tapd_setuju=false,
            is_penyelia_setuju=false,
            is_bagian_hukum_setuju=false,
            is_inspektorat_setuju=false,
            is_badan_kepegawaian_setuju=false,
            is_lppa_setuju=false,
            is_bagian_organisasi_setuju=false,
            catatan='',
            catatan_pembahasan='',
            catatan_bpkpd='',
            catatan_penyelia='',
            catatan_bappeko='',
            catatan_bagian_hukum='',
            catatan_inspektorat='',
            catatan_badan_kepegawaian='',
            catatan_lppa='',
            catatan_bagian_organisasi='',
            tahap='$tahap_detail',
            tambahan_pagu=0,
            ubah_f1_dinas=NULL,
            ubah_f1_peneliti = NULL,
            sisa_lelang_dinas = NULL,
            sisa_lelang_peneliti = NULL,
            catatan_ubah_f1_dinas = '',
            catatan_sisa_lelang_peneliti = '',
            verifikasi_bpkpd = '',
            verifikasi_bappeko = '',
            verifikasi_penyelia = '',
            verifikasi_bagian_hukum = '',
            verifikasi_inspektorat = '',
            verifikasi_badan_kepegawaian = '',
            verifikasi_lppa = '',
            verifikasi_bagian_organisasi = ''
            WHERE unit_id = '$unit_id'
            AND kode_kegiatan IN ('$kegiatan_code')";
            $stmt = $con->prepareStatement($query);
            $stmt->executeQuery();

            $query =
            "UPDATE " . sfConfig::get('app_default_schema') . ".dinas_rincian
            SET rincian_level = 3, lock = true
            WHERE unit_id = '$unit_id'
            AND kegiatan_code IN ('$kegiatan_code')";
            $stmt = $con->prepareStatement($query);
            $stmt->executeQuery();

            $query =
            "UPDATE " . sfConfig::get('app_default_schema') . ".log_perubahan_revisi
            SET status = -1
            WHERE unit_id = '$unit_id'
            AND kegiatan_code IN ('$kegiatan_code')
            AND tahap = $tahap_angka";
            $stmt = $con->prepareStatement($query);
            $stmt->executeQuery();

            $query =
            "UPDATE " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail
            SET
            status_level=0,
            is_bappeko_setuju=false,
            is_tapd_setuju=false,
            is_penyelia_setuju=false,
            is_bagian_hukum_setuju=false,
            is_inspektorat_setuju=false,
            is_badan_kepegawaian_setuju=false,
            is_lppa_setuju=false,
            is_bagian_organisasi_setuju=false,
            note_skpd='',
            note_peneliti='',
            note_tapd='',
            note_bappeko='',
            status_sisipan=false
            WHERE unit_id = '$unit_id'
            AND kegiatan_code IN ('$kegiatan_code')";
            $stmt = $con->prepareStatement($query);
            $stmt->executeQuery();

            if(isset($id_max)) {
                $query =
                "DELETE FROM " . sfConfig::get('app_default_schema') . ".deskripsi_resume_rapat_rekening
                WHERE id_deskripsi_resume_rapat = $id_max
                AND kegiatan_code = '$kegiatan_code'";
                $stmt = $con->prepareStatement($query);
                $stmt->executeQuery();

                $query =
                "DELETE FROM " . sfConfig::get('app_default_schema') . ".deskripsi_resume_rapat_kegiatan
                WHERE id_deskripsi_resume_rapat = $id_max
                AND kegiatan_code = '$kegiatan_code'";
                $stmt = $con->prepareStatement($query);
                $stmt->executeQuery();
            }
        } catch(Exception $e) {
            $con->rollback();
            $this->setFlash('gagal', 'Gagal membatalkan revisi karena: ' . $e->getMessage());
            return $this->redirect('kegiatan/list?unit_id=' . $unit_id);
        }
        $con->commit();
        budgetLogger::log('Membatalkan revisi SKPD ' . $unit_id . ' dan kegiatan ' . $kegiatan_code);

        $this->setFlash('berhasil', 'Berhasil membatalkan revisi untuk kegiatan ' . $kegiatan_code);
        return $this->redirect('kegiatan/listRevisi?unit_id=' . $unit_id);
    }

    public function executeGetUserId(){
        $unit_id = $this->getRequestParameter('unit_id');
        $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');

        if(strlen($unit_id) < 4){
            $unit_id = "0"."$unit_id";
        }

        // die($unit_id);
        $c = new Criteria();
        $c->add(SchemaAksesV2Peer::USER_ID, '%'.$unit_id, Criteria::ILIKE);
        $c->addAnd(SchemaAksesV2Peer::LEVEL_ID, 1, Criteria::EQUAL);
        $rs_user = SchemaAksesV2Peer::doSelect($c);
        $data['html'] = '<option value="">--Pilih User--</option>';
        foreach($rs_user as $key => $value) {
            $data['html'] .= '<option value="'.$value->getUserId().'">'.$value->getUserId().'</option>';
        }

        return $this->renderText(json_encode($data));
    }

    public function executeAmbilKegiatan(){
        try {
            $unit_id = $this->unit_id = $this->getRequestParameter('unit_id');
            $kode_kegiatan = $this->kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
            $user_id = $this->user_id = $this->getRequestParameter('user_id');
            if(strlen($unit_id) < 4){
                $unit_id = "0"."$unit_id";
            }
            $query = "update " . sfConfig::get('app_default_schema') . ".master_kegiatan set user_id = '$user_id' where unit_id='$unit_id' and kode_kegiatan='$kode_kegiatan'";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $stmt->executeQuery();
            $query = "update " . sfConfig::get('app_default_schema') . ".dinas_master_kegiatan set user_id = '$user_id' where unit_id='$unit_id' and kode_kegiatan='$kode_kegiatan'";
            $stmt = $con->prepareStatement($query);
            budgetLogger::log('kode Kegiatan ' . $kode_kegiatan . ' dari unit id ' . $unit_id . ' diambil oleh ' . $user_id);
            $stmt->executeQuery();
            $this->setFlash('berhasil', "Kegiatan $kode_kegiatan berhasil di ambil oleh ( $user_id )");
        } catch (Exception $exc) {
            $this->setFlash('gagal', $exc->getTraceAsString());
        }
        return $this->redirect('kegiatan/listRevisi?menu=Belanja&unit_id=' . $unit_id . '&kode_kegiatan=' . $kode_kegiatan);
    }

    public function executeLepasKegiatan() {//dj
        try {
            $unit_id = $this->unit_id = $this->getRequestParameter('unit_id');
            $kode_kegiatan = $this->kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
            $user_id = $this->user_id = $this->getRequestParameter('user_id');
            $query = "update " . sfConfig::get('app_default_schema') . ".master_kegiatan set user_id = '' where unit_id='$unit_id' and kode_kegiatan='$kode_kegiatan' and user_id='$user_id'";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $stmt->executeQuery();
            $query = "update " . sfConfig::get('app_default_schema') . ".dinas_master_kegiatan set user_id = '' where unit_id='$unit_id' and kode_kegiatan='$kode_kegiatan' and user_id='$user_id'";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            budgetLogger::log('kode Kegiatan ' . $kode_kegiatan . ' dari unit id ' . $unit_id . ' dilepas  oleh ' . $user_id . ' melalui superadmin');
            $stmt->executeQuery();
            $this->setFlash('berhasil', "Kegiatan $kode_kegiatan berhasil dilepas oleh ( $user_id )");
        } catch (Exception $exc) {
            $this->setFlash('gagal', $exc->getTraceAsString());
        }
        return $this->redirect('kegiatan/listRevisi?menu=Belanja&unit_id=' . $unit_id . '&kode_kegiatan=' . $kode_kegiatan);
// echo $unit_id.''.$kode_kegiatan;
    }

    public function executeGetKodeKegiatanByPd(){
        $unit_id = $this->getRequestParameter('unit_id');
        $menu = $this->getRequestParameter('menu');
        $tahap = $this->getRequestParameter('tahap');

        if(strlen($unit_id) < 4){
            $unit_id = "0"."$unit_id";
        }

        // die($unit_id);
        if($tahap == 'murni'){
            $c = new Criteria();
            $c->add(MurniMasterKegiatanPeer::UNIT_ID, $unit_id, Criteria::EQUAL);
            if($menu == 'Pendapatan'){
                $c->add(MurniMasterKegiatanPeer::IS_BTL,TRUE);
            }else{
                $c->add(MurniMasterKegiatanPeer::IS_BTL,FALSE);
            }
            $c->addAscendingOrderByColumn('CAST('.MurniMasterKegiatanPeer::KODE_KEGIATAN.' AS INT)');
            $rs_user = MurniMasterKegiatanPeer::doSelect($c);
        }else{
            $c = new Criteria();
            $c->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id, Criteria::EQUAL);
            if($menu == 'Pendapatan'){
                $c->add(DinasMasterKegiatanPeer::IS_BTL,TRUE);
            }else{
                $c->add(DinasMasterKegiatanPeer::IS_BTL,FALSE);
            }
            $c->addAscendingOrderByColumn('CAST('.DinasMasterKegiatanPeer::KODE_KEGIATAN.' AS INT)');
            $rs_user = DinasMasterKegiatanPeer::doSelect($c);
        }

        $data['html'] = '<option value="">--Pilih Kode--</option>';
        foreach($rs_user as $key => $value) {
            $data['html'] .= '<option value="'.$value->getKodeKegiatan().'">'.$value->getKodeKegiatan().'</option>';
        }

        return $this->renderText(json_encode($data));
    }
    
    public function executeGetUserByPd(){
        $unit_id = $this->getRequestParameter('unit_id');
        $menu = $this->getRequestParameter('menu');
        $tahap = $this->getRequestParameter('tahap');

        if(strlen($unit_id) < 4){
            $unit_id = "0"."$unit_id";
        }

        // die($unit_id);
        if($tahap == 'murni'){
            $c = new Criteria();
            $c->add(MurniMasterKegiatanPeer::UNIT_ID, $unit_id, Criteria::EQUAL);
            if($menu == 'Pendapatan'){
                $c->add(MurniMasterKegiatanPeer::IS_BTL,TRUE);
            }else{
                $c->add(MurniMasterKegiatanPeer::IS_BTL,FALSE);
            }
            $c->add(MurniMasterKegiatanPeer::USER_ID, NULL, Criteria::ISNOTNULL);
            $c->setDistinct(MurniMasterKegiatanPeer::USER_ID);
            $c->addAscendingOrderByColumn(MurniMasterKegiatanPeer::USER_ID);
            $rs_user = MurniMasterKegiatanPeer::doSelect($c);
        }else{
            $c = new Criteria();
            $c->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id, Criteria::EQUAL);
            if($menu == 'Pendapatan'){
                $c->add(DinasMasterKegiatanPeer::IS_BTL,TRUE);
                $btl = " is_btl=true ";
            }else{
                $c->add(DinasMasterKegiatanPeer::IS_BTL,FALSE);
                $btl = " is_btl=false ";
            }
            $c->add(DinasMasterKegiatanPeer::USER_ID, NULL, Criteria::ISNOTNULL);
            $c->setDistinct(DinasMasterKegiatanPeer::USER_ID);
            $c->addAscendingOrderByColumn(DinasMasterKegiatanPeer::USER_ID);
            $rs_user = DinasMasterKegiatanPeer::doSelect($c);

            // $query = "select distinct user_id from ebudget.dinas_master_kegiatan where unit_id='".$this->filters['unit_id']."' and user_id is not null and ".$btl." order by user_id asc";
            // $con = Propel::getConnection();
            // $con->begin();
            // $load_user_id = $con->prepareStatement($query);
            // $rs = $load_user_id->executeQuery();
        }
        $temp = '';
        $data['html'] = '<option value="">--Pilih User Id--</option>';
        foreach($rs_user as $key => $value) {
            if($value->getUserId() != $temp){
                $data['html'] .= '<option value="'.$value->getUserId().'">'.$value->getUserId().'</option>';
            }
            $temp = $value->getUserId();
        }

        // while($rs->next()){
        //     $data['html'] .= '<option value="'.$rs->getString('user_id').'">'.$rs->getString('user_id').'</option>';
        // }

        return $this->renderText(json_encode($data));
    }

}
