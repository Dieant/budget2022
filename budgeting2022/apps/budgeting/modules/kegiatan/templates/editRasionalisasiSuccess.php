<?php use_helper('I18N', 'Date', 'Url', 'Javascript', 'Form', 'Object', 'Number', 'Validation') ?>
<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Edit Rasionalisasi</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Lembar Kerja</a></li>
          <li class="breadcrumb-item active">Edit Rasionalisasi</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 stretch-card">
                <div class="card">
                    <div class="card-body table-responsive p-0">
                        <table class="table table-hover">
                            <thead class="head_peach">
                                <tr>
                                    <th><b>Detail Kegiatan</b></th>
                                    <th><b>Nama Komponen</b></th>
                                    <th><b>Satuan</b></th>
                                    <th><b>Keterangan Koefisien</b></th>
                                    <th><b>Harga</b></th>
                                    <th><b>Nilai Anggaran</b></th>
                                    <th><b>Nilai Rasionalisasi</b></th>
                                    <th><b>Action</b></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $counter = 0;
                                while($rs_rinciandetail->next()) {
                                    $counter++;
                                    $nama = $rs_rinciandetail->getString('komponen_name');
                                    $style = '';
                                    echo form_tag('kegiatan/prosesUbahRasionalisasi');           
                                    echo "<tr $style>";
                                    echo "<td>".$rs_rinciandetail->getString('detail_kegiatan')."</td>";
                                    echo "<td>".$rs_rinciandetail->getString('komponen_name')." ".$rs_rinciandetail->getString('detail_name')."</td>";
                                    echo "<td>".$rs_rinciandetail->getString('satuan')."</td>";
                                    echo "<td>".$rs_rinciandetail->getString('keterangan_koefisien')."</td>";
                                    echo "<td align='right'>".number_format($rs_rinciandetail->getString('komponen_harga'), 0, ',', '.')."</td>";
                                    echo "<td align='right'>".number_format($rs_rinciandetail->getString('nilai_anggaran'), 0, ',', '.')."</td>";
                                    echo "<td>".input_tag('nilai_rasionalisasi', $rs_rinciandetail->getString('anggaran_rasionalisasi'))."</td>";
                                    echo "<td>";
                                        echo input_hidden_tag('detail_no', $rs_rinciandetail->getString('detail_no'));
                                        echo input_hidden_tag('unit_id', $unit_id);
                                        echo input_hidden_tag('kode_kegiatan', $kode_kegiatan);
                                        echo submit_tag('Simpan', array('name' => 'proses', 'class' => 'btn btn-outline-primary btn-sm')) . '&nbsp;';
                                    echo "</td>";
                                    echo "</tr>";
                                    echo '</form>';
                                }
                                ?>
                                <?php if($counter <= 0): ?>
                                    <tr><td colspan="16" align="center">Tidak ada komponen yang bisa ditampilkan</td></tr>
                                <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $("#cekSemua").change(function () {
        $(".cek").prop('checked', $(this).prop("checked"));
    });
</script>