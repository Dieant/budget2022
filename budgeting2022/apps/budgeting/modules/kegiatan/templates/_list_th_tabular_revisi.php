<th class="align-middle">
    <?php echo __('ID') ?>
</th>
<th class="align-middle">
    <?php echo __('Nama Sub Kegiatan') ?>
</th>
<?php if (sfConfig::get('app_tahap_edit') <> 'pak' && sfConfig::get('app_tahap_edit') <> 'murni') { ?>
   <th class="align-middle">
    <?php echo __('Semula') ?>
</th>
<?php } ?>
<?php if (sfConfig::get('app_tahap_edit') == 'murni') { ?>
    <th class="align-middle">
        <?php  echo __('Pagu') ?>
    </th>
<?php } ?>
<?php if (sfConfig::get('app_tahap_edit') == 'pak') { ?>
    <th id="sf_admin_list_th_alokasidana">
        <?php  echo __('Revisi 6') ?>
    </th>
<?php } ?>
<?php if (sfConfig::get('app_tahap_edit') == 'pak') { ?>
    <th class="align-middle">
        <?php echo __('Pagu KUA PPAS') ?>
    </th>
<?php } ?>
<?php if (sfConfig::get('app_tahap_edit') != 'pak'  && sfConfig::get('app_tahap_edit') <> 'murni' ) { ?>
<th class="align-middle">
    <?php echo __('Menjadi') ?>
</th>
<?php } ?>
<th class="align-middle">
    <?php echo __('Rincian') ?>
</th>
<th class="align-middle">
    <?php echo __('Selisih') ?>
</th>
<?php if (sfConfig::get('app_tahap_edit') == 'pak' || sfConfig::get('app_tahap_edit') == 'murni') { ?>
<th class="align-middle">
    <?php echo __('Penyesuaian Pagu') ?>
</th>
<?php } ?>
<th class="align-middle">
    <?php echo __('Posisi') ?>
</th>
<th class="align-middle">
    <?php echo __('Approval') ?>
</th>
<th class="align-middle">
    <?php echo __('Tahap') ?>
</th>
