<?php use_helper('I18N', 'Date', 'Object', 'Javascript', 'Validation') ?>
<?php
    //$menu = $_GET['menu'];
    $menu = $sf_params->get('menu');
    //$menu = $this->getRequestParameter('menu');
    if (sfConfig::get('app_tahap_edit') == 'murni') {
        if($menu == 'Pendapatan')
        $nama_sistem = 'Pendapatan Daerah';
        else
        $nama_sistem = 'Belanja Daerah';
    } elseif (sfConfig::get('app_tahap_edit') == 'pak') {
        $nama_sistem = 'PAK';
    } elseif (sfConfig::get('app_tahap_edit') == 'penyesuaian') {
        $nama_sistem = 'Penyesuaian';
    }else {
        $nama_sistem = 'Revisi';
    }
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Sub Kegiatan - (<?php echo $nama_sistem ?>)</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Lembar Kerja</a></li>
                    <li class="breadcrumb-item active"><?php echo $nama_sistem ?></li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<!-- Main content -->
<section class="content">
    <?php include_partial('kegiatan/list_messages'); ?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="fas fa-filter"></i> Filter
                        </h3>
                    </div>
                    <div class="card-body">
                        <?php echo form_tag('kegiatan/listRevisi?menu='.$menu, array('method' => 'get', 'class' => 'form-horizontal')) ?>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Perangkat Daerah</label>
                                    <?php
                                    $c = new Criteria();
                                    $c->addAscendingOrderByColumn(UnitKerjaPeer::UNIT_NAME);
                                    $v = UnitKerjaPeer::doSelect($c);
                                    echo select_tag('filters[unit_id]', objects_for_select($v, 'getUnitId', 'getUnitName', isset($filters['unit_id']) ? $filters['unit_id'] : null, array('include_custom' => '--Pilih Perangkat Daerah--')), Array('class' => 'js-example-basic-single form-control select2', 'onchange'=>'loadKodeKegiatan(this);loadUserId(this)'));
                                ?>
                                </div>
                            </div>
                            <div class="col-md-1">
                                <div class="form-group">
                                    <label>ID</label>
                                    <select name="filters[id]" id="filters_id" class="form-control js-example-basic-single select2">
                                        <option value="">--Pilih Kode--</option>
                                        <?php
                                            if(!empty($load_id)){
                                                foreach($load_id as $key=>$value){
                                                    $text = '';
                                                    if(isset($filters['id']) && $filters['id']==$value->getKodeKegiatan()){
                                                        $text = 'selected';
                                                    }
                                                    echo '<option value="'.$value->getKodeKegiatan().'" '.$text.'>'.$value->getKodeKegiatan().'</option>';
                                                }
                                            }
                                            // if(isset($filters['unit_id']) && $filters['unit_id'] != ''){
                                            //     $c = new Criteria();
                                            //     $c->add(DinasMasterKegiatanPeer::UNIT_ID, $filters['unit_id'], Criteria::EQUAL);
                                            //     if($menu == 'Pendapatan'){
                                            //         $c->add(DinasMasterKegiatanPeer::IS_BTL,TRUE);
                                            //     }else{
                                            //         $c->add(DinasMasterKegiatanPeer::IS_BTL,FALSE);
                                            //     }
                                            //     $load_id = DinasMasterKegiatanPeer::doSelect($c);
                                            //     foreach($load_id as $key=>$value){
                                            //         $text = '';
                                            //         if(isset($filters['id']) && $filters['id']==$value->getKodeKegiatan()){
                                            //             $text = 'selected';
                                            //         }
                                            //         echo '<option value="'.$value->getKodeKegiatan().'" '.$text.'>'.$value->getKodeKegiatan().'</option>';
                                            //     }
                                            // }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-1">
                                <div class="form-group">
                                    <label>User ID</label>
                                    <select name="filters[user_id]" id="filters_user_id" class="form-control js-example-basic-single select2">
                                        <option value="">--Pilih User Id--</option>
                                        <?php
                                            if(!empty($load_user_id)){
                                                $temp = '';
                                                foreach($load_user_id as $key=>$value){
                                                    $text = '';
                                                    if(isset($filters['user_id']) && $filters['user_id']==$value->getUserId()){
                                                        $text = 'selected';
                                                    }
                                                    if($value->getUserId() != $temp){
                                                        echo '<option value="'.$value->getUserId().'" '.$text.'>'.$value->getUserId().'</option>';
                                                    }
                                                    $temp = $value->getUserId();
                                                }
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Nama sub kegiatan</label>
                                    <?php
                                    echo input_tag('filters[nama_kegiatan]', isset($filters['nama_kegiatan']) ? $filters['nama_kegiatan'] : null, array('class' => 'form-control'));
                                ?>
                                </div>
                            </div>
                            <!-- /.col -->
                            <?php if (sfConfig::get('app_tahap_edit') != 'murni') { ?>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Tahap</label>
                                    <?php
                                    echo select_tag('filters[tahap]', options_for_select(
                                    array(
                                            // 'murnibp' => 'Murni (Buku Putih)',
                                            // 'murnibb' => 'Murni (Buku Biru)',
                                            // 'murnipp' => 'Murni (Penyesuaian)',
                                            'murni' => 'Murni (Final)',
                                            //  'revisi1' => 'Revisi 1',
                                            // 'revisi1_1' => 'Penyesuaian Komponen 1',
                                            //  'revisi2' => 'Revisi 2',
                                            // 'revisi2_1' => 'Penyesuaian Komponen 2',
                                            // 'revisi2_2' => 'Penyesuaian Komponen 3',
                                            //  'revisi3' => 'Revisi 3',
                                            // 'revisi3_1' => 'Penyesuaian Komponen 4',
                                            //  'revisi4' => 'Revisi 4',
                                            //  'revisi5' => 'Revisi 5',
                                            //  'revisi6' => 'Revisi 6',
                                            //  'pakbp'   => 'PAK Buku Putih'
                                        ),
                                        isset($filters['tahap']) ? $filters['tahap'] : '', array('include_custom' => '--Pilih Tahap--')), array('class' => 'js-example-basic-single form-control select2'));
                                ?>
                                </div>
                            </div>
                            <!-- /.col -->
                            <?php } ?>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="tombol_filter">Tombol Filter</label><br />
                                    <button type="submit" name="filter" class="btn btn-outline-primary btn-sm">Filter <i
                                            class="fas fa-search"></i></button>
                                    <?php
                                    if ($menu== 'Pendapatan'){
                                        // echo link_to('Reset <i class="fa fa-backspace"></i>', 'kegiatan/listRevisi?filter=filter', array('class' => 'btn btn-outline-danger btn-sm')).' ';
                                       echo link_to('Reset <i class="fa fa-backspace"></i>', 'kegiatan/listRevisi?menu=Pendapatan&filter=filter', array('class' => 'btn btn-outline-danger btn-sm')).' ';
                                    }
                                   else{
                                       echo link_to('Reset <i class="fa fa-backspace"></i>', 'kegiatan/listRevisi?menu=Belanja&filter=filter', array('class' => 'btn btn-outline-danger btn-sm')).' ';
                                        
                                        
                                        echo link_to('Buka Rekening <i class="fa fa-unlock-alt"></i>', 'kegiatan/prosesBukaRek?unit_id=' . (isset($filters['unit_id']) ? $filters['unit_id'] : null), array('class' => 'btn btn-outline-info btn-sm'));
                                   }

                                ?>
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <?php
                        echo '</form>';
                        echo '<div class="text-right">';
                        $penyelia = '';
                        $bappeko = '';
                        if (isset($filters['unit_id'])) {
                            $con = Propel::getConnection();
                            $unit_id = $filters['unit_id'];
                            $query = "select u.user_id, u.user_name from master_user_v2 u, user_handle_v2 h, schema_akses_v2 a, ebudget.master_penyelia p
                                    where u.user_id=h.user_id and u.user_id=a.user_id and a.level_id=2 and h.unit_id='$unit_id' and u.user_enable=true and u.user_id=p.username";
                            $stmt = $con->prepareStatement($query);
                            $rs_penyelia = $stmt->executeQuery();
                            $satu = true;
                            while ($rs_penyelia->next()) {
                                if ($satu) {
                                    $satu = false;
                                    $penyelia.=$rs_penyelia->getString('user_name');
                                } else {
                                    $penyelia.=', ' . $rs_penyelia->getString('user_name');
                                }
                            }
                            if ($penyelia)
                                echo '<strong>Penyelia: <span class="badge badge-primary">[ ' . $penyelia . ' ] </span></strong><br>';

                            $query = "select u.user_id, u.user_name from master_user_v2 u, user_handle_v2 h, schema_akses_v2 a
                                    where u.user_id=h.user_id and u.user_id=a.user_id and a.level_id=16 and h.unit_id='$unit_id' and u.user_enable=true";
                            $stmt = $con->prepareStatement($query);
                            $rs_bappeko = $stmt->executeQuery();
                            $satu = true;
                            while ($rs_bappeko->next()) {
                                if ($satu) {
                                    $satu = false;
                                    $bappeko.=$rs_bappeko->getString('user_name');
                                } else {
                                    $bappeko.=', ' . $rs_bappeko->getString('user_name');
                                }
                            }
                            if ($bappeko)
                                echo '<strong>Bappeko: <span class="badge badge-primary">[ ' . $bappeko . ' ] </span></strong>';
                        }
                        ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 stretch-card">
                <div class="card">
                    <?php if (!$pager->getNbResults()): ?>
                    <?php echo __('no result') ?>
                    <?php else: ?>
                    <?php include_partial('kegiatan/list_revisi', array('pager' => $pager, 'filters' => $filters, 'menu' => $menu)) ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myModalLabel">Ambil Kegiatan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php echo form_tag('kegiatan/ambilKegiatan', array('class' => 'form-horizontal')); ?>
            <div class="modal-body">
                <input type="hidden" name="unit_id" id="unit_id">
                <input type="hidden" name="kode_kegiatan" id="kode_kegiatan">
                <div class="form-group" id="label_subkegiatan"></div>
                <div class="form-group">
                    User yang ambil
                    <select name="user_id" id="user_id" class="form-control"></select>
                    <!-- <select name="user_id" id="user_id" class="js-example-basic-single select2"></select> -->
                </div>
            </div>
            <div class="modal-footer">
                <input type="submit" name="submit" value="Simpan" class="btn btn-outline-primary btn-sm"/>
                <input type="reset" name="reset" value="Reset" class="btn btn-outline-warning btn-sm"/>                
            </div>
            <?php echo '</form>'; ?>
        </div>
    </div>
</div>

<script>

    function setAmbilKegiatan(unit_id,kode_kegiatan){
        // alert('unit_id='+unit_id)+' kode_kegiatan='+kode_kegiatan;
        // var unit_id_baru = "'"+unit_id+"'";
        // if(unit_id_baru.length < 4){
        //     unit_id_baru = '0'+unit_id_baru;
        // }
        // var option = '';
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/kegiatan/getUserId/unit_id/" + unit_id + "/kode_kegiatan/" + kode_kegiatan + ".html",
            type: "GET",
            dataType: 'json',
            success: function(hasil){
                console.log(hasil);
                $('#myModal').find('#user_id').html(hasil.html);
            }
        })


        $('#myModal').find('#unit_id').val(unit_id);
        $('#myModal').find('#kode_kegiatan').val(kode_kegiatan);
        
        $('#myModal').modal('show');
    }

    function loadKodeKegiatan(obj){
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/kegiatan/getKodeKegiatanByPd/unit_id/" + $(obj).val() + "/menu/<?php echo $menu ?>/tahap/<?php echo isset($filters['tahap']) ? $filters['tahap'] : '' ?>.html",
            type: "GET",
            dataType: 'json',
            success: function(hasil){
                console.log(hasil);
                $('#filters_id').html(hasil.html);
            }
        });
        $('#filters_id').select2();
    }
    
    function loadUserId(obj){
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/kegiatan/getUserByPd/unit_id/" + $(obj).val() + "/menu/<?php echo $menu ?>/tahap/<?php echo isset($filters['tahap']) ? $filters['tahap'] : '' ?>.html",
            type: "GET",
            dataType: 'json',
            success: function(hasil){
                console.log(hasil);
                $('#filters_user_id').html(hasil.html);
            }
        });
        $('#filters_user_id').select2();
    }

</script>