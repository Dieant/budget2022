<?php
    $kode = $master_kegiatan->getKegiatanId();
    $unit = $master_kegiatan->getUnitId();

    $query = "SELECT * from " . sfConfig::get('app_default_schema') . ".rincian_detail where ((note_peneliti is not Null and note_peneliti <> '') or (note_skpd is not Null and note_skpd <> '')) and unit_id = '$unit' and kegiatan_code = '$kode' and status_hapus = false ";

    $con = Propel::getConnection();
    $statement = $con->prepareStatement($query);
    $rs = $statement->executeQuery();
    $jml = $rs->getRecordCount();

    $query2 = "SELECT * from " . sfConfig::get('app_default_schema') . ".master_kegiatan where catatan is not Null and trim(catatan) <> '' and unit_id = '$unit' and kode_kegiatan = '$kode' ";

    //diambil nilai terpakai
    $con = Propel::getConnection();
    $statement2 = $con->prepareStatement($query2);
    $rs = $statement2->executeQuery();
    $jml2 = $rs->getRecordCount();

    if ($jml > 0 or $jml2 > 0) {
    ?>
        <td style="background: #e8f3f1"><?php echo $master_kegiatan->getKodekegiatan(); ?></td>
        <td style="background: #e8f3f1"><?php
            $e = new Criteria;
            $e->add(RincianPeer::KEGIATAN_CODE, $master_kegiatan->getKodeKegiatan());
            $e->add(RincianPeer::UNIT_ID, $master_kegiatan->getUnitId());
            //$e->add(RincianPeer::LOCK, TRUE);
            $x = RincianPeer::doSelectOne($e);
            if ($x) {
                if ($x->getLock() == TRUE) {
                    $gambar = (image_tag('gembok.gif', array('width' => '25', 'height' => '25')));
                } elseif ($x->getLock() == FALSE) {
                    $gambar = '';
                }
            }
            echo $master_kegiatan->getNamakegiatan() . ' ' . $gambar;
            ?> <?php
            if ($master_kegiatan->getUserId() != '') {
                echo ' <font color=#FF0000>(' . $master_kegiatan->getUserId() . ')</font><br/>';
            }
            echo 'Kode: [ '.$master_kegiatan->getKegiatanId().' ]<br/>';
            if ($x && $sf_user->getNamaUser() == 'superadmin' || $sf_user->getNamaUser() == 'admin') {
                if (sfConfig::get('app_fasilitas_cekeProject') == 'buka') {
                    echo '<strong>Sinkronisasi e-Project ( ' . date('j F Y h:i:s A', strtotime($x->getLastUpdateTime())) . ' )</strong>';
                }
            }
            ?></td>
        <?php 
        if (sfConfig::get('app_tahap_edit') == 'murni') 
        { 
        ?>
            <td align="right"  style="background: #e8f3f1">
                <?php echo number_format($master_kegiatan->getAlokasidana(), 0, ',', '.') ?>
                <?php // echo get_partial('kegiatan/nilaisemula', array('type' => 'list', 'master_kegiatan' => $master_kegiatan))  ?>
            </td>
        <?php   
        } 
        else if (sfConfig::get('app_tahap_edit') != 'murni') 
        { 
        ?>
            <td align="right" style="background: #e8f3f1"><?php echo get_partial('kegiatan/nilaisemula', array('type' => 'list', 'master_kegiatan' => $master_kegiatan)) ?></td>
        <?php 
        } 
        ?>     
        <td align="right" style="background: #e8f3f1"><?php echo get_partial('kegiatan/nilairincian', array('type' => 'list', 'master_kegiatan' => $master_kegiatan)) ?></td>
        <td align="right" style="background: #e8f3f1"><?php echo get_partial('kegiatan/selisih', array('type' => 'list', 'master_kegiatan' => $master_kegiatan)) ?></td>
        <td align="center" style="background: #e8f3f1; width: 200px">
            <div id="<?php echo 'tempat_ajax2_' . str_replace('.', '_', $master_kegiatan->getKodeKegiatan()) ?>" align="right">
                <?php
                echo number_format($master_kegiatan->getTambahanPagu(), 0, ',', '.') . '<br>';
                ?>
            </div>
        </td>
        <td style="background: #e8f3f1"><?php echo get_partial('posisi', array('type' => 'list', 'master_kegiatan' => $master_kegiatan)) ?></td>     
    <?php 
    } 
    else 
    { 
    ?>
        <td><?php echo $master_kegiatan->getKodekegiatan(); ?></td>
        <td><?php
            $e = new Criteria;
            $e->add(RincianPeer::KEGIATAN_CODE, $master_kegiatan->getKodeKegiatan());
            $e->add(RincianPeer::UNIT_ID, $master_kegiatan->getUnitId());
            //$e->add(RincianPeer::LOCK, TRUE);
            $x = RincianPeer::doSelectOne($e);
            if ($x) {
                if ($x->getLock() == TRUE) {
                    $gambar = (image_tag('gembok.gif', array('width' => '25', 'height' => '25')));
                } elseif ($x->getLock() == FALSE) {
                    $gambar = '';
                }
            }
            echo $master_kegiatan->getNamakegiatan() . ' ' . $gambar;
            if ($master_kegiatan->getUserId() != '') {
                echo ' <font color=#FF0000>(' . $master_kegiatan->getUserId() . ')</font><br/>';
            }
            echo 'Kode: [ '.$master_kegiatan->getKegiatanId().' ]<br/>';
            if ($x && $sf_user->getNamaUser() == 'superadmin' || $sf_user->getNamaUser() == 'admin') {
                if (sfConfig::get('app_fasilitas_cekeProject') == 'buka') {
                    echo '<strong>Sinkronisasi e-Project ( ' . date('j F Y h:i:s A', strtotime($x->getLastUpdateTime())) . ' )</strong>';
                }
            }
            ?>
        </td>
    <?php 
    if (sfConfig::get('app_tahap_edit') == 'murni') 
        { 
    ?>
        <td align="right" >
            <?php echo number_format($master_kegiatan->getAlokasidana(), 0, ',', '.') ?>
        </td>
    <?php 
        } 
        else if (sfConfig::get('app_tahap_edit') != 'murni') 
        { 
    ?>
        <td align="right" ><?php echo get_partial('kegiatan/nilaisemula', array('type' => 'list', 'master_kegiatan' => $master_kegiatan)) ?></td>
    <?php 
        } 
    ?>     
        <td align="right"><?php echo get_partial('kegiatan/nilairincian', array('type' => 'list', 'master_kegiatan' => $master_kegiatan)) ?></td>
        <td align="right"><?php echo get_partial('kegiatan/selisih', array('type' => 'list', 'master_kegiatan' => $master_kegiatan)) ?></td>
        <td align="center" style="width: 200px">
            <div id="<?php echo 'tempat_ajax2_' . str_replace('.', '_', $master_kegiatan->getKodeKegiatan()) ?>" align="right">
                <?php
                echo number_format($master_kegiatan->getTambahanPagu(), 0, ',', '.') . '<br>';
                ?>
            </div>
        </td>
        <td><?php echo get_partial('posisi', array('type' => 'list', 'master_kegiatan' => $master_kegiatan)) ?></td>     
<?php 
}
?>
<script>
    function showTambahanPagu(edit, pagu, unit, kegiatan, id) {
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/kegiatan/tambahanPagu/edit/" + edit + "/tambahPagu/" + pagu + "/unit_id/" + unit + "/kode_kegiatan/" + kegiatan + ".html",
            context: document.body
        }).done(function(msg) {
            $('#tempat_ajax2_' + id).html(msg);
        });
    }
</script>

