<?php use_helper('I18N', 'Date', 'Url', 'Javascript', 'Form', 'Object', 'Number', 'Validation') ?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>Daftar Isian Rincian Komponen Kegiatan SKPD</h1>
</section>

<!-- Main content -->
<section class="content">
    <?php include_partial('kegiatan/list_messages') ?>
    <!-- Default box -->
    <div class="box box-primary box-solid">
        <div class="box-header with-border">
            <h3 class="box-title">Mencari Komponen Berdasarkan Nama Komponen</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body">
            <?php echo form_tag('kegiatan/carikomponen', array('method' => 'get', 'class' => 'form-horizontal')) ?>
            <div class="form-group">
                <label class="col-sm-2 control-label">Nama Komponen</label>
                <div class="col-sm-10">
                    <?php echo input_tag('filters[nama_komponen]', isset($filters['nama_komponen']) ? $filters['nama_komponen'] : null, array('class' => 'form-control', 'placeholder' => 'Nama Komponen')); ?>
                </div>                        
                <?php
                echo input_hidden_tag('kegiatan', $sf_params->get('kegiatan'));
                echo input_hidden_tag('unit', $sf_params->get('unit'));
                ?>
            </div>
            <div id="sf_admin_container">
                <ul class="sf_admin_actions">
                    <li><?php echo submit_tag('cari', 'name=filter class=sf_admin_action_filter') ?></li>
                </ul>
            </div>
            <?php echo '</form>'; ?> 
        </div><!-- /.box-body -->
    </div><!-- /.box -->

    <!-- Default box -->
    <div class="box box-primary box-solid">
        <div class="box-body">
            <div id="sf_admin_container">
                <div id="sf_admin_content">        
                    <?php echo form_tag('kegiatan/baruKegiatan') ?>
                    <table cellspacing="0" class="sf_admin_list">
                        <thead>
                            <tr>
                                <th><b>Nama</b></th>
                                <th>&nbsp;</th>
                                <th><b>Isian</b></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="sf_admin_row_0" align='right'>
                                <td>Kelompok Belanja</td>
                                <td align="center">:</td>
                                <td align="left">
                                    <?php
                                    $rekening_code = trim($sf_params->get('rekening'));
                                    $belanja_code = substr($rekening_code, 0, 5);
                                    $c = new Criteria();
                                    $c->add(KelompokBelanjaPeer::BELANJA_CODE, $belanja_code);
                                    $rs_belanja = KelompokBelanjaPeer::doSelectOne($c);
                                    if ($rs_belanja) {
                                        echo $rs_belanja->getBelanjaName();
                                    }
                                    ?></td>
                            </tr>
                            <tr class="sf_admin_row_1" align='right'>
                                <td>Kode Rekening</td>
                                <td align="center">:</td>
                                <td align="left">
                                    <?php
                                    $c = new Criteria();
                                    $c->add(RekeningPeer::REKENING_CODE, $rekening_code);
                                    $rs_rekening = RekeningPeer::doSelectOne($c);
                                    if ($rs_rekening) {
                                        $pajak_rekening = $rs_rekening->getRekeningPpn();
                                        echo $rekening_code . ' ' . $rs_rekening->getRekeningName();
                                    }
                                    ?></td>
                            </tr>
                            <tr class="sf_admin_row_0" align='right'>
                                <td>Komponen</td>
                                <td align="center">:</td>
                                <td align="left"><?php echo $rs_komponen->getKomponenName() ?></td>
                            </tr>
                            <tr class="sf_admin_row_1" align='right'>
                                <td>Harga</td>
                                <td align="center">:</td>
                                <?php
                                $komponen_id = $rs_komponen->getKomponenId();
                                $komponen_harga = $rs_komponen->getKomponenHarga();
                                if ((substr($komponen_id, 0, 14) == '23.01.01.04.12') or ( substr($komponen_id, 0, 14) == '23.01.01.04.13') or ( substr($komponen_id, 0, 11) == '23.04.04.01')) {
                                    ?>
                                    <td align='left'><?php echo number_format($komponen_harga, 3, ',', '.') ?></td>
                                    <?php
                                } else {
                                    ?>
                                    <td align='left'><?php echo '&nbsp;' . number_format($komponen_harga, 0, ',', '.') ?> </td>
                                    <?php
                                }
                                ?>
                            </tr>
                            <tr class="sf_admin_row_0" align='right'>
                                <td>Satuan</td>
                                <td align="center">:</td>
                                <td align="left"><?php echo $rs_komponen->getSatuan(); ?></td>
                            </tr>
                            <tr class="sf_admin_row_1" align='right'>
                                <td>Pajak</td>
                                <td align="center">:</td>
                                <td align="left"><?php echo $sf_params->get('pajak') . '%' ?></td>
                            </tr>
                            <tr class="sf_admin_row_0" align='right'>
                                <td><span style="color:red;">*</span> Subtitle</td>
                                <td align="center">:</td>
                                <td align="left">
                                    <?php
                                    $kode_sub = '';
                                    if ($sf_params->get('subtitle')) {
                                        $kode_sub = $sf_params->get('subtitle');
                                    }

                                    echo select_tag('subtitle', objects_for_select($rs_subtitleindikator, 'getSubId', 'getSubtitle', $kode_sub, 'include_custom=---Pilih Subtitle---'));
                                    ?>
                                </td>
                            </tr>
                            <tr class="sf_admin_row_1" align='right'>
                                <td>Sub - Subtitle</td>
                                <td align="center">:</td>
                                <td align="left">
                                    <div id="indicator" style="display:none;" align="center"><dt>&nbsp;</dt><dd><b>Mohon Tunggu </b><?php echo image_tag('loading.gif', array('align' => 'absmiddle')) ?></dd></div>
                                    <?php
                                    if ($sf_params->get('sub')) {
                                        $kode_subsubtitle = $sf_params->get('sub');
                                        $d = new Criteria();
                                        $d->add(RincianSubParameterPeer::KODE_SUB, $kode_subsubtitle);
                                        $rs_rinciansubparameter = RincianSubParameter::doSelectOne($d);
                                        if ($rs_rinciansubparameter) {
                                            echo select_tag('sub', options_for_select(array($rs_rinciansubparameter->getKodeSub() => $rs_rinciansubparameter->getNewSubtitle()), $rs_rinciansubparameter->getKodeSub(), 'include_custom=---Pilih Subtitle Dulu---'), Array('id' => 'sub1'));
                                        }
                                    } elseif (!$sf_params->get('sub')) {       //select_tag('sub',options_for_select(array(), '','include_custom=---Pilih Subtitle Dulu---'), Array('id'=>'sub1'));
                                        echo select_tag('sub', options_for_select(array(), '', 'include_custom=---Pilih Subtitle Dulu---'), Array('id' => 'sub1'));
                                    }
                                    ?>

                                </td>
                            </tr>

                            <?php
                            $tipe = $sf_params->get('tipe');
                            if (($tipe == 'FISIK') || ($sf_params->get('lokasi'))) {
                                if ($sf_params->get('lokasi')) {
                                    $lokasi = base64_decode($sf_params->get('lokasi'));
                                } elseif (!$sf_params->get('lokasi')) {
                                    $lokasi = '';
                                }
                                ?>
                                <tr class="sf_admin_row_0" align='right' valign="top">
                                    <td>Lokasi</td>
                                    <td align="center">:</td>
                                    <td align="left">
                                        <?php
                                        echo input_tag('lokasi', $lokasi, 'size=50') . submit_tag('cari', 'name=cari');
                                        echo ' Usulan dari : ';
                                        echo select_tag('jasmas', objects_for_select($rs_jasmas, 'getKodeJasmas', 'getNama', $sf_params->get('jasmas'), 'include_custom=--Pilih--'));
                                        echo '<br />';
                                        if (!$lokasi == '') {
                                            $kode_lokasi = '';
                                            $banyak = 0;
                                            $c = new Criteria();
                                            $c->add(VLokasiPeer::NAMA, $lokasi, Criteria::ILIKE);
                                            $rs_lokasi = VLokasiPeer::doSelectOne($c);
                                            if ($rs_lokasi) {
                                                $kode_lokasi = $rs_lokasi->getKode();
                                            }

                                            $sql = new Criteria();
                                            if ($kode_lokasi) {
                                                $kod = '%' . $kode_lokasi . '%';
                                                $sql->add(HistoryPekerjaanPeer::KODE, strtr($kod, '*', '%'), Criteria::ILIKE);

                                                $sql->addAscendingOrderByColumn(HistoryPekerjaanPeer::TAHUN);
                                                $sqls = HistoryPekerjaanPeer::doSelect($sql);
                                                ?>
                                                <table cellspacing="0" class="sf_admin_list">
                                                    <thead>
                                                        <tr>
                                                            <th>Tahun</th>
                                                            <th>Lokasi</th>
                                                            <th>Nilai</th>
                                                            <th>volume</th>
                                                        <tr>
                                                    </thead>
                                                    <?php
                                                    $tahun = '';
                                                    $total_lokasi = 0;
                                                    foreach ($sqls as $vx) {
                                                        //while ($rsPeta->next())
                                                        if ($tahun == '') {
                                                            $tahun = $vx->getTahun();
                                                        }
                                                        //$s -> addOr(SubtitleIndikatorPeer::SUBTITLE, $vx->getSubtitle());
                                                        if (($tahun <> $vx->getTahun()) && ($tahun <> '')) {
                                                            $tahun = $vx->getTahun();
                                                            ?>
                                                            <tbody>
                                                                <tr class="sf_admin_row_1" align='right'>
                                                                    <td>&nbsp;</td>
                                                                    <td align="right">Total :</td>
                                                                    <td align="right"> <?php echo number_format($total_lokasi, 0, ',', '.'); ?></td>
                                                                    <td> <?php $total_lokasi = 0 ?></td>
                                                                </tr>
                                                                <tr><td colspan="4"></td></tr>
                                                                <tr>
                                                                    <td> <?php echo $vx->getTahun(); ?></td>
                                                                    <td> <?php echo $vx->getLokasi(); ?></td>
                                                                    <td align="right"> <?php echo number_format($vx->getNilai(), 0, ',', '.'); ?></td>
                                                                    <td> <?php echo $vx->getVolume(); ?></td>
                                                                </tr>
                                                                <?php
                                                                if ($vx->getNilai() > 0) {
                                                                    $banyak+=1;
                                                                }
                                                                $total_lokasi+=$vx->getNilai();
                                                            } else {
                                                                ?>
                                                                <tr>
                                                                    <td> <?php echo $vx->getTahun(); //$rsPeta->getInt('tahun')       ?></td>
                                                                    <td> <?php echo $vx->getLokasi(); //$rsPeta->getString('lokasi')       ?></td>
                                                                    <td align="right"> <?php echo number_format($vx->getNilai(), 0, ',', '.'); //$rsPeta->getString('nilai')       ?></td>
                                                                    <td> <?php echo $vx->getVolume(); //$rsPeta->getString('nomor')       ?></td>
                                                                </tr>
                                                                <?php
                                                                if ($vx->getNilai() > 0) {
                                                                    $banyak+=1;
                                                                }
                                                                $total_lokasi+=$vx->getNilai();
                                                            }
                                                        }
                                                        ?>
                                                        <tr class="sf_admin_row_1" align='right'>
                                                            <td> </td>
                                                            <td align="right">Total :</td>
                                                            <td align="right"> <?php echo number_format($total_lokasi, 0, ',', '.'); //$rsPeta->getString('nilai')       ?></td>
                                                            <td> <?php $total_lokasi = 0 ?></td>
                                                        </tr>
                                                        <tr><td colspan="4">&nbsp;</td></tr>
                                                        <?php
                                                        $query = "select * from " . sfConfig::get('app_default_schema') . ".rincian_detail where detail_name ilike '$lokasi'";
                                                        $con = Propel::getConnection();
                                                        $stmt = $con->prepareStatement($query);
                                                        $rs_rincianlokasi = $stmt->executeQuery();
                                                        while ($rs_rincianlokasi->next()) {
                                                            ?>
                                                            <tr>
                                                                <td><?php echo sfConfig::get('app_tahun_default') ?></td>
                                                                <td> <?php echo $rs_rincianlokasi->getString('komponen_name') . ' ' . $rs_rincianlokasi->getString('detail_name') ?></td>
                                                                <td align="right"> 
                                                                    <?php
                                                                    $pajak = $rs_rincianlokasi->getString('pajak');
                                                                    $harga = $rs_rincianlokasi->getString('komponen_harga_awal');
                                                                    $volume = $rs_rincianlokasi->getString('volume');
                                                                    $nilai = ($volume * $harga * (100 + $pajak) / 100);
                                                                    echo number_format($nilai, 0, ',', '.');
                                                                    ?></td>
                                                                <td> <?php echo $rs_rincianlokasi->getString('keterangan_koefisien'); ?></td>
                                                            </tr>
                                                            <?php
                                                            if ($volume > 0) {
                                                                $banyak+=1;
                                                            }
                                                            $total_lokasi+=$nilai;
                                                        }
                                                        ?>
                                                        <tr class="sf_admin_row_1" align='right'>
                                                            <td> </td>
                                                            <td align="right">Total :</td>
                                                            <td align="right"> <?php echo number_format($total_lokasi, 0, ',', '.'); //$rsPeta->getString('nilai')       ?></td>
                                                            <td> <?php $total_lokasi = 0 ?></td>
                                                        </tr>
                                                        <tr><td colspan="4">&nbsp;</td></tr>
                                                    </tbody>

                                                </table>
                                                <?php
                                            }
                                            if ($banyak <= 0) {
                                                echo '<span style="color:green;"> <small> :. Belum pernah ada Pekerjaan .: </small></span>';
                                                echo input_hidden_tag('status', 'ok');
                                            }

                                            if ($banyak > 0) {
                                                echo '<span style="color:red;">  <small> :. Pekerjaan Pada Lokasi Ini Sudah Ada, Dapat Disimpan, Tetapi Tidak Dapat Masuk RKA .: </small></span>';
                                                echo input_hidden_tag('status', 'pending');
                                            }
                                            ?>
                                        </td>
                                    </tr>
                                    <?php
                                }
                            } else {
                                ?>
                                <tr class="sf_admin_row_0" align='right' valign="top">
                                    <td>Keterangan</td>
                                    <td align="center">:</td>
                                    <td align='left'><?php echo input_tag('keterangan', $sf_params->get('keterangan')) ?></td>
                                </tr>
                                <?php
                            }
                            ?>
                            <tr class="sf_admin_row_1" align='right' valign="top">
                                <td><span style="color:red;">*</span>Volume</td>
                                <td align="center">:</td>
                                <td align="left">
                                    <?php
                                    $keterangan_koefisien = $sf_params->get('keterangan_koefisien');
                                    $pisah_kali = explode('X', $keterangan_koefisien);
                                    for ($i = 0; $i < 4; $i++) {
                                        $satuan = '';
                                        $volume = '';
                                        $nama_input = 'vol' . ($i + 1);
                                        $nama_pilih = 'volume' . ($i + 1);
                                        ;
                                        if (!empty($pisah_kali[$i])) {
                                            $pisah_spasi = explode(' ', $pisah_kali[$i]);
                                            $j = 0;

                                            for ($s = 0; $s < count($pisah_spasi); $s++) {
                                                if ($pisah_spasi[$s] != NULL) {
                                                    if ($j == 0) {
                                                        $volume = $pisah_spasi[$s];
                                                        $j++;
                                                    } elseif ($j == 1) {
                                                        $satuan = $pisah_spasi[$s];
                                                        $j++;
                                                    } else {
                                                        $satuan.=' ' . $pisah_spasi[$s];
                                                    }
                                                }
                                            }
                                        }
                                        if ($i !== 3) {
                                            echo input_tag($nama_input, $volume) . ' ' . select_tag($nama_pilih, objects_for_select($rs_satuan, 'getSatuanName', 'getSatuanName', $satuan, 'include_custom=---Pilih Satuan--')) . '<br />  X <br />';
                                        } else {
                                            echo input_tag($nama_input, $volume) . ' ' . select_tag($nama_pilih, objects_for_select($rs_satuan, 'getSatuanName', 'getSatuanName', $satuan, 'include_custom=---Pilih Satuan--'));
                                        }
                                    }
                                    ?></td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr class="sf_admin_row_0" align='right' valign="top">
                                <td>&nbsp; </td>
                                <td>
                                    <?php
                                    echo input_hidden_tag('kegiatan', $sf_params->get('kegiatan'));
                                    echo input_hidden_tag('unit', $sf_params->get('unit'));
                                    echo input_hidden_tag('id', $sf_params->get('komponen'));
                                    echo input_hidden_tag('pajak', $sf_params->get('pajak'));
                                    echo input_hidden_tag('tipe', $sf_params->get('tipe'));
                                    echo input_hidden_tag('rekening', $sf_params->get('rekening'));
                                    echo input_hidden_tag('referer', $sf_request->getAttribute('referer'));
                                    ?>
                                </td>
                                <td><?php echo submit_tag('simpan', 'name=simpan') . ' ' . button_to('kembali', '#', array('onClick' => "javascript:history.back()")) ?></li></td>
                            </tr>
                        </tfoot>
                    </table>
                    <?php echo '</form>'; ?>

                </div>
            </div>
        </div>
    </div>
</section>

<script>
    $("#subtitle").change(function() {
        var id = $(this).val();
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/kegiatan/pilihsubx/kegiatan_code/<?php echo $sf_params->get('kegiatan') ?>/unit_id/<?php echo $sf_params->get('unit') ?>/b/" + id + ".html",
            context: document.body
        }).done(function(msg) {
            $('#sub1').html(msg);
        });

    });

    $("#kecamatan").change(function() {
        var id = $(this).val();
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/kegiatan/pilihKelurahan/b/" + id + ".html",
            context: document.body
        }).done(function(msg) {
            $('#kelurahan1').html(msg);
        });

    });

    function hitungTotal() {
        var harga = $('harga').value;
        var pajakx = $('pajakx').value;
        var vol1 = $('vol1').value;
        var vol2 = $('vol2').value;
        var vol3 = $('vol3').value;
        var vol4 = $('vol4').value;
        var volume;

        if (vol1 !== '' || vol2 !== '' || vol3 !== '' || vol4 !== '') {
            if (vol2 === '') {
                vol2 = 1;
                volume = vol1 * vol2;
            } else if (vol2 !== '') {
                volume = vol1 * vol2;
            }
            if (vol3 === '') {
                vol3 = 1;
                volume = volume * vol3;
            } else if (vol3 !== '') {
                volume = vol1 * vol2 * vol3;
            }
            if (vol4 === '') {
                vol4 = 1;
                volume = volume * vol4;
            } else if (vol4 !== '') {
                volume = vol1 * vol2 * vol3 * vol4;
            }
        }

        if (pajakx === 10) {
            var hitung = (harga * volume * (110) / 100);
        } else if (pajakx === 0) {
            var hitung = (harga * volume * 1);
        }

        $('total').value = hitung;

    }
</script>