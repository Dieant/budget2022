<td class='tombol_actions'>
<?php 
 $nama = $sf_user->getNamaLogin();
    echo link_to('<i class="fa fa-edit"></i>', 'kegiatan/editRevisi?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan() . '&tahap=' . (isset($filters['tahap']) ? $filters['tahap'] : ''), array('class' => 'btn btn-outline-primary btn-sm'));
    if (isset($filters['tahap']) && $filters['tahap'] <> '') {
        echo link_to('<i class="fas fa-list-ul"></i>', 'report/tampillaporanaslisemua?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan() . '&tahap=' . $filters['tahap'], array('alt' => __('laporan perbandingan asli-draft'), 'title' => __('laporan perbandingan asli-draft'), 'class' => 'btn btn-outline-info btn-sm'));
        echo link_to('<i class="fa fa-list-alt"></i>', 'report/tampillaporanrevisi?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan() . '&tahap=' . $filters['tahap'], array('alt' => __('laporan perbandingan revisi'), 'title' => __('laporan perbandingan revisi'), 'class' => 'btn btn-outline-secondary btn-sm'));  
    } else {
        echo link_to('<i class="fas fa-list"></i>', 'report/tampillaporanasli?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan(), array('alt' => __('laporan perbandingan asli-draft'), 'title' => __('laporan perbandingan asli-draft'), 'class' => 'btn btn-outline-warning btn-sm'));
    }
    if (!(isset($filters['tahap']) && $filters['tahap'] <> '')) 
    {
        if(sfConfig::get('app_tahap_edit') == 'pak' || sfConfig::get('app_tahap_edit') == 'murni') {                   
            echo link_to('<i class="fa fa-th-list"></i>', 'report/tampillaporanBukuputih?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan(), array('alt' => __('laporan perbandingan asli-bukuputih'), 'title' => __('laporan perbandingan murni bukuputih-bukubiru'), 'class' => 'btn btn-outline-secondary btn-sm'));
        }

        $kegiatan_code = $master_kegiatan->getKodeKegiatan();
        $unit_id = $master_kegiatan->getUnitId();

         
        $c = new Criteria();
        $c->add(DinasRincianPeer::KEGIATAN_CODE, $kegiatan_code);
        $c->add(DinasRincianPeer::UNIT_ID, $unit_id);
        $rs_rincian = DinasRincianPeer::doSelectOne($c);
        if ($rs_rincian) {
            $posisi = $rs_rincian->getRincianLevel();
            $kunci = $rs_rincian->getLock();
        }

        if($nama !== 'tim_shs'){
            if ($posisi == '2') {
                echo link_to('<i class="fa fa-lock"></i>', 'kegiatan/kuncirevisi?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan() . '&kunci=' . md5('dinas'), array('alt' => __('Klik untuk buka ke Peneliti'), 'title' => __('Klik untuk buka ke Peneliti'), 'class' => 'btn btn-outline-danger btn-sm'));
            } else if ($posisi == '3') {
                echo link_to('<i class="fa fa-lock-open"></i>', 'kegiatan/bukarevisi?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan() . '&buka=' . md5('dinas'), array('alt' => __('Klik untuk buka ke Dinas'), 'title' => __('Klik untuk buka ke Dinas'), 'class' => 'btn btn-outline-primary btn-sm'));
            }
        }

        
                // echo '<li> ';
                // if ($posisi == '1') {
                //      echo link_to('<i class="fa fa-lock"></i>', 'kegiatan/kuncijk?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan() . '&kunci=' . md5('dinas'), array('alt' => __('Klik untuk kunci JK'), 'title' => __('Klik untuk kunci JK'), 'class' => 'btn btn-outline-primary btn-sm', 'style' => 'width:35px;'));
                // } else if ($posisi == '3') {
                //     echo link_to('<i class="fa fa-unlock"></i>', 'kegiatan/bukajk?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan() . '&buka=' . md5('dinas'), array('alt' => __('Klik untuk buka JK'), 'title' => __('Klik untuk buka JK'), 'class' => 'btn btn-outline-primary btn-sm', 'style' => 'width:35px;color:pink'));
                // }
                // echo '</li> ';
    }

// if ($nama == 'superadmin') // tutup sementara untuk selain superadmin
//     {
    if($nama !== 'tim_shs'){
        if (!(isset($filters['tahap']) && $filters['tahap'] <> '')) {
            if ($kunci == TRUE) {
                // if ($nama == 'superadmin') 
                echo link_to('<i class="fa fa-lock-open"></i>', 'kegiatan/bukadpRevisi?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan(), array('alt' => __('Klik untuk Buka RKA'), 'title' => __('Klik untuk Buka RKA'), 'class' => 'btn btn-outline-primary btn-sm'));
            } else if ($kunci == FALSE) {
                echo link_to('<i class="fa fa-lock"></i>', 'kegiatan/kuncidpRevisi?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan(),  array('alt' => __('Klik untuk Kunci RKA'), 'title' => __('Klik untuk Kunci RKA'), 'class' => 'btn btn-outline-danger btn-sm'));
            }
        }
    }
// }

        if (!(isset($filters['tahap']) && $filters['tahap'] <> '')) {
            echo link_to('<i class="fa fa-history"></i>', 'historyuser/historylist?unit_id=' . $master_kegiatan->getUnitId() . '&kegiatan_code=' . $master_kegiatan->getKodeKegiatan(),  array('alt' => __('History User'), 'title' => __('History User'), 'class' => 'btn btn-outline-success btn-sm'));          
             $nama = $sf_user->getNamaLogin();
            if ($nama == 'superadmin') {
            echo link_to('<i class="fa fa-donate"></i>', 'kegiatan/penandaBtl?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan(), array('title' => __('Penanda Sumber Dana'), 'class' => 'btn btn-outline-secondary btn-sm'));
             echo link_to('<i class="fa fa-shield-virus"></i>', 'kegiatan/penandaCovid?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan(), array('title' => __('Penanda Covid 19'), 'class' => 'btn btn-outline-info btn-sm'));
            echo link_to('<i class="fa fa-times-circle"></i>', 'kegiatan/batalRevisi?unit_id=' . $master_kegiatan->getUnitId() . '&kegiatan_code=' . $master_kegiatan->getKodeKegiatan(),  array('confirm' => 'Apakah anda yakin untuk membatalkan revisi SKPD ' . $master_kegiatan->getUnitId() . ' dengan kode kegiatan ' . $master_kegiatan->getKodeKegiatan() . '?', 'alt' => __('Batal Revisi'), 'title' => __('Batal Revisi'), 'class' => 'btn btn-outline-danger btn-sm'));            
             echo link_to('<i class="fa fa-exclamation-triangle"></i>', 'report/penandaPrioritas?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan(), array('alt' => __('Penanda Prioritas'), 'title' => __('Penanda Prioritas'), 'class' => 'btn btn-outline-warning btn-sm'));             
            }
             if ($nama == 'superadmin' || $nama == 'admin.aji')
             {
            echo link_to('<i class="fab fa-vimeo"></i>', 'kegiatan/ubahVolumeOrang?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan(), array('alt' => __('Edit Volume Orang'), 'title' => __('Edit Volume Orang'), 'class' => 'btn btn-outline-info btn-sm'));
            }
             echo link_to('<i class="fa fa-hand-holding-usd"></i>', 'report/editRasionalisasi?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan(), array('alt' => __('Edit Nilai Rasionalisasi'), 'title' => __('Edit Nilai Rasionalisasi'), 'class' => 'btn btn-outline-info btn-sm'));
            echo link_to('<i class="fa fa-magnet"></i>', 'report/reportDetailKegiatan?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan(), array('alt' => __('Data Komponen dan Realisasi'), 'title' => __('Data Komponen dan Realisasi'), 'class' => 'btn btn-outline-info btn-sm'));

            echo link_to('<i class="fa fa-chalkboard-teacher"></i>', 'report/menuViewDevplan?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan() . '&tahap=' . (isset($filters['tahap']) ? $filters['tahap'] : ''), array('alt' => __('Melihat Komponen Devplan'), 'title' => __('Melihat Komponen Devplan'), 'class' => 'btn btn-outline-primary btn-sm'));

          }

        $tahap = DinasMasterKegiatanPeer::getTahapKegiatan($master_kegiatan->getUnitId(), $master_kegiatan->getKodeKegiatan());
        $c = new Criteria();
        $c->add(RincianBappekoPeer::UNIT_ID, $master_kegiatan->getUnitId());
        $c->add(RincianBappekoPeer::KODE_KEGIATAN, $master_kegiatan->getKodeKegiatan());
        $c->add(RincianBappekoPeer::TAHAP, $tahap);
        if ($rs_rincian_bappeko = RincianBappekoPeer::doSelectOne($c)):
             echo link_to('<i class="fa fa-chalkboard-teacher"></i>', 'report/menuViewDevplan?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan() . '&tahap=' . (isset($filters['tahap']) ? $filters['tahap'] : ''), array('alt' => __('Melihat Komponen Devplan'), 'title' => __('Melihat Komponen Devplan'), 'class' => 'btn btn-outline-primary btn-sm'));
        endif;

        if (!(isset($filters['tahap']) && $filters['tahap'] <> '')) {
            if($master_kegiatan->getUserId() == ''){ ?>
              <!-- <button class="btn btn-outline-success btn-sm" title="Ambil" onclick="setAmbilKegiatan('<?php //echo $master_kegiatan->getUnitId() ?>',<?php //echo $master_kegiatan->getKodeKegiatan() ?>)" style="margin: -1.5px;"><i class="fa fa-fast-forward"></i></button> -->
            <?php 
                echo link_to('<i class="fa fa-fast-forward"></i>', '#', array('alt' => __('Ambil'), 'title' => __('Ambil'), 'class' => 'btn btn-outline-primary btn-sm', 'class' => 'btn btn-outline-success btn-sm', 'onclick' => "setAmbilKegiatan('".$master_kegiatan->getUnitId()."',".$master_kegiatan->getKodeKegiatan().")", 'style' => 'margin-left: -1.5px'));   
            }else{
                echo link_to('<i class="fa fa-fast-backward"></i>', 'kegiatan/lepasKegiatan?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan() . '&user_id=' . $master_kegiatan->getUserId(), array('alt' => __('Lepas'), 'title' => __('Lepas'), 'class' => 'btn btn-outline-primary btn-sm', 'class' => 'btn btn-outline-danger btn-sm'));
            }
        }
?>
</td>