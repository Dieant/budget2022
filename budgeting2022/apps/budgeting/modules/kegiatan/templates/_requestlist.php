<?php use_helper('I18N', 'Date', 'Url', 'Javascript', 'Form', 'Object', 'Validation') ?>
<div class="card-body table-responsive p-0">
    <table class="table table-hover">
        <thead class="head_peach">
            <tr>
                <th>Perangkat Daerah</th>
                <th>Kode Kegiatan</th>
                <th>Penyelia</th>
                <th>Catatan Penyelia</th>
                <th>Tipe Request</th>
                <th>File</th>
                <th>Tanggal</th>
                <th>Status</th>
                <th>Action</th>
                <th>Hapus</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $i = 1;
            foreach ($pager->getResults() as $data):
                ?>
            <div class="modal fade" id="myModal<?php echo $data->getId() ?>" tabindex="-1" role="dialog"
                aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="myModalLabel">Alasan Tolak</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <?php echo $data->getCatatanTolak() ?>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            $odd = fmod(++$i, 2);
            $c = new Criteria();
            $c->add(UnitKerjaPeer::UNIT_ID, $data->getUnitId());
            if ($rs_skpd = UnitKerjaPeer::doSelectOne($c)) {
                $skpd = $rs_skpd->getUnitName();
            }
            ?>
            <tr class="sf_admin_row_<?php echo $odd ?>">
                <td><?php echo '(' . $data->getUnitId() . ') ' . $skpd ?></td>
                <td><?php echo str_replace('|', ', ', $data->getKegiatanCode()) ?></td>
                <td><?php echo $data->getPenyelia() ?></td>
                <td><?php echo $data->getCatatan() ?></td>
                <?php
                $h = $data->getTipe();
                if ($h == 0)
                    $tipe = 'Buka Kunci Kegiatan';
                else if ($h == 1)
                    $tipe = 'Tambah Pagu';
                else if ($h == 2)
                    $tipe = 'Upload Dokumen';

                if ($data->getStatus() == 0) {
                    $status = 'Pending';
                } elseif ($data->getStatus() == 1) {
                    $status = '<font style="color:green">Diterima</font>';
                } elseif ($data->getStatus() == 2) {
                    $status = '<button type="button" class="btn btn-link btn-xs" data-toggle="modal" data-target="#myModal' . $data->getId() . '">
                            Ditolak
                        </button>';
                }
                ?>
                <td><?php echo $tipe ?></td>
                <td>
                    <?php
                    if ($data->getIsKhusus()) {
                        echo 'KHUSUS';
                    } else {
                        echo link_to('Download File', sfConfig::get('app_path_default_sf') . 'uploads/log/' . $data->getPath());
                    }
                    ?>
                </td>
                <td><?php echo $data->getUpdatedAt(); ?></td>
                <td><?php echo $status; ?></td>
                <td>
                    <?php if ($data->getStatus() == 0) { ?>
                    <div id="<?php echo 'tempat_ajax2_' . $data->getId() ?>" align="right">
                        <?php echo link_to('Terima', 'kegiatan/terimaRequest?id=' . $data->getId(), array('confirm' => 'Yakin untuk menerima?', 'class' => 'btn btn-xs btn-success')); ?>
                        <?php echo link_to_function('Tolak', 'showAlasanTolak("' . $data->getId() . '")', array('class' => 'btn btn-xs btn-danger')); ?>
                    </div>
                    <?php } ?>
                </td>
                <td>
                    <?php
                    if ($data->getStatus() != 1) {
                        echo link_to('<i class="fa fa-times-circle"></i>', 'kegiatan/hapusRequest?id=' . $data->getId(),  array('confirm' => 'Apakah anda Yakin untuk menghapus?', 'alt' => __('Hapus'), 'title' => __('Hapus'), 'class' => 'btn btn-outline-danger btn-sm'));
                    }
                    ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
<div class="card-footer clearfix">
    <ul class="pagination pagination-sm m-0 float-left">
        <?php
        echo format_number_choice('[0] no result|[1] 1 result|(1,+Inf] %1% results', array('%1%' => $pager->getNbResults()), $pager->getNbResults()) 
        ?>
    </ul>
    <ul class="pagination pagination-sm m-0 float-right">
        <?php 
        if ($pager->haveToPaginate()): 
            echo '<li class="page-item">'.link_to('Previous', 'kegiatan/requestlist?page=' . $pager->getPreviousPage(), array('class' => 'page-link', 'align' => 'absmiddle', 'alt' => __('Previous'), 'title' => __('Previous'))).'</li>';

            foreach ($pager->getLinks() as $page):
                echo '<li class="page-item">'.link_to_unless($page == $pager->getPage(), $page, "kegiatan/requestlist?page={$page}", array('class' => 'page-link')).'</li>';
            endforeach;
            echo '<li class="page-item">'.link_to('Next', 'kegiatan/requestlist?page=' . $pager->getNextPage(), array('class' => 'page-link', 'align' => 'absmiddle', 'alt' => __('Next'), 'title' => __('Next'))).'</li>'; 
        endif;
        ?>
    </ul>
</div>
<script>
    function showAlasanTolak(id) {
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/kegiatan/setTolak/id/" + id +
                "/request/peneliti.html",
            context: document.body
        }).done(function (msg) {
            $('#tempat_ajax2_' + id).html(msg);
        });
    }
</script>