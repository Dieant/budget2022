<?php  use_helper('Url','Javascript','Form','Object'); ?>
<?php
//menu untuk edit dan hapus
	$i = 0;
	$kode_sub='';
	while($rs_rinciandetail->next())
	{
		$odd = fmod($i++, 2);
		$unit_id = $rs_rinciandetail->getString('unit_id');
		$kegiatan_code = $rs_rinciandetail->getString('kegiatan_code');
		
		if($kode_sub!=$rs_rinciandetail->getString('kode_sub'))
		{
			$kode_sub = $rs_rinciandetail->getString('kode_sub');
			$c = new Criteria();
			$c->add(RincianSubParameterPeer::KODE_SUB, $kode_sub);
			$rs_subparameter = RincianSubParameterPeer::doSelectOne($c);
			if($rs_subparameter){
?>
		<tr class="pekerjaans_<?php echo $id ?>" bgcolor="#AFC4EF">
			<td colspan="7"><?php echo link_to_function(image_tag('/sf/sf_admin/images/delete.png'),'hapusSubKegiatan('.$id.',"'.$kegiatan.'","'.$unit.'","'.$kode_sub.'")');?> <b> :. <?php echo $rs_subparameter->getSubKegiatanName().' '.$rs_subparameter->getDetailName(); ?></b></td>
			<td align="right">
			<?php
				$query2="select sum(volume * komponen_harga_awal * (100+pajak)/100) as hasil_kali
				from ". sfConfig::get('app_default_schema') .".rincian_detail
				where kode_sub='$kode_sub'";
				$con = Propel::getConnection();
				$stmt = $con->prepareStatement($query2);
				$t = $stmt->executeQuery();
				while($t->next())
				{
					echo number_format($t->getString('hasil_kali'), 0, ',', '.'); 
				}
			?></td>
			<td>&nbsp;</td>
		</tr>
<?php
			}
		}
		
?>
		<tr class="pekerjaans_<?php echo $id ?>">
			<td>
			<?php
				$kegiatan =$rs_rinciandetail->getString('kegiatan_code');
				$unit=$rs_rinciandetail->getString('unit_id');
				$no=$rs_rinciandetail->getString('detail_no');
				if($rs_rinciandetail->getString('from_sub_kegiatan')==''){
				echo link_to_function(image_tag('/sf/sf_admin/images/delete.png'),'hapusKegiatan('.$id.',"'.$kegiatan.'","'.$unit.'",'.$no.')');
				echo link_to(image_tag('/sf/sf_admin/images/edit.png'), 'kegiatan/editKegiatan?id='.$rs_rinciandetail->getString('detail_no').'&unit='.$rs_rinciandetail->getString('unit_id').'&kegiatan='.$rs_rinciandetail->getString('kegiatan_code').'&edit='.md5('ubah'));}
			?>
			</td>
			<td><?php echo $rs_rinciandetail->getString('komponen_name').' '.$rs_rinciandetail->getString('detail_name') ?></td>
			<td align="center"><?php echo $rs_rinciandetail->getString('satuan') ?></td>
			<td align="center"><?php echo $rs_rinciandetail->getString('keterangan_koefisien'); ?></td>
			<td align="right"><?php echo number_format($rs_rinciandetail->getString('komponen_harga_awal'),0,',','.') ?></td>
			<td align="right"><?php 
			$volume = $rs_rinciandetail->getString('volume');
			$harga = $rs_rinciandetail->getString('komponen_harga_awal');
			$hasil = $volume * $harga;
			echo number_format($hasil,0,',','.');
			 ?></td>
			 <td align="right"><?php echo $rs_rinciandetail->getString('pajak').'%'; ?></td>
			 <td align="right"><?php 
			$volume = $rs_rinciandetail->getString('volume');
			$harga = $rs_rinciandetail->getString('komponen_harga_awal');
			$pajak = $rs_rinciandetail->getString('pajak');
			$total = $volume * $harga * (100 + $pajak) / 100;
			echo number_format($total,0,',','.');
			 ?></td>
			<td align="center">
			<?php
				$rekening = $rs_rinciandetail->getString('rekening_code');
				$rekening_code = substr($rekening,0,5);
				$c = new Criteria();
				$c->add(KelompokBelanjaPeer::BELANJA_CODE, $rekening_code);
				$rs_rekening = KelompokBelanjaPeer::doSelectOne($c);
				if($rs_rekening)
				{
					echo $rs_rekening->getBelanjaName();
				}
			?></td>
		</tr>
<?php
	}
?>
