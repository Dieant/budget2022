<?php use_helper('Object', 'Javascript') ?>
<?php echo form_tag('kegiatan/list', array('method' => 'get', 'class' => 'form-horizontal')); ?>
<?php
$e = new Criteria();
$e->addAscendingOrderByColumn(UnitKerjaPeer::UNIT_NAME);
$v = UnitKerjaPeer::doSelect($e);
?>
<div class="row">
    <div class="col-md-3">
        <div class="form-group">
            <label>Perangkat Daerah</label>
            <?php 
                echo select_tag('filters[unit_id]', objects_for_select($v, 'getUnitId', 'getUnitName', isset($filters['unit_id']) ? $filters['unit_id'] : null, array('include_custom' => '--Pilih Perangkat Daerah--')), array('class' => 'js-example-basic-single form-control select2')); 
            ?> 
        </div>
    </div>
    <!-- /.col -->
    <div class="col-md-3">
        <div class="form-group">
            <label>Kode Kegiatan</label>
            <?php 
                echo input_tag('filters[kode_kegiatan]', isset($filters['kode_kegiatan']) ? $filters['kode_kegiatan'] : null, array('class' => 'form-control')); 
            ?>
        </div>
      </div>
      <!-- /.col -->
    <div class="col-md-3">
        <div class="form-group">
            <label>Nama</label>
            <?php
                echo input_tag('filters[nama_kegiatan]', isset($filters['nama_kegiatan']) ? $filters['nama_kegiatan'] : null, array('class' => 'form-control'));
            ?>
        </div>
    </div>
    <!-- /.col -->
    <?php if (sfConfig::get('app_tahap_edit') != 'murni') { ?>
    <div class="col-md-3">
        <div class="form-group">
            <label>Tahap (Untuk Tarik e-Project)</label>
            <?php
                echo select_tag('filters[tahap]', options_for_select(
                    array(
                            'murnibp' => 'Murni (Buku Putih)' ,
                            'murnibbpraevagub' => 'Murni (Buku Biru Pra Evaluasi Gubernur)',
                            'murnibb' => 'Murni (Buku Biru)',
                            'murni' => 'Murni',
                            'revisi1' => 'Revisi 1',
                            'revisi2' => 'Revisi 2',
                            'revisi3' => 'Revisi 3',
                            'revisi3_1' => 'Revisi 3_1',
                            'revisi4' => 'Revisi 4',
                            'revisi5' => 'Revisi 5',
                            'revisi6' => 'Revisi 6',
                            'revisi7' => 'Revisi 7',
                            'revisi8' => 'Revisi 8',
                            'pak'     => 'PAK'
                        ),
                    isset($filters['tahap']) ? $filters['tahap'] : '', array('include_custom' => '--Pilih Tahap--')), array('class' => 'js-example-basic-single form-control select2'));
            ?>
        </div>
    </div>
    <!-- /.col -->
    <?php }?>
    <div class="col-md-2">
        <div class="form-group">
            <label class="tombol_filter">Tombol Filter</label><br/>
            <button type="submit" name="filter" class="btn btn-outline-primary btn-sm">Filter <i class="fas fa-search"></i></button>
            <?php
                echo link_to('Reset <i class="fas fa-backspace"></i>', 'kegiatan/list?filter=filter', array('class' => 'btn btn-outline-danger btn-sm'));
            ?>
        </div>
        <!-- /.form-group -->
    </div>
    <!-- /.col -->
</div>
<?php echo '</form>'; ?>

