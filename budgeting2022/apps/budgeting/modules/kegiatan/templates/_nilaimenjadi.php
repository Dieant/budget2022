<?php
    if (isset($filters['tahap']) && $filters['tahap'] == 'pakbp') {
        $tabel_semula = 'revisi6_';
        $tabel_dpn = 'pak_bukuputih_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'pakbb') {
        $tabel_dpn = 'pak_bukubiru_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'murni') {
        $tabel_semula='murni_bukubiru_';
        $tabel_dpn = 'murni_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'murnibp') {
        $tabel_semula='murni_bukuputih_';
        $tabel_dpn = 'murni_bukuputih_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'murnibb') {
        $tabel_semula='murni_bukuputih_';
        $tabel_dpn = 'murni_bukubiru_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'murnibbpraevagub') {
        $tabel_dpn = 'murni_bukubiru_praevagub_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi1') {
        $tabel_semula='murni_';
        $tabel_dpn = 'revisi1_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi1_1') {
        $tabel_dpn = 'revisi1_1_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi2') {
        $tabel_semula='revisi1_';
        $tabel_dpn = 'revisi2_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi2_1') {
        $tabel_dpn = 'revisi2_1_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi2_2') {
        $tabel_dpn = 'revisi2_2_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi3') {
        $tabel_semula='revisi2_';
        $tabel_dpn = 'revisi3_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi3_1') {
        $tabel_dpn = 'revisi3_1_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi4') {
        $tabel_semula='revisi3_';
        $tabel_dpn = 'revisi4_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi5') {
        $tabel_semula='revisi4_';
        $tabel_dpn = 'revisi5_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi6') {
        $tabel_semula='revisi5_';
        $tabel_dpn = 'revisi6_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi7') {
        $tabel_dpn = 'revisi7_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi8') {
        $tabel_dpn = 'revisi8_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi9') {
        $tabel_dpn = 'revisi9_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi10') {
        $tabel_dpn = 'revisi10_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'rkua') {
        $tabel_dpn = 'rkua_';
    } else {
        $tabel_semula='';
        $tabel_dpn = 'dinas_';
    }

$query = "select alokasi as nilai
		from " . sfConfig::get('app_default_schema') . ".pagu
		where kode_kegiatan ='" . $master_kegiatan->getKodeKegiatan() . "' and unit_id ='" . $master_kegiatan->getUnitId() . "'";

$con = Propel::getConnection();
$stmt = $con->prepareStatement($query);
$rs = $stmt->executeQuery();
while ($rs->next()) {
    $nilai = $rs->getFloat('nilai');
}

$query2 = "select sum(nilai_anggaran) as nilai_semula
		from " . sfConfig::get('app_default_schema') . "." . $tabel_semula. "rincian_detail
		where status_hapus=false and kegiatan_code ='" . $master_kegiatan->getKodeKegiatan() . "' and unit_id ='" . $master_kegiatan->getUnitId() . "'";

$con = Propel::getConnection();
$stmt2 = $con->prepareStatement($query2);
$rs2 = $stmt2->executeQuery();
while ($rs2->next()) {
    $nilai_semula = $rs2->getFloat('nilai_semula');
}

$query1 = "select tambahan_pagu as tambahan_pagu
		from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn. "master_kegiatan
		where kode_kegiatan ='" . $master_kegiatan->getKodeKegiatan() . "' and unit_id ='" . $master_kegiatan->getUnitId() . "'";

$stmt1 = $con->prepareStatement($query1);
$rs1 = $stmt1->executeQuery();
while ($rs1->next()) {
    $tambahan_pagu = $rs1->getFloat('tambahan_pagu');
}

if($master_kegiatan->getIsDak()==true)
{
	$nilai_menjadi=$nilai + $tambahan_pagu ;
}
else
{
	$nilai_menjadi= $nilai_semula  + $tambahan_pagu ;
}


echo number_format($nilai_menjadi, 0, ',', '.');
//irul 19 feb 2014 - sama kayak dinas
if ($sf_user->getNamaUser() == 'parlemen2') {
    echo ' ' . link_to(image_tag('/sf/sf_admin/images/edit_icon.png', array('alt' => __('RKA'), 'title' => __('RKA'))), 'report/printrekeningParlemen?kode_kegiatan=' . $master_kegiatan->getKodeKegiatan() . '&unit_id=' . $master_kegiatan->getUnitId());
}
?>
