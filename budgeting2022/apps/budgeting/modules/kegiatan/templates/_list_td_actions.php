<?php
    if (sfConfig::get('app_tahap_edit') == 'murni') {
        $nama_sistem = 'Pra-RKA';
    } elseif (sfConfig::get('app_tahap_edit') == 'pak') {
        $nama_sistem = 'PAK';
    } elseif (sfConfig::get('app_tahap_edit') == 'penyesuaian') {
        $nama_sistem = 'Penyesuaian';
    } else {
        $nama_sistem = 'Revisi';
    }
echo "<td class='tombol_actions'>";
    echo link_to('<i class="fas fa-pencil-alt"></i>', 'kegiatan/edit?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan() . '&tahap=' . $master_kegiatan->getTahap(), array('alt' => __('Mengubah Nilai Anggaran'), 'title' => __('Mengubah Nilai Anggaran'), 'class' => 'btn btn-outline-primary btn-sm'));
    echo link_to('<i class="fa fa-file-pdf"></i>', 'report/buatPDFrka?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan(), array('alt' => __('Pdf RKA'), 'title' => __('Pdf RKA'), 'class' => 'btn btn-outline-info btn-sm'));
    echo link_to('<i class="fa fa-th-list"></i>', 'report/tampillaporan?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan(), array('alt' => __('Laporan Perbandingan Komponen'), 'title' => __('Laporan Perbandingan Komponen'), 'class' => 'btn btn-outline-warning btn-sm'));
    $nama = $sf_user->getNamaLogin();
    if ($nama == 'superadmin') 
    {
        echo link_to('<i class="fa fa-download"></i>', 'kegiatan/tarikrka?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan() . '&tahap=' . $master_kegiatan->getTahap() . '&tarik=' . md5('project'),  array('confirm' => 'Apakah anda yakin untuk menaruh data SKPD dengan kegiatan ' . $master_kegiatan->getKodeKegiatan() . ' ini ke e-Project? ', 'alt' => __('Menarik RKA ke e-Project'), 'title' => __('Menarik RKA ke e-Project'), 'class' => 'btn btn-outline-secondary btn-sm'));
        echo link_to('<i class="fa fa-backward"></i>', 'kegiatan/tarikrevisi?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan() . '&tarik=' . md5('dinas'), array('confirm' => 'Apakah anda yakin untuk menaruh data SKPD dengan kegiatan ' . $master_kegiatan->getKodeKegiatan() . ' ini ke ' . $nama_sistem . '? ', 'alt' => __("Menarik RKA ke $nama_sistem"), 'title' => __("Menarik RKA ke $nama_sistem"), 'class' => 'btn btn-outline-danger btn-sm'));
    } 
    echo link_to('<i class="fa fa-list-ol"></i>', 'report/bandingRekeningSimple?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan(), array('alt' => __('Laporan Perbandingan Rekening'), 'title' => __('Laporan Perbandingan Rekening'), 'class' => 'btn btn-outline-info btn-sm'));
?>
     
        <!-- <li>
            <?php
                echo '<li>'.link_to('<i class="fa fa-history"></i>', 'historyuser/historylist?unit_id=' . $master_kegiatan->getUnitId() . '&kegiatan_code=' . $master_kegiatan->getKodeKegiatan(),  array('alt' => __('History User'), 'title' => __('History User'), 'class' => 'btn btn-flat btn-default btn-sm', 'style' => 'width:35px')).'</li>';
            ?>
        </li>  
        <li>
            <?php
            $kegiatan_code = $master_kegiatan->getKodeKegiatan();
            $unit_id = $master_kegiatan->getUnitId();

            $c = new Criteria();
            $c->add(RincianPeer::KEGIATAN_CODE, $kegiatan_code);
            $c->add(RincianPeer::UNIT_ID, $unit_id);
            $rs_rincian = RincianPeer::doSelectOne($c);
            if ($rs_rincian) {
                $posisi = $rs_rincian->getRincianLevel();
                $kunci = $rs_rincian->getLock();
            }
            if ($posisi == '2') {
                echo link_to(image_tag('/sf/sf_admin/images/lock.png', array('alt' => __('Klik untuk buka ke Peneliti'), 'title' => __('Klik untuk buka ke Peneliti'))), 'kegiatan/kuncirka?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan() . '&kunci=' . md5('dinas'));
            } else if ($posisi == '3') {
                echo link_to(image_tag('/sf/sf_admin/images/unlock.png', array('alt' => __('Klik untuk buka ke Dinas'), 'title' => __('Klik untuk buka ke Dinas'))), 'kegiatan/bukarka?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan() . '&buka=' . md5('dinas'));
            }
            ?>
        </li>
        <li>
            <?php
            if ($kunci == TRUE) {
                echo link_to(image_tag('/images/gembokx.gif', array('alt' => __('Klik untuk Buka RKA'), 'width' => '20', 'height' => '20', 'title' => __('Klik untuk Buka RKA'))), 'kegiatan/bukadp?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan());
            } else if ($kunci == FALSE) {
                echo link_to(image_tag('/images/gembok.gif', array('alt' => __('Klik untuk Kunci RKA'), 'width' => '20', 'height' => '20', 'title' => __('Klik untuk Kunci RKA'))), 'kegiatan/kuncidp?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan());
            }
            ?>
        </li>
        <li><?php echo link_to(image_tag('/sf/sf_admin/images/list.png', array('alt' => __('Perbandingan Buku Putih'), 'title' => __('Perbandingan Buku Putih'))), 'report/tampillaporanBukuPutih?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan()) ?></li>-->        
           
        <?php
        if ($sf_user->getNamaUser() != 'peninjau' && $sf_user->getNamaUser() != 'inspect' && $sf_user->getNamaUser() != 'dppk' && $sf_user->getNamaUser() != 'bppk' && $sf_user->getNamaUser() != 'wawali' && $sf_user->getNamaUser() != 'walikota' && $sf_user->getNamaUser() != 'sekda' && $sf_user->getNamaUser() != 'asisten' && $sf_user->getNamaUser() != 'parlemen2' && $sf_user->getNamaUser() != 'masger') {
            //irul 10 feb 2014 - lock unlock komponen
            $terkunci = 0;
            $terbuka = 0;
            $kegiatan_code = $master_kegiatan->getKodeKegiatan();
            $unit_id = $master_kegiatan->getUnitId();

            $query = "select count(*) as jumlah from " . sfConfig::get('app_default_schema') . ".rincian_detail "
                    . "where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and status_hapus = false and lock_subtitle = 'LOCK' and rekening_code <> '5.2.1.04.01'";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs1 = $stmt->executeQuery();
            while ($rs1->next()) {
                $terkunci = $rs1->getString('jumlah');
            }
            $query = "select count(*) as jumlah from " . sfConfig::get('app_default_schema') . ".rincian_detail "
                    . "where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and status_hapus = false and lock_subtitle = '' and rekening_code <> '5.2.1.04.01'";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs1 = $stmt->executeQuery();
            while ($rs1->next()) {
                $terbuka = $rs1->getString('jumlah');
            }
            // if ($terbuka == 0 && $terkunci > 1) {
            //     echo '<li>' . link_to(image_tag('/images/Lock-Unlock-icon.png', array('width' => '18', 'height' => '18', 'alt' => __('Klik untuk buka Kunci Komponen'), 'title' => __('Klik untuk buka Kunci Komponen'))), 'kegiatan/bukaKomponenAll?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan()) . '</li>&nbsp;';
            // } else if ($terkunci == 0 && $terbuka > 1) {
            //     echo '<li>' . link_to(image_tag('/images/Lock-Lock-icon.png', array('width' => '18', 'height' => '18', 'alt' => __('Klik untuk tutup Kunci Komponen'), 'title' => __('Klik untuk tutup Kunci Komponen'))), 'kegiatan/tutupKomponenAll?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan()) . '</li>&nbsp;';
            // } else if ($terkunci > $terbuka) {
            //     echo '<li>' . link_to(image_tag('/images/Lock-Unlock-icon.png', array('width' => '18', 'height' => '18', 'alt' => __('Klik untuk buka Kunci Komponen'), 'title' => __('Klik untuk buka Kunci Komponen'))), 'kegiatan/bukaKomponenAll?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan()) . '</li>&nbsp;';
            // } else if ($terkunci < $terbuka) {
            //     echo '<li>' . link_to(image_tag('/images/Lock-Lock-icon.png', array('width' => '18', 'height' => '18', 'alt' => __('Klik untuk tutup Kunci Komponen'), 'title' => __('Klik untuk tutup Kunci Komponen'))), 'kegiatan/tutupKomponenAll?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan()) . '</li>&nbsp;';
            // } else if ($terbuka == $terkunci) {
            //     echo '<li>' . link_to(image_tag('/images/Lock-Lock-icon.png', array('width' => '18', 'height' => '18', 'alt' => __('Klik untuk tutup Kunci Komponen'), 'title' => __('Klik untuk tutup Kunci Komponen'))), 'kegiatan/tutupKomponenAll?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan()) . '</li>&nbsp;';
            // }
            //irul 10 feb 2014 - lock unlock komponen
        }
        ?>
    <!--    <hr style='margin:0;padding:0'/>    
        <br/>
        <ul class="sf_admin_td_actions">
            <li><?php echo link_to(image_tag('/images/printButton.png', array('alt' => __('Pdf RKA Murni Buku Putih'), 'title' => __('Pdf RKA Murni Buku Putih'))), 'report/buatPDFMurniBukuPutihrka?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan()) ?></li>
            <li><?php echo link_to(image_tag('/images/printButton.png', array('alt' => __('Pdf Rekening Murni Buku Putih'), 'title' => __('Pdf Rekening Murni Buku Putih'))), 'report/buatPDFMurniBukuPutihrkarekening?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan()) ?></li>
    
        </ul>-->
    <!--</ul>-->
    <!--    <hr/>
        <ul class="sf_admin_td_actions">
            <li><?php echo link_to(image_tag('/images/printButton.png', array('alt' => __('Print Rekening RKA'), 'title' => __('Print Rekening RKA'))), 'report/buatPDFPrintRekening?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan()) ?></li>
        </ul>
        <hr/>
        <ul class="sf_admin_td_actions">
            <li><?php echo link_to(image_tag('/images/printButton.png', array('alt' => __('Pdf RKA Fisik'), 'title' => __('Pdf RKA Fisik'))), 'report/buatPDFrkafisik?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan()) ?></li>
    
        </ul>-->
</td>
