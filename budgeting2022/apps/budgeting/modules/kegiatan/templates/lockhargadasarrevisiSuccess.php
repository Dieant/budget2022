<?php
use_helper('Url', 'Javascript', 'Form', 'Object');
if ($act == 'unlock') {
    echo "<div class='btn-group'>";
    echo link_to_function('<i class="fa fa-money"></i>', '', array('class' => 'btn btn-success btn-flat btn-sm', 'disable' => true));
    if ($sf_user->getNamaUser() != 'peninjau' && $sf_user->getNamaUser() != 'dppk' && $sf_user->getNamaUser() != 'bppk' && $sf_user->getNamaUser() != 'wawali' && $sf_user->getNamaUser() != 'walikota' && $sf_user->getNamaUser() != 'sekda' && $sf_user->getNamaUser() != 'asisten' && $sf_user->getNamaUser() != 'masger') {
        echo link_to_function('Lock Harga Dasar', 'execLockHargaDasarRevisi("lock","' . $id . '","' . $no . '","' . $unit_id . '","' . $kegiatan . '")', array('class' => 'btn btn-success btn-flat btn-sm'));
    }
    echo "</div>";
} else {
    echo "<div class='btn-group'>";
    if ($sf_user->getNamaUser() != 'peninjau' && $sf_user->getNamaUser() != 'dppk' && $sf_user->getNamaUser() != 'bppk' && $sf_user->getNamaUser() != 'wawali' && $sf_user->getNamaUser() != 'walikota' && $sf_user->getNamaUser() != 'sekda' && $sf_user->getNamaUser() != 'asisten' && $sf_user->getNamaUser() != 'masger') {
        echo link_to_function('Unlock Harga Dasar', 'execLockHargaDasarRevisi("unlock","' . $id . '","' . $no . '","' . $unit_id . '","' . $kegiatan . '")', array('class' => 'btn btn-success btn-flat btn-sm'));
    }
    echo "</div>";
}

?>