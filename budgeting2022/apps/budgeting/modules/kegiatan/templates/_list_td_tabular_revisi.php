<?php
   if (isset($filters['tahap']) && $filters['tahap'] == 'pakbp') {
        $tabel_semula = 'revisi6_';
        $tabel_dpn = 'pak_bukuputih_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'pakbb') {
        $tabel_dpn = 'pak_bukubiru_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'murni') {
        $tabel_semula='murni_bukubiru_';
        $tabel_dpn = 'murni_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'murnibp') {
        $tabel_semula='murni_bukuputih_';
        $tabel_dpn = 'murni_bukuputih_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'murnibb') {
        $tabel_semula='murni_bukuputih_';
        $tabel_dpn = 'murni_bukubiru_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'murnibbpraevagub') {
        $tabel_dpn = 'murni_bukubiru_praevagub_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi1') {
        $tabel_semula='murni_';
        $tabel_dpn = 'revisi1_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi1_1') {
        $tabel_dpn = 'revisi1_1_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi2') {
        $tabel_semula = 'revisi1_';
        $tabel_dpn = 'revisi2_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi3') {
        $tabel_semula = 'revisi2_';
        $tabel_dpn = 'revisi3_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi3_1') {
        $tabel_semula = 'revisi2_';
        $tabel_dpn = 'revisi3_1_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi4') {
        $tabel_semula = 'revisi3_';
        $tabel_dpn = 'revisi4_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi5') {
        $tabel_semula = 'revisi4_';
        $tabel_dpn = 'revisi5_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi6') {
        $tabel_semula = 'revisi5_';
        $tabel_dpn = 'revisi6_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi7') {
        $tabel_dpn = 'revisi7_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi8') {
        $tabel_dpn = 'revisi8_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi9') {
        $tabel_dpn = 'revisi9_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi10') {
        $tabel_dpn = 'revisi10_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'rkua') {
        $tabel_dpn = 'rkua_';
    } else {
        $tabel_semula='';
        $tabel_dpn = 'dinas_';
    }
    $kode = $master_kegiatan->getKodeKegiatan();
    $unit = $master_kegiatan->getUnitId();

    $query = "SELECT * from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "rincian_detail where ((note_peneliti is not Null and note_peneliti <> '') or (note_skpd is not Null and note_skpd <> '')) and unit_id = '$unit' and kegiatan_code = '$kode' and status_hapus = false ";
    //diambil nilai terpakai
    $con = Propel::getConnection();
    $statement = $con->prepareStatement($query);
    $rs = $statement->executeQuery();
    $jml = $rs->getRecordCount();

    $query2 = "SELECT * from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "master_kegiatan where ((catatan is not Null and trim(catatan) <> '') or (catatan_pembahasan is not Null and trim(catatan_pembahasan) <> '')) and unit_id = '$unit' and kode_kegiatan = '$kode' and tahap='". $filters['tahap'] ."'";
    //diambil nilai terpakai
    $con = Propel::getConnection();
    $statement2 = $con->prepareStatement($query2);
    $rs = $statement2->executeQuery();
    $jml2 = $rs->getRecordCount();
    // jika ada perubahan di dalam kegiatan
    if ($jml > 0 or $jml2 > 0) {
        $queryrev = "select k.unit_id, k.kode_kegiatan, k.tahap as tahap_terakhir from " . sfConfig::get('app_default_schema') .".". $tabel_dpn ."master_kegiatan k where unit_id = '".$unit."' and kode_kegiatan = '".$kode."'"            ;
        $conrev = Propel::getConnection();
        $statementrev = $conrev->prepareStatement($queryrev);
        $rsqrev = $statementrev->executeQuery();
        $z = 0;
        $rsqrev->first();
        $var_tahap = $filters['tahap'];
        do {
            if ($var_tahap != "") {
                if ($var_tahap == 'pakbp' or $var_tahap == 'pakbb') {
                    $var_tahap = 'pak';
                }
                if ($rsqrev->getString('tahap_terakhir') == $var_tahap) 
                {   
                    $warna = "#e8f3f1";//pink
                }
                elseif ($rsqrev->getString('tahap_terakhir') != $var_tahap) 
                {
                    if ($odd == 0) {
                        $warna = "";
                    }
                    elseif ($odd == 1) {
                        $warna = "#e8f3f1";
                    }
                }
            }
            elseif ($var_tahap == "") {
                $warna = '#e8f3f1';//pink
            }
        } while ($rsqrev->next());
    ?>
    <td style="background: <?php echo $warna;?>;"><?php echo $master_kegiatan->getKodekegiatan(); ?></td>
    <!-- nama kegiatan -->
    <td style="background: <?php echo $warna;?>;">
    <?php
        if (isset($filters['tahap']) && $filters['tahap'] == 'pakbp') {
            $e = new Criteria;
            $e->add(PakBukuPutihRincianPeer::KEGIATAN_CODE, $master_kegiatan->getKodeKegiatan());
            $e->add(PakBukuPutihRincianPeer::UNIT_ID, $master_kegiatan->getUnitId());
            $x = PakBukuPutihRincianPeer::doSelectOne($e);
        } elseif (isset($filters['tahap']) && $filters['tahap'] == 'pakbb') {
            $e = new Criteria;
            $e->add(PakBukuBiruRincianPeer::KEGIATAN_CODE, $master_kegiatan->getKodeKegiatan());
            $e->add(PakBukuBiruRincianPeer::UNIT_ID, $master_kegiatan->getUnitId());
            $x = PakBukuBiruRincianPeer::doSelectOne($e);
        } elseif (isset($filters['tahap']) && $filters['tahap'] == 'murni') {
            $e = new Criteria;
            $e->add(MurniRincianPeer::KEGIATAN_CODE, $master_kegiatan->getKodeKegiatan());
            $e->add(MurniRincianPeer::UNIT_ID, $master_kegiatan->getUnitId());
            $x = MurniRincianPeer::doSelectOne($e);
        } elseif (isset($filters['tahap']) && $filters['tahap'] == 'murnibp') {
            $e = new Criteria;
            $e->add(MurniBukuPutihRincianPeer::KEGIATAN_CODE, $master_kegiatan->getKodeKegiatan());
            $e->add(MurniBukuPutihRincianPeer::UNIT_ID, $master_kegiatan->getUnitId());
            $x = MurniBukuPutihRincianPeer::doSelectOne($e);
        } elseif (isset($filters['tahap']) && $filters['tahap'] == 'murnibb') {
            $e = new Criteria;
            $e->add(MurniBukuBiruRincianPeer::KEGIATAN_CODE, $master_kegiatan->getKodeKegiatan());
            $e->add(MurniBukuBiruRincianPeer::UNIT_ID, $master_kegiatan->getUnitId());
            $x = MurniBukuBiruRincianPeer::doSelectOne($e);
        } elseif (isset($filters['tahap']) && $filters['tahap'] == 'murnibbpraevagub') {
            $e = new Criteria;
            $e->add(MurniBukubiruPraevagubRincianPeer::KEGIATAN_CODE, $master_kegiatan->getKodeKegiatan());
            $e->add(MurniBukubiruPraevagubRincianPeer::UNIT_ID, $master_kegiatan->getUnitId());
            $x = MurniBukubiruPraevagubRincianPeer::doSelectOne($e);
        } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi1') {
            $e = new Criteria;
            $e->add(Revisi1RincianPeer::KEGIATAN_CODE, $master_kegiatan->getKodeKegiatan());
            $e->add(Revisi1RincianPeer::UNIT_ID, $master_kegiatan->getUnitId());
            $x = Revisi1RincianPeer::doSelectOne($e);
        } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi1_1') {
            $e = new Criteria;
            $e->add(Revisi1bRincianPeer::KEGIATAN_CODE, $master_kegiatan->getKodeKegiatan());
            $e->add(Revisi1bRincianPeer::UNIT_ID, $master_kegiatan->getUnitId());
            $x = Revisi1bRincianPeer::doSelectOne($e);
        } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi2') {
            $e = new Criteria;
            $e->add(Revisi2RincianPeer::KEGIATAN_CODE, $master_kegiatan->getKodeKegiatan());
            $e->add(Revisi2RincianPeer::UNIT_ID, $master_kegiatan->getUnitId());
            $x = Revisi2RincianPeer::doSelectOne($e);
        } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi2_1') {
            $e = new Criteria;
            $e->add(Revisi21RincianPeer::KEGIATAN_CODE, $master_kegiatan->getKodeKegiatan());
            $e->add(Revisi21RincianPeer::UNIT_ID, $master_kegiatan->getUnitId());
            $x = Revisi21RincianPeer::doSelectOne($e);
        } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi2_2') {
            $e = new Criteria;
            $e->add(Revisi22RincianPeer::KEGIATAN_CODE, $master_kegiatan->getKodeKegiatan());
            $e->add(Revisi22RincianPeer::UNIT_ID, $master_kegiatan->getUnitId());
            $x = Revisi22RincianPeer::doSelectOne($e);
        } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi3') {
            $e = new Criteria;
            $e->add(Revisi3RincianPeer::KEGIATAN_CODE, $master_kegiatan->getKodeKegiatan());
            $e->add(Revisi3RincianPeer::UNIT_ID, $master_kegiatan->getUnitId());
            $x = Revisi3RincianPeer::doSelectOne($e);
        } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi3_1') {
            $e = new Criteria;
            $e->add(Revisi31RincianPeer::KEGIATAN_CODE, $master_kegiatan->getKodeKegiatan());
            $e->add(Revisi31RincianPeer::UNIT_ID, $master_kegiatan->getUnitId());
            $x = Revisi31RincianPeer::doSelectOne($e);
        } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi4') {
            $e = new Criteria;
            $e->add(Revisi4RincianPeer::KEGIATAN_CODE, $master_kegiatan->getKodeKegiatan());
            $e->add(Revisi4RincianPeer::UNIT_ID, $master_kegiatan->getUnitId());
            $x = Revisi4RincianPeer::doSelectOne($e);
        } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi5') {
            $e = new Criteria;
            $e->add(Revisi5RincianPeer::KEGIATAN_CODE, $master_kegiatan->getKodeKegiatan());
            $e->add(Revisi5RincianPeer::UNIT_ID, $master_kegiatan->getUnitId());
            $x = Revisi5RincianPeer::doSelectOne($e);
        } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi6') {
            $e = new Criteria;
            $e->add(Revisi6RincianPeer::KEGIATAN_CODE, $master_kegiatan->getKodeKegiatan());
            $e->add(Revisi6RincianPeer::UNIT_ID, $master_kegiatan->getUnitId());
            $x = Revisi6RincianPeer::doSelectOne($e);
        } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi7') {
            $e = new Criteria;
            $e->add(Revisi7RincianPeer::KEGIATAN_CODE, $master_kegiatan->getKodeKegiatan());
            $e->add(Revisi7RincianPeer::UNIT_ID, $master_kegiatan->getUnitId());
            $x = Revisi7RincianPeer::doSelectOne($e);
        } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi8') {
            $e = new Criteria;
            $e->add(Revisi8RincianPeer::KEGIATAN_CODE, $master_kegiatan->getKodeKegiatan());
            $e->add(Revisi8RincianPeer::UNIT_ID, $master_kegiatan->getUnitId());
            $x = Revisi8RincianPeer::doSelectOne($e);
        } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi9') {
            $e = new Criteria;
            $e->add(Revisi9RincianPeer::KEGIATAN_CODE, $master_kegiatan->getKodeKegiatan());
            $e->add(Revisi9RincianPeer::UNIT_ID, $master_kegiatan->getUnitId());
            $x = Revisi9RincianPeer::doSelectOne($e);
        } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi10') {
            $e = new Criteria;
            $e->add(Revisi10RincianPeer::KEGIATAN_CODE, $master_kegiatan->getKodeKegiatan());
            $e->add(Revisi10RincianPeer::UNIT_ID, $master_kegiatan->getUnitId());
            $x = Revisi10RincianPeer::doSelectOne($e);
        } elseif (isset($filters['tahap']) && $filters['tahap'] == 'rkua') {
            $e = new Criteria;
            $e->add(RkuaRincianPeer::KEGIATAN_CODE, $master_kegiatan->getKodeKegiatan());
            $e->add(RkuaRincianPeer::UNIT_ID, $master_kegiatan->getUnitId());
            $x = RkuaRincianPeer::doSelectOne($e);
        } else {
            $e = new Criteria;
            $e->add(DinasRincianPeer::KEGIATAN_CODE, $master_kegiatan->getKodeKegiatan());
            $e->add(DinasRincianPeer::UNIT_ID, $master_kegiatan->getUnitId());
            $x = DinasRincianPeer::doSelectOne($e);
        }
        if ($x) {
            if ($x->getLock() == TRUE) {
                $gambar = (image_tag('gembok.gif', array('width' => '16', 'height' => '16')));
            } elseif ($x->getLock() == FALSE) {
                $gambar = '';
            }
        }
        echo $master_kegiatan->getNamakegiatan() . $gambar;
        echo ' <font color=#FF0000>(' . $master_kegiatan->getUserId() . ')</font></br>';
        echo 'Kode: [ '.$master_kegiatan->getKegiatanId().' ]';
        ?>
    </td>

    <?php if (sfConfig::get('app_tahap_edit') == 'murni') { ?>
        <!-- semula murni -->
        <?php if ($filters['tahap'] != '')
        {?>       
        <td align="right" style="background: <?php echo $warna;?>;"><?php echo get_partial('nilaisemula', array('type' => 'list', 'master_kegiatan' => $master_kegiatan,'filters' => $filters)) ?> 
        </td>
        <?php 
        }
        else { ?>
        <td align="right" style="background: <?php echo $warna;?>;">
            <?php
            $query = "select alokasi from " . sfConfig::get('app_default_schema') . ".pagu where unit_id='$unit' and kode_kegiatan='$kode'";
            $stmt = $con->prepareStatement($query);
            $rs = $stmt->executeQuery();
            if ($rs->next()) {
                echo number_format($rs->getString('alokasi'), 0, ',', '.');
            }
            ?>
        </td>
        <?php } ?>

        <!-- pagu perangkaan murni -->
        <!-- <td align="right" style="background: pink">
            <?php
            // $query = "select alokasi_dana from " . sfConfig::get('app_default_schema') . ".backup_murni_bukubiru4_master_kegiatan where unit_id='$unit' and kode_kegiatan='$kode'";
            // $stmt = $con->prepareStatement($query);
            // $rs = $stmt->executeQuery();
            // if ($rs->next()) {
            //     echo number_format($rs->getString('alokasi_dana'), 0, ',', '.');
            // }
            ?>
        </td> -->
        
        <!-- pagu penyesuaian murni -->
        <!-- <td align="right" style="<b></b>ackground: pink">
            <?php // echo number_format($master_kegiatan->getAlokasidana(), 0, ',', '.'); ?>
        </td> -->
    <?php } else if (sfConfig::get('app_tahap_edit') != 'murni') { ?>
        <!--nilaisemula sudah
            revisi 1, revisi 2, revisi 3 masuk sini
        -->
        <td align="right" style="background: <?php echo $warna;?>;"><?php echo get_partial('nilaisemula', array('type' => 'list', 'master_kegiatan' => $master_kegiatan,'filters' => $filters)) ?> 
        </td>
        <?php if (sfConfig::get('app_tahap_edit') != 'pak') { ?>
        <td align="right" style="background: <?php echo $warna;?>;"><?php echo get_partial('nilaimenjadi', array('type' => 'list', 'master_kegiatan' => $master_kegiatan,'filters' => $filters)) ?> 
        </td>
        <?php } ?> 

    <?php } ?>

    <?php if (sfConfig::get('app_tahap_edit') == 'pak') { ?>
        <td align="right" style="background: <?php echo $warna;?>;">
            <?php
            $c = new Criteria();
            $c->add(PaguPakPeer::UNIT_ID, $master_kegiatan->getUnitId());
            $c->add(PaguPakPeer::KEGIATAN_CODE, $master_kegiatan->getKodeKegiatan());
            $pagu_pak = PaguPakPeer::doSelectOne($c);
            echo number_format($pagu_pak->getPagu(), 0, ',', '.');
            ?>
        </td>
    <?php } ?> 
    <!-- nilairincian_revisi sudah -->
    <td align="right" style="background: <?php echo $warna;?>; "><?php echo get_partial('nilairincian_revisi', array('type' => 'list', 'master_kegiatan' => $master_kegiatan, 'filters' => $filters)) ?></td>
    <td align="right" style="background: <?php echo $warna;?>;"><?php echo get_partial('selisih_revisi', array('type' => 'list', 'master_kegiatan' => $master_kegiatan, 'filters' => $filters)) ?></td>
     <?php if (sfConfig::get('app_tahap_edit') == 'pak' || sfConfig::get('app_tahap_edit') == 'murni') { ?>
    <td align="center" style="background: <?php echo $warna;?>; width: 200px">
        <div id="<?php echo 'tempat_ajax2_' . str_replace('.', '_', $master_kegiatan->getKodeKegiatan()) ?>" align="right">
            <?php
            if($filters['tahap'] == 'murni'  && $master_kegiatan->getStatusLevel()=='7')
            echo number_format(0, 0, ',', '.') . '<br>';
            else 
            echo number_format($master_kegiatan->getTambahanPagu(), 0, ',', '.') . '<br>';
            
            if ((!isset($filters['tahap']) || $filters['tahap'] == '') && (sfConfig::get('app_tahap_edit') == 'murni' || sfConfig::get('app_tahap_edit') == 'pak' || sfConfig::get('app_tahap_detail') == 'revisi3')) {
                echo link_to_function('<i class="fas fa-plus-circle"></i>', 'showTambahanPagu("edit","' . $master_kegiatan->getTambahanPagu() . '","' . $master_kegiatan->getUnitId() . '","' . $master_kegiatan->getKodeKegiatan() . '","' . str_replace('.', '_', $master_kegiatan->getKodeKegiatan()) . '")', array('class' => 'btn btn-outline-primary btn-sm'));
            }
            ?>
        </div>
    </td>
    <?php } ?> 
    <td align="right" style="background: <?php echo $warna;?>;"><?php echo get_partial('posisi2', array('type' => 'list', 'master_kegiatan' => $master_kegiatan, 'filters' => $filters)) ?></td>
    <td align="right" style="background: <?php echo $warna;?>;"><?php echo get_partial('posisi_revisi', array('type' => 'list', 'master_kegiatan' => $master_kegiatan, 'filters' => $filters)) ?></td>

<?php 
    }
    //jika tidak ada perubahan di dalam kegiatan
    else 
    {
?>
    <td>
        <?php echo $master_kegiatan->getKodekegiatan(); ?>
    </td>
    <td><?php
        if (isset($filters['tahap']) && $filters['tahap'] == 'pakbp') {
            $e = new Criteria;
            $e->add(PakBukuPutihRincianPeer::KEGIATAN_CODE, $master_kegiatan->getKodeKegiatan());
            $e->add(PakBukuPutihRincianPeer::UNIT_ID, $master_kegiatan->getUnitId());
            $x = PakBukuPutihRincianPeer::doSelectOne($e);
        } elseif (isset($filters['tahap']) && $filters['tahap'] == 'pakbb') {
            $e = new Criteria;
            $e->add(PakBukuBiruRincianPeer::KEGIATAN_CODE, $master_kegiatan->getKodeKegiatan());
            $e->add(PakBukuBiruRincianPeer::UNIT_ID, $master_kegiatan->getUnitId());
            $x = PakBukuBiruRincianPeer::doSelectOne($e);
        } elseif (isset($filters['tahap']) && $filters['tahap'] == 'murni') {
            $e = new Criteria;
            $e->add(MurniRincianPeer::KEGIATAN_CODE, $master_kegiatan->getKodeKegiatan());
            $e->add(MurniRincianPeer::UNIT_ID, $master_kegiatan->getUnitId());
            $x = MurniRincianPeer::doSelectOne($e);
        } elseif (isset($filters['tahap']) && $filters['tahap'] == 'murnibp') {
            $e = new Criteria;
            $e->add(MurniBukuPutihRincianPeer::KEGIATAN_CODE, $master_kegiatan->getKodeKegiatan());
            $e->add(MurniBukuPutihRincianPeer::UNIT_ID, $master_kegiatan->getUnitId());
            $x = MurniBukuPutihRincianPeer::doSelectOne($e);
        } elseif (isset($filters['tahap']) && $filters['tahap'] == 'murnibb') {
            $e = new Criteria;
            $e->add(MurniBukuBiruRincianPeer::KEGIATAN_CODE, $master_kegiatan->getKodeKegiatan());
            $e->add(MurniBukuBiruRincianPeer::UNIT_ID, $master_kegiatan->getUnitId());
            $x = MurniBukuBiruRincianPeer::doSelectOne($e);
        } elseif (isset($filters['tahap']) && $filters['tahap'] == 'murnibbpraevagub') {
            $e = new Criteria;
            $e->add(MurniBukubiruPraevagubRincianPeer::KEGIATAN_CODE, $master_kegiatan->getKodeKegiatan());
            $e->add(MurniBukubiruPraevagubRincianPeer::UNIT_ID, $master_kegiatan->getUnitId());
            $x = MurniBukubiruPraevagubRincianPeer::doSelectOne($e);
        } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi1') {
            $e = new Criteria;
            $e->add(Revisi1RincianPeer::KEGIATAN_CODE, $master_kegiatan->getKodeKegiatan());
            $e->add(Revisi1RincianPeer::UNIT_ID, $master_kegiatan->getUnitId());
            $x = Revisi1RincianPeer::doSelectOne($e);
        } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi1_1') {
            $e = new Criteria;
            $e->add(Revisi1bRincianPeer::KEGIATAN_CODE, $master_kegiatan->getKodeKegiatan());
            $e->add(Revisi1bRincianPeer::UNIT_ID, $master_kegiatan->getUnitId());
            $x = Revisi1bRincianPeer::doSelectOne($e);
        } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi2') {
            $e = new Criteria;
            $e->add(Revisi2RincianPeer::KEGIATAN_CODE, $master_kegiatan->getKodeKegiatan());
            $e->add(Revisi2RincianPeer::UNIT_ID, $master_kegiatan->getUnitId());
            $x = Revisi2RincianPeer::doSelectOne($e);
        } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi2_1') {
            $e = new Criteria;
            $e->add(Revisi21RincianPeer::KEGIATAN_CODE, $master_kegiatan->getKodeKegiatan());
            $e->add(Revisi21RincianPeer::UNIT_ID, $master_kegiatan->getUnitId());
            $x = Revisi21RincianPeer::doSelectOne($e);
        } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi2_2') {
            $e = new Criteria;
            $e->add(Revisi22RincianPeer::KEGIATAN_CODE, $master_kegiatan->getKodeKegiatan());
            $e->add(Revisi22RincianPeer::UNIT_ID, $master_kegiatan->getUnitId());
            $x = Revisi22RincianPeer::doSelectOne($e);
        } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi3') {
            $e = new Criteria;
            $e->add(Revisi3RincianPeer::KEGIATAN_CODE, $master_kegiatan->getKodeKegiatan());
            $e->add(Revisi3RincianPeer::UNIT_ID, $master_kegiatan->getUnitId());
            $x = Revisi3RincianPeer::doSelectOne($e);
        } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi3_1') {
            $e = new Criteria;
            $e->add(Revisi31RincianPeer::KEGIATAN_CODE, $master_kegiatan->getKodeKegiatan());
            $e->add(Revisi31RincianPeer::UNIT_ID, $master_kegiatan->getUnitId());
            $x = Revisi31RincianPeer::doSelectOne($e);
        } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi4') {
            $e = new Criteria;
            $e->add(Revisi4RincianPeer::KEGIATAN_CODE, $master_kegiatan->getKodeKegiatan());
            $e->add(Revisi4RincianPeer::UNIT_ID, $master_kegiatan->getUnitId());
            $x = Revisi4RincianPeer::doSelectOne($e);
        } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi5') {
            $e = new Criteria;
            $e->add(Revisi5RincianPeer::KEGIATAN_CODE, $master_kegiatan->getKodeKegiatan());
            $e->add(Revisi5RincianPeer::UNIT_ID, $master_kegiatan->getUnitId());
            $x = Revisi5RincianPeer::doSelectOne($e);
        } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi6') {
            $e = new Criteria;
            $e->add(Revisi6RincianPeer::KEGIATAN_CODE, $master_kegiatan->getKodeKegiatan());
            $e->add(Revisi6RincianPeer::UNIT_ID, $master_kegiatan->getUnitId());
            $x = Revisi6RincianPeer::doSelectOne($e);
        } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi7') {
            $e = new Criteria;
            $e->add(Revisi7RincianPeer::KEGIATAN_CODE, $master_kegiatan->getKodeKegiatan());
            $e->add(Revisi7RincianPeer::UNIT_ID, $master_kegiatan->getUnitId());
            $x = Revisi7RincianPeer::doSelectOne($e);
        } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi8') {
            $e = new Criteria;
            $e->add(Revisi8RincianPeer::KEGIATAN_CODE, $master_kegiatan->getKodeKegiatan());
            $e->add(Revisi8RincianPeer::UNIT_ID, $master_kegiatan->getUnitId());
            $x = Revisi8RincianPeer::doSelectOne($e);
        } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi9') {
            $e = new Criteria;
            $e->add(Revisi9RincianPeer::KEGIATAN_CODE, $master_kegiatan->getKodeKegiatan());
            $e->add(Revisi9RincianPeer::UNIT_ID, $master_kegiatan->getUnitId());
            $x = Revisi9RincianPeer::doSelectOne($e);
        } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi10') {
            $e = new Criteria;
            $e->add(Revisi10RincianPeer::KEGIATAN_CODE, $master_kegiatan->getKodeKegiatan());
            $e->add(Revisi10RincianPeer::UNIT_ID, $master_kegiatan->getUnitId());
            $x = Revisi10RincianPeer::doSelectOne($e);
        } elseif (isset($filters['tahap']) && $filters['tahap'] == 'rkua') {
            $e = new Criteria;
            $e->add(RkuaRincianPeer::KEGIATAN_CODE, $master_kegiatan->getKodeKegiatan());
            $e->add(RkuaRincianPeer::UNIT_ID, $master_kegiatan->getUnitId());
            $x = RkuaRincianPeer::doSelectOne($e);
        } else {
            $e = new Criteria;
            $e->add(DinasRincianPeer::KEGIATAN_CODE, $master_kegiatan->getKodeKegiatan());
            $e->add(DinasRincianPeer::UNIT_ID, $master_kegiatan->getUnitId());
            $x = DinasRincianPeer::doSelectOne($e);
        }
        if ($x) {
            if ($x->getLock() == TRUE) {
                $gambar = (image_tag('gembok.gif', array('width' => '16', 'height' => '16')));
            } elseif ($x->getLock() == FALSE) {
                $gambar = '';
            }
        }
        echo $master_kegiatan->getNamakegiatan() . $gambar;
        echo ' <font color=#FF0000>(' . $master_kegiatan->getUserId() . ')</font></br>';
        echo 'Kode: [ '.$master_kegiatan->getKegiatanId().' ]';
        ?>
        </td>

    <?php 
    if (sfConfig::get('app_tahap_edit') == 'murni') 
    { 
        if ($filters['tahap'] != '')
        {
        ?>
        <td align="right" ><?php echo get_partial('nilaisemula', array('type' => 'list', 'master_kegiatan' => $master_kegiatan,'filters' => $filters)) ?> 
        </td>
        <?php 
        } else {
        ?>
        <td align="right" >
            <?php
            $query = "select alokasi from " . sfConfig::get('app_default_schema') . ".pagu where unit_id='$unit' and kode_kegiatan='$kode'";
            $stmt = $con->prepareStatement($query);
            $rs = $stmt->executeQuery();
            if ($rs->next()) {
                echo number_format($rs->getString('alokasi'), 0, ',', '.');
            }
            ?>
        </td>
        <?php } ?>
        <!-- pagu perangkaan -->
        <!-- <td align="right">
            <?php
            // $query = "select alokasi_dana from " . sfConfig::get('app_default_schema') . ".backup_murni_bukubiru4_master_kegiatan where unit_id='$unit' and kode_kegiatan='$kode'";
            // $stmt = $con->prepareStatement($query);
            // $rs = $stmt->executeQuery();
            // if ($rs->next()) {
            //     echo number_format($rs->getString('alokasi_dana'), 0, ',', '.');
            // }
            ?>
        </td> -->
        
        <!-- pagu penyesuaian -->
        <!-- <td align="right" >
            <?php //echo number_format($master_kegiatan->getAlokasidana(), 0, ',', '.'); ?>
        </td> -->

    <?php 
    } 
    else if (sfConfig::get('app_tahap_edit') != 'murni') 
    { 
    ?> 
        <td align="right"><?php echo get_partial('nilaisemula', array('type' => 'list', 'master_kegiatan' => $master_kegiatan,'filters' => $filters)) ?> 
        </td>
         <?php if (sfConfig::get('app_tahap_edit') != 'pak') { ?>
        <td align="right"><?php echo get_partial('nilaimenjadi', array('type' => 'list', 'master_kegiatan' => $master_kegiatan,'filters' => $filters)) ?> 
        </td>
        <?php } ?> 
        
    <?php 
    } 
    ?>
    <?php if (sfConfig::get('app_tahap_edit') == 'pak') { ?>
        <td align="right">
            <?php
            $c = new Criteria();
            $c->add(PaguPakPeer::UNIT_ID, $master_kegiatan->getUnitId());
            $c->add(PaguPakPeer::KEGIATAN_CODE, $master_kegiatan->getKodeKegiatan());
            $pagu_pak = PaguPakPeer::doSelectOne($c);
            echo number_format($pagu_pak->getPagu(), 0, ',', '.');
            ?>
        </td>
    <?php } ?>
    <td align="right"><?php echo get_partial('nilairincian_revisi', array('type' => 'list', 'master_kegiatan' => $master_kegiatan, 'filters' => $filters)) ?></td>
    <td align="right"><?php echo get_partial('selisih_revisi', array('type' => 'list', 'master_kegiatan' => $master_kegiatan, 'filters' => $filters)) ?></td>
    <?php if (sfConfig::get('app_tahap_edit') == 'pak' || sfConfig::get('app_tahap_edit') == 'murni') { ?>
    <td align="center" style="width: 200px">
        <div id="<?php echo 'tempat_ajax2_' . str_replace('.', '_', $master_kegiatan->getKodeKegiatan()) ?>" align="right">
            <?php
            if( $filters['tahap'] == 'murni'  && $master_kegiatan->getStatusLevel() =='7')
            echo number_format(0, 0, ',', '.') . '<br>';
            else 
            echo number_format($master_kegiatan->getTambahanPagu(), 0, ',', '.') . '<br>';
            if ((!isset($filters['tahap']) || $filters['tahap'] == '') && (sfConfig::get('app_tahap_edit') == 'murni' || sfConfig::get('app_tahap_edit') == 'pak' || sfConfig::get('app_tahap_detail') == 'revisi3')) {
                echo link_to_function('<i class="fas fa-plus-circle"></i>', 'showTambahanPagu("edit","' . $master_kegiatan->getTambahanPagu() . '","' . $master_kegiatan->getUnitId() . '","' . $master_kegiatan->getKodeKegiatan() . '","' . str_replace('.', '_', $master_kegiatan->getKodeKegiatan()) . '")',  array('class' => 'btn btn-outline-primary btn-sm'));
            }
            ?>
        </div>
    </td>
    <?php } ?> 
    <td align="right"><?php echo get_partial('posisi2', array('type' => 'list', 'master_kegiatan' => $master_kegiatan, 'filters' => $filters)) ?></td>
    <td align="right"><?php echo get_partial('posisi_revisi', array('type' => 'list', 'master_kegiatan' => $master_kegiatan, 'filters' => $filters)) ?></td>


<?php } ?>
<!--fungsine iki--> 
<!--tombol e sing image pagu tambahan + catatan-->
<script>
    function showTambahanPagu(edit, pagu, unit, kegiatan, id) {
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/kegiatan/tambahanPaguRevisi/edit/" + edit + "/tambahPagu/" + pagu + "/unit_id/" + unit + "/kode_kegiatan/" + kegiatan + ".html",
            context: document.body
        }).done(function (msg) {
            $('#tempat_ajax2_' + id).html(msg);
        });
    }
</script>