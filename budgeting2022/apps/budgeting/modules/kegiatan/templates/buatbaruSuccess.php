<?php use_helper('I18N', 'Date', 'Url', 'Javascript', 'Form', 'Object', 'Number', 'Validation') ?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>Daftar Isian Rincian Komponen Kegiatan SKPD</h1>
</section>

<!-- Main content -->
<section class="content">
    <?php include_partial('kegiatan/list_messages') ?>
    <!-- Default box -->
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Mencari Komponen Berdasarkan Nama Komponen</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body">
            <?php echo form_tag('kegiatan/carikomponen', array('method' => 'get', 'class' => 'form-horizontal')) ?>
            <div class="form-group">
                <label class="col-sm-2 control-label">Nama Komponen</label>
                <div class="col-sm-10">
                    <?php echo input_tag('filters[nama_komponen]', isset($filters['nama_komponen']) ? $filters['nama_komponen'] : null, array('class' => 'form-control', 'placeholder' => 'Nama Komponen')); ?>
                </div>                        
                <?php
                echo input_hidden_tag('kegiatan', $sf_params->get('kegiatan'));
                echo input_hidden_tag('unit', $sf_params->get('unit'));
                ?>
            </div>
            <div id="sf_admin_container">
                <ul class="sf_admin_actions">
                    <li><?php echo submit_tag('cari', 'name=filter class=sf_admin_action_filter') ?></li>
                </ul>
            </div>
            <?php echo '</form>'; ?> 
        </div><!-- /.box-body -->
    </div><!-- /.box -->

    <!-- Default box -->
    <div class="box box-info">
        <div class="box-body">
            <div id="sf_admin_container">
                <div id="sf_admin_content">        
                    <?php echo form_tag('kegiatan/baruKegiatan') ?>
                    <table cellspacing="0" class="sf_admin_list">
                        <thead>
                            <tr>
                                <th><b>Nama</b></th>
                                <th>&nbsp;</th>
                                <th><b>Isian</b></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="sf_admin_row_0" align='right'>
                                <td>Kelompok Belanja</td>
                                <td align="center">:</td>
                                <td align="left">
                                    <?php
                                    $rekening_code = trim($sf_params->get('rekening'));
                                    $belanja_code = substr($rekening_code, 0, 5);
                                    $c = new Criteria();
                                    $c->add(KelompokBelanjaPeer::BELANJA_CODE, $belanja_code);
                                    $rs_belanja = KelompokBelanjaPeer::doSelectOne($c);
                                    if ($rs_belanja) {
                                        echo $rs_belanja->getBelanjaName();
                                    }
                                    ?></td>
                            </tr>
                            <tr class="sf_admin_row_1" align='right'>
                                <td>Kode Rekening</td>
                                <td align="center">:</td>
                                <td align="left">
                                    <?php
                                    $c = new Criteria();
                                    $c->add(RekeningPeer::REKENING_CODE, $rekening_code);
                                    $rs_rekening = RekeningPeer::doSelectOne($c);
                                    if ($rs_rekening) {
                                        $pajak_rekening = $rs_rekening->getRekeningPpn();
                                        echo $rekening_code . ' ' . $rs_rekening->getRekeningName();
                                    }
                                    ?></td>
                            </tr>
                            <tr class="sf_admin_row_0" align='right'>
                                <td>Komponen</td>
                                <td align="center">:</td>
                                <td align="left"><?php echo $rs_komponen->getKomponenName() ?></td>
                            </tr>
                            <tr class="sf_admin_row_1" align='right'>
                                <td>Harga</td>
                                <td align="center">:</td>
                                <?php
                                $komponen_id = $rs_komponen->getKomponenId();
                                $komponen_harga = $rs_komponen->getKomponenHarga();
                                if ((substr($komponen_id, 0, 14) == '23.01.01.04.12') or ( substr($komponen_id, 0, 14) == '23.01.01.04.13') or ( substr($komponen_id, 0, 11) == '23.04.04.01')) {
                                    ?>
                                    <td align='left'><?php echo number_format($komponen_harga, 3, ',', '.') ?></td>
                                    <?php
                                } else {
                                    ?>
                                    <td align='left'><?php echo '&nbsp;' . number_format($komponen_harga, 0, ',', '.') ?> </td>
                                    <?php
                                }
                                echo input_hidden_tag('harga', $komponen_harga);
                                ?>
                            </tr>
                            <tr class="sf_admin_row_0" align='right'>
                                <td>Satuan</td>
                                <td align="center">:</td>
                                <td align="left"><?php echo $rs_komponen->getSatuan(); ?></td>
                            </tr>
                            <tr class="sf_admin_row_1" align='right'>
                                <td>Pajak</td>
                                <td align="center">:</td>
                                <td align="left"><?php echo $sf_params->get('pajak') . '%' ?>
                                    <?php echo input_hidden_tag('pajakx', $sf_params->get('pajak')); ?>
                                </td>
                            </tr>
                            <tr class="sf_admin_row_0" align='right'>
                                <td><span style="color:red;">*</span> Subtitle</td>
                                <td align="center">:</td>
                                <td align="left">
                                    <?php
                                    $kode_sub = '';
                                    if ($sf_params->get('subtitle')) {
                                        $kode_sub = $sf_params->get('subtitle');
                                    }

                                    echo select_tag('subtitle', objects_for_select($rs_subtitleindikator, 'getSubId', 'getSubtitle', $kode_sub, 'include_custom=---Pilih Subtitle---'), array('class' => 'js-example-basic-single', 'style' => 'width:100%'));
                                    ?>
                                </td>
                            </tr>
                            <tr class="sf_admin_row_1" align='right'>
                                <td>Sub - Subtitle</td>
                                <td align="center">:</td>
                                <td align="left">
                                    <div id="indicator" style="display:none;" align="center"><dt>&nbsp;</dt><dd><b>Mohon Tunggu </b><?php echo image_tag('loading.gif', array('align' => 'absmiddle')) ?></dd></div>
                                    <?php
                                    if ($sf_params->get('sub')) {
                                        $kode_subsubtitle = $sf_params->get('sub');
                                        $d = new Criteria();
                                        $d->add(RincianSubParameterPeer::KODE_SUB, $kode_subsubtitle);
                                        $rs_rinciansubparameter = RincianSubParameter::doSelectOne($d);
                                        if ($rs_rinciansubparameter) {
                                            echo select_tag('sub', options_for_select(array($rs_rinciansubparameter->getKodeSub() => $rs_rinciansubparameter->getNewSubtitle()), $rs_rinciansubparameter->getKodeSub(), 'include_custom=---Pilih Subtitle Dulu---'), Array('id' => 'sub1', 'class' => 'js-example-basic-single', 'style' => 'width:100%'));
                                        }
                                    } elseif (!$sf_params->get('sub')) {       //select_tag('sub',options_for_select(array(), '','include_custom=---Pilih Subtitle Dulu---'), Array('id'=>'sub1'));
                                        echo select_tag('sub', options_for_select(array(), '', 'include_custom=---Pilih Subtitle Dulu---'), Array('id' => 'sub1', 'class' => 'js-example-basic-single', 'style' => 'width:100%'));
                                    }
                                    ?>

                                </td>
                            </tr>
                            <?php if ($rs_komponen->getKomponenTipe2() == 'KONSTRUKSI' || $rs_komponen->getKomponenTipe2() == 'ATRIBUSI' || $rs_komponen->getKomponenTipe2() == 'PERENCANAAN' || $rs_komponen->getKomponenTipe2() == 'PENGAWASAN'): ?>
                                <tr class="sf_admin_row_0" align='right'>
                                    <td><span style="color:red;">*</span> Kode Akrual</td>
                                    <td align="center">:</td>
                                    <td align="left">
                                        <?php
                                        $akrual_code = '';
                                        if ($sf_params->get('akrual_code')) {
                                            $akrual_code = $sf_params->get('akrual_code');
                                        }

                                        echo select_tag('akrual_code', objects_for_select($rs_akrualcode, 'getAkrualCode', 'getAkrualKodeNama', $akrual_code, 'include_custom=---Pilih Kode Akrual---'), array('class' => 'js-example-basic-single', 'style' => 'width:100%'));
                                        ?>
                                    </td>
                                </tr>
                            <?php elseif ($rs_komponen->getKomponenTipe2() == 'NONKONSTRUKSI'): ?>
                                <tr class="sf_admin_row_0" align='right'>
                                    <td>Kode Akrual</td>
                                    <td align="center">:</td>
                                    <td align="left">
                                        <?php
                                        if ($rs_komponen->getAkrualCode()) {
                                            echo KomponenPeer::buatKodeNamaAkrual($rs_komponen->getAkrualCode());
                                            echo input_hidden_tag('akrual_code', $rs_komponen->getAkrualCode());
                                        } else {
                                            echo input_hidden_tag('akrual_code', '');
                                        }
                                        ?>
                                    </td>
                                </tr>
                            <?php endif; ?>
                                
                            <?php
                            $tipe = $sf_params->get('tipe');

                            $c = new Criteria();
                            $c->add(KomponenPeer::KOMPONEN_ID, $komponen_id);
                            $rs_est_fisik = KomponenPeer::doSelectOne($c);
                            $est_fisik = $rs_est_fisik->getIsEstFisik();
                            if (($rs_komponen->getKomponenTipe2() == 'KONSTRUKSI' || $tipe == 'FISIK' || $est_fisik) || ($sf_params->get('lokasi'))) {
                                ?> 
                                <tr class="sf_admin_row_0" align='right' valign="top">
                                    <td>Lokasi</td>
                                    <td align="center">:</td>
                                    <td align="left">
                                        <div class="row col-xs-12">
                                            <div class="div-induk-lokasi row">
                                                <div class="form-group form-group-options col-xs-3 col-sm-3 col-md-3">
                                                    <div class="input-group input-group-option col-xs-12">
                                                        <input type="text" name="lokasi_jalan[]" class="form-control" placeholder="Nama Jalan (*wajib diisi apabila lokasi berupa Jalan)">
                                                    </div>
                                                </div>
                                                <div class="form-group form-group-options col-xs-1 col-sm-1 col-md-1">
                                                    <div class="input-group input-group-option col-xs-12">
                                                        <select class="form-control" name="tipe_gang[]">
                                                            <option value="GG">---</option>
                                                            <option value="GG">GANG</option>
                                                            <option value="BLOK">BLOK</option>
                                                            <option value="KAV">KAVLING</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group form-group-options col-xs-1 col-sm-1 col-md-1">
                                                    <div class="input-group input-group-option col-xs-12">
                                                        <input type="text" name="lokasi_gang[]" class="form-control" placeholder="Nama Gang">
                                                    </div>
                                                </div>
                                                <div class="form-group form-group-options col-xs-1 col-sm-1 col-md-1">
                                                    <div class="input-group input-group-option col-xs-12">
                                                        <input type="text" name="lokasi_nomor[]" class="form-control" placeholder="Nomor">
                                                    </div>
                                                </div>
                                                <div class="form-group form-group-options col-xs-1 col-sm-1 col-md-1">
                                                    <div class="input-group input-group-option col-xs-12">
                                                        <input type="text" name="lokasi_rw[]" class="form-control" placeholder="RW">
                                                    </div>
                                                </div>
                                                <div class="form-group form-group-options col-xs-1 col-sm-1 col-md-1">
                                                    <div class="input-group input-group-option col-xs-12">
                                                        <input type="text" name="lokasi_rt[]" class="form-control" placeholder="RT">
                                                    </div>
                                                </div>
                                                <div class="form-group form-group-options col-xs-2 col-sm-2 col-md-2">
                                                    <div class="input-group input-group-option col-xs-12">
                                                        <input type="text" name="lokasi_keterangan[]" class="form-control" placeholder="Keterangan Lokasi">
                                                    </div>
                                                </div>
                                                <div class="form-group form-group-options col-xs-2 col-sm-2 col-md-2">
                                                    <div class="input-group input-group-option col-xs-12">
                                                        <input type="text" name="lokasi_tempat[]" class="form-control" placeholder="Nama Bangunan/Saluran (*wajib diisi apabila lokasi berupa bangunan/saluran/tempat)">
                                                        <span class="input-group-addon input-group-addon-remove">
                                                            <span class="glyphicon glyphicon-remove"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row col-xs-12">
                                            <ul>
                                                <li>Isi kolom "Nama Jalan" dengan data nama jalan. Isi tanpa kata "Jl.". Karena akan otomatis ditambahkan oleh aplikasi. Untuk isian "Nama Jalan" tidak dapat dikosongkan</li>
                                                <li>Dropdown untuk memilih "Gang", "BLOK", atau "Kavling", apabila memilih "---" akan default terpilih "Gang"</li>
                                                <li>Isi kolom "Nama Gang/Blok/Kavling" dengan data nama gang atau nama blok atau nama kavling. Isi tanpa kata "Gang", "BLOK", atau "Kavling". Karena akan otomatis ditambahkan oleh aplikasi.Apabila tidak ada, dapat dikosongkan.</li>
                                                <li>Isi kolom "Nomor" dengan data nomor lokasi. Isi tanpa kata "Nomor.". Karena akan otomatis ditambahkan oleh aplikasi.Apabila tidak ada, dapat dikosongkan.</li>
                                                <li>Isi kolom "RW" dengan data RW. Isi tanpa kata "RW". Karena akan otomatis ditambahkan oleh aplikasi.Apabila tidak ada, dapat dikosongkan.</li>
                                                <li>Isi kolom "RT" dengan data RT. Isi tanpa kata "RT". Karena akan otomatis ditambahkan oleh aplikasi.Apabila tidak ada, dapat dikosongkan.</li>
                                                <li>Isi kolom "Keterangan Lokasi" dengan data keterangan pemerjelas lokasi pekerjaan (seperti sebelah barat). Apabila tidak ada, dapat dikosongkan.</li>
                                                <li>Isi kolom "Nama Tempat" dengan data nama bangunan atau tempat. Apabila tidak ada, dapat dikosongkan.</li>
                                                <li>Pemberian lokasi berdasarkan Nama Jalan atau Nama Tempat. Salah satu tidak boleh dikosongkan.</li>
                                            </ul>
                                        </div>
                                        <div class="row col-xs-12">
                                            <?php
//                                        lokasi lama
//                                        echo '<br/>';
//                                        echo textarea_tag('lokasi', $lokasi, 'size=140x3, readonly=true') . submit_tag('cari', 'name=cari') . '<br><font color="magenta">[untuk multi lokasi, tambahkan lokasi dengan klik "cari" lagi]<br></font>';
//                                        lokasi lama         
                                            echo '<br> Usulan dari : ';
                                            echo select_tag('jasmas', objects_for_select($rs_jasmas, 'getKodeJasmas', 'getNama', $sf_params->get('jasmas'), 'include_custom=---Pilih Jasmas Apabila Ada---'));
                                            echo '<br />';
                                            if (!$lokasi == '') {
                                                
                                            }
                                            ?>
                                        </div>
                                    </td>
                                </tr>         
                                <?php
                            } else {
                                ?>
                                <tr class="sf_admin_row_0" align='right' valign="top">
                                    <td>Keterangan</td>
                                    <td align="center">:</td>
                                    <td align='left'><?php echo input_tag('keterangan', $sf_params->get('keterangan')) ?></td>
                                </tr>
                                <?php
                            }
                            ?>
                            <tr class="sf_admin_row_1" align='right' valign="top">
                                <td><span style="color:red;">*</span>Volume</td>
                                <td align="center">:</td>
                                <td align="left">
                                    <?php
                                    $keterangan_koefisien = $sf_params->get('keterangan_koefisien');
                                    $pisah_kali = explode('X', $keterangan_koefisien);
                                    for ($i = 0; $i < 4; $i++) {
                                        $satuan = '';
                                        $volume = '';
                                        $nama_input = 'vol' . ($i + 1);
                                        $nama_pilih = 'volume' . ($i + 1);

                                        if (!empty($pisah_kali[$i])) {
                                            $pisah_spasi = explode(' ', $pisah_kali[$i]);
                                            $j = 0;

                                            for ($s = 0; $s < count($pisah_spasi); $s++) {
                                                if ($pisah_spasi[$s] != NULL) {
                                                    if ($j == 0) {
                                                        $volume = $pisah_spasi[$s];
                                                        $j++;
                                                    } elseif ($j == 1) {
                                                        $satuan = $pisah_spasi[$s];
                                                        $j++;
                                                    } else {
                                                        $satuan.=' ' . $pisah_spasi[$s];
                                                    }
                                                }
                                            }
                                        }
                                        if ($i !== 3) {
                                            echo input_tag($nama_input, $volume, array('onChange' => 'hitungTotal()')) . ' ' . select_tag($nama_pilih, objects_for_select($rs_satuan, 'getSatuanName', 'getSatuanName', $satuan, 'include_custom=---Pilih Satuan--')) . '<br />  X <br />';
                                        } else {
                                            echo input_tag($nama_input, $volume, array('onChange' => 'hitungTotal()')) . ' ' . select_tag($nama_pilih, objects_for_select($rs_satuan, 'getSatuanName', 'getSatuanName', $satuan, 'include_custom=---Pilih Satuan--'));
                                        }
                                    }
                                    ?>
                                </td>
                            </tr>
                            <tr class="sf_admin_row_1" align='right' valign="top">
                                <td>Total</td>
                                <td align="center">:</td>
                                <td align="left"><?php echo input_tag('total', '', array('readonly' => 'true')) ?></td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr class="sf_admin_row_0" align='right' valign="top">
                                <td>&nbsp; </td>
                                <td>
                                    <?php
                                    echo input_hidden_tag('kegiatan', $sf_params->get('kegiatan'));
                                    echo input_hidden_tag('unit', $sf_params->get('unit'));
                                    echo input_hidden_tag('id', $sf_params->get('komponen'));
                                    echo input_hidden_tag('pajak', $sf_params->get('pajak'));
                                    echo input_hidden_tag('tipe', $sf_params->get('tipe'));
                                    echo input_hidden_tag('rekening', $sf_params->get('rekening'));
                                    echo input_hidden_tag('referer', $sf_request->getAttribute('referer'));
                                    ?>
                                </td>
                                <td><?php echo submit_tag('simpan', 'name=simpan') . ' ' . button_to('kembali', '#', array('onClick' => "javascript:history.back()")) ?></li></td>
                            </tr>
                        </tfoot>
                    </table>
                    <?php echo '</form>'; ?>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    $(function () {
        $(document).on('focus', 'div.form-group-options div.input-group-option:last-child input', function () {
            var divIluminati = $(this).parents('.div-induk-lokasi');
            var sDivIluminatiHtml = divIluminati.html();
            var sInputGroupClasses = divIluminati.attr('class');

            //Gambiarra pra nao ficar criando mil inputs
            if (divIluminati.next().length >= 1)
                return;

            divIluminati.parent().append('<div class="' + sInputGroupClasses + '">' + sDivIluminatiHtml + '</div>');
        });

        $(document).on('click', 'div.form-group-options .input-group-addon-remove', function () {
            var divIluminati = $(this).parents('.div-induk-lokasi');
            divIluminati.remove();
        });
    });
    $("#subtitle").change(function () {
        var id = $(this).val();
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/kegiatan/pilihsubx/kegiatan_code/<?php echo $sf_params->get('kegiatan') ?>/unit_id/<?php echo $sf_params->get('unit') ?>/b/" + id + ".html",
            context: document.body
        }).done(function (msg) {
            $('#sub1').html(msg);
        });

    });

    function hitungTotal() {
        var harga = $('#harga').val();
        var pajakx = $('#pajakx').val();
        var vol1 = $('#vol1').val();
        var vol2 = $('#vol2').val();
        var vol3 = $('#vol3').val();
        var vol4 = $('#vol4').val();
        var volume;
        var hitung;


        if (vol1 !== '' || vol2 !== '' || vol3 !== '' || vol4 !== '') {
            if (vol2 === '') {
                vol2 = 1;
                volume = vol1 * vol2;
            } else if (vol2 !== '') {
                volume = vol1 * vol2;
            }
            if (vol3 === '') {
                vol3 = 1;
                volume = volume * vol3;
            } else if (vol3 !== '') {
                volume = vol1 * vol2 * vol3;
            }
            if (vol4 === '') {
                vol4 = 1;
                volume = volume * vol4;
            } else if (vol4 !== '') {
                volume = vol1 * vol2 * vol3 * vol4;
            }
        }

        if (pajakx == 10) {
            hitung = harga * volume * 1.1;
        } else if (pajakx == 0) {
            hitung = harga * volume * 1;
        }

        $('#total').val(hitung);

    }
</script>