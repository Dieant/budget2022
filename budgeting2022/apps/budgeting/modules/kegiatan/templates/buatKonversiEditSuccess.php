<?php use_helper('I18N', 'Form', 'Object', 'Javascript', 'Validation') ?>
<?php include_partial('admin/menufitur') ?>
<section class="content">
    <?php include_partial('kegiatan/list_messages'); ?>
    <div class="box box-primary box-solid">
        <div class="box-header with-border">
            <h3 class="box-title">Form Request Admin</h3>
        </div>
        <div class="box-body">
            <?php echo form_tag('kegiatan/buatKonversi', array('multipart' => true, 'class' => 'form-horizontal')); ?>
            
            <div class="form-group">
                <label class="col-xs-3 control-label">SKPD Awal :</label>
                <div class="col-xs-9">
                    <?php
                    echo select_tag('unit_id_lama', objects_for_select($unit_kerja, 'getUnitId', 'getUnitName', $unit_kerja, 'include_custom=---Pilih SKPD---'), array('id'=>'unit_id', 'class' => 'form-control js-example-basic-single', 'style' => 'width:100%'));
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-xs-3 control-label">Kegiatan Awal :</label>
                <div class="col-xs-9">
                    <?php echo select_tag('kode_kegiatan_lama', options_for_select(array(), '', null), array('id' => 'keg', 'class' => 'form-control js-example-basic-single', 'style' => 'width:100%')); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-xs-3 control-label">Subtitle Awal :</label>
                <div class="col-xs-9">
                    <?php echo select_tag('subtitle_lama', options_for_select(array(), '', null), array('id' => 'sub', 'class' => 'js-example-basic-multiple', 'style' => 'width:100%', 'multiple' => 'multiple')); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-xs-3 control-label">SKPD Menjadi :</label>
                <div class="col-xs-9">
                    <?php
                    echo select_tag('unit_id_baru', objects_for_select($unit_kerja, 'getUnitId', 'getUnitName', $unit_kerja, 'include_custom=---Pilih SKPD---'), array('id'=>'unit_id2','class' => 'form-control js-example-basic-single', 'style' => 'width:100%'));
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-xs-3 control-label">Kegiatan Menjadi :</label>
                <div class="col-xs-9">
                    <?php echo select_tag('kode_kegiatan_baru', options_for_select(array(), '', null), array('id' => 'keg2', 'class' => 'form-control js-example-basic-single', 'style' => 'width:100%')); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-xs-3 control-label">Subtitle Menjadi :</label>
                <div class="col-xs-9">
                    <?php echo select_tag('subtitle_baru', options_for_select(array(), '', null), array('id' => 'sub2', 'class' => 'form-control js-example-basic-single', 'style' => 'width:100%')); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-xs-3 control-label">&nbsp;</label>
                <div class="col-xs-9">
                    <input type="submit" name="submit" value="Submit" class="btn btn-primary btn-flat"/>
                </div>
            </div>
            <?php echo '</form>' ?>
        </div>
    </div>
</section>
<script>
    $(document).ready(function () {
        $(".js-example-basic-multiple").select2();
    });
    
    $("#unit_id").change(function () {
        var id = $(this).val();
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/kegiatan/pilihKegiatan/id/" + id + ".html",
            context: document.body
        }).done(function (msg) {
            $('#keg').html(msg);
        });

    });

    $("#keg").change(function () {
        var id2 = $(this).val();
        var id = $('#unit_id').val();
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/kegiatan/pilihSubtitle/unit_id/" + id + "/kegiatan_code/" + id2 + ".html",
            context: document.body
        }).done(function (msg) {
            $('#sub').html(msg);
        });

    });

    $("#unit_id2").change(function () {
        var id = $(this).val();
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/kegiatan/pilihKegiatan2/id/" + id + ".html",
            context: document.body
        }).done(function (msg) {
            $('#keg2').html(msg);
        });

    });

    $("#keg2").change(function () {
        var id2 = $(this).val();
        var id = $('#unit_id2').val();
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/kegiatan/pilihSubtitle2/unit_id/" + id + "/kegiatan_code/" + id2 + ".html",
            context: document.body
        }).done(function (msg) {
            $('#sub2').html(msg);
        });

    });
</script>