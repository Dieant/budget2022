<?php

if (isset($filters['tahap']) && $filters['tahap'] != '') {
    echo '';
} else {
    $con = Propel::getConnection();
    $query = "select max(status_level) as max "
            . " from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail "
            . " where status_hapus=false and unit_id='" . $master_kegiatan->getUnitId() . "' and kegiatan_code='" . $master_kegiatan->getKodeKegiatan() . "'";
    $stmt = $con->prepareStatement($query);
    $rs_max = $stmt->executeQuery();
    if ($rs_max->next()) {
        $max = $rs_max->getInt('max');
        if ($max == 0 || $max == null) {
            // echo "sini";die();
            $c_kegiatan = new Criteria();
            $c_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $master_kegiatan->getUnitId());
            $c_kegiatan->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $master_kegiatan->getKodeKegiatan());
            $kegiatan = DinasMasterKegiatanPeer::doSelectOne($c_kegiatan);
            if ($kegiatan->getStatusLevel() > $max || $max == null) {
                $max = $kegiatan->getStatusLevel();
            }
        }
        // echo "sini";die();
        if ($max == 0) {
            $status = 'ENTRI';
        } else if ($max == 1) {
            $status = 'PPTK';
        } else if ($max == 2) {
            $status = 'KPA';
        } else if ($max == 3) {
            $status = 'PA I';
        } else if ($max == 4) {
            // echo "4";die();
            $tapd = false;
            $bappeko = false;
            $penyelia = false;
            $bagian_hukum = false;
            $inspektorat = false;
            $badan_kepegawaian = false;
            $lppa = false;
            $bagian_organisasi = false;
            $c = new Criteria();
            $c->add(DinasRincianDetailPeer::UNIT_ID, $master_kegiatan->getUnitId());
            $c->add(DinasRincianDetailPeer::KEGIATAN_CODE, $master_kegiatan->getKodeKegiatan());
            $c->add(DinasRincianDetailPeer::STATUS_HAPUS, FALSE);
            $c->add(DinasRincianDetailPeer::STATUS_LEVEL, 4);
            $c->add(DinasRincianDetailPeer::IS_TAPD_SETUJU, FALSE);
            $c->add(DinasRincianDetailPeer::VOLUME, 0, Criteria::GREATER_THAN);
            if (DinasRincianDetailPeer::doSelectOne($c)) {
                $tapd = TRUE;
            }
            $c = new Criteria();
            $c->add(DinasRincianDetailPeer::UNIT_ID, $master_kegiatan->getUnitId());
            $c->add(DinasRincianDetailPeer::KEGIATAN_CODE, $master_kegiatan->getKodeKegiatan());
            $c->add(DinasRincianDetailPeer::STATUS_HAPUS, FALSE);
            $c->add(DinasRincianDetailPeer::STATUS_LEVEL, 4);
            $c->add(DinasRincianDetailPeer::IS_BAPPEKO_SETUJU, FALSE);
            $c->add(DinasRincianDetailPeer::VOLUME, 0, Criteria::GREATER_THAN);
            if (DinasRincianDetailPeer::doSelectOne($c)) {
                $bappeko = TRUE;
            }
            $c = new Criteria();
            $c->add(DinasRincianDetailPeer::UNIT_ID, $master_kegiatan->getUnitId());
            $c->add(DinasRincianDetailPeer::KEGIATAN_CODE, $master_kegiatan->getKodeKegiatan());
            $c->add(DinasRincianDetailPeer::STATUS_HAPUS, FALSE);
            $c->add(DinasRincianDetailPeer::STATUS_LEVEL, 4);
            $c->add(DinasRincianDetailPeer::IS_PENYELIA_SETUJU, FALSE);
            $c->add(DinasRincianDetailPeer::VOLUME, 0, Criteria::GREATER_THAN);
            if (DinasRincianDetailPeer::doSelectOne($c)) {
                $penyelia = TRUE;
            }
            $c = new Criteria();
            $c->add(DinasRincianDetailPeer::UNIT_ID, $master_kegiatan->getUnitId());
            $c->add(DinasRincianDetailPeer::KEGIATAN_CODE, $master_kegiatan->getKodeKegiatan());
            $c->add(DinasRincianDetailPeer::STATUS_HAPUS, FALSE);
            $c->add(DinasRincianDetailPeer::STATUS_LEVEL, 4);
            $c->add(DinasRincianDetailPeer::IS_BAGIAN_HUKUM_SETUJU, FALSE);
            $c->add(DinasRincianDetailPeer::VOLUME, 0, Criteria::GREATER_THAN);
            if (DinasRincianDetailPeer::doSelectOne($c)) {
                $bagian_hukum = TRUE;
            }
            $c = new Criteria();
            $c->add(DinasRincianDetailPeer::UNIT_ID, $master_kegiatan->getUnitId());
            $c->add(DinasRincianDetailPeer::KEGIATAN_CODE, $master_kegiatan->getKodeKegiatan());
            $c->add(DinasRincianDetailPeer::STATUS_HAPUS, FALSE);
            $c->add(DinasRincianDetailPeer::STATUS_LEVEL, 4);
            $c->add(DinasRincianDetailPeer::IS_INSPEKTORAT_SETUJU, FALSE);
            $c->add(DinasRincianDetailPeer::VOLUME, 0, Criteria::GREATER_THAN);
            if (DinasRincianDetailPeer::doSelectOne($c)) {
                $inspektorat = TRUE;
            }
            $c = new Criteria();
            $c->add(DinasRincianDetailPeer::UNIT_ID, $master_kegiatan->getUnitId());
            $c->add(DinasRincianDetailPeer::KEGIATAN_CODE, $master_kegiatan->getKodeKegiatan());
            $c->add(DinasRincianDetailPeer::STATUS_HAPUS, FALSE);
            $c->add(DinasRincianDetailPeer::STATUS_LEVEL, 4);
            $c->add(DinasRincianDetailPeer::IS_BADAN_KEPEGAWAIAN_SETUJU, FALSE);
            $c->add(DinasRincianDetailPeer::VOLUME, 0, Criteria::GREATER_THAN);
            if (DinasRincianDetailPeer::doSelectOne($c)) {
                $badan_kepegawaian = TRUE;
            }
            $c = new Criteria();
            $c->add(DinasRincianDetailPeer::UNIT_ID, $master_kegiatan->getUnitId());
            $c->add(DinasRincianDetailPeer::KEGIATAN_CODE, $master_kegiatan->getKodeKegiatan());
            $c->add(DinasRincianDetailPeer::STATUS_HAPUS, FALSE);
            $c->add(DinasRincianDetailPeer::STATUS_LEVEL, 4);
            $c->add(DinasRincianDetailPeer::IS_LPPA_SETUJU, FALSE);
            $c->add(DinasRincianDetailPeer::VOLUME, 0, Criteria::GREATER_THAN);
            if (DinasRincianDetailPeer::doSelectOne($c)) {
                $lppa = TRUE;
            }
            $c = new Criteria();
            $c->add(DinasRincianDetailPeer::UNIT_ID, $master_kegiatan->getUnitId());
            $c->add(DinasRincianDetailPeer::KEGIATAN_CODE, $master_kegiatan->getKodeKegiatan());
            $c->add(DinasRincianDetailPeer::STATUS_HAPUS, FALSE);
            $c->add(DinasRincianDetailPeer::STATUS_LEVEL, 4);
            $c->add(DinasRincianDetailPeer::IS_BAGIAN_ORGANISASI_SETUJU, FALSE);
            $c->add(DinasRincianDetailPeer::VOLUME, 0, Criteria::GREATER_THAN);
            if (DinasRincianDetailPeer::doSelectOne($c)) {
                $bagian_organisasi = TRUE;
            }

            $c = new Criteria();
            $c->add(DinasMasterKegiatanPeer::UNIT_ID, $master_kegiatan->getUnitId());
            $c->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $master_kegiatan->getKodeKegiatan());
            $c->add(DinasMasterKegiatanPeer::STATUS_LEVEL, 4);
            $c->add(DinasMasterKegiatanPeer::IS_TAPD_SETUJU, FALSE);
            if (DinasMasterKegiatanPeer::doSelectOne($c)) {
                $tapd = TRUE;
            }
            $c = new Criteria();
            $c->add(DinasMasterKegiatanPeer::UNIT_ID, $master_kegiatan->getUnitId());
            $c->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $master_kegiatan->getKodeKegiatan());
            $c->add(DinasMasterKegiatanPeer::STATUS_LEVEL, 4);
            $c->add(DinasMasterKegiatanPeer::IS_BAPPEKO_SETUJU, FALSE);
            if (DinasMasterKegiatanPeer::doSelectOne($c)) {
                $bappeko = TRUE;
            }
            $c = new Criteria();
            $c->add(DinasMasterKegiatanPeer::UNIT_ID, $master_kegiatan->getUnitId());
            $c->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $master_kegiatan->getKodeKegiatan());
            $c->add(DinasMasterKegiatanPeer::STATUS_LEVEL, 4);
            $c->add(DinasMasterKegiatanPeer::IS_PENYELIA_SETUJU, FALSE);
            if (DinasMasterKegiatanPeer::doSelectOne($c)) {
                $penyelia = TRUE;
            }
            $c = new Criteria();
            $c->add(DinasMasterKegiatanPeer::UNIT_ID, $master_kegiatan->getUnitId());
            $c->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $master_kegiatan->getKodeKegiatan());
            $c->add(DinasMasterKegiatanPeer::STATUS_LEVEL, 4);
            $c->add(DinasMasterKegiatanPeer::IS_BAGIAN_HUKUM_SETUJU, FALSE);
            if (DinasMasterKegiatanPeer::doSelectOne($c)) {
                $bagian_hukum = TRUE;
            }
            $c = new Criteria();
            $c->add(DinasMasterKegiatanPeer::UNIT_ID, $master_kegiatan->getUnitId());
            $c->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $master_kegiatan->getKodeKegiatan());
            $c->add(DinasMasterKegiatanPeer::STATUS_LEVEL, 4);
            $c->add(DinasMasterKegiatanPeer::IS_INSPEKTORAT_SETUJU, FALSE);
            if (DinasMasterKegiatanPeer::doSelectOne($c)) {
                $inspektorat = TRUE;
            }
            $c = new Criteria();
            $c->add(DinasMasterKegiatanPeer::UNIT_ID, $master_kegiatan->getUnitId());
            $c->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $master_kegiatan->getKodeKegiatan());
            $c->add(DinasMasterKegiatanPeer::STATUS_LEVEL, 4);
            $c->add(DinasMasterKegiatanPeer::IS_BADAN_KEPEGAWAIAN_SETUJU, FALSE);
            if (DinasMasterKegiatanPeer::doSelectOne($c)) {
                $badan_kepegawaian = TRUE;
            }
            $c = new Criteria();
            $c->add(DinasMasterKegiatanPeer::UNIT_ID, $master_kegiatan->getUnitId());
            $c->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $master_kegiatan->getKodeKegiatan());
            $c->add(DinasMasterKegiatanPeer::STATUS_LEVEL, 4);
            $c->add(DinasMasterKegiatanPeer::IS_LPPA_SETUJU, FALSE);
            if (DinasMasterKegiatanPeer::doSelectOne($c)) {
                $lppa = TRUE;
            }
            $c = new Criteria();
            $c->add(DinasMasterKegiatanPeer::UNIT_ID, $master_kegiatan->getUnitId());
            $c->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $master_kegiatan->getKodeKegiatan());
            $c->add(DinasMasterKegiatanPeer::STATUS_LEVEL, 4);
            $c->add(DinasMasterKegiatanPeer::IS_BAGIAN_ORGANISASI_SETUJU, FALSE);
            if (DinasMasterKegiatanPeer::doSelectOne($c)) {
                $bagian_organisasi = TRUE;
            }

            $penyetuju = array();
            if(!$tapd)
                array_push($penyetuju, 'BPKAD');
            if(!$bappeko)
                array_push($penyetuju, 'Bappeda');
            if(!$penyelia)
                array_push($penyetuju, 'BPBJAP');
            if(!$bagian_hukum)
                array_push($penyetuju, 'Bag. Hukum');
            if(!$inspektorat)
                array_push($penyetuju, 'Inspektorat');
            if(!$badan_kepegawaian)
                array_push($penyetuju, 'BKPSDM');
            if(!$lppa)
                // array_push($penyetuju, 'Bag. LPPA');
                array_push($penyetuju, 'Bapenda');
            if(!$bagian_organisasi)
                array_push($penyetuju, 'Bag. Organisasi');

            $strPenyetuju = implode(', ', $penyetuju);
            $status = 'Tim Anggaran ('.$strPenyetuju.')';
            
            // if ($tapd && $bappeko && $penyelia)
            //     $status = 'TIM ANGGARAN';
            // elseif ($tapd && $bappeko)
            //     $status = 'TIM ANGGARAN (Penyelia)';
            // elseif ($tapd && $penyelia)
            //     $status = 'TIM ANGGARAN (Bappeko)';
            // elseif ($penyelia && $bappeko)
            //     $status = 'TIM ANGGARAN (BPKPD)';
            // elseif ($tapd)
            //     $status = 'TIM ANGGARAN (Bappeko & Penyelia)';
            // elseif ($bappeko)
            //     $status = 'TIM ANGGARAN (BPKPD & Penyelia)';
            // elseif ($penyelia)
            //     $status = 'TIM ANGGARAN (Bappeko & BPKPD)';
            // else
            //     $status = 'TIM ANGGARAN (Bappeko, BPKPD, & Penyelia)';
        } else if ($max == 5) {
            $status = 'PA II';
        } else if ($max == 6) {
            $status = 'Penyelia II';
        } else if ($max == 7) {
            $status = 'RKA';
        }
    } else {
        $status = '';
    }
    echo $status;
    // echo "aaaa";
//cek penyesuaian
    $sub = "(detail_no in (select detail_no from " . sfConfig::get('app_default_schema') . ".rincian_detail where unit_id='" . $master_kegiatan->getUnitId() . "' and kegiatan_code='" . $master_kegiatan->getKodeKegiatan() . "' and detail_no=dinas_rincian_detail.detail_no and (nilai_anggaran<>dinas_rincian_detail.nilai_anggaran or rekening_code<>dinas_rincian_detail.rekening_code or detail_name<>dinas_rincian_detail.detail_name or keterangan_koefisien<>dinas_rincian_detail.keterangan_koefisien))
        or detail_no not in (select detail_no from " . sfConfig::get('app_default_schema') . ".rincian_detail where unit_id='" . $master_kegiatan->getUnitId() . "' and kegiatan_code='" . $master_kegiatan->getKodeKegiatan() . "'))";
    $c = new Criteria();
    $c->add(DinasRincianDetailPeer::UNIT_ID, $master_kegiatan->getUnitId());
    $c->add(DinasRincianDetailPeer::KEGIATAN_CODE, $master_kegiatan->getKodeKegiatan());
    $c->add(DinasRincianDetailPeer::STATUS_HAPUS, FALSE);
    $c->add(DinasRincianDetailPeer::STATUS_LEVEL, $max, Criteria::LESS_THAN);
    //$c->add(DinasRincianDetailPeer::STATUS_LEVEL_TOLAK, $max);
    $c->add(DinasRincianDetailPeer::DETAIL_NO, $sub, Criteria::CUSTOM);
    $c->add(DinasRincianDetailPeer::NILAI_ANGGARAN, 0, Criteria::GREATER_THAN);
    if (DinasRincianDetailPeer::doCount($c) > 0) {
        echo '<br>(Terdapat Penyesuaian)';
    }
}