<?php use_helper('I18N', 'Date', 'Url', 'Javascript', 'Form', 'Object', 'Number', 'Validation') ?>
<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Penanda Sumber Dana</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Lembar Kerja</a></li>
          <li class="breadcrumb-item active">Sumber Dana</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>
<!-- Main content -->
<section class="content">
    <?php include_partial('kegiatan/list_messages'); ?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 stretch-card">
                <div class="card">
                <?php echo form_tag('kegiatan/prosesUbahPenandaBtl'); ?>                 
                    <div class="card-body table-responsive p-0">
                        <table class="table table-hover">
                            <thead class="head_peach">
                                <tr>
                                    <th><b>Detail Kegiatan</b></th>
                                    <th><b>Nama Komponen</b></th>
                                    <th><b>Satuan</b></th>
                                    <th><b>Keterangan Koefisien</b></th>
                                    <th><b>Rekening</b></th>
                                    <th><b>Harga</b></th>
                                    <th><b>Total</b></th>
                                    <th><b>Sumber Dana</b></th>    
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $counter = 0;
                                while($rs_rinciandetail->next()) {
                                    $c = new Criteria();
                                    $c->addAscendingOrderByColumn(MasterSumberDanaPeer::SUMBER_DANA);
                                    $v = MasterSumberDanaPeer::doSelect($c);    
                                    $counter++;
                                    $nama = $rs_rinciandetail->getString('komponen_name');
                                    $style = '';
                                    echo "<tr $style>";
                                    echo "<td>".$rs_rinciandetail->getString('detail_kegiatan')."</td>";
                                    echo "<td>".$rs_rinciandetail->getString('komponen_name')." ".$rs_rinciandetail->getString('detail_name');
                                    echo " <span class='badge badge-info'>".$rs_rinciandetail->getString('sumber_dana')."</span></td>";
                                    echo "<td>".$rs_rinciandetail->getString('satuan')."</td>";
                                    echo "<td>".$rs_rinciandetail->getString('keterangan_koefisien')."</td>";
                                    echo "<td>".$rs_rinciandetail->getString('rekening_code')."</td>";
                                    echo "<td align='right'>".number_format($rs_rinciandetail->getString('komponen_harga'), 0, ',', '.')."</td>";
                                    echo "<td align='right'>".number_format($rs_rinciandetail->getString('nilai_anggaran'), 0, ',', '.')."</td>";
                                    echo "<td>";
                                        echo select_tag('sumber_dana[]', objects_for_select($v, 'getId', 'getSumberDana', '', 'include_custom=--Pilih Sumber Dana--'), array('class' => 'form-control', 'style' => 'width:100%'));
                                    echo "</td>";
                                    echo input_hidden_tag('detail_no[]', $rs_rinciandetail->getString('detail_no'));
                                    echo input_hidden_tag('unit_id', $unit_id);
                                    echo input_hidden_tag('kode_kegiatan', $kode_kegiatan);
                                    echo "</tr>";
                                }
                                ?>
                                <?php if($counter <= 0): ?>
                                    <tr><td colspan="16" align="center">Tidak ada komponen yang bisa ditampilkan</td></tr>
                                <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer">
                        <?php
                        if($counter > 0)
                            echo submit_tag('Simpan', array('name' => 'proses', 'class' => 'btn btn-outline-primary btn-sm')) . '&nbsp;';
                        ?>  
                    </div>
                </div>
                <?php echo '</form>'; ?>
            </div>
        </div>
    </div>
</section>
<script>
    $("#cekSemua").change(function () {
        $(".cek").prop('checked', $(this).prop("checked"));
    });
</script>