<th class="align-middle">
    <?php echo __('ID') ?>
</th>
<th class="align-middle">
    <?php echo __('Nama Sub Kegiatan') ?>
</th>
<!-- <th id="sf_admin_list_th_alokasidana">
    <?php echo __('Buku Putih') ?>
</th> -->
<th class="align-middle">
    <?php
    if (sfConfig::get('app_tahap_edit') == 'murni') {
        echo __('Pagu');
    } else if (sfConfig::get('app_tahap_edit') != 'murni') {
        echo __('Semula');
    }
    ?>
</th>
<th class="align-middle">
    <?php echo __('Rincian') ?>
</th>
<th class="align-middle">
    <?php echo __('Selisih') ?>
</th>
<th class="align-middle">
    <?php echo __('Pagu Tambahan') ?>
</th>
<th class="align-middle">
    <?php echo __('Posisi') ?>
</th>
<th class="align-middle">
    <?php echo __('Tahap') ?>
</th>          
