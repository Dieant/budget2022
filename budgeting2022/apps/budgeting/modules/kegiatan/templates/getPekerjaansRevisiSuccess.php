<?php use_helper('I18N', 'Date', 'Url', 'Javascript', 'Form', 'Object', 'Validation'); ?>
<?php
if ($tahap == 'pakbp') {
    $tabel_dpn = 'pak_bukuputih_';
    $status = 'CLOSE';
} elseif ($tahap == 'pakbb') {
    $tabel_dpn = 'pak_bukubiru_';
    $status = 'CLOSE';
} elseif ($tahap == 'murni') {
    $tabel_dpn = 'murni_';
    $status = 'CLOSE';
} elseif ($tahap == 'murnibp') {
    $tabel_dpn = 'murni_bukuputih_';
    $status = 'CLOSE';
} elseif ($tahap == 'murnibb') {
    $tabel_dpn = 'murni_bukubiru_';
    $status = 'CLOSE';
    
} elseif ($tahap == 'revisi1') {
    $tabel_dpn = 'revisi1_';
    $status = 'CLOSE';
} elseif ($tahap == 'revisi1_1') {
    $tabel_dpn = 'revisi1_1_';
    $status = 'CLOSE';
} elseif ($tahap == 'revisi2') {
    $tabel_dpn = 'revisi2_';
    $status = 'CLOSE';
} elseif ($tahap == 'revisi2_1') {
    $tabel_dpn = 'revisi2_1_';
    $status = 'CLOSE';
} elseif ($tahap == 'revisi2_2') {
    $tabel_dpn = 'revisi2_2_';
    $status = 'CLOSE';
} elseif ($tahap == 'revisi3') {
    $tabel_dpn = 'revisi3_';
    $status = 'CLOSE';
} elseif ($tahap == 'revisi3_1') {
    $tabel_dpn = 'revisi3_1_';
    $status = 'CLOSE';
} elseif ($tahap == 'revisi4') {
    $tabel_dpn = 'revisi4_';
    $status = 'CLOSE';
} elseif ($tahap == 'revisi5') {
    $tabel_dpn = 'revisi5_';
    $status = 'CLOSE';
} elseif ($tahap == 'revisi6') {
    $tabel_dpn = 'revisi6_';
    $status = 'CLOSE';
} elseif ($tahap == 'revisi7') {
    $tabel_dpn = 'revisi7_';
    $status = 'CLOSE';
} elseif ($tahap == 'revisi8') {
    $tabel_dpn = 'revisi8_';
    $status = 'CLOSE';
} elseif ($tahap == 'revisi9') {
    $tabel_dpn = 'revisi9_';
    $status = 'CLOSE';
} elseif ($tahap == 'revisi10') {
    $tabel_dpn = 'revisi10_';
    $status = 'CLOSE';
} elseif ($tahap == 'rkua') {
    $tabel_dpn = 'rkua_';
    $status = 'CLOSE';
} else {
    $tabel_dpn = 'dinas_';
    $status = $sf_user->getAttribute('status', '', 'status_admin');
}
$i = 0;
$kode_sub = '';
$temp_rekening = '';
?>

<?php
foreach ($rs_rd as $rd):
    $est_fisik = FALSE;
    $c = new Criteria();
    $c->add(KomponenPeer::KOMPONEN_ID, $rd->getKomponenId());
    $c->add(KomponenPeer::IS_EST_FISIK, TRUE);
    if ($rs_est_fisik = KomponenPeer::doSelectOne($c))
        $est_fisik = TRUE;

    $odd = fmod($i++, 2);
    $unit_id = $rd->getUnitId();
    $kegiatan_code = $rd->getKegiatanCode();


    if ($kode_sub != $rd->getKodeSub()) {
        $kode_sub = $rd->getKodeSub();
        $sub = $rd->getSub();
        $cekKodeSub = substr($kode_sub, 0, 4);

        if ($cekKodeSub == 'RKAM') {//RKA Member
            if ($tahap == 'pakbp') {
                $C_RKA = new Criteria();
                $C_RKA->add(PakBukuPutihRkaMemberPeer::KODE_SUB, $kode_sub);
                $rs_rkam = PakBukuPutihRkaMemberPeer::doSelectOne($C_RKA);
            } elseif ($tahap == 'pakbb') {
                $C_RKA = new Criteria();
                $C_RKA->add(PakBukuBiruRkaMemberPeer::KODE_SUB, $kode_sub);
                $rs_rkam = PakBukuBiruRkaMemberPeer::doSelectOne($C_RKA);
            } elseif ($tahap == 'murni') {
                $C_RKA = new Criteria();
                $C_RKA->add(MurniRkaMemberPeer::KODE_SUB, $kode_sub);
                $rs_rkam = MurniRkaMemberPeer::doSelectOne($C_RKA);
            } elseif ($tahap == 'murnibp') {
                $C_RKA = new Criteria();
                $C_RKA->add(MurniBukuPutihRkaMemberPeer::KODE_SUB, $kode_sub);
                $rs_rkam = MurniBukuPutihRkaMemberPeer::doSelectOne($C_RKA);
            } elseif ($tahap == 'murnibb') {
                $C_RKA = new Criteria();
                $C_RKA->add(MurniBukuBiruRkaMemberPeer::KODE_SUB, $kode_sub);
                $rs_rkam = MurniBukuBiruRkaMemberPeer::doSelectOne($C_RKA);
            } elseif ($tahap == 'revisi1') {
                $C_RKA = new Criteria();
                $C_RKA->add(Revisi1RkaMemberPeer::KODE_SUB, $kode_sub);
                $rs_rkam = Revisi1RkaMemberPeer::doSelectOne($C_RKA);
            } elseif ($tahap == 'revisi1_1') {
                $C_RKA = new Criteria();
                $C_RKA->add(Revisi1bRkaMemberPeer::KODE_SUB, $kode_sub);
                $rs_rkam = Revisi1bRkaMemberPeer::doSelectOne($C_RKA);
            } elseif ($tahap == 'revisi2') {
                $C_RKA = new Criteria();
                $C_RKA->add(Revisi2RkaMemberPeer::KODE_SUB, $kode_sub);
                $rs_rkam = Revisi2RkaMemberPeer::doSelectOne($C_RKA);
            } elseif ($tahap == 'revisi2_1') {
                $C_RKA = new Criteria();
                $C_RKA->add(Revisi21RkaMemberPeer::KODE_SUB, $kode_sub);
                $rs_rkam = Revisi21RkaMemberPeer::doSelectOne($C_RKA);
            }  elseif ($tahap == 'revisi2_2') {
                $C_RKA = new Criteria();
                $C_RKA->add(Revisi22RkaMemberPeer::KODE_SUB, $kode_sub);
                $rs_rkam = Revisi22RkaMemberPeer::doSelectOne($C_RKA);
            } elseif ($tahap == 'revisi3') {
                $C_RKA = new Criteria();
                $C_RKA->add(Revisi3RkaMemberPeer::KODE_SUB, $kode_sub);
                $rs_rkam = Revisi3RkaMemberPeer::doSelectOne($C_RKA);
            } elseif ($tahap == 'revisi3_1') {
                $C_RKA = new Criteria();
                $C_RKA->add(Revisi31RkaMemberPeer::KODE_SUB, $kode_sub);
                $rs_rkam = Revisi31RkaMemberPeer::doSelectOne($C_RKA);
            }  elseif ($tahap == 'revisi4') {
                $C_RKA = new Criteria();
                $C_RKA->add(Revisi4RkaMemberPeer::KODE_SUB, $kode_sub);
                $rs_rkam = Revisi4RkaMemberPeer::doSelectOne($C_RKA);
            } elseif ($tahap == 'revisi5') {
                $C_RKA = new Criteria();
                $C_RKA->add(Revisi5RkaMemberPeer::KODE_SUB, $kode_sub);
                $rs_rkam = Revisi5RkaMemberPeer::doSelectOne($C_RKA);
            } elseif ($tahap == 'revisi6') {
                $C_RKA = new Criteria();
                $C_RKA->add(Revisi6RkaMemberPeer::KODE_SUB, $kode_sub);
                $rs_rkam = Revisi6RkaMemberPeer::doSelectOne($C_RKA);
            } elseif ($tahap == 'revisi7') {
                $C_RKA = new Criteria();
                $C_RKA->add(Revisi7RkaMemberPeer::KODE_SUB, $kode_sub);
                $rs_rkam = Revisi7RkaMemberPeer::doSelectOne($C_RKA);
            } elseif ($tahap == 'revisi8') {
                $C_RKA = new Criteria();
                $C_RKA->add(Revisi8RkaMemberPeer::KODE_SUB, $kode_sub);
                $rs_rkam = Revisi8RkaMemberPeer::doSelectOne($C_RKA);
            } elseif ($tahap == 'revisi9') {
                $C_RKA = new Criteria();
                $C_RKA->add(Revisi9RkaMemberPeer::KODE_SUB, $kode_sub);
                $rs_rkam = Revisi9RkaMemberPeer::doSelectOne($C_RKA);
            } elseif ($tahap == 'revisi10') {
                $C_RKA = new Criteria();
                $C_RKA->add(Revisi10RkaMemberPeer::KODE_SUB, $kode_sub);
                $rs_rkam = Revisi10RkaMemberPeer::doSelectOne($C_RKA);
            } elseif ($tahap == 'rkua') {
                $C_RKA = new Criteria();
                $C_RKA->add(RkuaRkaMemberPeer::KODE_SUB, $kode_sub);
                $rs_rkam = RkuaRkaMemberPeer::doSelectOne($C_RKA);
            } else {
                $C_RKA = new Criteria();
                $C_RKA->add(DinasRkaMemberPeer::KODE_SUB, $kode_sub);
                $rs_rkam = DinasRkaMemberPeer::doSelectOne($C_RKA);
            }
            //print_r($rs_rkam);exit;
            if ($rs_rkam) {
                ?>
                <tr class="pekerjaans_<?php echo $id ?>" bgcolor="#AFC4EF">
                    <td colspan="11"><?php
                    if (($status == 'OPEN') and ( $rd->getLockSubtitle() <> 'LOCK')) {
                        echo link_to_function(image_tag('/sf/sf_admin/images/edit.png'), 'editHeaderKegiatan(' . $id . ',"' . $kegiatan_code . '","' . $unit_id . '","' . $kode_sub . '")');
                        echo link_to_function(image_tag('/sf/sf_admin/images/cancel.png'), 'hapusHeaderKegiatan(' . $id . ',"' . $kegiatan_code . '","' . $unit_id . '","' . $kode_sub . '")');
                    }
                    ?>
                    <div id="<?php echo 'header_' . $rs_rkam->getKodeSub() ?>"><b> .:. <?php echo $rs_rkam->getKomponenName() . ' ' . $rs_rkam->getDetailName(); ?></b></div>
                    <td align="right">
                        <?php
                        echo number_format($rd->getTotalKodeSub($kode_sub), 0, ',', '.');
                        ?></td>
                        <td>&nbsp;</td>
                    </tr>
                    <?php
                } else {

                }
            } else {

                $c = new Criteria();
                if ($tahap == 'pakbp') {
                    $c->add(PakBukuPutihRincianSubParameterPeer::KODE_SUB, $kode_sub);
                    $rs_subparameter = PakBukuPutihRincianSubParameterPeer::doSelectOne($c);
                } elseif ($tahap == 'pakbb') {
                    $c->add(PakBukuBiruRincianSubParameterPeer::KODE_SUB, $kode_sub);
                    $rs_subparameter = PakBukuBiruRincianSubParameterPeer::doSelectOne($c);
                } elseif ($tahap == 'murni') {
                    $c->add(MurniRincianSubParameterPeer::KODE_SUB, $kode_sub);
                    $rs_subparameter = MurniRincianSubParameterPeer::doSelectOne($c);
                } elseif ($tahap == 'murnibp') {
                    $c->add(MurniBukuPutihRincianSubParameterPeer::KODE_SUB, $kode_sub);
                    $rs_subparameter = MurniBukuPutihRincianSubParameterPeer::doSelectOne($c);
                } elseif ($tahap == 'murnibb') {
                    $c->add(MurniBukuBiruRincianSubParameterPeer::KODE_SUB, $kode_sub);
                    $rs_subparameter = MurniBukuBiruRincianSubParameterPeer::doSelectOne($c);
                } elseif ($tahap == 'revisi1') {
                    $c->add(Revisi1RincianSubParameterPeer::KODE_SUB, $kode_sub);
                    $rs_subparameter = Revisi1RincianSubParameterPeer::doSelectOne($c);
                } elseif ($tahap == 'revisi1_1') {
                    $c->add(Revisi1bRincianSubParameterPeer::KODE_SUB, $kode_sub);
                    $rs_subparameter = Revisi1bRincianSubParameterPeer::doSelectOne($c);
                } elseif ($tahap == 'revisi2') {
                    $c->add(Revisi2RincianSubParameterPeer::KODE_SUB, $kode_sub);
                    $rs_subparameter = Revisi2RincianSubParameterPeer::doSelectOne($c);
                } elseif ($tahap == 'revisi2_1') {
                    $c->add(Revisi21RincianSubParameterPeer::KODE_SUB, $kode_sub);
                    $rs_subparameter = Revisi21RincianSubParameterPeer::doSelectOne($c);
                } elseif ($tahap == 'revisi2_2') {
                    $c->add(Revisi22RincianSubParameterPeer::KODE_SUB, $kode_sub);
                    $rs_subparameter = Revisi22RincianSubParameterPeer::doSelectOne($c);
                } elseif ($tahap == 'revisi3') {
                    $c->add(Revisi3RincianSubParameterPeer::KODE_SUB, $kode_sub);
                    $rs_subparameter = Revisi3RincianSubParameterPeer::doSelectOne($c);
                } elseif ($tahap == 'revisi3_1') {
                    $c->add(Revisi31RincianSubParameterPeer::KODE_SUB, $kode_sub);
                    $rs_subparameter = Revisi31RincianSubParameterPeer::doSelectOne($c);
                } elseif ($tahap == 'revisi4') {
                    $c->add(Revisi4RincianSubParameterPeer::KODE_SUB, $kode_sub);
                    $rs_subparameter = Revisi4RincianSubParameterPeer::doSelectOne($c);
                } elseif ($tahap == 'revisi5') {
                    $c->add(Revisi5RincianSubParameterPeer::KODE_SUB, $kode_sub);
                    $rs_subparameter = Revisi5RincianSubParameterPeer::doSelectOne($c);
                } elseif ($tahap == 'revisi6') {
                    $c->add(Revisi6RincianSubParameterPeer::KODE_SUB, $kode_sub);
                    $rs_subparameter = Revisi6RincianSubParameterPeer::doSelectOne($c);
                } elseif ($tahap == 'revisi7') {
                    $c->add(Revisi7RincianSubParameterPeer::KODE_SUB, $kode_sub);
                    $rs_subparameter = Revisi7RincianSubParameterPeer::doSelectOne($c);
                } elseif ($tahap == 'revisi8') {
                    $c->add(Revisi8RincianSubParameterPeer::KODE_SUB, $kode_sub);
                    $rs_subparameter = Revisi8RincianSubParameterPeer::doSelectOne($c);
                } elseif ($tahap == 'revisi9') {
                    $c->add(Revisi9RincianSubParameterPeer::KODE_SUB, $kode_sub);
                    $rs_subparameter = Revisi9RincianSubParameterPeer::doSelectOne($c);
                } elseif ($tahap == 'revisi10') {
                    $c->add(Revisi10RincianSubParameterPeer::KODE_SUB, $kode_sub);
                    $rs_subparameter = Revisi10RincianSubParameterPeer::doSelectOne($c);
                } elseif ($tahap == 'rkua') {
                    $c->add(RkuaRincianSubParameterPeer::KODE_SUB, $kode_sub);
                    $rs_subparameter = RkuaRincianSubParameterPeer::doSelectOne($c);
                } else {
                    $c->add(DinasRincianSubParameterPeer::KODE_SUB, $kode_sub);
                    $rs_subparameter = DinasRincianSubParameterPeer::doSelectOne($c);
                }
                if ($rs_subparameter) {
                    ?>
                    <tr class="pekerjaans_<?php echo $id ?>" bgcolor="#AFC4EF">
                        <td colspan="11"><?php
                        if ($status == 'OPEN') {
                            if (($rs_subparameter->getFromSubKegiatan() == 'PJU001') or ( $rs_subparameter->getFromSubKegiatan() == '29.99.01.01') or ( $rs_subparameter->getFromSubKegiatan() == '29.99.01.02') or ( $rs_subparameter->getFromSubKegiatan() == '29.99.01.03')) {
                                echo link_to_function(image_tag('/sf/sf_default/images/icons/folder16.png'), 'if (confirm("Apakah anda yakin untuk me-nol kan Subtitle ini?")){satukanSubKegiatan(' . $id . ',"' . $kegiatan_code . '","' . $unit_id . '","' . $kode_sub . '")}');
                            }
                        }
                        ?> <b>
                            :. <?php echo $rs_subparameter->getSubKegiatanName() . ' ' . $rs_subparameter->getDetailName(); ?></b></td>
                            <td align="right">
                                <?php
                                echo number_format($rd->getTotalKodeSub($kode_sub), 0, ',', '.');
                                ?></td>
                                <td>&nbsp;</td>
                            </tr>
                            <?php
                        } else {
                            $ada = 'tidak';
                            $query = "select * from " . sfConfig::get('app_default_schema') . ".rincian_sub_parameter where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and new_subtitle ilike '%$sub%'";
                            $con = Propel::getConnection();
                            $stmt = $con->prepareStatement($query);
                            $t = $stmt->executeQuery();
                            while ($t->next()) {
                                if ($t->getString('kode_sub')) {
                                    ?>
                                    <tr class="pekerjaans_<?php echo $id ?>" bgcolor="#AFC4EF">
                                        <td colspan="11"><?php
                                        if ($status == 'OPEN') {
                                        }
                                        ?> <b> :. <?php echo $t->getString('sub_kegiatan_name') . ' ' . $t->getString('detail_name'); ?></b></td>
                                        <td align="right">
                                            <?php
                                            $query2 = "select sum(nilai_anggaran) as hasil_kali
                                            from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "rincian_detail
                                            where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and sub ilike '%$sub%' and status_hapus=false";

                                            $con = Propel::getConnection();
                                            $stmt = $con->prepareStatement($query2);
                                            $t = $stmt->executeQuery();
                                            while ($t->next()) {
                                                echo number_format($t->getString('hasil_kali'), 0, ',', '.');
                                            }
                                            $ada = 'ada';
                                            ?> </td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <?php
                                    }
                                }

                                if ($ada == 'tidak') {
                                    if ($kode_sub != '') {
                                        $query = "select * from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "rincian_detail
                                        where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and sub ilike '%$sub%' and status_hapus=false";
                                        $con = Propel::getConnection();
                                        $stmt = $con->prepareStatement($query);
                                        $t = $stmt->executeQuery();
                                        while ($t->next()) {
                                            if ($t->getString('kode_sub')) {
                                                ?>
                                                <tr class="pekerjaans_<?php echo $id ?>" bgcolor="#AFC4EF">
                                                    <td colspan="11">
                                                        <b> :. <?php echo $t->getString('komponen_name') . ' ' . $t->getString('detail_name'); ?></b>
                                                    </td>
                                                    <td align="right">
                                                        <?php
                                                        $query2 = "select sum(nilai_anggaran) as hasil_kali
                                                        from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "rincian_detail
                                                        where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and sub ilike '%$sub%' and status_hapus=false";
                                                        $con = Propel::getConnection();
                                                        $stmt = $con->prepareStatement($query2);
                                                        $t = $stmt->executeQuery();
                                                        while ($t->next()) {
                                                            echo number_format($t->getString('hasil_kali'), 0, ',', '.');
                                                        }
                                                        $ada = 'ada';
                                                    }
                                                    ?>
                                                </td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                }
                            }
                        }
                    } elseif (!$rd->getKodeSub()) {
                        $kode_sub = '';
                    }

                    $rekening_code = $rd->getRekeningCode();
                    if ($temp_rekening != $rekening_code) {
                        $temp_rekening = $rekening_code;
                        $c = new Criteria();
                        $c->add(RekeningPeer::REKENING_CODE, $rekening_code);
                        $rs_rekening = RekeningPeer::doSelectOne($c);
                        if ($rs_rekening) {
                            $rekening_name = $rs_rekening->getRekeningName();
                            $subtitle_name = $rd->getSubtitle();
                            $query_rekening = "select sum(nilai_anggaran) as jumlah_rekening from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "rincian_detail
                            where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and subtitle='$subtitle_name' and rekening_code='$rekening_code' and kode_sub='$kode_sub' and status_hapus=false";

                            $con = Propel::getConnection();
                            $stmt = $con->prepareStatement($query_rekening);
                            $t = $stmt->executeQuery();
                            while ($t->next()) {
                                $jumlah_rekening = number_format($t->getString('jumlah_rekening'), 0, ',', '.');
                                if ($t->getString('jumlah_rekening') == 0) {
                                    $query_rekening = "select sum(nilai_anggaran) as jumlah_rekening from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "rincian_detail
                                    where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and subtitle='$subtitle_name' and rekening_code='$rekening_code' and kode_sub isnull and status_hapus=false";
                                    $con = Propel::getConnection();
                                    $stmt = $con->prepareStatement($query_rekening);
                                    $ts = $stmt->executeQuery();
                                    while ($ts->next()) {
                                        $jumlah_rekening = number_format($ts->getString('jumlah_rekening'), 0, ',', '.');
                                    }
                                }
                            }
                            ?>
                            <tr class="pekerjaans_<?php echo $id ?>" bgcolor="#e6e6e6">
                                <td colspan="8"><i><?php echo $rekening_code . ' ' . $rekening_name; ?></i> </td>
                                <td align="right"><?php echo $jumlah_rekening ?></td>
                                <td>&nbsp;</td>
                            </tr>
                            <?php
                        }
                    }

                    if ($sf_user->getNamaUser() != 'parlemen2') 
                    {
                        ?>
                        <tr class="pekerjaans_<?php echo $id ?>">
                            <td id="action_pekerjaans_<?php echo $rd->getDetailNo() ?>">
                                <?php
                                $kegiatan = $rd->getKegiatanCode();
                                $unit = $rd->getUnitId();
                                $no = $rd->getDetailNo();
                                $sub = $rd->getSubtitle();
                                $komponen_id = $rd->getKomponenId();

                                $benar_musrenbang = 0;
                                if ($rd->getIsMusrenbang() == 'TRUE') {
                                    $benar_musrenbang = 1;
                                }
                                $benar_prioritas = 0;
                                if ($rd->getPrioritasWali() == 'TRUE') {
                                    $benar_prioritas = 1;
                                }
                                $benar_output = 0;
                                if ($rd->getIsOutput() == 'TRUE') {
                                    $benar_output = 1;
                                }
                                $benar_multiyears = 0;
                                if ($rd->getThKeMultiyears() <> null && $rd->getThKeMultiyears() > 0) {
                                    $benar_multiyears = 1;
                                }

                                $benar_hibah = 0;
                                if ($rd->getIsHibah() == 'TRUE') {
                                    $benar_hibah = 1;
                                }

                                if ($status == 'OPEN' && $sf_user->getNamaLogin() != 'admin' && $sf_user->getNamaLogin() !== 'tim_shs')  
                                {
                                    $query = "select distinct  rekening_code,rekening_name "
                                    . "from " . sfConfig::get('app_default_schema') . ".rekening "
                                    . "order by rekening_code";

                                    //--list rekening diambil dari ssh itu punya rekening apa saja
                                    $con = Propel::getConnection();
                                    $stmt = $con->prepareStatement($query);
                                    $ts = $stmt->executeQuery();
                                    $pilih = array();
                                    $ada = false;
                                    $select_str = "<select name='rek_$no' style='width:100px' class='select2'>";
                                    while ($ts->next()) {
                                        $ada = true;
                                        $r = $ts->getString('rekening_code');
                                        $rs = $ts->getString('rekening_code')." - ".$ts->getString('rekening_name');
                                        $select_str.="<option value='$r'>$rs</option>";
                                    }
                                    $select_str.="</select>";
                                    ?> 
                                    <div id="rekening_list_<?php echo $no ?>"> <?php
                                    if ($ada) {
                                        ?>
                                        <form method="post" id="form_rekening_list_<?php echo $no ?>">
                                            <input type="hidden" name="id" value="<?php echo $id; ?>" />
                                            <input type="hidden" name="unit_id" value="<?php echo $unit_id; ?>" />
                                            <input type="hidden" name="kegiatan_code" value="<?php echo $kegiatan_code; ?>" />
                                            <input type="hidden" name="detail_no" value="<?php echo $no; ?>" />
                                            <?php echo $select_str; ?>
                                            <input class="saveGantiRekening" type="submit" value="Simpan" class="btn btn-flat btn-xs" />
                                        </form>
                                    </div>
                                        <?php
                                    }
                                }
                            ?> 
                            <div class="btn-group">
                            <?php
                            if ($status == 'OPEN' && $rd->getLockSubtitle() != 'LOCK' && $sf_user->getNamaLogin() != 'admin' && $sf_user->getNamaLogin() !== 'tim_shs')
                            {                
                                echo link_to('<i class="fa fa-edit"></i> Edit', 'kegiatan/editKegiatanRevisi?id=' . $rd->getDetailNo() . '&unit=' . $rd->getUnitId() . '&kegiatan=' . $rd->getKegiatanCode() . '&edit=' . md5('ubah'), array('class' => 'btn btn-default btn-flat btn-sm'));                     
                                if (sfConfig::get('app_tahap_edit') == 'murni' && $benar_musrenbang == 0 && $benar_multiyears == 0) 
                                {
                                    $lelang = 0;
                                    $ceklelangselesaitidakaturanpembayaran = 0;

                                    if (sfConfig::get('app_fasilitas_cekeProject') == 'buka') {
                                        if (sfConfig::get('app_fasilitas_cekServer') == 'buka') {
                                            $lelang = $rd->getCekLelang($rd->getUnitId(), $rd->getKegiatanCode(), $rd->getDetailNo(), $rd->getNilaiAnggaran());
                                            if (sfConfig::get('app_fasilitas_cekeDelivery') == 'buka') {
                                                $ceklelangselesaitidakaturanpembayaran = $rd->getCekLelangTidakAdaAturanPembayaran($rd->getUnitId(), $rd->getKegiatanCode(), $rd->getDetailNo());
                                            }
                                        }
                                    }

                                    if ($lelang == 0 && $ceklelangselesaitidakaturanpembayaran == 0) 
                                    {
                                        ?>
                                        <button type="button" class="btn btn-default dropdown-toggle btn-flat btn-sm" data-toggle="dropdown">
                                            <span class="caret"></span>
                                            <span class="sr-only">Toggle Dropdown</span>
                                        </button>
                                        <div class="dropdown-menu" role="menu">
                                            <?php
                                            echo link_to('<i class="fa fa-trash"></i> Hapus Komponen', 'kegiatan/hapusPekerjaansRevisi?id=' . $id . '&no=' . $rd->getDetailNo() . '&unit=' . $rd->getUnitId() . '&kegiatan=' . $rd->getKegiatanCode(), Array('confirm' => 'Yakin untuk menghapus Komponen ' . $rd->getKomponenName() . ' ' . $rd->getDetailName() . ' ?', 'class' => 'btn btn-default btn-flat btn-sm'));
                                            ?>
                                        </div>
                                    <?php
                                    }
                                }
                                $arr_kunci_komponen = array('2.1.1.01.01.01.004.015','2.1.1.01.01.01.001.046','2.1.1.01.01.01.001.047','2.1.1.01.01.01.001.051','2.1.1.01.01.01.002.020','2.1.1.01.01.01.002.020','2.1.1.01.01.01.002.023','2.1.1.01.01.01.002.030','2.1.1.01.01.01.002.031','2.1.1.01.01.01.002.032','2.1.1.01.01.01.001.046.A','2.1.1.01.01.01.001.055','2.1.1.01.01.01.001.053.A','2.1.1.01.01.01.001.055','2.1.1.01.01.01.001.054');
                                $koKom = substr($rd->getKomponenId() , 0, 18);
                                $arr_tenaga_kontrak = array('2.1.1.01.01.01.008','2.1.1.03.05.01.001','2.1.1.01.01.01.004','2.1.1.01.01.02.099','2.1.1.01.01.01.005','2.1.1.01.01.01.006','2.1.1.01.01.02.001');
                                $query =
                                "SELECT COUNT(*) AS jumlah
                                FROM " . sfConfig::get('app_default_schema') . ".buka_komponen_murni
                                WHERE detail_kegiatan = '" . $rd->getDetailKegiatan() . "'";
                                $con = Propel::getConnection();
                                $stmt = $con->prepareStatement($query);
                                $rs = $stmt->executeQuery();
                                if($rs->next()) $jml = $rs->getString('jumlah');

                                $query_persediaan =
                                "SELECT detail_kegiatan
                                FROM " . sfConfig::get('app_default_schema') . ".kunci_komponen_murni
                                ";
                                $con = Propel::getConnection();
                                $stmt = $con->prepareStatement($query_persediaan);
                                $rs_persediaan = $stmt->executeQuery();
                                $arr_komponen_dikunci = array();
                                while($rs_persediaan->next()) {
                                    array_push($arr_komponen_dikunci, $rs_persediaan->getString('detail_kegiatan'));
                                }

                                if($jml <= 0 && ($rd->getIsOutput() || $rd->getIsMusrenbang() || $rd->getSumberDanaId() <> 11 || $rd->getPrioritasWali()|| $rd->getisCovid() == true || ( in_array($koKom,$arr_tenaga_kontrak) && ($rd->getSatuan() == 'Orang Bulan') && (strpos($rd->getKomponenName(), 'Biaya') ===  FALSE) ) || in_array($rd->getKomponenId(), $arr_kunci_komponen) || in_array($rd->getDetailKegiatan(), $arr_komponen_dikunci)))
                                {
                                    echo link_to('<i class="fas fa-unlock-alt"></i> Buka Komponen', 'kegiatan/bukaKomponenMurni?id=' . $rd->getDetailNo() . '&unit=' . $rd->getUnitId() . '&kegiatan=' . $rd->getKegiatanCode(), array('class' => 'btn btn-default btn-flat btn-sm'));
                                }
                                else
                                {
                                     echo link_to('<i class="fas fa-lock"></i> Kunci Komponen', 'kegiatan/kunciKomponenMurni?id=' . $rd->getDetailNo() . '&unit=' . $rd->getUnitId() . '&kegiatan=' . $rd->getKegiatanCode(), array('class' => 'btn btn-default btn-flat btn-sm'));
                                }
                                
                            } 
                            ?>
                            </div>
                            <div class="clearfix"></div>
                            <?php
                            //irul 25agustus 2014 - fungsi GMAP
                            if ($rd->getTipe2() == 'PERENCANAAN' || $rd->getTipe2() == 'PENGAWASAN' || $rd->getTipe2() == 'KONSTRUKSI' || $rd->getTipe() == 'FISIK' || $est_fisik) {
                                //22 Februari 2016 - kembalikan ke waitinglist
                                $con = Propel::getConnection();
                                $kode_rka = $unit_id . '.' . $kegiatan_code . '.' . $rd->getDetailNo();
                                $query_cek_waiting = "select id_waiting from ebudget.waitinglist_pu where kode_rka = '" . $kode_rka . "' ";
                                $stmt_cek_waiting = $con->prepareStatement($query_cek_waiting);
                                $rs_cek_waiting = $stmt_cek_waiting->executeQuery();
                                if ($rs_cek_waiting->next()) {
                                    ?>
                                    <div id="tempat_ajax_<?php echo str_replace('.', '_', $kegiatan_code) . '_' . $rd->getDetailNo() ?>">
                                        <?php echo link_to_function('<i class="fa fa-trash"></i> Kembalikan ke Waiting List', 'execKembalikanWaitingRevisi("' . $rd->getDetailNo() . '","' . $rd->getUnitId() . '","' . $rd->getKegiatanCode() . '", "' . str_replace('.', '_', $kegiatan_code) . '_' . $rd->getDetailNo() . '")', array('class' => 'btn btn-default btn-flat btn-sm')); ?>
                                    </div>
                                    <?php
                                }

                                $id_kelompok = 0;
                                $tot = 0;
                                $query = "select count(*) as tot "
                                . "from " . sfConfig::get('app_default_gis') . ".geojsonlokasi_rev1 "
                                . "where unit_id='" . $rd->getUnitId() . "' and kegiatan_code='" . $rd->getKegiatanCode() . "' and detail_no ='" . $rd->getDetailNo() . "' "
                                . "and tahun = '" . sfConfig::get('app_tahun_default') . "' and status_hapus = FALSE";
                                $stmt = $con->prepareStatement($query);
                                $rs = $stmt->executeQuery();
                                while ($rs->next()) {
                                    $tot = $rs->getString('tot');
                                }
                                if ($tot == 0) {
                                    $con = Propel::getConnection();
                                    $c2 = new Criteria ();
                                    $crit1 = $c2->getNewCriterion(KomponenPeer::KOMPONEN_TIPE, 'FISIK');
                                    $crit1->addOr($c2->getNewCriterion(KomponenPeer::KOMPONEN_TIPE, 'EST'));
                                    $crit1->addOr($c2->getNewCriterion(KomponenPeer::KOMPONEN_TIPE2, 'KONSTRUKSI'));
                                    $crit1->addOr($c2->getNewCriterion(KomponenPeer::KOMPONEN_TIPE2, 'PENGAWASAN'));
                                    $crit1->addOr($c2->getNewCriterion(KomponenPeer::KOMPONEN_TIPE2, 'PERENCANAAN'));
                                    $c2->add($c2->getNewCriterion(KomponenPeer::KOMPONEN_NAME, $rd->getKomponenName(), Criteria::ILIKE));
                                    $c2->addAnd($crit1);
                                    $rd2 = KomponenPeer::doSelectOne($c2);
                                    if ($rd2) {
                                        $komponen_id = $rd2->getKomponenId();
                                        $satuan = $rd2->getSatuan();
                                    } else {
                                        $komponen_id = '0';
                                        $satuan = '';
                                    }

                                    if ($komponen_id == '0') {
                                        $query2 = "select * from master_kelompok_gmap where '" . $rd->getKomponenName() . "' ilike nama_objek||'%'";
                                    } else {
                                        $query2 = "select * from master_kelompok_gmap where '" . $komponen_id . "' ilike kode_kelompok||'%'";
                                    }
                                    $stmt2 = $con->prepareStatement($query2);
                                    $rs2 = $stmt2->executeQuery();
                                    while ($rs2->next()) {
                                        $id_kelompok = $rs2->getString('id_kelompok');
                                    }

                                    if ($id_kelompok == '' || $id_kelompok == 0 || $id_kelompok == null) {
                                        $id_kelompok = 19;
                                        if (in_array($satuan, array('Kegiatan', 'Lokasi', 'M2', 'M²', 'm3', 'Paket', 'Set'))) {
                                            $id_kelompok = 100;
                                        } elseif (in_array($satuan, array('m', 'M', 'M1', 'Meter', 'Titik', 'Unit'))) {
                                            $id_kelompok = 101;
                                        }
                                    }
                                } else {
                                    $con = Propel::getConnection();
                                    $query = "select * from " . sfConfig::get('app_default_gis') . ".geojsonlokasi_rev1 
                                    where unit_id='" . $rd->getUnitId() . "' and kegiatan_code='" . $rd->getKegiatanCode() . "' and detail_no ='" . $rd->getDetailNo() . "' and tahun = '" . sfConfig::get('app_tahun_default') . "'";
                                    $stmt = $con->prepareStatement($query);
                                    $rs = $stmt->executeQuery();
                                    while ($rs->next()) {
                                        $mlokasi = $rs->getString('mlokasi');
                                        $id_kelompok = $rs->getString('id_kelompok');
                                    }

                                    $query = "select max(lokasi_ke) as total_lokasi from " . sfConfig::get('app_default_gis') . ".geojsonlokasi_rev1 
                                    where unit_id='" . $rd->getUnitId() . "' and kegiatan_code='" . $rd->getKegiatanCode() . "' and detail_no ='" . $rd->getDetailNo() . "' and tahun = '" . sfConfig::get('app_tahun_default') . "'";
                                    $stmt = $con->prepareStatement($query);
                                    $rs = $stmt->executeQuery();
                                    while ($rs->next()) {
                                        $total_lokasi = $rs->getString('total_lokasi');
                                    }
                                }
                                ?>
                                <div class="btn-group">                            
                                    <?php
                                    if ($status == 'OPEN') {
                                        echo link_to_function('<i class="fa fa-map-marker"></i>', '', array('class' => 'btn btn-default btn-flat btn-sm', 'disable' => true));
                                        if ($tot == 0) {
                                            $array_bersih_kurung = array('(', ')');
                                            $lokasi_bersih_kurung = str_replace($array_bersih_kurung, '', $lokasi);
                                            echo link_to('<i class="fa fa-edit"></i> Input Lokasi', sfConfig::get('app_path_gmap') . 'insertBaru.php?unit_id=' . $rd->getUnitId() . '&kode_kegiatan=' . $rd->getKegiatanCode() . '&detail_no=' . $rd->getDetailNo() . '&satuan=' . $rd->getSatuan() . '&volume=' . $rd->getVolume() . '&nilai_anggaran=' . $rd->getNilaiAnggaran() . '&tahun=' . sfConfig::get('app_tahun_default') . '&mlokasi=&id_kelompok=' . $id_kelompok . '&th_load=0&level=9&nm_user=' . $sf_user->getNamaUser() . '&lokasi_ke=1', array('class' => 'btn btn-default btn-flat btn-sm'));
                                        } else {
                                            echo link_to('<i class="fa fa-search"></i> View Lokasi', sfConfig::get('app_path_gmap') . 'viewData.php?unit_id=' . $rd->getUnitId() . '&kode_kegiatan=' . $rd->getKegiatanCode() . '&detail_no=' . $rd->getDetailNo() . '&satuan=' . $rd->getSatuan() . '&volume=' . $rd->getVolume() . '&nilai_anggaran=' . $rd->getNilaiAnggaran() . '&tahun=' . sfConfig::get('app_tahun_default') . '&mlokasi=' . $mlokasi . '&id_kelompok=' . $id_kelompok . '&th_load=0&level=9&nm_user=' . $sf_user->getNamaUser() . '&lokasi_ke=1&total_lokasi=' . $total_lokasi, array('class' => 'btn btn-default btn-flat btn-sm'));
                                            ?>
                                            <button type="button" class="btn btn-default dropdown-toggle btn-flat btn-sm" data-toggle="dropdown">
                                                <span class="caret"></span>
                                                <span class="sr-only">Toggle Dropdown</span>
                                            </button>
                                            <div class="dropdown-menu" role="menu">
                                                <?php
                                                echo link_to('Edit Lokasi', sfConfig::get('app_path_gmap') . 'updateData.php?unit_id=' . $rd->getUnitId() . '&kode_kegiatan=' . $rd->getKegiatanCode() . '&detail_no=' . $rd->getDetailNo() . '&satuan=' . $rd->getSatuan() . '&volume=' . $rd->getVolume() . '&nilai_anggaran=' . $rd->getNilaiAnggaran() . '&tahun=' . sfConfig::get('app_tahun_default') . '&mlokasi=' . $mlokasi . '&id_kelompok=' . $id_kelompok . '&th_load=0&level=9&nm_user=' . $sf_user->getNamaUser() . '&lokasi_ke=1&total_lokasi=' . $total_lokasi, array('class' => 'dropdown-item'));
                                                echo link_to('Hapus Lokasi', 'kegiatan/hapusLokasi?id=' . $id . '&no=' . $rd->getDetailNo() . '&unit=' . $rd->getUnitId() . '&kegiatan=' . $rd->getKegiatanCode(), Array('confirm' => 'Yakin untuk menghapus Lokasi untuk Komponen ' . $rd->getKomponenName() . ' ' . $rd->getDetailName() . ' ?', 'class' => 'dropdown-item'));
                                                ?>
                                            </div>
                                        <?php
                                    }
                                }
                                ?>
                            </div>
                            <?php
                        }
                    ?>
            </td>
            <td>
                <?php
                if ($benar_musrenbang == 1) {
                    echo '&nbsp;<span class="badge badge-success">Musrenbang</span>';
                }
                if ($benar_output == 1) {
                    echo '&nbsp;<span class="badge badge-info">Output</span>';
                }
                if ($benar_prioritas == 1) {
                    echo '&nbsp;<span class="badge badge-warning">Prioritas</span>';
                }
                if ($benar_hibah == 1) {
                    echo '&nbsp;<span class="badge badge-success">Hibah</span>';
                }
                if ($rd->getKecamatan() <> '') {
                    echo '&nbsp;<span class="badge badge-warning">Jasmas</span>';
                }
                if ($benar_multiyears == 1) {
                    echo '&nbsp;<span class="badge badge-primary">Multiyears Tahun ke ' . $rd->getThKeMultiyears() . '</span>';
                }
               
                echo '<br/>';
                echo $rd->getKomponenName();
                if (sfConfig::get('app_fasilitas_keteranganKomponen') == 'buka') {
                    echo ' ' . $rd->getDetailName();
                    echo '&nbsp;<span class="badge badge-danger">' . $rd->getTahap(). '</span><br/>';
                    if ($rd->getSumberDanaId() == 1) {
                        if ($rd->getSubSumberDanaId() == 1)
                        echo '&nbsp;<span class="badge badge-info">DAK (BOK)</span>';
                        else
                         echo '&nbsp;<span class="badge badge-info">DAK</span>';
                    }   
                    if ($rd->getSumberDanaId() == 2) {
                        echo '&nbsp;<span class="badge badge-info">>DBHCT</span>';
                    }
                    if ($rd->getSumberDanaId() == 3) {
                        echo '&nbsp;<span class="badge badge-info">DID</span>';
                    }
                    if ($rd->getSumberDanaId() == 4) {
                        echo '&nbsp;<span class="badge badge-info">DBH</span>';
                    }
                    if ($rd->getSumberDanaId() == 5) {
                        echo '&nbsp;<span class="badge badge-info">Pajak Rokok</span>';
                    }
                    if ($rd->getSumberDanaId() == 6) {
                        echo '&nbsp;<span class="badge badge-info">BLUD</span>';
                    }
                    if ($rd->getSumberDanaId() == 7) {
                        echo '&nbsp;<span class="badge badge-info">Bantuan Keuangan Provinsi</span>';
                    }
                    if ($rd->getSumberDanaId() == 8) {
                        echo '&nbsp;<span class="badge badge-info">Kapitasi JKN</span>';
                    }
                    if ($rd->getSumberDanaId() == 9) {
                        echo '&nbsp;<span class="badge badge-info">BOS</span>';
                    }
                    if ($rd->getSumberDanaId() == 10) {
                        echo '&nbsp;<span class="badge badge-info">DAU</span>';
                    }
                    if ($rd->getSumberDanaId() == 12) {
                        echo '&nbsp;<span class="badge badge-info">Hibah Pariwisata</span>';
                    }
                    if ($rd->getIsCovid() == true) {
                        echo '&nbsp;<span class="badge badge-info">Covid-19</span>';
                    }
                    if ($rd->getTipe2() == 'KONSTRUKSI' || $rd->getTipe() == 'FISIK' || $est_fisik) {
                        if ($rd->getLokasiKecamatan() <> '' && $rd->getLokasiKelurahan() <> '') {
                            echo '[' . $rd->getLokasiKelurahan() . ' - ' . $rd->getLokasiKecamatan() . ']';
                        }
                    }
                }

                $query2 = "select tahap, status_masuk from " . sfConfig::get('app_default_schema') . ".komponen "
                . "where komponen_id='" . $rd->getKomponenId() . "'";
                $con = Propel::getConnection();
                $stmt = $con->prepareStatement($query2);
                $t = $stmt->executeQuery();
                while ($t->next()) {

                    if ($t->getString('tahap') == DinasMasterKegiatanPeer::getTahapKegiatan($rd->getUnitId(), $rd->getKegiatanCode()) && sfConfig::get('app_tahap_edit') != 'murni') {
                        echo image_tag('/images/newanima.gif');
                    }
                }
                if (in_array($rd->getDetailNo(), $komponen_serupa)) {
                    echo image_tag('/images/warning.gif', array('title' => 'Ada komponen serupa', 'alt' => 'Ada komponen serupa'));
                }
                ?>
            </td>
            <td align="center">
                <?php echo $rd->getTipe(); ?>
            </td>
            <td align="center">
                <?php echo $rd->getSatuan(); ?>
            </td>
            <?php if( $rd->getAccres() > 1) 
            {
                $keterangan_koefisien= '('.$rd->getKeteranganKoefisien().') + '.$rd->getAccres().'%';
            }
            else
            {
                $keterangan_koefisien=$rd->getKeteranganKoefisien();
            }
            ?>
            <td align="center">
                <?php echo $keterangan_koefisien; ?>
            </td>
            <td align="right">
                <?php
                if ($rd->getSatuan() == '%') {
                    echo number_format($rd->getKomponenHargaAwal(), 4, ',', '.');
                } elseif ($rd->getKomponenHargaAwal() != floor($rd->getKomponenHargaAwal())) {
                    echo number_format($rd->getKomponenHargaAwal(), 2, ',', '.');
                } else {
                    echo number_format($rd->getKomponenHargaAwal(), 0, ',', '.');
                }
                ?>
            </td>
            <td align="right"><?php
            $volume = $rd->getVolume();
            $harga = $rd->getKomponenHargaAwal();
            $hasil = $volume * $harga;

            if ($hasil < 1) {
                echo number_format($hasil, 4, ',', '.');
            }
            else {
                echo number_format($hasil, 0, ',', '.');    
            }
            ?>
            </td>
            <td align="right">
                <?php echo $rd->getPajak() . '%'; ?>
            </td>
            <td align="right">
                <?php 
                $volume = $rd->getVolume();
                $harga = $rd->getKomponenHargaAwal();
                $pajak = $rd->getPajak();
                $total = $rd->getNilaiAnggaran();

                if ($total < 1) {
                    echo number_format($total, 4, ',', '.');
                }
                else {
                    echo number_format($total, 0, ',', '.');    
                }
                ?>
            </td>
            <td align="center">
                <?php
                $rekening = $rd->getRekeningCode();
                $rekening_code = substr($rekening, 0, 6);
                $c = new Criteria ();
                $c->add(KelompokBelanjaPeer::BELANJA_CODE, $rekening_code);
                $rs_rekening = KelompokBelanjaPeer::doSelectOne($c);
                if ($rs_rekening) {
                    echo $rs_rekening->getBelanjaName();
                }
                ?>
            </td>  
        </tr>
<?php
}
endforeach;
?>

<script>
    $(".saveGantiRekening").click(function () { // changed
        var id_form = $(this).closest("form").attr("id"); //parent form
        var detailno = id_form.split("_").pop();
        $.ajax({
            type: "POST",
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/kegiatan/gantiRekeningRevisi.html",
            data: $(this).parent().serialize(), // changed
            success: function () {
                $('#action_pekerjaans_' + detailno).html('<br/><font style="color: green;font-weight:bold">{Silahkan Refresh Halaman}</font><br/><br/>');
            }
        });
        return false; // avoid to execute the actual submit of the form.
    });

    function execLockHargaDasarRevisi(act, id, detNo, unitId, kegCode) {
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/kegiatan/lockhargadasarrevisi/act/" + act + "/id/" + id + "/detail_no/" + detNo + "/unit_id/" + unitId + "/kegiatan_code/" + kegCode + ".html",
            context: document.body
        }).done(function (msg) {
            $('#tempat_ajax_' + detNo).html(msg);
        });
    }

    function execKembalikanWaitingRevisi(detailno, unitid, kodekegiatan, id) {
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/kegiatan/kembalikanWaitingEditRevisi/unitid/" + unitid + "/kodekegiatan/" + kodekegiatan + "/detailno/" + detailno + ".html",
            context: document.body
        }).done(function (msg) {
            $('#tempat_ajax_' + id).html(msg);
        });
    }
</script>
