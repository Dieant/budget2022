<?php use_helper('I18N', 'Date', 'Url', 'Javascript', 'Form', 'Object', 'Number', 'Validation') ?>
<?php ?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>Lihat Verifikasi Kegiatan</h1>
</section>

<!-- Main content -->
<section class="content">
    <?php include_partial('kegiatan/list_messages'); ?>

    <div class="box box-primary box-solid">
        <div class="box-header with-border">
            <h3 class="box-title">Lihat Verifikasi Kegiatan</h3>
        </div>
        <div class="box-body">
            <div id="sf_admin_container" class="table-responsive">
                <?php echo form_tag('kegiatan/lihatVerifikasiKegiatan') ?>
                <table cellspacing="0" class="sf_admin_list">
                    <thead>  
                        <tr>
                            <th style="width: 40%" colspan="3"><b>Jawaban Perangkat Daerah</b></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(!is_null($jawabankegiatan->getUbahF1Dinas())): ?>
                            <tr class="sf_admin_row_0" align='right'>
                                <td style="width: 40%">Apakah kegiatan ini ada usulan perubahan F1 ?</td>
                                <td style="width: 1%" align="center">:</td>
                                <td style="width: 59%" align="left">
                                    <?php echo ($jawabankegiatan->getUbahF1Dinas()) ? 'Ya' : 'Tidak'; ?>
                                </td>
                            </tr>
                            <tr class="sf_admin_row_1" align='right'>
                                <td>Apakah kegiatan ini ada penggunaan sisa lelang ?</td>
                                <td align="center">:</td>
                                <td align="left">
                                    <?php echo ($jawabankegiatan->getSisaLelangDinas()) ? 'Ya' : 'Tidak'; ?>
                                </td>
                            </tr>
                            <tr class="sf_admin_row_0" align='right'>
                                <td>Catatan penggunaan sisa lelang</td>
                                <td align="center">:</td>
                                <td align="left">
                                    <?php echo (!is_null($jawabankegiatan->getCatatanSisaLelangDinas())) ? $jawabankegiatan->getCatatanSisaLelangDinas() : '' ; ?>
                                </td>
                            </tr>    
                        <?php endif; ?>
                        <tr class="sf_admin_row_0" align='right'>
                            <td style="width: 40%">Apakah anda akan merubah output kegiatan / output subtitle?</td>
                            <td style="width: 1%" align="center">:</td>
                            <td style="width: 59%" align="left">
                                <?php echo ($rs_rincian_bappeko->getJawaban1Dinas()) ? 'Ya' : 'Tidak'; ?>
                            </td>
                        </tr>
                        <tr class="sf_admin_row_1" align='right'>
                            <td>Apakah anda akan merubah judul subtitle?</td>
                            <td align="center">:</td>
                            <td align="left">
                                <?php echo ($rs_rincian_bappeko->getJawaban2Dinas()) ? 'Ya' : 'Tidak'; ?>
                            </td>
                        </tr>
                        <tr class="sf_admin_row_0" align='right'>
                            <td>Catatan Perangkat Daerah / Unit Kerja</td>
                            <td align="center">:</td>
                            <td align="left">
                                <?php echo (!is_null($rs_rincian_bappeko->getCatatanDinas()) ? $rs_rincian_bappeko->getCatatanDinas() : '' ) ; ?>
                            </td>
                        </tr>
                        <tr class="sf_admin_row_1" align='right'>
                            <td>Persetujuan Bappeko</td>
                            <td align="center">:</td>
                            <td align="left">
                                <?php
                                if (!is_null($rs_rincian_bappeko->getStatusBuka())) {
                                    echo ($rs_rincian_bappeko->getStatusBuka()) ? 'Setuju' : 'Tidak Setuju, karena ' . $rs_rincian_bappeko->getCatatanBappeko();
                                }
                                ?>
                            </td>
                        </tr>
                        <br>
                        <br>
                        
                    </tbody>
                </table>
                <?php if (!is_null($rs_rincian_bappeko->getJawaban1Bappeko()) && !is_null($rs_rincian_bappeko->getJawaban2Bappeko())): ?>
                    <table cellspacing="0" class="sf_admin_list">
                        <thead>  
                            <tr>
                                <th style="width: 40%" colspan="3"><b>Jawaban Bappeko</b></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="sf_admin_row_0" align='right'>
                                <td style="width: 40%">Apakah kegiatan ini ada perubahan output kegiatan / output subtitle?</td>
                                <td style="width: 1%" align="center">:</td>
                                <td style="width: 59%" align="left">
                                    <?php echo ($rs_rincian_bappeko->getJawaban1Bappeko()) ? 'Ya' : 'Tidak'; ?>
                                </td>
                            </tr>
                            <tr class="sf_admin_row_1" align='right'>
                                <td>Apakah kegiatan ini ada perubahan judul subtitle?</td>
                                <td align="center">:</td>
                                <td align="left">
                                    <?php echo ($rs_rincian_bappeko->getJawaban2Bappeko()) ? 'Ya' : 'Tidak'; ?>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                <?php endif; ?>

                <?php if (!is_null($jawabankegiatan->getUbahF1Peneliti()) ): ?>
                    <table cellspacing="0" class="sf_admin_list">
                        <thead>  
                            <tr>
                                <th style="width: 40%" colspan="3"><b>Jawaban Penyelia</b></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="sf_admin_row_0" align='right'>
                                <td style="width: 40%">Apakah kegiatan ini ada usulan perubahan F1 ? </td>
                                <td style="width: 1%" align="center">:</td>
                                <td style="width: 59%" align="left">
                                    <?php echo ($jawabankegiatan->getUbahF1Peneliti()) ? 'Ya' : 'Tidak'; ?>
                                </td>
                            </tr>
                            <tr class="sf_admin_row_1" align='right'>
                                <td style="width: 40%">Apakah kegiatan ini ada penggunaan sisa lelang ? </td>
                                <td style="width: 1%" align="center">:</td>
                                <td style="width: 59%" align="left">
                                    <?php echo ($jawabankegiatan->getSisaLelangPeneliti()) ? 'Ya' : 'Tidak'; ?>
                                </td>
                            </tr>
                            <tr class="sf_admin_row_0" align='right'>
                                <td style="width: 40%">Catatan penggunaan sisa lelang ? </td>
                                <td style="width: 1%" align="center">:</td>
                                <td style="width: 59%" align="left">
                                    <?php echo (!is_null($jawabankegiatan->getCatatanSisaLelangPeneliti())) ? $jawabankegiatan->getCatatanSisaLelangPeneliti() : ''; ?>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                <?php endif; ?>

                <table cellspacing="0" class="sf_admin_list">
                    <thead>  
                        <tr>
                            <th colspan="3"><b>Tindakan</b></th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr align='center' valign="top">
                            <td colspan="3">
                                <?php
                                echo input_hidden_tag('unit_id', $sf_params->get('unit_id'));
                                echo input_hidden_tag('kode_kegiatan', $sf_params->get('kode_kegiatan'));
                                if (!is_null($rs_rincian_bappeko->getStatusBuka())) {
                                    echo submit_tag('Ubah Status Persetujuan Bappeko', array('name' => 'ubah', 'class' => 'btn btn-success'));
                                }
                                echo ' ' . submit_tag('Hapus', array('name' => 'hapus', 'class' => 'btn btn-danger', 'confirm' => 'Apakah anda yakin untuk menghapus verifikasi ini?'));
                                echo ' ' . button_to('Kembali', '#', array('onClick' => "javascript:history.back()"));
                                ?>
                            </td>
                        </tr>
                    </tfoot>
                </table>
                <?php echo '</form>'; ?>
            </div>
        </div>
    </div>
</section>
