<div class="card-body table-responsive p-0">
    <table class="table table-hover">
        <thead class="head_peach">
            <!--  <?php include_partial('kegiatan/list_messages'); ?> -->
            <tr>
                <?php include_partial('list_th_tabular') ?>
                <th class="align-middle"><?php echo __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php
            $i = 1;
            foreach ($pager->getResults() as $master_kegiatan): $odd = fmod(++$i, 2)
                ?>
                <tr>
                    <?php include_partial('list_td_tabular', array('master_kegiatan' => $master_kegiatan)) ?>
                    <?php include_partial('list_td_tahap', array('master_kegiatan' => $master_kegiatan)) ?>
                    <?php include_partial('list_td_actions', array('master_kegiatan' => $master_kegiatan)) ?>
                </tr>
            <?php endforeach; ?>
            <tr>
                <td colspan="2">&nbsp;</td>
                <td align="right">
                    <?php
                    $kode_kegiatan = $master_kegiatan->getKodeKegiatan();
                    $unit_id = $master_kegiatan->getUnitId();
                    if (sfConfig::get('app_tahap_edit') == 'murni') {
                        $query = "select sum(alokasi_dana) as nilai from " . sfConfig::get('app_default_schema') . ".master_kegiatan where unit_id='$unit_id'";
                    } else if (sfConfig::get('app_tahap_edit') != 'murni') {
                        $query = "select sum(nilai_anggaran) as nilai from " . sfConfig::get('app_default_schema') . ".murni_rincian_detail where unit_id='$unit_id' and status_hapus=FALSE";
                    }
                    $con = Propel::getConnection(RincianDetailPeer::DATABASE_NAME);
                    $statement = $con->prepareStatement($query);
                    $rs_nilai = $statement->executeQuery();
                    while ($rs_nilai->next()) {
                        $nilai_awal = $rs_nilai->getString('nilai');
                    }
                    echo '<b/>'.number_format($nilai_awal, 0, ',', '.').'</b>';
                    ?>
                </td>
                <td align="right">
                    <?php
                    $kode_kegiatan = $master_kegiatan->getKodeKegiatan();
                    $unit_id = $master_kegiatan->getUnitId();
                    $query = "select sum(nilai_anggaran) as nilai from " . sfConfig::get('app_default_schema') . ".rincian_detail where unit_id='$unit_id' and status_hapus=FALSE";
                    $con = Propel::getConnection(RincianDetailPeer::DATABASE_NAME);
                    $statement = $con->prepareStatement($query);
                    $rs_nilai = $statement->executeQuery();
                    while ($rs_nilai->next()) {
                        $nilai = $rs_nilai->getString('nilai');
                    }
                    echo '<b/>'.number_format($nilai, 0, ',', '.').'</b>';
                    ?>
                </td>
                <td align="right">
                    <?php
                    if (sfConfig::get('app_tahap_edit') == 'murni') {
                        $selisih = $nilai_awal - $nilai;
                    } else if (sfConfig::get('app_tahap_edit') != 'murni') {
                        // $selisih = 0; // ==> TEMPORARY/SEMENTARA
                        $selisih = $nilai - $nilai_awal; //==>slaen pak
                        //$selisih = $nilai - $nilai_awal_pak;
                    }
                    echo '<b/>'.number_format($selisih, 0, ',', '.').'</b>';
                    ?>
                </td>
                <td align="right">
                    Total Pagu : 
                    <?php
                    $unit_id = $master_kegiatan->getUnitId();
                    $query = "select sum(tambahan_pagu) as nilai from " . sfConfig::get('app_default_schema') . ".master_kegiatan where unit_id='$unit_id'";
                    $con = Propel::getConnection(MasterKegiatanPeer::DATABASE_NAME);
                    $statement = $con->prepareStatement($query);
                    $rs_nilai = $statement->executeQuery();
                    while ($rs_nilai->next()) {
                        $nilai = $rs_nilai->getString('nilai');
                    }
                    echo '<b/>'.number_format($nilai, 0, ',', '.').'</b>';
                    ?>
                </td>
                <td colspan="5">&nbsp;</td>
            </tr>
        </tbody>
    </table>
</div>
<div class="card-footer clearfix">
    <ul class="pagination pagination-sm m-0 float-left">
        <?php
            echo format_number_choice('[0] no result|[1] 1 result|(1,+Inf] %1% results', array('%1%' => $pager->getNbResults()), $pager->getNbResults()) 
        ?>
    </ul>
    <ul class="pagination pagination-sm m-0 float-right">
        <?php 
        if ($pager->haveToPaginate()): 
            echo '<li class="page-item">'.link_to('Previous', 'kegiatan/list?page=' . $pager->getPreviousPage(), array('class' => 'page-link', 'align' => 'absmiddle', 'alt' => __('Previous'), 'title' => __('Previous'))).'</li>';

            foreach ($pager->getLinks() as $page):
                $activeclass = ($page == $pager->getPage()) ? 'active' : '';
                echo '<li class="page-item '.$activeclass.'">'.link_to_unless($page == $pager->getPage(), $page, "kegiatan/list?page={$page}", array('class' => 'page-link')).'</li>';
            endforeach;
            echo '<li class="page-item">'.link_to('Next', 'kegiatan/list?page=' . $pager->getNextPage(), array('class' => 'page-link', 'align' => 'absmiddle', 'alt' => __('Next'), 'title' => __('Next'))).'</li>'; 
            endif;
        ?>
    </ul>
</div>