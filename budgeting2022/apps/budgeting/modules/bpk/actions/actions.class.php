<?php

//bisma
/* * *
 * bpk actions.
 *
 * @package    budgeting
 * @subpackage bpk
 * @author     pemkot
 * @version    SVN: $Id: actions.class.php 2288 2006-10-02 15:22:13Z fabien $
 */
class bpkActions extends autobpkActions {

    public function executeUbahPassLogin() {
        $username = $this->getUser()->getNamaLogin();
        if ($this->getRequest()->getMethod() == sfRequest::POST) {
            if ($this->getRequestParameter('keluar')) {

                // if ($this->getUser()->hasCredential('dinas'))
                //     return $this->redirect('login/logoutDinas');
                // else if ($this->getUser()->hasCredential('dewan'))
                //     return $this->redirect('login/logoutDewan');
                // if ($this->getUser()->hasCredential('viewer'))
                //     return $this->redirect('login/logoutViewer');
                if ($this->getUser()->hasCredential('peneliti'))
                    return $this->redirect('login/logoutPeneliti');
                // else if ($this->getUser()->hasCredential('data'))
                //     return $this->redirect('login/logoutData');
                else
                    return $this->redirect('login/logout');
            }
            if ($this->getRequestParameter('simpan')) {
                $pass_lama = $this->getRequestParameter('pass_lama');
                $pass_baru = $this->getRequestParameter('pass_baru');
                $ulang_pass_baru = $this->getRequestParameter('ulang_pass_baru');
                $md5_pass_lama = md5($pass_lama);
                $md5_pass_baru = md5($pass_baru);

                $c_user = new Criteria();
                $c_user->add(MasterUserV2Peer::USER_ID, $username);
                $c_user->add(MasterUserV2Peer::USER_PASSWORD, $md5_pass_lama);
                $rs_user = MasterUserV2Peer::doSelectOne($c_user);
                if ($rs_user) {
                    if ($pass_baru == $ulang_pass_baru) {
                        $con = Propel::getConnection();
                        $con->begin();
                        try {
                            $newSetting = MasterUserV2Peer::retrieveByPK($username);
                            //$newSetting->setUserName($namaUser);
                            //$newSetting->setNip($nip);
                            $newSetting->setUserPassword($md5_pass_baru);
                            $newSetting->save($con);
                            budgetLogger::log('Username  ' . $username . ' telah mengganti password pada e-budgeting');
                            $this->setFlash('berhasil', 'Password sudah berhasil dirubah, silahkan keluar dan login kembali');
                            // $con->commit();

                            $c = new Criteria();
                            $c->add(SchemaAksesV2Peer::USER_ID, $username);
                            $c->add(SchemaAksesV2Peer::SCHEMA_ID, 2);
                            $cs = SchemaAksesV2Peer::doSelectOne($c);
                            $cs->setIsUbahPass(true);
                            $cs->save($con);
                            $con->commit();
                            // return $this->redirect('entri/eula?unit_id=' . $unit_id);
                        } catch (Exception $e) {
                            $this->setFlash('gagal', 'Password gagal dirubah karena ' . $e);
                            $con->rollback();
                        }
                    }
                }
            }
        }
        $settingPass = new Criteria();
        $settingPass->add(MasterUserV2Peer::USER_ID, $username);
        $this->profil = $rs_settingPass = MasterUserV2Peer::doSelectOne($settingPass);
        $this->setLayout('layouteula');
    }

    public function executeGetHeader() {
        $this->tahap = $tahap = $this->getRequestParameter('tahap');
        if ($tahap == 'murni') {
            $c = new Criteria();
            $c->add(MurniMasterKegiatanPeer::KODE_KEGIATAN, $this->getRequestParameter('kegiatan'));
            $c->add(MurniMasterKegiatanPeer::UNIT_ID, $this->getRequestParameter('unit'));

            $master_kegiatan = MurniMasterKegiatanPeer::doSelectOne($c);
            if ($master_kegiatan) {
                $this->master_kegiatan = $master_kegiatan;
            }
        } elseif ($tahap == 'murnibp') {
            $c = new Criteria();
            $c->add(MurniBukuPutihMasterKegiatanPeer::KODE_KEGIATAN, $this->getRequestParameter('kegiatan'));
            $c->add(MurniBukuPutihMasterKegiatanPeer::UNIT_ID, $this->getRequestParameter('unit'));

            $master_kegiatan = MurniBukuPutihMasterKegiatanPeer::doSelectOne($c);
            if ($master_kegiatan) {
                $this->master_kegiatan = $master_kegiatan;
            }
        } elseif ($tahap == 'murnibb') {
            $c = new Criteria();
            $c->add(MurniBukuBiruMasterKegiatanPeer::KODE_KEGIATAN, $this->getRequestParameter('kegiatan'));
            $c->add(MurniBukuBiruMasterKegiatanPeer::UNIT_ID, $this->getRequestParameter('unit'));

            $master_kegiatan = MurniBukuBiruMasterKegiatanPeer::doSelectOne($c);
            if ($master_kegiatan) {
                $this->master_kegiatan = $master_kegiatan;
            }
        } else {
            $c = new Criteria();
            $c->add(MasterKegiatanPeer::KODE_KEGIATAN, $this->getRequestParameter('kegiatan'));
            $c->add(MasterKegiatanPeer::UNIT_ID, $this->getRequestParameter('unit'));

            $master_kegiatan = MasterKegiatanPeer::doSelectOne($c);
            if ($master_kegiatan) {
                $this->master_kegiatan = $master_kegiatan;
            }
        }
        $this->setLayout('kosong');
    }
   public function executeEula() {
        //$this->unit_id = $this->getRequestParameter('unit_id');
        if ($this->getRequestParameter('act') == 'simpan') {
//print_r($this->getRequestParameter('unit_id').' '. $this->getRequestParameter('eula').' '. $this->getRequestParameter('eula_tolak'));exit;
            if ($this->getRequestParameter('eula_tolak') == 'Tidak Setuju') {
                return $this->redirect('login/logoutDinas');
            } else if ($this->getRequestParameter('eula') == 'Setuju') {
                return $this->redirect('bpk/list');
            }
        }
        $this->setLayout('layouteula');
    }
    public function executeEdit() {
        if ($this->getRequestParameter('unit_id')) {
            $unit_id = $this->getRequestParameter('unit_id');
            $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
            $this->tahap = $tahap = $this->getRequestParameter('tahap');

            if ($tahap == 'murni') {
                $c = new Criteria();
                $c->add(MurniSubtitleIndikatorPeer::UNIT_ID, $unit_id);
                $c->add(MurniSubtitleIndikatorPeer::KEGIATAN_CODE, $kode_kegiatan);
                $c->addAscendingOrderByColumn(MurniSubtitleIndikatorPeer::SUBTITLE);
                $rs_subtitle = MurniSubtitleIndikatorPeer::doSelect($c);
            } elseif ($tahap == 'murnibp') {
                $c = new Criteria();
                $c->add(MurniBukuPutihSubtitleIndikatorPeer::UNIT_ID, $unit_id);
                $c->add(MurniBukuPutihSubtitleIndikatorPeer::KEGIATAN_CODE, $kode_kegiatan);
                $c->addAscendingOrderByColumn(MurniBukuPutihSubtitleIndikatorPeer::SUBTITLE);
                $rs_subtitle = MurniBukuPutihSubtitleIndikatorPeer::doSelect($c);
            } elseif ($tahap == 'murnibb') {
                $c = new Criteria();
                $c->add(MurniBukuBiruSubtitleIndikatorPeer::UNIT_ID, $unit_id);
                $c->add(MurniBukuBiruSubtitleIndikatorPeer::KEGIATAN_CODE, $kode_kegiatan);
                $c->addAscendingOrderByColumn(MurniBukuBiruSubtitleIndikatorPeer::SUBTITLE);
                $rs_subtitle = MurniBukuBiruSubtitleIndikatorPeer::doSelect($c);
            } else {
                $c = new Criteria();
                $c->add(SubtitleIndikatorPeer::UNIT_ID, $unit_id);
                $c->add(SubtitleIndikatorPeer::KEGIATAN_CODE, $kode_kegiatan);
                $c->addAscendingOrderByColumn(SubtitleIndikatorPeer::SUBTITLE);
                $rs_subtitle = SubtitleIndikatorPeer::doSelect($c);
            }

            $this->rs_subtitle = $rs_subtitle;
            $this->rinciandetail = '';
            $this->rs_rinciandetail = '';

            if (sfConfig::get('app_fasilitas_warningRekening') == 'buka') {
                //untuk warning

                $query = "select sum(nilai_anggaran) as tot
					  from " . sfConfig::get('app_default_schema') . ".rincian_detail
					  where status_hapus=FALSE and unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rekening_code like '5.2.1%' and status_hapus=false";
                $con = Propel::getConnection();
                $statement = $con->prepareStatement($query);
                $rs = $statement->executeQuery();
                while ($rs->next())
                    $total_menjadi_1 = $rs->getString('tot');

                $query = "select sum(nilai_anggaran) as tot
					  from " . sfConfig::get('app_default_schema') . ".rincian_detail
					  where status_hapus=FALSE and unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rekening_code like '5.2.2%' and status_hapus=false";
                $con = Propel::getConnection();
                $statement = $con->prepareStatement($query);
                $rs = $statement->executeQuery();
                while ($rs->next())
                    $total_menjadi_2 = $rs->getString('tot');

                $query = "select sum(nilai_anggaran) as tot
					  from " . sfConfig::get('app_default_schema') . ".rincian_detail
					  where status_hapus=FALSE and unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rekening_code like '5.2.3%' and status_hapus=false";
                $con = Propel::getConnection();
                $statement = $con->prepareStatement($query);
                $rs = $statement->executeQuery();
                while ($rs->next())
                    $total_menjadi_3 = $rs->getString('tot');

                $query = "select sum(nilai_anggaran) as tot
                              from " . sfConfig::get('app_default_schema') . ".prev_rincian_detail
                              where status_hapus=FALSE and unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rekening_code like '5.2.1%' and status_hapus=false";

                $con = Propel::getConnection();
                $statement = $con->prepareStatement($query);
                $rs = $statement->executeQuery();
                while ($rs->next())
                    $total_semula_1 = $rs->getString('tot');

                $query = "select sum(nilai_anggaran) as tot
                              from " . sfConfig::get('app_default_schema') . ".prev_rincian_detail
                              where status_hapus=FALSE and unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rekening_code like '5.2.2%' and status_hapus=false";

                $con = Propel::getConnection();
                $statement = $con->prepareStatement($query);
                $rs = $statement->executeQuery();
                while ($rs->next())
                    $total_semula_2 = $rs->getString('tot');

                $query = "select sum(nilai_anggaran) as tot
                              from " . sfConfig::get('app_default_schema') . ".prev_rincian_detail
                              where status_hapus=FALSE and unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rekening_code like '5.2.3%' and status_hapus=false";

                $con = Propel::getConnection();
                $statement = $con->prepareStatement($query);
                $rs = $statement->executeQuery();
                while ($rs->next())
                    $total_semula_3 = $rs->getString('tot');


                $this->selisih_1 = number_format($total_menjadi_1 - $total_semula_1, 0, ',', '.');
                $this->selisih_2 = number_format($total_menjadi_2 - $total_semula_2, 0, ',', '.');
                $this->selisih_3 = number_format($total_menjadi_3 - $total_semula_3, 0, ',', '.');

                $this->total_menjadi_1 = number_format($total_menjadi_1, 0, ',', '.');
                $this->total_menjadi_2 = number_format($total_menjadi_2, 0, ',', '.');
                $this->total_menjadi_3 = number_format($total_menjadi_3, 0, ',', '.');

                $this->total_semula_1 = number_format($total_semula_1, 0, ',', '.');
                $this->total_semula_2 = number_format($total_semula_2, 0, ',', '.');
                $this->total_semula_3 = number_format($total_semula_3, 0, ',', '.');
            }
        }
    }

    public function executeGetPekerjaans() {
        if ($this->getRequestParameter('id')) {
            $sub_id = $this->getRequestParameter('id');
            $this->tahap = $tahap = $this->getRequestParameter('tahap');

            if ($tahap == 'murni') {
                $c = new Criteria();
                $c->add(MurniSubtitleIndikatorPeer::SUB_ID, $sub_id);
                $rs_subtitle = MurniSubtitleIndikatorPeer::doSelectOne($c);
                if ($rs_subtitle) {
                    $unit_id = $rs_subtitle->getUnitId();
                    $kegiatan_code = $rs_subtitle->getKegiatanCode();
                    $subtitle = $rs_subtitle->getSubtitle();
                    $nama_subtitle = trim($subtitle);
                }

                $query = "select *
                from " . sfConfig::get('app_default_schema') . ".murni_rincian_detail
                where unit_id = '$unit_id' and kegiatan_code = '$kegiatan_code' and subtitle ilike '$nama_subtitle' and status_hapus = false order by sub, rekening_code, komponen_name";

                $con = Propel::getConnection(MurniRincianDetailPeer::DATABASE_NAME);
                $statement = $con->prepareStatement($query);
                $rs_rinciandetail = $statement->executeQuery();
                $this->rs_rinciandetail = $rs_rinciandetail;

                $c_rincianDetail = new Criteria();
                $c_rincianDetail->add(MurniRincianDetailPeer::UNIT_ID, $unit_id);
                $c_rincianDetail->add(MurniRincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
                $c_rincianDetail->add(MurniRincianDetailPeer::SUBTITLE, $nama_subtitle, Criteria::ILIKE);
                $c_rincianDetail->add(MurniRincianDetailPeer::STATUS_HAPUS, false);
                $c_rincianDetail->add(MurniRincianDetailPeer::TAHUN, sfConfig::get('app_tahun_default'));
                $c_rincianDetail->addAscendingOrderByColumn(MurniRincianDetailPeer::KODE_SUB);
                $c_rincianDetail->addAscendingOrderByColumn(MurniRincianDetailPeer::REKENING_CODE);
                $c_rincianDetail->addDescendingOrderByColumn(MurniRincianDetailPeer::NILAI_ANGGARAN);
                $c_rincianDetail->addAscendingOrderByColumn(MurniRincianDetailPeer::LOKASI_KECAMATAN);
                $c_rincianDetail->addAscendingOrderByColumn(MurniRincianDetailPeer::LOKASI_KELURAHAN);
                $c_rincianDetail->addAscendingOrderByColumn(MurniRincianDetailPeer::KOMPONEN_NAME);
                $rs_rd = MurniRincianDetailPeer::doSelect($c_rincianDetail);
                $this->rs_rd = $rs_rd;

                $this->id = $sub_id;
                $this->rinciandetail = 'ada';
                $this->setLayout('kosong');
            } elseif ($tahap == 'murnibp') {
                $c = new Criteria();
                $c->add(MurniBukuPutihSubtitleIndikatorPeer::SUB_ID, $sub_id);
                $rs_subtitle = MurniBukuPutihSubtitleIndikatorPeer::doSelectOne($c);
                if ($rs_subtitle) {
                    $unit_id = $rs_subtitle->getUnitId();
                    $kegiatan_code = $rs_subtitle->getKegiatanCode();
                    $subtitle = $rs_subtitle->getSubtitle();
                    $nama_subtitle = trim($subtitle);
                }

                $query = "select *
                from " . sfConfig::get('app_default_schema') . ".murni_bukuputih_rincian_detail
                where unit_id = '$unit_id' and kegiatan_code = '$kegiatan_code' and subtitle ilike '$nama_subtitle' and status_hapus = false order by sub, rekening_code, komponen_name";

                $con = Propel::getConnection(MurniBukuPutihRincianDetailPeer::DATABASE_NAME);
                $statement = $con->prepareStatement($query);
                $rs_rinciandetail = $statement->executeQuery();
                $this->rs_rinciandetail = $rs_rinciandetail;

                $c_rincianDetail = new Criteria();
                $c_rincianDetail->add(MurniBukuPutihRincianDetailPeer::UNIT_ID, $unit_id);
                $c_rincianDetail->add(MurniBukuPutihRincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
                $c_rincianDetail->add(MurniBukuPutihRincianDetailPeer::SUBTITLE, $nama_subtitle, Criteria::ILIKE);
                $c_rincianDetail->add(MurniBukuPutihRincianDetailPeer::STATUS_HAPUS, false);
                $c_rincianDetail->add(MurniBukuPutihRincianDetailPeer::TAHUN, sfConfig::get('app_tahun_default'));
                $c_rincianDetail->addAscendingOrderByColumn(MurniBukuPutihRincianDetailPeer::KODE_SUB);
                $c_rincianDetail->addAscendingOrderByColumn(MurniBukuPutihRincianDetailPeer::REKENING_CODE);
                $c_rincianDetail->addDescendingOrderByColumn(MurniBukuPutihRincianDetailPeer::NILAI_ANGGARAN);
                $c_rincianDetail->addAscendingOrderByColumn(MurniBukuPutihRincianDetailPeer::LOKASI_KECAMATAN);
                $c_rincianDetail->addAscendingOrderByColumn(MurniBukuPutihRincianDetailPeer::LOKASI_KELURAHAN);
                $c_rincianDetail->addAscendingOrderByColumn(MurniBukuPutihRincianDetailPeer::KOMPONEN_NAME);
                $rs_rd = MurniBukuPutihRincianDetailPeer::doSelect($c_rincianDetail);
                $this->rs_rd = $rs_rd;

                $this->id = $sub_id;
                $this->rinciandetail = 'ada';
                $this->setLayout('kosong');
            } elseif ($tahap == 'murnibb') {
                $c = new Criteria();
                $c->add(MurniBukuBiruSubtitleIndikatorPeer::SUB_ID, $sub_id);
                $rs_subtitle = MurniBukuBiruSubtitleIndikatorPeer::doSelectOne($c);
                if ($rs_subtitle) {
                    $unit_id = $rs_subtitle->getUnitId();
                    $kegiatan_code = $rs_subtitle->getKegiatanCode();
                    $subtitle = $rs_subtitle->getSubtitle();
                    $nama_subtitle = trim($subtitle);
                }

                $query = "select *
                from " . sfConfig::get('app_default_schema') . ".murni_bukubiru_rincian_detail
                where unit_id = '$unit_id' and kegiatan_code = '$kegiatan_code' and subtitle ilike '$nama_subtitle' and status_hapus = false order by sub, rekening_code, komponen_name";

                $con = Propel::getConnection(MurniBukuBiruRincianDetailPeer::DATABASE_NAME);
                $statement = $con->prepareStatement($query);
                $rs_rinciandetail = $statement->executeQuery();
                $this->rs_rinciandetail = $rs_rinciandetail;

                $c_rincianDetail = new Criteria();
                $c_rincianDetail->add(MurniBukuBiruRincianDetailPeer::UNIT_ID, $unit_id);
                $c_rincianDetail->add(MurniBukuBiruRincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
                $c_rincianDetail->add(MurniBukuBiruRincianDetailPeer::SUBTITLE, $nama_subtitle, Criteria::ILIKE);
                $c_rincianDetail->add(MurniBukuBiruRincianDetailPeer::STATUS_HAPUS, false);
                $c_rincianDetail->add(MurniBukuBiruRincianDetailPeer::TAHUN, sfConfig::get('app_tahun_default'));
                $c_rincianDetail->addAscendingOrderByColumn(MurniBukuBiruRincianDetailPeer::KODE_SUB);
                $c_rincianDetail->addAscendingOrderByColumn(MurniBukuBiruRincianDetailPeer::REKENING_CODE);
                $c_rincianDetail->addDescendingOrderByColumn(MurniBukuBiruRincianDetailPeer::NILAI_ANGGARAN);
                $c_rincianDetail->addAscendingOrderByColumn(MurniBukuBiruRincianDetailPeer::LOKASI_KECAMATAN);
                $c_rincianDetail->addAscendingOrderByColumn(MurniBukuBiruRincianDetailPeer::LOKASI_KELURAHAN);
                $c_rincianDetail->addAscendingOrderByColumn(MurniBukuBiruRincianDetailPeer::KOMPONEN_NAME);
                $rs_rd = MurniBukuBiruRincianDetailPeer::doSelect($c_rincianDetail);
                $this->rs_rd = $rs_rd;

                $this->id = $sub_id;
                $this->rinciandetail = 'ada';
                $this->setLayout('kosong');
            } else {
                $c = new Criteria();
                $c->add(SubtitleIndikatorPeer::SUB_ID, $sub_id);
                $rs_subtitle = SubtitleIndikatorPeer::doSelectOne($c);
                if ($rs_subtitle) {
                    $unit_id = $rs_subtitle->getUnitId();
                    $kegiatan_code = $rs_subtitle->getKegiatanCode();
                    $subtitle = $rs_subtitle->getSubtitle();
                    $nama_subtitle = trim($subtitle);
                }

                $query = "select *
                from " . sfConfig::get('app_default_schema') . ".rincian_detail
                where unit_id = '$unit_id' and kegiatan_code = '$kegiatan_code' and subtitle ilike '$nama_subtitle' and status_hapus = false order by sub, rekening_code, komponen_name";

                $con = Propel::getConnection(RincianDetailPeer::DATABASE_NAME);
                $statement = $con->prepareStatement($query);
                $rs_rinciandetail = $statement->executeQuery();
                $this->rs_rinciandetail = $rs_rinciandetail;

                $c_rincianDetail = new Criteria();
                $c_rincianDetail->add(RincianDetailPeer::UNIT_ID, $unit_id);
                $c_rincianDetail->add(RincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
                $c_rincianDetail->add(RincianDetailPeer::SUBTITLE, $nama_subtitle, Criteria::ILIKE);
                $c_rincianDetail->add(RincianDetailPeer::STATUS_HAPUS, false);
                $c_rincianDetail->add(RincianDetailPeer::TAHUN, sfConfig::get('app_tahun_default'));
                $c_rincianDetail->addAscendingOrderByColumn(RincianDetailPeer::KODE_SUB);
                $c_rincianDetail->addAscendingOrderByColumn(RincianDetailPeer::REKENING_CODE);
                $c_rincianDetail->addDescendingOrderByColumn(RincianDetailPeer::NILAI_ANGGARAN);
                $c_rincianDetail->addAscendingOrderByColumn(RincianDetailPeer::LOKASI_KECAMATAN);
                $c_rincianDetail->addAscendingOrderByColumn(RincianDetailPeer::LOKASI_KELURAHAN);
                $c_rincianDetail->addAscendingOrderByColumn(RincianDetailPeer::KOMPONEN_NAME);
                $rs_rd = RincianDetailPeer::doSelect($c_rincianDetail);
                $this->rs_rd = $rs_rd;

                $this->id = $sub_id;
                $this->rinciandetail = 'ada';
                $this->setLayout('kosong');
            }
        }
    }

    public function executeList() {
        $this->processSort();
        $this->processFilters();
        $this->filters = $this->getUser()->getAttributeHolder()->getAll('sf_admin/master_kegiatan/filters');

        // pager
        $this->pager = new sfPropelPager('MasterKegiatan', 20);

        $c = new Criteria();
        $this->addSortCriteria($c);
        $nama = $this->getUser()->getNamaUser();
        if ($nama != '') {
            $e = new Criteria;
            $e->add(UserHandleV2Peer::USER_ID, $nama);
            $es = UserHandleV2Peer::doSelect($e);
            $this->satuan_kerja = $es;
            $unit_kerja = Array();
            foreach ($es as $x) {
                $unit_kerja[] = $x->getUnitId();
            }

            $c->add(MasterKegiatanPeer::UNIT_ID, $unit_kerja[0], Criteria::ILIKE);
            for ($i = 1; $i < count($unit_kerja); $i++) {
                $c->addOr(MasterKegiatanPeer::UNIT_ID, $unit_kerja[$i], Criteria::ILIKE);
            }
        }
        $unit_id = $this->getRequestParameter('unit_id');
        if (isset($unit_id)) {
            $c->add(MasterKegiatanPeer::UNIT_ID, $unit_id);
        }
        $c->addAscendingOrderByColumn(MasterKegiatanPeer::KODE_KEGIATAN);
        $this->addFiltersCriteria($c);
        $this->pager->setCriteria($c);
        $this->pager->setPage($this->getRequestParameter('page', 1));
        $this->pager->init();
    }

    protected function addFiltersCriteria($c) {
        if (isset($this->filters['kode_kegiatan_is_empty'])) {
            $criterion = $c->getNewCriterion(MasterKegiatanPeer::KODE_KEGIATAN, '');
            $criterion->addOr($c->getNewCriterion(MasterKegiatanPeer::KODE_KEGIATAN, null, Criteria::ISNULL));
            $c->add($criterion);
        } else if (isset($this->filters['kode_kegiatan']) && $this->filters['kode_kegiatan'] !== '') {
            $kode_kegiatan = $this->filters['kode_kegiatan'];
            $arr = explode('.', $kode_kegiatan);
            $jum_array = count($arr);
            //if($jum_array<2)
            //{
            $cton1 = $c->getNewCriterion(MasterKegiatanPeer::KODE_URUSAN, $kode_kegiatan . '%', Criteria::LIKE);
            $cton2 = $c->getNewCriterion(MasterKegiatanPeer::KODE_PROGRAM2, $kode_kegiatan . '%', Criteria::LIKE);
            $cton1->addOr($cton2);
            //$c->add($cton1);
            //}
            if ($jum_array == 2) {
                //$c->add(MasterKegiatanPeer::KODE_PROGRAM,$arr[1],Criteria::LIKE);
                $cton3 = $c->getNewCriterion(MasterKegiatanPeer::KODE_PROGRAM, $arr[1] . '%', Criteria::LIKE);
                $cton1->addAnd($cton3);
            }
            $c->add($cton1);
        }
        if (isset($this->filters['nama_kegiatan_is_empty'])) {
            $criterion = $c->getNewCriterion(MasterKegiatanPeer::NAMA_KEGIATAN, '');
            $criterion->addOr($c->getNewCriterion(MasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
            $c->add($criterion);
        } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
            $c->add(MasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
        }
        if (isset($this->filters['posisi_is_empty'])) {
            $criterion = $c->getNewCriterion(MasterKegiatanPeer::POSISI, '');
            $criterion->addOr($c->getNewCriterion(MasterKegiatanPeer::POSISI, null, Criteria::ISNULL));
            $c->add($criterion);
        } else if (isset($this->filters['posisi']) && $this->filters['posisi'] !== '') {
            //$query="select *
            //from ". sfConfig::get('app_default_schema') .".rincian r, ". sfConfig::get('app_default_schema') .".master_kegiatan mk
            //where r.rincian_level=".$this->filters['posisi']." and r.kegiatan_code=mk.kode_kegiatan";
            $crt1 = new Criteria();
            $crt1->add(RincianPeer::RINCIAN_LEVEL, $this->filters['posisi']);
            $rincian = RincianPeer::doSelect($crt1);
            foreach ($rincian as $r) {
                if ($r->getKegiatanCode() != '') {
                    $c->add(MasterKegiatanPeer::KODE_KEGIATAN, $r->getKegiatanCode());
                }
            }
        }
        if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
            $unit = $this->filters['unit_id'];
            $c->add(MasterKegiatanPeer::UNIT_ID, $unit);
        }
    }

}
