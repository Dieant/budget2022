<?php use_helper('Url', 'Javascript', 'Form', 'Object'); ?>
<?php
$status = $sf_user->getAttribute('status', '', 'status_peneliti');
$i = 0;
$kode_sub = '';
$temp_rekening = '';

if ($tahap == 'murni') {
    $tabel_dpn = 'murni_';
} elseif ($tahap == 'murnibp') {
    $tabel_dpn = 'murni_bukuputih_';
} elseif ($tahap == 'murnibb') {
    $tabel_dpn = 'murni_bukubiru_';
} else {
    $tabel_dpn = '';
}
?>

<?php
foreach ($rs_rd as $rd):
    $est_fisik = FALSE;
    $c = new Criteria();
    $c->add(KomponenPeer::KOMPONEN_ID, $rd->getKomponenId());
    $c->add(KomponenPeer::IS_EST_FISIK, TRUE);
    if ($rs_est_fisik = KomponenPeer::doSelectOne($c))
        $est_fisik = TRUE;

    $odd = fmod($i++, 2);
    $unit_id = $rd->getUnitId();
    $kegiatan_code = $rd->getKegiatanCode();

    if ($kode_sub != $rd->getKodeSub()) {
        $kode_sub = $rd->getKodeSub();
        $sub = $rd->getSub();
        $cekKodeSub = substr($kode_sub, 0, 4);

        if ($cekKodeSub == 'RKAM') {//RKA Member
            $C_RKA = new Criteria();
            $C_RKA->add(RkaMemberPeer::KODE_SUB, $kode_sub);
            $rs_rkam = RkaMemberPeer::doSelectOne($C_RKA);
//print_r($rs_rkam);exit;
            if ($rs_rkam) {
                ?>
                <tr class="pekerjaans_<?php echo $id ?>" bgcolor="#AFC4EF">
                    <td colspan="9"><?php
                        if (($status == 'OPEN') and ( $rd->getLockSubtitle() <> 'LOCK')) {
//                            echo link_to_function(image_tag('/sf/sf_admin/images/delete.png'), 'hapusSubKegiatan(' . $id . ',"' . $kegiatan_code . '","' . $unit_id . '","' . $kode_sub . '")');
                            echo link_to_function(image_tag('/sf/sf_admin/images/edit.png'), 'editHeaderKegiatan(' . $id . ',"' . $kegiatan_code . '","' . $unit_id . '","' . $kode_sub . '")');
                            echo link_to_function(image_tag('/sf/sf_admin/images/cancel.png'), 'hapusHeaderKegiatan(' . $id . ',"' . $kegiatan_code . '","' . $unit_id . '","' . $kode_sub . '")');
                        }
                        ?>
                        <div id="<?php echo 'header_' . $rs_rkam->getKodeSub() ?>"><b> .:. <?php echo $rs_rkam->getKomponenName() . ' ' . $rs_rkam->getDetailName(); ?></b></div>
                    <td align="right">
                        <?php
                        echo number_format($rd->getTotalKodeSub($kode_sub), 0, ',', '.');
                        ?></td>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <?php
            } else {
                
            }
        } else {

            $c = new Criteria();
            $c->add(RincianSubParameterPeer::KODE_SUB, $kode_sub);
            $rs_subparameter = RincianSubParameterPeer::doSelectOne($c);
            if ($rs_subparameter) {
                ?>
                <tr class="pekerjaans_<?php echo $id ?>" bgcolor="#AFC4EF">
                    <td colspan="9"><b>:. <?php echo $rs_subparameter->getSubKegiatanName() . ' ' . $rs_subparameter->getDetailName(); ?></b></td>
                    <td align="right">
                        <?php echo number_format($rd->getTotalKodeSub($kode_sub), 0, ',', '.'); ?>
                    </td>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <?php
            } else {
                $ada = 'tidak';
                $query = "select * from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "rincian_sub_parameter where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and new_subtitle ilike '%$sub%'";
//print_r($query);exit;
                $con = Propel::getConnection();
                $stmt = $con->prepareStatement($query);
                $t = $stmt->executeQuery();
                while ($t->next()) {
                    if ($t->getString('kode_sub')) {
                        ?>
                        <tr class="pekerjaans_<?php echo $id ?>" bgcolor="#AFC4EF">
                            <td colspan="9"><?php
                                if (($status == 'OPEN') and ( $rd->getLockSubtitle() <> 'LOCK')) {
//                                    echo link_to_function(image_tag('/sf/sf_admin/images/delete.png'), 'hapusSubKegiatan(' . $id . ',"' . $kegiatan_code . '","' . $unit_id . '","' . $t->getString('kode_sub') . '")');
                                }
                                ?> <b> :. <?php echo $t->getString('sub_kegiatan_name') . ' ' . $t->getString('detail_name'); ?></b></td>
                            <td align="right">
                                <?php
                                $query2 = "select sum(nilai_anggaran) as hasil_kali
								from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "rincian_detail
								where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and sub ilike '%$sub%' and status_hapus=false";

                                $con = Propel::getConnection();
                                $stmt = $con->prepareStatement($query2);
                                $t = $stmt->executeQuery();
                                while ($t->next()) {
                                    echo number_format($t->getString('hasil_kali'), 0, ',', '.');
                                }
                                $ada = 'ada';
                                ?> </td>
                            <td colspan="3">&nbsp;</td>
                        </tr>
                        <?php
                    }
                }

                if ($ada == 'tidak') {
                    if ($kode_sub != '') {
                        $query = "select * from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "rincian_detail
                                                where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and sub ilike '%$sub%' and status_hapus=false";
//print_r($query);exit;
                        $con = Propel::getConnection();
                        $stmt = $con->prepareStatement($query);
                        $t = $stmt->executeQuery();
                        while ($t->next()) {
                            if ($t->getString('kode_sub')) {
                                ?>
                                <tr class="pekerjaans_<?php echo $id ?>" bgcolor="#AFC4EF">
                                    <td colspan="9"><?php
                                        if (($status == 'OPEN') and ( $rd->getLockSubtitle() <> 'LOCK')) {
//                                            echo link_to_function(image_tag('/sf/sf_admin/images/delete.png'), 'hapusSubKegiatan(' . $id . ',"' . $kegiatan_code . '","' . $unit_id . '","' . $t->getString('kode_sub') . '")');
                                        }
                                        ?> <b> :. <?php echo $t->getString('komponen_name') . ' ' . $t->getString('detail_name'); ?></b></td>

                                    <td align="right">
                                        <?php
                                        $query2 = "select sum(nilai_anggaran) as hasil_kali "
                                                . "from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "rincian_detail "
                                                . "where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and sub ilike '%$sub%' and status_hapus=false";
                                        $con = Propel::getConnection();
                                        $stmt = $con->prepareStatement($query2);
                                        $t = $stmt->executeQuery();
                                        while ($t->next()) {
                                            echo number_format($t->getString('hasil_kali'), 0, ',', '.');
                                        }
                                        $ada = 'ada';
                                    }
                                    ?>
                                </td>
                                <td colspan="3">&nbsp;</td>
                            </tr>
                            <?php
                        }
                    }
                }
            }
        }
    } elseif (!$rd->getKodeSub()) {
        $kode_sub = '';
    }

    $rekening_code = $rd->getRekeningCode();
    if ($temp_rekening != $rekening_code) {
        $temp_rekening = $rekening_code;
        $c = new Criteria();
        $c->add(RekeningPeer::REKENING_CODE, $rekening_code);
        $rs_rekening = RekeningPeer::doSelectOne($c);
//$rs_rekening = RekeningPeer::retrieveByPk($rekening_code);
        if ($rs_rekening) {
            $rekening_name = $rs_rekening->getRekeningName();
            $subtitle_name = $rd->getSubtitle();
            $query_rekening = "select sum(nilai_anggaran) as jumlah_rekening from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "rincian_detail
                                where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and subtitle='$subtitle_name' and rekening_code='$rekening_code' and kode_sub='$kode_sub' and status_hapus=false";

            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query_rekening);
            $t = $stmt->executeQuery();
            while ($t->next()) {
                $jumlah_rekening = number_format($t->getString('jumlah_rekening'), 0, ',', '.');
                if ($t->getString('jumlah_rekening') == 0) {
                    $query_rekening = "select sum(nilai_anggaran) as jumlah_rekening from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "rincian_detail
                                            where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and subtitle='$subtitle_name' and rekening_code='$rekening_code' and kode_sub isnull and status_hapus=false";
                    $con = Propel::getConnection();
                    $stmt = $con->prepareStatement($query_rekening);
                    $ts = $stmt->executeQuery();
                    while ($ts->next()) {
                        $jumlah_rekening = number_format($ts->getString('jumlah_rekening'), 0, ',', '.');
                    }
                }
            }
            ?>
            <tr class="pekerjaans_<?php echo $id ?>" bgcolor="#AFC4EF">
                <td colspan="9"><i><?php echo $rekening_code . ' ' . $rekening_name; ?></i> </td>
                <td align="right"><?php echo $jumlah_rekening ?></td>
                <td colspan="3">&nbsp;</td>
            </tr>
            <?php
        }
    }

    if ($sf_user->getNamaUser() != 'parlemen2') {
        ?>
        <tr class="pekerjaans_<?php echo $id ?>">
            <td id="action_pekerjaans_<?php echo $rd->getDetailNo() ?>">
                <?php
                $kegiatan = $rd->getKegiatanCode();
                $unit = $rd->getUnitId();
                $no = $rd->getDetailNo();
                $sub = $rd->getSubtitle();
                $komponen_id = $rd->getKomponenId();

                //irul 6oktober 2014 - cek musrenbang
                $benar_musrenbang = 0;
                if ($rd->getIsMusrenbang() == 'TRUE') {
                    $benar_musrenbang = 1;
                }
                //irul 6oktober 2014 - cek musrenbang
                //irul 6oktober 2014 - cek multiyears
                $benar_multiyears = 0;
                if ($rd->getThKeMultiyears() <> null && $rd->getThKeMultiyears() > 0) {
                    $benar_multiyears = 1;
                }
                //irul 6oktober 2014 - cek multiyears

                $benar_hibah = 0;
                if ($rd->getIsHibah() == 'TRUE') {
                    $benar_hibah = 1;
                }

                if (($status == 'OPEN') && ( $rd->getLockSubtitle() <> 'LOCK')) {
                    $kode_sub = $rd->getKodeSub();
                    $cekKodeSub = substr($kode_sub, 0, 4);


                    if (sfConfig::get('app_fasilitas_gantiRekening4Peneliti') == 'buka') {
                        if ($cekKodeSub == 'RKAM') {
                            //RKA Member
                            //--list rekening diambil dari sub kegiatan yang sama
                            $query = "select distinct rekening_code "
                                    . "from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "rincian_detail "
                                    . "where kode_sub='$kode_sub' and status_hapus=false ";
                            //--list rekening diambil dari ssh itu punya rekening apa saja
                            $con = Propel::getConnection();
                            $stmt = $con->prepareStatement($query);
                            $ts = $stmt->executeQuery();
                            $pilih = array();
                            $ada = false;
                            $select_str = "<select name='rek_$no'>";
                            while ($ts->next()) {
                                $ada = true;
                                $r = $ts->getString('rekening_code');
                                $select_str.="<option value='$r'>$r</option>";
                            }
                            $select_str.="</select>";
                            ?> 
                            <div id="rekening_list_<?php echo $no ?>"> <?php
                                if ($ada) {
                                    ?>
                                    <form method="post" id="form_rekening_list_<?php echo $no ?>">
                                        <input type="hidden" name="id" value="<?php echo $id; ?>" />
                                        <input type="hidden" name="unit_id" value="<?php echo $unit_id; ?>" />
                                        <input type="hidden" name="kegiatan_code" value="<?php echo $kegiatan_code; ?>" />
                                        <input type="hidden" name="detail_no" value="<?php echo $no; ?>" />
                                        <?php echo $select_str; ?>
                                        <input class="saveGantiRekening" type="submit" value="Simpan"/>
                                    </form><br/>
                                    <?php
                                }
                                ?> 
                            </div>
                            <?php
                        } else {
                            //--list rekening diambil dari ssh itu punya rekening apa saja
                            $query = "select distinct rekening_code "
                                    . "from " . sfConfig::get('app_default_schema') . ".komponen_rekening "
                                    . "where komponen_id='$komponen_id' and rekening_code<>'$rekening_code'";
                            $con = Propel::getConnection();
                            $stmt = $con->prepareStatement($query);
                            $ts = $stmt->executeQuery();
                            $pilih = array();
                            $ada = false;
                            $select_str = "<select name='rek_$no'>";
                            while ($ts->next()) {
                                $ada = true;
                                $r = $ts->getString('rekening_code');
                                $select_str.="<option value='$r'>$r</option>";
                            }
                            $select_str.="</select>";
                            ?> 
                            <div id="rekening_list_<?php echo $no ?>"> 
                                <?php
                                if ($ada) {
                                    ?>
                                    <form method="post" id="form_rekening_list_<?php echo $no ?>">
                                        <input type="hidden" name="id" value="<?php echo $id; ?>" />
                                        <input type="hidden" name="unit_id" value="<?php echo $unit_id; ?>" />
                                        <input type="hidden" name="kegiatan_code" value="<?php echo $kegiatan_code; ?>" />
                                        <input type="hidden" name="detail_no" value="<?php echo $no; ?>" />
                                        <?php echo $select_str; ?>
                                        <input class="saveGantiRekening" type="submit" value="Simpan"/>
                                    </form><br/>
                                <?php }
                                ?>
                            </div>
                            <?php
                        }
                    }
                    if (sfConfig::get('app_fasilitas_gantiSubtitle4Peneliti') == 'buka' && 1 == 0) {
                        //ganti subtitle
                        $query = "select subtitle from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "subtitle_indikator where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' order by subtitle";
                        $con = Propel::getConnection();
                        $stmt = $con->prepareStatement($query);
                        $ts = $stmt->executeQuery();
                        //echo $query;
                        $pilih = array();
                        $aktif = false;
                        $select_str = "<select name='subtitle'>";
                        while ($ts->next()) {
                            $ada = true;
                            $r = $ts->getString('subtitle');
                            $select_str.="<option value='$r'>$r</option>";
                        }
                        $select_str.="</select>";
                        ?> 
                        <div id="subtitle_list"> 
                            <?php
                            if ($aktif == true) {
                                echo form_tag("bpk/gantiSubtitle?id=$id&unit_id=$unit_id&kegiatan_code=$kegiatan_code&detail_no=$no&subtitle=$subtitle");
                                echo $select_str;

                                echo submit_to_remote('gantiSubtitleHeader', 'Ganti', array(
                                    'update' => 'kegiatan_id',
                                    'url' => "bpk/gantiSubtitle?id=$id&unit_id=$unit_id&kegiatan_code=$kegiatan_code&detail_no=$no",
                                ));
                                //echo submit_tag('ganti');
                                echo "</form><br>";
                            }
                            ?> 
                        </div>
                        <?php
                    }
                    //eof ganti subtitle
                }
                if ($status == 'OPEN' && $rd->getLockSubtitle() <> 'LOCK') {
                    ?>
                    <div class="btn-group">
                        <?php
                        echo link_to_function('<i class="fa fa-file-text"></i>', '', array('class' => 'btn btn-default btn-flat btn-sm', 'disable' => true));
                        echo link_to('<i class="fa fa-pencil"></i> Edit Komponen', 'bpk/editKegiatan?id=' . $rd->getDetailNo() . '&unit=' . $rd->getUnitId() . '&kegiatan=' . $rd->getKegiatanCode() . '&edit=' . md5('ubah'), array('class' => 'btn btn-default btn-flat btn-sm'));
                        if (sfConfig::get('app_tahap_edit') == 'murni' && $benar_hibah == 0 && $benar_musrenbang == 0 && $benar_multiyears == 0 && substr($sf_user->getNamaLogin(), 0, 3) <> 'pa_') {
                            ?>
                            <button type="button" class="btn btn-default dropdown-toggle btn-flat btn-sm" data-toggle="dropdown">
                                <span class="caret"></span>
                                <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                                <li><?php
                                    echo link_to('<i class="fa fa-trash"></i> Hapus Komponen', 'bpk/hapusPekerjaans?id=' . $id . '&no=' . $rd->getDetailNo() . '&unit=' . $rd->getUnitId() . '&kegiatan=' . $rd->getKegiatanCode(), Array('confirm' => 'Yakin untuk menghapus Komponen ' . $rd->getKomponenName() . ' ' . $rd->getDetailName() . ' ?', 'class' => 'btn btn-default btn-flat btn-sm'));
                                    ?>
                                </li>
                            </ul>
                            <?php
                        }
                        ?>
                    </div>
                    <div class="clearfix"></div>
                    <?php
                }
                //irul 25agustus 2014 - fungsi GMAP
                if ($rd->getTipe() == 'FISIK' || $est_fisik) {
                    $id_kelompok = 0;
                    $tot = 0;
                    $con = Propel::getConnection();
                    $query = "select count(*) as tot "
                            . "from " . sfConfig::get('app_default_gis') . ".geojsonlokasi_rev1 "
                            . "where unit_id='" . $rd->getUnitId() . "' and kegiatan_code='" . $rd->getKegiatanCode() . "' and detail_no ='" . $rd->getDetailNo() . "' "
                            . "and tahun = '" . sfConfig::get('app_tahun_default') . "' and status_hapus = FALSE";
                    $stmt = $con->prepareStatement($query);
                    $rs = $stmt->executeQuery();
                    while ($rs->next()) {
                        $tot = $rs->getString('tot');
                    }
                    if ($tot == 0) {
                        $con = Propel::getConnection();
                        $c2 = new Criteria();
                        $crit1 = $c2->getNewCriterion(KomponenPeer::KOMPONEN_TIPE, 'FISIK');
                        $crit1->addOr($c2->getNewCriterion(KomponenPeer::KOMPONEN_TIPE, 'EST'));
                        $crit1->addOr($c2->getNewCriterion(KomponenPeer::KOMPONEN_TIPE2, 'KONSTRUKSI'));
                        $crit1->addOr($c2->getNewCriterion(KomponenPeer::KOMPONEN_TIPE2, 'PENGAWASAN'));
                        $crit1->addOr($c2->getNewCriterion(KomponenPeer::KOMPONEN_TIPE2, 'PERENCANAAN'));
                        $c2->add($c2->getNewCriterion(KomponenPeer::KOMPONEN_NAME, $rd->getKomponenName(), Criteria::ILIKE));
                        $c2->addAnd($crit1);
                        $rd2 = KomponenPeer::doSelectOne($c2);
                        if ($rd2) {
                            $komponen_id = $rd2->getKomponenId();
                            $satuan = $rd2->getSatuan();
                        } else {
                            $komponen_id = '0';
                            $satuan = '';
                        }

                        if ($komponen_id == '0') {
                            $query2 = "select * from master_kelompok_gmap where '" . $rd->getKomponenName() . "' ilike nama_objek||'%'";
                        } else {
                            $query2 = "select * from master_kelompok_gmap where '" . $komponen_id . "' ilike kode_kelompok||'%'";
                        }
                        $stmt2 = $con->prepareStatement($query2);
                        $rs2 = $stmt2->executeQuery();
                        while ($rs2->next()) {
                            $id_kelompok = $rs2->getString('id_kelompok');
                        }

                        if ($id_kelompok == '' || $id_kelompok == 0 || $id_kelompok == null) {
                            $id_kelompok = 19;
                            if (in_array($satuan, array('Kegiatan', 'Lokasi', 'M2', 'M²', 'm3', 'Paket', 'Set'))) {
                                $id_kelompok = 100;
                            } elseif (in_array($satuan, array('m', 'M', 'M1', 'Meter', 'Titik', 'Unit'))) {
                                $id_kelompok = 101;
                            }
                        }
                    } else {
                        $con = Propel::getConnection();
                        $query = "select * from " . sfConfig::get('app_default_gis') . ".geojsonlokasi_rev1 
                                    where unit_id='" . $rd->getUnitId() . "' and kegiatan_code='" . $rd->getKegiatanCode() . "' and detail_no ='" . $rd->getDetailNo() . "' and tahun = '" . sfConfig::get('app_tahun_default') . "'";
                        $stmt = $con->prepareStatement($query);
                        $rs = $stmt->executeQuery();
                        while ($rs->next()) {
                            $mlokasi = $rs->getString('mlokasi');
                            $id_kelompok = $rs->getString('id_kelompok');
                        }

                        $query = "select max(lokasi_ke) as total_lokasi from " . sfConfig::get('app_default_gis') . ".geojsonlokasi_rev1 
                                    where unit_id='" . $rd->getUnitId() . "' and kegiatan_code='" . $rd->getKegiatanCode() . "' and detail_no ='" . $rd->getDetailNo() . "' and tahun = '" . sfConfig::get('app_tahun_default') . "'";
                        $stmt = $con->prepareStatement($query);
                        $rs = $stmt->executeQuery();
                        while ($rs->next()) {
                            $total_lokasi = $rs->getString('total_lokasi');
                        }
                    }

                    if ($tot > 0) {
                        ?>
                        <div class="btn-group">                            
                            <?php
                            echo link_to_function('<i class="fa fa-map-marker"></i>', '', array('class' => 'btn btn-default btn-flat btn-sm', 'disable' => true));
                            echo link_to('<i class="fa fa-search"></i> View Lokasi', sfConfig::get('app_path_gmap') . 'viewData.php?unit_id=' . $rd->getUnitId() . '&kode_kegiatan=' . $rd->getKegiatanCode() . '&detail_no=' . $rd->getDetailNo() . '&satuan=' . $rd->getSatuan() . '&volume=' . $rd->getVolume() . '&nilai_anggaran=' . $rd->getNilaiAnggaran() . '&tahun=' . sfConfig::get('app_tahun_default') . '&mlokasi=' . $mlokasi . '&id_kelompok=' . $id_kelompok . '&th_load=0&level=1&nm_user=' . $sf_user->getNamaLogin() . '&total_lokasi=' . $total_lokasi . '&lokasi_ke=1', array('class' => 'btn btn-default btn-flat btn-sm'));
                            ?>                            
                        </div>
                        <?php
                    }
                }
                //irul 25agustus 2014 - fungsi GMAP
                ?>
            </td>
            <!--<td><?php echo $rd->getKomponenName() . ' ' . $rd->getDetailName(); ?>-->
            <!-- ///////////////////////////////////////warnain rows ///////////////////////////////////////////////////////////////////////////////// -->
            <?php
            //irul 29 jan 2014 - note peneliti + note skpd
            if ((($rd->getNotePeneliti() != '' and $rd->getNotePeneliti() != NULL) or ( $rd->getNoteSkpd() != '' and $rd->getNoteSkpd() != NULL)) and $rd->getStatusHapus() == false) {
                //irul 29 jan 2014 - note peneliti + note skpd
                ?>
                <td style="background: pink">                    
                    <?php
                    if ($benar_musrenbang == 1) {
                        echo '&nbsp;<span class="label label-success">Musrenbang</span>';
                    }
                    if ($benar_hibah == 1) {
                        echo '&nbsp;<span class="label label-success">Hibah</span>';
                    }
                    if ($rd->getKecamatan() <> '') {
                        echo '&nbsp;<span class="label label-warning">Jasmas</span>';
                    }
                    if ($benar_multiyears == 1) {
                        echo '&nbsp;<span class="label label-primary">Multiyears Tahun ke ' . $rd->getThKeMultiyears() . '</span>';
                    }
                    if ($rd->getIsBlud() == 1) {
                        echo '&nbsp;<span class="label label-info">BLUD</span>';
                    }
                    echo '<br/>';
                    echo $rd->getKomponenName();
                    if (sfConfig::get('app_fasilitas_keteranganKomponen') == 'buka') {
                        echo ' ' . $rd->getDetailName() . '<br/>';
                        if ($rd->getTipe() == 'FISIK' || $est_fisik) {
                            if ($rd->getLokasiKecamatan() <> '' && $rd->getLokasiKelurahan() <> '') {
                                echo '[' . $rd->getLokasiKelurahan() . ' - ' . $rd->getLokasiKecamatan() . ']';
                            }
                        }
                    }

//djiebrats : give image "new" when this kompone have status_masuk is new
                    $query2 = "select tahap from " . sfConfig::get('app_default_schema') . ".komponen "
                            . "where komponen_id='" . $rd->getKomponenId() . "'";
                    $con = Propel::getConnection();
                    $stmt = $con->prepareStatement($query2);
                    $t = $stmt->executeQuery();
                    while ($t->next()) {
                        if ($t->getString('tahap') == DinasMasterKegiatanPeer::getTahapKegiatan($rd->getUnitId(), $rd->getKegiatanCode())) {
                            echo image_tag('/images/newanima.gif');
                        }
                    }
//EO djiebrats give image new
//sisa lelang untuk fisik
                    ?>
                    <?php if ($sf_user->getNamaUser() != 'bpk') { ?>
                        <br/><br/>    
                        <div id="<?php echo 'tempat_ajax_' . $no ?>">
                            <?php
                            //$unit_id=array('0305','9999');
                            $kegiatan = $rd->getKegiatanCode();
                            $array_skpd_lock = array('9999');
                            if (in_array($unit_id, $array_skpd_lock) && sfConfig::get('app_tahun_default') != '2016') {
                                if ($rd->getStatusLelang() == 'unlock') {
                                    echo "<div class='btn-group'>";
                                    echo link_to_function('<i class="fa fa-money"></i>', '', array('class' => 'btn btn-success btn-flat btn-sm', 'disable' => true));
                                    if ($sf_user->getNamaUser() != 'inspect' && $sf_user->getNamaUser() != 'peninjau' && $sf_user->getNamaUser() != 'dppk' && $sf_user->getNamaUser() != 'bppk' && $sf_user->getNamaUser() != 'wawali' && $sf_user->getNamaUser() != 'walikota' && $sf_user->getNamaUser() != 'sekda' && $sf_user->getNamaUser() != 'asisten' && $sf_user->getNamaUser() != 'masger') {
                                        echo link_to_function('Lock Harga Dasar', 'execLockHargaDasar("lock","' . $id . '","' . $no . '","' . $unit_id . '","' . $kegiatan . '")', array('class' => 'btn btn-success btn-flat btn-sm'));
                                    }
                                    echo "</div>";
                                } else {
                                    echo "<div class='btn-group'>";
                                    if ($sf_user->getNamaUser() != 'inspect' && $sf_user->getNamaUser() != 'peninjau' && $sf_user->getNamaUser() != 'dppk' && $sf_user->getNamaUser() != 'bppk' && $sf_user->getNamaUser() != 'wawali' && $sf_user->getNamaUser() != 'walikota' && $sf_user->getNamaUser() != 'sekda' && $sf_user->getNamaUser() != 'asisten' && $sf_user->getNamaUser() != 'masger') {
                                        echo link_to_function('Unlock Harga Dasar', 'execLockHargaDasar("unlock","' . $id . '","' . $no . '","' . $unit_id . '","' . $kegiatan . '")', array('class' => 'btn btn-success btn-flat btn-sm'));
                                    }
                                    echo "</div>";
                                }
                            }
                            ?>
                        </div>
                        <div id="<?php echo 'tempat_ajax_lock_komponen_' . $no ?>">
                            <div class="btn-group">
                                <?php
                                //untuk membuat honorium DLL dan uang kinerja PNS
                                if ($rd->getRekeningCode() == '5.2.1.04.01' || $rd->getLockSubtitle() == 'LOCK') {
                                    echo link_to_function('<i class="fa fa-lock"></i>', '', array('class' => 'btn btn-success btn-flat btn-sm', 'disable' => true));
                                }
                                if ($sf_user->getNamaUser() != 'inspect' && $sf_user->getNamaUser() != 'peninjau' && $sf_user->getNamaUser() != 'bppk' && $sf_user->getNamaUser() != 'walikota') {
                                    if ($rd->getRekeningCode() <> '5.2.1.04.01') {
                                        if ($rd->getLockSubtitle() == 'LOCK') {
                                            echo link_to_function('Buka Komponen', 'execLockKomponen(1,"' . $id . '","' . $no . '","' . $unit_id . '","' . $kegiatan . '")', array('class' => 'btn btn-success btn-flat btn-sm'));
                                        } else {
                                            echo link_to_function('Kunci Komponen', 'execLockKomponen(0,"' . $id . '","' . $no . '","' . $unit_id . '","' . $kegiatan . '")', array('class' => 'btn btn-danger btn-flat btn-sm'));
                                        }
                                    }
                                }
                                ?>
                            </div>
                        </div>
                        <?php
                    }
                    // irul 7 feb 2014 lock unlock komponen 
                    ?>
                </td>
                <td style="background: pink" align="center">
                    <?php echo $rd->getTipe(); ?>
                </td>
                <td style="background: pink" align="center">
                    <?php echo $rd->getSatuan(); ?>
                </td>
                <td style="background: pink" align="center">
                    <?php echo $rd->getKeteranganKoefisien(); ?>
                </td>
                <td style="background: pink" align="right">
                    <?php
                    if ($rd->getSatuan() == '%') {
                        echo number_format($rd->getKomponenHargaAwal(), 4, ',', '.');
                    } elseif ($rd->getKomponenHargaAwal() != floor($rd->getKomponenHargaAwal())) {
                        echo number_format($rd->getKomponenHargaAwal(), 2, ',', '.');
                    } else {
                        echo number_format($rd->getKomponenHargaAwal(), 0, ',', '.');
                    }
                    ?>
                </td>
                <td style="background: pink" align="right"><?php
                    $volume = $rd->getVolume();
                    $harga = $rd->getKomponenHargaAwal();
                    $hasil = $volume * $harga;
                    echo number_format($hasil, 0, ',', '.');
                    ?></td>
                <td style="background: pink" align="right">
                    <?php echo $rd->getPajak() . '%'; ?>
                </td>
                <td style="background: pink" align="right">
                    <?php echo number_format($rd->getNilaiAnggaran(), 0, '$total,', '.'); ?>
                </td>
                <td style="background: pink" align="center">
                    <?php
                    $rekening = $rd->getRekeningCode();
                    $rekening_code = substr($rekening, 0, 5);
                    $c = new Criteria();
                    $c->add(KelompokBelanjaPeer::BELANJA_CODE, $rekening_code);
                    $rs_rekening = KelompokBelanjaPeer::doSelectOne($c);
                    if ($rs_rekening) {
                        echo $rs_rekening->getBelanjaName();
                    }
                    ?></td>
                <td style="background: pink">                              
                    <!-- irul 29 jan 2014 show note SKPD + note peneliti -->
                    <?php
                    echo 'Catatan Peneliti:<br/>';
                    echo textarea_tag('peneliti', $rd->getNotePeneliti(), 'readonly=readonly');
                    echo '<br/>Catatan SKPD:<br/>';
                    echo textarea_tag('skpd', $rd->getNoteSkpd(), 'readonly=readonly');
                    ?>
                    <!-- irul 29 jan 2014 show note SKPD + note peneliti -->                    
                </td>
                <?php
            } //////////////////////////////////// end of warna     ////////////////////////////////////////////////////////
            else {
                ?>
                <td>
                    <?php
                    if ($benar_musrenbang == 1) {
                        echo '&nbsp;<span class="label label-success">Musrenbang</span>';
                    }
                    if ($benar_hibah == 1) {
                        echo '&nbsp;<span class="label label-success">Hibah</span>';
                    }
                    if ($rd->getKecamatan() <> '') {
                        echo '&nbsp;<span class="label label-warning">Jasmas</span>';
                    }
                    if ($benar_multiyears == 1) {
                        echo '&nbsp;<span class="label label-primary">Multiyears Tahun ke ' . $rd->getThKeMultiyears() . '</span>';
                    }
                    if ($rd->getIsBlud() == 1) {
                        echo '&nbsp;<span class="label label-info">BLUD</span>';
                    }
                    echo '<br/>';
                    echo $rd->getKomponenName();
                    if (sfConfig::get('app_fasilitas_keteranganKomponen') == 'buka') {
                        echo ' ' . $rd->getDetailName() . '<br/>';
                        if ($rd->getTipe() == 'FISIK' || $est_fisik) {
                            if ($rd->getLokasiKecamatan() <> '' && $rd->getLokasiKelurahan() <> '') {
                                echo '[' . $rd->getLokasiKelurahan() . ' - ' . $rd->getLokasiKecamatan() . ']';
                            }
                        }
                    }

//djiebrats : give image "new" when this kompone have status_masuk is new
                    $query2 = "select tahap from " . sfConfig::get('app_default_schema') . ".komponen "
                            . "where komponen_id='" . $rd->getKomponenId() . "'";
                    $con = Propel::getConnection();
                    $stmt = $con->prepareStatement($query2);
                    $t = $stmt->executeQuery();
                    while ($t->next()) {
                        if ($t->getString('tahap') == DinasMasterKegiatanPeer::getTahapKegiatan($rd->getUnitId(), $rd->getKegiatanCode())) {
                            echo image_tag('/images/newanima.gif');
                        }
                    }
//EO djiebrats give image new
//sisa lelang untuk fisik
                    ?>
                    <?php if ($sf_user->getNamaUser() != 'bpk') { ?>
                        <br/><br/>    
                        <div id="<?php echo 'tempat_ajax_' . $no ?>">
                            <?php
                            // $unit_id=array ('9999','2600','2300','0309','0305');
                            // $kegiatan = $rd->getKegiatanCode();
                            $array_skpd_lock = array('9999');
                            // //if($unit_id=='0305' and $subtitle_name=='Pemeliharaan Rutin/Berkala Peralatan Gedung Kantor' and $kegiatan_code=='1.20.02.0030' and $komponen_id =='23.02.05.08.05.01'){
                            if (in_array($unit_id, $array_skpd_lock) && sfConfig::get('app_tahun_default') != '2016') {
                                if ($rd->getStatusLelang() == 'unlock') {
                                    echo "<div class='btn-group'>";
                                    echo link_to_function('<i class="fa fa-money"></i>', '', array('class' => 'btn btn-success btn-flat btn-sm', 'disable' => true));
                                    if ($sf_user->getNamaUser() != 'inspect' && $sf_user->getNamaUser() != 'peninjau' && $sf_user->getNamaUser() != 'dppk' && $sf_user->getNamaUser() != 'bppk' && $sf_user->getNamaUser() != 'wawali' && $sf_user->getNamaUser() != 'walikota' && $sf_user->getNamaUser() != 'sekda' && $sf_user->getNamaUser() != 'asisten' && $sf_user->getNamaUser() != 'masger') {
                                        echo link_to_function('Lock Harga Dasar', 'execLockHargaDasar("lock","' . $id . '","' . $no . '","' . $unit_id . '","' . $kegiatan . '")', array('class' => 'btn btn-success btn-flat btn-sm'));
                                    }
                                    echo "</div>";
                                } else {
                                    echo "<div class='btn-group'>";
                                    if ($sf_user->getNamaUser() != 'inspect' && $sf_user->getNamaUser() != 'peninjau' && $sf_user->getNamaUser() != 'dppk' && $sf_user->getNamaUser() != 'bppk' && $sf_user->getNamaUser() != 'wawali' && $sf_user->getNamaUser() != 'walikota' && $sf_user->getNamaUser() != 'sekda' && $sf_user->getNamaUser() != 'asisten' && $sf_user->getNamaUser() != 'masger') {
                                        echo link_to_function('Unlock Harga Dasar', 'execLockHargaDasar("unlock","' . $id . '","' . $no . '","' . $unit_id . '","' . $kegiatan . '")', array('class' => 'btn btn-success btn-flat btn-sm'));
                                    }
                                    echo "</div>";
                                }
                            }
                            ?>
                        </div>
                        <div id="<?php echo 'tempat_ajax_lock_komponen_' . $no ?>">
                            <div class="btn-group">
                                <?php
                                //untuk membuat honorium DLL dan uang kinerja PNS
                                if ($rd->getRekeningCode() == '5.2.1.04.01' && $rd->getLockSubtitle() == 'LOCK') {
                                    echo link_to_function('<i class="fa fa-lock"></i>', '', array('class' => 'btn btn-success btn-flat btn-sm', 'disable' => true));
                                }
                                if ($sf_user->getNamaUser() != 'inspect' && $sf_user->getNamaUser() != 'peninjau' && $sf_user->getNamaUser() != 'bppk' && $sf_user->getNamaUser() != 'walikota') {
                                    if ($rd->getRekeningCode() <> '5.2.1.04.01') {
                                        if ($rd->getLockSubtitle() == 'LOCK') {
                                            echo link_to_function('Buka Komponen', 'execLockKomponen(1,"' . $id . '","' . $no . '","' . $unit_id . '","' . $kegiatan . '")', array('class' => 'btn btn-success btn-flat btn-sm'));
                                        } else {
                                            echo link_to_function('Kunci Komponen', 'execLockKomponen(0,"' . $id . '","' . $no . '","' . $unit_id . '","' . $kegiatan . '")', array('class' => 'btn btn-danger btn-flat btn-sm'));
                                        }
                                    } else if ($rd->getRekeningCode() == '5.2.1.04.01' && (($unit_id == '2600' && ($kegiatan == '1.03.01.0001' || $kegiatan == '1.09.20.0003' || $kegiatan == '1.03.28.0005')) || ($unit_id == '0303' && $kegiatan == '1.20.28.0001'))) {
                                        if ($rd->getLockSubtitle() == 'LOCK') {
                                            echo link_to_function('Buka Komponen', 'execLockKomponen(1,"' . $id . '","' . $no . '","' . $unit_id . '","' . $kegiatan . '")', array('class' => 'btn btn-success btn-flat btn-sm'));
                                        } else {
                                            echo link_to_function('Kunci Komponen', 'execLockKomponen(0,"' . $id . '","' . $no . '","' . $unit_id . '","' . $kegiatan . '")', array('class' => 'btn btn-danger btn-flat btn-sm'));
                                        }
                                    }
                                }
                                ?>
                            </div>
                        </div>
                    <?php } ?>
                </td>
                <td align="center"> 
                    <?php echo $rd->getTipe(); ?>
                </td>
                <td align="center">
                    <?php echo $rd->getSatuan(); ?>
                </td>
                <td align="center">
                    <?php echo $rd->getKeteranganKoefisien(); ?>
                </td>
                <td align="right">
                    <?php
                    if ($rd->getSatuan() == '%') {
                        echo number_format($rd->getKomponenHargaAwal(), 4, ',', '.');
                    } elseif ($rd->getKomponenHargaAwal() != floor($rd->getKomponenHargaAwal())) {
                        echo number_format($rd->getKomponenHargaAwal(), 2, ',', '.');
                    } else {
                        echo number_format($rd->getKomponenHargaAwal(), 0, ',', '.');
                    }
                    ?>
                </td>
                <td align="right"><?php
                    $volume = $rd->getVolume();
                    $harga = $rd->getKomponenHargaAwal();
                    $hasil = $volume * $harga;
                    echo number_format($hasil, 0, ',', '.');
                    ?></td>
                <td align="right">
                    <?php echo $rd->getPajak() . '%'; ?>
                </td>
                <td align="right">
                    <?php echo number_format($rd->getNilaiAnggaran(), 0, ',', '.'); ?>
                </td>
                <td align="center">
                    <?php
                    $rekening = $rd->getRekeningCode();
                    $rekening_code = substr($rekening, 0, 5);
                    $c = new Criteria();
                    $c->add(KelompokBelanjaPeer::BELANJA_CODE, $rekening_code);
                    $rs_rekening = KelompokBelanjaPeer::doSelectOne($c);
                    if ($rs_rekening) {
                        echo $rs_rekening->getBelanjaName();
                    }
                    ?></td>                        
                <td>        
                    <!-- irul 29 jan 2014 show note SKPD + note peneliti -->
                    <?php
                    echo 'Catatan Peneliti:<br/>';
                    echo textarea_tag('peneliti', $rd->getNotePeneliti(), 'readonly=readonly');
                    echo '<br/>Catatan SKPD:<br/>';
                    echo textarea_tag('skpd', $rd->getNoteSkpd(), 'readonly=readonly');
                    ?>
                    <!-- irul 29 jan 2014 show note SKPD + note peneliti -->
                </td>
            <?php } ?>
        </tr>
        <?php
    }
endforeach;
?>

<script>
    $(".saveGantiRekening").click(function () { // changed
        var id_form = $(this).closest("form").attr("id"); //parent form
        var detailno = id_form.split("_").pop();
        $.ajax({
            type: "POST",
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/bpk/gantiRekening.html",
            data: $(this).parent().serialize(), // changed
            success: function () {
                $('#action_pekerjaans_' + detailno).html('<br/><font style="color: green;font-weight:bold">{Silahkan Refresh Halaman}</font><br/><br/>');
            }
        });
        return false; // avoid to execute the actual submit of the form.
    });

    function execLockHargaDasar(act, id, detNo, unitId, kegCode) {
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/bpk/lockhargadasar/act/" + act + "/id/" + id + "/detail_no/" + detNo + "/unit_id/" + unitId + "/kegiatan_code/" + kegCode + ".html",
            context: document.body
        }).done(function (msg) {
            $('#tempat_ajax_' + detNo).html(msg);
        });
    }

    function execLockKomponen(nilai_gembok, id, detNo, unitId, kegCode) {
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/bpk/lockkomponen/id/" + id + "/unit_id/" + unitId + "/kegiatan_code/" + kegCode + "/detail_no/" + detNo + "/nilai_gembok/" + nilai_gembok + ".html",
            context: document.body
        }).done(function (msg) {
            if (nilai_gembok === 0) {
                $('#tempat_ajax_lock_komponen_' + detNo).html('<br/><font style="color: green;font-weight:bold">{Berhasil dikunci untuk Komponen ini}</font><br/><br/>');
            } else {
                $('#tempat_ajax_lock_komponen_' + detNo).html('<br/><font style="color: green;font-weight:bold">{Berhasil dibuka untuk Komponen ini}</font><br/><br/>');
            }
            $('#action_pekerjaans_' + detNo).html('<br/><font style="color: green;font-weight:bold">{Silahkan Refresh Halaman}</font><br/><br/>');
        });
    }
</script>
