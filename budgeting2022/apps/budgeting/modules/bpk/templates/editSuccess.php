<?php use_helper('I18N', 'Date', 'Url', 'Javascript', 'Form', 'Object'); ?>
<?php
if ($tahap == 'murni') {
    $tabel_dpn = 'murni_';
} elseif ($tahap == 'murnibp') {
    $tabel_dpn = 'murni_bukuputih_';
} elseif ($tahap == 'murnibb') {
    $tabel_dpn = 'murni_bukubiru_';
} else {
    $tabel_dpn = '';
}

$kode_kegiatan = $sf_params->get('kode_kegiatan');
$unit_id = $sf_params->get('unit_id');
$sub_id = 1;

$kegiatan_code = $sf_params->get('kode_kegiatan');
$status = 'LOCK';
$c = new Criteria();
$c->add(RincianPeer::KEGIATAN_CODE, $kegiatan_code);
$c->add(RincianPeer::UNIT_ID, $unit_id);
$rs_rinciankegiatan = RincianPeer::doSelectOne($c);
$v = RincianPeer::doSelectOne($c);
if ($v) {
    if ($v->getRincianLevel() == 1) {
        $status = 'LOCK';
    } else if ($v->getRincianLevel() == 2) {
        $status = 'LOCK';
    } else if ($v->getRincianLevel() == 3) {
        $status = 'OPEN';
    }

    if ($v->getLock() == TRUE) {
        $status = 'LOCK';
    }
}

if ($sf_user->getNamaUser() == 'inspect' || $sf_user->getNamaUser() == 'peninjau' || $sf_user->getNamaUser() == 'dppk' || $sf_user->getNamaUser() == 'bppk') {
    $status = 'LOCK';
}

$sf_user->removeCredential('status_peneliti');
$sf_user->addCredential('status_peneliti');
$sf_user->setAttribute('status', $status, 'status_peneliti');
?>

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>Daftar Subtitle Kegiatan SKPD <?php echo $kode_kegiatan; ?></h1>
</section>

<!-- Main content -->
<section class="content">
    <?php include_partial('bpk/list_messages'); ?>
    <!-- Default box -->
    <div class="box box-info">        
        <div class="box-body text-center" id="headerrka_<?php echo $sub_id ?>">
            <div class="btn-group btn-group-justified" role="group">
                <?php echo link_to_function('<span>' . image_tag('down.png', array('name' => 'img_' . $sub_id, 'id' => 'img_' . $sub_id, 'border' => 0)) . '</span> Melihat Header Kegiatan', 'inKeterangan(' . $sub_id . ',"' . $kode_kegiatan . '","' . $unit_id . '")', array('class' => 'btn btn-flat btn-default text-bold col-xs-4')); ?>    
                <?php
                if ($tahap == 'murni') {
                    echo link_to(image_tag('b_print.png') . ' Format RKA Berdasarkan Subtitle', 'report/TemplateRKAMurni?kode_kegiatan=' . $kode_kegiatan . '&unit_id=' . $unit_id, array('class' => 'btn btn-flat btn-default text-bold col-xs-4', 'target' => '_blank'));
                    echo link_to(image_tag('b_print.png') . ' Format RKA Berdasarkan Rekening', 'report/printrekeningMurni?kode_kegiatan=' . $kode_kegiatan . '&unit_id=' . $unit_id, array('class' => 'btn btn-flat btn-default text-bold col-xs-4', 'target' => '_blank'));
                } elseif ($tahap == 'murnibp') {
                    echo link_to(image_tag('b_print.png') . ' Format RKA Berdasarkan Subtitle', 'report/TemplateRKAMurniBukuPutih?kode_kegiatan=' . $kode_kegiatan . '&unit_id=' . $unit_id, array('class' => 'btn btn-flat btn-default text-bold col-xs-4', 'target' => '_blank'));
                    echo link_to(image_tag('b_print.png') . ' Format RKA Berdasarkan Rekening', 'report/printrekeningMurniBukuPutih?kode_kegiatan=' . $kode_kegiatan . '&unit_id=' . $unit_id, array('class' => 'btn btn-flat btn-default text-bold col-xs-4', 'target' => '_blank'));
                } elseif ($tahap == 'murnibb') {
                    echo link_to(image_tag('b_print.png') . ' Format RKA Berdasarkan Subtitle', 'report/TemplateRKAMurniBukuBiru?kode_kegiatan=' . $kode_kegiatan . '&unit_id=' . $unit_id, array('class' => 'btn btn-flat btn-default text-bold col-xs-4', 'target' => '_blank'));
                    echo link_to(image_tag('b_print.png') . ' Format RKA Berdasarkan Rekening', 'report/printrekeningMurniBukuBiru?kode_kegiatan=' . $kode_kegiatan . '&unit_id=' . $unit_id, array('class' => 'btn btn-flat btn-default text-bold col-xs-4', 'target' => '_blank'));
                } else {
                    echo link_to(image_tag('b_print.png') . ' Format RKA Berdasarkan Subtitle', 'report/TemplateRKA?kode_kegiatan=' . $kode_kegiatan . '&unit_id=' . $unit_id, array('class' => 'btn btn-flat btn-default text-bold col-xs-4', 'target' => '_blank'));
                    echo link_to(image_tag('b_print.png') . ' Format RKA Berdasarkan Rekening', 'report/printrekening?kode_kegiatan=' . $kode_kegiatan . '&unit_id=' . $unit_id, array('class' => 'btn btn-flat btn-default text-bold col-xs-4', 'target' => '_blank'));
                }
                ?>
            </div>
            <div class="clearfix"></div>
        </div><!-- /.box-body -->            
        <div class="box-body"id="indicator" style="display:none;" align="center">
            <dt>&nbsp;</dt><dd><b>Mohon Tunggu </b><?php echo image_tag('loading.gif', array('align' => 'absmiddle')) ?></dd>
        </div>
    </div><!-- /.box -->
    <div class="box box-info">                    
        <div id="sf_admin_container">
            <div id="sf_admin_content" class="table-responsive">

                <?php if (!$rs_subtitle): ?>
                    <?php echo __('Tidak Ada Subtitle untuk SKPD dengan Kegiatan ini') ?>
                <?php else: ?>
                    <table cellspacing="0" class="sf_admin_list">
                        <thead>
                            <tr>
                                <th><b>Subtitle | Rekening</b></th>
                                <th><b>Komponen</b></th>
                                <th><b>Type</b></th>
                                <th><b>Satuan</b></th>
                                <th><b>Koefisien</b></th>
                                <th><b>Harga</b></th>
                                <th><b>Hasil</b></th>
                                <th><b>PPN</b></th>
                                <th><b>Total</b></th>
                                <th><b>Belanja</b></th>
                                <th><b>Catatan Tim Anggaran</b></th>
                        <!--        <th><b>Catatan SKPD</b></th>-->
                            </tr>
                        </thead>

                        <tbody>
                            <?php
                            $i = 1;
                            foreach ($rs_subtitle as $subtitle_indikator):
                                ?>
                                <?php
                                $unit_id = $subtitle_indikator->getUnitId();
                                $kode_kegiatan = $subtitle_indikator->getKegiatanCode();
                                $subtitle = $subtitle_indikator->getSubtitle();
                                $sub_id = $subtitle_indikator->getSubId();
                                $nama_subtitle = trim($subtitle);
                                $odd = fmod($i++, 2);

                                //////////////////////////////////////////////////////////////////////////////////
                                //* untuk memberi warning jika komponen ada catatan penyelia dan SKPD
                                $query = "SELECT * from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "rincian_detail 
                                        where ((note_peneliti is not Null and note_peneliti <> '') or (note_skpd is not Null and note_skpd <> '')) and unit_id = '$unit_id' and kegiatan_code = '$kode_kegiatan' and subtitle ilike '%" . str_replace('\'', '%', $nama_subtitle) . "%' and status_hapus = false ";
                                //print_r($query);exit;
                                //diambil nilai terpakai
                                $con = Propel::getConnection();
                                $statement = $con->prepareStatement($query);
                                $rs = $statement->executeQuery();
                                $jml = $rs->getRecordCount();
                                ////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                if ($jml > 0) {
                                    ?>
                                    <tr style="background: pink" class="sf_admin_row_<?php echo $odd ?>" id="subtitle_<?php print $sub_id ?>">
                                        <td style="background: pink" colspan="9">
                                            <b>
                                                <i>
                                                    <?php echo link_to_function(image_tag('b_plus.png', array('name' => 'img_' . $sub_id, 'id' => 'img_' . $sub_id, 'border' => 0)), 'showHideKegiatan(' . $sub_id . ')') . ' ' . $subtitle_indikator->getSubtitle() ?>
                                                </i>
                                            </b>
                                            <?php
                                            if ($status == 'OPEN') {
                                                //	echo link_to_remote(image_tag('/sf/sf_admin/images/cancel.png'), array('update'=>'subtitle_'.$sub_id,'url'=>'bpk/nolSubtitle?kegiatan='.$kode_kegiatan.'&unit='.$unit_id.'&idsub='.$sub_id, 'confirm'=>'Apakah anda yakin untuk me-nol kan Subtitle ini?'));
                                            }
                                            //djieb begin
                                            if ($sf_user->getNamaUser() != 'inspect' && $sf_user->getNamaUser() != 'peninjau' && $sf_user->getNamaUser() != 'dppk' && $sf_user->getNamaUser() != 'bppk' && $sf_user->getNamaUser() != 'wawali' && $sf_user->getNamaUser() != 'walikota' && $sf_user->getNamaUser() != 'sekda' && $sf_user->getNamaUser() != 'asisten' && $sf_user->getNamaUser() != 'parlemen2') {
                                                //echo link_to_remote(image_tag('/images/edit_icon2.png'),array('alt'=>'Mengubah Subtitle','title' => 'Mengubah Nilai Koefisien','update'=>'ubahSubtitle_'.$sub_id, 'url'=>'bpk/saran?act=edit&subLama='.$nama_subtitle.'&unit_id='.$unit_id.'&kegiatan_code='.$kode_kegiatan.'&id='.$sub_id,'loading'=>"Element.show('indikator')",'complete'=>"Element.hide('indikator')") );
                                                echo '&nbsp';
                                                //echo link_to_remote(image_tag('/images/personil.gif'),array('alt'=>'Menambah Personil','title' => 'menambah personil','update'=>'tambahPersonil_'.$sub_id, 'url'=>'bpk/personil?act=tambah&subtitle='.$subtitle_indikator->getSubtitle().'&unit_id='.$unit_id.'&kegiatan_code='.$kode_kegiatan,'loading'=>"Element.show('indikator')",'complete'=>"Element.hide('indikator')") );
                                            }
                                            //  echo $unit_id.' '.$kode_kegiatan.' '.$subtitle_indikator->getSubtitle();
                                            //djiebrats: kunci subtitle STATUS: CLOSE
                                            //echo $unit_id.' '.$kode_kegiatan.' '.$subtitle_indikator->getSubtitle().' tes'.$subtitle_indikator->getLockSubtitle();
//                                    if ($subtitle_indikator->getLockSubtitle() == TRUE) {
//
//                                        //$sf_user->getNamaUser()!='peninjau' &&
//                                        if ($sf_user->getNamaUser() != 'peninjau' && $sf_user->getNamaUser() != 'dppk' && $sf_user->getNamaUser() != 'bppk' && $sf_user->getNamaUser() != 'wawali' && $sf_user->getNamaUser() != 'walikota' && $sf_user->getNamaUser() != 'sekda' && $sf_user->getNamaUser() != 'asisten' && $sf_user->getNamaUser() != 'parlemen2') {
//                                            $subtitleReplace = str_replace('/', '=', $subtitle);
//                                            echo ' ' . link_to(image_tag('/sf/sf_admin/images/unlock.png', array('alt' => __('Buka Kunci Subtitle'), 'title' => __('Buka Kunci Subtitle'))), 'bpk/bukaSubtitle?unit_id=' . $unit_id . '&kegiatan_code=' . $kode_kegiatan . '&subtitle=' . $subtitleReplace . '&sub_id=' . $sub_id);
//                                        }
//                                    } else {  //if($subtitle_indikator->getLockSubtitle()== FALSE)
//                                        if ($sf_user->getNamaUser() != 'peninjau' && $sf_user->getNamaUser() != 'dppk' && $sf_user->getNamaUser() != 'bppk' && $sf_user->getNamaUser() != 'wawali' && $sf_user->getNamaUser() != 'walikota' && $sf_user->getNamaUser() != 'sekda' && $sf_user->getNamaUser() != 'asisten' && $sf_user->getNamaUser() != 'parlemen2') {
//                                            $subtitleReplace = str_replace('/', '=', $subtitle);
//                                            //echo image_tag('/images/digisign.png',array('alt' => __('Subtitle belum ditanda tangan digital'), 'title' => __('Subtitle belum ditanda tangan digital')));
//                                            echo ' ' . link_to(image_tag('/sf/sf_admin/images/lock.png', array('alt' => __('Kunci Subtitle'), 'title' => __('Kunci Subtitle'))), 'bpk/kunciSubtitle?unit_id=' . $unit_id . '&kegiatan_code=' . $kode_kegiatan . '&subtitle=' . $subtitleReplace . '&sub_id=' . $sub_id);
//                                        }
//                                    }
                                            echo '<font color="red"><b> Prioritas Ke : ' . $subtitle_indikator->getPrioritas() . '</b></font>';
                                            //EO kunci subtitle
                                            ?> 
                                            <div id="<?php echo 'ubahSubtitle_' . $sub_id ?>"></div>
                                            <div id="<?php echo 'tambahPersonil_' . $sub_id ?>"></div>
                                            <?php ?>

                                        </td>

                                        <td style="background: pink" align="right">
                                            <?php
                                            $query2 = "select sum(nilai_anggaran) as hasil_kali
                                            from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "rincian_detail
                                            where kegiatan_code ='$kode_kegiatan' and unit_id='$unit_id' and subtitle ilike '" . str_replace('\'', '%', $nama_subtitle) . "' and status_hapus=FALSE";
                                            //print_r($query2);
                                            $con = Propel::getConnection();
                                            $stmt = $con->prepareStatement($query2);
                                            $t = $stmt->executeQuery();
                                            while ($t->next()) {
                                                echo number_format($t->getString('hasil_kali'), 0, ',', '.');
                                            }
                                            ?></td>
                                        <td style="background: pink" colspan="3">&nbsp;</td>
                                    </tr>
                                    <?php
                                } else {
                                    ?>
                                    <tr class="sf_admin_row_<?php echo $odd ?>" id="subtitle_<?php print $sub_id ?>">
                                        <td colspan="9"><b><i>
                                                    <b>
                                                    <?php echo link_to_function(image_tag('b_plus.png', array('name' => 'img_' . $sub_id, 'id' => 'img_' . $sub_id, 'border' => 0)), 'showHideKegiatan(' . $sub_id . ')') . ' ' . $subtitle_indikator->getSubtitle() ?></i></b>;
                                            <?php
                                            if ($status == 'OPEN') {
                                                //	echo link_to_remote(image_tag('/sf/sf_admin/images/cancel.png'), array('update'=>'subtitle_'.$sub_id,'url'=>'bpk/nolSubtitle?kegiatan='.$kode_kegiatan.'&unit='.$unit_id.'&idsub='.$sub_id, 'confirm'=>'Apakah anda yakin untuk me-nol kan Subtitle ini?'));
                                            }
                                            //djieb begin
                                            if ($sf_user->getNamaUser() != 'inspect' && $sf_user->getNamaUser() != 'peninjau' && $sf_user->getNamaUser() != 'dppk' && $sf_user->getNamaUser() != 'bppk' && $sf_user->getNamaUser() != 'wawali' && $sf_user->getNamaUser() != 'walikota' && $sf_user->getNamaUser() != 'sekda' && $sf_user->getNamaUser() != 'asisten' && $sf_user->getNamaUser() != 'parlemen2') {
                                                //echo link_to_remote(image_tag('/images/edit_icon2.png'),array('alt'=>'Mengubah Subtitle','title' => 'Mengubah Nilai Koefisien','update'=>'ubahSubtitle_'.$sub_id, 'url'=>'bpk/saran?act=edit&subLama='.$nama_subtitle.'&unit_id='.$unit_id.'&kegiatan_code='.$kode_kegiatan.'&id='.$sub_id,'loading'=>"Element.show('indikator')",'complete'=>"Element.hide('indikator')") );
                                                echo '&nbsp';
                                                //echo link_to_remote(image_tag('/images/personil.gif'),array('alt'=>'Menambah Personil','title' => 'menambah personil','update'=>'tambahPersonil_'.$sub_id, 'url'=>'bpk/personil?act=tambah&subtitle='.$subtitle_indikator->getSubtitle().'&unit_id='.$unit_id.'&kegiatan_code='.$kode_kegiatan,'loading'=>"Element.show('indikator')",'complete'=>"Element.hide('indikator')") );
                                            }
                                            //  echo $unit_id.' '.$kode_kegiatan.' '.$subtitle_indikator->getSubtitle();
//djiebrats: kunci subtitle STATUS: CLOSE
                                            //echo $unit_id.' '.$kode_kegiatan.' '.$subtitle_indikator->getSubtitle().' tes'.$subtitle_indikator->getLockSubtitle();
//                                    if ($subtitle_indikator->getLockSubtitle() == TRUE) {
//
//                                        //$sf_user->getNamaUser()!='peninjau' &&
//                                        if ($sf_user->getNamaUser() != 'peninjau' && $sf_user->getNamaUser() != 'dppk' && $sf_user->getNamaUser() != 'bppk' && $sf_user->getNamaUser() != 'wawali' && $sf_user->getNamaUser() != 'walikota' && $sf_user->getNamaUser() != 'sekda' && $sf_user->getNamaUser() != 'asisten' && $sf_user->getNamaUser() != 'parlemen2') {
//                                            $subtitleReplace = str_replace('/', '=', $subtitle);
//                                            echo ' ' . link_to(image_tag('/sf/sf_admin/images/unlock.png', array('alt' => __('Buka Kunci Subtitle'), 'title' => __('Buka Kunci Subtitle'))), 'bpk/bukaSubtitle?unit_id=' . $unit_id . '&kegiatan_code=' . $kode_kegiatan . '&subtitle=' . $subtitleReplace . '&sub_id=' . $sub_id);
//                                        }
//                                    } else {  //if($subtitle_indikator->getLockSubtitle()== FALSE)
//                                        if ($sf_user->getNamaUser() != 'peninjau' && $sf_user->getNamaUser() != 'dppk' && $sf_user->getNamaUser() != 'bppk' && $sf_user->getNamaUser() != 'wawali' && $sf_user->getNamaUser() != 'walikota' && $sf_user->getNamaUser() != 'sekda' && $sf_user->getNamaUser() != 'asisten' && $sf_user->getNamaUser() != 'parlemen2') {
//                                            $subtitleReplace = str_replace('/', '=', $subtitle);
//                                            //echo image_tag('/images/digisign.png',array('alt' => __('Subtitle belum ditanda tangan digital'), 'title' => __('Subtitle belum ditanda tangan digital')));
//                                            echo ' ' . link_to(image_tag('/sf/sf_admin/images/lock.png', array('alt' => __('Kunci Subtitle'), 'title' => __('Kunci Subtitle'))), 'bpk/kunciSubtitle?unit_id=' . $unit_id . '&kegiatan_code=' . $kode_kegiatan . '&subtitle=' . $subtitleReplace . '&sub_id=' . $sub_id);
//                                        }
//                                    }
                                            echo '<font color="red"><b> Prioritas Ke : ' . $subtitle_indikator->getPrioritas() . '</b></font>';
//EO kunci subtitle
                                            ?> 
                                            <div id="<?php echo 'ubahSubtitle_' . $sub_id ?>"></div>
                                            <div id="<?php echo 'tambahPersonil_' . $sub_id ?>"></div>
                                            <?php ?>

                                        </td>

                                        <td align="right">
                                            <?php
                                            $query2 = "select sum(nilai_anggaran) as hasil_kali "
                                                    . "from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "rincian_detail "
                                                    . "where kegiatan_code ='$kode_kegiatan' and unit_id='$unit_id' and subtitle ilike '" . str_replace('\'', '%', $nama_subtitle) . "' and status_hapus=FALSE";
                                            //print_r($query2);
                                            $con = Propel::getConnection();
                                            $stmt = $con->prepareStatement($query2);
                                            $t = $stmt->executeQuery();
                                            while ($t->next()) {
                                                echo number_format($t->getString('hasil_kali'), 0, ',', '.');
                                            }
                                            ?></td>
                                        <td colspan="3">&nbsp;</td>
                                    </tr><?php } ?>
                                <tr id="indicator_<?php echo $sub_id ?>" style="display:none;" align="center"><td colspan="11">
                            <dt>&nbsp;</dt><dd><b>Mohon Tunggu </b><?php echo image_tag('loading.gif', array('align' => 'absmiddle')) ?></dd>
                            </td></tr>
                        <?php endforeach; ?>
                        <?php $sub_id = 0 ?>

                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="9" align="right">Total Keseluruhan:</td>
                                <td align="right">
                                    <?php
                                    $unit_id = $subtitle_indikator->getUnitId();
                                    $kode_kegiatan = $subtitle_indikator->getKegiatanCode();
                                    $subtitle = $subtitle_indikator->getSubtitle();
                                    $nama_subtitle = trim($subtitle);

                                    $query2 = "select sum(nilai_anggaran) as hasil_kali "
                                            . "from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "rincian_detail "
                                            . "where kegiatan_code ='$kode_kegiatan' and unit_id='$unit_id' and status_hapus=FALSE";

                                    $con = Propel::getConnection();
                                    $stmt = $con->prepareStatement($query2);
                                    $t = $stmt->executeQuery();
                                    while ($t->next()) {
                                        echo number_format($t->getString('hasil_kali'), 0, ',', '.');
                                    }
                                    ?></td>
                                <td colspan="3">&nbsp;</td>
                            </tr>
                        </tfoot>
                    </table>
                <?php endif; ?>

            </div>

            <div id="sf_admin_footer">
            </div>

        </div>
    </div><!-- /.box -->

</section>

<script>
    image1 = new Image();
    image1.src = '/<?php echo sfConfig::get('app_default_coding'); ?>/images/b_plus.png';

    image2 = new Image();
    image2.src = '/<?php echo sfConfig::get('app_default_coding'); ?>/images/b_minus.png';

    function showHideKegiatan(id) {
        var row = $('#subtitle_' + id);
        var img = $('#img_' + id);

        if (img) {
            var src = document.getElementById('img_' + id).getAttribute('src');
            var minus = src.indexOf('/<?php echo sfConfig::get('app_default_coding'); ?>/images/b_minus.png');
            if (minus !== -1) {
                src = '/<?php echo sfConfig::get('app_default_coding'); ?>/images/b_plus.png';
            } else {
                src = '/<?php echo sfConfig::get('app_default_coding'); ?>/images/b_minus.png';
            }
            img.attr('src', src);
        }

        if (minus === -1) {
            var kegiatan_id = 'subtitle_' + id;
            var pekerjaans = $('.pekerjaans_' + id);
            var n = pekerjaans.length;

            if (n > 0) {
                for (var i = 0; i < pekerjaans.length; i++) {
                    var pekerjaan = pekerjaans[i];
                    pekerjaan.style.display = 'table-row';
                }
            } else {
                $('#indicator_' + id).show();
                $.ajax({
                    url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/bpk/getPekerjaans/id/" + id + "/tahap/<?php echo $tahap; ?>.html",
                    context: document.body
                }).done(function (msg) {
                    $('#subtitle_' + id).after(msg);
                    $('#indicator_' + id).remove();
                });
            }
        } else {
            $('.pekerjaans_' + id).remove();
            minus = -1;
        }
    }

    function showHideKegiatanMasalah(id, kegiatan, unit) {
        var row = $('#subtitle_' + id);
        var img = $('#img_' + id);

        if (img) {
            var src = document.getElementById('img_' + id).getAttribute('src');
            var minus = src.indexOf('/<?php echo sfConfig::get('app_default_coding'); ?>/images/b_minus.png');
            if (minus !== -1) {
                src = '/<?php echo sfConfig::get('app_default_coding'); ?>/images/b_plus.png';
            } else {
                src = '/<?php echo sfConfig::get('app_default_coding'); ?>/images/b_minus.png';
            }
            img.attr('src', src);
        }


        if (minus === -1) {
            var kegiatan_id = 'subtitle_' + id;
            var pekerjaans = $('.pekerjaans_' + id);
            var n = pekerjaans.length;

            if (n > 0) {
                for (var i = 0; i < pekerjaans.length; i++) {
                    var pekerjaan = pekerjaans[i];
                    pekerjaan.style.display = 'table-row';
                }
            } else {
                $('#indicator_' + id).show();
                $.ajax({
                    url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/bpk/getPekerjaansMasalah/id/" + id + ".html",
                    context: document.body
                }).done(function (msg) {
                    $('#subtitle_0').after(msg);
                    $('#indicator_' + id).remove();
                });
            }
        } else {
            $('.pekerjaans_' + id).remove();
            minus = -1;
        }
    }

    function hapusHeaderKegiatan(id, kegiatan, unit, kodesub) {
        var a = confirm('Apakah anda yakin akan menghapus Header Sub Subtitle kegiatan ini??');
        if (a === true) {
            $('.pekerjaans_' + id).remove();
            $.ajax({
                url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/bpk/hapusHeaderPekerjaans/id/" + id + "/kegiatan/" + kegiatan + "/unit/" + unit + "/no/" + kodesub + ".html"
            }).done();

        }
    }

    function editHeaderKegiatan(id, kegiatan, unit, kodesub) {
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/bpk/ajaxEditHeader/act/editHeader/kodeSub/" + kodesub + "/unitId/" + unit + "/kegiatanCode/" + kegiatan + ".html",
            context: document.body
        }).done(function (msg) {
            $('#header_' + kodesub).before(msg);
            $('#header_' + kodesub).remove();
        });
    }

    function editSubtitle(id) {
        var loading = $('#indicator_' + id);
        loading.show();
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/bpk/getPekerjaans/id/" + id + ".html",
            context: document.body
        }).done(function (msg) {
            loading.remove();
        });
    }

    function inKeterangan(id, kegiatan, unit) {
        var row = $('headerrka_' + id);
        var img = $('#img_' + id);
        var pekerjaans = $('#depan_' + id);
        if (img) {
            var src = document.getElementById('img_' + id).getAttribute('src');
            var minus = src.indexOf('/<?php echo sfConfig::get('app_default_coding'); ?>/images/up.png');
            if (minus !== -1) {
                src = '/<?php echo sfConfig::get('app_default_coding'); ?>/images/down.png';
            } else {
                src = '/<?php echo sfConfig::get('app_default_coding'); ?>/images/up.png';
            }
            img.attr('src', src);
        }


        if (minus === -1) {
            var kegiatan_id = 'headerrka_' + id;
            var pekerjaans = $('#depan_' + id);
            var n = pekerjaans.length;

            if (n > 0) {
                for (var i = 0; i < pekerjaans.length; i++) {
                    var pekerjaan = pekerjaans[i];
                    pekerjaan.style.display = 'table-row';
                }
            } else {
                $('#indicator').show();
                $.ajax({
                    url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/bpk/getHeader/id/" + id + "/kegiatan/" + kegiatan + "/unit/" + unit + "/tahap/<?php echo $tahap; ?>.html",
                    context: document.body
                }).done(function (msg) {
                    $('#indicator').remove();
                    $("#headerrka_" + id).after(msg);
                });
            }
        } else {
            $('#depan_' + id).remove();
        }
    }

    function hapusSubKegiatan(id, kegiatan, unit, kodesub) {
        var a = confirm('Apakah anda yakin akan menghapus SUB kegiatan ini??');
        if (a === true) {
            var kegiatan_id = 'subtitle_' + id;
            var pekerjaans = $('.pekerjaans_' + id);
            for (var i = 0; i < pekerjaans.length; i++) {
                var pekerjaan = pekerjaans[i];
                pekerjaan.style.display = 'none';
            }
            $.ajax({
                url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/bpk/hapusSubPekerjaans/id/" + id + "/kegiatan/" + kegiatan + "/unit/" + unit + "/no/" + kodesub + ".html",
                context: document.body
            }).done();
        }
    }
</script>
