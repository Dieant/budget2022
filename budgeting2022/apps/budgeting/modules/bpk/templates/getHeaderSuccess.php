<?php use_helper('Url', 'Javascript', 'Form', 'Object', 'Validation'); ?>
<div class="box-body bg-gray-active" id="depan_<?php echo $sf_params->get('id') ?>">
    <h1 align="center"><?php echo 'RENCANA KERJA ANGGARAN' ?></h1>
    <?php
    if ($tahap == 'murni') {
        $tabel_dpn = 'murni_';
    } elseif ($tahap == 'murnibp') {
        $tabel_dpn = 'murni_bukuputih_';
    } elseif ($tahap == 'murnibb') {
        $tabel_dpn = 'murni_bukubiru_';
    }
    $kode_kegiatan = $sf_params->get('kegiatan');
    $unit_id = $sf_params->get('unit');

    $kode_program22 = substr($master_kegiatan->getKodeProgram2(), 5, 2);
    if (substr($master_kegiatan->getKodeProgram2(), 0, 4) == 'X.XX') {
        $kode_urusan = substr($master_kegiatan->getKodeUrusan(), 0, 4);
    } else {
        $kode_urusan = substr($master_kegiatan->getKodeProgram2(), 0, 4);
    }
    $e = new Criteria();
    $e->add(UnitKerjaPeer::UNIT_ID, $master_kegiatan->getUnitId());
    $es = UnitKerjaPeer::doSelectOne($e);
    if ($es) {
        $kode_permen = $es->getKodePermen();
    }
    $kode = $kode_urusan . '.' . $kode_permen . '.' . $kode_program22 . '.' . $master_kegiatan->getKodeKegiatan();
    $temp_kode = explode('.', $kode);
    if (count($temp_kode) >= 5) {
        $kode_kegiatan_pakai = $temp_kode[0] . '.' . $temp_kode[1] . '.' . $temp_kode[3] . '.' . $temp_kode[4];
    } else {
        $kode_kegiatan_pakai = $kode;
    }
    $query = "select mk.kode_kegiatan, mk.nama_kegiatan, mk.alokasi_dana, mk.output, mk.output_cm, mk.kode_program, mk.benefit, mk.impact, mk.kode_program2, mk.kode_misi, mk.kode_tujuan, mk.outcome, mk.target_outcome, mk.catatan, mk.ranking, uk.unit_name, uk.kode_permen, mk.kode_urusan, mk.masukan, mk.outcome "
            . "from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "master_kegiatan mk,unit_kerja uk "
            . "where mk.kode_kegiatan = '" . $kode_kegiatan . "' and mk.unit_id = '" . $unit_id . "' "
            . "and uk.unit_id=mk.unit_id";
    $con = Propel::getConnection();
    $stmt = $con->prepareStatement($query);
    $rs = $stmt->executeQuery();
//$c = new Criteria();
//$c->add(MasterKegiatanPeer::KODE_KEGIATAN and MasterKegiatanPeer::UNIT_ID, $kode_kegiatan and $unit_id);
//$kode = MasterKegiatanPeer::doSelect($c);
//foreach ($kode as $kegiatan)
    $ranking = '';

    while ($rs->next()) {
        $kegiatan_kode = $rs->getString('kode_kegiatan');
        $nama_kegiatan = $rs->getString('nama_kegiatan');
        $alokasi = $rs->getString('alokasi_dana');
        $organisasi = $rs->getString('unit_name');
        $benefit = $rs->getString('benefit');
        $impact = $rs->getString('impact');
        $kode_program = $rs->getString('kode_program');
        $program_p_13 = $rs->getString('kode_program2');
        $masukan = $rs->getString('masukan');
        //$urusan = $rs->getString('urusan');
        $kode_misi = $rs->getString('kode_misi');
        $kode_tujuan = $rs->getString('kode_tujuan');
        $outcome = $rs->getString('outcome');
        $target_outcome = $rs->getString('target_outcome');
        $catatan = $rs->getString('catatan');
        $ranking = $rs->getInt('ranking');
        $kode_permen = $rs->getString('kode_permen');
        $kode_urusan = $rs->getString('kode_urusan');
        $outcome = $rs->getString('outcome');
        $output = $rs->getString('output');
        $output_cm = $rs->getString('output_cm');
    }

    $query = "select distinct(rd.subtitle) "
            . "from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "rincian_detail rd "
            . "where rd.kegiatan_code = '" . $kode_kegiatan . "' and rd.unit_id = '" . $unit_id . "'";
    $con = Propel::getConnection();
    $stmt = $con->prepareStatement($query);
    $rs = $stmt->executeQuery();

    $kode_kegiatan_baru = substr($master_kegiatan->getKodeKegiatan(), 8, 4);
    $kode_tulis = $kode_urusan . '.' . $kode_permen . '.' . $kode_program22 . '.' . $kode_kegiatan_baru;
    ?>
    <div class="row" style="margin-bottom: 10px">
        <div class="col-xs-3 text-right text-bold">Organisasi</div>
        <div class="col-xs-9"><?php echo $kode_permen . ' ' . $organisasi ?></div>
        <div class="clearfix"></div>
    </div>
    <div class="row" style="margin-bottom: 10px">
        <div class="col-xs-3 text-right text-bold">Kegiatan</div>
        <div class="col-xs-9"><?php echo $kode_tulis . ' ' . $nama_kegiatan ?></div>
        <div class="clearfix"></div>
    </div>
    <div class="row" style="margin-bottom: 10px">
        <div class="col-xs-3 text-right text-bold">Nama Kegiatan</div>
        <div class="col-xs-9"><?php echo $nama_kegiatan ?></div>
        <div class="clearfix"></div>
    </div>
    <div class="row" style="margin-bottom: 10px">
        <div class="col-xs-3 text-right text-bold">Misi</div>
        <div class="col-xs-9">
            <?php
            $misi_name = '';
            $query = "select * "
                    . "from " . sfConfig::get('app_default_schema') . ".master_misi ms "
                    . "where ms.kode_misi='" . $kode_misi . "'";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs1 = $stmt->executeQuery();
            while ($rs1->next()) {
                $misi_name = $rs1->getString('nama_misi');
            }
            echo $misi_name;
            ?>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="row" style="margin-bottom: 10px">
        <div class="col-xs-3 text-right text-bold">Tujuan</div>
        <div class="col-xs-9">
            <?php
            $tujuan = '';
            $query = "select *
from " . sfConfig::get('app_default_schema') . ".master_tujuan mj
where mj.kode_misi='" . $kode_misi . "' and mj.kode_tujuan='" . $kode_tujuan . "'";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs1 = $stmt->executeQuery();
            while ($rs1->next()) {
                $tujuan = $rs1->getString('nama_tujuan');
            }
            echo $tujuan;
            ?>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="row" style="margin-bottom: 10px">
        <div class="col-xs-3 text-right text-bold">Program RPJM</div>
        <div class="col-xs-9">
            <?php
            $programRPJM = '';
            $string_tujuan = '%' . $kode_tujuan . '%';
            $query = "select *
from " . sfConfig::get('app_default_schema') . ".master_program kp
where kp.kode_program='" . $kode_program . "' and kp.kode_tujuan ilike '" . $string_tujuan . "'";
//print_r($query);exit;
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs1 = $stmt->executeQuery();
            while ($rs1->next()) {
                $programRPJM = $rs1->getString('kode_program') . ' ' . $rs1->getString('nama_program');
            }
            echo $programRPJM;
            ?>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="row" style="margin-bottom: 10px">
        <div class="col-xs-3 text-right text-bold">Urusan</div>
        <div class="col-xs-9">
            <?php
            $urusan = '';
            $u = new Criteria();
            $u->add(MasterUrusanPeer::KODE_URUSAN, $kode_urusan);
            $us = MasterUrusanPeer::doSelectOne($u);
            if ($us) {
                $nama_urusan = $us->getNamaUrusan();
            }
            $urusan = $kode_urusan . ' ' . $nama_urusan;
            echo $urusan;
            ?>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="row" style="margin-bottom: 10px">
        <div class="col-xs-3 text-right text-bold">Program 13</div>
        <div class="col-xs-9">
            <?php
            $program13 = '';
            $query = "select *
from " . sfConfig::get('app_default_schema') . ".master_program2 kp
where kp.kode_program='" . $kode_program . "' and kp.kode_program2='" . $program_p_13 . "'"; //"' and kp.ranking=".$ranking."";

            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs1 = $stmt->executeQuery();
            while ($rs1->next()) {
                $program13 = $rs1->getString('kode_program2') . ' ' . $rs1->getString('nama_program2');
            }
            echo $program13;
            ?>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="row" style="margin-bottom: 10px">
        <div class="col-xs-3 text-right text-bold">Output Kegiatan</div>
        <div class="col-xs-9">
            <?php
            $output = str_replace("#", "|", $output);
            $arr_output = explode("|", $output);
            ?>
            <table class="table-bordered table-responsive">
                <thead>
                    <tr class="text-center">
                        <th style="padding:5px">Tolak Ukur Kinerja</th>
                        <th style="padding:5px">Target</th>
                        <th style="padding:5px">Satuan</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    for ($i = 0; $i < count($arr_output); $i = $i + 3) {
                        ?>                    
                        <tr class="text-center">
                            <td style="padding:5px"><?php echo $arr_output[$i] ?></td>
                            <td style="padding:5px"><?php echo $arr_output[$i + 1] ?></td>
                            <td style="padding:5px"><?php echo $arr_output[$i + 2] ?></td>
                        </tr>

                        <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="row" style="margin-bottom: 10px">
        <div class="col-xs-3 text-right text-bold">Output Countermeasure Kegiatan</div>
        <div class="col-xs-9">
            <?php
            $output_cm = str_replace("#", "|", $output_cm);
            $arr_output_cm = explode("|", $output_cm);
            ?>
            <table class="table-bordered table-responsive">
                <thead>
                    <tr class="text-center">
                        <th style="padding:5px">Tolak Ukur Kinerja</th>
                        <th style="padding:5px">Target</th>
                        <th style="padding:5px">Satuan</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    for ($i = 0; $i < count($arr_output_cm); $i = $i + 3) {
                        ?>                    
                        <tr class="text-center">
                            <td style="padding:5px"><?php echo $arr_output_cm[$i] ?></td>
                            <td style="padding:5px"><?php echo $arr_output_cm[$i + 1] ?></td>
                            <td style="padding:5px"><?php echo $arr_output_cm[$i + 2] ?></td>
                        </tr>

                        <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="row" style="margin-bottom: 10px">
        <div class="col-xs-3 text-right text-bold">Alokasi</div>
        <div class="col-xs-9">
            <?php echo 'Rp. ' . number_format($alokasi, 0, ',', '.'); ?>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="row" style="margin-bottom: 10px">
        <div class="col-xs-3 text-right text-bold">Masukan</div>
        <div class="col-xs-9">
            <?php echo $masukan; ?>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="row" style="margin-bottom: 10px">
        <hr class="col-xs-12"/>
    </div>
    <div class="row" style="margin-bottom: 10px">
        <div class="col-xs-3 text-right text-bold">
            Subtitle - Output
        </div>
        <div class="col-xs-9 text-center">
            <table class="table-bordered table-responsive" >
                <thead>
                    <tr class="text-center">
                        <th style="padding:5px">Subtitle</th>
                        <th style="padding:5px">Output</th>
                        <th style="padding:5px">Target - Satuan</th>
                        <th style="padding:5px">Penunjang</th>
                    </tr>
                </thead>
                <?php
                if ($tahap == 'murni') {
                    $d = new Criteria();
                    $d->add(MurniSubtitleIndikatorPeer::UNIT_ID, $unit_id);
                    $d->add(MurniSubtitleIndikatorPeer::KEGIATAN_CODE, $kode_kegiatan);
                    $d->setDistinct(MurniSubtitleIndikatorPeer::SUBTITLE);
                    $d->addAscendingOrderByColumn(MurniSubtitleIndikatorPeer::SUBTITLE);
                    $v = MurniSubtitleIndikatorPeer::doSelect($d);
                } elseif ($tahap == 'murnibp') {
                    $d = new Criteria();
                    $d->add(MurniBukuPutihSubtitleIndikatorPeer::UNIT_ID, $unit_id);
                    $d->add(MurniBukuPutihSubtitleIndikatorPeer::KEGIATAN_CODE, $kode_kegiatan);
                    $d->setDistinct(MurniBukuPutihSubtitleIndikatorPeer::SUBTITLE);
                    $d->addAscendingOrderByColumn(MurniBukuPutihSubtitleIndikatorPeer::SUBTITLE);
                    $v = MurniBukuPutihSubtitleIndikatorPeer::doSelect($d);
                } elseif ($tahap == 'murnibb') {
                    $d = new Criteria();
                    $d->add(MurniBukuBiruSubtitleIndikatorPeer::UNIT_ID, $unit_id);
                    $d->add(MurniBukuBiruSubtitleIndikatorPeer::KEGIATAN_CODE, $kode_kegiatan);
                    $d->setDistinct(MurniBukuBiruSubtitleIndikatorPeer::SUBTITLE);
                    $d->addAscendingOrderByColumn(MurniBukuBiruSubtitleIndikatorPeer::SUBTITLE);
                    $v = MurniBukuBiruSubtitleIndikatorPeer::doSelect($d);
                } else {
                    $d = new Criteria();
                    $d->add(SubtitleIndikatorPeer::UNIT_ID, $unit_id);
                    $d->add(SubtitleIndikatorPeer::KEGIATAN_CODE, $kode_kegiatan);
                    $d->setDistinct(SubtitleIndikatorPeer::SUBTITLE);
                    $d->addAscendingOrderByColumn(SubtitleIndikatorPeer::SUBTITLE);
                    $v = SubtitleIndikatorPeer::doSelect($d);
                }
                ?>
                <tbody>
                    <?php
                    foreach ($v as $vs) {
                        $subtitle = $vs->getSubtitle();
                        $output = $vs->getIndikator();
                        $target = $vs->getNilai();
                        $satuan = $vs->getSatuan();
                        $penunjang = $vs->getTahap();
                        $last = $vs->getLastUpdateIp();
                        ?>
                        <tr class="text-center">
                            <td style="padding:5px"><?php echo $subtitle ?></td>
                            <td ><?php echo $output ?></td>
                            <td style="padding:5px"><?php echo $target . ' ' . $satuan ?> </td>
                            <td style="padding:5px">
                                <?php
                                if ($last == 'buka') {
                                    echo $penunjang;
                                }
                                ?> 
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="row" style="margin-bottom: 10px">
        <div class="col-xs-3 text-right text-bold">
            Subtitle - Output Countermeasure
        </div>
        <div class="col-xs-9 text-center">
            <table class="table-bordered table-responsive" >
                <thead>
                    <tr class="text-center">
                        <th style="padding:5px">Subtitle</th>
                        <th style="padding:5px">Output</th>
                        <th style="padding:5px">Target - Satuan</th>
                        <th style="padding:5px">Penunjang</th>
                    </tr>
                </thead>
                <?php
                $d = new Criteria();
                $d->add(DinasSubtitleIndikatorPeer::UNIT_ID, $unit_id);
                $d->add(DinasSubtitleIndikatorPeer::KEGIATAN_CODE, $kode_kegiatan);
                $d->setDistinct(DinasSubtitleIndikatorPeer::SUBTITLE);
                $d->addAscendingOrderByColumn(DinasSubtitleIndikatorPeer::SUBTITLE);
                $v = DinasSubtitleIndikatorPeer::doSelect($d);
                ?>
                <tbody>
                    <?php
                    foreach ($v as $vs) {
                        $subtitle = $vs->getSubtitle();
                        $output_cm = $vs->getIndikatorCm();
                        $target_cm = $vs->getNilaiCm();
                        $satuan_cm = $vs->getSatuanCm();
                        $penunjang = $vs->getTahap();
                        $last = $vs->getLastUpdateIp();
                        ?>
                        <tr class="text-center">
                            <td style="padding:5px"><?php echo $subtitle ?></td>
                            <td ><?php echo $output_cm ?></td>
                            <td style="padding:5px"><?php echo $target_cm . ' ' . $satuan_cm ?> </td>
                            <td style="padding:5px">
                                <?php
                                if ($last == 'buka') {
                                    echo $penunjang;
                                }
                                ?> 
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="row" style="margin-bottom: 10px">
        <hr class="col-xs-12"/>
    </div>
    <div class="row" style="margin-bottom: 10px">
        <div class="col-xs-3 text-right text-bold">Outcome</div>
        <div class="col-xs-9">
            <table class="table-bordered table-responsive">
                <thead>
                    <tr class="text-center">
                        <th style="padding:5px">Tolak Ukur Kinerja</th>
                        <th style="padding:5px">Target</th>
                        <th style="padding:5px">Satuan</th>
                    </tr>
                </thead>
                <?php
                $come = array();
                $temp_array_come = array();
                $come = explode('|', $outcome);
                for ($i = 0; $i < count($come); $i++) {
                    $temp_array_code[] = $come[$i];
                }
                ?>
                <?php
                for ($i = 0; $i < count($temp_array_code); $i = $i + 3) {
                    ?>
                    <tbody>
                        <tr class="text-center">
                            <td style="padding:5px"><?php echo $temp_array_code[$i]; ?></td>
                            <td style="padding:5px"><?php echo $temp_array_code[$i + 1]; ?></td>
                            <td style="padding:5px"><?php echo $temp_array_code[$i + 2]; ?></td>
                        </tr>
                    </tbody>
                    <?php
                }
                ?>
            </table>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="row" style="margin-bottom: 10px">
        <div class="col-xs-3 text-right text-bold">Benefit</div>
        <div class="col-xs-9">
            <?php echo $benefit ?>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="row" style="margin-bottom: 10px">
        <div class="col-xs-3 text-right text-bold">Impact</div>
        <div class="col-xs-9">
            <?php echo $impact ?>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="row" style="margin-bottom: 10px">
        <div class="col-xs-3 text-right text-bold">Catatan</div>
        <div class="col-xs-9">
            <?php echo $catatan ?>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="row" style="margin-bottom: 10px">
        <div class="col-xs-3 text-right text-bold">Total</div>
        <div class="col-xs-9">
            <?php
            $query = "select sum(nilai_anggaran) as total "
                    . "from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "rincian_detail rd "
                    . "where rd.kegiatan_code = '" . $kode_kegiatan . "' and rd.unit_id = '" . $unit_id . "' "
                    . "and rd.status_hapus = false ";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs = $stmt->executeQuery();
            while ($rs->next()) {
                $total = $rs->getFloat('total');
            }
            echo 'Rp ' . number_format($total, 0, ',', '.');
            ?>
        </div>
        <div class="clearfix"></div>
    </div>
</div>