<?php
// auto-generated by sfPropelAdmin
// date: 2008/11/07 15:34:50
?>
<?php // use_helper('I18N', 'Date', 'Validation', 'Object', 'Javascript','Url', 'Form')       ?>
<?php use_helper('I18N', 'Date', 'Url', 'Javascript', 'Form', 'Object', 'Validation') ?>

<section class="content-header">
    <h1>Daftar Kegiatan SKPD</h1>
</section>

<!-- Main content -->
<section class="content">
    <?php include_partial('bpk/list_messages'); ?>
    <!-- Default box -->
    <div class="box box-primary box-solid">
        <div class="box-header with-border">
            <h3 class="box-title">Filters</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body">
            <?php echo form_tag('bpk/list', array('method' => 'get', 'class' => 'form-horizontal')) ?>
            <div class="form-group">
                <label class="col-sm-2 control-label">Satuan Kerja</label>
                <div class="col-sm-10">
                    <?php
                    $e = new Criteria();
                    $nama = $sf_user->getNamaUser();
                    //print_r ($nama); exit;
                    $b = new Criteria;
                    $b->add(UserHandleV2Peer::USER_ID, $nama);
                    $es = UserHandleV2Peer::doSelect($b);
                    $satuan_kerja = $es;
                    $unit_kerja = Array();
                    foreach ($satuan_kerja as $x) {
                        $unit_kerja[] = $x->getUnitId();
                    }
                    $e->add(UnitKerjaPeer::UNIT_ID, $unit_kerja[0], criteria::ILIKE);
                    for ($i = 1; $i < count($unit_kerja); $i++) {
                        $e->addOr(UnitKerjaPeer::UNIT_ID, $unit_kerja[$i], criteria::ILIKE);
                    }
                    //$e->add(UnitKerjaPeer::UNIT_NAME, 'Latihan', criteria::NOT_EQUAL);
                    $e->addAscendingOrderByColumn(UnitKerjaPeer::UNIT_NAME);
                    $v = UnitKerjaPeer::doSelect($e);

                    echo select_tag('filters[unit_id]', objects_for_select($v, 'getUnitId', 'getUnitName', isset($filters['unit_id']) ? $filters['unit_id'] : null, array('include_custom' => '------Semua Dinas------')), array('class' => 'form-control'));
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Nama Kegiatan</label>
                <div class="col-sm-10">
                    <?php
                    echo input_tag('filters[nama_kegiatan]', isset($filters['nama_kegiatan']) ? $filters['nama_kegiatan'] : null, array('class' => 'form-control', 'placeholder' => 'Nama Kegiatan')
                    );
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Tahap RKA</label>
                <div class="col-sm-10">
                    <?php
                    echo select_tag('filters[tahap]', options_for_select(array('murnibp' => 'Murni Buku Putih', 'murnibb' => 'Murni Buku Biru'), isset($filters['tahap']) ? $filters['tahap'] : '', array('include_custom' => '---Pilih Tahap---')), array('class' => 'form-control'));
                    ?>
                </div>
            </div>
            <div id="sf_admin_container">
                <ul class="sf_admin_actions">
                    <li>
                        <?php
                        if (isset($filters['tahap']) && $filters['tahap'] == 'murnibp') {
                            echo '<b>sesuai data backup pada tanggal 4 November 2015 Pukul 20.01</b>';
                        } elseif (isset($filters['tahap']) && $filters['tahap'] == 'murnibb') {
                            echo '<b>sesuai data backup pada tanggal 30 November 2016 Pukul 11.00</b>';
                        }
                        ?>
                    </li>
                    <li><?php echo button_to(__('reset'), 'bpk/list?filter=filter', 'class=sf_admin_action_reset_filter') ?></li>
                    <li><?php echo submit_tag(__('cari'), 'name=filter class=sf_admin_action_filter') ?></li>
                    <?php if ($sf_user->getNamaUser() != 'bpk') { ?>
                        <li><?php echo submit_tag(__('kunci'), 'name=kunci class=sf_admin_action_lock') ?></li>
                        <li><?php echo submit_tag(__('buka kunci'), 'name=buka class=sf_admin_action_unlock') ?></li>
                    <?php } ?>
                </ul>
            </div>
            <?php echo '</form>'; ?>
        </div>
    </div><!-- /.box -->

    <!-- Default box -->
    <div class="box box-primary box-solid">
        <div class="box-body">
            <div id="sf_admin_container">
                <?php if (!$pager->getNbResults()): ?>
                    <?php echo __('no result') ?>
                <?php else: ?>
                    <?php include_partial('bpk/list', array('pager' => $pager, 'filters' => $filters)) ?>
                <?php endif; ?>
            </div>
        </div> <!--/.box-footer-->
    </div> <!--/.box -->

</section><!-- /.content -->
