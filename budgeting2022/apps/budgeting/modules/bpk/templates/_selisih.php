<?php

if (sfConfig::get('app_tahap_edit') == 'murni') {
    //digunakan ketika awal anggaran
//    $pagu = $master_kegiatan->getAlokasiDana();

    $query = "select SUM(alokasi_dana) as nilai
		from " . sfConfig::get('app_default_schema') . ".master_kegiatan
		where kode_kegiatan ='" . $master_kegiatan->getKodeKegiatan() . "' and unit_id ='" . $master_kegiatan->getUnitId() . "' and tahun='" . sfConfig::get('app_tahun_default') . "'";

    $con = Propel::getConnection();
    $stmt = $con->prepareStatement($query);
    $rs = $stmt->executeQuery();
    while ($rs->next()) {
        $pagu = $rs->getFloat('nilai');
    }

    $query = "select SUM(nilai_anggaran) as nilai
		from " . sfConfig::get('app_default_schema') . ".rincian_detail
		where kegiatan_code ='" . $master_kegiatan->getKodeKegiatan() . "' and unit_id ='" . $master_kegiatan->getUnitId() . "' and status_hapus=FALSE and tahun='" . sfConfig::get('app_tahun_default') . "' ";

    $con = Propel::getConnection();
    $stmt = $con->prepareStatement($query);
    $rs = $stmt->executeQuery();
    while ($rs->next()) {
        $nilai = $rs->getFloat('nilai');
    }

    $selisih = $pagu - $nilai;

    $m_selisih = '';
    if ($nilai > $pagu) {
        $m_selisih = "<br><font color=red style=text-decoration:blink>Melebihi Pagu!</font>";
    }
    echo number_format($selisih, 0, ',', '.') . $m_selisih;
} else if (sfConfig::get('app_tahap_edit') != 'murni') {
    $query = "select SUM(nilai_anggaran) as nilai
		from " . sfConfig::get('app_default_schema') . ".murni_rincian_detail
		where kegiatan_code ='" . $master_kegiatan->getKodeKegiatan() . "' and unit_id ='" . $master_kegiatan->getUnitId() . "' and status_hapus=FALSE";

    $con = Propel::getConnection();
    $stmt = $con->prepareStatement($query);
    $rs = $stmt->executeQuery();
    while ($rs->next()) {
        $pagu = $rs->getFloat('nilai');
    }
    if (isset($filters['tahap']) && $filters['tahap'] == 'murni') {
        $tabel_dpn = 'murni_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'murnibp') {
        $tabel_dpn = 'murni_bukuputih_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'murnibb') {
        $tabel_dpn = 'murni_bukubiru_';
    }
    $query1 = "select SUM(nilai_anggaran) as nilai
		from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "rincian_detail
		where kegiatan_code ='" . $master_kegiatan->getKodeKegiatan() . "' and unit_id ='" . $master_kegiatan->getUnitId() . "' and status_hapus=FALSE";

    $con1 = Propel::getConnection();
    $stmt1 = $con1->prepareStatement($query1);
    $rs1 = $stmt1->executeQuery();
    while ($rs1->next()) {
        $nilai = $rs1->getFloat('nilai');
    }
    //irul 19 feb 2014
    $selisih = $nilai - $pagu;

    $m_selisih = '';
    if ($nilai > $pagu) {
        //$m_selisih = "<br><font color=red style=text-decoration:blink>Melebihi Pagu!</font>";
    }
    echo number_format($selisih, 0, ',', '.') . $m_selisih;
}
