<?php

if (isset($filters['tahap']) && $filters['tahap'] == 'murni') {
    $tabel_dpn = 'murni_';
} elseif (isset($filters['tahap']) && $filters['tahap'] == 'murnibp') {
    $tabel_dpn = 'murni_bukuputih_';
} elseif (isset($filters['tahap']) && $filters['tahap'] == 'murnibb') {
    $tabel_dpn = 'murni_bukubiru_';
} else {
    $tabel_dpn = '';
}
$query = "select SUM(nilai_anggaran) as nilai
		from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "rincian_detail
		where kegiatan_code ='" . $master_kegiatan->getKodeKegiatan() . "' and unit_id ='" . $master_kegiatan->getUnitId() . "' and status_hapus=FALSE";

$con = Propel::getConnection();
$stmt = $con->prepareStatement($query);
$rs = $stmt->executeQuery();
while ($rs->next()) {
    $nilai = $rs->getFloat('nilai');
}

echo number_format($nilai, 0, ',', '.');
?>
