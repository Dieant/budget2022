<?php

/**
 * history actions.
 *
 * @package    budgeting
 * @subpackage history
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 2692 2006-11-15 21:03:55Z fabien $
 */
class historyActions extends sfActions
{
  /**
   * Executes index action
   *
   */
  public function executeIndex()
  {
      //echo 'cok';
      //historyTahap::insertNext('0305','0001','revisi1');
  }
  public function executeMulaiRevisi1()
  {
        $unit_id = $this->getRequestParameter('unit_id');
        $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
        //------------------------------------------------------------------------------------------------------------------
        //m_r1.png
        //insert ke prev dari RD (live)
        
      $con = Propel::getConnection();
      $con->begin();
      try {
          historyTahap::delPrev($unit_id, $kode_kegiatan);
        /*$sql1 = "delete from ". sfConfig::get('app_default_schema') .".prev_master_kegiatan where unit_id='".$unit_id."'  and kode_kegiatan='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($sql1);
        $stmt->executeQuery();

        $sql2 = "delete from ". sfConfig::get('app_default_schema') .".prev_subtitle_indikator where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($sql2);
        $stmt->executeQuery();

        $sql3 = "delete from ". sfConfig::get('app_default_schema') .".prev_rincian_detail where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";

        $stmt = $con->prepareStatement($sql3);
        $stmt->executeQuery();

        $sql4 = "delete from ". sfConfig::get('app_default_schema') .".prev_rincian where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($sql4);
        $stmt->executeQuery();

        $sql5 = "delete from ". sfConfig::get('app_default_schema') .".prev_rincian_sub_parameter where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($sql5);
        $stmt->executeQuery();

        $sql6 = "delete from ". sfConfig::get('app_default_schema') .".prev_rka_member where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($sql6);
        $stmt->executeQuery();*/


        $insert1 = "insert into ". sfConfig::get('app_default_schema') .".prev_master_kegiatan select * from ". sfConfig::get('app_default_schema') .".master_kegiatan
            where unit_id='".$unit_id."' and kode_kegiatan='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."';";
        $stmt = $con->prepareStatement($insert1);
        $stmt->executeQuery();

        $insert2 = "insert into ". sfConfig::get('app_default_schema') .".prev_subtitle_indikator select * from ". sfConfig::get('app_default_schema') .".subtitle_indikator
            where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."';";
        $stmt = $con->prepareStatement($insert2);
        $stmt->executeQuery();

        $insert3 = "insert into ". sfConfig::get('app_default_schema') .".prev_rincian_detail select * from ". sfConfig::get('app_default_schema') .".rincian_detail
            where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."';";
        $stmt = $con->prepareStatement($insert3);
        $stmt->executeQuery();

        $insert4 = "insert into ". sfConfig::get('app_default_schema') .".prev_rincian select * from ". sfConfig::get('app_default_schema') .".rincian
            where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."';";
        $stmt = $con->prepareStatement($insert4);
        $stmt->executeQuery();

        $insert5 = "insert into ". sfConfig::get('app_default_schema') .".prev_rincian_sub_parameter select * from ". sfConfig::get('app_default_schema') .".rincian_sub_parameter
            where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."';";
        $stmt = $con->prepareStatement($insert5);
        $stmt->executeQuery();
        
        $insert6 = "insert into ". sfConfig::get('app_default_schema') .".prev_rka_member select * from ". sfConfig::get('app_default_schema') .".rka_member
            where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."';";
        $stmt = $con->prepareStatement($insert6);
        $stmt->executeQuery();
        
        $updateTahap = "update ". sfConfig::get('app_default_schema') .".master_kegiatan set tahap='revisi1' where unit_id='".$unit_id."' and kode_kegiatan='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."';";
        $stmt = $con->prepareStatement($updateTahap);
        $stmt->executeQuery();
        
        //historyTahap::insertPrev($unit_id, $kode_kegiatan, 'revisi1');

        $con->commit();
        budgetLogger::log('Username  '.$user_id.' telah Memulai Revisi 1  pada unit_id='.$unit_id.' dan kegiatan='.$kode_kegiatan);
        $this->setFlash('berhasil', 'Kegiatan Telah Berhasil disiapkan untuk Revisi 1');
      } catch (Exception $e)
        {
        $con->rollback();
        $this->setFlash('gagal', 'Kegiatan GAGAL karena '.$e->getMessage());
      }
      //$this->forward('peneliti','list');
      return $this->redirect('peneliti/list?unit_id=' . $unit_id );
  }
  
  //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  
  public function executeKembaliMurni()
  {
        $unit_id = $this->getRequestParameter('unit_id');
        $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
        //------------------------------------------------------------------------------------------------------------------
        //m_r1.png
        //insert ke RD (live) dari murni dan insert ke next dari revisi1
        
      $con = Propel::getConnection();
      $con->begin();
      try {
        historyTahap::delNext($unit_id, $kode_kegiatan);
        historyTahap::insertNext($unit_id, $kode_kegiatan); 
        
        $sql1 = "delete from ". sfConfig::get('app_default_schema') .".master_kegiatan where unit_id='".$unit_id."'  and kode_kegiatan='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($sql1);
        $stmt->executeQuery();

        $sql2 = "delete from ". sfConfig::get('app_default_schema') .".subtitle_indikator where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($sql2);
        $stmt->executeQuery();

        $sql3 = "delete from ". sfConfig::get('app_default_schema') .".rincian_detail where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";

        $stmt = $con->prepareStatement($sql3);
        $stmt->executeQuery();

        $sql4 = "delete from ". sfConfig::get('app_default_schema') .".rincian where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($sql4);
        $stmt->executeQuery();

        $sql5 = "delete from ". sfConfig::get('app_default_schema') .".rincian_sub_parameter where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($sql5);
        $stmt->executeQuery();

        $sql6 = "delete from ". sfConfig::get('app_default_schema') .".rka_member where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($sql6);
        $stmt->executeQuery();


        $insert1 = "insert into ". sfConfig::get('app_default_schema') .".master_kegiatan select * from ". sfConfig::get('app_default_schema') .".murni_master_kegiatan
            where unit_id='".$unit_id."' and kode_kegiatan='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($insert1);
        $stmt->executeQuery();

        $insert2 = "insert into ". sfConfig::get('app_default_schema') .".subtitle_indikator select * from ". sfConfig::get('app_default_schema') .".murni_subtitle_indikator
            where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($insert2);
        $stmt->executeQuery();

        $insert3 = "insert into ". sfConfig::get('app_default_schema') .".rincian_detail select * from ". sfConfig::get('app_default_schema') .".murni_rincian_detail
            where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($insert3);
        $stmt->executeQuery();

        $insert4 = "insert into ". sfConfig::get('app_default_schema') .".rincian select * from ". sfConfig::get('app_default_schema') .".murni_rincian
            where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($insert4);
        $stmt->executeQuery();

        $insert5 = "insert into ". sfConfig::get('app_default_schema') .".rincian_sub_parameter select * from ". sfConfig::get('app_default_schema') .".murni_rincian_sub_parameter
            where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($insert5);
        $stmt->executeQuery();
        $insert6 = "insert into ". sfConfig::get('app_default_schema') .".rka_member select * from ". sfConfig::get('app_default_schema') .".murni_rka_member
            where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";

        $stmt = $con->prepareStatement($insert6);
        $stmt->executeQuery();
        
        historyTahap::delPrev($unit_id, $kode_kegiatan);
        historyTahap::insertPrev($unit_id, $kode_kegiatan, 'murni');
        //historyTahap::insertPrev($unit_id, $kode_kegiatan, 'murni');
        
        $updateTahap = "update ". sfConfig::get('app_default_schema') .".master_kegiatan set tahap='murni' where unit_id='".$unit_id."' and kode_kegiatan='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($updateTahap);
        $stmt->executeQuery();

        $con->commit();
        budgetLogger::log('Username  '.$user_id.' telah Membatalkan Revisi 1 dan mengembalikan ke Murni pada unit_id='.$unit_id.' dan kegiatan='.$kode_kegiatan);
        $this->setFlash('berhasil', 'Kegiatan Telah Berhasil dikembalikan ke murni');
      } catch (Exception $e)
        {
        $con->rollback();
        $this->setFlash('gagal', 'Kegiatan GAGAL karena '.$e->getMessage());
      }
      //$this->forward('peneliti','list');
      return $this->redirect('peneliti/list?unit_id=' . $unit_id );
  }
  public function executeMulaiRevisi2()
  {
        $unit_id = $this->getRequestParameter('unit_id');
        $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
        //------------------------------------------------------------------------------------------------------------------
        //m_r1.png
        //insert ke revisi1 dari RD (live) dan insert ke prev dari RD
        
      $con = Propel::getConnection();
      $con->begin();
      try {
          
        $sql1 = "delete from ". sfConfig::get('app_default_schema') .".revisi1_master_kegiatan where unit_id='".$unit_id."'  and kode_kegiatan='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($sql1);
        $stmt->executeQuery();

        $sql2 = "delete from ". sfConfig::get('app_default_schema') .".revisi1_subtitle_indikator where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($sql2);
        $stmt->executeQuery();

        $sql3 = "delete from ". sfConfig::get('app_default_schema') .".revisi1_rincian_detail where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";

        $stmt = $con->prepareStatement($sql3);
        $stmt->executeQuery();

        $sql4 = "delete from ". sfConfig::get('app_default_schema') .".revisi1_rincian where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($sql4);
        $stmt->executeQuery();

        $sql5 = "delete from ". sfConfig::get('app_default_schema') .".revisi1_rincian_sub_parameter where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($sql5);
        $stmt->executeQuery();

        $sql6 = "delete from ". sfConfig::get('app_default_schema') .".revisi1_rka_member where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($sql6);
        $stmt->executeQuery();  


        $insert1 = "insert into ". sfConfig::get('app_default_schema') .".revisi1_master_kegiatan select * from ". sfConfig::get('app_default_schema') .".master_kegiatan
            where unit_id='".$unit_id."' and kode_kegiatan='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($insert1);
        $stmt->executeQuery();

        $insert2 = "insert into ". sfConfig::get('app_default_schema') .".revisi1_subtitle_indikator select * from ". sfConfig::get('app_default_schema') .".subtitle_indikator
            where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($insert2);
        $stmt->executeQuery();

        $insert3 = "insert into ". sfConfig::get('app_default_schema') .".revisi1_rincian_detail select * from ". sfConfig::get('app_default_schema') .".rincian_detail
            where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($insert3);
        $stmt->executeQuery();

        $insert4 = "insert into ". sfConfig::get('app_default_schema') .".revisi1_rincian select * from ". sfConfig::get('app_default_schema') .".rincian
            where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($insert4);
        $stmt->executeQuery();

        $insert5 = "insert into ". sfConfig::get('app_default_schema') .".revisi1_rincian_sub_parameter select * from ". sfConfig::get('app_default_schema') .".rincian_sub_parameter
            where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($insert5);
        $stmt->executeQuery();
        
        $insert6 = "insert into ". sfConfig::get('app_default_schema') .".revisi1_rka_member select * from ". sfConfig::get('app_default_schema') .".rka_member
            where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($insert6);
        $stmt->executeQuery();
        
        $updateTahap = "update ". sfConfig::get('app_default_schema') .".master_kegiatan set tahap='revisi2' where unit_id='".$unit_id."' and kode_kegiatan='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($updateTahap);
        $stmt->executeQuery();

        historyTahap::delPrev($unit_id, $kode_kegiatan);
        historyTahap::insertPrev($unit_id, $kode_kegiatan, 'revisi1');
        
        $con->commit();
        budgetLogger::log('Username  '.$user_id.' telah Memulai Revisi 2  pada unit_id='.$unit_id.' dan kegiatan='.$kode_kegiatan);
        $this->setFlash('berhasil', 'Kegiatan Telah Berhasil disiapkan untuk Revisi 2');
      } catch (Exception $e)
        {
        $con->rollback();
        $this->setFlash('gagal', 'Kegiatan GAGAL  karena '.$e->getMessage());
      }
      //$this->forward('peneliti','list');
      return $this->redirect('peneliti/list?unit_id=' . $unit_id );
  }
  public function executeKembaliRevisi1()
  {
        $unit_id = $this->getRequestParameter('unit_id');
        $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
        //------------------------------------------------------------------------------------------------------------------
        //m_r1.png
        //insert ke next dari RD(LIVE) dan insert ke RD (live) dari revisi1
        
      $con = Propel::getConnection();
      $con->begin();
      try {
        historyTahap::delNext($unit_id, $kode_kegiatan);
        historyTahap::insertNext($unit_id, $kode_kegiatan);
        
        $sql1 = "delete from ". sfConfig::get('app_default_schema') .".master_kegiatan where unit_id='".$unit_id."'  and kode_kegiatan='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($sql1);
        $stmt->executeQuery();

        $sql2 = "delete from ". sfConfig::get('app_default_schema') .".subtitle_indikator where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($sql2);
        $stmt->executeQuery();

        $sql3 = "delete from ". sfConfig::get('app_default_schema') .".rincian_detail where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";

        $stmt = $con->prepareStatement($sql3);
        $stmt->executeQuery();

        $sql4 = "delete from ". sfConfig::get('app_default_schema') .".rincian where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($sql4);
        $stmt->executeQuery();

        $sql5 = "delete from ". sfConfig::get('app_default_schema') .".rincian_sub_parameter where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($sql5);
        $stmt->executeQuery();

        $sql6 = "delete from ". sfConfig::get('app_default_schema') .".rka_member where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($sql6);
        $stmt->executeQuery();


        $insert1 = "insert into ". sfConfig::get('app_default_schema') .".master_kegiatan select * from ". sfConfig::get('app_default_schema') .".revisi1_master_kegiatan
            where unit_id='".$unit_id."' and kode_kegiatan='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($insert1);
        $stmt->executeQuery();

        $insert2 = "insert into ". sfConfig::get('app_default_schema') .".subtitle_indikator select * from ". sfConfig::get('app_default_schema') .".revisi1_subtitle_indikator
            where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($insert2);
        $stmt->executeQuery();

        $insert3 = "insert into ". sfConfig::get('app_default_schema') .".rincian_detail select * from ". sfConfig::get('app_default_schema') .".revisi1_rincian_detail
            where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($insert3);
        $stmt->executeQuery();

        $insert4 = "insert into ". sfConfig::get('app_default_schema') .".rincian select * from ". sfConfig::get('app_default_schema') .".revisi1_rincian
            where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($insert4);
        $stmt->executeQuery();

        $insert5 = "insert into ". sfConfig::get('app_default_schema') .".rincian_sub_parameter select * from ". sfConfig::get('app_default_schema') .".revisi1_rincian_sub_parameter
            where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($insert5);
        $stmt->executeQuery();
        $insert6 = "insert into ". sfConfig::get('app_default_schema') .".rka_member select * from ". sfConfig::get('app_default_schema') .".revisi1_rka_member
            where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";

        $stmt = $con->prepareStatement($insert6);
        $stmt->executeQuery();
        
        historyTahap::delPrev($unit_id, $kode_kegiatan);
        historyTahap::insertPrev($unit_id, $kode_kegiatan, 'murni');
        historyTahap::delRevisi1($unit_id, $kode_kegiatan);
        
        $updateTahap = "update ". sfConfig::get('app_default_schema') .".master_kegiatan set tahap='revisi1' where unit_id='".$unit_id."' and kode_kegiatan='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($updateTahap);
        $stmt->executeQuery();

        $con->commit();
        budgetLogger::log('Username  '.$user_id.' telah Membatalkan Revisi 2 dan mengembalikan ke revisi 1 pada unit_id='.$unit_id.' dan kegiatan='.$kode_kegiatan);
        $this->setFlash('berhasil', 'Kegiatan Telah Berhasil dibatalkan dari revisi 2 dan dikembalikan ke revisi 1');
      } catch (Exception $e)
        {
        $con->rollback();
        $this->setFlash('gagal', 'Kegiatan GAGAL  karena '.$e->getMessage());
      }
      //$this->forward('peneliti','list');
      return $this->redirect('peneliti/list?unit_id=' . $unit_id );
  }
  
  public function executeMulaiRevisi3()
  {
        $unit_id = $this->getRequestParameter('unit_id');
        $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
        //------------------------------------------------------------------------------------------------------------------
        //m_r1.png
        //insert ke revisi2 dari RD (live) dan insert ke prev dari RD
        
      $con = Propel::getConnection();
      $con->begin();
      try {
                  
        $sql1 = "delete from ". sfConfig::get('app_default_schema') .".revisi2_master_kegiatan where unit_id='".$unit_id."'  and kode_kegiatan='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($sql1);
        $stmt->executeQuery();

        $sql2 = "delete from ". sfConfig::get('app_default_schema') .".revisi2_subtitle_indikator where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($sql2);
        $stmt->executeQuery();

        $sql3 = "delete from ". sfConfig::get('app_default_schema') .".revisi2_rincian_detail where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";

        $stmt = $con->prepareStatement($sql3);
        $stmt->executeQuery();

        $sql4 = "delete from ". sfConfig::get('app_default_schema') .".revisi2_rincian where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($sql4);
        $stmt->executeQuery();

        $sql5 = "delete from ". sfConfig::get('app_default_schema') .".revisi2_rincian_sub_parameter where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($sql5);
        $stmt->executeQuery();

        $sql6 = "delete from ". sfConfig::get('app_default_schema') .".revisi2_rka_member where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($sql6);
        $stmt->executeQuery();  


        $insert1 = "insert into ". sfConfig::get('app_default_schema') .".revisi2_master_kegiatan select * from ". sfConfig::get('app_default_schema') .".master_kegiatan
            where unit_id='".$unit_id."' and kode_kegiatan='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($insert1);
        $stmt->executeQuery();

        $insert2 = "insert into ". sfConfig::get('app_default_schema') .".revisi2_subtitle_indikator select * from ". sfConfig::get('app_default_schema') .".subtitle_indikator
            where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($insert2);
        $stmt->executeQuery();

        $insert3 = "insert into ". sfConfig::get('app_default_schema') .".revisi2_rincian_detail select * from ". sfConfig::get('app_default_schema') .".rincian_detail
            where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($insert3);
        $stmt->executeQuery();

        $insert4 = "insert into ". sfConfig::get('app_default_schema') .".revisi2_rincian select * from ". sfConfig::get('app_default_schema') .".rincian
            where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($insert4);
        $stmt->executeQuery();

        $insert5 = "insert into ". sfConfig::get('app_default_schema') .".revisi2_rincian_sub_parameter select * from ". sfConfig::get('app_default_schema') .".rincian_sub_parameter
            where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($insert5);
        $stmt->executeQuery();
        
        $insert6 = "insert into ". sfConfig::get('app_default_schema') .".revisi2_rka_member select * from ". sfConfig::get('app_default_schema') .".rka_member
            where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($insert6);
        $stmt->executeQuery();
        
        $updateTahap = "update ". sfConfig::get('app_default_schema') .".master_kegiatan set tahap='revisi3' where unit_id='".$unit_id."' and kode_kegiatan='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($updateTahap);
        $stmt->executeQuery();
        
        historyTahap::delPrev($unit_id, $kode_kegiatan);
        historyTahap::insertPrev($unit_id, $kode_kegiatan, 'revisi2');
        
        $con->commit();
        budgetLogger::log('Username  '.$user_id.' telah Memulai Revisi 3  pada unit_id='.$unit_id.' dan kegiatan='.$kode_kegiatan);
        $this->setFlash('berhasil', 'Kegiatan Telah Berhasil disiapkan untuk Revisi 3');
      } catch (Exception $e)
        {
        $con->rollback();
        $this->setFlash('gagal', 'Kegiatan GAGAL  karena '.$e->getMessage());
      }
      //$this->forward('peneliti','list');
      return $this->redirect('peneliti/list?unit_id=' . $unit_id );
  }
  public function executeKembaliRevisi2()
  {
        $unit_id = $this->getRequestParameter('unit_id');
        $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
        //------------------------------------------------------------------------------------------------------------------
        //m_r1.png
        //insert ke next dari RD(LIVE) dan insert ke RD (live) dari revisi2
        
      $con = Propel::getConnection();
      $con->begin();
      try {
        historyTahap::delNext($unit_id, $kode_kegiatan);
        historyTahap::insertNext($unit_id, $kode_kegiatan);
        
        $sql1 = "delete from ". sfConfig::get('app_default_schema') .".master_kegiatan where unit_id='".$unit_id."'  and kode_kegiatan='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($sql1);
        $stmt->executeQuery();

        $sql2 = "delete from ". sfConfig::get('app_default_schema') .".subtitle_indikator where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($sql2);
        $stmt->executeQuery();

        $sql3 = "delete from ". sfConfig::get('app_default_schema') .".rincian_detail where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";

        $stmt = $con->prepareStatement($sql3);
        $stmt->executeQuery();

        $sql4 = "delete from ". sfConfig::get('app_default_schema') .".rincian where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($sql4);
        $stmt->executeQuery();

        $sql5 = "delete from ". sfConfig::get('app_default_schema') .".rincian_sub_parameter where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($sql5);
        $stmt->executeQuery();

        $sql6 = "delete from ". sfConfig::get('app_default_schema') .".rka_member where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($sql6);
        $stmt->executeQuery();


        $insert1 = "insert into ". sfConfig::get('app_default_schema') .".master_kegiatan select * from ". sfConfig::get('app_default_schema') .".revisi2_master_kegiatan
            where unit_id='".$unit_id."' and kode_kegiatan='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($insert1);
        $stmt->executeQuery();

        $insert2 = "insert into ". sfConfig::get('app_default_schema') .".subtitle_indikator select * from ". sfConfig::get('app_default_schema') .".revisi2_subtitle_indikator
            where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($insert2);
        $stmt->executeQuery();

        $insert3 = "insert into ". sfConfig::get('app_default_schema') .".rincian_detail select * from ". sfConfig::get('app_default_schema') .".revisi2_rincian_detail
            where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($insert3);
        $stmt->executeQuery();

        $insert4 = "insert into ". sfConfig::get('app_default_schema') .".rincian select * from ". sfConfig::get('app_default_schema') .".revisi2_rincian
            where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($insert4);
        $stmt->executeQuery();

        $insert5 = "insert into ". sfConfig::get('app_default_schema') .".rincian_sub_parameter select * from ". sfConfig::get('app_default_schema') .".revisi2_rincian_sub_parameter
            where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($insert5);
        $stmt->executeQuery();
        
        $insert6 = "insert into ". sfConfig::get('app_default_schema') .".rka_member select * from ". sfConfig::get('app_default_schema') .".revisi2_rka_member
            where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";

        $stmt = $con->prepareStatement($insert6);
        $stmt->executeQuery();
        
        historyTahap::delPrev($unit_id, $kode_kegiatan);
        historyTahap::insertPrev($unit_id, $kode_kegiatan, 'revisi1');
        historyTahap::delRevisi2($unit_id, $kode_kegiatan);
        
        $updateTahap = "update ". sfConfig::get('app_default_schema') .".master_kegiatan set tahap='revisi2' where unit_id='".$unit_id."' and kode_kegiatan='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($updateTahap);
        $stmt->executeQuery();

        $con->commit();
        budgetLogger::log('Username  '.$user_id.' telah Membatalkan Revisi 3 dan mengembalikan ke revisi 2 pada unit_id='.$unit_id.' dan kegiatan='.$kode_kegiatan);
        $this->setFlash('berhasil', 'Kegiatan Telah berhasil dibatalkan revisi 3 dan dikembalikan ke revisi 2');
      } catch (Exception $e)
        {
        $con->rollback();
        $this->setFlash('gagal', 'Kegiatan GAGAL  karena '.$e->getMessage());
      }
      //$this->forward('peneliti','list');
      return $this->redirect('peneliti/list?unit_id=' . $unit_id );
  }
  public function executeMulaiRevisi4()
  {
        $unit_id = $this->getRequestParameter('unit_id');
        $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
        //------------------------------------------------------------------------------------------------------------------
        //m_r1.png
        //insert ke revisi3 dari RD (live) dan insert ke prev dari RD
        
      $con = Propel::getConnection();
      $con->begin();
      try {
        
        $sql1 = "delete from ". sfConfig::get('app_default_schema') .".revisi3_master_kegiatan where unit_id='".$unit_id."'  and kode_kegiatan='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($sql1);
        $stmt->executeQuery();

        $sql2 = "delete from ". sfConfig::get('app_default_schema') .".revisi3_subtitle_indikator where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($sql2);
        $stmt->executeQuery();

        $sql3 = "delete from ". sfConfig::get('app_default_schema') .".revisi3_rincian_detail where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";

        $stmt = $con->prepareStatement($sql3);
        $stmt->executeQuery();

        $sql4 = "delete from ". sfConfig::get('app_default_schema') .".revisi3_rincian where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($sql4);
        $stmt->executeQuery();

        $sql5 = "delete from ". sfConfig::get('app_default_schema') .".revisi3_rincian_sub_parameter where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($sql5);
        $stmt->executeQuery();

        $sql6 = "delete from ". sfConfig::get('app_default_schema') .".revisi3_rka_member where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($sql6);
        $stmt->executeQuery();  


        $insert1 = "insert into ". sfConfig::get('app_default_schema') .".revisi3_master_kegiatan select * from ". sfConfig::get('app_default_schema') .".master_kegiatan
            where unit_id='".$unit_id."' and kode_kegiatan='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($insert1);
        $stmt->executeQuery();

        $insert2 = "insert into ". sfConfig::get('app_default_schema') .".revisi3_subtitle_indikator select * from ". sfConfig::get('app_default_schema') .".subtitle_indikator
            where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($insert2);
        $stmt->executeQuery();

        $insert3 = "insert into ". sfConfig::get('app_default_schema') .".revisi3_rincian_detail select * from ". sfConfig::get('app_default_schema') .".rincian_detail
            where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($insert3);
        $stmt->executeQuery();

        $insert4 = "insert into ". sfConfig::get('app_default_schema') .".revisi3_rincian select * from ". sfConfig::get('app_default_schema') .".rincian
            where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($insert4);
        $stmt->executeQuery();

        $insert5 = "insert into ". sfConfig::get('app_default_schema') .".revisi3_rincian_sub_parameter select * from ". sfConfig::get('app_default_schema') .".rincian_sub_parameter
            where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($insert5);
        $stmt->executeQuery();
        
        $insert6 = "insert into ". sfConfig::get('app_default_schema') .".revisi3_rka_member select * from ". sfConfig::get('app_default_schema') .".rka_member
            where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($insert6);
        $stmt->executeQuery();
        
        $updateTahap = "update ". sfConfig::get('app_default_schema') .".master_kegiatan set tahap='revisi4' where unit_id='".$unit_id."' and kode_kegiatan='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($updateTahap);
        $stmt->executeQuery();
        
        historyTahap::delPrev($unit_id, $kode_kegiatan);
        historyTahap::insertPrev($unit_id, $kode_kegiatan, 'revisi3');
        
        $con->commit();
        budgetLogger::log('Username  '.$user_id.' telah Memulai Revisi 4  pada unit_id='.$unit_id.' dan kegiatan='.$kode_kegiatan);
        $this->setFlash('berhasil', 'Kegiatan Telah Berhasil disiapkan untuk Revisi 4');
      } catch (Exception $e)
        {
        $con->rollback();
        $this->setFlash('gagal', 'Kegiatan GAGAL karena '.$e->getMessage());
      }
      //$this->forward('peneliti','list');
      return $this->redirect('peneliti/list?unit_id=' . $unit_id );
  }
  public function executeKembaliRevisi3()
  {
        $unit_id = $this->getRequestParameter('unit_id');
        $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
        //------------------------------------------------------------------------------------------------------------------
        //m_r1.png
        //insert ke next dari RD(LIVE) dan insert ke RD (live) dari revisi3
        
      $con = Propel::getConnection();
      $con->begin();
      try {
        historyTahap::delNext($unit_id, $kode_kegiatan);
        historyTahap::insertNext($unit_id, $kode_kegiatan);
        
        $sql1 = "delete from ". sfConfig::get('app_default_schema') .".master_kegiatan where unit_id='".$unit_id."'  and kode_kegiatan='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($sql1);
        $stmt->executeQuery();

        $sql2 = "delete from ". sfConfig::get('app_default_schema') .".subtitle_indikator where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($sql2);
        $stmt->executeQuery();

        $sql3 = "delete from ". sfConfig::get('app_default_schema') .".rincian_detail where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";

        $stmt = $con->prepareStatement($sql3);
        $stmt->executeQuery();

        $sql4 = "delete from ". sfConfig::get('app_default_schema') .".rincian where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($sql4);
        $stmt->executeQuery();

        $sql5 = "delete from ". sfConfig::get('app_default_schema') .".rincian_sub_parameter where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($sql5);
        $stmt->executeQuery();

        $sql6 = "delete from ". sfConfig::get('app_default_schema') .".rka_member where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($sql6);
        $stmt->executeQuery();


        $insert1 = "insert into ". sfConfig::get('app_default_schema') .".master_kegiatan select * from ". sfConfig::get('app_default_schema') .".revisi3_master_kegiatan
            where unit_id='".$unit_id."' and kode_kegiatan='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($insert1);
        $stmt->executeQuery();

        $insert2 = "insert into ". sfConfig::get('app_default_schema') .".subtitle_indikator select * from ". sfConfig::get('app_default_schema') .".revisi3_subtitle_indikator
            where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($insert2);
        $stmt->executeQuery();

        $insert3 = "insert into ". sfConfig::get('app_default_schema') .".rincian_detail select * from ". sfConfig::get('app_default_schema') .".revisi3_rincian_detail
            where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($insert3);
        $stmt->executeQuery();

        $insert4 = "insert into ". sfConfig::get('app_default_schema') .".rincian select * from ". sfConfig::get('app_default_schema') .".revisi3_rincian
            where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($insert4);
        $stmt->executeQuery();

        $insert5 = "insert into ". sfConfig::get('app_default_schema') .".rincian_sub_parameter select * from ". sfConfig::get('app_default_schema') .".revisi3_rincian_sub_parameter
            where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($insert5);
        $stmt->executeQuery();
        
        $insert6 = "insert into ". sfConfig::get('app_default_schema') .".rka_member select * from ". sfConfig::get('app_default_schema') .".revisi3_rka_member
            where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";

        $stmt = $con->prepareStatement($insert6);
        $stmt->executeQuery();
        
        historyTahap::delPrev($unit_id, $kode_kegiatan);
        historyTahap::insertPrev($unit_id, $kode_kegiatan, 'revisi2');
        historyTahap::delRevisi3($unit_id, $kode_kegiatan);
        
        $updateTahap = "update ". sfConfig::get('app_default_schema') .".master_kegiatan set tahap='revisi3' where unit_id='".$unit_id."' and kode_kegiatan='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($updateTahap);
        $stmt->executeQuery();

        $con->commit();
        budgetLogger::log('Username  '.$user_id.' telah Membatalkan Revisi 4 dan mengembalikan ke revisi3 pada unit_id='.$unit_id.' dan kegiatan='.$kode_kegiatan);
        $this->setFlash('berhasil', 'Kegiatan Telah Berhasil dibatalkan revisi 4 dan dikembalikan ke revisi 3');
      } catch (Exception $e)
        {
        $con->rollback();
        $this->setFlash('gagal', 'Kegiatan GAGAL  karena '.$e->getMessage());
      }
      //$this->forward('peneliti','list');
      return $this->redirect('peneliti/list?unit_id=' . $unit_id );
  }
  public function executeMulaiRevisi5()
  {
        $unit_id = $this->getRequestParameter('unit_id');
        $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
        //------------------------------------------------------------------------------------------------------------------
        //m_r1.png
        //insert ke revisi4 dari RD (live) dan insert ke prev dari RD
        
      $con = Propel::getConnection();
      $con->begin();
      try {
        
          
        $sql1 = "delete from ". sfConfig::get('app_default_schema') .".revisi4_master_kegiatan where unit_id='".$unit_id."'  and kode_kegiatan='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($sql1);
        $stmt->executeQuery();

        $sql2 = "delete from ". sfConfig::get('app_default_schema') .".revisi4_subtitle_indikator where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($sql2);
        $stmt->executeQuery();

        $sql3 = "delete from ". sfConfig::get('app_default_schema') .".revisi4_rincian_detail where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";

        $stmt = $con->prepareStatement($sql3);
        $stmt->executeQuery();

        $sql4 = "delete from ". sfConfig::get('app_default_schema') .".revisi4_rincian where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($sql4);
        $stmt->executeQuery();

        $sql5 = "delete from ". sfConfig::get('app_default_schema') .".revisi4_rincian_sub_parameter where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($sql5);
        $stmt->executeQuery();

        $sql6 = "delete from ". sfConfig::get('app_default_schema') .".revisi4_rka_member where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($sql6);
        $stmt->executeQuery();  


        $insert1 = "insert into ". sfConfig::get('app_default_schema') .".revisi4_master_kegiatan select * from ". sfConfig::get('app_default_schema') .".master_kegiatan
            where unit_id='".$unit_id."' and kode_kegiatan='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($insert1);
        $stmt->executeQuery();

        $insert2 = "insert into ". sfConfig::get('app_default_schema') .".revisi4_subtitle_indikator select * from ". sfConfig::get('app_default_schema') .".subtitle_indikator
            where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($insert2);
        $stmt->executeQuery();

        $insert3 = "insert into ". sfConfig::get('app_default_schema') .".revisi4_rincian_detail select * from ". sfConfig::get('app_default_schema') .".rincian_detail
            where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($insert3);
        $stmt->executeQuery();

        $insert4 = "insert into ". sfConfig::get('app_default_schema') .".revisi4_rincian select * from ". sfConfig::get('app_default_schema') .".rincian
            where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($insert4);
        $stmt->executeQuery();

        $insert5 = "insert into ". sfConfig::get('app_default_schema') .".revisi4_rincian_sub_parameter select * from ". sfConfig::get('app_default_schema') .".rincian_sub_parameter
            where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($insert5);
        $stmt->executeQuery();
        
        $insert6 = "insert into ". sfConfig::get('app_default_schema') .".revisi4_rka_member select * from ". sfConfig::get('app_default_schema') .".rka_member
            where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($insert6);
        $stmt->executeQuery();
        
        $updateTahap = "update ". sfConfig::get('app_default_schema') .".master_kegiatan set tahap='revisi5' where unit_id='".$unit_id."' and kode_kegiatan='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($updateTahap);
        $stmt->executeQuery();
        
        historyTahap::delPrev($unit_id, $kode_kegiatan);
        historyTahap::insertPrev($unit_id, $kode_kegiatan, 'revisi4');
        
        $con->commit();
        budgetLogger::log('Username  '.$user_id.' telah Memulai Revisi 5  pada unit_id='.$unit_id.' dan kegiatan='.$kode_kegiatan);
        $this->setFlash('berhasil', 'Kegiatan Telah Berhasil disiapkan untuk Revisi 5');
      } catch (Exception $e)
        {
        $con->rollback();
        $this->setFlash('gagal', 'Kegiatan GAGAL  karena '.$e->getMessage());
      }
      //$this->forward('peneliti','list');
      return $this->redirect('peneliti/list?unit_id=' . $unit_id );
  }
  public function executeKembaliRevisi4()
  {
        $unit_id = $this->getRequestParameter('unit_id');
        $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
        //------------------------------------------------------------------------------------------------------------------
        //m_r1.png
        //insert ke next dari RD(LIVE) dan insert ke RD (live) dari revisi4
        
      $con = Propel::getConnection();
      $con->begin();
      try {
        historyTahap::delNext($unit_id, $kode_kegiatan);
        historyTahap::insertNext($unit_id, $kode_kegiatan);
        
        $sql1 = "delete from ". sfConfig::get('app_default_schema') .".master_kegiatan where unit_id='".$unit_id."'  and kode_kegiatan='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($sql1);
        $stmt->executeQuery();

        $sql2 = "delete from ". sfConfig::get('app_default_schema') .".subtitle_indikator where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($sql2);
        $stmt->executeQuery();

        $sql3 = "delete from ". sfConfig::get('app_default_schema') .".rincian_detail where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";

        $stmt = $con->prepareStatement($sql3);
        $stmt->executeQuery();

        $sql4 = "delete from ". sfConfig::get('app_default_schema') .".rincian where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($sql4);
        $stmt->executeQuery();

        $sql5 = "delete from ". sfConfig::get('app_default_schema') .".rincian_sub_parameter where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($sql5);
        $stmt->executeQuery();

        $sql6 = "delete from ". sfConfig::get('app_default_schema') .".rka_member where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($sql6);
        $stmt->executeQuery();


        $insert1 = "insert into ". sfConfig::get('app_default_schema') .".master_kegiatan select * from ". sfConfig::get('app_default_schema') .".revisi4_master_kegiatan
            where unit_id='".$unit_id."' and kode_kegiatan='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($insert1);
        $stmt->executeQuery();

        $insert2 = "insert into ". sfConfig::get('app_default_schema') .".subtitle_indikator select * from ". sfConfig::get('app_default_schema') .".revisi4_subtitle_indikator
            where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($insert2);
        $stmt->executeQuery();

        $insert3 = "insert into ". sfConfig::get('app_default_schema') .".rincian_detail select * from ". sfConfig::get('app_default_schema') .".revisi4_rincian_detail
            where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($insert3);
        $stmt->executeQuery();

        $insert4 = "insert into ". sfConfig::get('app_default_schema') .".rincian select * from ". sfConfig::get('app_default_schema') .".revisi4_rincian
            where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($insert4);
        $stmt->executeQuery();

        $insert5 = "insert into ". sfConfig::get('app_default_schema') .".rincian_sub_parameter select * from ". sfConfig::get('app_default_schema') .".revisi4_rincian_sub_parameter
            where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($insert5);
        $stmt->executeQuery();
        $insert6 = "insert into ". sfConfig::get('app_default_schema') .".rka_member select * from ". sfConfig::get('app_default_schema') .".revisi4_rka_member
            where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";

        $stmt = $con->prepareStatement($insert6);
        $stmt->executeQuery();
        
        historyTahap::delPrev($unit_id, $kode_kegiatan);
        historyTahap::insertPrev($unit_id, $kode_kegiatan, 'revisi3');
        historyTahap::delrevisi4($unit_id, $kode_kegiatan);
        
        $updateTahap = "update ". sfConfig::get('app_default_schema') .".master_kegiatan set tahap='revisi4' where unit_id='".$unit_id."' and kode_kegiatan='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($updateTahap);
        $stmt->executeQuery();

        $con->commit();
        budgetLogger::log('Username  '.$user_id.' telah Membatalkan Revisi 5 dan mengembalikan ke revisi 4 pada unit_id='.$unit_id.' dan kegiatan='.$kode_kegiatan);
        $this->setFlash('berhasil', 'Kegiatan Telah Berhasil dibatalkan revisi 5 dan dikembalikan ke revisi 4');
      } catch (Exception $e)
        {
        $con->rollback();
        $this->setFlash('gagal', 'Kegiatan GAGAL  karena '.$e->getMessage());
      }
      //$this->forward('peneliti','list');
      return $this->redirect('peneliti/list?unit_id=' . $unit_id );
  }
  public function executeMulaiPak()
  {
        $unit_id = $this->getRequestParameter('unit_id');
        $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
        //------------------------------------------------------------------------------------------------------------------
        //m_r1.png
        //insert ke Prev dari RD (live) dan insert ke prev dari RD
        
      $con = Propel::getConnection();
      $con->begin();
      try {
        
          
        $sql1 = "delete from ". sfConfig::get('app_default_schema') .".revisi5_master_kegiatan where unit_id='".$unit_id."'  and kode_kegiatan='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($sql1);
        $stmt->executeQuery();

        $sql2 = "delete from ". sfConfig::get('app_default_schema') .".revisi5_subtitle_indikator where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($sql2);
        $stmt->executeQuery();

        $sql3 = "delete from ". sfConfig::get('app_default_schema') .".revisi5_rincian_detail where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";

        $stmt = $con->prepareStatement($sql3);
        $stmt->executeQuery();

        $sql4 = "delete from ". sfConfig::get('app_default_schema') .".revisi5_rincian where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($sql4);
        $stmt->executeQuery();

        $sql5 = "delete from ". sfConfig::get('app_default_schema') .".revisi5_rincian_sub_parameter where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($sql5);
        $stmt->executeQuery();

        $sql6 = "delete from ". sfConfig::get('app_default_schema') .".revisi5_rka_member where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($sql6);
        $stmt->executeQuery();  


        $insert1 = "insert into ". sfConfig::get('app_default_schema') .".revisi5_master_kegiatan select * from ". sfConfig::get('app_default_schema') .".master_kegiatan
            where unit_id='".$unit_id."' and kode_kegiatan='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($insert1);
        $stmt->executeQuery();

        $insert2 = "insert into ". sfConfig::get('app_default_schema') .".revisi5_subtitle_indikator select * from ". sfConfig::get('app_default_schema') .".subtitle_indikator
            where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($insert2);
        $stmt->executeQuery();

        $insert3 = "insert into ". sfConfig::get('app_default_schema') .".revisi5_rincian_detail select * from ". sfConfig::get('app_default_schema') .".rincian_detail
            where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($insert3);
        $stmt->executeQuery();

        $insert4 = "insert into ". sfConfig::get('app_default_schema') .".revisi5_rincian select * from ". sfConfig::get('app_default_schema') .".rincian
            where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($insert4);
        $stmt->executeQuery();

        $insert5 = "insert into ". sfConfig::get('app_default_schema') .".revisi5_rincian_sub_parameter select * from ". sfConfig::get('app_default_schema') .".rincian_sub_parameter
            where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($insert5);
        $stmt->executeQuery();
        
        $insert6 = "insert into ". sfConfig::get('app_default_schema') .".revisi5_rka_member select * from ". sfConfig::get('app_default_schema') .".rka_member
            where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($insert6);
        $stmt->executeQuery();
        
        $updateTahap = "update ". sfConfig::get('app_default_schema') .".master_kegiatan set tahap='pak' where unit_id='".$unit_id."' and kode_kegiatan='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($updateTahap);
        $stmt->executeQuery();
        
        historyTahap::delPrev($unit_id, $kode_kegiatan);
        historyTahap::insertPrev($unit_id, $kode_kegiatan, 'revisi5');
        
        $con->commit();
        budgetLogger::log('Username  '.$user_id.' telah Memulai PAK  pada unit_id='.$unit_id.' dan kegiatan='.$kode_kegiatan);
        $this->setFlash('berhasil', 'Kegiatan Telah Berhasil disiapkan untuk PAK');
      } catch (Exception $e)
        {
        $con->rollback();
        $this->setFlash('gagal', 'Kegiatan GAGAL  karena '.$e->getMessage());
      }
      //$this->forward('peneliti','list');
      return $this->redirect('peneliti/list?unit_id=' . $unit_id );
  }
  
  public function executeMulaiPakLangsung()
  {
        $unit_id = $this->getRequestParameter('unit_id');
        $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
        //------------------------------------------------------------------------------------------------------------------
        //m_r1.png
        //insert ke Prev dari RD (live) dan insert ke prev dari RD
        $queryTahap = "select tahap from ". sfConfig::get('app_default_schema') .".master_kegiatan where unit_id='$unit_id' and kode_kegiatan='$kode_kegiatan'";
        $con=Propel::getConnection();
        $statement2=$con->prepareStatement($queryTahap);
        $rs_tahap = $statement2->executeQuery();
        while($rs_tahap->next())
        {
                $tahap = $rs_tahap->getString('tahap');
        }
      $con = Propel::getConnection();
      $con->begin();
      try {
          historyTahap::delPrev($unit_id, $kode_kegiatan);
          historyTahap::insertPrevDrLive($unit_id, $kode_kegiatan);
          
        $sql1 = "delete from ". sfConfig::get('app_default_schema') .".".$tahap."_master_kegiatan where unit_id='".$unit_id."'  and kode_kegiatan='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($sql1);
        $stmt->executeQuery();

        $sql2 = "delete from ". sfConfig::get('app_default_schema') .".".$tahap."_subtitle_indikator where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($sql2);
        $stmt->executeQuery();

        $sql3 = "delete from ". sfConfig::get('app_default_schema') .".".$tahap."_rincian_detail where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";

        $stmt = $con->prepareStatement($sql3);
        $stmt->executeQuery();

        $sql4 = "delete from ". sfConfig::get('app_default_schema') .".".$tahap."_rincian where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($sql4);
        $stmt->executeQuery();

        $sql5 = "delete from ". sfConfig::get('app_default_schema') .".".$tahap."_rincian_sub_parameter where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($sql5);
        $stmt->executeQuery();

        $sql6 = "delete from ". sfConfig::get('app_default_schema') .".".$tahap."_rka_member where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($sql6);
        $stmt->executeQuery();  


        $insert1 = "insert into ". sfConfig::get('app_default_schema') .".".$tahap."_master_kegiatan select * from ". sfConfig::get('app_default_schema') .".master_kegiatan
            where unit_id='".$unit_id."' and kode_kegiatan='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($insert1);
        $stmt->executeQuery();

        $insert2 = "insert into ". sfConfig::get('app_default_schema') .".".$tahap."_subtitle_indikator select * from ". sfConfig::get('app_default_schema') .".subtitle_indikator
            where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($insert2);
        $stmt->executeQuery();

        $insert3 = "insert into ". sfConfig::get('app_default_schema') .".".$tahap."_rincian_detail select * from ". sfConfig::get('app_default_schema') .".rincian_detail
            where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($insert3);
        $stmt->executeQuery();

        $insert4 = "insert into ". sfConfig::get('app_default_schema') .".".$tahap."_rincian select * from ". sfConfig::get('app_default_schema') .".rincian
            where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($insert4);
        $stmt->executeQuery();

        $insert5 = "insert into ". sfConfig::get('app_default_schema') .".".$tahap."_rincian_sub_parameter select * from ". sfConfig::get('app_default_schema') .".rincian_sub_parameter
            where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($insert5);
        $stmt->executeQuery();
        
        $insert6 = "insert into ". sfConfig::get('app_default_schema') .".".$tahap."_rka_member select * from ". sfConfig::get('app_default_schema') .".rka_member
            where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($insert6);
        $stmt->executeQuery();
        
        $updateTahap = "update ". sfConfig::get('app_default_schema') .".master_kegiatan set tahap='pak' where unit_id='".$unit_id."' and kode_kegiatan='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($updateTahap);
        $stmt->executeQuery();

        
        
        $con->commit();
        budgetLogger::log('Username  '.$user_id.' telah Memulai PAK  pada unit_id='.$unit_id.' dan kegiatan='.$kode_kegiatan);
        $this->setFlash('berhasil', 'Kegiatan Telah Berhasil disiapkan PAK');
      } catch (Exception $e)
        {
        $con->rollback();
        $this->setFlash('gagal', 'Kegiatan GAGAL  karena '.$e->getMessage());
      }
      //$this->forward('peneliti','list');
      return $this->redirect('peneliti/list?unit_id=' . $unit_id );
  }
  public function executeBatalPak()
  {
        $unit_id = $this->getRequestParameter('unit_id');
        $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
        //------------------------------------------------------------------------------------------------------------------
        //m_r1.png
        //insert RD ke Next dan insert  Prev ke RD
        
        
      $con = Propel::getConnection();
      $con->begin();
      try {
        historyTahap::delNext($unit_id, $kode_kegiatan);
        historyTahap::insertNext($unit_id, $kode_kegiatan);
        
        $sql1 = "delete from ". sfConfig::get('app_default_schema') .".master_kegiatan where unit_id='".$unit_id."'  and kode_kegiatan='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($sql1);
        $stmt->executeQuery();

        $sql2 = "delete from ". sfConfig::get('app_default_schema') .".subtitle_indikator where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($sql2);
        $stmt->executeQuery();

        $sql3 = "delete from ". sfConfig::get('app_default_schema') .".rincian_detail where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";

        $stmt = $con->prepareStatement($sql3);
        $stmt->executeQuery();

        $sql4 = "delete from ". sfConfig::get('app_default_schema') .".rincian where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($sql4);
        $stmt->executeQuery();

        $sql5 = "delete from ". sfConfig::get('app_default_schema') .".rincian_sub_parameter where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($sql5);
        $stmt->executeQuery();

        $sql6 = "delete from ". sfConfig::get('app_default_schema') .".rka_member where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($sql6);
        $stmt->executeQuery();


        $insert1 = "insert into ". sfConfig::get('app_default_schema') .".master_kegiatan select * from ". sfConfig::get('app_default_schema') .".prev_master_kegiatan
            where unit_id='".$unit_id."' and kode_kegiatan='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($insert1);
        $stmt->executeQuery();

        $insert2 = "insert into ". sfConfig::get('app_default_schema') .".subtitle_indikator select * from ". sfConfig::get('app_default_schema') .".prev_subtitle_indikator
            where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($insert2);
        $stmt->executeQuery();

        $insert3 = "insert into ". sfConfig::get('app_default_schema') .".rincian_detail select * from ". sfConfig::get('app_default_schema') .".prev_rincian_detail
            where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($insert3);
        $stmt->executeQuery();

        $insert4 = "insert into ". sfConfig::get('app_default_schema') .".rincian select * from ". sfConfig::get('app_default_schema') .".prev_rincian
            where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($insert4);
        $stmt->executeQuery();

        $insert5 = "insert into ". sfConfig::get('app_default_schema') .".rincian_sub_parameter select * from ". sfConfig::get('app_default_schema') .".prev_rincian_sub_parameter
            where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($insert5);
        $stmt->executeQuery();
        $insert6 = "insert into ". sfConfig::get('app_default_schema') .".rka_member select * from ". sfConfig::get('app_default_schema') .".prev_rka_member
            where unit_id='".$unit_id."' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";

        $stmt = $con->prepareStatement($insert6);
        $stmt->executeQuery();
        
        //historyTahap::delrevisi5($unit_id, $kode_kegiatan);
        /*
        $updateTahap = "update ". sfConfig::get('app_default_schema') .".master_kegiatan set tahap='revisi4' where unit_id='".$unit_id."' and kode_kegiatan='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."'";
        $stmt = $con->prepareStatement($updateTahap);
        $stmt->executeQuery();
*/
	$queryTahap = "select tahap from ". sfConfig::get('app_default_schema') .".master_kegiatan where unit_id='$unit_id' and kode_kegiatan='$kode_kegiatan'";
        $con=Propel::getConnection();
        $statement2=$con->prepareStatement($queryTahap);
        $rs_tahap = $statement2->executeQuery();
        while($rs_tahap->next())
        {
                $tahap = $rs_tahap->getString('tahap');
        }
        //print_r($insert3);exit;
	if($tahap=='pak')
		$tahap_prev='revisi_final';
	else if ($tahap=='revisi1')
		$tahap_prev='revisi_final';
	else if ($tahap=='revisi2')
		$tahap_prev='revisi1';
	else if ($tahap=='revisi3')
		$tahap_prev='revisi2';
	else if ($tahap=='revisi4')
		$tahap_prev='revisi3';
	else if ($tahap=='revisi5')
		$tahap_prev='revisi4';

	historyTahap::delPrev($unit_id, $kode_kegiatan);
        historyTahap::insertPrev($unit_id, $kode_kegiatan, $tahap_prev);

        $con->commit();
        budgetLogger::log('Username  '.$user_id.' telah Membatalkan PAK pada unit_id='.$unit_id.' dan kegiatan='.$kode_kegiatan);
        $this->setFlash('berhasil', 'Kegiatan Telah dibatalkan PAK dan dikembalikan ke tahap sebelumnya');
      } catch (Exception $e)
        {
        $con->rollback();
        $this->setFlash('gagal', 'Kegiatan GAGAL karena '.$e->getMessage());
      }
      //$this->forward('peneliti','list');
      return $this->redirect('peneliti/list?unit_id=' . $unit_id );
  }
}
