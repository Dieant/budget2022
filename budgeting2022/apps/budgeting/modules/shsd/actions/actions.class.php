<?php

/**
 * shsd actions.
 *
 * @package    budgeting
 * @subpackage shsd
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 2692 2006-11-15 21:03:55Z fabien $
 */
class shsdActions extends sfActions {

    /**
     * Executes index action
     *
     */

    public function executeUbahPassLogin() {
        $username = $this->getUser()->getNamaLogin();
        if ($this->getRequest()->getMethod() == sfRequest::POST) {
            if ($this->getRequestParameter('keluar')) {

                // if ($this->getUser()->hasCredential('dinas'))
                //     return $this->redirect('login/logoutDinas');
                // else if ($this->getUser()->hasCredential('dewan'))
                //     return $this->redirect('login/logoutDewan');
                // else if ($this->getUser()->hasCredential('viewer'))
                //     return $this->redirect('login/logoutViewer');
                // else if ($this->getUser()->hasCredential('peneliti'))
                //     return $this->redirect('login/logoutPeneliti');
                if ($this->getUser()->hasCredential('data'))
                    return $this->redirect('login/logoutData');
                else
                    return $this->redirect('login/logout');
            }
            if ($this->getRequestParameter('simpan')) {
                $pass_lama = $this->getRequestParameter('pass_lama');
                $pass_baru = $this->getRequestParameter('pass_baru');
                $ulang_pass_baru = $this->getRequestParameter('ulang_pass_baru');
                $md5_pass_lama = md5($pass_lama);
                $md5_pass_baru = md5($pass_baru);

                $c_user = new Criteria();
                $c_user->add(MasterUserV2Peer::USER_ID, $username);
                $c_user->add(MasterUserV2Peer::USER_PASSWORD, $md5_pass_lama);
                $rs_user = MasterUserV2Peer::doSelectOne($c_user);
                if ($rs_user) {
                    if ($pass_baru == $ulang_pass_baru) {
                        $con = Propel::getConnection();
                        $con->begin();
                        try {
                            $newSetting = MasterUserV2Peer::retrieveByPK($username);
                            //$newSetting->setUserName($namaUser);
                            //$newSetting->setNip($nip);
                            $newSetting->setUserPassword($md5_pass_baru);
                            $newSetting->save($con);
                            budgetLogger::log('Username  ' . $username . ' telah mengganti password pada e-budgeting');
                            $this->setFlash('berhasil', 'Password sudah berhasil dirubah, silahkan keluar dan login kembali');
                            // $con->commit();

                            $c = new Criteria();
                            $c->add(SchemaAksesV2Peer::USER_ID, $username);
                            $c->add(SchemaAksesV2Peer::SCHEMA_ID, 2);
                            $cs = SchemaAksesV2Peer::doSelectOne($c);
                            $cs->setIsUbahPass(true);
                            $cs->save($con);
                            $con->commit();
                            // return $this->redirect('entri/eula?unit_id=' . $unit_id);
                        } catch (Exception $e) {
                            $this->setFlash('gagal', 'Password gagal dirubah karena ' . $e);
                            $con->rollback();
                        }
                    }
                }
            }
        }
        $settingPass = new Criteria();
        $settingPass->add(MasterUserV2Peer::USER_ID, $username);
        $this->profil = $rs_settingPass = MasterUserV2Peer::doSelectOne($settingPass);
        $this->setLayout('layouteula');
    }

    public function executeIndex() {
        $this->forward('shsd', 'list');
    }

    public function executeShsdMirip() {
        $page = $this->getRequestParameter('page') ? $this->getRequestParameter('page') : 1;
        $bagi = 100;

        $c = new Criteria();
        $c->add(KomponenPeer::KOMPONEN_TIPE, 'SHSD', Criteria::EQUAL);
        $jml_data = KomponenPeer::doCount($c);
        $this->jml_page = round($jml_data / $bagi);
        $this->page = $page;
        $offset = $bagi * ($page - 1);

        $c = new Criteria();
        $c->add(KomponenPeer::KOMPONEN_TIPE, 'SHSD', Criteria::EQUAL);
        $c->setLimit($bagi);
        $c->setOffset($offset);
        $rs_usulan = KomponenPeer::doSelect($c);

        $hasil = array();
        $ctr = 0;
        foreach ($rs_usulan as $value) {
            //$cek = preg_replace("/[^[:alnum:]]/u", '|', $value->getKomponenName()) . '|' . preg_replace("/[^[:alnum:]]/u", '|', $value->getSatuan());
            $cek = str_replace(" ", "|", $value->getKomponenName()) . '|' . str_replace(" ", "|", $value->getSatuan());
            $arr_cek = explode('|', $cek);
            $temp = " komponen_name||' '||satuan ilike '%" . $arr_cek[0] . "%' ";
            for ($i = 1; $i < sizeof($arr_cek); $i++) {
                $temp .= " and komponen_name||' '||satuan ilike '%" . $arr_cek[$i] . "%' ";
            }

            $con = Propel::getConnection();
            $query_usulan_mirip = "select komponen_id, komponen_name, satuan "
                    . "from " . sfConfig::get('app_default_schema') . ".komponen "
                    . "where " . $temp . " "
                    . "and komponen_id <> '" . $value->getKomponenId() . "' "
                    . "order by komponen_name";
            $stmt_usulan_mirip = $con->prepareStatement($query_usulan_mirip);
            if ($rs_usulan_mirip = $stmt_usulan_mirip->executeQuery()) {
                while ($rs_usulan_mirip->next()) {
                    $hasil[$ctr]['komponen_id'] = $value->getKomponenId();
                    $hasil[$ctr]['komponen_name'] = $value->getKomponenName();
                    $hasil[$ctr]['komponen_satuan'] = $value->getSatuan();
                    $hasil[$ctr]['komponen_mirip_id'] = $rs_usulan_mirip->getString('komponen_id');
                    $hasil[$ctr]['komponen_mirip_name'] = $rs_usulan_mirip->getString('komponen_name');
                    $hasil[$ctr]['komponen_mirip_satuan'] = $rs_usulan_mirip->getString('satuan');
                    $ctr++;
                }
            }
        }
        //print_r($hasil);exit();
        $this->hasil = $hasil;
    }

    public function executePilihrekeningdibuka() {
        $c_rekening = new Criteria();
        $c_rekening->add(RekeningPeer::REKENING_CODE, '5.2.2.27.%', Criteria::NOT_ILIKE);
        $c_rekening->addAscendingOrderByColumn(RekeningPeer::REKENING_CODE);
        $this->list_rekening = $data_rekening = RekeningPeer::doSelect($c_rekening);

        $this->processFilterspilihrekeningdibuka();
        $this->filters = $this->getUser()->getAttributeHolder()->getAll('sf_admin/pilihrekeningdibuka/filters');

        $pagers = new sfPropelPager('Komponen', 50);
        $c = new Criteria();
        $c->setDistinct();
        $c->add(KomponenPeer::KOMPONEN_TIPE, 'HSPK', Criteria::NOT_EQUAL);
        $c->add(KomponenPeer::KOMPONEN_TIPE, 'SHSD', Criteria::EQUAL);
        $c->add(KomponenPeer::KOMPONEN_ID, '%.F', Criteria::NOT_ILIKE);
        $c->addJoin(KomponenPeer::KOMPONEN_ID, KomponenRekeningPeer::KOMPONEN_ID, Criteria::INNER_JOIN);
        $c->addAscendingOrderByColumn(KomponenPeer::KOMPONEN_NAME);
        $this->addFiltersCriteriapilihrekeningdibuka($c);

        $pagers->setCriteria($c);
        $pagers->setPage($this->getRequestParameter('page', 1));
        $pagers->init();

        $this->pager = $pagers;
    }

    protected function processFilterspilihrekeningdibuka() {
        if ($this->getRequest()->hasParameter('filter')) {
            $filters = $this->getRequestParameter('filters');
            $this->getUser()->getAttributeHolder()->removeNamespace('sf_admin/pilihrekeningdibuka/filters');
            $this->getUser()->getAttributeHolder()->add($filters, 'sf_admin/pilihrekeningdibuka/filters');
        }
    }

    protected function addFiltersCriteriapilihrekeningdibuka($c) {
        if (isset($this->filters['select'])) {
            if ($this->filters['select'] == 1) {
                if (isset($this->filters['nama_komponen_is_empty'])) {
                    $criterion = $c->getNewCriterion(KomponenPeer::KOMPONEN_NAME, '');
                    $criterion->addOr($c->getNewCriterion(KomponenPeer::KOMPONEN_NAME, null, Criteria::ISNULL));
                    $c->add($criterion);
                } else if (isset($this->filters['komponen']) && $this->filters['komponen'] !== '') {
                    $kata = '%' . $this->filters['komponen'] . '%';
                    $c->add(KomponenPeer::KOMPONEN_NAME, strtr($kata, '*', '%'), Criteria::ILIKE);
                }
            } else {
                if (isset($this->filters['komponen_is_empty'])) {
                    echo 'is empty';
                    $criterion = $c->getNewCriterion(KomponenPeer::KOMPONEN_NAME, '');
                    $criterion->addOr($c->getNewCriterion(KomponenPeer::KOMPONEN_NAME, null, Criteria::ISNULL));
                    $c->add($criterion);
                }
            }
        }
    }

    public function executeSaveRekeningDibuka() {
        $ada_komponen = 0;
        if ($this->getRequestParameter('komponen')) {
            $array_komponen = array();
            $array_komponen = $this->getRequestParameter('komponen');
            $ada_komponen = 1;
        }

        if (!$this->getRequestParameter('rekening')) {
            $this->setFlash('gagal', 'MILIH REKENING DONG BOS');
            return $this->redirect('shsd/pilihrekeningdibuka');
        } else if ($ada_komponen == 0) {
            $this->setFlash('gagal', 'MILIH KOMPONEN DONG BOS');
            return $this->redirect('shsd/pilihrekeningdibuka');
        } else {
            $rekening = $this->getRequestParameter('rekening');
            try {
                foreach ($array_komponen as $komponen_loop) {
                    $c_cek_komponen = new Criteria();
                    $c_cek_komponen->add(KomponenPeer::KOMPONEN_ID, $komponen_loop);
                    $dapat_komponen = KomponenPeer::doSelectOne($c_cek_komponen);

                    $c_cek_shsd = new Criteria();
                    $c_cek_shsd->add(ShsdPeer::SHSD_ID, $komponen_loop);
                    $dapat_shsd = ShsdPeer::doSelectOne($c_cek_shsd);
                    if ($dapat_komponen && $dapat_shsd) {
                        $rekening_shsd_sekarang = $dapat_shsd->getRekeningCode();
                        $rekening_komponen_sekarang = $dapat_komponen->getRekening();

                        $rekening_shsd_baru = $rekening_shsd_sekarang . '/' . $rekening;
                        $rekening_komponen_baru = $rekening_komponen_sekarang . '/' . $rekening;

                        $dapat_shsd->setRekeningCode($rekening_shsd_baru);
                        $dapat_shsd->save();

                        $dapat_komponen->setRekening($rekening_komponen_baru);
                        $dapat_komponen->save();

                        $c_insert_komponen_rekening = new KomponenRekening();
                        $c_insert_komponen_rekening->setKomponenId($komponen_loop);
                        $c_insert_komponen_rekening->setRekeningCode($rekening);
                        $c_insert_komponen_rekening->save();
                        budgetLogger::log($this->getUser()->getNamaUser() . ' menambah rekening ' . $rekening . ' pada kode komponen ' . $komponen_loop);
                    } else {
                        $this->setFlash('gagal', 'Komponen tidak ada');
                        return $this->redirect('shsd/pilihrekeningdibuka');
                    }
                }
                $this->setFlash('berhasil', 'Komponen telah dibuka untuk Rekening tersebut.');
                return $this->redirect('shsd/pilihrekeningdibuka');
            } catch (Exception $exc) {
                $this->setFlash('gagal', 'komponen gagal dibuka karena ' . $exc->getMessage());
                return $this->redirect('shsd/pilihrekeningdibuka');
            }
        }
    }

    public function executeList() {
        $this->processFiltersShsd();
        $this->filters = $this->getUser()->getAttributeHolder()->getAll('sf_admin/shsd/filters');

        $pagers = new sfPropelPager('Shsd', 50);
        $c = new Criteria();
        $c->addAscendingOrderByColumn(ShsdPeer::SHSD_ID);
        $this->addFiltersCriteriashsd($c);

        $pagers->setCriteria($c);
        $pagers->setPage($this->getRequestParameter('page', 1));
        $pagers->init();
        $this->pager = $pagers;
    }

    protected function processFiltersShsd() {
        if ($this->getRequest()->hasParameter('filters')) {
            $filters = $this->getRequestParameter('filters');

            $this->getUser()->getAttributeHolder()->removeNAMEspace('sf_admin/shsd/filters');
            $this->getUser()->getAttributeHolder()->add($filters, 'sf_admin/shsd/filters');
        }
    }

    protected function addFiltersCriteriaShsd($c) {
        if ($this->filters['select'] == 1) {
            if (isset($this->filters['shsd_name_is_empty'])) {
                //echo 'name empty';
                $criterion = $c->getNewCriterion(ShsdPeer::REKENING_NAME, '');
                $criterion->addOr($c->getNewCriterion(ShsdPeer::REKENING_NAME, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['shsd_text']) && $this->filters['shsd_text'] !== '') {
                //'name not empty';
                $kata = '%' . $this->filters['shsd_text'] . '%';
                $c->add(ShsdPeer::SHSD_NAME, strtr($kata, '*', '%'), Criteria::ILIKE);
                //$c->add(ShsdPeer::STATUS_HAPUS,FALSE);
            }
        } elseif ($this->filters['select'] == 2) {
            if (isset($this->filters['shsd_id_is_empty'])) {
                $criterion = $c->getNewCriterion(ShsdPeer::SHSD_ID, '');
                $criterion->addOr($c->getNewCriterion(ShsdPeer::SHSD_ID, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['shsd_text']) && $this->filters['shsd_text'] !== '') {
                $kata = '%' . $this->filters['shsd_text'] . '%';
                $c->add(ShsdPeer::SHSD_ID, strtr($kata, '*', '%'), Criteria::ILIKE);
            }
        } else {
            if (isset($this->filters['shsd_text_is_empty'])) {
                $criterion = $c->getNewCriterion(ShsdPeer::REKENING_CODE, '');
                $criterion->addOr($c->getNewCriterion(ShsdPeer::REKENING_CODE, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['shsd_text']) && $this->filters['shsd_text'] !== '') {
                echo $this->filters['shsd_text'];
                $kata = '%' . $this->filters['shsd_text'] . '%';
                $c->add(ShsdPeer::REKENING_CODE, strtr($kata, '*', '%'), Criteria::ILIKE);
            }
        }
    }

    //    ticket 20 - urgent list ssh
    public function executeSshlocked() {
        $this->processFilterssshlocked();
        $this->filters = $this->getUser()->getAttributeHolder()->getAll('sf_admin/sshlocked/filters');

        $pagers = new sfPropelPager('Komponen', 25);
        $c = new Criteria();
        $c->add(KomponenPeer::KOMPONEN_TIPE, 'SHSD', Criteria::EQUAL);
       if (!$this->getUser()->hasCredential('admin') && !$this->getUser()->hasCredential('viewer') && !$this->getUser()->hasCredential('peneliti')) {
           $c->add(KomponenPeer::KOMPONEN_ID, '%.F', Criteria::NOT_ILIKE);
           $c->addOr(KomponenPeer::KOMPONEN_ID, '%.f', Criteria::NOT_ILIKE);
       }
        $c->addJoin(KomponenPeer::KOMPONEN_ID, KomponenRekeningPeer::KOMPONEN_ID, Criteria::INNER_JOIN);
        // $c->addJoin(KomponenPeer::KOMPONEN_ID, ShsdPeer::SHSD_ID, Criteria::INNER_JOIN);
        $c->add(KomponenRekeningPeer::REKENING_CODE, '', Criteria::NOT_EQUAL);
        $c->addAscendingOrderByColumn(KomponenPeer::KOMPONEN_ID);
        $c->setDistinct();
        $this->addFiltersCriteriasshlocked($c);

        $pagers->setCriteria($c);
        $pagers->setPage($this->getRequestParameter('page', 1));
        $pagers->init();

        $this->pager = $pagers;
    }

    protected function processFilterssshlocked() {
        if ($this->getRequest()->hasParameter('filter')) {
            $filters = $this->getRequestParameter('filters');
            $this->getUser()->getAttributeHolder()->removeNamespace('sf_admin/sshlocked/filters');
            $this->getUser()->getAttributeHolder()->add($filters, 'sf_admin/sshlocked/filters');
        }
    }

    protected function addFiltersCriteriasshlocked($c) {
        // var_dump($this->filters);die();
        if (isset($this->filters['select'])) {
            if ($this->filters['select'] == 1) {
                if (isset($this->filters['komponen_name_is_empty'])) {
                    $criterion = $c->getNewCriterion(KomponenPeer::KOMPONEN_NAME, '');
                    $criterion->addOr($c->getNewCriterion(KomponenPeer::KOMPONEN_NAME, null, Criteria::ISNULL));
                    $c->add($criterion);
                } else if (isset($this->filters['komponen']) && $this->filters['komponen'] !== '') {
                    $kata = '%' . $this->filters['komponen'] . '%';
                    // do some join table with shsd and komponen to get hidden spec
                    $cri = new Criteria();
                    $cri->add(ShsdPeer::HIDDEN_SPEC,strtr($kata, '*', '%'), Criteria::ILIKE);
                    $hspec = ShsdPeer::doSelectOne($cri);

                    $criterion = $c->getNewCriterion(KomponenPeer::KOMPONEN_NAME, strtr($kata, '*', '%'), Criteria::ILIKE);
                    if($hspec)
                        $criterion->addOr($c->getNewCriterion(KomponenPeer::SHSD_ID,$hspec->getShsdId(), Criteria::EQUAL));

                    $c->add($criterion);
                }
            } elseif ($this->filters['select'] == 2) {
                if (isset($this->filters['rekening_code_is_empty'])) {
                    $criterion = $c->getNewCriterion(KomponenPeer::REKENING, '');
                    $criterion->addOr($c->getNewCriterion(KomponenPeer::REKENING, null, Criteria::ISNULL));
                    $c->add($criterion);
                } else if (isset($this->filters['komponen']) && $this->filters['komponen'] !== '') {
                    $kata = '%' . $this->filters['komponen'] . '%';
                    $c->add(KomponenPeer::REKENING, strtr($kata, '*', '%'), Criteria::ILIKE);
                }
            } elseif ($this->filters['select'] == 3) {
                if (isset($this->filters['komponen_code_is_empty'])) {
                    $criterion = $c->getNewCriterion(KomponenPeer::KOMPONEN_ID, '');
                    $criterion->addOr($c->getNewCriterion(KomponenPeer::KOMPONEN_ID, null, Criteria::ISNULL));
                    $c->add($criterion);
                } else if (isset($this->filters['komponen']) && $this->filters['komponen'] !== '') {
                    $kata = '%' . $this->filters['komponen'] . '%';
                     if (!$this->getUser()->hasCredential('admin')) {
                         $c->add(KomponenPeer::KOMPONEN_ID, '%.F', Criteria::NOT_ILIKE);
                         $c->addOr(KomponenPeer::KOMPONEN_ID, '%.f', Criteria::NOT_ILIKE);
                         if (strpos($kata, '.f') === false) 
                         {
                            $kata=strtr($kata, '.F', '% ');
                        }
                         else
                         {
                            $kata=strtr($kata, '.f', '% ');
                         }
                    }                                      
                    $c->add(KomponenPeer::KOMPONEN_ID, strtr($kata, '*', '%'), Criteria::ILIKE);
                }
            } else {
                if (isset($this->filters['komponen_is_empty'])) {
                    echo 'is empty';
                    $criterion = $c->getNewCriterion(KomponenPeer::KOMPONEN_NAME, '');
                    $criterion->addOr($c->getNewCriterion(KomponenPeer::KOMPONEN_NAME, null, Criteria::ISNULL));
                    $c->add($criterion);
                }
            }
        }
    }

    //    ticket 20 - urgent list ssh
    public function executeUbahPass() {
        if ($this->getRequest()->getMethod() == sfRequest::POST) {
            $member = MasterUserV2Peer::retrieveByPK($this->getUser()->getNamaUser());

            $member->setUserPassword(md5($this->getRequestParameter('pass_baru')));
            budgetLogger::log('Username ' . $this->getUser()->getNamaUser() . 'Merubah password');
            $member->save();
            $this->habis_ganti = true;
        }
    }

    public function handleErrorUbahPass() {
        return sfView::SUCCESS;
    }

}
