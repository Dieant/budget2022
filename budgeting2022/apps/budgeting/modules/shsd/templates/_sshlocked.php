<div class="card-body table-responsive p-0">
    <table class="table table-hover text-nowrap">
        <thead class="head_peach">
            <tr>
                <th>Kode Komponen</th>
                <th>Nama Komponen</th>
                <th>Spesifikasi Tambahan</th>
                <th>Satuan</th>
                <th>Harga</th>
                <th>Pajak</th>
                <th>Rekening</th>        
            </tr>
        </thead>
        <tbody>
            <?php
            $i = 1;
            foreach ($pager->getResults() as $sshlocked): $odd = fmod(++$i, 2)
            ?>
                <tr>
                    <td><?php echo $sshlocked->getKomponenId() ?></td>
                    <td><?php
                        echo $sshlocked->getKomponenName();
                        $con = Propel::getConnection();
                        $query = "select max(tahap) as tahap from " . sfConfig::get('app_default_schema') . ".komponen";
                        $stmt = $con->prepareStatement($query);
                        $rs = $stmt->executeQuery();
                        if ($rs->next()) {
                            if ($sshlocked->getTahap() == $rs->getString('tahap') && sfConfig::get('app_tahap_edit') != 'murni') {
                                echo image_tag('/images/newanima.gif');
                            }
                        }
                        ?></td>
                    <td style="text-align: center">
                        <?php
                        $con = Propel::getConnection();
                        $query = "select komponen_id, hidden_spec from " . sfConfig::get('app_default_schema') . ".komponen_all
                                    where komponen_id = '".$sshlocked->getKomponenId()."' and komponen_name ilike '%".$sshlocked->getKomponenName()."%'";
                        $stmt = $con->prepareStatement($query);
                        $rs = $stmt->executeQuery();
                        if ($rs->next()) {
                            if ($sshlocked->getShsdId() == $rs->getString('komponen_id')) {
                                if($rs->getString('komponen_id') != '')
                                    echo $rs->getString('hidden_spec');
                            }
                        }
                        ?>
                    </td>
                    <td style="text-align: center"><?php echo $sshlocked->getSatuan() ?></td>
                    <td style="text-align: right">
                        <?php
                        // if ($sshlocked->getKomponenHarga() != floor($sshlocked->getKomponenHarga())) {
                        //     echo number_format($sshlocked->getKomponenHarga(), 2, ',', '.');
                        // } elseif ($sshlocked->getSatuan() != '%') {
                        //     echo number_format($sshlocked->getKomponenHarga());
                        // } else {
                        //     echo $sshlocked->getKomponenHarga();
                        // }

                        //2565454545.4545
                        
                        //2.565.454.545,45
                        // if ($sshlocked->getKomponenHarga() != floor($sshlocked->getKomponenHarga())) {
                        //     $a = $sshlocked->getKomponenHarga();
                        //     $b = round($a);
                        //     echo number_format($b, 4, ',', '.');

                        // } elseif ($sshlocked->getSatuan() != '%') {
                        //     echo number_format($sshlocked->getKomponenHarga());
                        // } else {
                        //     echo $sshlocked->getKomponenHarga();
                        // }
                        // if ($sshlocked->getKomponenHarga() > 1) {

                        // }
                        if ($sshlocked->getKomponenHarga() != floor($sshlocked->getKomponenHarga()) && $sshlocked->getKomponenHarga() < 1) {
                            echo number_format($sshlocked->getKomponenHarga(), 4, ',', '.');
                        } elseif ($sshlocked->getSatuan() != '%') {
                            if(strpos($sshlocked->getKomponenHarga(),'.') > 1) {
                                echo number_format($sshlocked->getKomponenHarga(), 3, ',', '.');
                            }
                            else {
                                echo number_format($sshlocked->getKomponenHarga(), 0, ',', '.');
                            }
                        } else {
                            echo number_format($sshlocked->getKomponenHarga(), 0, ',', '.');
                        }
                        ?>
                    </td>
                    <td style="text-align: center">
                        <?php
                        if ($sshlocked->getKomponenNonPajak() == 'true') {
                            echo '0 %';
                        } else {
                            echo '10 %';
                        }
                        ?>
                    </td>
                    <td style="text-align: center">
                        <?php
                        $array_rekening = explode('/', $sshlocked->getRekening());
                        if (count($array_rekening) > 1) {
                            foreach ($array_rekening as $value) {
                                echo $value . ' (' . RekeningPeer::getStringRekeningName($value) . ')<br/>';
                            }
                        } else {
                            echo $sshlocked->getRekening() . ' (' . RekeningPeer::getStringRekeningName($sshlocked->getRekening()) . ')';
                        }
                        ?>
                    </td>  
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
<div class="card-footer clearfix">
    <ul class="pagination pagination-sm m-0 float-left">
        <?php
            echo format_number_choice('[0] no result|[1] 1 result|(1,+Inf] %1% results', array('%1%' => $pager->getNbResults()), $pager->getNbResults()) 
        ?>
    </ul>
    <ul class="pagination pagination-sm m-0 float-right">
        <?php 
        if ($pager->haveToPaginate()): 
            echo '<li class="page-item">'.link_to('Previous', 'shsd/sshlocked?page=' . $pager->getPreviousPage(), array('class' => 'page-link', 'align' => 'absmiddle', 'alt' => __('Previous'), 'title' => __('Previous'))).'</li>';

            foreach ($pager->getLinks() as $page):
                $activeclass = ($page == $pager->getPage()) ? 'active' : '';
                echo '<li class="page-item '.$activeclass.'">'.link_to_unless($page == $pager->getPage(), $page, "shsd/sshlocked?page={$page}", array('class' => 'page-link')).'</li>';
            endforeach;
            echo '<li class="page-item">'.link_to('Next', 'shsd/sshlocked?page=' . $pager->getNextPage(), array('class' => 'page-link', 'align' => 'absmiddle', 'alt' => __('Next'), 'title' => __('Next'))).'</li>'; 
            endif;
        ?>
    </ul>
</div>