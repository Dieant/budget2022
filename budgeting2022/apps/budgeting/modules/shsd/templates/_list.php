<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<table cellspacing="0" class="sf_admin_list">    
    <thead>
        <?php include_partial('shsd/list_header'); ?>
    </thead>
    <tbody>
        <?php
        $i = 1;
        foreach ($pager->getResults() as $komponen): $odd = fmod(++$i, 2)
            ?>
            <tr class="sf_admin_row_<?php echo $odd ?>">
                <td><?php echo $komponen->getShsdId() ?></td>
                <td><?php echo $komponen->getShsdName() ?></td>
                <td><?php echo $komponen->getSpec() ?></td>
                <td style="text-align: right">
                    <?php echo number_format($komponen->getShsdHarga(), 2, ',', '.') ?>
                </td>
                <td style="text-align: center">
                    <?php if($komponen->getKomponenNonPajak()=='false'){
                        echo '0 %';
                    }else{
                        echo '10 %';
                    } ?>
                </td>
                <td style="text-align: center"><?php echo $komponen->getRekeningCode() ?></td>
            </tr>
        <?php endforeach; ?>
    </tbody>
    <tfoot>
        <tr><th colspan="6">
    <div class="float-right">
        <?php if ($pager->haveToPaginate()): ?>
            <?php echo link_to(image_tag(sfConfig::get('sf_admin_web_dir') . '/images/first.png', array('align' => 'absmiddle', 'alt' => __('First'), 'title' => __('First'))), 'shsd/list?page=1') ?>
            <?php echo link_to(image_tag(sfConfig::get('sf_admin_web_dir') . '/images/previous.png', array('align' => 'absmiddle', 'alt' => __('Previous'), 'title' => __('Previous'))), 'shsd/list?page=' . $pager->getPreviousPage()) ?>

            <?php foreach ($pager->getLinks() as $page): ?>
                <?php echo link_to_unless($page == $pager->getPage(), $page, 'shsd/list?page=' . $page) ?>
            <?php endforeach; ?>

            <?php echo link_to(image_tag(sfConfig::get('sf_admin_web_dir') . '/images/next.png', array('align' => 'absmiddle', 'alt' => __('Next'), 'title' => __('Next'))), 'shsd/list?page=' . $pager->getNextPage()) ?>
            <?php echo link_to(image_tag(sfConfig::get('sf_admin_web_dir') . '/images/last.png', array('align' => 'absmiddle', 'alt' => __('Last'), 'title' => __('Last'))), 'shsd/list?page=' . $pager->getLastPage()) ?>
        <?php endif; ?>
    </div>
    <?php echo format_number_choice('[0] no result|[1] 1 result|(1,+Inf] %1% results', array('%1%' => $pager->getNbResults()), $pager->getNbResults()) ?>
</th></tr>
</tfoot>
</table>
