<?php use_helper('I18N', 'Date', 'Object', 'Javascript', 'Validation'); ?>
<section class="content-header">
    <h1>List SHSD Mirip</h1>
</section>

<!-- Main content -->
<section class="content">
    <?php include_partial('usulan_ssh/list_messages'); ?>
    <!-- Default box -->
    <div class="box box-primary box-solid">
        <div class="box-body">
            <div id="sf_admin_container" class="table-responsive">
                <table cellspacing="0" class="sf_admin_list">    
                    <thead>
                        <tr>
                            <th colspan="6">
                                <div class="float-right">
                                    <?php echo link_to(image_tag(sfConfig::get('sf_admin_web_dir') . '/images/first.png', array('align' => 'absmiddle', 'alt' => __('First'), 'title' => __('First'))), 'shsd/shsdMirip?page=1'). ' '; ?>
                                    <?php echo link_to(image_tag(sfConfig::get('sf_admin_web_dir') . '/images/previous.png', array('align' => 'absmiddle', 'alt' => __('Previous'), 'title' => __('Previous'))), 'shsd/shsdMirip?page=' . (($page - 1 < 1) ? '1' : $page - 1)). ' '; ?>
                                    <?php
                                    if ($jml_page < 5) {
                                        for ($i = 1; $i <= $jml_page; $i++) {
                                            echo link_to($i, 'shsd/shsdMirip?page=' . $i). ' ';
                                        }
                                    } else {
                                        if ($page <= 2) {
                                            for ($i = 1; $i <= 5; $i++) {
                                                echo link_to($i, 'shsd/shsdMirip?page=' . $i). ' ';
                                            }
                                        } else if ($page >= $jml_page - 2) {
                                            for ($i = $jml_page - 5; $i <= $jml_page; $i++) {
                                                echo link_to($i, 'shsd/shsdMirip?page=' . $i). ' ';
                                            }
                                        } else {
                                            for ($i = $page - 2; $i <= $page + 2; $i++) {
                                                echo link_to($i, 'shsd/shsdMirip?page=' . $i). ' ';
                                            }
                                        }
                                    }
                                    ?>
                                    <?php echo link_to(image_tag(sfConfig::get('sf_admin_web_dir') . '/images/next.png', array('align' => 'absmiddle', 'alt' => __('Next'), 'title' => __('Next'))), 'shsd/shsdMirip?page=' . (($page == $jml_page) ? $jml_page : $page + 1)). ' '; ?>
                                    <?php echo link_to(image_tag(sfConfig::get('sf_admin_web_dir') . '/images/last.png', array('align' => 'absmiddle', 'alt' => __('Last'), 'title' => __('Last'))), 'shsd/shsdMirip?page=' . $jml_page). ' '; ?>
                                </div>
                            </th>
                        </tr>
                        <tr class="sf_admin_row_3">
                            <td colspan="6"></td>
                        </tr>
                        <tr>
                            <th>Id Komponen</th>
                            <th>Satuan</th>
                            <th>Nama Komponen</th>
                            <th>Nama Komponen Mirip</th>
                            <th>Satuan Mirip</th>
                            <th>Id Komponen Mirip</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = 1;
                        foreach ($hasil as $value): $odd = fmod( ++$i, 2);
                            ?>
                            <tr class="sf_admin_row_<?php echo $odd ?>">
                                <td><?php echo $value['komponen_id'] ?></td>
                                <td><?php echo $value['komponen_satuan'] ?></td>
                                <td><?php echo $value['komponen_name'] ?></td>
                                <td><?php echo $value['komponen_mirip_name'] ?></td>
                                <td><?php echo $value['komponen_mirip_satuan'] ?></td>
                                <td><?php echo $value['komponen_mirip_id'] ?></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                    <tfoot>
                    <th colspan="6">
                        <div class="float-right">
                            <?php echo link_to(image_tag(sfConfig::get('sf_admin_web_dir') . '/images/first.png', array('align' => 'absmiddle', 'alt' => __('First'), 'title' => __('First'))), 'shsd/shsdMirip?page=1'). ' '; ?>
                            <?php echo link_to(image_tag(sfConfig::get('sf_admin_web_dir') . '/images/previous.png', array('align' => 'absmiddle', 'alt' => __('Previous'), 'title' => __('Previous'))), 'shsd/shsdMirip?page=' . (($page - 1 < 1) ? '1' : $page - 1)). ' '; ?>
                            <?php
                            if ($jml_page < 5) {
                                for ($i = 1; $i <= $jml_page; $i++) {
                                    echo link_to($i, 'shsd/shsdMirip?page=' . $i). ' ';
                                }
                            } else {
                                if ($page <= 2) {
                                    for ($i = 1; $i <= 5; $i++) {
                                        echo link_to($i, 'shsd/shsdMirip?page=' . $i). ' ';
                                    }
                                } else if ($page >= $jml_page - 2) {
                                    for ($i = $jml_page - 5; $i <= $jml_page; $i++) {
                                        echo link_to($i, 'shsd/shsdMirip?page=' . $i). ' ';
                                    }
                                } else {
                                    for ($i = $page - 2; $i <= $page + 2; $i++) {
                                        echo link_to($i, 'shsd/shsdMirip?page=' . $i). ' ';
                                    }
                                }
                            }
                            ?>
                            <?php echo link_to(image_tag(sfConfig::get('sf_admin_web_dir') . '/images/next.png', array('align' => 'absmiddle', 'alt' => __('Next'), 'title' => __('Next'))), 'shsd/shsdMirip?page=' . (($page == $jml_page) ? $jml_page : $page + 1)). ' '; ?>
                            <?php echo link_to(image_tag(sfConfig::get('sf_admin_web_dir') . '/images/last.png', array('align' => 'absmiddle', 'alt' => __('Last'), 'title' => __('Last'))), 'shsd/shsdMirip?page=' . $jml_page). ' '; ?>
                        </div>
                    </th>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</section>
