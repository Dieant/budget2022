<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<?php use_helper('I18N', 'Date', 'Object', 'Javascript', 'Validation') ?>

<div id="sf_admin_container">

    <h1><?php echo __('SHSD', array())
?></h1>


    <?php echo form_tag('shsd/list', array('method' => 'get')) ?>
    <fieldset>
        <h2><?php echo __('Cari') ?></h2>
        <div class="form-row">
<!--              <select name="search_option" class="inpText" id="search_option">
                        <option value="shsd_name">Nama Komponen</option>
                        <option value="shsd_id">Kode Komponen</option>
                        <option value="shsd_rek">Rekening</option>
                        
          </select>-->

            <?php
            $arrOptions = array(
                1 => 'Nama Komponen',
                2 => 'Kode Komponen'
            );
            echo select_tag('filters[select]', options_for_select($arrOptions, @$filters['select'] ? $filters['select'] : '', array('include_blank' => true)));
            echo input_tag('filters[shsd_text]', isset($filters['shsd_text']) ? $filters['shsd_text'] : null, array('class' => 'inpText', 'width' => '500'))
            ?></div>

    </fieldset>
    <ul class="sf_admin_actions">
        <li><?php echo button_to(__('reset'), 'shsd/list?filters=filter', 'class=sf_admin_action_reset_filter') ?></li>
        <li><?php echo submit_tag(__('cari'), 'name=filter class=sf_admin_action_filter') ?></li>
    </ul>
</form>
<br/>

<div id="sf_admin_header">


    <?php if (!$pager->getNbResults()): ?>
        <?php echo __('no result') ?>
    <?php else: ?>
        <?php include_partial('shsd/list', array('pager' => $pager)) ?>
    <?php endif; ?>


</div>
