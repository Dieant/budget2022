<?php

/**
 * bappekoprk actions.
 *
 * @package    budgeting
 * @subpackage bappekoprk
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 2692 2006-11-15 21:03:55Z fabien $
 */
class bappekoprkActions extends sfActions {

    /**
     * Executes index action
     *
     */
    public function executeIndex() {
        $this->forward('default', 'module');
    }

}
