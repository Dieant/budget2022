<?php use_helper('I18N', 'Date', 'Object', 'Javascript', 'Validation') ?>
<?php

 $menu = $sf_params->get('menu');

if (sfConfig::get('app_tahap_edit') == 'murni') {
   if($menu == 'Pendapatan')
        $nama_sistem = 'Pendapatan Daerah';
        else
        $nama_sistem = 'Belanja Daerah';
} elseif (sfConfig::get('app_tahap_edit') == 'pak') {
    $nama_sistem = 'PAK';
} elseif (sfConfig::get('app_tahap_edit') == 'penyesuaian') {
    $nama_sistem = 'Penyesuaian';
}else {
    $nama_sistem = 'Revisi';
}
?>
<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Sub Kegiatan - (<?php echo $nama_sistem ?>)</h1>
    </div>
    <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Lembar Kerja</a></li>
          <li class="breadcrumb-item active"><?php echo $nama_sistem ?></li>
      </ol>
  </div>
</div>
</div><!-- /.container-fluid -->
</section>
<!-- Main content -->
<section class="content">
    <?php include_partial('entri/list_messages'); ?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="fas fa-filter"></i> Filter
                        </h3>
                    </div>
                    <div class="card-body">
                            <?php echo form_tag('entri/list?menu='.$menu, array('method' => 'get', 'class' => 'form-horizontal','tahap' => $filters['tahap'],'menu' => $menu)) ?>                        
                            <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Nama sub kegiatan</label>
                                    <?php
                                    echo input_tag('filters[nama_kegiatan]', isset($filters['nama_kegiatan']) ? $filters['nama_kegiatan'] : null, array('class' => 'form-control'));
                                    ?>
                                </div>
                            </div>
                            <!-- /.col -->
                            <?php if (sfConfig::get('app_tahap_edit') != 'murni') { ?>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Tahap</label>
                                        <?php
                                        echo select_tag('filters[tahap]', options_for_select(
                                            array(
                                                // 'murnibp' => 'Murni (Buku Putih)',
                                                // 'murnibb' => 'Murni (Buku Biru)', 
                                                // 'murnipp' => 'Murni (Penyesuaian)',                                  
                                                'murni' => 'Murni',
                                                //  'revisi1' => 'Revisi 1',
                                                // 'revisi1_1' => 'Penyesuaian Komponen 1',
                                                //  'revisi2' => 'Revisi 2',
                                                // 'revisi2_1' => 'Penyesuaian Komponen 2',
                                                // 'revisi2_2' => 'Penyesuaian Komponen 3',
                                                //  'revisi3_0' => 'Revisi 3',
                                                //  'revisi3_1' => 'Revisi 3.1',
                                                //  'revisi3' => 'Revisi 3.2',
                                                // 'revisi3_1' => 'Penyesuaian Komponen 4',
                                                //  'revisi4' => 'Revisi 4',
                                                //  'revisi5' => 'Revisi 5',
                                                //  'revisi6' => 'Revisi 6',
                                                //  'pakbp'   => 'PAK Buku Putih'
                                            ),
                                            isset($filters['tahap']) ? $filters['tahap'] : '', array('include_custom' => '--Pilih Tahap--')), array('class' => 'js-example-basic-single form-control select2'));
                                            ?>
                                        </div>
                                    </div>
                                    <!-- /.col -->
                                <?php } ?>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="tombol_filter">Tombol Filter</label><br/>
                                        <button type="submit" name="filter" class="btn btn-outline-primary btn-sm">Filter <i class="fas fa-search"></i></button>
                                        <?php
                                        echo link_to('Reset <i class="fa fa-backspace"></i>', 'entri/list?filter=filter', array('class' => 'btn btn-outline-danger btn-sm'))."&nbsp;";
                                        if($sf_user->hasCredential('pa') && !empty($unit_id) && $kegiatan_bawah < 0 && is_null($filters['tahap'])) {
                                            //sementara tutup lpj 060421
                                            echo '<button type="button" class="btn btn-outline-success btn-sm" data-toggle="modal" data-target="#modal-pertanggungjawaban">Print Lembar Pertanggungjawaban Pergeseran Anggaran <i class="fa fa-print"></i></button>&nbsp;';
                                        } 
                                        else if($sf_user->hasCredential('pa') && !empty($unit_id) && $filters['tahap'] )
                                        {
                                            echo '<button type="button" class="btn btn-outline-success btn-sm" data-toggle="modal" data-target="#modal-pertanggungjawaban-tahap">Print Lembar Pertanggungjawaban Pergeseran Anggaran <i class="fa fa-print"></i></button>&nbsp;';
                                        } 
                                        ?>
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                                <!-- /.col -->                      
                            </div>
                            <?php 
                            echo '</form>';
                            $penyelia = '';
                            $bappeko = '';
                            $c = new Criteria();
                            $user_id = $sf_user->getNamaUser();

                            $d = new Criteria();
                            $d->add(UnitKerjaPeer::UNIT_NAME, $user_id);
                            $rs_unitkerja = UnitKerjaPeer::doSelectOne($d);
                            if ($rs_unitkerja) {
                                $con = Propel::getConnection();
                                $unit_id = $rs_unitkerja->getUnitId();

                                $query = "select u.user_id, u.user_name from master_user_v2 u, user_handle_v2 h, schema_akses_v2 a, ebudget.master_penyelia p
                                where u.user_id=h.user_id and u.user_id=a.user_id and a.level_id=2 and h.unit_id='$unit_id' and u.user_enable=true and u.user_id=p.username";
                                $stmt = $con->prepareStatement($query);
                                $rs_penyelia = $stmt->executeQuery();
                                $satu = true;
                                while ($rs_penyelia->next()) {
                                    if ($satu) {
                                        $satu = false;
                                        $penyelia.=$rs_penyelia->getString('user_name');
                                    } else {
                                        $penyelia.=', ' . $rs_penyelia->getString('user_name');
                                    }
                                }

                                $query = "select u.user_id, u.user_name from master_user_v2 u, user_handle_v2 h, schema_akses_v2 a
                                where u.user_id=h.user_id and u.user_id=a.user_id and a.level_id=16 and h.unit_id='$unit_id' and u.user_enable=true";
                                $stmt = $con->prepareStatement($query);
                                $rs_bappeko = $stmt->executeQuery();
                                $satu = true;
                                while ($rs_bappeko->next()) {
                                    if ($satu) {
                                        $satu = false;
                                        $bappeko.=$rs_bappeko->getString('user_name');
                                    } else {
                                        $bappeko.=', ' . $rs_bappeko->getString('user_name');
                                    }
                                }

                                $c = new Criteria();
                                $c->add(UnitKerjaPeer::UNIT_ID, $unit_id);
                                $skpd = UnitKerjaPeer::doSelectOne($c);
                                if ($penyelia && $bappeko)
                                    echo '<strong>Mitra/Penyelia ' . $skpd->getUnitName() . ': <span class="badge badge-primary">[ ' . $bappeko . ', ' . $penyelia . ' ]</span></strong>';
                                elseif ($penyelia)
                                    echo '<strong>Mitra/Penyelia ' . $skpd->getUnitName() . ': <span class="badge badge-primary">[ ' . $penyelia . ' ]</span></strong>';
                                elseif ($bappeko)
                                    echo '<strong>Mitra/Penyelia ' . $skpd->getUnitName() . ': <span class="badge badge-primary">[ ' . $bappeko . ' ]</span></strong>';
                            }
                            ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 stretch-card">
                <div class="card">                 
                <?php if (!$pager->getNbResults()): ?>
                    <?php echo __('no result') ?>
                    <?php else: ?>
                        <?php include_partial('entri/list', array('pager' => $pager,'filters' => $filters)) ?>
                    <?php endif; ?>            
                </div>
            </div>
        </div>
    </div>
</section>

<div id="modal-pertanggungjawaban" class="modal fade" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myModalLabel">Print Lembar Pertanggungjawaban Pergeseran Anggaran</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" style="text-align: center; margin: 20px;">
                <?php echo form_tag('report/printPertanggungjawabanMutlakPergeseranAnggaran', array('method' => 'post', 'class' => 'form-horizontal')); ?>
                <div class="form-group">
                    <label class="control-label">Pilih Tanggal : &nbsp;&nbsp;&nbsp;</label>
                    <input type="date" name="tanggal" value="<?php echo date('Y-m-d'); ?>">
                    <input type="hidden" name="unit_id" value="<?php echo $unit_id; ?>">
                </div>
                <input type="submit" name="print" value="CETAK" class="btn btn-flat btn-success">
                <?php
                echo "</form>";
                ?>
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>

<div id="modal-pertanggungjawaban-tahap" class="modal fade" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="myModalLabel" style="text-align: left;">Print Lembar Pertanggungjawaban Pergeseran Anggaran</h4>
            </div>
            <div class="modal-body" style="text-align: center; margin: 20px;">
                <?php echo form_tag('report/printPertanggungjawabanMutlakPergeseranAnggaran', array('method' => 'post', 'class' => 'form-horizontal')); ?>
                <div class="form-group">
                    <label class="control-label">Pilih Tanggal : &nbsp;&nbsp;&nbsp;</label>
                    <input type="date" name="tanggal" value="<?php echo date('Y-m-d'); ?>">
                    <input type="hidden" name="unit_id" value="<?php echo $unit_id; ?>">
                    <input type="hidden" name="tahap" value="<?php echo $filters['tahap']; ?>">
                    <input type="hidden" name="user" value="<?php echo $sf_user->getNamaLogin() ?>">
                </div>
                <input type="submit" name="print" value="CETAK" class="btn btn-flat btn-success">
                <?php
                echo "</form>";
                ?>
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>