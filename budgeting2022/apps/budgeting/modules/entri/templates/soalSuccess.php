<?php use_helper('I18N', 'Form', 'Object', 'Javascript', 'Validation') ?>


<!-- Main content -->
<section class="content">
    <?php include_partial('anggaran/list_messages'); ?>
    <div class="box box-success box-solid">
        <div class="box-header with-border">
            <h3 class="box-title"><b>Silahkan Jawab Pertanyaan Berikut</b></h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div><!-- /.box-tools -->
        </div><!-- /.box-header -->
        <div class="box-body">
            <?php echo form_tag('entri/saveSoal', array('multipart' => false, 'class' => 'form-horizontal')); ?>
            <?php echo input_hidden_tag('kode_kegiatan', $kode_kegiatan) ?>
            <?php echo input_hidden_tag('unit_id', $unit_id) ?>
            <ul class="list-group">

                <li class="list-group-item">
                    <span class="pull-left col-xs-12" style="text-align:justify">&nbsp;Apakah anda akan merubah output kegiatan / output subtitle?</span>
                    <br>
                    <span class="pull-left col-xs-2" style="text-align:justify">&nbsp;<?php echo radiobutton_tag('jawaban1', 'true', false, array('required' => 'true')) ?>Ya</span>
                    <span class="pull-left col-xs-2" style="text-align:justify">&nbsp;<?php echo radiobutton_tag('jawaban1', 'false'); ?>Tidak</span>

                    <div class="clearfix"></div>
                </li>
                <li class="list-group-item">
                    <span class="pull-left col-xs-12" style="text-align:justify">&nbsp;Apakah anda akan merubah judul subtitle?</span>
                    <br>
                    <span class="pull-left col-xs-2" style="text-align:justify">&nbsp;<?php echo radiobutton_tag('jawaban2', 'true', false, array('required' => 'true')) ?>Ya</span>
                    <span class="pull-left col-xs-2" style="text-align:justify">&nbsp;<?php echo radiobutton_tag('jawaban2', 'false', false) ?>Tidak</span>

                    <div class="clearfix"></div>
                </li>
                <li class="list-group-item">
                    <span class="pull-left col-xs-12" style="text-align:justify">&nbsp;Catatan:</span>
                    <span class="pull-left col-xs-12" style="text-align:justify">
                        <textarea name="catatan_dinas" placeholder="Isikan catatan" class="form-control" rows="8"></textarea>
                    <?php use_javascript('/AdminLTE/plugins/tinymce/tinymce.min.js') ?>
                    <script>
                        tinymce.init({
                            selector: 'textarea',
                            menubar: 'edit view format',
                            plugins: "paste",
                            paste_as_text: true
                        });
                    </script>
                    </span>
                    <div class="clearfix"></div>
                </li>
                
            </ul>
        </div><!-- /.box-body -->
        <div class="box-footer" style="text-align: right">
            <?php echo submit_tag('Selesai', array('align' => 'center', 'id' => 'submit', 'name' => 'submit', 'class' => 'btn btn-flat btn-success')) ?>
            <?php echo '</form>'; ?>
        </div>
    </div><!-- /.box -->
</section>