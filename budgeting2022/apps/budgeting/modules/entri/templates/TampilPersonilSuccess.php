<?php

echo form_tag('entri/tambahPersonilRka?kegiatan_code='.$kegiatan_code.'&subtitle='.$subtitle,Array('method'=>'post', 'id'=>'personil' ))
?>
<div id="pegawai">
    <h1>Pilih Pegawai</h1>
    <table width="80%" border="1" cellpadding="0" cellspacing="0" >
  <tr>
    <td width="15%"><div align="center"><strong>NIP</strong></div></td>
    <td width="25%"><div align="center"><strong>Nama</strong></div></td>
    <td width="25%"><div align="center"><strong>Instansi</strong></div></td>
    <td width="30%"><div align="center"><strong>Sub</strong></div></td>
    <td width="5%"><div align="center"><strong>Pilih</strong></div></td>
  </tr>
  <?php
        $c = new Criteria();
		$c->add(PegawaiPeer::UNIT_ID, $unit_id);
        $c->addAscendingOrderByColumn(PegawaiPeer::NIP);
		$x = PegawaiPeer::doSelect($c);
		foreach ($x as $pegawai){
?>
  <tr>
    <td><div align="center"><?php echo $pegawai->getNip();?></div></td>
    <td><div align="center"><?php echo $pegawai->getNama();?></div></td>
    <td><div align="center"><?php echo $pegawai->getNamaInstansi();?></div></td>
    <td><div align="center"><?php echo $pegawai->getNamaUnitKerja();?></div></td>
    <!--<td><div align="center"><?php echo link_to(image_tag('/images/up.png'),'entri/tambahPersonilRka?act=tambahPersonilRka&unit_id='.$unit_id.'&kegiatan_code='.$kegiatan_code.'&subtitle='.$subtitle.'&nip='.$pegawai->getNip().'&nama='.$pegawai->getNama(),array('alt'=>'Pilih Personil','title' => 'Pilih personil'));?></div></td>-->
	<td><div align="center"><?php echo checkbox_tag('pegawai[]',$pegawai->getNip())?></div></td>
	</tr>
	
  <?php }?>
  <tr>
		<td colspan='5' align ='right'><?php echo submit_tag('save', array('name'=>'addPersonil')) ?></td>
	</tr>
</table>

    <br><br><br>
</div>