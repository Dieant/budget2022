<?php //use_helper('Url', 'Javascript', 'Form', 'Object'); ?>
<?php use_helper('I18N', 'Date', 'Url', 'Javascript', 'Form', 'Object') ?>
<?php
if ($tahap == 'pakbp') {
    $tabel_dpn = 'pak_bukuputih_';
    $status = 'LOCK';
} elseif ($tahap == 'pakbb') {
    $tabel_dpn = 'pak_bukubiru_';
    $status = 'LOCK';
} elseif ($tahap == 'murni') {
    $tabel_dpn = 'murni_';
    $status = 'LOCK';
} elseif ($tahap == 'murnibp') {
    $tabel_dpn = 'murni_bukuputih_';
    $status = 'LOCK';
} elseif ($tahap == 'murnibb') {
    $tabel_dpn = 'murni_bukubiru_';
    $status = 'LOCK';
} elseif ($tahap == 'revisi1') {
    $tabel_dpn = 'revisi1_';
    $status = 'LOCK';
} elseif ($tahap == 'revisi1_1') {
    $tabel_dpn = 'revisi1_1_';
    $status = 'LOCK';
} elseif ($tahap == 'revisi2') {
    $tabel_dpn = 'revisi2_';
    $status = 'LOCK';
}  elseif ($tahap == 'revisi2_1') {
    $tabel_dpn = 'revisi2_1_';
    $status = 'LOCK';
} elseif ($tahap == 'revisi2_2') {
    $tabel_dpn = 'revisi2_2_';
    $status = 'LOCK';
} elseif ($tahap == 'revisi3') {
    $tabel_dpn = 'revisi3_';
    $status = 'LOCK';
} elseif ($tahap == 'revisi3_0') {
    $tabel_dpn = 'revisi3_0_';
    $status = 'LOCK';
} elseif ($tahap == 'revisi3_1') {
    $tabel_dpn = 'revisi3_1_';
    $status = 'LOCK';
} elseif ($tahap == 'revisi4') {
    $tabel_dpn = 'revisi4_';
    $status = 'LOCK';
} elseif ($tahap == 'revisi5') {
    $tabel_dpn = 'revisi5_';
    $status = 'LOCK';
} elseif ($tahap == 'revisi6') {
    $tabel_dpn = 'revisi6_';
    $status = 'LOCK';
} elseif ($tahap == 'revisi7') {
    $tabel_dpn = 'revisi7_';
    $status = 'LOCK';
} elseif ($tahap == 'revisi8') {
    $tabel_dpn = 'revisi8_';
    $status = 'LOCK';
} elseif ($tahap == 'revisi9') {
    $tabel_dpn = 'revisi9_';
    $status = 'LOCK';
} elseif ($tahap == 'revisi10') {
    $tabel_dpn = 'revisi10_';
    $status = 'LOCK';
} elseif ($tahap == 'rkua') {
    $tabel_dpn = 'rkua_';
    $status = 'LOCK';
} else {
    $tabel_dpn = 'dinas_';
    $status = 'LOCK';
    $c = new Criteria();
    $c->add(DinasRincianPeer::KEGIATAN_CODE, $kode_kegiatan);
    $c->add(DinasRincianPeer::UNIT_ID, $unit_id);
    $rs_rinciankegiatan = DinasRincianPeer::doSelectOne($c);
    $v = DinasRincianPeer::doSelectOne($c);
    if ($v) {
        if ($v->getLock() == TRUE) {
            $status = 'LOCK';
        } else if ($v->getRincianLevel() == 1) {
            $status = 'LOCK';
            $status_jk = 'OPEN';
        } else if ($v->getRincianLevel() == 2) {
            $status = 'OPEN';
        } else if ($v->getRincianLevel() == 3) {
            $status = 'LOCK';
        }
    }

    $i = 0;
    $i_modal = 0;
    $kode_sub = '';
    $temp_rekening = '';
    $c = new Criteria();
    $c->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
    $c->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
    $c->add(DinasRincianDetailPeer::STATUS_HAPUS, FALSE);
    $c->add(DinasRincianDetailPeer::STATUS_LEVEL, 4, Criteria::GREATER_THAN);
    if (DinasRincianDetailPeer::doSelectOne($c))
        $status_kunci = FALSE;
    else
        $status_kunci = TRUE;
    $lok = 'LOCK';
    $co = new Criteria();
    $co->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
    $co->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
    $co->add(DinasRincianDetailPeer::STATUS_HAPUS, FALSE);
    $co->add(DinasRincianDetailPeer::LOCK_SUBTITLE, $lok);
    if (DinasRincianDetailPeer::doSelectOne($co))
        $status_kunci_komponen = TRUE;
    else
        $status_kunci_komponen = FALSE;
    $status_kunci_komponen = TRUE;
}
?>
<?php
$pertama = 0;
foreach ($rs_rd as $rd):
    $est_fisik = FALSE;
    $c = new Criteria();
    $c->add(KomponenPeer::KOMPONEN_ID, $rd->getKomponenId());
    $c->add(KomponenPeer::IS_EST_FISIK, TRUE);
    if ($rs_est_fisik = KomponenPeer::doSelectOne($c))
        $est_fisik = TRUE;

    $odd = fmod($i++, 2);
    $unit_id = $rd->getUnitId();
    $kegiatan_code = $rd->getKegiatanCode();

    if ($kode_sub != $rd->getKodeSub()) 
    {
        $kode_sub = $rd->getKodeSub();
        $sub = $rd->getSub();
        $cekKodeSub = substr($kode_sub, 0, 4);

        if ($cekKodeSub == 'RKAM') {//RKA Member
            if ($tahap == 'pakbp') {
                $C_RKA = new Criteria();
                $C_RKA->add(PakBukuPutihRkaMemberPeer::KODE_SUB, $kode_sub);
                $C_RKA->addAscendingOrderByColumn(PakBukuPutihRkaMemberPeer::KODE_SUB);
                $rs_rkam = PakBukuPutihRkaMemberPeer::doSelectOne($C_RKA);
            } elseif ($tahap == 'pakbb') {
                $C_RKA = new Criteria();
                $C_RKA->add(PakBukuBiruRkaMemberPeer::KODE_SUB, $kode_sub);
                $C_RKA->addAscendingOrderByColumn(PakBukuBiruRkaMemberPeer::KODE_SUB);
                $rs_rkam = PakBukuBiruRkaMemberPeer::doSelectOne($C_RKA);
            } elseif ($tahap == 'murni') {
                $C_RKA = new Criteria();
                $C_RKA->add(MurniRkaMemberPeer::KODE_SUB, $kode_sub);
                $C_RKA->addAscendingOrderByColumn(MurniRkaMemberPeer::KODE_SUB);
                $rs_rkam = MurniRkaMemberPeer::doSelectOne($C_RKA);
            } elseif ($tahap == 'murnibp') {
                $C_RKA = new Criteria();
                $C_RKA->add(MurniBukuPutihRkaMemberPeer::KODE_SUB, $kode_sub);
                $C_RKA->addAscendingOrderByColumn(MurniBukuPutihRkaMemberPeer::KODE_SUB);
                $rs_rkam = MurniBukuPutihRkaMemberPeer::doSelectOne($C_RKA);
            } elseif ($tahap == 'murnibb') {
                $C_RKA = new Criteria();
                $C_RKA->add(MurniBukuBiruRkaMemberPeer::KODE_SUB, $kode_sub);
                $C_RKA->addAscendingOrderByColumn(MurniBukuBiruRkaMemberPeer::KODE_SUB);
                $rs_rkam = MurniBukuBiruRkaMemberPeer::doSelectOne($C_RKA);
            } elseif ($tahap == 'revisi1') {
                $C_RKA = new Criteria();
                $C_RKA->add(Revisi1RkaMemberPeer::KODE_SUB, $kode_sub);
                $C_RKA->addAscendingOrderByColumn(Revisi1RkaMemberPeer::KODE_SUB);
                $rs_rkam = Revisi1RkaMemberPeer::doSelectOne($C_RKA);
            } elseif ($tahap == 'revisi1_1') {
                $C_RKA = new Criteria();
                $C_RKA->add(Revisi1bRkaMemberPeer::KODE_SUB, $kode_sub);
                $C_RKA->addAscendingOrderByColumn(Revisi1bRkaMemberPeer::KODE_SUB);
                $rs_rkam = Revisi1bRkaMemberPeer::doSelectOne($C_RKA);
            } elseif ($tahap == 'revisi2') {
                $C_RKA = new Criteria();
                $C_RKA->add(Revisi2RkaMemberPeer::KODE_SUB, $kode_sub);
                $C_RKA->addAscendingOrderByColumn(Revisi2RkaMemberPeer::KODE_SUB);
                $rs_rkam = Revisi2RkaMemberPeer::doSelectOne($C_RKA);
            }  elseif ($tahap == 'revisi2_1') {
                $C_RKA = new Criteria();
                $C_RKA->add(Revisi21RkaMemberPeer::KODE_SUB, $kode_sub);
                $C_RKA->addAscendingOrderByColumn(Revisi21RkaMemberPeer::KODE_SUB);
                $rs_rkam = Revisi21RkaMemberPeer::doSelectOne($C_RKA);
            } elseif ($tahap == 'revisi2_2') {
                $C_RKA = new Criteria();
                $C_RKA->add(Revisi22RkaMemberPeer::KODE_SUB, $kode_sub);
                $C_RKA->addAscendingOrderByColumn(Revisi22RkaMemberPeer::KODE_SUB);
                $rs_rkam = Revisi22RkaMemberPeer::doSelectOne($C_RKA);
            } elseif ($tahap == 'revisi3') {
                $C_RKA = new Criteria();
                $C_RKA->add(Revisi3RkaMemberPeer::KODE_SUB, $kode_sub);
                $C_RKA->addAscendingOrderByColumn(Revisi3RkaMemberPeer::KODE_SUB);
                $rs_rkam = Revisi3RkaMemberPeer::doSelectOne($C_RKA);
            } elseif ($tahap == 'revisi3_1') {
                $C_RKA = new Criteria();
                $C_RKA->add(Revisi31RkaMemberPeer::KODE_SUB, $kode_sub);
                $C_RKA->addAscendingOrderByColumn(Revisi31RkaMemberPeer::KODE_SUB);
                $rs_rkam = Revisi31RkaMemberPeer::doSelectOne($C_RKA);
            } elseif ($tahap == 'revisi4') {
                $C_RKA = new Criteria();
                $C_RKA->add(Revisi4RkaMemberPeer::KODE_SUB, $kode_sub);
                $C_RKA->addAscendingOrderByColumn(Revisi4RkaMemberPeer::KODE_SUB);
                $rs_rkam = Revisi4RkaMemberPeer::doSelectOne($C_RKA);
            }  elseif ($tahap == 'revisi5') {
                $C_RKA = new Criteria();
                $C_RKA->add(Revisi5RkaMemberPeer::KODE_SUB, $kode_sub);
                $C_RKA->addAscendingOrderByColumn(Revisi5RkaMemberPeer::KODE_SUB);
                $rs_rkam = Revisi5RkaMemberPeer::doSelectOne($C_RKA);
            } elseif ($tahap == 'revisi6') {
                $C_RKA = new Criteria();
                $C_RKA->add(Revisi6RkaMemberPeer::KODE_SUB, $kode_sub);
                $C_RKA->addAscendingOrderByColumn(Revisi6RkaMemberPeer::KODE_SUB);
                $rs_rkam = Revisi6RkaMemberPeer::doSelectOne($C_RKA);
            } elseif ($tahap == 'revisi7') {
                $C_RKA = new Criteria();
                $C_RKA->add(Revisi7RkaMemberPeer::KODE_SUB, $kode_sub);
                $C_RKA->addAscendingOrderByColumn(Revisi7RkaMemberPeer::KODE_SUB);
                $rs_rkam = Revisi7RkaMemberPeer::doSelectOne($C_RKA);
            } elseif ($tahap == 'revisi8') {
                $C_RKA = new Criteria();
                $C_RKA->add(Revisi8RkaMemberPeer::KODE_SUB, $kode_sub);
                $C_RKA->addAscendingOrderByColumn(Revisi8RkaMemberPeer::KODE_SUB);
                $rs_rkam = Revisi8RkaMemberPeer::doSelectOne($C_RKA);
            } elseif ($tahap == 'revisi9') {
                $C_RKA = new Criteria();
                $C_RKA->add(Revisi9RkaMemberPeer::KODE_SUB, $kode_sub);
                $C_RKA->addAscendingOrderByColumn(Revisi9RkaMemberPeer::KODE_SUB);
                $rs_rkam = Revisi9RkaMemberPeer::doSelectOne($C_RKA);
            } elseif ($tahap == 'revisi10') {
                $C_RKA = new Criteria();
                $C_RKA->add(Revisi10RkaMemberPeer::KODE_SUB, $kode_sub);
                $C_RKA->addAscendingOrderByColumn(Revisi10RkaMemberPeer::KODE_SUB);
                $rs_rkam = Revisi10RkaMemberPeer::doSelectOne($C_RKA);
            } elseif ($tahap == 'rkua') {
                $C_RKA = new Criteria();
                $C_RKA->add(RkuaRkaMemberPeer::KODE_SUB, $kode_sub);
                $C_RKA->addAscendingOrderByColumn(RkuaRkaMemberPeer::KODE_SUB);
                $rs_rkam = RkuaRkaMemberPeer::doSelectOne($C_RKA);
            } else {
                $C_RKA = new Criteria();
                $C_RKA->add(DinasRkaMemberPeer::KODE_SUB, $kode_sub);
                $C_RKA->addAscendingOrderByColumn(DinasRkaMemberPeer::KODE_SUB);
                $rs_rkam = DinasRkaMemberPeer::doSelectOne($C_RKA);
            }
            if ($rs_rkam) {
                ?>
                <tr class="pekerjaans_<?php echo $id ?>" bgcolor="#e6e6e6">
                    <td colspan="9">
                        <?php
                        if (($status == 'OPEN') and ( substr($sf_user->getNamaLogin(), 0, 3) <> 'pa_')) {
                            echo link_to_function(image_tag('/sf/sf_admin/images/edit.png'), 'editHeaderKegiatan(' . $id . ',"' . $kegiatan_code . '","' . $unit_id . '","' . $kode_sub . '")');
                            echo link_to_function(image_tag('/sf/sf_admin/images/cancel.png'), 'hapusHeaderKegiatan(' . $id . ',"' . $kegiatan_code . '","' . $unit_id . '","' . $kode_sub . '")');
                        }
                        ?>
                        <div id="<?php echo 'header_' . $rs_rkam->getKodeSub() ?>"><b> .:. <?php echo $rs_rkam->getKomponenName() . ' ' . $rs_rkam->getDetailName(); ?></b></div>
                    </td>
                    <td align="right">
                        <?php
                        echo number_format($rd->getTotalKodeSub($kode_sub), 0, ',', '.');
                        ?>
                    </td>
                    <td colspan="5">&nbsp;</td>
                </tr>
                <?php
            } 
        } else { //else form SUB
            $c = new Criteria();
            if ($tahap == 'pakbp') {
                $c->add(PakBukuPutihRincianSubParameterPeer::KODE_SUB, $kode_sub);
                $rs_subparameter = PakBukuPutihRincianSubParameterPeer::doSelectOne($c);
            } elseif ($tahap == 'pakbb') {
                $c->add(PakBukuBiruRincianSubParameterPeer::KODE_SUB, $kode_sub);
                $rs_subparameter = PakBukuBiruRincianSubParameterPeer::doSelectOne($c);
            } elseif ($tahap == 'murni') {
                $c->add(MurniRincianSubParameterPeer::KODE_SUB, $kode_sub);
                $rs_subparameter = MurniRincianSubParameterPeer::doSelectOne($c);
            } elseif ($tahap == 'murnibp') {
                $c->add(MurniBukuPutihRincianSubParameterPeer::KODE_SUB, $kode_sub);
                $rs_subparameter = MurniBukuPutihRincianSubParameterPeer::doSelectOne($c);
            } elseif ($tahap == 'murnibb') {
                $c->add(MurniBukuBiruRincianSubParameterPeer::KODE_SUB, $kode_sub);
                $rs_subparameter = MurniBukuBiruRincianSubParameterPeer::doSelectOne($c);
            } elseif ($tahap == 'revisi1') {
                $c->add(Revisi1RincianSubParameterPeer::KODE_SUB, $kode_sub);
                $rs_subparameter = Revisi1RincianSubParameterPeer::doSelectOne($c);
            } elseif ($tahap == 'revisi1_1') {
                $c->add(Revisi1bRincianSubParameterPeer::KODE_SUB, $kode_sub);
                $rs_subparameter = Revisi1bRincianSubParameterPeer::doSelectOne($c);
            } elseif ($tahap == 'revisi2') {
                $c->add(Revisi2RincianSubParameterPeer::KODE_SUB, $kode_sub);
                $rs_subparameter = Revisi2RincianSubParameterPeer::doSelectOne($c);
            } elseif ($tahap == 'revisi2_1') {
                $c->add(Revisi21RincianSubParameterPeer::KODE_SUB, $kode_sub);
                $rs_subparameter = Revisi21RincianSubParameterPeer::doSelectOne($c);
            } elseif ($tahap == 'revisi2_2') {
                $c->add(Revisi22RincianSubParameterPeer::KODE_SUB, $kode_sub);
                $rs_subparameter = Revisi22RincianSubParameterPeer::doSelectOne($c);
            }  elseif ($tahap == 'revisi3') {
                $c->add(Revisi3RincianSubParameterPeer::KODE_SUB, $kode_sub);
                $rs_subparameter = Revisi3RincianSubParameterPeer::doSelectOne($c);
            } elseif ($tahap == 'revisi3_0') {
                $c->add(Revisi30RincianSubParameterPeer::KODE_SUB, $kode_sub);
                $rs_subparameter = Revisi30RincianSubParameterPeer::doSelectOne($c);
            } elseif ($tahap == 'revisi3_1') {
                $c->add(Revisi31RincianSubParameterPeer::KODE_SUB, $kode_sub);
                $rs_subparameter = Revisi31RincianSubParameterPeer::doSelectOne($c);
            } elseif ($tahap == 'revisi4') {
                $c->add(Revisi4RincianSubParameterPeer::KODE_SUB, $kode_sub);
                $rs_subparameter = Revisi4RincianSubParameterPeer::doSelectOne($c);
            } elseif ($tahap == 'revisi5') {
                $c->add(Revisi5RincianSubParameterPeer::KODE_SUB, $kode_sub);
                $rs_subparameter = Revisi5RincianSubParameterPeer::doSelectOne($c);
            } elseif ($tahap == 'revisi6') {
                $c->add(Revisi6RincianSubParameterPeer::KODE_SUB, $kode_sub);
                $rs_subparameter = Revisi6RincianSubParameterPeer::doSelectOne($c);
            } elseif ($tahap == 'revisi7') {
                $c->add(Revisi7RincianSubParameterPeer::KODE_SUB, $kode_sub);
                $rs_subparameter = Revisi7RincianSubParameterPeer::doSelectOne($c);
            } elseif ($tahap == 'revisi8') {
                $c->add(Revisi8RincianSubParameterPeer::KODE_SUB, $kode_sub);
                $rs_subparameter = Revisi8RincianSubParameterPeer::doSelectOne($c);
            } elseif ($tahap == 'revisi9') {
                $c->add(Revisi9RincianSubParameterPeer::KODE_SUB, $kode_sub);
                $rs_subparameter = Revisi9RincianSubParameterPeer::doSelectOne($c);
            } elseif ($tahap == 'revisi10') {
                $c->add(Revisi10RincianSubParameterPeer::KODE_SUB, $kode_sub);
                $rs_subparameter = Revisi10RincianSubParameterPeer::doSelectOne($c);
            } elseif ($tahap == 'rkua') {
                $c->add(RkuaRincianSubParameterPeer::KODE_SUB, $kode_sub);
                $rs_subparameter = RkuaRincianSubParameterPeer::doSelectOne($c);
            } else {
                $c->add(DinasRincianSubParameterPeer::KODE_SUB, $kode_sub);
                $rs_subparameter = DinasRincianSubParameterPeer::doSelectOne($c);
            }
            if ($rs_subparameter) {
                ?>
                <tr class="pekerjaans_<?php echo $id ?>" bgcolor="#e6e6e6">
                    <td colspan="9">
                        <b> :. <?php echo $rs_subparameter->getSubKegiatanName() . ' ' . $rs_subparameter->getDetailName(); ?></b>
                    </td>
                    <td align="right">
                        <?php echo number_format($rd->getTotalKodeSub($kode_sub), 0, ',', '.'); ?>
                    </td> 
                    <td>&nbsp;</td>                   
                </tr>
                <?php
            } else {
                $ada = 'tidak';
                $query = "select * from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "rincian_sub_parameter "
                . "where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and new_subtitle ilike '%$sub%'";
                $con = Propel::getConnection();
                $stmt = $con->prepareStatement($query);
                $t = $stmt->executeQuery();
                while ($t->next()) 
                {
                    if ($t->getString('kode_sub')) {
                        ?>
                        <tr class="pekerjaans_<?php echo $id ?>" bgcolor="#e6e6e6">
                            <td colspan="9">
                                <b> :. <?php echo $t->getString('sub_kegiatan_name') . ' ' . $t->getString('detail_name'); ?></b>
                            </td>
                            <td align="right">
                                <?php
                                echo number_format($rd->getTotalSub($sub), 0, ',', '.');
                                $ada = 'ada';
                                ?> 
                            </td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <?php
                        }
                }
                if ($ada == 'tidak') 
                {
                    if ($kode_sub != '') 
                    {
                        $query = "select * from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "rincian_detail "
                        . "where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and sub = '$sub' and status_hapus=false";
                        $con = Propel::getConnection();
                        $stmt = $con->prepareStatement($query);
                        $t = $stmt->executeQuery();
                        while ($t->next()) {
                            if ($t->getString('kode_sub')) 
                            {
                                ?>
                                <tr class="pekerjaans_<?php echo $id ?>" bgcolor="#e6e6e6">
                                    <td colspan="9"><b> :. <?php echo $t->getString('komponen_name') . ' ' . $t->getString('detail_name'); ?></b></td>
                                    <td align="right">
                                        <?php
                                        echo number_format($rd->getTotalSub($sub), 0, ',', '.');
                                        $ada = 'ada';
                                        ?>
                                    </td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                        <?php
                            }
                        }
                    }
                }
            }
        }
    }

    $rekening_code = $rd->getRekeningCode();
    if ($temp_rekening != $rekening_code) {
        if($pertama != 0){
        // $subtitle = $rd->getSubtitle();
        // echo $subtitle;die;
        $c = new Criteria();
        $c->add(RekeningPeer::REKENING_CODE, $temp_rekening);
        $rs_rekening = RekeningPeer::doSelectOne($c);
        if($rs_rekening){
            $rekening_name = $rs_rekening->getRekeningName();
        }

//         $queryTotalPerRekening = "select sum(nilai_anggaran) as hasil
// from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail rd
// where rd.kegiatan_code = '" . $kode_kegiatan . "' and rd.unit_id = '" . $unit_id . "' and rd.subtitle = '" . $subtitle . "' and rd.status_hapus=false";
//         //print_r($query2);
//         $con = Propel::getConnection();
//         $stmt5 = $con->prepareStatement($queryTotalPerRekening);
//         $total_rekening = $stmt5->executeQuery();
    ?>
        <tr class="pekerjaans_<?php echo $id ?>">
                <td colspan="7" class="text-right"><b>Total <?php echo $rekening_name ?></b></td>
                <td class="text-right"><b><?php echo number_format(round($total_per_rekening, 0), 0, ',', '.') ?></b></td>
                <td colspan="2"></td>
        </tr>
    <?php
        $total_per_rekening = 0;
        }
        $temp_rekening = $rekening_code;
        $c = new Criteria();
        $c->add(RekeningPeer::REKENING_CODE, $rekening_code);
        $rs_rekening = RekeningPeer::doSelectOne($c);

        $total_per_rekening = 0;
        if ($rs_rekening) {
            $rekening_name = $rs_rekening->getRekeningName();
            ?>
            <tr class="pekerjaans_<?php echo $id ?>" bgcolor="#e6e6e6">
                <td colspan="9"><i><?php echo $rekening_code . ' ' . $rekening_name ?></i> </td>
                <td colspan="2">
                    <?php
                        $c = new Criteria();
                        $c->add(CatatanRekeningPeer::UNIT_ID, $unit_id);
                        $c->add(CatatanRekeningPeer::KEGIATAN_CODE, $kegiatan_code);
                        $c->add(CatatanRekeningPeer::REKENING_CODE, $rekening_code);
                        $c->add(CatatanRekeningPeer::TAHAP, sfConfig::get('app_tahap_edit') );
                        $cek_catatan = CatatanRekeningPeer::doSelectOne($c);

                        if(empty($cek_catatan)){
                            $label = 'Tambah';
                        }else{
                            $label = 'Edit';
                        }
                    ?>

                    <?php if(strpos($kegiatan_code,'-') !== FALSE) { ?>
                    <a href="#" class="btn btn-primary btn-sm" data-unit="<?php echo $unit_id ?>" data-kegiatancode="<?php echo $kegiatan_code ?>" data-rekening="<?php echo $rekening_code ?>" data-namarekening="<?php echo $rekening_name ?>" data-toggle="modal" onclick="tampilModalCatatan(this)" ><?php echo $label ?> Catatan</a>
                    <!-- <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#ModalCatatan<?php echo $i_modal ?>">
                        Edit Catatan
                    </button> -->
                    <?php

                        if(!empty($cek_catatan)){
                            echo link_to('Hapus Catatan','entri/hapusCatatan?unit_id='.$unit_id.'&kegiatan_code='.$kegiatan_code.'&rekening_code='.$rekening_code, 'class=btn btn-danger btn-sm');
                        }
                    ?>

                    <?php } ?>
                </td>
            </tr>


            <!-- Modal Catatan Rekening -->
            <!-- <div class="modal fade" id="ModalCatatan<?php echo $i_modal ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"> -->
            <!-- <div id="ModalCatatan<?php echo $i_modal ?>" class="modal fade" tabindex="-1" role="dialog">  -->
                <!-- <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h3 class="modal-title"><code>Catatan Rekening</code></h3>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <?php //echo form_tag('entri/tambahCatatanRekening', array('method' => 'get', 'class' => 'form-horizontal')) ?>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md">
                                    <div class="form-group">
                                        <label>Rekening</label>
                                        <input type="text" class="form-control" id="catatan_rekening_nama" value="<?php echo $rekening_code . ' ' . $rekening_name ?>" readonly>
                                        <input type="hidden" name="catatan_unit_id" id="catatan_unit_id" value="<?php echo $unit_id ?>">
                                        <input type="hidden" name="catatan_kegiatan_code" id="catatan_kegiatan_code" value="<?php echo $kegiatan_code ?>">
                                        <input type="hidden" name="catatan_rekening_code" id="catatan_rekening_code" value="<?php echo $rekening_code ?>">
                                    </div>
                                    <div class="form-group">
                                        <label>Catatan</label>
                                        <textarea name="catatan" id="catatan" class="form-control"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary btn-xs">Simpan</button>
                        </div>
                        </form>
                    </div>
                </div>            
            </div> -->


            <?php
            $pertama = 1;
        }
    }
    ?>
    
    <tr class="pekerjaans_<?php echo $id ?>">
        <td>
            <?php
            $kegiatan = $rd->getKegiatanCode();
            $unit = $rd->getUnitId();
            $no = $rd->getDetailNo();
            $sub = $rd->getSubtitle();

            $benar_musrenbang = 0;
            if ($rd->getIsMusrenbang() == 'TRUE') {
                $benar_musrenbang = 1;
            }
            $benar_prioritas = 0;
            if($rd->getPrioritasWali() == 'TRUE') {
                $benar_prioritas = 1;
            }
            $benar_output = 0;
            if($rd->getIsOutput() == 'TRUE') {
                $benar_output = 1;
            }
            $benar_multiyears = 0;
            if ($rd->getThKeMultiyears() <> null && $rd->getThKeMultiyears() > 0) {
                $benar_multiyears = 1;
            }
            $benar_hibah = 0;
            if ($rd->getIsHibah() == 'TRUE') {
                $benar_hibah = 1;
            }
            $status_kunci_baru = FALSE;

            $c_rekening = new Criteria();
            $c_rekening->add(BukaRekeningBangunanPeer::UNIT_ID, $unit_id);
            $rekening_bangunan = BukaRekeningBangunanPeer::doCount($c_rekening);
            $array_rekening_dikunci = array('5.1.02.01.01.0001');

            if (in_array($rd->getRekeningCode(), $array_rekening_dikunci) && $rekening_bangunan <= 0) {
                $status_kunci_baru = TRUE;
            }

            $array_rekening_jk = array('5.2.2.24.01', '5.2.1.02.02');

            $arr_buka_edit_output = array('');
            $query =
            "SELECT detail_kegiatan
            FROM ebudget.buka_komponen_murni";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs = $stmt->executeQuery();
            while ($rs->next()) array_push($arr_buka_edit_output, $rs->getString('detail_kegiatan'));

            $arr_kunci_detail = array('');
            $query1 =
            "SELECT detail_kegiatan
            FROM ebudget.kunci_komponen_murni";
            $con = Propel::getConnection();
            $stmt1 = $con->prepareStatement($query1);
            $rs1 = $stmt1->executeQuery();
            while ($rs1->next()) array_push($arr_kunci_detail, $rs1->getString('detail_kegiatan'));
            $arr_kunci_komponen = array('2.1.1.01.01.01.004.015','2.1.1.01.01.01.001.046','2.1.1.01.01.01.001.047','2.1.1.01.01.01.001.051','2.1.1.01.01.01.002.020','2.1.1.01.01.01.002.020','2.1.1.01.01.01.002.030','2.1.1.01.01.01.002.031','2.1.1.01.01.01.002.032','2.1.1.01.01.01.001.046.A','2.1.1.01.01.01.001.055','2.1.1.01.01.01.001.053.A','2.1.1.01.01.01.001.055','2.1.1.01.01.01.001.054','2.1.1.01.01.01.001.053'); 
            $koRek = substr($rd->getKomponenId() , 0, 18);
            $arr_tenaga_kontrak = array('2.1.1.01.01.01.008','2.1.1.03.05.01.001','2.1.1.01.01.01.004','2.1.1.01.01.02.099','2.1.1.01.01.01.005','2.1.1.01.01.01.006','2.1.1.01.01.02.001');
            $arr_all_kode_sub=array('');
            if ((in_array($rd->getDetailKegiatan(), $arr_buka_edit_output)  || (!$rd->getIsOutput() && !$rd->getPrioritasWali())) && $rd->getStatusLevel() <= 1 && !$status_kunci_baru && $status_kunci && $status == 'OPEN' && $status_kunci_komponen && $rd->getLockSubtitle() != 'LOCK') {
                ?>
                <div class="btn-group">
                    <?php
                    // if(sfConfig::get('app_tahap_edit') == 'murni')
                    // {
                    //     $sumber_dana=70;
                    // }
                    // else
                    // {
                    //      $sumber_dana=11;
                    // }
                    // if(empty($rd->getDetailGaji()))
                    // $detail_gaji = 0;

                    if(( in_array($koRek,$arr_tenaga_kontrak) && ($rd->getSatuan() == 'Orang Bulan') && (strpos($rd->getKomponenName(), 'Biaya') ===  FALSE) && (!in_array($rd->getDetailKegiatan(), $arr_buka_edit_output)))|| (in_array($rd->getKomponenId(), $arr_kunci_komponen) && (!in_array($rd->getDetailKegiatan(), $arr_buka_edit_output))) || in_array($koRek,$arr_all_kode_sub) || ($rd->getSumberDanaId() <> 11 && (!in_array($rd->getDetailKegiatan(), $arr_buka_edit_output))) || ($rd->getisCovid() == true && (!in_array($rd->getDetailKegiatan(), $arr_buka_edit_output))) || (in_array($rd->getDetailKegiatan(), $arr_kunci_detail) && (!in_array($rd->getDetailKegiatan(), $arr_buka_edit_output))) || (strpos($rd->getRekeningCode(), '5.1.01.01.02.0001') !==  FALSE && $rd->getTipe() == 'BTL')  )
                    {

                    }
                    else
                    {
                        echo link_to('<i class="fa fa-edit"></i> Edit', 'entri/editKegiatan?id=' . $rd->getDetailNo() . '&unit=' . $rd->getUnitId() . '&kegiatan=' . $rd->getKegiatanCode() . '&komponen='.$rd->getKomponenId().'&edit=' . md5('ubah'), array('class' => 'btn btn-default btn-flat btn-sm'));
                        if (sfConfig::get('app_tahap_edit') == 'murni' && $benar_hibah == 0 && $benar_musrenbang == 0 && $benar_multiyears == 0 && substr($sf_user->getNamaLogin(), 0, 3) <> 'pa_') {
                            ?>
                            <button type="button" class="btn btn-default dropdown-toggle btn-flat btn-sm" data-toggle="dropdown">
                                <span class="caret"></span>
                                <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <div class="dropdown-menu" role="menu">
                                <?php
                                echo link_to('<i class="fa fa-trash"></i> Hapus', 'entri/hapusPekerjaans?id=' . $id . '&no=' . $rd->getDetailNo() . '&unit=' . $rd->getUnitId() . '&kegiatan=' . $rd->getKegiatanCode(), Array('confirm' => 'Yakin untuk menghapus Komponen ' . $rd->getKomponenName() . ' ' . $rd->getDetailName() . ' ?', 'class' => 'dropdown-item'));
                                ?>
                            </div>
                            <?php
                        }

                    }
                    if( $rd->getRekeningCode() == '')
                    {
                        $query="select distinct rekening_code,rekening_name from " . sfConfig::get('app_default_schema') . ".rekening
                        where rekening_code in (select rekening_code from " . sfConfig::get('app_default_schema') . ".komponen_rekening
                        where komponen_id='".$rd->getKomponenId()."')";
                        $con = Propel::getConnection();
                        $stmt = $con->prepareStatement($query);
                        $ts = $stmt->executeQuery();
                        $pilih = array();
                        $ada = false;
                        $select_str = "<select name='rek_$no'>";
                        while ($ts->next()) {
                            $ada = true;
                            $r = $ts->getString('rekening_code');
                            $rs = $ts->getString('rekening_code')." - ".$ts->getString('rekening_name');
                            $select_str.="<option value='$r'>$rs</option>";
                        }
                        $select_str.="</select>";
                        if ($ada) {
                            ?>
                            <div id="rekening_list_<?php echo $no ?>">
                                <form method="post" id="form_rekening_list_<?php echo $no ?>">
                                <input type="hidden" name="id" value="<?php echo $id; ?>" />
                                <input type="hidden" name="unit_id" value="<?php echo $unit_id; ?>" />
                                <input type="hidden" name="kegiatan_code" value="<?php echo $kegiatan_code; ?>" />
                                <input type="hidden" name="detail_no" value="<?php echo $no; ?>" />
                                <?php echo $select_str; ?>
                                <input class="saveGantiRekening" type="submit" value="Simpan"/>
                                </form><br/>
                            </div>
                            <?php
                        }
                    } 
                    ?>
                </div>
                <div class="clearfix"></div>
                <?php
                //irul 25agustus 2014 - fungsi GMAP
                if ($rd->getTipe2() == 'PERENCANAAN' || $rd->getTipe2() == 'PENGAWASAN' || $rd->getTipe2() == 'KONSTRUKSI' || $rd->getTipe2() == 'TANAH' || $rd->getTipe() == 'FISIK' || $est_fisik) 
                {
                    $con = Propel::getConnection();
                    $kode_rka = $unit_id . '.' . $kegiatan_code . '.' . $rd->getDetailNo();
                    $query_cek_waiting = "select id_waiting from ebudget.waitinglist_pu where kode_rka = '" . $kode_rka . "' ";
                    $stmt_cek_waiting = $con->prepareStatement($query_cek_waiting);
                    $rs_cek_waiting = $stmt_cek_waiting->executeQuery();
                    if ($rs_cek_waiting->next()) {
                        ?>
                        <div id="tempat_ajax_<?php echo str_replace('.', '_', $kegiatan_code) . '_' . $rd->getDetailNo() ?>">
                            <?php echo link_to_function('<i class="fa fa-trash"></i> Kembalikan ke Waiting List', 'execKembalikanWaiting("' . $rd->getDetailNo() . '","' . $rd->getUnitId() . '","' . $rd->getKegiatanCode() . '", "' . str_replace('.', '_', $kegiatan_code) . '_' . $rd->getDetailNo() . '")', array('class' => 'btn btn-default btn-flat btn-sm')); ?>
                        </div>
                        <?php
                    }

                    $id_kelompok = 0;
                    $tot = 0;
                    $query = "select count(*) as tot "
                    . "from " . sfConfig::get('app_default_gis') . ".geojsonlokasi_rev1 "
                    . "where unit_id='" . $rd->getUnitId() . "' and kegiatan_code='" . $rd->getKegiatanCode() . "' and detail_no ='" . $rd->getDetailNo() . "' "
                    . "and tahun = '" . sfConfig::get('app_tahun_default') . "' and status_hapus = FALSE";
                    $stmt = $con->prepareStatement($query);
                    $rs = $stmt->executeQuery();
                    while ($rs->next()) {
                        $tot = $rs->getString('tot');
                    }
                    if ($tot == 0) {
                        $con = Propel::getConnection();
                        $c2 = new Criteria();
                        $crit1 = $c2->getNewCriterion(KomponenPeer::KOMPONEN_TIPE, 'FISIK');
                        $crit1->addOr($c2->getNewCriterion(KomponenPeer::KOMPONEN_TIPE, 'EST'));
                        $crit1->addOr($c2->getNewCriterion(KomponenPeer::KOMPONEN_TIPE2, 'KONSTRUKSI'));
                        $crit1->addOr($c2->getNewCriterion(KomponenPeer::KOMPONEN_TIPE2, 'TANAH'));
                        $crit1->addOr($c2->getNewCriterion(KomponenPeer::KOMPONEN_TIPE2, 'PENGAWASAN'));
                        $crit1->addOr($c2->getNewCriterion(KomponenPeer::KOMPONEN_TIPE2, 'PERENCANAAN'));
                        $c2->add($c2->getNewCriterion(KomponenPeer::KOMPONEN_NAME, $rd->getKomponenName(), Criteria::ILIKE));
                        $c2->addAnd($crit1);
                        $rd2 = KomponenPeer::doSelectOne($c2);
                        if ($rd2) {
                            $komponen_id = $rd2->getKomponenId();
                            $satuan = $rd2->getSatuan();
                        } else {
                            $komponen_id = '0';
                            $satuan = '';
                        }

                        if ($komponen_id == '0') {
                            $query2 = "select * from master_kelompok_gmap "
                            . "where '" . $rd->getKomponenName() . "' ilike nama_objek||'%'";
                        } else {
                            $query2 = "select * from master_kelompok_gmap "
                            . "where '" . $komponen_id . "' ilike kode_kelompok||'%'";
                        }

                        $stmt2 = $con->prepareStatement($query2);
                        $rs2 = $stmt2->executeQuery();
                        while ($rs2->next()) {
                            $id_kelompok = $rs2->getString('id_kelompok');
                        }

                        if ($id_kelompok == '' || $id_kelompok == 0 || $id_kelompok == null) {
                            $id_kelompok = 19;

                            if (in_array($satuan, array('Kegiatan', 'Lokasi', 'M2', 'M²', 'm3', 'Paket', 'Set'))) {
                                $id_kelompok = 100;
                            } elseif (in_array($satuan, array('m', 'M', 'M1', 'Meter', 'Titik', 'Unit'))) {
                                $id_kelompok = 101;
                            }
                        }
                    } else {
                        $con = Propel::getConnection();
                        $query = "select * from " . sfConfig::get('app_default_gis') . ".geojsonlokasi_rev1 
                        where unit_id='" . $rd->getUnitId() . "' and kegiatan_code='" . $rd->getKegiatanCode() . "' and detail_no ='" . $rd->getDetailNo() . "' and tahun = '" . sfConfig::get('app_tahun_default') . "'";
                        $stmt = $con->prepareStatement($query);
                        $rs = $stmt->executeQuery();
                        while ($rs->next()) {
                            $mlokasi = $rs->getString('mlokasi');
                            $id_kelompok = $rs->getString('id_kelompok');
                        }

                        $query = "select max(lokasi_ke) as total_lokasi from " . sfConfig::get('app_default_gis') . ".geojsonlokasi_rev1 
                        where unit_id='" . $rd->getUnitId() . "' and kegiatan_code='" . $rd->getKegiatanCode() . "' and detail_no ='" . $rd->getDetailNo() . "' and tahun = '" . sfConfig::get('app_tahun_default') . "'";
                        $stmt = $con->prepareStatement($query);
                        $rs = $stmt->executeQuery();
                        while ($rs->next()) {
                            $total_lokasi = $rs->getString('total_lokasi');
                        }
                    }
                    ?>
                    <div class="btn-group">                            
                        <?php
                        echo link_to_function('<i class="fa fa-map-marker"></i>', '', array('class' => 'btn btn-default btn-flat btn-sm', 'disable' => true));
                        if ($tot == 0) {
                            $array_bersih_kurung = array('(', ')');
                            $lokasi_bersih_kurung = str_replace($array_bersih_kurung, '', $lokasi);
                            echo link_to('<i class="fa fa-edit"></i> Input Lokasi', sfConfig::get('app_path_gmap') . 'insertBaru_revisi.php?unit_id=' . $rd->getUnitId() . '&kode_kegiatan=' . $rd->getKegiatanCode() . '&detail_no=' . $rd->getDetailNo() . '&satuan=' . $rd->getSatuan() . '&volume=' . $rd->getVolume() . '&nilai_anggaran=' . $rd->getNilaiAnggaran() . '&tahun=' . sfConfig::get('app_tahun_default') . '&mlokasi=&id_kelompok=' . $id_kelompok . '&th_load=0&level=1&nm_user=' . $sf_user->getNamaLogin() . '&lokasi_ke=1', array('class' => 'btn btn-default btn-flat btn-sm'));
                        } else {
                            echo link_to('<i class="fa fa-edit"></i> Edit Lokasi', sfConfig::get('app_path_gmap') . 'updateData_revisi.php?unit_id=' . $rd->getUnitId() . '&kode_kegiatan=' . $rd->getKegiatanCode() . '&detail_no=' . $rd->getDetailNo() . '&satuan=' . $rd->getSatuan() . '&volume=' . $rd->getVolume() . '&nilai_anggaran=' . $rd->getNilaiAnggaran() . '&tahun=' . sfConfig::get('app_tahun_default') . '&mlokasi=' . $mlokasi . '&id_kelompok=' . $id_kelompok . '&th_load=0&level=1&nm_user=' . $sf_user->getNamaLogin() . '&total_lokasi=' . $total_lokasi . '&lokasi_ke=1', array('class' => 'btn btn-default btn-flat btn-sm'));
                            ?>
                            <button type="button" class="btn btn-default dropdown-toggle btn-flat btn-sm" data-toggle="dropdown">
                                <span class="caret"></span>
                                <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <div class="dropdown-menu" role="menu">
                                <?php
                                echo link_to('<i class="fa fa-trash"></i> Hapus Lokasi', 'entri/hapusLokasi?id=' . $id . '&no=' . $rd->getDetailNo() . '&unit=' . $rd->getUnitId() . '&kegiatan=' . $rd->getKegiatanCode(), Array('confirm' => 'Yakin untuk menghapus Lokasi untuk Komponen ' . $rd->getKomponenName() . ' ' . $rd->getDetailName() . ' ?', 'class' => 'dropdown-item'));
                                ?>
                            </div>
                        <?php
                        }
                        ?>
                </div>                
                    <?php
                }
                } 
                elseif ($status_jk == 'OPEN' && in_array($rd->getRekeningCode(), $array_rekening_jk)) 
                {
                ?>
                <div class="btn-group">
                    <?php
                    echo link_to('<i class="fa fa-edit"></i> Edit', 'entri/editKegiatan?id=' . $rd->getDetailNo() . '&unit=' . $rd->getUnitId() . '&kegiatan=' . $rd->getKegiatanCode() . '&edit=' . md5('ubah'), array('class' => 'btn btn-default btn-flat btn-sm'));
                    ?>
                </div>
                <div class="clearfix"></div>
                <?php
                } 
                ?>
        </td>
        <?php
            $cek = FALSE;
            if ($rd->getNotePeneliti() != '' and $rd->getNotePeneliti() != NULL and $rd->getStatusHapus() == false) {
            $warna = 'lightyellow';
            } elseif ($cek or ( $rd->getNoteSkpd() != '' and $rd->getNoteSkpd() != NULL ) and $rd->getStatusHapus() == false) {
            $warna = '#e8f3f1';
            } else {
            $warna = 'none';
            }
        ?>
        <td style="background: <?php echo $warna ?>">
            <?php
            if ($benar_musrenbang == 1) {
                echo '&nbsp;<span class="badge badge-success">Musrenbang</span>';
            }
            if ($benar_output == 1) {
                echo '&nbsp;<span class="badge badge-info">Output</span>';
            }
            if ($benar_prioritas == 1) {
                echo '&nbsp;<span class="badge badge-warning">Prioritas</span>';
            }
            if ($benar_hibah == 1) {
                echo '&nbsp;<span class="badge badge-success">Hibah</span>';
            }
            if ($rd->getKecamatan() <> '') {
                echo '&nbsp;<span class="badge badge-warning">Jasmas</span>';
            }
            if ($benar_multiyears == 1) {
                echo '&nbsp;<span class="badge badge-primary">Multiyears Tahun ke ' . $rd->getThKeMultiyears() . '</span>';
            }
                if ($rd->getSumberDanaId() == 1) {
                    echo '&nbsp;<span class="badge badge-info">DAK</span>';
                }
                if ($rd->getSumberDanaId() == 2) {
                    echo '&nbsp;<span class="badge badge-info">DBHCT</span>';
                }
                if ($rd->getSumberDanaId() == 3) {
                    echo '&nbsp;<span class="badge badge-info">DID</span>';
                }
                if ($rd->getSumberDanaId() == 4) {
                    echo '&nbsp;<span class="badge badge-info">DBH</span>';
                }
                if ($rd->getSumberDanaId() == 5) {
                    echo '&nbsp;<span class="badge badge-info">Pajak Rokok</span>';
                }
                if ($rd->getSumberDanaId() == 6) {
                    echo '&nbsp;<span class="badge badge-info">BLUD</span>';
                }
                if ($rd->getSumberDanaId() == 7) {
                    echo '&nbsp;<span class="badge badge-info">Bantuan Keuangan Provinsi</span>';
                }
                if ($rd->getSumberDanaId() == 8) {
                    echo '&nbsp;<span class="badge badge-info">Kapitasi JKN</span>';
                }
                if ($rd->getSumberDanaId() == 9) {
                    echo '&nbsp;<span class="badge badge-info">BOS</span>';
                }
                if ($rd->getSumberDanaId() == 10) {
                    echo '&nbsp;<span class="badge badge-info">DAU</span>';
                }
                if ($rd->getSumberDanaId() == 12) {
                    echo '&nbsp;<span class="badge badge-info">Hibah Pariwisata</span>';
                }
                if ($rd->getIsCovid() == true) {
                    echo '&nbsp;<span class="badge badge-info">Covid-19</span>';
                }
            echo '<br/>';
            echo $rd->getKomponenName();
            if (sfConfig::get('app_fasilitas_keteranganKomponen') == 'buka') {
                echo ' ' . $rd->getDetailName();
                echo '&nbsp;<span class="badge badge-danger">' . $rd->getTahap(). '</span><br/>';
                
                if ($rd->getTipe2() == 'KONSTRUKSI' || $rd->getTipe2() == 'TANAH' || $rd->getTipe() == 'FISIK' || $est_fisik) {
                    if ($rd->getLokasiKecamatan() <> '' && $rd->getLokasiKelurahan() <> '') {
                        echo '[' . $rd->getLokasiKelurahan() . ' - ' . $rd->getLokasiKecamatan() . ']';
                    }
                }
            }
            $query2 = "select tahap from " . sfConfig::get('app_default_schema') . ".komponen "
            . "where komponen_id='" . $rd->getKomponenId() . "'";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query2);
            $t = $stmt->executeQuery();
            while ($t->next()) {
                if ($t->getString('tahap') == DinasMasterKegiatanPeer::getTahapKegiatan($rd->getUnitId(), $rd->getKegiatanCode()) && sfConfig::get('app_tahap_edit') != 'murni') {
                    echo image_tag('/images/newanima.gif');
                }
            }

            if (in_array($rd->getDetailNo(), $komponen_serupa)) {
                echo image_tag('/images/warning.gif', array('title' => 'Ada komponen serupa', 'alt' => 'Ada komponen serupa'));
            }
            ?>
        </td>
        <td style="background: <?php echo $warna ?>" align="center"><?php echo $rd->getSatuan(); ?></td>
        <?php if( $rd->getAccres() > 1) 
        {
            $keterangan_koefisien= '('.$rd->getKeteranganKoefisien().') + '.$rd->getAccres().'%';
        }
        else
        {
            $keterangan_koefisien=$rd->getKeteranganKoefisien();
        }
        ?>
        <td style="background: <?php echo $warna ?>" align="center"><?php echo  $keterangan_koefisien; ?></td>
        <td style="background: <?php echo $warna ?>" align="right">
            <?php
            if ($rd->getSatuan() == '%') {
                echo $rd->getKomponenHargaAwal();
            } elseif ($rd->getKomponenHargaAwal() != floor($rd->getKomponenHargaAwal())) {
                echo number_format($rd->getKomponenHargaAwal(), 2, ',', '.');
            } else {
                echo number_format($rd->getKomponenHargaAwal(), 0, ',', '.');

            }
            if ($rd->getStatusLevel() <= 1 && !$status_kunci_baru && $status_kunci && $status == 'OPEN' && $status_kunci_komponen && !($rd->getLockSubtitle() == 'LOCK' && ($rd->getKomponenId() == '23.05.01.110' || $rd->getKomponenId() == '23.05.01.40' || $rd->getKomponenId() == '23.01.01.04.16'))) {
                        // if ($rd->getNoteSkpd() <> '') {

                if ($rd->getStatusLelang() == 'lock' && $rd->getSatuan()=='Paket' && $rd->getVolume()==1) {
                    echo image_tag('/images/bahaya2.gif');
                    echo '<br/><span class="badge badge-danger">[Telah dilakukan Penggunaan Sisa Lelang, tidak dapat mengedit volume]</span>';
                } else if ($rd->getStatusLelang() == 'unlock') {
                    $lelang = 0;
                    $ceklelangselesaitidakaturanpembayaran = 0;
                    if (!($unit_id == '3000' && ($kegiatan_code == '1.04.21.0002') && ($rd->getKomponenId() == '27.01.01.01' || $rd->getKomponenId() == '27.01.01.152' || $rd->getKomponenId() == '27.03.01.02'))) {
                        if (sfConfig::get('app_fasilitas_cekeProject') == 'buka') {
                            if (sfConfig::get('app_fasilitas_cekServer') == 'buka') {
                                $lelang = $rd->getCekLelang($rd->getUnitId(), $rd->getKegiatanCode(), $rd->getDetailNo(), $rd->getNilaiAnggaran());
                                if (sfConfig::get('app_fasilitas_cekeDelivery') == 'buka') {
                                    $ceklelangselesaitidakaturanpembayaran = $rd->getCekLelangTidakAdaAturanPembayaran($rd->getUnitId(), $rd->getKegiatanCode(), $rd->getDetailNo());
                                }
                            }
                        }
                    }

                    if ($lelang == 0 && $ceklelangselesaitidakaturanpembayaran == 0 && $rd->getNoteSkpd() != '' && !is_null($rd->getDetailName())) {
                        echo form_tag("entri/ubahhargadasar?id=$id&unit_id=$unit_id&kegiatan_code=$kegiatan_code&detail_no=$no");
                        echo input_tag("komponen_harga_awal", '', array('style' => 'width:50px', 'value' => $rd->getKomponenHargaAwal()));
                        echo "<br>";
                        echo submit_tag(' OK ');
                        echo "</form>";
                    } else {
                        if ($lelang > 0) {
                            echo '<br/><span class="badge badge-danger">[Lelang Sedang Berjalan]</span>';
                        }
                        if ($ceklelangselesaitidakaturanpembayaran == 1) {
                            echo '<br/><span class="badge badge-danger">[Belum ada Aturan Pembayaran]</span>';
                        }
                    }
                    if ($rd->getSatuan()=='Paket' && $rd->getVolume()==1)
                    {
                         echo '<br/><span class="badge badge-danger">[Telah dilakukan Penggunaan Sisa Lelang, tidak dapat mengedit volume]</span>';
                    }
                   
                   
                } else if($rd->getIsHpsp()) {
                    echo '<br/><span class="badge badge-primary">[Merupakan komponen Hasil PSP]</span>';
                }
            }
            ?>
        </td>
        <td style="background: <?php echo $warna ?>" align="right">
            <?php
            $volume = $rd->getVolume();
            $harga = $rd->getKomponenHargaAwal();
            $hasil = floor($volume * $harga);

            if ($hasil < 1) {
                echo number_format($hasil, 4, ',', '.');
            }
            else {
                echo number_format($hasil, 0, ',', '.');    
            }
            ?>
        </td>
        <td style="background: <?php echo $warna ?>" align="right">
            <?php echo $rd->getPajak() . '%'; ?>
        </td>
        <td style="background: <?php echo $warna ?>" align="right">
            <?php
            $volume = $rd->getVolume();
            $harga = $rd->getKomponenHargaAwal();
            $pajak = $rd->getPajak();
            $total = $rd->getNilaiAnggaran();
            if ($total < 1) {
                echo number_format($total, 4, ',', '.');
            }
            else {
                echo number_format($total, 0, ',', '.');    
            }
            $total_per_rekening += $total;
            ?>
        </td>
         <td style="background: <?php echo $warna ?>" align="right">
            <?php if (is_null($rd->getNoteSkpd()) ||$rd->getNoteSkpd() == '' ) {?>
            <?php echo number_format($rd->getAnggaranRasionalisasi(), 0, ',', '.'); }
                else 
                { echo '0';}?>
        </td>
        <td style="background: <?php echo $warna ?>" align="center">
            <?php
            $rekening = $rd->getRekeningCode();
            $rekening_code = substr($rekening, 0, 6);
            $c = new Criteria();
            $c->add(KelompokBelanjaPeer::BELANJA_CODE, $rekening_code);
            $rs_rekening = KelompokBelanjaPeer::doSelectOne($c);
            if ($rs_rekening) {
                echo $rs_rekening->getBelanjaName();
            }
            ?>
        </td>
        <?php if(sfConfig::get('app_tahap_edit') != 'murni'): ?>
            <td style="background: <?php echo $warna ?>">
                <?php

                $query = "select dp.pekerjaan_id as pekerjaan_id
                    from eproject.detail_kegiatan dk, eproject.detail_pekerjaan dp
                    where dp.detail_kegiatan_id=dk.id and 
                    dk.kode_detail_kegiatan= '".$rd->getDetailKegiatan()."'";

                $con = Propel::getConnection();
                $statement = $con->prepareStatement($query);
                $list_pekerjaan = $statement->executeQuery();

                $arr_list_pekerjaan  = array();
                foreach($list_pekerjaan  as $arrs){
                foreach($arrs as $index=>$arr){                    
                       // array_push($arr_list_pekerjaan, $index);
                        $arr_list_pekerjaan[$arr] = $arr;
                        }
                }

                
                if ($rd->getStatusLelang() == 'unlock') {
                    $detno = $rd->getDetailNo();
                    echo 'Catatan SKPD:<br/>';
                    echo form_tag("entri/simpanCatatan?unit_id=$unit_id&kegiatan_code=$kegiatan_code&detail_no=$detno");
                    echo input_tag("catatan_skpd", $rd->getNoteSkpd(), array('style' => 'width:100px'));
                    echo "<br>";
                    echo 'Keterangan SKPD:<br/>';
                    //echo input_tag("keterangan_skpd", $rd->getDetailName(), array('style' => 'width:100px'));
                    // echo select_tag('keterangan_skpd', objects_for_select($list_pekerjaan,$list_pekerjaan->getString('pekerjaan_id'),'','', 'include_custom=--Pilih Pekerjaan--'), array('class' => 'js-example-basic-single select2', 'style' => 'width:100%'));
                    if($rd->getSatuan() == 'Paket' && $rd->getTipe() == 'EST')
                    {$i++

                        ?>
                          <input type="hidden" name="keterangan_skpd" value=" " />
                        <?php
                    }
                    else
                    {
                      echo select_tag('keterangan_skpd', options_for_select($arr_list_pekerjaan, isset($keterangan_skpd) ? $keterangan_skpd : '', array('include_custom' => '--Pilih Pekerjaan--')), array('class' => 'select2','style' => 'width:100%'));
                    }
                    echo "<br>";
                    echo submit_tag(' OK ');
                    echo "</form>";
                    echo "<br/><font style='font-weight: bold; color: red'>--Isi kolom ini untuk membuka Field Penggunaan Sisa Lelang. Untuk kolom catatan skpd isi catatan, untuk kolom keterangan_skpd isi id pemaketan dan untuk komponen fisik ditambahkan alamat (muncul di lembar RKA).-</font>";
                }
                ?>
            </td>
        <?php endif; ?>
    </tr>
<?php
$i_modal++;
endforeach;
?>

<?php
    $c = new Criteria();
    $c->add(RekeningPeer::REKENING_CODE, $temp_rekening);
    $rs_rekening = RekeningPeer::doSelectOne($c);
    if($rs_rekening){
        $rekening_name = $rs_rekening->getRekeningName();
    }
?>

<tr class="pekerjaans_<?php echo $id ?>">
        <td colspan="7" class="text-right"><b>Total <?php echo $rekening_name ?></b></td>
        <td class="text-right"><b><?php echo number_format(round($total_per_rekening, 0), 0, ',', '.') ?></b></td>
        <td colspan="2"></td>
</tr>

<script>
     $(".saveGantiRekening").click(function () { // changed
        var id_form = $(this).closest("form").attr("id"); //parent form
        var detailno = id_form.split("_").pop();
        $.ajax({
            type: "POST",
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/entri/gantiRekeningRevisi.html",
            data: $(this).parent().serialize(), // changed
            success: function () {
                $('#action_pekerjaans_' + detailno).html('<br/><font style="color: green;font-weight:bold">{Silahkan Refresh Halaman}</font><br/><br/>');
            }
        });
        return false; // avoid to execute the actual submit of the form.
    });

     function execKembalikanWaiting(detailno, unitid, kodekegiatan, id) {
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/entri/kembalikanWaitingEdit/unitid/" + unitid + "/kodekegiatan/" + kodekegiatan + "/detailno/" + detailno + ".html",
            context: document.body
        }).done(function (msg) {
            $('#tempat_ajax_' + id).html(msg);
        });
    }

    function tampilModalCatatan(obj){
        var unit_id = $(obj).data('unit');
        var kegiatan_code = $(obj).data('kegiatancode');
        var rekening = $(obj).data('rekening');
        var nama_rekening = $(obj).data('namarekening');

        $.ajax({
            // url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/entri/cekCatatanRekening/unit_id/"+unit_id+"/kegiatan_code/"+kegiatan_code+"/rekening/"+rekening+".html",
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/entri/cekCatatanRekening.html",
            dataType: "json",
            type: 'POST',
            data: {
                unit_id: unit_id,
                kegiatan_code: kegiatan_code,
                rekening: rekening
            },
            success: function(data){
                console.log(data);

                $('#catatan').val(''); 
                if(data.ada == 1){
                    $('#catatan').val(data.catatan);
                }
                
                $('#catatan_unit_id').val(unit_id);
                $('#catatan_kegiatan_code').val(kegiatan_code);
                $('#catatan_rekening_code').val(rekening);
                $('#catatan_rekening_nama').val(rekening+' '+nama_rekening);
        
                $('#ModalCatatan').modal('show');
            }
        })

    }


</script>