<?php use_helper('I18N', 'Date', 'Object', 'Javascript', 'Validation') ?>
<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Ubah Profil User</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Pengguna</a></li>
          <li class="breadcrumb-item active">Ubah Profil User</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>
<!-- Main content -->
<section class="content">
    <?php include_partial('entri/list_messages'); ?>
    <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">
                        <i class="fab fa-foursquare"></i> Form
                    </h3>
                </div>
                <div class="card-body">
                    <?php echo form_tag('entri/ubahProfil', array('class' => 'form-horizontal')); ?>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                            <label>Nama User</label>
                            <?php echo input_tag('nama', $profil->getUserName(), array('class' => 'form-control', 'placeholder' => $profil->getUserName())) ?>
                        </div>
                      </div>
                      <!-- /.col -->
                      <div class="col-md-6">
                        <div class="form-group">
                            <label>NIP</label>
                            <?php echo input_tag('nip', $profil->getNip(), array('class' => 'form-control', 'placeholder' => $profil->getNip())) ?>
                        </div>
                      </div>
                      <!-- /.col -->
                      <div class="col-md-6">
                        <div class="form-group">
                            <label>Jenis Kelamin</label><br/>
                            <?php
                            if ($profil->getJenisKelamin() == 'lakilaki') {
                                echo radiobutton_tag('jenisKelamin', 'lakilaki', array('checked' => "checked")) . 'Laki Laki <br>';
                                echo radiobutton_tag('jenisKelamin', 'perempuan') . 'Perempuan';
                            } else if ($profil->getJenisKelamin() == 'perempuan') {
                                echo radiobutton_tag('jenisKelamin', 'lakilaki') . 'Laki Laki <br>';
                                echo radiobutton_tag('jenisKelamin', 'perempuan', array( 'checked' => "checked")) . 'Perempuan';
                            } else {
                                echo radiobutton_tag('jenisKelamin', 'lakilaki') . 'Laki Laki <br>';
                                echo radiobutton_tag('jenisKelamin', 'perempuan') . 'Perempuan';
                            }
                            ?>
                        </div>
                      </div>
                      <!-- /.col -->
                      <div class="col-md-6">
                        <div class="form-group">
                            <label>Email</label>
                            <?php echo input_tag('email', $profil->getEmail(), array('class' => 'form-control')); ?>
                        </div>
                      </div>
                      <!-- /.col -->
                      <div class="col-md-12">
                        <div class="form-group">
                            <label>No Telepon / HP</label>
                            <?php echo input_tag('telepon', $profil->getTelepon(), array('class' => 'form-control')); ?>
                        </div>
                      </div>
                      <!-- /.col -->
                      <div class="col-md-2">
                        <div class="form-group">
                            <label class="tombol_filter">Tombol Filter</label><br/>
                            <button type="submit" name="commit" class="btn btn-outline-primary btn-sm">Simpan <i class="far fa-save"></i></button>
                            <button type="reset" name="reset" class="btn btn-outline-danger btn-sm">Reset <i class="fa fa-backspace"></i></button>
                        </div>
                        <!-- /.form-group -->
                      </div>
                      <!-- /.col -->                      
                    </div>
                    <?php echo '</form>'; ?>
                </div>
            </div>
          </div>
        </div>
    </div>
</section>