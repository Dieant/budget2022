<?php

if (isset($filters['tahap']) && $filters['tahap'] != '') {
    echo '';
} else {
    $x = new Criteria();
    $x->add(DinasRincianPeer::KEGIATAN_CODE, $master_kegiatan->getKodeKegiatan());
    $x->add(DinasRincianPeer::UNIT_ID, $master_kegiatan->getUnitId());
    $v = DinasRincianPeer::doSelectOne($x);
    if ($v) {
        if ($v->getRincianLevel() == 1) {
            $status = 'BAPPEKO';
            $c_rincian_bappeko = new Criteria();
            $c_rincian_bappeko->add(RincianBappekoPeer::UNIT_ID, $master_kegiatan->getUnitId());
            $c_rincian_bappeko->add(RincianBappekoPeer::KODE_KEGIATAN, $master_kegiatan->getKodeKegiatan());
            $c_rincian_bappeko->add(RincianBappekoPeer::TAHAP, DinasMasterKegiatanPeer::getTahapKegiatan($master_kegiatan->getUnitId(), $master_kegiatan->getKodeKegiatan()));
            if (!$rs_rincian_bappeko = RincianBappekoPeer::doSelectOne($c_rincian_bappeko)) {
                $status = 'DINAS';
            }
        } else if ($v->getRincianLevel() == 2) {
            $status = 'DINAS';
        } else if ($v->getRincianLevel() == 3) {
            $status = 'PENELITI';
        }
    } else {
        $status = '';
    }
    echo $status;
}