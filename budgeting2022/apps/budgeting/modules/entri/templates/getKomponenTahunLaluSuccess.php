<?php use_helper('Url', 'Javascript', 'Form', 'Object'); ?>
<?php
$i = 0;
?>
<tr class="mirip3" style="text-align: center; font-weight: bold">
    <th>Nama Komponen</th>
    <th>Harga</th>
    <th>Pajak</th>
</tr>
<?php
foreach ($list as $value) {
    if ($value['komponen_non_pajak'] == 't') {
        $pajak = 0;
    } else {
        $pajak = 10;
    }
    ?>
    <tr class="mirip3" style="text-align: center">
        <td><?php echo $value['komponen_name'] ?></td>
        <td><?php echo number_format($value['komponen_harga'], 0, ',', '.') ?></td>
        <td><?php echo $pajak ?></td>
    </tr>
    <?php
}
?>
<tr class="mirip3">
    <td colspan="13">&nbsp;</td>
</tr>