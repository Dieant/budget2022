<?php use_helper('I18N', 'Date', 'Url', 'Javascript', 'Form', 'Object') ?>

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>Daftar Komponen Kegiatan SKPD <?php echo $kode_kegiatan; ?> Di Musrenbang</h1>
</section>

<!-- Main content -->
<section class="content">
    <!-- Default box -->

    <div class="box box-info">
        <div class="box-body">
            <div id="sf_admin_container">
                <div id="sf_admin_content" class="table-responsive">
                    <table cellspacing="0" class="sf_admin_list">
                        <thead>
                            <tr>
                                <th><b>Nama Komponen</b></th>
                                <th><b>Volume</b></th>
                                <th><b>Satuan</b></th>
                                <th><b>Dari</b></th>
                                <th><b>Lokasi</b></th>
                                <th><b>Level</b></th>
                                <th><b>Keterangan</b></th>
                                <th><b>Catatan</b></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $counter = 0;
                            while($rs_rinciandetail->next()) {
                                $counter++;
                                // $nama = $rs_rinciandetail->getString('komponen_name');
                                // if(empty($nama)) {
                                //     $style = "style='color: red;'";
                                //     $check = "";
                                //     $rek = true;
                                // } else {
                                //     $style = '';
                                //     $check = "name='pilihAction[]'";
                                //     $rek = false;
                                // }
                                echo "<tr $style>";
                                
                                echo "<td>".$rs_rinciandetail->getString('komponen_name')."</td>";
                                echo "<td>".$rs_rinciandetail->getString('volume')."</td>";
                                echo "<td>".$rs_rinciandetail->getString('satuan')."</td>";
                                echo "<td>".$rs_rinciandetail->getString('dari')."</td>";
                                echo "<td>".$rs_rinciandetail->getString('lokasi')."</td>";
                                echo "<td>".$rs_rinciandetail->getString('level')."</td>";
                                echo "<td>".$rs_rinciandetail->getString('keterangan')."</td>";
                                echo "<td>".$rs_rinciandetail->getString('catatan')."</td>";

                                echo "</tr>";
                            }
                            ?>
                            <?php if($counter <= 0): ?>
                            <tr><td colspan="8" align="center">Tidak ada komponen yang bisa ditampilkan</td></tr>
                            <?php endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <br/>
            <?php
            if($counter > 0)
                // echo submit_tag('Proses ke Penarikan', array('name' => 'proses', 'class' => 'btn btn-success btn-flat')) . '&nbsp;';
            ?>
        </div>
    </div>

</section><!-- /.content -->

<script>
    $("#cekSemua").change(function () {
        $(".cek").prop('checked', $(this).prop("checked"));
    });
</script>