<?php use_helper('Object', 'Javascript', 'Number', 'Validation') ?>

<?php use_stylesheet('/sf/sf_admin/css/main') ?>

<!-- Masih harus Melanjutkan Menu Edit -->
<div id="sf_admin_container">
    <h1><?php echo 'Daftar Isian Rincian Komponen Kegiatan SKPD' ?></h1>

    <div id="sf_admin_content">

        <div class="sf_admin_filters">
            <?php echo form_tag('entri/carikomponen', array('method' => 'get')) ?>

            <fieldset>
                <h2><?php echo 'Mencari Komponen Berdasarkan Nama Komponen' ?></h2>
                <div class="form-row">
                    <label for="filters_nama_komponen"><?php echo 'Nama komponen:' ?></label>
                    <div class="content" style="min-height: 25px">
                        <?php
                        echo input_tag('filters[nama_komponen]', isset($filters['nama_komponen']) ? $filters['nama_komponen'] : null, array(
                            'size' => NULL,
                        ))
                        ?>
                    </div>
                </div>

            </fieldset> 
            <?php
            echo input_hidden_tag('kegiatan', $sf_params->get('kegiatan'));
            echo input_hidden_tag('unit', $sf_params->get('unit'));
            ?>
            <ul class="sf_admin_actions">
                <li><?php echo submit_tag('cari', 'name=filter class=sf_admin_action_filter') ?></li>
            </ul>

            </form>
        </div>
        <?php if ($sf_flash->has('subtitletidakada')): ?>
            <div class="form-errors">
                <h2><?php echo ($sf_flash->get('subtitletidakada')) ?></h2>
            </div>
        <?php endif; ?>
        <?php if ($sf_flash->has('lokasitidakada')): ?>
            <div class="form-errors">
                <h2><?php echo ($sf_flash->get('lokasitidakada')) ?></h2>
            </div>

        <?php endif; ?>
        <?php echo form_tag('entri/baruKegiatan') ?>
        <table cellspacing="0" class="sf_admin_list">
            <thead>
                <tr>
                    <th><b>Nama</b></th>
                    <th>&nbsp;</th>
                    <th><b>Isian</b></th>
                </tr>
            </thead>
            <tbody>
                <tr class="sf_admin_row_0" align='right'>
                    <td>Kelompok Belanja</td>
                    <td align="center">:</td>
                    <td align="left">
                        <?php
                        $rekening_code = trim($sf_params->get('rekening'));
                        $belanja_code = substr($rekening_code, 0, 5);
                        $c = new Criteria();
                        $c->add(KelompokBelanjaPeer::BELANJA_CODE, $belanja_code);
                        $rs_belanja = KelompokBelanjaPeer::doSelectOne($c);
                        if ($rs_belanja) {
                            echo $rs_belanja->getBelanjaName();
                        }
                        ?></td>
                </tr>
                <tr class="sf_admin_row_1" align='right'>
                    <td>Kode Rekening</td>
                    <td align="center">:</td>
                    <td align="left">
                        <?php
                        $c = new Criteria();
                        $c->add(RekeningPeer::REKENING_CODE, $rekening_code);
                        $rs_rekening = RekeningPeer::doSelectOne($c);
                        if ($rs_rekening) {
                            $pajak_rekening = $rs_rekening->getRekeningPpn();
                            echo $rekening_code . ' ' . $rs_rekening->getRekeningName();
                        }
                        ?></td>
                </tr>
                <tr class="sf_admin_row_0" align='right'>
                    <td>Komponen</td>
                    <td align="center">:</td>
                    <td align="left"><?php echo $rs_komponen->getKomponenName() ?></td>
                </tr>
                <tr class="sf_admin_row_1" align='right'>
                    <td>Harga</td>
                    <td align="center">:</td>
                    <?php
                    $komponen_id = $rs_komponen->getKomponenId();
                    $komponen_harga = $rs_komponen->getKomponenHarga();
                    if ((substr($komponen_id, 0, 14) == '23.01.01.04.12') or ( substr($komponen_id, 0, 14) == '23.01.01.04.13') or ( substr($komponen_id, 0, 11) == '23.04.04.01')) {
                        ?>
                        <td align='left'><?php echo number_format($komponen_harga, 3, ',', '.') ?></td>
                        <?php
                    } else {
                        ?>
                        <td align='left'><?php echo '&nbsp;' . number_format($komponen_harga, 0, ',', '.') ?> </td>
                        <?php
                    }
                    ?>
                </tr>
                <tr class="sf_admin_row_0" align='right'>
                    <td>Satuan</td>
                    <td align="center">:</td>
                    <td align="left"><?php echo $rs_komponen->getSatuan(); ?></td>
                </tr>
                <tr class="sf_admin_row_1" align='right'>
                    <td>Pajak</td>
                    <td align="center">:</td>
                    <td align="left"><?php echo $sf_params->get('pajak') . '%' ?></td>
                </tr>
                <tr class="sf_admin_row_0" align='right'>
                    <td><span style="color:red;">*</span> Subtitle</td>
                    <td align="center">:</td>
                    <td align="left">
                        <?php
                        $kode_sub = '';
                        if ($sf_params->get('subtitle')) {
                            $kode_sub = $sf_params->get('subtitle');
                        }

                        echo select_tag('subtitle', objects_for_select($rs_subtitleindikator, 'getSubId', 'getSubtitle', $kode_sub, 'include_custom=---Pilih Subtitle---'), Array('onChange' => remote_function(Array('update' => 'sub1', 'url' => 'entri/pilihsubx?kegiatan_code=' . $sf_params->get('kegiatan') . '&unit_id=' . $sf_params->get('unit'), 'with' => "'b='+this.options[this.selectedIndex].value", 'loading' => "Element.show('indicator');Element.hide('sub1');", 'complete' => "Element.hide('indicator');Element.show('sub1');"))));
                        ?>
                    </td>
                </tr>
                <tr class="sf_admin_row_1" align='right'>
                    <td>Sub - Subtitle</td>
                    <td align="center">:</td>
                    <td align="left">
                        <div id="indicator" style="display:none;" align="center"><dt>&nbsp;</dt><dd><b>Mohon Tunggu </b><?php echo image_tag('loading.gif', array('align' => 'absmiddle')) ?></dd></div>
                        <?php
                        if ($sf_params->get('sub')) {
                            $kode_subsubtitle = $sf_params->get('sub');
                            $d = new Criteria();
                            $d->add(DinasRincianSubParameterPeer::KODE_SUB, $kode_subsubtitle);
                            $rs_rinciansubparameter = RincianSubParameter::doSelectOne($d);
                            if ($rs_rinciansubparameter) {

                                echo select_tag('sub', options_for_select(array($rs_rinciansubparameter->getKodeSub() => $rs_rinciansubparameter->getNewSubtitle()), $rs_rinciansubparameter->getKodeSub(), 'include_custom=---Pilih Subtitle Dulu---'), Array('id' => 'sub1'));
                            }
                        } elseif (!$sf_params->get('sub')) {
                            echo select_tag('sub', options_for_select(array(), '', 'include_custom=---Pilih Subtitle Dulu---'), Array('id' => 'sub1'));
                        }
                        ?>
                    </td>
                </tr>

                <?php
                $tipe = $sf_params->get('tipe');
                if (($tipe == 'FISIK') || ($sf_params->get('lokasi'))) {
                    if ($sf_params->get('lokasi')) {
                        $lokasi = base64_decode($sf_params->get('lokasi'));
                    } elseif (!$sf_params->get('lokasi')) {
                        $lokasi = '';
                    }
                    ?>
                    <tr class="sf_admin_row_0" align='right' valign="top">
                        <td>Lokasi</td>
                        <td align="center">:</td>
                        <td align="left">
                            <?php
                            echo input_tag('lokasi', $lokasi, 'size=50') . submit_tag('cari', 'name=cari');
                            echo ' Usulan dari : ';
                            echo select_tag('jasmas', objects_for_select($rs_jasmas, 'getKodeJasmas', 'getNama', $sf_params->get('jasmas'), 'include_custom=--Pilih--'));
                            echo '<br />';
                            if (!$lokasi == '') {
                                $kode_lokasi = '';
                                $banyak = 0;
                                $c = new Criteria();
                                $c->add(VLokasiPeer::NAMA, $lokasi, Criteria::ILIKE);
                                $rs_lokasi = VLokasiPeer::doSelectOne($c);
                                if ($rs_lokasi) {
                                    $kode_lokasi = $rs_lokasi->getKode();
                                }

                                $sql = new Criteria();
                                if ($kode_lokasi) {
                                    $kod = '%' . $kode_lokasi . '%';
                                    $sql->add(HistoryPekerjaanPeer::KODE, strtr($kod, '*', '%'), Criteria::ILIKE);

                                    $sql->addAscendingOrderByColumn(HistoryPekerjaanPeer::TAHUN);
                                    $sqls = HistoryPekerjaanPeer::doSelect($sql);
                                    ?>
                                    <table cellspacing="0" class="sf_admin_list">
                                        <thead>
                                            <tr>
                                                <th>Tahun</th>
                                                <th>Lokasi</th>
                                                <th>Nilai</th>
                                                <th>volume</th>
                                            <tr>
                                        </thead>
                                        <?php
                                        $tahun = '';
                                        $total_lokasi = 0;
                                        foreach ($sqls as $vx) {
                                        //while ($rsPeta->next())
                                            if ($tahun == '') {
                                                $tahun = $vx->getTahun();
                                            }
                                            //$s -> addOr(DinasSubtitleIndikatorPeer::SUBTITLE, $vx->getSubtitle());
                                            if (($tahun <> $vx->getTahun()) && ($tahun <> '')) {
                                                $tahun = $vx->getTahun();
                                                ?>
                                                <tbody>
                                                    <tr class="sf_admin_row_1" align='right'>
                                                        <td>&nbsp;</td>
                                                        <td align="right">Total :</td>
                                                        <td align="right"> <?php echo number_format($total_lokasi, 0, ',', '.'); ?></td>
                                                        <td> <?php $total_lokasi = 0 ?></td>
                                                    </tr>
                                                    <tr><td colspan="4"></td></tr>
                                                    <tr>
                                                        <td> <?php echo $vx->getTahun(); ?></td>
                                                        <td> <?php echo $vx->getLokasi(); ?></td>
                                                        <td align="right"> <?php echo number_format($vx->getNilai(), 0, ',', '.'); ?></td>
                                                        <td> <?php echo $vx->getVolume(); ?></td>
                                                    </tr>
                    <?php
                    $total_lokasi+=$vx->getNilai();
                } else {
                    ?>
                                                    <tr>
                                                        <td> <?php echo $vx->getTahun(); //$rsPeta->getInt('tahun') ?></td>
                                                        <td> <?php echo $vx->getLokasi(); //$rsPeta->getString('lokasi') ?></td>
                                                        <td align="right"> <?php echo number_format($vx->getNilai(), 0, ',', '.'); //$rsPeta->getString('nilai') ?></td>
                                                        <td> <?php echo $vx->getVolume(); //$rsPeta->getString('nomor') ?></td>
                                                    </tr>
                    <?php
                    $total_lokasi+=$vx->getNilai();
                }
                if ($vx->getNilai() > 0) {
                    $banyak+=1;
                }
            }
            ?>
                                            <tr class="sf_admin_row_1" align='right'>
                                                <td> </td>
                                                <td align="right">Total :</td>
                                                <td align="right"> <?php echo number_format($total_lokasi, 0, ',', '.'); //$rsPeta->getString('nilai') ?></td>
                                                <td> <?php $total_lokasi = 0 ?></td>
                                            </tr>
                                            <tr><td colspan="4">&nbsp;</td></tr>
            <?php
            $query = "select * from " . sfConfig::get('app_default_schema') . ".rincian_detail where detail_name ilike '$lokasi'";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs_rincianlokasi = $stmt->executeQuery();
            while ($rs_rincianlokasi->next()) {
                ?>
                                                <tr>
                                                    <td> 2009</td>
                                                    <td> <?php echo $rs_rincianlokasi->getString('komponen_name') . ' ' . $rs_rincianlokasi->getString('detail_name') ?></td>
                                                    <td align="right"> 
                                                <?php
                                                $pajak = $rs_rincianlokasi->getString('pajak');
                                                $harga = $rs_rincianlokasi->getString('komponen_harga_awal');
                                                $volume = $rs_rincianlokasi->getString('volume');
                                                $nilai = ($volume * $harga * (100 + $pajak) / 100);
                                                echo number_format($nilai, 0, ',', '.');
                                                ?></td>
                                                    <td> <?php echo $rs_rincianlokasi->getString('keterangan_koefisien'); ?></td>
                                                </tr>
                                                        <?php
                                                        if ($volume > 0) {
                                                            $banyak+=1;
                                                        }
                                                        $total_lokasi+=$nilai;
                                                    }
                                                    ?>
                                            <tr class="sf_admin_row_1" align='right'>
                                                <td> </td>
                                                <td align="right">Total :</td>
                                                <td align="right"> <?php echo number_format($total_lokasi, 0, ',', '.'); //$rsPeta->getString('nilai') ?></td>
                                                <td> <?php $total_lokasi = 0 ?></td>
                                            </tr>
                                            <tr><td colspan="4">&nbsp;</td></tr>
                                        </tbody>

                                    </table>
            <?php
        }
        if ($banyak <= 0) {
            echo '<span style="color:green;"> <small> :. Belum pernah ada Pekerjaan .: </small></span>';
            echo input_hidden_tag('status', 'ok');
        }

        if ($banyak > 0) {
            echo '<span style="color:red;">  <small> :. Pekerjaan Pada Lokasi Ini Sudah Ada, Dapat Disimpan, Tetapi Tidak Dapat Masuk RKA .: </small></span>';
            echo input_hidden_tag('status', 'pending');
        }
        ?>
                            </td>
                        </tr>
                                <?php
                            }
                        } else {
                            ?>
                    <tr class="sf_admin_row_0" align='right' valign="top">
                        <td>Keterangan</td>
                        <td align="center">:</td>
                        <td align='left'><?php echo input_tag('keterangan', $sf_params->get('keterangan')) ?></td>
                    </tr>
                    <?php
                }
                ?>
                <tr class="sf_admin_row_1" align='right' valign="top">
                    <td><span style="color:red;">*</span>Volume</td>
                    <td align="center">:</td>
                    <td align="left">
<?php
$keterangan_koefisien = $sf_params->get('keterangan_koefisien');
$pisah_kali = explode('X', $keterangan_koefisien);
for ($i = 0; $i < 4; $i++) {
    $satuan = '';
    $volume = '';
    $nama_input = 'vol' . ($i + 1);
    $nama_pilih = 'volume' . ($i + 1);
    ;
    if (!empty($pisah_kali[$i])) {
        $pisah_spasi = explode(' ', $pisah_kali[$i]);
        $j = 0;

        for ($s = 0; $s < count($pisah_spasi); $s++) {
            if ($pisah_spasi[$s] != NULL) {
                if ($j == 0) {
                    $volume = $pisah_spasi[$s];
                    $j++;
                } elseif ($j == 1) {
                    $satuan = $pisah_spasi[$s];
                    $j++;
                } else {
                    $satuan.=' ' . $pisah_spasi[$s];
                }
            }
        }
    }
    if ($i !== 3) {
        echo input_tag($nama_input, $volume) . ' ' . select_tag($nama_pilih, objects_for_select($rs_satuan, 'getSatuanName', 'getSatuanName', $satuan, 'include_custom=---Pilih Satuan--')) . '<br />  X <br />';
    } else {
        echo input_tag($nama_input, $volume) . ' ' . select_tag($nama_pilih, objects_for_select($rs_satuan, 'getSatuanName', 'getSatuanName', $satuan, 'include_custom=---Pilih Satuan--'));
    }
}
?></td>
                </tr>
            </tbody>
            <tfoot>
                <tr class="sf_admin_row_0" align='right' valign="top">
                    <td>&nbsp; </td>
                    <td>
                        <?php
                        echo input_hidden_tag('kegiatan', $sf_params->get('kegiatan'));
                        echo input_hidden_tag('unit', $sf_params->get('unit'));
                        echo input_hidden_tag('id', $sf_params->get('komponen'));
                        echo input_hidden_tag('pajak', $sf_params->get('pajak'));
                        echo input_hidden_tag('tipe', $sf_params->get('tipe'));
                        echo input_hidden_tag('rekening', $sf_params->get('rekening'));
                        echo input_hidden_tag('referer', $sf_request->getAttribute('referer'));
                        ?>
                    </td>
                    <td><?php echo submit_tag('simpan', 'name=simpan') . ' ' . button_to('kembali', '#', array('onClick' => "javascript:history.back()")) ?></li></td>
                </tr>
            </tfoot>
        </table>
        </form>

    </div>
</div>