<?php
// auto-generated by sfPropelAdmin
// date: 2009/02/19 14:40:39
?>

<th id="sf_admin_list_th_kodekegiatan">
    <?php echo __('ID') ?>
</th>
<th id="sf_admin_list_th_namakegiatan">
    <?php echo __('Nama Sub Kegiatan') ?>
</th>
<?php if (sfConfig::get('app_tahap_edit') == 'murni') { ?>
   <th id="sf_admin_list_th_alokasidana">
        <?php  echo __('Pagu') ?>
    </th>
    <!-- <th id="sf_admin_list_th_alokasidana" style="background-color: #e4ed6d;">
        <?php // echo __('Pagu') ?>
        <?php echo __('Pagu Perangkaan') ?>
    </th>
    <th id="sf_admin_list_th_alokasidana" style="background-color: #e4ed6d;">
        <?php // echo __('Pagu') ?>
        <?php echo __('Pagu Penyesuaian') ?>
    </th> -->
    <?php
} else if (sfConfig::get('app_tahap_edit') != 'murni' && sfConfig::get('app_tahap_edit') != 'pak') {
    ?>
    <th id="sf_admin_list_th_alokasidana">
        <?php echo __('Semula') ?>
    </th>
<?php }  else if (sfConfig::get('app_tahap_edit') == 'pak') {
    ?>
    <th id="sf_admin_list_th_alokasidana">
        <?php echo __('Revisi 6') ?>
    </th> 
<?php }?>
<?php if (sfConfig::get('app_tahap_edit') == 'pak') { ?>
    <th id="sf_admin_list_th_alokasidana">
        <?php echo __('Pagu KUA PPAS') ?>
    </th>
<?php } ?>
<?php if (sfConfig::get('app_tahap_edit') != 'pak'  && sfConfig::get('app_tahap_edit') <> 'murni' ) { ?>
<th id="sf_admin_list_th_nilairincian">
    <?php echo __('Menjadi') ?>
</th>
<?php } ?>
<th id="sf_admin_list_th_nilairincian">
    <?php echo __('Rincian') ?>
</th>
<th id="sf_admin_list_th_selisih">
    <?php echo __('Selisih') ?>
</th>
<?php if (sfConfig::get('app_tahap_edit') == 'pak' || sfConfig::get('app_tahap_edit') == 'murni') { ?>
<th id="sf_admin_list_th_selisih">
    <?php echo __('Tambahan Pagu') ?>
</th>
<?php } ?>
<th id="sf_admin_list_th_posisi">
    <?php echo __('Posisi') ?>
</th>
<th id="sf_admin_list_th_posisi">
    <?php echo __('Approval') ?>
</th>
<!-- irul 18 maret 2014 -- hidden catatan kegiatan dinas -->
<!--<th id="sf_admin_list_th_catatan">-->
<?php //echo __('Catatan Kegiatan') ?>
<!--</th>-->
<!-- irul 18 maret 2014 -- hidden catatan kegiatan dinas -->
<!--<th id="sf_admin_list_th_catatan" style="background-color: #e4ed6d;">
<?php //echo __('Catatan Rincian') ?>
</th>
<th id="sf_admin_list_th_catatan_pembahasan" style="background-color: #e4ed6d;">
<?php //echo __('Catatan Pembahasan') ?>
</th>-->
<th id="sf_admin_list_th_tahap">
    <?php echo __('Tahap') ?>
</th> 