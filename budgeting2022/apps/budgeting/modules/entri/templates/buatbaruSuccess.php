<?php use_helper('I18N', 'Date', 'Object', 'Javascript', 'Validation') ?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Tambah Komponen</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Lembar Kerja</a></li>
                    <li class="breadcrumb-item active">Tambah Komponen</li>
                </ol>
            </div>
        </div>
    </div>
</section>
<section class="content">
    <?php include_partial('entri/list_messages'); ?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="fab fa-foursquare"></i> Form Ubah
                        </h3>
                    </div>
                    <div class="card-body">
                        <?php echo form_tag('entri/baruKegiatan') ?>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                <label>Kelompok Belanja</label>
                                <?php
                                    $rekening_code = trim($sf_params->get('rekening'));
                                    if ( strpos($rekening_code,'5.2.')!==false ) {
                                        $belanja_code = substr($rekening_code, 0, 3);
                                    } else {
                                        $belanja_code = substr($rekening_code, 0, 6);
                                    }
                                    $c = new Criteria();
                                    $c->add(KelompokBelanjaPeer::BELANJA_CODE, $belanja_code);
                                    $rs_belanja = KelompokBelanjaPeer::doSelectOne($c);
                                    if ($rs_belanja) {
                                        echo  "<input class='form-control' value='".$rs_belanja->getBelanjaName()."'></input>";
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                <label>Kode Rekening</label>
                                <?php
                                    $c = new Criteria();
                                    $c->add(RekeningPeer::REKENING_CODE, $rekening_code);
                                    $rs_rekening = RekeningPeer::doSelectOne($c);
                                    if ($rs_rekening) {
                                        $pajak_rekening = $rs_rekening->getRekeningPpn();
                                        echo "<input class='form-control' value='".$rekening_code . ' ' . $rs_rekening->getRekeningName()."'></input>";
                                    }
                                ?>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                <label>Komponen</label>
                                    <?php echo "<input class='form-control' name='komponen_name' value='".$rs_komponen->getKomponenName()."'></input>"; ?>
                                    <input type="hidden" id="cek_nama_komponen" class="cek_nama_komponen" value="<?php (strpos($rs_komponen->getKomponenName(),'Gaji Pokok') !== false) ? 'Gaji Pokok' : '' ?>">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                <label>Harga</label>
                                <?php
                                    $komponen_id = $rs_komponen->getKomponenId();
                                    $komponen_harga = $rs_komponen->getKomponenHarga();
                                    if ($rs_komponen->getKomponenTipe() == 'FISIK') {
                                        $komponen_harga = $rs_komponen->getKomponenHargaBulat();
                                    }
                                    if ($komponen_harga != floor($komponen_harga)) {
                                        if ($rs_komponen->getSatuan() == '%') {
                                            echo "<span class='form-control'>".$komponen_harga."</span>";
                                        } else {
                                            echo "<span class='form-control'>".number_format($komponen_harga, 3, ',', '.')."</span>";
                                        } echo input_hidden_tag('harga', $komponen_harga);
                                    } else if ($rs_komponen->getHargaEditable() == 'TRUE')
                                    {
                                        echo input_tag('harga',$komponen_harga, array('name' => 'harga', 'required' => 'true', 'class' => 'form-control', 'onChange' => 'hitungTotal()'));
                                    }    
                                    else {
                                        if ($rs_komponen->getSatuan() == '%') {
                                            echo "<span class='form-control'>".$komponen_harga."</span>";
                                        } else {
                                            echo "<span class='form-control'>".number_format($komponen_harga, 0, ',', '.')."</span>";
                                        } echo input_hidden_tag('harga', $komponen_harga);
                                    }
                                ?>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                <label>Satuan</label>
                                    <?php 
                                        echo "<span class='form-control'>".$rs_komponen->getSatuan()."</span>";
                                        echo input_hidden_tag('satuanx', $rs_komponen->getSatuan());
                                    ?>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                <label>Pajak</label>
                                <?php
                                    echo "<span class='form-control'>". $sf_params->get('pajak') . '%</span>';
                                    echo input_hidden_tag('pajakx', $sf_params->get('pajak'));
                                ?>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group"> <label><span style="color:red;">*</span> Subtitle</label>
                                    <?php
                                    $kode_sub = '';
                                    if ($sf_params->get('subtitle')) {
                                        $kode_sub = $sf_params->get('subtitle');
                                    }
                                    echo select_tag('subtitle', objects_for_select($rs_subtitleindikator, 'getSubId', 'getSubtitle', $kode_sub), array('class' => 'form-control select2', 'style' => 'width:100%'));
                                    ?>
                                </div>
                            </div>
                            <?php
                            $tipe = $sf_params->get('tipe');
                            if ($rs_komponen->getKomponenTipe2() == 'KONSTRUKSI' || $rs_komponen->getKomponenTipe2() == 'TANAH' || $rs_komponen->getKomponenTipe() == 'FISIK' || ($sf_params->get('lokasi'))) 
                            {
                            ?>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Nama Jalan</label>
                                    <input type="text" name="lokasi_jalan[]" class="form-control lokasi_jalan_isian" placeholder="Nama Jalan (*wajib diisi apabila lokasi berupa Jalan)">
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Gang/Blok/Kavling</label>
                                    <select class="form-control select2 tipe_gang_isian" name="tipe_gang[]">
                                        <option value="GG">--Pilih--</option>
                                        <option value="GG">GANG</option>
                                        <option value="BLOK">BLOK</option>
                                        <option value="KAV">KAVLING</option>
                                    </select>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Nama Gang/Blok/Kavling</label>
                                    <input type="text" name="lokasi_gang[]" class="form-control lokasi_gang_isian" +placeholder="Nama Gang/Blok/Kavling">
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Nomor Lokasi</label>
                                    <input type="text" name="lokasi_nomor[]" class="form-control lokasi_nomor_isian" placeholder="Nomor">
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>RW</label>
                                    <input type="text" name="lokasi_rw[]" class="form-control lokasi_rw_isian" placeholder="RW">
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>RT</label>
                                    <input type="text" name="lokasi_rt[]" class="form-control lokasi_rt_isian" placeholder="RT">
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label><span style="color:red;">*</span> Nama Bangunan/Saluran</label>
                                    <input type="text" name="lokasi_tempat[]" class="form-control lokasi_tempat_isian" placeholder="Nama Bangunan/Saluran (*wajib diisi apabila lokasi berupa bangunan/saluran/tempat)">
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Keterangan Lokasi</label>
                                    <input type="text" name="lokasi_keterangan[]" class="form-control lokasi_keterangan_isian" placeholder="Keterangan Lokasi">
                                </div>
                            </div>
                            <!-- /.col -->
                                <div class="col-sm-6">
                                <div class="form-group">
                                    <label><span style="color:red;">*</span> Kecamatan</label>
                                    <?php 
                                        echo select_tag('kecamatan', objects_for_select($nama_kecamatan_kel, 'getId', 'getNama', '', 'include_custom=---Pilih Kecamatan---'), array('class' => 'form-control select2', 'style' => 'width:100%'));
                                    ?>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label><span style="color:red;">*</span> Kelurahan</label>
                                    <div id="indicator" style="display:none;" align="center"><dt>&nbsp;</dt><dd><b>Mohon Tunggu </b><?php echo image_tag('loading.gif', array('align' => 'absmiddle')) ?></dd></div>
                                    <?php
                                        echo select_tag('kelurahan', options_for_select(array(), '', 'include_custom=---Pilih Kecamatan Dulu---'), Array('id' => 'kelurahan1', 'class' => 'form-control select2', 'style' => 'width:100%'));
                                    ?>
                                </div>
                            </div>
                            <!-- /.col -->
                            <?php
                            } else {
                            ?>
                            <div class='col-sm-12'>
                                <div class="form-group">
                                <label>Keterangan</label>
                                    <?php echo input_tag('keterangan', $sf_params->get('keterangan'), array('class' => 'form-control')) ?>
                                </div>
                            </div>
                            <?php
                            }
                            $keterangan_koefisien = $sf_params->get('keterangan_koefisien');
                            $pisah_kali = explode('X', $keterangan_koefisien);
                            $accres = '';
                            for ($i = 0; $i < 4; $i++) 
                            {
                                $satuan = '';
                                $volume = '';
                                $nama_input = 'vol' . ($i + 1);
                                $nama_pilih = 'volume' . ($i + 1);
                                ;
                                $disabled = false;
                                if (!empty($pisah_kali[$i])) {
                                    $pisah_spasi = explode(' ', $pisah_kali[$i]);
                                    $j = 0;

                                    for ($s = 0; $s < count($pisah_spasi); $s++) {
                                        if ($pisah_spasi[$s] != NULL) {
                                            if ($j == 0) {
                                                $volume = $pisah_spasi[$s];
                                                $j++;
                                            } elseif ($j == 1) {
                                                $satuan = $pisah_spasi[$s];
                                                $j++;
                                            } else {
                                                $satuan.=' ' . $pisah_spasi[$s];
                                            }

                                            if($satuan == 'Accres'){
                                                $accres = 'ada';
                                                $disabled = true;
                                            }
                                        }
                                    }
                                }
                                
                                if ($i == 0) {
                                ?>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Volume</label>
                                        <?php echo input_tag($nama_input, $volume, array('class' => 'form-control', 'onChange' => 'hitungTotal()', 'required' => 'true'));?>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Satuan</label>
                                        <?php echo select_tag($nama_pilih, objects_for_select($rs_satuan, 'getSatuanName', 'getSatuanName', $satuan, 'include_custom=--Pilih Satuan--'), array('class' => 'form-control select2')); ?>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>X</label>
                                    </div>
                                </div>
                                <?php
                                } elseif ($i !== 3) {
                                ?>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Volume</label>
                                        <?php echo input_tag($nama_input, $volume, array('class' => 'form-control', 'onChange' => 'hitungTotal()'));?>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Satuan</label>
                                        <?php echo select_tag($nama_pilih, objects_for_select($rs_satuan, 'getSatuanName', 'getSatuanName', $satuan, 'include_custom=--Pilih Satuan--'), array('class' => 'form-control select2')); ?>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>X</label>
                                    </div>
                                </div>
                                <?php
                                } else {
                                ?>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Volume</label>
                                        <?php echo input_tag($nama_input, $volume, array('class' => 'form-control', 'onChange' => 'hitungTotal()'));?>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Satuan</label>
                                        <?php echo select_tag($nama_pilih, objects_for_select($rs_satuan, 'getSatuanName', 'getSatuanName', $satuan, 'include_custom=--Pilih Satuan--'), array('class' => 'form-control select2')); ?>
                                    </div>
                                </div>
                                <?php
                                }
                            }
                            ?>

                            <!-- Jika Komponen Gaji Pokok -->
                            <?php if(strpos($rs_komponen->getKomponenName(),'Gaji Pokok') !== false) { ?>
                            <div class="col-sm-6 mt-3 mb-3">
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Kenaikan Gapok</span>
                                        </div>
                                        <?php echo input_tag('kenaikan_gapok', $rs_komponen->getKenaikanGapok(), array('readonly' => 'true', 'name' => 'kenaikan_gapok', 'class' => 'form-control')) ?>
                                        <div class="input-group-append">
                                            <span class="input-group-text">%</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 mt-3 mb-3">
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Acress</span>
                                        </div>
                                        <?php echo input_tag('acress', $rs_komponen->getAccres(), array('readonly' => 'true', 'name' => 'acress', 'class' => 'form-control')) ?>
                                        <div class="input-group-append">
                                            <span class="input-group-text">%</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>Jumlah Suami/Istri</label>
                                    <?php echo input_tag('jumlah_suami_istri', '0', array('name' => 'jumlah_suami_istri', 'class' => 'form-control jumlah_suami_istri', 'onkeyup'=>'cekMaksSuamiIstri(this)')) ?>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>Jumlah Anak</label>
                                    <?php echo input_tag('jumlah_anak', '0', array('name' => 'jumlah_anak', 'class' => 'form-control jumlah_anak', 'onkeyup'=>'cekMaksAnak(this)')) ?>
                                </div>
                            </div>
                            <?php } ?>

                            <div class='col-sm-12'>
                                <div class="form-group">
                                <label>Total</label>
                                    <?php echo input_tag('total', '', array('class'=> 'form-control', 'readonly' => 'true', 'name' => 'total')) ?>
                                </div>
                            </div>
                            <?php $koRek = substr($rs_komponen->getKomponenId() , 0, 18);
                            if(($koRek == '2.1.1.01.01.01.008' or $koRek == '2.1.1.03.05.01.001'  or $koRek == '2.1.1.01.01.01.004' or $koRek == '2.1.1.01.01.02.099' or $koRek == '2.1.1.01.01.01.005' or $koRek == '2.1.1.01.01.01.006') and ($rs_komponen->getSatuan() == 'Orang Bulan'))
                            {
                            ?>
                            <div class='col-sm-6'>
                                <div class="form-group">
                                <label>Volume Orang</label>
                                    <?php echo input_tag('volume_orang', '', array('class' => 'form-control', 'readonly' => 'true', 'name' => 'volume_orang', 'type'=> 'number')) ?>
                                </div>
                            </div>
                            <?php 
                            } 
                            ?>
                            <?php if ($sf_params->get('unit') == '1800' || $sf_params->get('unit') == '0300' || $sf_params->get('unit') == '9999') { ?>
                                <div class='col-sm-6'>
                                    <div class="form-group">
                                    <label>Komponen BLUD</label><br />
                                        <?php echo checkbox_tag('blud') ?><font style="color: green"> *Centang jika termasuk komponen BLUD</font>
                                    </label>
                                </div>
                                <div class='col-sm-6'>
                                    <div class="form-group">
                                    <label>Komponen Kapitasi BPJS</label><br />
                                        <?php echo checkbox_tag('kapitasi') ?><font style="color: green"> *Centang jika termasuk komponen Kapitasi BPJS</font>
                                    </div>
                                </div>
                            <?php } ?>
                            <?php if ($sf_params->get('unit') == '2000' || $sf_params->get('unit') == '9999') { ?>
                                <div class='col-sm-6'>
                                    <div class="form-group">
                                    <label>Komponen BOS</label><br />
                                        <?php echo checkbox_tag('bos') ?><font style="color: green"> *Centang jika termasuk komponen BOS</font>
                                    </div>
                                </div>
                                <div class='col-sm-6'>
                                    <div class="form-group">
                                    <label>Komponen BOPDA</label><br />
                                        <?php echo checkbox_tag('bopda') ?><font style="color: green"> *Centang jika termasuk komponen BOPDA</font>
                                    </div>
                                </div>
                            <?php } ?>

                            <!-- Checkbox Accres -->
                            <?php if(strpos($rs_komponen->getKomponenName(),'Gaji Pokok') !== false) { ?>
                            <!-- <div class="col-sm-12">
                                <div class="form-group">
                                    <label>Accres</label><br />
                                    <?php
                                        // if ($accres == 'ada') {
                                        //     echo checkbox_tag('checkbox_accres', 1, TRUE, array('class' => 'checkbox_accres', 'onclick'=>'setAccres()'));
                                        // } else {
                                        //     echo checkbox_tag('checkbox_accres', 1, FALSE, array('class' => 'checkbox_accres', 'onclick'=>'setAccres()'));
                                        // }
                                    ?>
                                    <font style="color: green"> *Centang jika Accres</font>
                                </div>
                            </div> -->
                            <?php }else{ ?>

                            <?php //if(strpos($rs_komponen->getKomponenName(),'Gaji Pokok') !== true) { ?>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Musrenbang</label><br />
                                    <?php
                                        echo checkbox_tag('musrenbang', array('class' => 'form-control'));
                                    ?>
                                    <font style="color: green"> *Centang jika komponen Musrenbang</font>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Hibah</label><br />
                                    <?php
                                        echo checkbox_tag('hibah', array('class' => 'form-control'));
                                    ?>
                                    <font style="color: green"> *Centang jika komponen Hibah</font>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                <label>Anggaran Sisa Pengadaan</label><br/>
                                    <input type="checkbox" onchange="toggleSisaLelang(this)" id="lelang" name="lelang">
                                    <font style="color: green"> *Centang jika komponen diambil dari anggaran sisa pengadaan</font>
                                    <input type="hidden" name="lebihDariLelang" id="anomali">
                                    <div class="table-responsive" id="tblSisaLelang">
                                        <table class="table table-bordered">
                                            <thead class="head_peach">
                                                <tr>
                                                    <th></th>
                                                    <th>Sisa Pemaketan</th>
                                                    <th>Nilai Diambil</th>
                                                    <th>Catatan</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                foreach ($data_oper as $val)
                                                {
                                                    $nilai_sisa = $val['sisa']; 
                                                    ?>
                                                    <tr>
                                                        <td width="4%" align="center">
                                                            <input type="checkbox" onchange="enableTextBox(this)" name="chkPSP[]" value="<?php echo $val['det_no']; ?>">
                                                            <input type="hidden" value="<?php echo $val['kom_id']; ?>" id="idPSP<?php echo $val['det_no']; ?>" name="idPSP<?php echo $val['det_no']; ?>">
                                                            <input type="hidden" value="<?php echo $val['sisa']; ?>" id="sisaPSP<?php echo $val['det_no']; ?>" name="sisaPSP<?php echo $val['det_no']; ?>">
                                                            <input type="hidden" value="<?php echo $val['det_no']; ?>" id="detNoPSP<?php echo $val['det_no']; ?>" name="detNoPSP<?php echo $val['det_no']; ?>">
                                                        </td>
                                                        <td><?php echo $val['kom_name'] . ' ' . $val['det_name'] . ' ' . ' ('.$val['kom_id'].')'; ?></td>
                                                        <td>
                                                            <span>Rp.</span> <input type="text" id="nilaiPSP<?php echo $val['det_no']; ?>" name="<?php echo $val['det_no']; ?>" value="<?php echo $val['sisa']; ?>" onkeyup="cekLebihDariLelang(this)" disabled>
                                                            <span>,-</span><br />
                                                            <span>Sisa Pengadaan: Rp. <?php echo $val['sisa']; ?>,-</span><br />
                                                            <i style="color: red; display: none;" id="warningPSP<?php echo $val['det_no']; ?>">Nilai tidak boleh melebihi sisa pengadaan!</i>
                                                        </td>
                                                        <td>
                                                            <textarea rows="4" cols="40" id="catatanPSP<?php echo $val['det_no']; ?>" name="catatanPSP<?php echo $val['det_no']; ?>" disabled></textarea>
                                                        </td>
                                                    </tr>
                                                <?php
                                                }
                                                if(!count($data_oper)) { ?>
                                                    <tr>
                                                        <td colspan="4" style="text-align: center;">Belum ada data yang bisa ditampilkan</td>
                                                    </tr>
                                                <?php
                                                }
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                            <?php if (sfConfig::get('app_fasilitas_bukaCatatanPergeseran') == 'buka')
                            {
                            ?>
                            <div class='col-sm-12'>
                                <div class="form-group">
                                <label><span style="color:red;">*</span> Catatan Pergeseran Anggaran</label>
                                    <?php echo textarea_tag('catatan', $sf_params->get('catatan'), array('class' => 'form-control')) ?>
                                </div>
                            </div>
                            <?php
                            } else {
                                $c = new Criteria();
                                $c->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                                $c->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                                $rs_kegiatan = DinasMasterKegiatanPeer::doSelectOne($c);
                                if ($rs_kegiatan->getTahap() == 'murni') {
                            ?>
                            <div class='col-sm-12'>
                                <div class="form-group">
                                <label><span style="color:red;">*</span> catatan usulan Anggaran</label>
                                    <?php echo textarea_tag('catatan', $sf_params->get('catatan'), array('class' => 'form-control')) ?>
                                </div>
                            </div>
                            <?php
                                }
                            }
                            ?>
                            <div class='col-sm-12'>
                                <div class="form-group">
                                <label>Catatan Koefisien (Catatan penjelasan singkat rincian koefisien untuk volume anggaran yang dimasukkan secara global)</label>
                                    <?php echo textarea_tag('catatan_koefisien', $sf_params->get('catatan_koefisien'), array('class' => 'form-control')) ?><br/>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="tombol_filter">Tombol Filter</label><br />
                                    <?php
                                    echo input_hidden_tag('id_login', $sf_user->getUserLogin());
                                    echo input_hidden_tag('kegiatan', $sf_params->get('kegiatan'));
                                    echo input_hidden_tag('unit', $sf_params->get('unit'));
                                    echo input_hidden_tag('id', $sf_params->get('komponen'));
                                    echo input_hidden_tag('pajak', $sf_params->get('pajak'));
                                    echo input_hidden_tag('tipe', $sf_params->get('tipe'));
                                    echo input_hidden_tag('rekening', $sf_params->get('rekening'));
                                    echo input_hidden_tag('referer', $sf_request->getAttribute('referer'));
                                    echo submit_tag('simpan', 'name=simpan') . ' ' . button_to('kembali', '#', array('onClick' => "javascript:history.back()")); 
                                    ?>
                                </div>
                            </div>
                        </div>
                    <?php echo '</form>'; ?>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Untuk autocomplete jalan -->
<script type="text/javascript">
    $(document).ready(function () {
        $(".js-example-basic-single").select2();
        $(".js-example-basic-multiple").select2();

        var id = $("#subtitle").val();
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/entri/pilihsubx/kegiatan_code/<?php echo $sf_params->get('kegiatan') ?>/unit_id/<?php echo $sf_params->get('unit') ?>/b/" + id + ".html",
            context: document.body
        }).done(function (msg) {
            $('#sub1').html(msg);
        });

        var table = document.getElementById("tblSisaLelang");
        table.style.display = "none";
    });
    $(function () {
        $(".lokasi_jalan_isian").autocomplete({
            source: '/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/entri/autocompleteJalan/b/" + <?php echo $unit_id . '.' . $kegiatan_code; ?> + ".html'
        });
    });
    function showHideKomponen() {
        var id = 'usulan';
        var id_img = 'img_usulan';
        var row = $('#'+id);
        var img = $('#'+id_img);

        if (img) {
            var src = document.getElementById(id_img).getAttribute('src');
            var minus = src.indexOf('/<?php echo sfConfig::get('app_default_coding'); ?>/images/b_minus.png');
            if (minus !== -1) {
                src = '/<?php echo sfConfig::get('app_default_coding'); ?>/images/b_plus.png';
            } else {
                src = '/<?php echo sfConfig::get('app_default_coding'); ?>/images/b_minus.png';
            }
            img.attr('src', src);
        }


        if (minus === -1) {
            // alert("jika minus = -1");
            var usulan_id = 'usulan';
            var mirip = $('.mirip');
            var n = mirip.length;

            if (n > 0) {
                // alert("jika panjang mirip > 0");
                for (var i = 0; i < mirip.length; i++) {
                    var list_mirip = mirip[i];
                    list_mirip.style.display = 'table-row';
                }
            } else {
                // alert("jika panjang mirip <= 0");
                $('#indicator').show();
                $.ajax({
                    url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/entri/getKomponenSerupa/unit/<?php echo $sf_params->get('unit'); ?>/komponen/<?php echo $sf_params->get('komponen'); ?>/rekening/<?php echo $sf_params->get('rekening'); ?>.html",
                    context: document.body
                }).done(function (msg) {
                    row.after(msg);
                    $('#indicator').remove();
                });
            }
        } else {
            // alert("minus != -1");
            $('.mirip').remove();
            minus = -1;
        }
    }
    function showHideKomponenFisik() {
        var row = $('#usulan2');
        var img = $('#img_usulan2');

        if (img) {
            var src = document.getElementById('img_usulan2').getAttribute('src');
            var minus = src.indexOf('/<?php echo sfConfig::get('app_default_coding'); ?>/images/b_minus.png');
            if (minus !== -1) {
                src = '/<?php echo sfConfig::get('app_default_coding'); ?>/images/b_plus.png';
            } else {
                src = '/<?php echo sfConfig::get('app_default_coding'); ?>/images/b_minus.png';
            }
            img.attr('src', src);
        }


        if (minus === -1) {
            var usulan_id = 'usulan2';
            var mirip = $('.mirip2');
            var n = mirip.length;

            if (n > 0) {
                for (var i = 0; i < mirip.length; i++) {
                    var list_mirip = mirip[i];
                    list_mirip.style.display = 'table-row';
                }
            } else {
                $('#indicator').show();
                $.ajax({
                    url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/entri/getKomponenSerupaFisik/komponen/<?php echo $sf_params->get('komponen'); ?>.html",
                    context: document.body
                }).done(function (msg) {
                    $('#usulan2').after(msg);
                    $('#indicator').remove();
                });
            }
        } else {
            $('.mirip2').remove();
            minus = -1;
        }
    }
    $(function () {
        $(document).on('click', 'div.form-group-options .input-group-addon-add', function () {
            var divIluminati = $(this).parents('.div-induk-lokasi');
            var sDivIluminatiHtml = divIluminati.html();
            var sInputGroupClasses = divIluminati.attr('class');
            //Gambiarra pra nao ficar criando mil inputs
            if (divIluminati.next().length >= 1)
                return;
            divIluminati.parent().append('<div class="' + sInputGroupClasses + '">' + sDivIluminatiHtml + '</div>');
        });
        $(document).on('click', 'div.form-group-options .input-group-addon-remove', function () {
            var divIluminati = $(this).parents('.div-induk-lokasi');
            divIluminati.remove();
        });
    });
    $("#subtitle").change(function () {
        var id = $(this).val();
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/entri/pilihsubx/kegiatan_code/<?php echo $sf_params->get('kegiatan') ?>/unit_id/<?php echo $sf_params->get('unit') ?>/b/" + id + ".html",
            context: document.body
        }).done(function (msg) {
            $('#sub1').html(msg);
        });
    });
    $("#kecamatan").change(function () {
        var id = $(this).val();
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/entri/pilihKelurahan/b/" + id + ".html",
            context: document.body}).done(function (msg) {
            $('#kelurahan1').html(msg);
        });
    });
    function hitungTotal() {
        var harga = $('#harga').val();
        var pajakx = $('#pajakx').val();
        var vol1 = $('#vol1').val();
        var vol2 = $('#vol2').val();
        var vol3 = $('#vol3').val();
        var vol4 = $('#vol4').val();
        var volume;
        var hitung;
        if (vol1 !== '' || vol2 !== '' || vol3 !== '' || vol4 !== '') {
            if (vol2 === '') {
                vol2 = 1;
                volume = vol1 * vol2;
            } else if (vol2 !== '') {
                volume = vol1 * vol2;
            }
            if (vol3 === '') {
                vol3 = 1;
                volume = volume * vol3;
            } else if (vol3 !== '') {
                volume = vol1 * vol2 * vol3;
            }
            if (vol4 === '') {
                vol4 = 1;
                volume = volume * vol4;
            } else if (vol4 !== '') {
                volume = vol1 * vol2 * vol3 * vol4;
            }
        }

        if (pajakx == 10) {
            hitung = Math.floor(harga * volume * 1.1);
        } else if (pajakx == 0) {
            hitung = Math.floor(harga * volume * 1);
        }
        $('#total').val(hitung);

        var chkOpr = document.getElementsByName('penyusun_opr[]');
        for(var i = 0; i < chkOpr.length; i++) {
            if(chkOpr[i].checked) {
                var orang = document.getElementById("volOprOrang" + chkOpr[i].value);
                var bulan = document.getElementById("volOprBulan" + chkOpr[i].value);
                var rupiah = document.getElementById("volOprRupiah" + chkOpr[i].value);

                orang.value = vol1;
                bulan.value = vol2;
                rupiah.value = harga;
            }
        }
    }

    function toggleSisaLelang(element) {
        var table = document.getElementById("tblSisaLelang");
        if(element.checked) {
            table.style.display = "table-row";
        } else {
            table.style.display = "none";
        }
    }

    function enableTextBox(element) {
        var nilaiPSP = document.getElementById("nilaiPSP" + element.value);
        var catatanPSP = document.getElementById("catatanPSP" + element.value);
        if(element.checked) {
            nilaiPSP.disabled = false;
            catatanPSP.disabled = false;
        } else {
            nilaiPSP.disabled = true;
            catatanPSP.disabled = true;
        }
    }

    function cekLebihDariLelang(element) {
        var warningPSP = document.getElementById("warningPSP" + element.name);
        var diambil = document.getElementById("nilaiPSP" + element.name).value;
        var asli = document.getElementById("sisaPSP" + element.name).value;
        if(parseFloat(diambil) > parseFloat(asli)) {
            warningPSP.style.display = 'inline';
            $('#anomali').val('1');
        } else {
            warningPSP.style.display = 'none';
            $('#anomali').val('0');
        }
    }

    function enableTextBoxOpr(element) {
        var vol1 = $('#vol1').val();
        var vol2 = $('#vol2').val();
        var harga = $('#harga').val();
        var orang = document.getElementById("volOprOrang" + element.value);
        var bulan = document.getElementById("volOprBulan" + element.value);
        var rupiah = document.getElementById("volOprRupiah" + element.value);
        var keterangan = document.getElementById("keteranganOpr" + element.value);

        var vol1 = $('#vol1').val();
        var vol2 = $('#vol2').val();
        var vol3 = $('#vol3').val();
        var vol4 = $('#vol4').val();
        if (vol1 == '' || vol2 == '') {
            element.checked = false;
        } else {
            if(element.checked) {
                orang.disabled = false;
                bulan.disabled = false;
                rupiah.disabled = false;
                keterangan.disabled = false;

                orang.value = vol1;
                bulan.value = vol2;
                rupiah.value = harga;
            } else {
                orang.disabled = true;
                bulan.disabled = true;
                rupiah.disabled = true;
                keterangan.disabled = true;

                orang.value = 0;
                bulan.value = 0;
                rupiah.value = 0;
                keterangan.value = '';
            }
        }
    }

    $("#lokasi_lama").change(function () {
        var lokasi = $(this).val();
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/report/generateReportKomponenPerLokasi/lokasi/" + lokasi + ".html", 
            context: document.body
        }).done(function (msg) {
            $('#body').html(msg);
        });

    });

    function setAccres(){
        var vol1 = $('#vol1').val();
        var vol2 = $('#vol2').val();
        var vol3 = $('#vol3').val();
        var vol4 = $('#vol4').val();
        
        var satuan1 = $('#volume1').val();
        var satuan2 = $('#volume2').val();
        var satuan3 = $('#volume3').val();
        var satuan4 = $('#volume4').val();

        if($('.checkbox_accres').is(':checked')){
            if(vol1 == '' && satuan1 == ''){
                $('#vol1').val('1.025');
                $('#vol1').prop('readonly',true);
                $('#volume1').val('Accres').trigger('change');
                $('#volume1').select2('readonly',true);
            }else if(vol2 == '' && satuan2 == ''){
                $('#vol2').val('1.025');
                $('#vol2').prop('readonly',true);
                $('#volume2').val('Accres').trigger('change');
                $('#volume2').select2('readonly',true);
            }else if(vol3 == '' && satuan3 == ''){
                $('#vol3').val('1.025');
                $('#vol3').prop('readonly',true);
                $('#volume3').val('Accres').trigger('change');
                $('#volume3').select2('readonly',true);
            }else if(vol4 == '' && satuan4 == ''){
                $('#vol4').val('1.025');
                $('#vol4').prop('readonly',true);
                $('#volume4').val('Accres').trigger('change');
                $('#volume4').select2('readonly',true);
            }
        }else{
            if(vol1 == '1.025' && satuan1 == 'Accres'){
                $('#vol1').val('');
                $('#vol1').prop('readonly',false);
                $('#volume1').val('').trigger('change');
                $('#volume1').select2('readonly',false);
            }else if(vol2 == '1.025' && satuan2 == 'Accres'){
                $('#vol2').val('');
                $('#vol2').prop('readonly',false);
                $('#volume2').val('').trigger('change');
                $('#volume2').select2('readonly',false);
            }else if(vol3 == '1.025' && satuan3 == 'Accres'){
                $('#vol3').val('');
                $('#vol3').prop('readonly',false);
                $('#volume3').val('').trigger('change');
                $('#volume3').select2('readonly',false);
            }else if(vol4 == '1.025' && satuan4 == 'Accres'){
                $('#vol4').val('');
                $('#vol4').prop('readonly',false);
                $('#volume4').val('').trigger('change');
                $('#volume4').select2('readonly',false);
            }
        }
        hitungTotal();
    }

    function cekMaksSuamiIstri(obj){
        var suamiistri = $(obj).val();
        var cek_nama_komponen = $('#cek_nama_komponen').val();

        //cek volume orang
        var volume1 = $('#volume1').val();
        var volume2 = $('#volume2').val();
        var volume3 = $('#volume3').val();
        var volume4 = $('#volume4').val();
        var orang = 0;
        if(volume1 == 'Orang'){
            orang = $('#vol1').val();
        }else if(volume2 == 'Orang'){
            orang = $('#vol2').val();
        }else if(volume3 == 'Orang'){
            orang = $('#vol3').val();
        }else if(volume4 == 'Orang'){
            orang = $('#vol4').val();
        }

        if(orang=='' || parseInt(orang)==0){
            toastr.error('Volume orang belum diinputkan','Alert!');
            $(obj).val('0');
        }

        if(parseInt(suamiistri) > parseInt(orang)){
            toastr.error('Jumlah Suami/Istri Melebihi volume orang (1 Pegawai maks 1 Suami/Istri)','Alert!');
            $(obj).val('0');
        }
    }
    
    function cekMaksAnak(obj){
        var anak = $(obj).val();
        var cek_nama_komponen = $('#cek_nama_komponen').val();

        //cek volume orang
        var volume1 = $('#volume1').val();
        var volume2 = $('#volume2').val();
        var volume3 = $('#volume3').val();
        var volume4 = $('#volume4').val();
        var orang = 0;
        if(volume1 == 'Orang'){
            orang = $('#vol1').val();
        }else if(volume2 == 'Orang'){
            orang = $('#vol2').val();
        }else if(volume3 == 'Orang'){
            orang = $('#vol3').val();
        }else if(volume4 == 'Orang'){
            orang = $('#vol4').val();
        }

        if(orang=='' || parseInt(orang)==0){
            toastr.error('Volume orang belum diinputkan','Alert!');
            $(obj).val('0');
        }

        if(parseInt(anak) > parseInt(orang)*2){
            toastr.error('Jumlah Anak Melebihi volume orang (1 Pegawai maks 2 Anak)','Alert!');
            $(obj).val('0');
        }
    }

</script>