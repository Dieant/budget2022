<?php use_helper('Url', 'Javascript', 'Form', 'Object'); ?>
<?php
    $kode_kegiatan = $sf_params->get('kegiatan');
    $unit_id = $sf_params->get('unit');
    $tabel_dpn = 'dinas_';
    $status = 'LOCK';
    $c = new Criteria();
    $c->add(DinasRincianPeer::KEGIATAN_CODE, $kode_kegiatan);
    $c->add(DinasRincianPeer::UNIT_ID, $unit_id);
    $rs_rinciankegiatan = DinasRincianPeer::doSelectOne($c);
    $v = DinasRincianPeer::doSelectOne($c);
    if ($v) {
        if ($v->getLock() == TRUE) {
            $status = 'LOCK';
        } else if ($v->getRincianLevel() == 1) {
            $status = 'LOCK';
            $status_jk = 'OPEN';
        } else if ($v->getRincianLevel() == 2) {
            $status = 'OPEN';
        } else if ($v->getRincianLevel() == 3) {
            $status = 'LOCK';
        }
    }

    $i = 0;
    $kode_sub = '';
    $temp_rekening = '';
    $c = new Criteria();
    $c->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
    $c->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
    $c->add(DinasRincianDetailPeer::STATUS_HAPUS, FALSE);
    $c->add(DinasRincianDetailPeer::STATUS_LEVEL, 4, Criteria::GREATER_THAN);
    if (DinasRincianDetailPeer::doSelectOne($c))
        $status_kunci = FALSE;
    else
        $status_kunci = TRUE;
    $lok = 'LOCK';
    $co = new Criteria();
    $co->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
    $co->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
    $co->add(DinasRincianDetailPeer::STATUS_HAPUS, FALSE);
    $co->add(DinasRincianDetailPeer::LOCK_SUBTITLE, $lok);
    if (DinasRincianDetailPeer::doSelectOne($co))
        $status_kunci_komponen = TRUE;
    else
        $status_kunci_komponen = FALSE;
    $status_kunci_komponen = TRUE;
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Search Komponen - (<?php echo $cari ?>)</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Lembar Kerja</a></li>
                    <li class="breadcrumb-item active">Seacrh Komponen</li>
                </ol>
            </div>
        </div>
    </div>
</section>
<!-- Main content -->
<section class="content">
    <?php include_partial('entri/list_messages'); ?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body table-responsive p-0">
                        <table class="table table-hover">
                            <thead class="head_peach">
                                <tr>
                                    <th>Action</th>
                                    <th>Komponen</th>
                                    <th>Satuan</th>
                                    <th>Koefisien</th>
                                    <th>Harga</th>
                                    <th>Hasil</th>
                                    <th>PPN</th>
                                    <th>Total</th>
                                    <th>Nilai Rasionalisasi</th>
                                    <th>Belanja</th>
                                    <?php if(sfConfig::get('app_tahap_edit') != 'murni'): ?>
                                    <th>Catatan</th>
                                    <?php endif; ?>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($rs_rd as $rd):
                                ?>
                                <tr>
                                    <td>
                                        <?php
                                        $kegiatan = $rd['kegiatan_code'];
                                        $unit = $rd['unit_id'];
                                        $no = $rd['detail_no'];
                                        $sub = $rd['subtitle'];

                                        $benar_musrenbang = 0;
                                        if ($rd['is_musrenbang'] == 'TRUE') {
                                            $benar_musrenbang = 1;
                                        }
                                        $benar_prioritas = 0;
                                        if($rd['prioritas_wali'] == 'TRUE') {
                                            $benar_prioritas = 1;
                                        }
                                        $benar_output = 0;
                                        if($rd['is_output'] == 'TRUE') {
                                            $benar_output = 1;
                                        }
                                        $benar_multiyears = 0;
                                        if ($rd['th_ke_multiyears'] <> null && $rd['th_ke_multiyears'] > 0) {
                                            $benar_multiyears = 1;
                                        }
                                        $benar_hibah = 0;
                                        if ($rd['is_hibah'] == 'TRUE') {
                                            $benar_hibah = 1;
                                        }
                                        $status_kunci_baru = FALSE;

                                        $c_rekening = new Criteria();
                                        $c_rekening->add(BukaRekeningBangunanPeer::UNIT_ID, $unit_id);
                                        $rekening_bangunan = BukaRekeningBangunanPeer::doCount($c_rekening);
                                        $array_rekening_dikunci = array('5.1.02.01.01.0001');

                                        if (in_array($rd['rekening_code'], $array_rekening_dikunci) && $rekening_bangunan <= 0) {
                                            $status_kunci_baru = TRUE;
                                        }

                                        $array_rekening_jk = array('5.2.2.24.01', '5.2.1.02.02');

                                        $arr_buka_edit_output = array('');
                                        $query =
                                        "SELECT detail_kegiatan
                                        FROM ebudget.buka_komponen_murni";
                                        $con = Propel::getConnection();
                                        $stmt = $con->prepareStatement($query);
                                        $rs = $stmt->executeQuery();
                                        while ($rs->next()) array_push($arr_buka_edit_output, $rs->getString('detail_kegiatan'));

                                        $arr_kunci_komponen = array('2.1.1.01.01.01.004.015','2.1.1.01.01.01.001.046','2.1.1.01.01.01.001.047','2.1.1.01.01.01.001.051','2.1.1.01.01.01.002.020','2.1.1.01.01.01.002.020','2.1.1.01.01.01.002.023','2.1.1.01.01.01.002.030','2.1.1.01.01.01.002.031','2.1.1.01.01.01.002.032','2.1.1.01.01.01.001.046.A','2.1.1.01.01.01.001.055','2.1.1.01.01.01.001.053.A','2.1.1.01.01.01.001.055','2.1.1.01.01.01.001.054'); 
                                        $koRek = substr($rd['komponen_id'] , 0, 18);
                                        $arr_tenaga_kontrak = array('2.1.1.01.01.01.008','2.1.1.03.05.01.001','2.1.1.01.01.01.004','2.1.1.01.01.02.099','2.1.1.01.01.01.005','2.1.1.01.01.01.006','2.1.1.01.01.02.001');
                                        $arr_all_kode_sub=array('');

                                        if ((!in_array($rd['detail_kegiatan'], $arr_buka_edit_output)  || (!$rd['is_output'] && !$rd['prioritas_wali'])) && $rd['status_level'] <= 1 && !$status_kunci_baru && $status_kunci && $status == 'OPEN' && $status_kunci_komponen && $rd['lock_subtitle'] != 'LOCK') {
                                            ?>
                                            <div class="btn-group">
                                                <?php
                                                if(( in_array($koRek,$arr_tenaga_kontrak) && ($rd['satuan'] == 'Orang Bulan') && (strpos($rd['komponen_name'], 'Biaya') ===  FALSE) && (!in_array($rd['detail_kegiatan'], $arr_buka_edit_output)))|| (in_array($rd['komponen_id'], $arr_kunci_komponen) && (!in_array($rd['detail_kegiatan'], $arr_buka_edit_output))) || in_array($koRek,$arr_all_kode_sub) || ($rd['sumber_dana_id'] <> 11 && (!in_array($rd['detail_kegiatan'], $arr_buka_edit_output))) || ($rd['is_covid'] == 't' && (!in_array($rd['detail_kegiatan'], $arr_buka_edit_output))))                                                
                                                {

                                                }
                                                else
                                                {
                                                    echo link_to('<i class="fa fa-edit"></i> Edit', 'entri/editKegiatan?id=' . $rd['detail_no'] . '&unit=' . $rd['unit_id'] . '&kegiatan=' . $rd['kegiatan_code'] . '&komponen='.$rd['komponen_id'].'&edit=' . md5('ubah'), array('class' => 'btn btn-default btn-flat btn-sm'));
                                                    if (sfConfig::get('app_tahap_edit') == 'murni' && $benar_hibah == 0 && $benar_musrenbang == 0 && $benar_multiyears == 0 && substr($sf_user->getNamaLogin(), 0, 3) <> 'pa_') {
                                                        ?>
                                                        <button type="button" class="btn btn-default dropdown-toggle btn-flat btn-sm" data-toggle="dropdown">
                                                            <span class="caret"></span>
                                                            <span class="sr-only">Toggle Dropdown</span>
                                                        </button>
                                                        <div class="dropdown-menu" role="menu">
                                                            <?php
                                                            echo link_to('<i class="fa fa-trash"></i> Hapus', 'entri/hapusPekerjaans?id=' . $id . '&no=' . $rd['detail_no'] . '&unit=' . $rd['unit_id'] . '&kegiatan=' . $rd['kegiatan_code'], Array('confirm' => 'Yakin untuk menghapus Komponen ' . $rd['komponen_name'] . ' ' . $rd['detail_name'] . ' ?', 'class' => 'dropdown-item'));
                                                            ?>
                                                        </div>
                                                        <?php
                                                    }

                                                }
                                                if( $rd['rekening_code'] == '')
                                                {
                                                    $query="select distinct rekening_code,rekening_name from " . sfConfig::get('app_default_schema') . ".rekening
                                                    where rekening_code in (select rekening_code from " . sfConfig::get('app_default_schema') . ".komponen_rekening
                                                    where komponen_id='".$rd['komponen_id']."')";
                                                    $con = Propel::getConnection();
                                                    $stmt = $con->prepareStatement($query);
                                                    $ts = $stmt->executeQuery();
                                                    $pilih = array();
                                                    $ada = false;
                                                    $select_str = "<select name='rek_$no'>";
                                                    while ($ts->next()) {
                                                        $ada = true;
                                                        $r = $ts->getString('rekening_code');
                                                        $rs = $ts->getString('rekening_code')." - ".$ts->getString('rekening_name');
                                                        $select_str.="<option value='$r'>$rs</option>";
                                                    }
                                                    $select_str.="</select>";
                                                    if ($ada) {
                                                        ?>
                                                        <div id="rekening_list_<?php echo $no ?>">
                                                            <form method="post" id="form_rekening_list_<?php echo $no ?>">
                                                            <input type="hidden" name="id" value="<?php echo $id; ?>" />
                                                            <input type="hidden" name="unit_id" value="<?php echo $unit_id; ?>" />
                                                            <input type="hidden" name="kegiatan_code" value="<?php echo $kegiatan_code; ?>" />
                                                            <input type="hidden" name="detail_no" value="<?php echo $no; ?>" />
                                                            <?php echo $select_str; ?>
                                                            <input class="saveGantiRekening" type="submit" value="Simpan"/>
                                                            </form><br/>
                                                        </div>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </div>
                                            <div class="clearfix"></div>
                                            <?php
                                            //irul 25agustus 2014 - fungsi GMAP
                                            if ($rd['tipe2'] == 'PERENCANAAN' || $rd['tipe2'] == 'PENGAWASAN' || $rd['tipe2'] == 'KONSTRUKSI' || $rd['tipe2'] == 'TANAH' || $rd['tipe'] == 'FISIK' || $est_fisik) 
                                            {
                                                $con = Propel::getConnection();
                                                $kode_rka = $unit_id . '.' . $kegiatan_code . '.' . $rd['detail_no'];
                                                $query_cek_waiting = "select id_waiting from ebudget.waitinglist_pu where kode_rka = '" . $kode_rka . "' ";
                                                $stmt_cek_waiting = $con->prepareStatement($query_cek_waiting);
                                                $rs_cek_waiting = $stmt_cek_waiting->executeQuery();
                                                if ($rs_cek_waiting->next()) {
                                                    ?>
                                                    <div id="tempat_ajax_<?php echo str_replace('.', '_', $kegiatan_code) . '_' . $rd['detail_no'] ?>">
                                                        <?php echo link_to_function('<i class="fa fa-trash"></i> Kembalikan ke Waiting List', 'execKembalikanWaiting("' . $rd['detail_no'] . '","' . $rd['unit_id'] . '","' . $rd['kegiatan_code'] . '", "' . str_replace('.', '_', $kegiatan_code) . '_' . $rd['detail_no'] . '")', array('class' => 'btn btn-default btn-flat btn-sm')); ?>
                                                    </div>
                                                    <?php
                                                }

                                                $id_kelompok = 0;
                                                $tot = 0;
                                                $query = "select count(*) as tot "
                                                . "from " . sfConfig::get('app_default_gis') . ".geojsonlokasi_rev1 "
                                                . "where unit_id='" . $rd['unit_id'] . "' and kegiatan_code='" . $rd['kegiatan_code'] . "' and detail_no ='" . $rd['detail_no'] . "' "
                                                . "and tahun = '" . sfConfig::get('app_tahun_default') . "' and status_hapus = FALSE";
                                                $stmt = $con->prepareStatement($query);
                                                $rs = $stmt->executeQuery();
                                                while ($rs->next()) {
                                                    $tot = $rs->getString('tot');
                                                }
                                                if ($tot == 0) {
                                                    $con = Propel::getConnection();
                                                    $c2 = new Criteria();
                                                    $crit1 = $c2->getNewCriterion(KomponenPeer::KOMPONEN_TIPE, 'FISIK');
                                                    $crit1->addOr($c2->getNewCriterion(KomponenPeer::KOMPONEN_TIPE, 'EST'));
                                                    $crit1->addOr($c2->getNewCriterion(KomponenPeer::KOMPONEN_TIPE2, 'KONSTRUKSI'));
                                                    $crit1->addOr($c2->getNewCriterion(KomponenPeer::KOMPONEN_TIPE2, 'TANAH'));
                                                    $crit1->addOr($c2->getNewCriterion(KomponenPeer::KOMPONEN_TIPE2, 'PENGAWASAN'));
                                                    $crit1->addOr($c2->getNewCriterion(KomponenPeer::KOMPONEN_TIPE2, 'PERENCANAAN'));
                                                    $c2->add($c2->getNewCriterion(KomponenPeer::KOMPONEN_NAME, $rd['komponen_name'], Criteria::ILIKE));
                                                    $c2->addAnd($crit1);
                                                    $rd2 = KomponenPeer::doSelectOne($c2);
                                                    if ($rd2) {
                                                        $komponen_id = $rd2->getKomponenId();
                                                        $satuan = $rd2->getSatuan();
                                                    } else {
                                                        $komponen_id = '0';
                                                        $satuan = '';
                                                    }

                                                    if ($komponen_id == '0') {
                                                        $query2 = "select * from master_kelompok_gmap "
                                                        . "where '" . $rd['komponen_name'] . "' ilike nama_objek||'%'";
                                                    } else {
                                                        $query2 = "select * from master_kelompok_gmap "
                                                        . "where '" . $komponen_id . "' ilike kode_kelompok||'%'";
                                                    }

                                                    $stmt2 = $con->prepareStatement($query2);
                                                    $rs2 = $stmt2->executeQuery();
                                                    while ($rs2->next()) {
                                                        $id_kelompok = $rs2->getString('id_kelompok');
                                                    }

                                                    if ($id_kelompok == '' || $id_kelompok == 0 || $id_kelompok == null) {
                                                        $id_kelompok = 19;

                                                        if (in_array($satuan, array('Kegiatan', 'Lokasi', 'M2', 'M²', 'm3', 'Paket', 'Set'))) {
                                                            $id_kelompok = 100;
                                                        } elseif (in_array($satuan, array('m', 'M', 'M1', 'Meter', 'Titik', 'Unit'))) {
                                                            $id_kelompok = 101;
                                                        }
                                                    }
                                                } else {
                                                    $con = Propel::getConnection();
                                                    $query = "select * from " . sfConfig::get('app_default_gis') . ".geojsonlokasi_rev1 
                                                    where unit_id='" . $rd['unit_id'] . "' and kegiatan_code='" . $rd['kegiatan_code'] . "' and detail_no ='" . $rd['detail_no'] . "' and tahun = '" . sfConfig::get('app_tahun_default') . "'";
                                                    $stmt = $con->prepareStatement($query);
                                                    $rs = $stmt->executeQuery();
                                                    while ($rs->next()) {
                                                        $mlokasi = $rs->getString('mlokasi');
                                                        $id_kelompok = $rs->getString('id_kelompok');
                                                    }

                                                    $query = "select max(lokasi_ke) as total_lokasi from " . sfConfig::get('app_default_gis') . ".geojsonlokasi_rev1 
                                                    where unit_id='" . $rd['unit_id'] . "' and kegiatan_code='" . $rd['kegiatan_code'] . "' and detail_no ='" . $rd['detail_no'] . "' and tahun = '" . sfConfig::get('app_tahun_default') . "'";
                                                    $stmt = $con->prepareStatement($query);
                                                    $rs = $stmt->executeQuery();
                                                    while ($rs->next()) {
                                                        $total_lokasi = $rs->getString('total_lokasi');
                                                    }
                                                }
                                                ?>
                                                <div class="btn-group">
                                                    <?php
                                                    echo link_to_function('<i class="fa fa-map-marker"></i>', '', array('class' => 'btn btn-default btn-flat btn-sm', 'disable' => true));
                                                    if ($tot == 0) {
                                                        $array_bersih_kurung = array('(', ')');
                                                        $lokasi_bersih_kurung = str_replace($array_bersih_kurung, '', $lokasi);
                                                        echo link_to('<i class="fa fa-edit"></i> Input Lokasi', sfConfig::get('app_path_gmap') . 'insertBaru_revisi.php?unit_id=' . $rd['unit_id'] . '&kode_kegiatan=' . $rd['kegiatan_code'] . '&detail_no=' . $rd['detail_no'] . '&satuan=' . $rd['satuan'] . '&volume=' . $rd['volume'] . '&nilai_anggaran=' . $rd['nilai_anggaran'] . '&tahun=' . sfConfig::get('app_tahun_default') . '&mlokasi=&id_kelompok=' . $id_kelompok . '&th_load=0&level=1&nm_user=' . $sf_user->getNamaLogin() . '&lokasi_ke=1', array('class' => 'btn btn-default btn-flat btn-sm'));
                                                    } else {
                                                        echo link_to('<i class="fa fa-edit"></i> Edit Lokasi', sfConfig::get('app_path_gmap') . 'updateData_revisi.php?unit_id=' . $rd['unit_id'] . '&kode_kegiatan=' . $rd['kegiatan_code'] . '&detail_no=' . $rd['detail_no'] . '&satuan=' . $rd['satuan'] . '&volume=' . $rd['volume'] . '&nilai_anggaran=' . $rd['nilai_anggaran'] . '&tahun=' . sfConfig::get('app_tahun_default') . '&mlokasi=' . $mlokasi . '&id_kelompok=' . $id_kelompok . '&th_load=0&level=1&nm_user=' . $sf_user->getNamaLogin() . '&total_lokasi=' . $total_lokasi . '&lokasi_ke=1', array('class' => 'btn btn-default btn-flat btn-sm'));
                                                        ?>
                                                        <button type="button" class="btn btn-default dropdown-toggle btn-flat btn-sm" data-toggle="dropdown">
                                                            <span class="caret"></span>
                                                            <span class="sr-only">Toggle Dropdown</span>
                                                        </button>
                                                        <div class="dropdown-menu" role="menu">
                                                            <?php
                                                            echo link_to('<i class="fa fa-trash"></i> Hapus Lokasi', 'entri/hapusLokasi?id=' . $id . '&no=' . $rd['detail_no'] . '&unit=' . $rd['unit_id'] . '&kegiatan=' . $rd['kegiatan_code'], Array('confirm' => 'Yakin untuk menghapus Lokasi untuk Komponen ' . $rd['komponen_name'] . ' ' . $rd['detail_name'] . ' ?', 'class' => 'dropdown-item'));
                                                            ?>
                                                        </div>
                                                    <?php
                                                    }
                                                    ?>
                                            </div>
                                                <?php
                                            }
                                            } 
                                            elseif ($status_jk == 'OPEN' && in_array($rd['rekening_code'], $array_rekening_jk)) 
                                            {
                                            ?>
                                            <div class="btn-group">
                                                <?php
                                                echo link_to('<i class="fa fa-edit"></i> Edit', 'entri/editKegiatan?id=' . $rd['detail_no'] . '&unit=' . $rd['unit_id'] . '&kegiatan=' . $rd['kegiatan_code'] . '&edit=' . md5('ubah'), array('class' => 'btn btn-default btn-flat btn-sm'));
                                                ?>
                                            </div>
                                            <div class="clearfix"></div>
                                            <?php
                                            }
                                            ?>
                                    </td>
                                    <?php
                                        $cek = FALSE;
                                        if ($rd['note_peneliti'] != '' and $rd['note_peneliti'] != NULL and$rd['status_hapus'] == false) {
                                        $warna = 'lightyellow';
                                        } elseif ($cek or ( $rd['note_skpd'] != '' and $rd['note_skpd'] != NULL ) and$rd['status_hapus'] == false) {
                                        $warna = '#e8f3f1';
                                        } else {
                                        $warna = 'none';
                                        }
                                    ?>
                                    <td style="background: <?php echo $warna ?>">
                                        <?php
                                        if ($benar_musrenbang == 1) {
                                            echo '&nbsp;<span class="badge badge-success">Musrenbang</span>';
                                        }
                                        if ($benar_output == 1) {
                                            echo '&nbsp;<span class="badge badge-info">Output</span>';
                                        }
                                        if ($benar_prioritas == 1) {
                                            echo '&nbsp;<span class="badge badge-warning">Prioritas</span>';
                                        }
                                        if ($benar_hibah == 1) {
                                            echo '&nbsp;<span class="badge badge-success">Hibah</span>';
                                        }
                                        if ($rd['kecamatan'] <> '') {
                                            echo '&nbsp;<span class="badge badge-warning">Jasmas</span>';
                                        }
                                        if ($benar_multiyears == 1) {
                                            echo '&nbsp;<span class="badge badge-primary">Multiyears Tahun ke ' . $rd['th_ke_multiyears'] . '</span>';
                                        }
                                        echo '<br/>';
                                        echo $rd['komponen_name'];
                                        if (sfConfig::get('app_fasilitas_keteranganKomponen') == 'buka') {
                                            echo ' ' . $rd['detail_name'];
                                            echo '&nbsp;<span class="badge badge-danger">' . $rd['tahap']. '</span><br/>';
                                            if ($rd['sumber_dana_id'] == 1) {
                                                echo '&nbsp;<span class="badge badge-info">DAK</span>';
                                            }
                                            if ($rd['sumber_dana_id'] == 2) {
                                                echo '&nbsp;<span class="badge badge-info">DBHCT</span>';
                                            }
                                            if ($rd['sumber_dana_id'] == 3) {
                                                echo '&nbsp;<span class="badge badge-info">DID</span>';
                                            }
                                            if ($rd['sumber_dana_id'] == 4) {
                                                echo '&nbsp;<span class="badge badge-info">DBH</span>';
                                            }
                                            if ($rd['sumber_dana_id'] == 5) {
                                                echo '&nbsp;<span class="badge badge-info">Pajak Rokok</span>';
                                            }
                                            if ($rd['sumber_dana_id'] == 6) {
                                                echo '&nbsp;<span class="badge badge-info">BLUD</span>';
                                            }
                                            if ($rd['sumber_dana_id'] == 7) {
                                                echo '&nbsp;<span class="badge badge-info">Bantuan Keuangan Provinsi</span>';
                                            }
                                            if ($rd['sumber_dana_id'] == 8) {
                                                echo '&nbsp;<span class="badge badge-info">Kapitasi JKN</span>';
                                            }
                                            if ($rd['sumber_dana_id'] == 9) {
                                                echo '&nbsp;<span class="badge badge-info">BOS</span>';
                                            }
                                            if ($rd['sumber_dana_id'] == 10) {
                                                echo '&nbsp;<span class="badge badge-info">DAU</span>';
                                            }
                                            if ($rd['sumber_dana_id'] == 12) {
                                                echo '&nbsp;<span class="badge badge-info">Hibah Pariwisata</span>';
                                            }
                                            if ($rd['is_covid'] == 't') {
                                                echo '&nbsp;<span class="badge badge-info">Covid-19</span>';
                                            }
                                            if ($rd['tipe2'] == 'KONSTRUKSI' || $rd['tipe2'] == 'TANAH' || $rd['tipe'] == 'FISIK' || $est_fisik) {
                                                if ($rd['lokasi_kecamatan'] <> '' && $rd['lokasi_kelurahan'] <> '') {
                                                    echo '[' . $rd['lokasi_kelurahan'] . ' - ' . $rd['lokasi_kecamatan'] . ']';
                                                }
                                            }
                                        }
                                        $query2 = "select tahap from " . sfConfig::get('app_default_schema') . ".komponen "
                                        . "where komponen_id='" . $rd['komponen_id'] . "'";
                                        $con = Propel::getConnection();
                                        $stmt = $con->prepareStatement($query2);
                                        $t = $stmt->executeQuery();
                                        while ($t->next()) {
                                            if ($t->getString('tahap') == DinasMasterKegiatanPeer::getTahapKegiatan($rd['unit_id'], $rd['kegiatan_code']) && sfConfig::get('app_tahap_edit') != 'murni') {
                                                echo image_tag('/images/newanima.gif');
                                            }
                                        }

                                        if (in_array($rd['detail_no'], $komponen_serupa)) {
                                            echo image_tag('/images/warning.gif', array('title' => 'Ada komponen serupa', 'alt' => 'Ada komponen serupa'));
                                        }
                                        ?>
                                    </td>
                                    <td style="background: <?php echo $warna ?>" align="center"><?php echo $rd['satuan']; ?></td>
                                    <td style="background: <?php echo $warna ?>" align="center"><?php echo $rd['keterangan_koefisien']; ?></td>
                                    <td style="background: <?php echo $warna ?>" align="right">
                                        <?php
                                        if ($rd['satuan'] == '%') {
                                            echo $rd['komponen_harga_awal'];
                                        } elseif ($rd['komponen_harga_awal'] != floor($rd['komponen_harga_awal'])) {
                                            echo number_format($rd['komponen_harga_awal'], 2, ',', '.');
                                        } else {
                                            echo number_format($rd['komponen_harga_awal'], 0, ',', '.');
                                        }
                                        if ($rd['status_level'] <= 1 && !$status_kunci_baru && $status_kunci && $status == 'OPEN' && $status_kunci_komponen && !($rd['lock_subtitle'] == 'LOCK' && ($rd['komponen_id'] == '23.05.01.110' || $rd['komponen_id'] == '23.05.01.40' || $rd['komponen_id'] == '23.01.01.04.16'))) {
                                            if ($rd['status_lelang'] == 'lock') {
                                                echo image_tag('/images/bahaya2.gif');
                                                echo '<br/><span class="badge badge-danger">[Telah dilakukan Penggunaan Sisa Lelang, tidak dapat mengedit volume]</span>';
                                            } else if ($rd['status_lelang'] == 'unlock') {
                                                $lelang = 0;
                                                $ceklelangselesaitidakaturanpembayaran = 0;
                                                if (sfConfig::get('app_fasilitas_cekeProject') == 'buka') {
                                                    if (sfConfig::get('app_fasilitas_cekServer') == 'buka') {
                                                        $lelang = $rd->getCekLelang($rd['unit_id'], $rd['kegiatan_code'], $rd['detail_no'], $rd['nilai_anggaran']);
                                                        if (sfConfig::get('app_fasilitas_cekeDelivery') == 'buka') {
                                                            $ceklelangselesaitidakaturanpembayaran = $rd->getCekLelangTidakAdaAturanPembayaran($rd['unit_id'], $rd['kegiatan_code'], $rd['detail_no']);
                                                        }
                                                    }
                                                }

                                                if ($lelang == 0 && $ceklelangselesaitidakaturanpembayaran == 0) {
                                                    echo form_tag("entri/ubahhargadasar?id=$id&unit_id=$unit_id&kegiatan_code=$kegiatan_code&detail_no=$no");
                                                    echo input_tag("komponen_harga_awal", '', array('style' => 'width:50px', 'value' => $rd['komponen_harga_awal']));
                                                    echo "<br>";
                                                    echo submit_tag(' OK ');
                                                    echo "</form>";
                                                } else {
                                                    if ($lelang > 0) {
                                                        echo '<br/><span class="badge badge-danger">[Lelang Sedang Berjalan]</span>';
                                                    }
                                                    if ($ceklelangselesaitidakaturanpembayaran == 1) {
                                                        echo '<br/><span class="badge badge-danger">[Belum ada Aturan Pembayaran]</span>';
                                                    }
                                                }
                                                echo '<br/><span class="badge badge-danger">[Telah dilakukan Penggunaan Sisa Lelang, tidak dapat mengedit volume]</span>';
                                            } else if($rd['is_hpsp'] == 't') {
                                                echo '<br/><span class="badge badge-primary">[Merupakan komponen Hasil PSP]</span>';
                                            }
                                        }
                                        ?>
                                    </td>
                                    <td style="background: <?php echo $warna ?>" align="right">
                                        <?php
                                        $volume = $rd['volume'];
                                        $harga = $rd['komponen_harga_awal'];
                                        $hasil = floor($volume * $harga);

                                        if ($hasil < 1) {
                                            echo number_format($hasil, 4, ',', '.');
                                        }
                                        else {
                                            echo number_format($hasil, 0, ',', '.');    
                                        }
                                        ?>
                                    </td>
                                    <td style="background: <?php echo $warna ?>" align="right">
                                        <?php echo $rd['pajak'] . '%'; ?>
                                    </td>
                                    <td style="background: <?php echo $warna ?>" align="right">
                                        <?php
                                        $volume = $rd['volume'];
                                        $harga = $rd['komponen_harga_awal'];
                                        $pajak = $rd['pajak'];
                                        $total = $rd['nilai_anggaran'];
                                        if ($total < 1) {
                                            echo number_format($total, 4, ',', '.');
                                        }
                                        else {
                                            echo number_format($total, 0, ',', '.');    
                                        }
                                        ?>
                                    </td>
                                    <td style="background: <?php echo $warna ?>" align="right">
                                        <?php echo number_format($rd['anggaran_rasionalisasi'], 0, ',', '.'); ?>
                                    </td>
                                    <td style="background: <?php echo $warna ?>" align="center">
                                        <?php
                                        $rekening = $rd['rekening_code'];
                                        $rekening_code = substr($rekening, 0, 6);
                                        $c = new Criteria();
                                        $c->add(KelompokBelanjaPeer::BELANJA_CODE, $rekening_code);
                                        $rs_rekening = KelompokBelanjaPeer::doSelectOne($c);
                                        if ($rs_rekening) {
                                            echo $rs_rekening->getBelanjaName();
                                        }
                                        ?>
                                    </td>
                                    <?php if(sfConfig::get('app_tahap_edit') != 'murni'): ?>
                                        <td style="background: <?php echo $warna ?>">
                                            <?php
                                            if ($rd['status_lelang'] == 'unlock' && $rd['note_skpd'] == '') {
                                                $detno = $rd['detail_no'];
                                                echo 'Catatan SKPD:<br/>';
                                                echo form_tag("entri/simpanCatatan?unit_id=$unit_id&kegiatan_code=$kegiatan_code&detail_no=$detno");
                                                echo input_tag("catatan_skpd", '', array('style' => 'width:100px'));
                                                echo "<br>";
                                                echo submit_tag(' OK ');
                                                echo "</form>";
                                                echo "<br/><font style='font-weight: bold; color: red'>-Isi kolom ini untuk membuka Field Penggunaan Sisa Lelang.-</font>";
                                            }
                                            ?>
                                        </td>
                                    <?php endif; ?>
                                </tr>
                                <?php endforeach;?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section><!-- /.content -->