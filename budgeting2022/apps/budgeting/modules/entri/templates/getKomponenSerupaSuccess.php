<?php use_helper('Url', 'Javascript', 'Form', 'Object'); ?>
<?php
$i = 0;
?>
<tr class="mirip" style="text-align: center; font-weight: bold">
    <th>Kegiatan</th>
    <th>Subtitle</th>
    <th>Sub Subtitle</th>
    <th>Koefisien</th>
</tr>
<?php
foreach ($list as $value) {
    ?>
    <tr class="mirip" style="text-align: center">
        <td><?php echo $value['kode_kegiatan'] ?></td>
        <td><?php echo $value['subtitle'] ?></td>
        <td><?php echo $value['sub'] ?></td>
        <td><?php echo $value['keterangan_koefisien'] ?></td>
    </tr>
    <?php
}
?>
<tr class="mirip">
    <td colspan="13">&nbsp;</td>
</tr>