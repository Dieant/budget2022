<?php use_helper('I18N', 'Date', 'Object', 'Javascript', 'Validation') ?>
<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Ubah Profil Ka SKPD</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Master</a></li>
          <li class="breadcrumb-item active">Ubah Profil Ka SKPD</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>
<!-- Main content -->
<section class="content">
    <?php include_partial('entri/list_messages'); ?>
    <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">
                        <i class="fab fa-foursquare"></i> Form
                    </h3>
                </div>
                <div class="card-body">
                    <?php echo form_tag('entri/simpanset', array('class' => 'form-horizontal')); ?>
                    <div class="row">
                      <div class="col-md-3">
                        <div class="form-group">
                            <label>Nama</label>
                            <?php echo input_tag('kepala_nama', $kepala_nama, array('class' => 'form-control', 'placeholder' => $kepala_nama, 'required' => 'required')) ?>
                        </div>
                      </div>
                      <!-- /.col -->
                      <div class="col-md-3">
                        <div class="form-group">
                            <label>NIP</label>
                            <?php echo input_tag('kepala_nip', $kepala_nip, array('class' => 'form-control', 'placeholder' => $kepala_nip, 'required' => 'required')) ?>
                        </div>
                      </div>
                      <!-- /.col -->
                      <div class="col-md-3">
                        <div class="form-group">
                            <label>Pangkat</label>
                            <?php echo input_tag('kepala_pangkat', $kepala_pangkat, array('class' => 'form-control', 'placeholder' => $kepala_pangkat, 'required' => 'required')); ?>
                        </div>
                      </div>
                      <!-- /.col -->
                      <div class="col-md-2">
                        <div class="form-group">
                            <label class="tombol_filter">Tombol Filter</label><br/>
                            <button type="submit" name="commit" class="btn btn-outline-primary btn-sm">Simpan <i class="far fa-save"></i></button>
                            <button type="reset" name="reset" class="btn btn-outline-danger btn-sm">Reset <i class="fa fa-backspace"></i></button>
                            <?php
                                echo input_hidden_tag('referer', $sf_request->getAttribute('referer'));
                            ?>
                        </div>
                        <!-- /.form-group -->
                      </div>
                      <!-- /.col -->                      
                    </div>
                    <?php echo '</form>'; ?>
                </div>
            </div>
          </div>
        </div>
    </div>
</section>