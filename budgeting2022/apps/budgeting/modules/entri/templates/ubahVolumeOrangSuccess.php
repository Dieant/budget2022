<?php use_helper('I18N', 'Date', 'Url', 'Javascript', 'Form', 'Object') ?>

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>Ubah Volume Orang <?php echo $kode_kegiatan; ?></h1>
</section>

<!-- Main content -->
<section class="content">
    <?php include_partial('entri/list_messages'); ?>
    <!-- Default box -->

    <?php echo form_tag('entri/prosesUbahVolumeOrang'); ?>
    <div class="box box-primary box-solid">
        <div class="box-body">
            <div id="sf_admin_container">
                <div id="sf_admin_content" class="table-responsive">
                    <table cellspacing="0" class="sf_admin_list">
                        <thead>
                            <tr>
                                <th><b>Nama Komponen</b></th>
                                <th><b>Satuan</b></th>
                                <th><b>Keterangan Koefisien</b></th>
                                <th><b>Harga</b></th>
                                <th><b>Total</b></th>
                                <th><b>Volume Orang Semula</b></th>
                                <th><b>Volume Orang Menjadi</b></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $counter = 0;
                            while($rs_rinciandetail->next()) {
                                $counter++;
                                $nama = $rs_rinciandetail->getString('komponen_name');
                                $style = '';
                                echo "<tr $style>";

                                echo "<td>".$rs_rinciandetail->getString('komponen_name')." ".$rs_rinciandetail->getString('detail_name')."</td>";
                                echo "<td>".$rs_rinciandetail->getString('satuan')."</td>";
                                echo "<td>".$rs_rinciandetail->getString('keterangan_koefisien')."</td>";
                                echo "<td align='right'>".number_format($rs_rinciandetail->getString('komponen_harga'), 0, ',', '.')."</td>";
                                echo "<td align='right'>".number_format($rs_rinciandetail->getString('nilai_anggaran'), 0, ',', '.')."</td>";
                                echo "<td align='center'>".$rs_rinciandetail->getString('volume_orang'). "</td>";
                                echo "<td>".input_tag('volume_orang[]', array( 'name' => 'volume_orang', 'type'=> 'number'))."</td>";

                                // echo "<td align='left'>".select_tag('rekening_'.$rs_rinciandetail->getString('detail_no'), $arr_rekening[$rs_rinciandetail->getString('komponen_id')], array('style' => 'color:black', 'disabled' => $rek))."</td>";
                                echo input_hidden_tag('detail_no[]', $rs_rinciandetail->getString('detail_no'));
                                echo input_hidden_tag('unit_id', $unit_id);
                                echo input_hidden_tag('kode_kegiatan', $kode_kegiatan);
                                echo input_hidden_tag('vol_sebelum', $rs_rinciandetail->getString('volume_orang'));
                                echo "</tr>";
                            }
                            ?>
                            <?php if($counter <= 0): ?>
                                <tr><td colspan="16" align="center">Tidak ada komponen yang bisa ditampilkan</td></tr>
                            <?php endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <br/>
            <?php
            if($counter > 0)
                echo submit_tag('Proses Ubah Volume Orang', array('name' => 'proses', 'class' => 'btn btn-success btn-flat')) . '&nbsp;';
            ?>
        </div>
    </div>
    </form>

</section><!-- /.content -->

<script>
    $("#cekSemua").change(function () {
        $(".cek").prop('checked', $(this).prop("checked"));
    });
</script>