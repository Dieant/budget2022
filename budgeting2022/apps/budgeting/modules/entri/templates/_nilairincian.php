<?php

if (isset($filters['tahap']) && $filters['tahap'] == 'pakbp') {
        $tabel_semula = 'revisi6_';
        $tabel_dpn = 'pak_bukuputih_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'pakbb') {
        $tabel_dpn = 'pak_bukubiru_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'murni') {
        $tabel_semula='murni_bukubiru_';
        $tabel_dpn = 'murni_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'murnibp') {
        $tabel_semula='murni_bukuputih_';
        $tabel_dpn = 'murni_bukuputih_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'murnibb') {
        $tabel_semula='murni_bukuputih_';
        $tabel_dpn = 'murni_bukubiru_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'murnibbpraevagub') {
        $tabel_dpn = 'murni_bukubiru_praevagub_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi1') {
        $tabel_semula='murni_';
        $tabel_dpn = 'revisi1_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi1_1') {
        $tabel_dpn = 'revisi1_1_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi2') {
        $tabel_semula='revisi1_';
        $tabel_dpn = 'revisi2_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi2_1') {
        $tabel_dpn = 'revisi2_1_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi2_2') {
        $tabel_dpn = 'revisi2_2_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi3') {
        $tabel_semula='revisi2_';
        $tabel_dpn = 'revisi3_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi3_1') {
        $tabel_dpn = 'revisi3_1_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi4') {
        $tabel_semula='revisi3_';
        $tabel_dpn = 'revisi4_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi5') {
        $tabel_semula='revisi4_';
        $tabel_dpn = 'revisi5_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi6') {
        $tabel_semula='revisi5_';
        $tabel_dpn = 'revisi6_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi7') {
        $tabel_dpn = 'revisi7_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi8') {
        $tabel_dpn = 'revisi8_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi9') {
        $tabel_dpn = 'revisi9_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'revisi10') {
        $tabel_dpn = 'revisi10_';
    } elseif (isset($filters['tahap']) && $filters['tahap'] == 'rkua') {
        $tabel_dpn = 'rkua_';
    } else {
        $tabel_semula='';
        $tabel_dpn = 'dinas_';
    }

//$query="select SUM(volume*komponen_harga_awal*(100+pajak)/100) as nilai
//untuk pembulatan komponen
$query = "select SUM(nilai_anggaran) as nilai
		from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "rincian_detail
		where kegiatan_code ='" . $master_kegiatan->getKodeKegiatan() . "' and unit_id ='" . $master_kegiatan->getUnitId() . "' and status_hapus=FALSE";

$con = Propel::getConnection();
$stmt = $con->prepareStatement($query);
$rs = $stmt->executeQuery();
while ($rs->next()) {
    $nilai = $rs->getFloat('nilai');
}

echo number_format($nilai, 0, ',', '.');
//--> Dibuka Jika tampilan untuk dewan
//echo ' '.link_to(image_tag('/sf/sf_admin/images/list.png', array('alt' => __('Rincian RAPBD Pembahasan'), 'title' => __('Rincian RAPBD Pembahasan'))), 'report/bandingRekeningMurni?unit_id='.$master_kegiatan->getUnitId().'&kode_kegiatan='.$master_kegiatan->getKodeKegiatan());
?>