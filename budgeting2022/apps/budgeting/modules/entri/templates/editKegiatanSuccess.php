<?php use_helper('I18N', 'Date', 'Url', 'Javascript', 'Form', 'Object', 'Number', 'Validation') ?>
<?php
$unit_id = $rs_rinciandetail->getUnitId();
$kegiatan_code = $rs_rinciandetail->getKegiatanCode();
$detail_no = $rs_rinciandetail->getDetailNo();
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Ubah Komponen</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Lembar Kerja</a></li>
                    <li class="breadcrumb-item active">Ubah Komponen</li>
                </ol>
            </div>
        </div>
    </div>
</section>
<section class="content">
    <?php include_partial('entri/list_messages'); ?>
    <?php
//    fungsi warning lengkap
    $lelang = 0;
    $totNilaiAlokasi = 0;
    $totNilaiKontrak = 0;
    $totNilaiSwakelola = 0;
    $totNilaiRealisasi = 0;
    $totVolumeRealisasi = 0;
    $totNilaiHps = 0;
    $ceklelangselesaitidakaturanpembayaran = 0;
    $totNilaiKontrakTidakAdaAturanPembayaran = 0;

    if (sfConfig::get('app_fasilitas_cekeProject') == 'buka') {
        $totNilaiAlokasi = $rs_rinciandetail->getCekNilaiAlokasiProject($rs_rinciandetail->getUnitId(), $rs_rinciandetail->getKegiatanCode(), $rs_rinciandetail->getDetailNo());
        if (sfConfig::get('app_fasilitas_cekServer') == 'buka') {
            $lelang = $rs_rinciandetail->getCekLelang($rs_rinciandetail->getUnitId(), $rs_rinciandetail->getKegiatanCode(), $rs_rinciandetail->getDetailNo(), $rs_rinciandetail->getNilaiAnggaran());
            if (sfConfig::get('app_fasilitas_cekeDelivery') == 'buka') {
                $totNilaiSwakelola = $rs_rinciandetail->getCekNilaiSwakelolaDelivery2($rs_rinciandetail->getUnitId(), $rs_rinciandetail->getKegiatanCode(), $rs_rinciandetail->getDetailNo());

                $totNilaiRealisasi = $rs_rinciandetail->getCekRealisasi($rs_rinciandetail->getUnitId(), $rs_rinciandetail->getKegiatanCode(), $rs_rinciandetail->getDetailNo());
                //khusu PU per PAK 20 16 
                if (!(($unit_id == '1222' && $kegiatan_code == '1.10.15.0002') || $unit_id == '2000' || ($unit_id == '2600' && ($kegiatan_code == '1.03.28.0005' || $kegiatan_code == '1.03.31.0004' || $kegiatan_code == '1.03.31.0005' || $kegiatan_code == '1.03.31.0007')))) {
                    $totVolumeRealisasi = $rs_rinciandetail->getCekVolumeRealisasi($rs_rinciandetail->getUnitId(), $rs_rinciandetail->getKegiatanCode(), $rs_rinciandetail->getDetailNo());
                    $totNilaiKontrak = $rs_rinciandetail->getCekNilaiKontrakDelivery2($rs_rinciandetail->getUnitId(), $rs_rinciandetail->getKegiatanCode(), $rs_rinciandetail->getDetailNo());
                }
                $ceklelangselesaitidakaturanpembayaran = $rs_rinciandetail->getCekLelangTidakAdaAturanPembayaran($rs_rinciandetail->getUnitId(), $rs_rinciandetail->getKegiatanCode(), $rs_rinciandetail->getDetailNo());
                $totNilaiKontrakTidakAdaAturanPembayaran = $rs_rinciandetail->getCekNilaiDeliveryBelumAdaAturanPembayaran2($unit_id, $kode_kegiatan, $detail_no);
            }
        }
    } 

    if ($totNilaiAlokasi > 0 || $totNilaiSwakelola > 0 || $totNilaiKontrak > 0 || $totNilaiRealisasi > 0 || $totVolumeRealisasi > 0 || $lelang > 0 || $totNilaiHps > 0 || $ceklelangselesaitidakaturanpembayaran > 0 || $totNilaiKontrakTidakAdaAturanPembayaran == 1) {
        ?>
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12 stretch-card">                
                <div class="card card-default">
                    <div class="card-body">

                <div class="alert alert-warning alert-dismissable">
                <h4><i class="icon fa fa-ban"></i> Alert!</h4>
            <?php
            if ($totNilaiAlokasi > 0) {
                echo '<p>Nilai yang dialokasikan di eProject sebesar = Rp ' . number_format($totNilaiAlokasi, 0, ',', '.') . '</p>';
            }
            if ($totNilaiSwakelola > 0 || $totNilaiKontrak > 0 || $totNilaiRealisasi > 0) {
                if ($totNilaiRealisasi > 0) {
                    echo '<p>Nilai yang dialokasikan di eDelivery sebesar = Rp ' . number_format($totNilaiRealisasi, 0, ',', '.') . '</p>';
                } else if ($totNilaiSwakelola == 0) {
                    echo '<p>Nilai yang dialokasikan di eDelivery sebesar = Rp ' . number_format($totNilaiKontrak, 0, ',', '.') . '</p>';
                } else if ($totNilaiKontrak == 0) {
                    echo '<p>Nilai yang dialokasikan di eDelivery sebesar = Rp ' . number_format($totNilaiSwakelola, 0, ',', '.') . '</p>';
                } else {
                    echo '<p>Nilai yang dialokasikan di eDelivery sebesar = Rp ' . number_format($totNilaiKontrak, 0, ',', '.') . '</p>';
                }
            }
            if ($lelang > 0) {
                echo '<p>Komponen ini dalam Proses Lelang</p>';
            }
            if ($totNilaiHps > 0) {
                echo '<p>Nilai HPS Komponen sebesar = Rp ' . number_format($totNilaiHps, 0, ',', '.') . '</p>';
            }
            if ($ceklelangselesaitidakaturanpembayaran == 1) {
                echo '<p>Proses Lelang  Selesai & Belum ada Aturan Pembayaran</p>';
            }
            if ($totNilaiKontrakTidakAdaAturanPembayaran == 1) {
                echo '<p>Komponen ini belum ada isian aturan pembayaran di eDelivery. Silahkan mengisi Aturan Pembayaran terlebih dahulu.</p>';
            }
            if ($totVolumeRealisasi > 0) {
                echo '<p>Nilai yang dialokasikan di eDelivery dengan volume ' . number_format($totVolumeRealisasi, 0, ',', '.') . '</p>';
            }
            ?>
            </div>
                  </div>
                </div>               
            </div>
        </div>
     </div>
        <?php
    }
//    fungsi warning lengkap
    ?>

    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="fab fa-foursquare"></i> Form Ubah
                        </h3>
                    </div>
                    <div class="card-body">
                        <?php echo form_tag('entri/editKegiatan', array('class' => 'form-horizontal')) ?>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Kelompok Belanja</label>
                                    <?php
                                    $rekening_code = $rs_rinciandetail->getRekeningCode();
                                    if ( strpos($rekening_code,'5.2.')!==false ) {
                                            $belanja_code = substr($rekening_code, 0, 3);
                                        } else {
                                            $belanja_code = substr($rekening_code, 0, 6);
                                        }
                                    $c = new Criteria();
                                    $c->add(KelompokBelanjaPeer::BELANJA_CODE, $belanja_code);
                                    $rs_belanja = KelompokBelanjaPeer::doSelectOne($c);
                                    if ($rs_belanja) {
                                        echo  "<input class='form-control' value='".$rs_belanja->getBelanjaName()."' readonly></input>";
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Rekening</label>
                                    <?php
                                    $c = new Criteria();
                                    $c->add(RekeningPeer::REKENING_CODE, $rekening_code);
                                    $rs_rekening = RekeningPeer::doSelectOne($c);
                                    if ($rs_rekening) {
                                        $pajak_rekening = $rs_rekening->getRekeningPpn();
                                        echo "<input class='form-control' value='".$rekening_code . ' ' . $rs_rekening->getRekeningName()."' readonly></input>";
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Nama Komponen</label>
                                    <?php echo "<input class='form-control' name='nama_komponen' value='".$rs_rinciandetail->getKomponenName()."' readonly></input>"; ?>
                                    <input type="hidden" id="cek_nama_komponen" class="cek_nama_komponen" value="<?php (strpos($rs_rinciandetail->getKomponenName(),'Gaji Pokok') !== false) ? 'Gaji Pokok' : '' ?>">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Harga</label>
                                    <?php
                                    $komponen_harga = $rs_rinciandetail->getKomponenHarga();
                                    if ($rs_rinciandetail->getSatuan() == '%' || ($komponen_harga != floor($komponen_harga))) {
                                        echo "<span class='form-control'>".$komponen_harga."</span>";
                                        echo input_hidden_tag('harga', $komponen_harga);
                                    } else if ($rs_rinciandetail->getSatuan() == 'Tahun' && $rs_rinciandetail->getVolume()== 1 && $rs_rinciandetail->getTipe() == 'BTL')
                                    {
                                        echo input_tag('harga',$rs_rinciandetail->getKomponenHargaAwal(), array('name' => 'harga', 'required' => 'true', 'class' => 'form-control', 'onChange' => 'hitungTotal()'));
                                    }                                    
                                    else {
                                        echo "<span class='form-control'>".number_format($komponen_harga, 0, ',', '.')."</span>";
                                         echo input_hidden_tag('harga', $komponen_harga);
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Satuan</label>
                                    <?php echo "<span class='form-control'>".$rs_rinciandetail->getSatuan()."</span>"; ?>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Pajak</label>
                                    <?php
                                        echo "<span class='form-control'>". $rs_rinciandetail->getPajak() . '%</span>';
                                        echo input_hidden_tag('pajakx', $rs_rinciandetail->getPajak());
                                    ?>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>Subtitle</label>
                                    <?php
                                    $nama_subtitle = $rs_rinciandetail->getSubtitle();
                                    $spasi_nama_subtitle = trim($nama_subtitle);
                                    $query = "select subtitle,sub_id from " . sfConfig::get('app_default_schema') . ".dinas_subtitle_indikator "
                                            . "where unit_id='" . $sf_params->get('unit') . "' and kegiatan_code='" . $sf_params->get('kegiatan') . "' and subtitle = '$spasi_nama_subtitle'";
                                    $con = Propel::getConnection();
                                    $stmt = $con->prepareStatement($query);
                                    $rs1 = $stmt->executeQuery();
                                    while ($rs1->next()) {
                                        $kode_sub = $rs1->getString('sub_id');
                                    }
                                    echo select_tag('subtitle', objects_for_select($rs_subtitleindikator, 'getSubId', 'getSubtitle', $kode_sub), array('class' => 'form-control select2', 'style' => 'width:100%'));
                                    ?>
                                </div>
                            </div>
                            <!-- /.col -->
                            <?php
                            $tipe = $rs_rinciandetail->getTipe();
                            $tipe2 = $rs_rinciandetail->getTipe2();
                            $estimasi_opsi_lokasi = array('');
                            if (($tipe2 == 'KONSTRUKSI' || $tipe2 == 'TANAH' || $tipe == 'FISIK' || $est_fisik) || ($sf_params->get('lokasi')) || in_array($rs_rinciandetail->getKomponenId(), $estimasi_opsi_lokasi)) 
                            {
                            ?>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Lokasi Sekarang</label>
                                    <?php echo input_tag('lokasi', $rs_rinciandetail->getDetailName(), array('class' => 'form-control')); ?>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Usulan Jasmas</label>
                                    <?php
                                        echo select_tag('jasmas', objects_for_select($rs_jasmas, 'getKodeJasmas', 'getNama', $rs_rinciandetail->getKecamatan(), 'include_custom=--Pilih Jasmas--'), array('class' => 'form-control select2', 'style' => 'width:100%'));
                                    ?>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Nama Jalan</label>
                                    <input type="text" name="lokasi_jalan[]" class="form-control lokasi_jalan_isian" placeholder="Nama Jalan (*wajib diisi apabila lokasi berupa Jalan)">
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Gang/Blok/Kavling</label>
                                    <select class="form-control select2 tipe_gang_isian" name="tipe_gang[]">
                                        <option value="GG">--Pilih--</option>
                                        <option value="GG">GANG</option>
                                        <option value="BLOK">BLOK</option>
                                        <option value="KAV">KAVLING</option>
                                    </select>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Nama Gang/Blok/Kavling</label>
                                    <input type="text" name="lokasi_gang[]" class="form-control lokasi_gang_isian" +placeholder="Nama Gang/Blok/Kavling">
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Nomor Lokasi</label>
                                    <input type="text" name="lokasi_nomor[]" class="form-control lokasi_nomor_isian" placeholder="Nomor">
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>RW</label>
                                    <input type="text" name="lokasi_rw[]" class="form-control lokasi_rw_isian" placeholder="RW">
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>RT</label>
                                    <input type="text" name="lokasi_rt[]" class="form-control lokasi_rt_isian" placeholder="RT">
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Nama Bangunan/Saluran</label>
                                    <input type="text" name="lokasi_tempat[]" class="form-control lokasi_tempat_isian" placeholder="Nama Bangunan/Saluran (*wajib diisi apabila lokasi berupa bangunan/saluran/tempat)">
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Keterangan Lokasi</label>
                                    <input type="text" name="lokasi_keterangan[]" class="form-control lokasi_keterangan_isian" placeholder="Keterangan Lokasi">
                                </div>
                            </div>
                            <!-- /.col -->
                            <?php
                                foreach ($rs_geojson as $value_geojson) 
                                {
                                    if ($value_geojson->getGang() <> '') {
                                        $array_gang = explode('.', $value_geojson->getGang());
                                        $tipe_gang = $array_gang[0];
                                        $nama_gang = trim($array_gang[1]);
                                    }
                                ?>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>Nama Jalan</label>
                                        <span class="input-group-addon">JL.</span><input type="text" name="lokasi_jalan[]" class="form-control lokasi_jalan_isian" placeholder="Nama Jalan (*wajib diisi apabila lokasi berupa Jalan)" value="<?php echo $value_geojson->getJalan() ?>">
                                    </div>
                                </div>
                                <!-- /.col -->
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>Gang/Blok/Kavling</label>
                                        <select class="form-control tipe_gang_isian" name="tipe_gang[]">
                                            <option selected value="<?php echo $tipe_gang ?>"> <?php echo $tipe_gang ?></option>
                                            <option value="GG">--Pilih--</option>
                                            <option value="GG">GANG</option>
                                            <option value="BLOK">BLOK</option>
                                            <option value="KAV">KAVLING</option>
                                        </select>
                                    </div>
                                </div>
                                <!-- /.col -->
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>Nama Gang/Blok/Kavling</label>
                                        <input type="text" name="lokasi_gang[]" class="form-control lokasi_gang_isian" placeholder="Nama Gang/Blok/Kavling" value="<?php echo $nama_gang ?>">
                                    </div>
                                </div>
                                <!-- /.col -->
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>Nomor Lokasi</label>
                                        <input type="text" name="lokasi_nomor[]" class="form-control lokasi_nomor_isian" placeholder="Nomor" value="<?php echo $value_geojson->getNomor() ?>">
                                    </div>
                                </div>
                                <!-- /.col -->
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>RW</label>
                                        <input type="text" name="lokasi_rw[]" class="form-control lokasi_rw_isian" placeholder="RW" value="<?php echo $value_geojson->getRw() ?>">
                                    </div>
                                </div>
                                <!-- /.col -->
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>RT</label>
                                        <input type="text" name="lokasi_rt[]" class="form-control lokasi_rt_isian" placeholder="RT" value="<?php echo $value_geojson->getRt() ?>">
                                    </div>
                                </div>
                                <!-- /.col -->
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>Nama Bangunan/Saluran</label>
                                        <input type="text" name="lokasi_tempat[]" class="form-control lokasi_tempat_isian" placeholder="Nama Bangunan/Saluran (*wajib diisi apabila lokasi berupa bangunan/saluran/tempat)" value="<?php echo $value_geojson->getTempat() ?>">
                                    </div>
                                </div>
                                <!-- /.col -->
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>Keterangan Lokasi</label>
                                        <input type="text" name="lokasi_keterangan[]" class="form-control lokasi_keterangan_isian" placeholder="Keterangan Lokasi" value="<?php echo $value_geojson->getKeterangan() ?>">
                                    </div>
                                </div>
                                <!-- /.col -->
                                <?php
                                }
                                ?>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label><span style="color:red;">*</span> Kecamatan</label>
                                    <?php 
                                        $kec = new Criteria();
                                        $kec->addAscendingOrderByColumn(KecamatanPeer::NAMA); 
                                        $rs_kec = KecamatanPeer::doSelect($kec);
                                        $nama_kecamatan_kel = $rs_kec;
                                        if ($rs_rinciandetail->getLokasiKecamatan()) {
                                            $kec_ini = new Criteria();
                                            $kec_ini->add(KecamatanPeer::NAMA, $rs_rinciandetail->getLokasiKecamatan());
                                            $rs_kec_ini = KecamatanPeer::doSelectOne($kec_ini);
                                            echo select_tag('kecamatan', objects_for_select($nama_kecamatan_kel, 'getId', 'getNama', $rs_kec_ini->getId(), 'include_custom=--Pilih Kecamatan--'), array('class' => 'form-control select2', 'style' => 'width:100%'));
                                        } else {
                                            echo select_tag('kecamatan', objects_for_select($nama_kecamatan_kel, 'getId', 'getNama', '', 'include_custom=--Pilih Kecamatan--'), array('class' => 'form-control select2', 'style' => 'width:100%'));
                                        }
                                    ?>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label><span style="color:red;">*</span> Kelurahan</label>
                                    <?php
                                        if ($rs_rinciandetail->getLokasiKelurahan() && $rs_rinciandetail->getLokasiKecamatan())
                                        {
                                            $kec_kel_ini = new Criteria();
                                            $kec_kel_ini->add(KelurahanKecamatanPeer::NAMA_KELURAHAN, $rs_rinciandetail->getLokasiKelurahan());
                                            $kec_kel_ini->add(KelurahanKecamatanPeer::NAMA_KECAMATAN, $rs_rinciandetail->getLokasiKecamatan());
                                            $rs_kec_kel_ini = KelurahanKecamatanPeer::doSelectOne($kec_kel_ini);
                                            $kel_ini = new Criteria();
                                            $kel_ini->add(KelurahanKecamatanPeer::NAMA_KECAMATAN, $rs_rinciandetail->getLokasiKecamatan());
                                            $nama_kel_ini = KelurahanKecamatanPeer::doSelect($kel_ini);
                                            $options = array();
                                            foreach ($nama_kel_ini as $kel) {
                                                $options[$kel->getOid()] = $kel->getNamaKelurahan();
                                            }
                                            echo select_tag('kelurahan', options_for_select($options, $rs_kec_kel_ini->getOid(), 'include_custom=--Pilih Kecamatan Dulu--'), Array('id' => 'kelurahan1', 'class' => 'form-control select2', 'style' => 'width:100%'));
                                        } else {
                                            echo select_tag('kelurahan', options_for_select(array(), '', 'include_custom=--Pilih Kecamatan Dulu--'), Array('id' => 'kelurahan1', 'class' => 'form-control select2', 'style' => 'width:100%'));
                                        }
                                    ?>
                                </div>
                            </div>
                            <!-- /.col -->
                            <?php
                            } else {
                            ?>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>Keterangan</label>
                                    <?php echo textarea_tag('keterangan', $rs_rinciandetail->getDetailName(),array('class' => 'form-control')) ?>
                                </div>
                            </div>
                            <!-- /.col -->
                            <?php
                            }
                            $keterangan_koefisien = $rs_rinciandetail->getKeteranganKoefisien();
                            $pisah_kali = explode('X', $keterangan_koefisien);
                            $accres = '';
                            for ($i = 0; $i < 4; $i++)
                            {
                                $satuan = '';
                                $volume = '';
                                $nama_input = 'vol' . ($i + 1);
                                $nama_pilih = 'volume' . ($i + 1);
                                ;
                                $disabled = false;

                                if (!empty($pisah_kali[$i])) {
                                    $pisah_spasi = explode(' ', $pisah_kali[$i]);
                                    $j = 0;

                                    for ($s = 0; $s < count($pisah_spasi); $s++) {
                                        if ($pisah_spasi[$s] != NULL) {
                                            if ($j == 0) {
                                                $volume = $pisah_spasi[$s];
                                                $j++;
                                            } elseif ($j == 1) {
                                                $satuan = $pisah_spasi[$s];
                                                $j++;
                                            } else {
                                                $satuan.=' ' . $pisah_spasi[$s];
                                            }

                                            if($satuan == 'Accres'){
                                                $accres = 'ada';
                                                $disabled = true;
                                            }
                                        }
                                    }
                                }
                                if ($i == 0) {
                                ?>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Volume</label>
                                    <?php echo input_tag($nama_input, $volume, array('class' => 'form-control', 'onChange' => 'hitungTotal()', 'required' => 'true', 'readonly'=>$disabled));?>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Satuan</label>
                                    <?php echo select_tag($nama_pilih, objects_for_select($rs_satuan, 'getSatuanName', 'getSatuanName', $satuan, 'include_custom=--Pilih Satuan--'), array('class' => 'form-control select2', 'onChange' => 'hitungTotal()', 'readonly'=>$disabled)); ?>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>X</label>
                                </div>
                            </div>
                            <?php
                                } elseif ($i !== 3) {
                                ?>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Volume</label>
                                    <?php echo input_tag($nama_input, $volume, array('class' => 'form-control', 'onChange' => 'hitungTotal()', 'readonly'=>$disabled));?>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Satuan</label>
                                    <?php echo select_tag($nama_pilih, objects_for_select($rs_satuan, 'getSatuanName', 'getSatuanName', $satuan, 'include_custom=--Pilih Satuan--'), array('class' => 'form-control select2',  'onChange' => 'hitungTotal()','readonly'=>$disabled)); ?>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>X</label>
                                </div>
                            </div>
                            <?php
                                } else {
                            ?>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Volume</label>
                                    <?php echo input_tag($nama_input, $volume, array('class' => 'form-control', 'onChange' => 'hitungTotal()', 'readonly'=>$disabled));?>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Satuan</label>
                                    <?php echo select_tag($nama_pilih, objects_for_select($rs_satuan, 'getSatuanName', 'getSatuanName', $satuan, 'include_custom=--Pilih Satuan--'), array('class' => 'form-control select2',  'onChange' => 'hitungTotal()','readonly'=>$disabled)); ?>
                                </div>
                            </div>
                            <?php
                                }
                            }
                            ?>

                            <!-- Jika Komponen Gaji Pokok -->
                            <?php if(strpos($rs_rinciandetail->getKomponenName(),'Gaji Pokok') !== false || $rs_rinciandetail->getAccres() > 0 ) {

                               if (strpos($rs_rinciandetail->getKomponenName(),'Gaji Pokok') !== false) {
                             ?>
                            <div class="col-sm-6 mt-3 mb-3">
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Kenaikan Gapok</span>
                                        </div>
                                        <?php echo input_tag('kenaikan_gapok', '0', array('readonly' => 'true', 'name' => 'kenaikan_gapok', 'class' => 'form-control')) ?>
                                        <div class="input-group-append">
                                            <span class="input-group-text">%</span>
                                        </div>
                                    </div>
                                </div>
                            </div>    
                        <?php } 
                        if($rs_rinciandetail->getAccres() > 0 && strpos($rs_rinciandetail->getKomponenName(),'Gaji Pokok') === false)
                        {
                            $row='col-sm-12';
                        }
                        else
                        {
                            $row='col-sm-6';
                        }
                        ?>
                    
                            <div class="<?php echo $row;?> mt-3 mb-3">
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Acress</span>
                                        </div>
                                        <?php echo input_tag('acress', $rs_rinciandetail->getAccres(), array('readonly' => 'true', 'name' => 'acress', 'class' => 'form-control')) ?>
                                        <div class="input-group-append">
                                            <span class="input-group-text">%</span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <?php 
                            $volume_suami=0;
                            $volume_anak=0;
                            //cari volume tunjangan suami/istri dan anak
                             $detail_gaji=$rs_rinciandetail->getDetailKegiatan();
                             $query = " select  volume_orang,detail_gaji,komponen_name from ebudget.dinas_rincian_detail "
                            . " where status_hapus=false and detail_gaji='$detail_gaji' and komponen_name ilike '%Tunjangan Suami%'";
                             $stmt = $con->prepareStatement($query);
                             $rs = $stmt->executeQuery();
                             if ($rs->next()) {                               
                                $volume_suami = $rs->getInt('volume_orang');
                               
                             }

                            $query1 = " select  volume_orang,detail_gaji,komponen_name from ebudget.dinas_rincian_detail "
                            . " where status_hapus=false and detail_gaji='$detail_gaji' and komponen_name ilike '%Tunjangan Anak%'";
                             $stmt1 = $con->prepareStatement($query1);
                             $rs1 = $stmt1->executeQuery();
                             if ($rs1->next()) {                               
                                $volume_anak = $rs1->getInt('volume_orang');
                               
                             }
                            // die ($detail_gaji.'-'.$volume_suami.'-'.$volume_anak);
                              if(strpos($rs_rinciandetail->getKomponenName(),'Gaji Pokok') !== false )
                                {
                            ?>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>Jumlah Suami/Istri</label>
                                    <?php echo input_tag('jumlah_suami_istri',  $volume_suami, array('name' => 'jumlah_suami_istri', 'class' => 'form-control jumlah_suami_istri', 'onkeyup'=>'cekMaksSuamiIstri(this)')) ?>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>Jumlah Anak</label>
                                    <?php echo input_tag('jumlah_anak',  $volume_anak, array('name' => 'jumlah_anak', 'class' => 'form-control jumlah_anak', 'onkeyup'=>'cekMaksAnak(this)')) ?>
                                </div>
                            </div>
                            <?php 
                                }

                            } ?>

                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>Total</label>
                                    <?php echo input_tag('total', $rs_rinciandetail->getNilaiAnggaran(), array('readonly' => 'true', 'name' => 'total', 'required' => 'true', 'class' => 'form-control')) ?>
                                </div>
                            </div>
                            <?php
                            $koKom = substr($rs_rinciandetail->getKomponenId() , 0, 18);
                            if(($koKom == '2.1.1.01.01.01.008' or $koKom == '2.1.1.03.05.01.001'  or $koKom == '2.1.1.01.01.01.004' or $koKom == '2.1.1.01.01.02.099' or $koKom == '2.1.1.01.01.01.005' or $koKom == '2.1.1.01.01.01.006') or ($rs_rinciandetail->getSatuan() == 'Orang Bulan'))
                            {
                            ?>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>Volume Orang</label>
                                    <?php echo input_tag('volume_orang', $rs_rinciandetail->getVolumeOrang(), array('readonly' => 'true', 'type' => 'number', 'class' => 'form-control')) ?>
                                    <font style="color: green"> *Jumlah Tenaga Kontrak Per Komponen</font>
                                </div>
                            </div>
                            <?php
                            }
                            if ($unit_id == '1800' || $unit_id == '0300' || $unit_id == '9999')
                            {
                            ?>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>BLUD</label><br />
                                    <?php
                                        $nilai_blud = $rs_rinciandetail->getIsBlud();
                                        if ($nilai_blud == true && $rs_rinciandetail->getSumberDanaId()== 6) {
                                            echo checkbox_tag('blud', 1, TRUE, array('class' => ''));
                                        } else {
                                            echo checkbox_tag('blud', 1, FALSE,array('class' => ''));
                                        }
                                    ?>
                                    <font style="color: green"> *Centang jika komponen BLUD</font>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Kapitasi BPJS</label><br />
                                    <?php
                                        $nilai_kapitasi = $rs_rinciandetail->getIsKapitasiBpjs();
                                        if ($nilai_kapitasi == true) {
                                            echo checkbox_tag('kapitasi', 1, TRUE, array('class' => ''));
                                        } else {
                                            echo checkbox_tag('kapitasi', 1, FALSE,array('class' => ''));
                                        }
                                    ?>
                                    <font style="color: green"> *Centang jika komponen Kapitasi BPJS</font>
                                </div>
                            </div>
                            <?php
                            }
                            if ($unit_id == '2000' || $unit_id == '9999')
                            {
                            ?>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>BOS</label><br />
                                    <?php
                                        $nilai_bos = $rs_rinciandetail->getIsBos();
                                        if ($nilai_bos == true) {
                                            echo checkbox_tag('bos', 1, TRUE, array('class' => ''));
                                        } else {
                                            echo checkbox_tag('bos', 1, FALSE,array('class' => ''));
                                        }
                                    ?>
                                    <font style="color: green"> *Centang jika komponen BOS</font>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>BOPDA</label><br />
                                    <?php
                                        $nilai_bopda = $rs_rinciandetail->getIsBobda();
                                        if ($nilai_bopda == true) {
                                            echo checkbox_tag('bopda', 1, TRUE, array('class' => ''));
                                        } else {
                                            echo checkbox_tag('bopda', 1, FALSE,array('class' => ''));
                                        }
                                    ?>
                                    <font style="color: green"> *Centang jika komponen BOPDA</font>
                                </div>
                            </div>
                            <?php } ?>

                            <!-- Checkbox Accres -->
                            <?php if(strpos($rs_rinciandetail->getKomponenName(),'Gaji Pokok') !== false) { ?>
                            <!-- <div class="col-sm-12">
                                <div class="form-group">
                                    <label>Accres</label><br />
                                    <?php
                                        // if ($accres == 'ada') {
                                        //     echo checkbox_tag('checkbox_accres', 1, TRUE, array('class' => 'checkbox_accres', 'onclick'=>'setAccres()'));
                                        // } else {
                                        //     echo checkbox_tag('checkbox_accres', 1, FALSE, array('class' => 'checkbox_accres', 'onclick'=>'setAccres()'));
                                        // }
                                    ?>
                                    <font style="color: green"> *Centang jika Accres</font>
                                </div>
                            </div> -->
                            <?php }else{ ?>

                            <?php //if(strpos($rs_rinciandetail->getKomponenName(),'Gaji Pokok') !== true) { ?>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Musrenbang</label><br />
                                    <?php
                                        $nilai_mus = $rs_rinciandetail->getIsMusrenbang();
                                        if ($nilai_mus == true) {
                                            echo checkbox_tag('musrenbang', 1, TRUE, array('class' => ''));
                                        } else {
                                            echo checkbox_tag('musrenbang', 1, FALSE, array('class' => ''));
                                        }
                                    ?>
                                    <font style="color: green"> *Centang jika komponen Musrenbang</font>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Hibah</label><br />
                                    <?php
                                        $nilai_hibah = $rs_rinciandetail->getIsHibah();
                                        if ($nilai_hibah == true) {
                                            echo checkbox_tag('hibah', 1, TRUE, array('class' => ''));
                                        } else {
                                            echo checkbox_tag('hibah', 1, FALSE, array('class' => ''));
                                        }
                                    ?>
                                    <font style="color: green"> *Centang jika komponen Hibah</font>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>Anggaran Sisa Pengadaan</label><br />
                                    <input type="checkbox" onchange="toggleSisaLelang(this)" id="lelang" name="lelang" <?php if(count($data_existed)) echo "checked"; ?>>
                                    <font style="color: green"> *Centang jika komponen diambil dari anggaran sisa pengadaan</font>
                                    <input type="hidden" name="lebihDariLelang" id="anomali">
                                    <div id="tblSisaLelang" class="table-responsive">
                                        <table class="table table-bordered">
                                            <thead class="head_peach">
                                                <tr>
                                                    <th></th>
                                                    <th>Sisa Pemaketan</th>
                                                    <th>Nilai Diambil</th>
                                                    <th>Catatan</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                foreach ($data_oper as $val)
                                                {
                                                    $nilai_sisa = $val['sisa'];
                                                    $key = $val['uid'].'.'.$val['keg_code'].'.'.$val['det_no'];
                                                    if($val['uid'] != $data_existed[$key]['unit_id'] || $val['keg_code'] != $data_existed[$key]['kegiatan_code'] || $val['det_no'] != $data_existed[$key]['detail_no'])
                                                    {
                                                    ?>
                                                    <tr>
                                                        <td align="center">
                                                            <input type="checkbox" onchange="enableTextBox(this)" name="chkPSP[]" value="<?php echo $val['det_no']; ?>">
                                                            <input class="form-control" type="hidden" value="<?php echo $val['kom_id']; ?>" id="idPSP<?php echo $val['det_no']; ?>"  name="idPSP<?php echo $val['det_no']; ?>">
                                                            <input class="form-control" type="hidden" value="<?php echo $val['sisa']; ?>" id="sisaPSP<?php echo $val['det_no']; ?>" name="sisaPSP<?php echo $val['det_no']; ?>">
                                                            <input class="form-control" type="hidden" value="<?php echo $val['det_no']; ?>" id="detNoPSP<?php echo $val['det_no']; ?>" name="detNoPSP<?php echo $val['det_no']; ?>">
                                                            <input class="form-control" type="hidden" value="<?php echo $val['keg_code']; ?>" id="keg_codePSP<?php echo $val['det_no']; ?>" name="keg_codePSP<?php echo $val['det_no']; ?>">
                                                        </td>
                                                        <td><?php echo $val['kom_name'] . ' ' . $val['det_name'] . ' ' . ' ('.$val['kom_id'].')'; ?></td>
                                                        <td>
                                                            <span>Rp.</span>
                                                            <input class="form-control" type="text" id="nilaiPSP<?php echo $val['det_no']; ?>" name="<?php echo $val['det_no']; ?>" value="<?php echo $val['sisa']; ?>" onkeyup="cekLebihDariLelang(this)" disabled>
                                                            <span>,-</span><br/>
                                                            <span>Sisa Pengadaan: Rp. <?php echo $val['sisa']; ?>,-</span><br />
                                                            <i style="color: red; display: none;"  id="warningPSP<?php echo $val['det_no']; ?>">Nilai  tidak boleh melebihi sisa pengadaan!</i>
                                                        </td>
                                                        <td>
                                                            <textarea class="form-control" rows="4" cols="35"  id="catatanPSP<?php echo $val['det_no']; ?>" name="catatanPSP<?php echo $val['det_no']; ?>" disabled></textarea>
                                                        </td>
                                                    </tr>
                                                <?php } else { ?>
                                                    <tr>
                                                        <td align="center">
                                                            <input class="form-control" type="checkbox" onchange="enableTextBox(this)" name="chkPSP[]" value="<?php echo $val['det_no']; ?>" checked>
                                                            <input class="form-control" type="hidden" value="<?php echo $val['kom_id']; ?>"  id="idPSP<?php echo $val['det_no']; ?>" name="idPSP<?php echo $val['det_no']; ?>">
                                                            <input class="form-control" type="hidden" value="<?php echo $val['sisa']; ?>" id="sisaPSP<?php echo $val['det_no']; ?>" name="sisaPSP<?php echo $val['det_no']; ?>">
                                                            <input type="hidden" value="<?php echo $val['det_no']; ?>" id="detNoPSP<?php echo $val['det_no']; ?>" name="detNoPSP<?php echo $val['det_no']; ?>">
                                                            <input class="form-control" type="hidden" value="<?php echo $val['keg_code']; ?>" id="keg_codePSP<?php echo $val['det_no']; ?>" name="keg_codePSP<?php echo $val['det_no']; ?>">
                                                        </td>
                                                        <td><?php echo $val['kom_name'] . ' ' . $val['det_name'] . ' ' . ' ('.$val['kom_id'].')'; ?> </td>
                                                        <td>
                                                            <span>Rp.</span>
                                                            <input class="form-control" type="text" id="nilaiPSP<?php echo $val['det_no']; ?>" name="<?php echo $val['det_no']; ?>" value="<?php echo $data_existed[$key]['sisa_anggaran']; ?>" onkeyup="cekLebihDariLelang(this)">
                                                            <span>,-</span><br />
                                                            <span>Sisa Pengadaan: Rp. <?php echo $val['sisa']; ?>,-</span><br />
                                                            <i style="color: red; display: none;"  id="warningPSP<?php echo $val['det_no']; ?>">Nilai tidak boleh melebihi sisa pengadaan!</i>
                                                        </td>
                                                        <td>
                                                            <textarea class="form-control" rows="4" cols="35" id="catatanPSP<?php echo $val['det_no']; ?>" name="catatanPSP<?php echo $val['det_no']; ?>"><?php echo $data_existed[$val['det_keg']]['catatan']; ?></textarea>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                    }
                                                }
                                                if(count($data_oper) == 0)
                                                {
                                                    foreach($data_existed as $val)
                                                    {
                                                    ?>
                                                    <tr>
                                                        <td align="center">
                                                            <input class="form-control" type="checkbox" onchange="enableTextBox(this)" name="chkPSP[]" value="<?php echo $val['detail_no']; ?>" checked>
                                                            <input class="form-control" type="hidden" value="<?php echo $val['komponen_id']; ?>" id="idPSP<?php echo $val['detail_no']; ?>" name="idPSP<?php echo $val['detail_no']; ?>">
                                                            <input class="form-control" type="hidden" value="<?php echo $val['sisa_anggaran']; ?>" id="sisaPSP<?php echo $val['detail_no']; ?>" name="sisaPSP<?php echo $val['detail_no']; ?>">
                                                            <input type="hidden" value="<?php echo $val['detail_no']; ?>" id="detNoPSP<?php echo $val['detail_no']; ?>" name="detNoPSP<?php echo $val['detail_no']; ?>">
                                                        </td>
                                                        <td><?php echo $val['komponen_name'] . ' ' . $val['detail_name'] . ' ' . ' ('.$val['komponen_id'].')'; ?></td>
                                                        <td>
                                                            <span>Rp.</span>
                                                            <input class="form-control" type="text" id="nilaiPSP<?php echo $val['detail_no']; ?>" name="<?php echo $val['detail_no']; ?>" value="<?php echo $val['sisa_anggaran']; ?>" onkeyup="cekLebihDariLelang(this)">
                                                            <span>,-</span><br />
                                                            <span>Sisa Pengadaan: Rp. <?php echo $val['sisa_anggaran']; ?>,-</span><br />
                                                            <i style="color: red; display: none;" id="warningPSP<?php echo $val['detail_no']; ?>">Nilai tidak boleh melebihi sisa pengadaan!</i>
                                                        </td>
                                                        <td>
                                                            <textarea class="form-control" rows="4" cols="35" id="catatanPSP<?php echo $val['detail_no']; ?>" name="catatanPSP<?php echo $val['detail_no']; ?>"><?php echo $val['catatan']; ?></textarea>
                                                        </td>
                                                    </tr>
                                                <?php
                                                    }
                                                }
                                                if(!count($data_oper))
                                                {
                                                ?>
                                                    <tr>
                                                        <td colspan="4" style="text-align: center;">Belum ada data yang bisa ditampilkan</td>
                                                    </tr>
                                                    <?php
                                                }
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                            <?php
                            if (sfConfig::get('app_fasilitas_bukaCatatanPergeseran') == 'buka')
                            {
                            ?>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label><span style="color:red;">*</span>Catatan Pergeseran Anggaran</label>
                                    <?php echo textarea_tag('catatan', $rs_rinciandetail->getNoteSkpd(),array('class' => 'form-control', 'required' => 'true')) ?>
                                </div>
                            </div>
                            <!-- /.col -->
                            <?php
                            } else {
                                $c = new Criteria();
                                $c->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                                $c->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kegiatan_code);
                                $rs_kegiatan = DinasMasterKegiatanPeer::doSelectOne($c);
                                if ($rs_kegiatan->getTahap() == 'murni') {
                            ?>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label><span style="color:red;">*</span>Catatan Pergeseran Anggaran</label>
                                    <?php echo textarea_tag('catatan', $rs_rinciandetail->getNoteSkpd(),array('class' => 'form-control', 'required' => 'false')) ?>
                                </div>
                            </div>
                            <!-- /.col -->
                            <?php
                                }
                            }
                            ?>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>Catatan Koefisien (Catatan singkat rincian koefisien untuk volume yang dimasukkan)</label>
                                    <?php echo textarea_tag('catatan_koefisien', $rs_rinciandetail->getNoteKoefisien(),array('class' => 'form-control')) ?>
                                   
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="tombol_filter">Tombol Filter</label><br />
                                    <?php
                                        echo input_hidden_tag('kegiatan', $sf_params->get('kegiatan'));
                                        echo input_hidden_tag('unit', $sf_params->get('unit'));
                                        echo input_hidden_tag('id', $sf_params->get('id'));
                                        echo input_hidden_tag('referer', $sf_request->getAttribute('referer'));
                                        if (FALSE) {
                                            echo submit_tag('simpan', 'name=simpan') . ' ' . button_to('kembali', '#', array('onClick' => "javascript:history.back()"));
                                        } elseif ($lelang == 1 || $ceklelangselesaitidakaturanpembayaran == 1) {
                                            echo button_to('kembali', '#', array('onClick' => "javascript:history.back()"));
                                        } elseif ($lelang == 0 && $ceklelangselesaitidakaturanpembayaran == 0) {
                                            echo submit_tag('simpan', 'name=simpan') . ' ' . button_to('kembali', '#', array('onClick' => "javascript:history.back()"));
                                        }
                                    ?>
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <?php echo '</form>'; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $(document).ready(function () {

        var id = $("#subtitle").val();
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/entri/pilihsubx/kegiatan_code/<?php echo $sf_params->get('kegiatan') ?>/unit_id/<?php echo $sf_params->get('unit') ?>/b/" + id + ".html",
            context: document.body
        }).done(function (msg) {
            $('#sub1').html(msg);
        });

        <?php if(!count($data_existed)) { ?>
        var table = document.getElementById("tblSisaLelang");
        table.style.display = "none";
        <?php } ?>
    });

    $(function () {
        $(document).on('click', 'div.form-group-options .input-group-addon-add', function () {
            var divIluminati = $(this).parents('.div-induk-lokasi');
            var sDivIluminatiHtml = divIluminati.html();
            var sInputGroupClasses = divIluminati.attr('class');
            //Gambiarra pra nao ficar criando mil inputs
            if (divIluminati.next().length >= 1)
                return;
            divIluminati.parent().append('<div class="' + sInputGroupClasses + '">' + sDivIluminatiHtml + '</div>');
        });
        $(document).on('click', 'div.form-group-options .input-group-addon-remove', function () {
            var divIluminati = $(this).parents('.div-induk-lokasi');
            divIluminati.remove();
        });
    });

    $("#subtitle").change(function () {
        var id = $(this).val();
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/entri/pilihsubx/kegiatan_code/<?php echo $sf_params->get('kegiatan') ?>/unit_id/<?php echo $sf_params->get('unit') ?>/b/" + id + ".html",
            context: document.body
        }).done(function (msg) {
            $('#sub1').html(msg);
        });

    });

    $("#kecamatan").change(function () {
        var id = $(this).val();
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/entri/pilihKelurahan/b/" + id + ".html",
            context: document.body
        }).done(function (msg) {
            $('#kelurahan1').html(msg);
        });

    });

    function hitungTotal() {
        var harga = $('#harga').val();
        var pajakx = $('#pajakx').val();
        var vol1 = $('#vol1').val();
        var volume1=$('#select2-volume1-container').val();
        var vol2 = $('#vol2').val();
        var volume2=$('#select2-volume2-container').val();
        var vol3 = $('#vol3').val();
        var volume3=$('#select2-volume3-container').val();
        var vol4 = $('#vol4').val();
        var volume4=$('#select2-volume4-container').val();

        if (volume1=='%')
        {
            vol1=vol1/100;
        }
        if  (volume2=='%')
        {
            vol2=vol2/100;
        }
        if  (volume3=='%')
        {
            vol3=vol3/100;
        }
        if  (volume4=='%')
        {
            vol4=vol4/100;
        }

        var volume;
        var hitung;


        if (vol1 !== '' || vol2 !== '' || vol3 !== '' || vol4 !== '') {
            if (vol2 === '') {
                vol2 = 1;
                volume = vol1 * vol2;
            } else if (vol2 !== '') {
                volume = vol1 * vol2;
            }
            if (vol3 === '') {
                vol3 = 1;
                volume = volume * vol3;
            } else if (vol3 !== '') {
                volume = vol1 * vol2 * vol3;
            }
            if (vol4 === '') {
                vol4 = 1;
                volume = volume * vol4;
            } else if (vol4 !== '') {
                volume = vol1 * vol2 * vol3 * vol4;
            }
        }

        if (pajakx == 10) {
            hitung = Math.floor(harga * volume * 1.1);
        } else if (pajakx == 0) {
            hitung = Math.floor(harga * volume * 1);
        }



        $('#total').val(hitung);

    }

    function toggleSisaLelang(element) {
        var table = document.getElementById("tblSisaLelang");
        if(element.checked) {
            table.style.display = "table-row";
        } else {
            table.style.display = "none";
        }
    }

    function enableTextBox(element) {
        var nilaiPSP = document.getElementById("nilaiPSP" + element.value);
        var catatanPSP = document.getElementById("catatanPSP" + element.value);
        if(element.checked) {
            nilaiPSP.disabled = false;
            catatanPSP.disabled = false;
        } else {
            nilaiPSP.disabled = true;
            catatanPSP.disabled = true;
        }
    }

    function cekLebihDariLelang(element) {
        var warningPSP = document.getElementById("warningPSP" + element.name);
        var diambil = document.getElementById("nilaiPSP" + element.name).value;
        var asli = document.getElementById("sisaPSP" + element.name).value;
        if(parseFloat(diambil) > parseFloat(asli)) {
            warningPSP.style.display = 'inline';
            $('#anomali').val('1');
        } else {
            warningPSP.style.display = 'none';
            $('#anomali').val('0');
        }
    }

    function setAccres(){
        var vol1 = $('#vol1').val();
        var vol2 = $('#vol2').val();
        var vol3 = $('#vol3').val();
        var vol4 = $('#vol4').val();
        
        var satuan1 = $('#volume1').val();
        var satuan2 = $('#volume2').val();
        var satuan3 = $('#volume3').val();
        var satuan4 = $('#volume4').val();

        if($('.checkbox_accres').is(':checked')){
            if(vol1 == '' && satuan1 == ''){
                $('#vol1').val('1.025');
                $('#vol1').prop('readonly',true);
                $('#volume1').val('Accres').trigger('change');
                $('#volume1').select2('readonly',true);
            }else if(vol2 == '' && satuan2 == ''){
                $('#vol2').val('1.025');
                $('#vol2').prop('readonly',true);
                $('#volume2').val('Accres').trigger('change');
                $('#volume2').select2('readonly',true);
            }else if(vol3 == '' && satuan3 == ''){
                $('#vol3').val('1.025');
                $('#vol3').prop('readonly',true);
                $('#volume3').val('Accres').trigger('change');
                $('#volume3').select2('readonly',true);
            }else if(vol4 == '' && satuan4 == ''){
                $('#vol4').val('1.025');
                $('#vol4').prop('readonly',true);
                $('#volume4').val('Accres').trigger('change');
                $('#volume4').select2('readonly',true);
            }
        }else{
            if(vol1 == '1.025' && satuan1 == 'Accres'){
                $('#vol1').val('');
                $('#vol1').prop('readonly',false);
                $('#volume1').val('').trigger('change');
                $('#volume1').select2('readonly',false);
            }else if(vol2 == '1.025' && satuan2 == 'Accres'){
                $('#vol2').val('');
                $('#vol2').prop('readonly',false);
                $('#volume2').val('').trigger('change');
                $('#volume2').select2('readonly',false);
            }else if(vol3 == '1.025' && satuan3 == 'Accres'){
                $('#vol3').val('');
                $('#vol3').prop('readonly',false);
                $('#volume3').val('').trigger('change');
                $('#volume3').select2('readonly',false);
            }else if(vol4 == '1.025' && satuan4 == 'Accres'){
                $('#vol4').val('');
                $('#vol4').prop('readonly',false);
                $('#volume4').val('').trigger('change');
                $('#volume4').select2('readonly',false);
            }
        }
        hitungTotal();
    }

    function cekMaksSuamiIstri(obj){
        var suamiistri = $(obj).val();
        var cek_nama_komponen = $('#cek_nama_komponen').val();

        //cek volume orang
        var volume1 = $('#volume1').val();
        var volume2 = $('#volume2').val();
        var volume3 = $('#volume3').val();
        var volume4 = $('#volume4').val();
        var orang = 0;
        if(volume1 == 'Orang'){
            orang = $('#vol1').val();
        }else if(volume2 == 'Orang'){
            orang = $('#vol2').val();
        }else if(volume3 == 'Orang'){
            orang = $('#vol3').val();
        }else if(volume4 == 'Orang'){
            orang = $('#vol4').val();
        }

        if(orang=='' || parseInt(orang)==0){
            toastr.error('Volume orang belum diinputkan','Alert!');
            $(obj).val('0');
        }

        if(parseInt(suamiistri) > parseInt(orang)){
            toastr.error('Jumlah Suami/Istri Melebihi volume orang (1 Pegawai maks 1 Suami/Istri)','Alert!');
            $(obj).val('0');
        }
    }
    
    function cekMaksAnak(obj){
        var anak = $(obj).val();
        var cek_nama_komponen = $('#cek_nama_komponen').val();

        //cek volume orang
        var volume1 = $('#volume1').val();
        var volume2 = $('#volume2').val();
        var volume3 = $('#volume3').val();
        var volume4 = $('#volume4').val();
        var orang = 0;
        if(volume1 == 'Orang'){
            orang = $('#vol1').val();
        }else if(volume2 == 'Orang'){
            orang = $('#vol2').val();
        }else if(volume3 == 'Orang'){
            orang = $('#vol3').val();
        }else if(volume4 == 'Orang'){
            orang = $('#vol4').val();
        }

        if(orang=='' || parseInt(orang)==0){
            toastr.error('Volume orang belum diinputkan','Alert!');
            $(obj).val('0');
        }

        if(parseInt(anak) > parseInt(orang)*2){
            toastr.error('Jumlah Anak Melebihi volume orang (1 Pegawai maks 2 Anak)','Alert!');
            $(obj).val('0');
        }
    }
</script>
