<?php use_helper('I18N', 'Date', 'Object', 'Javascript', 'Validation') ?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Cari Komponen - (<?php echo $cari ?>)</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Lembar Kerja</a></li>
                    <li class="breadcrumb-item active">Cari Komponen</li>
                </ol>
            </div>
        </div>
    </div>
</section>
<!-- Main content -->
<section class="content">
    <?php include_partial('entri/list_messages'); ?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body table-responsive p-0">
                        <table class="table table-hover">
                            <thead class="head_peach">
                                <tr>
                                    <th>Komponen</th>
                                    <th>Spesifikasi</th>
                                    <th>Satuan</th>
                                    <th>Tipe</th>
                                    <th>Harga</th>
                                    <th>Pajak</th>
                                    <th>Rekening</th>
                                    <th> </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $kode_kegiatan = $sf_params->get('kegiatan');
                                $unit_id = $sf_params->get('unit');

                                $rekening_dikunci = '';
                                $belanja_dikunci = '';
                                $rekening_dibuka_belanja_dikunci = '';
                                $komponen_dikunci = '';
                                $komponen_dibuka = '';

                                $rekening_dikunci_urutan = 0;
                                $komponen_dikunci_urutan = 0;
                                $komponen_dibuka_urutan = 0;

                                $belanja_dikunci_urutan = 0;
                                $rekening_dibuka_belanja_dikunci_urutan = 0;

                                $con = Propel::getConnection();

                                $query_rekening_dikunci = "select * from " . sfConfig::get('app_default_schema') . ".rekening_dikunci "
                                        . "where unit_id = '$unit_id' and kegiatan_code = '$kode_kegiatan' ";
                                $stmt_rekening_dikunci = $con->prepareStatement($query_rekening_dikunci);
                                $data_rekening_dikunci = $stmt_rekening_dikunci->executeQuery();
                                while ($data_rekening_dikunci->next()) {
                                    $rekening_code = $data_rekening_dikunci->getString('rekening_code');

                                    if ($rekening_dikunci_urutan == 0) {
                                        $rekening_dikunci = $rekening_dikunci . '\'' . $rekening_code . '\'';
                                    } else {
                                        $rekening_dikunci = $rekening_dikunci . ',\'' . $rekening_code . '\'';
                                    }
                                    $rekening_dikunci_urutan++;
                                }

                                $query_komponen_dikunci = "select * from " . sfConfig::get('app_default_schema') . ".komponen_dikunci "
                                        . "where unit_id = '$unit_id' and kegiatan_code = '$kode_kegiatan' ";
                                $stmt_komponen_dikunci = $con->prepareStatement($query_komponen_dikunci);
                                $data_komponen_dikunci = $stmt_komponen_dikunci->executeQuery();
                                while ($data_komponen_dikunci->next()) {
                                    $komponen_id = $data_komponen_dikunci->getString('komponen_id');

                                    if ($komponen_dikunci_urutan == 0) {
                                        $komponen_dikunci = $komponen_dikunci . '\'' . $komponen_id . '\'';
                                    } else {
                                        $komponen_dikunci = $komponen_dikunci . ',\'' . $komponen_id . '\'';
                                    }
                                    $komponen_dikunci_urutan++;
                                }

                                $query_komponen_dibuka = "select * from " . sfConfig::get('app_default_schema') . ".komponen_dibuka "
                                        . "where unit_id = '$unit_id' and kegiatan_code = '$kode_kegiatan' ";
                                $stmt_komponen_dibuka = $con->prepareStatement($query_komponen_dibuka);
                                $data_komponen_dibuka = $stmt_komponen_dibuka->executeQuery();
                                while ($data_komponen_dibuka->next()) {
                                    $komponen_id = $data_komponen_dibuka->getString('komponen_id');

                                    if ($komponen_dibuka_urutan == 0) {
                                        $komponen_dibuka = $komponen_dibuka . '\'' . $komponen_id . '\'';
                                    } else {
                                        $komponen_dibuka = $komponen_dibuka . ',\'' . $komponen_id . '\'';
                                    }
                                    $komponen_dibuka_urutan++;
                                }

                                $query_belanja_dikunci = "select * from " . sfConfig::get('app_default_schema') . ".belanja_dikunci "
                                        . "where unit_id = '$unit_id' and kegiatan_code = '$kode_kegiatan' ";
                                $stmt_belanja_dikunci = $con->prepareStatement($query_belanja_dikunci);
                                $data_belanja_dikunci = $stmt_belanja_dikunci->executeQuery();
                                while ($data_belanja_dikunci->next()) {
                                    $kode_belanja = $data_belanja_dikunci->getString('kode_belanja');
                                    $id_belanja_dikunci = $data_belanja_dikunci->getString('id');

                                    $belanja_dikunci = $belanja_dikunci . ' AND kr.rekening_code not ilike \'' . $kode_belanja . '\'';
                                    $belanja_dikunci_urutan++;

                                    $query_rekening_dibuka_belanja_dikunci = "select * from " . sfConfig::get('app_default_schema') . ".rekening_dibuka_belanja_dikunci "
                                            . "where unit_id = '$unit_id' and kegiatan_code = '$kode_kegiatan' and belanja_dikunci_id = $id_belanja_dikunci ";
                                    $stmt_rekening_dibuka_belanja_dikunci = $con->prepareStatement($query_rekening_dibuka_belanja_dikunci);
                                    $data_rekening_dibuka_belanja_dikunci = $stmt_rekening_dibuka_belanja_dikunci->executeQuery();
                                    while ($data_rekening_dibuka_belanja_dikunci->next()) {
                                        $kode_rekening_belanja_dikunci = $data_rekening_dibuka_belanja_dikunci->getString('rekening_code');

                                        if ($rekening_dibuka_belanja_dikunci_urutan == 0) {
                                            $rekening_dibuka_belanja_dikunci = $rekening_dibuka_belanja_dikunci . '\'' . $kode_rekening_belanja_dikunci . '\'';
                                        } else {
                                            $rekening_dibuka_belanja_dikunci = $rekening_dibuka_belanja_dikunci . ',\'' . $kode_rekening_belanja_dikunci . '\'';
                                        }
                                        $rekening_dibuka_belanja_dikunci_urutan++;
                                        ;
                                    }
                                }

                                if ($rekening_dikunci_urutan == 0) {
                                    $rekening_dikunci = '\'\'';
                                }
                                if ($komponen_dikunci_urutan == 0) {
                                    $komponen_dikunci = '\'\'';
                                }
                                if ($komponen_dibuka_urutan == 0) {
                                    $komponen_dibuka = '\'\'';
                                }
                                if ($belanja_dikunci_urutan == 0) {
                                    $belanja_dikunci = '';
                                }
                                if ($rekening_dibuka_belanja_dikunci_urutan == 0) {
                                    $rekening_dibuka_belanja_dikunci = '\'\'';
                                }

                                $c_kegiatan = new Criteria();
                                $c_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                                $c_kegiatan->addAnd(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                                $dapat_kegiatan = DinasMasterKegiatanPeer::doSelectOne($c_kegiatan);
                                $tahap = DinasMasterKegiatanPeer::ubahTahapAngka($dapat_kegiatan->getTahap());
                                // untuk jenis kegiatan btl atau bl
                                // die($dapat_kegiatan->getIsBtl()); 

                                


                                // $jenis="and k.komponen_tipe <> 'BTL'";

                                // $is_btls = "select * from " . sfConfig::get('app_default_schema') . ".dinas_master_kegiatan                                     
                                //  WHERE nama_kegiatan = 'Penyediaan Gaji dan Tunjangan'";
                                // $con = Propel::getConnection();
                                // $stmt3 = $con->prepareStatement($is_btls);
                                // $rs_cek3 = $stmt3->executeQuery();
                                // while ($rs_cek3->next()) {
                                //     $is_btl= $rs_cek3->getString('nama_kegiatan');                       
                                // }
                                
                                //pengecualian kode sub bisa ambil rekening 5.1.01
                                $pengecualian_buka_pegawai = array ('3670','3657');
                              
                                if (strpos($dapat_kegiatan->getNamaKegiatan(), 'Gaji') !== FALSE || $dapat_kegiatan->getAlokasiDana()==0 || strpos($dapat_kegiatan->getNamaKegiatan(), 'Dana') !== FALSE ||
                                strpos($dapat_kegiatan->getNamaKegiatan(), 'Administrasi Keuangan') !== FALSE ) 
                                {
                                     $jenis=" and (komponen_name not ilike '%Tunjangan Suami%' and komponen_name not ilike '%Tunjangan Anak%')  " ;
                                }
                                else
                                {
                                    if(in_array($dapat_kegiatan->getKodeKegiatan(), $pengecualian_buka_pegawai))
                                    {
                                        $jenis="and (komponen_name not ilike '%Tunjangan Suami%' and komponen_name not ilike '%Tunjangan Anak%') and komponen_tipe <> 'BTL'";
                                    }
                                    else
                                    $jenis="and (komponen_name not ilike '%Tunjangan Suami%' and komponen_name not ilike '%Tunjangan Anak%') and komponen_tipe <> 'BTL' and kr.rekening_code not ilike '5.1.01%'
                                       ";
                                       // 
                                }
                                
                                if (($dapat_kegiatan->getUnitId() == '')) 
                                {
                                    $query = "( select kb.belanja_name,kr.rekening_code,r.rekening_name,  k.satuan, k.komponen_harga,  k.komponen_tipe as tipe, k.komponen_non_pajak as pajak, k.komponen_id, k.komponen_name,k.status_masuk from " . sfConfig::get('app_default_schema') . ".komponen k, " . sfConfig::get('app_default_schema') . ".komponen_rekening kr, " . sfConfig::get('app_default_schema') . ".kelompok_belanja kb, " . sfConfig::get('app_default_schema') . ".rekening r "
                                        . "where k.komponen_name ilike '%" . $cari . "%' and k.komponen_id = kr.komponen_id
                                            and k.komponen_tipe not in ('HSPK')
                                            and kr.rekening_code = r.rekening_code and r.belanja_id = kb.belanja_id
                                    and kr.rekening_code not in ($rekening_dikunci) "
                                        . "and k.komponen_id not in($komponen_dikunci) and k.tahap<='$tahap' and (k.komponen_id not ilike '%.F')"
                                        . " $belanja_dikunci
                                        ) union (
                                    select kb.belanja_name,kr.rekening_code,r.rekening_name,  k.satuan, k.komponen_harga,  k.komponen_tipe as tipe, k.komponen_non_pajak as pajak, k.komponen_id, k.komponen_name,k.status_masuk
                                    from " . sfConfig::get('app_default_schema') . ".komponen k, " . sfConfig::get('app_default_schema') . ".komponen_rekening kr, " . sfConfig::get('app_default_schema') . ".kelompok_belanja kb, " . sfConfig::get('app_default_schema') . ".rekening r "
                                        . "where k.komponen_name ilike '%" . $cari . "%' and k.komponen_id = kr.komponen_id
                                            and k.komponen_tipe not in ('HSPK')
                                            and kr.rekening_code = r.rekening_code and r.belanja_id = kb.belanja_id
                                    and k.komponen_id in($komponen_dibuka) and k.tahap<='$tahap' and (k.komponen_id not ilike '%.F')
                                        ) union (
                                    select kb.belanja_name,kr.rekening_code,r.rekening_name,  k.satuan, k.komponen_harga,  k.komponen_tipe as tipe, k.komponen_non_pajak as pajak, k.komponen_id, k.komponen_name,k.status_masuk
                                    from " . sfConfig::get('app_default_schema') . ".komponen k, " . sfConfig::get('app_default_schema') . ".komponen_rekening kr, " . sfConfig::get('app_default_schema') . ".kelompok_belanja kb, " . sfConfig::get('app_default_schema') . ".rekening r "
                                        . "where k.komponen_name ilike '%" . $cari . "%' and k.komponen_id = kr.komponen_id
                                            and k.komponen_tipe not in ('HSPK')
                                            and kr.rekening_code = r.rekening_code and r.belanja_id = kb.belanja_id
                                    and kr.rekening_code in($rekening_dibuka_belanja_dikunci) and k.tahap<='$tahap' and (k.komponen_id not ilike '%.F')
                                        )
                                        order by komponen_id, rekening_code ";
                                } //tambahan untuk buka komponen HSPK
                                else if ($dapat_kegiatan->getUnitId() == '') 
                                {
                                    $query = "(select kb.belanja_name,kr.rekening_code,r.rekening_name,  k.satuan, k.komponen_harga,  k.komponen_tipe as tipe, k.komponen_non_pajak as pajak, k.komponen_id, k.komponen_name,k.status_masuk from " . sfConfig::get('app_default_schema') . ".komponen k, " . sfConfig::get('app_default_schema') . ".komponen_rekening kr, " . sfConfig::get('app_default_schema') . ".kelompok_belanja kb, " . sfConfig::get('app_default_schema') . ".rekening r "
                                        . "where k.komponen_name ilike '%" . $cari . "%' and k.komponen_id = kr.komponen_id  and
                                    kr.rekening_code = r.rekening_code and r.belanja_id = kb.belanja_id
                                    and kr.rekening_code not in ($rekening_dikunci) "
                                        . "and k.komponen_id not in($komponen_dikunci) and k.tahap<='$tahap' and (k.komponen_id not ilike '%.F') "
                                        . " $belanja_dikunci
                                        ) union (
                                    select kb.belanja_name,kr.rekening_code,r.rekening_name,  k.satuan, k.komponen_harga,  k.komponen_tipe as tipe, k.komponen_non_pajak as pajak, k.komponen_id, k.komponen_name,k.status_masuk
                                    from " . sfConfig::get('app_default_schema') . ".komponen k, " . sfConfig::get('app_default_schema') . ".komponen_rekening kr, " . sfConfig::get('app_default_schema') . ".kelompok_belanja kb, " . sfConfig::get('app_default_schema') . ".rekening r "
                                        . "where k.komponen_name ilike '%" . $cari . "%' and k.komponen_id = kr.komponen_id and
                                    kr.rekening_code = r.rekening_code and r.belanja_id = kb.belanja_id
                                    and k.komponen_id in($komponen_dibuka) and k.tahap<='$tahap' and (k.komponen_id not ilike '%.F')
                                        ) union (
                                    select kb.belanja_name,kr.rekening_code,r.rekening_name,  k.satuan, k.komponen_harga,  k.komponen_tipe as tipe, k.komponen_non_pajak as pajak, k.komponen_id, k.komponen_name,k.status_masuk
                                    from " . sfConfig::get('app_default_schema') . ".komponen k, " . sfConfig::get('app_default_schema') . ".komponen_rekening kr, " . sfConfig::get('app_default_schema') . ".kelompok_belanja kb, " . sfConfig::get('app_default_schema') . ".rekening r "
                                        . "where k.komponen_name ilike '%" . $cari . "%' and k.komponen_id = kr.komponen_id and
                                    kr.rekening_code = r.rekening_code and r.belanja_id = kb.belanja_id
                                    and kr.rekening_code in($rekening_dibuka_belanja_dikunci) and k.tahap<='$tahap' and (k.komponen_id not ilike '%.F')
                                        )
                                        order by komponen_id, rekening_code ";
                                }
                                else if (substr($dapat_kegiatan->getUnitId(), 0, 2) == '128')
                                {
                                    $query = "(select kb.belanja_name,kr.rekening_code,r.rekening_name,  k.satuan, k.komponen_harga,  k.komponen_tipe as tipe, k.komponen_non_pajak as pajak, k.komponen_id, k.komponen_name,k.status_masuk from " . sfConfig::get('app_default_schema') . ".komponen k, " . sfConfig::get('app_default_schema') . ".komponen_rekening kr, " . sfConfig::get('app_default_schema') . ".kelompok_belanja kb, " . sfConfig::get('app_default_schema') . ".rekening r "
                                        . "where k.komponen_name ilike '%" . $cari . "%' and k.komponen_id = kr.komponen_id and k.komponen_tipe <> 'HSPK' and
                                    kr.rekening_code = r.rekening_code and r.belanja_id = kb.belanja_id
                                    and kr.rekening_code not in ($rekening_dikunci) "
                                        . "and k.komponen_id not in($komponen_dikunci) and k.tahap<='$tahap' and k.komponen_id not ilike '%.F' "
                                        . " $belanja_dikunci
                                        ) union (
                                    select kb.belanja_name,kr.rekening_code,r.rekening_name,  k.satuan, k.komponen_harga,  k.komponen_tipe as tipe, k.komponen_non_pajak as pajak, k.komponen_id, k.komponen_name,k.status_masuk
                                    from " . sfConfig::get('app_default_schema') . ".komponen k, " . sfConfig::get('app_default_schema') . ".komponen_rekening kr, " . sfConfig::get('app_default_schema') . ".kelompok_belanja kb, " . sfConfig::get('app_default_schema') . ".rekening r "
                                        . "where k.komponen_name ilike '%" . $cari . "%' and k.komponen_id = kr.komponen_id and k.komponen_tipe <> 'HSPK' and
                                    kr.rekening_code = r.rekening_code and r.belanja_id = kb.belanja_id
                                    and k.komponen_id in($komponen_dibuka) and k.tahap<='$tahap' and k.komponen_id not ilike '%.F'
                                        ) union (
                                    select kb.belanja_name,kr.rekening_code,r.rekening_name,  k.satuan, k.komponen_harga,  k.komponen_tipe as tipe, k.komponen_non_pajak as pajak, k.komponen_id, k.komponen_name,k.status_masuk
                                    from " . sfConfig::get('app_default_schema') . ".komponen k, " . sfConfig::get('app_default_schema') . ".komponen_rekening kr, " . sfConfig::get('app_default_schema') . ".kelompok_belanja kb, " . sfConfig::get('app_default_schema') . ".rekening r "
                                        . "where k.komponen_name ilike '%" . $cari . "%' and k.komponen_id = kr.komponen_id and k.komponen_tipe <> 'HSPK' and
                                    kr.rekening_code = r.rekening_code and r.belanja_id = kb.belanja_id
                                    and kr.rekening_code in($rekening_dibuka_belanja_dikunci) and k.tahap<='$tahap' and k.komponen_id not ilike '%.F'
                                        )
                                        order by komponen_id, rekening_code ";
                                }
                                else
                                {
                                    $query = "(select kb.belanja_name,kr.rekening_code,r.rekening_name,  k.satuan, k.komponen_harga,  k.komponen_tipe as tipe, k.komponen_non_pajak as pajak, k.komponen_id, k.komponen_name,k.status_masuk from " . sfConfig::get('app_default_schema') . ".komponen k, " . sfConfig::get('app_default_schema') . ".komponen_rekening kr, " . sfConfig::get('app_default_schema') . ".kelompok_belanja kb, " . sfConfig::get('app_default_schema') . ".rekening r "
                                        . "where k.komponen_name ilike '%" . $cari . "%' and k.komponen_id = kr.komponen_id and k.komponen_tipe <> 'HSPK' and
                                    kr.rekening_code = r.rekening_code and r.belanja_id = kb.belanja_id
                                    and kr.rekening_code not in ($rekening_dikunci) "
                                        . "and k.komponen_id not in($komponen_dikunci) and k.tahap<='$tahap' and (k.komponen_id not ilike '%.F') "
                                        . " $belanja_dikunci".$jenis."
                                        ) union (
                                    select kb.belanja_name,kr.rekening_code,r.rekening_name,  k.satuan, k.komponen_harga,  k.komponen_tipe as tipe, k.komponen_non_pajak as pajak, k.komponen_id, k.komponen_name,k.status_masuk
                                    from " . sfConfig::get('app_default_schema') . ".komponen k, " . sfConfig::get('app_default_schema') . ".komponen_rekening kr, " . sfConfig::get('app_default_schema') . ".kelompok_belanja kb, " . sfConfig::get('app_default_schema') . ".rekening r "
                                        . "where k.komponen_name ilike '%" . $cari . "%' and k.komponen_id = kr.komponen_id and k.komponen_tipe <> 'HSPK' and
                                    kr.rekening_code = r.rekening_code and r.belanja_id = kb.belanja_id
                                    and k.komponen_id in($komponen_dibuka) and k.tahap<='$tahap' and (k.komponen_id not ilike '%.F')".$jenis."
                                        ) union (
                                    select kb.belanja_name,kr.rekening_code,r.rekening_name,  k.satuan, k.komponen_harga,  k.komponen_tipe as tipe, k.komponen_non_pajak as pajak, k.komponen_id, k.komponen_name,k.status_masuk
                                    from " . sfConfig::get('app_default_schema') . ".komponen k, " . sfConfig::get('app_default_schema') . ".komponen_rekening kr, " . sfConfig::get('app_default_schema') . ".kelompok_belanja kb, " . sfConfig::get('app_default_schema') . ".rekening r "
                                        . "where k.komponen_name ilike '%" . $cari . "%' and k.komponen_id = kr.komponen_id and k.komponen_tipe <> 'HSPK' and
                                    kr.rekening_code = r.rekening_code and r.belanja_id = kb.belanja_id
                                    and kr.rekening_code in($rekening_dibuka_belanja_dikunci) and k.tahap<='$tahap' and (k.komponen_id not ilike '%.F')".$jenis."
                                        )
                                        order by komponen_id, rekening_code ";
                                }
                                $con = Propel::getConnection();
                                $stmt = $con->prepareStatement($query);
                                $rs = $stmt->executeQuery();
                                $belanja = '';
                                $kode_rekening = '';
                                $shsd_name = '';
                                $keterangan = '';
                                $tipe = '';
                                $pertama = true;
                                $arr_rekening = array();
                                $arr_rekening[0] = 'Pilih rekening';
                                while ($rs->next())
                                {
                                    if ($rs->getString('status_masuk') == 'baru') {
                                        $status_masuk = ' ' . image_tag('new1.png', array('align' => 'absmiddle', 'alt' => 'Komponen Baru', 'width' => '20', 'height' => '20'));
                                    }
                                    if ($rs->getBoolean('pajak') == TRUE) {
                                        $pajak = 0;
                                    }
                                    if ($rs->getBoolean('pajak') == FALSE) {
                                        $pajak = 10;
                                    }
                                    $satuan = $rs->getString('satuan');
                                    $tipe = $rs->getString('tipe');
                                    $komponen_id = $rs->getString('komponen_id');
                                    if ((substr($rs->getString('rekening_code'), 0, 5) == '5.2.1')) {
                                        $warna_row = "5";
                                        $warna_button = 'btn btn-ouline-success btn-sm';
                                    } elseif ((substr($rs->getString('rekening_code'), 0, 5) == '5.2.2')) {
                                        $warna_row = "7";
                                        $warna_button = 'btn btn-ouline-danger btn-sm';
                                    } elseif ((substr($rs->getString('rekening_code'), 0, 5) == '5.2.3')) {
                                        $warna_row = "3";
                                        $warna_button = 'btn btn-ouline-info btn-sm';
                                    }
                                    if (($tipe == 'SAB') || ($tipe == 'FISIK') || ($tipe == 'OM') || ($tipe == 'HSPK')) {
                                        $shsd_name = $rs->getString('komponen_name');
                                        $keterangan = ' ' . link_to(image_tag('info.png', array('align' => 'absmiddle', 'alt' => 'view detail')), 'entri/lihatSAB?komponen_id=' . $komponen_id . '&coded=' . md5('cari'), array('popup' => array('entri/lihatSAB', 'width=1024px,height=600px,scrollbars')));
                                    } elseif (($tipe != 'SAB') && (substr($kode_rekening, 0, 8) != '5.2.1.01') && (substr($kode_rekening, 0, 8) != '5.2.1.02')) {
                                        $shsd_name = $rs->getString('komponen_name');
                                        $keterangan = ' ' . link_to(image_tag('info.png', array('align' => 'absmiddle', 'alt' => 'view detail')), 'entri/lihatSsh?komponen_id=' . $komponen_id . '&coded=' . md5('ssh'), array('popup' => array('entri/lihatSsh', 'width=1024px,height=600px,scrollbars')));
                                        $warna_t = '';
                                    } else {
                                        $shsd_name = $rs->getString('komponen_name');
                                        $keterangan = ' ' . link_to(image_tag('info.png', array('align' => 'absmiddle', 'alt' => 'view detail')), 'entri/lihatSsh?komponen_id=' . $komponen_id . '&coded=' . md5('ssh'), array('popup' => array('entri/lihatSsh', 'width=1024px,height=600px,scrollbars')));
                                    }
                                    $d = new Criteria();
                                    $d->add(KomponenAllPeer::KOMPONEN_ID, $komponen_id);
                                    $d->add(KomponenAllPeer::KOMPONEN_LOCKED, TRUE);
                                    $ds = KomponenAllPeer::doSelectOne($d);
                                    if ($ds) {
                                        $shsd_name = $ds->getKomponenName();
                                        $shsd_hidden_spec = $ds->getHiddenSpec();
                                        $shsd_spec = $ds->getSpec();
                                    }
                                    if ($komponen_sebelum != $rs->getString('komponen_id') && !$pertama) 
                                    {
                                    ?>
                                        <tr align='left' class="sf_admin_row_<?php echo $warna_row_sebelum ?>">
                                            <?php echo form_tag('entri/buatbaru', 'method=get id=form1 class=simpleForm') ?>
                                            <td><b><?php echo $kolom1 ?></b> </td>
                                            <td><b><?php echo $kolom2 ?></b></td>
                                            <td><b><?php echo $kolom3 ?></b> </td>
                                            <td><b><?php echo $kolom4 ?></b> </td>
                                            <td align='right'><b><?php echo $kolom5; ?></b></td>
                                            <td><b><?php echo $kolom6 ?></b> </td>
                                            <td><b><?php echo select_tag('rekening', $arr_rekening, array('class' => 'form-control')) ?></b></td>
                                            <td>
                                            <?php
                                                echo input_hidden_tag('cari', $cari_lama) .
                                                input_hidden_tag('kegiatan', $kode_kegiatan_lama) .
                                                input_hidden_tag('pajak', $pajak_lama) .
                                                input_hidden_tag('unit', $unit_id_lama) .
                                                input_hidden_tag('tipe', $tipe_lama) .
                                                input_hidden_tag('komponen', $komponen_id_lama) .
                                                input_hidden_tag('baru', md5('terbaru')) . '&nbsp;' .
                                                submit_tag('Pilih', Array('class' => $warna_button_sebelum));
                                            echo '</form>';
                                            ?>
                                            </td>
                                        </tr>
                                    <?php
                                        $arr_rekening = array();
                                        $arr_rekening[0] = 'Pilih rekening';
                                    }
                                    $pertama = false;
                                    $kolom1 = $shsd_name . $status_masuk . $keterangan;
                                    $kolom2 = $shsd_spec . ' ' . $shsd_hidden_spec;
                                    $kolom3 = '&nbsp;' . $satuan;
                                    if ($tipe == 'SHSD') {
                                        $tipe = 'SSH';
                                    }
                                    $kolom4 = '&nbsp;' . $tipe;
                                    if ($satuan == '%') {
                                        $kolom5 = '&nbsp;' . number_format($rs->getString('komponen_harga'), 4, ',', '.');
                                    }
                                    else
                                    {
                                        if ($rs->getString('komponen_harga') != floor($rs->getString('komponen_harga')))
                                        {
                                            $hrg_blt = number_format(round($rs->getString('komponen_harga')), 2, ',', '.');
                                        }
                                        else
                                        {
                                            $hrg_blt = number_format($rs->getString('komponen_harga'), 2, ',', '.');
                                        }
                                        //untuk harga
                                        $kolom5 = '&nbsp;' . $hrg_blt;
                                    }
                                    $kolom6 = '&nbsp;' . $pajak . '%';
                                    $cari_lama = $cari;
                                    $kode_kegiatan_lama = $kode_kegiatan;
                                    $pajak_lama = $pajak;
                                    $unit_id_lama = $unit_id;
                                    $tipe_lama = $tipe;
                                    $komponen_id_lama = $komponen_id;
                                    $arr_rekening[$rs->getString('rekening_code')] = $rs->getString('rekening_code') . ' - ' . $rs->getString('rekening_name');
                                    $komponen_sebelum = $rs->getString('komponen_id');
                                    $warna_row_sebelum = $warna_row;
                                    $warna_button_sebelum = $warna_button;
                                    $shsd_name = '';
                                    $status_masuk = '';
                                    $keterangan = '';
                                    $shsd_spec = '';
                                    $shsd_hidden_spec = '';
                                    $satuan = '';
                                    $tipe = '';
                                    $pajak = '';
                                }
                                if (!$pertama)
                                {
                                ?>
                                    <tr align='left' class="sf_admin_row_<?php echo $warna_row_sebelum ?>">
                                        <?php echo form_tag('entri/buatbaru', 'method=get id=form1 class=simpleForm') ?>
                                        <td><b><?php echo $kolom1 ?></b> </td>
                                        <td><b><?php echo $kolom2 ?></b></td>
                                        <td><b><?php echo $kolom3 ?></b> </td>
                                        <td><b><?php echo $kolom4 ?></b> </td>
                                        <td align='right'><b><?php echo $kolom5; ?></b></td>
                                        <td><b><?php echo $kolom6 ?></b> </td>
                                        <td><b><?php echo select_tag('rekening', $arr_rekening, array('class' => 'form-control')) ?></b></td>
                                        <td>
                                        <?php
                                            echo input_hidden_tag('cari', $cari_lama) .
                                            input_hidden_tag('kegiatan', $kode_kegiatan_lama) .
                                            input_hidden_tag('pajak', $pajak_lama) .
                                            input_hidden_tag('unit', $unit_id_lama) .
                                            input_hidden_tag('tipe', $tipe_lama) .
                                            input_hidden_tag('komponen', $komponen_id_lama) .
                                            input_hidden_tag('baru', md5('terbaru')) . '&nbsp;' .
                                            submit_tag('Pilih', Array('class' => $warna_button_sebelum));
                                        echo '</form>';
                                        ?>
                                        </td>
                                    </tr>
                                    <?php
                                }
                                $kata = '%' . $cari . '%';
                                $s = new Criteria();
                                $s->add(SubKegiatanPeer::SUB_KEGIATAN_NAME, strtr($kata, '*', '%'), Criteria::ILIKE);
                                $s->add(SubKegiatanPeer::STATUS, 'Close', Criteria::ILIKE);
                                $s->addAscendingOrderByColumn(SubKegiatanPeer::SUB_KEGIATAN_NAME);
                                $total_ada_asb_non_fisik = SubKegiatanPeer::doCount($s);
                                if ($total_ada_asb_non_fisik > 0) 
                                {
                                ?>
                                    <tr class="sf_admin_row_1" align='center'>
                                        <td colspan="8"><?php echo '<strong>ASB NON FISIK</strong>' ?></td>
                                    </tr>
                                    <?php
                                    $kata = '%' . $cari . '%';
                                    $s = new Criteria();
                                    $s->add(SubKegiatanPeer::SUB_KEGIATAN_NAME, strtr($kata, '*', '%'), Criteria::ILIKE);
                                    $s->add(SubKegiatanPeer::STATUS, 'Close', Criteria::ILIKE);
                                    $s->addAscendingOrderByColumn(SubKegiatanPeer::SUB_KEGIATAN_NAME);
                                    $ss = SubKegiatanPeer::doSelect($s);
                                    foreach ($ss as $r) 
                                    {
                                        echo form_tag('entri/pilihSubKegiatan', 'method=get id=form1 class=simpleForm');
                                        $keterangan = ' ' . link_to(image_tag('info.png', array('align' => 'absmiddle', 'alt' => 'view detail')), 'entri/lihatSAB?komponen_id=' . $r->getSubKegiatanId() . '&coded=' . md5('cari'), array('popup' => array('entri/lihatSAB', 'width=1024px,height=600px,scrollbars')));
                                        ?>
                                            <tr align='left' id='<?php echo $r->getSubKegiatanId() ?>' class="sf_admin_row_8">
                                                <td>
                                                    <font color="#000000">
                                                    <?php
                                                        echo $r->getSubKegiatanName() . '&nbsp;' . $keterangan;
                                                        echo input_hidden_tag('id', $r->getSubKegiatanId()) . input_hidden_tag('kode_kegiatan', $kode_kegiatan) . input_hidden_tag('unit_id', $unit_id);
                                                    ?>
                                                    </font>
                                                </td>
                                                <td colspan="2">
                                                    <font color="#000000">
                                                        <?php echo '&nbsp;' . $r->getSubKegiatanName() ?></font>
                                                </td>
                                                <td style="text-align: center">
                                                    <font color="#000000"><?php echo '&nbsp;' . 'ASB NON FISIK' ?>
                                                    </font>
                                                </td>
                                                <?php
                                                $sql = "select sum(komponen_harga_awal*volume*(pajak+100)/100) as total from " . sfConfig::get('app_default_schema') . ".sub_kegiatan_member where sub_kegiatan_id = '" . $r->getSubKegiatanId() . "'";
                                                $con = Propel::getConnection();
                                                $stmt = $con->prepareStatement($sql);
                                                $rs = $stmt->executeQuery();
                                                while ($rs->next()) {
                                                    $harga = $rs->getString('total');
                                                }
                                                ?>
                                                <td align='right'>
                                                    <font color="#000000"><?php echo '&nbsp;' . number_format($harga, 0, ',', '.'); ?></font>
                                                </td>
                                                <td colspan="2">&nbsp;</td>
                                                <td><?php echo submit_tag('Pilih', Array('class' => 'btn btn-default', 'name' => 'sub')); ?></td>
                                            </tr>
                                        <?php
                                        echo '</form>';
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section><!-- /.content -->