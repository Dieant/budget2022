<?php use_helper('I18N', 'Date', 'Url', 'Javascript', 'Form', 'Object', 'Number', 'Validation') ?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>PEDOMAN PENYUSUNAN RKA <?php echo sfConfig::get('app_tahun_default'); ?></h1>
            </div>
        </div>
    </div>
</section>
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <ul class="list-group">
                                <li class="list-group-item">
                                    <span class="badge pull-left">1</span> 
                                    <span class="pull-left" style="text-align:justify">&nbsp;Entry anggaran <b><u>wajib memperhatikan</u></b> kesesuaian antara rincian belanja dengan target output kegiatan/sub kegiatan yang ditetapkan.</span>
                                    <div class="clearfix"></div>
                                </li>
                                <li class="list-group-item">
                                    <span class="badge pull-left">2</span> 
                                    <span class="pull-left" style="text-align:justify">&nbsp;Perangkat Daerah untuk memperhatikan prinsip efektivitas dan efisiensi sehingga tidak terjadi double alokasi anggaran</span>
                                    <div class="clearfix"></div>
                                </li>
                                <div class="clearfix"></div>
                                <li class="list-group-item">
                                    <span class="badge pull-left">3</span> 
                                    <span class="pull-left" style="text-align:justify">&nbsp;Perangkat Daerah segera mengusulkan SSH apabila belum difasilitasi dengan melampirkan data pendukung minimal dari 3 (tiga) sumber</span>
                                    <div class="clearfix"></div>
                                </li>
                                <div class="clearfix"></div>
                                <li class="list-group-item">
                                    <span class="badge pull-left">4</span>
                                    <span class="pull-left" style="text-align:justify">&nbsp;Perangkat Daerah segera mengusulkan ASB / Estimasi Harga apabila belum difasilitasi dengan melampirkan data pendukung berupa  <i>Engineering Estimate (EE)</i></span>
                                    <div class="clearfix"></div>
                                </li>
                                <div class="clearfix"></div>
                                <li class="list-group-item">
                                    <span class="badge pull-left">5</span>
                                    <span class="pull-left" style="text-align:justify">&nbsp;Pemberian Honorarium Pegawai Negeri Sipil pada Belanja Langsung <strong><u>terbatas</u></strong> sesuai Peraturan Walikota Nomor 13 Tahun 2019 tentang Petunjuk Teknis Pemberian Uang Kinerja pada Belanja Langsung</span>
                                    <div class="clearfix"></div>
                                </li>
                                <div class="clearfix"></div>
                                <li class="list-group-item">
                                    <span class="badge pull-left">6</span>
                                    <span class="pull-left" style="text-align:justify">&nbsp;Honor Tim Pengadaan Barang/Jasa untuk pekerjaan dengan metode Pengadaan Langsung (Pejabat Pengadaan, Pejabat Pemeriksa Hasil Pekerjaan/PjPHP dan Tim Teknis) diberikan <u>per orang</u> untuk 5 paket pekerjaan pertama dan selanjutnya diberikan sebanyak 1 paket untuk setiap kelipatan 5 paket pekerjaan.</span><ul style="list-style:none;"><li> a. Pengalokasian anggaran untuk kebutuhan belanja pakai habis (Alat Tulis Kantor; Perlengkapan Komputer dan Printer; Perlengkapan Mesin, Angkutan, dan Alat Berat; dll) disesuaikan dengan kebutuhan riil yang didasarkan atas pelaksanaan tugas dan fungsi Perangkat Daerah, jumlah pegawai dan volume pekerjaan serta memperhitungkan estimasi <b>sisa persediaan barang yang terdapat pada e-Inventory</b></li></ul></span>
                                    <div class="clearfix"></div>
                                </li>
                                <div class="clearfix"></div>
                                <li class="list-group-item">
                                    <span class="badge pull-left">7</span> 
                                    <span class="pull-left" style="text-align:justify">&nbsp;Pembatasan alokasi anggaran untuk Belanja Bahan Bakar Kendaraan dengan hari kerja yang disesuaikan pada Peraturan Walikota No. 21 Tahun 2006 tentang Hari dan Jam Kerja bagi Instansi di Lingkungan Pemerintah Kota Surabaya: <ul><li>Roda 4 <strong>maksimal</strong> 8 liter BBM per hari atau 8 spl (satuan per liter) BBG per hari kerja</li><li>Roda 2 atau Roda 3 <strong>maksimal</strong> 2 liter BBM per hari kerja</li><li>Operasional angkutan/kendaraan patroli <strong>maksimal</strong> 15 liter BBM per hari kerja</li></ul></span>
                                    <div class="clearfix"></div>
                                </li>
                                <div class="clearfix"></div>
                                <li class="list-group-item">
                                    <span class="badge pull-left">8</span>
                                    <span class="pull-left" style="text-align:justify">&nbsp;Penganggaran Transport Lokal dapat diberikan untuk masyarakat yang mengikuti kegiatan Workshop/Pelatihan/Sosialisasi/Musrenbang/Pembinaan.</span>
                                    <div class="clearfix"></div>
                                </li>
                                <div class="clearfix"></div>
                                <li class="list-group-item">
                                    <span class="badge pull-left">9</span>
                                    <span class="pull-left" style="text-align:justify">&nbsp;Penyelenggaraan kegiatan Workshop/Sosialisasi/Musrenbang/Pembinaan/Rapat/Diklat/Bimtek atau sejenisnya <strong>diprioritaskan</strong> untuk menggunakan fasilitas aset daerah/gedung milik Pemerintah Kota.</span>
                                    <div class="clearfix"></div>
                                </li>
                                <div class="clearfix"></div>
                                <li class="list-group-item">
                                    <span class="badge pull-left">10</span>
                                    <span class="pull-left" style="text-align:justify">&nbsp;Anggaran Belanja Perjalanan Dinas Dalam Daerah, Perjalanan Dinas Luar Daerah dan Perjalanan Dinas Luar Negeri bagi PNS dan Non PNS akan dipindahkan penganggarannya pada Sekretariat DPRD, Bagian Umum dan Protokol serta Bagian Administrasi Kerjasama.</span>
                                    <div class="clearfix"></div>
                                </li>
                                <div class="clearfix"></div>
                                <li class="list-group-item">
                                    <span class="badge pull-left">11</span>
                                    <span class="pull-left" style="text-align:justify">&nbsp;Pembayaran Jaminan Sosial (Jaminan Kesehatan dan Jaminan Ketenagakerjaan) bagi Pegawai Pemerintah Non Pegawai Negeri dan Tenaga Harian Lepas dianggarkan dalam Belanja Langsung.</span>
                                    <div class="clearfix"></div>
                                </li>
                                <li class="list-group-item">
                                    <span class="badge pull-left">12</span>
                                    <span class="pull-left" style="text-align:justify">&nbsp;Pekerjaan rehabilitasi dan pembangunan fisik struktur; pengecatan bangunan seluruh gedung pemerintah; pembelian bahan bangunan (pasir, batu, semen, besi, dll) untuk gedung pemerintah; pemeliharaan lift dan eskalator dilaksanakan melalui Dinas Perumahan Rakyat dan Kawasan Permukiman, Cipta Karya dan Tata Ruang kecuali bangunan tertentu (Park and Ride, Terminal, Rumah Kompos, Rumah Pompa, LPS, Intermoda Transportasi, Sumur Kebakaran, dll).</span>
                                    <div class="clearfix"></div>
                                </li>
                                <div class="clearfix"></div>
                                <li class="list-group-item">
                                    <span class="badge pull-left">13</span>
                                    <span class="pull-left" style="text-align:justify">&nbsp;Pekerjaan pemeliharaan non struktur; pembelian perlengkapan bangunan (contoh : handle pintu, grendel pintu, kran air, dll) dilaksanakan oleh masing-masing Perangkat Daerah kecuali pada Kecamatan dan Kelurahan.</span>
                                    <div class="clearfix"></div>
                                </li>
                                <div class="clearfix"></div>
                                <li class="list-group-item">
                                    <span class="badge pull-left">14</span>
                                    <span class="pull-left" style="text-align:justify">&nbsp;Usulan anggaran pekerjaan konstruksi, wajib dilengkapi dengan dokumen perencanaan pekerjaan/DED.</span>
                                    <div class="clearfix"></div>
                                </li>
                                <div class="clearfix"></div>
                                <li class="list-group-item">
                                    <span class="badge pull-left">15</span>
                                    <span class="pull-left" style="text-align:justify">&nbsp;Hal – hal yang tidak diatur dalam Pedoman Umum tersebut diatas wajib dikonsultasikan dengan TAPD.</span>
                                    <div class="clearfix"></div>
                                </li>                 
                            </ul>
                        </div>
                    </div>
                    <div class="card-footer" style="text-align: right">
                        <?php echo form_tag("entri/eula?act=simpan&unit_id=$unit_id"); ?>
                        <?php echo submit_tag('Setuju', array('align' => 'center', 'id' => 'eula', 'name' => 'eula', 'class' => 'btn btn-outline-success')) . '  ' . submit_tag('Tidak Setuju', array('align' => 'center', 'id' => 'eula_tolak', 'name' => 'eula_tolak', 'class' => 'btn btn-outline-danger')) ?>
                        <?php echo '</form>'; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
