<?php use_helper('I18N', 'Date', 'Url', 'Javascript', 'Form', 'Object') ?>

<section class="content-header">
    <h1>List Lokasi</h1>
</section>

<!-- Main content -->
<section class="content">
    <?php include_partial('entri/list_messages'); ?>
    <!-- Default box -->    
    <div class="box box-primary box-solid">
        <div class="box-header with-border">
            <h3 class="box-title">Filters</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body">
            <?php echo form_tag('entri/lokasiList', array('method' => 'get', 'class' => 'form-horizontal')) ?>
            <div class="form-group">
                <label class="col-sm-2 control-label">Jalan</label>
                <div class="col-sm-10">
                    <?php
                    echo input_tag('filters[jalan]', (isset($filters['jalan']) ? $filters['jalan'] : null), array('class' => 'form-control'));
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Lokasi</label>
                <div class="col-sm-10">
                    <?php
                    echo input_tag('filters[lokasi]', (isset($filters['lokasi']) ? $filters['lokasi'] : null), array('class' => 'form-control'));
                    ?>
                </div>
            </div>

            <div id="sf_admin_container">
                <ul class="sf_admin_actions">
                    <li><?php echo link_to(button_to(__('tambah lokasi'), '#', 'class=sf_admin_action_new_user'), 'entri/lokasiBaru', 'target=_blank') ?></li>
                    <li><?php echo button_to(__('reset'), 'entri/lokasiList?filter=filter', 'class=sf_admin_action_reset_filter') ?></li>
                    <li><?php echo submit_tag(__('cari'), 'name=filter class=sf_admin_action_filter') ?></li>
                </ul>
            </div>

            <?php echo '</form>'; ?>
        </div>
    </div>
    <div class="box box-primary box-solid">
        <div class="box-body">
            <?php if (!$pager->getNbResults()): ?>
                <?php echo __('no result') ?>
            <?php else: ?>
                <div id="sf_admin_container" class="table-responsive">
                    <table cellspacing="0" class="sf_admin_list">    
                        <thead>
                            <tr>
                                <th colspan="13">
                                    <div class="float-right">
                                        <?php if ($pager->haveToPaginate()): ?>
                                            <?php echo link_to(image_tag(sfConfig::get('sf_admin_web_dir') . '/images/first.png', array('align' => 'absmiddle', 'alt' => __('First'), 'title' => __('First'))), 'entri/lokasiList?page=1'); ?>
                                            <?php echo link_to(image_tag(sfConfig::get('sf_admin_web_dir') . '/images/previous.png', array('align' => 'absmiddle', 'alt' => __('Previous'), 'title' => __('Previous'))), 'entri/lokasiList?page=' . $pager->getPreviousPage()); ?>

                                            <?php foreach ($pager->getLinks() as $page): ?>
                                                <?php echo link_to_unless($page == $pager->getPage(), $page, 'entri/lokasiList?page={$page}'); ?>
                                            <?php endforeach; ?>

                                            <?php echo link_to(image_tag(sfConfig::get('sf_admin_web_dir') . '/images/next.png', array('align' => 'absmiddle', 'alt' => __('Next'), 'title' => __('Next'))), 'entri/lokasiList?page=' . $pager->getNextPage()); ?>
                                            <?php echo link_to(image_tag(sfConfig::get('sf_admin_web_dir') . '/images/last.png', array('align' => 'absmiddle', 'alt' => __('Last'), 'title' => __('Last'))), 'entri/lokasiList?page=' . $pager->getLastPage()); ?>
                                        <?php endif; ?>
                                    </div>
                                    <?php echo format_number_choice('[0] no result|[1] 1 result|(1,+Inf] %1% results', array('%1%' => $pager->getNbResults()), $pager->getNbResults()) ?>
                                </th>
                            </tr>
                            <tr class="sf_admin_row_3">
                                <td colspan="13"></td>
                            </tr>
                            <tr>
                                <th>Nama Jalan</th>
                                <th>Gang</th>
                                <th>Nomor</th>
                                <th>RT</th>
                                <th>RW</th>
                                <th>Keterangan</th>
                                <th>Tempat</th>
                                <th>Kecamatan</th>
                                <th>Kelurahan</th>
                                <th>Lokasi</th>
                                <th>Usulan</th>
                                <th>Status</th>
                                <th>HAPUS</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i = 1;
                            foreach ($pager->getResults() as $data):
                                if ($data->getStatusVerifikasi()) {
                                    $c_cek_dipakai = new Criteria();
                                    $c_cek_dipakai->add(DinasRincianDetailPeer::ID_LOKASI, '%|' . $data->getIdLokasi() . '|%', Criteria::LIKE);
                                    if ($rs_dipakai = DinasRincianDetailPeer::doSelect($c_cek_dipakai)) {
                                        ?>
                                    <div class="modal fade" id="myModal<?php echo $data->getIdLokasi() ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                    <h4 class="modal-title" id="myModalLabel">Komponen yang telah menggunakan</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <?php
                                                    foreach ($rs_dipakai as $value) {
                                                        $c_skpd = new Criteria();
                                                        $c_skpd->add(UnitKerjaPeer::UNIT_ID, $value->getUnitId());
                                                        if ($rs_skpd = UnitKerjaPeer::doSelectOne($c_skpd)) {
                                                            $nama_skpd = $rs_skpd->getUnitName();
                                                        }
                                                        echo '<strong>' . $nama_skpd . ' - ' . $value->getKegiatanCode() . ':</strong> ' . $value->getKomponenName() . ' ' . $value->getDetailName() . '<br>';
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                    $status_verifikasi = '<button type="button" class="btn btn-link btn-xs" data-toggle="modal" data-target="#myModal' . $data->getIdLokasi() . '">Digunakan</button>';
                                } else {
                                    $status_verifikasi = '<span class="badge bg-green">MASUK</span>';
                                }
                            } else {
                                $status_verifikasi = '<span class="badge bg-red"><b>X</b></span>';
                            }

                            $c = new Criteria();
                            $c->add(UnitKerjaPeer::UNIT_ID, $data->getUsulanSkpd());
                            $rs_skpd = UnitKerjaPeer::doSelectOne($c);
                            $skpd = $rs_skpd->getUnitName();
                            ?>
                            <tr class="sf_admin_row_<?php echo $odd ?>">
                                <td><?php echo $data->getJalan() ?></td>
                                <td><?php echo $data->getGang() ?></td>
                                <td><?php echo $data->getNomor() ?></td>
                                <td><?php echo $data->getRt() ?></td>
                                <td><?php echo $data->getRw() ?></td>
                                <td><?php echo $data->getKeterangan() ?></td>
                                <td><?php echo $data->getTempat() ?></td>
                                <td><?php echo $data->getKecamatan() ?></td>
                                <td><?php echo $data->getKelurahan() ?></td>
                                <td><?php echo $data->getLokasi() ?></td>
                                <td><?php echo $skpd ?></td>
                                <td class="text-center"><?php echo $status_verifikasi ?></td>
                                <td class="text-center">
                                    <?php
                                    if ($data->getUsulanSkpd() == $unit_id) {
                                        echo link_to(image_tag('/images/cancel.png', array('alt' => __('Delete Lokasi'), 'title' => __('Delete Lokasi'), 'class' => 'btn btn-sm', 'height' => '26px')), 'entri/lokasiHapus?id_lokasi=' . $data->getIdLokasi(), array('confirm' => 'Yakin untuk menghapus ' . $data->getLokasi() . '?'));
                                    }
                                    ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th colspan="13">
                                    <div class="float-right">
                                        <?php if ($pager->haveToPaginate()): ?>
                                            <?php echo link_to(image_tag(sfConfig::get('sf_admin_web_dir') . '/images/first.png', array('align' => 'absmiddle', 'alt' => __('First'), 'title' => __('First'))), 'entri/lokasiList?page=1') ?>
                                            <?php echo link_to(image_tag(sfConfig::get('sf_admin_web_dir') . '/images/previous.png', array('align' => 'absmiddle', 'alt' => __('Previous'), 'title' => __('Previous'))), 'entri/lokasiList?page=' . $pager->getPreviousPage()) ?>

                                            <?php foreach ($pager->getLinks() as $page): ?>
                                                <?php echo link_to_unless($page == $pager->getPage(), $page, 'entri/lokasiList?page=' . $page) ?>
                                            <?php endforeach; ?>

                                            <?php echo link_to(image_tag(sfConfig::get('sf_admin_web_dir') . '/images/next.png', array('align' => 'absmiddle', 'alt' => __('Next'), 'title' => __('Next'))), 'entri/lokasiList?page=' . $pager->getNextPage()) ?>
                                            <?php echo link_to(image_tag(sfConfig::get('sf_admin_web_dir') . '/images/last.png', array('align' => 'absmiddle', 'alt' => __('Last'), 'title' => __('Last'))), 'entri/lokasiList?page=' . $pager->getLastPage()) ?>
                                        <?php endif; ?>
                                    </div>
                                    <?php echo format_number_choice('[0] no result|[1] 1 result|(1,+Inf] %1% results', array('%1%' => $pager->getNbResults()), $pager->getNbResults()) ?>
                                </th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            <?php endif; ?>
        </div>
    </div>
</section>