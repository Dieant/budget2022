<?php use_helper('I18N', 'Date', 'Url', 'Javascript', 'Form', 'Object') ?>

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>Daftar Komponen Kegiatan SKPD <?php echo $kode_kegiatan; ?> Di Devplan</h1>
</section>

<!-- Main content -->
<section class="content">
    <?php include_partial('entri/list_messages'); ?>
    <!-- Default box -->

    <?php if (sfConfig::get('app_fasilitas_warningRekening') == 'buka') { ?>
        <?php
        echo "<div class='box box-primary box-solid'>";
        echo "<div class='box-body' style='color:#FF0000'>";
        $query_warning_per_rekening = "( 
                        select semula.rekening_code, semula.total as prev_total,  rd.total as exist_total  
                        from 
                        ( 
                        select rekening_code, sum(nilai_anggaran) as total 
                        from " . sfConfig::get('app_default_schema') . ".rincian_detail "
                . "where status_hapus = false and unit_id = '$unit_id' and kegiatan_code ilike '$kode_kegiatan' 
                                group by rekening_code 
                                order by rekening_code 
                                ) semula, 
                                ( 
                                select rekening_code, sum(nilai_anggaran) as total 
                                from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail  "
                . "where status_hapus = false and unit_id = '$unit_id' and kegiatan_code ilike '$kode_kegiatan' 
                                group by rekening_code 
                                order by rekening_code 
                                ) rd  
                                where semula.rekening_code = rd.rekening_code and semula.total <> rd.total 
                                order by semula.rekening_code 
                                ) union all ( 
                                select semula.rekening_code, semula.total as prev_total,  0 as exist_total 
                                from 
                                ( 
                                select rekening_code, sum(nilai_anggaran) as total 
                                from " . sfConfig::get('app_default_schema') . ".rincian_detail  "
                . "where status_hapus = false and unit_id = '$unit_id' and kegiatan_code ilike '$kode_kegiatan' 
                                group by rekening_code 
                                order by rekening_code 
                                ) semula 
                                where semula.rekening_code not in 
                                ( select rekening_code  
                                from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail  "
                . "where status_hapus = false and unit_id = '$unit_id' and kegiatan_code ilike '$kode_kegiatan' 
                                group by rekening_code 
                                order by rekening_code ) 
                                ) union all ( 
                                select rd.rekening_code, 0 as prev_total,  rd.total as exist_total 
                                from 
                                ( 
                                select rekening_code, sum(nilai_anggaran) as total 
                                from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail  "
                . "where status_hapus = false and unit_id = '$unit_id' and kegiatan_code ilike '$kode_kegiatan' 
                                group by rekening_code 
                                order by rekening_code 
                                ) rd where rd.rekening_code not in 
                                ( select rekening_code  
                                from " . sfConfig::get('app_default_schema') . ".rincian_detail  "
                . "where status_hapus = false and unit_id = '$unit_id' and kegiatan_code ilike '$kode_kegiatan' 
                                group by rekening_code 
                                order by rekening_code ) 
                                )";
        $munculNotif = 0;
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query_warning_per_rekening);
        $t = $stmt->executeQuery();
        while ($t->next()) {
            $rek_lama = $t->getString('rekening_code');
            $tot_lama = number_format($t->getString('prev_total'), 0, ',', '.');
            $tot_baru = number_format($t->getString('exist_total'), 0, ',', '.');
            $beda = $t->getString('prev_total') - $t->getString('exist_total');
            $selisih = number_format($beda, 0, ',', '.');
            if ($selisih <> 0) {
                if ($munculNotif == 0) {
                    echo "Terdapat perubahan total nilai per rekening dari RKA : <br>";
                }
                $munculNotif = 1;
            }
            echo "Kode Rekening : $rek_lama   Selisih=$selisih [ Semula=$tot_lama, Menjadi=$tot_baru ] <br>";
        }
        echo "</div>";
        echo "</div>";
        ?>

        <?php
        $con = Propel::getConnection();
        $query = "SELECT kb.belanja_name, sum(d2.nilai_anggaran) AS nilai_draft, 
                    (SELECT sum(rd.nilai_anggaran) FROM " . sfConfig::get('app_default_schema') . ".dinas_master_kegiatan k1, " . sfConfig::get('app_default_schema') . ".dinas_rincian r1, unit_kerja u1, " . sfConfig::get('app_default_schema') . ".rekening rek1, " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail rd, " . sfConfig::get('app_default_schema') . ".kelompok_belanja kb1 
                        WHERE k1.kode_kegiatan = r1.kegiatan_code AND k1.unit_id = u1.unit_id AND k1.unit_id = r1.unit_id AND rd.unit_id = r1.unit_id AND rd.kegiatan_code = r1.kegiatan_code AND rd.rekening_code = rek1.rekening_code AND rek1.belanja_id = kb1.belanja_id AND rd.status_hapus = false AND rd.unit_id = k.unit_id AND rd.kegiatan_code = k.kode_kegiatan AND kb1.belanja_id = kb.belanja_id GROUP BY k1.unit_id, u1.unit_name, u1.kelompok_id, k1.kode_kegiatan, k1.tahap, k1.nama_kegiatan, kb1.belanja_id, kb1.belanja_name, kb1.belanja_urutan
                    ) AS nilai_prev
            FROM " . sfConfig::get('app_default_schema') . ".dinas_master_kegiatan k, " . sfConfig::get('app_default_schema') . ".dinas_rincian r, unit_kerja u, " . sfConfig::get('app_default_schema') . ".rekening rek, " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail d2, " . sfConfig::get('app_default_schema') . ".kelompok_belanja kb
            WHERE k.unit_id = '" . $unit_id . "' AND k.kode_kegiatan = '" . $kode_kegiatan . "' AND k.kode_kegiatan = r.kegiatan_code AND k.unit_id = u.unit_id AND k.unit_id = r.unit_id AND d2.unit_id = r.unit_id AND d2.kegiatan_code = r.kegiatan_code AND d2.rekening_code = rek.rekening_code AND rek.belanja_id = kb.belanja_id AND d2.status_hapus = false
            GROUP BY k.unit_id, u.unit_name, u.kelompok_id, k.kode_kegiatan, k.tahap, k.nama_kegiatan, kb.belanja_id, kb.belanja_name, kb.belanja_urutan";
        $stmt = $con->prepareStatement($query);
        $rs = $stmt->executeQuery();
        $hasil = '';
        while ($rs->next()) {
            $selisih_belanja = $rs->getInt('nilai_prev') - $rs->getInt('nilai_draft');
            if ($selisih_belanja != 0)
                $hasil .= $rs->getString('belanja_name') . " => Selisih=" . number_format($selisih_belanja, 0, ',', '.') . " [Semula=" . number_format($rs->getInt('nilai_prev'), 0, ',', '.') . ", Menjadi=" . number_format($rs->getInt('nilai_draft'), 0, ',', '.') . "]<br>";
        }
        if ($hasil != '') {
            ?>
            <div class='alert alert-danger alert-dismissable'>
                Terdapat perubahan total nilai per belanja dari prev : <br>
                <?php echo $hasil; ?>
            </div>
            <?php
        }
        ?>

        <?php
        $con = Propel::getConnection();
        $query =
        "SELECT kb.belanja_name,
        SUM(CASE WHEN k.komponen_non_pajak = FALSE THEN (drd.komponen_harga * 1.1 * drd.volume_semula) - drd.nilai_anggaran ELSE (drd.komponen_harga * drd.volume_semula) - drd.nilai_anggaran END) AS sisa
        FROM ".sfConfig::get('app_default_schema').".dinas_rincian_detail drd
        INNER JOIN ".sfConfig::get('app_default_schema').".komponen k ON drd.komponen_id = k.komponen_id
        INNER JOIN ".sfConfig::get('app_default_schema').".rekening r ON drd.rekening_code = r.rekening_code
        INNER JOIN ".sfConfig::get('app_default_schema').".kelompok_belanja kb ON r.belanja_id = kb.belanja_id
        WHERE drd.unit_id  = '$unit_id' AND drd.kegiatan_code = '$kode_kegiatan' AND drd.status_hapus = FALSE AND drd.satuan = 'Paket' AND (drd.komponen_harga * (drd.pajak / 100 + 1) * drd.volume_semula) > drd.nilai_anggaran
        GROUP BY kb.belanja_id, kb.belanja_name";
        $stmt = $con->prepareStatement($query);
        $rs = $stmt->executeQuery();
        $hasil = '';
        while ($rs->next()) {
            $sisa = $rs->getFloat('sisa');
            if ($sisa != 0)
                $hasil .= $rs->getString('belanja_name') . " => Sisa Pengadaan=" . number_format($sisa, 0, ',', '.') . "<br>";
        }
        if ($hasil != '') {
            ?>
            <div class='alert alert-info alert-dismissable'>
                Terdapat nilai sisa pengadaan per belanja sebagai berikut : <br>
                <?php echo $hasil; ?>
            </div>
            <?php
        }
        ?>

        <?php
        $cone = Propel::getConnection();
        //$pagu = "SELECT pagu FROM " . sfConfig::get('app_default_schema') . ".pagu_pak where unit_id = '$unit_id' and kegiatan_code ilike '$kode_kegiatan'";
        $pagu = "SELECT alokasi_dana as pagu FROM " . sfConfig::get('app_default_schema') . ".dinas_master_kegiatan where unit_id = '$unit_id' and kode_kegiatan ilike '$kode_kegiatan'";
        $st = $cone->prepareStatement($pagu);
        $pg = $st->executeQuery();
        while ($pg->next()) {
            $paguk = $pg->getFloat('pagu');
        }

        $nila = "select SUM(nilai_anggaran) as nilai
		from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail
		where kegiatan_code ='$kode_kegiatan' and unit_id ='$unit_id' and status_hapus=FALSE";

        $con1 = Propel::getConnection();
        $stmt1 = $con1->prepareStatement($nila);
        $nl = $stmt1->executeQuery();
        while ($nl->next()) {
            $nilaia = $nl->getFloat('nilai');
        }

        $tambahan = $rs_kegiatan->getTambahanPagu();
        $hasil2 = $nilaia - ($paguk + $tambahan);
        ?>
        <div class='alert alert-warning alert-dismissable'>
            Selisih PAGU + Penyesuaian dengan Nilai Anggaran  : <br>
            <?php
            echo "Semula : " . number_format($paguk, 0, ',', '.') . ", Penyesuaian : " . number_format($tambahan, 0, ',', '.') . ", Menjadi : " . number_format($nilaia, 0, ',', '.') . " Selisih  = " . number_format($hasil2, 0, ',', '.') . "  ";

            if ($hasil2 > 0) {
                echo " ( Melebihi Pagu ! )";
            }
            ?>
        </div>
        <?php
    }
    ?>

    <?php echo form_tag('entri/prosesTarikDevplan'); ?>
    <div class="box box-primary box-solid">
        <div class="box-body">
            <div id="sf_admin_container">
                <div id="sf_admin_content" class="table-responsive">
                    <table cellspacing="0" class="sf_admin_list">
                        <thead>
                            <tr>
                                <th colspan="6"><b>DEVPLAN</b></th>
                                <th><b>|</b></th>
                                <th colspan="6"><b>E-BUDGETING</b></th>
                                <th rowspan="2"><b>Rekening</b></th>
                            </tr>
                            <tr>
                                <th><b>Nama</b></th>
                                <th><b>Volume</b></th>
                                <th><b>Satuan</b></th>
                                <th><b>Pajak</b></th>
                                <th><b>Harga</b></th>
                                <th><b>Total</b></th>
                                <th><b>|</b></th>
                                <th><b>Nama</b></th>
                                <th><b>Volume</b></th>
                                <th><b>Satuan</b></th>
                                <th><b>Pajak</b></th>
                                <th><b>Harga</b></th>
                                <th><b>Total</b></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $counter = 0;
                            $total_devplan = 0;
                            $total_budgeting = 0;
                            while($rs_rinciandetail->next()) {
                                if(isset($komponen_terambil[$rs_rinciandetail->getString('komponen_id')]) && isset($komponen_terambil[$rs_rinciandetail->getString('subtitle')]))
                                    continue;

                                $counter++;
                                $nama = $rs_rinciandetail->getString('komponen_name_budgeting');
                                if(empty($nama)) {
                                    $style = "style='color: red;'";
                                    $check = "";
                                    $rek = true;
                                } else {
                                    $style = '';
                                    $check = "name='pilihAction[]'";
                                    $rek = false;
                                }
                                echo "<tr $style>";

                                echo "<td>".$rs_rinciandetail->getString('komponen_name')."</td>";
                                echo "<td>".$rs_rinciandetail->getString('volume')."</td>";
                                echo "<td>".$rs_rinciandetail->getString('satuan')."</td>";
                                echo "<td>".$rs_rinciandetail->getString('pajak')."</td>";
                                echo "<td>";
                                if ($rs_rinciandetail->getString('satuan') == '%') {
                                    $len = strlen(substr(strrchr($rs_rinciandetail->getFloat('komponen_harga_awal'), "."), 1));
                                    echo number_format($rs_rinciandetail->getFloat('komponen_harga_awal'), $len, ',', '.');
                                } else {
                                    // khusus komponen Biaya Rekening Listrik, dll
                                    $sub_id = substr($rs_rinciandetail->getString('komponen_id'), 0, 11);
                                    $arr_komp = array('23.02.02.03.03.B');
                                    $arr_name = array('Tenaga Operasional','Tenaga Administrasi 3','Tenaga Programmer 3','Tenaga Administrasi 3','Petugas Keamanan','Tenaga Operator 3','Tenaga Satgas','Pembantu Paramedis');
                                    if(
                                        in_array($rs_rinciandetail->getString('komponen_name'), $arr_name) ||
                                        in_array($rs_rinciandetail->getString('komponen_id'), $arr_komp) ||
                                        // all komponen honorer
                                        $sub_id == '23.01.01.08'
                                    ) {
                                        echo number_format($rs_rinciandetail->getFloat('komponen_harga_awal'), 2, ',', '.');
                                    } else {
                                        echo number_format($rs_rinciandetail->getFloat('komponen_harga_awal'), 0, ',', '.');
                                    }
                                }
                                echo "</td>";
                                echo "<td>".number_format(round($rs_rinciandetail->getString('nilai_anggaran')), 0, ',', '.')."</td>";

                                echo "<td>|</td>";

                                echo "<td>".$rs_rinciandetail->getString('komponen_name_budgeting')."</td>";
                                echo "<td>".$rs_rinciandetail->getString('volume')."</td>";
                                echo "<td>".$rs_rinciandetail->getString('satuan_budgeting')."</td>";
                                echo "<td>".$rs_rinciandetail->getString('pajak_budgeting')."</td>";
                                echo "<td>";
                                if ($rs_rinciandetail->getString('satuan_budgeting') == '%') {
                                    $len = strlen(substr(strrchr($rs_rinciandetail->getFloat('komponen_harga_awal_budgeting'), "."), 1));
                                    echo number_format($rs_rinciandetail->getFloat('komponen_harga_awal_budgeting'), $len, ',', '.');
                                } else {
                                    // khusus komponen Biaya Rekening Listrik, dll
                                    $sub_id = substr($rs_rinciandetail->getString('komponen_id'), 0, 11);
                                    $arr_komp = array('23.02.02.03.03.B');
                                    $arr_name = array('Tenaga Operasional','Tenaga Administrasi 3','Tenaga Programmer 3','Tenaga Administrasi 3','Petugas Keamanan','Tenaga Operator 3','Tenaga Satgas','Pembantu Paramedis');
                                    if(
                                        in_array($rs_rinciandetail->getString('komponen_name_budgeting'), $arr_name) ||
                                        in_array($rs_rinciandetail->getString('komponen_id'), $arr_komp) ||
                                        // all komponen honorer
                                        $sub_id == '23.01.01.08'
                                    ) {
                                        echo number_format($rs_rinciandetail->getFloat('komponen_harga_awal_budgeting'), 2, ',', '.');
                                    } else {
                                        echo number_format($rs_rinciandetail->getFloat('komponen_harga_awal_budgeting'), 0, ',', '.');
                                    }
                                }
                                echo "</td>";
                                echo "<td>".number_format(round($rs_rinciandetail->getString('nilai_anggaran_budgeting')), 0, ',', '.')."</td>";

                                // $arr_rekening[0] = 'Pilih rekening';
                                // $rekenings = explode('/', $rs_rinciandetail->getString('rekening_code'));
                                // foreach ($rekenings as $key => $rekening) {
                                //     if(!empty($rekening))
                                //         $arr_rekening[$rekening] = $rekening;
                                // }

                                echo "<td align='left'>".select_tag('rekening_'.$rs_rinciandetail->getString('detail_no'), $arr_rekening[$rs_rinciandetail->getString('komponen_id')], array('style' => 'color:black', 'disabled' => $rek))."</td>";

                                echo "<input type='hidden' $check value='".$rs_rinciandetail->getString('detail_no')."'>";
                                echo input_hidden_tag('unit_id', $unit_id);
                                echo input_hidden_tag('kode_kegiatan', $kode_kegiatan);

                                echo "</tr>";

                                $total_devplan += $rs_rinciandetail->getFloat('nilai_anggaran');
                                $total_budgeting += $rs_rinciandetail->getFloat('nilai_anggaran_budgeting');
                            }
                            ?>
                            <?php if($counter <= 0): ?>
                            <tr><td colspan="16" align="center">Tidak ada lagi komponen yang bisa ditarik</td></tr>
                            <?php endif; ?>
                            
                            <tr>
                                <td colspan="5"></td>
                                <td colspan="">Total Devplan = <?php echo number_format(round($total_devplan), 0, ',', '.'); ?></td>
                                <td></td>
                                <td colspan="5"></td>
                                <td colspan="">Total Budgeting = <?php echo number_format(round($total_budgeting), 0, ',', '.'); ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <br/>
            <?php
            if($counter > 0)
                echo submit_tag('Proses ke Penarikan', array('name' => 'proses', 'class' => 'btn btn-success btn-flat')) . '&nbsp;';
            ?>
        </div>
    </div>
    </form>

</section><!-- /.content -->

<script>
    $("#cekSemua").change(function () {
        $(".cek").prop('checked', $(this).prop("checked"));
    });
</script>