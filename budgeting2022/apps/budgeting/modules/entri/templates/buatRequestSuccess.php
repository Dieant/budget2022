<?php use_helper('I18N', 'Form', 'Object', 'Javascript', 'Validation') ?>
<section class="content">
    <?php include_partial('peneliti/list_messages'); ?>
    <div class="box box-primary box-solid">
        <div class="box-header with-border">
            <h3 class="box-title">Form Request Dinas</h3>
        </div>
        <div class="box-body">
            <?php echo form_tag('entri/buatRequestSave', array('multipart' => true, 'class' => 'form-horizontal')); ?>
            <div class="form-group">
                <label class="col-xs-3 control-label">User Id :</label>
                <div class="col-xs-9">
                    <?php
                    echo input_tag('user_id', $sf_user->getNamaLogin(), array('class' => 'form-control', 'readonly' => 'true', 'style' => 'width:100%'));
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-xs-3 control-label">SKPD :</label>
                <div class="col-xs-9">
                    <?php
                    $nama = $sf_user->getNamaLogin();

                    // print_r ($nama); exit;
                    $b = new Criteria;
                    $b->add(UserHandleV2Peer::USER_ID, $nama);
                    $es = UserHandleV2Peer::doSelectOne($b);
                    // $satuan_kerja = $es;
                    // $unit_kerja = Array();
                    // foreach ($satuan_kerja as $x) {
                    //     $unit_kerja[] = $x->getUnitId();
                    // }
                    $e = new Criteria();
                    $e->add(UnitKerjaPeer::UNIT_ID, $es->getUnitId());
                    // for ($i = 1; $i < count($unit_kerja); $i++) {
                    //     $e->addOr(UnitKerjaPeer::UNIT_ID, $unit_kerja[$i], criteria::ILIKE);
                    // }
                    if($nama != 'latihan')
                        $e->add(UnitKerjaPeer::UNIT_NAME, 'Latihan', criteria::NOT_EQUAL);
                    $e->addAscendingOrderByColumn(UnitKerjaPeer::UNIT_NAME);
                    $v = UnitKerjaPeer::doSelectOne($e);
                    // echo select_tag('unit_id', objects_for_select($v, $v->getUnitId(), $v->getUnitName(), null, array('include_custom' => '---Pilih SKPD---')), array('class' => 'form-control js-example-basic-single', 'style' => 'width:100%'));
                    //echo select_tag('unit_id', objects_for_select($unit_kerja, 'getUnitId', 'getUnitName', $unit_kerja, 'include_custom=---Pilih SKPD---'), array('class' => 'form-control js-example-basic-single', 'style' => 'width:100%'));
                    ?>
                    <select name="unit_id" class="form-control js-example-basic-single" id="unit_id" style => 'width:100%'>
                        <option value="" selected>---Pilih SKPD---</option>
                        <option value="<?php echo $v->getUnitId(); ?>" ><?php echo $v->getUnitName(); ?></option>
 
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-xs-3 control-label">Kegiatan :</label>
                <div class="col-xs-9">
                    <?php echo select_tag('kode_kegiatan', options_for_select(array(), '', null), array('id' => 'keg', 'class' => 'js-example-basic-multiple', 'style' => 'width:100%', 'multiple' => 'multiple')); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-xs-3 control-label">Tipe Request :</label>
                <div class="col-xs-9">
                    <?php
                    echo select_tag('tipe', options_for_select(array('0' => 'Buka Komponen', '1' => 'Buka Rekening', '2' => 'Buka PSP' ,'3' => 'Ganti Rekening'), null), array('class' => 'form-control'));
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-xs-3 control-label">Data Pendukung :<br>(Gunakan file xls, xlsx, zip, <br> rar, jpg, png, atau pdf)</label>
                <div class="col-xs-9">
                    <?php echo input_file_tag('file') ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-xs-3 control-label">Alasan :</label>
                <div class="col-xs-9">
                    <?php
                    echo textarea_tag('catatan', null, array('class' => 'form-control'));
                    ?>
                </div>
            </div>
            <!--
            <div class="form-group">
                <div class="col-xs-offset-3 col-xs-9">
                    <?php
                    echo checkbox_tag('khusus', 'Khusus', FALSE) . ' Request Khusus (jika tidak memiliki data pendukung request)';
                    ?>
                </div>
            </div>
            -->
            <div class="form-group">
                <label class="col-xs-3 control-label">&nbsp;</label>
                <div class="col-xs-9">
                    <input type="submit" name="submit" value="Submit" class="btn btn-primary btn-flat"/>
                </div>
            </div>
            <?php echo '</form>' ?>
        </div>
    </div>
</section>
<script>
    $(document).ready(function () {
        $(".js-example-basic-multiple").select2();
    });

    $("#unit_id").change(function () {
        var id = $(this).val();
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/entri/pilihKegiatan/id/" + id + ".html",
            context: document.body
        }).done(function (msg) {
            $('#keg').html(msg);
        });

    });
</script>