<?php use_helper('I18N', 'Date', 'Url', 'Javascript', 'Form', 'Object', 'Number', 'Validation') ?>
<?php
$unit_id = $rs_rinciandetail->getUnitId();
$kegiatan_code = $rs_rinciandetail->getKegiatanCode();
$detail_no = $rs_rinciandetail->getDetailNo();
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>Daftar Isian Rincian Komponen Kegiatan SKPD</h1>
</section>

<!-- Main content -->
<section class="content">
    <?php include_partial('entri/list_messages'); ?>
    <?php
//    fungsi warning lengkap
    $lelang = 0;
    $totNilaiAlokasi = 0;
    $totNilaiKontrak = 0;
    $totNilaiSwakelola = 0;
    $totNilaiHps = 0;
    $ceklelangselesaitidakaturanpembayaran = 0;

    if (sfConfig::get('app_fasilitas_cekeDelivery') == 'buka' && sfConfig::get('app_fasilitas_cekeProject') == 'buka') {
        if (sfConfig::get('app_fasilitas_cekServer') == 'buka') {
            $lelang = $rs_rinciandetail->getCekLelang($rs_rinciandetail->getUnitId(), $rs_rinciandetail->getKegiatanCode(), $rs_rinciandetail->getDetailNo(), $rs_rinciandetail->getNilaiAnggaran());
            $totNilaiHps = $rs_rinciandetail->getCekNilaiHPSKomponen($rs_rinciandetail->getUnitId(), $rs_rinciandetail->getKegiatanCode(), $rs_rinciandetail->getDetailNo());
            $ceklelangselesaitidakaturanpembayaran = $rs_rinciandetail->getCekLelangTidakAdaAturanPembayaran($rs_rinciandetail->getUnitId(), $rs_rinciandetail->getKegiatanCode(), $rs_rinciandetail->getDetailNo());
        }
        $totNilaiAlokasi = $rs_rinciandetail->getCekNilaiAlokasiProject($rs_rinciandetail->getUnitId(), $rs_rinciandetail->getKegiatanCode(), $rs_rinciandetail->getDetailNo());
        $totNilaiKontrak = $rs_rinciandetail->getCekNilaiKontrakDelivery2($rs_rinciandetail->getUnitId(), $rs_rinciandetail->getKegiatanCode(), $rs_rinciandetail->getDetailNo());
        $totNilaiSwakelola = $rs_rinciandetail->getCekNilaiSwakelolaDelivery2($rs_rinciandetail->getUnitId(), $rs_rinciandetail->getKegiatanCode(), $rs_rinciandetail->getDetailNo());

        if ($totNilaiAlokasi > 0 || $totNilaiSwakelola > 0 || $totNilaiKontrak > 0 || $lelang > 0 || $totNilaiHps > 0 || $ceklelangselesaitidakaturanpembayaran > 0) {
            ?>
            <div class="alert alert-danger alert-dismissable">
                <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                <?php
                if ($totNilaiAlokasi > 0) {
                    echo '<p>Nilai yang dialokasikan di eProject sebesar = Rp ' . number_format($totNilaiAlokasi, 0, ',', '.') . '</p>';
                }
                if ($totNilaiSwakelola > 0 || $totNilaiKontrak > 0) {
                    if ($totNilaiKontrak == 0) {
                        echo '<p>Nilai yang dialokasikan di eDelivery sebesar = Rp ' . number_format($totNilaiSwakelola, 0, ',', '.') . '</p>';
                    } else if ($totNilaiSwakelola == 0) {
                        echo '<p>Nilai yang dialokasikan di eDelivery sebesar = Rp ' . number_format($totNilaiKontrak, 0, ',', '.') . '</p>';
                    } else {
                        echo '<p>Nilai yang dialokasikan di eDelivery sebesar = Rp ' . number_format($totNilaiKontrak, 0, ',', '.') . '</p>';
                    }
                }
                if ($lelang > 0) {
                    echo '<p>Komponen ini dalam Proses Lelang</p>';
                }
                if ($totNilaiHps > 0) {
                    echo '<p>Nilai HPS Komponen sebesar = Rp ' . number_format($totNilaiHps, 0, ',', '.') . '</p>';
                }
                if ($ceklelangselesaitidakaturanpembayaran == 1) {
                    echo '<p>Proses Lelang  Selesai & Belum ada Aturan Pembayaran</p>';
                }
                ?>
            </div>
            <?php
        }
    }
//    fungsi warning lengkap
    ?>
    <div class="box box-primary box-solid">
        <div class="box-header with-border">
            <h3 class="box-title">Form Edit Komponen</h3>
        </div>
        <div class="box-body">
            <div id="sf_admin_container">
                <?php echo form_tag('entri/editKegiatan') ?>
                <table cellspacing="0" class="sf_admin_list">
                    <thead>  
                        <tr>
                            <th style="width: 19%"><b>Nama</b></th>
                            <th style="width: 1%">&nbsp;</th>
                            <th style="width: 80%"><b>Isian</b></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="sf_admin_row_0" align='right'>
                            <td>Kelompok Belanja</td>
                            <td align="center">:</td>
                            <td align="left">
                                <?php
                                $rekening_code = $rs_rinciandetail->getRekeningCode();
                                $belanja_code = substr($rekening_code, 0, 5);
                                $c = new Criteria();
                                $c->add(KelompokBelanjaPeer::BELANJA_CODE, $belanja_code);
                                $rs_belanja = KelompokBelanjaPeer::doSelectOne($c);
                                if ($rs_belanja) {
                                    echo $rs_belanja->getBelanjaName();
                                }
                                ?></td>
                        </tr>
                        <tr class="sf_admin_row_1" align='right'>
                            <td>Kode Rekening</td>
                            <td align="center">:</td>
                            <td align="left">
                                <?php
                                $c = new Criteria();
                                $c->add(RekeningPeer::REKENING_CODE, $rekening_code);
                                $rs_rekening = RekeningPeer::doSelectOne($c);
                                if ($rs_rekening) {
                                    $pajak_rekening = $rs_rekening->getRekeningPpn();
                                    echo $rekening_code . ' ' . $rs_rekening->getRekeningName();
                                }
                                ?></td>
                        </tr>
                        <tr class="sf_admin_row_0" align='right'>
                            <td>Komponen</td>
                            <td align="center">:</td>
                            <td align="left"><?php echo $rs_rinciandetail->getKomponenName(); ?></td>
                        </tr>
                        <tr class="sf_admin_row_1" align='right'>
                            <td>Harga</td>
                            <td align="center">:</td>
                            <?php
                            $komponen_id = $rs_rinciandetail->getKomponenId();
                            $komponen_harga = $rs_rinciandetail->getKomponenHargaAwal();
                            if ((substr($komponen_id, 0, 14) == '23.01.01.04.12') or ( substr($komponen_id, 0, 14) == '23.01.01.04.13') or ( substr($komponen_id, 0, 11) == '23.04.04.01')) {
                                ?>
                                <td align='left' ><?php
                                    if ($rs_rinciandetail->getSatuan() == '%') {
                                        echo $komponen_harga;
                                    } else {
                                        echo number_format($komponen_harga, 3, ',', '.');
                                    }echo input_hidden_tag('harga', $komponen_harga);
                                    ?></td>
                                <?php
                            } else {
                                ?>
                                <td align='left' ><?php
                                    if ($rs_rinciandetail->getSatuan() == '%') {
                                        echo $komponen_harga;
                                    } else {
                                        echo number_format($komponen_harga, 0, ',', '.');
                                    }echo input_hidden_tag('harga', $komponen_harga);
                                    ?> </td>
                                <?php
                            }
                            ?>
                        </tr>
                        <tr class="sf_admin_row_0" align='right'>
                            <td>Satuan</td>
                            <td align="center">:</td>
                            <td align="left"><?php echo $rs_rinciandetail->getSatuan(); ?></td>
                        </tr>
                        <tr class="sf_admin_row_1" align='right'>
                            <td>Pajak</td>
                            <td align="center">:</td>
                            <td align="left" ><?php
                                echo $rs_rinciandetail->getPajak() . '%';
                                echo input_hidden_tag('pajakx', $rs_rinciandetail->getPajak());
                                ?></td>
                        </tr>
                        <tr class="sf_admin_row_0" align='right'>
                            <td><span style="color:red;">*</span> Subtitle</td>
                            <td align="center">:</td>
                            <td align="left">
                                <?php
                                $nama_subtitle = $rs_rinciandetail->getSubtitle();
                                $spasi_nama_subtitle = trim($nama_subtitle);
                                $query = "select subtitle,sub_id from " . sfConfig::get('app_default_schema') . ".dinas_subtitle_indikator "
                                        . "where unit_id='" . $sf_params->get('unit') . "' and kegiatan_code='" . $sf_params->get('kegiatan') . "' and subtitle = '$spasi_nama_subtitle'";
                                //print_r($query);
                                $con = Propel::getConnection();
                                $stmt = $con->prepareStatement($query);
                                $rs1 = $stmt->executeQuery();
                                while ($rs1->next()) {
                                    $kode_sub = $rs1->getString('sub_id');
                                }
//                        echo select_tag('subtitle', objects_for_select($rs_subtitleindikator, 'getSubId', 'getSubtitle', $kode_sub, 'include_custom=---Pilih Subtitle---'), Array('onChange' => remote_function(Array('update' => 'sub1', 'url' => 'entri/pilihsubx?kegiatan_code=' . $sf_params->get('kegiatan') . '&unit_id=' . $sf_params->get('unit'), 'with' => "'b='+this.options[this.selectedIndex].value", 'loading' => "Element.show('indicator');Element.hide('sub1');", 'complete' => "Element.hide('indicator');Element.show('sub1');"))));

                                echo select_tag('subtitle', objects_for_select($rs_subtitleindikator, 'getSubId', 'getSubtitle', $kode_sub, 'include_custom=---Pilih Subtitle---'));
                                ?>
                            </td>
                        </tr>
                        <tr class="sf_admin_row_1" align='right'>
                            <td>Sub - Subtitle</td>
                            <td align="center">:</td>
                            <td align="left">
                                <div id="indicator" style="display:none;" align="center"><dt>&nbsp;</dt><dd><b>Mohon Tunggu </b><?php echo image_tag('loading.gif', array('align' => 'absmiddle')) ?></dd></div>
                                <?php
                                if ($rs_rinciandetail->getKodeSub()) {
                                    echo select_tag('sub', options_for_select(array($rs_rinciandetail->getKodeSub() => $rs_rinciandetail->getSub()), $rs_rinciandetail->getKodeSub(), 'include_custom=---Pilih Subtitle Dulu---'), Array('id' => 'sub1'));
                                } elseif (!$rs_rinciandetail->getKodeSub()) {
                                    echo select_tag('sub', options_for_select(array(), '', 'include_custom=---Pilih Subtitle Dulu---'), Array('id' => 'sub1'));
                                }
                                ?>
                            </td>
                        </tr>           
                        <?php
                        $tipe = $rs_rinciandetail->getTipe();
                        //irul 2may 2014 awal-comment  untuk menampilka opsi lokasi pada estimasi pembuatan separator jalan
                        $estimasi_opsi_lokasi = array('');
                        //irul 2may 2014 awal-comment  untuk menampilka opsi lokasi pada estimasi pembuatan separator jalan
                        if (($tipe == 'FISIK') || ($sf_params->get('lokasi')) || in_array($rs_rinciandetail->getKomponenId(), $estimasi_opsi_lokasi)) {
                            //ditutup sementara spy dinas tidak bisa edit lokasi
                            ?>
                            <tr class="sf_admin_row_0" align='right' valign="top">
                                <td>Lokasi</td>
                                <td align="center">:</td>
                                <td align="left" style="padding: 10px">
                                    <?php
                                    echo textarea_tag('lokasi', $rs_rinciandetail->getDetailName(), 'size=150; readonly=true'); //submit_tag('cari', 'name=cari').'<br><font color="magenta">[untuk multi lokasi, tambahkan lokasi dengan klik "cari" lagi]<br></font>'; 
                                    echo '<br> Usulan dari : ';
                                    echo select_tag('jasmas', objects_for_select($rs_jasmas, 'getKodeJasmas', 'getNama', $rs_rinciandetail->getKecamatan(), 'include_custom=--Pilih--'));
                                    echo '<br />';

                                    if (!$rs_rinciandetail->getDetailName() == '') {
                                        $kode_lokasi = '';
                                        $banyak = 0;
                                        $c = new Criteria();
                                        $c->add(VLokasiPeer::NAMA, $rs_rinciandetail->getDetailName(), Criteria::ILIKE);
                                        $rs_lokasi = VLokasiPeer::doSelectOne($c);
                                        if ($rs_lokasi) {
                                            $kode_lokasi = $rs_lokasi->getKode();
                                        }

                                        $sql = new Criteria();
                                        if ($kode_lokasi) {
                                            $kod = '%' . $kode_lokasi . '%';
                                            $sql->add(HistoryPekerjaanPeer::KODE, strtr($kod, '*', '%'), Criteria::ILIKE);

                                            $sql->addAscendingOrderByColumn(HistoryPekerjaanPeer::TAHUN);
                                            $sqls = HistoryPekerjaanPeer::doSelect($sql);
                                            ?>
                                            <table cellspacing="0" class="sf_admin_list">
                                                <thead>
                                                    <tr>
                                                        <th>Tahun</th>
                                                        <th>Lokasi</th>
                                                        <th>Nilai</th>
                                                        <th>volume</th>
                                                    <tr>
                                                </thead>
                                                <?php
                                                $tahun = '';
                                                $total_lokasi = 0;
                                                foreach ($sqls as $vx) {
                                                    if ($tahun == '') {
                                                        $tahun = $vx->getTahun();
                                                    }
                                                    if (($tahun <> $vx->getTahun()) && ($tahun <> '')) {
                                                        $tahun = $vx->getTahun();
                                                        ?>
                                                        <tbody>
                                                            <tr class="sf_admin_row_1" align='right'>
                                                                <td>&nbsp;</td>
                                                                <td align="right">Total :</td>
                                                                <td align="right"> <?php echo number_format($total_lokasi, 0, ',', '.'); ?></td>
                                                                <td> <?php $total_lokasi = 0 ?></td>
                                                            </tr>
                                                            <tr><td colspan="4"></td></tr>
                                                            <tr>
                                                                <td> <?php echo $vx->getTahun(); ?></td>
                                                                <td> <?php echo $vx->getLokasi(); ?></td>
                                                                <td align="right"> <?php echo number_format($vx->getNilai(), 0, ',', '.'); ?></td>
                                                                <td> <?php echo $vx->getVolume(); ?></td>
                                                            </tr>
                                                            <?php
                                                            $total_lokasi+=$vx->getNilai();
                                                        } else {
                                                            ?>
                                                            <tr>
                                                                <td> <?php echo $vx->getTahun(); //$rsPeta->getInt('tahun')                                                    ?></td>
                                                                <td> <?php echo $vx->getLokasi(); //$rsPeta->getString('lokasi')                                                    ?></td>
                                                                <td align="right"> <?php echo number_format($vx->getNilai(), 0, ',', '.'); //$rsPeta->getString('nilai')                                                    ?></td>
                                                                <td> <?php echo $vx->getVolume(); //$rsPeta->getString('nomor')                                                ?></td>
                                                            </tr>
                                                            <?php
                                                            $total_lokasi+=$vx->getNilai();
                                                        }
                                                        if ($vx->getNilai() > 0) {
                                                            $banyak+=1;
                                                        }
                                                    }
                                                    ?>
                                                    <tr class="sf_admin_row_1" align='right'>
                                                        <td> </td>
                                                        <td align="right">Total :</td>
                                                        <td align="right"> <?php echo number_format($total_lokasi, 0, ',', '.'); //$rsPeta->getString('nilai')                                                    ?></td>
                                                        <td> <?php $total_lokasi = 0 ?></td>
                                                    </tr>
                                                    <tr><td colspan="4">&nbsp;</td></tr>
                                                    <?php
                                                    $query = "select * from " . sfConfig::get('app_default_schema') . ".rincian_detail where detail_name ilike '".$rs_rinciandetail->getDetailName()."'";
                                                    $con = Propel::getConnection();
                                                    $stmt = $con->prepareStatement($query);
                                                    $rs_rincianlokasi = $stmt->executeQuery();
                                                    while ($rs_rincianlokasi->next()) {
                                                        ?>
                                                        <tr>
                                                            <td><?php echo sfConfig::get('app_tahun_default') ?></td>
                                                            <td> <?php echo $rs_rincianlokasi->getString('komponen_name') . ' ' . $rs_rincianlokasi->getString('detail_name') ?></td>
                                                            <td align="right"> 
                                                                <?php
                                                                $pajak = $rs_rincianlokasi->getString('pajak');
                                                                $harga = $rs_rincianlokasi->getString('komponen_harga_awal');
                                                                $volume = $rs_rincianlokasi->getString('volume');
                                                                $nilai = ($volume * $harga * (100 + $pajak) / 100);
                                                                echo number_format($nilai, 0, ',', '.');
                                                                ?></td>
                                                            <td> <?php echo $rs_rincianlokasi->getString('keterangan_koefisien'); ?></td>
                                                        </tr>
                                                        <?php
                                                        if ($volume > 0) {
                                                            $banyak+=1;
                                                        }
                                                        $total_lokasi+=$nilai;
                                                    }
                                                    ?>
                                                    <tr class="sf_admin_row_1" align='right'>
                                                        <td> </td>
                                                        <td align="right">Total :</td>
                                                        <td align="right"> <?php echo number_format($total_lokasi, 0, ',', '.'); //$rsPeta->getString('nilai')                                                    ?></td>
                                                        <td> <?php $total_lokasi = 0 ?></td>
                                                    </tr>
                                                    <tr><td colspan="4">&nbsp;</td></tr>
                                                </tbody>

                                            </table>
                                            <?php
                                        }
                                        if ($banyak <= 0) {
                                            echo '<span style="color:green;"> <small> :. Belum pernah ada Pekerjaan .: </small></span>';
                                            echo input_hidden_tag('status', 'ok');
                                        }

                                        if ($banyak > 0) {
                                            echo '<span style="color:red;">  <small> :. Pekerjaan Pada Lokasi Ini Sudah Ada, Tidak Dapat Disimpan .: </small></span>';
                                            echo input_hidden_tag('status', 'fisik');
                                        }
                                        ?>
                                    </td>
                                </tr>                                
                                <?php
                            }
                            ?>
                            <tr class="sf_admin_row_0" align='right'>
                                <td><span style="color:red;">*</span> Kecamatan</td>
                                <td align="center">:</td>
                                <td align="left">
                                    <?php
                                    $kec = new Criteria();
                                    $kec->addAscendingOrderByColumn(KecamatanPeer::NAMA);
                                    $rs_kec = KecamatanPeer::doSelect($kec);
                                    $nama_kecamatan_kel = $rs_kec;
                                    if ($rs_rinciandetail->getLokasiKecamatan()) {
                                        $kec_ini = new Criteria();
                                        $kec_ini->add(KecamatanPeer::NAMA, $rs_rinciandetail->getLokasiKecamatan());
                                        $rs_kec_ini = KecamatanPeer::doSelectOne($kec_ini);
                                        echo select_tag('kecamatan', objects_for_select($nama_kecamatan_kel, 'getId', 'getNama', $rs_kec_ini->getId(), 'include_custom=---Pilih Kecamatan---'));
                                    } else {
                                        echo select_tag('kecamatan', objects_for_select($nama_kecamatan_kel, 'getId', 'getNama', '', 'include_custom=---Pilih Kecamatan---'));
                                    }
                                    ?>
                                    <span style="color:red;">* untuk Komponen FISIK, diharuskan untuk mengisi data Kecamatan.</span>
                                </td>
                            </tr>				
                            <tr class="sf_admin_row_1" align='right'>
                                <td>Kelurahan</td>
                                <td align="center">:</td>
                                <td align="left">
                                    <div id="indicator" style="display:none;" align="center"><dt>&nbsp;</dt><dd><b>Mohon Tunggu </b><?php echo image_tag('loading.gif', array('align' => 'absmiddle')) ?></dd></div>
                                    <?php
                                    if ($rs_rinciandetail->getLokasiKelurahan() && $rs_rinciandetail->getLokasiKecamatan()) {
                                        $kec_kel_ini = new Criteria();
                                        $kec_kel_ini->add(KelurahanKecamatanPeer::NAMA_KELURAHAN, $rs_rinciandetail->getLokasiKelurahan());
                                        $kec_kel_ini->add(KelurahanKecamatanPeer::NAMA_KECAMATAN, $rs_rinciandetail->getLokasiKecamatan());
                                        $rs_kec_kel_ini = KelurahanKecamatanPeer::doSelectOne($kec_kel_ini);
                                        echo select_tag('kelurahan', options_for_select(array($rs_kec_kel_ini->getOid() => $rs_rinciandetail->getLokasiKelurahan()), $rs_kec_kel_ini->getOid(), 'include_custom=---Pilih Kecamatan Dulu---'), Array('id' => 'kelurahan1'));
                                    } else {
                                        echo select_tag('kelurahan', options_for_select(array(), '', 'include_custom=---Pilih Kecamatan Dulu---'), Array('id' => 'kelurahan1'));
                                    }
                                    ?>
                                    <span style="color:red;">* untuk Komponen FISIK, diharuskan untuk mengisi data Kelurahan.</span>
                                </td>
                            </tr>
                            <?php
                        } else {
                            ?>
                            <tr class="sf_admin_row_0" align='right' valign="top">
                                <td>Keterangan</td>
                                <td align="center">:</td>
                                <td align='left'>
                                    <?php echo textarea_tag('keterangan', $rs_rinciandetail->getDetailName(), 'size=140x3') ?> </td>
                            </tr>
                            <?php
                        }
                        ?>
                        <tr class="sf_admin_row_1" align='right' valign="top">
                            <td><span style="color:red;">*</span>Volume
                            </td>
                            <td align="center">:</td>
                            <td align="left">                        
                                <b>Gunakan tanda titik (.) untuk menyatakan bilangan koma</b><br/><br/>
                                <?php
                                $keterangan_koefisien = $rs_rinciandetail->getKeteranganKoefisien();
                                $pisah_kali = explode('X', $keterangan_koefisien);



                                for ($i = 0; $i < 4; $i++) {
                                    $satuan = '';
                                    $volume = '';
                                    $nama_input = 'vol' . ($i + 1);
                                    $nama_pilih = 'volume' . ($i + 1);
                                    ;
                                    if (!empty($pisah_kali[$i])) {
                                        $pisah_spasi = explode(' ', $pisah_kali[$i]);
                                        $j = 0;

                                        for ($s = 0; $s < count($pisah_spasi); $s++) {
                                            if ($pisah_spasi[$s] != NULL) {
                                                if ($j == 0) {
                                                    $volume = $pisah_spasi[$s];
                                                    $j++;
                                                } elseif ($j == 1) {
                                                    $satuan = $pisah_spasi[$s];
                                                    $j++;
                                                } else {
                                                    $satuan.=' ' . $pisah_spasi[$s];
                                                }
                                            }
                                        }
                                    }
                                    if ($i !== 3) {
                                        echo input_tag($nama_input, $volume, array('onChange' => 'hitungTotal()')) . ' ' . select_tag($nama_pilih, objects_for_select($rs_satuan, 'getSatuanName', 'getSatuanName', $satuan, 'include_custom=---Pilih Satuan--')) . '<br />  X <br />';
                                    } else {
                                        echo input_tag($nama_input, $volume, array('onChange' => 'hitungTotal()')) . ' ' . select_tag($nama_pilih, objects_for_select($rs_satuan, 'getSatuanName', 'getSatuanName', $satuan, 'include_custom=---Pilih Satuan--'));
                                    }
                                }
                                ?>
                                <br/>
                            </td>
                        </tr>
        <!--                <tr class="sf_admin_row_1" align='right' valign="top">
                            <td>Total</td>
                            <td align="center">:</td>
                            <td align="left"><?php echo input_tag('total', '', array('readonly' => 'true')) ?></td>
                        </tr>-->
                        <?php if (sfConfig::get('app_tahap_edit') <> 'murni') { ?>
                            <!-- irul 18maret 2014 - simpan catatan -->
                            <tr class="sf_admin_row_0" align='right' valign="top">
                                <td><span style="color:red;">*</span> Catatan Pergeseran Anggaran<br/>(Catatan SKPD)</td>
                                <td align="center">:</td>
                                <td align='left'>
                                    <?php echo textarea_tag('catatan', $rs_rinciandetail->getNoteSkpd(), array('style' => 'width:550px', 'size' => '100px')) ?><br/>
                                    <i style="color: red">Minimal 15 Karakter</i>
                                </td>
                            </tr>
                            <!-- irul 18maret 2014 - simpan catatan -->
                        <?php } ?>
                        <?php
                        $unit_id = $rs_rinciandetail->getUnitId();
                        if ($unit_id == '1800' || $unit_id == '0300') {
                            ?>
                            <tr class="sf_admin_row_1" align='right' valign="top">
                                <td>Komponen BLUD</td>
                                <td align="center">:</td>
                                <td align="left">
                                    <?php
                                    $nilai_blud = $rs_rinciandetail->getIsBlud();
                                    if ($nilai_blud == true) {
                                        echo checkbox_tag('blud', 1, TRUE);
                                    } else {
                                        echo checkbox_tag('blud');
                                    }
                                    ?>
                                    <font style="color: green"> *Centang jika termasuk komponen BLUD</font> </td>
                            </tr>    
                        <?php }
                        ?>
                    </tbody>
                    <tfoot>
                        <tr class="sf_admin_row_0" align='right' valign="top">
                            <td>&nbsp; </td>
                            <td>
                                <?php
                                echo input_hidden_tag('kegiatan', $sf_params->get('kegiatan'));
                                echo input_hidden_tag('unit', $sf_params->get('unit'));
                                echo input_hidden_tag('id', $sf_params->get('id'));
                                echo input_hidden_tag('referer', $sf_request->getAttribute('referer'));
                                ?>
                            </td>
                            <td>
                                <?php
                                if ($lelang == 1 || $ceklelangselesaitidakaturanpembayaran == 1) {
                                    echo button_to('kembali', '#', array('onClick' => "javascript:history.back()"));
                                } else if ($lelang == 0 && $ceklelangselesaitidakaturanpembayaran == 0) {
                                    echo submit_tag('simpan', 'name=simpan') . ' ' . button_to('kembali', '#', array('onClick' => "javascript:history.back()"));
                                }
                                ?>
                            </td>
                        </tr>
                    </tfoot>
                </table>
                <?php echo '</form>'; ?>
            </div>
        </div>
    </div>
</section>

<script>
    $("#subtitle").change(function() {
        var id = $(this).val();
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/entri/pilihsubx/kegiatan_code/<?php echo $sf_params->get('kegiatan') ?>/unit_id/<?php echo $sf_params->get('unit') ?>/b/" + id + ".html",
            context: document.body
        }).done(function(msg) {
            $('#sub1').html(msg);
        });

    });

    $("#kecamatan").change(function() {
        var id = $(this).val();
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/entri/pilihKelurahan/b/" + id + ".html",
            context: document.body
        }).done(function(msg) {
            $('#kelurahan1').html(msg);
        });

    });

    function hitungTotal() {
        var harga = $('harga').value;
        var pajakx = $('pajakx').value;
        var vol1 = $('vol1').value;
        var vol2 = $('vol2').value;
        var vol3 = $('vol3').value;
        var vol4 = $('vol4').value;
        var volume;

        if (vol1 !== '' || vol2 !== '' || vol3 !== '' || vol4 !== '') {
            if (vol2 === '') {
                vol2 = 1;
                volume = vol1 * vol2;
            } else if (vol2 !== '') {
                volume = vol1 * vol2;
            }
            if (vol3 === '') {
                vol3 = 1;
                volume = volume * vol3;
            } else if (vol3 !== '') {
                volume = vol1 * vol2 * vol3;
            }
            if (vol4 === '') {
                vol4 = 1;
                volume = volume * vol4;
            } else if (vol4 !== '') {
                volume = vol1 * vol2 * vol3 * vol4;
            }
        }
// alert(pajakx);
        if (pajakx === 10) {
            var hitung = (harga * volume * 1.1);
        } else if (pajakx === 0) {
            var hitung = (harga * volume * 1);
        }
//  alert(harga*volume*(100+10));
        $('total').value = hitung;

    }
</script>
