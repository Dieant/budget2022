<td class='tombol_actions'>
    <?php
    $kegiatan_code = $master_kegiatan->getKodeKegiatan();
    $unit_id = $master_kegiatan->getUnitId();

    $arr_hidden_edit = array(''); //kode_kegiatan

    $c = new Criteria();
    $c->add(DinasRincianPeer::KEGIATAN_CODE, $kegiatan_code);
    $c->add(DinasRincianPeer::UNIT_ID, $unit_id);
    $rs_rincian = DinasRincianPeer::doSelectOne($c);
    if ($rs_rincian) {
            $posisi = $rs_rincian->getRincianLevel();
            $lock_kegiatan = $rs_rincian->getLock();
        }
        $user = $sf_user->getNamaLogin();
        if ($user == $master_kegiatan->getUserId() && $sf_user->hasCredential('dinas')) {
            if(in_array($kegiatan_code, $arr_hidden_edit))
            {

            }
            else
            {
                echo link_to('<i class="fa fa-edit"></i>',  'entri/edit?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan() . '&tahap=' . (isset($filters['tahap']) ? $filters['tahap'] : ''), array('alt' => __('edit'), 'title' => __('edit'), 'class' => 'btn btn-outline-primary btn-sm'));
                echo link_to('<i class="fa fa-exclamation-triangle"></i>', 'report/penandaPrioritas?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan(), array('alt' => __('Penanda Prioritas'), 'title' => __('Detail Kegiatan'), 'class' => 'btn btn-outline-warning btn-sm'));  
            }
            
            if (!(isset($filters['tahap']) && $filters['tahap'] <> '')) {
                echo link_to('<i class="fa fa-th-list"></i>', 'report/tampillaporanasli?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan(), array('alt' => __('laporan perbandingan asli-draft'), 'title' => __('laporan perbandingan asli-draft'), 'class' => 'btn btn-outline-info btn-sm'));
                
                if(sfConfig::get('app_tahap_edit') == 'pak' || sfConfig::get('app_tahap_edit') == 'murni') {                   
                    echo link_to('<i class="fa fa-th-list"></i>', 'report/tampillaporanBukuputih?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan(), array('alt' => __('laporan perbandingan asli-bukuputih'), 'title' => __('laporan perbandingan murni bukuputih-bukubiru'), 'class' => 'btn btn-outline-secondary btn-sm'));
                }
                 // echo link_to('<i class="fa fa-hand-holding-usd"></i>', 'report/editRasionalisasi?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan(), array('alt' => __('Nilai Rasionalisasi'), 'title' => __('Nilai Rasionalisasi'), 'class' => 'btn btn-outline-info btn-sm'));
            } else {
                echo link_to('<i class="fa fa-th-list"></i>', 'report/tampillaporanaslisemua?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan() . '&tahap=' . $filters['tahap'], array('alt' => __('laporan perbandingan rka-draft'), 'title' => __('laporan perbandingan rka-draft'), 'class' => 'btn btn-outline-info btn-sm'));
                echo link_to('<i class="fa fa-list-alt"></i>', 'report/tampillaporanrevisi?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan() . '&tahap=' . $filters['tahap'], array('alt' => __('laporan perbandingan revisi'), 'title' => __('laporan perbandingan revisi'), 'class' => 'btn btn-outline-secondary btn-sm'));  
            }
        } else {
            if (!(isset($filters['tahap']) && $filters['tahap'] <> '')) {
                if ($sf_user->hasCredential('kpa')) {
                    $c = new Criteria();
                    $c->add(DinasMasterKegiatanPeer::USER_ID_KPA, $user);
                    $c->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                    $c->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kegiatan_code);
                    if ($rs = DinasMasterKegiatanPeer::doSelectOne($c)) {
                        echo link_to('<i class="fa fa-th-list"></i>', 'report/tampillaporanasli?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan(), array('alt' => __('Laporan Perbandingan RKA-Draft'), 'title' => __('Laporan Perbandingan RKA-Draft'), 'class' => 'btn btn-outline-secondary btn-sm'));
                    }
                } elseif ($sf_user->hasCredential('pa')) {
                    echo link_to('<i class="fa fa-th-list"></i>', 'report/tampillaporanasli?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan(), array('alt' => __('Laporan Perbandingan RKA-Draft'), 'title' => __('Laporan Perbandingan RKA-Draft'), 'class' => 'btn btn-outline-secondary btn-sm'));
                    $c = new Criteria();
                    $c->add(DinasMasterKegiatanPeer::USER_ID_PPTK, $user);
                    $c->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                    $c->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kegiatan_code);
                    if ($rs = DinasMasterKegiatanPeer::doSelectOne($c)) {
                        echo link_to('<i class="fa fa-th-list"></i>', 'report/tampillaporanasli?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan(), array('alt' => __('Laporan Perbandingan RKA-Draft'), 'title' => __('Laporan Perbandingan RKA-Draft'), 'class' => 'btn btn-outline-info btn-sm'));
                    }

                    if(sfConfig::get('app_tahap_edit') == 'pak') {                   
                    echo link_to('<i class="fa fa-th-list"></i>', 'report/tampillaporanBukuputih?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan(), array('alt' => __('laporan perbandingan asli-bukuputih'), 'title' => __('laporan perbandingan pak bukuputih-bukubiru'), 'class' => 'btn btn-outline-secondary btn-sm'));
                    }

                } elseif ($sf_user->hasCredential('pptk')) {
                    $c = new Criteria();
                    $c->add(DinasMasterKegiatanPeer::USER_ID_PPTK, $user);
                    $c->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                    $c->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kegiatan_code);
                    if ($rs = DinasMasterKegiatanPeer::doSelectOne($c)) {
                        echo link_to('<i class="fa fa-th-list"></i>', 'report/tampillaporanasli?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan(), array('alt' => __('Laporan Perbandingan RKA-Draft'), 'title' => __('Laporan Perbandingan RKA-Draft'), 'class' => 'btn btn-outline-secondary btn-sm'));
                    }
                }
            } else {
                echo link_to('<i class="fa fa-th-list"></i>', 'report/tampillaporanaslisemua?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan() . '&tahap=' . $filters['tahap'], array('alt' => __('laporan perbandingan rka-draft'), 'title' => __('laporan perbandingan rka-draft'), 'class' => 'btn btn-outline-info btn-sm'));
            }
        }

        if (!(isset($filters['tahap']) && $filters['tahap'] <> '')) {
            if ($master_kegiatan->getUserId() == '') {
                // echo link_to('<i class="fa fa-fast-forward"></i>', 'entri/ambilKegiatan?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan() . '&user_id=' . $sf_user->getNamaLogin(), array('alt' => __('Ambil'), 'title' => __('Ambil'), 'class' => 'btn btn-outline-primary btn-sm', 'class' => 'btn btn-outline-success btn-sm'));            
            }
            if ($sf_user->getNamaLogin() == $master_kegiatan->getUserId()) {
                if(sfConfig::get('app_tahap_edit') == 'murni') {
                  echo link_to('<i class="fa fa-chalkboard-teacher"></i>', 'report/menuViewDevplan?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan() . '&tahap=' . (isset($filters['tahap']) ? $filters['tahap'] : ''), array('alt' => __('Melihat Komponen Devplan'), 'title' => __('Melihat Komponen Devplan'), 'class' => 'btn btn-outline-primary btn-sm'));
                    // echo link_to(image_tag('/sf/sf_admin/images/hurufd.png', array('alt' => __('Tarik SSH Devplan'), 'title' => __('Tarik SSH Devplan (Murni)'), 'class' => 'btn btn-outline-primary btn-sm')), 'entri/menuTarikDevplan?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan());
                    // echo link_to('<i class="fa fa-maxcdn"></i>', 'entri/menuViewMusrenbang?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan(), array('alt' => __('Melihat Komponen Musrenbang'), 'title' => __('Melihat Komponen Musrenbang ' . (sfConfig::get('app_tahun_default') - 1)), 'class' => 'btn btn-outline-primary btn-sm', 'style' => 'width:35px'));
                }
                // echo link_to('<i class="fa fa-fast-backward"></i>', 'entri/lepasKegiatan?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan() . '&user_id=' . $sf_user->getNamaLogin(), array('alt' => __('Lepas'), 'title' => __('Lepas'), 'class' => 'btn btn-outline-primary btn-sm', 'class' => 'btn btn-outline-danger btn-sm'));
                // echo link_to('<i class="fa fa-hand-holding-usd"></i>', 'report/editRasionalisasi?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan(), array('alt' => __('Edit Nilai Rasionalisasi'), 'title' => __('Edit Nilai Rasionalisasi'), 'class' => 'btn btn-outline-info btn-sm'));
            }
        }
        ?>
</td>