<?php
// auto-generated by sfPropelAdmin
// date: 2008/05/13 12:49:58
?>
<?php use_helper('Object') ?>

<div class="sf_admin_filters">
    <?php echo form_tag('entri/sublist', array('method' => 'get')) ?>

    <fieldset>
        <h2><?php echo __('Cari') ?></h2>
        <div class="form-row">
            <label for="sub_kegiatan_id"><?php echo __('Kode:') ?></label>
            <div class="content" style="min-height: 25px">
                <?php
                echo input_tag('filters[sub_kegiatan_id]', isset($filters['sub_kegiatan_id']) ? $filters['sub_kegiatan_id'] : null, array(
                    'size' => 15,
                ))
                ?>
            </div>
        </div>

        <div class="form-row">
            <label for="sub_kegiatan_name"><?php echo __('Nama:') ?></label>
            <div class="content" style="min-height: 25px">
                <?php
                echo input_tag('filters[sub_kegiatan_name]', isset($filters['sub_kegiatan_name']) ? $filters['sub_kegiatan_name'] : null, array(
                    'size' => NULL,
                ))
                ?>

<?php echo button_to(__('reset'), 'entri/sublist?filter=filter', 'class=sf_admin_action_reset_filter') ?>
<?php echo submit_tag(__('cari'), 'name=filter class=sf_admin_action_filter') ?>
            </div>
        </div>

    </fieldset>

</form>
</div>
